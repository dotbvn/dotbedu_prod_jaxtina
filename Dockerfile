FROM --platform=linux/amd64 trankhanhtoan/apache-php71
WORKDIR /var/www/html
COPY --chown=www-data:www-data . .
COPY --chown=www-data:www-data ./config.sample.php ./config.php
ADD ./cron /etc/cron.d/
RUN chmod +x /etc/cron.d/cron
RUN crontab /etc/cron.d/cron
