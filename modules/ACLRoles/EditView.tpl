{*

*}


<script>
    {literal}
    function set_focus() {
        document.getElementById('name').focus();
    }

    $(document).ready(function () {
        $('.btn_selectDashboard').live('click', function(){
            clickSelectDashboard($(this));
        });
    });


    function clickSelectDashboard(thisButton){
        thisBtn = thisButton;
        window.top.App.drawer.open({
            layout: 'selection-list',
            context: {
                module: 'Dashboards'
            }
        }, function (model) {
            if (!_.isEmpty(model)) {
                $('.dashboard_id').val(model.id);
                $('.dashboard').val(model.name);
            }
        });
    }

    {/literal}
</script>

<form method='POST' name='EditView' action='index.php'>
    {dotb_csrf_form_token}
    <TABLE width='100%' border='0' cellpadding=0 cellspacing=0 class="actionsContainer">
        <tbody>
        <tr>
            <td>
                <input type='hidden' name='record' value='{$ROLE.id}'>
                <input type='hidden' name='module' value='ACLRoles'>
                <input type='hidden' name='action' value='Save'>
                <input type='hidden' name='isduplicate' value='{$ISDUPLICATE}'>
                <input type='hidden' name='return_record' value='{$RETURN.record}'>
                <input type='hidden' name='return_action' value='{$RETURN.action}'>
                <input type='hidden' name='return_module' value='{$RETURN.module}'> &nbsp;
                {dotb_action_menu id="roleEditActions" class="clickMenu fancymenu" buttons=$ACTION_MENU flat=true}
            </td>
        </tr>
        </tbody>
    </table>
    <TABLE width='100%' class="edit view" border='0' cellpadding=0 cellspacing=0>
        <TR>
            <td scope="row" align='right'>{$MOD.LBL_NAME}:<span class="required">{$APP.LBL_REQUIRED_SYMBOL}</span></td>
            <td>
                <input id='name' name='name' type='text' value='{$ROLE.name}'>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td scope="row" align='right'>{$MOD.LBL_DESCRIPTION}:</td>
            <td><textarea name='description' cols="80" rows="8">{$ROLE.description}</textarea></td>
        </tr>
        <tr>
            <td scope="row" align='right'>{$MOD.LBL_DASHBOARD}:</td>
            <td>
                <input type="text" name="dashboard" value="{$DASHBOARD_NAME}" class="dashboard">
                <input type="hidden" name="dashboard_id" class="dashboard_id" value="{$ROLE.DASHBOARD_ID}"/>
                <span class="id-ff multiple">
                <button type="button" name="btn_selectDashboard_name" tabindex="0" title="" class="button btn_selectDashboard" value="Select Dashboard" onclick="">
                    <img src="themes/default/images/id-ff-select.png">
                </button>
                </span>
            </td>
        </tr>
        <tr>
            <td scope="row" align='right'>{$MOD.LBL_LOCK_DATED}:</td>
            <td>
                <input tabindex="0" type="hidden" name="is_lock_dated" value="0">
                <input tabindex="0" {if $ROLE.is_lock_dated == 1} checked {/if} type="checkbox" id="is_lock_dated" name="is_lock_dated" value="1">
            </td>
        </tr>
        <tr>
            <td scope="row" align='right'>{$MOD.LBL_IS_ADMIN_GBS}:</td>
            <td>
                <input tabindex="0" type="hidden" name="is_admin_gbs" value="0">
                <input tabindex="0" {if $ROLE.is_admin_gbs == 1} checked {/if} type="checkbox" id="is_admin_gbs" name="is_admin_gbs" value="1">
            </td>
        </tr>
        <tr>
            <td scope="row" align='right'>{$MOD.LBL_SPECIAL_ROLE}: <img border="0" onclick="return DOTB.util.showHelpTips(this,'{$MOD.LBL_SPECIAL_ROLE_DES}');" src="themes/RacerX/images/helpInline.png"></td>
            <td>
                <input id='special_role' name='special_role' type='text' value='{$ROLE.special_role}'>
            </td>
        </tr>
    </table>

</form>
<script type="text/javascript">
    addToValidate('EditView', 'name', 'varchar', true, '{$MOD.LBL_NAME}');
</script>
