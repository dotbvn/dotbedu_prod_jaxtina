<?php


$dictionary['ACLRole'] = array('table' => 'acl_roles', 'comment' => 'ACL Role definition'
    , 'fields' => array(
        'id' =>
        array(
            'name' => 'id',
            'vname' => 'LBL_ID',
            'required' => true,
            'type' => 'id',
            'reportable' => false,
            'comment' => 'Unique identifier'
        ),
        'date_entered' =>
        array(
            'name' => 'date_entered',
            'vname' => 'LBL_DATE_ENTERED',
            'type' => 'datetime',
            'required' => true,
            'comment' => 'Date record created'
        ),
        'date_modified' =>
        array(
            'name' => 'date_modified',
            'vname' => 'LBL_DATE_MODIFIED',
            'type' => 'datetime',
            'required' => true,
            'comment' => 'Date record last modified'
        ),
        'modified_user_id' =>
        array(
            'name' => 'modified_user_id',
            'rname' => 'user_name',
            'id_name' => 'modified_user_id',
            'vname' => 'LBL_MODIFIED',
            'type' => 'assigned_user_name',
            'table' => 'modified_user_id_users',
            'isnull' => 'false',
            'dbType' => 'id',
            'required' => false,
            'len' => 36,
            'reportable' => true,
            'comment' => 'User who last modified record'
        ),
        'created_by' =>
        array(
            'name' => 'created_by',
            'rname' => 'user_name',
            'id_name' => 'created_by',
            'vname' => 'LBL_CREATED',
            'type' => 'assigned_user_name',
            'table' => 'created_by_users',
            'isnull' => 'false',
            'dbType' => 'id',
            'len' => 36,
            'comment' => 'User who created record'
        ),
        'name' =>
        array(
            'name' => 'name',
            'type' => 'varchar',
            'vname' => 'LBL_NAME',
            'len' => 150,
            'comment' => 'The role name'
        ),
        'description' =>
        array(
            'name' => 'description',
            'vname' => 'LBL_DESCRIPTION',
            'type' => 'text',
            'comment' => 'The role description'
        ),
        'deleted' =>
        array(
            'name' => 'deleted',
            'vname' => 'LBL_DELETED',
            'type' => 'bool',
            'reportable' => false,
            'comment' => 'Record deletion indicator'
        ),
        'users' =>
        array(
            'name' => 'users',
            'type' => 'link',
            'relationship' => 'acl_roles_users',
            'link_file' => 'modules/ACLRoles/UserLink.php',
            'link_class' => 'UserLink',
            'source' => 'non-db',
            'vname' => 'LBL_USERS',
        ),
        'actions' =>
        array(
            'name' => 'actions',
            'type' => 'link',
            'relationship' => 'acl_roles_actions',
            'source' => 'non-db',
            'vname' => 'LBL_USERS',
        ),
        'acl_role_sets' => array(
            'name' => 'acl_role_sets',
            'type' => 'link',
            'source' => 'non-db',
            'relationship' => 'acl_role_sets_acl_roles',
        ),
        'dashboard_id' => array(
            'name' => 'dashboard_id',
            'vname' => 'LBL_DASHBOARD_ID',
            'type' => 'id',
            'reportable' => false,
        ),
        'dashboard_name' => array(
            'name' => 'dashboard_name',
            'rname' => 'name',
            'id_name' => 'dashboard_id',
            'vname' => 'LBL_DASHBOARD',
            'type' => 'relate',
            'table' => 'dashboards',
            'isnull' => 'true',
            'module' => 'Dashboards',
            'dbType' => 'varchar',
            'len' => 'id',
            'source' => 'non-db',
            'reportable' => false,
            'massupdate' => true,
        ),
        'dashboard_link' => array(
            'name' => 'dashboard_link',
            'type' => 'link',
            'relationship' => 'role_dashboard',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_ROLE_DASHBOARD',
            'reportable' => false
        ),
        //----Lock Back Date-----
        'is_lock_dated' =>
        array (
            'name' => 'is_lock_dated',
            'vname' => 'LBL_LOCK_DATED',
            'type' => 'bool',
            'default' => 1,
            'massupdate' => true,
        ),
        'is_admin_gbs' =>
        array (
            'name' => 'is_admin_gbs',
            'vname' => 'LBL_IS_ADMIN_GBS',
            'type' => 'bool',
            'default' => 0,
            'massupdate' => true,
        ),
        'special_role' =>
        array(
            'name' => 'special_role',
            'type' => 'varchar',
            'vname' => 'LBL_SPECIAL_ROLE',
            'len' => 150,
            'comment' => 'The role Teacher/TA'
        ),
    ),
    'acls' => array('DotbACLDeveloperOrAdmin' => array('aclModule' => 'Users')),
    'indices' => array(
        array('name' => 'aclrolespk', 'type' => 'primary', 'fields' => array('id')),
        array('name' => 'idx_aclrole_id_del', 'type' => 'index', 'fields' => array('id', 'deleted')),
        array('name' => 'idx_aclrole_name', 'type' => 'index', 'fields' => array('name'))

    ),
    'relationships' => array(
        'role_dashboard' => array(
            'lhs_module' => 'ACLRoles',
            'lhs_table' => 'acl_roles',
            'lhs_key' => 'dashboard_id',
            'rhs_module' => 'Dashboards',
            'rhs_table' => 'dashboards',
            'rhs_key' => 'id',
            'relationship_type' => 'one-to-many'
        ),
//        'bmessage_aclroles' => array(
//            'lhs_module' => 'ACLRoles',
//            'lhs_table' => 'acl_roles',
//            'lhs_key' => 'id',
//            'rhs_module' => 'BMessage',
//            'rhs_table' => 'bmessage',
//            'rhs_key' => 'aclrole_id',
//            'relationship_type' => 'one-to-many'
//        ),
    )
);

$dictionary['ACLRoleSet'] = array(
    'table' => 'acl_role_sets',
    'fields' => array(
        'hash' => array(
            'name' => 'hash',
            'type' => 'varchar',
            'len' => 32,
            'isnull' => false,
        ),
        'acl_roles' => array(
            'name' => 'acl_roles',
            'type' => 'link',
            'source' => 'non-db',
            'relationship' => 'acl_role_sets_acl_roles',
            'duplicate_merge' => 'disabled',
        ),
    ),
    'indices' => array(
        array(
            'name' => 'idx_acl_role_sets_hash',
            'type' => 'unique',
            'fields' => array('hash'),
        ),
    ),
);

VardefManager::createVardef('ACLRoleSets', 'ACLRoleSet');
