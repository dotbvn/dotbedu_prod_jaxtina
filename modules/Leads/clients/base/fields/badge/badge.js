
/**
 * @class View.Fields.Base.Leads.BadgeField
 * @alias DOTB.App.view.fields.BaseLeadsBadgeField
 * @extends View.Fields.Base.BaseField
 */
({
    /**
     * @inheritdoc
     *
     * This field doesn't support `showNoData`.
     */
    showNoData: false,

    events: {
        'click [data-action=convert]': 'convertLead',
        'click [data-action=mark_visited]': 'markVisited'
    },

    /**
     * @inheritdoc
     *
     * The badge is always a readonly field.
     */
    initialize: function(options) {
        options.def.readonly = true;
        app.view.Field.prototype.initialize.call(this, options);
    },

    /**
     * Kick off convert lead process.
     */
    convertLead: function() {
        if (!_.contains(['PT/Demo', 'Deposit'], this.model.attributes.status)) {
            app.alert.show('message-id', {
                    level: 'error',
                    title: app.lang.get('LBL_ERR_CONVERT_TITLE', 'Leads'),
                    messages: app.lang.get('LBL_ERR_CONVERT_MESSAGE', 'Leads'),
                    autoClose: true
                });
        }else{
            var model = app.data.createBean(this.model.module);

            model.set(app.utils.deepCopy(this.model.attributes));

            app.drawer.open({
                layout : 'convert',
                context: {
                    forceNew: true,
                    skipFetch: true,
                    module: this.model.module,
                    leadsModel: model
                }
            });
        }

    },
    markVisited: function() {
        var preferences = {};
        preferences['lead_id'] = this.model.id;

        app.alert.show('wait', {
            level: 'process',
            title: 'Waiting'
        });

        DOTB.App.api.call('update', DOTB.App.api.buildURL('Leads', 'mark-visited'), preferences, {
            success: function (data) {
                var data = $.parseJSON(data);
                if (data) {
                    app.alert.show('message-id', {
                        level: 'success',
                        title: app.lang.get('LBL_MARK_VISITED_SUCCESS','Leads') + data.last_visited,
                        autoClose: true
                    });
                } else {
                    app.alert.show('message-id', {
                        level: 'error',
                        title: app.lang.get('LBL_MARK_VISITED_SUCCESS','Leads'),
                        autoClose: true
                    });
                }
                app.alert.dismiss('wait');
				app.controller.context.reloadData();
            },
            error: function (data) {
                console.log(data);
                app.alert.show('message-id', {
                        level: 'error',
                        title: app.lang.get('LBL_MARK_VISITED_SUCCESS','Leads'),
                        autoClose: true
                    });
                app.alert.dismiss('wait');
            }
        });
    }
})
