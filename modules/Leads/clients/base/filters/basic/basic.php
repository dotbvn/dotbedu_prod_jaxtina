<?php


$viewdefs['Leads']['base']['filter']['basic'] = array(
    'create' => true,
    'quicksearch_field' => array('name'),
    'quicksearch_priority' => 1,
    'quicksearch_split_terms' => false,
    'filters' => array(
        array(
            'id' => 'all_records',
            'name' => 'LBL_LISTVIEW_FILTER_ALL',
            'filter_definition' => array(),
            'editable' => false
        ),

        array(
            'id' => 'new',
            'name' => 'LBL_NEW_FILTER',
            'filter_definition' => array(
                'status' => 'new'
            ),
            'editable' => false
        ),

        array(
            'id' => 'to_do_list',
            'name' => 'LBL_TO_DO_FILTER',
            'filter_definition' => array(
                '$or' => array(
                    array(
                        'status' => array(
                            '$in' => array('Deposit', 'PT/Demo', 'Ready to PT', 'Ready to Demo', 'Appointment'),
                        ),
                    ),
                    array(
                        '$and' => array(
                            array(
                                '$favorite' => '',
                            ),
                            array(
                                'status' => array(
                                    '$in' => array('New', 'In Process', 'Deposit', 'PT/Demo', 'Ready to PT', 'Ready to Demo', 'Appointment'),
                                ),
                            ),
                        )
                    )
                ),

            ),
            'editable' => false
        ),

        array(
            'id' => '7_day_list',
            'name' => 'LBL_7_DAY_RECORDS',
            'filter_definition' => array(
                array(
                    'date_modified' => array(
                        '$dateRange' => 'last_7_days',
                    ),
                ),

                array(
                    'status' => array(
                        '$in' => array('New', 'In Process', 'Deposit', 'PT/Demo', 'Ready to PT', 'Ready to Demo', 'Appointment'),
                    ),
                )


            ),
            'editable' => false
        ),
        array(
              'id' => 'assigned_to_me',
              'name' => 'LBL_ASSIGNED_TO_ME',
              'filter_definition' => array(
                  '$owner' => '',
              ),
              'editable' => false
          ),
        array(
            'id'                => 'favorites',
            'name'              => 'LBL_FAVORITES',
            'filter_definition' => array(
                '$favorite' => '',
            ),
            'editable'          => false
        ),

        /*    array(
                'id' => 'recently_viewed',
                'name' => 'LBL_RECENTLY_VIEWED',
                'filter_definition' => array(
                    '$tracker' => '-7 DAY',
                ),
                'editable' => false
            ),
            array(
                'id' => 'recently_created',
                'name' => 'LBL_NEW_RECORDS',
                'filter_definition' => array(
                    'date_entered' => array(
                        '$dateRange' => 'last_7_days',
                    ),
                ),
                'editable' => false
            ),*/
    ),
);
