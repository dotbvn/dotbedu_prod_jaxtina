({
    extendsFrom: 'RecordlistView',
    initialize: function(options) {
        this._super("initialize", [options]);
        this.context.on('list:composeSMS:fire',this.composeSMS,this);
        this.context.on('list:composeZalo:fire',this.composeZalo,this);
    },
    composeSMS:function(){
        openPopupSendSMSForMulti('Leads','');
    },
    composeZalo:function(){
        openPopupSendZaloForMulti('Leads','');
    },
})
