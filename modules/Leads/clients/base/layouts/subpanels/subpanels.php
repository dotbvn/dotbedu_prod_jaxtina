<?php

$viewdefs['Leads']['base']['layout']['subpanels'] = array(
    'components' => array(
        array(
            'layout' => 'subpanel',
            'label' => 'LBL_CALLS_SUBPANEL_TITLE',
            'context' => array(
                'link' => 'calls',
            ),
        ),
        // Hide meeting subpanel by HP
       /* array(
            'layout' => 'subpanel',
            'label' => 'LBL_MEETINGS_SUBPANEL_TITLE_2',
            'context' => array(
                'link' => 'meetings',
            ),
        ),*/
        // Hide default tasks subpanel by phgiahannn
        /*array(
            'layout' => 'subpanel',
            'label' => 'LBL_TASKS_SUBPANEL_TITLE',
            'context' => array(
                'link' => 'tasks',
            ),
        ),*/
        array(
            'layout' => 'subpanel',
            'label' => 'LBL_TASKS_SUBPANEL_TITLE',
            'context' => array(
                'link' => 'normal_tasks_link',
            ),
        ),
        array(
            'layout' => 'subpanel',
            'label' => 'LBL_NOTES_SUBPANEL_TITLE',
            'context' => array(
                'link' => 'notes',
            ),
        ),
        array(
            'layout' => 'subpanel',
            'label' => 'LBL_CAMPAIGN_LIST_SUBPANEL_TITLE',
            'context' => array(
                'link' => 'campaigns',
            ),
        ),
        array(
            'layout' => 'subpanel',
            'label' => 'LBL_EMAILS_SUBPANEL_TITLE',
            'override_subpanel_list_view' => 'subpanel-for-leads-archived-emails',
            'context' => array(
                'link' => 'archived_emails',
            ),
        ),
//        array(
//            'layout' => 'subpanel',
//            'label' => 'LBL_DATAPRIVACY_SUBPANEL_TITLE',
//            'context' => array(
//                'link' => 'dataprivacy',
//            ),
//        ),
    ),
);
