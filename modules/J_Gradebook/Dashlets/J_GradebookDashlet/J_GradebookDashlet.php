<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_Gradebook/J_Gradebook.php');

class J_GradebookDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Gradebook/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Gradebook');

        $this->searchFields = $dashletData['J_GradebookDashlet']['searchFields'];
        $this->columns = $dashletData['J_GradebookDashlet']['columns'];

        $this->seedBean = new J_Gradebook();        
    }
}