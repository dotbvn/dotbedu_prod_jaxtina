<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_GradebookDashlet'] = array('module'		=> 'J_Gradebook',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Gradebook'), 
                                          'description' => 'A customizable view into J_Gradebook',
                                          'icon'        => 'icon_J_Gradebook_32.gif',
                                          'category'    => 'Module Views');