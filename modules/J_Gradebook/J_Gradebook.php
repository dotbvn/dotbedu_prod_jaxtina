<?php
/**
* THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
*/
require_once('modules/J_Gradebook/J_Gradebook_dotb.php');
class J_Gradebook extends J_Gradebook_dotb {
    var $class;
    var $gradebookConfig;
    var $students;
    var $config;
    var $gradebookDetail;
    var $fullDetail;

    public function __construct()
    {
        parent::__construct();
        $this->addVisibilityStrategy('TeacherVisibility');
    }

    public function _constructDefault($new_config = false, $student_id = '') {
        $this->class = new J_Class();
        $this->class->retrieve($this->j_class_j_gradebook_1j_class_ida);
        if(empty($student_id)){
            $sql = "SELECT DISTINCT IFNULL(l1.id, '') id,
            IFNULL(l1.full_student_name, '') name, IFNULL(l1.first_name, '') first_name,
            IFNULL(l1.last_name, '') last_name, IFNULL(l1.contact_id, '') contact_id,
            l1.birthdate birthdate, IFNULL(l1.phone_mobile, '') phone_mobile, IFNULL(l1.nick_name, '') nick_name,
            IFNULL(l2.email_address, '') email1, IFNULL(l3.full_user_name, '') assigned_user_name,
            IFNULL(j_gradebook.id, '') primaryid, j_gradebook.date_modified date_modified,
            j_gradebook.date_entered date_entered,j_gradebook.date_confirm date_confirm, 
            IFNULL(l1.gender, '') gender, l1.age age, IFNULL(l1.primary_address_street, '') primary_address_street
            FROM j_gradebook INNER JOIN j_gradebook_contacts_1_c l1_1 ON j_gradebook.id = l1_1.j_gradebook_contacts_1j_gradebook_ida AND l1_1.deleted = 0
            INNER JOIN contacts l1 ON l1.id = l1_1.j_gradebook_contacts_1contacts_idb AND l1.deleted = 0
            LEFT JOIN email_addr_bean_rel l2_1 ON l1.id = l2_1.bean_id AND l2_1.deleted = 0 AND l2_1.bean_module = 'Contacts' AND l2_1.primary_address = 1
            LEFT JOIN email_addresses l2 ON l2.id = l2_1.email_address_id AND l2.deleted = 0
            LEFT JOIN users l3 ON l1.assigned_user_id = l3.id AND l3.deleted = 0
            WHERE (j_gradebook.id = '{$this->id}') AND j_gradebook.deleted = 0
            ORDER BY first_name ASC, id";
            $result = $GLOBALS['db']->query($sql);
            while($row = $GLOBALS['db']->fetchByAssoc($result)){
                $student = new stdClass();
                foreach ($row as $key => $_value) $student->$key = $_value;
                $this->students[$row['id']] = $student;
            }
        }else //Load 1 Student
            $this->students = array($student_id => $student_id);

        if($new_config){
            $gradebookConfig = new J_GradebookConfig();
            $gradebookConfig->retrieve($this->gradebook_config_id);
            if(!empty($gradebookConfig->content) && !empty($gradebookConfig->id)){
                $this->grade_config = $gradebookConfig->content;
                //Save new config when Reload Config
                $this->save();
            }
            //Update Gradebook Name
            if(!empty($gradebookConfig->name) && ($gradebookConfig->name != $this->name)){
                $GLOBALS['db']->query("UPDATE j_gradebook SET name = '{$gradebookConfig->name}' WHERE id = '{$this->id}'");
            }
            //Update Gradebookdetail
            $content = json_decode(html_entity_decode($this->grade_config), true);
            $sql_gbdt_update = "SELECT id FROM j_gradebookdetail WHERE gradebook_id = '{$this->id}' AND deleted = 0";
            $result    = $GLOBALS['db']->query($sql_gbdt_update);
            while($row = $GLOBALS['db']->fetchByAssoc($result)) {
                $detail_new = new J_GradebookDetail();
                $detail_cur = BeanFactory::getBean('J_GradebookDetail',$row['id']);
                foreach ($detail_new->field_defs as $keyField => $aFieldName){
                    if($keyField != 'id')
                        $detail_new->$keyField = $detail_cur->$keyField;
                }
                $content_detail = json_decode(html_entity_decode($detail_new->content),true);
                foreach ($content as $key => $value){
                    if($value['visible'] !== 1){
                        $col = 'col_'.$key;
                        $detail_new->$col = null;
                        unset($content_detail[$key]);
                    }
                }
                $detail_new->content = json_encode($content_detail, JSON_UNESCAPED_UNICODE);
                $detail_new->save();
                $detail_cur->deleted = 1;
                $detail_cur->save();
            }
        }
        $this->config = json_decode(html_entity_decode($this->grade_config),true);
        $this->loadGradebookDetail($student_id);
    }

    /**
    * load bang diem chi tiet cac hoc vien
    *
    * @param bool $refresh
    *
    * @by Lap Nguyen
    */
    public function loadGradeContent($new_config = false, $refresh = false) {
        global $timedate;
        //Load config
        $this->_constructDefault($new_config);
        $keys = array();
        $note = $this->noticeStudentNotInGradebook(1);
        $html = '';
        $html .= $note;
        $html .= "<table class='table-border hover stripe' id='config_content' style=' overflow-x: scroll; width: inherit;'>
        <thead class='thead-light'>
        <tr>
        <td style='border-bottom: #b2b2b2 double;' width='1%' ><b>#</b></td>
        <td style='border-bottom: #b2b2b2 double;' width='5%' ><b>".translate('LBL_PICTURE_FILE','Contacts')."</b></td>
        <td style='border-bottom: #b2b2b2 double;' width='7%' ><b>".translate('LBL_CONTACT_ID','Contacts')."</b></td>
        <td style='border-bottom: #b2b2b2 double;' width='10%' ><b>".translate('LBL_LIST_CONTACT_NAME','Contacts')."</b></td>
        <td style='border-bottom: #b2b2b2 double;' width='5%' ><b>".translate('LBL_NICK_NAME','Contacts')."</b></td>
        <td style='border-bottom: #b2b2b2 double;border-right: #b2b2b2 double;' width='5%' ><b>".translate('LBL_BIRTHDATE','Contacts')."</b></td>";
        //Add Group Point
        $groups          = array();
        $lastGroupName  = '#***#';
        $keyGroup       = 0;
        foreach($this->config as $key => $params){
            if(!$params['visible']) continue;
            if($lastGroupName != $params['group']){
                $keyGroup++;
                $lastGroupName = $params['group'];
            }
            $groups[$keyGroup]['name']    = $params['group'];
            $groups[$keyGroup]['colspan']+=1;
        }

        //        foreach($groups as $key => $group)
        //            $html .=  "<td class='td-mark' colspan='{$group['colspan']}'><b>{$group['name']}</b></td>";
        //        $html .= "</tr><tr >";

        foreach($this->config as $key => $params){
            if(!$params['visible']) continue;
            if($params['type'] != 'comment'){
                $max_mark = " (".$params['max_mark'].")";
                $html.= "<td style='border-bottom: #b2b2b2 double;font-size:15px' width='5%' class ='td-mark'>";
                $html.= "<b class='more_tip' tip='Alias: ".$params['alias']."<br>Max mark: ".$params['max_mark']."<br>Formula: ".$params['formula']."'> ".$params['label'].'</b>';
                $html.= "<div style='height: 10px;'>";
                if(empty($params['formula'])){
                    //button import
                    $url = "window.open(\"index.php?module=J_Gradebook&action=handelAjaxsInputMark&dotb_body_only=true&type=ajaxDownloadTemplate&mark={$params['alias']}&id={$this->id}\")";
                    $html .="<i style='cursor: pointer; font-size:12px;' class='far fa-upload more_tip' tip='Import Excel' data-mark='{$params['alias']}' template_link='{$url}' onclick='showUploadMark(this)'></i>";
                }
                $html.= "</div>";
                //Custom Button Calculate Homework - Attendance
                if(!empty($params['custom_btn_label']) && !empty($params['custom_btn_function']))
                    $html .= "<br><button type='button' onclick='{$params['custom_btn_function']}(\"{$params['alias']}\");' style='line-height: 10px;' class='button'>{$params['custom_btn_label']}</button>";
                $html .= "</td>";
            }else
                $html .= "<td style='border-bottom: #b2b2b2 double;' width='15%'><b>".$params['label']."</b></td>";
        }
        $html .= "</thead><tbody> ";

        $no = 0;
        foreach($this->students as $student_id => $student) {
            $mark_progress = $this->getMarkProgress($student_id);
            $student_mark = $this->gradebookDetail[$student_id];
            $no++;

            $picture = '<a href="javascript:DOTB.image.lightbox(\'upload/origin/'.$student->picture.'\')"><img src="upload/resize/'.$student->picture.'" style="height: 80px;"></a>';
            if(empty($student->picture))
                $picture = '<img src="themes/default/images/noimage.png" style="height: 60px;clip-path: circle(50% at 50% 50%);">';
            $tr = "<tr>
            <td class = 'center table-active'>{$no}</td>
            <td class='table-active'>$picture</td>
            <td class='table-active'>{$student->contact_id}</td>
            <td class='table-active'><span class = 'student_name'>{$student->name}</span>
            <input name='student_id[]' value = '{$student->id}' type='hidden' >
            </td>
            <td class='table-active'>{$student->nick_name}</td>
            <td style='border-right: #cecece double;' class='table-active'>{$timedate->to_display_date($student->birthdate,false)}</td>";

            foreach($this->config as $key => $params) {
                if(!$params['visible']) continue;
                //Lock cac bang diem Progress
                $hardReadonly = false;
                if($this->type == 'Progress' && !empty($params['custom_btn_label']) && !empty($params['custom_btn_function'])) $hardReadonly = true;
                if($params['type'] != 'comment'){
                    $_mark = $student_mark[$key];
                    if($params['type'] != 'band')
                        $_mark = format_number($student_mark[$key],2,2);


                    $tr.= "<td class='td-mark'>
                    <input onClick='this.select();' name='{$key}[]'
                    id = '{$student_id}-{$key}'
                    config-data='".($_mark ? $_mark : 0)."'
                    config-max='{$params['max_mark']}'
                    config-readonly='{$params['readonly']}'
                    config-formula='{$params['formula']}'
                    config-type='{$params['type']}'
                    config-options='{$params['options']}'
                    config-alias='{$key}'
                    config-mark-progress='{$mark_progress}'
                    value = '".($_mark ? $_mark : 0 )."' class = 'input_mark'
                    size = '10'
                    ".(($params['readonly'] || $hardReadonly)  ?"readonly":"")."
                    ".($key == 'AA' ? " type='hidden'" : " type='text'")."/>
                    ".($key == 'AA' ? "<span class='final_result'>$_mark</span>" : "")."
                    </td>" ;
                }else{
                    if(strlen($student_mark[$key]) > 30) {
                        $cm = "<span class='value_teacher_".$params['name']."' title='{$student_mark[$key]}'>".(mb_substr($student_mark[$key],0,200,'UTF-8')."...")."</span>";
                    } else if(strlen($student_mark[$key]) > 0) {
                        $cm = "<span class='value_teacher_".$params['name']."' title='{$student_mark[$key]}'>".$student_mark[$key]."</span>";
                    } else {
                        $cm = "<span class='value_teacher_".$params['name']."' title='{$student_mark[$key]}'>--None--</span>";
                    }
                    $tr .= "<td class='comment' config-name='{$params['name']}' style=\"cursor:pointer\">
                    <input type='hidden' name='key_teacher_".$params['name']."[]' value = '".json_encode($student_mark['comment_key'])."'>
                    $cm
                    <input type='hidden' name='value_teacher_".$params['name']."[]' value = '{$student_mark[$key]}'>
                    </td>";
                }

                if($no==1)
                    $keys[] =  $key;

            }
            $tr .= "</tr>";
            $html.= $tr;
        }
        $html .= "</tbody>
        </table>
        <input type='hidden' name = 'key' value='".(json_encode($keys))."'>
        <input type='hidden' id = 'grade-reload' name = 'grade-reload' value='".(($new_config) ? '1' : '0')."'>
        <input type='hidden' id = 'gradebook_config_id' name = 'gradebook_config_id' value='".$this->gradebook_config_id."'>
        <input type='hidden' id = 'gradebook_progress' name = 'gradebook_progress' value='".$this->type."'>
        ";
        return $html;
    }

    public function noticeStudentNotInGradebook($getDetails = 0){
        // Warning students chưa add vào gradebook
        $res_std = $this->getStudentListInClass();
        if($res_std['count_std'] > 0){
            $note = "<div class='alert alert-warning' role='alert'><i class='fad fa-exclamation-triangle'></i> ";
            $note .= $res_std['count_std'].translate('LBL_STUDENT_ALERT_ADD','J_Gradebook');
            if($res_std['count_std'] == 1) $note =  str_replace("students have", "student has", $note);
            $note .= "</div>";
        }
        return $note;
    }

    public function getArrayGradebookChange ($class){
        global $db;
        $return_arr = array();
        $loop=-1;
        $p = $this->type;
        $sql = " SELECT  IFNULL(l1.type, '') type FROM j_gradebook l1
        INNER JOIN j_class_j_gradebook_1_c l1_1 ON l1.id = l1_1.j_class_j_gradebook_1j_gradebook_idb AND l1_1.deleted = 0
        WHERE l1_1.j_class_j_gradebook_1j_class_ida = '{$class}'
        AND l1.deleted = 0 AND l1.type = 'Overall'";
        $result = $db->getOne($sql);
        $return_arr = $this->getGradebookChange($return_arr,$p,$loop,$class);
        if(!empty($result)){
            $return_arr[] = 'Overall';
        }
        return json_encode($return_arr);
    }

    public function getGradebookChange ($result,$p,$loop,$class){
        //query formula to add arr...
        global $db;
        $arr = array();
        $sql = "
        SELECT  IFNULL(l1.grade_config, '') grade_config,
        IFNULL(l1.type, '') type
        FROM    j_gradebook l1
        INNER JOIN j_class_j_gradebook_1_c l1_1
        ON l1.id = l1_1.j_class_j_gradebook_1j_gradebook_idb
        AND l1_1.deleted = 0
        WHERE l1_1.j_class_j_gradebook_1j_class_ida = '{$class}'
        AND l1.deleted = 0 AND l1.type != 'Overall'";
        $result_sql = $db->query($sql);
        while($row = $db->fetchByAssoc($result_sql)){
            $content = json_decode(html_entity_decode($row['grade_config']), true);
            $formula = ' ';
            foreach ($content as $key=>$value){
                $formula .= $value['formula'];
            }
            $arr[$row['type']]= $formula;
        }
        unset($arr[$p]);
        foreach ($arr as $key => $value){
            if(strpos($value, $p)){
                $result[]=$key;
            }
        }
        $loop++;
        if($loop<count($result))
            return $this->getGradebookChange($result,$result[$loop],$loop,$class);
        else return $result;
    }

    public function getMarkProgress($student_id){
        global $db;
        $return = array();

        $sql1 = "SELECT
        IFNULL(l1.final_result, '') final_result,
        IFNULL(l1_1.type, '') type,
        IFNULL(l1.col_A, '') A, IFNULL(l1.col_B, '') B, IFNULL(l1.col_C, '') C, IFNULL(l1.col_D, '') D, IFNULL(l1.col_E, '') E,
        IFNULL(l1.col_F, '') F, IFNULL(l1.col_G, '') G, IFNULL(l1.col_H, '') H, IFNULL(l1.col_I, '') I, IFNULL(l1.col_J, '') J,
        IFNULL(l1.col_K, '') K, IFNULL(l1.col_L, '') L, IFNULL(l1.col_M, '') M, IFNULL(l1.col_N, '') N, IFNULL(l1.col_O, '') O,
        IFNULL(l1.col_P, '') P, IFNULL(l1.col_Q, '') Q, IFNULL(l1.col_R, '') R, IFNULL(l1.col_S, '') S, IFNULL(l1.col_T, '') T,
        IFNULL(l1.col_U, '') U, IFNULL(l1.col_V, '') V, IFNULL(l1.col_W, '') W, IFNULL(l1.col_X, '') X, IFNULL(l1.col_Y, '') Y,
        IFNULL(l1.col_Z, '') Z
        FROM  j_gradebookdetail l1
        INNER JOIN j_gradebook l1_1 ON l1_1.id = l1.gradebook_id AND l1_1.deleted = 0
        WHERE student_id = '{$student_id}' AND j_class_id IN (SELECT j_class_j_gradebook_1j_class_ida FROM j_class_j_gradebook_1_c WHERE j_class_j_gradebook_1j_gradebook_idb = '{$this->id}' AND deleted = 0)
        AND l1.deleted = 0";
        $result = $db->query($sql1);
        while($row = $db->fetchByAssoc($result)){
            $return[$row['type']] = array();
            $return[$row['type']]['final'] = $row['final_result'];
            foreach (range('A','Z') as $value){
                $test = $row[$value];
                if(!empty($row[$value]) && preg_match('/^[0-9]+\.[0-9]{2}/',$row[$value]))
                    $return[$row['type']][$value] = $row[$value];
            }
        }
        return json_encode($return);
    }

    /**
    * lay diem chi tiet cua mot hoc vien dùng trong Apps
    */
    public function getDetailForStudent($student_id) {
        $this->_constructDefault(false, $student_id);
        $student_mark = $this->fullDetail[$student_id];
        $gradebook              = array();
        $gradebook['name']      = $this->name;
        $gradebook['type']      = $this->type;
        $gradebook['date_input']= $this->date_input;
        $gradebook['date_confirm']= $this->date_confirm;
        $gradebook['status']    = $this->status;
        $gradebook['result']    = $student_mark['final_result'];
        $gradebook['conclusion']= $student_mark['certificate_level'];

        foreach($this->config as $key => $params) {
            if(!$params['visible']) continue;
            $gbDetail= array();
            $gbDetail['type']      = $params['type'];
            $gbDetail['label']     = $params['label'];
            $gbDetail['alias']     = $params['alias'];
            $gbDetail['max_mark']  = $params['max_mark'];
            if($params['type'] != 'comment'){
                if($params['type'] == 'band')
                    $gbDetail['result'] = $student_mark['column'][$key];
                else
                    $gbDetail['result'] = format_number($student_mark['column'][$key],2,2);
            }else
                $gbDetail['result'] = $student_mark['column'][$key];

            $gradebook['details'][]        = array_map('strval', $gbDetail);
        }
        return $gradebook;
    }

    /**
    * lay diem chi tiet cua cac hoc vien
    */
    function loadGradebookDetail($student_id = '') {
        //Load 1 student
        if(!empty($student_id)){
            $ext2 = "AND student_id = '$student_id'";
        }
        $sql = "SELECT IFNULL(gradebook_id,'') gradebook_id,
        IFNULL(id,'') gradebookdetail_id,
        IFNULL(student_id,'') student_id,
        IFNULL(content,'') content,
        IFNULL(col_A,'') col_A,
        IFNULL(col_B,'') col_B,
        IFNULL(col_C,'') col_C,
        IFNULL(col_D,'') col_D,
        IFNULL(col_E,'') col_E,
        IFNULL(col_F,'') col_F,
        IFNULL(col_G,'') col_G,
        IFNULL(col_H,'') col_H,
        IFNULL(col_I,'') col_I,
        IFNULL(col_J,'') col_J,
        IFNULL(col_K,'') col_K,
        IFNULL(col_L,'') col_L,
        IFNULL(col_M,'') col_M,
        IFNULL(col_N,'') col_N,
        IFNULL(col_O,'') col_O,
        IFNULL(col_P,'') col_P,
        IFNULL(col_Q,'') col_Q,
        IFNULL(col_R,'') col_R,
        IFNULL(col_S,'') col_S,
        IFNULL(col_T,'') col_T,
        IFNULL(col_U,'') col_U,
        IFNULL(col_V,'') col_V,
        IFNULL(col_W,'') col_W,
        IFNULL(col_X,'') col_X,
        IFNULL(col_Y,'') col_Y,
        IFNULL(col_Z,'') col_Z,
        IFNULL(final_result,'') final_result,
        IFNULL(certificate_type,'') certificate_type,
        IFNULL(certificate_level,'') certificate_level
        FROM j_gradebookdetail WHERE deleted = 0 AND gradebook_id = '{$this->id}' $ext2";
        $this->gradebookDetail  = array();
        $this->fullDetail       = array();
        $result     = $GLOBALS['db']->query($sql);
        $countGrade = 0;
        while($row  = $GLOBALS['db']->fetchByAssoc($result)){
            $countGrade++;
            foreach(range('A','Z') as $letter)
                $this->gradebookDetail[$row['student_id']][$letter] = $row['col_'.$letter];
            $this->fullDetail[$row['student_id']]['gradebookdetail_id'] = $row['gradebookdetail_id'];
            $this->fullDetail[$row['student_id']]['column']             = $this->gradebookDetail[$row['student_id']];
            $this->fullDetail[$row['student_id']]['final_result']       = $row['final_result'];
            $this->fullDetail[$row['student_id']]['certificate_type']   = $row['certificate_type'];
            $this->fullDetail[$row['student_id']]['certificate_level']  = $row['certificate_level'];
        }
    }

    function getGradebookDetailView($return_js = 0) {
        global $timedate;
        $this->_constructDefault();
        $html="";
        if(!$return_js)
            $html .= "<div id='report_results'>";

        $note = $this->noticeStudentNotInGradebook();

        $html .= $note."
        <table id = 'markdetail_content' class = 'mark-table-border' width='100%'>
        <thead>
        <tr>
        <td width='5%' rowspan='2'><input type='button' name='add_student_btn' id='add_student_btn' class='button primary' value='".translate('LBL_ADD_STUDENT','J_Gradebook')."'></td>
        <td width='3%' rowspan='2'><b>#</b></td>
        <td width='7%' rowspan='2' style=\"text-align:left;\"><b>".translate('LBL_PICTURE_FILE','Contacts')."</b></td>
        <td width='10%' rowspan='2' style=\"text-align:left;\"><b>".translate('LBL_CONTACT_ID','Contacts')."</b></td>
        <td width='10%' rowspan='2' style=\"text-align:left;\"><b>".translate('LBL_LIST_CONTACT_NAME','Contacts')."</b></td>
        <td width='5%' rowspan='2' style=\"text-align:left;\"><b>".translate('LBL_NICK_NAME','Contacts')."</b></td>
        <td width='7%' rowspan='2' style=\"text-align:left;\"><b>".translate('LBL_BIRTHDATE','Contacts')."</b></td>";
        //Add Group Point
        $groups          = array();
        $lastGroupName  = '#***#';
        $keyGroup       = 0;
        foreach($this->config as $key => $params){
            if(!$params['visible']) continue;
            if($lastGroupName != $params['group']){
                $keyGroup++;
                $lastGroupName = $params['group'];
            }
            $groups[$keyGroup]['name']    = $params['group'];
            $groups[$keyGroup]['colspan']+=1;
        }

        foreach($groups as $key => $group)
            $html .=  "<td class='td-mark' colspan='{$group['colspan']}'><b>{$group['name']}</b></td>";
        $html .= '</tr><tr>';

        foreach($this->config as $key => $params){
            if(!$params['visible']) continue;
            if($params['type'] != 'comment'){
                $html.= "<td width='5%' class ='td-mark'> <b> ".$params['label'].'</b>';
                $html.= "<center><div class='more' style='width: fit-content;'><span  style='margin: 5px; color:#FF0000;font-weight:bold;cursor:pointer;'>[?]</span>";
                $html.= "<div class='moreinfo_tip' class='moreinfo_tip' style='border:red solid 2px;background-color:#FFC;padding:10px;font-size:11px;position:absolute;z-index:10;text-align: left;display: none;' >
                <b> Alias: </b>".$params['alias']."
                <br><b> Max mark: </b>".$params['max_mark']."
                <br><b> Formula: </b>".$params['formula']."</div></div></center></td>";
            }else
                $html .= "<td width='15%' style=\"text-align:left;\"><b>".$params['label']."</b></td>";
        }
        $html .= "</thead><tbody> ";
        $no = 0;

        foreach($this->students as $student_id => $student){

            $student_mark = $this->gradebookDetail[$student_id];
            $no++;

            $picture = '<a href="javascript:DOTB.image.lightbox(\'upload/origin/'.$student->picture.'\')"><img src="upload/resize/'.$student->picture.'" style="height: 80px;"></a>';
            if(empty($student->picture))
                $picture = '<img src="themes/default/images/noimage.png" style="height: 80px;">';


            $tr = "<tr>
            <td class = 'center'><input style='font-size: 16px' type='button' student_id='$student_id' name='remove_student_btn' title='".translate('LBL_REMOVE_STUDENT','Contacts')."' class='remove_student_btn button btn-danger' value='-'></td>
            <td class = 'center'>{$no}</td>
            <td>$picture</td>
            <td><a href='index.php?module=Contacts&action=DetailView&record=$student_id'>{$student->contact_id}</a></td>
            <td><span class = 'student_name'><a href='index.php?module=Contacts&action=DetailView&record=$student_id'>{$student->name}</a></span>
            </td>
            <td>{$student->nick_name}</td>
            <td>{$timedate->to_display_date($student->birthdate,false)}</td>";

            foreach($this->config as $key => $params) {
                if(!$params['visible']) continue;
                if($params['type'] != 'comment'){
                    if($params['type'] == 'band')
                        $tr.= "<td class='td-mark'><span id = '{$student_id}-{$key}' class = 'input_mark' >{$student_mark[$key]}</td>";
                    else{
                        $_mark = format_number($student_mark[$key],2,2);
                        if($student_mark[$key] === null || $student_mark[$key] === '') $_mark = '';
                        $tr.= "<td class='td-mark'><span id = '{$student_id}-{$key}' class = 'input_mark' >$_mark</td>";
                    }

                }else{
                    if(strlen($student_mark[$key]) > 0)
                        $cm = "<p class=' block-ellipsis value_teacher_".$params['name']."' title='{$student_mark[$key]}'>".nl2br($student_mark[$key])."</p>";
                    else
                        $cm = "<p class='value_teacher_".$params['name']."'>--None--</p>";

                    $tr .= "<td class='comment'>$cm</td>";
                }
            }
            $tr .= "</tr>";
            $html.= $tr;
        }
        $html .= "</tbody>
        </table>";
        return $html;
    }

    function getConfigHTML($content = ''){
        if(!empty($content)){
            $title_html     = "";
            $alias_html     = "";
            $visible_html   = "";
            $max_mark_html  = "";
            $type_html      = "";
            $readonly_html  = "";
            $formula_html   = "";

            //Add Group Point
            $groups          = array();
            $lastGroupName  = '#***#';
            $keyGroup       = 0;
            foreach($content as $key => $defaut) {
                if(!$defaut['visible']) continue;
                if($lastGroupName != $defaut['group']){
                    $keyGroup++;
                    $lastGroupName = $defaut['group'];
                }
                $groups[$keyGroup]['name']    = $defaut['group'];
                $groups[$keyGroup]['colspan']+=1;

                $title_html .= "<td class = 'center no-bottom-border'><b>".$defaut['label']."</b></td>";
                $alias_html .= "<td class = 'center no-top-border'><b>(".$defaut['alias'].")</b></td>";
                $type_html .= "<td class = 'center no-top-border'><b>".$defaut['type']."</b></td>";
                if($defaut['type'] == 'comment'){
                    $max_mark_html .= "<td class = 'center'><span alias='{$key}' class = ' max_mark input_mark'> ".$GLOBALS['app_list_strings']['option_comment_list'][$defaut['comment_list']]."</span></td>";
                    $readonly_html .= "<td class = 'center'></td>";
                    $formula_html .= "<td class = 'center'></td>";
                }else{
                    $max_mark_html .= "<td class = 'center'>
                    <span alias='{$key}' class = ' max_mark input_mark'> ".$defaut['max_mark']."</span>
                    </td>";
                    $readonly_html .= "<td class = 'center'>
                    <span class = 'cf_readonly' alias='{$key}'> ".($defaut['readonly']?"Yes":"No")." </span>
                    </td>";
                    $formula_html .= "<td class = 'center'>
                    <span alias='{$key}' class = 'input_formula formula' >={$defaut['formula']}</span>
                    </td>";
                }

            }
            //Generate Group
            foreach($groups as $key => $group)
                $html .=  "<td class = 'center' colspan='{$group['colspan']}'><b>{$group['name']}</b></td>";

            $table = "
            <table id = 'config_content' class = 'table-border' width='100%' cellpadding=0 cellspacing=0>
            <thead>
            <tr>
            <td rowspan = 3> </td>
            $group_html
            </tr>
            <tr>
            $title_html
            </tr>
            <tr>
            $alias_html
            </tr>
            </thead>
            <tbody>
            <tr>
            <td><b>Max mark</b></td>
            $max_mark_html
            </tr>
            <td><b>Type</b></td>
            $type_html
            </tr>
            <tr>
            <td><b>Readonly</b></td>
            $readonly_html
            </tr>
            <tr>
            <td><b>Formula</b></td>
            $formula_html
            </tr>
            </tbody>
            </table>
            </div>";
            return $table;
        }else
            return '';
    }

    function getStudentListInClass(){
        global $timedate;
        $count_std = 0;
        $html = "";
        $html .= "<table id = 'markdetail_content' class = 'mark-table-border' ><thead>
        <tr>
        <td width='5%'><div class='selectedTopSupanel'></div>
        <input type='checkbox' class='checkall_custom_checkbox' module_name='J_Gradebook' onclick='handleCheckBox($(this));'></td>
        <td width='7%' style='text-align:left;'><b>".translate('LBL_PICTURE_FILE','Contacts')."</b></td>
        <td width='10%' style='text-align:left;'><b>".translate('LBL_CONTACT_ID','Contacts')."</b></td>
        <td width='10%' style='text-align:left;'><b>".translate('LBL_LIST_CONTACT_NAME','Contacts')."</b></td>
        <td width='5%' style='text-align:left;'><b>".translate('LBL_NICK_NAME','Contacts')."</b></td>
        <td width='7%' style='text-align:left;'><b>".translate('LBL_BIRTHDATE','Contacts')."</b></td>
        </tr></thead><tbody>";
        $sql_student = "SELECT DISTINCT
        IFNULL(contacts.id, '') student_id FROM contacts
        INNER JOIN j_classstudents u1 ON contacts.id = u1.student_id AND u1.deleted = 0 AND u1.student_type = 'Contacts'
        INNER JOIN j_class u2 ON u1.class_id = u2.id AND u2.deleted = 0
        WHERE (u2.id = '{$this->j_class_j_gradebook_1j_class_ida}') AND contacts.deleted = 0 AND IFNULL(contacts.id, '') NOT IN (SELECT DISTINCT
        IFNULL(ct.id, '') student_id FROM contacts ct INNER JOIN j_gradebook_contacts_1_c l1_1 ON ct.id = l1_1.j_gradebook_contacts_1contacts_idb AND l1_1.deleted = 0
        INNER JOIN j_gradebook l1 ON l1.id = l1_1.j_gradebook_contacts_1j_gradebook_ida AND l1.deleted = 0
        WHERE (l1.id = '{$this->id}') AND ct.deleted = 0)";
        $result    = $GLOBALS['db']->query($sql_student);
        while($row = $GLOBALS['db']->fetchByAssoc($result)){
            $student = BeanFactory::getBean('Contacts',$row['student_id']);
            $picture = '<a href="javascript:DOTB.image.lightbox(\'upload/origin/'.$student->picture.'\')"><img src="upload/resize/'.$student->picture.'" style="height: 80px;"></a>';
            if(empty($student->picture))
                $picture = '<img src="themes/default/images/noimage.png" style="height: 80px;">';
            $html .= "<tr>
            <td class = 'center'><input type='checkbox' class='student_checkbox custom_checkbox' module_name='J_Gradebook' onclick='handleCheckBox($(this));' value='{$student->id}'></td>
            <td>$picture</td>
            <td><a href='index.php?module=Contacts&action=DetailView&record={$student->id}'>{$student->contact_id}</a></td>
            <td><span class = 'student_name'><a href='index.php?module=Contacts&action=DetailView&record={$student->id}'>{$student->name}</a></span></td>
            <td>{$student->nick_name}</td>
            <td>{$timedate->to_display_date($student->birthdate,false)}</td></tr>";
            $count_std++;
        }
        $html .= "</tbody></table>";

        return array(
            'html' => $html,
            'count_std' => $count_std
        );
    }

    function saveOtherGradebook($class_id = ''){
        //Fixbug: Không lưu lại bảng điểm đang thao tác
        if(!empty($this->id)) $ext = "AND gb.id <> '{$this->id}'";

        if($class_id === '') $class_id = $this->j_class_j_gradebook_1j_class_ida;
        $arr_progress_change = json_decode(html_entity_decode($this->getArrayGradebookChange($class_id)),true); // mảng chứa các gradebook khác bị thay đổi theo gradebook vừa dc lưu
        if(count($arr_progress_change) > 0){
            $sql_get_detailgradebook = "SELECT IFNULL(gb.id, '') gradebook_id,
            IFNULL(gb.type, '') type, IFNULL(gb.grade_config, '') content,
            IFNULL(gbdt.student_id, '') student_id, IFNULL(gbdt.id, '') gradebookdetail_id
            FROM j_gradebook gb
            INNER JOIN j_class_j_gradebook_1_c c ON gb.id = c.j_class_j_gradebook_1j_gradebook_idb AND c.deleted = 0
            INNER JOIN j_gradebookdetail gbdt ON gb.id = gbdt.gradebook_id AND gbdt.deleted = 0
            WHERE gb.deleted = 0 AND c.j_class_j_gradebook_1j_class_ida = '{$class_id}' $ext";
            $result = $GLOBALS['db']->query($sql_get_detailgradebook);
            $arr_gradebook = array();
            while($row = $GLOBALS['db']->fetchByAssoc($result)){
                $arr_gradebook[$row['type']]['gradebook_id'] = $row['gradebook_id'];
                $arr_gradebook[$row['type']]['gradebookdetail_id'][$row['student_id']] = $row['gradebookdetail_id'];

                $content = json_decode(html_entity_decode($row['content']), true);
                foreach ($content as $key => $value){
                    if($value['type'] != 'score' && $value['type'] != 'comment' && $value['visible'] == 1){
                        $arr_gradebook[$row['type']]['col'][$key]['type'] = $value['type'];
                        $arr_gradebook[$row['type']]['col'][$key]['formula'] = $value['formula'];
                        $arr_gradebook[$row['type']]['col'][$key]['max_mark'] = $value['max_mark'];
                        if($value['type'] == 'band')
                            $arr_gradebook[$row['type']]['col'][$key]['options'] = $value['options'];
                    }
                }
            }
            // Duyệt mảng các gradebook bị thay đổi để tính lại giá trị cho đúng
            foreach ($arr_progress_change as $progress){
                foreach ($arr_gradebook[$progress]['gradebookdetail_id'] as $detail_id){
                    $detail_new = new J_GradebookDetail();
                    $detail_cur = BeanFactory::getBean('J_GradebookDetail',$detail_id);
                    foreach ($detail_new->field_defs as $keyField => $aFieldName){
                        if($keyField != 'id')
                            $detail_new->$keyField = $detail_cur->$keyField;
                    }
                    $content_detail = json_decode(html_entity_decode($detail_new->content),true);
                    do{
                        $is_change = 0; //biến kiểm tra các cột trong bảng điểm đã tính đúng chưa (đến khi value của cột không còn thay đổi nữa là đúng)
                        $f_mark = $detail_new->final_result;
                        $is_total = 0; // biến kiểm tra có cột thuộc loại total hay không, nếu không thì lấy score/formula cuối cùng làm final_result
                        foreach ($arr_gradebook[$progress]['col'] as $key1 => $value1){
                            $col_key = 'col_'.$key1;
                            $old_val = $detail_new->$col_key;
                            $max_mark = $value1['max_mark'];
                            $is_null = 1; // biến kiểm tra cột có null hay không
                            //Xử lý tính toán formula
                            $str_formula = $value1['formula'];
                            do{  //Xử lí formula có hàm COUNT(), SUM(), AVERAGE()
                                $is_function = 0;
                                $pos_count = strpos($str_formula, 'COUNT');
                                $pos_sum = strpos($str_formula, 'SUM');
                                $pos_average = strpos($str_formula, 'AVERAGE');
                                if($pos_count !== false){
                                    $pos = $pos_count;
                                    $name_function = 'COUNT(';
                                } elseif ($pos_sum !== false){
                                    $pos = $pos_sum;
                                    $name_function = 'SUM(';
                                }else{
                                    $pos = $pos_average;
                                    $name_function = 'AVERAGE(';
                                }
                                if($pos !== false){
                                    $is_function = 1;
                                    $is_null = 0;
                                    $formula_slice = substr($str_formula, $pos);
                                    $end_count = strpos($formula_slice, ')');
                                    $colon = strpos($formula_slice, ':');
                                    $bracket = strpos($formula_slice, '[');
                                    $count_col = 0;
                                    $sum_col = 0.00;
                                    $replace = '';
                                    if($formula_slice[$end_count-1] >= '0' && $formula_slice[$end_count-1] <= '9'){
                                        $begin = '';
                                        if($formula_slice[$colon-2]==='P') $begin .= $formula_slice[$colon-1];
                                        else $begin .= $formula_slice[$colon-2].$formula_slice[$colon-1];
                                        $begin = (float)$begin;
                                        $end = '';
                                        if($formula_slice[$end_count-2]==='P') $end .= $formula_slice[$end_count-1];
                                        else $end .= $formula_slice[$end_count-2].$formula_slice[$end_count-1];
                                        $end = (float)$end;
                                        $replace = 'P'.$begin.':P'.$end;
                                        for($j = $begin; $j <= $end; $j++){
                                            $detail_p_id = $arr_gradebook['P'.$j]['gradebookdetail_id'][$detail_new->student_id];
                                            if(!empty($detail_p_id)){
                                                $count_col++;
                                                $detail_p = BeanFactory::getBean('J_GradebookDetail',$detail_p_id);
                                                $sum_col += $detail_p->final_result;
                                            }
                                        }
                                    }
                                    else {
                                        $begin_word = $formula_slice[$colon-1];
                                        $end_word = $formula_slice[$colon+1];
                                        $replace .= $formula_slice[$colon-1].':'.$formula_slice[$colon+1];
                                        for( $j = $begin_word; $j <= $end_word && strlen($j) == 1; $j++){
                                            $col_ = 'col_'.$j;
                                            $val = $detail_new->$col_;
                                            if($val !== null && preg_match('/^[0-9]+\.[0-9]{2}/',$val)){
                                                $count_col++;
                                                $sum_col += $val;
                                            }
                                        }
                                    }
                                    $result_function = 0.00;
                                    switch ($name_function){
                                        case 'COUNT(':
                                            $result_function = $count_col;
                                            break;
                                        case 'SUM(':
                                            $result_function = $sum_col;
                                            break;
                                        default:
                                            if($count_col !== 0){
                                                $result_function = number_format((float)$sum_col/$count_col, 2, '.', '');
                                            }
                                            break;
                                    }
                                    $str_formula = str_replace($name_function.$replace.')', $result_function, $str_formula);
                                }
                            }while($is_function);

                            do{ //Xử lí formula có dạng P1[A] (lấy cột A trong P1)
                                $is_col_in_progress = 0;
                                $pos = strpos($str_formula, '[');
                                if($pos !== false){
                                    $is_col_in_progress = 1;
                                    $col_ = 'col_'.$str_formula[$pos+1];
                                    if($str_formula[$pos-2]=='P')
                                        $p = 'P'.$str_formula[$pos-1];
                                    else
                                        $p = 'P'.$str_formula[$pos-2].$str_formula[$pos-1];
                                    $detail_p_id = $arr_gradebook[$p]['gradebookdetail_id'][$detail_new->student_id];
                                    if(!empty($detail_p_id)){
                                        $detail_p = BeanFactory::getBean('J_GradebookDetail',$detail_p_id);
                                        $val = $detail_p->$col_;
                                        if($val === null){
                                            $val = 0.00;
                                        }else $is_null = 0;
                                    }
                                    else {
                                        $val = 0.00;
                                    }
                                    $str_formula = str_replace($p.'['.$str_formula[$pos+1].']', $val, $str_formula);
                                }
                            }while($is_col_in_progress);
                            do{ //Xử lí formula có P10, P11, P12
                                $is_p1x = 0;
                                for($j = 10; $j < 13; $j++){
                                    $pos = strpos($str_formula, 'P'.$j);
                                    if($pos !== false){
                                        $is_p1x = 1;
                                        $p = 'P1'.$str_formula[$pos+2];
                                        $detail_formula_id = $arr_gradebook[$p]['gradebookdetail_id'][$detail_new->student_id];
                                        if(!empty($detail_formula_id)){
                                            $detail_formula = BeanFactory::getBean('J_GradebookDetail',$detail_formula_id);
                                            $val = $detail_formula->final_result;
                                            $is_null = 0;
                                        }
                                        else {
                                            $val = 0.00;
                                        }
                                        $str_formula = str_replace($p, $val, $str_formula);
                                    }
                                }
                            }while($is_p1x);

                            $eval_formula = $str_formula;
                            for($index = 0; $index < strlen($str_formula); $index++){
                                $next = $str_formula[$index+1];
                                if($str_formula[$index] == 'P' && $next >= '1' && $next <= '9'){
                                    if($next == '1' && $str_formula[$index+2] >= '0' && $str_formula[$index+2] < '3'){
                                        $next .= $str_formula[$index+2];
                                    }
                                    $detail_formula_id = $arr_gradebook[$str_formula[$index].$next]['gradebookdetail_id'][$detail_new->student_id];
                                    if(!empty($detail_formula_id)){
                                        $detail_formula = BeanFactory::getBean('J_GradebookDetail',$detail_formula_id);
                                        $val = $detail_formula->final_result;
                                        if($val === '' || $val === null)$val = 0.00;
                                        $is_null = 0;
                                    }
                                    else {
                                        $val = 0.00;
                                    }
                                    $eval_formula = str_replace($str_formula[$index].$next, $val, $eval_formula);
                                }
                                else
                                    if(ord($str_formula[$index]) >= 65 && ord($str_formula[$index]) <=90 ){
                                        $col = 'col_'.$str_formula[$index];
                                        $val = $detail_new->$col;
                                        if($val === null) {
                                            $val = 0.00;
                                        }else $is_null = 0;
                                        $eval_formula = str_replace($str_formula[$index], $val, $eval_formula);
                                    }
                            }
                            $result_eval = number_format((float)eval('return '.$eval_formula.';'), 2, '.', '');
                            if($value1['type'] != 'band'){
                                if($max_mark)
                                    $detail_new->$col_key = $result_eval;
                                else $detail_new->$col_key = 0.00;
                                $f_mark = $result_eval;
                                if($value1['type']=='total'){
                                    $is_total = 1;
                                    $detail_new->final_result = $result_eval;
                                }
                            }
                            else{
                                $options = $value1['options'];
                                $ojbs = $GLOBALS['app_list_strings'][$options];
                                $result_band = '';
                                if(!empty($ojbs)){
                                    foreach ($ojbs as $key_ojbs => $value_ojbs){
                                        $key_ojbs = strtolower($key_ojbs);
                                        $pos_ = strpos($key_ojbs, '_');
                                        $posComma = strpos($key_ojbs, ',');

                                        // Dạng so sánh x dùng toán tử _ (vd: 5_10 -> 5 < x < 10)
                                        if($pos_ !== false){
                                            $key_ojbs = str_replace('_', ' < x < ', $key_ojbs);
                                        }
                                        // Dạng so sánh x dùng dấu ( ) [ ] (vd: (5,10] -> 5 < x <= 10)
                                        elseif ($posComma !== false){
                                            $before = '<';
                                            $after  = '<';
                                            if(strpos($key_ojbs, '[') !== false){
                                                $before .= '=';
                                                $key_ojbs = str_replace('[', '', $key_ojbs);
                                            }
                                            if(strpos($key_ojbs, ']') !== false){
                                                $after .= '=';
                                                $key_ojbs = str_replace(']', '', $key_ojbs);
                                            }
                                            $key_ojbs = str_replace('(', '', $key_ojbs);
                                            $key_ojbs = str_replace(')', '', $key_ojbs);
                                            $key_ojbs = str_replace(',', $before.' x '.$after, $key_ojbs);
                                        }

                                        //Dạng so sánh x dùng toán tử >, <, >=, <=
                                        $key_ojbs = explode("x", $key_ojbs);
                                        if(count($key_ojbs) == 1){
                                            $key_ojbs[0] .= '1';
                                        }
                                        else
                                            $key_ojbs[0].= $result_eval;
                                        $key_ojbs[1] = $result_eval.$key_ojbs[1];
                                        $result_compare = (eval('return '.$key_ojbs[0].';') !== false && eval('return '.$key_ojbs[1].';') !== false)? $value_ojbs : '';
                                        if($result_compare !== '') $result_band = $result_compare;
                                    }
                                    $detail_new->$col_key = $result_band;
                                }
                            }
                            if($is_null){
                                $detail_new->$col_key = null;
                                unset($content_detail[$key1]);
                            }
                            else $content_detail[$key1] = $detail_new->$col_key;
                            if($detail_new->$col_key !== $old_val) $is_change = 1;
                        }
                        if(!$is_total)
                            $detail_new->final_result = $f_mark;
                    }while ($is_change);
                    $detail_new->content = json_encode($content_detail, JSON_UNESCAPED_UNICODE);
                    $detail_new->save();
                    $arr_gradebook[$progress]['gradebookdetail_id'][$detail_new->student_id] = $detail_new->id;
                    $detail_cur->deleted = 1;
                    $detail_cur->save();
                }
            }
        }
    }
}
?>
