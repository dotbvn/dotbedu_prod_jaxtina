<?php



$dictionary['J_Gradebook'] = array(
    'table' => 'j_gradebook',
    'audited' => true,
    'activity_enabled' => false,
    'fields' => array (
        'name' => array(
            'name' => 'name',
            'vname' => 'LBL_NAME',
            'type' => 'name',
            'dbType' => 'varchar',
            'len' => 255,
            'unified_search' => true,
            'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 1.55),
            'required' => false,
            'importable' => 'required',
            'duplicate_merge' => 'enabled',
            //'duplicate_merge_dom_value' => '3',
            'merge_filter' => 'selected',
            'duplicate_on_record_copy' => 'always',
        ),
        'type' => array(
            'name' => 'type',
            'vname' => 'LBL_TYPE',
            'type' => 'enum',
            'len' => '100',
            'options' => 'gradebook_type_options',
        ),
        'custom_button' =>
        array (
            'name' => 'custom_button',
            'vname' => 'Button',
            'type' => 'varchar',
            'len' => '1',
            'studio' => 'visible',
            'source' => 'non-db',
        ),

        'status' => array(
            'name' => 'status',
            'vname' => 'LBL_STATUS',
            'type' => 'enum',
            'len' => '20',
            'default' => 'Not Approved',
            'options' => 'gradebook_status_options',
        ),
        'date_input' => array(
            'name' => 'date_input',
            'vname' => 'LBL_DATE_INPUT',
            'type' => 'date',
            'display_default' => 'now',
        ),
        'date_exam' => array(
            'name' => 'date_exam',
            'vname' => 'LBL_DATE_EXAM',
            'type' => 'date',
        ),
        'grade_config' => array(
            'name' => 'grade_config',
            'vname' => 'LBL_CONFIG',
            'type' => 'text',
        ),
        'date_confirm' => array(
            'name' => 'date_confirm',
            'vname' => 'LBL_DATE_CONFIRM',
            'type' => 'date',
            'required' => true
        ),

        'notificated' =>
        array (
            'name' => 'notificated',
            'vname' => 'notify to apps',
            'type' => 'bool',
            'audited' => true,
            'default' => '0',
            'source' => 'non-db',
        ),

        'datetime_notify_sent' => array (
            'name' => 'datetime_notify_sent',
            'vname' => 'LBL_DATETIME_NOTIFY_SENT',
            'type' => 'datetime',
        ),
        'check_box' =>
        array (
            'name' => 'check_box',
            'vname' => 'LBL_CHECKBOX',
            'type' => 'varchar',
            'studio' => 'visible',
            'source' => 'non-db',
        ),

        'gradebook_config_name' => array(
            'required'  => true,
            'source'    => 'non-db',
            'name'      => 'gradebook_config_name',
            'vname'     => 'LBL_GRADEBOOKCONFIG_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'gradebook_config_id',
            'join_name' => 'j_gradebookconfig',
            'link'      => 'gradebook_config_link',
            'table'     => 'j_gradebookconfig',
            'isnull'    => 'true',
            'module'    => 'J_GradebookConfig',
        ),

        'gradebook_config_id' => array(
            'name'              => 'gradebook_config_id',
            'rname'             => 'id',
            'vname'             => 'LBL_GRADEBOOKCONFIG_NAME',
            'type'              => 'id',
            'table'             => 'j_gradebookconfig',
            'isnull'            => 'true',
            'module'            => 'J_GradebookConfig',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
            'required'          => true,
        ),

        'gradebook_config_link' => array(
            'name'          => 'gradebook_config_link',
            'type'          => 'link',
            'relationship'  => 'gradebookconfig_gradebooks',
            'module'        => 'J_Class',
            'bean_name'     => 'J_Class',
            'source'        => 'non-db',
            'vname'         => 'LBL_GRADEBOOKCONFIG_NAME',
        ),

        //Custom Relationship Gradebook Detail
        'j_gradebook_j_gradebookdetail' => array(
            'name' => 'j_gradebook_j_gradebookdetail',
            'type' => 'link',
            'relationship' => 'j_gradebook_j_gradebookdetail',
            'module' => 'J_GradebookDetail',
            'bean_name' => 'J_GradebookDetail',
            'source' => 'non-db',
            'vname' => 'LBL_GRADEBOOK_DETAIL',
        ),
        //HTML field for portal by HoangHvy
        'mark_html' =>
        array (
            'name' => 'mark_html',
            'vname' => 'LBL_MARK',
            'type' => 'varchar',
            'studio' => 'visible',
            'source' => 'non-db',
        ),
    ),
    'relationships' => array (
        'j_gradebook_j_gradebookdetail' => array(
            'lhs_module'        => 'J_Gradebook',
            'lhs_table'         => 'j_gradebook',
            'lhs_key'           => 'id',
            'rhs_module'        => 'J_GradebookDetail',
            'rhs_table'         => 'j_gradebookdetail',
            'rhs_key'           => 'gradebook_id',
            'relationship_type' => 'one-to-many',
        ),
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

//SEND MASTER MESSAGE - CONFIG
$module = 'J_Gradebook';
require_once("custom/include/utils/bmes.php");
$label      = strtolower(bmes::bmes_parent_type[$module]);
$dictionary[$module]['fields']['bsend_messages'] = array (
    'name' => 'bsend_messages',
    'type' => 'link',
    'relationship' => 'send_messages_'.$label,
    'module' => 'BMessage',
    'bean_name' => 'BMessage',
    'source' => 'non-db',
);

$dictionary[$module]['relationships']['send_messages_'.$label] = array (
    'lhs_module'        => $module,
    'lhs_table'            => strtolower($module),
    'lhs_key'            => 'id',
    'rhs_module'        => 'BMessage',
    'rhs_table'            => 'bmessage',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => $module,
);
//END

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_Gradebook','J_Gradebook', array('basic','team_security','assignable','taggable','file'));
