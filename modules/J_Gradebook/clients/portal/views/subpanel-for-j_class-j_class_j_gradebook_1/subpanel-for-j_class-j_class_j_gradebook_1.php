<?php
// created: 2021-02-01 10:31:46
$viewdefs['J_Gradebook']['portal']['view']['subpanel-for-j_class-j_class_j_gradebook_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
          0 =>
              array (
                  'name' => 'name',
                  'label' => 'LBL_NAME',
                  'default' => true,
                  'enabled' => true,
                  'width' => 'large',
              ),
          1 =>
              array (
                  'name' => 'type',
                  'label' => 'LBL_TYPE',
                  'enabled' => true,
                  'default' => true,
                  'type' => 'html',
              ),
          2 =>
              array (
                  'name' => 'mark_html',
                  'label' => 'LBL_MARK',
                  'enabled' => true,
                  'default' => true,
                  'type' => 'html',
              ),
          3 =>
              array (
                  'name' => 'date_exam',
                  'label' => 'LBL_DATE_EXAM',
                  'enabled' => true,
                  'default' => true,
              ),
          4 =>
              array (
                  'name' => 'date_input',
                  'label' => 'LBL_DATE_INPUT',
                  'enabled' => true,
                  'default' => true,
              ),
          5 =>
              array (
                  'name' => 'c_teachers_j_gradebook_1_name',
                  'label' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_C_TEACHERS_TITLE',
                  'enabled' => true,
                  'id' => 'C_TEACHERS_J_GRADEBOOK_1C_TEACHERS_IDA',
                  'link' => true,
                  'sortable' => false,
                  'default' => true,
              ),
      ),
    ),
  ),
  'orderBy' =>
  array (
    'field' => 'date_input',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);