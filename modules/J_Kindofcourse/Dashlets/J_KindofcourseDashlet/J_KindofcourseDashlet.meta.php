<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_KindofcourseDashlet'] = array('module'		=> 'J_Kindofcourse',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Kindofcourse'), 
                                          'description' => 'A customizable view into J_Kindofcourse',
                                          'icon'        => 'icon_J_Kindofcourse_32.gif',
                                          'category'    => 'Module Views');