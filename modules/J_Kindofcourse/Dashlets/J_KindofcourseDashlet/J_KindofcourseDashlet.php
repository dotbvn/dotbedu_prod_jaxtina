<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_Kindofcourse/J_Kindofcourse.php');

class J_KindofcourseDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Kindofcourse/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Kindofcourse');

        $this->searchFields = $dashletData['J_KindofcourseDashlet']['searchFields'];
        $this->columns = $dashletData['J_KindofcourseDashlet']['columns'];

        $this->seedBean = new J_Kindofcourse();        
    }
}