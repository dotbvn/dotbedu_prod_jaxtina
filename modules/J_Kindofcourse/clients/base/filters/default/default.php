<?php


$module_name = 'J_Kindofcourse';
$viewdefs[$module_name]['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'name' => array(),
        'short_course_name' => array(),
        'status' => array(),
        'kind_of_course' => array(),
        'year' => array(),
        'team_name' => array(),
        'date_modified' => array(),
        'assigned_user_name' => array(),
        'date_entered' => array(),
        '$owner' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'created_by_name' => array(),
        'modified_by_name' => array(),
    ),
);
