<?php



$dictionary['J_Syllabus'] = array(
    'table' => 'j_syllabus',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'lesson' =>
        array (
            'required' => false,
            'name' => 'lesson',
            'vname' => 'LBL_LESSON',
            'type' => 'varchar',
            'len' => '100',
            'studio' => 'visible',
            'massupdate' => true,
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
        ),
        'lesson_type' =>
        array (
            'required' => false,
            'name' => 'lesson_type',
            'vname' => 'LBL_LESSON_TYPE',
            'type' => 'enum',
            'options' => 'syllabus_type_list',
            'studio' => 'visible',
            'len' => '150',
            'massupdate' => true,
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
        ),
        'learning_type' =>
        array (
            'required' => false,
            'name' => 'learning_type',
            'vname' => 'LBL_LEARNING_TYPE',
            'type' => 'enum',
            'options' => 'learning_type_list',
            'studio' => 'visible',
            'len' => '150',
            'massupdate' => true,
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
        ),
        'note_for_teacher' =>
        array(
            'name' => 'note_for_teacher',
            'vname' => 'LBL_NOTE_FOR_TEACHER',
            'type' => 'text',
            'comment' => '',
            'rows' => 4,
            'cols' => 60,
            'audited' => true,
        ),
        'homework' =>
        array(
            'name' => 'homework',
            'vname' => 'LBL_HOMEWORK',
            'type' => 'text',
            'comment' => '',
            'rows' => 4,
            'cols' => 60,
            'audited' => true,
        ),

        //add relationship between gradebook and gradebook detail
        'lessonplan_id' => array(
            'name' => 'lessonplan_id',
            'vname' => 'LBL_LP_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
        ),

        'lessonplan_name' => array(
            'name' => 'lessonplan_name',
            'rname' => 'name',
            'id_name' => 'lessonplan_id',
            'vname' => 'LBL_LP_NAME',
            'type' => 'relate',
            'link' => 'lessonplan_link',
            'table' => 'j_lessonplan',
            'isnull' => 'true',
            'module' => 'J_LessonPlan',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'source' => 'non-db',
        ),

        'lessonplan_link' => array(
            'name' => 'lessonplan_link',
            'type' => 'link',
            'relationship' => 'lp_syllabus_rel',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_LP_NAME',
        ),
        //end
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
    'indices' => array (
        array('name' =>'idx_j_syllabus_lessonplan_id_del', 'type'=>'index', 'fields'=>array('id','lessonplan_id','deleted')),
        array('name' =>'idx_sll_les_lid_del', 'type'=>'index', 'fields'=>array('id','lesson','lessonplan_id','deleted')),
    ),
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_Syllabus','J_Syllabus', array('basic','team_security','assignable','taggable'));