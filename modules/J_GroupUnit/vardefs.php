<?php



$dictionary['J_GroupUnit'] = array(
    'table' => 'j_groupunit',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'primary_unit' => array(
            'name' => 'primary_unit',
            'vname' => 'LBL_PRIMARY_UNIT',
            'label' => 'LBL_PRIMARY_UNIT',
            'type' => 'varchar',
            'len' => '100',
        ),
        'unit_in_group' => array (
            'name' => 'unit_in_group',
            'type' => 'link',
            'relationship' => 'unit_in_group',
            'source' => 'non-db',
            'module' => 'J_Unit',
            'bean_name' => 'j_unit',
            'id_name' => 'group_unit_id',
            'link-type' => 'one',
            'side' => 'left',
            'vname' => 'LBL_GROUP_UNIT_NAME',
        ),


        'course_fee_link' => array (
            'name' => 'course_fee_link',
            'type' => 'link',
            'relationship' => 'course_fee_group',
            'source' => 'non-db',
            'module' => 'J_Coursefee',
            'bean_name' => 'j_coursefee',
            'id_name' => 'group_unit_id',
            'link-type' => 'one',
            'side' => 'left',
            'vname' => 'LBL_COURSEFEE_NAME',
        ),
    ),
    'relationships' => array (
        'unit_in_group' => array (
            'lhs_module' => 'J_GroupUnit',
            'lhs_table' => 'j_groupunit',
            'lhs_key' => 'id',
            'rhs_module' => 'J_Unit',
            'rhs_table' => 'j_unit',
            'rhs_key' => 'group_unit_id',
            'relationship_type' => 'one-to-many'
        ),

        'course_fee_group' => array (
            'lhs_module' => 'J_GroupUnit',
            'lhs_table' => 'j_groupunit',
            'lhs_key' => 'id',
            'rhs_module' => 'J_Coursefee',
            'rhs_table' => 'j_coursefee',
            'rhs_key' => 'group_unit_id',
            'relationship_type' => 'one-to-many'
        ),
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_GroupUnit','J_GroupUnit', array('basic','team_security','assignable','taggable'));