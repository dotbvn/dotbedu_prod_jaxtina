<?php




$moduleName = 'pmse_Project';
$viewdefs[$moduleName]['base']['menu']['header'] = array(
    array(
        'route' => "#$moduleName/create",
        'label' => 'LNK_NEW_PMSE_PROJECT',
        'acl_action' => 'create',
        'acl_module' => $moduleName,
        'icon' => 'fa-plus',
    ),
    array(
        'route' => "#$moduleName",
        'label' => 'LNK_LIST',
        'acl_action' => 'list',
        'acl_module' => $moduleName,
        'icon' => 'fa-list',
    ),
    array(
        'route'=>'#'.$moduleName.'/layout/project-import',
        'label' =>'LNK_IMPORT_PMSE_PROJECT',
        'acl_action'=>'import',
        'acl_module'=>$moduleName,
        'icon' => 'fa-cloud-upload',
    ),
    array(
        'route'=>'#pmse_Emails_Templates',
        'label' =>'LNK_VIEW_EMAILS_TEMPLATES',
        'acl_action'=>'list',
        'acl_module'=>'pmse_Emails_Templates',
        'icon' => 'fa-list',
    ),
    array(
        'route'=>'#pmse_Business_Rules',
        'label' =>'LNK_VIEW_BUSSINESS_RULES',
        'acl_action'=>'list',
        'acl_module'=>'pmse_Business_Rules',
        'icon' => 'fa-list',
    ),
    array(
        'route'=>'#pmse_Inbox/layout/casesList',
        'label' =>'LNK_VIEW_INBOX',
        'acl_action'=>'list',
        'acl_module'=>'pmse_Inbox',
        'icon' => 'fa-list',
    ),
);
