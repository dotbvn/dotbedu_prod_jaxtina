<?php


$viewdefs['pmse_Project']['base']['view']['list-headerpane'] = array(

    'title' => 'LBL_MODULE_NAME',
    'buttons' => array(

        array(
            'label' => 'LNK_NEW_PMSE_PROJECT',
			'tooltip' => 'LNK_NEW_PMSE_PROJECT',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'pmse_Project',
            'route'=>'#pmse_Project/create',
            'icon' => 'fa-plus',
        ),
        array(
            'label' => 'LNK_IMPORT_PMSE_PROJECT',
            'tooltip' => 'LNK_IMPORT_PMSE_PROJECT',
            'acl_action' => 'import',
            'type' => 'button',
            'acl_module' => 'pmse_Project',
            'route'=>'#pmse_Project/layout/project-import',
            'icon' => 'fa-cloud-upload',
        ),
        array(
            'label' => 'LNK_VIEW_EMAILS_TEMPLATES',
            'tooltip' => 'LNK_VIEW_EMAILS_TEMPLATES',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'pmse_Emails_Templates',
            'route'=>'#pmse_Emails_Templates',
            'icon' => 'fa-list',
        ),
        array(
            'label' => 'LNK_VIEW_BUSSINESS_RULES',
            'tooltip' => 'LNK_VIEW_BUSSINESS_RULES',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'pmse_Business_Rules',
            'route'=>'#pmse_Business_Rules',
            'icon' => 'fa-list',
        ),
        array(
            'label' => 'LNK_VIEW_INBOX',
            'tooltip' => 'LNK_VIEW_INBOX',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'pmse_Emails_Templates',
            'route'=>'#pmse_Inbox/layout/casesList',
            'icon' => 'fa-list',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);
