<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_GalleryDetail/C_GalleryDetail.php');

class C_GalleryDetailDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_GalleryDetail/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_GalleryDetail');

        $this->searchFields = $dashletData['C_GalleryDetailDashlet']['searchFields'];
        $this->columns = $dashletData['C_GalleryDetailDashlet']['columns'];

        $this->seedBean = new C_GalleryDetail();        
    }
}