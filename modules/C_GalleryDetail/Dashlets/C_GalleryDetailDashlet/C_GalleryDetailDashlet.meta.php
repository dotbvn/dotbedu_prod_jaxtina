<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_GalleryDetailDashlet'] = array('module'		=> 'C_GalleryDetail',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_GalleryDetail'), 
                                          'description' => 'A customizable view into C_GalleryDetail',
                                          'icon'        => 'icon_C_GalleryDetail_32.gif',
                                          'category'    => 'Module Views');