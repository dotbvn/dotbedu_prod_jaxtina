<?php



$dictionary['C_GalleryDetail'] = array(
    'table' => 'c_gallerydetail',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('C_GalleryDetail','C_GalleryDetail', array('basic','team_security','assignable','taggable'));