<?php


$viewdefs['pmse_Inbox']['base']['view']['casesList-headerpane'] = array(
    'template' => 'headerpane',
    'title' => "LBL_PMSE_TITLE_PROCESSESS_LIST",
    'fields' => array(
        array(
            'name' => 'title',
            'type' => 'label',
            'default_value' => 'LBL_PMSE_TITLE_PROCESSESS_LIST',
        ),
    ),
    'buttons' => array(
        array(
            'label' => 'LNK_VIEW_PROCESS_DEFINITIONS',
            'tooltip' => 'LNK_VIEW_PROCESS_DEFINITIONS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'pmse_Project',
            'route'=>'#pmse_Project',
            'icon' => 'fa-list',
        ),

        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);
