<?php


$viewdefs['pmse_Inbox']['base']['view']['list-headerpane'] = array(
    'template' => 'headerpane',
    'title' => 'LBL_PMSE_MY_PROCESSES',
    'buttons' => array(
        array(
            'label' => 'LNK_VIEW_PROCESS_DEFINITIONS',
            'tooltip' => 'LNK_VIEW_PROCESS_DEFINITIONS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'pmse_Project',
            'route'=>'#pmse_Project',
            'icon' => 'fa-list',
        ),

        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);
