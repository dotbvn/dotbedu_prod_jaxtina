<?php



$dictionary['J_VietQRConfig'] = array(
    'table' => 'j_vietqrconfig',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'client_id' => array (
            'required' => false,
            'name' => 'client_id',
            'vname' => 'LBL_CLIENT_ID',
            'type' => 'varchar',
            'massupdate' => false,
            'no_default' => false,
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '255',
        ),
        'api_key' => array (
            'required' => false,
            'name' => 'api_key',
            'vname' => 'LBL_API_KEY',
            'help' => 'LBL_API_KEY_DES',
            'type' => 'varchar',
            'massupdate' => false,
            'no_default' => false,
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '255',
        ),
        'account_number' => array (
            'required' => true,
            'name' => 'account_number',
            'vname' => 'LBL_ACCOUNT_NUMBER',
            'help' => 'LBL_ACCOUNT_NUMBER_DES',
            'type' => 'varchar',
            'massupdate' => false,
            'no_default' => false,
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '24',
        ),
        'account_holder_name' => array (
            'required' => true,
            'name' => 'account_holder_name',
            'vname' => 'LBL_ACCOUNT_HOLDER_NAME',
            'help' => 'LBL_ACCOUNT_HOLDER_NAME_DES',
            'type' => 'varchar',
            'massupdate' => false,
            'no_default' => false,
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '100',

        ),
        'bank_id' => array (
            'name' => 'bank_id',
            'vname' => 'LBL_BANK_ID',
            'type' => 'enum',
            'options'=> 'vietqr_bank_list',
            'len' => '150',
            'required' => true,
            'massupdate' => false,
        ),
        'len_rid' => array (
            'name' => 'len_rid',
            'vname' => 'LBL_LEN_RID',
            'help' => 'LBL_LEN_RID_DES',
            'type' => 'int',
            'len' => 2,
            'default' => 8,
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'required' => true,
        ),
        'status' =>
        array (
            'required' => true,
            'name' => 'status',
            'vname' => 'LBL_STATUS',
            'type' => 'enum',
            'massupdate' => true,
            'no_default' => false,
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'default' => 'Enable',
            'calculated' => false,
            'len' => 100,
            'size' => '20',
            'options' => 'bank_connection_status_list',
            'dependency' => false,
        ),
        'pattern' =>
        array (
            'required' => true,
            'name' => 'pattern',
            'vname' => 'LBL_PATTERN',
            'help' => 'LBL_PATTERN_DES',
            'type' => 'multienum',
            'dbType' => 'varchar',
            'no_default' => true,
            'importable' => 'true',
            'duplicate_merge' => 'enabled',
            'duplicate_merge_dom_value' => '1',
            'audited' => true,
            'reportable' => true,
            'len' => 255,
            'default' => '^BRANDNAME^,^RECEIPT_ID^',
            'size' => '20',
            'options' => 'bank_vietqr_pattern_list',
            'studio' => 'visible',
            'massupdate' => false,
            'isMultiSelect' => true,
        ),
        'ext_content' =>
        array (
            'required' => false,
            'name' => 'ext_content',
            'vname' => 'LBL_EXT_CONTENT',
            'help' => 'LBL_EXT_CONTENT_DES',
            'type' => 'varchar',
            'no_default' => false,
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'massupdate' => false,
            'len' => '15',
        ),
        'picture' =>
        array(
            'name' => 'picture',
            'vname' => 'LBL_LOGO',
            'type' => 'image',
            'dbtype' => 'varchar',
            'massupdate' => false,
            'reportable' => false,
            'comment' => 'Avatar',
            'len' => '255',
            'width' => '42',
            'height' => '42',
            'border' => '',
            'duplicate_on_record_copy' => 'always',
        ),
        'engine_type' =>
        array (
            'required' => false,
            'name' => 'engine_type',
            'vname' => 'LBL_ENGINE_TYPE',
            'type' => 'enum',
            'massupdate' => 0,
            'no_default' => false,
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => 150,
            'size' => '20',
            'options' => 'bank_engine_type_list',
            'studio' => 'visible',
            'dependency' => false,
        ),
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_VietQRConfig','J_VietQRConfig', array('basic','team_security','assignable','taggable'));

//overwrite
$dictionary['J_VietQRConfig']['fields']['description']['rows'] = 4;
$dictionary['J_VietQRConfig']['fields']['name'] = array(
    'name' => 'name',
    'vname' => 'LBL_NAME',
    'type' => 'varchar',
    'readonly' => true,
    'massupdate' => false,
    'len' => 255,
    'required' => true,
    'unified_search' => true,
    'duplicate_merge' => 'disabled',
    'disable_num_format' => true,
    'studio' => array('quickcreate' => false,),
    'duplicate_on_record_copy' => 'no',
);