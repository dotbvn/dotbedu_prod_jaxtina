<?PHP

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/J_AttDevice/J_AttDevice_dotb.php');
class J_AttDevice extends J_AttDevice_dotb {
}
function getTeamList(){
    $q = "SELECT IFNULL(id, '') id,
                 IFNULL(name, '') team_name
          FROM teams WHERE deleted = 0 AND private = 0 ORDER BY name ASC";
    $rows = $GLOBALS['db']->fetchArray($q);
    $arr = array();
    foreach ($rows as $key => $row){
        $arr[$row['id']] = $row['team_name'];
    }
    return $arr;
}