<?php



$dictionary['J_AttDevice'] = array(
    'table' => 'j_attdevice',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_AttDevice','J_AttDevice', array('basic','team_security','assignable','taggable'));