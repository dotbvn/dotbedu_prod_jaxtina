<?php


$module_name = 'J_Coursefee';
$viewdefs[$module_name]['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'name' => array(),
        'status' => array(),
        'type' => array(),
        'type_of_course_fee' => array(),
        'apply_date' => array(),
        'unit_price' => array(),
        'kind_of_course' => array(),
        'team_name' => array(),
        'date_modified' => array(),
        'is_accumulate' => array(),
        'h_w' => array(),
        '$owner' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'created_by_name' => array(),
        'modified_by_name' => array(),
    ),
);
