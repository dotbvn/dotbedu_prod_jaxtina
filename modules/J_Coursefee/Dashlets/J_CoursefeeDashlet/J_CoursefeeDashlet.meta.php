<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_CoursefeeDashlet'] = array('module'		=> 'J_Coursefee',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Coursefee'), 
                                          'description' => 'A customizable view into J_Coursefee',
                                          'icon'        => 'icon_J_Coursefee_32.gif',
                                          'category'    => 'Module Views');