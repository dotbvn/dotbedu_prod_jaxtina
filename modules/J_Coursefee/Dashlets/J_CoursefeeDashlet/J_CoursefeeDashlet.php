<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_Coursefee/J_Coursefee.php');

class J_CoursefeeDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Coursefee/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Coursefee');

        $this->searchFields = $dashletData['J_CoursefeeDashlet']['searchFields'];
        $this->columns = $dashletData['J_CoursefeeDashlet']['columns'];

        $this->seedBean = new J_Coursefee();        
    }
}