<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_Inventory/J_Inventory.php');

class J_InventoryDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Inventory/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Inventory');

        $this->searchFields = $dashletData['J_InventoryDashlet']['searchFields'];
        $this->columns = $dashletData['J_InventoryDashlet']['columns'];

        $this->seedBean = new J_Inventory();        
    }
}