<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_InventoryDashlet'] = array('module'		=> 'J_Inventory',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Inventory'), 
                                          'description' => 'A customizable view into J_Inventory',
                                          'icon'        => 'icon_J_Inventory_32.gif',
                                          'category'    => 'Module Views');