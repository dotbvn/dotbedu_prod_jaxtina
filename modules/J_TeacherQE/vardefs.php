<?php



$dictionary['J_TeacherQE'] = array(
    'table' => 'j_teacherqe',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_TeacherQE','J_TeacherQE', array('basic','team_security','assignable','taggable'));