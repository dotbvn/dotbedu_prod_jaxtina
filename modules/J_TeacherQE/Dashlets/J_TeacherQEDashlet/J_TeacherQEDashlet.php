<?php
require_once('modules/J_TeacherQE/J_TeacherQE.php');

class J_TeacherQEDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_TeacherQE/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_TeacherQE');

        $this->searchFields = $dashletData['J_TeacherQEDashlet']['searchFields'];
        $this->columns = $dashletData['J_TeacherQEDashlet']['columns'];

        $this->seedBean = new J_TeacherQE();        
    }
}