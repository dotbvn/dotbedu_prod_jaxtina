<?php
global $app_strings;

$dashletMeta['J_TeacherQEDashlet'] = array('module'		=> 'J_TeacherQE',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_TeacherQE'), 
                                          'description' => 'A customizable view into J_TeacherQE',
                                          'icon'        => 'icon_J_TeacherQE_32.gif',
                                          'category'    => 'Module Views');