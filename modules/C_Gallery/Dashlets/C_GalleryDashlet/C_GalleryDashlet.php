<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_Gallery/C_Gallery.php');

class C_GalleryDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_Gallery/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_Gallery');

        $this->searchFields = $dashletData['C_GalleryDashlet']['searchFields'];
        $this->columns = $dashletData['C_GalleryDashlet']['columns'];

        $this->seedBean = new C_Gallery();        
    }
}