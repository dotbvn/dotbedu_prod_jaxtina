<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_GalleryDashlet'] = array('module'		=> 'C_Gallery',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_Gallery'), 
                                          'description' => 'A customizable view into C_Gallery',
                                          'icon'        => 'icon_C_Gallery_32.gif',
                                          'category'    => 'Module Views');