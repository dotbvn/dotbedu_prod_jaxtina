<?php



$dictionary['C_Gallery'] = array(
    'table' => 'c_gallery',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'attachment_list' => array(
            'name' => 'attachment_list',
            'type' => 's3-multi',
            'source' => 'non-db',
            'vname' => 'LBL_RATING',
            'duplicate_on_record_copy' => 'no',
            'studio' => false,
            'group' => 'attachments',
        ),
        'list_pic_id' => array(
            'name' => 'list_pic_id',
            'type' => 'text',
            'vname' => 'LBL_LIST_PIC_ID'
        ),
        'list_pic_remove_id' => array(
            'name' => 'list_pic_remove_id',
            'type' => 'text',
            'vname' => 'LBL_LIST_PIC_REMOVE_ID'
        ),
        'notes' => array(
            'name' => 'notes',
            'vname' => 'LBL_NOTES',
            'type' => 'link',
            'relationship' => 'c_gallery_notes',
            'module' => 'Notes',
            'bean_name' => 'Note',
            'source' => 'non-db',
        ),
        'attachments' => array(
            'name' => 'attachments',
            'vname' => 'LBL_ATTACHMENTS',
            'type' => 'link',
            'relationship' => 'c_gallery_attachments',
            'module' => 'Notes',
            'bean_name' => 'Note',
            'source' => 'non-db',
            'group' => 'attachments',
        ),
),
    'relationships' => array (
        'c_gallery_notes' => array(
            'lhs_module' => 'C_Gallery',
            'lhs_table' => 'c_gallery',
            'lhs_key' => 'id',
            'rhs_module' => 'Notes',
            'rhs_table' => 'notes',
            'rhs_key' => 'parent_id',
            'relationship_type' => 'one-to-many',
            'relationship_role_column' => 'parent_type',
            'relationship_role_column_value' => 'C_Gallery',
        ),
        'c_gallery_attachments' => array(
            'lhs_module' => 'C_Gallery',
            'lhs_table' => 'c_gallery',
            'lhs_key' => 'id',
            'rhs_module' => 'Notes',
            'rhs_table' => 'notes',
            'rhs_key' => 'parent_id',
            'relationship_type' => 'one-to-many',
            'relationship_role_column' => 'parent_type',
            'relationship_role_column_value' => 'C_Gallery',
        ),
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

    //SEND MASTER MESSAGE - CONFIG
    $module = 'C_Gallery';
    require_once("custom/include/utils/bmes.php");
    $label      = strtolower(bmes::bmes_parent_type[$module]);
    $dictionary[$module]['fields']['bsend_messages'] = array (
        'name' => 'bsend_messages',
        'type' => 'link',
        'relationship' => 'send_messages_'.$label,
        'module' => 'BMessage',
        'bean_name' => 'BMessage',
        'source' => 'non-db',
    );

    $dictionary[$module]['relationships']['send_messages_'.$label] = array (
        'lhs_module'        => $module,
        'lhs_table'            => strtolower($module),
        'lhs_key'            => 'id',
        'rhs_module'        => 'BMessage',
        'rhs_table'            => 'bmessage',
        'rhs_key'            => 'parent_id',
        'relationship_type'    => 'one-to-many',
        'relationship_role_column' => 'parent_type',
        'relationship_role_column_value' => $module,
    );
    //END

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('C_Gallery','C_Gallery', array('basic','team_security','assignable','taggable'));
