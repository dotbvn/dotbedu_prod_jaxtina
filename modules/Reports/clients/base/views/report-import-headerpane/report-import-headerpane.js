
({
    extendsFrom: 'HeaderpaneView',
    events:{
        'click [name=report_finish_button]': 'initiateFinish',
        'click [name=report_cancel_button]': 'initiateCancel'
    },

    initiateFinish: function() {
        var that = this;
        app.alert.show('report-import-confirmation',  {
            level: 'confirmation',
            messages: app.lang.get('LBL_REPORT_IMPORT_CONFIRMATION', 'Reports'),
            onConfirm: function () {
                app.cache.set("show_project_import_warning", false);
                that.context.trigger('report:import:finish');
            },
            onCancel: function () {
                app.router.goBack();
            }
        });
    },

    initiateCancel : function() {
        app.drawer.close('report-import')
        app.router.navigate(app.router.buildRoute(this.module), {trigger: true});
    }
})
