<?php


$module_name = 'Reports';
$viewdefs[$module_name]['base']['view']['report-import-headerpane'] = array(
    'template' => 'headerpane',
    'title' => "LBL_IMPORT_REPORT",
    'buttons' => array(
        array(
            'name'    => 'report_cancel_button',
            'type'    => 'button',
            'label'   => 'LBL_CANCEL_BUTTON_LABEL',
            'css_class' => 'btn-invisible btn-link',
        ),
        array(
            'name'    => 'report_finish_button',
            'type'    => 'button',
            'label'   => 'LBL_REPORT_IMPORT_BUTTON_LABEL',
            'acl_action' => 'create',
            'css_class' => 'btn-primary',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);
