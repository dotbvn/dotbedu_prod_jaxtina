
({
    events: {
        'change input[name=report_import]': 'readFile',
    },

    initialize: function(options) {
        app.view.View.prototype.initialize.call(this, options);
        this.context.off("report:import:finish", null, this);
        this.context.on("report:import:finish", this.importProject, this);
    },

    /**
     * Gets the file and parses its data
     */
    readFile: function() {
        var file = $('[name=report_import]')[0].files.item(0);
        if (!file) {
            this.context.trigger('updateData');
            return;
        }
        var callback = _.bind(function(text) {
            var json = {};
            try {
                json = JSON.parse(text);
            } catch (error) {
            }
            this.context.trigger('updateData', json);
        }, this);

        this.fileToText(file, callback);
    },

    /**
     * Use FileReader to read the file
     *
     * @param file
     * @param callback
     */
    fileToText: function(file, callback) {
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function() {
            callback(reader.result);
        };
    },
    /**
     * @inheritdoc
     *
     * Sets up the file field to edit mode
     *
     * @param {View.Field} field
     * @private
     */
    _renderField: function(field) {
        app.view.View.prototype._renderField.call(this, field);
        if (field.name === 'report_import') {
            field.setMode('edit');
        }
    },

    importProject: function() {
        var self = this,
            projectFile = $('[name=report_import]');
        let filePath = projectFile.val();
        // Check if a file was chosen
        if (_.isEmpty(filePath) || (!_.isEmpty(filePath) && filePath.split('.').pop() !== 'rp')) {
            app.alert.show('error_validation_process', {
                level:'error',
                messages: app.lang.get('LBL_REPORT_EMPTY_WARNING', self.module),
                autoClose: false
            });
        } else {
            app.alert.show('upload', {level: 'process', title: 'LBL_UPLOADING', autoclose: false});
            var callbacks = {
                    success: function (data) {
                        app.alert.dismiss('upload');
                        app.drawer.close('report-import')
                        app.alert.show('report-import-saved', {
                            level: 'success',
                            messages: `<span style="font-weight: bold; color: red">${data.count}</span>` + ' ' + app.lang.get('LBL_REPORT_IMPORT_SUCCESS', self.module),
                            autoClose: true
                        });
                    },
                    error: function (error) {
                        app.alert.dismiss('upload');
                        app.drawer.close('report-import')
                        app.router.refresh();
                    }
                };
            var attributes = {
                id: undefined,
                module: 'SavedReport',
                field: 'report_import'
            };
            var params = {
                format: 'dotb-html-json',
            };
            var ajaxParams = {
                files: projectFile,
                processData: false,
                iframe: true,
            };
            var body = ''
            var url = app.api.buildURL(this.model.module, 'import', attributes, params);
            app.api.call('create', url, body, callbacks, ajaxParams);
        }
    },

})
