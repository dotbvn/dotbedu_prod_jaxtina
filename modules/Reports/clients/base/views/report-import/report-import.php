<?php


$viewdefs['Reports']['base']['view']['report-import'] = array(
    'panels' => array(
        array(
            'fields' => array(
                array(
                    'name' => 'report_import',
                    'type' => 'file',
                    'view' => 'edit',
                ),
            ),
        ),
    ),
);
