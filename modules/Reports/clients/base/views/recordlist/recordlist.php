<?php

$viewdefs['Reports']['base']['view']['recordlist'] = array(
    'favorite' => true,
    'sticky_resizable_columns' => true,
    'selection' => array(
        'type' => 'multi',
        'actions' => array(
            array(
                'name' => 'massupdate_button',
                'type' => 'button',
                'label' => 'LBL_MASS_UPDATE',
                'primary' => true,
                'events' => array(
                    'click' => 'list:massupdate:fire',
                ),
                'acl_action' => 'massupdate',
            ),
            array(
                'name' => 'massdelete_button',
                'type' => 'button',
                'label' => 'LBL_DELETE',
                'acl_action' => 'delete',
                'primary' => true,
                'events' => array(
                    'click' => 'list:massdelete:fire',
                ),
            ),
            array(
                'name' => 'export_button',
                'type' => 'button',
                'label' => 'LBL_EXPORT_BUTTON_LABEL',
                'acl_action' => 'export',
                'events' => array(
                    'click' => 'list:button_export:fire',
                ),
                'primary' => true,
            ),
        ),
    ),
    'rowactions' => array(
        'actions' => array(
            array(
                'type' => 'rowaction',
                'name' => 'edit_button',
                'tooltip' => 'LBL_EDIT_BUTTON',
                'label' => 'LBL_EDIT_BUTTON',
                'event' => 'list:editrow:fire',
                'acl_action' => 'edit',
                'dismiss_label' => true,
                'icon' => 'fa-pencil',
            ),
            array(
                'type' => 'rowaction',
                'tooltip' => 'LBL_PREVIEW_REPORT',
                'label' => 'LBL_PREVIEW_REPORT',
                'event' => 'list:preview:fire',
                'icon' => 'fa-search-plus',
                'acl_action' => 'view',
            ),
            array(
                'type' => 'rowaction',
                'icon' => 'fa-pencil',
                'tooltip' => 'LBL_EDIT_REPORT_BUTTON',
                'label' => 'LBL_EDIT_REPORT_BUTTON',
                'event' => 'list:editreport:fire',
                'acl_action' => 'edit',
            ),
            array(
                'type' => 'rowaction',
                'icon' => 'fa-calendar',
                'tooltip' => 'LBL_SCHEDULE_REPORT_BUTTON',
                'label' => 'LBL_SCHEDULE_REPORT_BUTTON',
                'event' => 'list:schedulereport:fire',
                'acl_module' => 'ReportSchedules',
                'acl_action' => 'edit',
            ),
            array(
                'type' => 'rowaction',
                'icon' => 'fa-file-text',
                'tooltip' => 'LBL_VIEW_SCHEDULES_BUTTON',
                'label' => 'LBL_VIEW_SCHEDULES_BUTTON',
                'event' => 'list:viewschedules:fire',
                'acl_module' => 'ReportSchedules',
                'acl_action' => 'list',
            ),
            array(
                'type' => 'rowaction',
                'name' => 'delete_button',
                'event' => 'list:deleterow:fire',
                'label' => 'LBL_DELETE_BUTTON',
                'acl_action' => 'delete',
                'tooltip' => 'LBL_DELETE_BUTTON',
                'icon' => 'fa-trash-alt',
            ),
        ),
    ),
);
