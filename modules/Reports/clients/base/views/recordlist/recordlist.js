
/**
 * @class View.Views.Base.Reports.RecordlistView
 * @alias DOTB.App.view.views.BaseReportsRecordlistView
 * @extends View.Views.Base.RecordListView
 */
({
    extendsFrom: 'RecordListView',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this.contextEvents = _.extend({}, this.contextEvents, {
            'list:editreport:fire': 'editReport',
            'list:schedulereport:fire': 'scheduleReport',
            'list:viewschedules:fire': 'viewSchedules',
            'list:button_export:fire': 'exportReport'

        });
        this._super('initialize', [options]);
    },

    /**
     * Go to the Reports Wizard Edit page
     *
     * @param {Data.Bean} model Selected row's model.
     * @param {RowActionField} field
     */
    editReport: function(model, field) {
        var route = app.bwc.buildRoute('Reports', model.id, 'ReportsWizard');
        app.router.navigate(route, {trigger: true});
    },

    /**
     * Open schedule report drawer
     * @param model
     * @param field
     */
    scheduleReport: function(model, field) {
        var newModel = app.data.createBean('ReportSchedules');
        newModel.set({
            report_id: model.get('id'),
            report_name: model.get('name')
        });
        app.drawer.open({
            layout: 'create',
            context: {
                create: true,
                module: 'ReportSchedules',
                model: newModel
            }
        });
    },

    /**
     * View report schedules
     * @param model
     * @param field
     */
    viewSchedules: function(model, field) {
        var filterOptions = new app.utils.FilterOptions().config({
            initial_filter_label: model.get('name'),
            initial_filter: 'by_report',
            filter_populate: {
                'report_id': [model.get('id')]
            }
        });
        app.controller.loadView({
            module: 'ReportSchedules',
            module: 'ReportSchedules',
            layout: 'records',
            filterOptions: filterOptions.format()
        });
    },

    exportReport: function(model, field) {
        if (app.user.id == '1') {
            var massExport = this.context.get("mass_collection");
            app.alert.show('report-import-confirmation', {
                level: 'confirmation',
                messages: app.lang.get('LBL_REPORT_EXPORT_CONFIRMATION', 'Reports'),
                onConfirm: function () {
                    if (massExport) {
                        let id_list = massExport.pluck('id');
                        app.alert.show('massexport_loading', {level: 'process', title: app.lang.get('LBL_LOADING')});
                        app.api.call("update", app.api.buildURL('Reports/export'), {id_list: JSON.stringify(id_list)}, {
                            success: function (data) {
                                app.alert.show('message-id', {
                                    level: 'success',
                                    messages: `<span style="font-weight: bold; color: red">${id_list.length}</span>` + ' ' + app.lang.get('LBL_REPORT_EXPORT_SUCCESS', 'Reports'),
                                    autoClose: true
                                });
                                const blob = new Blob([data.content], {type: 'text/plain'});
                                const fileURL = URL.createObjectURL(blob);
                                const currentDate = new Date();

                                const year = currentDate.getFullYear();
                                const month = currentDate.getMonth() + 1;
                                const day = currentDate.getDate();
                                const hours = currentDate.getHours();
                                const minutes = currentDate.getMinutes();
                                const seconds = currentDate.getSeconds();
                                const currentDateTimeString = `${year}-${month}-${day}_${hours}:${minutes}:${seconds}`;

                                const a = document.createElement('a');
                                a.href = fileURL;
                                a.download = 'Report_' + currentDateTimeString + '.rp';
                                a.click();
                                app.alert.dismiss('massexport_loading');
                            },
                            error: function (data) {
                                app.alert.dismiss('alert-id');
                                app.alert.dismiss('massexport_loading');

                            }
                        });
                    } else {
                        let msg = app.lang.get('LBL_REJECT_EXPORT', 'Reports');
                        app.alert.show('reject-export', {
                            level: 'error',
                            messages: msg,
                            autoClose: true
                        });
                    }
                },
            })
        }
    }
})
