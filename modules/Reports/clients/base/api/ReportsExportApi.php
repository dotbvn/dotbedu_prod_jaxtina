<?php


require_once('include/download_file.php');
/**
 * @api
 */
class ReportsExportApi extends DotbApi {
    // how long the cache is ok, in minutes
    private $cacheLength = 10;

    public function registerApiRest() {
        return array(
            'exportRecord' => array(
                'reqType' => 'GET',
                'path' => array('Reports', '?', '?'),
                'pathVars' => array('module', 'record', 'export_type'),
                'method' => 'exportRecord',
                'rawReply'=> true,
                'shortHelp' => 'This method exports a record in the specified type',
                'longHelp' => '',
            ),
            'exportReport' => array(
                'reqType' => 'PUT',
                'path' => array('Reports', 'export'),
                'pathVars' => array(),
                'method' => 'exportReport'
            ),
            'importReport' => array(
                'reqType' => 'POST',
                'path' => array('Reports', 'import'),
                'pathVars' => array('module', '', ''),
                'method' => 'importReport'
            ),
        );
    }

    /**
     * Export Report records into various files, now just pdf
     * @param ServiceBase $api The service base
     * @param array $args Arguments array built by the service base
     * @return binary file
     */
    public function exportRecord(ServiceBase $api, array $args)
    {

        $this->requireArgs($args,array('record', 'export_type'));
        $args['module'] = 'Reports';

        $GLOBALS['disable_date_format'] = FALSE;

        $method = 'export' . ucwords($args['export_type']);

        if(!method_exists($this, $method)) {
            throw new DotbApiExceptionNoMethod('Export Type Does Not Exists');
        }

        $saved_report = $this->loadBean($api, $args, 'view');

        if(!$saved_report->ACLAccess('view')) {
            throw new DotbApiExceptionNotAuthorized('No access to view records for module: Reports');
        }

        return $this->$method($api, $saved_report);

    }

    /**
     * Export a Base64 PDF Report
     * @param DotbBean report
     * @return file contents
     */
    protected function exportBase64(ServiceBase $api, DotbBean $report)
    {
        global  $beanList, $beanFiles;
        global $dotb_config,$current_language;
        require_once('modules/Reports/templates/templates_pdf.php');
        $contents = '';
        $report_filename = false;
        if($report->id != null)
        {
            //Translate pdf to correct language
            $reporter = new Report(html_entity_decode($report->content), '', '');
            $reporter->layout_manager->setAttribute("no_sort",1);
            $reporter->fromApi = true;
            //Translate pdf to correct language
            $mod_strings = return_module_language($current_language, 'Reports');

            //Generate actual pdf
            $report_filename = template_handle_pdf($reporter, false);

            dotb_cache_put($report->id . '-' . $GLOBALS['current_user']->id, $report_filename, $this->cacheLength * 60);

            $dl = new DownloadFile();
            $contents = $dl->getFileByFilename($report_filename);
        }
        if(empty($contents)) {
            throw new DotbApiException('File contents empty.');
        }
        // Reply is raw just pass back the base64 encoded contents
        return base64_encode($contents);
    }

    /**
     * Export a Report As PDF
     * @param DotbBean $report
     * @return null
     */
    protected function exportPdf(ServiceBase $api, DotbBean $report)
    {
        global  $beanList, $beanFiles;
        global $dotb_config,$current_language;
        require_once('modules/Reports/templates/templates_pdf.php');
        $report_filename = false;
        if($report->id != null)
        {
            //Translate pdf to correct language
            $reporter = new Report(html_entity_decode($report->content), '', '');
            $reporter->layout_manager->setAttribute("no_sort",1);
            $reporter->fromApi = true;
            $reporter->saved_report_id = $report->id;
            $reporter->is_saved_report = true;
            //Translate pdf to correct language
            $mod_strings = return_module_language($current_language, 'Reports');

            //Generate actual pdf
            $report_filename = template_handle_pdf($reporter, false);

            $api->setHeader("Content-Type", "application/pdf");
            $api->setHeader("Content-Disposition", 'attachment; filename="'.basename($report_filename).'"');
            $api->setHeader("Expires", TimeDate::httpTime(time() + 2592000));
            $api->fileResponse($report_filename);
        }
    }

    //Export and Import function - add by Hoang Hvy
    public function exportReport(ServiceBase $api, $args){

        if(isset($args['id_list']) && !empty($args['id_list'])){
            $id_list = implode(json_decode($args['id_list'], true), "','");
            $sqlReports = "SELECT DISTINCT
            IFNULL(l1.id, '') primaryid,
            IFNULL(l1.name, '') name,
            IFNULL(l1.num, '') num,
            IFNULL(l1.description, '') description,
            IFNULL(l1.chart_type, 'none') chart_type,
            IFNULL(l1.report_type, '') report_type,
            IFNULL(l1.content, '') content,
            IFNULL(l1.is_published, '') is_published,
            IFNULL(l1.chart_type, '') chart_type,
            IFNULL(l1.schedule_type, '') schedule_type,
            IFNULL(l1.custom_url, '') custom_url,
            IFNULL(l1.is_admin_data, 0) is_admin_data,
            IFNULL(l1.row_number, '') number_row,
            IFNULL(l1.num, '') num,
            IFNULL(l1.replace_str, '') replace_str,
            IFNULL(l1.stats_type, '') stats_type,
            IFNULL(l1.relate_effi_id, '') relate_effi_id,
            IFNULL(l1.module, '') module,
            IFNULL (l1.list_of , '') category
            FROM saved_reports l1
            WHERE l1.id IN ('{$id_list}')
            AND l1.deleted = 0
            ORDER BY l1.date_entered ASC";
            $content = $GLOBALS['db']->fetchArray($sqlReports);
            return array('success' => 1, 'content' => base64_encode(json_encode($content)));
        }
        return array('success' => 0);
    }
    public function importReport(ServiceBase $api, array $args)
    {
        if(isset($_FILES) && count($_FILES) === 1){
            $reports = json_decode(base64_decode(file_get_contents($_FILES['report_import']['tmp_name'])), true);
            global $current_user;
            $count = 0;
            foreach($reports as $report){
                if(!empty($report['content']) && !empty($report['report_type']) && !empty($report['module'])){
                    $new_reports = BeanFactory::newBean('Reports');
                    $new_reports->id                    = $report['primaryid'];
                    $new_reports->new_with_id           = true;
                    $new_reports->name                  = $report['name'];
                    $new_reports->num                   = $report['num'];
                    $new_reports->description           = $report['description'];
                    $new_reports->chart_type            = $report['chart_type'];
                    $new_reports->report_type           = $report['report_type'];
                    $new_reports->content               = $report['content'];
                    $new_reports->is_published          = $report['is_published'];
                    $new_reports->schedule_type         = $report['schedule_type'];
                    $new_reports->custom_url            = $report['custom_url'];
                    $new_reports->is_admin_data         = $report['is_admin_data'];
                    $new_reports->number_row            = $report['number_row'];
                    $new_reports->replace_str           = $report['replace_str'];
                    $new_reports->module                = $report['module'];
                    $new_reports->category              = $report['category'];
                    $new_reports->stats_type            = $report['stats_type'];
                    if($new_reports->stats_type == 'scale'){
                        $sqlReports = "SELECT COUNT(1)
                                    FROM saved_reports l1
                                    WHERE l1.id = '{$report['relate_effi_id']}'
                                    AND l1.deleted = 0
                                    ORDER BY l1.date_entered ASC";
                        if(intVal($GLOBALS['db']->getOne($sqlReports)) != 0){
                            $new_reports->relate_effi_id    =  $report['relate_effi_id'];
                        } else {
                            $new_reports->relate_effi_id    = 'no';
                        }
                    }
                    $new_reports->team_id               = 1;
                    $new_reports->team_set_id           = 1;
                    $new_reports->assigned_user_id      = $current_user->id;
                    $new_reports->save();
                    $count++;
                }
            }
            return array('count' => $count);
        }
    }
    //End by Hoang Hvy
}