<?php



$dictionary['J_Metric'] = array(
    'table' => 'j_metric',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (


        'report_id' => array(
            'name' => 'report_id',
            'vname' => 'LBL_REPORT_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'comment' => ''
        ),
        'report_name' => array(
            'name' => 'report_name',
            'rname' => 'name',
            'id_name' => 'report_id',
            'vname' => 'LBL_REPORT_NAME',
            'type' => 'relate',
            'link' => 'report_link',
            'table' => 'Reports',
            'isnull' => 'true',
            'module' => 'Reports',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'source' => 'non-db',
            'processes' => true, //Áp đặt kích hoạt Process
        ),
        'report_link' => array(
            'name' => 'report_link',
            'type' => 'link',
            'relationship' => 'metrics_report_rel',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_REPORT_NAME',
        ),
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);


//SEND MASTER MESSAGE - CONFIG
$module = 'J_Metric';
require_once("custom/include/utils/bmes.php");
$label      = strtolower(bmes::bmes_parent_type[$module]);
$dictionary[$module]['fields']['bsend_messages'] = array (
    'name' => 'bsend_messages',
    'type' => 'link',
    'relationship' => 'send_messages_'.$label,
    'module' => 'BMessage',
    'bean_name' => 'BMessage',
    'source' => 'non-db',
);

$dictionary[$module]['relationships']['send_messages_'.$label] = array (
    'lhs_module'        => $module,
    'lhs_table'            => strtolower($module),
    'lhs_key'            => 'id',
    'rhs_module'        => 'BMessage',
    'rhs_table'            => 'bmessage',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => $module,
);
//END

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_Metric','J_Metric', array('basic','team_security','assignable','taggable'));
