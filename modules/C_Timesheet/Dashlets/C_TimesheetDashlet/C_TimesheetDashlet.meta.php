<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_TimesheetDashlet'] = array('module'		=> 'C_Timesheet',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_Timesheet'), 
                                          'description' => 'A customizable view into C_Timesheet',
                                          'icon'        => 'icon_C_Timesheet_32.gif',
                                          'category'    => 'Module Views');