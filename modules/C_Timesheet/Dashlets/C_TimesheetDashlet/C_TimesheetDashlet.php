<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_Timesheet/C_Timesheet.php');

class C_TimesheetDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_Timesheet/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_Timesheet');

        $this->searchFields = $dashletData['C_TimesheetDashlet']['searchFields'];
        $this->columns = $dashletData['C_TimesheetDashlet']['columns'];

        $this->seedBean = new C_Timesheet();        
    }
}