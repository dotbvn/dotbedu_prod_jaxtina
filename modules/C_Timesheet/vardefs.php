<?php



$dictionary['C_Timesheet'] = array(
    'table' => 'c_timesheet',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'add_date' =>
        array (
            'required' => false,
            'name' => 'add_date',
            'vname' => 'LBL_ADD_DATE',
            'type' => 'date',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => false,
        ),
        'duration' =>
        array (
            'required' => false,
            'name' => 'duration',
            'vname' => 'LBL_DURATION',
            'type' => 'decimal',
            'len' => '10',
            'precision' => '2',
        ),
        //Custom Relationship. Meeting - Timesheet 1 - n
        'meeting_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'meeting_name',
            'vname'     => 'LBL_MEETING_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'meeting_id',
            'join_name' => 'meetings',
            'link'      => 'meeting_link',
            'table'     => 'meetings',
            'isnull'    => 'true',
            'module'    => 'Meetings',
        ),
        'meeting_id' => array(
            'name'              => 'meeting_id',
            'rname'             => 'id',
            'vname'             => 'LBL_MEETING_NAME',
            'type'              => 'id',
            'table'             => 'meetings',
            'isnull'            => 'true',
            'module'            => 'Meetings',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),
        'meeting_link' => array(
            'name'          => 'meeting_link',
            'type'          => 'link',
            'relationship'  => 'meeting_c_timesheet',
            'module'        => 'Meetings',
            'bean_name'     => 'Meetings',
            'source'        => 'non-db',
            'vname'         => 'LBL_MEETING_NAME',
        ),
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('C_Timesheet','C_Timesheet', array('basic','team_security','assignable','taggable'));
