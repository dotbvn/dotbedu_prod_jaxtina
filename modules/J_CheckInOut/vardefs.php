<?php



$dictionary['J_CheckInOut'] = array(
    'table' => 'j_checkinout',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_CheckInOut','J_CheckInOut', array('basic','team_security','assignable','taggable'));
