//Add by Tuan Anh controller btn convert
/**
 * @class View.Fields.Base.Prospects.ConvertbuttonField
 * @alias DOTB.App.view.fields.BaseProspectsConvertbuttonField
 * @extends View.Fields.Base.RowactionField
 */
({
    extendsFrom: 'RowactionField',

    initialize: function (options) {

        this._super("initialize", [options]);
        this.type = 'rowaction';
    },

    _render: function () {
        if (this.model.get('converted') == 1 || this.model.get('lead_id')) {
            this.hide();
        } else {
            this._super("_render");
        }
    },

    rowActionSelect: function () {

        var prefill = app.data.createBean('Leads');

        prefill.copy(this.model);
        prefill.attributes.j_school_leads_1_name = this.model.attributes.j_school_prospects_1_name;
        prefill.attributes.j_school_leads_1 = this.model.attributes.j_school_prospects_1;
        prefill.attributes.j_school_leads_1j_school_ida = this.model.attributes.j_school_prospects_1j_school_ida;
        app.drawer.open({
            layout: 'create',
            context: {
                create: true,
                model: prefill,
                module: 'Leads',
                prospect_id: this.model.get('id')
            }
        }, _.bind(function (context, model) {
            //if lead is created, grab the new relationship to the target so the convert-results will refresh
            if (model && model.id && !this.disposed) {
                this.model.fetch();
                _.each(this.context.children, function (child) {
                    if (child.get('isSubpanel') && !child.get('hidden')) {
                        if (child.get('collapsed')) {
                            child.resetLoadFlag({recursive: false});
                        } else {
                            child.reloadData({recursive: false});
                        }
                    }
                });
            }
        }, this));

        prefill.trigger('duplicate:field', this.model);
    }
})
