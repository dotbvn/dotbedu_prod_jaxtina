

(function(app) {
    app.events.on('router:init', function(router) {
        var module = 'J_BankTrans';
        var routes = [
            {
                name: 'BankTransList',
                route: 'J_BankTrans',
                callback: function(params) {
                    var filterOptions;

                    if (params) {
                        var parsedParams = {filterReceipt: []};
                        // FIXME SC-5657 will handle url param parsing
                        var paramsArray = params.split('&');
                        _.each(paramsArray, function(paramPair) {
                            var keyValueArray = paramPair.split('=');
                            if (keyValueArray.length > 1) {
                                parsedParams[keyValueArray[0]] = keyValueArray[1];
                            }
                        });

                        if (!_.isEmpty(parsedParams.filterReceipt)){
                            filterOptions = new app.utils.FilterOptions().config({
                                initial_filter_label: (app.lang.get('LBL_SEARCH', 'J_BankTrans') + parsedParams.filterReceipt),
                                initial_filter: 'audit_key',
                                filter_populate: {
                                    'audit_key': [parsedParams.filterReceipt]
                                }
                            });
                        }
                    }

                    app.controller.loadView({
                        module: module,
                        layout: 'records',
                        filterOptions: filterOptions ? filterOptions.format() : null
                    });
                }
            },
        ];

        app.router.addRoutes(routes);
    });
})(DOTB.App);
