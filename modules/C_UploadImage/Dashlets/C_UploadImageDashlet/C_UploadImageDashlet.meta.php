<?php
global $app_strings;

$dashletMeta['C_UploadImageDashlet'] = array('module'		=> 'C_UploadImage',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_UploadImage'), 
                                          'description' => 'A customizable view into C_UploadImage',
                                          'icon'        => 'icon_C_UploadImage_32.gif',
                                          'category'    => 'Module Views');