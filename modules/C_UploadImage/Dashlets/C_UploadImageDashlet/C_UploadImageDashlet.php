<?php
require_once('modules/C_UploadImage/C_UploadImage.php');

class C_UploadImageDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_UploadImage/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_UploadImage');

        $this->searchFields = $dashletData['C_UploadImageDashlet']['searchFields'];
        $this->columns = $dashletData['C_UploadImageDashlet']['columns'];

        $this->seedBean = new C_UploadImage();        
    }
}