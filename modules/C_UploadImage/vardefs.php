<?php



$dictionary['C_UploadImage'] = array(
    'table' => 'c_uploadimage',
    'audited' => true,
    'activity_enabled' => false,
    'fields' => array (
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('C_UploadImage','C_UploadImage', array('basic','assignable','taggable','file'));