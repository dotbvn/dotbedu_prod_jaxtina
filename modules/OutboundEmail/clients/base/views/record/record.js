
/**
 * @class View.Views.Base.OutboundEmail.RecordView
 * @alias DOTB.App.view.views.BaseOutboundEmailRecordView
 * @extends View.Views.Base.RecordView
 */
({
    extendsFrom: 'RecordView',

    /**
     * Checks if authorized for google oauth2.
     *
     * @inheritdoc
     */
    saveClicked: function() {
        debugger
        if (this.model.get('mail_authtype') === 'oauth2' && !this.model.get('eapm_id')) {
            app.alert.show('oe-edit', {
                level: 'error',
                title: '',
                messages: [app.lang.get('LBL_EMAIL_PLEASE_AUTHORIZE', this.module)]
            });
        } else {
            this._super('saveClicked');
        }
    }
})
