<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_DeliveryRevenue/C_DeliveryRevenue.php');

class C_DeliveryRevenueDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_DeliveryRevenue/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_DeliveryRevenue');

        $this->searchFields = $dashletData['C_DeliveryRevenueDashlet']['searchFields'];
        $this->columns = $dashletData['C_DeliveryRevenueDashlet']['columns'];

        $this->seedBean = new C_DeliveryRevenue();        
    }
}