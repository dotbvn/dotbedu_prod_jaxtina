<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_DeliveryRevenueDashlet'] = array('module'		=> 'C_DeliveryRevenue',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_DeliveryRevenue'), 
                                          'description' => 'A customizable view into C_DeliveryRevenue',
                                          'icon'        => 'icon_C_DeliveryRevenue_32.gif',
                                          'category'    => 'Module Views');