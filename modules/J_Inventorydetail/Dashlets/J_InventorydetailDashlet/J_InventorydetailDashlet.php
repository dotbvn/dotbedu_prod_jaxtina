<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_Inventorydetail/J_Inventorydetail.php');

class J_InventorydetailDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Inventorydetail/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Inventorydetail');

        $this->searchFields = $dashletData['J_InventorydetailDashlet']['searchFields'];
        $this->columns = $dashletData['J_InventorydetailDashlet']['columns'];

        $this->seedBean = new J_Inventorydetail();        
    }
}