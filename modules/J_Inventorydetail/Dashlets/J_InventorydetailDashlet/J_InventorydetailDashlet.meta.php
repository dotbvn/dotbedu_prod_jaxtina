<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_InventorydetailDashlet'] = array('module'		=> 'J_Inventorydetail',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Inventorydetail'), 
                                          'description' => 'A customizable view into J_Inventorydetail',
                                          'icon'        => 'icon_J_Inventorydetail_32.gif',
                                          'category'    => 'Module Views');