<?php



$dictionary['J_GradebookDetail'] = array(
    'table' => 'j_gradebookdetail',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (

        'content' => array(
            'name' => 'content',
            'vname' => 'LBL_CONTENT',
            'type' => 'text',
        ),
        'final_result' =>
        array (
            'required' => false,
            'name' => 'final_result',
            'vname' => 'LBL_FINAL_RESULT',
            'type' => 'decimal',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '10',
            'size' => '20',
            'enable_range_search' => false,
            'precision' => '2',
        ),

        'date_input' => array(
            'name' => 'date_input',
            'vname' => 'LBL_DATE_INPUT',
            'type' => 'date',
            'display_default' => 'now',
        ),

        'certificate_type' => array(
            'name'  => 'certificate_type',
            'vname' => 'LBL_CERTIFICATE_TYPE',
            'type'  => 'varchar',
            'len'   => '255',
        ),
        'certificate_level' => array(
            'name'  => 'certificate_level',
            'vname' => 'LBL_CERTIFICATE_LEVEL',
            'type'  => 'varchar',
            'len'   => '255',
        ),

        'final_result_text' => array(
            'name' => 'final_result_text',
            'vname' => 'LBL_FINAL_RESULT',
            'type' => 'varchar',
            'source' => 'non-db',
        ),

        'col_A' =>
            array (
                'required' => false,
                'name' => 'col_A',
                'vname' => 'A',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_B' =>
            array (
                'required' => false,
                'name' => 'col_B',
                'vname' => 'B',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_C' =>
            array (
                'required' => false,
                'name' => 'col_C',
                'vname' => 'C',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_D' =>
            array (
                'required' => false,
                'name' => 'col_D',
                'vname' => 'D',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_E' =>
            array (
                'required' => false,
                'name' => 'col_E',
                'vname' => 'E',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_F' =>
            array (
                'required' => false,
                'name' => 'col_F',
                'vname' => 'F',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_G' =>
            array (
                'required' => false,
                'name' => 'col_G',
                'vname' => 'G',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_H' =>
            array (
                'required' => false,
                'name' => 'col_H',
                'vname' => 'H',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_I' =>
            array (
                'required' => false,
                'name' => 'col_I',
                'vname' => 'I',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_J' =>
            array (
                'required' => false,
                'name' => 'col_J',
                'vname' => 'J',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_K' =>
            array (
                'required' => false,
                'name' => 'col_K',
                'vname' => 'K',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_L' =>
            array (
                'required' => false,
                'name' => 'col_L',
                'vname' => 'L',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_M' =>
            array (
                'required' => false,
                'name' => 'col_M',
                'vname' => 'M',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_N' =>
            array (
                'required' => false,
                'name' => 'col_N',
                'vname' => 'N',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_O' =>
            array (
                'required' => false,
                'name' => 'col_O',
                'vname' => 'O',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_P' =>
            array (
                'required' => false,
                'name' => 'col_P',
                'vname' => 'P',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_Q' =>
            array (
                'required' => false,
                'name' => 'col_Q',
                'vname' => 'Q',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_R' =>
            array (
                'required' => false,
                'name' => 'col_R',
                'vname' => 'R',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_S' =>
            array (
                'required' => false,
                'name' => 'col_S',
                'vname' => 'S',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_T' =>
            array (
                'required' => false,
                'name' => 'col_T',
                'vname' => 'T',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_U' =>
            array (
                'required' => false,
                'name' => 'col_U',
                'vname' => 'U',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_V' =>
            array (
                'required' => false,
                'name' => 'col_V',
                'vname' => 'V',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),
        'col_W' =>
            array (
                'required' => false,
                'name' => 'col_W',
                'vname' => 'W',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_X' =>
            array (
                'required' => false,
                'name' => 'col_X',
                'vname' => 'X',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_Y' =>
            array (
                'required' => false,
                'name' => 'col_Y',
                'vname' => 'Y',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        'col_Z' =>
            array (
                'required' => false,
                'name' => 'col_Z',
                'vname' => 'Z',
                'type' => 'text',
                'reportable' => true,
                'comments' => 'column in gradebook'
            ),

        //CUSTOM RELATION
        //add relationship between student and gradebook detail
        'student_name' => array(
            'required'  => true,
            'source'    => 'non-db',
            'name'      => 'student_name',
            'vname'     => 'LBL_STUDENT_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'student_id',
            'join_name' => 'contacts',
            'link'      => 'student_j_gradebookdetail',
            'table'     => 'contacts',
            'isnull'    => 'true',
            'module'    => 'Contacts',
        ),
        'student_id' => array(
            'name'              => 'student_id',
            'rname'             => 'id',
            'vname'             => 'LBL_STUDENT_NAME',
            'type'              => 'id',
            'table'             => 'contacts',
            'isnull'            => 'true',
            'module'            => 'Contacts',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),
        'student_j_gradebookdetail' => array(
            'name'          => 'student_j_gradebookdetail',
            'type'          => 'link',
            'relationship'  => 'student_j_gradebookdetail',
            'module'        => 'Contacts',
            'bean_name'     => 'Contact',
            'source'        => 'non-db',
            'vname'         => 'LBL_STUDENT_NAME',
        ),
        //end
        //add relationship between gradebook and gradebook detail
        'gradebook_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'gradebook_name',
            'vname'     => 'LBL_GRADEBOOK_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'gradebook_id',
            'join_name' => 'j_gradebook',
            'link'      => 'j_gradebook_j_gradebookdetail',
            'table'     => 'j_gradebook',
            'isnull'    => 'true',
            'module'    => 'J_Gradebook',
        ),
        'gradebook_id' => array(
            'name'              => 'gradebook_id',
            'rname'             => 'id',
            'vname'             => 'LBL_GRADEBOOK_NAME',
            'type'              => 'id',
            'table'             => 'j_gradebook',
            'isnull'            => 'true',
            'module'            => 'J_Gradebook',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),
        'j_gradebook_j_gradebookdetail' => array(
            'name'          => 'j_gradebook_j_gradebookdetail',
            'type'          => 'link',
            'relationship'  => 'j_gradebook_j_gradebookdetail',
            'module'        => 'J_Gradebook',
            'bean_name'     => 'J_Gradebook',
            'source'        => 'non-db',
            'vname'         => 'LBL_GRADEBOOK_NAME',
        ),
        //end
        // add relate field between j_class and gradebook detail
        'j_class_id' => array (
            'required' => false,
            'name' => 'j_class_id',
            'vname' => 'LBL_J_CLASS_RELATE_FIELD',
            'type' => 'id',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'reportable' => false,
            'len' => 36,
            'size' => '20',
        ),
        'j_class_relate_field' => array (
            'required' => false,
            'source' => 'non-db',
            'name' => 'j_class_relate_field',
            'vname' => 'LBL_J_CLASS_RELATE_FIELD',
            'type' => 'relate',
            'massupdate' => 0,
            'comments' => 'Field type is Relate to J_Class',
            'help' => 'Field type is Relate to J_Class',
            'importable' => 'true',
            'duplicate_merge' => 'enabled',
            'duplicate_merge_dom_value' => '1',
            'reportable' => true,
            'len' => '255',
            'size' => '20',
            'id_name' => 'j_class_id',
            'ext2' => 'J_Class',
            'module' => 'J_Class',
            'rname' => 'name',
            'quicksearch' => 'enabled',
            'studio' => 'visible',
        ),
        //end
        //Add Related Fields Teacher
        'teacher_id' =>
        array (
            'required' => false,
            'name' => 'teacher_id',
            'vname' => 'LBL_TEACHER_NAME',
            'type' => 'id',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => 0,
            'reportable' => false,
            'len' => 36,
            'size' => '20',
        ),
        'teacher_name' =>
        array (
            'required' => false,
            'source' => 'non-db',
            'name' => 'teacher_name',
            'vname' => 'LBL_TEACHER_NAME',
            'type' => 'relate',
            'massupdate' => 0,
            'duplicate_merge' => 'enabled',
            'duplicate_merge_dom_value' => '1',
            'reportable' => true,
            'len' => '255',
            'size' => '20',
            'id_name' => 'teacher_id',
            'ext2' => 'C_Teachers',
            'module' => 'C_Teachers',
            'rname' => 'full_teacher_name',
            'quicksearch' => 'enabled',
            'studio' => 'visible',
        ),
        //END: CUSTOM RELATION
    ),
    'indices' => array (
        //custom Indices
        array('name' =>'idx_teacher_id', 'type'=>'index', 'fields'=>array('teacher_id','deleted')),
        array('name' =>'idx_class_id', 'type'=>'index', 'fields'=>array('j_class_id','deleted')),
        array('name' =>'idx_gradebook_id', 'type'=>'index', 'fields'=>array('gradebook_id','deleted')),
        array('name' =>'idx_student_id', 'type'=>'index', 'fields'=>array('student_id','deleted')),
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

    //SEND MASTER MESSAGE - CONFIG
    $module = 'J_GradebookDetail';
    require_once("custom/include/utils/bmes.php");
    $label      = strtolower(bmes::bmes_parent_type[$module]);
    $dictionary[$module]['fields']['bsend_messages'] = array (
        'name' => 'bsend_messages',
        'type' => 'link',
        'relationship' => 'send_messages_'.$label,
        'module' => 'BMessage',
        'bean_name' => 'BMessage',
        'source' => 'non-db',
    );

    $dictionary[$module]['relationships']['send_messages_'.$label] = array (
        'lhs_module'        => $module,
        'lhs_table'            => strtolower($module),
        'lhs_key'            => 'id',
        'rhs_module'        => 'BMessage',
        'rhs_table'            => 'bmessage',
        'rhs_key'            => 'parent_id',
        'relationship_type'    => 'one-to-many',
        'relationship_role_column' => 'parent_type',
        'relationship_role_column_value' => $module,
    );
    //END

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_GradebookDetail','J_GradebookDetail', array('basic','team_security','assignable','taggable'));
