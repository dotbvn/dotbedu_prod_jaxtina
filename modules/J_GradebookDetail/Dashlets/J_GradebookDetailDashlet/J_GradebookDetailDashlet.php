<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_GradebookDetail/J_GradebookDetail.php');

class J_GradebookDetailDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_GradebookDetail/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_GradebookDetail');

        $this->searchFields = $dashletData['J_GradebookDetailDashlet']['searchFields'];
        $this->columns = $dashletData['J_GradebookDetailDashlet']['columns'];

        $this->seedBean = new J_GradebookDetail();        
    }
}