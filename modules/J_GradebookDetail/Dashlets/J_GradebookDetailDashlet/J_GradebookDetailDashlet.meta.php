<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_GradebookDetailDashlet'] = array('module'		=> 'J_GradebookDetail',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_GradebookDetail'), 
                                          'description' => 'A customizable view into J_GradebookDetail',
                                          'icon'        => 'icon_J_GradebookDetail_32.gif',
                                          'category'    => 'Module Views');