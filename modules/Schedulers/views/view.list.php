<?php



class SchedulersViewList extends ViewList
{
 	public function display()
 	{
 		parent::display();
 		$this->seed->displayCronInstructions();
 	}

    public function listViewProcess(){
        $this->processSearchForm();
        $this->lv->searchColumns = $this->searchForm->searchColumns;

        if(!$this->headers)
            return;
        if(empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false){
            $this->lv->ss->assign("SEARCH",true);

            //hide Cronjob license
            if(!empty($this->where)) $this->where .= " AND";
            $this->where .= " schedulers.id !='99'";

            $this->lv->setup($this->seed, 'include/ListView/ListViewGeneric.tpl', $this->where, $this->params);
            $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
            echo $this->lv->display();
        }
    }
}
