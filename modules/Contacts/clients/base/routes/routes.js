(function (app) {
    DOTB.App.events.on("router:init", function () {
        var routes = [
            {
                route: 'Contacts/sendSMS',
                name: 'sendSMS',
                callback: function () {
                    app.router.redirect('bwc/index.php?module=C_SMS&action=sendSMS');
                }
            },
            {
                name: 'ContactsList',
                route: 'Contacts',
                callback: function(params) {
                    var filterOptions;
                    if (params) {
                        var parsedParams = {filterTeams: [], filterDevices: []};
                        var paramsArray = params.split('&');
                        _.each(paramsArray, function(paramPair) {
                            var keyValueArray = paramPair.split('=');
                            if (keyValueArray.length > 1) {
                                parsedParams[keyValueArray[0]] = keyValueArray[1];
                            }
                        });
                        if (!_.isEmpty(parsedParams.filterDevices) && !_.isEmpty(parsedParams.filterTeams)){
                            filterOptions = new app.utils.FilterOptions().config({
                                initial_filter_label: (app.lang.get('LBL_SEARCH', 'Contacts') + ' FaceID'),
                                initial_filter: 'face_id',
                                filter_populate: {
                                    'frc_device_id_list': [parsedParams.filterDevices],
                                    'team_id': [parsedParams.filterTeams]
                                }
                            });
                        }
                    }

                    app.controller.loadView({
                        module: 'Contacts',
                        layout: 'records',
                        filterOptions: filterOptions ? filterOptions.format() : null
                    });
                }
            },
        ];
        DOTB.App.router.addRoutes(routes);
    })
})(DOTB.App);
