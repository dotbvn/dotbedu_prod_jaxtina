<?php

$viewdefs['Contacts']['base']['view']['subpanel-list'] = array(
    'panels' =>
    array(
        array(
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' => array(
                array (
                    'name' => 'contact_id',
                    'label' => 'LBL_CONTACT_ID',
                    'enabled' => true,
                    'default' => true,
                ),
                array(
                    'name' => 'name',
                    'type' => 'fullname',
                    'fields' => array(
                        'salutation',
                        'first_name',
                        'last_name',
                    ),
                    'link' => true,
                    'label' => 'LBL_LIST_NAME',
                    'enabled' => true,
                    'default' => true,
                ),
                array (
                    'name' => 'status',
                    'label' => 'LBL_STATUS',
                    'type' => 'event-status',
                    'link' => false,
                    'default' => true,
                    'enabled' => true,
                    'css_class' => 'full-width',
                ),
                array (
                    'name' => 'phone_mobile',
                    'label' => 'LBL_MOBILE_PHONE',
                    'enabled' => true,
                    'default' => true,
                    'width' => 'large',
                ),
                array (
                    'name' => 'email',
                    'enabled' => true,
                    'default' => true,
                ),
            ),
        ),
    ),
);
