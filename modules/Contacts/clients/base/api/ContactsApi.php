<?php


class ContactsApi extends ModuleApi
{
    public function registerApiRest()
    {
        return array(
            'opportunity_stats' => array(
                'reqType' => 'GET',
                'path' => array('Contacts','?', 'opportunity_stats'),
                'pathVars' => array('module', 'record'),
                'method' => 'opportunityStats',
                'shortHelp' => 'Get opportunity statistics for current record',
                'longHelp' => '',
            ),
            'influencers' => array(
                'reqType' => 'GET',
                'path' => array('Contacts','?', 'influencers'),
                'pathVars' => array('module', 'record'),
                'method' => 'influencers',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'getFreeBusySchedule' => array(
                'reqType' => 'GET',
                'path' => array("Contacts", '?', "freebusy"),
                'pathVars' => array('module', 'record', ''),
                'method' => 'getFreeBusySchedule',
                'shortHelp' => 'Retrieve a list of calendar event start and end times for specified person',
                'longHelp' => 'include/api/help/contact_get_freebusy_help.html',
            ),
            'save-demo' => array(
                'reqType' => 'PUT',
                'path' => array('Contacts', 'save-demo'),
                'pathVars' => array(''),
                'method' => 'saveDemo',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'save-pt' => array(
                'reqType' => 'PUT',
                'path' => array('Contacts', 'save-pt'),
                'pathVars' => array(''),
                'method' => 'savePT',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'unlink-meeting' => array(
                'reqType' => 'PUT',
                'path' => array('Contacts', 'unlink-meeting'),
                'pathVars' => array(''),
                'method' => 'unlinkMeeting',
                'shortHelp' => '',
                'longHelp' => '',
            ),
        );
    }

    public function influencers(ServiceBase $api, array $args)
    {
        $account = $this->getAccountBean($api, $args);
        $relationships = array('calls' => 0, 'meetings' => 0);
        $data = array();
        foreach($relationships as $relationship => $ignore) {
            // Load up the relationship
            if (!$account->load_relationship($relationship)) {
                // The relationship did not load, I'm guessing it doesn't exist
                throw new DotbApiExceptionNotFound('Could not find a relationship name ' . $relationship);
            }
            // Figure out what is on the other side of this relationship, check permissions
            $linkModuleName = $account->$relationship->getRelatedModuleName();
            $linkSeed = BeanFactory::newBean($linkModuleName);
            if (!$linkSeed->ACLAccess('view')) {
                throw new DotbApiExceptionNotAuthorized('No access to view records for module: '.$linkModuleName);
            }

            $relationshipData = $account->$relationship->query(array());

            foreach ($relationshipData['rows'] as $id => $value) {
                $bean = BeanFactory::getBean(ucfirst($relationship), $id);
                $bean->load_relationship('users');
                $userModuleName = $bean->users->getRelatedModuleName();
                $userSeed = BeanFactory::newBean($userModuleName);
                $userData = $bean->users->query(array());

                foreach($userData['rows'] as $userId => $user) {
                    if(empty($data[$userId])) {
                        $userBean = BeanFactory::getBean('Users', $userId);
                        if($userBean) {
                            $data[$userId] = array_merge($this->formatBean($api, $args, $userBean), $relationships);
                            $data[$userId][$relationship]++;
                        }
                    } else {
                        $data[$userId][$relationship]++;
                    }
                }
            }
        }
        return array_values($data);
    }

    public function opportunityStats(ServiceBase $api, array $args)
    {
        $account = $this->getAccountBean($api, $args);
        $data = $this->getAccountRelationship($api, $args, $account, 'opportunities', null);
        $return = array(
            'won' => array('amount_usdollar' => 0, 'count' => 0),
            'lost' => array('amount_usdollar' => 0, 'count' => 0),
            'active' => array('amount_usdollar' => 0, 'count' => 0)
        );
        foreach ($data as $record) {
            switch($record['sales_stage']) {
                case "Closed Lost":
                    $status = 'lost';
                    break;
                case "Closed Won":
                    $status = 'won';
                    break;
                default:
                    $status = 'active';
                    break;
            }
            $return[$status]['amount_usdollar'] += $record['amount_usdollar'];
            $return[$status]['count']++;
        }
        return $return;
    }

    /**
    * Retrieve a list of calendar event start and end times for specified person
    * @param ServiceBase $api
    * @param array $args
    * @return array
    */
    public function getFreeBusySchedule(ServiceBase $api, array $args)
    {
        $bean = $this->loadBean($api, $args, 'view');
        return array(
            "module" => $bean->module_name,
            "id" => $bean->id,
            "freebusy" => $bean->getFreeBusySchedule($args),
        );
    }

    protected function getBean(ServiceBase $api, array $args)
    {
        // Load up the bean
        $record = BeanFactory::getBean($args['module'], $args['record']);

        if (empty($record)) {
            throw new DotbApiExceptionNotFound('Could not find parent record '.$args['record'].' in module '.$args['module']);
        }
        if (!$record->ACLAccess('view')) {
            throw new DotbApiExceptionNotAuthorized('No access to view records for module: '.$args['module']);
        }
        return $record;
    }

    protected function getAccountBean(ServiceBase $api, array $args)
    {
        $record = $this->getBean($api, $args);
        // Load up the relationship
        if (!$record->load_relationship('accounts')) {
            throw new DotbApiExceptionNotFound('Could not find a relationship name accounts');
        }

        // Figure out what is on the other side of this relationship, check permissions
        $linkModuleName = $record->accounts->getRelatedModuleName();
        $linkSeed = BeanFactory::newBean($linkModuleName);
        if (!$linkSeed->ACLAccess('view')) {
            throw new DotbApiExceptionNotAuthorized('No access to view records for module: '.$linkModuleName);
        }

        $accounts = $record->accounts->query(array());
        foreach ($accounts['rows'] as $accountId => $value) {
            $account = BeanFactory::getBean('Accounts', $accountId);
            if (empty($account)) {
                throw new DotbApiExceptionNotFound('Could not find parent record '.$accountId.' in module Accounts');
            }
            if (!$account->ACLAccess('view')) {
                throw new DotbApiExceptionNotAuthorized('No access to view records for module: Accounts');
            }

            // Only one account, so we can return inside the loop.
            return $account;
        }
    }

    protected function getAccountRelationship(ServiceBase $api, array $args, $account, $relationship, $limit = 5, $query = array())
    {
        // Load up the relationship
        if (!$account->load_relationship($relationship)) {
            // The relationship did not load, I'm guessing it doesn't exist
            throw new DotbApiExceptionNotFound('Could not find a relationship name ' . $relationship);
        }
        // Figure out what is on the other side of this relationship, check permissions
        $linkModuleName = $account->$relationship->getRelatedModuleName();
        $linkSeed = BeanFactory::newBean($linkModuleName);
        if (!$linkSeed->ACLAccess('view')) {
            throw new DotbApiExceptionNotAuthorized('No access to view records for module: '.$linkModuleName);
        }

        $relationshipData = $account->$relationship->query($query);
        $rowCount = 1;

        $data = array();
        foreach ($relationshipData['rows'] as $id => $value) {
            $rowCount++;
            $bean = BeanFactory::getBean(ucfirst($relationship), $id);
            $data[] = $this->formatBean($api, $args, $bean);
            if (!is_null($limit) && $rowCount == $limit) {
                // We have hit our limit.
                break;
            }
        }
        return $data;
    }

    /**
    * Added by Hieu Pham to save when select a Demo in Contact
    * @param ServiceBase $api
    * @param array $args
    * @return string
    */
    function saveDemo(ServiceBase $api, array $args){
        $meeting_id   = $args['meeting_id'];
        $student_id   = $args['id'];
        $student_name = $args['name'];
        $parent       = $args['parent'];
        if($this->checkStudentExitsInSchedule($student_id, $meeting_id, $parent))
            return array('success' => 0, 'message' => translate('LBL_ERROR_EXISTS_DEMO', 'Meetings'));

        $sql_get_meeting = "SELECT IFNULL(id,'') primaryId, IFNULL(meeting_type,'') type, IFNULL(name,'') name, date_start, IFNULL(team_id,'') team_id, IFNULL(team_set_id,'') team_set_id FROM meetings WHERE id='$meeting_id' AND deleted = 0";
        $meeting = $GLOBALS['db']->fetchOne($sql_get_meeting);
        if(!empty($meeting['primaryId'])){
            $result=new J_PTResult();
            $result->type_result= "Demo";
            $result->attended    = 0;
            $result->assigned_user_id = $GLOBALS['current_user']->id;
            $result->name         = $meeting['name'].' - '.$student_name;
            $result->parent       = $parent;
            $result->student_id   = $student_id;
            $result->meeting_id   = $meeting['primaryId'];
            $result->team_id      = $meeting['team_id'];
            $result->team_set_id  = $meeting['team_set_id'];
            $result->save();
        }
        return array('success' => 1, 'message' => translate('LBL_COMPLETE_DEMO', 'Meetings'));
    }

    /**
    * Added by Hieu Pham to save when select a PT in Contact
    * @param ServiceBase $api
    * @param array $args
    * @return string
    */
    function savePT(ServiceBase $api, array $args){
        $meeting_id   = $args['meeting_id'];
        $student_id   = $args['id'];
        $student_name = $args['name'];
        $parent       = $args['parent'];
        if($this->checkStudentExitsInSchedule($student_id, $meeting_id, $parent))
            return array('success' => 0, 'message' => translate('LBL_ERROR_EXISTS_PT', 'Meetings'));


        $sql_get_meeting = "SELECT IFNULL(id,'') primaryId, IFNULL(meeting_type,'') type, IFNULL(name,'') name, date_start, IFNULL(team_id,'') team_id, IFNULL(team_set_id,'') team_set_id FROM meetings WHERE id='$meeting_id' AND deleted = 0";
        $meeting = $GLOBALS['db']->fetchOne($sql_get_meeting);
        if(!empty($meeting['primaryId'])){
            $result=new J_PTResult();
            $result->time_start  = $meeting['date_start'];
            $result->time_end    = date('Y-m-d H:i:s',strtotime($result->time_start." +10 minutes"));
            $result->type_result = "Placement Test";
            $result->attended    = 0;
            $result->assigned_user_id = $GLOBALS['current_user']->id;
            $result->name         = $meeting['name'].' - '.$student_name;
            $result->parent       = $parent;
            $result->student_id   = $student_id;
            $result->meeting_id   = $meeting['primaryId'];
            $result->team_id      = $meeting['team_id'];
            $result->team_set_id  = $meeting['team_set_id'];
            $result->save();
        }
        return array('success' => 1, 'message' => translate('LBL_COMPLETE_PT', 'Meetings'));
    }

    /**
    * Added by Hieu Pham to remove a PT/Demo result and unlink with meeting in Contact
    * @param ServiceBase $api
    * @param array $args
    * @return string
    */
    function unlinkMeeting(ServiceBase $api, array $args){
        $thisResult = BeanFactory::getBean('J_PTResult', $args['ptresult_id']);
        if(!empty($thisResult->id)){
            $sql_get_meeting = "SELECT IFNULL(id,'') primaryId, IFNULL(name,'') name, IFNULL(meeting_type,'') type FROM meetings WHERE id='{$thisResult->meeting_id}' AND deleted = 0";
            $meeting = $GLOBALS['db']->fetchOne($sql_get_meeting);

            $thisResult->mark_deleted($thisResult->id);

        }
        if($meeting['type'] == 'Placement Test') $message = translate('LBL_REMOVED_PT', 'Meetings');
        elseif($meeting['type'] == 'Demo') $message = translate('LBL_REMOVED_DEMO', 'Meetings');

        return array('success' => 1, 'message' => $message);
    }

    function checkStudentExitsInSchedule($student_id, $meeting_id, $parent) {
        $sql = "SELECT count(pt.id)
        FROM j_ptresult pt
        INNER JOIN meetings l1 ON l1.id = pt.meeting_id AND l1.deleted = 0
        WHERE pt.student_id = '$student_id' AND l1.id = '$meeting_id' AND pt.parent = '$parent' AND pt.deleted = 0";
        return $GLOBALS['db']->getOne($sql) ? true : false;
    }

}
