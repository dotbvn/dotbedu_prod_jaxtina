<?php


$module_name='J_PTResult';
$subpanel_layout = array(
	'top_buttons' => array(
		array('widget_class' => 'SubPanelTopCreateButton'),
		array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name),
	),

	'where' => '',

	'list_fields' => array(
		'name'=>array(
	 		'vname' => 'LBL_NAME',
			'widget_class' => 'SubPanelDetailViewLink',
	 		'width' => '20%',
		),
        'time_start' =>
            array (
                'type' => 'datetimecombo',
                'vname' => 'LBL_TIME_START',
                'width' => '10%',
                'default' => true,
            ),
        'time_end' =>
            array (
                'type' => 'datetimecombo',
                'vname' => 'LBL_TIME_END',
                'width' => '10%',
                'default' => true,
            ),
        'listening' =>
            array (
                'type' => 'varchar',
                'vname' => 'LBL_LISTENING',
                'width' => '7%',
                'default' => true,
            ),
        'speaking' =>
            array (
                'type' => 'varchar',
                'vname' => 'LBL_SPEAKING',
                'width' => '7%',
                'default' => true,
            ),
        'reading' =>
            array (
                'type' => 'reading',
                'vname' => 'LBL_READING',
                'width' => '7%',
                'default' => true,
            ),
        'writing' =>
            array (
                'type' => 'varchar',
                'vname' => 'LBL_WRITING',
                'width' => '7%',
                'default' => true,
            ),
        'score' =>
            array (
                'type' => 'score',
                'vname' => 'LBL_SCORE',
                'width' => '10%',
                'default' => true,
            ),
        'result' =>
            array (
                'type' => 'result',
                'vname' => 'LBL_RESULT',
                'width' => '10%',
                'default' => true,
            ),
        'ec_note' =>
            array (
                'type' => 'text',
                'vname' => 'LBL_EC_NOTE',
                'sortable' => false,
                'width' => '10%',
                'default' => true,
            ),
        'teacher_comment' =>
            array (
                'type' => 'text',
                'vname' => 'LBL_TEACHER_COMMENT',
                'sortable' => false,
                'width' => '10%',
                'default' => true,
            ),
        'attended' =>
            array (
                'type' => 'bool',
                'default' => true,
                'vname' => 'LBL_ATTENDED',
                'width' => '10%',
            ),

		'remove_button'=>array(
            'vname' => 'LBL_REMOVE',
			'widget_class' => 'SubPanelRemoveButton',
		 	'module' => $module_name,
			'width' => '5%',
		),
	),
);

?>