<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_PTResult/J_PTResult.php');

class J_PTResultDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_PTResult/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_PTResult');

        $this->searchFields = $dashletData['J_PTResultDashlet']['searchFields'];
        $this->columns = $dashletData['J_PTResultDashlet']['columns'];

        $this->seedBean = new J_PTResult();        
    }
}