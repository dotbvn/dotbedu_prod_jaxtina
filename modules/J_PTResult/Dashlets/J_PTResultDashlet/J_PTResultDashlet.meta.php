<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_PTResultDashlet'] = array('module'		=> 'J_PTResult',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_PTResult'), 
                                          'description' => 'A customizable view into J_PTResult',
                                          'icon'        => 'icon_J_PTResult_32.gif',
                                          'category'    => 'Module Views');