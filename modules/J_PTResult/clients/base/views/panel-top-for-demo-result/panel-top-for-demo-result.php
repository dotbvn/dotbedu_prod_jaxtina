<?php

$viewdefs['J_PTResult']['base']['view']['panel-top-for-demo-result'] = array(
    'type' => 'panel-top',
    'template' => 'panel-top',

    'buttons' => array(
        array(
            'type' => 'button',
            'name' => 'create_demo_schedule',
            'icon' => 'fa-plus',
            'css_class' => 'btn',
            'label' => 'LBL_CREATE_DEMO',
            'event' => 'button:create_demo_schedule:click',
            'acl_module' => 'Meetings',
            'acl_action' => 'create',
        ),
        array(
            'name' => 'select_demo_schedule',
            'icon' => 'fa-search',
            'type' => 'button',
            'label' => 'LBL_ADD_DEMO',
            'css_class' => 'btn',
            'event' => 'button:select_demo_schedule:click',
        ),


    ),

);
