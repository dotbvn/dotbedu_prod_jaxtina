<?php

$viewdefs['J_PTResult']['base']['view']['subpanel-for-demo-result'] = array(
    'type' => 'subpanel-list',
    'panels' => array(
        array(
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
                array(
                    array(
                        'label' => 'LBL_MEETING',
                        'type' => 'relate',
                        'enabled' => true,
                        'default' => true,
                        'id' => 'MEETING_ID',
                        'name' => 'meeting_name',
                        'link' => true,
                        'widget_class' => 'SubPanelDetailViewLink',
                        'target_module' => 'Meetings',
                        'target_record_key' => 'meeting_id',
                    ),

                    array(
                        'label' => 'LBL_START_DATE',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'time_start',
                    ),

                    array(
                        'label' => 'LBL_DATE_END',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'time_end',
                    ),
                    array(
                        'label' => 'LBL_DURATION',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'duration_cal',
                    ),
                    array(
                        'label' => 'LBL_ATTENDED',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'attended',
                    ),
                ),
        ),
    ),
      'orderBy' =>
  array (
    'field' => 'time_start',
    'direction' => 'desc',
  ),
    'rowactions' => array(
        'actions' => array(
            array(
                'type' => 'rowaction',
                'css_class' => 'btn',
                'tooltip' => 'LBL_REMOVE',
                'event' => 'list:unlink_meeting:fire',
                'icon' => 'fa-trash-alt',
                'allow_bwc' => false,
            ),
        ),
    ),

);
