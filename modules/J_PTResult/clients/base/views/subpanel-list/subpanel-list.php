<?php

$module_name = 'J_PTResult';
$viewdefs[$module_name]['base']['view']['subpanel-list'] = array(
    'panels' =>
        array(
            array(
                'name' => 'panel_header',
                'label' => 'LBL_PANEL_1',
                'fields' =>
                    array(
                        array(
                            'label' => 'LBL_NAME',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'name',
                            'link' => true,
                        ),
                        array(
                            'label' => 'LBL_TIME_START',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'time_start',
                        ),
                        array(
                            'label' => 'LBL_TIME_END',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'time_end',
                        ),
                        array(
                            'label' => 'LBL_LISTENING',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'listening',
                        ),

                        array(
                            'label' => 'LBL_SPEAKING',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'speaking',
                        ),
                        array(
                            'label' => 'LBL_READING',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'reading',
                        ),
                        array(
                            'label' => 'LBL_WRITING',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'writing',
                        ),
                        array(
                            'label' => 'LBL_SCORE',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'score',
                        ), array(
                        'label' => 'LBL_RESULT',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'result',
                    ),
                        array(
                            'label' => 'LBL_EC_NOTE',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'ec_note',
                        ),
                        array(
                            'label' => 'LBL_TEACHER_COMMENT',
                            'enabled' => true,
                            'default' => true,
                            'name' => 'teacher_comment',
                        ),
                    ),
            ),
        ),
    'orderBy' => array(
        'field' => 'date_modified',
        'direction' => 'desc',
    ),
);
