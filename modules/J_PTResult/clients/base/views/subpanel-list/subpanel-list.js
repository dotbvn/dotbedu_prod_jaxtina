/**
 * Custom Subpanel Layout for Revenue Line Items.
 *
 * @class View.Views.Base.KBContents.SubpanelListView
 * @alias DOTB.App.view.views.BaseKBContentsSubpanelListView
 * @extends View.Views.Base.SubpanelListView
 */
({
    extendsFrom: 'SubpanelListView',

    /**
     * Added by Hieu Pham to unlink Meeting and delete PT/Demo result
     * @param model
     */
    unlinkMeeting: function (model) {
        var self = this;
        var preferences = {};
        var module = app.controller.context.attributes.module;
        preferences['ptresult_id'] = model.attributes.id;
        app.alert.show('wait', {
            level: 'process',
            title: 'Waiting'
        });
        DOTB.App.api.call('update', DOTB.App.api.buildURL('Contacts', 'unlink-meeting'), preferences, {
            success: function (data) {
                self.alertUser(data.message, 'success');
                $('[data-action=refreshList]').trigger('click');
                app.alert.dismiss('wait');
            },
            error: function (data) {
                self.alertUser(app.lang.get('LBL_ERROR', 'Meetings'), 'error');
                $('[data-action=refreshList]').trigger('click');
                app.alert.dismiss('wait');
            }
        });
    },

    /**
     * Added by Hieu Pham to show alert
     * @param msg - Label message in Module ( ex: LBL_ERROR)
     * @param level - level of alert (ex: error, success)
     */
    alertUser: function (msg, level) {
        app.alert.show('no-lumia-access', {
            level: level,
            messages: app.lang.get(msg, app.controller.context.attributes.module),
            autoClose: true,
        });
    }
})
