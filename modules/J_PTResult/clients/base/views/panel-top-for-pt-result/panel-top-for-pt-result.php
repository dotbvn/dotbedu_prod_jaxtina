<?php

$viewdefs['J_PTResult']['base']['view']['panel-top-for-pt-result'] = array(
    'type' => 'panel-top',
    'template' => 'panel-top',

    'buttons' => array(
        array(
            'type' => 'button',
            'name' => 'create_pt_schedule',
            'icon' => 'fa-plus',
            'css_class' => 'btn',
            'label' => 'LBL_CREATE_PT',
            'event' => 'button:create_pt_schedule:click',
            'acl_module' => 'Meetings',
            'acl_action' => 'create',
        ),
        array(
            'name' => 'select_pt_schedule',
            'icon' => 'fa-search',
            'type' => 'button',
            'label' => 'LBL_ADD_PT',
            'css_class' => 'btn',
            'event' => 'button:select_pt_schedule:click',
        ),


    ),


);
