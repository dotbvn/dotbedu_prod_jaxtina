<?php
$viewdefs['J_PTResult']['base']['view']['subpanel-for-pt-result'] = array(
    'type' => 'subpanel-list',
    'panels' => array(
        array(
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
                array(
                    array(
                        'label' => 'LBL_MEETING',
                        'type' => 'relate',
                        'enabled' => true,
                        'default' => true,
                        'id' => 'MEETING_ID',
                        'name' => 'meeting_name',
                        'link' => true,
                        'widget_class' => 'SubPanelDetailViewLink',
                        'target_module' => 'Meetings',
                        'target_record_key' => 'meeting_id',
                    ),

                    array(
                        'label' => 'LBL_LISTENING',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'listening',
                    ),

                    array(
                        'label' => 'LBL_SPEAKING',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'speaking',
                    ),
                    array(
                        'label' => 'LBL_READING',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'reading',
                    ),
                    array(
                        'label' => 'LBL_WRITING',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'writing',
                    ),
                    array(
                        'label' => 'LBL_SCORE',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'score',
                    ),
                    array(
                        'label' => 'LBL_RESULT',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'result',
                    ),
                    array(
                        'label' => 'LBL_ATTENDED',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'attended',
                    ),
                    array(
                        'label' => 'LBL_EC_NOTE',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'ec_note',
                    ),
                    array(
                        'label' => 'LBL_TEACHER_COMMENT',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'teacher_comment',
                    ),
                ),
        ),
    ),
    'orderBy' =>
  array (
    'field' => 'time_start',
    'direction' => 'desc',
  ),
    'rowactions' => array(
        'actions' => array(
            array(
                'type' => 'rowaction',
                'css_class' => 'btn',
                'tooltip' => 'LBL_REMOVE',
                'event' => 'list:unlink_meeting:fire',
                'icon' => 'fa-trash-alt',
                'allow_bwc' => false,
            ),
        ),
    ),
);
