<?php


class DemoLink extends Link2 {
    public function buildJoinDotbQuery($dotb_query, $option = array()) {
        $dotb_query->where()->equals('type_result', 'Demo');
        //        $dotb_query->joinTable('meetings', array('alias' => 'mtt' , 'joinType'=>"INNER", 'linkingTable' => true))->on()
        //        ->equalsField('j_ptresult.meeting_id','mtt.id')
        //        ->equals('mtt.deleted','0')
        //        ->equals('mtt.meeting_type', 'Demo');

        return $this->relationship->buildJoinDotbQuery($this, $dotb_query, $option);
    }
}