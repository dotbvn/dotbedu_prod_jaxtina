<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_TeachersDashlet'] = array('module'		=> 'C_Teachers',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_Teachers'), 
                                          'description' => 'A customizable view into C_Teachers',
                                          'icon'        => 'icon_C_Teachers_32.gif',
                                          'category'    => 'Module Views');