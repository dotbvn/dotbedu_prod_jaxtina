<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_Teachers/C_Teachers.php');

class C_TeachersDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_Teachers/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_Teachers');

        $this->searchFields = $dashletData['C_TeachersDashlet']['searchFields'];
        $this->columns = $dashletData['C_TeachersDashlet']['columns'];

        $this->seedBean = new C_Teachers();        
    }
}