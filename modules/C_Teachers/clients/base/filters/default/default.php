<?php


$module_name = 'C_Teachers';
$viewdefs[$module_name]['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'teacher_id' => array(),
        'full_teacher_name' => array(),
        'type' => array(),
        'title' => array(),
        'email' => array(),
        'phone_mobile' => array(),
        'date_entered' => array(),
        'team_name' => array(),
        'date_modified' => array(),
        '$owner' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'created_by_name' => array(),
        'modified_by_name' => array(),
    ),
);
