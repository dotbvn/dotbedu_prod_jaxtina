<?php


$viewdefs['C_Teachers']['base']['filter']['person'] = array(
    'quicksearch_field' => array(
        array(
            'first_name',
            'last_name',
        ),
    ),
    'quicksearch_priority' => 2,
);
