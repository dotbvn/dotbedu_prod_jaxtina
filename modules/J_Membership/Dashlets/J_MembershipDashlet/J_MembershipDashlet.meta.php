<?php
global $app_strings;

$dashletMeta['J_MembershipDashlet'] = array('module'		=> 'J_Membership',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Membership'), 
                                          'description' => 'A customizable view into J_Membership',
                                          'icon'        => 'icon_J_Membership_32.gif',
                                          'category'    => 'Module Views');