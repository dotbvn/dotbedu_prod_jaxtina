<?php
require_once('modules/J_Membership/J_Membership.php');

class J_MembershipDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Membership/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Membership');

        $this->searchFields = $dashletData['J_MembershipDashlet']['searchFields'];
        $this->columns = $dashletData['J_MembershipDashlet']['columns'];

        $this->seedBean = new J_Membership();        
    }
}