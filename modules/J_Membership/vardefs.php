<?php



$dictionary['J_Membership'] = array(
    'table' => 'j_membership',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_Membership','J_Membership', array('basic','team_security','assignable','taggable','person'));
