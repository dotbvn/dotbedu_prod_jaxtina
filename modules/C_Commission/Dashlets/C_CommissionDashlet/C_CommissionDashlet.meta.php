<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_CommissionDashlet'] = array('module'		=> 'C_Commission',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_Commission'), 
                                          'description' => 'A customizable view into C_Commission',
                                          'icon'        => 'icon_C_Commission_32.gif',
                                          'category'    => 'Module Views');