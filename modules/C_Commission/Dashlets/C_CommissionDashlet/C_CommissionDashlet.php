<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_Commission/C_Commission.php');

class C_CommissionDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_Commission/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_Commission');

        $this->searchFields = $dashletData['C_CommissionDashlet']['searchFields'];
        $this->columns = $dashletData['C_CommissionDashlet']['columns'];

        $this->seedBean = new C_Commission();        
    }
}