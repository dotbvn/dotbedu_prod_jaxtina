<?php



$dictionary['C_Commission'] = array(
    'table' => 'c_commission',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        //Tạo Fiels cho Report Customize
        'amount' =>
        array (
            'required' => false,
            'name' => 'amount',
            'vname' => 'LBL_AMOUNT',
            'type' => 'decimal',
            'massupdate' => false,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'default' => '',
            'calculated' => false,
            'len' => '13',
            'size' => '20',
            'enable_range_search' => false,
            'precision' => 2,
        ),

        'type' =>
        array (
            'required' => true,
            'name' => 'type',
            'vname' => 'LBL_TYPE',
            'type' => 'enum',
            'massupdate' => false,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'default' => '',
            'calculated' => false,
            'len' => 100,
            'size' => '20',
            'options' => 'commission_type_list',
            'dependency' => false,
        ),
        'input_date' =>
        array (
            'required' => true,
            'name' => 'input_date',
            'vname' => 'LBL_INPUT_DATE',
            'type' => 'date',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
            'importable' => 'required',
            'display_default' => 'now',
        ),
        //Relationship
        'pmd_id' => array(
            'name' => 'pmd_id',
            'vname' => 'LBL_PMD_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'comment' => ''
        ),

        'pmd_name' => array(
            'name' => 'pmd_name',
            'rname' => 'name',
            'id_name' => 'pmd_id',
            'vname' => 'LBL_PMD_NAME',
            'type' => 'relate',
            'link' => 'pmd_link',
            'table' => 'j_paymentdetail',
            'isnull' => 'true',
            'module' => 'J_PaymentDetail',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'source' => 'non-db',
        ),
        'pmd_link' => array(
            'name' => 'pmd_link',
            'type' => 'link',
            'relationship' => 'pmd_coms',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_PMD_NAME',
        ),

        //Add Relationship Payment - Commission
        'payment_id' => array(
            'name' => 'payment_id',
            'vname' => 'LBL_PAYMENT_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'comment' => ''
        ),
        'payment_name' => array(
            'name' => 'payment_name',
            'rname' => 'name',
            'id_name' => 'payment_id',
            'vname' => 'LBL_PAYMENT_NAME',
            'type' => 'relate',
            'link' => 'payment_link',
            'table' => 'j_payment',
            'isnull' => 'true',
            'module' => 'J_Payment',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'source' => 'non-db',
        ),
        'payment_link' => array(
            'name' => 'payment_link',
            'type' => 'link',
            'relationship' => 'payment_coms',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_PAYMENT_NAME',
        ),
        //END: Add Relationship Payment - Loyalty

    ),
    'indices' => array (
        array(
            'name' => 'idx_payment_id',
            'type' => 'index',
            'fields'=> array('payment_id'),
        ),
        array(
            'name' => 'idx_pmd_id',
            'type' => 'index',
            'fields'=> array('pmd_id'),
        ),
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('C_Commission','C_Commission', array('basic','team_security','assignable','taggable'));
