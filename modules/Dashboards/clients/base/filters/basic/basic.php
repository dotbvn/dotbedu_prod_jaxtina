<?php

$viewdefs['Dashboards']['base']['filter']['basic'] = array(
    'create' => false,
    'quicksearch_field' => array('name'),
    'quicksearch_priority' => 1,
    'quicksearch_split_terms' => false,
    'filters' => array(
        array(
            'id' => 'assigned_to_me',
            'name' => 'LBL_ASSIGNED_TO_ME',
            'filter_definition' => array(
                array('view_name' => array('$not_in' => array('records', 'record'))),
                array('dashboard_module' => array('$in' => array('Home'))),
                array('assigned_user_id' => array('$in' => array($GLOBALS['current_user']->id))),
            ),
            'editable' => false,
        ),
    ),
);
