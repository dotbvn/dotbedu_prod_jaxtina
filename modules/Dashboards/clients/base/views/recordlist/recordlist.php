<?php

$viewdefs['Dashboards']['base']['view']['recordlist'] = array(
    'favorite' => true,
    'following' => true,
    'sticky_resizable_columns' => true,
    'selection' => array(
        'type' => 'multi',
        'actions' => array(
            array(
                'name' => 'massupdate_button',
                'type' => 'button',
                'label' => 'LBL_MASS_UPDATE',
                'primary' => true,
                'events' => array(
                    'click' => 'list:massupdate:fire',
                ),
                'acl_action' => 'massupdate',
            ),
            array(
                'name' => 'massdelete_button',
                'type' => 'button',
                'label' => 'LBL_DELETE',
                'acl_action' => 'delete',
                'primary' => true,
                'events' => array(
                    'click' => 'list:massdelete:fire',
                ),
            ),
        ),
    ),
    'rowactions' => array(
        'actions' => array(
            array(
                'type' => 'rowaction',
                'name' => 'edit_button',
                'css_class' => 'btn',
                'label' => 'LBL_EDIT_BUTTON',
                'event' => 'list:editrow:fire',
                'acl_action' => 'edit',
                'icon' => 'fa-pencil',
            ),
            array(
                'type' => 'rowaction',
                'name' => 'delete_button',
                'label' => 'LBL_DELETE_BUTTON',
                'event' => 'list:deleterow:fire',
                'acl_action' => 'delete',
            ),
            array(
                'type' => 'customcopydashboard',
                'name' => 'copy_to_user',
                'label' => 'LBL_COPY_TO_USER',
                'target_module' => 'Users',
                'acl_action' => 'edit',
            ),
            array(
                'type' => 'customcopydashboard',
                'name' => 'copy_to_team',
                'label' => 'LBL_COPY_TO_TEAM',
                'target_module' => 'Teams',
                'acl_action' => 'edit',
            ),
            array(
                'type' => 'customcopydashboard',
                'name' => 'copy_to_role',
                'label' => 'LBL_COPY_TO_ROLE',
                'target_module' => 'ACLRoles',
                'acl_action' => 'edit',
            ),

        ),
    ),
    'last_state' => array(
        'id' => 'record-list',
    ),
);
