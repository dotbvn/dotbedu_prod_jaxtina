<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_ContactsDashlet'] = array('module'		=> 'C_Contacts',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_Contacts'), 
                                          'description' => 'A customizable view into C_Contacts',
                                          'icon'        => 'icon_C_Contacts_32.gif',
                                          'category'    => 'Module Views');