<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_Contacts/C_Contacts.php');

class C_ContactsDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_Contacts/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_Contacts');

        $this->searchFields = $dashletData['C_ContactsDashlet']['searchFields'];
        $this->columns = $dashletData['C_ContactsDashlet']['columns'];

        $this->seedBean = new C_Contacts();        
    }
}