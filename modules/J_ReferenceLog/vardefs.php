<?php



$dictionary['J_ReferenceLog'] = array(
    'table' => 'j_referencelog',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'hits' => array (
            'name' => 'hits',
            'vname' => 'LBL_HITS',
            'type' => 'int',
            'default'=>'0',
            'reportable'=>true,
            'comment' => 'Number of times the item has been invoked (e.g., submit)'
        ),
        //Applicants flex related Applicant Student/Lead
        'parent_type' =>
        array (
            'name' => 'parent_type',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'enum',
            'dbType' => 'varchar',
            'required' => true,
            'group' => 'parent_name',
            'options' => 'record_type_display_notes',
            'len' => 100,
            'default' => 'Leads',
        ),
        'parent_name' =>
        array (
            'name' => 'parent_name',
            'parent_type' => 'record_type_display_notes',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'parent',
            'group' => 'parent_name',
            'source' => 'non-db',
            'options' => 'record_type_display_notes',
            'studio' => true,
            'required' => true,
            'reportable' => true,
        ),
        'parent_id' =>
        array (
            'name' => 'parent_id',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'id',
            'group' => 'parent_name',
            'reportable' => false,
            'required' => true,
        ),

        'hed_contacts' => array(
            'name'          => 'hed_contacts',
            'type'          => 'link',
            'relationship'  => 'contact_referencelog',
            'module'        => 'Contacts',
            'bean_name'     => 'Contacts',
            'source'        => 'non-db',
            'vname'         => 'LBL_REFERENCE_STUDENT',
        ),
        'hed_leads' => array(
            'name'          => 'hed_leads',
            'type'          => 'link',
            'relationship'  => 'lead_referencelog',
            'module'        => 'Leads',
            'bean_name'     => 'Leads',
            'source'        => 'non-db',
            'vname'         => 'LBL_REFERENCE_LEADS',
        ),
        'hed_prospects' => array(
            'name'          => 'hed_prospects',
            'type'          => 'link',
            'relationship'  => 'prospect_referencelog',
            'module'        => 'Leads',
            'bean_name'     => 'Leads',
            'source'        => 'non-db',
            'vname'         => 'LBL_REFERENCE_PROSPECTS',
        ),
        //UTM fields
        'utm_source' => array(
            'name' => 'utm_source',
            'vname' => 'LBL_UTM_SOURCE',
            'type' => 'varchar',
            'len' => 150,
            'importable' => true,
            'audited' => true,
        ),
        'utm_medium' => array(
            'name' => 'utm_medium',
            'vname' => 'LBL_UTM_MEDIUM',
            'type' => 'varchar',
            'len' => '150',
        ),
        'utm_term' => array(
            'name' => 'utm_term',
            'vname' => 'LBL_UTM_TERM',
            'type' => 'varchar',
            'len' => '150',
        ),
        'source_description' => array(
            'name' => 'source_description',
            'vname' => 'LBL_SOURCE_DESCRIPTION',
            'type' => 'varchar',
            'len' => '255',
        ),
        'utm_content' => array(
            'name' => 'utm_content',
            'vname' => 'LBL_UTM_CONTENT',
            'type' => 'varchar',
            'len' => '150',
        ),
        //End UTM Fields
        'reference' => array(
            'name' => 'reference',
            'vname' => 'LBL_REFERENCE',
            'type' => 'url',
            'len' => 255,
            'importable' => true,
        ),
        'lead_source' => array(
            'name' => 'lead_source',
            'vname' => 'LBL_LEAD_SOURCE',
            'type' => 'enum',
            'options' => 'lead_source_list',
            'len' => '100',
            'audited' => false,
            'merge_filter' => 'enabled',
            'required' => true,
            'importable' => 'required',
        ),
        'channel' => array(
            'name' => 'channel',
            'vname' => 'LBL_CHANNEL',
            'type' => 'enum',
            'len' => 255,
            'options' => 'utm_source_list',
            'importable' => true,
        ),

        'campaign_id' => array(
            'name' => 'campaign_id',
            'type' => 'id',
            'reportable' => false,
            'vname' => 'LBL_CAMPAIGN_NAME',
        ),
        'campaign_name' => array(
            'name' => 'campaign_name',
            'rname' => 'name',
            'id_name' => 'campaign_id',
            'vname' => 'LBL_CAMPAIGN_NAME',
            'type' => 'relate',
            'link' => 'campaign_referencelog',
            'table' => 'campaigns',
            'isnull' => 'true',
            'module' => 'Campaigns',
            'source' => 'non-db',
            'additionalFields' => array('id' => 'campaign_id'),
            'massupdate' => false,
        ),
        'campaign_referencelog' => array(
            'name' => 'campaign_referencelog',
            'type' => 'link',
            'vname' => 'LBL_CAMPAIGN_NAME',
            'relationship' => 'campaign_referencelog',
            'source' => 'non-db',
        ),
        'phone_mobile' => array(
            'name' => 'phone_mobile',
            'vname' => 'LBL_PHONE_MOBILE',
            'type' => 'varchar',
            'len' => '255',
        ),
        'email1' => array(
            'name' => 'email1',
            'vname' => 'LBL_EMAIL',
            'type' => 'varchar',
            'len' => '255',
        ),
    ),
    'indices' => array (
        array(
            'name' => 'idx_parent_id',
            'type' => 'index',
            'fields'=> array('parent_id'),
        ),
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_ReferenceLog','J_ReferenceLog', array('basic','assignable','taggable'));


//SEND MASTER MESSAGE - CONFIG
$module = 'J_ReferenceLog';
require_once("custom/include/utils/bmes.php");
$label      = strtolower(bmes::bmes_parent_type[$module]);
$dictionary[$module]['fields']['bsend_messages'] = array(
    'name' => 'bsend_messages',
    'type' => 'link',
    'relationship' => 'send_messages_' . $label,
    'module' => 'BMessage',
    'bean_name' => 'BMessage',
    'source' => 'non-db',
);

$dictionary[$module]['relationships']['send_messages_' . $label] = array(
    'lhs_module' => $module,
    'lhs_table' => strtolower($module),
    'lhs_key' => 'id',
    'rhs_module' => 'BMessage',
    'rhs_table' => 'bmessage',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => $module,
);
//END
