<?php

$module_name = 'J_ReferenceLog';
$viewdefs[$module_name]['base']['view']['subpanel-list'] = array(
    'panels' =>
    array(
        array(
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array(
                0 =>
                array (
                    'name' => 'name',
                    'label' => 'LBL_NAME',
                    'default' => true,
                    'enabled' => true,
                    'link' => true,
                ),
                array (
                    'name' => 'campaign_name',
                    'label' => 'LBL_CAMPAIGN_NAME',
                    'enabled' => true,
                    'id' => 'CAMPAIGN_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                array (
                    'name' => 'phone_mobile',
                    'label' => 'LBL_PHONE_MOBILE',
                    'enabled' => true,
                    'default' => true,
                ),
                array (
                    'name' => 'email1',
                    'label' => 'LBL_EMAIL',
                    'enabled' => true,
                    'default' => true,
                ),
                array (
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'default' => true,
                    'width' => 'large',
                ),
                array (
                    'name' => 'reference',
                    'label' => 'LBL_REFERENCE',
                    'enabled' => true,
                    'default' => true,
                ),

                array (
                    'name' => 'hits',
                    'label' => 'LBL_HITS',
                    'enabled' => true,
                    'default' => true,
                ),

                array (
                    'name' => 'lead_source',
                    'label' => 'LBL_LEAD_SOURCE',
                    'enabled' => true,
                    'default' => true,
                ),

                array (
                    'name' => 'channel',
                    'label' => 'LBL_CHANNEL',
                    'enabled' => true,
                    'default' => true,
                ),

                array (
                    'name' => 'date_modified',
                    'enabled' => true,
                    'default' => true,
                ),

                array (
                    'name' => 'date_entered',
                    'enabled' => true,
                    'default' => true,
                ),

                array (
                    'name' => 'parent_type',
                    'label' => 'LBL_PARENT_NAME',
                    'enabled' => true,
                    'default' => false,
                ),

                array (
                    'name' => 'created_by_name',
                    'label' => 'LBL_CREATED',
                    'enabled' => true,
                    'readonly' => true,
                    'id' => 'CREATED_BY',
                    'link' => true,
                    'default' => false,
                ),

                array (
                    'name' => 'modified_by_name',
                    'label' => 'LBL_MODIFIED',
                    'enabled' => true,
                    'readonly' => true,
                    'id' => 'MODIFIED_USER_ID',
                    'link' => true,
                    'default' => false,
                ),
            ),
        ),
    ),
    'orderBy' => array(
        'field' => 'date_modified',
        'direction' => 'desc',
    ),
);
