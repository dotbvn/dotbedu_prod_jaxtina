<?php



$dictionary['C_Siblings'] = array(
    'table' => 'c_siblings',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        //Relationship Payment Lead - Student
        'parent_type' =>
        array (
            'name' => 'parent_type',
            'vname' => 'LBL_B_PARENT_TYPE',
            'type' => 'enum',
            'dbType' => 'varchar',
            'required' => true,
            'group' => 'parent_name',
            'options' => 'parent_rel_type_list',
            'len' => 100,
            'default' => 'Contacts',
            'studio' => 'visible',
        ),
        'parent_name' =>
        array (
            'name' => 'parent_name',
            'parent_type' => 'parent_rel_type_list',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'vname' => 'LBL_B_PARENT_NAME',
            'type' => 'parent',
            'group' => 'parent_name',
            'source' => 'non-db',
            'options' => 'parent_rel_type_list',
            'studio' => 'visible',
            'required' => true,
            'reportable' => true,
        ),
        'parent_id' =>
        array (
            'name' => 'parent_id',
            'vname' => 'LBL_B_PARENT_NAME',
            'type' => 'id',
            'group' => 'parent_name',
            'reportable' => false,
            'required' => false,
        ),
        'link_siblings_contacts' => array(
            'name'          => 'link_siblings_contacts',
            'type'          => 'link',
            'relationship'  => 'siblings_contacts',
            'module'        => 'Contacts',
            'bean_name'     => 'Contacts',
            'source'        => 'non-db',
            'vname'         => 'LBL_B_STUDENT',
        ),
        'link_siblings_leads' => array(
            'name'          => 'link_siblings_leads',
            'type'          => 'link',
            'relationship'  => 'siblings_leads',
            'module'        => 'Leads',
            'bean_name'     => 'Leads',
            'source'        => 'non-db',
            'vname'         => 'LBL_B_LEAD',
        ),
        'link_siblings_prospects' => array(
            'name'          => 'link_siblings_prospects',
            'type'          => 'link',
            'relationship'  => 'siblings_prospects',
            'module'        => 'Prospects',
            'bean_name'     => 'Prospects',
            'source'        => 'non-db',
            'vname'         => 'LBL_B_PROSPECT',
        ),

        //STUDENT
        'student_name' => array (
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'student_name',
            'vname'     => 'LBL_STUDENT_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'student_id',
            'join_name' => 'contacts',
            'link'      => 'link_contacts',
            'table'     => 'contacts',
            'isnull'    => 'true',
            'module'    => 'Contacts',
        ),
        'student_id' => array (
            'name'              => 'student_id',
            'rname'             => 'id',
            'vname'             => 'LBL_STUDENT_NAME',
            'type'              => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),
        'link_contacts' => array (
            'name'          => 'link_contacts',
            'type'          => 'link',
            'relationship'  => 'rel_siblings_contacts',
            'module'        => 'Contacts',
            'bean_name'     => 'Contacts',
            'source'        => 'non-db',
            'vname'         => 'LBL_STUDENT_NAME',
        ),


        //LEAD
        'lead_name' => array (
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'lead_name',
            'vname'     => 'LBL_LEAD_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'student_id',
            'join_name' => 'contacts',
            'link'      => 'link_leads',
            'table'     => 'contacts',
            'isnull'    => 'true',
            'module'    => 'Leads',
        ),
        'link_leads' => array (
            'name'          => 'link_leads',
            'type'          => 'link',
            'relationship'  => 'rel_siblings_leads',
            'module'        => 'Leads',
            'bean_name'     => 'Leads',
            'source'        => 'non-db',
            'vname'         => 'LBL_LEAD_NAME',
        ),


        //PROSPECT
        'prospect_name' => array (
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'prospect_name',
            'vname'     => 'LBL_PROSPECT_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'student_id',
            'join_name' => 'prospects',
            'link'      => 'link_prospects',
            'table'     => 'prospects',
            'isnull'    => 'true',
            'module'    => 'Prospects',
        ),
        'link_prospects' => array (
            'name'          => 'link_prospects',
            'type'          => 'link',
            'relationship'  => 'rel_siblings_prospects',
            'module'        => 'Prospects',
            'bean_name'     => 'Prospects',
            'source'        => 'non-db',
            'vname'         => 'LBL_PROSPECT_NAME',
        ),

        'type' =>
        array (
            'required' => true,
            'name' => 'type',
            'vname' => 'LBL_TYPE',
            'type' => 'enum',
            'massupdate' => true,
            'importable' => 'true',
            'len' => 150,
            'size' => '20',
            'options' => 'sibling_type_list',
            'dependency' => false,
            'default' => 'Siblings',
        ),
        'is_referrer' =>
        array (
            'required' => false,
            'name' => 'is_referrer',
            'vname' => 'LBL_IS_REFERRER',
            'type' => 'bool',
            'massupdate' => false,
            'no_default' => false,
            'size' => '20',
            'default' => 1,
        ),


    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);
if (!class_exists('VardefManager')){
}
VardefManager::createVardef('C_Siblings','C_Siblings', array('basic','team_security','assignable','taggable'));