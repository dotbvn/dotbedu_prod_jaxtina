<?php



$dictionary['J_ClassStudents'] = array(
    'table' => 'j_classstudents',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (



        'student_type' =>
        array(
            'name' => 'student_type',
            'vname' => 'LBL_STUDENT_TYPE',
            'type' => 'enum',
            'dbType' => 'varchar',
            'required' => false,
            'group' => 'student_name',
            'options' => 'student_type_list',
            'len' => 100,
            'default' => 'Contacts',
            'studio' => 'visible',
        ),
        'student_name' => array(
            'name' => 'student_name',
            'parent_type' => 'student_type_list',
            'type_name' => 'student_type',
            'id_name' => 'student_id',
            'vname' => 'LBL_STUDENT',
            'type' => 'parent',
            'group' => 'student_name',
            'source' => 'non-db',
            'options' => 'student_type_list',
            'studio' => true,
            'required' => true
        ),
        'student_id' => array(
            'name' => 'student_id',
            'vname' => 'LBL_STUDENT',
            'type' => 'id',
            'group' => 'student_name',
            'reportable' => false,
        ),
        'contact_link' => array(
            'name'          => 'contact_link',
            'type'          => 'link',
            'relationship'  => 'class_students',
            'module'        => 'Contacts',
            'bean_name'     => 'Contact',
            'source'        => 'non-db',
            'vname'         => 'LBL_STUDENT',
        ),
        'lead_link' => array(
            'name'          => 'lead_link',
            'type'          => 'link',
            'relationship'  => 'class_leads',
            'module'        => 'Leads',
            'bean_name'     => 'Lead',
            'source'        => 'non-db',
            'vname'         => 'LBL_LEAD',
        ),
        //Class
        'class_name' => array(
            'required'  => true,
            'source'    => 'non-db',
            'name'      => 'class_name',
            'vname'     => 'LBL_CLASS_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'class_id',
            'join_name' => 'j_class',
            'link'      => 'class_link',
            'table'     => 'j_class',
            'isnull'    => 'true',
            'module'    => 'J_Class',
        ),

        'class_id' => array(
            'name'              => 'class_id',
            'rname'             => 'id',
            'vname'             => 'LBL_CLASS_NAME',
            'type'              => 'id',
            'table'             => 'j_class',
            'isnull'            => 'true',
            'module'            => 'J_Class',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'class_link' => array(
            'name'          => 'class_link',
            'type'          => 'link',
            'relationship'  => 'class_classstudents',
            'module'        => 'J_Class',
            'bean_name'     => 'J_Class',
            'source'        => 'non-db',
            'vname'         => 'LBL_CLASS_NAME',
        ),
        //LMS
        'lms_enrollment_id' =>
        array (
            'name' => 'lms_enrollment_id',
            'type' => 'id',
        ),
        //ONLINE LEARNING : EX CLASSIN
        'onl_enrolled' => array(
            'name' => 'onl_enrolled',
            'type' => 'bool',
            'default'=>0
        ),
        //Runtime cache fields
        'status' => array(
            'name' => 'status',
            'vname' => 'LBL_STATUS',
            'type' => 'enum',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'len' => 50,
            'size' => '20',
            'options' => 'situation_status_list',
            'studio' => 'visible',
            'massupdate' => 0,
        ),
        'total_ssc' =>
        array (
            'required' => false,
            'name' => 'total_ssc',
            'vname' => 'LBL_TOTAL_SESSION_COMPLETED',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'total_ss_registered' =>
        array (
            'required' => false,
            'name' => 'total_ss_registered',
            'vname' => 'LBL_TOTAL_SESSION_REGISTERED',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'total_hc' =>
        array (
            'required' => false,
            'name' => 'total_hc',
            'vname' => 'LBL_TOTAL_HOURS_COMPLETED',
            'type' => 'decimal',
            'len' => 13,
            'size' => '20',
            'precision' => 2,
        ),
        'total_amount' =>
        array (
            'required' => false,
            'name' => 'total_amount',
            'vname' => 'LBL_TOTAL_AMOUNT',
            'type' => 'decimal',
            'len' => 26,
            'precision' => 6,
            'display_precision' => 0,
        ),
        'total_hrs' =>
        array (
            'required' => false,
            'name' => 'total_hrs',
            'vname' => 'LBL_TOTAL_HRS',
            'type' => 'decimal',
            'len' => 13,
            'size' => '20',
            'precision' => 2,
        ),
        'count_ost' =>
        array (
            'required' => false,
            'name' => 'count_ost',
            'vname' => 'LBL_COUNT_OST_COMPLETED',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'count_ern' =>
        array (
            'required' => false,
            'name' => 'count_ern',
            'vname' => 'LBL_COUNT_ERN_COMPLETED',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'count_demo' =>
        array (
            'required' => false,
            'name' => 'count_demo',
            'vname' => 'LBL_COUNT_DEMO_COMPLETED',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'total_attended' =>
        array (
            'required' => false,
            'name' => 'total_attended',
            'vname' => 'LBL_TOTAL_ATTENDED',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'total_absent' =>
        array (
            'required' => false,
            'name' => 'total_absent',
            'vname' => 'LBL_TOTAL_ABSENT',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'total_dohw' =>
        array (
            'required' => false,
            'name' => 'total_dohw',
            'vname' => 'LBL_TOTAL_DOHW',
            'type' => 'int',
            'len' => '10',
            'size' => '20',
            'min' => false,
            'max' => false,
        ),
        'start_study' =>
        array (
            'required' => false,
            'name' => 'start_study',
            'vname' => 'LBL_START_STUDY',
            'type' => 'date',
            'massupdate' => false,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => false,
        ),
        'end_study' =>
        array (
            'required' => false,
            'name' => 'end_study',
            'vname' => 'LBL_END_STUDY',
            'type' => 'date',
            'massupdate' => false,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => false,
        ),
        'avg_attendance' =>
        array(
            'required' => true,
            'name' => 'avg_attendance',
            'vname' => 'LBL_AVG_ATTENDANCE',
            'type' => 'decimal',
            'len' => '13',
            'size' => '20',
            'precision' => '6',
            'display_precision' => 2,
        ),

        //END _ Runtime cache fields


        'delay_from_date' =>
        array (
            'required' => true,
            'name' => 'delay_from_date',
            'vname' => 'LBL_DELAY_FROM_DATE',
            'type' => 'date',
            'massupdate' => false,
            'no_default' => false,
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => true,
        ),

        'process_trigger' => array(
            'required' => false,
            'name' => 'process_trigger',
            'vname' => 'Process/Workflow Trigger',
            'type' => 'bool',
            'source' => 'non-db',
            'processes' => true, //Áp đặt kích hoạt Process
        ),

        'process_action' => array(
            'name' => 'process_action',
            'vname' => 'LBL_PROCESS_ACTION',
            'type' => 'enum',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'len' => 50,
            'size' => '20',
            'options' => 'situation_process_action_list',
            'studio' => 'visible',
            'massupdate' => 0,
        ),
        'run_time' =>
        array (
            'name' => 'run_time',
            'vname' => 'LBL_RUN_TIME',
            'type' => 'date',
            'comment' => 'Run-time',
            'importable' => false,
            'required' => false,
            'massupdate' => false,
            'audited' => false,
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
            'no_default' => true,
        ),

        'type' =>
        array (
            'required' => false,
            'name' => 'type',
            'vname' => 'LBL_TYPE',
            'type' => 'enum',
            'massupdate' => true,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'default' => '',
            'calculated' => false,
            'len' => 100,
            'size' => '20',
            'options' => 'situation_type_list',
            'dependency' => false,
        ),
        'dl_reason_for' => array (
            'name' => 'dl_reason_for',
            'vname' => 'LBL_DL_REASON_FOR',
            'type' => 'enum',
            'len' => 150,
            'options' => 'dl_reason_for_options',
            'studio' => 'false',
            'processes' => true,
        ),

        //Relationship Session ( 1 - n ) - Lap Nguyen
        'meeting_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'meeting_name',
            'vname'     => 'LBL_MEETING_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'meeting_id',
            'join_name' => 'meetings',
            'link'      => 'meeting_attendances',
            'table'     => 'meetings',
            'isnull'    => 'true',
            'module'    => 'Meetings',
        ),
        'meeting_id' => array(
            'name'              => 'meeting_id',
            'rname'             => 'id',
            'vname'             => 'LBL_MEETING_NAME',
            'type'              => 'id',
            'table'             => 'meetings',
            'isnull'            => 'true',
            'module'            => 'Meetings',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),
        'meeting_link' => array (
            'name'          => 'meeting_link',
            'type'          => 'link',
            'relationship'  => 'meeting_class_std',
            'module'        => 'Meetings',
            'bean_name'     => 'Meetings',
            'source'        => 'non-db',
            'vname'         => 'LBL_MEETING_NAME',
        ),
        //END: Relationship
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

//SEND MASTER MESSAGE - CONFIG
$module = 'J_ClassStudents';
require_once("custom/include/utils/bmes.php");
$label      = strtolower(bmes::bmes_parent_type[$module]);
$dictionary[$module]['fields']['bsend_messages'] = array (
    'name' => 'bsend_messages',
    'type' => 'link',
    'relationship' => 'send_messages_'.$label,
    'module' => 'BMessage',
    'bean_name' => 'BMessage',
    'source' => 'non-db',
);

$dictionary[$module]['relationships']['send_messages_'.$label] = array (
    'lhs_module'        => $module,
    'lhs_table'            => strtolower($module),
    'lhs_key'            => 'id',
    'rhs_module'        => 'BMessage',
    'rhs_table'            => 'bmessage',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => $module,
);
//END

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_ClassStudents','J_ClassStudents', array('basic','assignable','taggable'));
