<?php
//require_once('nusoap/nusoap.php');
require_once("modules/C_SMS/SMS/SMS_Provider.php");
class SMSSender{
    /**
    * Function send a sms to target
    *
    * @param mixed $phone_number
    * @param mixed $content content of  sms
    * @return mixed
    */

    function sendSMS($phone_number = '', $content = '', $team_id = '', $smsId = ''){
        global $current_user;
        if (empty($team_id)) $team_id = $current_user->team_id;
        $sms_config = $GLOBALS['db']->getOne("SELECT IFNULL(sms_config, '') sms_config FROM teams WHERE id = '$team_id'");
        $sms_config = json_decode(html_entity_decode($sms_config), true);
        if(empty($sms_config)) return 0;
        if(empty($phone_number)) return 0;

        $ws_server    = $sms_config['sms_ws_link'];
        $ws_pass      = $sms_config['sms_ws_pass'];
        $ws_account   = $sms_config['sms_ws_account'];
        $ws_brandname = $sms_config['sms_ws_brandname'];
        $ws_supplier  = $sms_config['sms_ws_supplier'];
        $ws_groupid   = $sms_config['sms_ws_groupid'];
        $ws_deptid    = $sms_config['sms_ws_deptid'];

        if(!in_array($ws_supplier, $GLOBALS['app_list_strings']['sms_supplier_no_parse']))
            $phone_number = parsePhoneNumber($phone_number);

        //Fix loi khong gui duoc SMS
        $SMS_Provider      = new SMS_Provider($ws_server, $ws_account, $ws_pass);
        $result['success'] = $SMS_Provider->send_sms($phone_number, $content, $ws_brandname, $ws_supplier, $ws_groupid, $ws_deptid, $smsId);
        return $result;
    }
}

?>
