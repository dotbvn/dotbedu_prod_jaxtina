<?php
require_once ("modules/C_SMS/SMS/sms_interface.php");
class sms implements sms_interface {
    //sending on ems
    function send_message($phone_number, $text, $parent_type, $parent_id, $user_id, $team_id='', $template_id='', $date_in_content='') {
        $sms               = new C_SMS();
//        $sms->id           = create_guid();
//        $sms->new_with_id  = true;
        $sms->description  = $text;  //text message
        $sms->parent_type  = $parent_type;
        $sms->parent_id    = $parent_id;
        $sms->template_id  = $template_id;
        $sms->phone_number = $phone_number;
        $sms->date_in_content = $date_in_content;
        $sms->assigned_user_id    = $user_id;
        $sms->team_id             = $team_id;
        $sms->team_set_id         = $team_id;
        $sms->save();
        $response = 1;
        if($sms->delivery_status == 'RECEIVED')   $response = 1;
        if($sms->delivery_status == 'FAILED')     $response = 0;
        if($sms->delivery_status == 'DELIVERED')  $response = 2;
        return $response;
    }
    //sending SMS on EMS
    private function send_to_multi($to_array, $text, $parent_type, $template_id='', $date_in_content='', $team_id=''){
        $summary = array();
        for($i = 0; $i<count($to_array); $i++){
            $arrData    = $to_array[$i];
            $name       = $arrData[0];
            $parent_id  = $arrData[2];
            $phone_number = preg_replace('/[^0-9]/', '', $arrData[1]);
            //Can not detect phone number
            if (empty($phone)){
                $summary[$i] = array(0 => 'Send to '.$name.'<empty phone number>', 1 => 'Failed');
                continue;
            }
            $sms               = new C_SMS();
            $sms->description  = $text;  //text message
            $sms->parent_type  = $parent_type;
            $sms->parent_id    = $parent_id;
            $sms->template_id  = $template_id;
            $sms->phone_number = $phone_number;
            $sms->date_in_content = $date_in_content;
            $sms->assigned_user_id= $GLOBALS['current_user']->id;
            $sms->team_id         = $team_id;
            $sms->team_set_id     = $team_id;
            $sms->save();
            $summary[$i] = array(0 => 'Send to '.$name.' - '.$phone_number, 1 => $sms->delivery_status);
        }
        return $summary;
    }
    # need to create batch sending on EMS
    function send_batch_message($to_array, $text, $parent_type, $template_id='', $date_in_content='') {
        global $current_user;
        $summary = $this->send_to_multi($to_array, $text, $parent_type, $template_id, $date_in_content);
        return $summary;

    }
}
?>
