<?php



$dictionary['C_Comments'] = array(
    'table' => 'c_comments',
    'audited' => true,
    'activity_enabled' => false,
    'fields' => array (
        'direction' => array(
            'len' => 50,
            'name' => 'direction',
            'options' => 'comments_direction_options',
            'required' => true,
            'type' => 'enum',
            'dbType' => 'varchar',
            'vname' => 'LBL_DIRECTION',
            'default' => 'outbound'
        ),
        'is_read_inapp' => array(
            'required' => false,
            'name' => 'is_read_inapp',
            'vname' => 'LBL_IS_READ_INAPP',
            'type' => 'bool',
            'massupdate' => true,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 0,
            'default' => 1,
            'reportable' => 1,
            'studio' => false,
        ),
        'is_read_inems' => array(
            'required' => false,
            'name' => 'is_read_inems',
            'vname' => 'LBL_IS_READ_INEMS',
            'type' => 'bool',
            'massupdate' => true,
            'comments' => '',
            'help' => '',
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => 0,
            'default' => 1,
            'reportable' => 1,
            'studio' => false,
        ),
        'namee' => array(
            'name' => 'namee',
            'type' => 'varchar',
            'label' => 'LBL_DESCRIPTION',
            'len' => 100,
            'source'=>'non-db',
        ),
        //Custom Relationship. Feedback  - Comment (1-n)  By Lap Nguyen
        'parent_type' =>
        array (
            'name' => 'parent_type',
            'vname' => 'LBL_PARENT_TYPE',
            'type' => 'enum',
            'dbType' => 'varchar',
            'required' => false,
            'group' => 'parent_name',
            'options' => 'related_comment_list',
            'len' => 100,
            'default' => 'Cases',
            'studio' => 'visible',
        ),
        'parent_name' =>
        array (
            'name' => 'parent_name',
            'parent_type' => 'related_comment_list',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'parent',
            'group' => 'parent_name',
            'source' => 'non-db',
            'options' => 'related_comment_list',
            'studio' => 'visible',
            'required' => false,
            'reportable' => true,
        ),
        'parent_id' =>
        array (
            'name' => 'parent_id',
            'vname' => 'LBL_PARENT_ID',
            'type' => 'id',
            'group' => 'parent_name',
            'reportable' => false,
            'required' => false,
        ),
        'feedback_link' => array(
            'name'          => 'feedback_link',
            'type'          => 'link',
            'relationship'  => 'comments_cases',
            'module'        => 'Cases',
            'bean_name'     => 'Cases',
            'source'        => 'non-db',
            'vname'         => 'LBL_COMMENT_CASE_LINK',
        ),
        'gallery_link' => array(
            'name'          => 'gallery_link',
            'type'          => 'link',
            'relationship'  => 'comments_c_gallery',
            'module'        => 'C_Gallery',
            'bean_name'     => 'C_Gallery',
            'source'        => 'non-db',
            'vname'         => 'LBL_COMMENT_C_GALLERY_LINK',
        ),
        'news_link' => array(
            'name'          => 'news_link',
            'type'          => 'link',
            'relationship'  => 'comments_c_news',
            'module'        => 'C_News',
            'bean_name'     => 'C_News',
            'source'        => 'non-db',
            'vname'         => 'LBL_COMMENT_C_NEWS_LINK',
        ),
        //END: Feedback  - Comment (1-n)  By Lap Nguyen

        //Custom field for Portal - HoangHvy
        'first_created_by_portal' => array(
            'name'          => 'first_created_by_portal',
            'type'          => 'bool',
            'source'        => 'non-db',
        ),
    ),
    'relationships' => array (),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}

//SEND MASTER MESSAGE - CONFIG
$module = 'C_Comments';
require_once("custom/include/utils/bmes.php");
$label      = strtolower(bmes::bmes_parent_type[$module]);
$dictionary[$module]['fields']['bsend_messages'] = array (
    'name' => 'bsend_messages',
    'type' => 'link',
    'relationship' => 'send_messages_'.$label,
    'module' => 'BMessage',
    'bean_name' => 'BMessage',
    'source' => 'non-db',
);

$dictionary[$module]['relationships']['send_messages_'.$label] = array (
    'lhs_module'        => $module,
    'lhs_table'            => strtolower($module),
    'lhs_key'            => 'id',
    'rhs_module'        => 'BMessage',
    'rhs_table'            => 'bmessage',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => $module,
);
//END


VardefManager::createVardef('C_Comments','C_Comments', array('basic','assignable','taggable','file'));
