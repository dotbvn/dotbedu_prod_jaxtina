<?php



$dictionary['J_LessonPlan'] = array(
    'table' => 'j_lessonplan',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'lp_link'=>array(
            'name' => 'lp_link',
            'type' => 'link',
            'relationship' => 'lp_syllabus_rel',
            'module' => 'J_Syllabus',
            'bean_name' => 'J_Syllabus',
            'source' => 'non-db',
            'vname' => 'LBL_SYLLABUS',
        ),
    ),
    'relationships' => array (
        //Add Relationship Lesson Plan - Syllabus
        'lp_syllabus_rel' => array(
            'lhs_module' => 'J_LessonPlan',
            'lhs_table' => 'j_lessonplan',
            'lhs_key' => 'id',
            'rhs_module' => 'J_Syllabus',
            'rhs_table' => 'j_syllabus',
            'rhs_key' => 'lessonplan_id',
            'relationship_type' => 'one-to-many'
        ),
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_LessonPlan','J_LessonPlan', array('basic','team_security','assignable','taggable'));
