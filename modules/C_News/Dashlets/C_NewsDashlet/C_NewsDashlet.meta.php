<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_NewsDashlet'] = array('module'		=> 'C_News',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_News'), 
                                          'description' => 'A customizable view into C_News',
                                          'icon'        => 'icon_C_News_32.gif',
                                          'category'    => 'Module Views');