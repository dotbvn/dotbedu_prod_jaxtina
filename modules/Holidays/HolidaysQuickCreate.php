<?php





class HolidaysQuickCreate extends QuickCreate {

    var $javascript;

    function process() {
        global $current_user, $timedate, $app_list_strings, $current_language, $mod_strings;
        $mod_strings = return_module_language($current_language, 'Holidays');

        parent::process();

        $this->ss->assign("CALENDAR_DATEFORMAT", $timedate->get_cal_date_format());

        if($this->viaAJAX) { // override for ajax call
            $this->ss->assign('saveOnclick', "onclick='if(check_form(\"holidaysQuickCreate\")) return DOTB.subpanelUtils.inlineSave(this.form.id, \"holidays\"); else return false;'");
            $this->ss->assign('cancelOnclick', "onclick='return DOTB.subpanelUtils.cancelCreate(\"subpanel_holidays\")';");
        }

        $this->ss->assign('viaAJAX', $this->viaAJAX);

        $this->javascript = new javascript();
        $this->javascript->setFormName('holidayQuickCreate');

        $focus = BeanFactory::newBean('Holidays');
        $this->javascript->setDotbBean($focus);
        $this->javascript->addAllFields('');

        $this->ss->assign('additionalScripts', $this->javascript->getScript(false));
    }
}
?>