<?php


use Dotbcrm\Dotbcrm\Entitlements\SubscriptionManager;

class AuthApi extends DotbApi
{
    const CONNECTOR_LABELS = [
        'GoogleEmail' => 'LBL_SMTPTYPE_GOOGLE_OAUTH2',
        'MicrosoftEmail' => 'LBL_SMTPTYPE_MICROSOFT',
    ];

    public function registerApiRest()
    {
        return [
            'getAuthInfo' => [
                'reqType'   => 'GET',
                'path' => array('EAPM', 'auth'),
                'pathVars' => array('module', ''),
                'method'    => 'getAuthInfo',
                'shortHelp' => 'Get auth info for an application',
                'longHelp'  => 'include/api/help/module_get_help.html',
            ],
        ];
    }

    /**
     * Gets auth url for an application.
     *
     * @param ServiceBase $api The API class of the request
     * @param array $args The arguments array passed in from the API
     * @return array Auth URL
     * @throws DotbApiExceptionMissingParameter
     */
    public function getAuthInfo(ServiceBase $api, array $args): array
    {
        $this->requireArgs($args, ['application']);
        $data = ['auth_warning' => 'Connector has not been configured.'];
        $extApi = $this->getExternalApi($args['application']);
        if ($extApi) {
            $client = $extApi->getClient();
            $data['auth_url'] = $client->createAuthUrl();
        }
        return $data;
    }

    /**
     * Gets warning message for oauth2 connector.
     *
     * @param string $application
     * @return string
     */
    public function getAuthWarning(string $application): string
    {
        global $current_user;

        $docUrl = 'https://www.dotbcrm.com/crm/product_doc.php?edition=' . $GLOBALS['dotb_flavor'] . '&version=' .
            $GLOBALS['dotb_version'] . '&lang=' . $GLOBALS['current_language'] . '&module=Email&route=Outgoing';
        $productCodes = $current_user->getProductCodes();
        $productCodes = urlencode(implode(',', $productCodes));
        $docUrl .= '&products=' . $productCodes;
        $docLink = '<a href="' . $docUrl . '" target="_blank" rel="nofollow noopener noreferrer">' . translate('LBL_EMAILS') . '</a>';
        $connectorName = translate(self::CONNECTOR_LABELS[$application] ?? '');
        return string_format(translate('LBL_EMAIL_AUTH_WARNING'), [$connectorName]);
    }

    /**
     * Gets external api object for an application.
     *
     * @param string $application
     * @return ExternalAPIBase|bool
     */
    public function getExternalApi(string $application)
    {
        return ExternalAPIFactory::loadAPI($application, true);
    }
}
