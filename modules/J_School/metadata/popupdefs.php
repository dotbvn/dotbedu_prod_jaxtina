<?php

$module_name = 'J_School';
$object_name = 'J_School';
$_module_name = 'j_school';
$popupMeta = array('moduleMain' => $module_name,
    'varName' => $object_name,
    'orderBy' => $_module_name.'.name',
    'whereClauses' =>
        array('name' => $_module_name . '.name',
        ),
    'searchInputs'=> array($_module_name. '_number', 'name', 'priority','status'),

);
