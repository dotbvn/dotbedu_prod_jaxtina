<?php



$dictionary['J_School'] = array(
    'table' => 'j_school',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'level' =>
        array (
            'required' => false,
            'name' => 'level',
            'vname' => 'LBL_LEVEL',
            'type' => 'enum',
            'massupdate' => 0,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => 100,
            'size' => '20',
            'options' => 'level_school_list',
            'studio' => 'visible',
            'dependency' => false,
            'default'=>' ',
        ),
        'billing_address_street' =>
        array (
            'name' => 'billing_address_street',
            'vname' => 'LBL_BILLING_ADDRESS_STREET',
            'type' => 'text',
            'dbType' => 'varchar',
            'len' => '150',
            'comment' => 'The street address used for billing address',
            'group'=>'billing_address',
            'group_label' => 'LBL_BILLING_ADDRESS',
            'merge_filter' => 'enabled',
            'duplicate_on_record_copy' => 'always',
            'full_text_search' => array(
                'enabled' => true,
                'searchable' => true,
                'boost' => 0.26,
            ),
        ),
        'billing_address_street_2' =>
        array (
            'name' => 'billing_address_street_2',
            'vname' => 'LBL_BILLING_ADDRESS_STREET_2',
            'type' => 'varchar',
            'len' => '150',
            'source'=>'non-db',
            'duplicate_on_record_copy' => 'always',
        ),
        'billing_address_street_3' =>
        array (
            'name' => 'billing_address_street_3',
            'vname' => 'LBL_BILLING_ADDRESS_STREET_3',
            'type' => 'varchar',
            'len' => '150',
            'source'=>'non-db',
            'duplicate_on_record_copy' => 'always',
        ),
        'billing_address_street_4' =>
        array (
            'name' => 'billing_address_street_4',
            'vname' => 'LBL_BILLING_ADDRESS_STREET_4',
            'type' => 'varchar',
            'len' => '150',
            'source'=>'non-db',
            'duplicate_on_record_copy' => 'always',
        ),
        'billing_address_city' =>
        array (
            'name' => 'billing_address_city',
            'vname' => 'LBL_BILLING_ADDRESS_CITY',
            'type' => 'varchar',
            'len' => '100',
            'comment' => 'The city used for billing address',
            'group'=>'billing_address',
            'merge_filter' => 'enabled',
            'duplicate_on_record_copy' => 'always',
        ),
        'billing_address_state' =>
        array (
            'name' => 'billing_address_state',
            'vname' => 'LBL_BILLING_ADDRESS_STATE',
            'type' => 'varchar',
            'len' => '100',
            'group'=>'billing_address',
            'comment' => 'The state used for billing address',
            'merge_filter' => 'enabled',
            'duplicate_on_record_copy' => 'always',
        ),
        'billing_address_postalcode' =>
        array (
            'name' => 'billing_address_postalcode',
            'vname' => 'LBL_BILLING_ADDRESS_POSTALCODE',
            'type' => 'varchar',
            'len' => '20',
            'group'=>'billing_address',
            'comment' => 'The postal code used for billing address',
            'merge_filter' => 'enabled',
            'duplicate_on_record_copy' => 'always',
        ),
        'billing_address_country' =>
        array (
            'name' => 'billing_address_country',
            'vname' => 'LBL_BILLING_ADDRESS_COUNTRY',
            'type' => 'varchar',
            'group'=>'billing_address',
            'comment' => 'The country used for the billing address',
            'merge_filter' => 'enabled',
            'duplicate_on_record_copy' => 'always',
        ),

        'website' =>
        array (
            'required' => false,
            'name' => 'website',
            'vname' => 'LBL_WEBSITE',
            'type' => 'varchar',
            'len' => 100,
            'size' => '20',
            'audited' => true,
        ),
        'phone' =>
        array (
            'required' => false,
            'name' => 'phone',
            'vname' => 'LBL_PHONE',
            'type' => 'varchar',
            'len' => 100,
            'size' => '20',
            'audited' => true,
        ),
        'fax' =>
        array (
            'required' => false,
            'name' => 'fax',
            'vname' => 'LBL_FAX',
            'type' => 'varchar',
            'len' => 100,
            'size' => '20',
            'audited' => true,
        ),
        'email' =>
        array (
            'required' => false,
            'name' => 'email',
            'vname' => 'LBL_EMAIL',
            'type' => 'varchar',
            'len' => 100,
            'size' => '20',
            'audited' => true,
        ),

                //Add school link
        'prospect_link'=>array(
            'name' => 'prospect_link',
            'type' => 'link',
            'relationship' => 'j_school_prospect',
            'module' => 'Prospects',
            'bean_name' => 'Prospects',
            'source' => 'non-db',
            'vname' => 'LBL_PROSPECT',
        ),
        'lead_link'=>array(
            'name' => 'lead_link',
            'type' => 'link',
            'relationship' => 'j_school_lead',
            'module' => 'Leads',
            'bean_name' => 'Leads',
            'source' => 'non-db',
            'vname' => 'LBL_LEAD',
        ),
        'contact_link'=>array(
            'name' => 'contact_link',
            'type' => 'link',
            'relationship' => 'j_school_contact',
            'module' => 'Contacts',
            'bean_name' => 'Contacts',
            'source' => 'non-db',
            'vname' => 'LBL_CONTACT',
        ),
    ),
    'relationships' => array (
      //Add Relationship
        'j_school_prospect' => array(
            'lhs_module' => 'J_School',
            'lhs_table' => 'j_school',
            'lhs_key' => 'id',
            'rhs_module' => 'Prospects',
            'rhs_table' => 'prospects',
            'rhs_key' => 'school_id',
            'relationship_type' => 'one-to-many'
        ),

        'j_school_lead' => array(
            'lhs_module' => 'J_School',
            'lhs_table' => 'j_school',
            'lhs_key' => 'id',
            'rhs_module' => 'Leads',
            'rhs_table' => 'leads',
            'rhs_key' => 'school_id',
            'relationship_type' => 'one-to-many'
        ),

        'j_school_contact' => array(
            'lhs_module' => 'J_School',
            'lhs_table' => 'j_school',
            'lhs_key' => 'id',
            'rhs_module' => 'Contacts',
            'rhs_table' => 'contacts',
            'rhs_key' => 'school_id',
            'relationship_type' => 'one-to-many'
        ),
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_School','J_School', array('basic','assignable','taggable'));
