<?php


class UserHooks {
    /** Added by LapNguyen to remove User Teacher/TA From selection list
     * @param DotbBean $team
     * @param $event
     * @param $args
     */
    public static function RemoveTeacherUsers(DotbBean $team, $event, $args) {
        $args[0]->where()->isEmpty('teacher_id');
    }
}
