<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_GradebookConfigDashlet'] = array('module'		=> 'J_GradebookConfig',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_GradebookConfig'), 
                                          'description' => 'A customizable view into J_GradebookConfig',
                                          'icon'        => 'icon_J_GradebookConfig_32.gif',
                                          'category'    => 'Module Views');