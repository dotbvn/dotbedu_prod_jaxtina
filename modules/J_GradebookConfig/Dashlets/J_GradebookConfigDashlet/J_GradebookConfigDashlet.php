<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_GradebookConfig/J_GradebookConfig.php');

class J_GradebookConfigDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_GradebookConfig/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_GradebookConfig');

        $this->searchFields = $dashletData['J_GradebookConfigDashlet']['searchFields'];
        $this->columns = $dashletData['J_GradebookConfigDashlet']['columns'];

        $this->seedBean = new J_GradebookConfig();        
    }
}