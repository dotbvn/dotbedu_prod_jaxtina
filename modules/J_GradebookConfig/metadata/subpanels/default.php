<?php


    $module_name='J_GradebookConfig';
    $subpanel_layout = array(
        'top_buttons' => array(
            array('widget_class' => 'SubPanelTopCreateButton'),
            array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name),
        ),

        'where' => '',

        'list_fields' => array(
            'gradebook_setting_name'=>array(
                'vname' => 'LBL_GRADEBOOK_SETTING_NAME',
                'widget_class' => 'SubPanelDetailViewLink',
                'width' => '15%',
            ),
            'name'=>array(
                'vname' => 'LBL_NAME',
                'widget_class' => 'SubPanelDetailViewLink',
                'width' => '20%',
            ),
            'type'=>array(
                'vname' => 'LBL_TYPE',
                'width' => '15%',
            ),
            'description' => array (
                'width' => '15%',
                'vname' => 'LBL_DESCRIPTION',
            ),
            'assigned_user_name' => array (
                'width' => '10%',
                'vname' => 'LBL_ASSIGNED_TO_NAME',
            ),
            'date_modified'=>array(
                'vname' => 'LBL_DATE_MODIFIED',
                'width' => '10%',
            ),
            'edit_button'=>array(
                'vname' => 'LBL_EDIT_BUTTON',
                'widget_class' => 'SubPanelEditButton',
                'module' => $module_name,
                'width' => '4%',
            ),
            'remove_button'=>array(
                'vname' => 'LBL_REMOVE',
                'widget_class' => 'SubPanelRemoveButton',
                'module' => $module_name,
                'width' => '5%',
            ),
        ),
    );

?>