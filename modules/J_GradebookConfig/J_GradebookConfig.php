<?PHP

    /**
    * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
    */
    require_once('modules/J_GradebookConfig/J_GradebookConfig_dotb.php');
    class J_GradebookConfig extends J_GradebookConfig_dotb {
        //config cot diem trong config
        //progress
        var $gradebook_config_P = array(
            'TT' => array(
                'name' => 'col0',
                'alias' => 'TT',
                'label' => 'Col 0',
                'group' => '',
                'type'  => 'score',

                'visible' => false,
                'max_mark' => 10,
                'readonly' => false,
                'formula' => '',
            ),
            'A' => array(
                'name' => 'col1',
                'alias' => 'A',
                'label' => 'Col 1',
                'group' => '',
                'type'  => 'score',

                'visible' => false,
                'max_mark' => 10,
                'readonly' => false,
                'formula' => '',
            ),
            'B' => array(
                'name' => 'col2',
                'alias' => 'B',
                'label' => 'Col 2',
                'group' => '',
                'type'  => 'score',

                'visible' => false,
                'max_mark' => 10,
                'readonly' => false,
                'formula' => '',
            ),
            'Z' => array(
                'name' => 'overall',
                'alias' => 'Z',
                'label' => 'Overall',
                'group' => '',
                'type'  => 'total',

                'visible' => true,
                'max_mark' => 10,
                'readonly' => true,
                'formula' => 'A+B',
            ),
        );

        // overall
        var $gradebook_config_Overall =  array(
            'TT' => array(
                'name' => 'col0',
                'alias' => 'TT',
                'label' => 'Col 0',
                'group' => '',
                'type'  => 'score',

                'visible' => false,
                'max_mark' => 10,
                'readonly' => false,
                'formula' => '',
            ),
            'A' => array(
                'name' => 'col1',
                'alias' => 'A',
                'label' => 'Col 1',
                'group' => '',
                'type'  => 'score',

                'visible' => false,
                'max_mark' => 10,
                'readonly' => false,
                'formula' => '',
            ),
            'B' => array(
                'name' => 'col2',
                'alias' => 'B',
                'label' => 'Col 2',
                'group' => '',
                'type'  => 'score',

                'visible' => false,
                'max_mark' => 10,
                'readonly' => false,
                'formula' => '',
            ),
            'Z' => array(
                'name' => 'overall',
                'alias' => 'Z',
                'label' => 'Overall',
                'group' => '',
                'type'  => 'total',

                'visible' => true,
                'max_mark' => 10,
                'readonly' => true,
                'formula' => 'A+B',
            ),
        );

        public function loadConfigContent() {
            $config_array = json_decode(html_entity_decode($this->content),true);
            if(empty($config_array) || $config_array == '{}' || !is_array($config_array))
                $config_array = array();

            return $this->toHtmlContent($config_array);
        }
        function getMaxMark($gbSettingId){
            $return = array();
//            foreach ($GLOBALS['app_list_strings']['gradebook_type_options'] as $option => $value) $return[$option] = 0;
            $sql = "SELECT DISTINCT
            IFNULL(l2.id, '') primaryId,
            IFNULL(l2.type, '') type,
            IFNULL(l2.content, '') content
            FROM j_gradebookconfig
            INNER JOIN j_gradebooksettinggroup l1 ON j_gradebookconfig.gbsettinggroup_id = l1.id AND l1.deleted = 0
            INNER JOIN j_gradebookconfig l2 ON l1.id = l2.gbsettinggroup_id AND l2.deleted = 0
            WHERE (j_gradebookconfig.id = '$gbSettingId')
            AND j_gradebookconfig.deleted = 0";
            $result = $GLOBALS['db']->query($sql);

            while($row = $GLOBALS['db']->fetchByAssoc($result)){
                $content = json_decode(html_entity_decode($row['content']), true);
                $return[$row['type']] = array();
                foreach ($content as $key=>$value){
                    if($value['type'] == 'total' || $value['type'] == 'formula' || $value['type'] == 'score')
                        $return[$row['type']][$key] = $value['max_mark'];
                    if($value['type'] == 'total')
                        $max_mark = $value['max_mark'];
                }
                $return[$row['type']]['final'] = "{$max_mark}";
            }
            return json_encode($return);
        }
        public function toHtmlContent($configs) {
            if(empty($this->type)) return '';

            $data_progress = $this->getMaxMark($this->id);

            $config_name = 'gradebook_config_'.$this->type;

            if($this->type != 'Overall'){
                $this->type = preg_replace('/[0-9]+/', '', $this->type);
                $config_name = 'gradebook_config_'.$this->type;
            }

            $config_def = $this->$config_name;

            //Push Array template
            if(count($configs) > 0 && isset($configs)){
                $new_configs = array( 'TT' => $config_def['TT']);
                foreach($configs as $_key => $_val)
                    $new_configs[$_key] = $_val;
                $configs = $new_configs;
            }else $configs = $config_def;

            //$group          = array();
            $type_html      = "";
            $group_html     = "";
            $title_html     = "";
            $alias_html     = "";
            $visible_html   = "";
            $max_mark_html  = "";
            $formula_html   = "";
            $add_html       = "";
            $comment_list   = $GLOBALS['app_list_strings']['option_comment_list'];
            $band_list      = $GLOBALS['app_list_strings']['option_band_list'];

            foreach($configs as $key => $config) {

                $prex       = $config['name']."_";
                if($config['visible']) $class = ''; else $class = 'readonly';

                $_label = $config['label'];
                $_group = $config['group'];

                $_class = '';
                if($key == 'TT') $_class = 'template'; //Set Template

                $type_html .= "<td class='center $_class'>
                <select style='width: 100px;' class='type ".(($config['visible']) ? "": "readonly")."' name='type[]' alias='{$key}' >";

                $type_html .= get_select_options_with_id(['score'=>'score','formula'=>'formula','total'=>'total','band'=>'band','comment'=>'comment'],$config['type']);

                $type_html .= "</select></td>";

                $group_html .= "<td class='center $_class gradebook-column'><textarea class='$class label group' name='group[]' alias='{$key}' ".(($config['visible']) ? "" : "readonly").">$_group</textarea> <input type='hidden' class='alias' name='alias[]' value='$key'></td>";
                $title_html .= "<td class='center $_class'><textarea class='$class label title' name='label[]' alias='{$key}' ".(($config['visible']) ? "" : "readonly").">$_label</textarea></td>";
                $alias_html .= "<td class = 'center $_class'><b alias='{$key}'>(".$config['alias'].")</b></td>";
                $visible_html .= "<td class = 'center $_class'><input type='checkbox' name='visible[]' alias='{$key}' class='visible' config_type='{$config['type']}' value='1' ".( $config['visible'] ?"checked":"").">  <input type='hidden' name='visibled[]' alias='{$key}' class='visibled' value='".( $config['visible'] ? "1":"0")."'>  </td>";

                $max_mark_html .= "<td class = 'center $_class'>";

                $max_mark_html .= "<select class='comment ".(($config['visible']) ? "": "readonly")."' style='width: 100px; ".(($config['type'] == 'comment') ? "": "display:none;")."' name='comment_list[]' alias='{$key}'>";
                $max_mark_html .= get_select_options_with_id($comment_list,$config['options']);
                $max_mark_html .= "</select>";

                $max_mark_html .= "<select class='band ".(($config['visible']) ? "": "readonly")."' style='width: 100px; ".(($config['type'] == 'band') ? "": "display:none;")."' name='band_list[]' alias='{$key}'>";
                $max_mark_html .= get_select_options_with_id($band_list,$config['options']);
                $max_mark_html .= "</select>";

                $max_mark_html .= "<input type = 'text' name = 'max_mark[]' alias='{$key}' ".($config['visible'] ? "": "readonly")." value = '".$config['max_mark']."' class = 'max_mark input_mark $class' ".(($config['type'] == 'score' || $config['type'] == 'formula' || $config['type'] == 'total') ? "": "style='display:none;'").">";
                $max_mark_html .= "</td>";

                $formula_html .= "<td class='center $_class' nowrap><span style='font-weight: bold;'>= </span><input type = 'text' style='text-transform: uppercase; ".( ($config['type'] == 'formula' ||  $config['type'] == 'total' ||  $config['type'] == 'band') ? "" : "display:none;" )."' alias='{$key}' value = '{$config['formula']}' class = 'input_formula formula' name = 'formula[]' ></td>
                <div id='formulatip' style='border:red solid 2px;background-color:#FFC;padding:10px;font-size:11px;position:absolute;z-index:10;float:right;display:none;width:250px;'></div>";

                if($key == 'Z')
                    $add_html .= "<td class='center $_class'><img class='add_col' src='custom/include/images/add.png' border='0' alt='Add' height='16' width='16'></td>";
                else{
                    if(empty($config['custom_btn_function']))
                        $add_html .= "<td class='center $_class'><img class='remove_col' alias='{$key}' src='custom/include/images/remove.png' border='0' alt='Remove' height='16' width='16'></td>";
                    else
                        $add_html .= "<td class='center'></td>";
                }

            }

            $table = "
            <input type = 'hidden' id = 'config_content_js' value = '{}'>
            
            <div style='margin-bottom: 5px;'>
                <span style='margin: 5px; color:#FF0000;font-weight:bold;cursor:pointer;'>[?]</span>
                <a class='show_tip tip_' >How to use formula in types: total, formula, band. >></a>
                <div class = 'formula_tip' style='display: none;width: 500px;'>
                    <p><div><b>Use characters in formula</b></div>
                    <div> P1 -> P12: Progress 1 -> Progress 12</div>
                    <p><div><u>Example</u>:</p>
                    <table><tr><td> Progress 1 + Progress 2: </td><td>&nbsp;&nbsp;<strong>P1+P2</strong></td></tr>
                        <tr><td> Progress 1 + column A: </td><td>&nbsp;&nbsp;<strong>P1+A</strong></div></td></tr>
                        <tr><td> Get column A in Progress 1: </td><td>&nbsp;&nbsp;<strong>P1[A]</strong></div></td></tr>
                        <tr><td> Count the number of Progresses from P1 to P6: </td><td>&nbsp;&nbsp;<strong>COUNT(P1:P6)</strong></div></td></tr>
                        <tr><td> Add the scores of Progresses from P1 to P6: </td><td>&nbsp;&nbsp;<strong>SUM(P1:P6)</strong></div></td></tr>
                        <tr><td> Add the scores of columns from A to C: </td><td>&nbsp;&nbsp;<strong>SUM(A:C)</strong></div></td></tr>
                        <tr><td> Average the scores of Progresses from P1 to P6: </td><td>&nbsp;&nbsp;<strong>AVERAGE(P1:P6)</strong></div></td></tr>
                        <tr><td> Average the scores of columns from A to C: </td><td>&nbsp;&nbsp;<strong>AVERAGE(A:C)</strong></div></td></tr>
                    </table>
                    <a class='hide_tip tip_'  style='margin-bottom: 10px;'>Hide <<</a>
                </div>
            </div>
            <table id = 'config_content' class='table-border' width = '50%'>
            <thead>
            <tr class='r_alias'>
            <td><b>Alias</b></td>
            $alias_html
            </tr>
            <tr class='r_group'>
            <td><b>Group</b></td>
            $group_html
            </tr>
            <tr class='r_title'>
            <td><b>Title</b></td>
            $title_html
            </tr>
            <tr class='r_type'>
            <td><b>Type</b></td>
            $type_html
            </tr>
            </thead>
            <tbody>
            <tr class='r_visible'>
            <td><b>Visible</b></td>
            $visible_html
            </tr>
            <tr class='r_max_mark'>
            <td><b>Max mark</b></td>
            $max_mark_html
            </tr>
            <tr class='r_formula'>
            <td><b>Formula</b></td>
            $formula_html
            </tr>
            <tr class='r_add'>
            <td></td>
            $add_html
            </tr>
            </tbody>
            </table>
            <div style='display:none;' id='data_progress'>$data_progress</div>";

            return $table;
        }
    }
?>
