<?php
    if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point'); 

    global $mod_strings, $app_strings, $dotb_config;

    $module_menu = Array();
    if(ACLController::checkAccess('J_GradebookConfig','edit',true))
        $module_menu[]=    Array("index.php?module=J_GradebookConfig&action=EditView&return_module=J_GradebookConfig&return_action=DetailView", $mod_strings['LNK_NEW_RECORD'], "CreateJ_GradebookConfig","J_GradebookConfig");
    
    if(ACLController::checkAccess('J_GradebookConfig', 'list', true))
        $module_menu[]=Array("index.php?module=J_GradebookConfig&action=index", $mod_strings['LNK_LIST'],"J_GradebookConfig", 'J_GradebookConfig');
        
   /* if(ACLController::checkAccess('J_GradebookConfig', 'edit', true))
        $module_menu[]=Array("index.php?module=J_GradebookConfig&action=Config", $mod_strings['LNK_CONFIG_GRADEBOOK'],"CreateJ_GradebookConfig", 'J_GradebookConfig');
   */