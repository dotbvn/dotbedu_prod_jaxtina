<?php



$dictionary['J_GradebookConfig'] = array(
    'table' => 'j_gradebookconfig',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'level' =>
        array (
            'required' => true,
            'name' => 'level',
            'vname' => 'LBL_LEVEL',
            'type' => 'varchar',
            'massupdate' => 0,
            'default' => '',
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => 255,
            'size' => '20',
            'studio' => 'visible',
            'dependency' => false,
        ),

        'type' => array(
            'name' => 'type',
            'vname' => 'LBL_TYPE',
            'type' => 'enum',
            'required' => true,
            'len' => '100',
            'default' => '',
            'no_default' => false,
            'options' => 'gradebook_type_options',
        ),

        'content' => array(
            'name' => 'content',
            'vname' => 'LBL_CONTENT',
            'type' => 'text',
        ),
        'gradebook_setting_name' => array(
            'name' => 'gradebook_setting_name',
            'vname' => 'LBL_GRADEBOOK_SETTING_NAME',
            'type' => 'varchar',
            'len' => '100',
        ),

        //Add Relationship GB Setting - GB Config
        'gbsettinggroup_id' => array(
            'name' => 'gbsettinggroup_id',
            'vname' => 'LBL_GBSETTINGGROUP',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'comment' => ''
        ),

        'gbsettinggroup_name' => array(
            'name' => 'gbsettinggroup_name',
            'rname' => 'name',
            'id_name' => 'gbsettinggroup_id',
            'vname' => 'LBL_GBSETTINGGROUP',
            'type' => 'relate',
            'link' => 'gbsettinggroup_link',
            'table' => 'j_gradebooksettinggroup',
            'isnull' => 'true',
            'module' => 'J_GradebookSettingGroup',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'source' => 'non-db',
            'required' => true
        ),

        'gbsettinggroup_link' => array(
            'name' => 'gbsettinggroup_link',
            'type' => 'link',
            'relationship' => 'gbsettinggroup_gbconfig',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_GBSETTINGGROUP',
        ),

        //Bổ sung quan hệ  Kind of Course - Class
        'koc_name' =>
        array(
            'required'  => true,
            'source'    => 'non-db',
            'name'      => 'koc_name',
            'vname'     => 'LBL_KOC_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'koc_id',
            'join_name' => 'j_kindofcourse',
            'link'      => 'kindofcourse_config',
            'table'     => 'j_kindofcourse',
            'isnull'    => 'true',
            'module'    => 'J_Kindofcourse',
            'additionalFields' => array('id' => 'koc_id'),
        ),
        'koc_id' =>
        array(
            'name'              => 'koc_id',
            'rname'             => 'id',
            'vname'             => 'LBL_KOC_NAME',
            'type'              => 'id',
            'table'             => 'j_kindofcourse',
            'isnull'            => 'true',
            'module'            => 'J_Kindofcourse',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),
        'kindofcourse_config' =>
        array(
            'name'          => 'kindofcourse_config',
            'type'          => 'link',
            'relationship'  => 'kindofcourse_gradeconfig',
            'module'        => 'J_Kindofcourse',
            'bean_name'     => 'J_Kindofcourse',
            'source'        => 'non-db',
            'vname'         => 'LBL_KOC_NAME',
        ),
        //Relationship Gradebook
        'ju_gradebook'=>array(
            'name' => 'ju_gradebook',
            'type' => 'link',
            'relationship' => 'gradebookconfig_gradebooks',
            'module' => 'J_Gradebook',
            'bean_name' => 'J_Gradebook',
            'source' => 'non-db',
            'vname' => 'LBL_GRADEBOOK_LINK',
        ),
    ),
    'relationships' => array (
        //Relationship Gradebook
        'gradebookconfig_gradebooks' => array(
            'lhs_module'        => 'J_GradebookConfig',
            'lhs_table'            => 'j_gradebookconfig',
            'lhs_key'            => 'id',
            'rhs_module'        => 'J_Gradebook',
            'rhs_table'            => 'j_gradebook',
            'rhs_key'            => 'gradebook_config_id',
            'relationship_type'    => 'one-to-many',
        ),
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_GradebookConfig','J_GradebookConfig',array('basic', 'assignable'));
