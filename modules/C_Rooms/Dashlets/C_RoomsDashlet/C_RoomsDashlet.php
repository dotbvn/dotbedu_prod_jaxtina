<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_Rooms/C_Rooms.php');

class C_RoomsDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_Rooms/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_Rooms');

        $this->searchFields = $dashletData['C_RoomsDashlet']['searchFields'];
        $this->columns = $dashletData['C_RoomsDashlet']['columns'];

        $this->seedBean = new C_Rooms();        
    }
}