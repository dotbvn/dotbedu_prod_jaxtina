<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_RoomsDashlet'] = array('module'		=> 'C_Rooms',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_Rooms'), 
                                          'description' => 'A customizable view into C_Rooms',
                                          'icon'        => 'icon_C_Rooms_32.gif',
                                          'category'    => 'Module Views');