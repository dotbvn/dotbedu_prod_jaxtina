<?php



$moduleName = 'J_Loyalty';
$viewdefs[$moduleName]['base']['menu']['header'] = array(
    array(
        'route' => "#$moduleName/create",
        'label' => 'LNK_NEW_RECORD',
        'acl_action' => 'create',
        'acl_module' => $moduleName,
        'icon' => 'fa-plus',
    ),
    array(
        'route' => "#$moduleName",
        'label' => 'LNK_LIST',
        'acl_action' => 'list',
        'acl_module' => $moduleName,
        'icon' => 'fa-bars',
    ),
        array(
        'route'=>'#bwc/index.php?module=Import&action=Step1&import_module='.$moduleName.'&return_module='.$moduleName.'&return_action=index',
        'label' =>'LNK_IMPORT_J_LOYALTY',
        'acl_action'=>'import',
        'acl_module'=>$module_name,
        'icon' => 'fa-cloud-upload',
    ),
);
