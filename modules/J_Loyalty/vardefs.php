<?php



$dictionary['J_Loyalty'] = array(
    'table' => 'j_loyalty',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'type'=> array (
            'required' => true,
            'name' => 'type',
            'vname' => 'LBL_TYPE',
            'type' => 'enum',
            'massupdate' => 0,
            'default' => '',
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'importable' => 'required',
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => 100,
            'size' => '20',
            'options' => 'loyalty_type_list',
            'studio' => 'visible',
            'dependency' => false,
        ),
        'point' =>
        array (
            'required'  => true,
            'name'      => 'point',
            'vname'     => 'LBL_POINT',
            'type'      => 'int',
            'no_default'=> false,
            'len'       => '100',
            'min'       => '1',
            'max'       => '99999999',
            'importable' => 'required',
        ),
        'point_label' =>
        array (
            'name'      => 'point_label',
            'vname'     => 'LBL_POINT',
            'type'      => 'varchar',
            'source' => 'non-db',
            'importable' => 'required',
        ),
        'discount_amount' =>
        array (
            'required' => false,
            'name' => 'discount_amount',
            'vname' => 'LBL_DISCOUNT_AMOUNT',
            'type' => 'currency',
            'len' => 13,
            'size' => '20',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
            'precision' => 2,
            'default' => '',
        ),
        'rate_in_out' =>
        array (
            'required' => false,
            'name' => 'rate_in_out',
            'vname' => 'LBL_RATE_IN_OUT',
            'type' => 'currency',
            'len' => 13,
            'size' => '20',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
            'precision' => 2,
            'default' => '',
        ),
        'exp_date' =>
        array (
            'required' => true,
            'name' => 'exp_date',
            'vname' => 'LBL_EXP_DATE',
            'type' => 'date',
            'massupdate' => 0,
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
            'display_default' => '+12 month',
        ),
        'input_date' =>
        array (
            'required' => true,
            'name' => 'input_date',
            'vname' => 'LBL_INPUT_DATE',
            'type' => 'date',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'required',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
            'importable' => 'required',
            'display_default' => 'now',
        ),

        //Add Relationship Payment - Loyalty
        'payment_id' => array(
            'name' => 'payment_id',
            'vname' => 'LBL_PAYMENT_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'importable' => false,
            'comment' => ''
        ),
        'payment_name' => array(
            'name' => 'payment_name',
            'rname' => 'name',
            'id_name' => 'payment_id',
            'vname' => 'LBL_PAYMENT_NAME',
            'type' => 'relate',
            'link' => 'payment_link',
            'table' => 'j_payment',
            'isnull' => 'true',
            'module' => 'J_Payment',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'importable' => false,
            'source' => 'non-db',
        ),
        'payment_link' => array(
            'name' => 'payment_link',
            'type' => 'link',
            'relationship' => 'payment_loyaltys',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_PAYMENT_NAME',
        ),
        //END: Add Relationship Payment - Loyalty

        //Add Relationship Student - Loyalty
        'student_id' => array(
            'name' => 'student_id',
            'vname' => 'LBL_STUDENT_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'importable' => false,
            'comment' => ''
        ),
        'student_name' => array(
            'required' => true,
            'name' => 'student_name',
            'rname' => 'name',
            'id_name' => 'student_id',
            'vname' => 'LBL_STUDENT_NAME',
            'type' => 'relate',
            'link' => 'student_link',
            'table' => 'contacts',
            'isnull' => 'true',
            'module' => 'Contacts',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'importable' => false,
            'source' => 'non-db',
            'importable' => false,
        ),
        'student_link' => array(
            'name' => 'student_link',
            'type' => 'link',
            'relationship' => 'student_loyaltys',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_STUDENT_NAME',
        ),
        //END: Add Relationship Student - Loyalty

        //Add Relationship KPI Settings - Loyalty
        'target_id' => array(
            'name' => 'target_id',
            'vname' => 'LBL_TARGET_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'importable' => false,
            'comment' => ''
        ),
        'target_name' => array(
            'required' => true,
            'name' => 'target_name',
            'rname' => 'name',
            'id_name' => 'target_id',
            'vname' => 'LBL_TARGET_NAME',
            'type' => 'relate',
            'link' => 'target_link',
            'table' => 'j_targetconfig',
            'isnull' => 'true',
            'module' => 'J_Targetconfig',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'importable' => false,
            'source' => 'non-db',
        ),
        'target_link' => array(
            'name' => 'target_link',
            'type' => 'link',
            'relationship' => 'target_loyaltys',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_TARGET_NAME',
        ),
        //END: Add Relationship Student - Loyalty

        //Add Relationship Receipt - Loyalty
        'paymentdetail_id' => array(
            'name' => 'paymentdetail_id',
            'vname' => 'LBL_PAYMENT_DETAIL_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'importable' => false,
            'comment' => ''
        ),
        'paymentdetail_name' => array(
            'name' => 'paymentdetail_name',
            'rname' => 'name',
            'id_name' => 'paymentdetail_id',
            'vname' => 'LBL_PAYMENT_DETAIL_NAME',
            'type' => 'relate',
            'link' => 'paymentdetail_link',
            'table' => 'j_paymentDetail',
            'isnull' => 'true',
            'module' => 'J_PaymentDetail',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'importable' => false,
            'source' => 'non-db',
        ),
        'paymentdetail_link' => array(
            'name' => 'paymentdetail_link',
            'type' => 'link',
            'relationship' => 'paymentdetail_loyaltys',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_PAYMENT_DETAIL_NAME',
        ),
        //END: Add Relationship Receipt - Loyalty

        //Import Field
        'full_student_name' =>
        array (
            'required' => false,
            'name' => 'full_student_name',
            'vname' => 'Student Name (Import)',
            'type' => 'varchar',
            'source'=>'non-db',
            'importable' => 'required',
        ),
        'phone_mobile' =>
        array (
            'required' => false,
            'name' => 'phone_mobile',
            'vname' => 'Mobile (Import)',
            'type' => 'varchar',
            'len' => '225',
            'source'=>'non-db',
        ),
        'old_student_id' =>
        array (
            'required' => false,
            'name' => 'old_student_id',
            'vname' => 'Student Id (Import)',
            'type' => 'varchar',
            'calculated' => false,
            'source'=>'non-db',
            'len' => '150',
            'size' => '20',
            'importable' => 'required',
        ),
        //IMPORT
    ),
    'relationships' => array (

    ),
    'optimistic_locking' => true,
    'unified_search' => false,
    'full_text_search' => false,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_Loyalty','J_Loyalty', array('basic','team_security','assignable','taggable'));
