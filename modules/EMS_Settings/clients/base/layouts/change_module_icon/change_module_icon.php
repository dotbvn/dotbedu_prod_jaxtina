<?php

$viewdefs['EMS_Settings']['base']['layout']['change_module_icon'] = array(
    'components' => array(
        array(
            'layout' => array(
                'type' => 'base',
                'name' => 'main-pane',
                'css_class' => 'main-pane',
                'components' => array(
                    array(
                        'view' => 'change_module_icon',
                    ),
                ),
            ),
        ),
    ),
);
