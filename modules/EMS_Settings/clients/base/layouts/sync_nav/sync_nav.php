<?php
$viewdefs['EMS_Settings']['base']['layout']['sync_nav'] = array(
    'components' => array(
        array(
            'layout' => array(
                'type' => 'base',
                'name' => 'main-pane',
                'css_class'=>'main-pane',
                'components' => array(
                    array(
                        'view' => 'sync_nav',
                    ),
                ),
            ),
        ),
    ),
);
