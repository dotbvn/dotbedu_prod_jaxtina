<?php
$viewdefs['EMS_Settings']['base']['layout']['payment_setting'] = array(
    'components' => array(
        array(
            'layout' => array(
                'type' => 'base',
                'name' => 'main-pane',
                'css_class'=>'main-pane',
                'components' => array(
                    array(
                        'view' => 'payment_setting',
                    ),
                ),
            ),
        ),
    ),
);
