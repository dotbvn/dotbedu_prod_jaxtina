<?php
$viewdefs['EMS_Settings']['base']['layout']['zalo_config'] = array(
    'components' => array(
        array(
            'layout' => array(
                'type' => 'base',
                'name' => 'main-pane',
                'css_class' => 'main-pane',
                'components' => array(
                    array(
                        'view' => 'zalo_config',
                    ),
                ),
            ),
        ),
    ),
);
