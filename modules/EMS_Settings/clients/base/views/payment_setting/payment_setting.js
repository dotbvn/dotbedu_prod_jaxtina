({
    events: {
        'click .record-panel-arrow': 'showTag',
        'click .save_config': 'saveConfig',
    },

    initialize: function (options) {
        this._super('initialize', [options]);
        this.loadData(options);
    },

    loadData: function (options) {
        options = options || {};

        _.extend(options, {
            success: _.bind(function (res) {
                this.customData = res.data;
                this.render();
            }, this)
        });

        app.api.call("read", app.api.buildURL('adminconfig/get_payment_setting'), null, options);
    },

    showTag: function (e){
        var arrowButton = $(e.currentTarget);
        if(arrowButton.attr('arrow') == 'up'){
            arrowButton.attr('arrow', 'down');
            arrowButton.html('<i class="fa fa-chevron-down" rel="tooltip" data-container="body" data-title="Toggle Visibility"></i>');
            arrowButton.closest('.card-header-custom').next().hide();
        }else if(arrowButton.attr('arrow') == 'down'){
            arrowButton.attr('arrow', 'up');
            arrowButton.html('<i class="fa fa-chevron-up" rel="tooltip" data-container="body" data-title="Toggle Visibility"></i>');
            arrowButton.closest('.card-header-custom').next().show();
        }
    },

    showErrorAlert: function (error) {
        var name = 'invalid-data';
        app.alert.show(name, {
            level: 'error',
            messages: error.message
        });
    },

    showLoading: function () {
        var name = 'loading';
        app.alert.show(name, {
            level: 'process',
            title: 'saving'
        });
    },

    showSuccessAlert: function () {
        var name = 'saved';
        app.alert.show(name, {
            level: 'success',
            messages: "",
            autoClose: true
        });
    },

    saveConfig: function (e) {
        // this.getField('save_button').setDisabled(true);
        var saveButton = $(e.currentTarget);
        saveButton.attr('disabled', 'disabled');
        this.showLoading();
        var options = {
            success: _.bind(function (res) {
                if(res.success){
                    this.showSuccessAlert();
                }else{
                    this.showErrorAlert({message:'Error'});
                }
                // this.getField('save_button').setDisabled(false);
                saveButton.removeAttr('disabled');
                app.alert.dismiss('loading');
            }, this),
            error: _.bind(function (error) {
                if (error.status === 412 && !error.request.metadataRetry) {
                    this.handleMetadataSyncError(error);
                } else {
                    this.showErrorAlert(error);
                }
                // this.getField('save_button').setDisabled(false);
                saveButton.removeAttr('disabled');
                app.alert.dismiss('loading');
            }, this),
        };

        var data = {};
        saveButton.closest('.card-custom').find('.record-save').each(function (){
            var nameField = $(this).attr('data-fieldname');
            data[nameField] = $(this).val();
        });

        var path = saveButton.attr('button-type');

        app.api.call("update", app.api.buildURL('adminconfig/' + path), data, options);
    },

    handleMetadataSyncError: function (error) {
        //On a metadata sync error, retry the save after the app is synced
        var self = this;
        self.resavingAfterMetadataSync = true;

        app.once('app:sync:complete', function () {
            error.request.metadataRetry = true;
            self.model.once('sync', function () {
                self.resavingAfterMetadataSync = false;
                app.router.refresh();
            });
            error.request.execute(null, app.api.getMetadataHash());
        });
    },
})
