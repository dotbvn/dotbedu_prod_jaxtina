<?php
$viewdefs['EMS_Settings']['base']['view']['sync_nav'] = array(
    'buttons' => array(
        array(
            'name' => 'cancel_button',
            'type' => 'button',
            'label' => 'LBL_CANCEL_BUTTON_LABEL',
            'tooltip' => 'LBL_CANCEL_BUTTON_LABEL',
            'icon' => 'fa-times',

            'events' => ["click" => "cancel_config"]
        )
    )
);
