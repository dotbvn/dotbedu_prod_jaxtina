({
    messagelog: '0',
    events: {
        "click .save": "saveConfig"
    },
    initialize: function (options) {
        this._super('initialize', [options]);
        this.context.on("cancel_config", _.bind(this.cancelConfig, this));
    },

    showErrorAlert: function (error) {
        var name = 'invalid-data';
        app.alert.show(name, {
            level: 'error',
            messages: error.message
        });
    },

    showLoading: function () {
        var name = 'loading';
        app.alert.show(name, {
            level: 'process',
            title: 'saving'
        });
    },

    showSuccessAlert: function () {
        var name = 'saved';
        app.alert.show(name, {
            level: 'success',
            messages: "",
            autoClose: true
        });
    },

    saveConfig: function () {
        if ($('.start_date').val() == '' || $('.end_date').val() == '') {
            this.showErrorAlert({message: 'Please input start date and end date to sync NAV'});
        } else {
            this.showLoading();
            var options = {
                success: _.bind(function (res) {
                    console.log(res);
                    if (res.success) {
                        this.showSuccessAlert();
                    } else {
                        this.showErrorAlert({message: 'Error'});
                    }
                    var t = JSON.parse(res.message);
                    if (t.hasOwnProperty('data') && t.data.hasOwnProperty('salesHeaders')) {
                        this.messagelog = JSON.parse(res.message).data.salesHeaders.length;
                    }
                    this.render();
                    app.alert.dismiss('loading');
                }, this),
                error: _.bind(function (error) {
                    if (error.status === 412 && !error.request.metadataRetry) {
                        this.handleMetadataSyncError(error);
                    } else {
                        this.showErrorAlert(error);
                    }
                    app.alert.dismiss('loading');
                }, this),
            };

            var data = {
                start: $('.start_date').val(),
                end: $('.end_date').val(),
                require_invoice_no: $('.require_invoice_no').is(':checked') ? 1 : 0
            };
            app.api.call("update", app.api.buildURL('adminconfig/sync_nav'), data, options);
        }
    },

    cancelConfig: function () {
        app.router.navigate("#bwc/index.php?module=Administration", {trigger: true});
    },

    handleMetadataSyncError: function (error) {
        //On a metadata sync error, retry the save after the app is synced
        var self = this;
        self.resavingAfterMetadataSync = true;

        app.once('app:sync:complete', function () {
            error.request.metadataRetry = true;
            self.model.once('sync', function () {
                self.resavingAfterMetadataSync = false;
                app.router.refresh();
            });
            error.request.execute(null, app.api.getMetadataHash());
        });
    },
})
