({
    initialize: function (options) {
        this._super('initialize', [options]);
        this.loadData(options);

        this.context.on("save", _.bind(this.save, this));
        this.context.on("cancel", _.bind(this.cancel, this));
    },

    loadData: function (options) {
        options = options || {};
        _.extend(options, {
            success: _.bind(function (res) {
                this.data = res.data;
                this.render();
            }, this)
        });
        app.api.call("read", app.api.buildURL('zalo/config'), null, options);
    },

    showErrorAlert: function (error) {
        app.alert.show('error', {
            level: 'error',
            messages: error.message
        });
    },

    showLoading: function () {
        app.alert.show('loading', {
            level: 'process',
            title: 'saving'
        });
    },

    showSuccessAlert: function () {
        app.alert.show('success', {
            level: 'success',
            messages: "",
            autoClose: true
        });
    },

    save: function () {
        this.showLoading();
        var options = {
            success: _.bind(function (data) {
                this.showSuccessAlert();
                app.alert.dismiss('loading');
            }, this),
            error: _.bind(function (error) {
                if (error.status === 412 && !error.request.metadataRetry) {
                    this.handleMetadataSyncError(error);
                } else {
                    this.showErrorAlert(error);
                }
                app.alert.dismiss('loading');
            }, this)
        };
        var result = {
            enable: $('#zalo_enable').val(),
            link: $('#zalo_link').val(),
            key: $('#zalo_key').val(),
            supplier: $('#zalo_supplier').val(),
            username: $('#zalo_username').val()
        };

        app.api.call("update", app.api.buildURL('zalo/saveConfig'), result, options);
    },

    cancel: function () {
        app.router.navigate("#bwc/index.php?module=Administration", {trigger: true});
    }
})
