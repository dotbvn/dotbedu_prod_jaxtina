(function (app) {
    app.events.on('router:init', function (router) {
        var routes = [
            {
                name: 'EMS_Settings_Subscription',
                route: 'EMS_Settings/layout/:layout',
                callback: function (layout) {
                    if (layout === 'subscription') {
                        // Use BWC DetailView
                        var route = app.bwc.buildRoute('EMS_Settings', null, layout);
                        app.router.redirect(route);
                    } else {
                        // Use RecordView with detail mode
                        app.router.record('EMS_Settings', null, null, layout);
                    }
                }
            }
        ];
        app.router.addRoutes(routes);
    });
})(DOTB.App);
