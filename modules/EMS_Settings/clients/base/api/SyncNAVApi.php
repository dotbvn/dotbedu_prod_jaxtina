<?php
//require_once("custom/include/SqlServer.php");

class SyncNAVApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'admin_config_sync_nav' => array(
                'reqType' => 'PUT',
                'path' => array('adminconfig', 'sync_nav'),
                'pathVars' => array(''),
                'method' => 'syncNav'
            ),
            'admin_config_sync_nav_get' => array(
                'reqType' => 'GET',
                'path' => array('adminconfig', 'sync_nav'),
                'pathVars' => array(''),
                'method' => 'syncNavGet'
            ),
        );
    }

    function check($arr, $key, $value)
    {
        foreach ($arr as $a) {
            if ($a[$key] == $value) return true;
        }
        return false;
    }

    function getData($start, $end, $require_invoice_no)
    {
        //branch code, location code, ký số của các center Sylvan
        $center = array(
            'S10' => array(
                'branch_code' => 'HCM.AO.0013',
                'location_code' => '3T2',
                'serial' => 'AE/20E',
            ),
            'SPN' => array(
                'branch_code' => 'HCM.AO.0011',
                'location_code' => 'PDL',
                'serial' => 'AC/20E',
            ),
            'S05' => array(
                'branch_code' => 'HCM.AO.0012',
                'location_code' => 'TBT',
                'serial' => 'AD/20E',
            ),
            'STB' => array(
                'branch_code' => 'HCM.AO.0019',
                'location_code' => 'NTB',
                'serial' => 'AH/20E',
            ),
            'SBT' => array(
                'branch_code' => 'HCM.AO.0014',
                'location_code' => 'AEON',
                'serial' => 'AG/20E',
            ),
            'S06' => array(
                'branch_code' => 'HCM.AO.0022',
                'location_code' => 'HG',
                'serial' => 'AK/20E',
            ),
            'S09' => array(
                'branch_code' => 'HCM.AO.0021',
                'location_code' => 'DXH',
                'serial' => 'AL/20E',
            ),
            'S12' => array(
                'branch_code' => 'HCM.AO.0024',
                'location_code' => 'NAT',
                'serial' => 'AM/20E',
            ),
            'STĐ' => array(
                'branch_code' => 'HCM.AO.0027',
                'location_code' => 'TNV',
                'serial' => 'AN/20E',
            ),
            'SGV' => array(
                'branch_code' => 'HCM.AO.0028',
                'location_code' => 'PVC',
                'serial' => 'AP/20E',
            ),
            'SOL' => array(
                'branch_code' => 'HCM.AO.0016',
                'location_code' => 'SOL',
                'serial' => 'C22TAB',
            ),

        );

        $sql = "SELECT 
                        IFNULL(l4.contact_id, '') contact_id,
                        IFNULL(l4.full_student_name, '') full_name,
                        IFNULL(l4.primary_address_state, ' ') address,
                        IFNULL(l4.primary_address_city, '') city,
                        IFNULL(l4.phone_mobile, '') phone,
                        IFNULL(l7.email_address, '') email,
                        IFNULL(l6.kind_of_course, '') kind_of_course,
                        IFNULL(l5.code_prefix, '') center_code,
                        IFNULL(l5.short_name, '') short_name,
                        IFNULL(l1.name, '') receipt_code,
                        IFNULL(l6.extend_vat, 'RRDP') product_code,
                        IFNULL(l3.name, '') payment_id,
                        IFNULL(l1.payment_date, '') receipt_date,
                        IFNULL(l1.payment_method, '') method,
                        IFNULL(l2.name, '') invoice_no,
                        IFNULL(l1.id, '') receipt_id,
                        IFNULL(l1.description, '') description,
                        IFNULL(l3.amount_bef_discount, '0') amount_bef_discount,
                        IFNULL(l3.discount_amount, '0') discount_amount,
                        IFNULL(l3.final_sponsor, '0') final_sponsor,
                        IFNULL(l1.payment_amount, '0') receipt_amount,
                        IFNULL(l1.status, '') status,
                        IFNULL(l3.id, '') payment_primary_id,
                        IFNULL(l6.name, '') coursefee_name
                    FROM
                        j_paymentdetail l1
                            INNER JOIN
                        j_payment l3 ON l1.payment_id = l3.id AND l3.deleted = 0
                            INNER JOIN
                        contacts l4 ON l4.id = l3.parent_id AND l3.parent_type = 'Contacts'
                            AND l4.deleted = 0
                            INNER JOIN
                        teams l5 ON l3.team_id = l5.id AND l5.deleted = 0
                            LEFT JOIN
                        j_invoice l2 ON l1.invoice_id = l2.id AND l2.deleted = 0
                            LEFT JOIN
                        j_coursefee_j_payment_1_c l6_1 ON l3.id = l6_1.j_coursefee_j_payment_1j_payment_idb
                            AND l6_1.deleted = 0
                            LEFT JOIN
                        j_coursefee l6 ON l6.id = l6_1.j_coursefee_j_payment_1j_coursefee_ida
                            AND l6.deleted = 0
                            LEFT JOIN
                        email_addr_bean_rel l7_1 ON l7_1.bean_id = l4.id
                            AND l7_1.deleted = 0
                            AND l7_1.bean_module = 'Contacts'
                            LEFT JOIN
                        email_addresses l7 ON l7_1.email_address_id = l7.id
                            AND l7.deleted = 0
                    WHERE
                        l1.payment_date >= '{$start}'
                            AND l1.payment_date <= '{$end}' AND l1.deleted = 0";

        $receipts_data = $GLOBALS['db']->fetchArray($sql);
        if (count($receipts_data) == 0)
            return array(
                'success' => false,
                'error' => "No data to sync NAV",
            );
        $customer = array();
        $sales_header = array();
        $sales_line = array();
        $dimension_set_entry = array();

        foreach ($receipts_data as $receipt_data) {
            if (strlen($receipt_data['address']) > 30) $receipt_data['address'] = substr($receipt_data['address'], strlen($receipt_data['address']) - 30);
            if (strlen($receipt_data['city']) > 30) $receipt_data['city'] = substr($receipt_data['city'], strlen($receipt_data['city']) - 30);
            if ($require_invoice_no == 1 && empty($receipt_data['invoice_no'])) {
                $koc = str_replace('^', '', $receipt_data['kind_of_course']);
                if ($koc == 'Other') {
                    $customer_no = 'CFHV_' . $receipt_data['contact_id'];
                    $customer_posting_group = 'OTHER';
                    if (!$this->check($customer, 'No_', $customer_no)) {
                        $customer[] = array(
                            'No_' => $customer_no,
                            'Name' => $receipt_data['full_name'],
                            'Address' => $receipt_data['address'],
                            'City' => $receipt_data['city'],
                            'E_Mail' => $receipt_data['email'],
                            'Phone_No_' => $receipt_data['phone'],
                            'Customer_Posting_Group' => $customer_posting_group,
                            'Payment_Terms_Code' => 'C00',
                            'Bill_to_Customer_No_' => $customer_no,
                            'Gen__Bus__Posting_Group' => 'DOMESTIC',
                            'VAT_Bus__Posting_Group' => 'DOMESTIC',
                            'Credit_Limit__LCY_' => 0,
                            'Country_Region_Code' => 'VN',
                            'Blocked' => 0,
                            'Prices_Including_VAT' => false,
                            'Post_Code' => '70000',
                            'NAV_Conversion_Status' => 0,
                            'Contact' => '',
                            'Currency_Code' => '',
                            'Global_Dimension_1_Code' => '',
                            'Global_Dimension_2_Code' => '',
                            'NAV_Errors' => '',
                            'NAV_Errors_2' => '',
                            'VAT_Registration_No_' => ' ',
                            'Payment_Method_Code' => ''
                        );
                    }
                }
                continue;
            } elseif ($require_invoice_no == 0 && empty($receipt_data['invoice_no'])) {
                $receipt_data['invoice_no'] = ' ';
            }

            $koc = str_replace('^', '', $receipt_data['kind_of_course']);
            //nếu field apply with kind of cousre trong bảng course fee = other thì là lệ phí thi, ngược lại là học phí
            if ($koc == 'Other') {
                $customer_no = 'CFHV_' . $receipt_data['contact_id'];
                $customer_posting_group = 'OTHER';
                $sale_line_no = '1388';
            } else {
                $customer_no = 'HV_' . $receipt_data['contact_id'];
                $customer_posting_group = 'DOMESTIC';
                $sale_line_no = '3387-01';
            }
            // cho table Customer
            if (!$this->check($customer, 'No_', $customer_no)) {
//            if (!in_array($customer_no, array_keys($customer))) {
                $customer[] = array(
                    'No_' => $customer_no,
                    'Name' => $receipt_data['full_name'],
                    'Address' => $receipt_data['address'],
                    'City' => $receipt_data['city'],
                    'E_Mail' => $receipt_data['email'],
                    'Phone_No_' => $receipt_data['phone'],
                    'Customer_Posting_Group' => $customer_posting_group,
                    'Payment_Terms_Code' => 'C00',
                    'Bill_to_Customer_No_' => $customer_no,
                    'Gen__Bus__Posting_Group' => 'DOMESTIC',
                    'VAT_Bus__Posting_Group' => 'DOMESTIC',
                    'Credit_Limit__LCY_' => 0,
                    'Country_Region_Code' => 'VN',
                    'Blocked' => 0,
                    'Prices_Including_VAT' => false,
                    'Post_Code' => '70000',
                    'NAV_Conversion_Status' => 0,
                    'Contact' => '',
                    'Currency_Code' => '',
                    'Global_Dimension_1_Code' => '',
                    'Global_Dimension_2_Code' => '',
                    'NAV_Errors' => '',
                    'NAV_Errors_2' => '',
                    'VAT_Registration_No_' => ' ',
                    'Payment_Method_Code' => ''
                );
            }
            // cho table Sales Header
            $bill_no = $receipt_data['center_code'] . '-' . $receipt_data['receipt_code'] . '-' . $receipt_data['payment_id'];
            if (!$this->check($sales_header, 'Bill_No_', $bill_no)) {
//            if (!in_array($bill_no, array_keys($sales_header))) {

                // unpaid amount
                $sqlPayDtl = "SELECT DISTINCT
            IFNULL(pmd.payment_no, '') payment_no,
            IFNULL(pmd.status, '') status,
            IFNULL(pmd.payment_amount, '0') payment_amount
            FROM j_paymentdetail pmd
            INNER JOIN j_payment l1 ON pmd.payment_id = l1.id AND l1.deleted = 0
            WHERE l1.id = '{$receipt_data['payment_primary_id']}' AND pmd.deleted = 0 AND pmd.status <> 'Cancelled' AND pmd.status<>'Unpaid'
            ORDER BY pmd.payment_no";
                $resultPayDtl = $GLOBALS['db']->query($sqlPayDtl);
                $unpaidAmount = 0;
                while ($rowPayDtl = $GLOBALS['db']->fetchByAssoc($resultPayDtl)) {
                    if ($rowPayDtl['status'] == "Unpaid") $unpaidAmount += $rowPayDtl['payment_amount'];
                }

                $sales_header[] = array(
                    'Bill_No_' => $bill_no,
                    'Correction_ID' => 0,
                    'Sell_to_Customer_No_' => $customer_no,
                    'Bill_to_Customer_No_' => $customer_no,
                    'Posting_Date' => $receipt_data['receipt_date'],
                    'Payment_Terms_Code' => 'C00',
                    'Payment_Method' => ($receipt_data['method'] == 'Cash') ? $center[$receipt_data['short_name']]['location_code'] : '',
                    'Document_Date' => $receipt_data['receipt_date'],
                    'External_Document_No_' => empty($receipt_data['invoice_no']) ? ' ' : $receipt_data['invoice_no'],
                    'VAT_Bus__Posting_Group' => 'DOMESTIC',
                    'Origin_Bill_No_' => $receipt_data['receipt_code'],
                    'NAV_Conversion_Status' => 0,
                    'Header_Type' => 1,
                    'Explanation__VN_' => $receipt_data['invoice_no'] . ':' . $receipt_data['center_code'] . '-' . $receipt_data['receipt_code'] . ';' . $center[$receipt_data['short_name']]['serial'] . ';' . $receipt_data['description'],
                    'Explanation' => 'REF[#:' . $bill_no . '_' . $receipt_data['receipt_id']
                        . ';C:' . $center[$receipt_data['short_name']]['branch_code']
                        . ';T:' . format_number($receipt_data['amount_bef_discount'])
                        . ';D:' . format_number($receipt_data['discount_amount'] + $receipt_data['final_sponsor'])
                        . ';P:' . format_number($receipt_data['receipt_amount'])
                        . ';U:' . format_number($unpaidAmount)
                        . ';VAT#:' . $receipt_data['invoice_no'] . ']',
                    'Currency_Code' => '',
                    'NAV_Errors' => '',
                    'NAV_Errors_2' => ''
                );
            }
            // cho table Sales Line
            if (!$this->check($sales_line, 'Bill_No_', $bill_no)) {
//            if (!in_array($bill_no, array_keys($sales_line))) {
                $sales_line[] = array(
                    'Bill_No_' => $bill_no,
                    'Correction_ID' => 0,
                    'Line_No_' => 1,
                    'Type' => 1,
                    'No_' => $sale_line_no,
                    'Location_Code' => $center[$receipt_data['short_name']]['location_code'],
                    'Description' => $receipt_data['coursefee_name'] ? $receipt_data['coursefee_name'] : 'DEPOSIT',
                    'Quantity' => 1,
                    'Unit_Price' => $receipt_data['receipt_amount'],
                    'VAT_Prod__Posting_Group' => 'NOVAT',
                    'Line_Discount_Amount' => 0,
                    'NAV_Errors' => '',
                    'NAV_Errors_2' => ''
                );
                $dimension_set_entry[] = array(
                    'Bill_No_' => $bill_no,
                    'Correction ID' => 0,
                    'Line_No_' => 0,
                    'Dimension_Code' => 'BRANCH',
                    'Dimension_Value_Code' => $center[$receipt_data['short_name']]['branch_code'],
                    'NAV_Errors' => '',
                    'NAV_Errors_2' => ''
                );
                $dimension_set_entry[] = array(
                    'Bill_No_' => $bill_no,
                    'Correction ID' => 0,
                    'Line_No_' => 0,
                    'Dimension_Code' => 'REVENUE',
                    'Dimension_Value_Code' => $receipt_data['product_code'],
                    'NAV_Errors' => '',
                    'NAV_Errors_2' => ''
                );
                $dimension_set_entry[] = array(
                    'Bill_No_' => $bill_no,
                    'Correction ID' => 0,
                    'Line_No_' => 1,
                    'Dimension_Code' => 'REVENUE',
                    'Dimension_Value_Code' => $receipt_data['product_code'],
                    'NAV_Errors' => '',
                    'NAV_Errors_2' => ''
                );
            }
        }

        return array(
            'ErpCustomers' => $customer,
            'ErpSalesHeaders' => $sales_header,
            'ErpSalesLines' => $sales_line,
            'ErpDimensionSetEntries' => $dimension_set_entry
        );
    }

    function syncNav(ServiceBase $api, array $args)
    {
        $finalData = $this->getData($args['start'], $args['end'], $args['require_invoice_no']);
        $GLOBALS['log']->fatal($finalData);

        $ch = curl_init();
        $headers = [
            'Content-Type: application/json'
        ];
        curl_setopt($ch, CURLOPT_URL, "http://113.161.81.45:26201/api/systemService/ems/interfaceNAV");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($finalData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $GLOBALS['log']->fatal($result);
        return array(
            'success' => 1,
            'data' => $finalData,
            'message' => $result
        );
    }

    function syncNavGet(ServiceBase $api, array $args)
    {
        $finalData = $this->getData($args['start'], $args['end'], $args['require_invoice_no']);
        return array(
            'success' => 1,
            'data' => $finalData
        );
    }
}
