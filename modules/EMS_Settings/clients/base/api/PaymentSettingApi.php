<?php

class PaymentSettingApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'admin_config_set_payment_sale_rule' => array(
                'reqType' => 'PUT',
                'path' => array('adminconfig', 'set_payment_sale_rule'),
                'pathVars' => array(''),
                'method' => 'setPaymentSaleRule'
            ),
            'admin_config_get_payment_setting' => array(
                'reqType' => 'GET',
                'path' => array('adminconfig', 'get_payment_setting'),
                'pathVars' => array(''),
                'method' => 'getPaymentSetting'
            ),
            'admin_config_set_einvoice_setting' => array(
                'reqType' => 'PUT',
                'path' => array('adminconfig', 'set_einvoice_setting'),
                'pathVars' => array(''),
                'method' => 'setEInvoiceSetting'
            ),
            'admin_config_set_sponsor_setting' => array(
                'reqType' => 'PUT',
                'path' => array('adminconfig', 'set_sponsor_setting'),
                'pathVars' => array(''),
                'method' => 'setSponsorSetting'
            ),
        );
    }


    function getPaymentSetting(ServiceBase $api, array $args)
    {
        $admin = new Administration();
        $admin->retrieveSettings();
        $data=null;
        if (!empty($admin->settings['payment_setting_payment_sale_rule'])) $data['payment_sale_rule'] = $admin->settings['payment_setting_payment_sale_rule'];
        if (!empty($admin->settings['einvoice_setting_export_einvoice_type'])) $data['export_einvoice_type'] = $admin->settings['einvoice_setting_export_einvoice_type'];
        if (!empty($admin->settings['sponsor_setting_sponsor_type'])) {
            $data['sponsor_default'] = in_array('default', $admin->settings['sponsor_setting_sponsor_type']) ? 1 : 0;
            $data['sponsor_TAPTAP'] = in_array('TAPTAP Sponsor', $admin->settings['sponsor_setting_sponsor_type']) ? 1 : 0;
        }
        if (!empty($admin->settings['taptap_config_keycloak_host'])) $data['keycloak_host'] = $admin->settings['taptap_config_keycloak_host'];
        if (!empty($admin->settings['taptap_config_gateway_host'])) $data['gateway_host'] = $admin->settings['taptap_config_gateway_host'];
        if (!empty($admin->settings['taptap_config_grant_type'])) $data['grant_type'] = $admin->settings['taptap_config_grant_type'];
        if (!empty($admin->settings['taptap_config_client_id'])) $data['client_id'] = $admin->settings['taptap_config_client_id'];
        if (!empty($admin->settings['taptap_config_client_secret'])) $data['client_secret'] = $admin->settings['taptap_config_client_secret'];
        return array(
           'data'=>$data
        );
    }

    function setPaymentSaleRule(ServiceBase $api, array $args)
    {
        $admin = new Administration();
        $admin->retrieveSettings();
        $admin->saveSetting('payment_setting', 'payment_sale_rule', $args['payment_sale_rule']);
        return array("success" => 1);
    }

    function setEInvoiceSetting(ServiceBase $api, array $args){
        $admin = new Administration();
        $admin->retrieveSettings();
        $admin->saveSetting('einvoice_setting', 'export_einvoice_type', $args['export_einvoice_type']);
        return array("success" => 1);
    }

    function setSponsorSetting(ServiceBase $api, array $args){
        $admin = new Administration();
        $admin->retrieveSettings();
        $admin->saveSetting('sponsor_setting', 'sponsor_type', $args['sponsor_type']);
        $admin->saveSetting('taptap_config', 'keycloak_host', $args['keycloak_host']);
        $admin->saveSetting('taptap_config', 'gateway_host', $args['gateway_host']);
        $admin->saveSetting('taptap_config', 'grant_type', $args['grant_type']);
        $admin->saveSetting('taptap_config', 'client_id', $args['client_id']);
        $admin->saveSetting('taptap_config', 'client_secret', $args['client_secret']);
        return array("success" => 1);
    }
}
