<?php



$dictionary['EMS_Settings'] = array(
    'table' => 'ems_settings',
    'audited' => true,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (


),
    'relationships' => array (
),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('EMS_Settings','EMS_Settings', array('basic','assignable','taggable'));
