<?php

$moduleName = 'J_Class';
$viewdefs[$moduleName]['base']['menu']['header'] = array(
    array(
        'route' => "#$moduleName/create",
        'label' => 'LNK_NEW_RECORD',
        'acl_action' => 'create',
        'acl_module' => $moduleName,
        'icon' => 'fa-plus',
    ),
    array(
        'route' => "#$moduleName",
        'label' => 'LNK_LIST',
        'acl_action' => 'list',
        'acl_module' => $moduleName,
        'icon' => 'fa-bars',
    ),
    array(
        'route'=>'#bwc/index.php?module=Calendar&action=index&view=week',
        'label' =>'LNK_VIEW_CALENDAR',
        'acl_action'=>'list',
        'acl_module'=>'Calendar',
        'icon' => 'fa-calendar-alt',
    ),
    array(
        'route' => "#$moduleName/attendance_screen",
        'label' => 'LBL_ATTENDANCE_CHECKING',
        'acl_action' => 'import',
        'acl_module' => $moduleName,
        'icon' => 'fa-cloud-upload',
    ),
     array(
        'route' => "#bwc/index.php?module=J_Class&action=attendance",
        'label' => 'LBL_TAKE_ATTENDANCE',
        'acl_action' => 'view',
        'acl_module' => $moduleName,
        'icon' => 'fa-clipboard-user',
    ),
);
