
({
    extendsFrom: "MassupdateView",
    
    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super("initialize", [options]);
    },


    save: function(forCalcFields) {
        forCalcFields = !!forCalcFields;
        var massUpdate = this.getMassUpdateModel(this.module),
            self = this;

        massUpdate.setChunkSize(this._settings.mass_update_chunk_size);

        this.once('massupdate:validation:complete', function(validate) {
            var errors = validate.errors,
                emptyValues = validate.emptyValues,
                confirmMessage = app.lang.get('LBL_MASS_UPDATE_EMPTY_VALUES'),
                attributes = validate.attributes || this.getAttributes();
            if(this.collection.filterDef !== undefined) attributes.filterDef = this.collection.filterDef;
            this.$(".fieldPlaceHolder .error").removeClass("error");
            this.$(".fieldPlaceHolder .help-block").hide();

            if (_.isEmpty(errors)) {
                confirmMessage += '<br>[' + emptyValues.join(',') + ']<br>' + app.lang.get('LBL_MASS_UPDATE_EMPTY_CONFIRM') + '<br>';
                if (massUpdate) {
                    var fetchMassupdate = _.bind(function() {
                        var successMessages = this.buildSaveSuccessMessages(massUpdate);
                        massUpdate.fetch({
                            //Show alerts for this request
                            showAlerts: true,
                            attributes: attributes,
                            error: function() {
                                app.alert.show('error_while_mass_update', {
                                    level: 'error',
                                    title: app.lang.get('ERR_INTERNAL_ERR_MSG'),
                                    messages: ['ERR_HTTP_500_TEXT_LINE1', 'ERR_HTTP_500_TEXT_LINE2']
                                });
                            },
                            success: function(data, response, options) {
                                self.hide();
                                if (options.status === 'done') {
                                    //TODO: Since self.layout.trigger("list:search:fire") is deprecated by filterAPI,
                                    //TODO: Need trigger for fetching new record list
                                    self.collection.fetch({
                                        //Don't show alerts for this request
                                        showAlerts: false,
                                        remove: true,
                                        // Boolean coercion.
                                        relate: !!self.layout.collection.link
                                    });
                                } else if (options.status === 'queued') {
                                    app.alert.show('jobqueue_notice', {level: 'success', messages: successMessages[options.status], autoClose: true});
                                }
                            }
                        });
                    }, this);
                    if (emptyValues.length === 0) {
                        fetchMassupdate.call(this);
                    } else {
                        app.alert.show('empty_confirmation', {
                            level: 'confirmation',
                            messages: confirmMessage,
                            onConfirm: fetchMassupdate
                        });
                    }
                }
            } else {
                this.handleValidationError(errors);
            }
        }, this);

        if (forCalcFields) {
            this.trigger('massupdate:validation:complete', {
                errors: [],
                emptyValues: [],
                attributes: {}
            });
        } else {
            this.checkValidationError();
        }
    },
})
