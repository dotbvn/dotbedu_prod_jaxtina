<?php


$module_name = 'J_Class';
$viewdefs[$module_name]['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
       'class_code' => array(),
       'name' => array(),
       'main_teachers' => array(),
       'period' => array(),
       'status' => array(),
       'number_of_student' => array(),
       'start_date' => array(),
       'end_date' => array(),
       'koc_name' => array(),
       'team_name' => array(),
       'date_entered' => array(),
       'date_modified' => array(),
        '$owner' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'created_by_name' => array(),
        'modified_by_name' => array(),
    ),
);
