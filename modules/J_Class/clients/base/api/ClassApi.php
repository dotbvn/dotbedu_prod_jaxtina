<?php


class ClassApi extends DotbApi {

    public function registerApiRest() {
        return array(
            'addDemo' => array(
                'reqType' => 'PUT',
                'path' => array('J_Class', 'add-demo-popup'),
                'method' => 'getAddDemoPopup',
            ),

        );
    }


    public function getAddDemoPopup(ServiceBase $api, array $args) {
        global $timedate;
        require_once("custom/include/_helper/junior_revenue_utils.php");

        $ss = new Dotb_Smarty();
        $ss->assign("dm_student_id", $args['student_id']);
        $ss->assign("dm_student_name", $args['student_name']);
        $ss->assign("dm_student_type", $args['student_type']);
        $args['model']['start_date'] = $timedate->to_display_date($args['model']['start_date'],false);
        $args['model']['end_date'] = $timedate->to_display_date($args['model']['end_date'],false);
        $ss->assign("model", $args['model']);

        $ss->assign('MOD', return_module_language($GLOBALS['current_language'], 'J_Class'));

        // Display schedule
        $schedule = $args['model']['short_course_name'];
        $ss->assign("SCHEDULE", $schedule);

        // Get Session list
        $arr = array();
        $sss = get_list_lesson_by_class($args['model']['id']);
        $count_ss   = 0;
        $today      = date('Y-m-d');
        $defautDate = '';

        foreach($sss as $key => $row){
            $date_start_int = date('Y-m-d',strtotime("+7 hours ".$row['date_start']));

            if($date_start_int != $last_date_start_int)
                $delivery_hour = $row['delivery_hour'];
            else $delivery_hour += $row['delivery_hour'];

            $arr[$date_start_int]  = $delivery_hour;

            $last_date_start_int = $date_start_int;
            $count_ss++;

            if($date_start_int >= $today && empty($defautDate))
                $defautDate = $date_start_int;
        }
        $json_ss = json_encode($arr);

        $ss->assign("json_ss", $json_ss);
        $ss->assign('next_session_date',$timedate->to_display_date($row['class_start_date'],true) );
        $ss->assign('class_start',$timedate->to_display_date($row['class_start_date'],true) );
        $ss->assign('class_end',$timedate->to_display_date($row['class_end_date'],true) );
        return  $ss->fetch('custom/modules/J_Class/tpls/demo_template_for_lumia.tpl');

    }
}
