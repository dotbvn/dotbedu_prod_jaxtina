<?php


    $viewdefs['J_Class']['portal']['view']['panel-top'] = array(
        'type' => 'panel-top',
        'template' => 'panel-top',
        'buttons' => array(
            array(
               'type' => 'actiondropdown',
            'name' => 'panel_dropdown', 'notCustomButton' => true,
                'css_class' => 'pull-right',
                'buttons' => array(
                    array(
                        'type' => 'link-action',
                        'icon' => 'fa-link',
                        'name' => 'select_button',
                        'label' => ' ',
                        'tooltip' => 'LBL_ASSOC_RELATED_RECORD',
                    ),
                    array(
                    ),
                ),
            ),
        ),
    );
