<?php

$moduleName = 'J_Class';
$viewdefs[$moduleName]['portal']['menu']['header'] = array(
    array(
        'route' => "#$moduleName",
        'label' => 'LNK_LIST',
        'acl_action' => 'list',
        'acl_module' => $moduleName,
        'icon' => 'fa-bars',
    ),
    array(
        'route'=>'#layout/calendar',
        'label' =>'LNK_VIEW_CALENDAR',
        'acl_action'=>'list',
        'acl_module'=>'Calendar',
        'icon' => 'fa-calendar-alt',
    ),
    array(
        'route' => "#$moduleName/attendance_screen",
        'label' => 'LBL_ATTENDANCE_CHECKING',
        'acl_action' => 'import',
        'acl_module' => $moduleName,
        'icon' => 'fa-cloud-upload',
    ),
);
