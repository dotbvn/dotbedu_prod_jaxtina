<?PHP

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/J_Class/J_Class_dotb.php');
class J_Class extends J_Class_dotb {
    //Add by HoHoangHvy to add visibility for Teacher role
    public function __construct()
    {
        parent::__construct();
        $this->addVisibilityStrategy('ClassVisibility');
    }

}
