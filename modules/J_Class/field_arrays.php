<?php

/*********************************************************************************

 * Description:  Contains field arrays that are used for caching
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
$fields_array['J_Class'] = array (
    'export_fields' => array(
        'class_code',
        'name',
        'teacher_name',
        'main_schedule',
        'koc_name',
        'level',
        'status',
        'hours',
        'number_student',
        'start_date',
        'end_date',
    ),
);
?>
