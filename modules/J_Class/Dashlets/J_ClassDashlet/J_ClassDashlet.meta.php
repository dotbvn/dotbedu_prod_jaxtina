<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_ClassDashlet'] = array('module'		=> 'J_Class',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Class'), 
                                          'description' => 'A customizable view into J_Class',
                                          'icon'        => 'icon_J_Class_32.gif',
                                          'category'    => 'Module Views');