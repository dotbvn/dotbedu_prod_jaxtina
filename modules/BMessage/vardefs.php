<?php $dictionary['BMessage'] = array(
    'table' => 'bmessage',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => false,
    'fields' => array(

        'action_type' => array(
            'len' => 150,
            'name' => 'action_type',
            'options' => 'bmes_action_type',
            'required' => true,
            'type' => 'enum',
            'dbType' => 'varchar',
            'vname' => 'LBL_ACTION_TYPE',
            //'isMultiSelect' => true,
        ),
        'send_to' => array(
            'len' => 150,
            'name' => 'send_to',
            'options' => 'bmes_send_to_list',
            'required' => true,
            'type' => 'enum',
            'dbType' => 'varchar',
            'vname' => 'LBL_SEND_TO',
            // 'isMultiSelect' => true,
        ),
        'title' =>
        array(
            'required' => false,
            'name' => 'title',
            'vname' => 'LBL_TITLE',
            'type' => 'text',
            'massupdate' => 0,
            'required' => true,
            'studio' => true,
        ),

        'parent_type' =>
        array(
            'name' => 'parent_type',
            'vname' => 'LBL_PARENT_TYPE',
            'type' => 'enum',
            'dbType' => 'varchar',
            'required' => true,
            'group' => 'moduleList',
            'options' => 'moduleList',
            'len' => 150,
            'studio' => false,
        ),
        'parent_name' =>
        array(
            'name' => 'parent_name',
            'parent_type' => 'bmes_parent_type',
            'type_name' => 'parent_type',
            'id_name' => 'parent_id',
            'vname' => 'LBL_PARENT_NAME',
            'type' => 'parent',
            'group' => 'parent_name',
            'source' => 'non-db',
            'options' => 'moduleList',
            'studio' => true,
            'required' => true,
            'reportable' => true,
        ),
        'parent_id' =>
        array(
            'name' => 'parent_id',
            'vname' => 'LBL_PARENT_ID',
            'type' => 'id',
            'group' => 'parent_name',
            'reportable' => false,
            'required' => true,
        ),
        'status' => array(
            'name' => 'status',
            'vname' => 'LBL_STATUS',
            'type' => 'varchar',
            'len' => 100,
            'default' => 'new',
            'processes' => false,
        ),
        'role_name' => array(
            'name' => 'role_name',
            'vname' => 'LBL_ROLE_NAME',
            'type' => 'varchar',
            'len' => 255,
        ),
        'priority' => array(
            'name' => 'priority',
            'vname' => 'LBL_PRIORITY',
            'type' => 'enum',
            'no_default' => false,
            'options' => 'notifications_severity_list',
            'len' => 100,
            'default' => 'information'
        ),
    ),
    'relationships' => array(),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => false,
);

$dictionary['BMessage']['fields']['name'] = array(
    'name' => 'name',
    'vname' => 'LBL_NUMBER',
    'type' => 'int',
    'readonly' => true,
    'len' => 11,
    'required' => true,
    'auto_increment' => true,
    'unified_search' => true,
    'duplicate_merge' => 'disabled',
    'disable_num_format' => true,
    'studio' => array('quickcreate' => false,),
    'duplicate_on_record_copy' => 'no',
);

$dictionary['BMessage']['fields']['name_html'] = array(
    'required' => false,
    'name' => 'name_html',
    'vname' => 'LBL_NUMBER',
    'type' => 'varchar',
    'source' => 'non-db',
);
require_once("custom/include/utils/bmes.php");
$send_modules = bmes::bmes_parent_type;
foreach ($send_modules as $module => $label) {
    $bean_name = $module;
    if ($module == 'Contacts') $bean_name = 'Contact';
    if ($module == 'Leads') $bean_name = 'Lead';
    if ($module == 'Prospects') $bean_name = 'Prospect';
    if ($module == 'Meetings') $bean_name = 'Meeting';
    if ($module == 'Cases') $bean_name = 'Case';
    $dictionary['BMessage']['fields']['bmes_' . strtolower($label)] = array(
        'name' => 'bmes_' . strtolower($label),
        'type' => 'link',
        'relationship' => 'send_messages_' . strtolower($label),
        'module' => $module,
        'bean_name' => $bean_name,
        'source' => 'non-db',
    );
}
if (!class_exists('VardefManager')) {
}
VardefManager::createVardef('BMessage','BMessage', array('basic','team_security','assignable','taggable'));

//Bỏ fields khỏi màn hình Process - Overwrite fields
$dictionary['BMessage']['fields']['assigned_user_id']['processes'] = false;
