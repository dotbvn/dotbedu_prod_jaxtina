<?php


$module_name = 'J_Budget';
$viewdefs[$module_name]['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'name' => array(),
        'budget_for_center' => array(),
        'budget_for_company' => array(),
        'team_name' => array(),
        'amount' => array(),
        'assigned_user_name' => array(),
        'date_modified' => array(),
        'date_entered' => array(),
        'modified_by_name' => array(),
        'description' => array(),
        'tag' => array(),
        'created_by_name' => array(),
        '$owner' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
    ),
);
