<?php


$module_name = 'J_Teachercontract';
$viewdefs[$module_name]['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'name' => array(),
        'name' => array(),
        'c_teachers_j_teachercontract_1_name' => array(),
        'contract_type' => array(),
        'contract_date' => array(),
        'contract_until' => array(),
        'day_off' => array(),
        'status' => array(),
        'assigned_user_name' => array(),
        'team_name' => array(),
        'less_non_working_hours' => array(),
        'teach_hours' => array(),
        'working_hours_monthly' => array(),
        'admin_hours' => array(),
        'date_modified' => array(),
        'date_entered' => array(),
        '$owner' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'created_by_name' => array(),
        'modified_by_name' => array(),
    ),
);
