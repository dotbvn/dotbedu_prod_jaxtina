<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_Teachercontract/J_Teachercontract.php');

class J_TeachercontractDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Teachercontract/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Teachercontract');

        $this->searchFields = $dashletData['J_TeachercontractDashlet']['searchFields'];
        $this->columns = $dashletData['J_TeachercontractDashlet']['columns'];

        $this->seedBean = new J_Teachercontract();        
    }
}