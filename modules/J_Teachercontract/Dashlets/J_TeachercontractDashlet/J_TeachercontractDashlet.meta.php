<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_TeachercontractDashlet'] = array('module'		=> 'J_Teachercontract',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Teachercontract'), 
                                          'description' => 'A customizable view into J_Teachercontract',
                                          'icon'        => 'icon_J_Teachercontract_32.gif',
                                          'category'    => 'Module Views');