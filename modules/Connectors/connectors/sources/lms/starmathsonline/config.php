<?php



$config = array (
    'name' => 'S.T.A.R Maths Online',
    'eapm' => array(
        'enabled' => true,
        'only' => true,
    ),
    'order' => 12,
    'properties' => array (
        'server_host' => '',
        'secure_id' => '',
        'secure_key' => '',
    ),
);
