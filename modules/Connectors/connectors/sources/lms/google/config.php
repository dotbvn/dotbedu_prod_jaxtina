<?php



$config = array (
  'name' => 'Google',
  'eapm' => array(
    'enabled' => true,
    'only' => true,
  ),
  'order' => 1,
  'properties' => array (
    'oauth2_client_id' => '',
    'oauth2_client_secret' => '',
    'calendar_id' => '',
  ),
);
