<?php
class NotificationsApi extends ModuleApi {
    public function registerApiRest() {
        return array(
            'mark-visited' => array(
                'reqType' => 'GET',
                'path' => array('Notifications', 'mark_all_read'),
                'method' => 'markAllRead',
            ),
            'set_last_viewed_1' => array(
                'reqType' => 'PUT',
                'path' => array('Notifications', 'set_last_viewed'),
                'pathVars' => array(),
                'method' => 'setLastViewed',
            ),
            'get_last_viewed_1' => array(
                'reqType' => 'GET',
                'path' => array('Notifications', 'get_last_viewed'),
                'method' => 'getLastViewed',
            ),
        );
    }
    function markAllRead(ServiceBase $api, array $args){
        global $current_user, $timedate;
        $GLOBALS['db']->query("UPDATE notifications
            SET is_read = 1, modified_user_id = '{$current_user->id}',
            date_modified = '{$timedate->nowDb()}'
            WHERE assigned_user_id = '{$current_user->id}' AND is_read = 0
            AND deleted = 0");
    }

    function setLastViewed(ServiceBase $api, array $args){
        global $current_user;
        $current_user->setPreference('last_notif_viewed', $args['lastViewed'], 0, 'global', $current_user);
        return array('success' => 1);
    }


    function getLastViewed(ServiceBase $api, array $args){
        global $current_user;
        $last_notif_viewed = $current_user->getPreference('last_notif_viewed', 'global');
        return array(
            'success' => 1,
            'last_notif_viewed' => $last_notif_viewed,
        );
    }
}