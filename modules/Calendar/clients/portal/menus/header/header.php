<?php

$module_name = 'Calendar';
$viewdefs[$module_name]['portal']['menu']['header'] = array(
    array(
        'route'=>'#layout/calendar',
        'label' =>'LNK_VIEW_CALENDAR',
        'acl_action'=>'list',
        'acl_module'=>'Calendar',
        'icon' => 'fa-calendar-alt',
    ),
);
