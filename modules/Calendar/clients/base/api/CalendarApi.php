<?php



class CalendarApi extends ModuleApi {
    public function registerApiRest() {
        return array(
            'invitee_search' => array(
                'reqType' => 'GET',
                'path' => array('Calendar', 'invitee_search'),
                'pathVars' => array('', ''),
                'method' => 'inviteeSearch',
                'shortHelp' => 'This method searches for people to invite to an event',
                'longHelp' => 'modules/Calendar/clients/base/api/help/calendar_invitee_search_get_help.html',
            ),
            'get-class-list' => array(
                'reqType' => 'GET',
                'path' => array('calendar', 'get-class-list'),
                'pathVars' => array('', ''),
                'method' => 'getClassList',
            ),
            'get-calendar-data' => array(
                'reqType' => 'POST',
                'path' => array('calendar', 'get-calendar-data'),
                'pathVars' => array('', ''),
                'method' => 'getCalendarData',
            ),

        );
    }
    public function getClassList(ServiceBase $api, array $args)
    {
        $student_id = $_SESSION["contact_id"];
        $q2 = "SELECT DISTINCT
                IFNULL(l3.id, '') class_id,
                IFNULL(l3.name, '') class_name
                FROM contacts
                INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0
                INNER JOIN j_class l3 ON l1.ju_class_id = l3.id AND l3.deleted = 0
                WHERE (((contacts.id = '{$student_id}')                
                AND (IFNULL(l1.type, '') IN ('Enrolled' , 'OutStanding', 'Demo'))))
                AND contacts.deleted = 0";
        $class_list = $GLOBALS['db']->fetchArray($q2);
        return [
            'returnData' => $class_list
        ];
    }
    public function getCalendarData(ServiceBase $api, array $args){
        global $timedate, $current_language;
        $student_id = $_SESSION['contact_id'];
        $mod_list_strings = return_mod_list_strings_language($current_language,'Calendar');
        //Check view
        $date        = $args['select_date']['year'].'-'.$args['select_date']['month'].'-'.$args['select_date']['day'];
        $filter_date = date($timedate->get_db_date_format(),strtotime($date));
        $week        = $args['select_date']['week'];
        $year        = $args['select_date']['year'];
        switch ($args['select_date']['view']){
            case 'day':
            case 'list':
                $start = $end = $filter_date;
                break;
            case 'week':
                $start  = date('Y-m-d',strtotime('monday this week', strtotime($date)));
                $end    = date('Y-m-d',strtotime('sunday this week', strtotime($date)));
                break;
            case 'month':
                $start = date('Y-m-01',strtotime($filter_date)); //first_day_this_month
                //if(date('N',strtotime($start)) != '1') $start = date('Y-m-d',strtotime('monday this week', strtotime($start)));
                $end   = date('Y-m-t',strtotime($filter_date)); //last_day_this_month
                //if(date('N',strtotime($end)) != '7') $end = date('Y-m-d',strtotime('sunday this week', strtotime($end)));
                break;
        }
        //Filter CLASS/CENTER *Bắt buộc
        if(empty($args['session_class']) )
            return json_encode(array("success" => 1, "timetables" => array(), "count" => 0));
        else{
            $ext_class = "AND l3.id IN ('".implode("','", $args['session_class'])."')";
        }
        $sqlSelect = "SELECT DISTINCT
                IFNULL(l3.id, '') class_id,
                IFNULL(l3.name, '') class_name,
                IFNULL(l3.level, '') level,
                IFNULL(l3.status, '') class_status,
                IFNULL(l6.id, '') room_id,
                IFNULL(l6.name, '') room_name,
                IFNULL(l7.id, '') teacher_id,
                IFNULL(l7.full_teacher_name, '') teacher_name,
                IFNULL(l2.id, '') meeting_id,
                IFNULL(l2.week_no, '') ss_week_no,
                IFNULL(l2.syllabus_id, '') ss_syllabus_id,
                IFNULL(l2.meeting_type, '') meeting_type,
                IFNULL(l13.name, '') ss_syllabus_topic,
                IFNULL(l13.description, '') ss_syllabus_content,
                IFNULL(l13.homework, '') ss_syllabus_homework,
                l2.duration_cal ss_duration_cal,
                IFNULL(l9.id, '') l9_id,
                IFNULL(l9.full_teacher_name, '') ss_ta_name,
                IFNULL(l10.id, '') l10_id,
                IFNULL(l10.full_teacher_name, '') ss_teacher_name,
                IFNULL(l11.id, '') l11_id,
                IFNULL(l11.name, '') ss_room_name,
                IFNULL(l12.id, '') attendance_id,
                IFNULL(l12.homework_comment, '') homework_comment,
                IFNULL(l12.description, '') teacher_comment,
                IFNULL(l12.star_rating, '') star_rating,
                l2.date_modified date_modified,
                l2.duration_cal duration_cal,
                l2.date date,
                DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%Y-%m-%d %H:%i:%s') date_time,
                DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%Y-%m-%d %H:%i:%s') date_time_start,
                DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%Y-%m-%d %H:%i:%s') date_time_end,
                LCASE(REPLACE(DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%l:%i%p'),':00','')) start_time,
                DATE_FORMAT(CONVERT_TZ(l2.date_end,'+0:00','+7:00'),'%H:%i') end_time,
                DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%H:%i') start_time_2,
                DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%H:00') start_hour,
                DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%H:%i') time_24hr,
                DATE_FORMAT(CONVERT_TZ(l2.date_start,'+0:00','+7:00'),'%W') week_date
                FROM contacts
                INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0
                INNER JOIN meetings_contacts l2_1 ON l1.id = l2_1.situation_id AND l2_1.deleted = 0 AND contacts.id = l2_1.contact_id
                INNER JOIN meetings l2 ON l2.id = l2_1.meeting_id AND l2.deleted = 0
                INNER JOIN j_class l3 ON l2.ju_class_id = l3.id AND l3.deleted = 0 $ext_class
                LEFT JOIN c_rooms l6 ON l3.room_id = l6.id AND l6.deleted = 0
                LEFT JOIN c_teachers l7 ON l3.teacher_id = l7.id AND l7.deleted = 0
                LEFT JOIN c_teachers l9 ON l2.teacher_cover_id=l9.id AND l9.deleted=0
                LEFT JOIN c_teachers l10 ON l2.teacher_id=l10.id AND l10.deleted=0
                LEFT JOIN c_rooms l11 ON l2.room_id=l11.id AND l11.deleted=0
                LEFT JOIN c_attendance l12 ON l2.id = l12.meeting_id AND l12.deleted = 0 AND l12.student_id = contacts.id
                LEFT JOIN j_syllabus l13 ON l2.syllabus_id = l13.id AND l13.deleted = 0
                WHERE (((contacts.id = '{$student_id}')                
                AND (IFNULL(l2.session_status, '') <> 'Cancelled' OR (IFNULL(l2.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
                AND (l2.date >= '$start' AND l2.date <= '$end')
                AND (IFNULL(l1.type, '') IN ('Enrolled' , 'OutStanding', 'Demo'))))
                ORDER BY start_time_2, end_time, date, date_modified";

        $rows = $GLOBALS['db']->fetchArray($sqlSelect);
        $timetables = $array_day_list = array();
        $count = 0;
        //Duyệt qua từng dòng kết quả
        $mod_strings = return_module_language($GLOBALS["current_language"], 'Calendar');

        foreach ($rows as $row){
            //format kết quả
            $row['name']       = $row['class_name'];
            $row['date_db']    = $row['date'];
            $row['wd_n']       = date('w',strtotime($row['date']));
            $row['week_date_s']= $mod_list_strings['dom_cal_weekdays'][$row['wd_n']];
            $row['week_date_l']= $mod_list_strings['dom_cal_weekdays_long'][$row['wd_n']];
            $row['date']       = $timedate->to_display_date($row['date'],false);
            $row['time_start'] = substr($row['date_start'], 11,5);
            $row['time_end']   = substr($row['date_end'], 11);
            $row['time_range']   = $row['start_time_2'] .' - '. $row['end_time'];
            $row['duration']   = $this->toDisplayDuration($row['duration_cal']);
            $row['type'] = $GLOBALS['app_list_strings']['learning_type_list'][$row['type']];
            $row['type_view'] = $args['select_date']['view'];

            if($row['date_time_end'] < date('Y-m-d H:i:s',(strtotime($timedate->nowDb() . '+7 hours')))){
                $row['class_color'] = 'gray';
                $row['color_popup'] = 'dotb-gray';
            } else {
                if($row['meeting_type'] == 'Placement Test'){
                    $row['class_color'] = 'red';
                    $row['color_popup'] = 'dotb-red';
                } else {
                    $row['class_color'] = 'blue';
                    $row['color_popup'] = 'dotb-blue';
                }
            }
            if(!empty($row['join_url'])) $row['join_url'] = '<a class="btn btn-info" title="'.translate('LBL_JOIN_EXT_MEETING','Meetings').'" role="button" href="'.$row['join_url'].'" target="_blank"><img style="height: 15px;" src="themes/default/images/'.str_replace(' ','_',strtolower($row['type'])).'.png">'.' '.$row['type'].'</a>';

            //Convert Array to Ojbect results
            $timetable = new stdClass();
            foreach ($row as $key => $value) $timetable->$key = $value;

            // assign popup
            $tpl = new Dotb_Smarty();
            $tpl->assign('data', $row);
            $tpl->assign('MOD', $mod_strings);
            $timetable->popup_modal = $tpl->fetch("portal2/custom/modules/Calendar/tpls/popup.tpl");

            //Biuld day list
            $day_list = new Dotb_Smarty();
            $day_list->assign('data', $row);
            $day_list->assign('MOD', $mod_strings);
            $day_list->assign('popup_modal', $timetable->popup_modal);
            $path_item_tpls = $args['select_date']['view'] !== 'month' ? "portal2/custom/modules/Calendar/tpls/modal_week_item.tpl" : "portal2/custom/modules/Calendar/tpls/modal_day_list.tpl";
            $timetable->row_day = $day_list->fetch($path_item_tpls);

            //Show more
            if ($args['select_date']['view'] == 'month') $array_day_list[$row['date_db']] .= $timetable->row_day;
            elseif ($args['select_date']['view'] == 'week') $array_day_list[$row['date_db'].'-'.$row['start_hour']] .= $timetable->row_day;


            array_push($timetables, $timetable);
            $count++;
        }

        return array("success" => 1, "timetables" => $timetables, "showmore" => $array_day_list, "count" => $count);
    }
    function toDisplayDuration($time) {
        $hours = intVal($time);
        $minutes = ($time - $hours) * 60;
        $s_char = ($hours > 1 && $GLOBALS['current_language'] == 'en_us') ? 's ' : ' ';
        $label_hours = translate('LBL_HOURS', 'Calendar').$s_char;
        $s_char = ($minutes > 1 && $GLOBALS['current_language'] == 'en_us') ? 's ' : ' ';
        $label_minutes = translate('LBL_MINUTES', 'Calendar').$s_char;
        $duration_str_hour = $hours > 0 ? $hours . $label_hours : '';
        $duration_str_min = $minutes > 0 ? $minutes . $label_minutes : '';
        return $duration_str_hour . $duration_str_min;
    }
    /**
     * Run a search for possible invitees to invite to a calendar event.
     *
     * TODO: currently uses legacy code - either replace this backend
     *   implementation with global search when it supports searching across
     *   linked fields like account_name or remove the endpoint altogether.
     *   Either way - remember to update api docs.
     *
     * @param ServiceBase $api
     * @param array $args
     * @return array
     * @throws DotbApiExceptionMissingParameter
     */
    public function inviteeSearch(ServiceBase $api, array $args)
    {
        $api->action = 'list';
        $this->requireArgs($args, array('q', 'module_list', 'search_fields', 'fields'));

        //make legacy search request
        $params = $this->buildSearchParams($args);
        $searchResults = $this->runInviteeQuery($params);

        return $this->transformInvitees($api, $args, $searchResults);
    }

    /**
     * Map from global search api arguments to search params expected by
     * legacy invitee search code
     *
     * @param array $args
     * @return array
     */
    protected function buildSearchParams(array $args)
    {
        $modules = explode(',', $args['module_list']);
        $searchFields = explode(',', $args['search_fields']);
        $fieldList = explode(',', $args['fields']);
        $fieldList = array_merge($fieldList, $searchFields);

        $conditions = array();
        foreach ($searchFields as $searchField) {
            $conditions[] = array(
                'name' => $searchField,
                'op' => 'starts_with',
                'value' => $args['q'],
            );
        }

        return array(
            array(
                'modules' => $modules,
                'group' => 'or',
                'field_list' => $fieldList,
                'conditions' => $conditions,
            ),
        );
    }

    /**
     * Run the the legacy invitee query
     *
     * @param $params
     * @return array
     */
    protected function runInviteeQuery($params)
    {
        $requestId = '1'; //not really used
        $jsonServer = new LegacyJsonServer();
        return $jsonServer->query($requestId, $params, true);
    }

    /**
     * Map from legacy invitee search code's result format to a format
     * that is closer to what global search returns
     *
     * Pagination is not supported
     *
     * @param ServiceBase $api
     * @param array $args
     * @param $searchResults
     * @return array
     */
    protected function transformInvitees(ServiceBase $api, array $args, $searchResults)
    {
        $resultList = $searchResults['result']['list'];
        $records = array();
        foreach ($resultList as $result) {
            if (!empty($args['erased_fields'])) {
                $options = ['erased_fields' => true, 'use_cache' => false, 'encode' => false];
                $result['bean'] = BeanFactory::retrieveBean($result['bean']->module_dir, $result['bean']->id, $options);
            }
            $record = $this->formatBean($api, $args, $result['bean']);
            $highlighted = $this->getMatchedFields($args, $record, 1);
            $record['_search'] = array(
                'highlighted' => $highlighted,
            );
            $records[] = $record;
        }

        return array(
            'next_offset' => -1,
            'records' => $records,
        );
    }

    /**
     * Returns an array of fields that matched search query
     *
     * @param array $args Api arguments
     * @param array $record Search result formatted from bean into array form
     * @param int $maxFields Number of highlighted fields to return, 0 = all
     *
     * @return array matched fields key value pairs
     */
    protected function getMatchedFields(array $args, $record, $maxFields = 0)
    {
        $query = $args['q'];
        $searchFields = explode(',', $args['search_fields']);

        $matchedFields = array();
        foreach ($searchFields as $searchField) {
            if (!isset($record[$searchField])) {
                continue;
            }

            $fieldValues = array();
            if ($searchField === 'email') {
                //can be multiple email addresses
                foreach ($record[$searchField] as $email) {
                    $fieldValues[] = $email['email_address'];
                }
            } elseif (is_string($record[$searchField])) {
                $fieldValues = array($record[$searchField]);
            }

            foreach ($fieldValues as $fieldValue) {
                if (stripos($fieldValue, $query) !== false) {
                    $matchedFields[$searchField] = array($fieldValue);
                }
            }
        }

        $ret = array();
        if (!empty($matchedFields) && is_array($matchedFields)) {
            $highlighter = new DotbSearchEngineHighlighter();
            $highlighter->setModule($record['_module']);
            $ret = $highlighter->processHighlightText($matchedFields);
            if ($maxFields > 0) {
                $ret = array_slice($ret, 0, $maxFields);
            }
        }

        return $ret;
    }
}
