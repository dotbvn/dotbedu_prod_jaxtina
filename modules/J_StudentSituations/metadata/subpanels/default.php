<?php


$module_name='J_StudentSituations';
$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name),
    ),

    'where' => '',

    'list_fields' => array(
        'name' =>
        array (
            'type' => 'name',
            'link' => true,
            'vname' => 'LBL_NAME',
            'width' => '10%',
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => NULL,
            'target_record_key' => NULL,
        ),
        'ju_class_name' =>
        array (
            'type' => 'relate',
            'link' => true,
            'vname' => 'LBL_CLASS_NAME',
            'id' => 'JU_CLASS_ID',
            'width' => '10%',
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => 'J_Class',
            'target_record_key' => 'ju_class_id',
        ),
        'type' =>
        array (
            'type' => 'enum',
            'studio' => 'visible',
            'vname' => 'LBL_TYPE',
            'width' => '6%',
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
        ),
        'start_study' =>
        array (
            'type' => 'date',
            'vname' => 'LBL_START_STUDY',
            'width' => '7%',
            'default' => true,
        ),
        'end_study' =>
        array (
            'type' => 'date',
            'vname' => 'LBL_END_STUDY',
            'width' => '7%',
            'default' => true,
        ),
        'total_hour' =>
        array (
            'type' => 'varchar',
            'vname' => 'Total Hours',
            'width' => '7%',
            'default' => true,
        ),
        'total_amount' =>
        array (
            'type' => 'currency',
            'default' => true,
            'vname' => 'LBL_TOTAL_AMOUNT',
            'currency_format' => true,
            'width' => '10%',
        ),
        'description' =>
        array (
            'type' => 'text',
            'vname' => 'LBL_DESCRIPTION',
            'sortable' => false,
            'width' => '10%',
            'default' => true,
        ),
        'team_name' =>
        array (
            'type' => 'relate',
            'link' => true,
            'studio' =>
            array (
                'portallistview' => false,
                'portaldetailview' => false,
                'portaleditview' => false,
            ),
            'vname' => 'LBL_TEAMS',
            'id' => 'TEAM_ID',
            'width' => '10%',
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => 'Teams',
            'target_record_key' => 'team_id',
        ),
    ),
);

?>