<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_StudentSituations/J_StudentSituations.php');

class J_StudentSituationsDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_StudentSituations/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_StudentSituations');

        $this->searchFields = $dashletData['J_StudentSituationsDashlet']['searchFields'];
        $this->columns = $dashletData['J_StudentSituationsDashlet']['columns'];

        $this->seedBean = new J_StudentSituations();        
    }
}