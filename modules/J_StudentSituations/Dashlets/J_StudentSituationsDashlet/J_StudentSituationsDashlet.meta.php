<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_StudentSituationsDashlet'] = array('module'		=> 'J_StudentSituations',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_StudentSituations'), 
                                          'description' => 'A customizable view into J_StudentSituations',
                                          'icon'        => 'icon_J_StudentSituations_32.gif',
                                          'category'    => 'Module Views');