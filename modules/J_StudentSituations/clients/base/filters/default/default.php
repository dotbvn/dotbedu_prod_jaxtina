<?php


$module_name = 'J_StudentSituations';
$viewdefs[$module_name]['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' => array(
        'name' => array(),
        'payment_name' => array(),
        'type' => array(),
        'start_hour' => array(),
        'start_study' => array(),
        'end_study' => array(),
        'total_hour' => array(),
        'total_amount' => array(),
        'team_name' => array(),
        'date_entered' => array(),
        'assigned_user_name' => array(),
        'date_modified' => array(),
        '$owner' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' => array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'created_by_name' => array(),
        'modified_by_name' => array(),
    ),
);
