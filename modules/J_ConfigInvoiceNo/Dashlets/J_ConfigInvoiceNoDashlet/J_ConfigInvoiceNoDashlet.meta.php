<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_ConfigInvoiceNoDashlet'] = array('module'		=> 'J_ConfigInvoiceNo',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_ConfigInvoiceNo'), 
                                          'description' => 'A customizable view into J_ConfigInvoiceNo',
                                          'icon'        => 'icon_J_ConfigInvoiceNo_32.gif',
                                          'category'    => 'Module Views');