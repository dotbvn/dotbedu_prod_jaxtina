<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_ConfigInvoiceNo/J_ConfigInvoiceNo.php');

class J_ConfigInvoiceNoDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_ConfigInvoiceNo/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_ConfigInvoiceNo');

        $this->searchFields = $dashletData['J_ConfigInvoiceNoDashlet']['searchFields'];
        $this->columns = $dashletData['J_ConfigInvoiceNoDashlet']['columns'];

        $this->seedBean = new J_ConfigInvoiceNo();        
    }
}