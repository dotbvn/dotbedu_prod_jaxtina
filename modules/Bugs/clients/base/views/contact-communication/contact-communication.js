/**
 * @author Exsitec AB
 */
({
    iconList: {
        'docx': 'fa-file-word-o',
        'txt': 'fa-file-text',
        'excel': 'fa-file-excel-o',
        'video': 'fa-file-video-o',
        'pptx': 'fa-file-powerpoint-o',
        'audio': 'fa-file-audio-o',
        'pdf': 'fa-file-pdf-o',
        'zip': 'fa-file-archive-o',
        'others': 'fa-file'
    },
    events: {
        'click .add-file-button': 'handleFileBrowsed',
        'click .send-message-button': 'handleSendMessage',
        'keypress .chat__conversation-panel__input': 'checkForEnter',
        'input .chat__conversation-panel__input': 'toggleSendButton',
        'change #file-input': 'handleFileChanged',
        'dragover .boxChatContainer': 'dragOverEvt',
        'dragleave .boxChatContainer': 'dragLeaveEvt',
        'drop .boxChatContainer': 'dropEvt',
        'click .delete-file': 'handleDeleteFile',
        'click .img-preview': 'popupImage',
        'click .messageFile .file-element': 'handleDownloadFile',
        'click .reopen-button': 'handleReopenCase',
        'click .rate-button': 'handleRateCase',
        'click .container__items': 'enableRateButton',
        'paste .chat__conversation-panel__input': 'handlePasteEvent',
        'click .jb-emoji-btn': 'handleInputEmoji',
        'click .add-emoji-button': 'showEmojiPicker',
        'mouseleave .jb-emoji': 'hideEmojiPicker',
        'mouseleave .msg-action-group': 'hideMessageAction',
        'click .fa-trash': 'handleDeleteMessage',
        'click .fa-pencil': 'handleEditMessage',
        'click #cancel-editing': 'cancelEditMessage',
    },
    isLoading: false,
    canLoad: true,
    currentPage: 1,
    page: 1,
    countUploading: 0,
    commentDummy: undefined,
    fileList: [],
    countFile: 0,
    unSentList: [],
    editingId: '',
    editingMesasge: '',
    initialize: function (options) {
        this._super('initialize', [options]);

        this.platform = app.config.platform;

        this.initCommentDummy();
        this.initSocketListener();
        this.fileList = [];
        this.$el.on('mouseover', '.messageContent', this.showMessageAction.bind(this));
        this.$el.on('mouseover', '.messageFile', this.showMessageAction.bind(this));
    },
    /*
    * Socket listener init to listen to the chat channel
    */
    initSocketListener: function () {
        var socketURL = "https://socket.dotb.cloud/";
        var socket = io.connect(socketURL, {"path": "", "transports": ["websocket"], "reconnection": true});
        socket.on('connect', _.bind(function () {
            console.log('Socket server is live!');
            socket.emit('join', '/loadQuyettamMessage/edu/' + this.layout.layout.bugsModel.get('id'));
        }, this));
        socket.on('error', function () {
            console.log('Cannot connect to socket server!')
        })
        socket.on('event-phenikaa', _.bind(function (msg) {
            if (this.matchCaseID(msg.caseId)) {
                let lastComment = $('.messageArea.portal .commNote').last();
                var firstPWithContent = lastComment.find('p').filter(function () {
                    return $.trim($(this).text()).length > 0;
                }).first();
                var signatureDate = lastComment.find('.signatureDate').text();

                if (lastComment.length > 0 && firstPWithContent.text() !== msg.message || (firstPWithContent.text() == msg.message && signatureDate != msg.date_sent)) {
                    if (!this.isCurrentUser(msg.created_by)) {
                        _.each(msg.attachments, _.bind(function (fileModel) {
                            this.appendFile(fileModel, msg.direction, msg.picture, msg.created_by, msg.chatter_name);
                        }, this));
                    }
                    if(msg.isDelete) {
                        $('[data-id="'+msg.id+'"]').closest('.commNote').remove();
                    } else if(msg.isEdit){
                        $('[data-id="'+msg.id+'"]').find('p').text(msg.message);
                    } else {
                        if (msg.message != '' && !this.isCurrentUser(msg.created_by)) {
                            this.appendMessage(msg);
                        }
                    }
                }
            }
        }, this));
    },
    matchCaseID: function (caseID) {
        return caseID == window.location.hash.split('/')[window.location.hash.split('/').length - 1] ;
    },
    /*
    * Init drag drop event
    * */
    initDragDrop: function () {
        this.$(".fileInput").on('dragenter', _.bind(function (ev) {
            this.$(".fileInput").addClass("highlightDropArea");
        }, this));

        this.$(".fileInput").on('dragleave', _.bind(function (ev) {
            this.$(".fileInput").removeClass("highlightDropArea");
        }, this));

        this.$(".fileInput").on('drop', _.bind(function (ev) {
            // Dropping files
            ev.preventDefault();
            ev.stopPropagation();
            if (ev.originalEvent.dataTransfer) {
                if (ev.originalEvent.dataTransfer.files.length) {
                    let droppedFile = _.first(ev.originalEvent.dataTransfer.files);
                    let list = new DataTransfer();
                    list.items.add(droppedFile);
                    this.$('input[type="file"]')[0].files = list.files;
                    this.commentDummy.set('filename', droppedFile.name);
                }
            }

            this.$(".fileInput").removeClass("highlightDropArea");
            return false;
        }, this));

        this.$(".fileInput").on('dragover', _.bind(function (ev) {
            ev.preventDefault();
        }, this));
    },
    /*
    * Show/Hide the outline when drag over the box chat container
    */
    dragOverEvt: function (evt) {
        evt.preventDefault();
        $('.boxChatContainer').addClass("onDragover");
        $('#file-input').toggleClass("dragover", evt.type === "dragover");
    },
    dragLeaveEvt: function (evt) {
        evt.preventDefault();
        $('.boxChatContainer').removeClass("onDragover");
        $('#file-input').removeClass("dragover");
    },

    /*
    * Drop file to box chat container
    * Init the file list and show the file list at chat box
     */
    dropEvt: function (evt) {
        evt.preventDefault();
        $('.boxChatContainer').removeClass("onDragover");
        $('#file-input').removeClass("dragover");
        if(_.isEmpty(this.fileList)) this.fileList = [];
        this.showFileList();
        this.handleFileSelected(evt.originalEvent.dataTransfer.files);
    },
    // Popup file selector when click on attachent button
    handleFileBrowsed: function () {
        $('#file-input').click();
    },
    /*
    * FileChanged event mean that user has selected file from file input
    */

    handleFileChanged: function (e) {
        let files = e.target.files;
        if(_.isEmpty(this.fileList)) this.fileList = [];
        this.showFileList();
        this.handleFileSelected(files);
    },

    /*
    * The file selected will be handled here
    * If the file size is greater than 100000KB, show the alert
    * If the file size is less than 100000KB, add the file to the file list
    **/
    handleFileSelected: function (files) {
        if (files.length > 0) {
            $.each(files, _.bind(function (index, file) {
                var fileSize = (file.size / 1024).toFixed(2);
                var fileType = file.type;
                if (fileSize > 100000) {
                    app.alert.show('error', {
                        level: 'error',
                        messages: app.lang.get('LBL_ALERT_FILE_SIZE_LIMIT'),
                        autoClose: true
                    });
                    return;
                }
                this.addFileRow(file, this.countFile);
            }, this));
        }
    },

    /*
    * File row will be added to the file list
    * We use countFile to tracking how many file has been added
    * We use countUploading to tracking how many file is being uploaded
    * */
    addFileRow: function (file, countFile) {
        let fileName = file.name;
        let fileSize = (file.size / 1024).toFixed(2);
        let fileType = file.type;
        let icon = this.getFileIcon(fileName, fileType);
        let fileSrc = URL.createObjectURL(file);
        let preview = fileType.startsWith("image")
            ? `<img class="img-preview show-image" src="${fileSrc}" alt="${fileName}">`
            : `<i class="fa ${icon}"></i>`;

        this.countFile++;
        this.countUploading++;
        let fileModel = {
            isImage: fileType.startsWith("image"),
            fileName: fileName,
            fileSize: fileSize,
            fileType: fileType,
            preview: preview,
            countFile: countFile,
            fileUrl: fileSrc,
            showDownload: false
        };
        this.fileList.push(fileModel);
        this.appendFileList(fileModel);
        this.uploadFile(file, countFile);
    },
    getFileExtension: function (filename) {
        if (filename.includes('zip')) {
            return 'zip';
        }
        return filename.slice(((filename.lastIndexOf(".") - 1) >>> 0) + 2);
    },
    getFileIcon: function (fileName, fileType) {
        let fileIcon = 'others';
        if (fileType.startsWith("video")) {
            fileIcon = 'video';
        }
        let extension = this.getFileExtension(fileName);
        switch (extension) {
            case 'csv':
            case 'xls':
            case 'xlsx':
                fileIcon = 'excel';
                break;
            case 'docx':
            case 'doc':
                fileIcon = 'docx';
                break;
            case 'txt':
                fileIcon = 'txt';
                break;
            case 'pptx':
                fileIcon = 'pptx';
                break;
            case 'pdf':
                fileIcon = 'pdf';
                break;
            case 'zip':
            case 'rar':
                fileIcon = 'zip';
                break;
        }
        return this.iconList[fileIcon];
    },

    handleDeleteFile: function (evt) {
        let noteId = evt.target.parentElement.parentElement.parentElement.dataset.noteid;
        let saved = evt.target.parentElement.parentElement.parentElement.dataset.saved;

        evt.target.closest(".file-element").remove();
        this.fileList = this.fileList.filter(_.bind(function (file) {
            return file.countFile != this.countFile - 1;
        }, this));
        this.countFile--;

        if (this.countFile === 0) {
            this.clearFileInput();
        }
    },

    /*
    * Add file to the file list to show on the chat box
    */
    appendFileList: function (fileModel) {
        var fileHtml = '<div class="file-element" id="file-upload-' + fileModel.countFile + '">' +
            '<div class="file-info">' +
            '<div class="file-preview">' + fileModel.preview + '</div>' +
            '<div class="file-content">' +
            '<span class="file-name ellipsis_inline" data-original-title="' + fileModel.fileName + '"> ' + fileModel.fileName + '</span>' +
            '<div class="file-size-contain">' +
            '<span class="file-size">' + fileModel.fileSize + ' KB</span>' +
            (fileModel.showDownload ? '<a class="file-download" id="file-download-' + fileModel.countFile + '" download="' + fileModel.fileName + '" href="' + fileModel.fileSrc + '">LBL_DOWNLOAD<i class="fa fa-download" aria-hidden="true"></i></a>' : '') +
            '</div>' +
            '</div>' +
            '<div class="exit-and-percent">' +
            '<i class="fa fa-times delete-file" aria-hidden="true"></i>' +
            '<div id="loader-' + fileModel.countFile + '" ><span class="send-message-loader" style="border: 2px solid #717171;"></span></div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $('.file-list').append(fileHtml);
    },

    /*
    * Event handler
    * */
    cancelEditMessage: function(e) {
        this.clearMessage();
        $('.edit-message-box ').css('display', 'none');
        this.editingId = '';
        this.disableSendButton();
    },
    handleEditMessage: function (e) {
        this.editingMesasge = $(e.currentTarget).closest('.commNote').find('.messageText p');
        this.editingId = e.currentTarget.dataset.id;
        $('.edit-message-box ').css('display', 'grid');
        $('.edit-message-box ').attr('data-original-title', $(this.editingMesasge).text());
        $(".chat__conversation-panel__input").val($(this.editingMesasge).text());
        this.editing = true;
    },
    handleDeleteMessage: function (e) {
        app.alert.show('confirm-rating', {
            level: 'confirmation',
            messages: app.lang.get('LBL_ALERT_DEL_MSG_CONFIRM'),
            onConfirm: _.bind(function (){
                let commentId = e.currentTarget.dataset.id;
                app.api.call(
                    'create',
                    app.api.buildURL('ticket-support/deleteMessage'),
                    {
                        id: commentId,
                        isFile: e.currentTarget.dataset.isfile
                    },
                    {
                        success: function (respone) {
                            e.currentTarget.closest('.commNote').remove();
                            app.alert.show('delete-success', {
                                level: 'success',
                                messages: app.lang.get('LBL_ALERT_DEL_MSG_SUCCESS'),
                                autoClose: true
                            });
                        },
                        error: function (respone) {
                            app.alert.show('error', {
                                level: 'error',
                                messages: app.lang.get('LBL_ALERT_DEL_MSG_FAILED'),
                            })
                        }
                    }
                )
            }, self),
        });
    },
    showMessageAction: function (e) {
        $(e.currentTarget.previousElementSibling).addClass('active');
        setTimeout(function () {
            $(e.currentTarget.previousElementSibling).removeClass('active');
        }, 3000)
    },
    hideMessageAction: function (e) {
        $(e.currentTarget).removeClass('active');
    },
    hideEmojiPicker: function(){
        $('.jb-emoji').hide();
    },
    showEmojiPicker: function(){
        $('.jb-emoji').show();
        setTimeout(function(){
            $('.jb-emoji').hide();
        }, 10000)
    },
    handleInputEmoji: function(e) {
        if(e.currentTarget.innerHTML != ''){
            var emoji = e.currentTarget.innerHTML;
            var $input = $(".chat__conversation-panel__input");
            var input = $input[0];
            var startPos = input.selectionStart;
            var endPos = input.selectionEnd;
            var inputVal = $input.val();

            // Update the input field's value
            $input.val(inputVal.substring(0, startPos) + emoji + inputVal.substring(endPos, inputVal.length));

            // Set cursor position right after the inserted emoji
            input.selectionStart = input.selectionEnd = startPos + emoji.length;
            this.enableSendButton();
        }
    },
    checkForEnter: function (e) {
        // Check if the pressed key is Enter
        if (e.which === 13) { // 13 is the keycode for Enter
            this.handleSendMessage();
        }
    },
    toggleSendButton: function () {
        if ($(".chat__conversation-panel__input").val() == '') {
            $('.send-message-button').addClass('disabled');
        } else {
            $('.send-message-button').removeClass('disabled');
        }
    },
    handlePasteEvent: function (e) {
        if (e.originalEvent.clipboardData.files.length > 0) {
            if(_.isEmpty(this.fileList)) this.fileList = [];
            this.showFileList();
            this.handleFileSelected(e.originalEvent.clipboardData.files);
        }
    },
    /*
    * */
    initCommentDummy: function () {
        this.commentDummy = App.data.createBean('C_Comments', {
            id: this.generateUUID(),
            new_with_id: true,
            parent_id: this.layout.layout.bugsModel.get('id'),
            parent_type: 'Cases',
            direction: 'inbound',
            assigned_user_id: App.user.get('id'),
            created_by: App.user.get('id'),
            picture: App.config.site_url + '/download_attachment.php?id=' + App.user.get('picture'),
            chatter_name: App.user.get('full_name'),
            attachment_list: [],
            team_id: 1,
            team_set_id: 1,
        });
    },

    _render: function () {
        this._super('_render');
        this.$el.find('.noteAttachment').off('click', this.downloadNote).on('click', this.downloadNote);
        $('.contact-communication .messageArea').scrollTop($('.contact-communication .messageArea').prop("scrollHeight"));
        if(this.layout.layout.bugsModel.attributes.status != undefined && this.layout.layout.bugsModel.attributes.status !== 'resolved' && this.layout.layout.bugsModel.attributes.status !== 'closed') {
            this.initDragDrop();
        }
        this.toggleChatState();
        this.$el.css('display', 'flex');
        this.$el.css('justify-content', 'center');
        this.$el.css('align-items', 'center');
        this.$el.css('height', '100%');
        $('.jb-emoji').jbEmoji(function(){});
        $('.communication-container.row-fluid.ninja').css('height', '550px');
        $('.messageArea').on('scroll', _.bind(this.onScrollHandling, this));
    },
    /*
    * Send message related events
    * */
    clearMessage: function () {
        $(".chat__conversation-panel__input").val('');
    },
    getMessage: function () {
        return $(".chat__conversation-panel__input").val();
    },
    scrollBottom: function () {
        $('.contact-communication .messageArea').scrollTop($('.contact-communication .messageArea').prop("scrollHeight"));
    },
    disableSendButton: function () {
        $('.send-message-button').addClass('disabled'); // Disable button send
    },
    enableSendButton: function () {
        $('.send-message-button').removeClass('disabled'); // Disable button send
    },
    toggleSendState: function (id, type) {
        if(type == 'success') {
            $('#' + id).html(app.lang.get("LBL_SENT") + ' <i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;color: black;font-weight: 400;"></i>');
            $('.file-send-loading').each(function() {
                $(this).html('<i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;font-weight: 400;"></i>');
            });
            setTimeout(function(){
                $('#' + id).remove();
            }, 3000)
        } else {
            $('#' + id).html(app.lang.get("LBL_SENT_FAILED") + ' <i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;color: black;font-weight: 400;"></i>');
            $('.file-send-loading').each(function() {
                $(this).html('Sent Failed');
            });
        }
    },
    handleSendMessage: function () {
        if(!_.isEmpty(this.editingId)){
            this.editingMesasge.text($(".chat__conversation-panel__input").val());
            if($(".chat__conversation-panel__input").val() != ''){
                app.api.call(
                    'create',
                    app.api.buildURL('ticket-support/editMessage'),
                    {
                        id: this.editingId,
                        message: $(".chat__conversation-panel__input").val()
                    },
                    {
                        success: function (respone) {
                            app.alert.show('success', {
                                level: 'success',
                                messages: app.lang.get('LBL_ALERT_EDIT_MSG_SUCCESS'),
                                autoClose: true
                            });
                        },
                        error: function (respone) {
                            app.alert.show('error', {
                                level: 'error',
                                messages: app.lang.get('LBL_ALERT_EDIT_MSG_FAILED'),
                            })
                        }
                    }
                )
            }
            this.cancelEditMessage();
            return;
        }
        if (this.validateContent()) { // Make sure message not empty
            this.disableSendButton(); // Disable button send
            this.clearFileInput();
            this.commentDummy.set('description', this.getMessage());
            this.commentDummy.set('direction', 'inbound');
            let noteList = [];
            _.each(this.fileList, _.bind(function (fileModel) {
                noteList.push({
                    id: fileModel.id,
                    action: 'add'
                })
            }), this);
            this.commentDummy.set('attachment_list', noteList);
            app.api.call(
                'create',
                app.api.buildURL('ticket-support/saveMessage'),
                this.commentDummy,
                {
                    success: _.bind(function (respone) {
                        this.toggleSendState(respone.id, 'success')
                    }, this),
                    error: _.bind(function (respone) {
                        console.log(respone)
                        this.toggleSendState(respone.id, 'failed')
                    }, this),
                });
            if (this.commentDummy.get('attachment_list').length > 0) {
                this.sendFile('inbound');
                this.countFile = 0;
            }
            if (this.getMessage() != '') {
                //add message
                let message = {
                    'id': this.commentDummy.id,
                    'message': $(".chat__conversation-panel__input").val(),
                    'direction': 'inbound',
                    'chatter_name': this.commentDummy.get('chatter_name'),
                    'created_by': this.commentDummy.get('created_by'),
                    'picture': this.commentDummy.get('picture'),
                    'date_entered': this.formatCurrentDateTime()
                }
                this.appendMessage(message);
                this.clearMessage();
            }
            this.scrollBottom();
            //init new message object
            this.initCommentDummy();
        }
    },
    isCurrentUser: function (userId) {
        return App.user.get('id') == userId;
    },
    getDirection: function (direction) {
        return ['inbound', 'from'].includes(direction) ? 'from' : 'to';
    },
    appendMessage: function (messageObject) {
        let direction = this.getDirection(messageObject.direction);
        let dateEntered = this.formatCurrentDateTime();
        let messageHtml = this.renderMessageHtml(messageObject.id, dateEntered, direction, messageObject.created_by, messageObject.chatter_name, messageObject.picture, messageObject.message, {},true);
        $('.messageArea').append(messageHtml);
        this.scrollBottom();
    },
    prependMessage: function (messageObject) {
        let direction = this.getDirection(messageObject.direction);
        _.each(messageObject.attachments, _.bind(function (fileModel) {
            this.prependFile(fileModel, direction, messageObject.picture, messageObject);
        }, this));

        if(messageObject.message != '') {
            let messageHtml = this.renderMessageHtml(messageObject.id, messageObject.date_entered, direction, messageObject.assigned_user_id, messageObject.chatter_name, messageObject.picture, messageObject.message, {});
            $('.messageArea').prepend(messageHtml);
        }
    },
    formatCurrentDateTime: function () {
        const now = new Date();
        const day = now.getDate().toString().padStart(2, '0');
        const month = (now.getMonth() + 1).toString().padStart(2, '0'); // +1 because months are 0-indexed.
        const year = now.getFullYear();
        let hours = now.getHours();
        const minutes = now.getMinutes().toString().padStart(2, '0');
        const ampm = hours >= 12 ? 'PM' : 'AM';

        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        hours = hours.toString().padStart(2, '0');

        return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ' ' + ampm;
    },
    validateContent: function () {
        if ($(".chat__conversation-panel__input").val() == '' && this.countFile == 0) {
            return false;
        }
        if (this.countUploading > 0) {
            app.alert.show('error_validation_process', {
                level: 'error',
                messages: app.lang.get('LBL_ALERT_FINISH_UPLOADING'),
                autoClose: false
            });
            return false;
        }
        return true;
    },
    generateUUID: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    /*
    * Lazy loading
    * */
    onScrollHandling: function (e) {
        if (this.canLoad && !this.isLoading) {
            let scrollHeight = $('.contact-communication .messageArea').prop("scrollHeight");
            if ($('.contact-communication .messageArea').scrollTop() == 0) {
                this.isLoading = true;
                this.page++;
                this.showLoading();
                if (parseInt(this.page) == parseInt(this.currentPage) + 1) {

                    app.api.call(
                        'read',
                        app.api.buildURL('ticket-support/loadMessage?bug_id=' + this.layout.layout.bugsModel.get('id') + '&page=' + this.page + '&platform=edu'),
                        null,
                        {
                            success: _.bind(function (respone) {
                                this.isLoading = false;
                                this.canLoad = respone.canLoad;
                                this.currentPage = respone.currentPage;
                                $('.contact-communication .messageArea').scrollTop($('.contact-communication .messageArea').prop("scrollHeight") - scrollHeight)
                                this.hideLoading();
                                _.each(respone.messageList.toReversed(), _.bind(function (messageObject) {
                                    if (messageObject.message != '' || messageObject.attachments.length != 0) this.prependMessage(messageObject);
                                }, this));
                            }, this),
                        });
                } else {
                    this.page = parseInt(this.currentPage) + 1;
                }
            }
        }
    },
    showLoading: function () {
        $('.messageArea').prepend('<span class="message-loader"></span>') //Add loader
    },
    hideLoading: function () {
        $('.message-loader').remove();
    },

    /*
    * File handler
    * */
    sendFile: function (direction) {
        // this.s3Upload();
        _.each(this.fileList, _.bind(function (fileModel) {
            this.appendFile(fileModel, direction, this.commentDummy.get('picture'), this.commentDummy.get('created_by'), this.commentDummy.get('chatter_name'));
        }, this));
        this.fileList = [];
    },
    appendFile: function (fileModel, direction, avt, created_by, chatter_name) {
        let direction_mark = this.getDirection(direction),
            dateEntered = this.formatCurrentDateTime();

        let messageHtml = this.renderMessageHtml('', dateEntered, direction_mark, created_by, chatter_name, avt, '', fileModel, true);
        this.scrollBottom();
        $('.messageArea').append(messageHtml);
    },
    prependFile: function (fileModel, direction, avt, messageObject) {
        let messageHtml = this.renderMessageHtml(messageObject.id, messageObject.date_entered, messageObject.direction, messageObject.assigned_user_id, messageObject.chatter_name, messageObject.picture, '', fileModel);
        $('.messageArea').prepend(messageHtml);
    },
    showFileList: function () {
        $('.file-list').css('display', 'flex');
        $('.messageArea').css('height', '77%');
        $('.send-message-button').removeClass('disabled');
    },
    s3Upload: function (){
        var s3upload = new S3Upload({
            file_dom_selector: '#file-input',
            s3_sign_put_url: 'index.php?entryPoint=signS3',
            onProgress: function (percent, message, publicUrl, file) {
                console.log('Filepicker progress', percent, message);
            },
            onFinishS3Put: _.bind(function (response) {

                // Handle successful upload
                var response = JSON.parse(response);
                this.completeHandler(response, countFile);
            }, this),
            onError: _.bind(function (error) {
                // Handle upload error
                console.error('Upload failed:', error);
            }, this)
        });

    },
    uploadFile: function (file, countFile) {
        var formData = new FormData();
        let self = this;

        formData.append('file', file);
        formData.append('action', 'createNote');
        formData.append('parentModule', 'C_Comments');
        formData.append('userId', app.user.id);

        $.ajax({
            url: 'index.php?entryPoint=uploadFile',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: _.bind(function (response) {
                // Handle successful upload
                var response = JSON.parse(response);
                this.completeHandler(response, countFile);
            }, this),
            error: _.bind(function (error) {
                // Handle upload error
                console.error('Upload failed:', error);
                this.uploadFailed(countFile);
            }, this)
        });
    },
    clearFileInput: function () {
        $('.file-list').empty();
        $('.file-list').css('display', 'none');
        $('.messageArea').css('height', '');
        $('.actionBar').css('height', '');
        $('.send-message-button').addClass('disabled');
    },
    completeHandler: function (response, countFile) {
        let fileSelector = "#file-upload-" + countFile;
        let loader = "#loader-" + countFile;
        // Successful upload
        var id = response.id;
        if (response.success === true) {
            $('.send-message-button').removeClass('disabled');
            this.countUploading--;
            $(fileSelector).attr("data-noteId", id);
            $(loader).html('<i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;font-weight: 400;"></i>');
            this.fileList[countFile].id = id;
        }
        if (response.error_num === 102) {
            this.countUploading--;
            this.uploadFailed(countFile);
        }
    },
    uploadFailed: function (countFile) {
        $(`#file-upload-${countFile} .file-size-contain`).text('Failed to upload')
    },
    popupImage: function (evt) {
        var sourceImage = evt.target.src;
        var width = evt.target.naturalWidth;
        var height = evt.target.naturalHeight;
        if (width > 1000 || height > 1000) {
            width = width / 2;
            height = height / 2;
        }
        var data = `<div style="background-color: #fff;
                height: ${height}px; width: ${width}px;
                border-radius: 3px;
                box-shadow: 5px 5px 20px rgba(0,0,0,0.05);
                position: relative;"><img style="height: ${height}px; width: ${width}px;" src="${sourceImage}"></div>`;
        window.top.$.openPopupLayer({
            name: "imagePopup",
            html: data
        });
    },
    handleDownloadFile: function (evt) {
        $.ajax({
            url: App.config.saas_url + '/index.php?entryPoint=uploadFile',
            type: 'POST',
            dataType: 'json',
            data: {
                action: 'downloadNotes',
                noteId: evt.currentTarget.dataset.noteid
            },
            success: function (response) {
                var a = document.createElement('a');
                a.download = evt.currentTarget.children[0].children[1].children[1].children[1].download;
                a.href = App.config.saas_url +'/'+ response.data.file_source;
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                console.log('Download successfully');
            },
            error: function (error) {
                console.log('Download failed')
            }
        });
    },
    refreshBug: function () {
        //We need to fetch the case model to avoid edit conflicts from hook(s) that sets value on Case when
        //Note is saved. We fetch and then restore any changed values on the Case (if user is editing)
        let changedAttrs = this.context.get('model').changedAttributes(this.context.get('model').getSynced());
        _.each(changedAttrs, function (value, field) {
            //The changed attributes are showing the synced values for some reason, so overwrite them with the changed values
            changedAttrs[field] = this.context.get('model').get(field);
        }, this);
        this.context.get('model').fetch({
            complete: _.bind(function (model) {
                //Defer restore of changed attributes to not have it overridden from fetch of model
                _.defer(_.bind(function () {
                    _.each(changedAttrs, function (value, field) {
                        this.context.get('model').set(field, value);
                    }, this)
                }, this));
            }, this)
        });
    },
    toggleChatState: function () {
        let status = this.layout.layout.bugsModel.get('status');
        if (status == 'resolved' || status == 'closed') {
            $('.chat__conversation-panel').hide();
            $('.chat__conversation-panel-reopen').show();
        }
    },
    handleReopenCase: function () {
        let bugsModel = this.layout.layout.bugsModel;
        bugsModel.set('status', 'reopen');
        bugsModel.save(null, {
            success: _.bind(function () {
                this.refreshBug();
                this.render();
            }, this)
        });
    },
    enableRateButton: function (){
        $('.rate-button').removeClass('disabled');
    },
    handleRateCase: function (evt) {
        let self = this;
        let rate = 0;
        if(evt.target.classList.contains('disabled')){
            app.alert.show('error', {
                level: 'error',
                messages: app.lang.get('LBL_ALERT_RATE'),
                autoClose: true
            });
            return;
        }
        $('input[type="radio"][name="stars"]').each(function() {
            if($(this).is(':checked')) {
                rate = parseInt($(this).attr('id').replace('st', ''), 10);
            }
        });
        app.alert.show('confirm-rating', {
            level: 'confirmation',
            messages: 'Do you want to rate us with ' + rate + ' stars?',
            onConfirm: _.bind(function (){
                let bugsModel = this.layout.layout.bugsModel;
                bugsModel.set('rate', rate);
                bugsModel.set('status', 'closed');
                bugsModel.save(null, {
                    success: _.bind(function () {
                        this.refreshBug();
                        this.render();
                    }, this)
                });
            }, self),
        });
    },

    /*
* Handle render user avatar
* */
    randomColor: function(name) {
        const hRange = [0, 360];
        const sRange = [50, 75];
        const lRange = [25, 60];

        var getHashOfString = function(str) {
            let hash = 0;
            for (let i = 0; i < str.length; i++) {
                hash = str.charCodeAt(i) + ((hash << 5) - hash);
            }
            hash = Math.abs(hash);
            return hash;
        };
        var normalizeHash = function(hash, min, max) {
            return Math.floor((hash % (max - min)) + min);
        };

        const hash = getHashOfString(name);
        const h = normalizeHash(hash, hRange[0], hRange[1]);
        const s = normalizeHash(hash, sRange[0], sRange[1]);
        const l = normalizeHash(hash, lRange[0], lRange[1]);

        return `hsl(${h}, ${s}%, ${l}%)`;
    },
    initName: function(name) {
        var splitArr = name.split(" "),
            initials = splitArr.shift().charAt(0);
        return initials.toUpperCase();
    },

    /*
    * Handle render html for message
    * */
    renderMessageHtml(messageId, dateEntered, direction, userId, userName, userAvatar, content = '', fileModel = {}, is_append = false) {
        let messageHtml = ''
        let pullDirection = direction == 'from' ? 'right' : 'left';

        // User avatar
        let avatarHtml = `<div class ="commIcon userIcon pull-${pullDirection}">`;
        let avatarSrc = userAvatar;
        if (_.isEmpty(avatarSrc)) {
            if (direction == 'to')
                avatarHtml +=`<div class="image-rounded" style="width: 42px; "><img style="object-fit:cover" src="themes/default/images/logo.png"></div>`;
            else avatarHtml +=`<span class="label label-module label-module-lg" style="background-color: ${this.randomColor(userName)}">${this.initName(userName)}</span>`;
        } else avatarHtml += `<div class='image_rounded' style='width: 42px;'><img style="object-fit:cover" src="${avatarSrc}" alt="${userName}"></div>`;
        avatarHtml += '</div>';

        // Content: File or message
        let msgContentHtml = '';
        if(content == '' && Object.hasOwn(fileModel, 'id')) {
            if (direction == 'from')
                msgContentHtml += `<div class="msg-action-group pull-right"><i class="fa fa-trash" aria-hidden="true" rel="tooltip" data-title="Delete message" data-id="${fileModel.id}" data-isfile="true"></i></div>`;

            let fileHtml = '';
            if(fileModel.isImage){
                fileHtml = fileModel.preview;
            } else {
                fileHtml = `<div class="file-element" id="file-upload-${fileModel.countFile}" data-s3uploaded="${fileModel.s3Uploaded}" data-noteId="${fileModel.id}">` +
                    `<div class="file-info"><div class="file-preview">${fileModel.preview}</div>` +
                    `<div class="file-content">` +
                    `<span class="file-name ellipsis_inline" data-original-title="${fileModel.fileName}">${fileModel.fileName}</span>` +
                    `<div class="file-size-contain"><span class="file-size">${fileModel.fileSize} KB</span>` +
                    `<a class="file-download" id="file-download-${fileModel.countFile}" download="${fileModel.fileName}" href="${fileModel.fileUrl}">${app.lang.get('LBL_DOWNLOAD')}<i class="fa fa-download" aria-hidden="true"></i></a>`;

                if(is_append && direction == 'from' && this.isCurrentUser(userId))
                    fileHtml += '<div class="file-send-loading"><span class="send-message-loader" style="border: 2px solid #717171;"></span></div>'
                fileHtml += `</div></div></div></div>`;
            }
            msgContentHtml += `<div class="messageFile pull-${pullDirection}">${fileHtml}</div>`;
        } else {
            if (direction == 'from') {
                msgContentHtml += `<div class="msg-action-group pull-right">
                    <i class="fa fa-pencil" aria-hidden="true" rel="tooltip" data-title="Edit message" data-id="${messageId}"></i>
                    <i class="fa fa-trash" aria-hidden="true" rel="tooltip" data-title="Delete message" data-id=${messageId}"></i></div>`;
            }
            msgContentHtml += `<div class="messageContent pull-${pullDirection}" data-id ="${messageId}">`;
            msgContentHtml += `<div class="messageText"><p>${this.convertHyperLink(content)}</p>\n`;
            msgContentHtml += `<span class='commSignature'>${userName}: `;
            msgContentHtml += `<span class="signatureDate">${dateEntered}</span></span>`;
            msgContentHtml += `</div>`;
            if (is_append && direction == 'from' && this.isCurrentUser(userId))
                msgContentHtml +=`<div class="sending-title" id="${this.commentDummy.id}">${app.lang.get('LBL_SENDING')}<span class='send-message-loader'></span></div>`;
            msgContentHtml += `</div>`;
        }

        if (direction == 'to')
            messageHtml += (avatarHtml + msgContentHtml);
        else messageHtml += (msgContentHtml + avatarHtml);
        return `<div class ="commNote ${direction} clearfix">${messageHtml}</div>`;
    },

    convertHyperLink: function (text) {
        const pattern = /\bhttps?:\/\/[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|\/))*/g;

        const matches = text.match(pattern);

        if (matches) {
            // Remove duplicates to avoid multiple replacements
            const uniqueMatches = [...new Set(matches)];

            uniqueMatches.forEach(match => {
                // Replace each URL in the text with an anchor tag
                text = text.replace(new RegExp(this.escapeRegExp(match), 'g'), `<a href="${match}" target="_blank">${match}</a>`);
            });
        }

        return text;
    },
    escapeRegExp: function (string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }
})