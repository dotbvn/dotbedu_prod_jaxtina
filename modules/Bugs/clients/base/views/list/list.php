<?php

$viewdefs['Bugs']['base']['view']['list'] = array(
    'panels' => array(
        array(
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' => array(
                array(
                    'name' => 'bug_number',
                    'enabled' => true,
                    'default' => true,
                    'readonly' => true,
                ),
                array(
                    'name'=>  'name',
                    'enabled' => true,
                    'default'=>true,
                    'link' => true,
                    'type' => 'html',
                    'width' => 'xxlarge',
                ),
                array (
                    'name' => 'status',
                    'label' => 'LBL_STATUS',
                    'type' => 'status-field',
                    'readonly' => true,
                    'colorClass' => [
                        'open' => 'label-success',
                        'assigned' => 'label-success',
                        'inprogress' => 'label-info',
                        'resolved' => 'label-warning',
                        'reopen' => 'label-info',
                        'closed' => 'label-important'
                    ],
                    'enabled' => true,
                    'default'=>true,
                ),
                array(
                    'name'=>  'type',
                    'enabled' => true,
                    'default'=>true
                ),
                array(
                    'name'=>  'severity',
                    'enabled' => true,
                    'default'=>true,
                ),
                array(
                    'name'=>  'release_name',
                    'enabled' => true,
                    'default' => false,
                    'link' => false,
                ),
                array(
                    'name'=>  'resolution',
                    'enabled' => true,
                    'default'=>false,
                ),
                array(
                    'name'=>  'team_name',
                    'enabled' => true,
                    'default'=>false,
                ),
                array(
                    'name' => 'date_modified',
                    'enabled' => true,
                    'default' => true,
                ),
                array(
                    'name' => 'date_entered',
                    'enabled' => true,
                    'default' => true,
                ),
            ),

        ),
    ),
);
