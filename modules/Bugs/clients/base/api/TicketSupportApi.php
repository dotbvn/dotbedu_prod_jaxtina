<?php
require_once 'clients/base/api/ModuleApi.php';
require_once('custom/include/utils/DotbWebhook.php');

use CaseChat\ConnectorHelper;
use CaseChat\Exception\InvalidLicenseException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class TicketSupportApi extends ModuleApi{

    public function registerApiRest() {
        return array(
            'retrieveBugsRecord' => array(
                'reqType' => 'GET',
                'path' => array('Bugs','?'),
                'pathVars' => array('module','record'),
                'method' => 'retrieveRecord',
                'shortHelp' => 'Returns a single record',
                'longHelp' => 'include/api/help/module_record_get_help.html',
            ),
            'saveMessage' => array(
                'reqType' => 'POST',
                'path' => array('ticket-support','saveMessage'),
                'method' => 'saveMessage',
                'shortHelp' => 'save the message',
                'longHelp' => '',
            ),
            'loadMessage' => array(
                'reqType' => 'GET',
                'path' => array('ticket-support','loadMessage'),
                'method' => 'loadMessage',
                'shortHelp' => 'load the message',
                'longHelp' => '',
            ),
            'updateCaseSupport' => array (
                'reqType' => 'POST',
                'path' => array('ticket-support','updateCaseSupport'),
                'method' => 'updateCaseSupport',
                'shortHelp' => 'update the case support',
                'longHelp' => '',
            ),
            'deleteComment' => array(
                'reqType' => 'POST',
                'path' => array('ticket-support','deleteMessage'),
                'method' => 'deleteMessage',
                'shortHelp' => 'delete the comment',
                'longHelp' => '',
            ),
            'editComment' => array(
                'reqType' => 'POST',
                'path' => array('ticket-support','editMessage'),
                'method' => 'editMessage',
                'shortHelp' => 'edit the comment',
                'longHelp' => '',
            ),
        );
    }

    /**
     * Fix for other api's beeing overwritten.
     *
     * @param ServiceBase $api
     * @param array $args
     *
     * @return string
     * @throws Exception
     * @throws DotbApiExceptionEditConflict
     * @throws DotbApiExceptionInvalidParameter
     */
    public function retrieveRecord($api, $args)
    {
        if (isset($args['record']) && $args['record'] == "filter") {
            require_once 'clients/base/api/FilterApi.php';
            $filterApi = new FilterApi();
            return $filterApi->filterList($api, $args);
        } elseif (isset($args['record']) && $args['record'] == "config") {
            require_once 'clients/base/api/ConfigModuleApi.php';
            $configApi = new ConfigModuleApi();
            return $configApi->config($api, $args);
        } elseif (isset($args['record']) && $args['record'] == "count") {
            require_once 'clients/base/api/FilterApi.php';
            $configApi = new FilterApi();
            return $configApi->getFilterListCount($api, $args);
        } else {
            return $this->retrieveBugsRecord($api, $args);
        }
    }

    public function retrieveBugsRecord($api, $args)
    {
        $this->requireArgs($args, array('module', 'record'));

        $data = parent::retrieveRecord($api, $args);
        $this->fetchCommunication($api, $data);
        return $data;
    }

    private function fetchCommunication($api, array &$data)
    {
        $db = DBManagerFactory::getInstance();
        $data['valid_license'] = true;
        $messageList = $this->fetchComment($api, $db, $data);
        $data['customerMessages'] = $messageList['messageList'];
    }

    private function fetchComment($api, DBManager $db, $data)
    {
        $webhook = new DotbWebhook();
        $result = $webhook->callQuyettamAPI('ticket-support/loadMessage', 'GET',
            [
                'case_id' => $data['id'],
                'page' => $data['page'],
                'platform' => 'tenant',
                'tenant_id' => $GLOBALS['license']['lic_tenant_id']
            ]
        );
        $data = $result['data'];
        foreach($data['messageList'] as $key => $message){
            $dateEntered = TimeDate::getInstance()->fromDb($message['date_entered']);;
            $data['messageList'][$key]['date_entered'] = TimeDate::getInstance()->asUser($dateEntered);
        }
        return $data;
    }
    public function loadMessage($api, $args){
        return $this->fetchComment($api, $GLOBALS['db'], ['id' => $args['bug_id'], 'page' => $args['page'], 'platform' => $args['platform']]);
    }
    public function saveMessage($api, $args)
    {
        $webhook = new DotbWebhook();
        unset($args['__dotb_url']);
        foreach($args as $field => $value){
            $data[$field] = $value;
        }
        $data['assigned_user_id'] = $GLOBALS['current_user']->id;
        require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';
        //Check connection S3
        $AWS = new AWSHelper();
        if (!$AWS->getS3()) return array('success' => 0);

        foreach($data['attachment_list'] as $key => $attachment) {
            $note = BeanFactory::getBean('Notes', $attachment['id']);
            $file_url = 'upload/tmp/' . $note->name;
            $path = $GLOBALS['dotb_config']['brand_id'].'/comments/';
            if(file_exists($file_url)) {
                $result = $AWS->uploadAWS($path, $note->name, $file_url);
                if($result['success']) {
                    $data['attachment_list'][$key]['file_url'] = $file_url;
                    $data['attachment_list'][$key]['file_name'] = $note->filename;
                    $data['attachment_list'][$key]['file_size'] = $note->file_size;
                    $data['attachment_list'][$key]['file_mime_type'] = $note->file_mime_type;
                    $data['attachment_list'][$key]['file_ext'] = $note->file_ext;
                    $data['attachment_list'][$key]['upload_id'] = $result['key_name'];
                    $data['attachment_list'][$key]['note_name'] = $note->name;
                    $note->mark_deleted($note->id);
                    $note->save();
                }
            }
        }

        $data['chatter_name'] = $GLOBALS['current_user']->full_name;
        if($GLOBALS['current_user']->picture) $data['picture'] = $GLOBALS['current_user']->picture ? $GLOBALS['dotb_config']['site_url'].'/download_attachment.php?id='.$GLOBALS['current_user']->picture : '';
        $data['platform'] = 'tenant';
        $result = $webhook->callQuyettamAPI('ticket-support/saveMessage', 'POST', $data);
        $dateEntered = TimeDate::getInstance()->fromDb($result['data']['date_sent']);

        return [
            'success' => true,
            'id' => $data['id'],
            'message' => $data['description'],
            'direction' => $data['direction'],
            'date_sent' => TimeDate::getInstance()->asUser($dateEntered),
        ];
    }
    function updateCaseSupport(ServiceBase $api, $args){
        if(!empty($args['id'])){
            $sqlUpdate = "UPDATE bugs SET status = '{$args['status']}', supporter_name = '{$args['supporter_name']}' WHERE id = '{$args['id']}'";
            $GLOBALS['db']->query($sqlUpdate);
            return ['success' => true];
        }
    }
    public function deleteMessage(ServiceBase $api, $args){
        $webhook = new DotbWebhook();
        $result = $webhook->callQuyettamAPI('ticket-support/deleteMessage', 'POST',
            [
                'id' => $args['id'],
                'isFile' => $args['isFile']
            ]
        );
        return ['success' => true];
    }
    public function editMessage(ServiceBase $api, $args){
        $webhook = new DotbWebhook();
        $result = $webhook->callQuyettamAPI('ticket-support/editMessage', 'POST',
            [
                'id' => $args['id'],
                'message' => $args['message'],
            ]
        );
        return ['success' => true];
    }
}