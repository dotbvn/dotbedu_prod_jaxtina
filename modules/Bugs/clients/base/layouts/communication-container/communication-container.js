/**
 * @author Exsitec AB
 */
({
    activeNote: undefined,
    initialize: function (options) {
        this._super('initialize', [options]);

        Mousetrap.bind(['enter'], _.bind(function (e) {
            if (_.isUndefined(this.activeNote)) {
                return;
            }
            if (!_.isUndefined(this.activeNote.get('portal_flag')) && this.activeNote.get('portal_flag')) {
                this.context.trigger('save-contact-note');
            } else {
                this.context.trigger('save-user-note');
            }
        }, this));
    },
    _render: function (){
        this._super('_render');
    }
})