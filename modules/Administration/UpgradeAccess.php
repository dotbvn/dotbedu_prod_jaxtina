<?php


global $mod_strings;
global $dotb_config;

$ignoreCase = (substr_count(strtolower($_SERVER['SERVER_SOFTWARE']), 'apache/2') > 0)?'(?i)':'';
$htaccess_file   = getcwd() . "/.htaccess";
$contents = getHtaccessData($htaccess_file);

$status =  file_put_contents($htaccess_file, $contents);
if( !$status ){
    echo '<p>' . $mod_strings['LBL_HT_NO_WRITE'] . "<span class=stop>{$htaccess_file}</span></p>\n";
    echo '<p>' . $mod_strings['LBL_HT_NO_WRITE_2'] . "</p>\n";
    echo "{$contents}\n";
}


// cn: bug 9365 - security for filesystem
$uploadDir='';
$uploadHta='';

if (empty($GLOBALS['dotb_config']['upload_dir'])) {
    $GLOBALS['dotb_config']['upload_dir']='upload/';
}

$uploadHta = "upload://.htaccess";

$denyAll =<<<eoq
	Order Deny,Allow
	Deny from all
eoq;

if(file_exists($uploadHta) && filesize($uploadHta)) {
	// file exists, parse to make sure it is current
	if(is_writable($uploadHta)) {
		$oldHtaccess = file_get_contents($uploadHta);
		// use a different regex boundary b/c .htaccess uses the typicals
		if(strstr($oldHtaccess, $denyAll) === false) {
		    $oldHtaccess .= "\n";
			$oldHtaccess .= $denyAll;
		}
		if(!file_put_contents($uploadHta, $oldHtaccess)) {
		    $htaccess_failed = true;
		}
	} else {
		$htaccess_failed = true;
	}
} else {
	// no .htaccess yet, create a fill
	if(!file_put_contents($uploadHta, $denyAll)) {
		$htaccess_failed = true;
	}
}

function getHtaccessData($htaccess_file)
{
    global $dotb_config;

    $contents = '';

    // Adding RewriteBase path for vhost and alias configurations
    $basePath = parse_url($dotb_config['site_url'], PHP_URL_PATH);
    if(empty($basePath)) $basePath = '/';

    $restrict_str = <<<EOQ
# BEGIN DOTBCRM RESTRICTIONS

EOQ;
    if (ini_get('suhosin.perdir') !== false && strpos(ini_get('suhosin.perdir'), 'e') !== false)
    {
        $restrict_str .= "php_value suhosin.executor.include.whitelist upload\n";
    }

    $restrict_str .= <<<EOQ
# Fix mimetype for logo.svg (SP-1395)
AddType     image/svg+xml     .svg
AddType     application/json  .json
AddType     application/javascript  .js

<IfModule mod_rewrite.c>
    Options +FollowSymLinks
    RewriteEngine On
    RewriteBase {$basePath}

EOQ;

    foreach (getForbiddenPaths() as $path) {
        $restrict_str .= sprintf('    RewriteRule (?i)%s - [F]', $path) . PHP_EOL;
    }

// @codingStandardsIgnoreStart
    $restrict_str .= <<<EOQ

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^rest/(.*)$ api/rest.php?__dotb_url=$1 [L,QSA]
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^cache/api/metadata/lang_(.._..)_(.*)_public(_ordered)?\.json$ rest/v10/lang/public/$1?platform=$2&ordered=$3 [N,QSA,DPI]

    RewriteRule ^cache/api/metadata/lang_(.._..)_([^_]*)(_ordered)?\.json$ rest/v10/lang/$1?platform=$2&ordered=$3 [N,QSA,DPI]
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^cache/Expressions/dotbcrm8(_debug)?.js$ rest/v10/ExpressionEngine/functions?debug=$1 [N,QSA,DPI]
    RewriteRule ^cache/jsLanguage/(.._..).js$ index.php?entryPoint=jslang&module=app_strings&lang=$1 [L,QSA,DPI]
    RewriteRule ^cache/jsLanguage/(\w*)/(.._..).js$ index.php?entryPoint=jslang&module=$1&lang=$2 [L,QSA,DPI]
</IfModule>

<IfModule mod_mime.c>
    AddType application/x-font-woff .woff
</IfModule>
<FilesMatch "\.(jpg|png|gif|js|css|ico|woff|svg)$">
        <IfModule mod_headers.c>
                Header set ETag ""
                Header set Cache-Control "max-age=2592000"
                Header set Expires "01 Jan 2112 00:00:00 GMT"
        </IfModule>
</FilesMatch>
<IfModule mod_expires.c>
        ExpiresByType text/css "access plus 1 month"
        ExpiresByType text/javascript "access plus 1 month"
        ExpiresByType application/x-javascript "access plus 1 month"
        ExpiresByType image/gif "access plus 1 month"
        ExpiresByType image/jpg "access plus 1 month"
        ExpiresByType image/png "access plus 1 month"
        ExpiresByType application/x-font-woff "access plus 1 month"
        ExpiresByType image/svg "access plus 1 month"
</IfModule>
# END DOTBCRM RESTRICTIONS

EOQ;
    // @codingStandardsIgnoreEnd

        if(file_exists($htaccess_file)){
            $fp = fopen($htaccess_file, 'r');
            $skip = false;
            while($line = fgets($fp)){

                if(preg_match("/\s*#\s*BEGIN\s*DOTBCRM\s*RESTRICTIONS/i", $line))$skip = true;
                if(!$skip)$contents .= $line;
                if(preg_match("/\s*#\s*END\s*DOTBCRM\s*RESTRICTIONS/i", $line))$skip = false;
            }
        }

        return $contents . $restrict_str;
}