<?php
global $mod_strings;
?>
<h2 style="color:#000;margin-left:15px;"><?php echo $mod_strings['LBL_UPGRADE_TITLE']; ?></h2>
<style>
    ul {
        list-style-type: none;
        padding: 0;
        margin: 10px;
        display: inline-block;
    }

    li {
        text-align: center;
        vertical-align: middle;
        display: inline-block;
        width: 100px;
        height: 95px;
        padding: 5px;
        margin: 5px;
        border: solid 1px #ccc;
        line-height: 1.4;
        border-radius: 2px;
    }

    li:hover {
        border: solid 1px #000;
    }

    li:hover>a{
        color:#000 !important;
    }

    a {
        display: grid;
        text-decoration: none;
        vertical-align: middle;
        padding-top: 0px;
        font-family: SFUIText;
        line-height: 13px;
        color: #535353 !important;
    }

    a:link {
        text-decoration: none;
    }

    li > a > i.fal {
        font-size: 40px;
    }
</style>
<ul>
    <li><a href="./index.php?module=Administration&action=repair"><i class="fal fa-cogs"></i><br/><?php echo $mod_strings['LBL_QUICK_REPAIR_AND_REBUILD']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=RebuildRelationship"><i class="fal fa-network-wired"></i><br/><?php echo $mod_strings['LBL_REBUILD_REL_TITLE']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=RebuildJSLang"><i class="fal fa-cog"></i><br/><?php echo $mod_strings['LBL_REBUILD_JAVASCRIPT_LANG_TITLE']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=upgradeTeams"><i class="fal fa-users"></i><br/><?php echo $mod_strings['LBL_UPGRADE_TEAM_TITLE']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=RebuildSchedulers"><i class="fal fa-alarm-clock"></i><br/><?php echo $mod_strings['LBL_REBUILD_SCHEDULERS_TITLE']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=RebuildDashlets"><i class="fal fa-tachometer-alt-average"></i><br/><?php echo $mod_strings['LBL_REBUILD_DASHLETS_TITLE']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=RepairTeams&silent=0"><i class="fal fa-user-md"></i><br/><?php echo $mod_strings['LBL_REPAIR_TEAMS']; ?></a></li>
    <li><a href="./index.php?module=ACL&action=install_actions"><i class="fal fa-key"></i><br/><?php echo $mod_strings['LBL_REPAIR_ROLES']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=RepairXSS"><i class="fal fa-bug"></i><br/><?php echo $mod_strings['LBL_REPAIR_XSS']; ?></a></li>
    <li><a href="./index.php?module=Administration&action=RepairActivities"><i class="fal fa-cogs"></i><br/><?php echo $mod_strings['LBL_REPAIR_ACTIVITIES']; ?></a></li>
<!--    <li><a href="./index.php?module=Administration&action=repairlang"><i class="fal fa-flag"></i><br/><?php// echo $mod_strings['LBL_REPAIR_LANGUAGE']; ?></a></li>   -->
</ul>