<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/C_Attendance/C_Attendance.php');

class C_AttendanceDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/C_Attendance/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'C_Attendance');

        $this->searchFields = $dashletData['C_AttendanceDashlet']['searchFields'];
        $this->columns = $dashletData['C_AttendanceDashlet']['columns'];

        $this->seedBean = new C_Attendance();        
    }
}