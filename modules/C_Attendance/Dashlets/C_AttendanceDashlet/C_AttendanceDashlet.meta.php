<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['C_AttendanceDashlet'] = array('module'		=> 'C_Attendance',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'C_Attendance'), 
                                          'description' => 'A customizable view into C_Attendance',
                                          'icon'        => 'icon_C_Attendance_32.gif',
                                          'category'    => 'Module Views');