<?php



$dictionary['C_Attendance'] = array(
    'table' => 'c_attendance',
    'audited' => false,
    'activity_enabled' => false,
    'duplicate_merge' => true,
    'fields' => array (
        'date_input' =>
        array (
            'required' => false,
            'name' => 'date_input',
            'vname' => 'LBL_DATE_INPUT',
            'type' => 'date',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'size' => '20',
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
        ),
        'process_trigger' => array(
            'required' => false,
            'name' => 'process_trigger',
            'vname' => 'Process/Workflow Trigger',
            'type' => 'bool',
            'source' => 'non-db',
            'processes' => true, //Áp đặt kích hoạt Process
        ),
        'notificated' =>
        array (
            'name' => 'notificated',
            'vname' => 'notify to apps',
            'type' => 'bool',
            'audited' => true,
            'default' => '0',
            'source' => 'non-db',
        ),

        'attended' =>
        array (
            'name' => 'attended',
            'vname' => 'LBL_ATTENDED',
            'type' => 'bool',
            'default' => '0',
        ),
        'homework' =>
        array (
            'name' => 'homework',
            'vname' => 'LBL_DO_HOMEWORK',
            'type' => 'enum',
            'massupdate' => 0,
            'no_default' => true,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => 50,
            'size' => '20',
            'options' => 'do_homework_list',
            'studio' => 'visible',
            'dependency' => false,
        ),
        'attendance_type' =>
        array (
            'required' => false,
            'name' => 'attendance_type',
            'vname' => 'LBL_ATTENDANCE_TYPE',
            'type' => 'enum',
            'massupdate' => 0,
            'no_default' => true,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => 50,
            'size' => '20',
            'options' => 'attendance_type_list',
            'studio' => 'visible',
            'dependency' => false,
        ),
        'attendance_type_text' =>
        array(
            'name' => 'attendance_type_text',
            'vname' => 'LBL_ATTENDANCE_TYPE',
            'type' => 'varchar',
            'len' => 100,
            'source'=> 'non-db',
        ),
        'homework_score' =>
        array (
            'required' => false,
            'name' => 'homework_score',
            'vname' => 'LBL_HOMEWORK_SCORE',
            'type' => 'decimal',
            'len' => 10,
            'size' => '20',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
            'precision' => 2,
            'default' => '',
        ),
        'homework_comment' =>
        array (
            'required' => false,
            'name' => 'homework_comment',
            'vname' => 'LBL_HOMEWORK_COMMENT',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),
        'sms_content' =>
        array (
            'required' => false,
            'name' => 'sms_content',
            'vname' => 'LBL_SMS_CONTENT',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),
        'care_comment' =>
        array (
            'required' => false,
            'name' => 'care_comment',
            'vname' => 'LBL_CARE_COMMENT',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),

        'star_rating' =>
        array (
            'required' => false,
            'name' => 'star_rating',
            'vname' => 'LBL_STAR_RATING',
            'type' => 'decimal',
            'massupdate' => false,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'pii' => false,
            'default' => '',
            'calculated' => false,
            'len' => '10',
            'enable_range_search' => false,
        ),

        'customer_comment' =>
        array (
            'required' => false,
            'name' => 'customer_comment',
            'vname' => 'LBL_CUSTOMER_COMMENT',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),

        // Relationship Student ( 1 - n ) Attendance - Lap Nguyen
        'student_name' => array(
            'name' => 'student_name',
            'parent_type' => 'student_type_list',
            'type_name' => 'student_type',
            'id_name' => 'student_id',
            'vname' => 'LBL_STUDENT_NAME',
            'type' => 'parent',
            'group' => 'student_name',
            'source' => 'non-db',
            'options' => 'student_type_list',
            'studio' => true,
            'required' => true
        ),

        'student_id' => array(
            'name' => 'student_id',
            'vname' => 'LBL_STUDENT_NAME',
            'type' => 'id',
            'group' => 'student_name',
            'reportable' => false,
        ),

        'student_type' =>
        array(
            'name' => 'student_type',
            'vname' => 'LBL_STUDENT_TYPE',
            'type' => 'enum',
            'dbType' => 'varchar',
            'required' => false,
            'group' => 'student_name',
            'options' => 'student_type_list',
            'len' => 100,
        ),
        'leads' => array(
            'name'          => 'leads',
            'type'          => 'link',
            'relationship'  => 'lead_attendances',
            'module'        => 'Leads',
            'bean_name'     => 'Leads',
            'source'        => 'non-db',
            'vname'         => 'LBL_LEAD',
        ),

        'contacts' => array(
            'name'          => 'contacts',
            'type'          => 'link',
            'relationship'  => 'student_attendances',
            'module'        => 'Contacts',
            'bean_name'     => 'Contacts',
            'source'        => 'non-db',
            'vname'         => 'LBL_STUDENT',
        ),
        //END: Relationship Student ( 1 - n ) Attendance

        // Relationship Session ( 1 - n ) Attendance - Lap Nguyen
        'meeting_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'meeting_name',
            'vname'     => 'LBL_MEETING_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'meeting_id',
            'join_name' => 'meetings',
            'link'      => 'meeting_attendances',
            'table'     => 'meetings',
            'isnull'    => 'true',
            'module'    => 'Meetings',
        ),

        'meeting_id' => array(
            'name'              => 'meeting_id',
            'rname'             => 'id',
            'vname'             => 'LBL_MEETING_NAME',
            'type'              => 'id',
            'table'             => 'meetings',
            'isnull'            => 'true',
            'module'            => 'Meetings',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'meeting_attendances' => array (
            'name'          => 'meeting_attendances',
            'type'          => 'link',
            'relationship'  => 'meeting_attendances',
            'module'        => 'Meetings',
            'bean_name'     => 'Meetings',
            'source'        => 'non-db',
            'vname'         => 'LBL_MEETING_NAME',
        ),
        //END: Relationship Student ( 1 - n ) Attendance

        // Relationship Session ( 1 - n ) Attendance - Lap Nguyen
        'class_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'class_name',
            'vname'     => 'LBL_CLASS_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'class_id',
            'join_name' => 'j_class',
            'link'      => 'class_link',
            'table'     => 'j_class',
            'isnull'    => 'true',
            'module'    => 'J_Class',
        ),

        'class_id' => array(
            'name'              => 'class_id',
            'rname'             => 'id',
            'vname'             => 'LBL_CLASS_NAME',
            'type'              => 'id',
            'table'             => 'j_class',
            'isnull'            => 'true',
            'module'            => 'J_Class',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'class_link' => array (
            'name'          => 'class_link',
            'type'          => 'link',
            'relationship'  => 'class_attendances',
            'module'        => 'J_Class',
            'bean_name'     => 'J_Class',
            'source'        => 'non-db',
            'vname'         => 'LBL_CLASS',
        ),
        //END: Relationship Student ( 1 - n ) Attendance
        //Relationship Attendance ( 1 - n ) Task
        'task_link' =>
        array (
            'name' => 'task_link',
            'type' => 'link',
            'relationship' => 'attendance_tasks',
            'module' => 'Tasks',
            'bean_name' => 'Tasks',
            'source' => 'non-db',
            'vname' => 'LBL_TASK_NAME',
        ),
        //END

        //Non-DB fields - helper change layout Attendance
        'birthdate' =>
        array(
            'name' => 'birthdate',
            'vname' => '*Birthdate',
            'type' => 'varchar',
            'len' => 100,
            'source'=> 'non-db',
        ),
        'nick_name' =>
        array(
            'name' => 'nick_name',
            'vname' => '*Nick-name',
            'type' => 'varchar',
            'len' => 100,
            'source'=> 'non-db',
        ),
        'email' =>
        array(
            'name' => 'email',
            'vname' => '*Email',
            'type' => 'varchar',
            'len' => 100,
            'source'=> 'non-db',
        ),
        'phone_mobile' =>
        array(
            'name' => 'phone_mobile',
            'vname' => '*Phone Mobile',
            'type' => 'varchar',
            'len' => 100,
            'source'=> 'non-db',
        ),
    ),
    'relationships' => array (
        //Relationship Attendance ( 1 - n ) Task
        'attendance_tasks' => array(
            'lhs_module'        => 'C_Attendance',
            'lhs_table'            => 'c_attendance',
            'lhs_key'            => 'id',
            'rhs_module'        => 'Tasks',
            'rhs_table'            => 'tasks',
            'rhs_key'            => 'attendance_id',
            'relationship_type'    => 'one-to-many',
        ),
    ),
    'indices' => array (
        //custom Indices
        array('name' =>'idx_student_id', 'type'=>'index', 'fields'=>array('student_id','deleted')),
        array('name' =>'idx_meeting_id', 'type'=>'index', 'fields'=>array('meeting_id','deleted')),
        array('name' =>'idx_class_id', 'type'=>'index', 'fields'=>array('class_id','deleted')),
        array('name' =>'idx_msc_id', 'type'=>'index', 'fields'=>array('meeting_id','class_id','student_id','deleted')),
        array('name' =>'idx_attendance_all', 'type'=>'index', 'fields'=>array('attendance_type','student_id','meeting_id','deleted')),
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

//SEND MASTER MESSAGE - CONFIG
$module = 'C_Attendance';
require_once("custom/include/utils/bmes.php");
$label      = strtolower(bmes::bmes_parent_type[$module]);
$dictionary[$module]['fields']['bsend_messages'] = array (
    'name' => 'bsend_messages',
    'type' => 'link',
    'relationship' => 'send_messages_'.$label,
    'module' => 'BMessage',
    'bean_name' => 'BMessage',
    'source' => 'non-db',
);

$dictionary[$module]['relationships']['send_messages_'.$label] = array (
    'lhs_module'        => $module,
    'lhs_table'            => strtolower($module),
    'lhs_key'            => 'id',
    'rhs_module'        => 'BMessage',
    'rhs_table'            => 'bmessage',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => $module,
);
//END

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('C_Attendance','C_Attendance', array('basic','team_security','assignable','taggable'));
