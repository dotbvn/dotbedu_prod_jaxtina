<?php

$dictionary['Meeting'] = array('table' => 'meetings','activity_enabled'=>false,
    'unified_search' => true, 'full_text_search' => true, 'unified_search_default_enabled' => true,
    'comment' => 'Meeting activities'
    ,'fields' => array (
        'name' =>
        array (
            'name' => 'name',
            'vname' => 'LBL_SUBJECT',
            'required' => false,
            'type' => 'name',
            'dbType' => 'varchar',
            'unified_search' => true,
            'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 1.43),
            'len' => '50',
            'comment' => 'Meeting name',
            'importable' => 'required',
        ),
        'accept_status' => array (
            'name' => 'accept_status',
            'vname' => 'LBL_ACCEPT_STATUS',
            'type' => 'varchar',
            'dbType' => 'varchar',
            'len' => '20',
            'source'=>'non-db',
        ),
        //bug 39559
        'set_accept_links' => array (
            'name' => 'set_accept_links',
            'vname' => 'LBL_ACCEPT_LINK',
            'type' => 'varchar',
            'dbType' => 'varchar',
            'len' => '20',
            'source'=>'non-db',
        ),
        'location' =>
        array (
            'name' => 'location',
            'vname' => 'LBL_LOCATION',
            'type' => 'varchar',
            'len' => '50',
            'comment' => 'Meeting location',
            'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.36),
        ),
        'password' =>
        array (
            'name' => 'password',
            'vname' => 'LBL_PASSWORD',
            'type' => 'varchar',
            'len' => '50',
            'comment' => 'Meeting password',
            'studio' => array('wirelesseditview'=>false, 'wirelessdetailview'=>false, 'wirelesslistview'=>false, 'wireless_basic_search'=>false),
            'dependency' => 'isInEnum($type,getDD("extapi_meeting_password"))',
        ),
        'join_url' =>
        array (
            'name' => 'join_url',
            'vname' => 'LBL_URL',
            'type' => 'varchar',
            'len' => '600',
            'comment' => 'Join URL',
            'studio' => 'visible',
            'reportable' => true,
        ),
        /*        'host_url' =>
        array (
        'name' => 'host_url',
        'vname' => 'LBL_HOST_URL',
        'type' => 'varchar',
        'len' => '600',
        'comment' => 'Host URL',
        'studio' => 'false',
        ),
        'displayed_url' =>
        array (
        'name' => 'displayed_url',
        'vname' => 'LBL_DISPLAYED_URL',
        'type' => 'url',
        'len' => '400',
        'comment' => 'Meeting URL',
        'studio' => array('wirelesseditview'=>false, 'wirelessdetailview'=>false, 'wirelesslistview'=>false, 'wireless_basic_search'=>false),
        'dependency' => 'and(isAlpha($type),not(equal($type,"Dotb")))',
        'studio' => 'false',
        ), */
        'creator' =>
        array (
            'name' => 'creator',
            'vname' => 'LBL_CREATOR',
            'type' => 'varchar',
            'len' => '255',
            'comment' => 'Meeting creator',
        ),
        'external_id' =>
        array (
            'name' => 'external_id',
            'vname' => 'LBL_EXTERNALID',
            'type' => 'varchar',
            'len' => '255',
            'comment' => 'Meeting ID for external app API',
            'studio' => 'false',
        ),
        'duration_hours' =>
        array (
            'name' => 'duration_hours',
            'vname' => 'LBL_DURATION_HOURS',
            'type' => 'int',
            'comment' => 'Duration (hours)',
            'importable' => 'required',
            'required' => true,
            'massupdate' => false,
            'studio' => false,
            'processes' => true,
            'default' => 0,
            'group'=>'end_date',
            'group_label' => 'LBL_DATE_END',
        ),
        'duration_minutes' =>
        array (
            'name' => 'duration_minutes',
            'vname' => 'LBL_DURATION_MINUTES',
            'type' => 'enum',
            'dbType' => 'int',
            'options' => 'duration_intervals',
            'group'=>'end_date',
            'group_label' => 'LBL_DATE_END',
            'len' => '2',
            'comment' => 'Duration (minutes)',
            'required' => true,
            'massupdate' => false,
            'studio' => false,
            'processes' => true,
            'default' => 0,
        ),
        'duration' =>
        array(
            'name' => 'duration',
            'vname' => 'LBL_DURATION',
            'type' => 'enum',
            'options' => 'duration_dom',
            'source' => 'non-db',
            'comment' => 'Duration handler dropdown',
            'massupdate' => false,
            'reportable' => false,
            'importable' => false,
            'studio' => 'false',
        ),
        'duration_text' =>
        array(
            'name' => 'duration_text',
            'vname' => 'LBL_DURATION',
            'type' => 'varchar',
            'source' => 'non-db',
            'massupdate' => false,
            'reportable' => false,
            'importable' => false,
            'studio' => 'false',
        ),
        'date_start' =>
        array (
            'name' => 'date_start',
            'vname' => 'LBL_CALENDAR_START_DATE',
            'type' => 'datetimecombo',
            'dbType' => 'datetime',
            'comment' => 'Date of start of meeting',
            'importable' => 'required',
            'required' => true,
            'massupdate' => false,
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
            'validation' => array('type' => 'isbefore', 'compareto' => 'date_end', 'blank' => false),
            'studio' => array('recordview' => false, 'wirelesseditview'=>false),
            'full_text_search' => array('enabled' => true, 'searchable' => false),
        ),

        'date_end' =>
        array (
            'name' => 'date_end',
            'vname' => 'LBL_CALENDAR_END_DATE',
            'type' => 'datetimecombo',
            'dbType' => 'datetime',
            'massupdate' => false,
            'comment' => 'Date meeting ends',
            'enable_range_search' => true,
            'options' => 'date_range_search_dom',
            'studio' => array('recordview' => false, 'wirelesseditview'=>false), // date_end is computed by the server from date_start and duration
            'readonly' => true,
            'full_text_search' => array('enabled' => true, 'searchable' => false),
            'group'=>'end_date',
            'group_label' => 'LBL_DATE_END',
        ),
        'date' =>
        array(
            'name' => 'date',
            'vname' => 'LBL_DATE_SCHEDULE',
            'type' => 'date',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => true,
        ),
        'time_range' => array (
            'name' => 'time_range',
            'vname' => 'LBL_TIME_RANGE',
            'required' => false,
            'type' => 'varchar',
            'len' => '100',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => false,
        ),
        'parent_type' =>
        array (
            'name' => 'parent_type',
            'vname'=>'LBL_PARENT_TYPE',
            'type' =>'parent_type',
            'dbType' => 'varchar',
            'group'=>'parent_name',
            'options'=> 'parent_type_display',
            'len' => 100,
            'comment' => 'Module meeting is associated with',
            'studio' => array('searchview'=>false, 'wirelesslistview'=>false),
        ),
        'status' =>
        array (
            'name' => 'status',
            'vname' => 'LBL_STATUS',
            'type' => 'enum',
            'len' => 100,
            'options' => 'meeting_status_dom',
            'comment' => 'Meeting status (ex: Planned, Held, Not held)',
            'default' => 'Planned',
            'duplicate_on_record_copy' => 'no',
            'full_text_search' => array('enabled' => true, 'searchable' => false),
        ),
        // Bug 24170 - Added only to allow the sidequickcreate form to work correctly
        'direction' =>
        array (
            'name' => 'direction',
            'vname' => 'LBL_DIRECTION',
            'type' => 'enum',
            'len' => 100,
            'options' => 'call_direction_dom',
            'comment' => 'Indicates whether call is inbound or outbound',
            'source' => 'non-db',
            'importable' => 'false',
            'massupdate'=>false,
            'reportable'=>false,
            'studio' => 'false',
        ),
        'parent_id' =>
        array (
            'name' => 'parent_id',
            'vname'=>'LBL_PARENT_ID',
            'type' => 'id',
            'group'=>'parent_name',
            'reportable'=>false,
            'comment' => 'ID of item indicated by parent_type',
            'studio' => array('searchview'=>false),
        ),
        'reminder_checked' => array(
            'name' => 'reminder_checked',
            'vname' => 'LBL_POPUP_REMINDER',
            'type' => 'bool',
            'source' => 'non-db',
            'comment' => 'checkbox indicating whether or not the reminder value is set (Meta-data only)',
            'massupdate' => false,
            'studio' => false,
        ),
        'reminder_time' =>
        array (
            'name' => 'reminder_time',
            'vname' => 'LBL_POPUP_REMINDER_TIME',
            'type' => 'enum',
            'dbType' => 'int',
            'options' => 'reminder_time_options',
            'reportable' => false,
            'massupdate' => false,
            'default'=> -1,
            'comment' => 'Specifies when a reminder alert should be issued; -1 means no alert; otherwise the number of seconds prior to the start',
            'studio' => array('recordview' => false, 'wirelesseditview' => false),
        ),
        'email_reminder_checked' => array(
            'name' => 'email_reminder_checked',
            'vname' => 'LBL_EMAIL_REMINDER',
            'type' => 'bool',
            'source' => 'non-db',
            'comment' => 'checkbox indicating whether or not the email reminder value is set (Meta-data only)',
            'massupdate' => false,
            'studio' => false,
        ),
        'email_reminder_time' =>
        array (
            'name' => 'email_reminder_time',
            'vname' => 'LBL_EMAIL_REMINDER_TIME',
            'type' => 'enum',
            'dbType' => 'int',
            'options' => 'reminder_time_options',
            'reportable' => false,
            'massupdate' => false,
            'default'=> -1,
            'comment' => 'Specifies when a email reminder alert should be issued; -1 means no alert; otherwise' .
            ' the number of seconds prior to the start',
        ),
        'email_reminder_sent' => array(
            'name' => 'email_reminder_sent',
            'vname' => 'LBL_EMAIL_REMINDER_SENT',
            'default' => 0,
            'type' => 'bool',
            'comment' => 'Whether email reminder is already sent',
            'studio' => false,
            'massupdate'=> false,
        ),
        'outlook_id' =>
        array (
            'name' => 'outlook_id',
            'vname' => 'LBL_OUTLOOK_ID',
            'type' => 'varchar',
            'len' => '255',
            'reportable' => false,
            'comment' => 'When the Dotb Plug-in for Microsoft Outlook syncs an Outlook appointment, this is the Outlook appointment item ID',
            'studio' => false,
        ),
        'sequence' =>
        array (
            'name' => 'sequence',
            'vname' => 'LBL_SEQUENCE',
            'type' => 'int',
            'len' => '11',
            'reportable' => false,
            'default'=>0,
            'comment' => 'Meeting update sequence for meetings as per iCalendar standards',
            'studio' => false,
        ),
        'contact_name' =>
        array (
            'name' => 'contact_name',
            'rname' => 'name',
            'db_concat_fields'=> array(0=>'first_name', 1=>'last_name'),
            'id_name' => 'contact_id',
            'massupdate' => false,
            'vname' => 'LBL_CONTACT_NAME',
            'type' => 'relate',
            'link'=>'contacts',
            'table' => 'contacts',
            'isnull' => 'true',
            'module' => 'Contacts',
            'join_name' => 'contacts',
            'dbType' => 'varchar',
            'source'=>'non-db',
            'len' => 36,
            'importable' => 'false',
            'studio' => false,
        ),

        'contacts' =>
        array (
            'name' => 'contacts',
            'type' => 'link',
            'module' => 'Contacts',
            'relationship' => 'meetings_contacts',
            'source'=>'non-db',
            'vname'=>'LBL_CONTACTS',
        ),
        'parent_name'=>
        array(
            'name'=> 'parent_name',
            'parent_type'=>'record_type_display' ,
            'type_name'=>'parent_type',
            'id_name'=>'parent_id',
            'vname'=>'LBL_LIST_RELATED_TO',
            'type'=>'parent',
            'group'=>'parent_name',
            'source'=>'non-db',
            'options'=> 'parent_type_display',
            'studio' => 'visible',
        ),
        'users' =>
        array (
            'name' => 'users',
            'type' => 'link',
            'relationship' => 'meetings_users',
            'source'=>'non-db',
            'vname'=>'LBL_USERS',
        ),
        'accept_status_users' => array(
            'massupdate' => false,
            'name' => 'accept_status_users',
            'type' => 'enum',
            'studio' => 'false',
            'source' => 'non-db',
            'vname' => 'LBL_ACCEPT_STATUS',
            'options' => 'dom_meeting_accept_status',
            'importable' => 'false',
            'link' => 'users',
            'rname_link' => 'accept_status',
        ),
        'accounts' =>
        array (
            'name' => 'accounts',
            'type' => 'link',
            'relationship' => 'account_meetings',
            'source'=>'non-db',
            'vname'=>'LBL_ACCOUNT',
        ),
        'revenuelineitems' => array(
            'name' => 'revenuelineitems',
            'type' => 'link',
            'relationship' => 'revenuelineitem_meetings',
            'module' => 'RevenueLineItems',
            'bean_name' => 'RevenueLineItem',
            'source' => 'non-db',
            'vname' => 'LBL_REVENUELINEITEMS',
            'workflow' => false
        ),
        'products' => array(
            'name' => 'products',
            'type' => 'link',
            'relationship' => 'product_meetings',
            'module' => 'Products',
            'bean_name' => 'Product',
            'source' => 'non-db',
            'vname' => 'LBL_PRODUCTS',
        ),
        'bugs' =>
        array (
            'name' => 'bugs',
            'type' => 'link',
            'relationship' => 'bug_meetings',
            'source'=>'non-db',
            'vname'=>'LBL_BUGS',
            'module'=>'Bugs',
        ),
        'leads' =>
        array (
            'name' => 'leads',
            'type' => 'link',
            'relationship' => 'meetings_leads',
            'source'=>'non-db',
            'vname'=>'LBL_LEADS',
        ),
        'opportunity' =>
        array (
            'name' => 'opportunity',
            'type' => 'link',
            'relationship' => 'opportunity_meetings',
            'source'=>'non-db',
            'vname'=>'LBL_OPPORTUNITY',
        ),
        'prospects' =>
        array(
            'name'         => 'prospects',
            'type'         => 'link',
            'relationship' => 'prospect_meetings',
            'source'       => 'non-db',
            'vname'        => 'LBL_PROSPECTS',
            'module'       => 'Prospects',
        ),
        'quotes' =>
        array(
            'name' => 'quotes',
            'type' => 'link',
            'relationship' => 'quote_meetings',
            'source'=>'non-db',
            'vname'=>'LBL_QUOTES',
        ),
        'cases' =>
        array (
            'name' => 'cases',
            'type' => 'link',
            'relationship' => 'case_meetings',
            'source'=>'non-db',
            'vname'=>'LBL_CASE',
        ),
        'notes' =>
        array (
            'name' => 'notes',
            'type' => 'link',
            'relationship' => 'meetings_notes',
            'module'=>'Notes',
            'bean_name'=>'Note',
            'source'=>'non-db',
            'vname'=>'LBL_NOTES',
        ),
        'contact_id' => array(
            'name' => 'contact_id',
            'type' => 'relate',
            'link' => 'contacts',
            'rname' => 'id',
            'vname' => 'LBL_CONTACT_ID',
            'source' => 'non-db',
            'studio' => false,
        ),
        'repeat_type' =>
        array(
            'name' => 'repeat_type',
            'vname' => 'LBL_CALENDAR_REPEAT_TYPE',
            'type' => 'enum',
            'len' => 36,
            'options' => 'repeat_type_dom',
            'comment' => 'Type of recurrence',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_interval' =>
        array(
            'name' => 'repeat_interval',
            'vname' => 'LBL_CALENDAR_REPEAT_INTERVAL',
            'type' => 'int',
            'len' => 3,
            'default' => 1,
            'comment' => 'Interval of recurrence',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_dow' =>
        array(
            'name' => 'repeat_dow',
            'vname' => 'LBL_CALENDAR_REPEAT_DOW',
            'type' => 'varchar',
            'len' => 7,
            'comment' => 'Days of week in recurrence',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_until' =>
        array(
            'name' => 'repeat_until',
            'vname' => 'LBL_CALENDAR_REPEAT_UNTIL_DATE',
            'type' => 'date',
            'comment' => 'Repeat until specified date',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_count' =>
        array(
            'name' => 'repeat_count',
            'vname' => 'LBL_CALENDAR_REPEAT_COUNT',
            'type' => 'int',
            'len' => 7,
            'comment' => 'Number of recurrence',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_selector' =>
        array(
            'name' => 'repeat_selector',
            'vname' => 'LBL_CALENDAR_REPEAT_SELECTOR',
            'type' => 'enum',
            'len' => 36,
            'options' => 'repeat_selector_dom',
            'comment' => 'Repeat selector',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
            'visibility_grid' => array(
                'trigger' => 'repeat_type',
                'values' => array(
                    '' => array(
                        'None',
                    ),
                    'Daily' => array(
                        'None',
                    ),
                    'Weekly' => array(
                        'None',
                    ),
                    'Monthly' => array(
                        'None',
                        'Each',
                        'On',
                    ),
                    'Yearly' => array(
                        'None',
                        'On',
                    ),
                ),
            ),
        ),
        'repeat_days' =>
        array(
            'name' => 'repeat_days',
            'vname' => 'LBL_CALENDAR_REPEAT_DAYS',
            'type' => 'varchar',
            'len' => 128,
            'comment' => 'Days of month',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_ordinal' =>
        array(
            'name' => 'repeat_ordinal',
            'vname' => 'LBL_CALENDAR_REPEAT_ORDINAL',
            'type' => 'enum',
            'len' => 36,
            'options' => 'repeat_ordinal_dom',
            'comment' => 'Repeat ordinal value',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_unit' =>
        array(
            'name' => 'repeat_unit',
            'vname' => 'LBL_CALENDAR_REPEAT_UNIT',
            'type' => 'enum',
            'len' => 36,
            'options' => 'repeat_unit_dom',
            'comment' => 'Repeat unit value',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'repeat_parent_id' =>
        array(
            'name' => 'repeat_parent_id',
            'vname' => 'LBL_REPEAT_PARENT_ID',
            'type' => 'id',
            'len' => 36,
            'comment' => 'Id of the first element of recurring records',
            'importable' => 'false',
            'massupdate' => false,
            'reportable' => false,
            'studio' => 'false',
        ),
        'recurrence_id' => array(
            'name' => 'recurrence_id',
            'vname' => 'LBL_CALENDAR_RECURRENCE_ID',
            'type' => 'datetime',
            'dbType' => 'datetime',
            'comment' => 'Recurrence ID of meeting. Original meeting start date',
            'importable' => false,
            'exportable' => false,
            'massupdate' => false,
            'studio' => false,
            'processes' => false,
            'visible' => false,
            'reportable' => false,
            'hideacl' => true,
        ),
        'recurring_source' =>
        array(
            'name' => 'recurring_source',
            'vname' => 'LBL_RECURRING_SOURCE',
            'type' => 'varchar',
            'len' => 36,
            'comment' => 'Source of recurring meeting',
            'importable' => false,
            'massupdate' => false,
            'reportable' => false,
            'studio' => false,
        ),
        'send_invites' => array(
            'name' => 'send_invites',
            'vname' => 'LBL_SEND_INVITES',
            'type' => 'bool',
            'source' => 'non-db',
            'comment' => 'checkbox indicating whether or not to send out invites (Meta-data only)',
            'massupdate' => false,
        ),
        'invitees' => array(
            'name' => 'invitees',
            'source' => 'non-db',
            'type' => 'collection',
            'vname' => 'LBL_INVITEES',
            'links' => array(
                'contacts',
                'leads',
                'users',
            ),
            'order_by' => 'name:asc',
            'studio' => false,
        ),
        'auto_invite_parent' => array(
            'name' => 'auto_invite_parent',
            'type' => 'bool',
            'source' => 'non-db',
            'comment' => 'Flag to allow for turning off auto invite of parent record -  (Meta-data only)',
            'massupdate' => false,
        ),
        'task_parent' => array(
            'name' => 'task_parent',
            'type' => 'link',
            'relationship' => 'task_meetings_parent',
            'source' => 'non-db',
            'reportable' => false,
        ),
        'contact_parent' => array(
            'name' => 'contact_parent',
            'type' => 'link',
            'relationship' => 'contact_meetings_parent',
            'source' => 'non-db',
            'reportable' => false,
        ),
        'lead_parent' => array(
            'name' => 'lead_parent',
            'type' => 'link',
            'relationship' => 'lead_meetings',
            'source' => 'non-db',
            'reportable' => false
        ),
        'kbcontents_parent' => array(
            'name' => 'kbcontents_parent',
            'type' => 'link',
            'relationship' => 'kbcontent_meetings',
            'source' => 'non-db',
            'vname' => 'LBL_KBDOCUMENTS',
            'reportable' => false,
        ),

        //START: CUSTOM FIEDLS
        'week_date' => array(
            'required' => false,
            'name' => 'week_date',
            'vname' => 'LBL_WEEK_DATE',
            'type' => 'varchar',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '30',
            'size' => '20',
        ),
        'time_start_end' => array(
            'name' => 'time_start_end',
            'vname' => 'LBL_TIME',
            'type' => 'varchar',
            'calculated' => false,
            'source'    => 'non-db',
        ),

        //START: Fields for : Subpanel PT.Demo
        'custom_button' => array (
            'name' => 'custom_button',
            'vname' => 'LBL_CUSTOM_BUTTON',
            'type' => 'varchar',
            'studio' => 'false',
            'source' => 'non-db',
        ),
        //END: Fields for : Subpanel PT.Demo

        //START: Fields for : Feature Schedule Teacher

        'teaching_type' => array(
            'required' => false,
            'name' => 'teaching_type',
            'vname' => 'LBL_TEACHING_TYPE',
            'type' => 'enum',
            'no_default' => false,
            'importable' => 'false',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => 100,
            'size' => '20',
            'options' => 'teaching_type_options',
            'studio' => 'visible',
            'dependency' => false,
        ),
        // change_teacher_reason Đổi thành Descrition
        'meeting_type' => array (
            'name' => 'meeting_type',
            'vname' => 'LBL_MEETING_TYPE',
            'type' => 'enum',
            'len' => 100,
            'comment' => 'Meeting type (ex: Session, PT  )',
            'options' => 'type_meeting_list',
            'default'    => '',
            'massupdate' => false
        ),
        'session_status' => array(
            'name' => 'session_status',
            'vname' => 'LBL_SESSION_STATUS',
            'type' => 'enum',
            'len' => 100,
            'comment' => 'Session Status',
            'options' => 'session_status_list',
            'no_default' => true,
            'massupdate' => false
        ),

        //SYLLABUS FIELDS
        'topic_custom' =>
        array (
            'required' => false,
            'name' => 'topic_custom',
            'vname' => 'LBL_TOPIC_CUSTOM',
            'type' => 'varchar',
            'massupdate' => 0,
            'len' => 255,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),
        'syllabus_custom' =>
        array (
            'required' => false,
            'name' => 'syllabus_custom',
            'vname' => 'LBL_SYLLABUS_CUSTOM',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),
        'homework' =>
        array (
            'required' => false,
            'name' => 'homework',
            'vname' => 'LBL_HOMEWORK_CUSTOM',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),
        'objective_custom' => //Syllabus field: note_for_teacher
        array (
            'required' => false,
            'name' => 'objective_custom',
            'vname' => 'LBL_NOTE_FOR_TEACHER_CUSTOM',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),
        //END SYLLABUS CUSTOM FIELDS
        'syllabus_type' =>
        array (
            'required' => false,
            'name' => 'syllabus_type',
            'vname' => 'LBL_SYL_TYPE',
            'type' => 'enum',
            'len' => 50,
            'options' => 'syllabus_type_list',
        ),
        'type' =>  //Syllabus field: learning_type
        array (
            'name' => 'type',
            'vname' => 'LBL_SYL_LEARNING_TYPE',
            'type' => 'enum',
            'len' => 155,
            'options' => 'learning_type_list',
            'massupdate' => false,
            'studio' => 'visible',
        ),
        'syllabus_topic' =>
        array (
            'required' => false,
            'name' => 'syllabus_topic',
            'vname' => 'LBL_SYL_TOPIC',
            'type' => 'varchar',
            'len' => 225,
        ),
        'syllabus_activities' => //Syllabus field: description (syllabus)
        array (
            'required' => false,
            'name' => 'syllabus_activities',
            'vname' => 'LBL_SYL_SYLLABUS',
            'type' => 'varchar',
            'source'=>'non-db',
        ),
        'syllabus_homework' =>
        array (
            'required' => false,
            'name' => 'syllabus_homework',
            'vname' => 'LBL_SYL_HOMEWORK',
            'type' => 'varchar',
            'source'=>'non-db',
        ),

        'syllabus_objective' => //Syllabus field: note_for_teacher
        array (
            'required' => false,
            'name' => 'syllabus_objective',
            'vname' => 'LBL_SYL_NOTE_FOR_TEACHER',
            'type' => 'varchar',
            'source'=>'non-db',
        ),
        //END SYLLABUS

        'observe_note' =>
        array (
            'required' => false,
            'name' => 'observe_note',
            'vname' => 'LBL_OBSERVE_NOTE',
            'type' => 'text',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
        ),
        'observe_score' =>
        array (
            'name' => 'observe_score',
            'vname' => 'LBL_OBSERVE_SCORE',
            'required' => false,
            'type' => 'varchar',
            'dbType' => 'int',
            'len' => '10',
            'enable_range_search' => true,
        ),



        'week_no' =>
        array (
            'required' => false,
            'name' => 'week_no',
            'vname' => 'LBL_WEEK_NO',
            'type' => 'varchar',
            'massupdate' => 0,
            'no_default' => false,
            'comments' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => false,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '10',
            'size' => '20',
        ),


        //add new field to custom button in subpanel
        'subpanel_button' => array(
            'name' => 'subpanel_button',
            'type' => 'varchar',
            'len' => '1',
            'studio' => 'visible',
            'source' => 'non-db',
            'studio' => 'false',
        ),
        //CANCEL Feature:  Fields
        'makeup_session_name' => array (
            'required' => false,
            'source' => 'non-db',
            'name' => 'makeup_session_name',
            'vname' => 'LBL_MAKEUP_SESSION',
            'type' => 'relate',
            'id_name' => 'makeup_session_id',
            'ext2' => 'Meetings',
            'module' => 'Meetings',
            'rname' => 'name',
            'quicksearch' => 'enabled',
            'studio' => 'false',
        ),
        'makeup_session_id' => array (
            'required' => false,
            'name' => 'makeup_session_id',
            'vname' => 'LBL_MAKEUP_SESSION',
            'type' => 'id',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'merge_filter' => 'disabled',
            'len' => '36',
        ),

        'cancel_by' => array (
            'name' => 'cancel_by',
            'vname' => 'LBL_CANCEL_BY',
            'type' => 'enum',
            'options' => 'cancel_reason_list',
            'len' => 150,
        ),
        'cancel_reason' => array (
            'name' => 'cancel_reason',
            'vname' => 'LBL_CANCEL_REASON',
            'type' => 'text',
        ),
        'avg_attendance' =>
        array(
            'required' => true,
            'name' => 'avg_attendance',
            'vname' => 'LBL_AVG_ATTENDANCE',
            'type' => 'decimal',
            'len' => '13',
            'size' => '20',
            'precision' => '6',
            'display_precision' => 2,
            'processes' => true,
        ),
        'total_student' =>
        array (
            'required' => false,
            'name' => 'total_student',
            'vname' => 'LBL_TOTAL_STUDENT',
            'type' => 'int',
            'len' => '5',
        ),
        'total_attended' =>
        array (
            'required' => false,
            'name' => 'total_attended',
            'vname' => 'LBL_TOTAL_ATTENDED',
            'type' => 'int',
            'len' => '5',
        ),
        'total_absent' =>
        array (
            'required' => false,
            'name' => 'total_absent',
            'vname' => 'LBL_TOTAL_ABSENT',
            'type' => 'int',
            'len' => '5',
        ),
        'attendance_status' => array(
            'name' => 'attendance_status',
            'vname' => 'LBL_ATTENDANCE_STATUS',
            'default' => 0,
            'type' => 'bool',
            'studio' => true,
            'massupdate'=> false,
        ),
        'total_sent' => array(
            'required' => false,
            'name' => 'total_sent',
            'vname' => 'LBL_TOTAL_SENT',
            'type' => 'int',
            'len' => '5',
        ),

        //END: CANCEL Feature Fields
        'duration_cal' =>
        array (
            'required' => false,
            'name' => 'duration_cal',
            'vname' => 'LBL_DURATION',
            'type' => 'decimal',
            'len' => '13',
            'precision' => '4',
            'display_precision' => '2',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
        ),
        'till_hour' =>
        array (
            'required' => false,
            'name' => 'till_hour',
            'vname' => 'LBL_TILL_HOUR',
            'type' => 'decimal',
            'len' => '13',
            'precision' => '2',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
        ),
        'delivery_hour' =>
        array (
            'required' => false,
            'name' => 'delivery_hour',
            'vname' => 'LBL_DELIVERY_HOUR',
            'type' => 'decimal',
            'audited' => true,
            'len' => '13',
            'precision' => '4',
            'display_precision' => '2',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
        ),
        'teaching_hour' =>
        array (
            'required' => false,
            'name' => 'teaching_hour',
            'vname' => 'LBL_TEACHING_HOUR',
            'type' => 'decimal',
            'audited' => true,
            'len' => '13',
            'precision' => '4',
            'display_precision' => '2',
            'enable_range_search' => true,
            'options' => 'numeric_range_search_dom',
        ),
        'lesson_number' =>
        array (
            'required' => false,
            'name' => 'lesson_number',
            'vname' => 'LBL_LESSON_NUMBER',
            'type' => 'int',
            'dbType' => 'varchar',
            'massupdate' => 0,
            'default' => '',
            'comments' => '',
            'help' => '',
            'importable' => 'true',
            'duplicate_merge' => 'disabled',
            'duplicate_merge_dom_value' => '0',
            'audited' => true,
            'reportable' => true,
            'unified_search' => false,
            'merge_filter' => 'disabled',
            'calculated' => false,
            'len' => '5',
            'size' => '20',
            'enable_range_search' => false,
            'disable_num_format' => '',
        ),
        //Non-db field for portal - HoangHvy
        array(
            'name' => 'student_feedback',
            'vname' => 'LBL_STUDENT_FEEDBACK',
            'type' => 'varchar',
            'source' => 'non-db',
        ),
        array(
            'name' => 'teacher_comment',
            'vname' => 'LBL_TEACHER_COMMENT',
            'type' => 'varchar',
            'source' => 'non-db',
        ),
        array(
            'name' => 'attendance',
            'vname' => 'LBL_ATTENDANCE',
            'type' => 'varchar',
            'source' => 'non-db',
        ),
        array(
            'name' => 'student_rate',
            'vname' => 'LBL_RATE',
            'type' => 'varchar',
            'source' => 'non-db',
        ),
        //Custom Relationship. Class - Meeting 1 - n
        'ju_class_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'ju_class_name',
            'vname'     => 'LBL_JU_CLASS_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'ju_class_id',
            'join_name' => 'j_class',
            'link'      => 'j_classes',
            'table'     => 'j_class',
            'isnull'    => 'true',
            'module'    => 'J_Class',
        ),

        'ju_class_id' => array(
            'name'              => 'ju_class_id',
            'rname'             => 'id',
            'vname'             => 'LBL_JU_CLASS_NAME',
            'type'              => 'id',
            'table'             => 'j_class',
            'isnull'            => 'true',
            'module'            => 'J_Class',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'j_classes' => array(
            'name'          => 'j_classes',
            'type'          => 'link',
            'relationship'  => 'j_class_meetings',
            'module'        => 'J_Class',
            'bean_name'     => 'J_Class',
            'source'        => 'non-db',
            'vname'         => 'LBL_JU_CLASS_NAME',
        ),

        'process_trigger' => array(
            'required' => false,
            'name' => 'process_trigger',
            'vname' => 'Process/Workflow Trigger',
            'type' => 'bool',
            'source' => 'non-db',
            'processes' => true, //Áp đặt kích hoạt Process
        ),

        //Custom Relationship. Meeting - Timesheet 1 - n
        'c_timesheet_link'=>array(
            'name' => 'c_timesheet_link',
            'type' => 'link',
            'relationship' => 'meeting_c_timesheet',
            'module' => 'C_Timesheet',
            'bean_name' => 'C_Timesheet',
            'source' => 'non-db',
            'vname' => 'LBL_TIMESHEET_NAME',
        ),

        'is_timesheet' => array(
            'name' => 'is_timesheet',
            'vname' => 'LBL_IS_TIMESHEET',
            'type' => 'bool',
            'default' => 0,
        ),

        //Custom Relationship. Teacher - Meeting
        'teacher_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'teacher_name',
            'vname'     => 'LBL_TEACHER_NAME',
            'type'      => 'relate',
            'rname'     => 'full_teacher_name',
            'id_name'   => 'teacher_id',
            'join_name' => 'c_teachers',
            'link'      => 'c_teachers',
            'table'     => 'c_teachers',
            'isnull'    => 'true',
            'module'    => 'C_Teachers',
        ),

        'teacher_id' => array(
            'name'              => 'teacher_id',
            'rname'             => 'id',
            'vname'             => 'LBL_TEACHER_NAME',
            'type'              => 'id',
            'table'             => 'c_teachers',
            'isnull'            => 'true',
            'module'            => 'C_Teachers',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'c_teachers' => array(
            'name'          => 'c_teachers',
            'type'          => 'link',
            'relationship'  => 'teachers_meetings',
            'module'        => 'C_Teachers',
            'bean_name'     => 'C_Teachers',
            'source'        => 'non-db',
            'vname'         => 'LBL_TEACHER_NAME',
        ),

        //Custom Relationship. Teacher - Meeting
        'sub_teacher_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'sub_teacher_name',
            'vname'     => 'LBL_SUB_TEACHER_NAME',
            'type'      => 'relate',
            'rname'     => 'full_teacher_name',
            'id_name'   => 'sub_teacher_id',
            'join_name' => 'c_teachers',
            'link'      => 'sub_c_teachers',
            'table'     => 'c_teachers',
            'isnull'    => 'true',
            'module'    => 'C_Teachers',
        ),

        'sub_teacher_id' => array(
            'name'              => 'sub_teacher_id',
            'rname'             => 'id',
            'vname'             => 'LBL_SUB_TEACHER_NAME',
            'type'              => 'id',
            'table'             => 'c_teachers',
            'isnull'            => 'true',
            'module'            => 'C_Teachers',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'sub_c_teachers' => array(
            'name'          => 'sub_c_teachers',
            'type'          => 'link',
            'relationship'  => 'sub_teachers_meetings',
            'module'        => 'C_Teachers',
            'bean_name'     => 'C_Teachers',
            'source'        => 'non-db',
            'vname'         => 'LBL_SUB_TEACHER_NAME',
        ),

        //Custom Relationship Cover Teacher - Meeting

        'teacher_cover_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'teacher_cover_name',
            'vname'     => 'LBL_TEACHER_COVER_NAME',
            'type'      => 'relate',
            'rname'     => 'full_teacher_name',
            'id_name'   => 'teacher_cover_id',
            'join_name' => 'c_teachers',
            'link'      => 'c_teachers_cover',
            'table'     => 'c_teachers',
            'isnull'    => 'true',
            'module'    => 'C_Teachers',
        ),

        'teacher_cover_id' => array(
            'name'              => 'teacher_cover_id',
            'rname'             => 'id',
            'vname'             => 'LBL_TEACHER_COVER_NAME',
            'type'              => 'id',
            'table'             => 'c_teachers',
            'isnull'            => 'true',
            'module'            => 'C_Teachers',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'c_teachers_cover' => array(
            'name'          => 'c_teachers_cover',
            'type'          => 'link',
            'relationship'  => 'teachers_cover_meetings',
            'module'        => 'C_Teachers',
            'bean_name'     => 'C_Teachers',
            'source'        => 'non-db',
            'vname'         => 'LBL_TEACHER_COVER_NAME',
        ),

        //Custom Relationship Room - Meeting
        'room_name' => array(
            'required'  => false,
            'source'    => 'non-db',
            'name'      => 'room_name',
            'vname'     => 'LBL_ROOM_NAME',
            'type'      => 'relate',
            'rname'     => 'name',
            'id_name'   => 'room_id',
            'join_name' => 'c_rooms',
            'link'      => 'c_rooms',
            'table'     => 'c_rooms',
            'isnull'    => 'true',
            'module'    => 'C_Rooms',
        ),

        'room_id' => array(
            'name'              => 'room_id',
            'rname'             => 'id',
            'vname'             => 'LBL_ROOM_NAME',
            'type'              => 'id',
            'table'             => 'c_rooms',
            'isnull'            => 'true',
            'module'            => 'C_Rooms',
            'dbType'            => 'id',
            'reportable'        => false,
            'massupdate'        => false,
            'duplicate_merge'   => 'disabled',
        ),

        'c_rooms' => array(
            'name'          => 'c_rooms',
            'type'          => 'link',
            'relationship'  => 'ju_rooms_meetings',
            'module'        => 'C_Rooms',
            'bean_name'     => 'C_Rooms',
            'source'        => 'non-db',
            'vname'         => 'LBL_ROOM_NAME',
        ),

        //Relationship Meeting(Session) ( 1 - n ) Attendance
        'meeting_attendances' => array(
            'name' => 'meeting_attendances',
            'type' => 'link',
            'relationship' => 'meeting_attendances',
            'module' => 'C_Attendance',
            'bean_name' => 'C_Attendance',
            'source' => 'non-db',
            'vname' => 'LBL_ATTENDANCE',
        ),

        //Relationship Meeting(Session) ( 1 - n ) ClassStudent
        'class_std_link' => array(
            'name' => 'class_std_link',
            'type' => 'link',
            'relationship' => 'meeting_class_std',
            'module' => 'J_ClassStudents',
            'bean_name' => 'J_ClassStudents',
            'source' => 'non-db',
            'vname' => 'LBL_CLASS_STUDENT',
        ),
        //Relationship Session ( 1 - n ) Delivery Revenue
        'session_revenue' => array(
            'name' => 'session_revenue',
            'type' => 'link',
            'relationship' => 'session_revenue',
            'module' => 'C_DeliveryRevenue',
            'bean_name' => 'C_DeliveryRevenue',
            'source' => 'non-db',
            'vname' => 'LBL_DELIVERY_REVENUE',
        ),

        //Custom Relationship Situatin - Meeting (n) - (n)
        'j_studentsituations' =>
        array (
            'name' => 'j_studentsituations',
            'type' => 'link',
            'relationship' => 'meetings_situations',
            'source'=>'non-db',
            'vname'=>'LBL_SITUATION',
        ),

        //START: CUSTOM NON-DB FIEDLS
        'number_of_student' =>
        array (
            'required' => false,
            'name' => 'number_of_student',
            'vname' => 'LBL_NUMBER_OF_STUDENT',
            'type' => 'varchar',
            'source'=>'non-db',
        ),
        //END: CUSTOM NON-DB FIEDLS

        //New Relationship J_PTResult
        'ptresult_link'=>array(
            'name' => 'ptresult_link',
            'type' => 'link',
            'relationship' => 'meeting_ptresults',
            'module' => 'J_PTResult',
            'bean_name' => 'J_PTResult',
            'source' => 'non-db',
            'vname' => 'LBL_PTRESULT',
        ),

        'onl_record_link' => array(
            'name' => 'onl_record_link',
            'type' => 'varchar',
            'len' => 255,
            'vname' => 'LBL_ONL_RECORD_LINK'
        )

    ),
    'relationships' => array (
        'meetings_assigned_user' =>
        array('lhs_module'=> 'Users', 'lhs_table'=> 'users', 'lhs_key' => 'id',
            'rhs_module'=> 'Meetings', 'rhs_table'=> 'meetings', 'rhs_key' => 'assigned_user_id',
            'relationship_type'=>'one-to-many')

        ,'meetings_modified_user' =>
        array('lhs_module'=> 'Users', 'lhs_table'=> 'users', 'lhs_key' => 'id',
            'rhs_module'=> 'Meetings', 'rhs_table'=> 'meetings', 'rhs_key' => 'modified_user_id',
            'relationship_type'=>'one-to-many')

        ,'meetings_created_by' =>
        array('lhs_module'=> 'Users', 'lhs_table'=> 'users', 'lhs_key' => 'id',
            'rhs_module'=> 'Meetings', 'rhs_table'=> 'meetings', 'rhs_key' => 'created_by',
            'relationship_type'=>'one-to-many')

        ,'meetings_notes' => array('lhs_module'=> 'Meetings', 'lhs_table'=> 'meetings', 'lhs_key' => 'id',
            'rhs_module'=> 'Notes', 'rhs_table'=> 'notes', 'rhs_key' => 'parent_id',
            'relationship_type'=>'one-to-many', 'relationship_role_column'=>'parent_type',
            'relationship_role_column_value'=>'Meetings'),

        // Relationship Session ( 1 - n ) Delivery Revenue
        'session_revenue' => array(
            'lhs_module'        => 'Meetings',
            'lhs_table'            => 'meetings',
            'lhs_key'            => 'id',
            'rhs_module'        => 'C_DeliveryRevenue',
            'rhs_table'            => 'c_deliveryrevenue',
            'rhs_key'            => 'session_id',
            'relationship_type'    => 'one-to-many',
        ),

        // Relationship Session ( 1 - n ) Attendance - Lap Nguyen
        'meeting_attendances' => array(
            'lhs_module'        => 'Meetings',
            'lhs_table'            => 'meetings',
            'lhs_key'            => 'id',
            'rhs_module'        => 'C_Attendance',
            'rhs_table'            => 'c_attendance',
            'rhs_key'            => 'meeting_id',
            'relationship_type'    => 'one-to-many',
        ),
        // Relationship Session ( 1 - n ) Class Student - Lap Nguyen
        'meeting_class_std' => array(
            'lhs_module'        => 'Meetings',
            'lhs_table'            => 'meetings',
            'lhs_key'            => 'id',
            'rhs_module'        => 'J_ClassStudents',
            'rhs_table'            => 'j_classstudents',
            'rhs_key'            => 'meeting_id',
            'relationship_type'    => 'one-to-many',
        ),

        // Relationship Schedule ( 1 - n ) PT/Demo - Lap Nguyen
        'meeting_ptresults' => array(
            'lhs_module'        => 'Meetings',
            'lhs_table'            => 'meetings',
            'lhs_key'            => 'id',
            'rhs_module'        => 'J_PTResult',
            'rhs_table'            => 'j_ptresult',
            'rhs_key'            => 'meeting_id',
            'relationship_type'    => 'one-to-many',
        ),

        //Custom Relationship. Meeting - Timesheet 1 - n
        'meeting_c_timesheet'   => array(
            'lhs_module'        => 'Meetings',
            'lhs_table'         => 'meetings',
            'lhs_key'           => 'id',
            'rhs_module'        => 'C_Timesheet',
            'rhs_table'         => 'c_timesheet',
            'rhs_key'           => 'meeting_id',
            'relationship_type' => 'one-to-many',
        ),
    ),
    'acls' => array('DotbACLOpi' => true, 'DotbACLStatic' => true),


    'indices' => array (
        array('name' =>'idx_mtg_name', 'type'=>'index', 'fields'=>array('name')),
        array('name' =>'idx_meet_par_del', 'type'=>'index', 'fields'=>array('parent_id','parent_type','deleted')),
        array('name' => 'idx_meet_stat_del', 'type' => 'index', 'fields'=> array('assigned_user_id', 'status', 'deleted')),
        array('name' => 'idx_meet_date_start', 'type' => 'index', 'fields'=> array('date_start')),
        array('name' => 'idx_meet_recurrence_id', 'type' => 'index', 'fields' => array('recurrence_id')),
        array('name' => 'idx_meet_date_start_end_del', 'type' => 'index', 'fields'=> array('date_start', 'date_end', 'deleted')),
        array(
            'name' => 'idx_meet_repeat_parent_id',
            'type' => 'index',
            'fields' => array('repeat_parent_id', 'deleted'),
        ),
        // due to pulls from client side to check if there are reminders to handle.
        array('name' => 'idx_meet_date_start_reminder', 'type' => 'index', 'fields'=> array('date_start', 'reminder_time')),

        //custom Indices
        array('name' =>'idx_teacher_id', 'type'=>'index', 'fields'=>array('teacher_id','deleted')),
        array('name' =>'idx_class_id', 'type'=>'index', 'fields'=>array('ju_class_id','deleted')),
        array('name' =>'idx_mt_teacher_class', 'type'=>'index', 'fields'=>array('id','ju_class_id','teacher_id','deleted')),
        array('name' =>'idx_timesheet_id', 'type'=>'index', 'fields'=>array('timesheet_id','deleted')),
        array('name' =>'idx_room_id', 'type'=>'index', 'fields'=>array('room_id','deleted')),
        array('name' =>'idx_teacher_cover_id', 'type'=>'index', 'fields'=>array('teacher_cover_id','deleted')),
        array('name' =>'idx_session_status_id', 'type'=>'index', 'fields'=>array('session_status','deleted')),
    )
    //This enables optimistic locking for Saves From EditView
    ,'optimistic_locking'=>true,

    'duplicate_check' => array(
        'enabled' => false
    ),
);

//SEND MASTER MESSAGE - CONFIG
$module     = 'Meetings';
$module_dic = 'Meeting';
require_once("custom/include/utils/bmes.php");
$label      = strtolower(bmes::bmes_parent_type[$module]);
$dictionary[$module_dic]['fields']['bsend_messages'] = array (
    'name' => 'bsend_messages',
    'type' => 'link',
    'relationship' => 'send_messages_'.$label,
    'module' => 'BMessage',
    'bean_name' => 'BMessage',
    'source' => 'non-db',
);

$dictionary[$module_dic]['relationships']['send_messages_'.$label] = array (
    'lhs_module'        => $module,
    'lhs_table'            => strtolower($module),
    'lhs_key'            => 'id',
    'rhs_module'        => 'BMessage',
    'rhs_table'            => 'bmessage',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => $module,
);
//END

VardefManager::createVardef('Meetings','Meeting', array('default', 'assignable',
    'team_security',
));

//boost value for full text search
$dictionary['Meeting']['fields']['description']['full_text_search']['boost'] = 0.55;

