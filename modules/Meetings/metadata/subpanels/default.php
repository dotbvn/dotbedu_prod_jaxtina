<?php


$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class'=>'SubPanelTopCreateButton'),
        array('widget_class'=>'SubPanelTopSelectButton', 'popup_module' => 'Meetings'),
    ),

    'where' => '',


    'list_fields' => array(
        'lesson_number' => 
        array (
            'type' => 'int',
            'default' => true,
            'vname' => 'LBL_LESSON_NUMBER',
            'width' => 3,
        ),
        'till_hour' => 
        array (
            'type' => 'decimal',
            'vname' => 'LBL_TILL_HOUR',
            'width' => 3,
            'default' => true,
        ),
        'name' => 
        array (
            'name' => 'name',
            'vname' => 'LBL_LIST_SUBJECT',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => 10,
            'default' => true,
        ),
        'session_status' => 
        array (
            'type' => 'enum',
            'default' => true,
            'vname' => 'LBL_SESSION_STATUS',
            'width' => 12,
        ),
        'week_date' =>
        array (
            'type' => 'varchar',
            'vname' => 'LBL_WEEK_DATE',
            'width' => 7,
            'default' => true,
        ),
        'time_start_end' =>
        array (
            'name' => 'time_start_end',
            'vname' => 'LBL_TIME_DATE',
            'width' => 7,
            'default' => true,
            'sort_by' => 'date_start',
        ),
        'duration_cal' => 
        array (
            'type' => 'decimal',
            'vname' => 'LBL_DURATION',
            'width' => 5,
            'default' => true,
        ),
        'teaching_hour' => 
        array (
            'type' => 'decimal',
            'vname' => 'LBL_TEACHING_HOUR',
            'width' => 10,
            'default' => true,
        ),
        'teacher_name' => 
        array (
            'type' => 'relate',
            'link' => true,
            'vname' => 'LBL_TEACHER_NAME',
            'id' => 'TEACHER_ID',
            'width' => 12,
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => 'C_Teachers',
            'target_record_key' => 'teacher_id',
        ), 
        'teacher_cover_name' => 
        array (
            'type' => 'relate',
            'link' => true,
            'vname' => 'LBL_TEACHER_COVER_NAME',
            'id' => 'TEACHER_COVER_ID',
            'width' => 12,
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => 'C_Teachers',
            'target_record_key' => 'teacher_cover_id',
        ),   
        'teaching_type' => 
        array (
            'type' => 'enum',
            'studio' => 'visible',
            'vname' => 'LBL_TEACHING_TYPE',
            'width' => 10,
            'default' => true,
        ),
        'description' => 
        array (
            'type' => 'text',
            'vname' => 'LBL_DESCRIPTION',
            'sortable' => false,
            'width' => 10,
            'default' => true,
        ),
        'subpanel_button' =>
        array (
            'type' => 'varchar',
            'studio' => 'visible',
            'vname' => 'LBL_SUBPANEL_BUTTON',
            'width' => 10,
            'default' => true,
            'align' => 'center',
        ),
        'recurring_source' => 
        array (
            'usage' => 'query_only',
        ),
        'meeting_type' =>
        array (
            'usage' => 'query_only',
        ),
        'date_start' =>
        array (
            'usage' => 'query_only',
        ),
        'date_end' =>
        array (
            'usage' => 'query_only',
        ),
    ),
);
?>
