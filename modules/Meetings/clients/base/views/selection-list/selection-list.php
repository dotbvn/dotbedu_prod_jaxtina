<?php

$viewdefs['Meetings']['base']['view']['selection-list'] = array(
    'panels' => array(
        array(
            'label' => 'LBL_PANEL_1',
            'fields' => array(
                array(
                    'name' => 'name',
                    'label' => 'LBL_LIST_SUBJECT',
                    'link' => true,
                    'default' => true,
                    'enabled' => true,
                    'width' => 'xlarge',
                ),

                array(
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'default' => true,
                    'enabled' => true,
                    'sortable' => false,
                ),
                array(
                    'name' => 'status',
                    'label' => 'LBL_STATUS',
                    'default' => true,
                    'enabled' => true,
                    'sortable' => false,
                ),
                array(
                    'name' => 'date_start',
                    'label' => 'LBL_LIST_DATE',
                    'link' => false,
                    'default' => true,
                    'enabled' => true,
                    'width' => 'large',
                ),
                array(
                    'name' => 'number_of_student',
                    'label' => 'LBL_REGISTER',
                    'default' => true,
                    'enabled' => true,
                    'type' => 'html'
                ),
                array(
                    'name' => 'attended',
                    'label' => 'LBL_TAKER',
                    'default' => true,
                    'enabled' => true,
                    'type' => 'html'
                ),

                array(
                    'link' => true,
                    'type' => 'relate',
                    'label' => 'LBL_ASSIGNED_TO_NAME',
                    'id' => 'ASSIGNED_USER_ID',
                    'default' => true,
                    'name' => 'assigned_user_name',
                ),
            ),
        ),
    ),
);
