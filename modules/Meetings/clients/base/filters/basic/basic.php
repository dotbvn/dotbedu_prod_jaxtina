<?php

$viewdefs['Meetings']['base']['filter']['basic'] = array(
    'create' => true,
    'quicksearch_field' => array('name'),
    'quicksearch_priority' => 1,
    'quicksearch_split_terms' => false,
    'filters' => array(
        /**
        * Added by Hieu Pham
        * addon record for edu
        */
        array(
            'id' => 'all_records',
            'name' => 'LBL_LISTVIEW_FILTER_ALL',
            'filter_definition' => array(
                array(
                    'meeting_type' => array(
                        '$in' => array('Meeting', 'Demo', 'Placement Test'),
                    ),
                ),
            ),
            'editable' => false,
            'order' => 1,
        ),

        array(
            'id' => 'by_type_pt',
            'name' => 'LBL_MY_PT',
            'filter_definition' => array(
                array(
                    'meeting_type' => array(
                        '$in' => array('Placement Test'),
                    ),
                ),
            ),
            'editable' => false,
            'order' => 2,
            'orderBy' => array(
                    'field' => 'date_start',
                    'direction' => 'desc'
            ),
        ),

        array(
            'id' => 'by_type_demo',
            'name' => 'LBL_MY_DEMO',
            'filter_definition' => array(
                array(
                    'meeting_type' => array(
                        '$in' => array('Demo'),
                    ),
                ),
            ),
            'editable' => false,
            'order' => 2,
            'orderBy' => array(
                    'field' => 'date_start',
                    'direction' => 'desc'
            ),
        ),
        array(
            'id' => 'recently_viewed',
            'name' => 'LBL_RECENTLY_VIEWED',
            'filter_definition' => array(
                '$tracker' => '-7 DAY',
            ),
            'editable' => false,
            'order' => 6,
        ),
        array(
            'id' => 'recently_created',
            'name' => 'LBL_NEW_RECORDS',
            'filter_definition' => array(
                'date_entered' => array(
                    '$dateRange' => 'last_7_days',
                ),
            ),
            'editable' => false,
            'order' => 7,
        ),
    ),
);
