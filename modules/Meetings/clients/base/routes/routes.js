(function (app) {
    app.events.on('router:init', function (router) {
        var routes = [
            {
                // Added by Hiếu Phạm to redirect from recordView to DetailView when meeting_type = PT/Demo
                name: 'MeetingsRecord',
                route: 'Meetings/:id(/:action)',
                callback: function (id, action) {
                    // If create use side car view
                    if (id == 'create') {
                        app.controller.loadView({
                            module: 'Meetings',
                            layout: 'records'
                        });

                        app.drawer.open({
                            layout: 'create',
                            context: {
                                create: true
                            }
                        });
                    } else {
                        var preferences = {};
                        preferences['id'] = id;

                        DOTB.App.api.call('update', DOTB.App.api.buildURL('Meetings', 'get-type'), preferences, {
                            success: function (data) {
                                if (data === 'Demo' || data === 'Placement Test' || data === 'Session' ) {
                                    // Use BWC DetailView
                                    var route = app.bwc.buildRoute('Meetings', id);
                                    app.router.redirect(route);
                                } else {
                                    // Use RecordView with detail mode
                                    app.controller.loadView({
                                        module: 'Meetings',
                                        layout: 'record',
                                        action: 'detail',
                                        modelId: id
                                    });
                                }
                            },
                        });
                    }

                }
            }
        ];
        app.router.addRoutes(routes);
    });
})(DOTB.App);
