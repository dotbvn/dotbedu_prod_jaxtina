{*

*}

<style>
{literal}
.moduleTitle h2
{
    font-size: 18px;
}
{/literal}
</style>
<script type="text/javascript" src="{dotb_getjspath file='javascript/dotbcrm12.min.js' get_cache=true}"></script>
<div class="dashletPanelMenu wizard">
    <div class="bd">
            <div class="screen">
                {$MODULE_TITLE}
                <br>
                {$CONTENT}
            </div>
    </div>
</div>

<script>
{$JAVASCRIPT}
</script>