<?php

/*********************************************************************************

 * Description: Holds import setting for CSV files
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 ********************************************************************************/
 

class ImportMapCsv extends ImportMapOther
{
	/**
     * String identifier for this import
     */
    public $name = 'csv';
	/**
     * Field delimiter
     */
    public $delimiter = ',';
    /**
     * Field enclosure
     */
    public $enclosure;

    /**
     * Gets the default mapping for a module based on headers
     *
     * @param  string $module
     * @param  string $language
     * @return array field mappings
     */
    public function getMappingByLanguage($module, $language)
    {
        $mod_strings = return_module_language($language, $module);
        $bean = BeanFactory::newBean($module);
        $importable_fields = $bean->get_importable_fields();
        $mapping_arr = array();
        foreach ($importable_fields as $field_name => $defs) {
            $mapping_arr[$mod_strings[$defs['vname']]] = $field_name;
        }
        return $mapping_arr;
    }
}
?>
