<?php



$dictionary['J_Workpermit'] = array(
    'table' => 'j_workpermit',
    'audited' => true,
    'activity_enabled' => false,
    'fields' => array (
        'teacher_id' => array(
            'name' => 'teacher_id',
            'vname' => 'LBL_TEACHER_NAME',
            'type' => 'id',
            'required'=>false,
            'reportable'=>false,
            'comment' => ''
        ),
        'teacher_name' => array(
            'name' => 'teacher_name',
            'rname' => 'name',
            'id_name' => 'teacher_id',
            'vname' => 'LBL_TEACHER_NAME',
            'type' => 'relate',
            'link' => 'teacher_link',
            'table' => 'c_teachers',
            'isnull' => 'true',
            'module' => 'C_Teachers',
            'dbType' => 'varchar',
            'len' => 'id',
            'reportable'=>true,
            'source' => 'non-db',
        ),
        'teacher_link' => array(
            'name' => 'teacher_link',
            'type' => 'link',
            'relationship' => 'teachers_workpermit',
            'link_type' => 'one',
            'side' => 'right',
            'source' => 'non-db',
            'vname' => 'LBL_TEACHER_NAME',
        ),

        'license_status' => array(
            'name' => 'license_status',
            'vname' => 'LBL_LICENSE_STATUS',
            'type' => 'enum',
            'dbType' => 'varchar',
            'required' => false,
            'reportable' => true,
            'importable' => true,
            'mass_update' => true,
            'len' => 200,
            'duplicate_merge' => false,
            'options' => 'license_status_list',
        ),
        'start_date' => array(
            'name' => 'start_date',
            'vname' => 'LBL_START_DATE',
            'type' => 'date',
            'required' => false,
            'reportable' => true,
            'default_value' => '',
            'importable' => true,
            'mass_update' => false,
            'duplicate_merge' => false,
        ),
        'end_date' => array(
            'name' => 'end_date',
            'vname' => 'LBL_END_DATE',
            'type' => 'date',
            'required' => false,
            'reportable' => true,
            'default_value' => '',
            'importable' => true,
            'mass_update' => false,
            'duplicate_merge' => false,
        ),
    ),
    'relationships' => array (
    ),
    'optimistic_locking' => true,
    'unified_search' => true,
    'full_text_search' => true,
);

if (!class_exists('VardefManager')){
}
VardefManager::createVardef('J_Workpermit','J_Workpermit', array('basic','team_security','assignable','taggable','file'));
