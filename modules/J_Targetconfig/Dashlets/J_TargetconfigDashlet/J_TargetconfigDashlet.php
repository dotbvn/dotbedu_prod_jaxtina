<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/J_Targetconfig/J_Targetconfig.php');

class J_TargetconfigDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_Targetconfig/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_Targetconfig');

        $this->searchFields = $dashletData['J_TargetconfigDashlet']['searchFields'];
        $this->columns = $dashletData['J_TargetconfigDashlet']['columns'];

        $this->seedBean = new J_Targetconfig();        
    }
}