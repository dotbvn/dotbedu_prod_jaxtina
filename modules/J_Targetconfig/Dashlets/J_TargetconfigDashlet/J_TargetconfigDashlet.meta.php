<?php

/*********************************************************************************
 * Description:  Defines the English language pack for the base application.
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 
global $app_strings;

$dashletMeta['J_TargetconfigDashlet'] = array('module'		=> 'J_Targetconfig',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_Targetconfig'), 
                                          'description' => 'A customizable view into J_Targetconfig',
                                          'icon'        => 'icon_J_Targetconfig_32.gif',
                                          'category'    => 'Module Views');