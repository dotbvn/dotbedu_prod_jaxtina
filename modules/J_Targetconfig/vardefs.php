<?php



    $dictionary['J_Targetconfig'] = array(
        'table' => 'j_targetconfig',
        'audited' => false,
        'activity_enabled' => false,
        'duplicate_merge' => true,
        'fields' => array (
            'type' =>
            array (
                'required' => false,
                'name' => 'type',
                'vname' => 'LBL_TYPE',
                'type' => 'varchar',
                'len' => 255,
                'size' => '20',
                'studio' => 'visible',
                'dependency' => false,
            ),
            'by_c' =>
            array (
                'required' => false,
                'name' => 'by_c',
                'vname' => 'LBL_BY',
                'type' => 'enum',
                'default' => '',
                'no_default' => false,
                'massupdate' => 1,
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => true,
                'reportable' => true,
                'unified_search' => false,
                'merge_filter' => 'disabled',
                'calculated' => false,
                'len' => 100,
                'size' => '20',
                'options' => 'targetby_list',
                'studio' => 'visible',
                'dependency' => false,
            ),
            'year' =>
            array (
                'required' => false,
                'name' => 'year',
                'vname' => 'LBL_YEAR',
                'type' => 'enum',
                'default' => '',
                'no_default' => false,
                'massupdate' => 1,
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => true,
                'reportable' => true,
                'unified_search' => false,
                'merge_filter' => 'disabled',
                'calculated' => false,
                'len' => 10,
                'size' => '100',
                'options' => 'year_list',
                'studio' => 'visible',
                'dependency' => false,
            ),
            'frequency' =>
            array (
                'required' => false,
                'name' => 'frequency',
                'vname' => 'LBL_FREQUENCY',
                'type' => 'enum',
                'default' => '',
                'no_default' => false,
                'massupdate' => 1,
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'audited' => true,
                'reportable' => true,
                'unified_search' => false,
                'merge_filter' => 'disabled',
                'calculated' => false,
                'len' => 150,
                'size' => '20',
                'options' => 'frequency_targetconfig_list',
                'studio' => 'visible',
                'dependency' => false,
            ),
            'time_unit' =>
            array (
                'required' => true,
                'name' => 'time_unit',
                'vname' => 'LBL_TIME_UNIT',
                'type' => 'date',
                'massupdate' => false,
                'no_default' => false,
                'importable' => 'true',
                'duplicate_merge' => 'disabled',
                'duplicate_merge_dom_value' => '0',
                'reportable' => true,
                'unified_search' => false,
                'merge_filter' => 'disabled',
                'pii' => false,
                'calculated' => false,
                'size' => '20',
                'enable_range_search' => true,
            ),
            'value' =>
            array (
                'required' => false,
                'name'     => 'value',
                'vname'    => 'LBL_VALUE',
                'type'     => 'varchar',
                'len'      => 255,
            ),
            //Add Relationship Payment - Loyalty
            'loyalty_link'=>array(
                'name' => 'loyalty_link',
                'type' => 'link',
                'relationship' => 'target_loyaltys',
                'module' => 'J_Loyalty',
                'bean_name' => 'J_Loyalty',
                'source' => 'non-db',
                'vname' => 'LBL_LOYALTY',
            ),
        ),
        'relationships' => array (
            //Custom Relationship Loyalty - KPI Settings
            'target_loyaltys' => array(
                'lhs_module' => 'J_Targetconfig',
                'lhs_table' => 'j_targetconfig',
                'lhs_key' => 'id',
                'rhs_module' => 'J_Loyalty',
                'rhs_table' => 'j_loyalty',
                'rhs_key' => 'target_id',
                'relationship_type' => 'one-to-many'
            ),
        ),
        'optimistic_locking' => true,
        'unified_search' => true,
        'full_text_search' => true,
    );

    if (!class_exists('VardefManager')){
    }
    VardefManager::createVardef('J_Targetconfig','J_Targetconfig', array('basic','taggable'));
