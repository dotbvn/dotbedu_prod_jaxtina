<?php
require_once('modules/J_GradebookSettingGroup/J_GradebookSettingGroup.php');

class J_GradebookSettingGroupDashlet extends DashletGeneric { 
    public function __construct($id, $def = null)
    {
		global $current_user, $app_strings;
		require('modules/J_GradebookSettingGroup/metadata/dashletviewdefs.php');

        parent::__construct($id, $def);

        if(empty($def['title'])) $this->title = translate('LBL_HOMEPAGE_TITLE', 'J_GradebookSettingGroup');

        $this->searchFields = $dashletData['J_GradebookSettingGroupDashlet']['searchFields'];
        $this->columns = $dashletData['J_GradebookSettingGroupDashlet']['columns'];

        $this->seedBean = new J_GradebookSettingGroup();        
    }
}