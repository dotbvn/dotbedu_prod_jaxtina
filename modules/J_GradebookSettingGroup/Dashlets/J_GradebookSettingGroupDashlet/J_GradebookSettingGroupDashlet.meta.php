<?php
global $app_strings;

$dashletMeta['J_GradebookSettingGroupDashlet'] = array('module'		=> 'J_GradebookSettingGroup',
										  'title'       => translate('LBL_HOMEPAGE_TITLE', 'J_GradebookSettingGroup'), 
                                          'description' => 'A customizable view into J_GradebookSettingGroup',
                                          'icon'        => 'icon_J_GradebookSettingGroup_32.gif',
                                          'category'    => 'Module Views');