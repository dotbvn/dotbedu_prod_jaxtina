<?php



    $dictionary['J_GradebookSettingGroup'] = array(
        'table' => 'j_gradebooksettinggroup',
        'audited' => false,
        'activity_enabled' => false,
        'duplicate_merge' => true,
        'fields' => array (
            'copy_gdsetting' => array(
                'name' => 'copy_gdsetting',
                'vname' => 'LBL_COPY_GBSETTING',
                'type' => 'bool',
                'default' => '1',
            ),
            //Add Relationship GB Setting - GB Config
            'gbconfig_link'=>array(
                'name' => 'gbconfig_link',
                'type' => 'link',
                'relationship' => 'gbsettinggroup_gbconfig',
                'module' => 'J_GradebookConfig',
                'bean_name' => 'J_GradebookConfig',
                'source' => 'non-db',
                'vname' => 'LBL_GBCONFIG',
            ),
        ),
        'relationships' => array (
            //Add Relationship GB Setting - GB Config
            'gbsettinggroup_gbconfig' => array(
                'lhs_module' => 'J_GradebookSettingGroup',
                'lhs_table' => 'j_gradebooksettinggroup',
                'lhs_key' => 'id',
                'rhs_module' => 'J_GradebookConfig',
                'rhs_table' => 'j_gradebookconfig',
                'rhs_key' => 'gbsettinggroup_id',
                'relationship_type' => 'one-to-many'
            ),
        ),
        'optimistic_locking' => true,
        'unified_search' => true,
        'full_text_search' => true,
    );

    if (!class_exists('VardefManager')){
    }
    VardefManager::createVardef('J_GradebookSettingGroup','J_GradebookSettingGroup', array('basic','team_security','assignable','taggable'));
