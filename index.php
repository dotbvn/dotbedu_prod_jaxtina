<?php
//ini_set('display_errors',1);
//ini_set('display_startup_errors',0);
//error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);

if (!defined('dotbEntry')) define('dotbEntry', true);
define('ENTRY_POINT_TYPE', 'gui');
include('include/MVC/preDispatch.php');
$startTime = microtime(true);
require_once('include/entryPoint.php');
ob_start();
DotbAutoLoader::requireWithCustom('include/MVC/DotbApplication.php');
$appClass = DotbAutoLoader::customClass('DotbApplication');
$app = new $appClass();
$app->execute();
