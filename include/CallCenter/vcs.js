CallCenterHelperVCS = {
    dial: function (phoneNumber) {
        CallCenterHelper.phone = phoneNumber;
        CallCenterHelper.status = 'waiting';
        App.api.call("create", app.api.buildURL('callcenter/vcs/click2call'), {
            ext: window.callcenter_ext,
            phone: phoneNumber
        }, {
            success: function (res) {
                console.log(res);
            }
        });
    },
    init: function () {
        var socket = io.connect('https://service.dotb.cloud:3005');
        socket.emit('login', JSON.stringify({user: App.user.id, key: App.config.uniqueKey}));
        socket.on('login', res => {
            console.log(res);
        });
        socket.on('message', function (res) {
            res = JSON.parse(res);
            console.log(res.data);
            res = res.data;

            //outbound
            if (res.from === CallCenterHelper.ext) {
                var callboxId;
                callboxId = CallCenterHelper.getCallboxWithCallid(res.callid);
                if (callboxId === false) {
                    callboxId = CallCenterHelper.getCallboxWithPhoneAndDirection(res.from, 'Outbound');
                }
                if (callboxId === false) {
                    CallCenterHelper.createCallbox({phone: res.from});
                    callboxId = CallCenterHelper.getCallboxWithPhoneAndDirection(res.from, 'Outbound');
                }


                if (res.event === 'ringing') {
                    CallCenterHelper.status = 'Ringing';
                    CallCenterHelper.changeCallStatus(callboxId, 'Ringing');
                    CallCenterHelper.setData(callboxId, {
                        callId: res.callid,
                        ext: CallCenterHelper.ext,
                        source: 'VCS',
                        start_timestamp: new Date().getTime(),
                        start: CallCenterHelper.getNowUTC()
                    });
                } else if (res.event === 'connected') {
                    CallCenterHelper.status = 'Connected';
                    CallCenterHelper.changeCallStatus(callboxId, 'Connected');
                } else if (res.event === 'bye') {
                    CallCenterHelper.status = 'HangUp';
                    CallCenterHelper.changeCallStatus(callboxId, 'HangUp');
                    CallCenterHelper.setData(callboxId, {
                        end_timestamp: new Date().getTime(),
                        end: CallCenterHelper.getNowUTC(),
                        recording: '',
                        duration: parseInt((parseInt(new Date().getTime(), 10) - parseInt($('#'+callboxId).find('#start_timestamp').val(), 10)) / 1000, 10)
                    });
                    CallCenterHelper.save(callboxId);
                }
            }
        });
    }
};
