CallCenterHelper = {
    ext: '',
    secret: '',
    phone: '',
    status: '',
    getNowUTC: function () {
        var dt;
        dt = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000);
        dt = dt.toISOString();
        dt = dt.replace('T', ' ');
        dt = dt.replace('Z', '');
        dt = dt.substr(0, 19);
        return dt;
    },
    getCallboxWithPhoneAndDirection: function (phone, direction) {
        var result = false;
        $('.callbox').each(function () {
            var $this = $(this);
            if ($this.find('#phoneNumber').val() === phone && $this.find('#direction') === direction) {
                result = $this.attr('id');
            }
        });
        return result;
    },
    getCallboxWithCallid: function (callid) {
        var result = false;
        $('.callbox').each(function () {
            var $this = $(this);
            if ($this.find('#callId').val() === callid) {
                result = $this.attr('id');
            }
        });
        return result;
    },
    createCallbox: function (options) {
        options = _.extend({
            direction: 'Outbound',
            status: 'Waiting',
            phone: '',
            bean: '',
            id: ''
        }, options);
        App.view.createView({type: 'callCenter', call_direction: options.direction, call_status: options.status, data: {phoneNumber: options.phone, beanName: options.bean, beanId: options.id}});
    },
    createCallRinging: function (phone) {
        App.view.createView({type: 'callCenterRinging', data: {phoneNumber: phone}});
    },
    changeCallStatus: function (callboxid, call_status) {
        var $thisBox = $('#' + callboxid);
        $thisBox.find('.call_status_callbox').text(call_status);
        $thisBox.find('.callStatus_callbox').val(call_status);
        if (call_status === 'Connected') {
            $thisBox.find('.call_status_callbox').removeClass('label-inverse').addClass('label-success');
        } else if (call_status === 'Hangup') {
            $thisBox.find('.call_status_callbox').removeClass('label-success').addClass('label-important');
        }
    },
    setData: function (callboxid, data) {
        var $thisBox = $('#' + callboxid);
        if (data.hasOwnProperty('callId')) $thisBox.find('#callId').val(data.callId);
        if (data.hasOwnProperty('ext')) $thisBox.find('#ext').val(data.ext);
        if (data.hasOwnProperty('start')) $thisBox.find('#start').val(data.start);
        if (data.hasOwnProperty('start_timestamp')) $thisBox.find('#start_timestamp').val(data.start_timestamp);
        if (data.hasOwnProperty('source')) $thisBox.find('#source').val(data.source);
        if (data.hasOwnProperty('end')) $thisBox.find('#end').val(data.end);
        if (data.hasOwnProperty('end_timestamp')) $thisBox.find('#end_timestamp').val(data.end_timestamp);
        if (data.hasOwnProperty('recording')) $thisBox.find('#recording').val(data.recording);
        if (data.hasOwnProperty('duration')) $thisBox.find('#duration').val(data.duration);
    },
    save: function (callboxid) {
        var $thisBox = $('#' + callboxid);
        $thisBox.find('.save_callbox').prop('disabled', false);
        $thisBox.find('.save_callbox').attr('data-is-auto-save', '1').click();
    }
};
