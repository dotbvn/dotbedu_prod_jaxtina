<?php

/**
 * @author Tran Le Khanh Hong
 */


class ExtAPIClassIn
{
    /** School ID
     * It is available through the eeo.cn application
     * @var string
     */
    public $_SID = "";

    /**  Accredited institution secret key
     * @var string
     */
    public $_SECRET = "";

    /** School authentication security key
     * Fixed 32-bit all lowercase characters
     * safeKey=MD5(SECRET+timeStamp)
     * @var string
     */
    public $_safeKey = "";

    public $_currentTimeStamp = "";

    public $_COURSE_SERVER = "https://api.eeo.cn/partner/api/course.api.php";

    public $_CLOUD_SERVER = "https://api.eeo.cn/partner/api/cloud.api.php";

    public $_AppUrl = "classin://www.eeo.cn/";

    public $_enable = false;

    public static $configFilePath = "custom/modules/Connectors/connectors/sources/ext/eapm/classin/config.php";

    public function __construct()
    {
        $this->loadConfig();
        $this->_safeKey = $this->getSafeKey();
    }

    private function loadConfig()
    {
        $rowC   = $GLOBALS['db']->fetchArray("SELECT * FROM config WHERE category = 'lms_classin'");
        $config = array();
        foreach($rowC as $key => $row)
            $config[$row['name']] = $row['value'];


        $this->_SID = $config['s_id'];
        $this->_SECRET = $config['secret'];
        $this->_enable = $config['enable'] == 1;
    }

    public static function hasAPIConfig(): bool
    {
        $rowC   = $GLOBALS['db']->fetchArray("SELECT * FROM config WHERE category = 'lms_classin'");
        $config = array();
        foreach($rowC as $key => $row)
            $config[$row['name']] = $row['value'];

        return  !empty($config['s_id']) && !empty($config['secret']) && $config['enable'] == 1;
    }

    public function getSID(): string
    {
        return $this->_SID;
    }
    public function getAppUrl(): string
    {
        return $this->_AppUrl;
    }

    public function getSECRET(): string
    {
        return $this->_SECRET;
    }

    public function getEnable(): bool
    {
        return $this->_enable;
    }

    private function getSafeKey(): string
    {
        $timeStamp = time();
        $this->_currentTimeStamp = $timeStamp;
        return md5($this->_SECRET . $timeStamp);
    }

    private function getRequestUrl(string $action, string $server = "course"): string
    {
        if ($server == "course") {
            return $this->_COURSE_SERVER . '?action=' . $action;
        } else { // $server == "cloud"
            return $this->_CLOUD_SERVER . '?action=' . $action;
        }
    }

    private function sendRequest(string $action, string $method, array $arguments, string $server = "course"): string
    {
        $requestUrl = $this->getRequestUrl($action, $server);

        $requestBody = array(
            'SID' => $this->_SID,
            'safeKey' => $this->_safeKey,
            'timeStamp' => $this->_currentTimeStamp
        );

        $requestBody = array_merge($requestBody, $arguments);
        //Generate URL-encoded query string. Because content type is application/x-www-form-urlencoded
        $requestBody = http_build_query($requestBody);

        /////////////////////////// SEND REQUEST BY CURL//////////////////////////
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $requestUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $requestBody,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }


    //==== Users related interfaces ====/

    /**
     *  param addToSchoolMember (no requried)
     * 0 no action；1 add as school student；2 add as school teacher ；other no action. default=0
     */
    public function registerUsers(array $arguments): array
    {
        $thisApiAction = "register";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function registerMultipleUsers(array $arguments): array
    {
        $thisApiAction = "registerMultiple";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function modifyStudentNickname(array $arguments): array
    {
        $thisApiAction = "editSchoolStudent";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }
    public function editTeacher(array $arguments): array
    {
        $thisApiAction = "editTeacher";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function modifyUserPassword(array $arguments): array
    {
        $thisApiAction = "modifyPassword";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function addStudentIntoSchool(array $arguments): array
    {
        $thisApiAction = "addSchoolStudent";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function addTeacherIntoSchool(array $arguments): array
    {
        $thisApiAction = "addTeacher";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function stopTeacherInSchool(array $arguments): array
    {
        $thisApiAction = "stopUsingTeacher";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    // ==== Classroom related interfaces ==== //

    public function createCourse(array $arguments): array
    {
        $thisApiAction = "addCourse";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function editCourse(array $arguments): array
    {
        $thisApiAction = "editCourse";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /**
     * If there is a class under the course that has ended or is in session, it cannot be deleted。
     * When there are no classes under the course，You can call End Of The Course API。
     */
    public function deleteCourse(array $arguments): array
    {
        $thisApiAction = "delCourse";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function endCourse(array $arguments): array
    {
        $thisApiAction = "endCourse";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function createClass(array $arguments): array
    {
        $thisApiAction = "addCourseClass";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function editClass(array $arguments): array
    {
        $thisApiAction = "editCourseClass";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function deleteClass(array $arguments): array
    {
        $thisApiAction = "delCourseClass";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /* single */
    public function addStudentToCourse(array $arguments): array
    {
        $thisApiAction = "addCourseStudent";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /** multiple
     * @param array $arguments
     * @return array
     */
    public function addStudentsToCourse(array $arguments): array
    {
        $thisApiAction = "addCourseStudentMultiple";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /*
     * single
     */
    public function deleteStudentInCourse(array $arguments): array
    {
        $thisApiAction = "delCourseStudent";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /*
     * multiple
     */
    public function deleteStudentsInCourse(array $arguments): array
    {
        $thisApiAction = "delCourseStudentMultiple";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /*
     *  multiple students
     */
    public function addStudentsToClass(array $arguments): array
    {
        $thisApiAction = "addClassStudentMultiple";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /*
     * multiple students
     */
    public function deleteStudentsInClass(array $arguments): array
    {
        $thisApiAction = "delClassStudentMultiple";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    /*
     * add student to multiple classes
     */
    public function addStudentToMultipleClasses(array $arguments): array
    {
        $thisApiAction = "addCourseClassStudent";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }


    public function changeCourseTeacher(array $arguments): array
    {
        $thisApiAction = "modifyCourseTeacher";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function removeCourseTeacher(array $arguments): array
    {
        $thisApiAction = "removeCourseTeacher";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    // ==== Cloud drive related interfaces ==== //

    public function getCloudDriveFolders(): array
    {
        $thisApiAction = "getFolderList";
        $responseString = $this->sendRequest($thisApiAction, "POST", array(), "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function getCloudDriveFoldersFromSpecifiedFolder(array $arguments): array
    {
        $thisApiAction = "getCloudList";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function getTopLevelFolderID(): array
    {
        $thisApiAction = "getTopFolderId";
        $responseString = $this->sendRequest($thisApiAction, "POST", array(), "cloud");
        if(!empty($responseString)){
             $responseArray = json_decode($responseString, true);
        }

        return $responseArray;
    }

    public function uploadFile(array $arguments): array
    {
        $thisApiAction = "uploadFile";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function renameFile(array $arguments): array
    {
        $thisApiAction = "renameFile";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function deleteFile(array $arguments): array
    {
        $thisApiAction = "delFile";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function createFolder(array $arguments): array
    {
        $thisApiAction = "createFolder";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function renameFolder(array $arguments): array
    {
        $thisApiAction = "renameFolder";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function deleteFolder(array $arguments): array
    {
        $thisApiAction = "delFolder";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    public function updateAuthorizedResourceForTeacher(array $arguments): array
    {
        $thisApiAction = "updateTeacherCloudFolders";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments, "cloud");
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }

    //==== Login linked ====//

    public function getLoginClientUrl(array $arguments): array
    {
        $thisApiAction = "getLoginLinked";
        $responseString = $this->sendRequest($thisApiAction, "POST", $arguments);
        $responseArray = json_decode($responseString, true);
        return $responseArray;
    }
}
