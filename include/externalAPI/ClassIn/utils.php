<?php
/**
* @author Tran Le Khanh Hong
* ClassIn API Utils
*/

require_once('include/externalAPI/ClassIn/ExtAPIClassIn.php');

//======= User Functions =======//

/*
* Notes:
* - telephone, password is required (raw password or md5 password).
* - nickname is optional.
* - addToSchoolMember: 0 no action；1 add as school student；2 add as school teacher ；other no action. default=0
*/
function registerUser($nickname, $telephone, $password, $addToSchoolMember): array
{
    $result = array('success' => false);

    $pwdArgument = array();
    if (!empty($password))
        $pwdArgument['password'] = $password;
    //    elseif (!empty($md5pass))
    //        $pwdArgument['md5pass'] = $md5pass;

    $arguments = array(
        'telephone' => $telephone,
        'nickname' => $nickname,
        'addToSchoolMember' => $addToSchoolMember //Role
    );

    $arguments = array_merge($arguments, $pwdArgument);

    $ExtApiClassIn = new ExtAPIClassIn();
    $response = $ExtApiClassIn->registerUsers($arguments);


    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['userId'] = $response['data'];
    }

    return $result;
}

/* Example for userJson paramater in this API:
*  'userJson=
[
{
"telephone": 18516900101,
"password": 123456,
"addToSchoolMember": 1
},
{
"telephone": 18516900102,
"password": 123456,
"addToSchoolMember": 1
}
]'
* Notes: Use raw password in all of new users
*/
function registerMultipleUsers(array $users): array
{
    $result = array(
        'success' => false,
    );

    $userJson = array();

    foreach ($users as $user) {
        $userJson[] = $user;
    }

    $arguments = array(
        'userJson' => json_encode($userJson)
    );

    $ExtApiClassIn = new ExtAPIClassIn();
    $response = $ExtApiClassIn->registerMultipleUsers($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['usersId'] = $response['data'];
    }

    return $result;
}


function addStudentIntoSchool(string $telephone, string $name): array
{
    $arguments = array(
        'studentAccount' => $telephone,
        'studentName' => $name
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->addStudentIntoSchool($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    return $result;
}

function editStudentName(string $studentUid, string $studentName): array
{
    $arguments = array(
        'studentUid' => $studentUid,
        'studentName' => $studentName
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->modifyStudentNickname($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    return $result;
}

function editTeacherName(string $teacherUid, string $teacherName): array
{
    $arguments = array(
        'teacherUid' => $teacherUid,
        'teacherName' => $teacherName
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->editTeacher($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    return $result;
}

/*
* Response return data: The ID of the relationship between the school and the teacher (It is needed in the edit teacher information interface
*/
function addTeacherIntoSchool(string $telephone, string $name): array
{
    $arguments = array(
        'teacherAccount' => $telephone,
        'teacherName' => $name
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->addTeacherIntoSchool($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    return $result;
}


function stopTeacherInSchool(string $teacherUid): array
{
    $arguments = array(
        'teacherUid' => $teacherUid
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->stopTeacherInSchool($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    return $result;
}


//======= End User Functions =======//

//======= Classroom Functions =======//

/*
* optional data could be followed:
* - folderId
* - courseName
* - expiryTime
* - stamp
* - Filedata
* - courseIntroduce
* - classroomSettingId
* - mainTeacherUid
*/
function createCourse(string $courseName, array $optionalParamaters = array()): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseName' => $courseName
    );

    if (sizeof($optionalParamaters) > 0) {
        $arguments = array_merge($arguments, $optionalParamaters);
    }


    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->createCourse($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['courseId'] = $response['data']; //Created success returned course ID
    }

    return $result;
}

function editCourse(string $courseId, array $optional = array()): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId
    );
    $arguments = array_merge($arguments, $optional);

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->editCourse($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    return $result;
}

/*
* Note: if there is no class under the course, you can finish the course.
* If there are classes under the course that have not yet started, the classes that have not started will be deleted and the course will end.
*/
function endTheCourse(string $courseId): array
{
    $arguments = array(
        'courseId' => $courseId
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->endCourse($arguments);


    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];

    return $result;
}

/*
* begin time value = Unix Epoch timeStamp
* end time value = Unix Epoch timeStamp
* optional could be followed:
* folderId/seatNum/record/live/replay/assistantUid/isAutoOnstage/isHd/courseUniqueIdentity/classIntroduce/watchByLogin/allowUnloggedChat
*/
function createClass(string $courseId, string $className, string $beginTime, string $endTime, string $teacherUid, array $optional = array()): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId,
        'className' => $className,
        'beginTime' => $beginTime,
        'endTime' => $endTime,
        'teacherUid' => $teacherUid
    );

    if (sizeof($optional) > 0) {
        $arguments = array_merge($arguments, $optional);
    }

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->createClass($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['classId'] = $response['data'];
        $result['data'] = $response['more_data'];
    }

    return $result;
}

function editClassMessage(string $courseId, string $classId, array $optional): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId,
        'classId' => $classId,
    );

    $arguments = array_merge($arguments, $optional);

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->editClass($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['more_data'];
    }

    return $result;
}

function deleteClass(string $courseId, string $classId): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId,
        'classId' => $classId
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->deleteClass($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];

    return $result;
}

/*
* Note: the maximum number of spectators can be added by default to 20.
*/
function addStudentsToCourse(string $courseId, array $students, string $identity): array
{
    $result = array(
        'success' => false
    );

    $addedStudents = array();
    foreach ($students as $std) {
        $addedStudents[] = array(
            'name' => $std['name'],
            'uid' => $std['userId'],
        );
    }

    $arguments = array(
        'courseId' => $courseId,
        'identity' => $identity, //Student and audit identification(1 is student,2 is attend)
        'studentJson' => json_encode($addedStudents)
    );
    // log kết quả
    $GLOBALS['log']->fatal('Add student to course req: ', $arguments);

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->addStudentsToCourse($arguments);
    // Log kết quả
    $GLOBALS['log']->fatal('Add student to course res: ', $response);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data'];
    }

    return $result;
}

function removeStudentsInCourse(string $courseId, array $students, string $identity): array
{
    $result = array(
        'success' => false
    );

    $removedStudentsId = array();
    foreach ($students as $std) {
        $removedStudentsId[] = $std['userId'];
    }

    $arguments = array(
        'courseId' => $courseId,
        'identity' => $identity, //Student and audit identification(1 is student,2 is attend)
        'studentUidJson' => json_encode($removedStudentsId)
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->deleteStudentsInCourse($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data'];
    }

    return $result;
}

function addAStudentToCourse(string $courseId = '', string $studentUid = '', string $studentName = ""): array
{
    if(empty($courseId) || empty($studentUid))
        return array(
            'success' => false,
        );

    $arguments = array(
        'courseId' => $courseId,
        'identity' => '1', //Student and audit identification(1 is student,2 is attend),
        'studentUid' => $studentUid,
        'studentName' => $studentName
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->addStudentToCourse($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];

    return $result;
}

function removeAStudentInCourse(string $courseId = '', string $studentUid = ''): array
{
    if(empty($courseId) || empty($studentUid))
        return array(
            'success' => false,
        );

    $arguments = array(
        'courseId' => $courseId,
        'identity' => '1',
        'studentUid' => $studentUid
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->deleteStudentInCourse($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];

    return $result;
}

function addStudentsToClass(string $courseId, string $classId, array $students): array
{
    $result = array(
        'success' => false
    );

    $addedStudents = array();
    foreach ($students as $std) {
        $addedStudents[] = array(
            'name' => $std['name'],
            'uid' => $std['onl_uid'],
        );
    }

    $arguments = array(
        'courseId' => $courseId,
        'classId' => $classId,
        'identity' => '1', //Student and audit identification(1 is student,2 is attend)
        'studentJson' => json_encode($addedStudents)
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->addStudentsToClass($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data'];
    }

    return $result;
}

function addStudentToClasses(string $courseId, string $studentUid, array $classesIds): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId,
        'studentUid' => $studentUid,
        'classJson' => $classesIds
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->addStudentToMultipleClasses($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data'];
    }

    return $result;
}

/*
* Haven't single api remove a student from a class
* studentUidJson: If the array is not empty, the length of at least 1, can be multiple uids.
* Either studentUidJson or studentJson. If studentUidJson and studentJson are sent at the same time, studentUidJson shall prevail
*/
function removeStudentsInClass(string $courseId, string $classId, array $students): array
{
    $result = array(
        'success' => false
    );

    $removedStudentsId = array();
    foreach ($students as $std) {
        $removedStudentsId[] = $std['userId'];
    }

    $arguments = array(
        'courseId' => $courseId,
        'classId' => $classId,
        'identity' => '1', //Student and audit identification(1 is student,2 is attend)
        'studentUidJson' => json_encode($removedStudentsId)
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->deleteStudentsInClass($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data'];
    }

    return $result;
}


/*
* Change Course Teacher (Change teachers of all classes that under the course)
*/
function changeCourseTeacher(string $courseId, string $teacherId): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId,
        'teacherUid' => $teacherId
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->changeCourseTeacher($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data']; //Returns an array of Data information (array of Class Id)
    }

    return $result;
}

function removeCourseTeacher(string $courseId, string $teacherId): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId,
        'teacherUid' => $teacherId
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->removeCourseTeacher($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];

    return $result;
}


//======= End Classroom Functions =======//

function getLoginUrl(string $courseId, string $classId, string $uid): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'courseId' => $courseId,
        'classId' => $classId,
        'uid' => $uid
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->getLoginClientUrl($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['link'] = $response['data']; //In the website login ClassIn client links
    }

    return $result;
}

function getDriveFolderList(): array
{
    $result = array(
        'success' => false
    );

    $ExtClassInApi = new ExtAPIClassIn();
    $response = $ExtClassInApi->getCloudDriveFolders();
    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data']; //List of drive folders
    }

    return $result;
}

function getCloudListInSpecifiedFolder(string $parentFolderId = ""): array
{
    $result = array(
        'success' => false
    );

    $arguments = array();

    if (!empty($parentFolderId)) {
        $arguments['folderId'] = $parentFolderId;
    }

    $ExtClassInApi = new ExtAPIClassIn();
    $response = $ExtClassInApi->getCloudDriveFoldersFromSpecifiedFolder($arguments);
    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['folder_list'];
    }

    return $result;
}

function createNewFolder(string $folderName, string $parentId): array
{
    $result = array(
        'success' => false
    );

    $arguments = array(
        'folderId' => $parentId,
        'folderName' => $folderName
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->createFolder($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data']; // new folder id
    }

    return $result;
}

function renameFolder(string $folderId, string $folderName)
{
    $result = array('success' => false);

    $arguments = array(
        'folderId' => $folderId,
        'folderName' => $folderName
    );

    $ExtApiClassIn = new ExtApiClassIn();
    $response = $ExtApiClassIn->renameFolder($arguments);

    $result['success'] = getResponseResultHandle($response);
    $result['message'] = $response['error_info']['error'];
    $result['errorNo'] = $response['error_info']['errno'];
    if ($result['success'] == true) {
        $result['data'] = $response['data']; // new folder id
    }

    return $result;
}

//=======  Handle Returned Response =======//

function getResponseResultHandle(array $response): bool
{
    $result = false;

    switch ($response['error_info']['errno']) {
        /* for all of API '1' is Expression successful execution */
        case 1:
        case 133: /* for add teacher and add student to school API */
        case 135: /* register API: the mobile phone is registered */
        case 820: /* for register user API*/
        case 821: /* for register user API*/
        case 823: /* for delete class API */
        case 824: /* for create class API */
        case 841: /* add student to course API */
            $result = true;
            break;
        default:

    }

    return $result;
}

//=======  End Handle Returned Response =======//


//===================================== EMS Functions ==================================//

function registerClassInStudent($bean)
{
    global $dotb_config;
    //Custom nickname
    $nickname       = $bean->classin_nickname;
    $telephone      = getFormattedTelephone($bean->phone_mobile, $bean->phone_prefix);
    $password       = $GLOBALS['db']->getOne("SELECT IFNULL(value, '') value FROM config WHERE name = 'defaultportalpwd' AND category = 'pwd'");
    if(empty($password)) $password = strtolower(str_replace(' ', '', $dotb_config['brand_name'])).'123';
    $schoolMember   = 1; //1 is student role
    $responseData   = registerUser($nickname, $telephone, $password, $schoolMember);
    if ($responseData['success'] == true) {
        $onl_uid    = $responseData['userId'];
        //save UID
        $GLOBALS['db']->query("UPDATE {$bean->table_name} SET onl_uid = '{$onl_uid}' WHERE id = '{$bean->id}' ");

        //TH nếu SDT đã được đăng ký thì gọi thêm API add student to school
        if ($responseData['errorNo'] == 135) {
            $addResponse = addStudentIntoSchool($telephone, $nickname);
            // Cần update nickname của nó lại theo bean hiện tại
            if($addResponse['success']) editStudentName($onl_uid, $nickname);
        }
        return true;
    }
    return false;
}

// Nick name Class In
//[<fisrt_name>-<center_legal>] last_name
function formatCINickName($bean): string
{
    $firstName = $bean->first_name;
    $lastName = $bean->last_name;
    $centerCode = "";
    global $db;
    $result = $db->query("SELECT legal_name FROM teams WHERE id = '{$bean->team_id}' LIMIT  1");
    if ($row = $db->fetchByAssoc($result)) {
        $centerCode = $row['legal_name'];
    }
    $outputArray = array();
    $matchResult = preg_match_all('/([a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ])[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]*/u', $lastName, $outputArray);
    $result = "[" . $firstName . "-" . $centerCode . "] ";
    if ($matchResult) {
        $result .= $outputArray[0][0] . " ";
        for ($i = 1; $i < sizeof($outputArray[1]); $i++) {
            //Kiểm tra phụ âm đơn hay kép
            $findResult = preg_match_all('/(Ch|Gh|Gi|Kh|Ng|Ngh|Nh|Th|Tr|Qu)/m', $outputArray[0][$i], $outArr);
            if ($findResult){ //kép
                $result .= $outArr[0][0];
            }else{ //đơn
                $result .= $outputArray[1][$i];
            }
            if ($i != sizeof($outputArray[1]) - 1){
                $result .= ' ';
            }
        }
    }
    return $result;
}

function registerClassInTeacher($bean, $isTeacher = false): bool{
    global $dotb_config;
    $nickname = $bean->name;
    $telephone = getFormattedTelephone($bean->phone_mobile, $bean->phone_prefix);
    $password = $GLOBALS['db']->getOne("SELECT IFNULL(value, '') value FROM config WHERE name = 'defaultteacherpwd' AND category = 'pwd'");
    if(empty($password)) $password = strtolower(str_replace(' ', '', $dotb_config['brand_name'])).'123';
    $schoolMember = $isTeacher ? 2 : 1; //2 is teacher role; 1 Auditor is a student
    $response = registerUser($nickname, $telephone, $password, $schoolMember);
    if ($response['success'] == true) {
        $teacher_uid = $response['userId'];
        //save UID
        $GLOBALS['db']->query("UPDATE {$bean->table_name} SET onl_uid = '{$teacher_uid}' WHERE id = '{$bean->id}' ");
        if($schoolMember == 2)
            $res2 = addTeacherIntoSchool($telephone, $nickname);
        return true;
    }
    return false;
}

function stopClassInTeacher($bean, $deleteBean = true): bool
{
    $response = stopTeacherInSchool($bean->onl_uid);
    if ($response['success'] == true) {
        //Remove teacher uid from db
        if ($deleteBean)
            $GLOBALS['db']->query("UPDATE {$bean->table_name} SET onl_uid = '' WHERE id = '{$bean->id}' ");
        return true;
    } else {
        return false;
    }
}

function createClassInCourse($bean): bool
{
    $classInAPI = new ExtAPIClassIn();
    $courseSettings = getClassInCourseSettings($bean->id);
    $retRes = false;

    //Step 1: Create new folder (If don't select exists folder)
    if ($courseSettings['classin_folder_option'] === '1' && empty($courseSettings['classin_folder_id'])) {
        //Get Root FolderID
        $res = $classInAPI->getTopLevelFolderID();
        $parentFolderId = $res['data'];//Top-level folder id
        $folderRes = createNewFolder($bean->name, $parentFolderId);
        if ($folderRes['success']) {
            //Update ClassIn Folder Id to db settings
            $courseSettings['classin_folder_id'] = $folderRes['data'];
            $courseSettings['classin_folder_name'] = $bean->name;
            saveClassInCourseSettings($courseSettings, $bean->id);
        }
    }

    //Step 2: Create course
    $configOptional = array();
    if(!empty($courseSettings['classin_folder_id']))
        $configOptional['folderId'] = $courseSettings['classin_folder_id'];

    if(!empty($courseSettings['classin_head_teacher']))
        $configOptional['mainTeacherUid'] = $courseSettings['classin_head_teacher'];

    $createCourseRes = createCourse($bean->name, $configOptional);
    $courseId = "";
    if ($createCourseRes['success'] == true) {
        $courseId = $createCourseRes['courseId'];
        //Save courseId
        $GLOBALS['db']->query("UPDATE j_class SET onl_course_id = '{$courseId}' WHERE  id = '{$bean->id}' ");

        $retRes = true;
    }

    //Step 3: Add auditors to course
    if (!empty($courseSettings['classin_auditors']) && $retRes) {
        $auditors = array();
        foreach ($courseSettings['classin_auditors'] as $auditor) {
            $auditorName = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid='{$auditor}'");
            $auditors[] = array(
                'userId' => $auditor,
                'name' => $auditorName
            );
        }
        addStudentsToCourse($courseId, $auditors, '2');
    }

    return $retRes;
}

function addStudentsToClassInCourse(string $emsClassId): void
{
    $courseId = getClassInCourseIdByEMSClassId($emsClassId);
    $students = getStudentListInClass($emsClassId);
    foreach ($students as $student) {
        $std = BeanFactory::getBean($student['student_type'], $student['id'], array('disable_row_level_security' => true));
        $std->classin_nickname = formatCINickName($std);
        if (empty($std->onl_uid)){  //Tạo tài khoản Class In
            $res = registerClassInStudent($std);
            $std->onl_uid = getClassInUIDByEMSBeanId($student['student_type'], $student['id']);
        }
        if(!empty($courseId) && !empty($std->onl_uid)){
            $addRes = addAStudentToCourse($courseId, $std->onl_uid, $std->classin_nickname);
            if($addRes || $addRes['errorNo'] == 163) $GLOBALS['db']->query("UPDATE j_classstudents SET onl_enrolled = 1 WHERE student_id = '{$student['id']}' AND class_id = '{$emsClassId}' AND deleted = 0 ");
        }
    }
}

function updateStudentsInEMSClassToClassInCourseHandle(string $emsClassId)
{
    //Get removed students
    $removed = getRemovedStudentListInClass($emsClassId);
    $courseId = getClassInCourseIdByEMSClassId($emsClassId);
    if (sizeof($removed) > 0){
        $stdList = array();
        foreach ($removed as $r){
            $stdList[] = array('userId' => $r['onl_uid']);
        }

        if (sizeof($stdList) > 20) {
            while (sizeof($stdList) > 20) {
                $stdListTemp1 = array_slice($stdList, 0, 20);
                $stdList = array_slice($stdList, 20);
                removeStudentsInCourse($courseId, $stdListTemp1, '1');
            }
            removeStudentsInCourse($courseId, $stdList, '1');
        } else {
            removeStudentsInCourse($courseId, $stdList, '1');
        }
    }

    // Add students
    addStudentsToClassInCourse($emsClassId);
}

/* add single student */
function addStudentToClassInCourse(string $emsClassId, $studentBean)
{
    $courseId = getClassInCourseIdByEMSClassId($emsClassId);
    $studentUid = $studentBean->onl_uid;
    if(empty($studentUid)) {
        //register new ClassIn account and add student to the school
        registerClassInStudent($studentBean);
        $studentUid = getClassInUIDByEMSBeanId($studentBean->module_name, $studentBean->id); //Lấy lại Uid mới đã được register
        if(empty($studentUid)) return false;
    }
    $response = addAStudentToCourse($courseId, $studentUid, $studentBean->classin_nickname);
    if($response || $response['errorNo'] == 163) $GLOBALS['db']->query("UPDATE j_classstudents SET onl_enrolled = 1 WHERE student_id = '{$studentBean->id}' AND class_id = '{$emsClassId}' AND deleted = 0 ");
    return $response['success'];
}

/* remove single */
function removeStudentFromClassInCourse(string $emsClassId, string $beanType, string $beanId): bool
{
    $courseId = getClassInCourseIdByEMSClassId($emsClassId);
    $studentUid = getClassInUIDByEMSBeanId($beanType, $beanId);
    if(!empty($courseId) && !empty($studentUid)){
        $response = removeAStudentInCourse($courseId, $studentUid);
        return $response['success'];
    }else return false;
}

function createClassInLesson($bean): bool{
    global $timedate;
    $today = $timedate->nowDbDate();
    $result = true;

    // If class online learning active status is active, we will create lesson
    if (getClassActiveClassInStatus($bean->ju_class_id) == false || !ExtAPIClassIn::hasAPIConfig()){
        $bean->creator = "LBL_API_ERR_CREATE3";
        $result = false;
    }

    // Chưa schedule room cho lesson
    if (empty($bean->room_id)){
        $bean->creator = 'LBL_API_ERR_CREATE2';
        $result = false;
    }

    $teacherUid = getClassInTeacherUidByEMSRoomId($bean->room_id);
    if(empty($teacherUid)){
        $bean->creator = 'LBL_API_ERR_CREATE2';
        $result = false;
    }

    //Update bằng query để hạn chế rủi ro
    if(!$result){
        $GLOBALS['db']->query("UPDATE meetings SET creator='{$bean->creator}' WHERE id = '{$bean->id}'");
        return $result;
    }

    // Create new ClassIn lesson ( class )
    $class = $GLOBALS['db']->fetchOne("SELECT IFNULL(onl_course_id, '') onl_course_id, IFNULL(name, '') name, IFNULL(onl_setting, '') onl_setting FROM j_class WHERE id = '{$bean->ju_class_id}' AND deleted = 0 LIMIT  1");
    $courseId = $class['onl_course_id'];
    // Lấy tên của Lớp (Class) để tạo ClassIn lesson
    // BỞI VÌ để fix trường hợp đổi tên lớp thì các meetings được không đổi tên theo
    $className = $class['name'] . ' - ' . $bean->lesson_number;

    try {
        $beginTime  = $timedate->asUserTs(new DateTime($bean->date_start));
        $endTime    = $timedate->asUserTs(new DateTime($bean->date_end));
    } catch (Exception $e) {
        $beginTime  = strtotime("+7 hours " . $bean->date_start);
        $endTime    = strtotime("+7 hours " . $bean->date_end);
        $GLOBALS['log']->fatal("API: ClassIn Datetime Error: beginTime: $beginTime | endTime: $endTime | {$e->getMessage()}");
    }
    //Get settings
    $settings = json_decode(html_entity_decode($class['onl_setting']), true);
    $optionalSettings = array(
        'record' => $settings['classin_recording'],
        'seatNum' => $settings['classin_on_stage'],
    );
    if (!empty($settings['classin_assistant'])) {
        $optionalSettings['assistantUid'] = $settings['classin_assistant'];
    }
    //API CREATE CLASS
    $response = createClass($courseId, $className, $beginTime, $endTime, $teacherUid, $optionalSettings);
    if ($response['success'] == true) {
        // Save classId
        $bean->external_id = $response['classId'];//lesson

        // Get Client Login URL
        $ExtApiClassIn  = new ExtApiClassIn();
        $schoolId       = $ExtApiClassIn->_SID;
        $AppUrl         = $ExtApiClassIn->_AppUrl;
        $bean->join_url = $AppUrl."enterclass?classId={$bean->external_id}&courseId=$courseId&schoolId={$schoolId}";

        $result = true;
        $bean->creator = ''; //Clear error
    } else {
        $EnglishMessage = getClassInErrorMessage($response['errorNo']);
        $message = empty($EnglishMessage) ? $response['message'] : $EnglishMessage;
        $bean->creator = 'API ERROR: ' . $message; //Error message
    }
    //Update bằng query để hạn chế rủi ro
    $GLOBALS['db']->query("UPDATE meetings SET
        external_id='{$bean->external_id}',
        type='{$bean->type}',
        join_url='{$bean->join_url}',
        creator='{$bean->creator}' WHERE id = '{$bean->id}'");

    return $result;
}

//Trường hợp này edit room_id(teacher_uid)
// Tạm thời ko thể xoá teacher cũ khỏi course được bởi vì ko biết được teacher đó có đang được chọn làm teacher cho lesson khác trong course đó hay ko
function updateClassInLesson($bean): bool
{
    $result = false;

    if (getClassActiveClassInStatus($bean->ju_class_id) && ExtAPIClassIn::hasAPIConfig()) {
        $courseId = getClassInCourseIdByEMSClassId($bean->ju_class_id);
        $teacherUid = getClassInTeacherUidByEMSRoomId($bean->room_id);
        $optionalSettings = array(
            'teacherUid' => $teacherUid,
        );
        $response = editClassMessage($courseId,$bean->external_id, $optionalSettings);
        if ($response['success']){
            $result = true;
        }
    }

    return $result;
}

function deleteClassInLesson($bean): bool
{
    $result = false;
    $courseId = getClassInCourseIdByEMSClassId($bean->ju_class_id);
    $classId = $bean->external_id;
    $response = deleteClass($courseId, $classId);
    $bean->external_id = '';
    $bean->join_url = '';
    if ($response['success'] == true) {
        $result = true;
    } else {
        $EnglishMessage = getClassInErrorMessage($response['errorNo']);
        $bean->creator = 'API ERROR: ' . empty($EnglishMessage) ? $response['message'] : $EnglishMessage;
    }
    //Update bằng query để hạn chế rủi ro
    $GLOBALS['db']->query("UPDATE meetings SET
        external_id='{$bean->external_id}',
        type='{$bean->type}',
        join_url='{$bean->join_url}',
        creator='{$bean->creator}' WHERE id = '{$bean->id}'");

    return $result;
}

//Edit course name of course head teacher
function updateClassInCourse($classBean): bool
{
    $courseId = getClassInCourseIdByEMSClassId($classBean->id);
    $courseSettings = getClassInCourseSettings($classBean->id);
    $changeData = array(
        'courseName' => $classBean->name
    );
    if (!empty($courseSettings['classin_head_teacher'])) {
        $changeData['mainTeacherUid'] = $courseSettings['classin_head_teacher'];
    }
    if (!empty($courseSettings['classin_folder_id'])) {
        $changeData['folderId'] = $courseSettings['classin_folder_id'];
    }
    $response = editCourse($courseId, $changeData);
    if ($response['success'] == true) {
        return true;
    }
    return false;
}

/* Note: all classes not started under the course are changed to this teacher. */
function changeClassInAllClassTeacherHandle($classBean): bool
{
    $teacherUid = getClassInTeacherUidByEMSRoomId($classBean->room_id);
    $courseId = getClassInCourseIdByEMSClassId($classBean->id);
    $response = changeCourseTeacher($courseId, $teacherUid);
    return $response['success'];
}

function updateClassInCourseAssistants(string $classId, string $assistantUid){
    if(empty($assistantUid)) return false;
    $courseId = getClassInCourseIdByEMSClassId($classId);
    $result = [];
    // Lấy những buổi học đã sinh ra trên ClassIn và chưa kết thúc để thay đổi assistant
    $rows = getClassInUnfinishedLessonsInCourse($classId);
    foreach ($rows as $row){
        $optional = [
            'assistantUid'   => $assistantUid
        ];
        $response = editClassMessage($courseId, $row['external_id'], $optional);
        $result[$row['id']] = $response;
    }
    return $result;
}

function updateClassInCourseAuditors(string $classId, array $beforeAuditors, array $afterAuditors){
    $courseId = getClassInCourseIdByEMSClassId($classId);

    $auditors = array();
    //Remove old auditors from course
    foreach ($beforeAuditors as $auditor) {
        if(!empty($auditor))
            $auditors[] = array('userId' => $auditor);
    }
    //Không xét trường hợp 20 bởi vì auditors không thêm quá 20 người
    if (sizeof($auditors)>0) removeStudentsInCourse($courseId, $auditors, '2');

    //Add new auditor to course
    $auditors = [];
    foreach ($afterAuditors as $auditor) {
        if(!empty($auditor)){
            $auditorName = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid = '{$auditor}'");
            $auditors[] = array(
                'userId' => $auditor,
                'name' => $auditorName
            );
        }

    }
    if (sizeof($auditors) > 0) addStudentsToCourse($courseId, $auditors, '2');
}

// xử lý cập nhật thay đổi sdt của học sinh trong các course ClassIn
// remove sdt cũ khỏi course, đồng thời thêm sdt mới vào course
// remove sdt cũ khỏi các lesson đã tạo ClassIn, đồng thời thêm sdt mới vào các lesson đó
function updateStudentPhoneInClassIn($bean, $newUid = '', $oldUid = ''){
    // find the classes (courses) the student attends
    $classes = getClassesStudentAttends($bean->id);
    foreach ($classes as $class){
        $courseId = $class['classin_course_id'];
        // remove old uid from course
        removeAStudentInCourse($courseId, $oldUid);
        // add new uid to course
        addAStudentToCourse($courseId, $newUid, $bean->classin_nickname);
    }
}

//======= Help Functions =======//

/*
* Formatted according to ClassIn
* Format: 00 country number - mobile phone number;
* note: mobile phone Numbers in mainland China do not include country.
* Either studentUid or studentUid. If studentAccount is passed to studentUid at the same time, the studentUid parameter shall prevail
*/
function getFormattedTelephone(string $telephone, string $countryCode = '84' ): string
{
    // Only support Vietnamese phone mobile number +84
    if(empty($countryCode)) $countryCode = '84';
    if ($telephone[0] == '0') {
        $telephone = substr($telephone, 1);
        $telephone = '00' . $countryCode . '-' . $telephone;
    } else {
        $telephone = '00' . $countryCode . '-' . $telephone;
    }
    return $telephone;
}

/**
* @param string $classId
* @return array
*  an elemenet of returned array may include contact_id, name, phone_mobile, onl_uid
*/
function getStudentListInClass(string $classId): array{
    $students = array();
    $query = "SELECT c.id, c.full_student_name as full_name, c.first_name first_name, c.last_name last_name, c.team_id team_id, c.phone_mobile, c.onl_uid, 'Contacts' as student_type, cs.onl_enrolled, c.phone_prefix FROM j_classstudents cs INNER JOIN contacts c ON cs.student_id = c.id AND c.deleted = 0 WHERE cs.class_id = '{$classId}' AND cs.deleted = 0
    UNION SELECT l.id, l.full_lead_name as full_name, l.first_name first_name, l.last_name last_name, l.team_id team_id, l.phone_mobile, l.onl_uid, 'Leads' as student_type, cs.onl_enrolled, l.phone_prefix FROM j_classstudents cs INNER JOIN leads l ON cs.student_id = l.id AND l.deleted =  0 WHERE cs.class_id = '{$classId}' AND cs.deleted = 0 ";
    $result = $GLOBALS['db']->query($query);
    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
        $students[] = array(
            'id'            => $row['id'],
            'name'          => $row['full_name'],
            'phone_mobile'  => $row['phone_mobile'],
            'onl_uid'       => $row['onl_uid'],
            'student_type'  => $row['student_type'],
            'enrolled'      => $row['onl_enrolled']
        );
    }
    return $students;
}

function getRemovedStudentListInClass(string $classId): array
{
    $students = array();
    global $db;

    $query = "SELECT c.id, c.full_student_name as full_name, c.phone_mobile, c.onl_uid, 'Contacts' as student_type FROM j_classstudents cs INNER JOIN contacts c ON cs.student_id = c.id AND c.deleted = 0 WHERE cs.class_id = '{$classId}' AND cs.deleted = 1
    UNION
    SELECT l.id , l.full_lead_name as full_name, l.phone_mobile, l.onl_uid, 'Leads' as student_type FROM j_classstudents cs INNER JOIN leads l ON cs.student_id = l.id AND l.deleted  0 WHERE cs.class_id = '{$classId}' AND cs.deleted = 1 ";
    $result = $db->query($query);

    while ($row = $db->fetchByAssoc($result)) {
        $students[] = array(
            'id' => $row['id'],
            'name' => $row['full_name'],
            'phone_mobile' => $row['phone_mobile'],
            'onl_uid' => $row['onl_uid'],
            'student_type' => $row['student_type']
        );
    }
    return $students;
}

function getStudentsInMeeting(string $meetingId): array
{
    $students = array();
    global $db;
    $query = "SELECT c.id, c.full_student_name, c.phone_mobile, c.onl_uid FROM meetings_contacts m JOIN contacts c ON m.contact_id = c.id WHERE m.deleted = 0 AND c.deleted = 0 AND m.meeting_id = '{$meetingId}'";
    $result = $db->query($query);
    while ($row = $db->fetchByAssoc($result)) {
        $students[] = array(
            'id' => $row['id'],
            'full_name' => $row['full_student_name'],
            'phone_mobile' => $row['phone_mobile'],
            'onl_uid' => $row['onl_uid'],
            'student_type' => 'Contacts',
            'name' => formatCINickName(BeanFactory::getBean('Contacts', $row['id'], array('disable_row_level_security'=>true)))
        );
    }

    return $students;
}

function getLeadsInMeeting(string $meetingId): array
{
    $students = array();
    global $db;

    $query = "SELECT l.id, l.full_lead_name, l.phone_mobile, l.onl_uid FROM meetings_leads m
    JOIN leads l ON m.lead_id = l.id
    WHERE m.meeting_id ='{$meetingId}' AND m.deleted <> 1 AND l.deleted <> 1";

    $result = $db->query($query);
    while ($row = $db->fetchByAssoc($result)) {
        $students[] = array(
            'id' => $row['id'],
            'full_name' => $row['full_lead_name'],
            'phone_mobile' => $row['phone_mobile'],
            'onl_uid' => $row['onl_uid'],
            'student_type' => 'Leads',
            'name' => formatCINickName(BeanFactory::getBean('Leads', $row['id'], array('disable_row_level_security'=>true)))
        );
    }

    return $students;
}

function getClassInCourseIdByEMSClassId(string $classId){
    return $GLOBALS['db']->getOne("SELECT onl_course_id FROM j_class WHERE id = '{$classId}' AND deleted = 0 LIMIT  1");
}

function getClassInTeacherUidByEMSRoomId(string $roomId){
    return $GLOBALS['db']->getOne("SELECT onl_uid FROM c_rooms WHERE id = '{$roomId}' AND deleted =0 LIMIT  1");
}

function getClassInUIDByEMSBeanId(string $beanType, string $id){
    return $GLOBALS['db']->getOne("SELECT onl_uid FROM ".strtolower($beanType)." WHERE id='{$id}' AND deleted = 0");
}

/*
* Default message is Chinese. This function will return English message.
*/
function getClassInErrorMessage(int $errorNo): string
{
    $ENErrorMessageArr = include('include/externalAPI/ClassIn/ClassInErrorMessage.php');

    $message = $ENErrorMessageArr[$errorNo];

    return empty($message) ? "" : $message;
}

function getFolderIdFromKindOfCourseDocUrl(string $kocId, string $levelCode): string
{
    global $db;
    $result = '';
    $contentString = $db->getOne("SELECT content FROM j_kindofcourse WHERE deleted = 0 AND id = '${kocId}' LIMIT 1");
    $contentArr = json_decode(html_entity_decode($contentString), true);
    foreach ($contentArr as $content) {
        if ($content['level_code'] == $levelCode) {
            $result = $content['doc_url'];
            break;
        }
    }
    return $result;
}

function getClassInCourseSettings(string $classId): array
{
    $settings = $GLOBALS['db']->getOne("SELECT onl_setting FROM j_class WHERE id = '{$classId}' ");
    $settingsArr = json_decode(html_entity_decode($settings), true);
    return $settingsArr;
}

function saveClassInCourseSettings(array $settings, string $classId)
{
    $settingsString = json_encode($settings);
    $GLOBALS['db']->query("UPDATE j_class SET onl_setting = '{$settingsString}' WHERE id  = '{$classId}'");
}

function getEMSContactsIdByClassInUID(string $uid): array
{
    $result = array(
        'id' => '',
        'type' => ''
    );
    global $db;

    //Contacts
    $res = $db->getOne("SELECT id FROM contacts WHERE onl_uid = '{$uid}' AND deleted = 0 LIMIT 1 ");
    if ($res) {
        $result['id'] = $res;
        $result['type'] = 'Contacts';
    } else {
        //Leads
        //        $res = $db->getOne("SELECT id FROM leads WHERE onl_uid = '{$uid}' AND deleted = 0 LIMIT 1 ");
        //        if ($res){
        //            $result['id'] = $res;
        //            $result['type'] = 'Leads';
        //        }
    }

    return $result;
}

function getEMSMeetingIdByClassInClassId(string $classId)
{
    global $db;
    $res = $db->getOne("SELECT id FROM meetings WHERE external_id = '{$classId}' AND deleted <> 1 AND (IFNULL(session_status, '') <> 'Cancelled' OR (IFNULL(session_status, '') IS NULL
        AND 'Cancelled' IS NOT NULL)) LIMIT 1");
    if ($res) {
        return $res;
    }
    return "";
}

function getClassActiveClassInStatus(string $classId): bool
{
    $res = $GLOBALS['db']->getOne("SELECT onl_status as status FROM j_class WHERE id = '{$classId}'");
    return $res === '1';
}

// Lấy những lớp có hs này tham gia, lớp đã có tạo course bên ClassIn và phải là lớp chưa kết thúc
function getClassesStudentAttends(string $studentId): array
{
    $query = "SELECT DISTINCT
    IFNULL(l2.id, '') class_id,
    IFNULL(l2.onl_course_id, '') classin_course_id
    FROM contacts INNER JOIN j_classstudents l1 ON contacts.id = l1.student_id AND l1.deleted = 0
    INNER JOIN j_class l2 ON l1.class_id = l2.id AND l2.deleted = 0
    WHERE (contacts.id = '$studentId') AND (IFNULL(l2.status,'') <> 'Finished') AND contacts.deleted = 0 AND (IFNULL(l2.onl_course_id,'') <> '')";
    $retArray = $GLOBALS['db']->fetchArray($query);
    return $retArray;
}

// Lấy những buổi học đã sinh ra trên ClassIn và chưa kết thúc
function getClassInUnfinishedLessonsInCourse($emsClassId):array
{
    $todayTime = $GLOBALS['timedate']->nowDb();
    $q1 = "SELECT id, external_id FROM meetings
    WHERE ju_class_id = '{$emsClassId}' AND deleted = 0 AND date_end > '{$todayTime}' AND external_id IS NOT NULL AND external_id <> '' ";
    $rows = $GLOBALS['db']->fetchArray($q1);
    return $rows;
}

//======= End Help Functions =======//
