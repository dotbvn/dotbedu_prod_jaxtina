<?php

require_once 'vendor/Zend/Gdata/Contacts.php';
//Add By Lap Nguyen
require_once("custom/include/GoogleAPI/vendor/autoload.php");
use DateTime;

/**
* ExtAPIGoogle
*/
class ExtAPIGoogle extends ExternalAPIBase implements WebDocument
{
    public $supportedModules = array('Meetings');
    public $authMethod = 'oauth2';
    public $connector = 'lms_google';

    public $useAuth = true;
    public $requireAuth = true;

    protected $scopes = array(
        //        'https://www.googleapis.com/auth/contacts.readonly',
        //        Google_Service_Drive::DRIVE_READONLY,
        //        Google_Service_Drive::DRIVE_FILE,
        Google_Service_Calendar::CALENDAR
    );

    public $docSearch = true;
    public $needsUrl = false;
    public $sharingOptions = null;

    const APP_STRING_ERROR_PREFIX = 'ERR_GOOGLE_API_';

    public function getClient()
    {
        $client = $this->getGoogleClient();
        $eapm = EAPM::getLoginInfo('Google',true, $this->host_user_id);
        if ($eapm && !empty($eapm->api_data)) {
            $client->setAccessToken($eapm->api_data);
            if ($client->isAccessTokenExpired()) {
                $this->refreshToken($client);
            }
        }

        return $client;
    }

    protected function refreshToken(Google_Client $client){
        /** @var Google_Auth_OAuth2 $auth Edit by Lap Nguyen*/
        if ($client->getRefreshToken()) {
            try {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } catch (Google_Auth_Exception $e) {
                $GLOBALS['log']->fatal($e->getMessage());
                return;
            }
            $token = $client->getAccessToken();
            if (!empty($token)){
                $this->saveToken($token);
            }
        }
    }

    protected function saveToken($accessToken){
        global $current_user;
        $bean = EAPM::getLoginInfo('Google', true, $this->host_user_id);
        if (!$bean) {
            $bean = BeanFactory::newBean('EAPM');
            if(!empty($this->host_user_id))
                $bean->assigned_user_id = $this->host_user_id;
            else
                $bean->assigned_user_id = $current_user->id;
            $bean->application = 'Google';
            $bean->validated = true;
        }

        $bean->api_data = json_encode($accessToken);
        $bean->save();
    }

    public function revokeToken(){
        $this->host_user_id = $_POST['assigned_user_id'];
        if(empty($this->host_user_id)) $this->host_user_id = $_POST['return_id'];
        $client = $this->getClient();

        try {
            $client->revokeToken();
        } catch (Google_Auth_Exception $e) {
            return false;
        }

        $eapm = EAPM::getLoginInfo('Google', true, $this->host_user_id);
        if ($eapm) {
            $eapm->mark_deleted($eapm->id);
        }

        return true;
    }

    protected function getGoogleClient(){
        $config = $this->getGoogleOauth2Config();

        $client = new Google_Client();
        $client->setClientId($config['oauth2_client_id']);
        $client->setClientSecret($config['oauth2_client_secret']);
        $client->setRedirectUri($config['redirect_uri']);
        $client->setApprovalPrompt($config['approval_prompt']);

        $client->setAccessType('offline');
        $client->setScopes($this->scopes);

        return $client;
    }

    public function getGoogleOauth2Config(){
        $config = array();

        //Load config
        $rowC   = $GLOBALS['db']->fetchArray("SELECT * FROM config WHERE category = 'lms_google'");
        $config = array();
        foreach($rowC as $key => $row) $config[$row['name']] = $row['value'];

        $config['redirect_uri'] = rtrim(DotbConfig::getInstance()->get('site_url'), '/')
        . '/index.php?module=EAPM&action=GoogleOauth2Redirect';

        //Set default calendar_id
        if(empty($config['calendar_id']))
            $config['calendar_id'] = 'primary';
        //always request refresh token - Lap Nguyen
        $config['approval_prompt'] = 'force';
        return $config;
    }

    public function hasAPIConfig($config){
        if(empty($config)) return false;
        if( !$config['enable']
            || empty($config['oauth2_client_id'])
            || empty($config['oauth2_client_secret'])
            || empty($config['calendar_id'])) return false;
        return true;
    }

    public function authenticate($authCode){
        $client = $this->getClient();
        try {
            // $client->authenticate($code);
            $client->fetchAccessTokenWithAuthCode($authCode);
        } catch (Google_Auth_Exception $e) {
            $GLOBALS['log']->fatal($e->getMessage());
            return false;
        }

        $token = $client->getAccessToken();
        if ($token)
            $this->saveToken($token);
        return $token;
    }

    public function uploadDoc($bean, $fileToUpload, $docName, $mimeType){
        $client = $this->getClient();
        $service = new Google_Service_Drive($client);

        $file = new Google_Service_Drive_DriveFile($client);
        $file->setTitle($docName);
        $file->setDescription($bean->description);

        try {
            $createdFile = $service->files->insert($file, array(
                'data' => file_get_contents($fileToUpload),
                'uploadType' => 'multipart'
            ));
        } catch (Google_Exception $e) {
            return array(
                'success' => false,
                'errorMessage' => $GLOBALS['app_strings']['ERR_EXTERNAL_API_SAVE_FAIL'],
            );
        }

        $bean->doc_id = $createdFile->id;
        $bean->doc_url = $createdFile->alternateLink;

        return array(
            'success' => true,
        );
    }

    public function downloadDoc($documentId, $documentFormat)
    {
    }

    public function deleteDoc($documentId)
    {
    }

    public function shareDoc($documentId, $emails)
    {
    }

    public function searchDoc($keywords, $flushDocCache = false)
    {
        global $dotb_config;

        $client = $this->getClient();
        $drive = new Google_Service_Drive($client);

        $options = array(
            'maxResults' => $dotb_config['list_max_entries_per_page']
        );

        $queryString = "trashed = false ";
        if (!empty($keywords)) {
            $queryString .= "and title contains '{$keywords}'";
        }
        $options['q'] = $queryString;

        try {
            $files = $drive->files->listFiles($options);
        } catch (Google_Exception $e) {
            $GLOBALS['log']->fatal('Unable to retrieve google drive files:' .  $e);
            return false;
        }

        $results = array();
        foreach ($files as $file) {
            $results[] = array(
                'url' => $file->alternateLink,
                'name' => $file->title,
                'date_modified' => $file->modifiedDate,
                'id' => $file->id
            );
        }

        return $results;
    }

    //Create Gooogle Event
    public function createEvent(&$bean, $param){
        global $timedate;
        $this->host_user_id = $param['host_user_id'];

        $client = $this->getClient();
        $config = $this->getGoogleOauth2Config();
        $calendarId = $config['calendar_id'];
        if(!$this->hasAPIConfig($config)){
            return array(
                'success' => 0,
                'errorCode' => "API error: No config!",
            );
        }

        $service = new Google_Service_Calendar($client);
        $conferenceData = array(
            'createRequest' => array(
                'requestId' => $bean->id,
                'conferenceSolutionKey' => array(
                    'type' => 'hangoutsMeet'
                )
            )
        );

        $startTime  = date("c", strtotime("+7 hours ".$bean->date_start));
        $endTime    = date("c", strtotime("+7 hours ".$bean->date_end));

        $event = new Google_Service_Calendar_Event(array(
            'summary' => $bean->name,
            'location' => $bean->team_name,
            'description' => $bean->description,
            'start' => array(
                'dateTime' => $startTime,
            ),
            'end' => array(
                'dateTime' => $endTime,
            ),
            'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                    array('method' => 'email', 'minutes' => 24 * 60),
                    array('method' => 'email', 'minutes' => 10),
                    array('method' => 'popup', 'minutes' => 10),
                ),
            ),
            'conferenceData' => $conferenceData
        ));
        if(!empty($param['attendees']))
        $event->setAttendees($param['attendees']);

        try {
            $event = $service->events->insert($calendarId, $event, ['conferenceDataVersion' => 1]);
        } catch (Google_Exception $e) {
            $message = json_decode($e->getMessage());
            return array(
                'success' => 0,
                'errorCode' => "API error: ".$message->error->status." ".$message->error->errors[0]->message,
            );
        }

        return array(
            'success'       => 1,
            'external_id'   => $event->id,
            'eventCreated'  => $event->htmlLink,
            'hangoutLink'   => $event->hangoutLink,
        );
    }

    public function updateEvent(&$bean, $param){
        global $timedate;
        $this->host_user_id = '1';   //Set quyền edit = admin

        $client = $this->getClient();
        $config     = $this->getGoogleOauth2Config();
        if(!$this->hasAPIConfig($config)){
            return array(
                'success' => 0,
                'errorCode' => "API error: No config!",
            );
        }

        //find Event
        $res = $this->findEvent($bean->external_id);
        if(!$res['success'] || $res['event']['status'] == 'cancelled'){       // || $res['event']['status'] == 'cancelled'
            return array(
                'success'       => 0,
                'errorCode' => "API error: Event has been cancelled. Waiting for re-install Event!",
            );
        }else {
            $count_change = 0;
            //UPDATE - PATCH ALL
            $googleEvent = $res['event'];
            //update Event
            $calendarId = $config['calendar_id'];

            //name
            if($googleEvent->getSummary() != $bean->name){
                $googleEvent->setSummary($bean->name);
                $count_change++;
            }

            //center
            if($googleEvent->getLocation() != $bean->team_name){
                $googleEvent->setLocation($bean->team_name);
                $count_change++;
            }


            //description
            if($googleEvent->getDescription() != $bean->description){
                $googleEvent->setDescription($bean->description);
                $count_change++;
            }


            //Attendees
            $googleAtts = array_column($googleEvent->getAttendees(),'email');
            $emsAtts    = array_unique(array_column($param['attendees'],'email'));
            if($googleAtts != $emsAtts && !empty($param['attendees'])){
                $googleEvent->setAttendees($param['attendees']);
                $count_change++;
            }


            //Start/End
            $startTime  = date("c", strtotime("+7 hours ".$bean->date_start));
            if($googleEvent->getStart()->dateTime != $startTime){
                $eventDateTime = new Google_Service_Calendar_EventDateTime;
                $eventDateTime->setDateTime($startTime);
                $googleEvent->setStart($eventDateTime);
                $count_change++;
            }
            $endTime    = date("c", strtotime("+7 hours ".$bean->date_end));
            if($googleEvent->getEnd()->dateTime != $endTime){
                $eventDateTime = new Google_Service_Calendar_EventDateTime;
                $eventDateTime->setDateTime($endTime);
                $googleEvent->setEnd($eventDateTime);
                $count_change++;
            }
            if($count_change > 0){ // Nếu có thay đổi thì mới update
                //patch
                $service = new Google_Service_Calendar($client);
                try {
                    $event = $service->events->patch($calendarId, $googleEvent->id, $googleEvent, ['conferenceDataVersion' => 1]);
                } catch (Google_Exception $e) {
                    $message = json_decode($e->getMessage());
                    return array(
                        'success' => 0,
                        'errorCode' => "API error: ".$message->error->status." ".$message->error->errors[0]->message,
                    );
                }
            }else{
                $event = new stdClass();
                $event->id = $bean->external_id;
                $event->hangoutLink = $bean->join_url;
                $event->eventCreated = '';
            }

            return array(
                'success'       => 1,
                'external_id'   => $event->id,
                'eventCreated'  => $event->htmlLink,
                'hangoutLink'   => $event->hangoutLink,
            );

        }
    }

    public function deleteEvent($bean){
        global $timedate;
        $this->host_user_id = '1';//Fix tạm dùng quyền admin của Calendar để Delete
        $client     = $this->getClient();
        $config     = $this->getGoogleOauth2Config();
        $eventId    = $bean->external_id;
        $calendarId = $config['calendar_id'];

        if(!$this->hasAPIConfig($config)){
            return array(
                'success' => 0,
                'errorCode' => "API error: No config!",
            );
        }
        if(empty($eventId))
            return array(
                'success' => 0,
                'errorCode' => "API error: Empty eventId",
            );

        $service = new Google_Service_Calendar($client);
        $optParams = array( //KHÔNG NÊN GỬI
            'sendNotifications' => true,
            'sendUpdates' => 'all',
        );
        //find Event
        try {
            $res = $service->events->delete($calendarId, $eventId);
        } catch (Google_Exception $e) {
            $message = json_decode($e->getMessage());
            return array(
                'success' => 0,
                'errorCode' => "API error: ".$message->error->status." ".$message->error->errors[0]->message,
            );
        }
        return array(
            'success' => 1,
            'deletedId' => $eventId,
        );
    }

    public function findEvent($eventId){
        global $timedate;
        $this->host_user_id = '1';  //Set tạm
        $client     = $this->getClient();
        $config     = $this->getGoogleOauth2Config();
        //find Event
        $calendarId = $config['calendar_id'];
        if(!$this->hasAPIConfig($config)){
            return array(
                'success' => 0,
                'errorCode' => "API error: No config!",
            );
        }
        if(empty($eventId))
            return array(
                'success' => 0,
                'errorCode' => "EMS error: Empty eventId",
            );

        $service = new Google_Service_Calendar($client);
        //find Event
        try {
            $event = $service->events->get($calendarId, $eventId);
        } catch (Google_Exception $e) {
            $message = json_decode($e->getMessage());
            return array(
                'success' => 0,
                'errorCode' => "API error: ".$message->error->status." ".$message->error->errors[0]->message,
            );
        }
        return array(
            'success' => 1,
            'event' => $event
        );
    }

    public function getListEvent(){
        global $timedate;
        $this->host_user_id = '1';  //Set tạm
        $client     = $this->getClient();
        $config     = $this->getGoogleOauth2Config();
        //find Event
        $calendarId = $config['calendar_id'];
        if(!$this->hasAPIConfig($config)){
            return array(
                'success' => 0,
                'errorCode' => "API error: No config!",
            );
        }
        $nowDb = $timedate->nowDb();
        $startTime  = date("c", strtotime("+7 hours ".$nowDb));
        $params = array(
            'timeMin' => $startTime,
        );



        $service = new Google_Service_Calendar($client);
        //find Event
        try {
            $events  = $service->events->listEvents($calendarId, $params);
        } catch (Google_Exception $e) {
            $message = json_decode($e->getMessage());
            return array(
                'success' => 0,
                'errorCode' => "API error: ".$message->error->status." ".$message->error->errors[0]->message,
            );
        }
        return array(
            'success' => 1,
            'events' => $events
        );
    }
}
