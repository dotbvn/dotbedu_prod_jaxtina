<?php


use Dotbcrm\Dotbcrm\Security\InputValidation\InputValidation;

/**
* Known Entry Points as of 4.5
* acceptDecline.php
* campaign_tracker.php
* campaign_trackerv2.php
* cron.php
* dictionary.php
* download.php
* emailmandelivery.php
* export_dataset.php
* export.php
* image.php
* index.php
* install.php
* json.php
* json_server.php
* leadCapture.php
* maintenance.php
* metagen.php
* pdf.php
* phprint.php
* process_queue.php
* process_workflow.php
* removeme.php
* schedulers.php
* soap.php
* su.php
* dotb_version.php
* TreeData.php
* tree_level.php
* tree.php
* vcal_server.php
* vCard.php
* zipatcher.php
* WebToLeadCapture.php
* HandleAjaxCall.php */

/*
* for 50, added:
* minify.php
*/

/*
* for 510, added:
* dceActionCleanup.php
*/

$GLOBALS['starttTime'] = microtime(true);

if (!defined('DOTB_BASE_DIR')) {
    define('DOTB_BASE_DIR', str_replace('\\', '/', realpath(dirname(__FILE__) . '/..')));
}

if (!defined('SHADOW_INSTANCE_DIR') && extension_loaded('shadow') && ini_get('shadow.enabled')) {
    $shadowConfig = shadow_get_config();
    if ($shadowConfig['instance']) {
        define('SHADOW_INSTANCE_DIR', $shadowConfig['instance']);
    }
}

set_include_path(
    DOTB_BASE_DIR . PATH_SEPARATOR .
    DOTB_BASE_DIR . '/vendor' . PATH_SEPARATOR .
    get_include_path()
);
/**
* DotB Single & Multi-tenancy
* Fast and simple dealing with Dotb-ems
* @author alovip
* @since Jan 7, 2024
*/
if(is_file('config.php'))  require_once('config.php');
if(!$dotb_config && is_file('config.sample.php'))  require_once('config.sample.php');
$flag_repair = !file_exists('tenancy/tenant_map.php');
if (!file_exists('tenancy/tenant_map.php')) {
    if (!is_dir('tenancy')) mkdir('tenancy');
    $fp = fopen("tenancy/tenant_map.php", "wb");
    fwrite($fp, "<?php \n\$tenant_map = [];");
    fclose($fp);
}

$tenant_license = $_REQUEST['tenant_license']; //set default  tenant_license
$site_host = parse_url($dotb_config['site_url'])['host'];
$tenant_name = ($site_host == 'production.dotb.cloud') ? $_SERVER['SERVER_NAME'] : $site_host;
require_once('tenancy/tenant_map.php');
$tenant_id = $tenant_map[$tenant_name];
if (empty($_REQUEST) && empty($tenant_id)) {
    // Call Quyettam api to get tenant_id from domain_name
    $url = $dotb_config['saas_url'] . "/rest/v11_3/tenant/get";
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('tenant_name' => $tenant_name),
    ));
    $response = json_decode(curl_exec($curl), true);
    curl_close($curl);

    if ($response['success']) {
        // If tenant_id is returned, then biuld tenant (config files,custom,cache...) first times
        $tenant_id = $response['data']['tenant']['id'];
        if ($tenant_id) {
            $tenant_license = $response['data']['tenant']['license'];
            // Update tenant_map
            if (!isset($tenant_map[$tenant_name])) {
                $contents = "\n\$tenant_map['" . $tenant_name ."'] = '$tenant_id';";
                file_put_contents("tenancy/tenant_map.php", $contents . PHP_EOL, FILE_APPEND);
            }
            $tenant_map[$tenant_name] = $tenant_id;

            if ($response['data']['tenant']['is_multi_tenant']) {
                $flag_repair = true;
                // Build tenant dir & config file
                $tenant_dir = "tenancy/$tenant_id";
                if (!is_dir($tenant_dir)) {
                    mkdir($tenant_dir);
                }
                if (file_exists('config.sample.php') && is_readable('config.sample.php')) {
                    $json_content = json_encode($response['data']['tenant']['config'], JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                    file_put_contents("$tenant_dir/config.json", $json_content);
                }
            }
        }
    } else {
        // Otherwise, echo "error"
        echo('<b>Error: </b>' . $response['error_info']['error_msg']);
        exit();
    }
}
$GLOBALS['tenant_id'] = $tenant_id;
$GLOBALS['tenant_path'] = ($site_host == 'production.dotb.cloud' && $tenant_name != 'production.dotb.cloud' && !empty($tenant_id)) ? "tenancy/$tenant_id/" : "";

//load tenant_path - provides $dotb_config
if(is_file('config.php')) {
    require_once('config.php');
    //set tenant_path global components cache only
    $GLOBALS['dotb_config']['cache_dir'] = $GLOBALS['tenant_path'].$dotb_config['cache_dir'];
    $GLOBALS['dotb_config']['upload_dir'] = $GLOBALS['tenant_path'].$dotb_config['upload_dir'];
}else{
    echo('Error: config.php file not found<br>
    Error: Config file does not exist. Please run \'dotb core config\' first.<br>');
    exit();
}

// Load tenant config
if (file_exists($GLOBALS['tenant_path'] . 'config.json')) {
    $config = json_decode(file_get_contents($GLOBALS['tenant_path'] . 'config.json'), true);
    $dotb_config['dbconfig']['db_host_name'] = $config['host'];
    $dotb_config['dbconfig']['db_user_name'] = $config['user'];
    $dotb_config['dbconfig']['db_password'] = $config['pass'];
    $dotb_config['dbconfig']['db_name'] = $config['name'];
    $dotb_config['dbconfig']['db_port'] = $config['port'];
    $dotb_config['site_url'] = $config['url'];
    $dotb_config['brand_name'] = $config['brand_name'];
    $dotb_config['full_text_engine']['Elastic']['host'] = $config['search_host'];
    $dotb_config['full_text_engine']['Elastic']['port'] = $config['search_port'];
    $dotb_config['brand_id'] = $config['brand_id'];
    $dotb_config['unique_key'] = $config['app_key'];
    $dotb_config['db']['reports']['db_host_name'] = $config['db_report_host'];
    $dotb_config['db']['reports']['db_user_name'] = $config['db_report_user'];
    $dotb_config['db']['reports']['db_password'] = $config['db_report_pass'];
    $dotb_config['db']['reports']['db_name'] = $config['db_report_name'];
    $dotb_config['db']['reports']['db_port'] = $config['db_report_port'];
    $dotb_config['db']['reports']['db_type'] = 'mysqli';
    $dotb_config['db']['reports']['db_manager'] = 'MysqliManager';
    $dotb_config['db']['reports']['db_host_instance'] = 'SQLEXPRESS';
    $dotb_config['log_dir'] = $GLOBALS['tenant_path'].$dotb_config['log_dir'];

    if (!empty($config['db_host_slave'])) {
        $dotb_config['db']['reports'] = array(
            'db_host_name' => $config['db_host_slave'],
            'db_user_name' => $config['db_user_slave'],
            'db_password' => $config['db_pass_slave'],
            'db_name' => $config['db_name_slave'],
            'db_type' => 'mysql',
            'db_port' => $config['db_port_slave'],
        );
        $dotb_config['db']['listviews'] = array(
            'db_host_name' => $config['host'],
            'db_user_name' => $config['user'],
            'db_password' => $config['pass'],
            'db_name' => $config['name'],
            'db_type' => 'mysql',
            'db_port' => $config['port'],
        );
    }
}

// load up the config_override.php file.  This is used to provide default user settings
if(is_file($GLOBALS['tenant_path'].'config_override.php')) require $GLOBALS['tenant_path'].'config_override.php';
//END- DotB Single & Multi-tenancy - By alovip

if (!is_dir($GLOBALS['dotb_config']['cache_dir'])) mkdir($GLOBALS['dotb_config']['cache_dir']);
if (!is_dir($GLOBALS['dotb_config']['upload_dir'])) mkdir($GLOBALS['dotb_config']['upload_dir']);

require_once 'custom/data/License/LicenseBean.php';
require_once('include/utils.php');
require_once 'custom/include/license_utils.php';
require_once 'include/dir_inc.php';
require_once 'include/utils/array_utils.php';
require_once 'include/utils/file_utils.php';
require_once 'include/utils/security_utils.php';
require_once 'include/utils/logic_utils.php';
require_once 'include/utils/dotb_file_utils.php';
require_once 'include/utils/mvc_utils.php';
require_once 'include/utils/db_utils.php';
require_once 'include/utils/encryption_utils.php';
require_once 'include/DotbCache/DotbCache.php';
require_once 'include/utils/autoloader.php';
DotbAutoLoader::init();

if(empty($GLOBALS['installing']) &&empty($dotb_config['dbconfig']['db_name']))
{
    header('Location: install.php');
    exit ();
}

if (empty($GLOBALS['installing'])) {
    $GLOBALS['log'] = LoggerManager::getLogger('DotBCRM');
}

if (!empty($dotb_config['xhprof_config'])) {
    DotbXHprof::getInstance()->start();
}

register_shutdown_function('dotb_cleanup');


// cn: set php.ini settings at entry points
setPhpIniSettings();
create_htaccess();

require_once('dotb_version.php'); // provides $dotb_version, $dotb_db_version, $dotb_flavor

// Initialize InputValdation service as soon as possible. Up to this point
// it is expected that no code has altered any input superglobals.
InputValidation::initService();

// Check to see if custom utils exist and load them too
// not doing it in utils since autoloader is not loaded there yet
foreach (DotbAutoLoader::existing('include/custom_utils.php', 'custom/include/custom_utils.php', DotbAutoLoader::loadExtension('utils')) as $file) {
    require_once $file;
}

require_once('include/modules.php'); // provides $moduleList, $beanList, $beanFiles, $modInvisList, $adminOnlyList, $modInvisListActivities
require_once 'modules/Currencies/Currency.php';

// rebuild application if not found
$application_path = 'custom/application/Ext';
if (isset($GLOBALS['tenant_id'])) {
    $application_path = tenancy_path($application_path);
}

if (count(scandir($application_path)) < 3) {
    global $beanList;
    $modBeans = array_flip($beanList);
    $moduleInstallerClass = DotbAutoLoader::customClass('ModuleInstaller');
    $mi = new $moduleInstallerClass();
    $mi->rebuild_all(true, $modBeans);
}

UploadStream::register();

///////////////////////////////////////////////////////////////////////////////
////    Handle loading and instantiation of various Dotb* class
if (!defined('DOTB_PATH')) {
    define('DOTB_PATH', realpath(dirname(__FILE__) . '/..'));
}
if(empty($GLOBALS['installing'])){
    ///////////////////////////////////////////////////////////////////////////////
    ////	SETTING DEFAULT VAR VALUES
    $error_notice = '';
    $use_current_user_login = false;

    LogicHook::initialize()->call_custom_logic('', 'entry_point_variables_setting');

    if(!empty($dotb_config['session_dir'])) {
        session_save_path($dotb_config['session_dir']);
    }

    if (class_exists('SessionHandler')) {
        session_set_save_handler(new DotbSessionHandler());
    }

    $GLOBALS['dotb_version'] = $dotb_version;
    $GLOBALS['dotb_flavor'] = $dotb_flavor;
    $GLOBALS['js_version_key'] = get_js_version_key();
    // Because this line is *supposed* to be indented...
    $GLOBALS['dotb_mar_version'] = $dotb_mar_version;

    DotbApplication::preLoadLanguages();

    $timedate = TimeDate::getInstance();
    $GLOBALS['timedate'] = $timedate;

    $db = DBManagerFactory::getInstance();
    $db->resetQueryCount();
    $locale = Localization::getObject();

    // Emails uses the REQUEST_URI later to construct dynamic URLs.
    // IIS does not pass this field to prevent an error, if it is not set, we will assign it to ''.
    if (!isset ($_SERVER['REQUEST_URI'])) {
        $_SERVER['REQUEST_URI'] = '';
    }

    $current_user = BeanFactory::newBean('Users');
    $current_entity = null;
    $system_config = Administration::getSettings();

    if (!$GLOBALS['dotb_config']['activity_streams_enabled']) {
        Activity::disable();
    }

    LogicHook::initialize()->call_custom_logic('', 'after_entry_point');
}
if(empty($_REQUEST) || $_REQUEST['enforced_license'] == 1) {
    if (count($tenant_license) > 0) {
        //add scheduler license
        $GLOBALS['db']->query("DELETE FROM schedulers WHERE job = 'function::checkTenantLicense';");
        $GLOBALS['db']->query("INSERT INTO schedulers (`id`, `name`, `date_entered`, `date_modified`, `modified_user_id`, `created_by`, `description`, `deleted`, `job`, `date_time_start`, `job_interval`, `status`, `catch_up`) VALUES ('99', '*.b* Check Tenant License', '2024-05-24 04:59:12', '2024-05-24 05:01:39', '1', '1', '', '0', 'function::checkTenantLicense', '2005-01-01 05:00:00', '5::*/12::*::*::*', 'Active', '0');");

        //Xoá license cũ
//        $GLOBALS['db']->query("DELETE FROM config WHERE category = 'license'");

        $admin = new Administration();
        $admin->retrieveSettings();
        // Save new config
        foreach ($tenant_license as $key => $value) {
            $admin->saveSetting('license', $key, $value);
        }
    }
    // Need repair if this is multi-tenant site or
    if ($flag_repair || $_REQUEST['enforced_license'] == 1) {
        //  START: BUILD CACHE
        ob_flush();
        $randc = new RepairAndClear();
        $randc->module_list = array(translate('LBL_ALL_MODULES'));
        $randc->show_output = false;
        $randc->execute = true;
        $randc->clearLanguageCache();
        ACLField::clearACLCache();
        $randc->clearTpls();
        $randc->clearJsFiles();
        $randc->clearJsLangFiles();
        $randc->clearSmarty();
        $randc->clearExternalAPICache();
        $randc->clearAdditionalCaches();
        $randc->rebuildExtensions();
        $randc->rebuildFileMap();
        $randc->repairMetadataAPICache('');
        $randc->rebuildJSCacheFiles();

        /// clean bwc js due to get the newest version
        $minifyUtils = new DotbMinifyUtils();
        $minifyUtils->clearCacheBWC();

        // Run the metadata cache refresh queue. This will turn queueing off after it is run
        MetaDataManager::runCacheRefreshQueue();
        //  END: BUILD CACHE
    }
}
refreshGlobalLicense();
////	END SETTING DEFAULT VAR VALUES
///////////////////////////////////////////////////////////////////////////////
