<?php


namespace Dotbcrm\Dotbcrm\inc\Entitlements\Exception;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Dotbcrm\Dotbcrm\Logger\Factory as LoggerFactory;
use Throwable;

class SubscriptionException extends \RuntimeException implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $this->setLogger(LoggerFactory::getLogger('subscription'));
        $this->logger->alert($message);

        parent::__construct($message, $code, $previous);
    }
}
