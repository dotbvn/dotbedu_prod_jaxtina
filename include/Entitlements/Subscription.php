<?php


namespace Dotbcrm\Dotbcrm\Entitlements;

// This section of code is a portion of the code referred
// to as Critical Control Software under the End User
// License Agreement.  Neither the Company nor the Users
// may modify any portion of the Critical Control Software.
use Dotbcrm\Dotbcrm\inc\Entitlements\Exception\SubscriptionException;

/**
 * Class Subscription
 *
 * dotb subscription object, it parses raw subscription data and provides APIs to access those data
 */
class Subscription
{
    /**
     * license keys or types
     */
    const DOTB_SELL_KEY = 'DOTB_SELL';
    const DOTB_SELL_ESSENTIALS_KEY = 'DOTB_SELL_ESSENTIALS';
    const DOTB_SELL_BUNDLE_KEY = 'DOTB_SELL_BUNDLE';
    const DOTB_SELL_PREMIER_BUNDLE_KEY = 'DOTB_SELL_PREMIER_BUNDLE';
    const DOTB_SELL_ADVANCED_BUNDLE_KEY = 'DOTB_SELL_ADVANCED_BUNDLE';
    const DOTB_SERVE_KEY = 'DOTB_SERVE';
    const DOTB_BASIC_KEY = 'CURRENT';
    const DOTB_HINT_KEY = 'HINT';
    const DOTB_PREDICT_ADVANCED_KEY = 'PREDICT_ADVANCED';
    const DOTB_PREDICT_PREMIER_KEY = 'PREDICT_PREMIER';
    const DOTB_CONNECT_KEY = 'CONNECT';
    const DOTB_DISCOVER_KEY = 'DISCOVER';
    const DOTB_MAPS_KEY = 'MAPS';
    const DOTB_ADVANCEDFORECAST_KEY = 'ADVANCEDFORECAST';

    /**
     * unknown type
     */
    const UNKNOWN_TYPE = 'UNKNOWN';

    /**
     * Mango CRM keys,
     * please keep the order of this list, the order of this list will be used in displaying user's license type
     */
    const MANGO_KEYS = [
        Subscription::DOTB_SELL_PREMIER_BUNDLE_KEY,
        Subscription::DOTB_SELL_ADVANCED_BUNDLE_KEY,
        Subscription::DOTB_SELL_ESSENTIALS_KEY,
        Subscription::DOTB_SELL_BUNDLE_KEY,
        Subscription::DOTB_SELL_KEY,
        Subscription::DOTB_SERVE_KEY,
        Subscription::DOTB_BASIC_KEY,
    ];

    /**
     * Bundle keys, the bundle will contain other products
     */
    const BUNDLE_KEYS = [
        Subscription::DOTB_SELL_ESSENTIALS_KEY,
        Subscription::DOTB_SELL_BUNDLE_KEY,
        Subscription::DOTB_SELL_PREMIER_BUNDLE_KEY,
        Subscription::DOTB_SELL_ADVANCED_BUNDLE_KEY,
    ];

    /**
     * current supported keys
     */
    const SELL_KEYS = [
        self::DOTB_SELL_KEY,
        self::DOTB_SELL_ESSENTIALS_KEY,
        self::DOTB_SELL_BUNDLE_KEY,
        self::DOTB_SELL_PREMIER_BUNDLE_KEY,
        self::DOTB_SELL_ADVANCED_BUNDLE_KEY,
    ];

    /**
     * current supported keys
     */
    const SUPPORTED_KEYS = [
        self::DOTB_BASIC_KEY,
        self::DOTB_SELL_KEY,
        self::DOTB_SERVE_KEY,
        self::DOTB_HINT_KEY,
        // new in Mango 12.0
        self::DOTB_SELL_ESSENTIALS_KEY,
        self::DOTB_SELL_BUNDLE_KEY,
        self::DOTB_SELL_PREMIER_BUNDLE_KEY,
        self::DOTB_SELL_ADVANCED_BUNDLE_KEY,
        self::DOTB_PREDICT_ADVANCED_KEY,
        self::DOTB_PREDICT_PREMIER_KEY,
        self::DOTB_CONNECT_KEY,
        self::DOTB_DISCOVER_KEY,
        self::DOTB_MAPS_KEY,
        self::DOTB_ADVANCEDFORECAST_KEY,
    ];

    /**
     * mapping product codes to internal keys
     */
    const PRODUCT_CODE_MAPPING = [
        'ENT' => self::DOTB_BASIC_KEY,
        'PRO' => self::DOTB_BASIC_KEY,
        'ULT' => self::DOTB_BASIC_KEY,
        'SELL' => self::DOTB_SELL_KEY,
        'SERVE' => self::DOTB_SERVE_KEY,
        'HINT' => self::DOTB_HINT_KEY,
        'PREDICT_ADVANCED' => self::DOTB_PREDICT_ADVANCED_KEY,
        'PREDICT_PREMIER' => self::DOTB_PREDICT_PREMIER_KEY,
        'CONNECT' => self::DOTB_CONNECT_KEY,
        'DISCOVER' => self::DOTB_DISCOVER_KEY,
        'MAPS' => self::DOTB_MAPS_KEY,
        'ADVANCED_FORECAST' => self::DOTB_ADVANCEDFORECAST_KEY,
    ];

    /**
     * mapping for edition
     */
    const SELL_EDITIONS_MAPPING = [
        'SELL_PREMIER' => self::DOTB_SELL_PREMIER_BUNDLE_KEY,
        'SELL_ADVANCED' => self::DOTB_SELL_ADVANCED_BUNDLE_KEY,
        'SELL_ESSENTIALS' => self::DOTB_SELL_ESSENTIALS_KEY,
        'OTHERS' => self::DOTB_SELL_BUNDLE_KEY,
    ];

    /**
     * visible Non CRM products in user's licese types' list
     */
    const VISIBLE_NONCRM_PRODUCTS = [
        Subscription::DOTB_HINT_KEY,
        Subscription::DOTB_MAPS_KEY,
    ];

    /**
     * internal data
     * @var array
     */
    protected $data = [];

    /**
     * parsed subscription data
     * @var array
     */
    protected $subscriptions = [];
    protected $expiredSubscriptions = [];

    /**
     * @var array of Addons
     */
    protected $addons = [];

    /**
     * use default value from license config
     * @var bool
     */
    protected $useDefault = false;

    /**
     * private Subscription constructor.
     * @param mixed $jsonData
     */
    public function __construct($jsonData)
    {
        if ($jsonData === false || $jsonData === '') {
            $this->useDefault = true;
            $this->subscriptions[self::DOTB_BASIC_KEY] = $this->getDefaultSubscription();
        } else {
            $this->parse($jsonData);
        }
    }

    /**
     * parse the raw subscription data
     * @param string $jsonData
     */
    protected function parse(string $jsonData)
    {
        $decodedData = json_decode($jsonData, true);
        if ($decodedData === null) {
            throw new SubscriptionException('Invalid subscription json data');
        }
        
        if (empty($decodedData['subscription'])) {
            return;
        }

        foreach ($decodedData['subscription'] as $key => $value) {
            if ($key === 'addons' && count($decodedData['subscription'][$key]) > 0) {
                foreach ($decodedData['subscription'][$key] as $addonId => $addonData) {
                    $this->addons[$addonId] = new Addon($addonId, $addonData);
                }
            } else {
                $this->data[$key] = $value;
            }
        }
        $this->data['addons'] = $this->addons;
    }

    /**
     * access method
     * @param string $name
     * @return mixed|null
     */
    public function __get(string $name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return null;
    }

    /**
     * to get subscriptions
     * only gets the subscriptions with quantity > 0 and not expired.
     *
     * return in array format
     * [
     *      'quantity' => ...,
     *      'expiration_date' => ...,
     * ];
     * @return array
     */
    public function getSubscriptions() : array
    {
        if (!empty($this->subscriptions) || $this->useDefault) {
            return $this->subscriptions;
        }

        if (empty($this->data)) {
            return [];
        }

        if (!empty($this->error)) {
            $GLOBALS['log']->fatal("there is an error in license server response: " . $this->error);
            return [];
        }

        $subscriptions = $this->getSubscriptionFromAddons($this->addons);
        $this->subscriptions = $subscriptions;

        return $subscriptions;
    }

    /**
     * parse the list of addons.
     * @param array $addons
     * @param bool $isTopLevel
     * @return array
     */
    protected function getSubscriptionFromAddons(array $addons, bool $isTopLevel = true, $expDate = '', $sttDate = '') : array
    {
        $subscriptions = [];
        // will skip top level of the subscription
        // initiate quantity count
        foreach (self::SUPPORTED_KEYS as $type) {
            $subscriptions[$type]['quantity'] = 0;
        }

        // check addons, only interested in 'SELL', 'SERVE' and Legacy product codes such as 'ENT', 'PRO' and 'ULT'.
        // also need to check bundles
        // ignore any other addons for now
        foreach ($addons as $addon) {
            $quantity = (int)$addon->quantity;
            $expirationDate = $addon->expiration_date;
            if (empty($expirationDate)) {
                $expirationDate = $expDate;
            }

            $startDate = $addon->start_date_c;
            if (empty($startDate)) {
                $startDate = $sttDate;
            }
            if (($quantity > 0)
                && ((empty($expirationDate) && !$isTopLevel) || (!empty($expirationDate) && is_numeric($expirationDate) && $expirationDate > time()))
                && (empty($startDate) || !empty($startDate) && is_numeric($startDate) && $startDate < time())) {
                // using product code to find out subscription types
                $productCode = $addon->product_code_c;
                if (!empty($productCode) && !empty(self::PRODUCT_CODE_MAPPING[strtoupper($productCode)])) {
                    $key = self::PRODUCT_CODE_MAPPING[strtoupper($productCode)];
                    $bundlesSubscriptions = [];
                    if ($key === self::DOTB_SELL_KEY) {
                        // reassign to bundle key based on edition
                        $edition = $addon->product_edition_c;
                        if (!empty($edition) && isset(self::SELL_EDITIONS_MAPPING[strtoupper($edition)])) {
                            $key = self::SELL_EDITIONS_MAPPING[strtoupper($edition)];
                        }
                        if ($addon->hasBundledProducts()) {
                            if (!empty($edition)) {
                                $key = self::SELL_EDITIONS_MAPPING[strtoupper($edition)] ?? self::DOTB_SELL_BUNDLE_KEY;
                            } else {
                                $key = self::DOTB_SELL_BUNDLE_KEY;
                            }
                            // get bundles
                            $bundles = $addon->getBundledProducts();
                            $bundlesSubscriptions = $this->getSubscriptionFromAddons($bundles, false, $expirationDate, $startDate);
                        }
                    }
                    // calculate the expiration date, using the min date as expiration date
                    if (!empty($subscriptions[$key]['expiration_date'])
                        && $expirationDate > $subscriptions[$key]['expiration_date']
                    ) {
                        $expirationDate = $subscriptions[$key]['expiration_date'];
                    }

                    $customerProductName = $addon->customer_product_name_c;
                    $subscriptions[$key] = [
                        'quantity' => $subscriptions[$key]['quantity'] + $quantity,
                        'expiration_date' => $expirationDate,
                        'start_date' => $startDate,
                        'customer_product_name' => $customerProductName,
                        // this may not be needed since PM has promised 1 bundle product for a license key
                        Addon::BUNDLED_PRODUCTS_KEY => array_merge($subscriptions[$key][Addon::BUNDLED_PRODUCTS_KEY] ?? [], $bundlesSubscriptions),
                    ];
                }
            }
        }

        // remove 0 quantity keys
        foreach ($subscriptions as $key => $value) {
            if ($subscriptions[$key]['quantity'] === 0) {
                unset($subscriptions[$key]);
            }
        }

        return $subscriptions;
    }
    /**
     * to get all subscriptions, including expired and not yet started
     *
     * return in array format
     * [
     *      'quantity' => ...,
     *      'expiration_date' => ...,
     * ];
     *
     * @return array
     */
    public function getExpiredSubscriptions() : array
    {
        if (!empty($this->expiredSubscriptions)) {
            return $this->expiredSubscriptions;
        }
        $subscriptions = [];
        if (empty($this->data)) {
            return [];
        }

        if (!empty($this->error)) {
            $GLOBALS['log']->fatal("there is an error in license server response: " . $this->error);
            return [];
        }

        // will skip top level of the subscription
        // initiate quantity count
        foreach (self::SUPPORTED_KEYS as $type) {
            $subscriptions[$type]['quantity'] = 0;
        }

        // check addons, only interested in 'SELL', 'SELL' BUNDLES, 'SERVE' and Legacy product codes such as 'ENT', 'PRO' and 'ULT'.
        // ignore any other addons for now
        foreach ($this->addons as $addon) {
            $quantity = (int)$addon->quantity;
            $expirationDate = $addon->expiration_date;
            $startDate = $addon->start_date_c;

            if ($quantity === 0 || !isset($expirationDate) || $expirationDate >= time()) {
                continue;
            }

            $customerProductName = $addon->customer_product_name_c;
            // using product code to find out subscription types
            $productCode = $addon->product_code_c;
            if (!empty($productCode) && !empty(self::PRODUCT_CODE_MAPPING[strtoupper($productCode)])) {
                $key = self::PRODUCT_CODE_MAPPING[strtoupper($productCode)];
                $subscriptions[$key] = [
                    'quantity' => $subscriptions[$key]['quantity'] + $quantity,
                    'expiration_date' => $expirationDate,
                    'start_date' => $startDate,
                    'customer_product_name' => $customerProductName,
                ];
            }
        }

        // remove 0 quantity keys
        $this->expiredSubscriptions = array_filter($subscriptions, static function (array $item) {
            return $item['quantity'] !== 0;
        });

        return $this->expiredSubscriptions;
    }

    /**
     * get keys for subscriptions
     *
     * @param bool $getAll to get all subscription keys if it is true
     * need to take care of ENT, PRO, etc
     */
    public function getSubscriptionKeys(bool $getAll = true) : array
    {
        $subscriptions = $this->getSubscriptions();
        if (empty($subscriptions)) {
            return [];
        }

        $keys = [];
        foreach ($subscriptions as $key => $value) {
            if (!in_array($key, $this->getAddonProducts())) {
                $keys[self::DOTB_BASIC_KEY] = true;
            } else {
                $keys[$key] = true;
                if ($getAll && self::isBundleKey($key) && !empty($value[Addon::BUNDLED_PRODUCTS_KEY]) && is_array($value[Addon::BUNDLED_PRODUCTS_KEY])) {
                    foreach ($value[Addon::BUNDLED_PRODUCTS_KEY] as $bundledKey => $bundledItem) {
                        if (!empty($bundledItem)) {
                            $keys[$bundledKey] = true;
                        }
                    }
                }
            }
        }
        return $keys;
    }

    /**
     * get all subscription keys
     * @return array
     */
    public function getAllSubscriptionKeys() : array
    {
        return $this->getSubscriptionKeys(true);
    }

    /**
     * get top level subscription keys only
     * @return array
     */
    public function getTopLevelSubscriptionKeys() : array
    {
        return $this->getSubscriptionKeys(false);
    }
    /**
     * get subscription(s) for a key
     *
     * @param string $key key to search
     * need to take care of ENT, PRO, etc
     */
    public function getSubscriptionByKey(?string $key) : array
    {
        $subscriptions = $this->getSubscriptions();
        if (empty($subscriptions)) {
            return [];
        }

        if (!empty($subscriptions[$key])) {
            return $subscriptions[$key];
        }

        foreach ($subscriptions as $subKey => $value) {
            if (self::isBundleKey($subKey) && !empty($value[Addon::BUNDLED_PRODUCTS_KEY]) && is_array($value[Addon::BUNDLED_PRODUCTS_KEY])) {
                foreach ($value[Addon::BUNDLED_PRODUCTS_KEY] as $bundledKey => $bundledItem) {
                    if ($bundledKey === $key && !empty($bundledItem)) {
                        return $bundledItem;
                    }
                }
            }
        }
        return [];
    }

    /**
     * get current supported addon products,
     * @return array
     */
    public function getAddonProducts() : array
    {
        return [
            Subscription::DOTB_SELL_KEY,
            Subscription::DOTB_SERVE_KEY,
            Subscription::DOTB_HINT_KEY,
            Subscription::DOTB_PREDICT_ADVANCED_KEY,
            Subscription::DOTB_PREDICT_PREMIER_KEY,
            Subscription::DOTB_CONNECT_KEY,
            Subscription::DOTB_DISCOVER_KEY,
            Subscription::DOTB_MAPS_KEY,
            Subscription::DOTB_ADVANCEDFORECAST_KEY,
            Subscription::DOTB_SELL_ESSENTIALS_KEY,
            Subscription::DOTB_SELL_BUNDLE_KEY,
            Subscription::DOTB_SELL_ADVANCED_BUNDLE_KEY,
            Subscription::DOTB_SELL_PREMIER_BUNDLE_KEY,
        ];
    }


    /**
     * check if a key is Mango key
     * @return array
     */
    public static function isMangoKey(?string $key) : bool
    {
        return in_array($key, self::MANGO_KEYS);
    }

    /**
     * check is bundle key
     * @param string|null $key
     * @return bool
     */
    public static function isBundleKey(?string $key) : bool
    {
        return in_array($key, self::BUNDLE_KEYS);
    }

    /**
     * get all CRM keys for a bundle, for instance DOTB_SELL_BUNDLE_KEY will return [DOTB_SELL_KEY, DOTB_SELL_BUNDLE_KEY]
     *
     * @param string $key
     * @return string[]
     */
    public static function getBundledKeys(string $key) : array
    {
        switch ($key) {
            case (self::DOTB_SELL_ESSENTIALS_KEY):
            case (self::DOTB_SELL_BUNDLE_KEY):
            case (self::DOTB_SELL_PREMIER_BUNDLE_KEY):
            case (self::DOTB_SELL_ADVANCED_BUNDLE_KEY):
                return [self::DOTB_SELL_KEY, $key];
            default:
                return [];
        }
    }

    /**
     * get the first SELL key from a license types array
     * @param array $licenseTypes license types array
     * @return string
     */
    public static function getSellKey(array $licenseTypes) : string
    {
        foreach (Subscription::SELL_KEYS as $key) {
            if (in_array($key, $licenseTypes)) {
                return $key;
            }
        }
        return '';
    }
    /**
     * get default subscription in case of offline, client is not able to download from license server
     * @return array
     */
    protected function getDefaultSubscription() : array
    {

        $expiredDate = $this->getLicenseSettingByKey('license_expire_date', '+12 months');
        if (strtotime($expiredDate) - time() < 0) {
            if (!empty($GLOBALS['log'])) {
                $GLOBALS['log']->fatal("license was expired at " . $expiredDate);
            }
            return [];
        }

        return [
            'quantity' => $this->getLicenseSettingByKey('license_users', 1),
            'expiration_date' => strtotime($expiredDate),
            'start_date' => null,
            'customer_product_name' => 'DotbCRM',
            Addon::BUNDLED_PRODUCTS_KEY => [],
        ];
    }

    /**
     * get license setting values, it will take the default value if it is during installation
     * @param string $key
     * @param $defaultValue
     */
    protected function getLicenseSettingByKey(string $key, $defaultValue)
    {
        if (isset($GLOBALS['installing']) && $GLOBALS['installing'] === true) {
            return $defaultValue;
        }

        if (!isset($GLOBALS['license'])) {
            loadLicense(true);
        }

        if (!empty($GLOBALS['license']->settings[$key])) {
            return $GLOBALS['license']->settings[$key];
        }
        return $defaultValue;
    }

    /**
     * order license type
     * @param array|null $licTypes user's license type
     * @return array
     */
    public static function getOrderedLicenseTypes(?array $licTypes) : array
    {
        if (empty($licTypes)) {
            return [];
        }

        $retLicenseTypes = [];
        // order by Mango keys first, tehn non-crm keys
        foreach ([Subscription::MANGO_KEYS, self::VISIBLE_NONCRM_PRODUCTS] as $keys) {
            foreach ($keys as $key) {
                if (in_array($key, $licTypes)) {
                    $retLicenseTypes[] = $key;
                }
            }
        }
        return $retLicenseTypes;
    }
}
//END REQUIRED CODE DO NOT MODIFY
