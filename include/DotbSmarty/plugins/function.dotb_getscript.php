<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty {dotb_getscript} function plugin
 *
 * Type:     function<br>
 * Name:     dotb_getscript<br>
 * Purpose:  Creates script tag for filename with caching string
 *
 * @param array
 * @param Smarty
 */
function smarty_function_dotb_getscript($params, &$smarty)
{
    if(!isset($params['file'])) {
        $smarty->trigger_error($GLOBALS['app_strings']['ERR_MISSING_REQUIRED_FIELDS'] . 'file');
    }

    if (isset($GLOBALS['dotb_config']['bwc_development'])) {
        if ($GLOBALS['dotb_config']['bwc_development'] === true) {
            return '<script type="text/javascript" src="'.$params['file'].'"></script>';
        }
    }

    /// todo: check if cache exist

    /// target dir
    $jsDir = dotb_cached('javascript/bwc');

    $duplicated = checkDuplicatedCache(basename($params['file']));

    if ($duplicated != '') {
        return '<script type="text/javascript" src="'.$duplicated.'"></script>';
    }

    if (!file_exists($jsDir)) {
        mkdir($jsDir);
    }

    /// get bwc js mapping
    global $bwc_js;

    if (file_exists(dotb_cached('bwc_js.php'))) {
        require_once dotb_cached('bwc_js.php');
    }

    $cachePath = $jsDir."/".$bwc_js[$params['file']];

    if (!is_file($cachePath)) {
        $content = '';

        /// make sure file exist and readable
        if (is_file($params['file']) && is_readable($params['file'])) {
            $content = file_get_contents($params['file']);
        }

        /// minified content in needed
        $stringExp = explode('/', $params['file']);
        $fileName = end($stringExp);

        $already_minified = strpos($fileName, 'min.js');

        /// get raw file name
        $stringExp = explode('.', $fileName);
        $fileName = $stringExp[0];

        if (!$already_minified) {
            try {
                $content = DotbMin::minify($content);
            } catch (Exception $e) {}
        }

        $md5 = md5($content);

        $cachePath = "$jsDir/$fileName"."_"."$md5.js";

        file_put_contents($cachePath, $content);

        mappingBWC($params['file'], $fileName."_".$md5, $bwc_js);

        return '<script type="text/javascript" src="'.$cachePath.'"></script>';
    } else {
        return '<script type="text/javascript" src="'.$cachePath.'"></script>';
    }
}


function mappingBWC($path, $fileName, &$bwc_js = array()) {

    /// bwc js mapping
    $existed = 0;

    foreach ($bwc_js as $key => $value) {
        //// if original path exist => update it
        if ($key === $path) {
            $bwc_js[$path] = "$fileName.js";
            $existed++;
            break; /// break at the first time find out the existent
        }
    }

    /// if there is a new path
    if ($existed === 0) {
        $bwc_js = array_merge($bwc_js, array($path => "$fileName.js"));
    }

    $content = "<?php\n"."//This file is auto generate\n".'$bwc_js = array('."\n";

    foreach ($bwc_js as $key => $value) {
        $content .= "'$key' => '$value',\n";
    }

    $content .= ");";

    file_put_contents(dotb_cached('bwc_js.php'), $content);
}

function checkDuplicatedCache($name) {
    if (file_exists(dotb_cached('javascript'))) {
        foreach (DotbAutoLoader::existing(glob(dotb_cached('javascript/*js')), GLOB_ONLYDIR|GLOB_NOSORT) as $file) {
            if (explode(".", basename($file))[0] === explode(".", $name)[0]) {
                return $file;
            }
        }
    }
    return '';
}
?>