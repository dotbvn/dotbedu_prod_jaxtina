<?php


/**
*
* Custom Query - Role Teacher - By Tuấn Nguyễn
*
*/
class TeacherVisibility extends DotbVisibility
{
    /**
    * {@inheritdoc}
    */
    public function addVisibilityWhere(&$query)
    {
        //Custom Query - Role Teacher
        if(empty($GLOBALS['current_user']->teacher_id)){
            $sqlTeacher = "SELECT id FROM c_teachers WHERE user_id = '{$GLOBALS['current_user']->id}' AND deleted = 0";
            $teacher_id = $GLOBALS['db']->getOne($sqlTeacher);
        } else {
            $teacher_id = $GLOBALS['current_user']->teacher_id;
        }

        if($teacher_id) {
            $custom_where = " IFNULL(j_gradebook.id,'') IN (SELECT DISTINCT IFNULL(u2.j_class_j_gradebook_1j_gradebook_idb, '') gradebook_id
            FROM j_class u1
            INNER JOIN j_class_j_gradebook_1_c u2 ON u1.id = u2.j_class_j_gradebook_1j_class_ida AND u2.deleted = 0
            INNER JOIN meetings l1 ON u1.id = l1.ju_class_id AND l1.deleted = 0
            WHERE ('{$teacher_id}' IN (l1.teacher_id, l1.teacher_cover_id, l1.sub_teacher_id, u1.teacher_id))
            AND l1.session_status <> 'Cancelled')";
            if (!empty($query)) $query .= " AND $custom_where";
            else $query = $custom_where;
        }
        return $query;
    }

    /**
    * {@inheritdoc}
    */
    public function addVisibilityWhereQuery(DotbQuery $dotbQuery, $options = array())
    {
        $where = null;
        $this->addVisibilityWhere($where, $options);

        if (!empty($where)) {
            $dotbQuery->where()->queryAnd()->addRaw($where);
        }

        return $dotbQuery;
    }
}
