<?php


/**
*
* Custom Query For Class - Role Teacher - By HoHoangHvy
*
*/
class ClassVisibility extends DotbVisibility
{
    /**
    * {@inheritdoc}
    */
    public function addVisibilityWhere(&$query)
    {
        if(empty($GLOBALS['current_user']->teacher_id)){
            $sqlTeacher = "SELECT id FROM c_teachers WHERE user_id = '{$GLOBALS['current_user']->id}' AND deleted = 0";
            $teacher_id = $GLOBALS['db']->getOne($sqlTeacher);
        } else {
            $teacher_id = $GLOBALS['current_user']->teacher_id;
        }
        //Custom Query - Role Teacher
        if(!empty($teacher_id)) {
            $custom_where .= " IFNULL(j_class.id,'') IN (
        SELECT DISTINCT IFNULL(j_class.id, '') class_id
        FROM j_class
        INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
            AND ('{$teacher_id}' IN (l1.teacher_id, l1.teacher_cover_id, l1.sub_teacher_id, j_class.teacher_id))
            AND l1.session_status <> 'Cancelled')";
            if (!empty($query)) $query .= " AND $custom_where";
            else $query = $custom_where;

        }

        return $query;
    }

    /**
    * {@inheritdoc}
    */
    public function addVisibilityWhereQuery(DotbQuery $dotbQuery, $options = array())
    {
        $where = null;
        $this->addVisibilityWhere($where, $options);

        if (!empty($where)) {
            $dotbQuery->where()->queryAnd()->addRaw($where);
        }

        return $dotbQuery;
    }
}
