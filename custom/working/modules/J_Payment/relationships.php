<?php

$relationships = array (
  'j_coursefee_j_payment_1' => 
  array (
    'name' => 'j_coursefee_j_payment_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_coursefee_j_payment_1' => 
      array (
        'lhs_module' => 'J_Coursefee',
        'lhs_table' => 'j_coursefee',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Payment',
        'rhs_table' => 'j_payment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_coursefee_j_payment_1_c',
        'join_key_lhs' => 'j_coursefee_j_payment_1j_coursefee_ida',
        'join_key_rhs' => 'j_coursefee_j_payment_1j_payment_idb',
      ),
    ),
    'table' => 'j_coursefee_j_payment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_coursefee_j_payment_1j_coursefee_ida' => 
      array (
        'name' => 'j_coursefee_j_payment_1j_coursefee_ida',
        'type' => 'id',
      ),
      'j_coursefee_j_payment_1j_payment_idb' => 
      array (
        'name' => 'j_coursefee_j_payment_1j_payment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_coursefee_j_payment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_coursefee_j_payment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_coursefee_j_payment_1j_coursefee_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_coursefee_j_payment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_coursefee_j_payment_1j_payment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_coursefee_j_payment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_coursefee_j_payment_1j_payment_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Coursefee',
    'lhs_table' => 'j_coursefee',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_coursefee_j_payment_1_c',
    'join_key_lhs' => 'j_coursefee_j_payment_1j_coursefee_ida',
    'join_key_rhs' => 'j_coursefee_j_payment_1j_payment_idb',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_j_payment_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_coursefee_j_payment_2' => 
  array (
    'name' => 'j_coursefee_j_payment_2',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_coursefee_j_payment_2' => 
      array (
        'lhs_module' => 'J_Coursefee',
        'lhs_table' => 'j_coursefee',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Payment',
        'rhs_table' => 'j_payment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_coursefee_j_payment_2_c',
        'join_key_lhs' => 'j_coursefee_j_payment_2j_coursefee_ida',
        'join_key_rhs' => 'j_coursefee_j_payment_2j_payment_idb',
      ),
    ),
    'table' => 'j_coursefee_j_payment_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_coursefee_j_payment_2j_coursefee_ida' => 
      array (
        'name' => 'j_coursefee_j_payment_2j_coursefee_ida',
        'type' => 'id',
      ),
      'j_coursefee_j_payment_2j_payment_idb' => 
      array (
        'name' => 'j_coursefee_j_payment_2j_payment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_coursefee_j_payment_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_coursefee_j_payment_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_coursefee_j_payment_2j_coursefee_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_coursefee_j_payment_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_coursefee_j_payment_2j_payment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_coursefee_j_payment_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_coursefee_j_payment_2j_coursefee_ida',
          1 => 'j_coursefee_j_payment_2j_payment_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Coursefee',
    'lhs_table' => 'j_coursefee',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'j_coursefee_j_payment_2_c',
    'join_key_lhs' => 'j_coursefee_j_payment_2j_coursefee_ida',
    'join_key_rhs' => 'j_coursefee_j_payment_2j_payment_idb',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_j_payment_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_payment_j_discount_1' => 
  array (
    'name' => 'j_payment_j_discount_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_payment_j_discount_1' => 
      array (
        'lhs_module' => 'J_Payment',
        'lhs_table' => 'j_payment',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Discount',
        'rhs_table' => 'j_discount',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_payment_j_discount_1_c',
        'join_key_lhs' => 'j_payment_j_discount_1j_payment_ida',
        'join_key_rhs' => 'j_payment_j_discount_1j_discount_idb',
      ),
    ),
    'table' => 'j_payment_j_discount_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_payment_j_discount_1j_payment_ida' => 
      array (
        'name' => 'j_payment_j_discount_1j_payment_ida',
        'type' => 'id',
      ),
      'j_payment_j_discount_1j_discount_idb' => 
      array (
        'name' => 'j_payment_j_discount_1j_discount_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_payment_j_discount_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_payment_j_discount_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_payment_j_discount_1j_payment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_payment_j_discount_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_payment_j_discount_1j_discount_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_payment_j_discount_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_payment_j_discount_1j_payment_ida',
          1 => 'j_payment_j_discount_1j_discount_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Discount',
    'rhs_table' => 'j_discount',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'j_payment_j_discount_1_c',
    'join_key_lhs' => 'j_payment_j_discount_1j_payment_ida',
    'join_key_rhs' => 'j_payment_j_discount_1j_discount_idb',
    'readonly' => true,
    'relationship_name' => 'j_payment_j_discount_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_payment_j_inventory_1' => 
  array (
    'name' => 'j_payment_j_inventory_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_payment_j_inventory_1' => 
      array (
        'lhs_module' => 'J_Payment',
        'lhs_table' => 'j_payment',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Inventory',
        'rhs_table' => 'j_inventory',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_payment_j_inventory_1_c',
        'join_key_lhs' => 'j_payment_j_inventory_1j_payment_ida',
        'join_key_rhs' => 'j_payment_j_inventory_1j_inventory_idb',
      ),
    ),
    'table' => 'j_payment_j_inventory_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_payment_j_inventory_1j_payment_ida' => 
      array (
        'name' => 'j_payment_j_inventory_1j_payment_ida',
        'type' => 'id',
      ),
      'j_payment_j_inventory_1j_inventory_idb' => 
      array (
        'name' => 'j_payment_j_inventory_1j_inventory_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_payment_j_inventory_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_payment_j_inventory_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_payment_j_inventory_1j_payment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_payment_j_inventory_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_payment_j_inventory_1j_inventory_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_payment_j_inventory_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_payment_j_inventory_1j_inventory_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventory',
    'rhs_table' => 'j_inventory',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_payment_j_inventory_1_c',
    'join_key_lhs' => 'j_payment_j_inventory_1j_payment_ida',
    'join_key_rhs' => 'j_payment_j_inventory_1j_inventory_idb',
    'readonly' => true,
    'relationship_name' => 'j_payment_j_inventory_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_payment_j_payment_1' => 
  array (
    'name' => 'j_payment_j_payment_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_payment_j_payment_1' => 
      array (
        'lhs_module' => 'J_Payment',
        'lhs_table' => 'j_payment',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Payment',
        'rhs_table' => 'j_payment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_payment_j_payment_1_c',
        'join_key_lhs' => 'payment_ida',
        'join_key_rhs' => 'payment_idb',
      ),
    ),
    'table' => 'j_payment_j_payment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'payment_ida' => 
      array (
        'name' => 'payment_ida',
        'type' => 'id',
      ),
      'payment_idb' => 
      array (
        'name' => 'payment_idb',
        'type' => 'id',
      ),
      'amount' => 
      array (
        'name' => 'amount',
        'type' => 'decimal',
        'len' => 20,
        'precision' => '6',
      ),
      'hours' => 
      array (
        'name' => 'hours',
        'type' => 'decimal',
        'len' => 13,
        'precision' => '2',
      ),
      'situation_id' => 
      array (
        'name' => 'situation_id',
        'type' => 'id',
      ),
      'issue_date' => 
      array (
        'name' => 'issue_date',
        'type' => 'date',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_payment_j_payment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_payment_ida_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'payment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_payment_idb_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'payment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_payment_j_payment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'payment_idb',
        ),
      ),
      4 => 
      array (
        'name' => 'idx_situation_id',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'situation_id',
        ),
      ),
    ),
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_payment_j_payment_1_c',
    'join_key_lhs' => 'payment_ida',
    'join_key_rhs' => 'payment_idb',
    'readonly' => true,
    'relationship_name' => 'j_payment_j_payment_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_payment_j_payment_2' => 
  array (
    'name' => 'j_payment_j_payment_2',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_payment_j_payment_2' => 
      array (
        'lhs_module' => 'J_Payment',
        'lhs_table' => 'j_payment',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Payment',
        'rhs_table' => 'j_payment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_payment_j_payment_2_c',
        'join_key_lhs' => 'j_payment_j_payment_2j_payment_ida',
        'join_key_rhs' => 'j_payment_j_payment_2j_payment_idb',
      ),
    ),
    'table' => 'j_payment_j_payment_2_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_payment_j_payment_2j_payment_ida' => 
      array (
        'name' => 'j_payment_j_payment_2j_payment_ida',
        'type' => 'id',
      ),
      'j_payment_j_payment_2j_payment_idb' => 
      array (
        'name' => 'j_payment_j_payment_2j_payment_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_payment_j_payment_2_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_payment_j_payment_2_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_payment_j_payment_2j_payment_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_payment_j_payment_2_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_payment_j_payment_2j_payment_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_payment_j_payment_2_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_payment_j_payment_2j_payment_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_payment_j_payment_2_c',
    'join_key_lhs' => 'j_payment_j_payment_2j_payment_ida',
    'join_key_rhs' => 'j_payment_j_payment_2j_payment_idb',
    'readonly' => true,
    'relationship_name' => 'j_payment_j_payment_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_invoice_j_payment_1' => 
  array (
    'name' => 'j_invoice_j_payment_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_invoice_j_payment_1' => 
      array (
        'lhs_module' => 'J_Invoice',
        'lhs_table' => 'j_invoice',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Payment',
        'rhs_table' => 'j_payment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_invoice_j_payment_1_c',
        'join_key_lhs' => 'invoice_id',
        'join_key_rhs' => 'payment_id',
      ),
    ),
    'table' => 'j_invoice_j_payment_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'invoice_id' => 
      array (
        'name' => 'invoice_id',
        'type' => 'id',
      ),
      'payment_id' => 
      array (
        'name' => 'payment_id',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_invoice_j_payment_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_invoice_j_payment_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'invoice_id',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_invoice_j_payment_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'payment_id',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_invoice_j_payment_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'invoice_id',
          1 => 'payment_id',
        ),
      ),
    ),
    'lhs_module' => 'J_Invoice',
    'lhs_table' => 'j_invoice',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'j_invoice_j_payment_1_c',
    'join_key_lhs' => 'invoice_id',
    'join_key_rhs' => 'payment_id',
    'readonly' => true,
    'relationship_name' => 'j_invoice_j_payment_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'payment_user_closed_sale' => 
  array (
    'name' => 'payment_user_closed_sale',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'user_closed_sale_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'payment_user_closed_sale',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'payment_user_pt_demo' => 
  array (
    'name' => 'payment_user_pt_demo',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'user_pt_demo_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'payment_user_pt_demo',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_payments' => 
  array (
    'name' => 'lead_payments',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'readonly' => true,
    'relationship_name' => 'lead_payments',
    'rhs_subpanel' => 'ForLeadsNew_payments',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'student_payments' => 
  array (
    'name' => 'student_payments',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'student_payments',
    'rhs_subpanel' => 'ForContactsBookgift_link',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'account_payments' => 
  array (
    'name' => 'account_payments',
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'account_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'account_payments',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_modified_user' => 
  array (
    'name' => 'j_payment_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_payment_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_created_by' => 
  array (
    'name' => 'j_payment_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_payment_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_activities' => 
  array (
    'name' => 'j_payment_activities',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Payment',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'j_payment_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_studentsituations' => 
  array (
    'name' => 'j_payment_studentsituations',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_StudentSituations',
    'rhs_table' => 'j_studentsituations',
    'rhs_key' => 'payment_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_payment_studentsituations',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_j_sponsor' => 
  array (
    'name' => 'j_payment_j_sponsor',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Sponsor',
    'rhs_table' => 'j_sponsor',
    'rhs_key' => 'payment_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_payment_j_sponsor',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_moving_transfer' => 
  array (
    'name' => 'j_payment_moving_transfer',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'payment_out_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_payment_moving_transfer',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'payment_paymentdetails' => 
  array (
    'name' => 'payment_paymentdetails',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_PaymentDetail',
    'rhs_table' => 'j_paymentdetail',
    'rhs_key' => 'payment_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'payment_paymentdetails',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'payment_loyaltys' => 
  array (
    'name' => 'payment_loyaltys',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Loyalty',
    'rhs_table' => 'j_loyalty',
    'rhs_key' => 'payment_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'payment_loyaltys',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'payment_coms' => 
  array (
    'name' => 'payment_coms',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Commission',
    'rhs_table' => 'c_commission',
    'rhs_key' => 'payment_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'payment_coms',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ju_payment_revenue' => 
  array (
    'name' => 'ju_payment_revenue',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'C_DeliveryRevenue',
    'rhs_table' => 'c_deliveryrevenue',
    'rhs_key' => 'ju_payment_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ju_payment_revenue',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'send_messages_payment' => 
  array (
    'name' => 'send_messages_payment',
    'lhs_module' => 'J_Payment',
    'lhs_table' => 'j_payment',
    'lhs_key' => 'id',
    'rhs_module' => 'BMessage',
    'rhs_table' => 'bmessage',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Payment',
    'readonly' => true,
    'relationship_name' => 'send_messages_payment',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_following' => 
  array (
    'name' => 'j_payment_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Payment',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_payment_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_favorite' => 
  array (
    'name' => 'j_payment_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'J_Payment',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_payment_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_payment_assigned_user' => 
  array (
    'name' => 'j_payment_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_payment_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'class_enrollments' => 
  array (
    'name' => 'class_enrollments',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'ju_class_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'class_enrollments',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);