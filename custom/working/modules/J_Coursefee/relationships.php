<?php

$relationships = array (
  'j_coursefee_j_discount_1' =>
  array (
    'name' => 'j_coursefee_j_discount_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_coursefee_j_discount_1' =>
      array (
        'lhs_module' => 'J_Coursefee',
        'lhs_table' => 'j_coursefee',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Discount',
        'rhs_table' => 'j_discount',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_coursefee_j_discount_1_c',
        'join_key_lhs' => 'j_coursefee_j_discount_1j_coursefee_ida',
        'join_key_rhs' => 'j_coursefee_j_discount_1j_discount_idb',
      ),
    ),
    'table' => 'j_coursefee_j_discount_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_coursefee_j_discount_1j_coursefee_ida' =>
      array (
        'name' => 'j_coursefee_j_discount_1j_coursefee_ida',
        'type' => 'id',
      ),
      'j_coursefee_j_discount_1j_discount_idb' =>
      array (
        'name' => 'j_coursefee_j_discount_1j_discount_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_coursefee_j_discount_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_coursefee_j_discount_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_coursefee_j_discount_1j_coursefee_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_coursefee_j_discount_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_coursefee_j_discount_1j_discount_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_coursefee_j_discount_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_coursefee_j_discount_1j_coursefee_ida',
          1 => 'j_coursefee_j_discount_1j_discount_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Coursefee',
    'lhs_table' => 'j_coursefee',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Discount',
    'rhs_table' => 'j_discount',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'j_coursefee_j_discount_1_c',
    'join_key_lhs' => 'j_coursefee_j_discount_1j_coursefee_ida',
    'join_key_rhs' => 'j_coursefee_j_discount_1j_discount_idb',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_j_discount_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_coursefee_j_payment_1' =>
  array (
    'name' => 'j_coursefee_j_payment_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_coursefee_j_payment_1' =>
      array (
        'lhs_module' => 'J_Coursefee',
        'lhs_table' => 'j_coursefee',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Payment',
        'rhs_table' => 'j_payment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_coursefee_j_payment_1_c',
        'join_key_lhs' => 'j_coursefee_j_payment_1j_coursefee_ida',
        'join_key_rhs' => 'j_coursefee_j_payment_1j_payment_idb',
      ),
    ),
    'table' => 'j_coursefee_j_payment_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_coursefee_j_payment_1j_coursefee_ida' =>
      array (
        'name' => 'j_coursefee_j_payment_1j_coursefee_ida',
        'type' => 'id',
      ),
      'j_coursefee_j_payment_1j_payment_idb' =>
      array (
        'name' => 'j_coursefee_j_payment_1j_payment_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_coursefee_j_payment_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_coursefee_j_payment_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_coursefee_j_payment_1j_coursefee_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_coursefee_j_payment_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_coursefee_j_payment_1j_payment_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_coursefee_j_payment_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_coursefee_j_payment_1j_payment_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Coursefee',
    'lhs_table' => 'j_coursefee',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_coursefee_j_payment_1_c',
    'join_key_lhs' => 'j_coursefee_j_payment_1j_coursefee_ida',
    'join_key_rhs' => 'j_coursefee_j_payment_1j_payment_idb',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_j_payment_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_coursefee_j_payment_2' =>
  array (
    'name' => 'j_coursefee_j_payment_2',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_coursefee_j_payment_2' =>
      array (
        'lhs_module' => 'J_Coursefee',
        'lhs_table' => 'j_coursefee',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Payment',
        'rhs_table' => 'j_payment',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_coursefee_j_payment_2_c',
        'join_key_lhs' => 'j_coursefee_j_payment_2j_coursefee_ida',
        'join_key_rhs' => 'j_coursefee_j_payment_2j_payment_idb',
      ),
    ),
    'table' => 'j_coursefee_j_payment_2_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_coursefee_j_payment_2j_coursefee_ida' =>
      array (
        'name' => 'j_coursefee_j_payment_2j_coursefee_ida',
        'type' => 'id',
      ),
      'j_coursefee_j_payment_2j_payment_idb' =>
      array (
        'name' => 'j_coursefee_j_payment_2j_payment_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_coursefee_j_payment_2_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_coursefee_j_payment_2_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_coursefee_j_payment_2j_coursefee_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_coursefee_j_payment_2_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_coursefee_j_payment_2j_payment_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_coursefee_j_payment_2_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_coursefee_j_payment_2j_coursefee_ida',
          1 => 'j_coursefee_j_payment_2j_payment_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Coursefee',
    'lhs_table' => 'j_coursefee',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'j_coursefee_j_payment_2_c',
    'join_key_lhs' => 'j_coursefee_j_payment_2j_coursefee_ida',
    'join_key_rhs' => 'j_coursefee_j_payment_2j_payment_idb',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_j_payment_2',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_coursefee_modified_user' =>
  array (
    'name' => 'j_coursefee_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Coursefee',
    'rhs_table' => 'j_coursefee',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_coursefee_created_by' =>
  array (
    'name' => 'j_coursefee_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Coursefee',
    'rhs_table' => 'j_coursefee',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_coursefee_activities' =>
  array (
    'name' => 'j_coursefee_activities',
    'lhs_module' => 'J_Coursefee',
    'lhs_table' => 'j_coursefee',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Coursefee',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' =>
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' =>
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' =>
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' =>
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'j_coursefee_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'coursefee_inventorydetails' =>
  array (
    'name' => 'coursefee_inventorydetails',
    'lhs_module' => 'J_Coursefee',
    'lhs_table' => 'j_coursefee',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventorydetail',
    'rhs_table' => 'j_inventorydetail',
    'rhs_key' => 'coursefee_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'coursefee_inventorydetails',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_coursefee_following' =>
  array (
    'name' => 'j_coursefee_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Coursefee',
    'rhs_table' => 'j_coursefee',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Coursefee',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_coursefee_favorite' =>
  array (
    'name' => 'j_coursefee_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Coursefee',
    'rhs_table' => 'j_coursefee',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'J_Coursefee',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_coursefee_assigned_user' =>
  array (
    'name' => 'j_coursefee_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Coursefee',
    'rhs_table' => 'j_coursefee',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_coursefee_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_coursefee_j_kindofcourse_1' =>
  array (
    'rhs_label' => 'Apply with Kind of Courses',
    'lhs_label' => 'Course Fees',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'J_Coursefee',
    'rhs_module' => 'J_Kindofcourse',
    'relationship_type' => 'many-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'j_coursefee_j_kindofcourse_1',
  ),
);