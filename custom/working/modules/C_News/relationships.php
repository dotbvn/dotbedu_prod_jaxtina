<?php

$relationships = array (
  'c_news_contacts_1' => 
  array (
    'name' => 'c_news_contacts_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'c_news_contacts_1' => 
      array (
        'lhs_module' => 'C_News',
        'lhs_table' => 'c_news',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_news_contacts_1_c',
        'join_key_lhs' => 'c_news_contacts_1c_news_ida',
        'join_key_rhs' => 'c_news_contacts_1contacts_idb',
      ),
    ),
    'table' => 'c_news_contacts_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_news_contacts_1c_news_ida' => 
      array (
        'name' => 'c_news_contacts_1c_news_ida',
        'type' => 'id',
      ),
      'c_news_contacts_1contacts_idb' => 
      array (
        'name' => 'c_news_contacts_1contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_c_news_contacts_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_c_news_contacts_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_news_contacts_1c_news_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_c_news_contacts_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_news_contacts_1contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'c_news_contacts_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'c_news_contacts_1c_news_ida',
          1 => 'c_news_contacts_1contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_News',
    'lhs_table' => 'c_news',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'c_news_contacts_1_c',
    'join_key_lhs' => 'c_news_contacts_1c_news_ida',
    'join_key_rhs' => 'c_news_contacts_1contacts_idb',
    'readonly' => true,
    'relationship_name' => 'c_news_contacts_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'c_news_c_teachers_1' => 
  array (
    'name' => 'c_news_c_teachers_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'c_news_c_teachers_1' => 
      array (
        'lhs_module' => 'C_News',
        'lhs_table' => 'c_news',
        'lhs_key' => 'id',
        'rhs_module' => 'C_Teachers',
        'rhs_table' => 'c_teachers',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_news_c_teachers_1_c',
        'join_key_lhs' => 'c_news_c_teachers_1c_news_ida',
        'join_key_rhs' => 'c_news_c_teachers_1c_teachers_idb',
      ),
    ),
    'table' => 'c_news_c_teachers_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_news_c_teachers_1c_news_ida' => 
      array (
        'name' => 'c_news_c_teachers_1c_news_ida',
        'type' => 'id',
      ),
      'c_news_c_teachers_1c_teachers_idb' => 
      array (
        'name' => 'c_news_c_teachers_1c_teachers_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_c_news_c_teachers_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_c_news_c_teachers_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_news_c_teachers_1c_news_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_c_news_c_teachers_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_news_c_teachers_1c_teachers_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'c_news_c_teachers_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'c_news_c_teachers_1c_news_ida',
          1 => 'c_news_c_teachers_1c_teachers_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_News',
    'lhs_table' => 'c_news',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'c_news_c_teachers_1_c',
    'join_key_lhs' => 'c_news_c_teachers_1c_news_ida',
    'join_key_rhs' => 'c_news_c_teachers_1c_teachers_idb',
    'readonly' => true,
    'relationship_name' => 'c_news_c_teachers_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'c_news_modified_user' => 
  array (
    'name' => 'c_news_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_News',
    'rhs_table' => 'c_news',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_news_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_news_created_by' => 
  array (
    'name' => 'c_news_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_News',
    'rhs_table' => 'c_news',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_news_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_news_activities' => 
  array (
    'name' => 'c_news_activities',
    'lhs_module' => 'C_News',
    'lhs_table' => 'c_news',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_News',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'c_news_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_news_following' => 
  array (
    'name' => 'c_news_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_News',
    'rhs_table' => 'c_news',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_News',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_news_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_news_favorite' => 
  array (
    'name' => 'c_news_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_News',
    'rhs_table' => 'c_news',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'C_News',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_news_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_news_assigned_user' => 
  array (
    'name' => 'c_news_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_News',
    'rhs_table' => 'c_news',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_news_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_news_contacts_2' => 
  array (
    'rhs_label' => 'Students',
    'lhs_label' => 'News',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'C_News',
    'rhs_module' => 'Contacts',
    'relationship_type' => 'many-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'c_news_contacts_2',
  ),
);