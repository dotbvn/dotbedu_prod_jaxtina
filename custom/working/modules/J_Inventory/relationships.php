<?php

$relationships = array (
  'j_class_j_inventory_1' => 
  array (
    'name' => 'j_class_j_inventory_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_class_j_inventory_1' => 
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Inventory',
        'rhs_table' => 'j_inventory',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_j_inventory_1_c',
        'join_key_lhs' => 'j_class_j_inventory_1j_class_ida',
        'join_key_rhs' => 'j_class_j_inventory_1j_inventory_idb',
      ),
    ),
    'table' => 'j_class_j_inventory_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_j_inventory_1j_class_ida' => 
      array (
        'name' => 'j_class_j_inventory_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_j_inventory_1j_inventory_idb' => 
      array (
        'name' => 'j_class_j_inventory_1j_inventory_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_class_j_inventory_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_class_j_inventory_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_class_j_inventory_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_class_j_inventory_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_class_j_inventory_1j_inventory_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_class_j_inventory_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_class_j_inventory_1j_inventory_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventory',
    'rhs_table' => 'j_inventory',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_j_inventory_1_c',
    'join_key_lhs' => 'j_class_j_inventory_1j_class_ida',
    'join_key_rhs' => 'j_class_j_inventory_1j_inventory_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_j_inventory_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_inventory_modified_user' => 
  array (
    'name' => 'j_inventory_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventory',
    'rhs_table' => 'j_inventory',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_inventory_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_inventory_created_by' => 
  array (
    'name' => 'j_inventory_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventory',
    'rhs_table' => 'j_inventory',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_inventory_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_inventory_activities' => 
  array (
    'name' => 'j_inventory_activities',
    'lhs_module' => 'J_Inventory',
    'lhs_table' => 'j_inventory',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Inventory',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'j_inventory_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'inventory_inventorydetails' => 
  array (
    'name' => 'inventory_inventorydetails',
    'lhs_module' => 'J_Inventory',
    'lhs_table' => 'j_inventory',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventorydetail',
    'rhs_table' => 'j_inventorydetail',
    'rhs_key' => 'inventory_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'inventory_inventorydetails',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_inventory_following' => 
  array (
    'name' => 'j_inventory_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventory',
    'rhs_table' => 'j_inventory',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Inventory',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_inventory_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_inventory_favorite' => 
  array (
    'name' => 'j_inventory_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventory',
    'rhs_table' => 'j_inventory',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'J_Inventory',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_inventory_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_inventory_assigned_user' => 
  array (
    'name' => 'j_inventory_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Inventory',
    'rhs_table' => 'j_inventory',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_inventory_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);