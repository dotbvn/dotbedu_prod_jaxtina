<?php

$relationships = array (
  'meetings_contacts' =>
  array (
    'name' => 'meetings_contacts',
    'table' => 'meetings_contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => '36',
      ),
      'meeting_id' =>
      array (
        'name' => 'meeting_id',
        'type' => 'id',
        'len' => '36',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
        'len' => '36',
      ),
      'required' =>
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' =>
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
      0 =>
      array (
        'name' => 'contract_id',
        'type' => 'id',
        'len' => '36',
      ),
      1 =>
      array (
        'name' => 'situation_id',
        'type' => 'id',
        'len' => '36',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'meetings_contactspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_mtg_mtg',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'meeting_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_mtg_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_meeting_contact',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'meeting_id',
          1 => 'contact_id',
        ),
      ),
      4 =>
      array (
        'name' => 'idx_con_mtg_situa',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'situation_id',
        ),
      ),
      5 =>
      array (
        'name' => 'idx_con_mtg_contract',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contract_id',
        ),
      ),
      6 =>
      array (
        'name' => 'idx_meeting_contact_situa',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'meeting_id',
          1 => 'contact_id',
          2 => 'situation_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'meetings_contacts' =>
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_contacts',
        'join_key_lhs' => 'meeting_id',
        'join_key_rhs' => 'contact_id',
      ),
      'meetings_situations' =>
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'J_StudentSituations',
        'rhs_table' => 'j_studentsituations',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_contacts',
        'join_key_lhs' => 'meeting_id',
        'join_key_rhs' => 'situation_id',
      ),
    ),
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'meetings_contacts',
    'join_key_lhs' => 'meeting_id',
    'join_key_rhs' => 'contact_id',
    'readonly' => true,
    'relationship_name' => 'meetings_contacts',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'ForContactsMeetings',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_situations' =>
  array (
    'name' => 'meetings_situations',
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'J_StudentSituations',
    'rhs_table' => 'j_studentsituations',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'meetings_contacts',
    'join_key_lhs' => 'meeting_id',
    'join_key_rhs' => 'situation_id',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => '36',
      ),
      'meeting_id' =>
      array (
        'name' => 'meeting_id',
        'type' => 'id',
        'len' => '36',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
        'len' => '36',
      ),
      'required' =>
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' =>
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
      0 =>
      array (
        'name' => 'contract_id',
        'type' => 'id',
        'len' => '36',
      ),
      1 =>
      array (
        'name' => 'situation_id',
        'type' => 'id',
        'len' => '36',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'meetings_situations',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_users' =>
  array (
    'name' => 'meetings_users',
    'table' => 'meetings_users',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => '36',
      ),
      'meeting_id' =>
      array (
        'name' => 'meeting_id',
        'type' => 'id',
        'len' => '36',
      ),
      'user_id' =>
      array (
        'name' => 'user_id',
        'type' => 'id',
        'len' => '36',
      ),
      'required' =>
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' =>
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'meetings_userspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_usr_mtg_mtg',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'meeting_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_usr_mtg_usr',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'user_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_meeting_users',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'meeting_id',
          1 => 'user_id',
        ),
      ),
      4 =>
      array (
        'name' => 'idx_meeting_users_del',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'meeting_id',
          1 => 'user_id',
          2 => 'deleted',
        ),
      ),
    ),
    'relationships' =>
    array (
      'meetings_users' =>
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'Users',
        'rhs_table' => 'users',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_users',
        'join_key_lhs' => 'meeting_id',
        'join_key_rhs' => 'user_id',
      ),
    ),
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Users',
    'rhs_table' => 'users',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'meetings_users',
    'join_key_lhs' => 'meeting_id',
    'join_key_rhs' => 'user_id',
    'readonly' => true,
    'relationship_name' => 'meetings_users',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_leads' =>
  array (
    'name' => 'meetings_leads',
    'table' => 'meetings_leads',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => '36',
      ),
      'meeting_id' =>
      array (
        'name' => 'meeting_id',
        'type' => 'id',
        'len' => '36',
      ),
      'lead_id' =>
      array (
        'name' => 'lead_id',
        'type' => 'id',
        'len' => '36',
      ),
      'required' =>
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' =>
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
      0 =>
      array (
        'name' => 'situation_id',
        'type' => 'id',
        'len' => '36',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'meetings_leadspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_lead_meeting_meeting',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'meeting_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_lead_meeting_lead',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'lead_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_meeting_lead',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'meeting_id',
          1 => 'lead_id',
        ),
      ),
      4 =>
      array (
        'name' => 'idx_situation_id',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'situation_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'meetings_leads' =>
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'Leads',
        'rhs_table' => 'leads',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_leads',
        'join_key_lhs' => 'meeting_id',
        'join_key_rhs' => 'lead_id',
      ),
    ),
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'meetings_leads',
    'join_key_lhs' => 'meeting_id',
    'join_key_rhs' => 'lead_id',
    'readonly' => true,
    'relationship_name' => 'meetings_leads',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'ForLeadsMeetings',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'case_meetings' =>
  array (
    'name' => 'case_meetings',
    'lhs_module' => 'Cases',
    'lhs_table' => 'cases',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Cases',
    'readonly' => true,
    'relationship_name' => 'case_meetings',
    'rhs_subpanel' => 'ForCasesMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_modified_user' =>
  array (
    'name' => 'meetings_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'meetings_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_created_by' =>
  array (
    'name' => 'meetings_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'meetings_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meeting_activities' =>
  array (
    'name' => 'meeting_activities',
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Meetings',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' =>
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' =>
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' =>
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' =>
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'meeting_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_assigned_user' =>
  array (
    'name' => 'meetings_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'meetings_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_notes' =>
  array (
    'name' => 'meetings_notes',
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Meetings',
    'readonly' => true,
    'relationship_name' => 'meetings_notes',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'session_revenue' =>
  array (
    'name' => 'session_revenue',
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'C_DeliveryRevenue',
    'rhs_table' => 'c_deliveryrevenue',
    'rhs_key' => 'session_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'session_revenue',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meeting_attendances' =>
  array (
    'name' => 'meeting_attendances',
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Attendance',
    'rhs_table' => 'c_attendance',
    'rhs_key' => 'meeting_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'meeting_attendances',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_following' =>
  array (
    'name' => 'meetings_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Meetings',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'meetings_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_favorite' =>
  array (
    'name' => 'meetings_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'Meetings',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'meetings_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_flex_relate_dri_workflows' =>
  array (
    'name' => 'meetings_flex_relate_dri_workflows',
    'lhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'rhs_module' => 'DRI_Workflows',
    'rhs_table' => 'dri_workflows',
    'relationship_role_column_value' => 'Meetings',
    'relationship_role_column' => 'parent_type',
    'readonly' => true,
    'relationship_name' => 'meetings_flex_relate_dri_workflows',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meeting_dri_workflow_templates' =>
  array (
    'name' => 'meeting_dri_workflow_templates',
    'relationship_type' => 'one-to-many',
    'lhs_key' => 'id',
    'lhs_module' => 'DRI_Workflow_Templates',
    'lhs_table' => 'dri_workflow_templates',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'dri_workflow_template_id',
    'readonly' => true,
    'relationship_name' => 'meeting_dri_workflow_templates',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meeting_dri_subworkflow_templates' =>
  array (
    'name' => 'meeting_dri_subworkflow_templates',
    'relationship_type' => 'one-to-many',
    'lhs_key' => 'id',
    'lhs_module' => 'DRI_SubWorkflow_Templates',
    'lhs_table' => 'dri_subworkflow_templates',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'dri_subworkflow_template_id',
    'readonly' => true,
    'relationship_name' => 'meeting_dri_subworkflow_templates',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meeting_dri_workflow_task_templates' =>
  array (
    'name' => 'meeting_dri_workflow_task_templates',
    'relationship_type' => 'one-to-many',
    'lhs_key' => 'id',
    'lhs_module' => 'DRI_Workflow_Task_Templates',
    'lhs_table' => 'dri_workflow_task_templates',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'dri_workflow_task_template_id',
    'readonly' => true,
    'relationship_name' => 'meeting_dri_workflow_task_templates',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meeting_dri_subworkflows' =>
  array (
    'name' => 'meeting_dri_subworkflows',
    'relationship_type' => 'one-to-many',
    'lhs_key' => 'id',
    'lhs_module' => 'DRI_SubWorkflows',
    'lhs_table' => 'dri_subworkflows',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'dri_subworkflow_id',
    'readonly' => true,
    'relationship_name' => 'meeting_dri_subworkflows',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'task_meetings_parent' =>
  array (
    'name' => 'task_meetings_parent',
    'lhs_module' => 'Tasks',
    'lhs_table' => 'tasks',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Tasks',
    'readonly' => true,
    'relationship_name' => 'task_meetings_parent',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'lead_meetings' =>
  array (
    'name' => 'lead_meetings',
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
    'readonly' => true,
    'relationship_name' => 'lead_meetings',
    'rhs_subpanel' => 'ForLeadsMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_meetings_parent' =>
  array (
    'name' => 'contact_meetings_parent',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'contact_meetings_parent',
    'rhs_subpanel' => 'ForContactsMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'account_meetings' =>
  array (
    'name' => 'account_meetings',
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Accounts',
    'readonly' => true,
    'relationship_name' => 'account_meetings',
    'rhs_subpanel' => 'ForAccountsMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'opportunity_meetings' =>
  array (
    'name' => 'opportunity_meetings',
    'lhs_module' => 'Opportunities',
    'lhs_table' => 'opportunities',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Opportunities',
    'readonly' => true,
    'relationship_name' => 'opportunity_meetings',
    'rhs_subpanel' => 'ForOpportunitiesMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'prospect_meetings' =>
  array (
    'name' => 'prospect_meetings',
    'lhs_module' => 'Prospects',
    'lhs_table' => 'prospects',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Prospects',
    'readonly' => true,
    'relationship_name' => 'prospect_meetings',
    'rhs_subpanel' => 'ForProspectsMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'quote_meetings' =>
  array (
    'name' => 'quote_meetings',
    'lhs_module' => 'Quotes',
    'lhs_table' => 'quotes',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Quotes',
    'readonly' => true,
    'relationship_name' => 'quote_meetings',
    'rhs_subpanel' => 'ForQuotesMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'product_meetings' =>
  array (
    'name' => 'product_meetings',
    'lhs_module' => 'Products',
    'lhs_table' => 'products',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Products',
    'readonly' => true,
    'relationship_name' => 'product_meetings',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'bug_meetings' =>
  array (
    'name' => 'bug_meetings',
    'lhs_module' => 'Bugs',
    'lhs_table' => 'bugs',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Bugs',
    'readonly' => true,
    'relationship_name' => 'bug_meetings',
    'rhs_subpanel' => 'ForBugsMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'kbcontent_meetings' =>
  array (
    'name' => 'kbcontent_meetings',
    'lhs_module' => 'KBContents',
    'lhs_table' => 'kbcontents',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'KBContents',
    'readonly' => true,
    'relationship_name' => 'kbcontent_meetings',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'ju_rooms_meetings' =>
  array (
    'name' => 'ju_rooms_meetings',
    'lhs_module' => 'C_Rooms',
    'lhs_table' => 'c_rooms',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'room_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'ju_rooms_meetings',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_timesheet_meeting' =>
  array (
    'name' => 'c_timesheet_meeting',
    'lhs_module' => 'C_Timesheet',
    'lhs_table' => 'c_timesheet',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'timesheet_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_timesheet_meeting',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_meetings' =>
  array (
    'name' => 'j_class_meetings',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'ju_class_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_class_meetings',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'revenuelineitem_meetings' =>
  array (
    'name' => 'revenuelineitem_meetings',
    'lhs_module' => 'RevenueLineItems',
    'lhs_table' => 'revenue_line_items',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'RevenueLineItems',
    'readonly' => true,
    'relationship_name' => 'revenuelineitem_meetings',
    'rhs_subpanel' => 'ForRevenuelineitemsMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'project_tasks_meetings' =>
  array (
    'name' => 'project_tasks_meetings',
    'lhs_module' => 'ProjectTask',
    'lhs_table' => 'project_task',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'ProjectTask',
    'readonly' => true,
    'relationship_name' => 'project_tasks_meetings',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);