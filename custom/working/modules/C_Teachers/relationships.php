<?php

$relationships = array (
  'c_teachers_j_gradebook_1' => 
  array (
    'name' => 'c_teachers_j_gradebook_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'c_teachers_j_gradebook_1' => 
      array (
        'lhs_module' => 'C_Teachers',
        'lhs_table' => 'c_teachers',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Gradebook',
        'rhs_table' => 'j_gradebook',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_teachers_j_gradebook_1_c',
        'join_key_lhs' => 'c_teachers_j_gradebook_1c_teachers_ida',
        'join_key_rhs' => 'c_teachers_j_gradebook_1j_gradebook_idb',
      ),
    ),
    'table' => 'c_teachers_j_gradebook_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_teachers_j_gradebook_1c_teachers_ida' => 
      array (
        'name' => 'c_teachers_j_gradebook_1c_teachers_ida',
        'type' => 'id',
      ),
      'c_teachers_j_gradebook_1j_gradebook_idb' => 
      array (
        'name' => 'c_teachers_j_gradebook_1j_gradebook_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_c_teachers_j_gradebook_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_c_teachers_j_gradebook_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_teachers_j_gradebook_1c_teachers_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_c_teachers_j_gradebook_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_teachers_j_gradebook_1j_gradebook_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'c_teachers_j_gradebook_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'c_teachers_j_gradebook_1j_gradebook_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Gradebook',
    'rhs_table' => 'j_gradebook',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'c_teachers_j_gradebook_1_c',
    'join_key_lhs' => 'c_teachers_j_gradebook_1c_teachers_ida',
    'join_key_rhs' => 'c_teachers_j_gradebook_1j_gradebook_idb',
    'readonly' => true,
    'relationship_name' => 'c_teachers_j_gradebook_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'c_teachers_j_teachercontract_1' => 
  array (
    'name' => 'c_teachers_j_teachercontract_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'c_teachers_j_teachercontract_1' => 
      array (
        'lhs_module' => 'C_Teachers',
        'lhs_table' => 'c_teachers',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Teachercontract',
        'rhs_table' => 'j_teachercontract',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_teachers_j_teachercontract_1_c',
        'join_key_lhs' => 'c_teachers_j_teachercontract_1c_teachers_ida',
        'join_key_rhs' => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
      ),
    ),
    'table' => 'c_teachers_j_teachercontract_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_teachers_j_teachercontract_1c_teachers_ida' => 
      array (
        'name' => 'c_teachers_j_teachercontract_1c_teachers_ida',
        'type' => 'id',
      ),
      'c_teachers_j_teachercontract_1j_teachercontract_idb' => 
      array (
        'name' => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_c_teachers_j_teachercontract_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_c_teachers_j_teachercontract_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_teachers_j_teachercontract_1c_teachers_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_c_teachers_j_teachercontract_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'c_teachers_j_teachercontract_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Teachercontract',
    'rhs_table' => 'j_teachercontract',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'c_teachers_j_teachercontract_1_c',
    'join_key_lhs' => 'c_teachers_j_teachercontract_1c_teachers_ida',
    'join_key_rhs' => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
    'readonly' => true,
    'relationship_name' => 'c_teachers_j_teachercontract_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_class_c_teachers_1' => 
  array (
    'name' => 'j_class_c_teachers_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_class_c_teachers_1' => 
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'C_Teachers',
        'rhs_table' => 'c_teachers',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_c_teachers_1_c',
        'join_key_lhs' => 'j_class_c_teachers_1j_class_ida',
        'join_key_rhs' => 'j_class_c_teachers_1c_teachers_idb',
      ),
    ),
    'table' => 'j_class_c_teachers_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_c_teachers_1j_class_ida' => 
      array (
        'name' => 'j_class_c_teachers_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_c_teachers_1c_teachers_idb' => 
      array (
        'name' => 'j_class_c_teachers_1c_teachers_idb',
        'type' => 'id',
      ),
      'lms_enrollment_id' => 
      array (
        'name' => 'lms_enrollment_id',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_class_c_teachers_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_class_c_teachers_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_class_c_teachers_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_class_c_teachers_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_class_c_teachers_1c_teachers_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_class_c_teachers_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_class_c_teachers_1j_class_ida',
          1 => 'j_class_c_teachers_1c_teachers_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'j_class_c_teachers_1_c',
    'join_key_lhs' => 'j_class_c_teachers_1j_class_ida',
    'join_key_rhs' => 'j_class_c_teachers_1c_teachers_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_c_teachers_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_teacherqe_c_teachers' => 
  array (
    'name' => 'j_teacherqe_c_teachers',
    'true_relationship_type' => 'one-to-many',
    'relationships' => 
    array (
      'j_teacherqe_c_teachers' => 
      array (
        'lhs_module' => 'C_Teachers',
        'lhs_table' => 'c_teachers',
        'lhs_key' => 'id',
        'rhs_module' => 'J_TeacherQE',
        'rhs_table' => 'j_teacherqe',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_teacherqe_c_teachers_c',
        'join_key_lhs' => 'j_teacherqe_c_teachersc_teachers_ida',
        'join_key_rhs' => 'j_teacherqe_c_teachersj_teacherqe_idb',
      ),
    ),
    'table' => 'j_teacherqe_c_teachers_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_teacherqe_c_teachersc_teachers_ida' => 
      array (
        'name' => 'j_teacherqe_c_teachersc_teachers_ida',
        'type' => 'id',
      ),
      'j_teacherqe_c_teachersj_teacherqe_idb' => 
      array (
        'name' => 'j_teacherqe_c_teachersj_teacherqe_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_teacherqe_c_teachers_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_teacherqe_c_teachers_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_teacherqe_c_teachersc_teachers_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_teacherqe_c_teachers_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_teacherqe_c_teachersj_teacherqe_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_teacherqe_c_teachers_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_teacherqe_c_teachersj_teacherqe_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'J_TeacherQE',
    'rhs_table' => 'j_teacherqe',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_teacherqe_c_teachers_c',
    'join_key_lhs' => 'j_teacherqe_c_teachersc_teachers_ida',
    'join_key_rhs' => 'j_teacherqe_c_teachersj_teacherqe_idb',
    'readonly' => true,
    'relationship_name' => 'j_teacherqe_c_teachers',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'user_teachers_rel' => 
  array (
    'name' => 'user_teachers_rel',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'user_teachers_rel',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_teachers_modified_user' => 
  array (
    'name' => 'c_teachers_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_teachers_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_teachers_created_by' => 
  array (
    'name' => 'c_teachers_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_teachers_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_teachers_activities' => 
  array (
    'name' => 'c_teachers_activities',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Teachers',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'c_teachers_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_teachers_performed_by' => 
  array (
    'name' => 'c_teachers_performed_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'performed_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_teachers_performed_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'teachers_meetings' => 
  array (
    'name' => 'teachers_meetings',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'teacher_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'teachers_meetings',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'sub_teachers_meetings' => 
  array (
    'name' => 'sub_teachers_meetings',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'sub_teacher_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'sub_teachers_meetings',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'teachers_cover_meetings' => 
  array (
    'name' => 'teachers_cover_meetings',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'teacher_cover_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'teachers_cover_meetings',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'timesheet_teacher' => 
  array (
    'name' => 'timesheet_teacher',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Timesheet',
    'rhs_table' => 'c_timesheet',
    'rhs_key' => 'teacher_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'timesheet_teacher',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'teacher_classes' => 
  array (
    'name' => 'teacher_classes',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'teacher_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'teacher_classes',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'send_messages_teacher' => 
  array (
    'name' => 'send_messages_teacher',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'BMessage',
    'rhs_table' => 'bmessage',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Teachers',
    'readonly' => true,
    'relationship_name' => 'send_messages_teacher',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_teachers_following' => 
  array (
    'name' => 'c_teachers_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Teachers',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_teachers_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_teachers_favorite' => 
  array (
    'name' => 'c_teachers_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'C_Teachers',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_teachers_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_teachers_assigned_user' => 
  array (
    'name' => 'c_teachers_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_teachers_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);