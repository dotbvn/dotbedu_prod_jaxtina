<?php

$relationships = array (
  'j_class_cases_1' =>
  array (
    'name' => 'j_class_cases_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_class_cases_1' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_cases_1_c',
        'join_key_lhs' => 'j_class_cases_1j_class_ida',
        'join_key_rhs' => 'j_class_cases_1cases_idb',
      ),
    ),
    'table' => 'j_class_cases_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_cases_1j_class_ida' =>
      array (
        'name' => 'j_class_cases_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_cases_1cases_idb' =>
      array (
        'name' => 'j_class_cases_1cases_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_class_cases_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_class_cases_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_cases_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_class_cases_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_cases_1cases_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_class_cases_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_class_cases_1cases_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_cases_1_c',
    'join_key_lhs' => 'j_class_cases_1j_class_ida',
    'join_key_rhs' => 'j_class_cases_1cases_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_cases_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_class_c_gallery_1' =>
  array (
    'name' => 'j_class_c_gallery_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_class_c_gallery_1' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'C_Gallery',
        'rhs_table' => 'c_gallery',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_c_gallery_1_c',
        'join_key_lhs' => 'j_class_c_gallery_1j_class_ida',
        'join_key_rhs' => 'j_class_c_gallery_1c_gallery_idb',
      ),
    ),
    'table' => 'j_class_c_gallery_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_c_gallery_1j_class_ida' =>
      array (
        'name' => 'j_class_c_gallery_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_c_gallery_1c_gallery_idb' =>
      array (
        'name' => 'j_class_c_gallery_1c_gallery_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_class_c_gallery_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_class_c_gallery_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_c_gallery_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_class_c_gallery_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_c_gallery_1c_gallery_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_class_c_gallery_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_class_c_gallery_1c_gallery_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Gallery',
    'rhs_table' => 'c_gallery',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_c_gallery_1_c',
    'join_key_lhs' => 'j_class_c_gallery_1j_class_ida',
    'join_key_rhs' => 'j_class_c_gallery_1c_gallery_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_c_gallery_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_class_c_teachers_1' =>
  array (
    'name' => 'j_class_c_teachers_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_class_c_teachers_1' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'C_Teachers',
        'rhs_table' => 'c_teachers',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_c_teachers_1_c',
        'join_key_lhs' => 'j_class_c_teachers_1j_class_ida',
        'join_key_rhs' => 'j_class_c_teachers_1c_teachers_idb',
      ),
    ),
    'table' => 'j_class_c_teachers_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_c_teachers_1j_class_ida' =>
      array (
        'name' => 'j_class_c_teachers_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_c_teachers_1c_teachers_idb' =>
      array (
        'name' => 'j_class_c_teachers_1c_teachers_idb',
        'type' => 'id',
      ),
      'lms_enrollment_id' =>
      array (
        'name' => 'lms_enrollment_id',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_class_c_teachers_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_class_c_teachers_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_c_teachers_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_class_c_teachers_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_c_teachers_1c_teachers_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_class_c_teachers_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_class_c_teachers_1j_class_ida',
          1 => 'j_class_c_teachers_1c_teachers_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Teachers',
    'rhs_table' => 'c_teachers',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'j_class_c_teachers_1_c',
    'join_key_lhs' => 'j_class_c_teachers_1j_class_ida',
    'join_key_rhs' => 'j_class_c_teachers_1c_teachers_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_c_teachers_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_class_j_class_1' =>
  array (
    'name' => 'j_class_j_class_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_class_j_class_1' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Class',
        'rhs_table' => 'j_class',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_j_class_1_c',
        'join_key_lhs' => 'j_class_j_class_1j_class_ida',
        'join_key_rhs' => 'j_class_j_class_1j_class_idb',
      ),
    ),
    'table' => 'j_class_j_class_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_j_class_1j_class_ida' =>
      array (
        'name' => 'j_class_j_class_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_j_class_1j_class_idb' =>
      array (
        'name' => 'j_class_j_class_1j_class_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_class_j_class_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_class_j_class_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_j_class_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_class_j_class_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_j_class_1j_class_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_class_j_class_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_class_j_class_1j_class_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_j_class_1_c',
    'join_key_lhs' => 'j_class_j_class_1j_class_ida',
    'join_key_rhs' => 'j_class_j_class_1j_class_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_j_class_1',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_class_j_gradebook_1' =>
  array (
    'name' => 'j_class_j_gradebook_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_class_j_gradebook_1' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Gradebook',
        'rhs_table' => 'j_gradebook',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_j_gradebook_1_c',
        'join_key_lhs' => 'j_class_j_gradebook_1j_class_ida',
        'join_key_rhs' => 'j_class_j_gradebook_1j_gradebook_idb',
      ),
    ),
    'table' => 'j_class_j_gradebook_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_j_gradebook_1j_class_ida' =>
      array (
        'name' => 'j_class_j_gradebook_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_j_gradebook_1j_gradebook_idb' =>
      array (
        'name' => 'j_class_j_gradebook_1j_gradebook_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_class_j_gradebook_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_class_j_gradebook_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_j_gradebook_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_class_j_gradebook_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_j_gradebook_1j_gradebook_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_class_j_gradebook_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_class_j_gradebook_1j_gradebook_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Gradebook',
    'rhs_table' => 'j_gradebook',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_j_gradebook_1_c',
    'join_key_lhs' => 'j_class_j_gradebook_1j_class_ida',
    'join_key_rhs' => 'j_class_j_gradebook_1j_gradebook_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_j_gradebook_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_class_j_loyalty_1' =>
  array (
    'name' => 'j_class_j_loyalty_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_class_j_loyalty_1' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'J_Loyalty',
        'rhs_table' => 'j_loyalty',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_j_loyalty_1_c',
        'join_key_lhs' => 'j_class_j_loyalty_1j_class_ida',
        'join_key_rhs' => 'j_class_j_loyalty_1j_loyalty_idb',
      ),
    ),
    'table' => 'j_class_j_loyalty_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_j_loyalty_1j_class_ida' =>
      array (
        'name' => 'j_class_j_loyalty_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_j_loyalty_1j_loyalty_idb' =>
      array (
        'name' => 'j_class_j_loyalty_1j_loyalty_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_class_j_loyalty_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_class_j_loyalty_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_j_loyalty_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_class_j_loyalty_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_j_loyalty_1j_loyalty_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_class_j_loyalty_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_class_j_loyalty_1j_loyalty_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Loyalty',
    'rhs_table' => 'j_loyalty',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_j_loyalty_1_c',
    'join_key_lhs' => 'j_class_j_loyalty_1j_class_ida',
    'join_key_rhs' => 'j_class_j_loyalty_1j_loyalty_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_j_loyalty_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_class_tasks_1' =>
  array (
    'name' => 'j_class_tasks_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'j_class_tasks_1' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'Tasks',
        'rhs_table' => 'tasks',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_tasks_1_c',
        'join_key_lhs' => 'j_class_tasks_1j_class_ida',
        'join_key_rhs' => 'j_class_tasks_1tasks_idb',
      ),
    ),
    'table' => 'j_class_tasks_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_tasks_1j_class_ida' =>
      array (
        'name' => 'j_class_tasks_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_tasks_1tasks_idb' =>
      array (
        'name' => 'j_class_tasks_1tasks_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_class_tasks_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_class_tasks_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_tasks_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_class_tasks_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_class_tasks_1tasks_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_class_tasks_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_class_tasks_1tasks_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_tasks_1_c',
    'join_key_lhs' => 'j_class_tasks_1j_class_ida',
    'join_key_rhs' => 'j_class_tasks_1tasks_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_tasks_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'j_teacherqe_j_class' =>
  array (
    'name' => 'j_teacherqe_j_class',
    'true_relationship_type' => 'one-to-many',
    'relationships' =>
    array (
      'j_teacherqe_j_class' =>
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'J_TeacherQE',
        'rhs_table' => 'j_teacherqe',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_teacherqe_j_class_c',
        'join_key_lhs' => 'j_teacherqe_j_classj_class_ida',
        'join_key_rhs' => 'j_teacherqe_j_classj_teacherqe_idb',
      ),
    ),
    'table' => 'j_teacherqe_j_class_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_teacherqe_j_classj_class_ida' =>
      array (
        'name' => 'j_teacherqe_j_classj_class_ida',
        'type' => 'id',
      ),
      'j_teacherqe_j_classj_teacherqe_idb' =>
      array (
        'name' => 'j_teacherqe_j_classj_teacherqe_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_j_teacherqe_j_class_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_j_teacherqe_j_class_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_teacherqe_j_classj_class_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_j_teacherqe_j_class_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'j_teacherqe_j_classj_teacherqe_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'j_teacherqe_j_class_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'j_teacherqe_j_classj_teacherqe_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_TeacherQE',
    'rhs_table' => 'j_teacherqe',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_teacherqe_j_class_c',
    'join_key_lhs' => 'j_teacherqe_j_classj_class_ida',
    'join_key_rhs' => 'j_teacherqe_j_classj_teacherqe_idb',
    'readonly' => true,
    'relationship_name' => 'j_teacherqe_j_class',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'from_studio' => false,
  ),
  'class_cso_rel' =>
  array (
    'name' => 'class_cso_rel',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'cso_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'class_cso_rel',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'room_classes' =>
  array (
    'name' => 'room_classes',
    'lhs_module' => 'C_Rooms',
    'lhs_table' => 'c_rooms',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'room_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'room_classes',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'teacher_classes' =>
  array (
    'name' => 'teacher_classes',
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'teacher_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'teacher_classes',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_modified_user' =>
  array (
    'name' => 'j_class_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_class_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_created_by' =>
  array (
    'name' => 'j_class_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_class_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_activities' =>
  array (
    'name' => 'j_class_activities',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Class',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' =>
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' =>
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' =>
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' =>
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'j_class_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_meetings' =>
  array (
    'name' => 'j_class_meetings',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'ju_class_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_class_meetings',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'class_attendances' =>
  array (
    'name' => 'class_attendances',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Attendance',
    'rhs_table' => 'c_attendance',
    'rhs_key' => 'class_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'class_attendances',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_studentsituations' =>
  array (
    'name' => 'j_class_studentsituations',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_StudentSituations',
    'rhs_table' => 'j_studentsituations',
    'rhs_key' => 'ju_class_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_class_studentsituations',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'student_cur_class' =>
  array (
    'name' => 'student_cur_class',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'current_class_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'student_cur_class',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'class_enrollments' =>
  array (
    'name' => 'class_enrollments',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'ju_class_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'class_enrollments',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'send_messages_class' =>
  array (
    'name' => 'send_messages_class',
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'BMessage',
    'rhs_table' => 'bmessage',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Class',
    'readonly' => true,
    'relationship_name' => 'send_messages_class',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_following' =>
  array (
    'name' => 'j_class_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'J_Class',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_class_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_favorite' =>
  array (
    'name' => 'j_class_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'J_Class',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'j_class_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'j_class_assigned_user' =>
  array (
    'name' => 'j_class_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'j_class_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'kindofcourse_class' =>
  array (
    'name' => 'kindofcourse_class',
    'lhs_module' => 'J_Kindofcourse',
    'lhs_table' => 'j_kindofcourse',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'koc_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'kindofcourse_class',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);