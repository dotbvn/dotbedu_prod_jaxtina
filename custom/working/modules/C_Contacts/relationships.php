<?php

$relationships = array (
  'c_contacts_modified_user' =>
  array (
    'name' => 'c_contacts_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Contacts',
    'rhs_table' => 'c_contacts',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_contacts_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_contacts_created_by' =>
  array (
    'name' => 'c_contacts_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Contacts',
    'rhs_table' => 'c_contacts',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_contacts_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_contacts_activities' =>
  array (
    'name' => 'c_contacts_activities',
    'lhs_module' => 'C_Contacts',
    'lhs_table' => 'c_contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' =>
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' =>
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' =>
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' =>
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'c_contacts_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_contacts_following' =>
  array (
    'name' => 'c_contacts_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Contacts',
    'rhs_table' => 'c_contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Contacts',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_contacts_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_contacts_favorite' =>
  array (
    'name' => 'c_contacts_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Contacts',
    'rhs_table' => 'c_contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'C_Contacts',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_contacts_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_contacts_assigned_user' =>
  array (
    'name' => 'c_contacts_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Contacts',
    'rhs_table' => 'c_contacts',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_contacts_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_contacts_contacts_1' =>
  array (
    'name' => 'c_contacts_contacts_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'c_contacts_contacts_1' =>
      array (
        'lhs_module' => 'C_Contacts',
        'lhs_table' => 'c_contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_contacts_contacts_1_c',
        'join_key_lhs' => 'c_contacts_contacts_1c_contacts_ida',
        'join_key_rhs' => 'c_contacts_contacts_1contacts_idb',
      ),
    ),
    'table' => 'c_contacts_contacts_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_contacts_contacts_1c_contacts_ida' =>
      array (
        'name' => 'c_contacts_contacts_1c_contacts_ida',
        'type' => 'id',
      ),
      'c_contacts_contacts_1contacts_idb' =>
      array (
        'name' => 'c_contacts_contacts_1contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_c_contacts_contacts_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_c_contacts_contacts_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'c_contacts_contacts_1c_contacts_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_c_contacts_contacts_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'c_contacts_contacts_1contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'c_contacts_contacts_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'c_contacts_contacts_1contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_Contacts',
    'lhs_table' => 'c_contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'c_contacts_contacts_1_c',
    'join_key_lhs' => 'c_contacts_contacts_1c_contacts_ida',
    'join_key_rhs' => 'c_contacts_contacts_1contacts_idb',
    'readonly' => true,
    'relationship_name' => 'c_contacts_contacts_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
);