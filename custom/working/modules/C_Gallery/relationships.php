<?php

$relationships = array (
  'j_class_c_gallery_1' => 
  array (
    'name' => 'j_class_c_gallery_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'j_class_c_gallery_1' => 
      array (
        'lhs_module' => 'J_Class',
        'lhs_table' => 'j_class',
        'lhs_key' => 'id',
        'rhs_module' => 'C_Gallery',
        'rhs_table' => 'c_gallery',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'j_class_c_gallery_1_c',
        'join_key_lhs' => 'j_class_c_gallery_1j_class_ida',
        'join_key_rhs' => 'j_class_c_gallery_1c_gallery_idb',
      ),
    ),
    'table' => 'j_class_c_gallery_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'j_class_c_gallery_1j_class_ida' => 
      array (
        'name' => 'j_class_c_gallery_1j_class_ida',
        'type' => 'id',
      ),
      'j_class_c_gallery_1c_gallery_idb' => 
      array (
        'name' => 'j_class_c_gallery_1c_gallery_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_j_class_c_gallery_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_j_class_c_gallery_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_class_c_gallery_1j_class_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_j_class_c_gallery_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'j_class_c_gallery_1c_gallery_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'j_class_c_gallery_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'j_class_c_gallery_1c_gallery_idb',
        ),
      ),
    ),
    'lhs_module' => 'J_Class',
    'lhs_table' => 'j_class',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Gallery',
    'rhs_table' => 'c_gallery',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'j_class_c_gallery_1_c',
    'join_key_lhs' => 'j_class_c_gallery_1j_class_ida',
    'join_key_rhs' => 'j_class_c_gallery_1c_gallery_idb',
    'readonly' => true,
    'relationship_name' => 'j_class_c_gallery_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'c_gallery_contacts_1' => 
  array (
    'name' => 'c_gallery_contacts_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' => 
    array (
      'c_gallery_contacts_1' => 
      array (
        'lhs_module' => 'C_Gallery',
        'lhs_table' => 'c_gallery',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_gallery_contacts_1_c',
        'join_key_lhs' => 'c_gallery_contacts_1c_gallery_ida',
        'join_key_rhs' => 'c_gallery_contacts_1contacts_idb',
      ),
    ),
    'table' => 'c_gallery_contacts_1_c',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_gallery_contacts_1c_gallery_ida' => 
      array (
        'name' => 'c_gallery_contacts_1c_gallery_ida',
        'type' => 'id',
      ),
      'c_gallery_contacts_1contacts_idb' => 
      array (
        'name' => 'c_gallery_contacts_1contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' => 
    array (
      0 => 
      array (
        'name' => 'idx_c_gallery_contacts_1_pk',
        'type' => 'primary',
        'fields' => 
        array (
          0 => 'id',
        ),
      ),
      1 => 
      array (
        'name' => 'idx_c_gallery_contacts_1_ida1_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_gallery_contacts_1c_gallery_ida',
          1 => 'deleted',
        ),
      ),
      2 => 
      array (
        'name' => 'idx_c_gallery_contacts_1_idb2_deleted',
        'type' => 'index',
        'fields' => 
        array (
          0 => 'c_gallery_contacts_1contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 => 
      array (
        'name' => 'c_gallery_contacts_1_alt',
        'type' => 'alternate_key',
        'fields' => 
        array (
          0 => 'c_gallery_contacts_1c_gallery_ida',
          1 => 'c_gallery_contacts_1contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_Gallery',
    'lhs_table' => 'c_gallery',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'c_gallery_contacts_1_c',
    'join_key_lhs' => 'c_gallery_contacts_1c_gallery_ida',
    'join_key_rhs' => 'c_gallery_contacts_1contacts_idb',
    'readonly' => true,
    'relationship_name' => 'c_gallery_contacts_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'c_gallery_modified_user' => 
  array (
    'name' => 'c_gallery_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Gallery',
    'rhs_table' => 'c_gallery',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_gallery_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_created_by' => 
  array (
    'name' => 'c_gallery_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Gallery',
    'rhs_table' => 'c_gallery',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_gallery_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_activities' => 
  array (
    'name' => 'c_gallery_activities',
    'lhs_module' => 'C_Gallery',
    'lhs_table' => 'c_gallery',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Gallery',
    'fields' => 
    array (
      'id' => 
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' => 
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' => 
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' => 
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' => 
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' => 
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' => 
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'c_gallery_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_notes' => 
  array (
    'name' => 'c_gallery_notes',
    'lhs_module' => 'C_Gallery',
    'lhs_table' => 'c_gallery',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Gallery',
    'readonly' => true,
    'relationship_name' => 'c_gallery_notes',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_attachments' => 
  array (
    'name' => 'c_gallery_attachments',
    'lhs_module' => 'C_Gallery',
    'lhs_table' => 'c_gallery',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Gallery',
    'readonly' => true,
    'relationship_name' => 'c_gallery_attachments',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'send_messages_gallery' => 
  array (
    'name' => 'send_messages_gallery',
    'lhs_module' => 'C_Gallery',
    'lhs_table' => 'c_gallery',
    'lhs_key' => 'id',
    'rhs_module' => 'BMessage',
    'rhs_table' => 'bmessage',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Gallery',
    'readonly' => true,
    'relationship_name' => 'send_messages_gallery',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_following' => 
  array (
    'name' => 'c_gallery_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Gallery',
    'rhs_table' => 'c_gallery',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Gallery',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_gallery_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_favorite' => 
  array (
    'name' => 'c_gallery_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Gallery',
    'rhs_table' => 'c_gallery',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'C_Gallery',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'c_gallery_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_assigned_user' => 
  array (
    'name' => 'c_gallery_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Gallery',
    'rhs_table' => 'c_gallery',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'c_gallery_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'comments_c_gallery' => 
  array (
    'name' => 'comments_c_gallery',
    'lhs_module' => 'C_Gallery',
    'lhs_table' => 'c_gallery',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Comments',
    'rhs_table' => 'c_comments',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Gallery',
    'readonly' => true,
    'relationship_name' => 'comments_c_gallery',
    'rhs_subpanel' => 'ForC_newsComments_link',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'c_gallery_c_teachers_1' => 
  array (
    'rhs_label' => 'Teachers',
    'lhs_label' => 'Gallery',
    'lhs_subpanel' => 'default',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'C_Gallery',
    'rhs_module' => 'C_Teachers',
    'relationship_type' => 'many-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'c_gallery_c_teachers_1',
  ),
);