<?php

$relationships = array (
  'accounts_contacts' =>
  array (
    'name' => 'accounts_contacts',
    'table' => 'accounts_contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'account_id' =>
      array (
        'name' => 'account_id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'primary_account' =>
      array (
        'name' => 'primary_account',
        'type' => 'bool',
        'default' => '0',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'required' => false,
        'default' => '0',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'accounts_contactspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_account_contact',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'account_id',
          1 => 'contact_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_contid_del_accid',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
          1 => 'deleted',
          2 => 'account_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'accounts_contacts' =>
      array (
        'lhs_module' => 'Accounts',
        'lhs_table' => 'accounts',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'accounts_contacts',
        'join_key_lhs' => 'account_id',
        'join_key_rhs' => 'contact_id',
        'primary_flag_column' => 'primary_account',
        'primary_flag_side' => 'rhs',
        'primary_flag_default' => true,
      ),
    ),
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'accounts_contacts',
    'join_key_lhs' => 'account_id',
    'join_key_rhs' => 'contact_id',
    'primary_flag_column' => 'primary_account',
    'primary_flag_side' => 'rhs',
    'primary_flag_default' => true,
    'readonly' => true,
    'relationship_name' => 'accounts_contacts',
    'rhs_subpanel' => 'ForAccountsAccounts_contacts_1',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'calls_contacts' =>
  array (
    'name' => 'calls_contacts',
    'table' => 'calls_contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => '36',
      ),
      'call_id' =>
      array (
        'name' => 'call_id',
        'type' => 'id',
        'len' => '36',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
        'len' => '36',
      ),
      'required' =>
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' =>
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'calls_contactspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_call_call',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'call_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_call_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_call_contact',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'call_id',
          1 => 'contact_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'calls_contacts' =>
      array (
        'lhs_module' => 'Calls',
        'lhs_table' => 'calls',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'calls_contacts',
        'join_key_lhs' => 'call_id',
        'join_key_rhs' => 'contact_id',
      ),
    ),
    'lhs_module' => 'Calls',
    'lhs_table' => 'calls',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'calls_contacts',
    'join_key_lhs' => 'call_id',
    'join_key_rhs' => 'contact_id',
    'readonly' => true,
    'relationship_name' => 'calls_contacts',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'ForContactsCalls',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_bugs' =>
  array (
    'name' => 'contacts_bugs',
    'table' => 'contacts_bugs',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'bug_id' =>
      array (
        'name' => 'bug_id',
        'type' => 'id',
      ),
      'contact_role' =>
      array (
        'name' => 'contact_role',
        'type' => 'varchar',
        'len' => '50',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'contacts_bugspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_bug_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_bug_bug',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'bug_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_contact_bug',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'contact_id',
          1 => 'bug_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'contacts_bugs' =>
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'Bugs',
        'rhs_table' => 'bugs',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_bugs',
        'join_key_lhs' => 'contact_id',
        'join_key_rhs' => 'bug_id',
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Bugs',
    'rhs_table' => 'bugs',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'contacts_bugs',
    'join_key_lhs' => 'contact_id',
    'join_key_rhs' => 'bug_id',
    'readonly' => true,
    'relationship_name' => 'contacts_bugs',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_cases' =>
  array (
    'name' => 'contacts_cases',
    'table' => 'contacts_cases',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'case_id' =>
      array (
        'name' => 'case_id',
        'type' => 'id',
      ),
      'contact_role' =>
      array (
        'name' => 'contact_role',
        'type' => 'varchar',
        'len' => '50',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'contacts_casespk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_case_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_case_case',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'case_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_contacts_cases',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'contact_id',
          1 => 'case_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'contacts_cases' =>
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_cases',
        'join_key_lhs' => 'contact_id',
        'join_key_rhs' => 'case_id',
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'contacts_cases',
    'join_key_lhs' => 'contact_id',
    'join_key_rhs' => 'case_id',
    'readonly' => true,
    'relationship_name' => 'contacts_cases',
    'rhs_subpanel' => 'ForContactsContacts_cases_1',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_users' =>
  array (
    'name' => 'contacts_users',
    'table' => 'contacts_users',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'user_id' =>
      array (
        'name' => 'user_id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'contacts_userspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_users_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_users_user',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'user_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_contacts_users',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'contact_id',
          1 => 'user_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'contacts_users' =>
      array (
        'lhs_module' => 'Users',
        'lhs_table' => 'users',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'user-based',
        'join_table' => 'contacts_users',
        'join_key_lhs' => 'user_id',
        'join_key_rhs' => 'contact_id',
        'user_field' => 'user_id',
      ),
    ),
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'contacts_users',
    'join_key_lhs' => 'user_id',
    'join_key_rhs' => 'contact_id',
    'user_field' => 'user_id',
    'readonly' => true,
    'relationship_name' => 'contacts_users',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'meetings_contacts' =>
  array (
    'name' => 'meetings_contacts',
    'table' => 'meetings_contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => '36',
      ),
      'meeting_id' =>
      array (
        'name' => 'meeting_id',
        'type' => 'id',
        'len' => '36',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
        'len' => '36',
      ),
      'required' =>
      array (
        'name' => 'required',
        'type' => 'varchar',
        'len' => '1',
        'default' => '1',
      ),
      'accept_status' =>
      array (
        'name' => 'accept_status',
        'type' => 'varchar',
        'len' => '25',
        'default' => 'none',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
      'contract_id' =>
      array (
        'name' => 'contract_id',
        'type' => 'id',
        'len' => '36',
      ),
      'situation_id' =>
      array (
        'name' => 'situation_id',
        'type' => 'id',
        'len' => '36',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'meetings_contactspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_mtg_mtg',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'meeting_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_mtg_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_meeting_contact',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'meeting_id',
          1 => 'contact_id',
        ),
      ),
      4 =>
      array (
        'name' => 'idx_situa_id',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'situation_id',
        ),
      ),
      5 =>
      array (
        'name' => 'idx_contract_id',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contract_id',
        ),
      ),
      6 =>
      array (
        'name' => 'idx_meeting_contact_situa',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'meeting_id',
          1 => 'contact_id',
          2 => 'situation_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'meetings_contacts' =>
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_contacts',
        'join_key_lhs' => 'meeting_id',
        'join_key_rhs' => 'contact_id',
      ),
      'meetings_situations' =>
      array (
        'lhs_module' => 'Meetings',
        'lhs_table' => 'meetings',
        'lhs_key' => 'id',
        'rhs_module' => 'J_StudentSituations',
        'rhs_table' => 'j_studentsituations',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'meetings_contacts',
        'join_key_lhs' => 'meeting_id',
        'join_key_rhs' => 'situation_id',
      ),
    ),
    'lhs_module' => 'Meetings',
    'lhs_table' => 'meetings',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'meetings_contacts',
    'join_key_lhs' => 'meeting_id',
    'join_key_rhs' => 'contact_id',
    'readonly' => true,
    'relationship_name' => 'meetings_contacts',
    'rhs_subpanel' => 'ForMeetings',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'opportunities_contacts' =>
  array (
    'name' => 'opportunities_contacts',
    'table' => 'opportunities_contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'opportunity_id' =>
      array (
        'name' => 'opportunity_id',
        'type' => 'id',
      ),
      'contact_role' =>
      array (
        'name' => 'contact_role',
        'type' => 'varchar',
        'len' => '50',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'opportunities_contactspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_opp_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_opp_opp',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'opportunity_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_opportunities_contacts',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'opportunity_id',
          1 => 'contact_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'opportunities_contacts' =>
      array (
        'lhs_module' => 'Opportunities',
        'lhs_table' => 'opportunities',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'opportunities_contacts',
        'join_key_lhs' => 'opportunity_id',
        'join_key_rhs' => 'contact_id',
      ),
    ),
    'lhs_module' => 'Opportunities',
    'lhs_table' => 'opportunities',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'opportunities_contacts',
    'join_key_lhs' => 'opportunity_id',
    'join_key_rhs' => 'contact_id',
    'readonly' => true,
    'relationship_name' => 'opportunities_contacts',
    'rhs_subpanel' => 'ForOpportunities',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'quotes_contacts_shipto' =>
  array (
    'name' => 'quotes_contacts_shipto',
    'rhs_module' => 'Quotes',
    'rhs_table' => 'quotes',
    'rhs_key' => 'id',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'true_relationship_type' => 'one-to-many',
    'join_table' => 'quotes_contacts',
    'join_key_rhs' => 'quote_id',
    'join_key_lhs' => 'contact_id',
    'relationship_role_column' => 'contact_role',
    'relationship_role_column_value' => 'Ship To',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'quote_id' =>
      array (
        'name' => 'quote_id',
        'type' => 'id',
      ),
      'contact_role' =>
      array (
        'name' => 'contact_role',
        'type' => 'varchar',
        'len' => '20',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'quotes_contacts_shipto',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'quotes_contacts_billto' =>
  array (
    'name' => 'quotes_contacts_billto',
    'rhs_module' => 'Quotes',
    'rhs_table' => 'quotes',
    'rhs_key' => 'id',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'true_relationship_type' => 'one-to-many',
    'join_table' => 'quotes_contacts',
    'join_key_rhs' => 'quote_id',
    'join_key_lhs' => 'contact_id',
    'relationship_role_column' => 'contact_role',
    'relationship_role_column_value' => 'Bill To',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'quote_id' =>
      array (
        'name' => 'quote_id',
        'type' => 'id',
      ),
      'contact_role' =>
      array (
        'name' => 'contact_role',
        'type' => 'varchar',
        'len' => '20',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'quotes_contacts_billto',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contracts_contacts' =>
  array (
    'name' => 'contracts_contacts',
    'table' => 'contracts_contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'contract_id' =>
      array (
        'name' => 'contract_id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'contracts_contacts_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'contracts_contacts_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'contact_id',
          1 => 'contract_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'contracts_contacts' =>
      array (
        'lhs_module' => 'Contracts',
        'lhs_table' => 'contracts',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contracts_contacts',
        'join_key_lhs' => 'contract_id',
        'join_key_rhs' => 'contact_id',
      ),
    ),
    'lhs_module' => 'Contracts',
    'lhs_table' => 'contracts',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'contracts_contacts',
    'join_key_lhs' => 'contract_id',
    'join_key_rhs' => 'contact_id',
    'readonly' => true,
    'relationship_name' => 'contracts_contacts',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_dataprivacy' =>
  array (
    'name' => 'contacts_dataprivacy',
    'table' => 'contacts_dataprivacy',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
      ),
      'dataprivacy_id' =>
      array (
        'name' => 'dataprivacy_id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => false,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'contacts_dataprivacypk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_con_dataprivacy_con',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contact_id',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_con_dataprivacy_dataprivacy',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'dataprivacy_id',
        ),
      ),
      3 =>
      array (
        'name' => 'idx_contacts_dataprivacy',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'contact_id',
          1 => 'dataprivacy_id',
        ),
      ),
    ),
    'relationships' =>
    array (
      'contacts_dataprivacy' =>
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'DataPrivacy',
        'rhs_table' => 'data_privacy',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_dataprivacy',
        'join_key_lhs' => 'contact_id',
        'join_key_rhs' => 'dataprivacy_id',
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'DataPrivacy',
    'rhs_table' => 'data_privacy',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'contacts_dataprivacy',
    'join_key_lhs' => 'contact_id',
    'join_key_rhs' => 'dataprivacy_id',
    'readonly' => true,
    'relationship_name' => 'contacts_dataprivacy',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'default',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'documents_contacts' =>
  array (
    'name' => 'documents_contacts',
    'true_relationship_type' => 'many-to-many',
    'relationships' =>
    array (
      'documents_contacts' =>
      array (
        'lhs_module' => 'Documents',
        'lhs_table' => 'documents',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'documents_contacts',
        'join_key_lhs' => 'document_id',
        'join_key_rhs' => 'contact_id',
      ),
    ),
    'table' => 'documents_contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'len' => '1',
        'default' => '0',
        'required' => true,
      ),
      'document_id' =>
      array (
        'name' => 'document_id',
        'type' => 'id',
        'len' => 36,
      ),
      'contact_id' =>
      array (
        'name' => 'contact_id',
        'type' => 'id',
        'len' => 36,
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'documents_contactsspk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'documents_contacts_contact_id',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'contact_id',
          1 => 'document_id',
        ),
      ),
      2 =>
      array (
        'name' => 'documents_contacts_document_id',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'document_id',
          1 => 'contact_id',
        ),
      ),
    ),
    'lhs_module' => 'Documents',
    'lhs_table' => 'documents',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'documents_contacts',
    'join_key_lhs' => 'document_id',
    'join_key_rhs' => 'contact_id',
    'readonly' => true,
    'relationship_name' => 'documents_contacts',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_cases_1' =>
  array (
    'name' => 'contacts_cases_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'contacts_cases_1' =>
      array (
        'lhs_module' => 'Contacts',
        'lhs_table' => 'contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'Cases',
        'rhs_table' => 'cases',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'contacts_cases_1_c',
        'join_key_lhs' => 'contacts_cases_1contacts_ida',
        'join_key_rhs' => 'contacts_cases_1cases_idb',
      ),
    ),
    'table' => 'contacts_cases_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'contacts_cases_1contacts_ida' =>
      array (
        'name' => 'contacts_cases_1contacts_ida',
        'type' => 'id',
      ),
      'contacts_cases_1cases_idb' =>
      array (
        'name' => 'contacts_cases_1cases_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_contacts_cases_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_contacts_cases_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contacts_cases_1contacts_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_contacts_cases_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'contacts_cases_1cases_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'contacts_cases_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'contacts_cases_1cases_idb',
        ),
      ),
    ),
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'contacts_cases_1_c',
    'join_key_lhs' => 'contacts_cases_1contacts_ida',
    'join_key_rhs' => 'contacts_cases_1cases_idb',
    'readonly' => true,
    'relationship_name' => 'contacts_cases_1',
    'rhs_subpanel' => 'ForContactsContacts_cases_1',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'c_news_contacts_1' =>
  array (
    'name' => 'c_news_contacts_1',
    'true_relationship_type' => 'many-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'c_news_contacts_1' =>
      array (
        'lhs_module' => 'C_News',
        'lhs_table' => 'c_news',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_news_contacts_1_c',
        'join_key_lhs' => 'c_news_contacts_1c_news_ida',
        'join_key_rhs' => 'c_news_contacts_1contacts_idb',
      ),
    ),
    'table' => 'c_news_contacts_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_news_contacts_1c_news_ida' =>
      array (
        'name' => 'c_news_contacts_1c_news_ida',
        'type' => 'id',
      ),
      'c_news_contacts_1contacts_idb' =>
      array (
        'name' => 'c_news_contacts_1contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_c_news_contacts_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_c_news_contacts_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'c_news_contacts_1c_news_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_c_news_contacts_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'c_news_contacts_1contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'c_news_contacts_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'c_news_contacts_1c_news_ida',
          1 => 'c_news_contacts_1contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_News',
    'lhs_table' => 'c_news',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'c_news_contacts_1_c',
    'join_key_lhs' => 'c_news_contacts_1c_news_ida',
    'join_key_rhs' => 'c_news_contacts_1contacts_idb',
    'readonly' => true,
    'relationship_name' => 'c_news_contacts_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => 'default',
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'c_contacts_contacts_1' =>
  array (
    'name' => 'c_contacts_contacts_1',
    'true_relationship_type' => 'one-to-many',
    'from_studio' => true,
    'relationships' =>
    array (
      'c_contacts_contacts_1' =>
      array (
        'lhs_module' => 'C_Contacts',
        'lhs_table' => 'c_contacts',
        'lhs_key' => 'id',
        'rhs_module' => 'Contacts',
        'rhs_table' => 'contacts',
        'rhs_key' => 'id',
        'relationship_type' => 'many-to-many',
        'join_table' => 'c_contacts_contacts_1_c',
        'join_key_lhs' => 'c_contacts_contacts_1c_contacts_ida',
        'join_key_rhs' => 'c_contacts_contacts_1contacts_idb',
      ),
    ),
    'table' => 'c_contacts_contacts_1_c',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'type' => 'bool',
        'default' => 0,
      ),
      'c_contacts_contacts_1c_contacts_ida' =>
      array (
        'name' => 'c_contacts_contacts_1c_contacts_ida',
        'type' => 'id',
      ),
      'c_contacts_contacts_1contacts_idb' =>
      array (
        'name' => 'c_contacts_contacts_1contacts_idb',
        'type' => 'id',
      ),
    ),
    'indices' =>
    array (
      0 =>
      array (
        'name' => 'idx_c_contacts_contacts_1_pk',
        'type' => 'primary',
        'fields' =>
        array (
          0 => 'id',
        ),
      ),
      1 =>
      array (
        'name' => 'idx_c_contacts_contacts_1_ida1_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'c_contacts_contacts_1c_contacts_ida',
          1 => 'deleted',
        ),
      ),
      2 =>
      array (
        'name' => 'idx_c_contacts_contacts_1_idb2_deleted',
        'type' => 'index',
        'fields' =>
        array (
          0 => 'c_contacts_contacts_1contacts_idb',
          1 => 'deleted',
        ),
      ),
      3 =>
      array (
        'name' => 'c_contacts_contacts_1_alt',
        'type' => 'alternate_key',
        'fields' =>
        array (
          0 => 'c_contacts_contacts_1contacts_idb',
        ),
      ),
    ),
    'lhs_module' => 'C_Contacts',
    'lhs_table' => 'c_contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'one-to-many',
    'join_table' => 'c_contacts_contacts_1_c',
    'join_key_lhs' => 'c_contacts_contacts_1c_contacts_ida',
    'join_key_rhs' => 'c_contacts_contacts_1contacts_idb',
    'readonly' => true,
    'relationship_name' => 'c_contacts_contacts_1',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'is_custom' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'utm_agent_rel_c' =>
  array (
    'name' => 'utm_agent_rel_c',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'utm_agent_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'utm_agent_rel_c',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_modified_user' =>
  array (
    'name' => 'contacts_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'modified_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contacts_modified_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_created_by' =>
  array (
    'name' => 'contacts_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'created_by',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contacts_created_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_activities' =>
  array (
    'name' => 'contact_activities',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Activities',
    'rhs_table' => 'activities',
    'rhs_key' => 'id',
    'rhs_vname' => 'LBL_ACTIVITY_STREAM',
    'relationship_type' => 'many-to-many',
    'join_table' => 'activities_users',
    'join_key_lhs' => 'parent_id',
    'join_key_rhs' => 'activity_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'fields' =>
    array (
      'id' =>
      array (
        'name' => 'id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'activity_id' =>
      array (
        'name' => 'activity_id',
        'type' => 'id',
        'len' => 36,
        'required' => true,
      ),
      'parent_type' =>
      array (
        'name' => 'parent_type',
        'type' => 'varchar',
        'len' => 100,
      ),
      'parent_id' =>
      array (
        'name' => 'parent_id',
        'type' => 'id',
        'len' => 36,
      ),
      'fields' =>
      array (
        'name' => 'fields',
        'type' => 'json',
        'dbType' => 'longtext',
        'required' => true,
      ),
      'date_modified' =>
      array (
        'name' => 'date_modified',
        'type' => 'datetime',
      ),
      'deleted' =>
      array (
        'name' => 'deleted',
        'vname' => 'LBL_DELETED',
        'type' => 'bool',
        'default' => '0',
      ),
    ),
    'readonly' => true,
    'relationship_name' => 'contact_activities',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_performed_by' =>
  array (
    'name' => 'contacts_performed_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'performed_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contacts_performed_by',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_direct_reports' =>
  array (
    'name' => 'contact_direct_reports',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'reports_to_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contact_direct_reports',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_leads' =>
  array (
    'name' => 'contact_leads',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Leads',
    'rhs_table' => 'leads',
    'rhs_key' => 'contact_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contact_leads',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_notes' =>
  array (
    'name' => 'contact_notes',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'rhs_key' => 'contact_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contact_notes',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_notes_parent' =>
  array (
    'name' => 'contact_notes_parent',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Notes',
    'rhs_table' => 'notes',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'contact_notes_parent',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_calls_parent' =>
  array (
    'name' => 'contact_calls_parent',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Calls',
    'rhs_table' => 'calls',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'contact_calls_parent',
    'rhs_subpanel' => 'ForContactsCalls',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_meetings_parent' =>
  array (
    'name' => 'contact_meetings_parent',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'contact_meetings_parent',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => 'ForMeetings',
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_tasks' =>
  array (
    'name' => 'contact_tasks',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'contact_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contact_tasks',
    'rhs_subpanel' => 'ForContactsAll_tasks',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_tasks_parent' =>
  array (
    'name' => 'contact_tasks_parent',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Tasks',
    'rhs_table' => 'tasks',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'contact_tasks_parent',
    'rhs_subpanel' => 'ForContactsAll_tasks',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_assigned_user' =>
  array (
    'name' => 'contacts_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'assigned_user_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contacts_assigned_user',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_products' =>
  array (
    'name' => 'contact_products',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Products',
    'rhs_table' => 'products',
    'rhs_key' => 'contact_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contact_products',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_following' =>
  array (
    'name' => 'contacts_following',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'subscriptions',
    'join_key_lhs' => 'created_by',
    'join_key_rhs' => 'parent_id',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'contacts_following',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contacts_favorite' =>
  array (
    'name' => 'contacts_favorite',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'id',
    'relationship_type' => 'many-to-many',
    'join_table' => 'dotbfavorites',
    'join_key_lhs' => 'modified_user_id',
    'join_key_rhs' => 'record_id',
    'relationship_role_column' => 'module',
    'relationship_role_column_value' => 'Contacts',
    'user_field' => 'created_by',
    'readonly' => true,
    'relationship_name' => 'contacts_favorite',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_dri_workflow_templates' =>
  array (
    'name' => 'contact_dri_workflow_templates',
    'relationship_type' => 'one-to-many',
    'lhs_key' => 'id',
    'lhs_module' => 'DRI_Workflow_Templates',
    'lhs_table' => 'dri_workflow_templates',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'dri_workflow_template_id',
    'readonly' => true,
    'relationship_name' => 'contact_dri_workflow_templates',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_smses' =>
  array (
    'name' => 'contact_smses',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_SMS',
    'rhs_table' => 'c_sms',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'contact_smses',
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'student_attendances' =>
  array (
    'name' => 'student_attendances',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Attendance',
    'rhs_table' => 'c_attendance',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'student_attendances',
    'rhs_subpanel' => 'ForContactsAttendances_parent',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'student_revenue' =>
  array (
    'name' => 'student_revenue',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_DeliveryRevenue',
    'rhs_table' => 'c_deliveryrevenue',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'student_revenue',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_studentsituations' =>
  array (
    'name' => 'contact_studentsituations',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_StudentSituations',
    'rhs_table' => 'j_studentsituations',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Contacts',
    'readonly' => true,
    'relationship_name' => 'contact_studentsituations',
    'rhs_subpanel' => 'ForContactsJu_studentsituations',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'student_j_gradebookdetail' =>
  array (
    'name' => 'student_j_gradebookdetail',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_GradebookDetail',
    'rhs_table' => 'j_gradebookdetail',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'student_j_gradebookdetail',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'student_loyaltys' =>
  array (
    'name' => 'student_loyaltys',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Loyalty',
    'rhs_table' => 'j_loyalty',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'student_loyaltys',
    'rhs_subpanel' => 'ForContactsLoyalty_link',
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'campaign_contacts' =>
  array (
    'name' => 'campaign_contacts',
    'lhs_module' => 'Campaigns',
    'lhs_table' => 'campaigns',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'campaign_id',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'relationship_name' => 'campaign_contacts',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'dri_workflow_contacts' =>
  array (
    'name' => 'dri_workflow_contacts',
    'relationship_type' => 'one-to-many',
    'lhs_key' => 'id',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'rhs_module' => 'DRI_Workflows',
    'rhs_table' => 'dri_workflows',
    'rhs_key' => 'contact_id',
    'readonly' => true,
    'relationship_name' => 'dri_workflow_contacts',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
  'contact_vouchers' =>
  array (
    'name' => 'contact_vouchers',
    'lhs_module' => 'J_Voucher',
    'lhs_table' => 'j_voucher',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'voucher_id',
    'relationship_type' => 'one-to-one',
    'readonly' => true,
    'relationship_name' => 'contact_vouchers',
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => false,
  ),
);
