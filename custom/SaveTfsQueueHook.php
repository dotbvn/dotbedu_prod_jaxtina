<?php


use Dotbcrm\Dotbcrm\SearchEngine\SearchEngine;

/**
 *
 * Logic hook handler
 *
 */
class SaveTfsQueueHook
{
    const FTS_QUEUE = 'fts_queue';

    protected $db;

    protected $container;

    /**
     * insert to tft_queue table no matter what user modified record
     */
    public function saveTfsQueue($bean, $event, $arguments)
    {
        $this->container = SearchEngine::getInstance()->getEngine()->getContainer();

        /// check if module is enabled
        $enableModules = $this->getAllModules();
        if (!in_array($bean->module_name, $enableModules)) {
            //$this->getLogger()->fatal("saveTfsQueue: $bean->module_name is disabled");
            return;
        }

        // no cookies if you are not a real bean
        if (!$bean instanceof \DotbBean) {
            $this->getLogger()->fatal("Indexbean: Not bean ->" . var_export(get_class($bean), true));
            return;
        }

        $deleteOldRecords = $this->deleteOldRecords($bean->id);

        if ($deleteOldRecords) {
            $this->insertRecord($bean->id, $bean->module_name);
        }

    }

    /**
     * Get search engine object
     * @return \Dotbcrm\Dotbcrm\SearchEngine\SearchEngine
     */
    protected function getSearchEngine()
    {
        return SearchEngine::getInstance();
    }

    /**
     * Get logger object
     * @return \LoggerManager
     */
    protected function getLogger()
    {
        return \LoggerManager::getLogger();
    }

    protected function deleteOldRecords($id)
    {
        $this->db = \DBManagerFactory::getInstance();

        $sql = 'DELETE FROM ' . self::FTS_QUEUE .  ' WHERE bean_id = \''. $id . '\'';

        $this->db->query($sql);
        return true;
    }

    protected function insertRecord($id, $module)
    {
        $this->db = \DBManagerFactory::getInstance();

        $sql = sprintf(
            'INSERT INTO %s (id, bean_id, bean_module, date_modified, date_created)
            VALUES (%s, %s, %s, %s, %s)',
            self::FTS_QUEUE,
            $this->db->getGuidSQL(),
            $this->db->quoted($id),
            $this->db->quoted($module),
            $this->db->now(),
            $this->db->now()
        );
        $this->db->query($sql);
    }

    /**
     * Get all enable module
     */
    protected function getAllModules()
    {
        return $this->container->metaDataHelper->getAllEnabledModules();
    }
}
