<?php

$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/utils.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/lib/jcore.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/utils.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/style.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/arraylist.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/base.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/modal.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/proxy.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/element.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/element_helper.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/container.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/window.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/action.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/menu.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/checkbox_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/separator_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/menu_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/layout.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/tooltip.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/form.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/field.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/validator.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/validator_types.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/field_types.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/button.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/rest_proxy.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/dotb_proxy.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/item_matrix.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/item_updater.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/html_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/store.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/grid.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/history_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/log_field.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/message_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/updater_field.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/note_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/reassign_field.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/reassign_form.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/data_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/single_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/list_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/item_container_control.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/field_panel_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/field_panel_button.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/field_panel_button_group.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/collapsible_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/form_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/list_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/multiple_collapsible_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/field_panel_item_factory.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/field_panel.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/multiple_item.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/email_picker.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/expression_builder2.js';
$js_groupings_hash['dotbcrm9'][] = 'include/javascript/pmse/ui/criteria_field.js';