<?php

$js_groupings_hash['dotbcrm5'][] = 'include/javascript/datetimepicker/jquery.datetimepicker.full.min.js';
$js_groupings_hash['dotbcrm5'][] = 'modules/Administration/smsPhone/javascript/jquery.jmpopups-0.5.1.js';
$js_groupings_hash['dotbcrm5'][] = 'modules/Administration/smsPhone/javascript/smsPhone.js';
$js_groupings_hash['dotbcrm5'][] = 'include/javascript/popup_parent_helper.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/heart.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/date.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/toastr/toastr.min.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/mk/mk-notifications.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/jquery-confirm/jquery-confirm.min.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/customRoutes.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/fte-config-route.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/socket.io.min.js';
$js_groupings_hash['dotbcrm5'][] = 'include/CallCenter/CallCenter.js';
$js_groupings_hash['dotbcrm5'][] = 'include/CallCenter/vcs.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/jssip.min.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/sipjs/sip.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/sdp-transform.min.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/MultiStreamsMixer.min.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/dotbcallcenter.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/file_upload/fileUpload.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/jquery-emoji/jquery.jb-emoji.js';
$js_groupings_hash['dotbcrm5'][] = 'custom/include/javascript/s3Upload/s3upload.js';