<?php

// Since there won't be a js for BR anymore, its files
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ui/expression_container.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ui/decision_table.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ui/close_list_item.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ui/dropdown_selector.js';
// files under the modules/pmse_Inbox/js/ folder
$js_groupings_hash['dotbcrm10'][] = 'modules/pmse_Inbox/js/formAction.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/lib/jquery.layout-latest.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/tree.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/drag_behavior.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/drop_behavior.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/shapes.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/flow.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/command.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/command_annotation_resize.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/command_single_property.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/container_behavior.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/resize_behavior.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/project.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/canvas.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/marker.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/event.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/gateway.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/activity.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/artifact.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/properties_grid.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/artifact_resize_behavior.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/command_default_flow.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/command_connection_condition.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/command_reconnect.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/pmtree.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/progrid.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ErrorMessageItem.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ListContainer.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ErrorListItem.js';
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/ErrorListPanel.js';
//including next files to original file to have one request only
$js_groupings_hash['dotbcrm10'][] = 'include/javascript/pmse/designer.js';