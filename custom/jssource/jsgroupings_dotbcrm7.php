<?php

$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/tutorial.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/bwc.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/utils.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/utils-filters.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/utils-search.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/field.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/hacks.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/alert.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/language.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/help.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/hbs-helpers.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/underscore-mixins.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/filter-analytics.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/metadata-manager.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/sweetspot.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/tooltip.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/import-export-warnings.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/shortcuts.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/accessibility/accessibility.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/accessibility/click.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/accessibility/label.js';
$js_groupings_hash['dotbcrm7'][] = 'include/javascript/dotb7/clipboard.js';