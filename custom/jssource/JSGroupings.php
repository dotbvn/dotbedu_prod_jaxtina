<?php

foreach (glob('custom/jssource/jsgroupings_*.php') as $path) {
    require_once $path;
}

if (isset($GLOBALS['tenant_id'])) {
    foreach (glob(tenancy_path('custom/jssource/jsgroupings_*.php')) as $path) {
        require_once $path;
    }
}