<?php


$viewdefs['portal']['view']['panel-top'] = array(
    'buttons' => array(
        array(
           'type' => 'actiondropdown',
            'name' => 'panel_dropdown',
            'css_class' => 'pull-right',
            'notCustomButton' => true,
            'buttons' => array(
            ),
        ),
    ),
    'fields' => array(
        array(
            'name' => 'collection-count',
            'type' => 'collection-count',
        ),
    ),
);
