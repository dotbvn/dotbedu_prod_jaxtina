<?php

//tao moi
use GuzzleHttp\Psr7\Request;

class NotificationMobile
{
    public static $channel_list = array(
        'C_News'       => 'news',
        'C_Gallery'    => 'news',
        'Contacts'  => 'chat',
        'Leads'     => 'chat',
        'Cases'        => 'chat',
        'C_Comments'   => 'chat',
        'Notifications'    => 'chat',
        'J_PaymentDetail'  => 'payment',
        'J_Payment'        => 'payment',
        'C_Attendance'     => 'course',
        'J_Class'          => 'course',
        'J_Gradebook'      => 'course',
        'J_GradebookDetail'=> 'course',
    );

    public function pushNotification( $title = '', $body = '', $module_name = '', $module_id = '', $to_id = '', $type = 'Student', $team_set_id = ''){
        //Send to all User in brand
        global $dotb_config;
        $brand_id   = str_replace(' ','',(empty($dotb_config['brand_id']) ? ($dotb_config['brand_name']) : ($dotb_config['brand_id'])));
        $__type     = strtolower($type);
        $brand_id   = strtolower($brand_id);
        $channel    = self::$channel_list[$module_name];
        //If channel is empty -> default module will be Notifications - HoangHvy
        if(empty($channel)) {
            $module_id = '';
            $channel = 'chat';
        }
        if(!empty($to_id)) $channel .= ".$to_id"; else $to_id = 'all';

        if(!empty($GLOBALS['dotb_config']['FCM_SERVER_KEY']) && !empty($brand_id)){

            //New notification
            $curToken[] = "/topics/$brand_id.$__type.$channel";
            if(!empty($title)){
                //Create new Notification
                $notify = BeanFactory::getBean("Notifications");
                $notify->id = create_guid();
                $notify->new_with_id = true;
                $notify->name        = $title;
                //assigned student id
                $ext = '';
                if($module_name == 'C_News'){
                    $notify->assigned_user_id         = 'all';
                    $ext = "AND assigned_user_id = 'all'";
                    $notify->team_set_id           = $team_set_id;
                } else {
                    $notify->assigned_user_id         = $to_id;
                    $ext = "AND assigned_user_id = '$to_id'";
                }
                //Xử lý TH tin nhan noi bo
                if(empty($module_id)){
                    $module_id   = $notify->id;
                    $module_name = 'Notifications';
                }

                $notify->parent_id          = $module_id;
                $notify->parent_type        = $module_name;
                $notify->apps_type          = $type;
                //Giữ lại các tin nhắn cũ
                //if(!empty($module_id))
                //    $GLOBALS['db']->query("DELETE FROM notifications WHERE parent_id='$module_id' AND parent_type='$module_name' AND apps_type LIKE '%{$type}%' $ext ");

                $notify->description        = $body;
                //set is_read to no
                $notify->is_read            = 0;
                //set the level of severity
                $notify->severity           = "success";
                $notify->save();

                if($module_name == "Cases"){
                    $parent_model = $this->getLastComment($module_id);
                } else if($module_name == "J_PaymentDetail"){
                    $parent_model = $this->getLastPaymentdetail($module_id);
                }

                foreach($curToken as $ind => $token){
                    $ch = curl_init();
                    $data = array (
                        'notification' =>
                        array (
                            'title' => $title,
                            'body'  => $body,
                            'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                        ),
                        'priority' => 'high',
                        'data' =>
                        array (
                            'title' => $title,
                            'body'  => $body,
                            'click_action'  => 'FLUTTER_NOTIFICATION_CLICK',
                            'id'            => '1',
                            'status'        => 'done',
                            'primaryId'     => $notify->id,
                            'module_name'   => $module_name,  //schedules,dailyreport,payments,payment,news
                            'record_id'     => $module_id,
                            'type'          => $type,
                            'student_id'    => $to_id,
                            'user_id'       => $to_id,
                            'date_entered'  => date('Y-m-d H:i:s'),
                            'parent_model' => json_encode($parent_model)
                        ),
                        'to' => $token,
                    );
                    //goc /topics/dotb.student.news
                    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    $headers = array();
                    $headers[] = 'Content-Type: application/json';
                    $headers[] = 'Authorization: key='.$GLOBALS['dotb_config']['FCM_SERVER_KEY'];
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $err = '';
                    $result = curl_exec($ch);
                    if (curl_errno($ch)) {
                        $err = 'Error:' . curl_error($ch);
                    }
                    curl_close ($ch);
                    //end goc
                }
                return true;
            }
        }
    }
    function getLastPaymentdetail($receipt_id){
        $q3 = "SELECT DISTINCT
                IFNULL(j_paymentdetail.id, '') receipt_id,
                IFNULL(l1.id, '') payment_id,
                IFNULL(l2.id, '') student_id,
                IFNULL(j_paymentdetail.name,'') receipt_code,
                j_paymentdetail.expired_date expired_date,
                j_paymentdetail.payment_date receipt_date,
                j_paymentdetail.payment_datetime receipt_datetime,
                j_paymentdetail.payment_amount receipt_amount,
                j_paymentdetail.audited_amount audited_amount,
                IFNULL(j_paymentdetail.status, '') status,
                IFNULL(j_paymentdetail.reconcile_status, '') reconcile_status,
                IFNULL(j_paymentdetail.payment_method, '') method,
                IFNULL(j_paymentdetail.bank_account, '') bank_account,
                IFNULL(j_paymentdetail.description, '') description,
                IFNULL(l4.name, '') course_fee,
                IFNULL(l1.payment_type, '') payment_type,
                IFNULL(l1.kind_of_course, '') kind_of_course,
                IFNULL(l3.name, '') team_name
                FROM j_paymentdetail
                INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
                INNER JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
                INNER JOIN teams l3 ON l1.team_id = l3.id AND l3.deleted = 0
                LEFT JOIN j_coursefee_j_payment_1_c l4_1 ON l1.id = l4_1.j_coursefee_j_payment_1j_payment_idb AND l4_1.deleted = 0
                LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_1j_coursefee_ida AND l4.deleted = 0
                WHERE j_paymentdetail.id = '$receipt_id'";
        $result = $GLOBALS['db']->fetchOne($q3);
        return $result;
    }
    //Add by HoangHvy to get Comment
    public function getLastComment($case_id){
        global $timedate, $locale;

        $q = "SELECT DISTINCT
            IFNULL(l1.id, '') case_id,
            IFNULL(c_comments.id, '') primaryid,
            IFNULL(c_comments.description, '') description,
            IFNULL(c_comments.direction, '') direction,
            c_comments.date_entered date_entered,
            IFNULL(l2.id, '') user_id,
            IFNULL(l2.first_name, '') first_name,
            IFNULL(l2.last_name, '') last_name,
            IFNULL(l2.picture, '') avatar,
            IFNULL(c_comments.filename, '') filename,
            IFNULL(c_comments.file_ext, '') file_ext,
            IFNULL(c_comments.file_mime_type, '') file_mime_type,
            IFNULL(c_comments.is_read_inems, 0) is_read_inems,
            IFNULL(c_comments.is_read_inapp, 0) is_read_inapp,
            IFNULL(c_comments.is_updated, 0) is_updated
           FROM c_comments
             INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases'
             LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
           WHERE (((l1.id = '{$case_id}')))
                AND c_comments.is_unsent = 0 AND c_comments.deleted = 0  AND c_comments.date_entered = (SELECT MAX(date_entered) FROM c_comments)";
        $row = $GLOBALS['db']->fetchOne($q);

        $qfile = "SELECT google_url, file_mime_type FROM notes WHERE parent_id = '{$row['primaryid']}' AND parent_type = 'C_Comments' AND deleted = 0";
        $result = $GLOBALS['db']->fetchArray($qfile);
        $attachment = [];
        foreach($result as $file){
            $is_image = false;
            if(explode("/",$file['file_mime_type'])[0] == 'image'){
                $is_image = true;
            }
            $attachment[] = [
                'is_image' => $is_image,
                's3Link' => $file['google_url']
            ];
        }
        if(!empty($row)) {
            return array(
                'id' => $row['primaryid'],
                'direction' => $row['direction'],
                'description' => $row['description'],
                'date_entered' => $timedate->to_display_date_time($row['date_entered']),
                'created_by' => $row['user_id'],
                'created_by_name' => $locale->getLocaleFormattedName($row['first_name'], $row['last_name']),
                'created_by_avatar' => $row['avatar'],
                'attachment' => $attachment,
                'is_read_inems' => $row['is_read_inems'],
                'is_read_inapp' => $row['is_read_inapp'],
                'is_updated' => $row['is_updated'],
            );
        }
    }
    public function deleteNotification($assigned_user_id = '', $app_type = 'Student'){
        if (!empty($assigned_user_id))
            $GLOBALS['db']->query("DELETE FROM notifications WHERE assigned_user_id = '$assigned_user_id' AND apps_type = '$app_type' AND deleted = 0");
    }

    //Push notification to Metrikal Apps
    public function pushNotificationMetrikal($title='',$body='', $user_id='', $report_id=''){
        $ch = curl_init();
        $to = "/topics/metrikal.".$GLOBALS['dotb_config']['brand_name'].".alert.".$user_id;
        //if(empty($user_id)) $to = "/topics/dotb.bi.all";
        $data = array(
            'notification' =>
            array(
                'title' => $title,
                'body' => $body,
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
            ),
            'priority' => 'high',
            'data' =>
            array(
                'title' => $title,
                'body' => $body,
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                'id' => '1',
                'status' => 'done',
                'primaryId' => $user_id,
                'module_name' => 'Reports',
                'record_id' => $report_id,
            ),
            'to' => $to
        );
        //goc /topics/dotb.student.news
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=' . $GLOBALS['dotb_config']['FCM_SERVER_KEY'];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $err = '';
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $err = 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return true;
        //end goc
    }
}
