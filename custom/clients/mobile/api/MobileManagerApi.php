<?php

class MobileManagerApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'get-list-report-v2' => array(
                'reqType' => 'POST',
                'path' => array('manager','getlistreport'),
                'pathVars' => array(''),
                'method' => 'getListReport',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'get-stats-report-v2' => array(
                'reqType' => 'POST',
                'path' => array('manager','getliststats'),
                'pathVars' => array(''),
                'method' => 'getListStats',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'get-report-chart-v2' => array(
                'reqType' => 'POST',
                'path' => array('manager','chart-report'),
                'pathVars' => array(''),
                'method' => 'getReportChart',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'update-stats-dashlet' => array(
                'reqType' => 'POST',
                'path' => array('manager','update-stats-dashlet'),
                'pathVars' => array(''),
                'method' => 'updateStatsDashlet',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'update-reports-dashlet' => array(
                'reqType' => 'POST',
                'path' => array('manager','update-reports-dashlet'),
                'pathVars' => array(''),
                'method' => 'updateReportsDashlet',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get-stats-info' => array(
                'reqType' => 'GET',
                'path' => array('manager','get-stats-info'),
                'pathVars' => array(''),
                'method' => 'getStatsInfo',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get-stats-dasboard' => array(
                'reqType' => 'GET',
                'path' => array('manager','get-dashboard'),
                'pathVars' => array(''),
                'method' => 'getDashboard',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'get-list-report' => array(
                'reqType' => 'POST',
                'path' => array('getlistreport'),
                'pathVars' => array(''),
                'method' => 'getListReport',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'get-stats-report' => array(
                'reqType' => 'POST',
                'path' => array('getliststats'),
                'pathVars' => array(''),
                'method' => 'getListStats',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'get-report-chart' => array(
                'reqType' => 'POST',
                'path' => array('chart-report'),
                'pathVars' => array(''),
                'method' => 'getReportChart',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'create-record' => array(
                'reqType' => 'POST',
                'path' => array('createrecord'),
                'pathVars' => array(''),
                'method' => 'updateReportsDashlet',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get-stats-info-v2' => array(
                'reqType' => 'GET',
                'path' => array('v2','manager', 'get-stats-info'),
                'pathVars' => array(''),
                'method' => 'getStatsInfoV2',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'set-scale-target' => array(
                'reqType' => 'POST',
                'path' => array('manager', 'set-scale-target'),
                'pathVars' => array(''),
                'method' => 'setScaleTarget',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get-scale-target' => array(
                'reqType' => 'GET',
                'path' => array('manager', 'get-scale-target'),
                'pathVars' => array(''),
                'method' => 'getScaleTarget',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'delete-scale-target' => array(
                'reqType' => 'POST',
                'path' => array('manager', 'delete-scale-target'),
                'pathVars' => array(''),
                'method' => 'deleteScaleTarget',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }

    function getListReport(ServiceBase $api, array $args)
    {
        $reports = array();

        if (!empty($args['api_user_id'])) {

            $GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['api_user_id']);
            $current_user = $GLOBALS['current_user'];
            if (!$current_user->isAdmin()) {
                $ext_query = " AND saved_reports.team_set_id IN (SELECT tst.team_set_id
            FROM team_sets_teams tst
            INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
            AND team_memberships.user_id = '" . $current_user->id . "' AND team_memberships.deleted = 0)";
            }

            $sqlReports = "SELECT DISTINCT
            IFNULL(saved_reports.id, '') primaryid,
            IFNULL(saved_reports.name, '') name,
            IFNULL(saved_reports.num, '') num,
            IFNULL(saved_reports.description, '') description,
            IFNULL(saved_reports.chart_type, 'none') chart_type,
            IFNULL(saved_reports.report_type, '') report_type,
            IFNULL(saved_reports.module, '') module,
            IFNULL(REPLACE(saved_reports.list_of ,'^',''), '') category
            FROM saved_reports
            WHERE saved_reports.chart_type != 'none'
            AND saved_reports.num IS NOT NULL
            AND saved_reports.report_type != 'stats'
            AND saved_reports.deleted = 0 $ext_query
            ORDER BY saved_reports.num ASC";

            $result = $GLOBALS['db']->query($sqlReports);
            while ($r = $GLOBALS['db']->fetchbyAssoc($result)) {
                $reports[$r['primaryid']] = array(
                    'id' => $r['primaryid'],
                    'name' => $r['name'],
                    'num' => $r['num'],
                    'description' => $r['description'],
                    'chart_type' => $r['chart_type'],
                    'report_type' => $r['report_type'],
                    'module' => $r['module'],
                    'categories' => $r['category'],
                );
            }

            return array(
                'success' => 1,
                'report_list' => array_values($reports),
            );
        }
        return array(
            'success' => 0,
            'message' => 'Missing required parameters',
        );
    }
    //Add by HoHoangHvy to get stats report for manager app
    function getListStats(ServiceBase $api, array $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['api_user_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $reports = array();

        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['api_user_id']);
        $current_user = $GLOBALS['current_user'];
        if (!$current_user->isAdmin()) {
            $ext_query = " AND saved_reports.team_set_id IN (SELECT tst.team_set_id
        FROM team_sets_teams tst
        INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
        AND team_memberships.user_id = '" . $current_user->id . "' AND team_memberships.deleted = 0)";
        }
        $sqlReports = "SELECT DISTINCT
        IFNULL(saved_reports.id, '') primaryid,
        IFNULL(saved_reports.name, '') name,
        IFNULL(saved_reports.num, '') num,
        IFNULL(saved_reports.description, '') description,
        IFNULL(saved_reports.module, '') module,
        IFNULL(REPLACE(saved_reports.list_of ,'^',''), '') category
        FROM saved_reports
        WHERE saved_reports.num IS NOT NULL
        AND saved_reports.report_type = 'stats'
        AND saved_reports.stats_type = 'scale'
        AND saved_reports.deleted = 0 $ext_query
        ORDER BY saved_reports.num ASC";

        $result = $GLOBALS['db']->query($sqlReports);
        while ($r = $GLOBALS['db']->fetchbyAssoc($result)) {
            $reports[$r['primaryid']] = array(
                'id' => $r['primaryid'],
                'name' => $r['name'],
                'num' => $r['num'],
                'description' => $r['description'],
                'module' => $r['module'],
                'categories' => $r['category'],
            );
        }

        return array(
            'success' => true,
            'data' => [
                'report_list' => array_values($reports),
            ],
            'error_info' => throwApiError(1)
        );
    }
    function getDashboard(ServiceBase $api, array $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['api_user_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $q = "SELECT DISTINCT 
                IFNULL(dbs.id, '') id, 
                IFNULL(dbs.name, '') dash_name, 
                IFNULL(dbs.metadata, '') metadata, 
                IFNULL(dbs.dashboard_module, '') dashboard_module
            FROM dashboards dbs
            WHERE is_mobile_dashboard = '1' AND deleted = 0 AND assigned_user_id = '{$args['api_user_id']}'";
        $result = $GLOBALS['db']->fetchOne($q);
        if(!empty($result['id'])){
            $return_data = [
                'id' => $result['id'],
                'name' => $result['dash_name'],
                'metadata' => json_decode($result['metadata']),
                'dashboard_module' => $result['dashboard_module'],
            ];
        } else {
            $GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['api_user_id']);
            $current_user = $GLOBALS['current_user'];
            if (!$current_user->isAdmin()) {
                $ext_query_report = " AND saved_reports.team_set_id IN (SELECT tst.team_set_id
                FROM team_sets_teams tst
                INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
                AND team_memberships.user_id = '" . $current_user->id . "' AND team_memberships.deleted = 0)";
            }
            $sqlReports = "SELECT DISTINCT
            IFNULL(saved_reports.id, '') primaryid
            FROM saved_reports
            WHERE saved_reports.num IS NOT NULL
            AND saved_reports.report_type = 'stats'
            AND saved_reports.stats_type = 'scale'
            AND saved_reports.deleted = 0 $ext_query_report
            ORDER BY saved_reports.num ASC
            LIMIT 8";
            $rows = array_column($GLOBALS['db']->fetchArray($sqlReports), 'primaryid');
            $stats_list_str = implode('","', $rows);

            $dashboard = BeanFactory::newBean('Dashboards');
            $metadata_string = '{
                       "components":[
                          {
                             "rows":[
                                 [
                                    {
                                    "view":{
                                        "type":"stats-report",
                                        "label":"Stat Report",
                                        "module":null,
                                        "id_dashlet":"'.create_guid().'",
                                        "display_type":"grid",
                                        "display_stats":["'.$stats_list_str.'"]
                                    },
                                    "width":12
                                    }
                                 ]
                             ],
                             "width":12
                          }
                       ]
                    }';
            $dashboard->metadata = str_replace(["\n", "\t", "\r"], '', $metadata_string);
            $dashboard->name = 'Manager Default Dashboard';
            $dashboard->assigned_user_id = $current_user->id;
            $dashboard->dashboard_module = 'Home';
            $dashboard->default_dashboard = 1;
            $dashboard->is_mobile_dashboard = '1';
            $dashboard->save();
            $return_data = [
                'id' => $dashboard->id,
                'name' => $dashboard->name,
                'metadata' => json_decode($dashboard->metadata),
                'dashboard_module' => $dashboard->dashboard_module,
            ];
        }
        if(!empty($return_data)){
            return array(
                'success' => true,
                'data' => [
                    'dashboard_model' => $return_data
                ],
                'error_info' => throwApiError(1)
            );
        } else{
            return array(
                'success' => false,
                'error_info' => throwApiError(202)
            );
        }


    }
    function updateStatsDashlet(ServiceBase $api, array $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['stats_list_id', 'api_user_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $stats_list = json_encode($args['stats_list_id']);
        $query = "UPDATE dashboards
              SET metadata = JSON_SET(metadata, '$.components[0].rows[0][0].view.display_stats', CAST('{$stats_list}' AS JSON))
              WHERE assigned_user_id = '{$args['api_user_id']}'
              AND is_mobile_dashboard = '1'";
        $GLOBALS['db']->query($query);
        return array(
            'success' => true,
            'error_info' => throwApiError(1)
        );
    }
    //end by HoHoangHvy
    function getReportChart(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['api_user_id', 'reportId']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $returnData = array();

        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['api_user_id']);

        $chartReport = BeanFactory::getBean('Reports', $args['reportId'], array("encode" => false));

        if (!empty($chartReport)) {
            $reportData = array();

            $reporter = new Report($chartReport->content);
            $reporter->is_saved_report = true;
            $reporter->saved_report_id = $chartReport->id;

            $tmp = new Report($chartReport->content);
            $tmp->is_saved_report = true;
            $tmp->saved_report_id = $chartReport->id;

            if (!empty($args['begin_date'])) {
                if (isset($reporter->report_def['filters_def']['Filter_1'][0]['operator'])) {
                    for ($i = count($reporter->report_def['filters_def']['Filter_1']) - 2; $i >= 0; $i--) {
                        for ($j = count($reporter->report_def['filters_def']['Filter_1'][$i]) - 2; $j >= 0; $j--) {
                            if (empty($tmp->report_def['filters_def']['Filter_1'][$i][$j]['type'])) {
                                $tmp->register_field_for_query($tmp->report_def['filters_def']['Filter_1'][$i][$j]);
                            }
                            if (!empty($tmp->report_def['filters_def']['Filter_1'][$i][$j]['type'])
                                && ($tmp->report_def['filters_def']['Filter_1'][$i][$j]['runtime'] == '1')
                                && ($tmp->report_def['filters_def']['Filter_1'][$i][$j]['type'] == 'datetime'
                                    || $tmp->report_def['filters_def']['Filter_1'][$i][$j]['type'] == 'datetimecombo'
                                    || $tmp->report_def['filters_def']['Filter_1'][$i][$j]['type'] == 'date')) {
                                $reporter->report_def['filters_def']['Filter_1'][$i][$j]['qualifier_name'] = 'between_dates';
                                $reporter->report_def['filters_def']['Filter_1'][$i][$j]['input_name0'] = $args['begin_date'];
                                $reporter->report_def['filters_def']['Filter_1'][$i][$j]['input_name1'] = $args['end_date'];
                                break;
                            }
                        }
                    }
                } else {
                    for ($i = count($reporter->report_def['filters_def']['Filter_1']) - 2; $i >= 0; $i--) {
                        if (empty($tmp->report_def['filters_def']['Filter_1'][$i]['type'])) {
                            $tmp->register_field_for_query($tmp->report_def['filters_def']['Filter_1'][$i]);
                        }
                        if (!empty($tmp->report_def['filters_def']['Filter_1'][$i]['type'])
                            && ($tmp->report_def['filters_def']['Filter_1'][$i]['runtime'] == '1')
                            && ($tmp->report_def['filters_def']['Filter_1'][$i]['type'] == 'datetime'
                                || $tmp->report_def['filters_def']['Filter_1'][$i]['type'] == 'datetimecombo'
                                || $tmp->report_def['filters_def']['Filter_1'][$i]['type'] == 'date')) {
                            $reporter->report_def['filters_def']['Filter_1'][$i]['qualifier_name'] = 'between_dates';
                            $reporter->report_def['filters_def']['Filter_1'][$i]['input_name0'] = $args['begin_date'];
                            $reporter->report_def['filters_def']['Filter_1'][$i]['input_name1'] = $args['end_date'];
                            break;
                        }
                    }
                }
            }

            $reporter->run_chart_queries();

            $reportData['id'] = $reporter->saved_report_id;
            $reportData['name'] = $reporter->name;
            $reportData['chart_type'] = $reporter->chart_type;
            $reportData['group_defs'] = $reporter->report_def['group_defs'];
            $reportData['summary_columns'] = $reporter->report_def['summary_columns'];
            $reportData['display_columns'] = $reporter->report_def['display_columns'];
            $reportData['chart_rows'] = $reporter->chart_rows;
            $sql_total = $GLOBALS['db']->query($reporter->total_query);
            while ($r = $GLOBALS['db']->fetchbyAssoc($sql_total)) {
                $reportData['grand_total'] = $r;
            }

            // add reportData to returnData
            $returnData['reportData'] = $reportData;

            $chartDisplay = new ChartDisplay();
            $chartDisplay->setReporter($reporter);

            $chart = $chartDisplay->getDotbChart();

            if (!isset($args['ignore_datacheck'])) {
                $args['ignore_datacheck'] = false;
            }

            $json = json_decode($chart->buildJson($chart->generateXML(), $args['ignore_datacheck']));

            $returnData['chartData'] = $json;
        } else {
            return array(
                'success' => false,
                'error_info' => throwApiError(202),
            );
        }
        return array(
            'success' => true,
            'data' => [
                'report_model' => $returnData
            ],
            'error_info' => throwApiError(1)
        );
    }

    function updateReportsDashlet(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['dashboard_id', 'metadata']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $moduleName = 'Dashboards';
        $id = $args['dashboard_id'];

        $bean = BeanFactory::getBean($moduleName, $id);

        $metadataArr = json_decode($bean->metadata, true);
        if(empty($bean->id)){
            return array(
                'success' => false,
                'error_info' => throwApiError(202)
            );
        }
        $dashlet = '['.json_encode($metadataArr["components"][0]["rows"][0][0]).'],';
        foreach ($args['metadata'] as $row) {
            if ($row['type'] == 'saved-reports-chart') {
                $dashlet .= '[
                           {
                              "width":12,
                              "context":{
                                 "module":"' . $row['module'] . '"
                              },
                              "view":{
                                 "label":"' . $row['label'] . '",
                                 "type":"' . $row['type'] . '",
                                 "module":"' . $row['module'] . '",
                                 "saved_report_id":"' . $row['saved_report_id'] . '",
                                 "saved_report":"' . $row['saved_report'] . '",
                                 "allowScroll":true,
                                 "colorData":"class",
                                 "hideEmptyGroups":true,
                                 "reduceXTicks":true,
                                 "rotateTicks":true,
                                 "show_controls":false,
                                 "show_title":true,
                                 "show_x_label":false,
                                 "show_y_label":false,
                                 "staggerTicks":true,
                                 "wrapTicks":true,
                                 "x_axis_label":"test1",
                                 "y_axis_label":"test2",
                                 "report_title":"test3",
                                 "show_legend":true,
                                 "stacked":true,
                                 "allow_drillthru":"1",
                                 "vertical":true,
                                 "direction":"ltr",
                                 "chart_type":"' . $row['chart_type'] . '"
                              }
                           }
                        ],';
            }
        }

        $metaData = '{
                   "components":[
                      {
                         "rows":[
                         ' . trim($dashlet, ",") . '
                         ],
                         "width":12
                      }
                   ]
                }';
        $args['metadata'] = str_replace(["\n", "\t", "\r"], '', $metaData);

        unset($args['dashboard_id']);

        //luu
        foreach ($args as $field => $Val)
            $bean->$field = $Val;

        $bean->save();

        return array(
            'success' => true,
            'data' => [
                'bean_id' => $bean->id,
            ],
            'error_info' => throwApiError(1)
        );
    }
    public function getStatsInfo(ServiceBase $api, $args)
    {
        $stat_id = $args['stats_id'];
        if (empty($stat_id)) {
            return array(
                'success' => 0,
                'error' => 'Missing Stats ID'
            );
        }

        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['api_user_id']);

        $stat_bean = BeanFactory::getBean('Reports', $args['stats_id'], array("encode" => false));
        $reporter = new Report($stat_bean->content);
        $reporter->is_saved_report = true;
        $reporter->saved_report_id = $stat_bean->id;
        $detailResult = true;
        if ($args['type'] == 'detail') $detailResult = $this->prepareForDetailReports($reporter, $stat_bean->content, $stat_bean->id, $args);

        if(!$detailResult) return array('success' => false, 'error' => 'Missing Date');

        //Prepare stats data
        $reporter->run_chart_queries();

        $chartDisplay = new ChartDisplay();
        $chartDisplay->setReporter($reporter);
        $chart = $chartDisplay->getDotbChart();
        $stats_to_display = $chart->data_set;

        if(empty($stats_to_display)){
            return array(
                'success' => false,
                'error' => 'Cant retrieve this report'
            );
        }

        // Calculate and prepare comparison data
        if($args['type'] == 'summary'){
            //Logic to handle if the order is desc or asc, it still can be display right data
            $unit = $reporter->report_def['group_defs'][0]['qualifier'];
            if($unit == 'month') {
                if($GLOBALS['current_language'] == 'vn_vn') $stats_to_display = convertMonthKeyToEnglish($stats_to_display);
                uksort($stats_to_display, function($a, $b) {
                    return strtotime($a) - strtotime($b);
                });
            }else if($unit == 'day'){
                uksort($stats_to_display, function($a, $b) {
                    return strtotime($a) - strtotime($b);
                });
            } else {
                uksort($stats_to_display, function($a, $b) {
                    sscanf($a, "W%d %d", $weekA, $yearA);
                    sscanf($b, "W%d %d", $weekB, $yearB);
                    if ($yearA === $yearB) {
                        return $weekA - $weekB;
                    } else {
                        return $yearA - $yearB;
                    }
                });
            }

            $lastElement = end($stats_to_display);
            $keyCurrent = key($lastElement);
            $current_data = $lastElement[$keyCurrent]['numerical_value'];
            $percent = '';
            if(count($chart->data_set) > 1){
                $secondLastElement = prev($stats_to_display);
                $keyPrevious = key($secondLastElement);
                $previous_data = $secondLastElement[$keyPrevious]['numerical_value'];
                $percent = round(((($current_data / $previous_data) - 1)*100));
            } else {
                $previous_data = 0;
            }
            $keyCompare = key(array_splice($stats_to_display, count($stats_to_display) - 1, 1));
            $is_not_available = false;

            if($unit == 'month') {
                $compare = DateTime::createFromFormat("F Y", $keyCompare);
                $date = $compare->format('Y-m');
                if(date("Y-m") != $date){
                    $is_not_available = true;
                }
            }else if($unit == 'day'){
                if(date("Y-m-d") != strtotime($keyCompare)){
                    $is_not_available = true;
                }
            } else {
                sscanf($keyCompare, "W%d %d", $week, $year);
                if(date("W") != $week || date("Y") != $year){
                    $is_not_available = true;
                }
            }

            if($is_not_available) {
                return array(
                    'success' => true,
                    'data' => array(
                        'id' => $reporter->saved_report_id,
                        'name' => $reporter->name,
                        'error_throw' => 'Not Available',
                    )
                );
            }
            //Previous Data is the difference between two months, it will be renamed on the next version
            return array(
                'success' => true,
                'data' => array(
                    'id' => $reporter->saved_report_id,
                    'name' => $reporter->name,
                    'unit' => $reporter->report_def['group_defs'][0]['qualifier'],
                    'percent' => $percent,
                    'current_stats' => thousandCurrencyFormat($current_data),
                    'previous_stats' => thousandCurrencyFormat($current_data - $previous_data)
                )
            );
        } else if($args['type'] == 'detail'){
            $reportData['id'] = $reporter->saved_report_id;
            $reportData['name'] = $reporter->name;
            $reportData['chart_type'] = $reporter->chart_type;
            $reportData['group_defs'] = $reporter->report_def['group_defs'];
            $reportData['summary_columns'] = $reporter->report_def['summary_columns'];
            $reportData['display_columns'] = $reporter->report_def['display_columns'];
            $reportData['chart_rows'] = $reporter->chart_rows;
            $sql_total = $GLOBALS['db']->query($reporter->total_query);
            while ($r = $GLOBALS['db']->fetchbyAssoc($sql_total)) {
                $reportData['grand_total'] = $r;
            }
            $returnData['reportData'] = $reportData;

            if (!isset($args['ignore_datacheck'])) {
                $args['ignore_datacheck'] = false;
            }

            $json = json_decode($chart->buildJson($chart->generateXML(), $args['ignore_datacheck']));

            $returnData['chartData'] = $json;

            return array(
                'success' => true,
                'data' => $returnData
            );
        } else {
            return array(
                'success' => 0,
                'error' => 'Invalid Type'
            );
        }
    }


    public function getStatsInfoV2(ServiceBase $api, $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['stats_id', 'api_user_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        $year = isset($args['year']) ? $args['year'] : date("Y");
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['api_user_id']);

        $scaleBean = BeanFactory::getBean('Reports', $args['stats_id'], array("encode" => false));
        $scaleReporter = new Report($scaleBean->content);
        $scaleReporter->is_saved_report = true;
        $scaleReporter->saved_report_id = $scaleBean->id;

        if ($args['type'] == 'detail') {
            $effiBean = BeanFactory::getBean('Reports', $scaleBean->relate_effi_id, array("encode" => false));
            if(!empty($effiBean->id)){
                $effi = new Report($effiBean->content);
                $effi->is_saved_report = true;
                $effi->saved_report_id = $effiBean->id;
                $this->prepareForDetailReports($effi, $effiBean->content, $effiBean->id, $year);
                $effi->run_summary_query();
            }

            $this->prepareForDetailReports($scaleReporter, $scaleBean->content, $scaleBean->id, $year);
        }
        //Prepare stats data
        $scaleReporter->run_summary_query();
        $scales = $this->generateReportArray($scaleReporter);

        if (empty($scales)) {
            return array(
                'success' => false,
                'error_info' => throwApiError(202)
            );
        }

        // Calculate and prepare comparison data
        if ($args['type'] == 'summary') {
            $is_not_available = false;
            if($scales[1]['month'] != date("Y-m") || !isset($scales[1]['month'])){
                $is_not_available = true;
            }
            $current_data = $scales[1]['value'];
            $percent = '';
            if (count($scales) > 1) {
                $previous_data = $scales[0]['value'];
                if($previous_data == 0){
                    $is_not_available = true;
                }
                $percent = round(((($current_data / $previous_data) - 1) * 100));
            } else {
                $previous_data = 0;
            }
            if ($is_not_available) {
                return array(
                    'success' => true,
                    'data' => array(
                        'scale_id' => $scaleReporter->saved_report_id,
                        'scale_name' => $scaleReporter->name,
                        'error_throw' => 'Not Available',
                    ),
                    'error_info' => throwApiError(1)
                );
            }
            return array(
                'success' => true,
                'data' => array(
                    'year' => $year,
                    'scale_name' => $scaleReporter->name,
                    'scale_id' => $scaleReporter->saved_report_id,
                    'scale_num' => $scaleBean->num,
                    'percent' => $percent,
                    'current_stats' => thousandCurrencyFormat($current_data),
                    'difference' => thousandCurrencyFormat($current_data - $previous_data)
                ),
                'error_info' => throwApiError(1)
            );
        } else if ($args['type'] == 'detail') {
            $data['year'] = $year;
            $data['scale_name'] = $scaleBean->name;
            $data['scale_num'] = (string)$scaleBean->num;
            $data['scale_id'] = $scaleBean->id;
            $data['effi_name'] = $effiBean->name;
            $data['effi_num'] = (string)$effiBean->num;
            $data['effi_id'] = $effiBean->id;


            if (!isset($args['ignore_datacheck'])) {
                $args['ignore_datacheck'] = false;
            }

            $efiiArray = $this->generateReportArray($effi);

            $values = [];
            $index = 0;
            foreach($scales as $value){
                $effiValue = 0;
                if($value['month'] == $efiiArray[$index]['month'] && !empty($efiiArray[$index]['value'])){
                    $effiValue = $efiiArray[$index]['value'];
                } else {
                    $index--;
                }
                $scaleValue = empty($value['value']) ? 0 : $value['value'];

                $values[] =
                    [
                        'month' => $value['month'],
                        'scale_value' => $scaleValue,
                        'effi_value' => $effiValue
                    ];
                $index++;
            }

            $data['values'] = $values;

            return array(
                'success' => true,
                'data' => $data,
                'error_info' => throwApiError(1)
            );
        } else {
            return array(
                'success' => false,
                'error_info' => throwApiError(106)
            );
        }
    }

    private function prepareForDetailReports(&$reporter, $content, $id, $year)
    {
        $tmp = new Report($content);
        $tmp->is_saved_report = true;
        $tmp->saved_report_id = $id;
        $reportFilterDef = $reporter->report_def['filters_def']['Filter_1'][0];
        $newFilter = [];
        $newFilter['operator'] = $reportFilterDef['operator'];
        unset($reportFilterDef['operator']);
        $is_date = false;
        foreach($reportFilterDef as $item){
            if(empty($item['type']) && !$is_date){
                $tmp->register_field_for_query($item);
            }
            if (!empty($item['type']) && !$is_date
                && ($item['runtime'] == '1')
                && ($item['type'] == 'datetime' || $item['type'] == 'datetimecombo' || $item['type'] == 'date')) {
                $is_date = true;
                $item['qualifier_name'] = 'between_dates';
                $item['input_name0'] = '01/01/'.$year;
                $item['input_name1'] = '31/12/'.$year;
                $item['type'] = 'date';
            }
            $newFilter[] =  $item;
        }
        if(!empty($newFilter)){
            $reporter->report_def['filters_def']['Filter_1'] = [0 => $newFilter];
            $reporter->report_def['filters_def']['Filter_1']['operator'] = 'OR';
        }
    }
    private function generateReportArray($reporter){
        if(empty($reporter->summary_query)) return [];
        $reportSummaryArray =  $GLOBALS['db']->fetchArray($reporter->summary_query);
        $reportsValueList = [];
        $groupFunction = '';
        foreach($reporter->report_def['summary_columns'] as $summary){
            if(!empty($summary['group_function']) || isset($summary['group_function'])) $groupFunction = $summary['group_function'];
        }
        foreach($reportSummaryArray as $summary_value){
            $data = [];
            foreach($summary_value as $key => $value){
                if(strpos(strtolower($key), '_month')){
                    $data['month'] = $value;
                } else if (strpos(strtolower($key), '_'.$groupFunction)) {
                    $data['value'] = (double)$value;
                    $reportsValueList[] = $data;
                }
            }
        }
        return $reportsValueList;
    }
    function setScaleTarget(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');
        $validate_param = validateParam($args, ['user_id', 'stats_id', 'target']);
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['user_id']);

        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $report_id = $args['stats_id'];
        $user_id = $args['user_id'];
        $q = "SELECT 
            IFNULL(saved_reports.id, '') primaryid,
            IFNULL(saved_reports.name, '') name
            FROM saved_reports
            WHERE saved_reports.id = '$report_id'";
        $result = $GLOBALS['db']->fetchOne($q);
        if($result){
            //Count total
            $q1 = "SELECT 
            COUNT(m_metrikalalert.id) count_alert
            FROM m_metrikalalert
            WHERE m_metrikalalert.assigned_user_id = '$user_id' AND m_metrikalalert.deleted = 0";
            $count_total = $GLOBALS['db']->getOne($q1);
            if($count_total == 50){
                return [
                    'success' => false,
                    'error_infio' => throwApiError(119)
                ];
            }
            //Count stats
            $q2 = "SELECT 
            COUNT(m_metrikalalert.report_id) count_report
            FROM m_metrikalalert
            WHERE m_metrikalalert.assigned_user_id = '$user_id' AND m_metrikalalert.deleted = 0 AND m_metrikalalert.report_id = '$report_id'";
            $count_stats = $GLOBALS['db']->getOne($q2);

            if($count_stats == 10){
                return [
                    'success' => false,
                    'error_infio' => throwApiError(120)
                ];
            }

            $metrikal_alert = BeanFactory::newBean('M_MetrikalAlert');
            $metrikal_alert->name = !empty($args['name']) ? $args['name'] : '';
            $metrikal_alert->assigned_user_id = $args['user_id'];
            $metrikal_alert->target = $args['target'];
            $metrikal_alert->report_id = $args['stats_id'];
            $metrikal_alert->report_name = $result['name'];
            $metrikal_alert->save();
            return [
                'success' => true,
                'error_info' => throwApiError(1)
            ];
        } else {
            return [
                'success' => false,
                'error_info' => throwApiError(106)
            ];
        }
    }
    function getScaleTarget(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['user_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $user_id = $args['user_id'];
        $q = "SELECT 
                IFNULL(m_metrikalalert.id, '') AS primaryid,
                IFNULL(m_metrikalalert.name, '') AS primaryname,
                date_entered,
                IFNULL(m_metrikalalert.report_id, '') AS report_id,
                IFNULL(m_metrikalalert.report_name, '') AS report_name,
                IFNULL(m_metrikalalert.target, '') AS target
            FROM m_metrikalalert
            WHERE m_metrikalalert.assigned_user_id = '$user_id' 
                AND deleted = 0 
                AND DATEDIFF(NOW(), date_entered) < 90;";
        $result = $GLOBALS['db']->fetchArray($q);
        $target_list = [];
        foreach ($result as $item){
            $data['alert_id'] = $item['primaryid'];
            $data['stat_id'] = $item['report_id'];
            $data['stat_name'] = $item['report_name'];
            $data['value'] = $item['target'];
            $data['alert_name'] = $item['primaryname'];

            $target_list[] = $data;
        }
        return [
            'success' => true,
            'data' => [
                'target_list' => $target_list
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function deleteScaleTarget(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['alert_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $alert_id = $args['alert_id'];
        $q = "DELETE FROM m_metrikalalert WHERE id = '$alert_id'";
        $result = $GLOBALS['db']->query($q);
        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }
}