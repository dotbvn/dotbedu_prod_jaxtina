<?php


class TeacherApiV2 extends DotbApi {
    function registerApiRest() {
        return array(
            'teacher-login' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'teacher', 'login'),
                'pathVars' => array(''),
                'method' => 'teacherLogin',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'edit-teacher' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'teacher', 'editteacher'),
                'pathVars' => array(''),
                'method' => 'editTeacher',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'teacher-get-teaching-hour' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'teacher', 'getteachinghour'),
                'pathVars' => array(''),
                'method' => 'getTeachingHour',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'teacher-get-teaching-hour2' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'teacher', 'getteachinghour2'),
                'pathVars' => array(''),
                'method' => 'getTeachingHour2',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //New API - Ho Hoang Huy
            'list-notifications' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'teacher', 'listNotifications'),
                'pathVars' => array(''),
                'method' => 'listNotifications',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'count-unread' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'teacher', 'countUnreadNoti'),
                'pathVars' => array(''),
                'method' => 'countUnreadNoti',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'update-notification' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'teacher', 'updateNotification'),
                'pathVars' => array(''),
                'method' => 'updateNotification',
                'shortHelp' => '',
                'longHelp' => ''
            ),

        );
    }
    function getListGallery($classIds, $str_student_id = ''){
        $ext1 = '';
        $ext2 = '';
        if(!empty($str_student_id)){
            $ext1 = "INNER JOIN j_studentsituations l4 ON
            j_class.id = l4.ju_class_id AND l4.deleted = 0
            INNER JOIN contacts l5 ON l4.student_id = l5.id
            AND l5.deleted = 0 AND l4.student_type = 'Contacts'";
            $ext2 = "AND (IFNULL(l4.type, '') IN ('OutStanding' , 'Enrolled'))
            AND (l4.start_study <= l1.gallery_date)
            AND (l4.end_study >= l1.gallery_date)
            AND l5.id IN ('{$str_student_id}')
            AND (IFNULL(l5.status, '') NOT IN ('Cancelled'))";
        }
        $sqlAlbum ="SELECT DISTINCT
        IFNULL(j_class.id, '') class_id,
        IFNULL(j_class.name, '') class_name,
        IFNULL(l1.id, '') gallery_id,
        IFNULL(l1.name, '') gallery_name,
        IFNULL(l1.gallery_date, '') gallery_date,
        IFNULL(l1.description, '') gallery_description,
        IFNULL(l1.gallery_type, '') gallery_type,
        IFNULL(l2.id, '') pic_id,
        IFNULL(l2.google_id, '') google_id,
        IFNULL(l2.google_url, '') google_url,
        IFNULL(l1.date_entered, '') date_entered,
        IFNULL(GROUP_CONCAT(l3.name), '') tag_name
        FROM j_class
        INNER JOIN j_class_c_gallery_1_c l1_1 ON j_class.id = l1_1.j_class_c_gallery_1j_class_ida AND l1_1.deleted = 0
        INNER JOIN c_gallery l1 ON l1.id = l1_1.j_class_c_gallery_1c_gallery_idb AND l1.deleted = 0
        INNER JOIN notes l2 ON l1.id = l2.parent_id AND l2.deleted = 0 AND l2.parent_type = 'C_Gallery'
        LEFT JOIN tag_bean_rel l3_1 ON
        l1.id = l3_1.bean_id AND l3_1.deleted = 0 AND l3_1.bean_module = 'C_Gallery'
        LEFT JOIN tags l3 ON
        l3.id = l3_1.tag_id AND l3.deleted = 0
        {$ext1}
        WHERE j_class.id IN ('".implode("','",$classIds)."') AND j_class.deleted = 0
        {$ext2}
        GROUP BY pic_id
        ORDER BY date_entered DESC";
        $albums = $GLOBALS['db']->query($sqlAlbum);
        $gls = array();
        while($album = $GLOBALS['db']->fetchbyAssoc($albums)){
            if(empty($album['class_id'])) break;
            $gls[$album['class_id']][$album['gallery_id']]['gallery_id']            = $album['gallery_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_id']              = $album['class_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_name']            = $album['class_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_name']          = $album['gallery_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_description']   = $album['gallery_description'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_type']   = $album['gallery_type'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_date']          = $album['gallery_date'];
            $gls[$album['class_id']][$album['gallery_id']]['date_entered']          = date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered']));
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['picture_id']= $album['pic_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['picture_id']= $album['google_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['google_url']= $album['google_url'];
            $gls[$album['class_id']][$album['gallery_id']]['images'][] = $album['google_url'];
            if(!empty($album['tag_name']))
                $gls[$album['class_id']][$album['gallery_id']]['tags']              = explode(",", $album['tag_name']);
            else $gls[$album['class_id']][$album['gallery_id']]['tags']             = array();
        }
        foreach($gls as $class_id => $album){
            foreach ($album as $gallery_id => $albumVal){
                foreach ($albumVal['image_list'] as $pic_id => $picVal){
                    $gls[$class_id][$gallery_id]['image_list'] = array_values($gls[$class_id][$gallery_id]['image_list']);
                }
            }
        }
        return $gls;
    }

    function teacherLogin(ServiceBase $api, array $args) {
        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        global $timedate, $current_user, $app_list_strings;
        if (isset($args) && !empty($args)){
            $now = $timedate->nowDb();
            $user_name  = $args['portal_name'];
            $password   = $args['password'];
            $res = array();

            //Check Username credential
            $q1 = "SELECT DISTINCT
            IFNULL(c_teachers.id, '') primaryid,
            IFNULL(c_teachers.picture, '') picture,
            IFNULL(c_teachers.cover_app, '') cover_app,
            IFNULL(c_teachers.teacher_id, '') teacher_id,
            IFNULL(c_teachers.full_teacher_name, '') name,
            IFNULL(c_teachers.first_name, '') first_name,
            IFNULL(c_teachers.last_name, '') last_name,
            IFNULL(c_teachers.title, '') title,
            c_teachers.dob birthdate,
            IFNULL(c_teachers.phone_mobile, '') phone_mobile,
            IFNULL(l1.id, '') user_id,
            IFNULL(l1.user_name, '') user_name,
            IFNULL(l1.user_hash, '') user_hash,
            IFNULL(l2.id, '') assigned_to_user_id,
            IFNULL(l2.full_user_name, '') assigned_to_user,
            IFNULL(l3.id, '') center_id,
            IFNULL(l3.name, '') center_name
            FROM c_teachers
            INNER JOIN
            users l1 ON c_teachers.user_id = l1.id
            AND l1.deleted = 0
            INNER JOIN
            users l2 ON c_teachers.assigned_user_id = l2.id
            AND l2.deleted = 0
            LEFT JOIN
            teams l3 ON c_teachers.team_id = l3.id
            AND l3.deleted = 0
            WHERE (((l1.user_name = '$user_name')
            AND (IFNULL(l1.status, '') = 'Active')))
            AND c_teachers.deleted = 0";

            $teachers = $GLOBALS['db']->fetchArray($q1);
            if(count($teachers) == 0)
                return array(
                    'success' => false,
                    'error' => "invalid_username",
                );

            $teacher = array();
            //Check Password credential
            $pwdCheck = false;

            foreach($teachers as $teacher){
                if ( (User::checkPassword($password, $teacher['user_hash']) || md5($password) == $teacher['user_hash']) && !$pwdCheck)
                    $pwdCheck = true;
                //add student array
                $teacher['birthdate'] = $timedate->to_display_date($teacher['birthdate'],false);//format birthdate
                $teacher = $teacher;

                $teacher['class_list']     = array();
                $teacher['unread_noti']    = 0;
                $teacher['notifications']  = array();
            }

            if(!$pwdCheck)
                return array(
                    'success' => false,
                    'error' => "invalid_password",
                );

            $str_teacher_id = implode("','",array_column($teachers, 'primaryid'));
            $str_user_id = implode("','",array_column($teachers, 'user_id'));

            //Get Current Class
            $nowDb = $timedate->nowDb();
            $q2 = "SELECT DISTINCT
            IFNULL(l2.id, '') class_id,
            IFNULL(l2.picture, '') picture,
            IFNULL(l2.name, '') class_name,
            IFNULL(l2.class_code, '') class_code,
            IFNULL(l2.main_schedule, '') main_schedule,
            l2.start_date start_date,
            l2.end_date end_date,
            l2.date_entered date_entered,
            IFNULL(l2.kind_of_course, '') kind_of_course,
            IFNULL(l2.level, '') level,
            IFNULL(l2.max_size, '') max_size,
            IFNULL(l2.hours, 0) hours,
            IFNULL(l2.status, '') status,
            IFNULL(l3.id, '') koc_id,
            IFNULL(l3.name, '') koc_name,
            IFNULL(l4.id, '') center_id,
            IFNULL(l4.name, '') center_name,
            IFNULL(l4.code_prefix, '') center_code,
            IFNULL(l8.name, '') room_name,
            IFNULL(l9.full_teacher_name, '') teacher_name,
            IFNULL(meetings.id, '') ss_id,
            IFNULL(meetings.name, '') ss_name,
            IFNULL(meetings.type, '') ss_learning_type,
            IFNULL(meetings.displayed_url, '') ss_displayed_url,
            IFNULL(meetings.lesson_number, '') ss_lesson,
            IFNULL(meetings.week_no, '') ss_week_no,
            meetings.till_hour ss_till_hour,
            meetings.date_start ss_date_start,
            meetings.date_end ss_date_end,
            IFNULL(l1.id, '') ss_teacher_id,
            IFNULL(l1.full_teacher_name, '') ss_teacher_name,
            IFNULL(l5.id, '') ss_ta_id,
            IFNULL(l5.full_teacher_name, '') ss_ta_name,
            IFNULL(l6.id, '') ss_room_id,
            IFNULL(l6.name, '') ss_room_name,
            IFNULL(meetings.observe_note, '') ss_observe_note,
            IFNULL(meetings.syllabus_custom, '') ss_syllabus_custom,
            IFNULL(meetings.topic_custom, '') ss_topic_custom,
            IFNULL(meetings.objective_custom, '') ss_note_for_teacher_custom,
            IFNULL(meetings.syllabus_id, '') ss_syllabus_id,
            IFNULL(l10.name, '') ss_syllabus_topic,
            IFNULL(l10.description, '') ss_syllabus_content,
            IFNULL(l10.homework, '') ss_syllabus_homework,
            IFNULL(l10.note_for_teacher, '') ss_syllabus_note_for_teacher,
            meetings.duration_cal ss_duration_cal,
            meetings.observe_score ss_observe_score,
            IFNULL(l7.id,'') assigned_user_id,
            IFNULL(l7.full_user_name,'') assigned_to,
            IFNULL(meetings.total_attended,0) total_attended,
            IFNULL(meetings.total_absent,0) total_absent,
            IFNULL(meetings.total_student,0) total_student
            FROM meetings
            INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN c_teachers l1 ON  l1.id IN (meetings.teacher_id, meetings.teacher_cover_id, meetings.sub_teacher_id)  AND l1.deleted = 0
            INNER JOIN j_kindofcourse l3 ON l2.koc_id = l3.id AND l3.deleted = 0
            INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
            LEFT JOIN c_teachers l5 ON meetings.teacher_cover_id = l5.id AND l5.deleted = 0
            LEFT JOIN c_rooms l6 ON meetings.room_id = l6.id AND l6.deleted = 0
            LEFT JOIN users l7 ON l2.assigned_user_id=l7.id AND l7.deleted=0
            LEFT JOIN c_rooms l8 ON l2.room_id=l8.id AND l8.deleted=0
            LEFT JOIN c_teachers l9 ON l2.teacher_id = l9.id AND l9.deleted = 0
            LEFT JOIN j_syllabus l10 ON meetings.syllabus_id = l10.id AND l10.deleted = 0
            WHERE (((l1.id = '$str_teacher_id' )
            AND (meetings.session_status <> 'Cancelled')
            AND (meetings.meeting_type = 'Session')))
            AND meetings.deleted = 0
            GROUP BY meetings.id
            ORDER BY status DESC, meetings.date_start ASC, class_id ASC";
            $rowS = $GLOBALS['db']->fetchArray($q2);
            $runClass   = '####';
            $classes = array();

            //Get Kind Of Course Syllabus - For best perfomance
            $classIds = implode("','",array_unique(array_column($rowS,'class_id')));
            $koc = array();
            $q2_1 = "SELECT DISTINCT
            IFNULL(l1.id, '') koc_id,
            IFNULL(l1.name, '') koc_name,
            IFNULL(l1.kind_of_course, '') kind_of_course,
            IFNULL(l1.content, '') content,
            IFNULL(l1.lms_content, '') lms_content
            FROM j_class INNER JOIN j_kindofcourse l1 ON j_class.koc_id = l1.id  AND l1.deleted = 0
            WHERE (j_class.id IN ('$classIds')) AND j_class.deleted = 0";
            $rowK = $GLOBALS['db']->fetchArray($q2_1);
            foreach($rowK as $key => $rK){
                $koc[$rK['koc_id']]['koc_id'] = $rK['koc_id'];
                $koc[$rK['koc_id']]['koc_name'] = $rK['koc_name'];
                $koc[$rK['koc_id']]['kind_of_course'] = $rK['kind_of_course'];
                $koc[$rK['koc_id']]['content'] = ((!empty($rK['content'])) ? json_decode(html_entity_decode($rK['content']),true) : '');
            }

            //List Class ID
            $classIds = array();

            foreach($rowS as $_key => $r2){
                if($runClass != $r2['class_id']){
                    $runClass   = $r2['class_id'];
                    $classes[$runClass]['id'] = $r2['class_id'];
                    $classes[$runClass]['picture'] = $r2['picture'];
                    $classes[$runClass]['class_name']  = $r2['class_name'];
                    $classes[$runClass]['class_code']  = $r2['class_code'];

                    $schedule = json_decode(html_entity_decode($r2['main_schedule']),true);
                    $sche = array();
                    foreach ($schedule as $key => $value) {
                        $detail = array();
                        foreach ($value as $k => $val) {
                            $detail[$k]['day'] = $key;
                            $detail[$k]['start_time'] = $val['start_time'];
                            $detail[$k]['end_time'] = $val['end_time'];
                            $detail[$k]['duration_hour'] = $val['duration_hour'];
                            $detail[$k]['revenue_hour'] = $val['revenue_hour'];
                            $detail[$k]['teaching_hour'] = $val['teaching_hour'];
                        }
                        $sche = $detail;
                    }
                    $classes[$runClass]['main_schedule'] =  $sche;

                    $classes[$runClass]['start_date']  = $timedate->to_display_date($r2['start_date'],false);
                    $classes[$runClass]['end_date']    = $timedate->to_display_date($r2['end_date'],false);
                    $classes[$runClass]['date_entered']    = date('Y-m-d H:i:s',strtotime("+7 hours ".$r2['date_entered']));
                    $classes[$runClass]['kind_of_course']  = $r2['kind_of_course'];
                    $classes[$runClass]['level']           = $r2['level'];
                    $classes[$runClass]['hours']           = $r2['hours'];
                    $classes[$runClass]['assigned_to']     = $r2['assigned_to'];
                    $classes[$runClass]['koc_id']          = $r2['koc_id'];
                    $classes[$runClass]['koc_name']        = $r2['koc_name'];
                    $classes[$runClass]['room_name']       = $r2['room_name'];
                    $classes[$runClass]['teacher_name']    = $r2['teacher_name'];
                    $classes[$runClass]['max_size']        = $r2['max_size'];
                    $classes[$runClass]['team_name']       = $r2['center_name'];
                    $classes[$runClass]['team_code']       = $r2['center_code'];
                    $classes[$runClass]['status']          = $r2['status'];


                    $content = $koc[$r2['koc_id']]['content'];
                    foreach ($content as $ct){
                        if ($ct['levels'] == $r2['level'])
                            $docs = $ct['doc_url'];
                    }
                    $classes[$runClass]['doc_url'] = $docs;

                    $classes[$runClass]['sessions'] = array();
                    $classes[$runClass]['albums'] = array();
                    $classes[$runClass]['feedback'] = array();
                    $classes[$runClass]['news'] = array();

                }
                $session = array();
                $session['ss_id']         = $r2['ss_id'];
                $session['ss_name']       = $r2['ss_name'];
                $session['ss_lesson']     = $r2['ss_lesson'];
                $session['ss_week_no']    = $r2['ss_week_no'];
                $session['ss_week_date']  = date('l',strtotime("+7 hours ".$r2['ss_date_start']));
                $session['ss_till_hour']  = $r2['ss_till_hour'];
                $session['ss_date_start'] = $timedate->to_display_date_time($r2['ss_date_start']);
                $session['ss_date_end']   = $timedate->to_display_date_time($r2['ss_date_end']);
                $session['ss_db_date_start']= date('Y-m-d H:i:s',strtotime("+7 hours ".$r2['ss_date_start']));
                $session['ss_db_date_end']  = date('Y-m-d H:i:s',strtotime("+7 hours ".$r2['ss_date_end']));
                $session['ss_db_date']      = date('Y-m-d',strtotime("+7 hours ".$r2['ss_date_start']));

                if($now < $r2['ss_date_start']) $session['ss_status'] = "not_stated";
                elseif ($now >= $r2['ss_date_start'] && $now <= $r2['ss_date_end']) $session['ss_status'] = "in_progress";
                elseif ($now > $r2['ss_date_end']) $session['ss_status'] = "finished";

                $session['ss_duration_cal'] = $r2['ss_duration_cal'];
                $session['ss_ta_name']      = $r2['ss_ta_name'];
                $session['ss_teacher_name'] = $r2['ss_teacher_name'];
                $session['ss_room_name']    = $r2['ss_room_name'];
                $session['ss_observe_score']= $r2['ss_observe_score'];
                $session['ss_lesson_note']  = $r2['ss_lesson_note'];
                $session['ss_syllabus_custom']  = $r2['ss_syllabus_custom'];
                $session['ss_topic_custom']  = $r2['ss_topic_custom'];
                $session['ss_note_for_teacher_custom'] = $r2['ss_note_for_teacher_custom'];
                $session['ss_learning_type']  = $r2['ss_learning_type'];
                $session['ss_displayed_url']  = $r2['ss_displayed_url'];
                $session['total_student']  = $r2['total_student'];
                $session['total_attended']  = $r2['total_attended'];
                $session['total_absent']  = $r2['total_absent'];
                $session['syllabus_topic'] = $r2['ss_syllabus_topic'];
                $session['syllabus_activities'] = $r2['ss_syllabus_content'];
                $session['syllabus_homework'] = $r2['ss_syllabus_homework'];
                $session['syllabus_note_for_teacher'] = $r2['ss_syllabus_note_for_teacher'];

                $classes[$runClass]['sessions'][] = $session;

                if(!in_array($runClass, $classIds))
                    $classIds[] = $runClass;
            }

            // Get total session of class
            $qSession = "SELECT DISTINCT
            IFNULL(c.id, '') class_id,
            COUNT(l1.id) total_session
            FROM j_class c
            INNER JOIN meetings l1 ON l1.ju_class_id = c.id AND l1.deleted = 0
            WHERE (c.id IN ('".implode("','",$classIds)."'))
            AND (IFNULL(l1.meeting_type, '') = 'Session')
            AND (IFNULL(l1.session_status, '') <> 'Cancelled' OR (IFNULL(l1.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
            GROUP BY c.id";
            $rSession = $GLOBALS['db']->query($qSession);
            while($row = $GLOBALS['db']->fetchbyAssoc($rSession)){
                $classes[$row['class_id']]['total_session']   = $row['total_session'];
            }

            //Add Gallery Image
            $gls = $this->getListGallery($classIds);
            //Gallery By Class
            foreach ($classes as $class_id => $class) {
                if (array_key_exists($class_id, $gls))
                    $classes[$class_id]['albums'] = array_values($gls[$class_id]);
            }

            //Get List Student
            $getListStudent = "SELECT p.* FROM (
            SELECT DISTINCT
            IFNULL(c.id, '') student_id,
            IFNULL(c.first_name, '') first_name,
            IFNULL(c.full_student_name, '') student_name,
            IFNULL(c.picture, '') picture,
            IFNULL(c.contact_id, '') portal_id,
            IFNULL(c.phone_mobile, '') student_phone,
            'Contacts' student_type,
            c.birthdate birthdate,
            IFNULL(c.nick_name, '') nick_name,
            c.guardian_name parent_name,
            c.relationship relationship,
            IFNULL(c.phone_guardian, '') parent_phone,
            c.guardian_name_2 parent_name_2,
            c.relationship2 relationship_2,
            IFNULL(c.other_mobile, '') parent_phone_2,
            '' type,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.status, '') class_status
            FROM j_classstudents
            INNER JOIN j_class l1 ON j_classstudents.class_id = l1.id AND l1.deleted = 0
            INNER JOIN contacts c ON j_classstudents.student_id = c.id AND c.deleted = 0 AND j_classstudents.student_type = 'Contacts'
            WHERE (l1.id IN('".implode("','",$classIds)."') ) AND j_classstudents.deleted = 0
            UNION ALL
            SELECT DISTINCT
            IFNULL(l.id, '') student_id,
            IFNULL(l.first_name, '') first_name,
            IFNULL(l.full_lead_name, '') student_name,
            IFNULL(l.picture, '') picture,
            IFNULL(l.contact_id, '') portal_id,
            IFNULL(l.phone_mobile, '') student_phone,
            'Leads' student_type,
            l.birthdate birthdate,
            IFNULL(l.nick_name, '') nick_name,
            l.guardian_name parent_name,
            l.relationship relationship,
            IFNULL(l.phone_guardian, '') parent_phone,
            l.guardian_name_2 parent_name_2,
            l.relationship2 relationship_2,
            IFNULL(l.other_mobile, '') parent_phone_2,
            '' type,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.status, '') class_status
            FROM j_classstudents
            INNER JOIN j_class l1 ON j_classstudents.class_id = l1.id AND l1.deleted = 0
            INNER JOIN leads l ON j_classstudents.student_id = l.id AND l.deleted = 0 AND j_classstudents.student_type = 'Leads'
            WHERE (l1.id IN('".implode("','",$classIds)."') ) AND j_classstudents.deleted = 0) p ORDER BY p.first_name ASC";

            $rGetListStudent = $GLOBALS['db']->query($getListStudent);

            while($r = $GLOBALS['db']->fetchbyAssoc($rGetListStudent)) {
                $student = array();
                $student['student_id']      = $r['student_id'];
                $student['student_name']    = $r['student_name'];
                $student['picture']         = $r['picture'];
                $student['student_phone']   = $r['student_phone'];
                $student['student_type']    = $r['student_type'];
                $student['birthdate']       = $timedate->to_display_date($r['birthdate'],false);;
                $student['nick_name']       = $r['nick_name'];
                $student['parent_name']     = $r['parent_name'];
                $student['status_in_class'] = $r['class_status'];
                $student['type_in_class']   = $r['type'];
                $classes[$r['class_id']]['student'][] = $student;
            }

            //Get Feedback
            $q4 = "SELECT DISTINCT
            IFNULL(cases.id, '') primaryid,
            IFNULL(cases.name, '') subject,
            IFNULL(cases.status, '') status,
            IFNULL(cases.case_number, '') case_number,
            IFNULL(cases.type, '') type,
            IFNULL(cases.relate_feedback_list, '') sub_type,
            IFNULL(cases.priority, '') priority,
            cases.date_entered date_entered,
            cases.received_date received_date,
            IFNULL(cases.description, '') feedback_content,
            cases.resolved_date resolved_date,
            IFNULL(cases.resolution, '') response_to_customer,
            IFNULL(l2.id, '') student_id,
            IFNULL(l2.full_student_name, '') student_name,
            IFNULL(l2.contact_id, '') student_code,
            IFNULL(l2.phone_mobile, '') mobile,
            IFNULL(l2.guardian_name, '') parent,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.name, '') class_name,
            IFNULL(l4.full_user_name, '') assigned_to,
            IFNULL(l3.name,'') center,
            IFNULL(l1.id,'') center_id
            FROM cases
            INNER JOIN j_class_cases_1_c l1_1 ON cases.id = l1_1.j_class_cases_1cases_idb AND l1_1.deleted = 0
            INNER JOIN j_class l1 ON l1.id = l1_1.j_class_cases_1j_class_ida AND l1.deleted = 0
            INNER JOIN contacts_cases_1_c l2_1 ON cases.id = l2_1.contacts_cases_1cases_idb AND l2_1.deleted = 0
            INNER JOIN contacts l2 ON l2.id = l2_1.contacts_cases_1contacts_ida AND l2.deleted = 0
            INNER JOIN teams l3 ON cases.team_id=l3.id AND l3.deleted=0
            INNER JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
            LEFT JOIN users l5 ON cases.cso_id = l5.id AND l5.deleted = 0 AND (l5.id = '$str_user_id')
            WHERE (l1.id IN ('".implode("','",$classIds)."')) AND cases.deleted = 0
            ORDER BY class_id ASC,  date_entered DESC";
            $rs4 = $GLOBALS['db']->query($q4);

            while($r4 = $GLOBALS['db']->fetchbyAssoc($rs4)){
                $feedback = array();
                $feedback['number']           = $r4['case_number'];
                $feedback['feedback_id']      = $r4['primaryid'];
                $feedback['subject']          = $r4['subject'];
                $feedback['class_id']         = $r4['class_id'];
                $feedback['class_name']       = $r4['class_name'];
                $feedback['type']             = $r4['type'];
                $feedback['sub_type']         = $r4['sub_type'];
                $feedback['status']           = $r4['status'];
                $feedback['received_date']    = $timedate->to_display_date($r4['received_date'],false);
                $feedback['date_entered']     = $timedate->to_display_date_time($r4['date_entered']);
                $feedback['resolved_date']    = $timedate->to_display_date($r4['resolved_date'],false);
                $feedback['feedback_content'] = $r4['feedback_content'];
                $feedback['response_to_customer'] = $r4['response_to_customer'];
                $feedback['assigned_to']      = $r4['assigned_to'];
                $feedback['center_id']        = $r4['center_id'];
                $feedback['center']           = $r4['center'];
                $feedback['last_comment']           = $r4['last_comment'];
                $feedback['last_comment_date']      = date('Y-m-d H:i:s',strtotime("+7 hours ".$r4['last_comment_date']));
                $feedback['last_comment_depositor'] = $r4['last_comment_depositor'];
                $feedback['count_unread_comment']   = $r4['count_unread_comment'];
                $classes[$r4['class_id']]['feedback'][] = $feedback;
            }

            $teacher['class_list'] = array_values($classes);

            //Get New
            $nowDate = date('Y-m-d');
            $q5 = "SELECT DISTINCT
            IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin_teacher, 0) pin,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            WHERE (c_news.end_date >= '$nowDate')
            AND (c_news.type LIKE '%^Teacher^%')
            AND c_news.deleted = 0
            ORDER BY date_entered DESC";
            $rs5 = $GLOBALS['db']->query($q5);
            $news = array();
            while($r = $GLOBALS['db']->fetchbyAssoc($rs5)){
                $news[$r['primaryid']] = array(
                    'id'        => $r['primaryid'],
                    'name'      => $r['name'],
                    'thumbnail' => $r['thumbnail'],
                    'url'       => $r['url'],
                    'type'      => $r['type'],
                    'pin'       => $r['pin'],
                    'description'   => $r['description'],
                    'type_news'   => $r['type_news'],
                    'date_entered'  => $timedate->to_display_date_time($r['date_entered']),
                    'end_date'      => $timedate->to_display_date($r['end_date'],false),
                    'start_date'    => $timedate->to_display_date($r['start_date'],false),
                );
            }

            //Get Notification
            $q7 = "SELECT nttt.* FROM (SELECT DISTINCT
            IFNULL(nt.id, '') primaryid,
            CASE nt.assigned_user_id WHEN 'all' THEN IFNULL(l1.id, '') ELSE (IFNULL(l1.id, '')) END assigned_user_id,
            IFNULL(nt.name, '') title,
            IFNULL(nt.description, '') body,
            IFNULL(nt.is_read, 0) is_read,
            IFNULL(nt.parent_id, '') record_id,
            IFNULL(nt.parent_type, '') module_name,
            IFNULL(nt.created_by, '') created_by,
            IFNULL(nt.student_read, '') student_read,
            nt.date_entered date_entered
            FROM notifications nt
            INNER JOIN users l1 ON (nt.assigned_user_id = l1.id OR nt.assigned_user_id = 'all') AND l1.deleted = 0 AND (nt.assigned_user_id = '$str_user_id' OR nt.assigned_user_id = 'all')
            WHERE nt.deleted = 0 AND (nt.apps_type = 'Teacher')) nttt WHERE nttt.assigned_user_id = '$str_user_id'
            ORDER BY nttt.assigned_user_id ASC, nttt.date_entered DESC";
            $rs = $GLOBALS['db']->query($q7);
            $key = 0;
            while($r = $GLOBALS['db']->fetchbyAssoc($rs)){

                $nt                 = array();
                $nt['primaryid']    = $r['primaryid'];
                $nt['title']        = $r['title'];
                $nt['body']         = $r['body'];
                //get notification All
                if(!empty($r['student_read']) && $r['student_read'] != '[]' ){
                    $student_read = json_decode(html_entity_decode($r['student_read']),true);
                    if(in_array( $r['assigned_user_id'], $student_read))
                        $r['is_read'] = '1';
                }
                $nt['is_read']      = ($r['is_read'] == '1') ? true : false;
                if($r['is_read'] != '1') $teacher['unread_noti'] += 1; //Add Number of Notification

                $nt['module_name']  = $r['module_name'];
                $nt['record_id']    = $r['record_id'];
                $nt['created_by']   = $r['created_by'];
                $nt['date_entered'] = date('Y-m-d H:i:s',strtotime("+7 hours ".$r['date_entered']));

                $teacher['notifications'][$key]  = $nt;

                $key++;
            }

            $language_options = array('case_status_dom','case_sub_type_dom','status_class_list', 'attendance_type_list', 'do_homework_list', 'ss_status_list', 'situation_type_list', 'gallery_type_list'); //mảng chứa các options trong app_list_strings


            //Set App_String
            $app_strings = array(
                'en' => parseAppListString('en_us',$language_options),
                'vn' => parseAppListString('vn_vn',$language_options),
            );

            //list tag
            $tag_list = array();
            $qGetTags = "SELECT IFNULL(id, '') primaryid
            , IFNULL(name, '') tag_name
            , IFNULL(name_lower, '') name_lower
            FROM tags
            WHERE deleted=0";
            $resGetTags = $GLOBALS['db']->query($qGetTags);
            while ($row = $GLOBALS['db']->fetchByAssoc($resGetTags)){
                $tag_list[$row['primaryid']]['primaryid'] = $row['primaryid'];
                $tag_list[$row['primaryid']]['tag_name'] = $row['tag_name'];
                $tag_list[$row['primaryid']]['name_lower'] = $row['name_lower'];
            }
            // Get list center
            $list_center = $GLOBALS['db']->fetchArray("
                SELECT id,
                name,
                IFNULL(legal_name,'' ) legal_name,
                IFNULL(short_name,'') short_name,
                IFNULL(code_prefix,'') code_prefix ,
                IFNULL(phone_number,'') phone ,
                IFNULL(description,'') description
                FROM teams
            WHERE deleted =0 and private = 0");
            //get Config
            $admin = new Administration();
            $admin->retrieveSettings();
            $config = array(
                'doc_folder' => empty($admin->settings['default_doc_folder'])?'':$admin->settings['default_doc_folder'],
                'google_folder' => empty($admin->settings['default_google_folder'])?'':$admin->settings['default_google_folder'],
                'hot_line' => empty($admin->settings['default_hotline'])?'':substr($admin->settings['default_hotline'], 0, strlen($admin->settings['default_hotline']) - 1),
            );
            //Return
            return array(
                'success'       =>  true,
                'portal_name'   =>  $user_name,
                'password'      =>  $password,
                'teacher'       =>  $teacher,
                'news_list'     =>  array_values($news),
                'tag_list'      =>  array_values($tag_list),
                'center_list'   =>  $list_center,
                'app_strings_en'=>  $app_strings['en'],
                'app_strings_vn'=>  $app_strings['vn'],
                'config'        =>  $config
            );


        }else
            return array(
                'success' => false,
                'error' =>  "sending_failed."
            );

    }

    function editTeacher(ServiceBase $api, $args)
    {
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');

        $id = $args['id'];
        $bean = BeanFactory::getBean('C_Teachers', $id);
        $name = create_guid();

        if (!is_dir('upload/origin')) {
            mkdir('upload/origin');
        }
        if (!is_dir('upload/resize')) {
            mkdir('upload/resize');
        }
        $file1 = 'upload/origin/' . $name;
        $file2 = 'upload/resize/' . $name;

        move_uploaded_file($_FILES['picture']['tmp_name'], $file1);

        file_put_contents($file2, file_get_contents($file1));

        $imgOri1 = new ImageHelper($file1);
        $imgOri1->resize(500);
        $imgOri1->save($file1);

        $imgOri2 = new ImageHelper($file2);
        $imgOri2->resize(220, 220);
        $imgOri2->save($file2);

        if ($args['type'] == 'avatar') {
            $q1 = "UPDATE c_teachers
            SET c_teachers.picture = '$name' WHERE c_teachers.id = '$id'";
            $GLOBALS['db']->query($q1);
        } else {
            $q1 = "UPDATE c_teachers
            SET c_teachers.cover_app = '$name' WHERE c_teachers.id = '$id'";
            $GLOBALS['db']->query($q1);
        }

        return array(
            'success' => true,
            'bean_id' => $bean->id,
        );
    }

    function getTeachingHour(ServiceBase $api, $args){
        global $timedate;
        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $start = $timedate->convertToDBDate($args['begin']);
        $end   = $timedate->convertToDBDate($args['end']);


        if(empty($start) || empty($end) || empty($args['teacher_id'])){
            return array(
                'success' => false,
                'error' => 'empty_param',
            );
        }else{
            $q1 = "SELECT DISTINCT
            IFNULL(c_teachers.id, '') teacher_id,
            SUM(IFNULL(l1.teaching_hour, 0)) total_hous,
            SUM(CASE WHEN l1.date_start <= '{$timedate->nowDb()}' THEN l1.teaching_hour ELSE 0 END) teaching_hours,
            COUNT(DISTINCT l1.id) total_session,
            SUM(CASE WHEN l1.date_start <= '{$timedate->nowDb()}' THEN 1 ELSE 0 END) teaching_session,
            SUM(IFNULL(l1.total_attended,0)) count_attended,
            SUM(IFNULL(l1.total_absent,0)) count_absent,
            SUM(IFNULL(l1.total_student,0)) total_student,
            SUM(IFNULL(l1.total_attended,0) * IFNULL(l1.teaching_hour, 0)) learning_hours
            FROM c_teachers
            INNER JOIN meetings l1 ON c_teachers.id = l1.teacher_id AND l1.deleted = 0
            INNER JOIN j_class l2 ON l1.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN teams l3 ON l2.team_id = l3.id AND l3.deleted = 0
            WHERE (l1.date >= '$start' AND l1.date <= '$end')
            AND (l1.session_status <> 'Cancelled') AND c_teachers.deleted = 0 AND c_teachers.id = '{$args['teacher_id']}'
            GROUP BY teacher_id";
            $row = $GLOBALS['db']->fetchOne($q1);
            $teacher = array();
            $teacher['total_hous'] = $row['total_hous'];
            $teacher['teaching_hours'] = $row['teaching_hours'];
            $teacher['teaching_late_time'] = 0;
            $teacher['total_session'] = $row['total_session'];
            $teacher['teaching_session'] = $row['teaching_session'];
            $teacher['teaching_hours_by_class'] = 0;
            return array(
                'success' => true,
                'data' => $teacher,
            );
        }
    }

    //API mới viết lại
    function getTeachingHour2(ServiceBase $api, $args){
        global $timedate;
        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $start = $timedate->convertToDBDate($args['begin']);
        $end   = $timedate->convertToDBDate($args['end']);

        if(empty($start) || empty($end) || empty($args['teacher_id'])){
            return array(
                'success' => false,
                'error' => 'empty_param',
            );
        }else{
            $q1 = "SELECT DISTINCT
            IFNULL(c_teachers.id, '') teacher_id,
            IFNULL(c_teachers.full_teacher_name, '') teacher_name,
            IFNULL(l3.id, '') center_id,
            IFNULL(l3.name, '') center_name,
            IFNULL(l2.id, '') class_id,
            IFNULL(l2.name, '') class_name,
            IFNULL(l1.id, '') meeting_id,
            DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') ss_date_start,
            IFNULL(l1.teaching_hour, 0) teaching_hour,
            IFNULL(l1.total_attended,0) count_attended,
            IFNULL(l1.total_absent,0) count_absent,
            IFNULL(l1.total_student,0) total_student,
            (IFNULL(l1.total_attended,0) * IFNULL(l1.teaching_hour, 0)) learning_hours
            FROM c_teachers
            INNER JOIN meetings l1 ON c_teachers.id = l1.teacher_id AND l1.deleted = 0
            INNER JOIN j_class l2 ON l1.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN teams l3 ON l2.team_id = l3.id AND l3.deleted = 0
            WHERE (l1.date >= '$start' AND l1.date <= '$end')
            AND (l1.session_status <> 'Cancelled') AND c_teachers.deleted = 0 AND c_teachers.id = '{$args['teacher_id']}'
            GROUP BY teacher_id, teacher_name ASC, center_id, center_name ASC, class_id ASC, class_name, ss_date_start ASC, meeting_id";

            return array(
                'success' => true,
                'data' => $GLOBALS['db']->fetchArray($q1),
            );
        }
    }
    //List unread notification divide page - Hoang Huy
    function listNotifications(ServiceBase $api, $args){
        //Get the page from the mobile app to set LIMIT
        $limit = (!empty($args['page'])) ? 30*$args['page'] : 30;
        $limit_bot = $limit - 30;
        $limit_top = $limit;
        $teacher_id = $args['teacher_id'];

        if($limit == 30){//First Pages
            $time = date('Y-m-d H:i:s', strtotime("+7 hours " . $GLOBALS['timedate']->nowDb()));
            $GLOBALS['db']->query("UPDATE c_teachers SET lasted_view_noti = '{$time}' WHERE id = '{$teacher_id}' AND deleted = 0");
        }

        $q1 = "SELECT IFNULL(team_id, '') team_id
        FROM c_teachers ct
        WHERE ct.id = '{$teacher_id}' AND ct.deleted = 0";
        $team_id = $GLOBALS['db']->getOne($q1);

        $q2 = "SELECT nttt.* FROM ((SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        WHERE nt.deleted = 0
        AND nt.assigned_user_id = '{$teacher_id}'
        ORDER BY nt.date_entered
        LIMIT $limit_bot, $limit_top)
        UNION
        (SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        INNER JOIN team_sets_teams ts
        ON nt.team_set_id = ts.team_set_id
        WHERE (ts.team_id = ('{$team_id}') OR nt.team_set_id = 1) AND nt.parent_type = 'C_News' AND nt.assigned_user_id = 'all' AND nt.apps_type LIKE '%Teacher%'
        LIMIT $limit_bot, $limit_top)) nttt
        ORDER BY date_entered DESC
        LIMIT $limit_bot, $limit_top";

        $q4 = "SELECT DISTINCT
        IFNULL(c_news.id, '') news_id,
        IFNULL(l1.id, '') student_id,
        CASE WHEN IFNULL(l1.id, '') = '' THEN '0' ELSE 1 END is_read
        FROM c_news
        LEFT JOIN c_news_c_teachers_1_c l1_1 ON c_news.id = l1_1.c_news_c_teachers_1c_news_ida AND l1_1.deleted = 0
        LEFT JOIN c_teachers l1 ON l1.id = l1_1.c_news_c_teachers_1c_teachers_idb AND l1.deleted = 0
        WHERE l1.id = '{$teacher_id}' AND c_news.deleted = 0
        AND (c_news.type LIKE '%Teacher%')";
        $row1 = $GLOBALS['db']->query($q2);
        $row2 = $GLOBALS['db']->fetchArray($q4);


        //List of news that the student read
        //The target of make this read news list is to make sense that which is the notification about news that the student read
        $read_news = array();
        foreach($row2 as $_key => $row) $read_news[$row['news_id']] = $row['is_read'];
        //Loop through all notifications and push it into $notification_list

        while ($r = $GLOBALS['db']->fetchByAssoc($row1)) {
            //get notification All
            $nt                 = array();
            $nt['primaryid']    = $r['primaryid'];
            $nt['title']        = $r['title'];
            $nt['body']         = $r['body'];
            $nt['is_read']      = ($r['parent_type'] == "C_News") ? boolVal($read_news[$r['parent_id']]) : boolVal($r['is_read']);
            $nt['module_name']  = $r['module_name'];
            $nt['record_id']    = $r['record_id'];
            $nt['created_by']   = $r['created_by'];
            $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));
            $notification_list[] = $nt;
        }
        return array(
            'success' => 1,
            'notification_list' => $notification_list,
        );
    }

    //Count Unread Notification - Hoang Huy
    function countUnreadNoti(ServiceBase $api, $args){
        $unread_noti_list = array();
        $teacher_id = $args['teacher_id'];
        $lated_date_view_noti = $GLOBALS['db']->getOne("SELECT c_teachers.lasted_view_noti FROM c_teachers WHERE id = '{$teacher_id}' AND deleted = 0");
        if(empty($lated_date_view_noti)) $lated_date_view_noti = '1970-01-01';
        $q7 = "SELECT COUNT(id) FROM notifications WHERE assigned_user_id = '{$teacher_id}' AND date_entered >= '{$lated_date_view_noti}' AND is_read = 0 AND deleted = 0";
        $unread_noti_list[$teacher_id] = intVal($GLOBALS['db']->getOne($q7));
        return array(
            'success' => 1,
            'unread_noti_list' => $unread_noti_list,
        );
    }

    //Update Notification Read/Unread/Delete  - Hoang Huy
    function updateNotification(ServiceBase $api, array $args){
        //Get noti beans
        $noti       = BeanFactory::getBean('Notifications', $args['noti_id']);
        $cnew_id    = $noti->parent_type == 'C_News' ? $noti->parent_id : "";
        $teacher_id = $args['teacher_id'];
        if($noti->parent_type == "C_News" && !empty($cnew_id))
        $new = BeanFactory::getBean('C_News', $cnew_id);
        switch ($args['action_type']) {
            case 'read':
                if ($noti->parent_type == "C_News" && !empty($cnew_id)){
                    if(!empty($new->id) && $new->load_relationship('c_news_c_teachers_1')){
                        //If the student haven't read the news that is the parent_id of notifications we will add the new relation
                        //On the other side we will count the view of new by 1
                        $new->c_news_c_teachers_1->add($teacher_id);
                        $GLOBALS['db']->query("UPDATE c_news SET addon_views = addon_views + 1 WHERE deleted = 0 AND id = '{$cnew_id}'");
                    }
                }elseif(!$noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'unread':
                if($noti->parent_type == "C_News" && !empty($cnew_id)){
                    //Incase unread we will need to delete the relationship existing
                    if(!empty($new->id) && $new->load_relationship('c_news_c_teachers_1'))
                        $new->c_news_c_teachers_1->delete($cnew_id, $teacher_id);
                }elseif($noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 0 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'delete':
                //the student only can delete the notification that relate to them and cant delete the notification about news
                if($noti->parent_type != 'C_News') $noti->mark_deleted($noti->id);
                break;
            case 'markallread':
                //Mark All Read Notification
                $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE assigned_user_id = '$teacher_id' AND is_read = 0 AND deleted=0");
                //Read all news
                //Step1: get all the news that the students have read
                $q1    = "SELECT IFNULL(c_news_c_teachers_1c_news_ida, '') news_id FROM c_news_c_teachers_1_c crl WHERE crl.deleted = 0 AND crl.c_news_c_teachers_1c_teachers_idb = '{$teacher_id}'";
                $row1  = $GLOBALS['db']->fetchArray($q1);

                //Step2: convert $read_news array to string to query get all the news that doesn't exist in our read list
                $str_read_news = (!empty($row1)) ? implode("','", array_column($row1, 'news_id')) : "";
                $q2            = "SELECT IFNULL(id, '') news_id FROM c_news WHERE deleted = 0 AND id NOT IN ('$str_read_news') AND type LIKE '%Student%'";
                $row2          = $GLOBALS['db']->fetchArray($q2);
                $date_modified = $GLOBALS['timedate']->nowDb(); //Get time at now
                foreach($row2 as $r2){
                    //Count the relationship that was deleted
                    $countRelationDeleted = $GLOBALS['db']->getOne("SELECT COUNT(id)
                        FROM c_news_c_teachers_1_c
                        WHERE c_news_c_teachers_1c_news_ida = '{$r2['news_id']}'
                        AND c_news_c_teachers_1c_teachers_idb = '{$teacher_id}' AND deleted = 1");
                    if ($countRelationDeleted > 0) {
                        //If exist the deleted realtionship we will update deleted field to 1 instead of create new record
                        $qUpdateRelation = "UPDATE c_news_c_teachers_1_c
                        SET deleted = 0, date_modified ='{$date_modified}'
                        WHERE deleted = 1
                        AND c_news_c_teachers_1c_news_ida = '{$r2['news_id']}'
                        AND c_news_c_teachers_1c_teachers_idb = '{$teacher_id}'";
                        $GLOBALS['db']->query($qUpdateRelation);
                    } else {
                        //If doesn't exist the deleted relationship we will add new relationship
                        $relation_id = create_guid();
                        $qInsertRelation = "INSERT INTO c_news_c_teachers_1_c (id, date_modified, deleted, c_news_c_teachers_1c_news_ida, c_news_c_teachers_1c_teachers_idb)
                        VALUES ('{$relation_id}','{$date_modified}', 0,'{$r2['news_id']}','{$args['teacher_id']}')";
                        $GLOBALS['db']->query($qInsertRelation);
                    }
                }
                break;
        }
        return array(
            'success' => 1,
        );
    }

}
