<?php


class StudentApiV2 extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'student-login' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'login'),
                'pathVars' => array(''),
                'method' => 'studentLogin',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'edit-student' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'editstudent'),
                'pathVars' => array(''),
                'method' => 'editStudent',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'edit-address' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'editaddress'),
                'pathVars' => array(''),
                'method' => 'editAddress',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'edit-parents' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'editparents'),
                'pathVars' => array(''),
                'method' => 'editParents',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'edit-other' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'editother'),
                'pathVars' => array(''),
                'method' => 'editOther',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'list-unread-notifications' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'unreadnotifications'),
                'pathVars' => array(''),
                'method' => 'listUnreadNotifications',
                'shortHelp' => '',
                'longHelp' => '',
                'noLoginRequired' => true,
            ),
            'get-payment-info' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'getpaymentinfo'),
                'pathVars' => array(''),
                'method' => 'getPaymentInfo',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'request-payment-check' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'requestpaymentcheck'),
                'pathVars' => array(''),
                'method' => 'requestPaymentCheck',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //NEW handle Notifications
            'list-notifications' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'listNotifications'),
                'pathVars' => array(''),
                'method' => 'listNotifications',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'count-unread' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'countUnreadNoti'),
                'pathVars' => array(''),
                'method' => 'countUnreadNoti',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'update-notification' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'students', 'updateNotification'),
                'pathVars' => array(''),
                'method' => 'updateNotification',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }

    function getListGallery($classIds, $str_student_id = ''){
        $ext1 = '';
        $ext2 = '';
        if (!empty($str_student_id)) {
            $ext1 = "INNER JOIN j_studentsituations l4 ON
            j_class.id = l4.ju_class_id AND l4.deleted = 0
            INNER JOIN contacts l5 ON l4.student_id = l5.id
            AND l5.deleted = 0 AND l4.student_type = 'Contacts'";
            $ext2 = "AND (IFNULL(l4.type, '') IN ('OutStanding' , 'Enrolled'))
            AND (l4.start_study <= DATE(l1.gallery_date))
            AND (l4.end_study >= DATE(l1.gallery_date))
            AND l5.id IN ('{$str_student_id}')
            AND (IFNULL(l5.status, '') NOT IN ('Cancelled'))";
        }
        $sqlAlbum = "SELECT DISTINCT
        IFNULL(j_class.id, '') class_id,
        IFNULL(j_class.name, '') class_name,
        IFNULL(l1.id, '') gallery_id,
        IFNULL(l1.name, '') gallery_name,
        IFNULL(l1.gallery_date, '') gallery_date,
        IFNULL(l1.description, '') gallery_description,
        IFNULL(l1.gallery_type, '') gallery_type,
        IFNULL(l2.id, '') pic_id,
        IFNULL(l2.google_id, '') google_id,
        IFNULL(l2.google_url, '') google_url,
        IFNULL(l2.file_mime_type, '') file_mime_type,
        IFNULL(l1.date_entered, '') date_entered,
        IFNULL(GROUP_CONCAT(l3.name), '') tag_name
        FROM j_class
        INNER JOIN j_class_c_gallery_1_c l1_1 ON j_class.id = l1_1.j_class_c_gallery_1j_class_ida AND l1_1.deleted = 0
        INNER JOIN c_gallery l1 ON l1.id = l1_1.j_class_c_gallery_1c_gallery_idb AND l1.deleted = 0
        INNER JOIN notes l2 ON l1.id = l2.parent_id AND l2.deleted = 0 AND l2.parent_type = 'C_Gallery'
        LEFT JOIN tag_bean_rel l3_1 ON
        l1.id = l3_1.bean_id AND l3_1.deleted = 0 AND l3_1.bean_module = 'C_Gallery'
        LEFT JOIN tags l3 ON
        l3.id = l3_1.tag_id AND l3.deleted = 0
        {$ext1}
        WHERE j_class.id IN ('" . implode("','", $classIds) . "') AND j_class.deleted = 0
        {$ext2}
        GROUP BY pic_id
        ORDER BY date_entered DESC";
        $albums = $GLOBALS['db']->query($sqlAlbum);
        $gls = array();
        while ($album = $GLOBALS['db']->fetchbyAssoc($albums)) {
            if (empty($album['class_id'])) break;
            $gls[$album['class_id']][$album['gallery_id']]['gallery_id'] = $album['gallery_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_id'] = $album['class_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_name'] = $album['class_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_name'] = $album['gallery_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_description'] = $album['gallery_description'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_type'] = $album['gallery_type'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_date'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $album['gallery_date']));
            $gls[$album['class_id']][$album['gallery_id']]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered']));
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['picture_id'] = $album['pic_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['picture_id'] = $album['google_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['google_url'] = $album['google_url'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['file_mime_type'] = $album['file_mime_type'];
            $gls[$album['class_id']][$album['gallery_id']]['images'][] = $album['google_url'];
            if (!empty($album['tag_name']))
                $gls[$album['class_id']][$album['gallery_id']]['tags'] = explode(",", $album['tag_name']);
            else $gls[$album['class_id']][$album['gallery_id']]['tags'] = array();
        }
        foreach ($gls as $class_id => $album) {
            foreach ($album as $gallery_id => $albumVal) {
                foreach ($albumVal['image_list'] as $pic_id => $picVal) {
                    $gls[$class_id][$gallery_id]['image_list'] = array_values($gls[$class_id][$gallery_id]['image_list']);
                }
            }
        }
        return $gls;
    }

    function studentLogin(ServiceBase $api, $args){
        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        global $timedate, $current_user, $app_list_strings;
        if (isset($args) && !empty($args)) {
            $now = $timedate->nowDb();
            $user_name = $args['portal_name'];
            $password = $args['password'];
            $module = $args['module'];

            $res = array();

            $nowDate = date('Y-m-d');

            //Check Username credential
            $q1 = "SELECT DISTINCT IFNULL(contacts.id, '') primaryid,
            IFNULL(contacts.first_name, '') first_name,
            IFNULL(contacts.last_name, '') last_name,
            IFNULL(contacts.full_student_name, '') name,
            IFNULL(contacts.contact_id, '') student_id,
            contacts.birthdate birthdate,
            IFNULL(contacts.status, '') status,
            IFNULL(contacts.phone_mobile, '') phone_mobile,
            IFNULL(contacts.picture, '') picture,
            IFNULL(contacts.cover_app, '') cover_app,
            IFNULL(l1.email_address, '') email,
            IFNULL(contacts.nick_name, '') nick_name,
            IFNULL(contacts.guardian_name, '') guardian_name,
            IFNULL(contacts.relationship, '') relationship,
            IFNULL(contacts.lead_source, '') lead_source,
            IFNULL(contacts.utm_source, '') utm_source,
            IFNULL(contacts.gender, '') gender,
            IFNULL(contacts.facebook, '') facebook,
            IFNULL(contacts.primary_address_street, '') address,
            IFNULL(contacts.primary_address_postalcode, '') address_postalcode,
            IFNULL(contacts.primary_address_state, '') address_state,
            IFNULL(contacts.primary_address_city, '') address_city,
            IFNULL(contacts.primary_address_country, '') address_country,
            IFNULL(contacts.assigned_user_id, '') assigned_to_user_id,
            IFNULL(contacts.team_id, '') team_id,
            IFNULL(l2.name, '') team_name,
            IFNULL(contacts.team_set_id, '') team_set_id,
            IFNULL(contacts.portal_name, '') portal_name,
            IFNULL(contacts.portal_password, '') portal_password,
            IFNULL(contacts.phone_guardian, '') phone_guardian,
            IFNULL(contacts.guardian_name_2, '') guardian_name_2,
            IFNULL(contacts.relationship2, '') relationship2,
            IFNULL(contacts.other_mobile, '') phone_guardian_2,
            contacts.lasted_view_noti lasted_view_noti
            FROM contacts
            LEFT JOIN email_addr_bean_rel l1_1 ON contacts.id = l1_1.bean_id
            AND l1_1.deleted = 0
            AND l1_1.bean_module = 'Contacts'
            AND l1_1.primary_address = 1
            LEFT JOIN
            email_addresses l1 ON l1.id = l1_1.email_address_id
            AND l1.deleted = 0
            LEFT JOIN teams l2 ON contacts.team_id = l2.id
            AND l2.deleted = 0
            WHERE (contacts.portal_name = '$user_name'
            OR contacts.phone_mobile = '$user_name')
            AND (contacts.portal_active = '1')
            AND contacts.deleted = 0
            ORDER BY first_name ASC";

            $students = $GLOBALS['db']->fetchArray($q1);
            if (count($students) == 0)
                return array(
                    'success' => false,
                    'error' => "invalid_username",
                );

            $student_list = array();
            //Check Password credential
            $pwdCheck = false;
            $studentIds = array_column($students, 'primaryid');
            foreach ($students as $student) {
                if ((User::checkPassword($password, $student['portal_password']) || md5($password) == $student['portal_password']) && !$pwdCheck)
                    $pwdCheck = true;
                //add student array
                $student['birthdate'] = $timedate->to_display_date($student['birthdate'], false);//format birthdate
                $student_list[$student['primaryid']] = $student;

                $student_list[$student['primaryid']]['school_name'] = '';
                $student_list[$student['primaryid']]['loyalty_point'] = 0;
                $student_list[$student['primaryid']]['total_payment'] = 0;
                $student_list[$student['primaryid']]['unread_noti'] = 0;
                $student_list[$student['primaryid']]['notifications'] = array();
                $student_list[$student['primaryid']]['sms'] = array();
                $student_list[$student['primaryid']]['loyalty'] = array();

                if (empty($module) || $module == 'payments')
                    $student_list[$student['primaryid']]['payments'] = array();

                if (empty($module) || $module == 'feedback')
                    $student_list[$student['primaryid']]['feedback'] = array();

                if (empty($module) || $module == 'class_list')
                    $student_list[$student['primaryid']]['class_list'] = array();

                if (empty($module) || $module == 'news_list')
                    $student_list[$student['primaryid']]['news_list'] = array();

            }

            if (!$pwdCheck)
                return array(
                    'success' => false,
                    'error' => "invalid_password",
                );

            $str_student_id = implode("','", array_column($students, 'primaryid'));

            //get Student ID login and push array
            $arrStudentId = array_column($students, 'primaryid');

            //get school
            $qgetschool = "SELECT DISTINCT
            IFNULL(contacts.id, '') student_id,
            IFNULL(l0.name, '') school_name
            FROM contacts
            INNER JOIN j_school l0 ON contacts.school_id = l0.id AND l0.deleted = 0
            WHERE (contacts.id IN ('$str_student_id')) AND contacts.deleted = 0";
            $getschoolstudent = $GLOBALS['db']->query($qgetschool);
            while ($res = $GLOBALS['db']->fetchbyAssoc($getschoolstudent)) {
                $student_list[$res['student_id']]['school_name'] = $res['school_name'];
            }

            if (empty($module) || $module == 'class_list') {
                require_once('include/externalAPI/ClassIn/ExtAPIClassIn.php');
                $ExtApiClassIn  = new ExtApiClassIn();
                $schoolId       = $ExtApiClassIn->_SID;
                $AppUrl         = $ExtApiClassIn->_AppUrl;
                //Get Current Class
                $nowDb = $timedate->nowDb();
                $q2 = "SELECT DISTINCT
                IFNULL(contacts.id, '') student_id,
                IFNULL(l3.id, '') class_id,
                IFNULL(l3.picture, '') picture,
                IFNULL(l3.name, '') class_name,
                IFNULL(l3.class_code, '') class_code,
                l3.start_date start_date,
                l3.end_date end_date,
                l3.date_entered date_entered,
                IFNULL(l3.kind_of_course, '') kind_of_course,
                IFNULL(l3.level, '') level,
                CONCAT(IFNULL(l3.kind_of_course, ''), ' ', IFNULL(l3.level, '')) koc_level,
                IFNULL(l3.modules, '') modules,
                l3.hours hours,
                IFNULL(l3.status, '') class_status,
                IFNULL(l2.lesson_number, '') ss_lesson,
                IFNULL(l4.id, '') assigned_to_id,
                IFNULL(l4.full_user_name, '') assigned_to,
                IFNULL(l5.id, '') koc_id,
                IFNULL(l5.name, '') koc_name,
                IFNULL(l6.id, '') room_id,
                IFNULL(l6.name, '') room_name,
                IFNULL(l7.id, '') teacher_id,
                IFNULL(l7.full_teacher_name, '') teacher_name,
                IFNULL(l3.max_size, '') max_size,
                IFNULL(l8.id, '') team_id,
                IFNULL(l8.name, '') team_name,
                IFNULL(l8.code_prefix, '') team_code,
                IFNULL(l2.id, '') ss_id,
                IFNULL(l2.name, '') ss_name,
                IFNULL(l2.week_no, '') ss_week_no,
                IFNULL(l2.observe_score, 0) ss_observe_score,
                IFNULL(l2.syllabus_custom, '') ss_lesson_note,
                IFNULL(l2.syllabus_custom, '') ss_syllabus_custom,
                IFNULL(l2.topic_custom, '') ss_topic_custom,
                IFNULL(l2.syllabus_id, '') ss_syllabus_id,
                IFNULL(l13.name, '') ss_syllabus_topic,
                IFNULL(l13.description, '') ss_syllabus_content,
                IFNULL(l13.homework, '') ss_syllabus_homework,
                l2.till_hour ss_till_hour,
                l2.date_start ss_date_start,
                l2.date_end ss_date_end,
                l2.duration_cal ss_duration_cal,
                IFNULL(l9.id, '') l9_id,
                IFNULL(l9.full_teacher_name, '') ss_ta_name,
                IFNULL(l10.id, '') l10_id,
                IFNULL(l10.full_teacher_name, '') ss_teacher_name,
                IFNULL(l11.id, '') l11_id,
                IFNULL(l11.name, '') ss_room_name,
                IFNULL(l2.type, '') ss_learning_type,
                (CASE
                WHEN DATE_SUB(l2.date_end, INTERVAL 1 HOUR) < '$nowDb' THEN ''
                WHEN l2.type = 'ClassIn' THEN (CONCAT('$AppUrl','enterclass?telephone=',IFNULL(contacts.phone_prefix,'84'),'-',SUBSTR(contacts.phone_mobile,2),'&classId=',l2.external_id,'&courseId=',l3.onl_course_id,'&schoolId=$schoolId'))
                ELSE IFNULL(l2.join_url, '')
                END) ss_displayed_url,
                IFNULL(l12.id, '') attendance_id,
                IFNULL(l12.homework_comment, '') homework_comment,
                l12.homework_score homework_score,
                IFNULL(l12.homework, '') do_homework,
                IFNULL(l12.description, '') teacher_comment,
                IFNULL(l12.care_comment, '') care_comment,
                IFNULL(l12.attended, 0) attended,
                IFNULL(l12.attendance_type, '') attendance_type,
                IFNULL(l12.star_rating, '') star_rating,
                IFNULL(l12.customer_comment, '') customer_comment,
                IFNULL(l12.check_in_time, '') check_in_time,
                IFNULL(l12.check_out_time, '') check_out_time,
                l2.lesson_number lesson_number,
                IFNULL(l14.total_attended, '') total_attended
                FROM contacts
                INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0
                INNER JOIN meetings_contacts l2_1 ON l1.id = l2_1.situation_id AND l2_1.deleted = 0 AND contacts.id = l2_1.contact_id
                INNER JOIN meetings l2 ON l2.id = l2_1.meeting_id AND l2.deleted = 0
                INNER JOIN j_class l3 ON l2.ju_class_id = l3.id AND l3.deleted = 0
                LEFT JOIN users l4 ON l3.assigned_user_id = l4.id AND l4.deleted = 0
                LEFT JOIN j_kindofcourse l5 ON l3.koc_id = l5.id AND l5.deleted = 0
                LEFT JOIN c_rooms l6 ON l3.room_id = l6.id AND l6.deleted = 0
                LEFT JOIN c_teachers l7 ON l3.teacher_id = l7.id AND l7.deleted = 0
                LEFT JOIN teams l8 ON l3.team_id = l8.id AND l8.deleted = 0
                LEFT JOIN c_teachers l9 ON l2.teacher_cover_id=l9.id AND l9.deleted=0
                LEFT JOIN c_teachers l10 ON l2.teacher_id=l10.id AND l10.deleted=0
                LEFT JOIN c_rooms l11 ON l2.room_id=l11.id AND l11.deleted=0
                LEFT JOIN c_attendance l12 ON l2.id = l12.meeting_id AND l12.deleted = 0 AND l12.student_id = contacts.id
                LEFT JOIN j_syllabus l13 ON l2.syllabus_id = l13.id AND l13.deleted = 0
                INNER JOIN j_classstudents l14 ON contacts.id = l14.student_id AND l3.id = l14.class_id AND l14.deleted = 0
                WHERE (((contacts.id IN ('$str_student_id'))
                AND (IFNULL(l2.meeting_type, '') = 'Session')
                AND (IFNULL(l2.session_status, '') <> 'Cancelled' OR (IFNULL(l2.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
                AND (IFNULL(l1.type, '') IN ('Enrolled' , 'OutStanding', 'Demo'))))
                AND contacts.deleted = 0
                ORDER BY student_id, CASE";
                $types = $GLOBALS['app_list_strings']['status_class_list'];
                $count_k = 1;
                foreach ($types as $_type => $val_)
                    $q2 .= " WHEN class_status = '$_type' THEN " . $count_k++;
                $q2 .= " ELSE $count_k END ASC,l3.end_date DESC, class_id, l2.date_start";
                $rowS = $GLOBALS['db']->fetchArray($q2);
                //Get Kind Of Course Syllabus - For best perfomance
                $classIds = implode("','", array_unique(array_column($rowS, 'class_id')));
                $koc = array();
                $q2_1 = "SELECT DISTINCT
                IFNULL(l1.id, '') koc_id,
                IFNULL(l1.name, '') koc_name,
                IFNULL(l1.kind_of_course, '') kind_of_course,
                IFNULL(l1.content, '') content,
                IFNULL(l1.lms_content, '') lms_content
                FROM j_class INNER JOIN j_kindofcourse l1 ON j_class.koc_id = l1.id  AND l1.deleted = 0
                WHERE (j_class.id IN ('$classIds')) AND j_class.deleted = 0";
                $rowK = $GLOBALS['db']->fetchArray($q2_1);
                foreach ($rowK as $key => $rK) {
                    $koc[$rK['koc_id']]['koc_id'] = $rK['koc_id'];
                    $koc[$rK['koc_id']]['koc_name'] = $rK['koc_name'];
                    $koc[$rK['koc_id']]['kind_of_course'] = $rK['kind_of_course'];
                    $koc[$rK['koc_id']]['content'] = ((!empty($rK['content'])) ? json_decode(html_entity_decode($rK['content']), true) : '');
                }
                $classes = array();

                $arrClassFinish = array();
                $arrResultStudent = array();

                $runStudent = '####';
                $runClass = '####';
                //List Class ID
                $classIds = array();

                foreach ($rowS as $_k2 => $r2) {
                    if ($runStudent != $r2['student_id'] || $runClass != $r2['class_id']) {
                        //Đã tham gia
                        $count_studied = 0;
                        $ss_studied = 0;
                        $attended_lesson = 0;
                        $ss_count = 0;
                        $runStudent = $r2['student_id'];
                        $runClass = $r2['class_id'];

                        $classes[$runStudent][$runClass]['id'] = $r2['class_id'];
                        $classes[$runStudent][$runClass]['class_name'] = $r2['class_name'];
                        $classes[$runStudent][$runClass]['picture'] = $r2['picture'];
                        $classes[$runStudent][$runClass]['class_code'] = $r2['class_code'];
                        $classes[$runStudent][$runClass]['start_date'] = $timedate->to_display_date($r2['start_date'], false);
                        $classes[$runStudent][$runClass]['end_date'] = $timedate->to_display_date($r2['end_date'], false);
                        $classes[$runStudent][$runClass]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['date_entered']));
                        $classes[$runStudent][$runClass]['kind_of_course'] = $r2['kind_of_course'];
                        $classes[$runStudent][$runClass]['level'] = $r2['level'];
                        $classes[$runStudent][$runClass]['hours'] = $r2['hours'];
                        $classes[$runStudent][$runClass]['assigned_to'] = $r2['assigned_to'];
                        $classes[$runStudent][$runClass]['koc_id'] = $r2['koc_id'];
                        $classes[$runStudent][$runClass]['koc_name'] = $r2['koc_name'];
                        $classes[$runStudent][$runClass]['room_name'] = $r2['room_name'];
                        $classes[$runStudent][$runClass]['teacher_name'] = $r2['teacher_name'];
                        $classes[$runStudent][$runClass]['max_size'] = $r2['max_size'];
                        $classes[$runStudent][$runClass]['team_name'] = $r2['team_name'];
                        $classes[$runStudent][$runClass]['team_code'] = $r2['team_code'];

                        $classes[$runStudent][$runClass]['status'] = $r2['class_status'];

                        //Add new average rating by nnamnguyen
                        $count_session_is_rated = 0;
                        $rating_score = 0;
                        $classes[$runStudent][$runClass]['average_rating'] = '';

                        $content = $koc[$r2['koc_id']]['content'];
                        foreach ($content as $ct) {
                            if ($ct['levels'] == $r2['level'])
                                $docs = $ct['doc_url'];
                        }
                        $classes[$runStudent][$runClass]['doc_url'] = $docs;

                        $classes[$runStudent][$runClass]['sessions'] = array();
                        $classes[$runStudent][$runClass]['albums'] = array();
                        $classes[$runStudent][$runClass]['gradebooks'] = array();

                    }

                    //nếu thời gian hiện tại nằm trong thòi gian student situation thì lấy trạng thái lớp là trạng thái lớp hiện tại
                    if ($r2['class_status'] != 'Finished' && $classes[$runStudent][$runClass]['status'] != $r2['class_status']) {
                        $classes[$runStudent][$runClass]['status'] = $r2['class_status'];
                    }

                    $session = array();
                    $session['ss_id'] = $r2['ss_id'];
                    $session['ss_name'] = $r2['ss_name'];
                    $session['ss_lesson'] = $r2['ss_lesson'];
                    $session['ss_week_no'] = $r2['ss_week_no'];
                    $session['ss_week_date'] = date('l', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_till_hour'] = $r2['ss_till_hour'];
                    $session['ss_db_date_start'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_db_date_end'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_end']));
                    $session['ss_date_start'] = $timedate->to_display_date_time($r2['ss_date_start']);
                    $session['ss_date_end'] = $timedate->to_display_date_time($r2['ss_date_end']);
                    $session['ss_db_date'] = date('Y-m-d', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_learning_type'] = $r2['ss_learning_type'];
                    $session['ss_displayed_url'] = $r2['ss_displayed_url'];
                    if ($now < $r2['ss_date_start']) {
                        $session['ss_status'] = "not_stated";
                        if ($lesson_num_prev == (int)$r2['lesson_number']) {
                            $count_studied--;
                        }
                    }
                    elseif ($now >= $r2['ss_date_start'] && $now <= $r2['ss_date_end']) {
                        $session['ss_status'] = "in_progress";
                        if ($lesson_num_prev == (int)$r2['lesson_number']) {
                            $count_studied--;
                        }
                    }
                    elseif ($now > $r2['ss_date_end']) {
                        $session['ss_status'] = "finished";
                        if($lesson_num_prev != (int)$r2['lesson_number']){
                            $count_studied++;
                            $lesson_num_prev = (int)$r2['lesson_number'];
                        }
                        $ss_studied++;
                    }

                    $session['ss_duration_cal'] = $r2['ss_duration_cal'];
                    $session['ss_ta_name'] = $r2['ss_ta_name'];
                    $session['ss_teacher_name'] = $r2['ss_teacher_name'];
                    $session['ss_room_name'] = $r2['ss_room_name'];
                    $session['ss_observe_score'] = $r2['ss_observe_score'];
                    $session['ss_lesson_note'] = $r2['ss_lesson_note'];
                    $session['ss_syllabus_custom'] = $r2['ss_syllabus_custom'];
                    $session['ss_topic_custom'] = $r2['ss_topic_custom'];
                    $session['syllabus_topic'] = $r2['ss_syllabus_topic'];
                    $session['syllabus_activities'] = $r2['ss_syllabus_content'];
                    $session['syllabus_homework'] = $r2['ss_syllabus_homework'];

                    //LAY THONG TIN SO LIEN LAC
                    $session['att_id'] = $r2['attendance_id'];
                    $session['att_homework_comment'] = $r2['homework_comment'];
                    $session['att_homework_score'] = $r2['homework_score'];
                    $session['att_do_homework'] = $r2['do_homework'];
                    $session['att_teacher_comment'] = $r2['teacher_comment'];
                    $session['att_care_comment'] = $r2['care_comment'];
                    $session['att_attended'] = $r2['attended'];
                    $session['att_attendance_type'] = $r2['attendance_type'];
                    $session['att_star_rating'] = $r2['star_rating'];
                    $session['att_customer_comment'] = $r2['customer_comment'];
                    $session['check_in_time'] = $timedate->to_display_time($r2['check_in_time']);
                    $session['check_out_time'] = $timedate->to_display_time($r2['check_out_time']);
                    //Custom: $attended_session
                    if ($session['ss_status'] === 'finished') {
                        if ($session['att_attendance_type'] === 'P' || $session['att_attendance_type'] === 'L') {
                            if($lesson_num_prev2 != (int)$r2['lesson_number']){
                                $attended_lesson++;
                                $lesson_num_prev2 = (int)$r2['lesson_number'];
                            }
                        } else {
                            if($lesson_num_prev2 == (int)$r2['lesson_number']){
                                $attended_lesson--;
                            }
                        }
                    }
                    $ss_count++;
                    $qCountStudied = "SELECT COUNT(DISTINCT l2.lesson_number) 
                        FROM j_class 
                        INNER JOIN meetings l2 ON l2.ju_class_id = j_class.id AND l2.deleted = 0
                        WHERE l2.date_end < '{$now}' AND j_class.id = '{$runClass}'";
                    $lesson_studied = $GLOBALS['db']->getOne($qCountStudied);

                    $classes[$runStudent][$runClass]['sessions'][] = $session;
                    $classes[$runStudent][$runClass]['count_studied_lesson'] = $count_studied;
                    $classes[$runStudent][$runClass]['count_studied'] = $ss_studied;
                    $classes[$runStudent][$runClass]['attended_session'] = (int)$r2['total_attended'];
                    $classes[$runStudent][$runClass]['total_session'] = (string)$ss_count;
                    $classes[$runStudent][$runClass]['total_lesson'] = (int)$r2['lesson_number'];
                    $classes[$runStudent][$runClass]['completed_lesson'] = $lesson_studied;
                    $classes[$runStudent][$runClass]['attended_lesson'] = $attended_lesson < 0 ? 0 : $attended_lesson;

                    //Custom average rating
                    if ($session['att_homework_score'] !== "" && $session['att_homework_score'] !== null) {
                        $count_session_is_rated += 1;
                        if ($session['att_homework_score'] > 5) {
                            $rating_score += 5;
                        } else {
                            $rating_score += $session['att_homework_score'];
                        }
                        $rating = number_format($rating_score / $count_session_is_rated, 1);
                        $classes[$runStudent][$runClass]['average_rating'] = $rating;
                    }
                    //Custom attended session percent
                    if ($classes[$runStudent][$runClass]['count_studied'] !== 0) {
                        $attended_session_percent = (($classes[$runStudent][$runClass]['attended_session']) / ($classes[$runStudent][$runClass]['count_studied'])) * 100;
                        $attended_session_percent = number_format($attended_session_percent, 0);
                    } else {
                        $attended_session_percent = 0;
                    }
                    if ($classes[$runStudent][$runClass]['count_studied_lesson'] !== 0) {
                        $attended_lesson_percent = (($classes[$runStudent][$runClass]['attended_lesson']) / ($classes[$runStudent][$runClass]['count_studied_lesson'])) * 100;
                        $attended_lesson_percent = number_format($attended_lesson_percent, 0);
                    } else {
                        $attended_lesson_percent = 0;
                    }
                    $classes[$runStudent][$runClass]['attended_session_percent'] = $attended_session_percent;
                    $classes[$runStudent][$runClass]['attended_lesson_percent'] = $attended_lesson_percent;
                    $classes[$runStudent][$runClass]['rank_in_class'] = null;

                    //lấy điểm số của tất cả các học sinh trong lớp của học viên đang login để set rank cho học viên đó
                    if ($r2['class_status'] == 'Finished') {
                        $qClassFinish = "SELECT DISTINCT
                        IFNULL(c.id, '') student_id,
                        IFNULL (l2.id,'') class_id,
                        IFNULL(l5.final_result, '') final_result,
                        @rank:=1 AS rank_in_class
                        FROM contacts c
                        INNER JOIN j_classstudents l1 ON l1.student_id = c.id AND l1.deleted=0
                        INNER JOIN j_class l2 ON l2.id = l1.class_id AND l2.deleted=0 AND l2.status='Finished'
                        INNER JOIN j_class_j_gradebook_1_c l3 ON l3. j_class_j_gradebook_1j_class_ida = l2.id AND l3.deleted=0
                        INNER JOIN j_gradebook l4 ON l4.id = l3. j_class_j_gradebook_1j_gradebook_idb AND l4.deleted=0 AND l4.status='Approved' AND l4.type='Overall'
                        INNER JOIN j_gradebookdetail l5 ON l5.gradebook_id = l4.id AND l5.student_id = c.id AND l5.j_class_id=l2.id AND l5.deleted=0
                        WHERE c.deleted = 0 AND (l2.id IN ('$runClass'))
                        ORDER BY final_result DESC";
                        $rowClassFinish = $GLOBALS['db']->fetchArray($qClassFinish);
                        $arrClassFinish[$runClass] = $rowClassFinish;
                    }

                    if (!in_array($runClass, $classIds)){
                        $classIds[] = $runClass;
                    }

                }
                // Set rank cho học viên theo điều kiện config trên mobile app config
                $admin = new Administration();
                $admin->retrieveSettings();
                $isShowRank = $admin->settings['default_student_ranking'];
                if ($isShowRank == 'true') {
                    $rankPercent = $admin->settings['default_percentile_rank'];
                    foreach ($arrStudentId as $classId => $infoStudent) {
                        foreach ($arrClassFinish as $key => $studentClass) {
                            foreach ($studentClass as $key2 => $infoStudentClass) {
                                if ($infoStudentClass['student_id'] == $infoStudent) {
                                    $arrResultStudent[$infoStudent][$key] = $infoStudentClass;
                                }
                            }
                        }
                    }
                    foreach ($arrResultStudent as $idStudent => $infoStudentInClass) {
                        foreach ($infoStudentInClass as $idClassStudentCheck => $infoStudent) {
                            foreach ($arrClassFinish as $classId => $studentClass) {
                                if ($classId == $idClassStudentCheck) {
                                    $number_of_student = count($studentClass);
                                    $opRank = 1;
                                    $final_result_student_ago = 0;
                                    foreach ($studentClass as $key2 => $infoStudentClass) {
                                        if ((float)$infoStudentClass['final_result'] > (float)$infoStudent['final_result'] && (float)$infoStudentClass['final_result'] != $final_result_student_ago) {
                                            $opRank += 1;
                                        }
                                        $final_result_student_ago = (float)$infoStudentClass['final_result'];
                                    }
                                    $topRank = ($rankPercent / 100) * $number_of_student;
                                    $topRank = number_format($topRank, 0);
                                    //Nếu nằm trong top rank thì lấy, ngược lại mặc định trả về null
                                    if ($opRank <= $topRank) {
                                        $classes[$idStudent][$idClassStudentCheck]['rank_in_class'] = $opRank;
                                    }
                                }
                            }
                        }
                    }
                }
                //Add Gallery Image
                $gls = $this->getListGallery($classIds, $str_student_id);
                //Assigned Gallery By Class
                foreach ($classes as $stC => $st_class) {
                    foreach ($st_class as $class_id => $class) {
                        if (array_key_exists($class_id, $gls))
                            $classes[$stC][$class_id]['albums'] = array_values($gls[$class_id]);
                    }
                }

                $q4 = "SELECT DISTINCT
                IFNULL(l2.id,'') student_id,
                IFNULL(l1.id, '') class_id,
                IFNULL(j_gradebook.id, '') gradebook_id,
                IFNULL(j_gradebook.status, '') status,
                IFNULL(j_gradebook.rate, '') rate,
                IFNULL(j_gradebook.type, '') gradebook_type,
                IFNULL(j_gradebook.minitest, '') minitest
                FROM j_gradebook
                INNER JOIN j_class_j_gradebook_1_c l1_1 ON j_gradebook.id = l1_1.j_class_j_gradebook_1j_gradebook_idb AND l1_1.deleted = 0
                INNER JOIN j_class l1 ON l1.id = l1_1.j_class_j_gradebook_1j_class_ida AND l1.deleted = 0
                INNER JOIN  j_classstudents l2_1 ON l1.id=l2_1.class_id AND l2_1.deleted=0
                INNER JOIN  contacts l2 ON l2.id=l2_1.student_id AND l2.deleted=0
                WHERE (((l1.id IN ('" . implode("','", $classIds) . "'))
                AND (l2.id IN ('$str_student_id'))
                AND (IFNULL(j_gradebook.status, '') = 'Approved')))
                AND j_gradebook.deleted = 0
                ORDER BY class_id ASC,
                CASE WHEN
                (j_gradebook.type = '' OR j_gradebook.type IS NULL) THEN 0";
                $types = $GLOBALS['app_list_strings']['gradebook_type_options'];
                $count_k = 1;
                foreach ($types as $_type => $val_)
                    $q4 .= " WHEN j_gradebook.type = '$_type' THEN " . $count_k++;
                $q4 .= " ELSE $count_k END ASC";
                $rs4 = $GLOBALS['db']->query($q4);
                while ($r4 = $GLOBALS['db']->fetchbyAssoc($rs4)) {
                    $gradebook = BeanFactory::getBean('J_Gradebook', $r4['gradebook_id']);
                    $gbArray = $gradebook->getDetailForStudent($r4['student_id']);
                    $gbArray['gradebook_id'] = $r4['gradebook_id'];
                    $gbArray['rate'] = $r4['rate'];
                    $classes[$r4['student_id']][$r4['class_id']]['gradebooks'][] = $gbArray;
                }

                //Assign to result
                foreach ($student_list as $stid => $student)
                    $student_list[$stid]['class_list'] = array_values($classes[$stid]);

                $lastDate = date('Y-m-d', strtotime('last day of this month'));
            }

            if (empty($module) || $module == 'payments') {
                //Get Payment
                $q3 = "SELECT DISTINCT
                IFNULL(j_paymentdetail.id, '') receipt_id,
                IFNULL(l1.id, '') payment_id,
                IFNULL(l2.id, '') student_id,
                IFNULL(j_paymentdetail.name,'') receipt_code,
                j_paymentdetail.expired_date expired_date,
                j_paymentdetail.payment_date receipt_date,
                j_paymentdetail.payment_datetime receipt_datetime,
                j_paymentdetail.payment_amount receipt_amount,
                j_paymentdetail.audited_amount audited_amount,
                IFNULL(j_paymentdetail.status, '') status,
                IFNULL(j_paymentdetail.reconcile_status, '') reconcile_status,
                IFNULL(j_paymentdetail.payment_method, '') method,
                IFNULL(j_paymentdetail.bank_account, '') bank_account,
                IFNULL(j_paymentdetail.description, '') description,
                IFNULL(l4.name, '') course_fee,
                IFNULL(l1.payment_type, '') payment_type,
                IFNULL(l1.kind_of_course, '') kind_of_course,
                IFNULL(l3.name, '') team_name
                FROM j_paymentdetail
                INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
                INNER JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
                INNER JOIN teams l3 ON l1.team_id = l3.id AND l3.deleted = 0
                LEFT JOIN j_coursefee_j_payment_1_c l4_1 ON l1.id = l4_1.j_coursefee_j_payment_1j_payment_idb AND l4_1.deleted = 0
                LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_1j_coursefee_ida AND l4.deleted = 0
                WHERE (((l2.id IN ('$str_student_id'))
                AND (j_paymentdetail.payment_amount > 0)
                AND ( ((j_paymentdetail.status = 'Unpaid') AND ( (j_paymentdetail.expired_date <= '$lastDate') OR (j_paymentdetail.expired_date IS NULL) OR (j_paymentdetail.expired_date = '') )) OR ((j_paymentdetail.status = 'Paid')))))
                AND j_paymentdetail.deleted = 0
                ORDER BY l2.id ASC,
                CASE WHEN IFNULL(j_paymentdetail.status, '') = 'Paid' THEN 1
                WHEN IFNULL(j_paymentdetail.status, '') = 'Unpaid' THEN 2
                ELSE 3 END DESC, j_paymentdetail.payment_date DESC, j_paymentdetail.expired_date DESC";
                $rs3 = $GLOBALS['db']->query($q3);
                $key = 0;
                $runStudent = '####';

                while ($r3 = $GLOBALS['db']->fetchbyAssoc($rs3)) {
                    if ($runStudent != $r3['student_id']) $key = 0;
                    $payment = array();
                    $payment['receipt_id']      = $r3['receipt_id'];
                    $payment['payment_id']      = $r3['payment_id'];
                    $payment['receipt_code']    = $r3['receipt_code'];
                    $payment['receipt_date_db'] = $r3['receipt_date'];
                    $payment['receipt_date']    = $timedate->to_display_date($r3['receipt_date'], false);
                    $payment['expired_date']    = $timedate->to_display_date($r3['expired_date'], false);
                    $payment['receipt_datetime']= $timedate->to_display_date_time($r3['receipt_datetime']);

                    $payment['reconcile_status']= $r3['reconcile_status'];
                    $payment['audited_amount']  = $r3['audited_amount'];
                    $payment['bank_account']    = $r3['bank_account'];

                    $payment['expired_date']    = $timedate->to_display_date($r3['expired_date'], false);
                    $payment['receipt_amount']  = format_number($r3['receipt_amount']);
                    $payment['status']          = $r3['status'];
                    if ($payment['status'] == 'Unpaid') {
                        $payment['receipt_date_db'] = '';
                        $payment['receipt_date'] = '';
                    }
                    $payment['method']          = $r3['method'];
                    $payment['course_fee']      = $r3['course_fee'];
                    if ($r3['payment_type'] == 'Book/Gift' || $r3['payment_type'] == 'Deposit')
                        $payment['course_fee']  =  $r3['payment_type'];

                    $payment['payment_type']    = $r3['payment_type'];
                    $payment['description']     = $r3['description'];
                    $payment['team_name']       = $r3['team_name'];
                    $student_list[$r3['student_id']]['payments'][$key] = $payment;
                    $student_list[$r3['student_id']]['total_payment'] += $r3['receipt_amount'];
                    $runStudent = $r3['student_id'];
                    $key++;
                }
            }

            //Lấy ra các id của center có đã chọn trong Contacts
            $center_list_contact = array();
            $qCenterInContact = "SELECT DISTINCT IFNULL(contacts.id, '') student_id,
            IFNULL(l1.id, '') team_id,
            IFNULL(l1.name, '') team_name,
            IFNULL(l1.parent_id, '') parent_team_id
            FROM contacts
            INNER JOIN team_sets_teams l1_1 ON contacts.team_set_id = l1_1.team_set_id AND l1_1.deleted = 0
            INNER JOIN teams l1 ON l1.id = l1_1.team_id AND l1.deleted = 0 AND l1.private = 0
            WHERE contacts.deleted = 0
            AND (contacts.id IN ('$str_student_id'))";

            $resultCenter = $GLOBALS['db']->query($qCenterInContact);

            $key = 0;
            $runStudent = '####';
            while ($r = $GLOBALS['db']->fetchbyAssoc($resultCenter)) {
                if ($runStudent != $r['student_id']) $key = 0;
                $teams = array();
                $teams['team_id'] = $r['team_id'];
                $teams['team_name'] = $r['team_name'];
                // Get parent center
                if (!empty($r['parent_team_id'])) {
                    $parentCenterIds = array();
                    $parentCenterIds[] = $r['parent_team_id'];
                    $parentCenter = BeanFactory::getBean('Teams', $r['parent_team_id']);
                    while (!empty($parentCenter->parent_id)) {
                        $parentCenterIds[] = $parentCenter->parent_id;
                        $parentCenter = BeanFactory::getBean('Teams', $parentCenter->parent_id);
                    }
                }

                $center_list_contact[$r['student_id']][$key] = $teams;
                $center_list_contact[$r['student_id']][$key]['parent_team_id'] = $parentCenterIds;
                $runStudent = $r['student_id'];
                $key++;
            }
            //Thêm news tương ứng với từng học viên (news theo center và parent center của HV)
            foreach ($center_list_contact as $student_id => $valueCenter) {
                $arrIdCenter = array();
                foreach ($valueCenter as $key => $idCenter) {
                    array_push($arrIdCenter, $idCenter['team_id']);
                    foreach ($idCenter['parent_team_id'] as $idParentCenter) {
                        $arrIdCenter[] = $idParentCenter;
                    }
                }
                $qGetNews = "SELECT DISTINCT IFNULL(c_news.id, '') primaryid,
                IFNULL(c_news.name, '') name,
                IFNULL(c_news.picture, '') thumbnail,
                IFNULL(c_news.url, '') url,
                IFNULL(c_news.pin, 0) pin,
                IFNULL(c_news.description, '') description,
                IFNULL(c_news.type_news, '') type_news,
                c_news.start_date start_date,
                c_news.end_date end_date,
                c_news.date_entered date_entered
                FROM c_news
                INNER JOIN team_sets_teams l1_1 ON c_news.team_set_id = l1_1.team_set_id AND l1_1.deleted = 0
                INNER JOIN teams l1 ON l1.id = l1_1.team_id AND l1.deleted = 0 AND l1.private = 0
                WHERE (c_news.status NOT IN ('Inactive'))
                AND (c_news.type LIKE '%^Student^%')
                AND c_news.deleted = 0
                AND l1.id IN ('" . implode("', '", $arrIdCenter) . "')
                ORDER BY date_entered DESC";

                $rsGetNews = $GLOBALS['db']->query($qGetNews);

                $key = 0;
                $runStudent = '####';

                while ($r = $GLOBALS['db']->fetchbyAssoc($rsGetNews)) {
                    if ($runStudent != $student_id) $key = 0;

                    $qViewNews = "SELECT IFNULL(SUM(count_read_news) , 0)
                    FROM c_news_contacts_1_c
                    WHERE deleted = 0 AND c_news_contacts_1c_news_ida = '{$r['primaryid']}'";
                    $views = $GLOBALS['db']->getOne($qViewNews);

                    $news = array();

                    $news['id']= $r['primaryid'];
                    $news['name']= $r['name'];
                    $news['thumbnail']= $r['thumbnail'];
                    $news['url']= $r['url'];
                    $news['type']= $r['type'];
                    $news['pin']= $r['pin'];
                    $news['description']= $r['description'];
                    $news['type_news']= $r['type_news'];
                    $news['date_entered']= $timedate->to_display_date_time($r['date_entered']);
                    $news['end_date']= $timedate->to_display_date($r['end_date'], false);
                    $news['start_date']= $timedate->to_display_date($r['start_date'], false);
                    $news['views']= $views;
                    $news['db_date_entered']= $r['db_date_entered'];
                    $news['db_end_date']= $r['db_end_date'];
                    $news['db_start_date']= $r['db_start_date'];

                    $student_list[$student_id]['news_list'][$key] = $news;
                    $runStudent = $student_id;
                    $key++;
                }
            }

            if (empty($module) || $module == 'feedback') {
                //Get Feedback
                $q4 = "SELECT DISTINCT
                IFNULL(cases.id, '') primaryid,
                IFNULL(cases.name, '') subject,
                IFNULL(l3.id, '') student_id, IFNULL(l3.full_student_name, '') student_name,
                IFNULL(l2.id, '') class_id, IFNULL(l2.name, '') class_name,
                IFNULL(cases.case_number, '') case_number,
                IFNULL(cases.type, '') type,
                IFNULL(cases.status, '') case_status,
                IFNULL(cases.rate, '') rate,
                cases.date_entered date_entered,
                IFNULL(cases.description, '') feedback_content,
                IFNULL(l4.id, '') assigned_to_id, IFNULL(l4.full_user_name, '') assigned_to,
                IFNULL(l5.id, '') create_by, IFNULL(l5.full_user_name, '') create_by_name,
                IFNULL(l5.picture, '') created_by_avatar,
                IFNULL(l1.id, '') center_id,
                IFNULL(l1.name, '') center
                FROM cases
                INNER JOIN teams l1 ON cases.team_id = l1.id AND l1.deleted = 0
                LEFT JOIN j_class_cases_1_c l2_1 ON cases.id = l2_1.j_class_cases_1cases_idb AND l2_1.deleted = 0
                LEFT JOIN j_class l2 ON l2.id = l2_1.j_class_cases_1j_class_ida AND l2.deleted = 0
                LEFT JOIN contacts_cases_1_c l3_1 ON cases.id = l3_1.contacts_cases_1cases_idb AND l3_1.deleted = 0
                LEFT JOIN contacts l3 ON l3.id = l3_1.contacts_cases_1contacts_ida AND l3.deleted = 0
                LEFT JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
                LEFT JOIN users l5 ON cases.created_by = l5.id AND l5.deleted = 0
                WHERE (((l3.id IN ('$str_student_id'))))
                AND cases.portal_viewable = 1
                AND cases.deleted = 0
                GROUP BY cases.id
                ORDER BY l3.id ASC, CASE";
                $fb_status = $GLOBALS['app_list_strings']['case_status_dom'];
                $i = 1;
                foreach ($fb_status as $_status => $val)
                    $q4 .= " WHEN case_status = '$_status' THEN " . $i++;
                $q4 .= " ELSE $i END ASC, last_comment_date DESC";
                $rs4 = $GLOBALS['db']->fetchArray($q4);

                //Get Last Feedback Cmt
                $fbIds = implode("','",array_column($rs4, 'primaryid'));
                $q9 = "SELECT cs.*
                FROM c_comments cs, (SELECT  cc.parent_id parent_id, max(cc.date_entered) max_date_entered
                FROM c_comments cc
                WHERE cc.deleted = 0 AND cc.parent_id IN ('$fbIds') AND cc.parent_type = 'Cases' AND cc.is_unsent = 0
                GROUP BY cc.parent_id) cc_ma
                WHERE (cs.parent_id = cc_ma.parent_id) AND (cs.parent_type = 'Cases')
                AND (cc_ma.max_date_entered = cs.date_entered) AND (cs.parent_id IN ('$fbIds'))
                GROUP BY cs.id";
                $rs9 = $GLOBALS['db']->fetchArray($q9);
                $fb_last = array();
                foreach($rs9 as $ind => $r9) {
                    $row_u = $GLOBALS['db']->fetchOne("SELECT IFNULL(full_user_name, '') full_user_name, IFNULL(picture, '') created_by_avatar FROM users WHERE id = '{$r9['created_by']}'");
                    $fb_last[$r9['parent_id']]['lcmt_content']          = $r9['description'];
                    $fb_last[$r9['parent_id']]['lcmt_datetime']         = date('Y-m-d H:i:s', strtotime("+7 hours ".$r9['date_entered']));
                    $fb_last[$r9['parent_id']]['lcmt_created_by_name']  = $row_u['full_user_name'];
                    $fb_last[$r9['parent_id']]['lcmt_created_by']       = $r9['created_by'];
                    $fb_last[$r9['parent_id']]['lcmt_created_by_avatar']= $row_u['created_by_avatar'];
                    $fb_last[$r9['parent_id']]['lcmt_unread_app']       = (($r9['is_read_inapp'] == 0) ? '1' : '0');
                    $fb_last[$r9['parent_id']]['lcmt_unread_ems']       = (($r9['is_read_inems'] == 0) ? '1' : '0');
                }


                $key = 0;
                $runStudent = '####';
                foreach($rs4 as $_ind => $r4) {
                    if ($runStudent != $r4['student_id']) $key = 0;
                    $feedback = array();
                    $feedback['number'] = $r4['case_number'];
                    $feedback['feedback_id'] = $r4['primaryid'];
                    $feedback['subject'] = $r4['subject'];
                    $feedback['class_id'] = $r4['class_id'];
                    $feedback['class_name'] = $r4['class_name'];
                    $feedback['type'] = $r4['type'];
                    $feedback['case_status'] = $r4['case_status'];
                    $feedback['date_entered'] = $timedate->to_display_date_time($r4['date_entered']);
                    $feedback['date_entered_db'] = date('Y-m-d H:i:s', strtotime("+7 hours ".$r4['date_entered']));
                    $feedback['feedback_content'] = $r4['feedback_content'];
                    $feedback['assigned_to_id'] = $r4['assigned_to_id'];
                    $feedback['assigned_to'] = $r4['assigned_to'];
                    $feedback['create_by'] = $r4['create_by'];
                    $feedback['create_by_name'] = $r4['create_by_name'];
                    $feedback['created_by_avatar'] = $r4['created_by_avatar'];
                    $feedback['center_id'] = $r4['center_id'];
                    $feedback['center'] = $r4['center'];
                    $feedback['rate'] = $r4['rate'];
                    $feedback['last_comment']           = $fb_last[$r4['primaryid']]['lcmt_content'];
                    $feedback['last_comment_date']      = $fb_last[$r4['primaryid']]['lcmt_datetime'];
                    $feedback['last_comment_depositor'] = $fb_last[$r4['primaryid']]['lcmt_created_by_name'];
                    $feedback['count_unread_comment']   = $fb_last[$r4['primaryid']]['lcmt_unread_app'];
                    $feedback['lcmt_created_by_name']   = $fb_last[$r4['primaryid']]['lcmt_created_by_name'];
                    $feedback['lcmt_created_by']        = $fb_last[$r4['primaryid']]['lcmt_created_by'];
                    $student_list[$r4['student_id']]['feedback'][$key] = $feedback;
                    $runStudent = $r4['student_id'];
                    $key++;
                }
            }

            //Get Loyalty
            $q6 = "SELECT DISTINCT
            IFNULL(l1.id, '') student_id,
            IFNULL(l2.id,'') payment_id,
            IFNULL(l3.id,'') receipt_id,
            IFNULL(l2.name,'') payment_code,
            IFNULL(j_loyalty.id, '') primaryid,
            j_loyalty.input_date date,
            j_loyalty.exp_date exp_date,
            IFNULL(j_loyalty.type, '') type,
            j_loyalty.point point,
            j_loyalty.rate_in_out rate_in_out,
            IFNULL(j_loyalty.name, '') name,
            j_loyalty.description description
            FROM j_loyalty
            INNER JOIN contacts l1 ON j_loyalty.student_id = l1.id AND l1.deleted = 0
            LEFT JOIN  j_payment l2 ON j_loyalty.payment_id=l2.id AND l2.deleted=0
            LEFT JOIN  j_paymentdetail l3 ON l3.id =j_loyalty.paymentdetail_id AND l3.deleted=0
            WHERE
            (((l1.id IN ('$str_student_id'))))
            AND j_loyalty.deleted = 0
            ORDER BY student_id ASC, exp_date DESC";
            $rs = $GLOBALS['db']->query($q6);
            $key = 0;
            $runStudent = '####';
            while ($r = $GLOBALS['db']->fetchbyAssoc($rs)) {
                if ($runStudent != $r['student_id']) $key = 0;

                $loyalty = array();
                $loyalty['loyalty_id'] = $r['primaryid'];
                $loyalty['receipt_id'] = $r['receipt_id'];
                $loyalty['payment_id'] = $r['payment_id'];
                $loyalty['payment_code'] = $r['payment_code'];
                $loyalty['date'] = $timedate->to_display_date($r['date'], false);
                $loyalty['exp_date'] = $timedate->to_display_date($r['exp_date'], false);
                $loyalty['type'] = $r['type'];
                $loyalty['point'] = $r['point'];
                $loyalty['amount'] = format_number(abs($r['point'] * $r['rate_in_out']));
                $loyalty['name'] = $r['name'];
                $loyalty['description'] = $r['description'];

                $student_list[$r['student_id']]['loyalty_point'] += $r['point'];
                $student_list[$r['student_id']]['loyalty'][$key] = $loyalty;
                $runStudent = $r['student_id'];
                $key++;
            }

            //Get Notification - HOT FIX - Bỏ trong các bản cập sau - By HOàng Huy
            foreach($student_list as $student_id => $student){
                $q1 = "SELECT DISTINCT
                IFNULL(nt.id, '') primaryid,
                IFNULL(nt.assigned_user_id, '') student_id,
                IFNULL(nt.name, '') title,
                IFNULL(nt.is_read, 0) is_read,
                IFNULL(nt.description, '') body,
                IFNULL(nt.parent_id, '') record_id,
                IFNULL(nt.parent_type, '') module_name,
                IFNULL(nt.created_by, '') created_by,
                nt.date_entered date_entered,
                IFNULL(nt.parent_type, '') parent_type,
                IFNULL(nt.parent_id, '') parent_id
                FROM notifications nt
                WHERE nt.deleted = 0 AND nt.assigned_user_id = '{$student_id}' AND nt.parent_type <> 'C_News'
                ORDER BY nt.date_entered DESC
                LIMIT 0, 50";
                $rs1 = $GLOBALS['db']->query($q1);

                //Loop through all notifications and push it into $notification_list
                while ($r = $GLOBALS['db']->fetchByAssoc($rs1)) {
                    //Get notification All
                    $nt                 = array();
                    $nt['primaryid']    = $r['primaryid'];
                    $nt['title']        = $r['title'];
                    $nt['body']         = $r['body'];
                    $nt['is_read']      = boolVal($r['is_read']);
                    $nt['module_name']  = $r['module_name'];
                    $nt['record_id']    = $r['record_id'];
                    $nt['created_by']   = $r['created_by'];
                    $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));
                    $student_list[$student_id]['notifications'][] = $nt;
                }
            }
            //END: Get Notification - HOT FIX - BY Hoàng Huy

            //handle count notifications Unread - BY Hoàng Huy
            $res =  $this::countUnreadNoti($api,array('ids' => $studentIds));
            if($res['success']){
                foreach($student_list as $student_id => $student){
                    $student_list[$student_id]['unread_noti'] = intVal($res['unread_noti_list'][$student_id]);
                }
            }
            //END: - BY Hoàng Huy

            //Get New
            $q5 = "SELECT DISTINCT
            IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin, 0) pin,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            WHERE (c_news.status NOT IN ('Inactive'))
            AND (c_news.type LIKE '%^Student^%')
            AND c_news.deleted = 0
            ORDER BY date_entered DESC";
            $rs5 = $GLOBALS['db']->query($q5);
            $news = array();
            while ($r = $GLOBALS['db']->fetchbyAssoc($rs5)) {
                $qViewNews = "SELECT IFNULL(SUM(count_read_news) , 0)
                FROM c_news_contacts_1_c
                WHERE deleted = 0 AND c_news_contacts_1c_news_ida = '{$r['primaryid']}'";
                $views = $GLOBALS['db']->getOne($qViewNews);
                $news[$r['primaryid']] = array(
                    'id' => $r['primaryid'],
                    'name' => $r['name'],
                    'thumbnail' => $r['thumbnail'],
                    'url' => $r['url'],
                    'type' => $r['type'],
                    'pin' => $r['pin'],
                    'description' => $r['description'],
                    'type_news' => $r['type_news'],
                    'date_entered' => $timedate->to_display_date_time($r['date_entered']),
                    'end_date' => $timedate->to_display_date($r['end_date'], false),
                    'start_date' => $timedate->to_display_date($r['start_date'], false),
                    'views' => $views,
                    //Add by nnamnguyen
                    'db_date_entered' => $r['date_entered'],
                    'db_end_date' => $r['end_date'],
                    'db_start_date' => $r['start_date']
                );
            }

            $language_options = array('case_status_dom', 'case_sub_type_dom', 'status_class_list', 'attendance_type_list', 'do_homework_list', 'ss_status_list', 'relationship_list', 'address_city_2_options', 'address_state_2_options', 'gallery_type_list', 'reconcile_status_list'); //mảng chứa các options trong app_list_strings
            //Set App_String
            $app_strings = array(
                'en' => parseAppListString('en_us', $language_options),
                'vn' => parseAppListString('vn_vn', $language_options),
            );
            //Cập nhật thời gian mỗi lần gọi api login cho database (last access)
            $dtlastaccess = date('Y-m-d H:i:s', strtotime("-7 hours"));
            foreach ($arrStudentId as $key => $valueid) {
                $qr = "UPDATE contacts
                SET contacts.last_login = '$dtlastaccess' WHERE contacts.id = '$valueid'";
                $GLOBALS['db']->query($qr);
            }

            //Return
            return array(
                'success' => true,
                'portal_name' => $user_name,
                'password' => $password,
                'student_list' => array_values($student_list),
                'news_list' => array_values($news),
                'app_strings_en' => $app_strings['en'],
                'app_strings_vn' => $app_strings['vn'],
            );


        } else
            return array(
                'success' => false,
                'error' => "sending_failed."
            );

    }

    function editStudent(ServiceBase $api, $args){

        if (isset($args) && !empty($args)) {
            //Set current User
            $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
            if (empty($api_user)) $api_user = '1';
            $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

            global $timedate;
            $id = $args['id'];
            $contacts = BeanFactory::getBean('Contacts', $id);
            unset($args['id']);;
            $birthdate = $timedate->convertToDBDate($args['birthdate']);
            $gender = $args['gender'];
            $nick_name = $args['nick_name'];

            if (!empty($id)) {
                $contacts->birthdate = $birthdate;
                $contacts->gender = $gender;
                $contacts->nick_name = $nick_name;
                $contacts->save();
            }

            return array(
                'success' => true,
                'bean_id' => $contacts->id,
            );
        } else
            return array(
                'success' => false,
                'error' => "sending_failed."
            );
    }

    function editAddress(ServiceBase $api, $args){
        if (isset($args) && !empty($args)) {

            $id = $args['id'];
            $contacts = BeanFactory::getBean('Contacts', $id);
            unset($args['id']);

            $primary_address_street = $args['primary_address_street'];
            $primary_address_city = $args['primary_address_city'];
            $primary_address_state = $args['primary_address_state'];
            $primary_address_postalcode = $args['primary_address_postalcode'];

            if (!empty($id)) {
                $contacts->primary_address_street = $primary_address_street;
                $contacts->primary_address_city = $primary_address_city;
                $contacts->primary_address_state = $primary_address_state;
                $contacts->primary_address_postalcode = $primary_address_postalcode;
                $contacts->save();
            }

            return array(
                'success' => true,
                'bean_id' => $contacts->id,
            );
        } else
            return array(
                'success' => false,
                'error' => "sending_failed."
            );
    }

    function editParents(ServiceBase $api, $args){
        if (isset($args) && !empty($args)) {
            $id = $args['id'];
            $contacts = BeanFactory::getBean('Contacts', $id);
            unset($args['id']);

            $relationship = $args['relationship'];
            $guardian_name = $args['guardian_name'];
            $phone_guardian = $args['phone_guardian'];
            $relationship2 = $args['relationship2'];
            $guardian_name_2 = $args['guardian_name_2'];
            $phone_guardian_2 = $args['phone_guardian_2'];

            if (!empty($id)) {
                $contacts->guardian_name = $guardian_name;
                $contacts->relationship = $relationship;
                $contacts->phone_guardian = $phone_guardian;
                $contacts->guardian_name_2 = $guardian_name_2;
                $contacts->relationship2 = $relationship2;
                $contacts->other_mobile = $phone_guardian_2;
                $contacts->save();
            }

            return array(
                'success' => true,
                'bean_id' => $contacts->id,
            );
        } else
            return array(
                'success' => false,
                'error' => "sending_failed."
            );
    }

    function editOther(ServiceBase $api, $args){
        if (isset($args) && !empty($args)) {
            $id = $args['id'];
            $contacts = BeanFactory::getBean('Contacts', $id);
            unset($args['id']);

            $school_name_note = $args['school_name_note'];
            $education_level = $args['education_level'];
            $education_level_note = $args['education_level_note'];

            if (!empty($id)) {
                $contacts->school_name_note = $school_name_note;
                $contacts->education_level = $education_level;
                $contacts->education_level_note = $education_level_note;
                $contacts->save();
            }

            return array(
                'success' => true,
                'bean_id' => $contacts->id,
            );
        } else
            return array(
                'success' => false,
                'error' => "sending_failed."
            );
    }


    //BỎ trong bản cập nhật sau - HOT FIX by Hoàng Huy
    function listUnreadNotifications(ServiceBase $api, $args){
        $unread_noti_list = array();
        foreach($args['ids'] as $key => $student_id){
            $lated_date_view_noti = $GLOBALS['db']->getOne("SELECT lasted_view_noti FROM contacts WHERE id = '{$student_id}' AND deleted = 0");
            if(empty($lated_date_view_noti)) $lated_date_view_noti = '1970-01-01';
            $q7 = "SELECT COUNT(id) FROM notifications WHERE assigned_user_id = '{$student_id}' AND date_entered >= '{$lated_date_view_noti}' AND is_read = 0 AND deleted = 0";
            $unread_noti_list[$student_id] = intVal($GLOBALS['db']->getOne($q7));
        }
        return array(
            'success' => 1,
            'unread_notifications' => $unread_noti_list,
        );
    }
    //END:by Hoàng Huy

    //Get transfer and payment + VietQR Info:
    function getPaymentInfo(ServiceBase $api, array $args){
        $receipt = BeanFactory::getBean('J_PaymentDetail', $args['receipt_id'], array('disable_row_level_security'=>true));
        if($receipt->status == 'Unpaid') $vietQR = getVietQR($receipt);
        return array(
            'success'=>1,
            'id'=>$receipt->id,
            'payment_amount'=>$receipt->payment_amount,
            'status'=>$receipt->status,
            'payment_datetime'=>$receipt->payment_datetime,
            'payment_date'=>$receipt->payment_date,
            'payment_method'=>$receipt->payment_method,
            'bank_account'=>$receipt->bank_account,
            'audited_amount'=>$receipt->audited_amount,
            'reconcile_status'=>$receipt->reconcile_status,
            'vietQR'=>$vietQR,
        );
    }
    //Check payment status
    function requestPaymentCheck(ServiceBase $api, array $args){
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $receipt = BeanFactory::getBean('J_PaymentDetail', $args['receipt_id'], array('disable_row_level_security'=>true));
        //Xử lý realtime check
        if($receipt->status == 'Unpaid'){
            $receipt->reconcile_status = 'processing';
            $receipt->save();
        }
        return array(
            'success'=>1,
            'id'=>$receipt->id,
            'payment_amount'=>$receipt->payment_amount,
            'status'=>$receipt->status,
            'payment_datetime'=>$receipt->payment_datetime,
            'payment_date'=>$receipt->payment_date,
            'payment_method'=>$receipt->payment_method,
            'bank_account'=>$receipt->bank_account,
            'audited_amount'=>$receipt->audited_amount,
            'reconcile_status'=>$receipt->reconcile_status
        );
    }

    function getChildrenCenter($centerIds) {
        $childIds = array();
        $ids = implode(",", $centerIds);
        $q = "SELECT IFNULL(teams.id, '') primaryId
        FROM teams WHERE teams.parent_id IN ('$ids') AND teams.deleted = 0";
        $row = $GLOBALS['db']->query($q);

        while ($r = $GLOBALS['db']->fetchbyAssoc($row)) {
            $childIds[] = $r['primaryId'];
            $centerIds[] = $r['primaryId'];
        }
        if (!empty($childIds)) {
            $this->getChildrenCenter($childIds);
        }

        return $centerIds;
    }

    //List unread notification divide page - Hoang Huy
    function listNotifications(ServiceBase $api, $args){
        $type_noti = $args['type'];
        $channel_list = array(
            'news'       => array('C_News','C_Gallery'),
            'chat' => array('Contacts','Leads','Cases','C_Comments','Notifications'),
            'payment' => array('J_PaymentDetail','J_Payment'),
            'course' => array('C_Attendance', 'J_Class','J_Gradebook','J_GradebookDetail')
        );
        $ext = "";
        if($args['type'] != 'all' && !empty($args['type'])){
            if($type_noti == 'news'){
                $str_type = 'C_Gallery';
            } else {
                $str_type =implode("','", $channel_list[$type_noti]);
            }
            $ext = "AND nt.parent_type IN ('{$str_type}')";
        }
        //Get the page from the mobile app to set LIMIT
        $limit = (!empty($args['page'])) ? 30*$args['page'] : 30;
        $limit_bot = $limit - 30;
        $limit_top = $limit;
        $student_id = $args['student_id'];


        $args['update_time'] = empty($args['update_time']) ? "update" : $args['update_time'];
        //When click to the bell we will get the lasted view of students
        if($limit == 30 && $type_noti == 'all' && $args['update_time'] == "update"){ //First Page
            $time = date('Y-m-d H:i:s', strtotime($GLOBALS['timedate']->nowDb()));
            $GLOBALS['db']->query("UPDATE contacts SET lasted_view_noti = '{$time}' WHERE id = '{$student_id}' AND deleted = 0");
        }

        //Query to get the team of students
        $q1 = "SELECT IFNULL(team_id, '') team_id
        FROM contacts ct
        WHERE ct.id = '{$student_id}' AND ct.deleted = 0";
        $team_id = $GLOBALS['db']->getOne($q1);
        //Build query
        //Query notification
        $query2 = "SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        WHERE nt.deleted = 0
        AND nt.assigned_user_id = '{$student_id}'
        {$ext}
        ORDER BY nt.date_entered DESC
        LIMIT $limit_bot, $limit_top";
        //Query c_news
        $query3 = "SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        INNER JOIN team_sets_teams ts
        ON nt.team_set_id = ts.team_set_id
        WHERE (ts.team_id = ('{$team_id}') OR nt.team_set_id = 1) AND nt.parent_type = 'C_News' AND nt.assigned_user_id = 'all' AND nt.apps_type LIKE '%Student%' AND nt.deleted = 0
        ORDER BY nt.date_entered DESC
        LIMIT $limit_bot, $limit_top";

        $q4 = "SELECT DISTINCT
        IFNULL(c_news.id, '') news_id,
        IFNULL(l1.id, '') student_id,
        CASE WHEN IFNULL(l1.id, '') = '' THEN '0' ELSE 1 END is_read
        FROM c_news
        LEFT JOIN c_news_contacts_1_c l1_1 ON c_news.id = l1_1.c_news_contacts_1c_news_ida AND l1_1.deleted = 0
        LEFT JOIN contacts l1 ON l1.id = l1_1.c_news_contacts_1contacts_idb AND l1.deleted = 0
        WHERE l1.id = '{$student_id}' AND c_news.deleted = 0
        AND (c_news.type LIKE '%Student%')";

        if($type_noti == "all" || $type_noti == "news"){
            $q2 = "SELECT nttt.* FROM (($query2) UNION ($query3)) nttt ORDER BY date_entered DESC LIMIT $limit_bot, $limit_top";
            $row1 = $GLOBALS['db']->query($q2);
        } else {
            $row1 = $GLOBALS['db']->query($query2);
        }

        //List of news that the student read
        $read_news = array();
        if($type_noti == "all" || $type_noti == "news"){
            $row2 = $GLOBALS['db']->fetchArray($q4);
            foreach($row2 as $_key => $row) $read_news[$row['news_id']] = $row['is_read'];
        }
        //The target of make this read news list is to make sense that which is the notification about news that the student read

        //Loop through all notifications and push it into $notification_list
        $count_noti = 0;
        while ($r = $GLOBALS['db']->fetchByAssoc($row1)) {
            //get notification All
            $nt                 = array();
            $nt['primaryid']    = $r['primaryid'];
            $nt['title']        = $r['title'];
            $nt['body']         = $r['body'];
            $nt['is_read']      = ($r['parent_type'] == "C_News")? boolVal($read_news[$r['parent_id']]) : boolVal($r['is_read']);
            $nt['module_name']  = $r['module_name'];
            $nt['record_id']    = $r['record_id'];
            $nt['created_by']   = $r['created_by'];
            $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));
            $notification_list[] = $nt;
            $count_noti++;
        }
        $can_load = true;
        if($count_noti < 30 || empty($notification_list)){
            $can_load = false;
        }
        return array(
            'success' => 1,
            'notification_list' => $notification_list,
            'can_load' => $can_load
        );
    }

    //Count Unread Notification - Hoang Huy
    function countUnreadNoti(ServiceBase $api, $args){
        $unread_noti_list = array();
        foreach($args['ids'] as $key => $student_id){
            $lated_date_view_noti = $GLOBALS['db']->getOne("SELECT lasted_view_noti FROM contacts WHERE id = '{$student_id}' AND deleted = 0");
            if(empty($lated_date_view_noti)) $lated_date_view_noti = '1970-01-01';
            $q7 = "SELECT COUNT(id) FROM notifications WHERE assigned_user_id = '{$student_id}' AND date_entered >= '{$lated_date_view_noti}' AND is_read = 0 AND deleted = 0";
            $unread_noti_list[$student_id] = intVal($GLOBALS['db']->getOne($q7));
        }
        return array(
            'success' => 1,
            'unread_noti_list' => $unread_noti_list,
        );
    }

    //Update Notification Read/Unread/Delete  - Hoang Huy
    function updateNotification(ServiceBase $api, array $args){
        //Get noti beans
        $noti       = BeanFactory::getBean('Notifications', $args['noti_id']);
        $cnew_id    = $noti->parent_type == 'C_News' ? $noti->parent_id : "";
        $student_id = $args['student_id'];
        if($noti->parent_type == "C_News" && !empty($cnew_id))
        $new = BeanFactory::getBean('C_News', $cnew_id);
        switch ($args['action_type']) {
            case 'read':
                if ($noti->parent_type == "C_News" && !empty($cnew_id)){
                    if(!empty($new->id) && $new->load_relationship('c_news_contacts_1')){
                        //If the student haven't read the news that is the parent_id of notifications we will add the new relation
                        //On the other side we will count the view of new by 1
                        $new->c_news_contacts_1->add($student_id);
                        $GLOBALS['db']->query("UPDATE c_news SET addon_views = addon_views + 1 WHERE deleted = 0 AND id = '{$cnew_id}'");
                    }
                }elseif(!$noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'unread':
                if($noti->parent_type == "C_News" && !empty($cnew_id)){
                    //Incase unread we will need to delete the relationship existing
                    if(!empty($new->id) && $new->load_relationship('c_news_contacts_1'))
                        $new->c_news_contacts_1->delete($cnew_id, $student_id);
                }elseif($noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 0 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'delete':
                //the student only can delete the notification that relate to them and cant delete the notification about news
                if($noti->parent_type != 'C_News') $noti->mark_deleted($noti->id);
                break;
            case 'markallread':
                //Mark All Read Notification
                $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE assigned_user_id = '$student_id' AND is_read = 0 AND deleted=0");
                //Read all news
                //Step1: get all the news that the students have read
                $q1    = "SELECT IFNULL(c_news_contacts_1c_news_ida, '') news_id FROM c_news_contacts_1_c crl WHERE crl.deleted = 0 AND crl.c_news_contacts_1contacts_idb = '{$student_id}'";
                $row1  = $GLOBALS['db']->fetchArray($q1);

                //Step2: convert $read_news array to string to query get all the news that doesn't exist in our read list
                $str_read_news = (!empty($row1)) ? implode("','", array_column($row1, 'news_id')) : "";
                $q2            = "SELECT IFNULL(id, '') news_id FROM c_news WHERE deleted = 0 AND id NOT IN ('$str_read_news') AND type LIKE '%Student%'";
                $row2          = $GLOBALS['db']->fetchArray($q2);
                $date_modified = $GLOBALS['timedate']->nowDb(); //Get time at now
                foreach($row2 as $r2){
                    //Count the relationship that was deleted
                    $countRelationDeleted = $GLOBALS['db']->getOne("SELECT COUNT(id)
                        FROM c_news_contacts_1_c
                        WHERE c_news_contacts_1c_news_ida = '{$r2['news_id']}'
                        AND c_news_contacts_1contacts_idb = '{$student_id}' AND deleted = 1");
                    if ($countRelationDeleted > 0) {
                        //If exist the deleted realtionship we will update deleted field to 1 instead of create new record
                        $qUpdateRelation = "UPDATE c_news_contacts_1_c
                        SET deleted = 0, date_modified ='{$date_modified}'
                        WHERE deleted = 1
                        AND c_news_contacts_1c_news_ida = '{$r2['news_id']}'
                        AND c_news_contacts_1contacts_idb = '{$student_id}'";
                        $GLOBALS['db']->query($qUpdateRelation);
                    } else {
                        //If doesn't exist the deleted relationship we will add new relationship
                        $relation_id = create_guid();
                        $qInsertRelation = "INSERT INTO c_news_contacts_1_c (id, date_modified, deleted, c_news_contacts_1c_news_ida, c_news_contacts_1contacts_idb, count_read_news)
                        VALUES ('{$relation_id}','{$date_modified}', 0,'{$r2['news_id']}','{$args['student_id']}', 0)";
                        $GLOBALS['db']->query($qInsertRelation);
                    }
                }
                break;
        }
        return array(
            'success' => 1,
        );
    }
}
