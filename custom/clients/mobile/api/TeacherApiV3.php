<?php


class TeacherApiV3 extends DotbApi
{
    function registerApiRest()
    {
        return array(
            //Move from Mobile API
            'teacher_login_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'login'),
                'pathVars' => array(''),
                'method' => 'teacherLoginV3',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'edit_teacher_avt_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'edit-avt'),
                'pathVars' => array(''),
                'method' => 'editTeacherAvt',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'teacher_get_teaching_hour_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-teaching-hour'),
                'pathVars' => array(''),
                'method' => 'getTeachingHour',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'teacher_get_learning_hour_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-learning-hour'),
                'pathVars' => array(''),
                'method' => 'getLearningHour',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //End move from Mobile API
            //New API V3 - Ho Hoang Huy
            'list_notifications_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'list-notifications'),
                'pathVars' => array(''),
                'method' => 'listNotifications',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'count_unread_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'count-unread-noti'),
                'pathVars' => array(''),
                'method' => 'countUnreadNoti',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'update_notification_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'update-notification'),
                'pathVars' => array(''),
                'method' => 'updateNotification',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'save_gallery_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'handle-gallery'),
                'pathVars' => array(''),
                'method' => 'handleGallery',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_gallery_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-gallery-list'),
                'pathVars' => array(''),
                'method' => 'getGalleryList',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'check_listParent_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'list-parent'),
                'pathVars' => array(),
                'method' => 'listParent',
                'noLoginRequired' => true,
            ),
            'check_listStudent_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'list-student'),
                'pathVars' => array(),
                'method' => 'listStudent',
                'noLoginRequired' => true,
            ),
            'get_list_attendance_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-list-attendance'),
                'pathVars' => array(''),
                'method' => 'getListAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'save_attendance_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'save-attendance'),
                'pathVars' => array(''),
                'method' => 'saveAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'save_list_attendance_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'save-list-attendance'),
                'pathVars' => array(''),
                'method' => 'saveListAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'send_attendance_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'send-attendance'),
                'pathVars' => array(''),
                'method' => 'sendAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //API for comment and like
            'create_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'create-comment'),
                'pathVars' => array(''),
                'method' => 'createComment',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'delete_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'delete-comment'),
                'pathVars' => array(''),
                'method' => 'deleteComment',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-comment-list'),
                'pathVars' => array(''),
                'method' => 'getCommentList',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'like_action_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'like'),
                'pathVars' => array(''),
                'method' => 'likeAction',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'unlike_action_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'unlike'),
                'pathVars' => array(''),
                'method' => 'unlikeAction',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'add_view_new_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'update-news-view'),
                'pathVars' => array(''),
                'method' => 'addNewsView',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_gallery_by_id_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-gallery-by-id'),
                'pathVars' => array(''),
                'method' => 'getGalleryById',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'update_custom_syllabus_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'update-custom-syllabus'),
                'pathVars' => array(''),
                'method' => 'updateCustomSyllabus',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_unapproved_gallery_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-unapproved-gallery'),
                'pathVars' => array(''),
                'method' => 'getUnapprovedGallery',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_new_by_id_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'teacher', 'get-news-by-id'),
                'pathVars' => array(''),
                'method' => 'getNewsById',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }

    function getGalleryDetail($classIds, $teacher_id = '', $type = '', $page = 1, $filter)
    {
        global $current_user;
        $current_user = new User();
        $current_user->retrieve_by_string_fields(array('teacher_id' => $teacher_id));

        $limit = (!empty($page)) ? 20 * $page : 20;
        $limit_bot = $limit - 20;
        $limit_top = $limit + 1;

        if (!empty($teacher_id) && $type == 'owner') {
            $class_where_clause = '';
            $center_where_clause = '';
            if (!empty($filter['class_id'])) {
                $class_where_clause = "AND class.id = '{$filter['class_id']}'";
            } else if (!empty($filter['team_id'])) {
                $center_where_clause = "AND team.id = '{$filter['team_id']}'";
            }

            $sqlAlbum = "SELECT DISTINCT
                IFNULL(gallery.id, '') gallery_id,
                IFNULL(class.id, '') class_id,
                IFNULL(class.name, '') class_name,
                IFNULL(team.id, '') team_id,
                IFNULL(team.name, '') team_name,
                IFNULL(gallery.name, '') gallery_name,
                IFNULL(gallery.gallery_date, '') gallery_date,
                IFNULL(gallery.date_modified, '') date_modified,
                IFNULL(gallery.visibility_range, '') visibility_range,
                IFNULL(gallery.description, '') gallery_description,
                IFNULL(gallery.assigned_user_id, '') created_by,
                IFNULL(gallery.gallery_type, '') gallery_type,
                IFNULL(gallery.date_entered, '') date_entered,
                IFNULL(creator.picture, '') user_avatar,
                IFNULL(teacher.picture, '') teacher_avatar,
                IFNULL(creator.full_user_name, '') full_user_name
            FROM c_teachers teacher
                 INNER JOIN team_sets_teams team_set ON teacher.team_set_id = team_set.team_set_id
                 RIGHT JOIN c_gallery gallery ON team_set.team_id = gallery.team_id
                 LEFT JOIN teams team ON team.id = gallery.team_id AND team.deleted = 0
                 LEFT JOIN j_class_c_gallery_1_c class_gallery ON gallery.id = class_gallery.j_class_c_gallery_1c_gallery_idb AND class_gallery.deleted = 0
                 LEFT JOIN j_class class ON class.id = class_gallery.j_class_c_gallery_1j_class_ida AND gallery.deleted = 0
                 INNER JOIN users creator ON creator.id = gallery.created_by
            WHERE teacher.id = '$teacher_id' AND gallery.created_by = '$current_user->id' AND gallery.deleted = 0 AND gallery.status = 'approved'
            $class_where_clause
            $center_where_clause
            ORDER BY date_modified DESC";
        } else if ($type == 'all'){
            $class_where_clause = '';
            $center_where_clause = '';
            if (!empty($filter['class_id'])) {
                $class_where_clause = "AND class.id = '{$filter['class_id']}'";
                $center_where_clause = 'AND 1 = 0'; // To avoid getting center gallery
            } else if (!empty($filter['team_id'])) {
                $center_where_clause = "AND team.id = '{$filter['team_id']}'";
                $class_where_clause = 'AND 1 = 0'; // To avoid getting class gallery
            }
            $sqlAlbumClass = "SELECT DISTINCT
            IFNULL(gallery.id, '') gallery_id,
            IFNULL(class.id, '') class_id,
            IFNULL(class.name, '') class_name,
            '' team_name,
            '' team_id,
            IFNULL(gallery.name, '') gallery_name,
            IFNULL(gallery.gallery_date, '') gallery_date,
            IFNULL(gallery.description, '') gallery_description,
            IFNULL(gallery.visibility_range, '') visibility_range,
            IFNULL(gallery.assigned_user_id, '') created_by,
            IFNULL(gallery.gallery_type, '') gallery_type,
            IFNULL(gallery.date_entered, '') date_entered,
            IFNULL(gallery.date_modified, '') date_modified,
            IFNULL(creator.picture, '') user_avatar,
            IFNULL(teacher.picture, '') teacher_avatar,
            IFNULL(creator.full_user_name, '') full_user_name
            FROM c_gallery gallery
                 INNER JOIN j_class_c_gallery_1_c class_gallery ON gallery.id = class_gallery.j_class_c_gallery_1c_gallery_idb AND class_gallery.deleted = 0
                 INNER JOIN j_class class ON class.id = class_gallery.j_class_c_gallery_1j_class_ida AND class.deleted = 0
                 INNER JOIN users creator ON creator.id = gallery.created_by
                 LEFT JOIN c_teachers teacher ON teacher.id = creator.teacher_id
            WHERE class.id IN ('$classIds')
                AND gallery.visibility_range = 'class'
                AND gallery.deleted = 0
                AND gallery.status = 'approved'
                {$class_where_clause}";

            $sqlAlbumCenter = "SELECT DISTINCT
                IFNULL(gallery.id, '') AS gallery_id,
                '' AS class_id,
                '' AS class_name,
                IFNULL(team.name, '') AS team_name,
                IFNULL(team.id, '') AS team_id,
                IFNULL(gallery.name, '') AS gallery_name,
                IFNULL(gallery.gallery_date, '') AS gallery_date,
                IFNULL(gallery.description, '') AS gallery_description,
                IFNULL(gallery.visibility_range, '') AS visibility_range,
                IFNULL(gallery.assigned_user_id, '') AS created_by,
                IFNULL(gallery.gallery_type, '') AS gallery_type,
                IFNULL(gallery.date_entered, '') AS date_entered,
                IFNULL(gallery.date_modified, '') AS date_modified,
                IFNULL(creator.picture, '') AS user_avatar,
                IFNULL(teacher.picture, '') AS teacher_avatar,
                IFNULL(creator.full_user_name, '') AS full_user_name
            FROM c_teachers teacher
                INNER JOIN team_sets_teams team_set ON teacher.team_set_id = team_set.team_set_id
                INNER JOIN c_gallery gallery ON team_set.team_id = gallery.team_id
                INNER JOIN users creator ON creator.id = gallery.created_by
                INNER JOIN teams team ON team.id = gallery.team_id AND team.deleted = 0
            WHERE teacher.id = '$teacher_id'
                AND gallery.visibility_range = 'center'
                AND gallery.deleted = 0
                AND gallery.status = 'approved'
                {$center_where_clause}";
            $sqlAlbum = "$sqlAlbumCenter UNION $sqlAlbumClass ORDER BY date_modified DESC LIMIT $limit_bot, $limit_top";
        }
        $albums = $GLOBALS['db']->query($sqlAlbum);
        $gls = array();
        $count_gallery = 0;
        $can_load = false;
        while ($album = $GLOBALS['db']->fetchbyAssoc($albums)) {
            $count_gallery++;
            if($count_gallery == $limit_top && $type == 'all') {
                $can_load = true;
                break;
            }
            $formatted_img_list = $this->getNote($album['gallery_id'], 'C_Gallery');
            $gls[] = array(
                'gallery_id' => $album['gallery_id'],
                'class_id' => $album['class_id'],
                'class_name' => $album['class_name'],
                'team_id' => $album['team_id'],
                'team_name' => $album['team_name'],
                'user_avt' => empty($album['user_avatar']) ? $album['teacher_avatar'] : $album['user_avatar'],
                'created_by' => $album['created_by'],
                'visibility_range' => $album['visibility_range'],
                'full_user_name' => $album['full_user_name'],
                'gallery_name' => $album['gallery_name'],
                'gallery_description' => $album['gallery_description'],
                'gallery_type' => $album['gallery_type'],
                'total_like' => $this->countLikes($album['gallery_id'], 'C_Gallery'),
                'total_comment' => $this->countComments($album['gallery_id'], 'C_Gallery'),
                'gallery_date' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['gallery_date'])),
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered'])),
                'date_modified' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_modified'])),
                'image_list' => $formatted_img_list,
                'is_liked' => $this->isLiked($album['gallery_id'], 'C_Gallery', $teacher_id)
            );
        }
        return array(
            'gallery_list' => $gls,
            'can_load' => $can_load,
        );
    }
    function getCommentList(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['parent_id', 'parent_type', 'page']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $limit = (!empty($args['page'])) ? 20 * $args['page'] : 20;
        $limit_bot = $limit - 20;
        $limit_top = $limit + 1;
        $parent_type = $args['parent_type'];
        $parent_id = $args['parent_id'];
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE teacher_id = '{$args['teacher_id']}'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $r = $GLOBALS['db']->query("SELECT DISTINCT
                IFNULL(comment.id, '') primaryid,
                IFNULL(comment.description, '') description,
                comment.date_entered date_entered,
                IFNULL(student.id, '') student_id,
                IFNULL(student.full_student_name, '') full_student_name,
                IFNULL(student.picture, '') student_avatar,
                IFNULL(teacher.id, '') teacher_id,
                IFNULL(teacher.picture, '') teacher_avatar,
                IFNULL(teacher.full_teacher_name, '') full_teacher_name
                FROM c_comments comment
                LEFT JOIN contacts student ON comment.student_id = student.id AND student.deleted = 0
                LEFT JOIN c_teachers teacher ON comment.created_by = teacher.user_id AND teacher.deleted = 0
                WHERE comment.parent_id = '{$parent_id}' AND comment.parent_type = '{$parent_type}' AND comment.deleted = 0
                ORDER BY comment.date_entered DESC
                LIMIT $limit_bot, $limit_top");
        $count_comment = 0;
        $can_load = false;
        while ($row = $GLOBALS['db']->fetchByAssoc($r)) {
            $count_comment++;
            if($count_comment == $limit_top){
                $can_load = true;
                break;
            }
            if(empty($row['student_id'])){
                $chatter_name = $row['full_teacher_name'];
                $picture = $GLOBALS['dotb_config']['site_url'] . '/upload/' .$row['teacher_avatar'];
                $chatter_id = $row['teacher_id'];
            } else {
                $chatter_name = $row['full_student_name'];
                $picture = $GLOBALS['dotb_config']['site_url'] . '/upload/origin/' .$row['student_avatar'];
                $chatter_id = $row['student_id'];
            }
            $data[] = array(
                'id' => $row['primaryid'],
                'content' => $row['description'],
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $row['date_entered'])),
                'created_by_name' => $chatter_name,
                'created_by_avatar' => $picture,
                'created_by_id' => $chatter_id
            );
        }
        return [
            'success' => true,
            'data' => [
                'comment_list' => $data,
                'can_load' => $can_load,
                'page' => (int)$args['page']
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function isLiked($parent_id, $parent_type, $teacher_id){
        $relationship_table_map = [
            'C_Gallery' => 'c_gallery_c_teachers_1',
        ];
        $table = $relationship_table_map[$parent_type] . '_c';
        $fieldA = $relationship_table_map[$parent_type] . strtolower($parent_type) . '_ida';
        $fieldB = $relationship_table_map[$parent_type] . 'c_teachers_idb';
        $sqlCountLike = "SELECT COUNT(*)
            FROM $table
            WHERE deleted = 0 AND $fieldA = '{$parent_id}' AND $fieldB = '{$teacher_id}'";
        return $GLOBALS['db']->getOne($sqlCountLike) == 1;
    }
    function countLikes($parent_id, $parent_type)
    {
        $relationship_table_map = [
            'C_Gallery' => 'c_gallery_contacts_1',
            'C_News' => 'c_news_contacts_2'
        ];
        $table = $relationship_table_map[$parent_type] . '_c';
        $field = $relationship_table_map[$parent_type] . strtolower($parent_type) . '_ida';
        $sqlCountLike1 = "SELECT COUNT(*)
        FROM $table
        WHERE deleted = 0 AND $field = '{$parent_id}'";
        $studentLikeAmount =  $GLOBALS['db']->getOne($sqlCountLike1);

        $relationship_table_map = [
            'C_Gallery' => 'c_gallery_c_teachers_1',
        ];
        $table = $relationship_table_map[$parent_type] . '_c';
        $field = $relationship_table_map[$parent_type] . strtolower($parent_type) . '_ida';
        $sqlCountLike2 = "SELECT COUNT(*)
        FROM $table
        WHERE deleted = 0 AND $field = '{$parent_id}'";
        $teacherLikeAmount =  $GLOBALS['db']->getOne($sqlCountLike2);

        return $teacherLikeAmount + $studentLikeAmount;
    }
    function countComments($parentId, $parentType){
        $sqlCountComment = "SELECT COUNT(*)
                FROM c_comments
                WHERE parent_id = '{$parentId}'
                  AND parent_type = '{$parentType}'
                  AND deleted = 0
                  ORDER BY c_comments.date_entered DESC";
        $result = $GLOBALS['db']->getOne($sqlCountComment);
        return $result;
    }
    function teacherLoginV3(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['portal_name', 'password']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        global $timedate, $current_user, $app_list_strings;

        $now = $timedate->nowDb();
        $user_name = $args['portal_name'];
        $password = $args['password'];
        $res = array();

        //Check Username credential
        $q1 = "SELECT DISTINCT
            IFNULL(c_teachers.id, '') primaryid,
            IFNULL(c_teachers.picture, '') picture,
            IFNULL(c_teachers.cover_app, '') cover_app,
            IFNULL(c_teachers.type, '') teacher_type,
            IFNULL(c_teachers.teacher_id, '') teacher_id,
            IFNULL(c_teachers.full_teacher_name, '') name,
            IFNULL(c_teachers.first_name, '') first_name,
            IFNULL(c_teachers.last_name, '') last_name,
            IFNULL(c_teachers.title, '') title,
            IFNULL(c_teachers.gender, '') gender,
            IFNULL(c_teachers.status, '') status,
            IFNULL(c_teachers.team_set_id, '') team_set_id,
            IFNULL(l1_2.email_address, '') email,
            c_teachers.dob dob,
            IFNULL(c_teachers.phone_mobile, '') phone_mobile,
            IFNULL(l1.id, '') user_id,
            IFNULL(l1.user_name, '') user_name,
            IFNULL(l1.user_hash, '') user_hash,
            IFNULL(l2.id, '') assigned_to_user_id,
            IFNULL(l2.full_user_name, '') assigned_to_user,
            IFNULL(l3.id, '') center_id,
            IFNULL(l3.name, '') center_name,
            IFNULL(l3.phone_number, '') phone
            FROM c_teachers
            LEFT JOIN email_addr_bean_rel l1_1 ON c_teachers.id = l1_1.bean_id
            AND l1_1.deleted = 0
            AND l1_1.bean_module = 'C_Teachers'
            AND l1_1.primary_address = 1
            LEFT JOIN email_addresses l1_2 ON l1_2.id = l1_1.email_address_id
            AND l1_2.deleted = 0
            INNER JOIN users l1 ON c_teachers.user_id = l1.id
            AND l1.deleted = 0
            INNER JOIN users l2 ON c_teachers.assigned_user_id = l2.id
            AND l2.deleted = 0
            LEFT JOIN teams l3 ON c_teachers.team_id = l3.id
            AND l3.deleted = 0
            WHERE (((l1.user_name = '$user_name')
            AND (IFNULL(l1.status, '') = 'Active')))
            AND c_teachers.deleted = 0";

        $teacher = $GLOBALS['db']->fetchOne($q1);

        if (empty($teacher))
            return array(
                'success' => false,
                'error_info' => throwApiError(100),
            );

        //Check Password credential
        $pwdCheck = false;

        if ((User::checkPassword($password, $teacher['user_hash']) || md5($password) == $teacher['user_hash']) && !$pwdCheck)
            $pwdCheck = true;
        //add student array
        $teacher['birthdate'] = $timedate->to_display_date($teacher['birthdate'], false);//format birthdate
        $teacher['class_list'] = array();
        $teacher['unread_noti'] = 0;
        $teacher['notifications'] = array();


        if (!$pwdCheck)
            return array(
                'success' => false,
                'error_info' => throwApiError(101),
            );

        $str_teacher_id = $teacher['primaryid'];
        $str_user_id =$teacher['user_id'];
        //Get Current Class
        $nowDb = $timedate->nowDb();
        $q2 = "SELECT DISTINCT
            IFNULL(l2.id, '') class_id,
            IFNULL(l2.picture, '') picture,
            IFNULL(l2.name, '') class_name,
            IFNULL(l2.class_code, '') class_code,
            IFNULL(l2.main_schedule, '') main_schedule,
            l2.start_date start_date,
            l2.end_date end_date,
            l2.date_entered date_entered,
            IFNULL(l2.kind_of_course, '') kind_of_course,
            IFNULL(l2.level, '') level,
            IFNULL(l2.max_size, '') max_size,
            IFNULL(l2.hours, 0) hours,
            IFNULL(l2.status, '') status,
            IFNULL(l3.id, '') koc_id,
            IFNULL(l3.name, '') koc_name,
            IFNULL(l4.id, '') center_id,
            IFNULL(l4.name, '') center_name,
            IFNULL(l4.code_prefix, '') center_code,
            IFNULL(l8.name, '') room_name,
            IFNULL(l1.full_teacher_name, '') teacher_name,
            IFNULL(meetings.id, '') ss_id,
            IFNULL(meetings.name, '') ss_name,
            IFNULL(meetings.type, '') ss_learning_type,
            (CASE
            WHEN DATE_SUB(meetings.date_end, INTERVAL 1 HOUR) < '$nowDb' THEN ''
            ELSE IFNULL(meetings.join_url, '')
            END) ss_displayed_url,
            IFNULL(meetings.lesson_number, '') ss_lesson,
            IFNULL(meetings.week_no, '') ss_week_no,
            meetings.till_hour ss_till_hour,
            meetings.date_start ss_date_start,
            meetings.date_end ss_date_end,
            IFNULL(l9_2.id, '') ss_teacher_id,
            IFNULL(l9_2.full_teacher_name, '') ss_teacher_name,
            IFNULL(l9_1.full_teacher_name, '') ss_ta_name,
            IFNULL(l9_1.id, '') ss_ta_id,
            IFNULL(l9_3.full_teacher_name, '') ss_ta_2_name,
            IFNULL(l9_3.id, '') ss_ta_2_id,
            IFNULL(l6.id, '') ss_room_id,
            IFNULL(l6.name, '') ss_room_name,
            IFNULL(meetings.observe_note, '') ss_observe_note,
            IFNULL(meetings.syllabus_custom, '') ss_syllabus_custom,
            IFNULL(meetings.topic_custom, '') ss_topic_custom,
            IFNULL(meetings.objective_custom, '') ss_note_for_teacher_custom,
            IFNULL(meetings.syllabus_id, '') ss_syllabus_id,
            IFNULL(l10.name, '') ss_syllabus_topic,
            IFNULL(l10.description, '') ss_syllabus_content,
            IFNULL(l10.homework, '') ss_syllabus_homework,
            IFNULL(l10.note_for_teacher, '') ss_syllabus_note_for_teacher,
            meetings.duration_cal ss_duration_cal,
            meetings.observe_score ss_observe_score,
            IFNULL(l7.id,'') assigned_user_id,
            IFNULL(l7.full_user_name,'') assigned_to,
            IFNULL(meetings.total_attended,0) total_attended,
            IFNULL(meetings.total_absent,0) total_absent,
            IFNULL(meetings.total_student,0) total_student
            FROM meetings
            INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN c_teachers l1 ON  l1.id IN (meetings.teacher_id, meetings.teacher_cover_id, meetings.sub_teacher_id, l2.teacher_id)  AND l1.deleted = 0
            INNER JOIN j_kindofcourse l3 ON l2.koc_id = l3.id AND l3.deleted = 0
            INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
            LEFT JOIN c_rooms l6 ON meetings.room_id = l6.id AND l6.deleted = 0
            LEFT JOIN users l7 ON l2.assigned_user_id=l7.id AND l7.deleted=0
            LEFT JOIN c_rooms l8 ON l2.room_id=l8.id AND l8.deleted=0
            LEFT JOIN c_teachers l9_1 ON meetings.teacher_cover_id = l9_1.id AND l9_1.deleted = 0
            LEFT JOIN c_teachers l9_2 ON meetings.teacher_id = l9_2.id AND l9_2.deleted = 0
            LEFT JOIN c_teachers l9_3 ON meetings.sub_teacher_id = l9_3.id AND l9_3.deleted = 0
            LEFT JOIN j_syllabus l10 ON meetings.syllabus_id = l10.id AND l10.deleted = 0
            WHERE (((l1.id = '$str_teacher_id' )
            AND (IFNULL(meetings.session_status, '') <> 'Cancelled'
            OR (IFNULL(meetings.session_status, '') IS NULL
            AND 'Cancelled' IS NOT NULL))
            AND (IFNULL(meetings.meeting_type, '') = 'Session')))
            AND meetings.deleted = 0
            GROUP BY meetings.id
            ORDER BY status DESC,class_id ASC, meetings.date_start ASC";
        $rowS = $GLOBALS['db']->fetchArray($q2);
        $runClass = '####';
        $classes = array();

        //Get Kind Of Course Syllabus - For best perfomance
        $classIds = implode("','", array_unique(array_column($rowS, 'class_id')));

        $koc = array();
        $q2_1 = "SELECT DISTINCT
            IFNULL(l1.id, '') koc_id,
            IFNULL(l1.name, '') koc_name,
            IFNULL(l1.kind_of_course, '') kind_of_course,
            IFNULL(l1.content, '') content,
            IFNULL(l1.lms_content, '') lms_content
            FROM j_class INNER JOIN j_kindofcourse l1 ON j_class.koc_id = l1.id  AND l1.deleted = 0
            WHERE (j_class.id IN ('$classIds')) AND j_class.deleted = 0";
        $rowK = $GLOBALS['db']->fetchArray($q2_1);
        foreach ($rowK as $key => $rK) {
            $koc[$rK['koc_id']]['koc_id'] = $rK['koc_id'];
            $koc[$rK['koc_id']]['koc_name'] = $rK['koc_name'];
            $koc[$rK['koc_id']]['kind_of_course'] = $rK['kind_of_course'];
            $koc[$rK['koc_id']]['content'] = ((!empty($rK['content'])) ? json_decode(html_entity_decode($rK['content']), true) : '');
        }

        //List Class ID
        $classIds = array();

        foreach ($rowS as $_key => $r2) {
            if ($runClass != $r2['class_id']) {
                $runClass = $r2['class_id'];
                $classes[$runClass]['id'] = $r2['class_id'];
                $classes[$runClass]['picture'] = $r2['picture'];
                $classes[$runClass]['class_name'] = $r2['class_name'];
                $classes[$runClass]['class_code'] = $r2['class_code'];

                $schedule = json_decode(html_entity_decode($r2['main_schedule']), true);
                $sche = array();
                foreach ($schedule as $key => $value) {
                    $detail = array();
                    foreach ($value as $k => $val) {
                        $detail[$k]['day'] = $key;
                        $detail[$k]['start_time'] = $val['start_time'];
                        $detail[$k]['end_time'] = $val['end_time'];
                        $detail[$k]['duration_hour'] = $val['duration_hour'];
                        $detail[$k]['revenue_hour'] = $val['revenue_hour'];
                        $detail[$k]['teaching_hour'] = $val['teaching_hour'];
                    }
                    $sche = $detail;
                }
                $classes[$runClass]['main_schedule'] = $sche;

                $classes[$runClass]['start_date'] = $timedate->to_display_date($r2['start_date'], false);
                $classes[$runClass]['end_date'] = $timedate->to_display_date($r2['end_date'], false);
                $classes[$runClass]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['date_entered']));
                $classes[$runClass]['kind_of_course'] = $r2['kind_of_course'];
                $classes[$runClass]['level'] = $r2['level'];
                $classes[$runClass]['hours'] = $r2['hours'];
                $classes[$runClass]['assigned_to'] = $r2['assigned_to'];
                $classes[$runClass]['koc_id'] = $r2['koc_id'];
                $classes[$runClass]['koc_name'] = $r2['koc_name'];
                $classes[$runClass]['room_name'] = $r2['room_name'];
                $classes[$runClass]['teacher_name'] = $r2['teacher_name'];
                $classes[$runClass]['max_size'] = $r2['max_size'];
                $classes[$runClass]['team_name'] = $r2['center_name'];
                $classes[$runClass]['team_code'] = $r2['center_code'];
                $classes[$runClass]['status'] = $r2['status'];


                $content = $koc[$r2['koc_id']]['content'];
                foreach ($content as $ct) {
                    if ($ct['levels'] == $r2['level'])
                        $docs = $ct['doc_url'];
                }
                $classes[$runClass]['doc_url'] = $docs;

                $classes[$runClass]['sessions'] = array();
                $classes[$runClass]['albums'] = array();
                $classes[$runClass]['feedback'] = array();
                $classes[$runClass]['news'] = array();

            }
            $session = array();
            $session['ss_id'] = $r2['ss_id'];
            $session['ss_name'] = $r2['ss_name'];
            $session['ss_lesson'] = $r2['ss_lesson'];
            $session['ss_week_no'] = $r2['ss_week_no'];
            $session['ss_week_date'] = date('l', strtotime("+7 hours " . $r2['ss_date_start']));
            $session['ss_till_hour'] = $r2['ss_till_hour'];
            $session['ss_date_start'] = $timedate->to_display_date_time($r2['ss_date_start']);
            $session['ss_date_end'] = $timedate->to_display_date_time($r2['ss_date_end']);
            $session['ss_db_date_start'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_start']));
            $session['ss_db_date_end'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_end']));
            $session['ss_db_date'] = date('Y-m-d', strtotime("+7 hours " . $r2['ss_date_start']));

            if ($now < $r2['ss_date_start']) $session['ss_status'] = "not_stated";
            elseif ($now >= $r2['ss_date_start'] && $now <= $r2['ss_date_end']) $session['ss_status'] = "in_progress";
            elseif ($now > $r2['ss_date_end']) $session['ss_status'] = "finished";

            $session['ss_duration_cal'] = $r2['ss_duration_cal'];
            $session['ss_teacher_name'] = $r2['ss_teacher_name'];
            $session['ss_teacher_id'] = $r2['ss_teacher_id'];
            $session['ss_ta_name'] = $r2['ss_ta_name'];
            $session['ss_ta_id'] = $r2['ss_ta_id'];
            $session['ss_ta_2_name'] = $r2['ss_ta_2_name'];
            $session['ss_ta_2_id'] = $r2['ss_ta_2_id'];
            $session['ss_room_name'] = $r2['ss_room_name'];
            $session['ss_observe_score'] = $r2['ss_observe_score'];
            $session['ss_syllabus_custom'] = $r2['ss_syllabus_custom'];
            $session['ss_topic_custom'] = $r2['ss_topic_custom'];
            $session['ss_note_for_teacher_custom'] = $r2['ss_note_for_teacher_custom'];
            $session['ss_learning_type'] = $r2['ss_learning_type'];
            $session['ss_displayed_url'] = $r2['ss_displayed_url'];
            $session['total_student'] = $r2['total_student'];
            $session['total_attended'] = $r2['total_attended'];
            $session['total_absent'] = $r2['total_absent'];
            $session['syllabus_topic'] = $r2['ss_syllabus_topic'];
            $session['syllabus_activities'] = $r2['ss_syllabus_content'];
            $session['syllabus_homework'] = $r2['ss_syllabus_homework'];
            $session['syllabus_note_for_teacher'] = $r2['ss_syllabus_note_for_teacher'];

            $classes[$runClass]['sessions'][] = $session;

            if (!in_array($runClass, $classIds))
                $classIds[] = $runClass;
        }

        // Get total session of class
        $qSession = "SELECT DISTINCT
            IFNULL(c.id, '') class_id,
            COUNT(l1.id) total_session
            FROM j_class c
            INNER JOIN meetings l1 ON l1.ju_class_id = c.id AND l1.deleted = 0
            WHERE (c.id IN ('" . implode("','", $classIds) . "'))
            AND (IFNULL(l1.meeting_type, '') = 'Session')
            AND (IFNULL(l1.session_status, '') <> 'Cancelled' OR (IFNULL(l1.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
            GROUP BY c.id";
        $rSession = $GLOBALS['db']->query($qSession);
        while ($row = $GLOBALS['db']->fetchbyAssoc($rSession)) {
            $classes[$row['class_id']]['total_session'] = $row['total_session'];
        }
        //Get List Student
        $getListStudent = "SELECT p.* FROM (
            SELECT DISTINCT
            IFNULL(c.id, '') student_id,
            IFNULL(c.first_name, '') first_name,
            IFNULL(c.full_student_name, '') student_name,
            IFNULL(c.picture, '') picture,
            IFNULL(c.phone_mobile, '') student_phone,
            'Contacts' student_type,
            c.birthdate birthdate,
            IFNULL(c.nick_name, '') nick_name,
            c.guardian_name parent_name,
            c.relationship relationship,
            IFNULL(c.phone_guardian, '') parent_phone,
            c.guardian_name_2 parent_name_2,
            c.relationship2 relationship_2,
            IFNULL(c.other_mobile, '') parent_phone_2,
            '' type,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.status, '') class_status
            FROM j_classstudents
            INNER JOIN j_class l1 ON j_classstudents.class_id = l1.id AND l1.deleted = 0
            INNER JOIN contacts c ON j_classstudents.student_id = c.id AND c.deleted = 0 AND j_classstudents.student_type = 'Contacts'
            WHERE (l1.id IN('" . implode("','", $classIds) . "') ) AND j_classstudents.deleted = 0
            UNION ALL
            SELECT DISTINCT
            IFNULL(l.id, '') student_id,
            IFNULL(l.first_name, '') first_name,
            IFNULL(l.full_lead_name, '') student_name,
            IFNULL(l.picture, '') picture,
            IFNULL(l.phone_mobile, '') student_phone,
            'Leads' student_type,
            l.birthdate birthdate,
            IFNULL(l.nick_name, '') nick_name,
            l.guardian_name parent_name,
            l.relationship relationship,
            IFNULL(l.phone_guardian, '') parent_phone,
            l.guardian_name_2 parent_name_2,
            l.relationship2 relationship_2,
            IFNULL(l.other_mobile, '') parent_phone_2,
            '' type,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.status, '') class_status
            FROM j_classstudents
            INNER JOIN j_class l1 ON j_classstudents.class_id = l1.id AND l1.deleted = 0
            INNER JOIN leads l ON j_classstudents.student_id = l.id AND l.deleted = 0 AND j_classstudents.student_type = 'Leads'
            WHERE (l1.id IN('" . implode("','", $classIds) . "') ) AND j_classstudents.deleted = 0) p ORDER BY p.first_name ASC";

        $rGetListStudent = $GLOBALS['db']->query($getListStudent);

        while ($r = $GLOBALS['db']->fetchbyAssoc($rGetListStudent)) {
            $student = array();
            $student['student_id'] = $r['student_id'];
            $student['student_name'] = $r['student_name'];
            $student['picture'] = $r['picture'];
            $student['student_phone'] = $r['student_phone'];
            $student['student_type'] = $r['student_type'];
            $student['birthdate'] = $timedate->to_display_date($r['birthdate'], false);;
            $student['nick_name'] = $r['nick_name'];
            $student['parent_name'] = $r['parent_name'];
            $student['status_in_class'] = $r['class_status'];
            $student['type_in_class'] = $r['type'];
            $classes[$r['class_id']]['student'][] = $student;
        }

        //Get Feedback
        $q4 = "SELECT DISTINCT
            IFNULL(cases.id, '') primaryid,
            IFNULL(cases.name, '') subject,
            IFNULL(cases.status, '') status,
            IFNULL(cases.case_number, '') case_number,
            IFNULL(cases.type, '') type,
            IFNULL(cases.relate_feedback_list, '') sub_type,
            IFNULL(cases.priority, '') priority,
            cases.date_entered date_entered,
            cases.received_date received_date,
            IFNULL(cases.description, '') feedback_content,
            cases.resolved_date resolved_date,
            IFNULL(cases.resolution, '') response_to_customer,
            IFNULL(l2.id, '') student_id,
            IFNULL(l2.full_student_name, '') student_name,
            IFNULL(l2.contact_id, '') student_code,
            IFNULL(l2.phone_mobile, '') mobile,
            IFNULL(l2.guardian_name, '') parent,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.name, '') class_name,
            IFNULL(l4.full_user_name, '') assigned_to,
            IFNULL(l3.name,'') center,
            IFNULL(l1.id,'') center_id
            FROM cases
            INNER JOIN j_class_cases_1_c l1_1 ON cases.id = l1_1.j_class_cases_1cases_idb AND l1_1.deleted = 0
            INNER JOIN j_class l1 ON l1.id = l1_1.j_class_cases_1j_class_ida AND l1.deleted = 0
            INNER JOIN contacts_cases_1_c l2_1 ON cases.id = l2_1.contacts_cases_1cases_idb AND l2_1.deleted = 0
            INNER JOIN contacts l2 ON l2.id = l2_1.contacts_cases_1contacts_ida AND l2.deleted = 0
            INNER JOIN teams l3 ON cases.team_id=l3.id AND l3.deleted=0
            INNER JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
            LEFT JOIN users l5 ON cases.cso_id = l5.id AND l5.deleted = 0 AND (l5.id = '$str_user_id')
            WHERE (l1.id IN ('" . implode("','", $classIds) . "')) AND cases.deleted = 0
            ORDER BY class_id ASC,  date_entered DESC";
        $rs4 = $GLOBALS['db']->query($q4);

        while ($r4 = $GLOBALS['db']->fetchbyAssoc($rs4)) {
            $feedback = array();
            $feedback['number'] = $r4['case_number'];
            $feedback['feedback_id'] = $r4['primaryid'];
            $feedback['subject'] = $r4['subject'];
            $feedback['class_id'] = $r4['class_id'];
            $feedback['class_name'] = $r4['class_name'];
            $feedback['type'] = $r4['type'];
            $feedback['sub_type'] = $r4['sub_type'];
            $feedback['status'] = $r4['status'];
            $feedback['received_date'] = $timedate->to_display_date($r4['received_date'], false);
            $feedback['date_entered'] = $timedate->to_display_date_time($r4['date_entered']);
            $feedback['resolved_date'] = $timedate->to_display_date($r4['resolved_date'], false);
            $feedback['feedback_content'] = $r4['feedback_content'];
            $feedback['response_to_customer'] = $r4['response_to_customer'];
            $feedback['assigned_to'] = $r4['assigned_to'];
            $feedback['center_id'] = $r4['center_id'];
            $feedback['center'] = $r4['center'];
            $feedback['last_comment'] = $r4['last_comment'];
            $feedback['last_comment_date'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r4['last_comment_date']));
            $feedback['last_comment_depositor'] = $r4['last_comment_depositor'];
            $feedback['count_unread_comment'] = $r4['count_unread_comment'];
            $classes[$r4['class_id']]['feedback'][] = $feedback;
        }

        $teacher['class_list'] = array_values($classes);

        //Get New
        $nowDate = date('Y-m-d');
        $q5 = "SELECT DISTINCT
            IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin_teacher, 0) pin,
            IFNULL(c_news.addon_views, 0) addon_views,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.news_content, '') content,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            WHERE (c_news.end_date >= '$nowDate')
            AND (c_news.type LIKE '%^Teacher^%')
            AND c_news.deleted = 0
            ORDER BY date_entered DESC";
        $rs5 = $GLOBALS['db']->query($q5);
        $news = array();
        while ($r = $GLOBALS['db']->fetchbyAssoc($rs5)) {
            $srcImg =  strlen($r['thumbnail']) != 36 ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] .  $r['thumbnail'] : $GLOBALS['dotb_config']['site_url'] . '/download_attachment.php?id=s3_storage/' . $r['thumbnail'];
            $periodPosition = strrpos($r['thumbnail'], '.');
            if ($periodPosition === false) {
                $periodPosition = strlen($r['thumbnail']);
            }
            $startPosition = max(0, $periodPosition - 36);
            $thumbnail_id = substr($r['thumbnail'], $startPosition, $periodPosition - $startPosition);
            $news[$r['primaryid']] = array(
                'id' => $r['primaryid'],
                'name' => $r['name'],
                'thumbnail' => $thumbnail_id,
                'thumbnail_url' => $srcImg,
                'content' => $r['content'],
                'addon_views' => $r['addon_views'],
                'url' => $r['url'],
                'type' => $r['type'],
                'pin' => $r['pin'],
                'description' => $r['description'],
                'type_news' => $r['type_news'],
                'date_entered' => $timedate->to_display_date_time($r['date_entered']),
                'end_date' => $timedate->to_display_date($r['end_date'], false),
                'start_date' => $timedate->to_display_date($r['start_date'], false),
            );
        }

        //Get Notification
        $q7 = "SELECT nttt.* FROM (SELECT DISTINCT
            IFNULL(nt.id, '') primaryid,
            CASE nt.assigned_user_id WHEN 'all' THEN IFNULL(l1.id, '') ELSE (IFNULL(l1.id, '')) END assigned_user_id,
            IFNULL(nt.name, '') title,
            IFNULL(nt.description, '') body,
            IFNULL(nt.is_read, 0) is_read,
            IFNULL(nt.parent_id, '') record_id,
            IFNULL(nt.parent_type, '') module_name,
            IFNULL(nt.created_by, '') created_by,
            nt.date_entered date_entered
            FROM notifications nt
            INNER JOIN users l1 ON (nt.assigned_user_id = l1.id OR nt.assigned_user_id = 'all') AND l1.deleted = 0 AND (nt.assigned_user_id = '$str_user_id' OR nt.assigned_user_id = 'all')
            WHERE nt.deleted = 0 AND (nt.apps_type = 'Teacher')) nttt WHERE nttt.assigned_user_id = '$str_user_id'
            ORDER BY nttt.assigned_user_id ASC, nttt.date_entered DESC";
        $rs = $GLOBALS['db']->query($q7);
        $key = 0;
        while ($r = $GLOBALS['db']->fetchbyAssoc($rs)) {

            $nt = array();
            $nt['primaryid'] = $r['primaryid'];
            $nt['title'] = $r['title'];
            $nt['body'] = $r['body'];
            //get notification All
            if (!empty($r['student_read']) && $r['student_read'] != '[]') {
                $student_read = json_decode(html_entity_decode($r['student_read']), true);
                if (in_array($r['assigned_user_id'], $student_read))
                    $r['is_read'] = '1';
            }
            $nt['is_read'] = ($r['is_read'] == '1') ? true : false;
            if ($r['is_read'] != '1') $teacher['unread_noti'] += 1; //Add Number of Notification

            $nt['module_name'] = $r['module_name'];
            $nt['record_id'] = $r['record_id'];
            $nt['created_by'] = $r['created_by'];
            $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));

            $teacher['notifications'][$key] = $nt;

            $key++;
        }

        $language_options = array('case_status_dom', 'case_sub_type_dom', 'status_class_list', 'attendance_type_list', 'do_homework_list', 'ss_status_list', 'situation_type_list', 'gallery_type_list'); //mảng chứa các options trong app_list_strings


        //Set App_String
        $app_strings = array(
            'en' => parseAppListString('en_us', $language_options),
            'vn' => parseAppListString('vn_vn', $language_options),
        );

        //list tag
        $tag_list = array();
        $qGetTags = "SELECT IFNULL(id, '') primaryid
            , IFNULL(name, '') tag_name
            , IFNULL(name_lower, '') name_lower
            FROM tags
            WHERE deleted=0";
        $resGetTags = $GLOBALS['db']->query($qGetTags);
        while ($row = $GLOBALS['db']->fetchByAssoc($resGetTags)) {
            $tag_list[$row['primaryid']]['primaryid'] = $row['primaryid'];
            $tag_list[$row['primaryid']]['tag_name'] = $row['tag_name'];
            $tag_list[$row['primaryid']]['name_lower'] = $row['name_lower'];
        }

        $team_list = array();
        // Get all team set
        $teamSetBean = new TeamSet();
        $teams = $teamSetBean->getTeams($teacher['team_set_id']);
        // Add all team set to  $team_list
        foreach ($teams as $key => $value){
            $teamInfo = $GLOBALS['db']->fetchOne("
                SELECT id,
                name,
                IFNULL(legal_name,'' ) legal_name,
                IFNULL(short_name,'') short_name,
                IFNULL(code_prefix,'') code_prefix ,
                IFNULL(phone_number,'') phone ,
                IFNULL(description,'') description
                FROM teams
            WHERE id = '$key' AND private = 0");
            $team_list[] = [
                'id' => $teamInfo['id'],
                'name' => $teamInfo['name'],
                'legal_name' => $teamInfo['legal_name'],
                'code_prefix' => $teamInfo['code_prefix'],
                'phone_number' => $teamInfo['phone'],
                'short_name' => $teamInfo['short_name'],
                'description' => $teamInfo['description'],
            ];
        }
        //get Config
        $admin = new Administration();
        $admin->retrieveSettings();
        $config = array(
            'doc_folder' => empty($admin->settings['default_doc_folder']) ? '' : $admin->settings['default_doc_folder'],
            'google_folder' => empty($admin->settings['default_google_folder']) ? '' : $admin->settings['default_google_folder'],
            'hot_line' => empty($admin->settings['default_hotline']) ? '' : substr($admin->settings['default_hotline'], 0, strlen($admin->settings['default_hotline']) - 1),
        );
        //Return
        return array(
            'success' => true,
            'data' => [
                'portal_name' => $user_name,
                'password' => $password,
                'teacher' => $teacher,
                'news_list' => array_values($news),
                'tag_list' => array_values($tag_list),
                'center_list' => $team_list,
                'app_strings_en' => $app_strings['en'],
                'app_strings_vn' => $app_strings['vn'],
                'config' => $config
            ],
            'error_info' => throwApiError(1)
        );

    }

    //List unread notification divide page - Hoang Huy
    function listNotifications(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id', 'page']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        //Get the page from the mobile app to set LIMIT
        $limit = (!empty($args['page'])) ? 30 * $args['page'] : 30;
        $limit_bot = $limit - 30;
        $limit_top = $limit;
        $teacher_id = $args['teacher_id'];

        $args['update_time'] = empty($args['update_time']) ? "update" : $args['update_time'];
        if ($limit == 30 && $args['update_time'] == "update") { //First Page
            $time = date('Y-m-d H:i:s', strtotime($GLOBALS['timedate']->nowDb()));
            $GLOBALS['db']->query("UPDATE c_teachers SET lasted_view_noti = '{$time}' WHERE id = '{$teacher_id}' AND deleted = 0");
        }

        $q1 = "SELECT IFNULL(team_id, '') team_id
        FROM c_teachers ct
        WHERE ct.id = '{$teacher_id}' AND ct.deleted = 0";
        $team_id = $GLOBALS['db']->getOne($q1);

        $q2 = "SELECT nttt.* FROM ((SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        WHERE nt.deleted = 0
        AND nt.assigned_user_id = '{$teacher_id}'
        ORDER BY nt.date_entered
        LIMIT $limit_bot, $limit_top)
        UNION
        (SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        INNER JOIN team_sets_teams ts
        ON nt.team_set_id = ts.team_set_id
        WHERE (ts.team_id = ('{$team_id}') OR nt.team_set_id = 1) AND nt.parent_type = 'C_News' AND nt.assigned_user_id = 'all' AND nt.apps_type LIKE '%Teacher%'
        LIMIT $limit_bot, $limit_top)) nttt
        ORDER BY date_entered DESC
        LIMIT $limit_bot, $limit_top";

        $q4 = "SELECT DISTINCT
        IFNULL(c_news.id, '') news_id,
        IFNULL(l1.id, '') student_id,
        CASE WHEN IFNULL(l1.id, '') = '' THEN '0' ELSE 1 END is_read
        FROM c_news
        LEFT JOIN c_news_c_teachers_1_c l1_1 ON c_news.id = l1_1.c_news_c_teachers_1c_news_ida AND l1_1.deleted = 0
        LEFT JOIN c_teachers l1 ON l1.id = l1_1.c_news_c_teachers_1c_teachers_idb AND l1.deleted = 0
        WHERE l1.id = '{$teacher_id}' AND c_news.deleted = 0
        AND (c_news.type LIKE '%Teacher%')";
        $row1 = $GLOBALS['db']->query($q2);
        $row2 = $GLOBALS['db']->fetchArray($q4);

        //List of news that the teacher read
        //The target of make this read news list is to make sense that which is the notification about news that the student read
        $read_news = array();
        foreach ($row2 as $_key => $row) $read_news[$row['news_id']] = $row['is_read'];
        //Loop through all notifications and push it into $notification_list

        while ($r = $GLOBALS['db']->fetchByAssoc($row1)) {
            //get notification All
            $nt = array();
            $nt['primaryid'] = $r['primaryid'];
            $nt['title'] = $r['title'];
            $nt['body'] = $r['body'];
            $nt['is_read'] = ($r['parent_type'] == "C_News") ? boolVal($read_news[$r['parent_id']]) : boolVal($r['is_read']);
            $nt['module_name'] = $r['module_name'];
            $nt['record_id'] = $r['record_id'];
            $nt['created_by'] = $r['created_by'];
            $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));
            $notification_list[] = $nt;
        }
        return array(
            'success' => true,
            'data' => [
                'notification_list' => $notification_list,
            ],
            'error_info' => throwApiError(1)
        );
    }

    //Count Unread Notification - Hoang Huy
    function countUnreadNoti(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $unread_noti_list = array();
        $teacher_id = $args['teacher_id'];
        $lasted_date_view_noti = $GLOBALS['db']->getOne("SELECT c_teachers.lasted_view_noti FROM c_teachers WHERE id = '{$teacher_id}' AND deleted = 0");
        if (empty($lasted_date_view_noti)) $lasted_date_view_noti = '1970-01-01';
        $q7 = "SELECT COUNT(id) FROM notifications WHERE assigned_user_id = '{$teacher_id}' AND date_entered >= '{$lasted_date_view_noti}' AND is_read = 0 AND deleted = 0";
        $unread_noti_list[$teacher_id] = intVal($GLOBALS['db']->getOne($q7));
        return array(
            'success' => true,
            'data' => [
                'unread_noti_list' => $unread_noti_list,
            ],
            'error_info' => throwApiError(1)
        );
    }

    //Update Notification Read/Unread/Delete  - Hoang Huy
    function updateNotification(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id', 'action_type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        //Get noti beans
        $noti = BeanFactory::getBean('Notifications', $args['noti_id']);
        $cnew_id = $noti->parent_type == 'C_News' ? $noti->parent_id : "";
        $teacher_id = $args['teacher_id'];
        if ($noti->parent_type == "C_News" && !empty($cnew_id))
            $new = BeanFactory::getBean('C_News', $cnew_id);
        switch ($args['action_type']) {
            case 'read':
                if ($noti->parent_type == "C_News" && !empty($cnew_id)) {
                    if (!empty($new->id) && $new->load_relationship('c_news_c_teachers_1')) {
                        $new->c_news_c_teachers_1->add($teacher_id);
                        $GLOBALS['db']->query("UPDATE c_news SET addon_views = addon_views + 1 WHERE deleted = 0 AND id = '{$cnew_id}'");
                    }
                } elseif (!$noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'unread':
                if ($noti->parent_type == "C_News" && !empty($cnew_id)) {
                    //Incase unread we will need to delete the relationship existing
                    if (!empty($new->id) && $new->load_relationship('c_news_c_teachers_1'))
                        $new->c_news_c_teachers_1->delete($cnew_id, $teacher_id);
                } elseif ($noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 0 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'delete':
                //the student only can delete the notification that relate to them and cant delete the notification about news
                if ($noti->parent_type != 'C_News') $noti->mark_deleted($noti->id);
                break;
            case 'markallread':
                //Mark All Read Notification
                $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE assigned_user_id = '$teacher_id' AND is_read = 0 AND deleted=0");
                //Read all news
                //Step1: get all the news that the students have read
                $q1 = "SELECT IFNULL(c_news_c_teachers_1c_news_ida, '') news_id FROM c_news_c_teachers_1_c crl WHERE crl.deleted = 0 AND crl.c_news_c_teachers_1c_teachers_idb = '{$teacher_id}'";
                $row1 = $GLOBALS['db']->fetchArray($q1);

                //Step2: convert $read_news array to string to query get all the news that doesn't exist in our read list
                $str_read_news = (!empty($row1)) ? implode("','", array_column($row1, 'news_id')) : "";
                $q2 = "SELECT IFNULL(id, '') news_id FROM c_news WHERE deleted = 0 AND id NOT IN ('$str_read_news') AND type LIKE '%Teacher%'";
                $row2 = $GLOBALS['db']->fetchArray($q2);
                $date_modified = $GLOBALS['timedate']->nowDb(); //Get time at now
                foreach ($row2 as $r2) {
                    //Count the relationship that was deleted
                    $countRelationDeleted = $GLOBALS['db']->getOne("SELECT COUNT(id)
                    FROM c_news_c_teachers_1_c
                    WHERE c_news_c_teachers_1c_news_ida = '{$r2['news_id']}'
                    AND c_news_c_teachers_1c_teachers_idb = '{$teacher_id}' AND deleted = 1");
                    if ($countRelationDeleted > 0) {
                        //If exist the deleted realtionship we will update deleted field to 1 instead of create new record
                        $qUpdateRelation = "UPDATE c_news_c_teachers_1_c
                    SET deleted = 0, date_modified ='{$date_modified}'
                    WHERE deleted = 1
                    AND c_news_c_teachers_1c_news_ida = '{$r2['news_id']}'
                    AND c_news_c_teachers_1c_teachers_idb = '{$teacher_id}'";
                        $GLOBALS['db']->query($qUpdateRelation);
                    } else {
                        //If doesn't exist the deleted relationship we will add new relationship
                        $relation_id = create_guid();
                        $qInsertRelation = "INSERT INTO c_news_c_teachers_1_c (id, date_modified, deleted, c_news_c_teachers_1c_news_ida, c_news_c_teachers_1c_teachers_idb)
                    VALUES ('{$relation_id}','{$date_modified}', 0,'{$r2['news_id']}','{$args['teacher_id']}')";
                        $GLOBALS['db']->query($qInsertRelation);
                    }
                }
                break;
        }
        return array(
            'success' => true,
            'error_info' => throwApiError(1)
        );
    }

    function handleGallery(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');
        switch ($args['action']){
            case 'delete': {
                $validate_param = validateParam($args, ['gallery_id']);
                break;
            }
            case 'update': {
                $validate_param = validateParam($args, ['gallery_id']);
            }
            case 'create': {
                $validate_param = validateParam($args, ['teacher_id', 'gallery_name', 'gallery_date', 'gallery_type']);
                break;
            }
        }

        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        $teacher_id = $args['teacher_id'];
        $class_id = $args['class_id'];
        $gallery_name = $args['gallery_name'];
        $gallery_date = $args['gallery_date'];
        $description = $args['description'];
        $gallery_id = $args['gallery_id'];
        $gallery_type = $args['gallery_type'];
        $list_remove_id = $args['list_remove_id'];
        $created_with_class = !empty($class_id);
        //Get User apps_admin
        global $current_user;
        $current_user = new User();
        $current_user->retrieve_by_string_fields(array('teacher_id' => $teacher_id));

        if (empty($gallery_id)) {
            $gallery = BeanFactory::newBean('C_Gallery');
            $gallery->id = create_guid();
            $gallery->new_with_id = true;
        } else {
            $gallery = BeanFactory::getBean('C_Gallery', $gallery_id);
            if($args['action'] == 'delete'){
                $gallery->mark_deleted($gallery_id);
                return [
                    'success' => true,
                    'error_info' => throwApiError(1)
                ];
            }
        }
        $qTeacher = "SELECT DISTINCT
                        IFNULL(team_id, '') team_id,
                        IFNULL(team_set_id, '') team_set_id
                        FROM c_teachers
                        WHERE id = '$teacher_id' AND deleted = 0";
        $result = $GLOBALS['db']->fetchOne($qTeacher);

        $gallery->name = $gallery_name;
        $gallery->assigned_user_id = $teacher_id;
        //Set default team if create by class
        $gallery->team_id = $result['team_id'];
        $gallery->team_set_id = $result['team_id'];
        $gallery->description = $description;
        if($created_with_class) {
            $gallery->j_class_c_gallery_1j_class_ida = $class_id;
            $gallery->visibility_range = 'class';
        } else {
            $gallery->visibility_range = 'center';
            $gallery->team_id = $args['team_id'];
            $gallery->team_set_id = $args['team_id'];
        }
        if($args['action'] == 'create') {
            $gallery->gallery_date = $GLOBALS['timedate']->nowDb() ;
        }
        $gallery->gallery_type = $gallery_type;
        $gallery->status = "wait_for_approval";

        $list_img = [];
        if (!empty($_FILES['attachment'])) {
            $count_picture = count($_FILES['attachment']['name']);
            if ($count_picture > 0)
                for ($i = 0; $i < $count_picture; $i++) {
                    $note = BeanFactory::newBean('Notes');
                    $note->id = create_guid();
                    $note->new_with_id = true;
                    $note->name = $note->id .'.'.pathinfo($_FILES['attachment']['name'][$i], PATHINFO_EXTENSION);
                    $note->file_mime_type = $_FILES['attachment']['type'][$i];
                    $note->file_ext = pathinfo($_FILES['attachment']['name'][$i], PATHINFO_EXTENSION);
                    $note->file_size = $_FILES['attachment']['size'][$i];
                    $note->filename = $_FILES['attachment']['name'][$i];
                    $note->document_name = $_FILES['attachment']['name'][$i];
                    $note->parent_type = 'C_Gallery';
                    $note->parent_id = $gallery->id;
                    $note->save();
                    $sourceFile = 'upload/s3_storage/' . $note->name;
                    if (move_uploaded_file($_FILES['attachment']['tmp_name'][$i], $sourceFile)) {
                        $list_img[] = $note->id;
                    }
                }
        }
        $remove_pic_list = [];
        $add_pic_list = [];
        if (!empty($list_remove_id)){
            foreach($list_remove_id as $delete_pic) {
                $remove_pic_list[] = [
                    'id' => $delete_pic,
                    'action' => 'delete'
                ];
            }
        }
        if (!empty($list_img)){
            foreach($list_img as $add_pic) {
                $add_pic_list[] = [
                    'id' => $add_pic,
                    'action' => 'add'
                ];
            }
        }
        $gallery->attachment_list = array_merge($remove_pic_list, $add_pic_list);
        $gallery->save();

        if($created_with_class){
            $gallery->load_relationship('j_class_c_gallery_1');
            $gallery->j_class_c_gallery_1->add($class_id);
        }
        if($args['action'] == 'create' || $args['action'] = 'update'){
            $sqlTeacher = "SELECT
                        IFNULL(full_teacher_name, '') full_teacher_name,
                        IFNULL(picture, '') picture
                    FROM c_teachers
                    WHERE id = '$teacher_id' AND deleted = 0";
            $resultTeacher = $GLOBALS['db']->fetchOne($sqlTeacher);
            $sqlClass = "SELECT
                        IFNULL(name, '') class_name
                    FROM j_class
                    WHERE id = '$class_id' AND deleted = 0";
            $resultClass = $GLOBALS['db']->fetchOne($sqlClass);
            return [
                'success' => true,
                'data' => [
                    'gallery_model' => [
                        'gallery_id' => $gallery->id,
                        'class_id' => $class_id,
                        'class_name' => $resultClass['class_name'],
                        'team_id' => $gallery->team_id,
                        'team_name' => $gallery->team_name,
                        'user_avt' => $resultTeacher['picture'],
                        'created_by' => $gallery->created_by,
                        'visibility_range' => $gallery->visibility_range,
                        'full_user_name' => $resultTeacher['full_teacher_name'],
                        'gallery_name' => $gallery->name,
                        'gallery_description' => $gallery->description,
                        'gallery_type' => $gallery->gallery_type,
                        'total_like' => $this->countLikes($gallery->id, 'C_Gallery'),
                        'total_comment' => $this->countComments($gallery->id, 'C_Gallery'),
                        'gallery_date' => date('Y-m-d H:i:s', strtotime("+7 hours " . $gallery->gallery_date)),
                        'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $gallery->date_entered)),
                        'date_modified' => date('Y-m-d H:i:s', strtotime("+7 hours " . $gallery->date_modified)),
                        'image_list' => $this->getNote($gallery->id, 'C_Gallery'),
                        'is_liked' => $this->isLiked($gallery->id, 'C_Gallery', $teacher_id)
                    ],
                ],
                'error_info' => throwApiError(1)
            ];
        } else {
            return [
                'success' => true,
                'error_info' => throwApiError(1)
            ];
        }
    }
    function getNote($parent_id, $parent_type){
        //Get the image of each gallery
        $sqlNote = "SELECT IFNULL(l1.id, '') pic_id,
                        IFNULL(l1.name, '') name,
                        IFNULL(l1.upload_id, '') upload_id,
                        IFNULL(l1.file_source, '') file_source,
                        IFNULL(l1.file_mime_type, '') file_mime_type,
                        IFNULL(l1.file_ext, '') file_ext
                        FROM notes l1
                        WHERE l1.parent_id = '{$parent_id}' AND l1.parent_type = '{$parent_type}' AND l1.deleted = 0";
        $img_list = $GLOBALS['db']->fetchArray($sqlNote);
        $formatted_img_list = [];
        //Format img list
        foreach($img_list as $pic){
            $srcImg = $pic['file_source'] == 'S3'
                ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $pic['upload_id']
                : $GLOBALS['dotb_config']['site_url'] . '/download_attachment.php?path=s3_storage/'.$pic['name'];
            $fileType = explode('/', $pic['file_mime_type'])[0]; //image or video
            $formatted_img_list[] = array(
                'pic_id' => $pic['pic_id'],
                'file_source' => empty($pic['file_source']) ? 'local' : $pic['file_source'],
                'file_type' => $fileType,
                'file_ext' => $pic['file_ext'],
                'picture_url' => $srcImg
            );
        }
        return $formatted_img_list;
    }

    function getGalleryList(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $teacher_id = $args['teacher_id'];
        $class_id = $args['class_id'];
        $team_id = $args['team_id'];
        if($args['type'] == 'owner'){
            $gallery_list = $this->getGalleryDetail('', $teacher_id, 'owner',1, ['class_id' => $class_id, 'team_id' => $team_id]);
        } else {
            $q2 = "SELECT DISTINCT
            IFNULL(l2.id, '') class_id
            FROM meetings
            INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN c_teachers l1 ON  l1.id IN (meetings.teacher_id, meetings.teacher_cover_id, meetings.sub_teacher_id, l2.teacher_id)  AND l1.deleted = 0
            INNER JOIN j_kindofcourse l3 ON l2.koc_id = l3.id AND l3.deleted = 0
            INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
            WHERE (((l1.id = '$teacher_id' )
            AND (IFNULL(meetings.session_status, '') <> 'Cancelled'
            OR (IFNULL(meetings.session_status, '') IS NULL
            AND 'Cancelled' IS NOT NULL))
            AND (IFNULL(meetings.meeting_type, '') = 'Session')))
            AND meetings.deleted = 0";
            $result = array_column($GLOBALS['db']->fetchArray($q2), 'class_id');
            $class_list_id = implode("','",$result);
            $gallery_list = $this->getGalleryDetail($class_list_id, $teacher_id, 'all', $args['page'],  ['class_id' => $class_id, 'team_id' => $team_id]);
        }
        if(!empty($gallery_list)){
            //Load the first page of comment for gallery
            foreach ($gallery_list['gallery_list'] as $key => $gallery){
                $comment_list = $this->getCommentList($api, ['parent_id' => $gallery['gallery_id'], 'parent_type' => 'C_Gallery', 'page' => 1]);
                if(!empty($comment_list['data']['comment_list'])){
                    $comment_list = $comment_list['data']['comment_list'];
                } else {
                    $comment_list = [];
                }
                $gallery_list['gallery_list'][$key]['comment_list'] = $comment_list;
            }
            return [
                'success' => true,
                'data' => [
                    'gallery_list' => $gallery_list['gallery_list'],
                    'can_load' => $gallery_list['can_load'],
                    'page' => (int)$args['page']
                ],
                'error_info' => throwApiError(1)
            ];
        } else {
            return [
                'success' => false,
                'data' => [
                    'gallery_list' => []
                ],
                'error_info' => throwApiError(202)
            ];
        }
    }
    //Move from TeaAPI
    function listParent(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['student_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $timedate;

        $qStudent = "SELECT DISTINCT
                        IFNULL(c.id, '') student_id,
                        IFNULL(c.full_student_name, '') student_name,
                        IFNULL(c.picture, '') picture,
                        IFNULL(c.contact_id, '') portal_id,
                        IFNULL(c.phone_mobile, '') phone,
                        'Contacts' student_type,
                        c.birthdate birthdate,
                        IFNULL(c.nick_name, '') nick_name,
                        IFNULL(c.guardian1, '') p1_id,
                        IFNULL(c.guardian_type_1, '') p1_type,
                        IFNULL(c.guardian2, '') p2_id,
                        IFNULL(c.guardian_type_2, '') p2_type,
                        IFNULL(c.guardian3, '') p3_id,
                        IFNULL(c.guardian_type_3, '') p3_type,
                        IFNULL(l1.last_name, '') p1_last_name,
                        IFNULL(l1.first_name, '') p1_first_name,
                        IFNULL(l1.phone_mobile, '') p1_phone_mobile,
                        IFNULL(l1.picture, '') p1_picture,
                        IFNULL(l1.birthdate, '') p1_birthdate,
                        IFNULL(l2.last_name, '') p2_last_name,
                        IFNULL(l2.first_name, '') p2_first_name,
                        IFNULL(l2.phone_mobile, '') p2_phone_mobile,
                        IFNULL(l2.picture, '') p2_picture,
                        IFNULL(l2.birthdate, '') p2_birthdate,
                        IFNULL(l3.last_name, '') p3_last_name,
                        IFNULL(l3.first_name, '') p3_first_name,
                        IFNULL(l3.phone_mobile, '') p3_phone_mobile,
                        IFNULL(l3.picture, '') p3_picture,
                        IFNULL(l3.birthdate, '') p3_birthdate
                    FROM contacts c
                    LEFT JOIN j_membership l1 ON c.guardian1 = l1.id AND l1.deleted = 0
                    LEFT JOIN j_membership l2 ON c.guardian2 = l2.id AND l2.deleted = 0
                    LEFT JOIN j_membership l3 ON c.guardian3 = l3.id AND l3.deleted = 0
                    WHERE (c.contact_id = '{$args['student_id']}')
                    AND c.deleted = 0";
        $rStudent = $GLOBALS['db']->query($qStudent);
        $student = array();
        $parent = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($rStudent)) {
            $student['student_id'] = $row['student_id'];
            $student['student_name'] = $row['student_name'];
            $student['picture'] = $row['picture'];
            $student['portal_id'] = $row['portal_id'];
            $student['nick_name'] = $row['nick_name'];
            $student['birthdate'] = $timedate->to_display_date($row['birthdate'], false);
            $student['student_phone'] = $row['phone'];
            $student['student_type'] = $row['student_type'];
            if (!empty($row['p1_id'])) {
                $parent[0]['parent_id'] = $row['p1_id'];
                $parent[0]['type'] = $row['p1_type'];
                $parent[0]['last_name'] = $row['p1_last_name'];
                $parent[0]['first_name'] = $row['p1_first_name'];
                $parent[0]['phone_mobile'] = $row['p1_phone_mobile'];
                $parent[0]['picture'] = $row['p1_picture'];
                $parent[0]['birthdate'] = $timedate->to_display_date($row['p1_birthdate'], false);
            }
            if (!empty($row['p2_id'])) {
                $parent[1]['parent_id'] = $row['p2_id'];
                $parent[1]['type'] = $row['p2_type'];
                $parent[1]['last_name'] = $row['p2_last_name'];
                $parent[1]['first_name'] = $row['p2_first_name'];
                $parent[1]['phone_mobile'] = $row['p2_phone_mobile'];
                $parent[1]['picture'] = $row['p2_picture'];
                $parent[1]['birthdate'] = $timedate->to_display_date($row['p2_birthdate'], false);
            }
            if (!empty($row['p3_id'])) {
                $parent[2]['parent_id'] = $row['p3_id'];
                $parent[2]['type'] = $row['p3_type'];
                $parent[2]['last_name'] = $row['p3_last_name'];
                $parent[2]['first_name'] = $row['p3_first_name'];
                $parent[2]['phone_mobile'] = $row['p3_phone_mobile'];
                $parent[2]['picture'] = $row['p3_picture'];
                $parent[2]['birthdate'] = $timedate->to_display_date($row['p3_birthdate'], false);
            }
        }

        $student['parent'] = $parent;

        return array(
            'success' => true,
            'data' => [
                'student' => $student
            ],
            'error_info' => throwApiError(1)
        );
    }

    /**
     * @param array $args = lesson_date
     *  'path' => array('checkin-out', 'list-student'),
     */
    function listStudent(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['student_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $timedate;
        $lessonDateDb = $timedate->convertToDBDate($args['lesson_date'], false);
        $qClass = "SELECT ju_class_id class_id
                    FROM meetings
                    WHERE deleted = 0 AND DATE(CONVERT_TZ(meetings.date_start,'+00:00','+7:00')) = '$lessonDateDb'";
        $classId = array();
        $rClassId = $GLOBALS['db']->query($qClass);
        while ($row = $GLOBALS['db']->fetchByAssoc($rClassId)) {
            $classId[] = $row['class_id'];
        }
        if (empty($classId)) return array('success' => false, 'error_info' => throwApiError(202));

        $getListStudent = "SELECT DISTINCT
            IFNULL(c.id, '') student_id,
            IFNULL(c.full_student_name, '') student_name,
            IFNULL(c.picture, '') picture,
            IFNULL(c.contact_id, '') portal_id,
            IFNULL(c.phone_mobile, '') phone,
            'Contacts' type,
            c.birthdate birthdate,
            IFNULL(c.nick_name, '') nick_name,
            IFNULL(l2.id, '') class_id,
            IFNULL(l2.name, '') class_name,
            IFNULL(c.guardian1, '') p1_id,
            IFNULL(c.guardian_type_1, '') p1_type,
            IFNULL(l3.last_name, '') p1_last_name,
            IFNULL(l3.first_name, '') p1_first_name,
            IFNULL(l3.phone_mobile, '') p1_phone_mobile,
            IFNULL(l3.picture, '') p1_picture,
            IFNULL(l3.birthdate, '') p1_birthdate,
            IFNULL(c.guardian2, '') p2_id,
            IFNULL(c.guardian_type_2, '') p2_type,
            IFNULL(l4.last_name, '') p2_last_name,
            IFNULL(l4.first_name, '') p2_first_name,
            IFNULL(l4.phone_mobile, '') p2_phone_mobile,
            IFNULL(l4.picture, '') p2_picture,
            IFNULL(l4.birthdate, '') p2_birthdate,
            IFNULL(c.guardian3, '') p3_id,
            IFNULL(c.guardian_type_3, '') p3_type,
            IFNULL(l5.last_name, '') p3_last_name,
            IFNULL(l5.first_name, '') p3_first_name,
            IFNULL(l5.phone_mobile, '') p3_phone_mobile,
            IFNULL(l5.picture, '') p3_picture,
            IFNULL(l5.birthdate, '') p3_birthdate
            FROM contacts c
            INNER JOIN j_classstudents l1 ON l1.student_id = c.id AND c.deleted = 0
            AND l1.deleted = 0 AND l1.class_id IN ('" . implode("','", $classId) . "')
            INNER JOIN j_class l2 ON l2.id = l1.class_id AND l2.deleted = 0
            LEFT JOIN j_membership l3 ON c.guardian1 = l3.id AND l3.deleted = 0
            LEFT JOIN j_membership l4 ON c.guardian2 = l4.id AND l4.deleted = 0
            LEFT JOIN j_membership l5 ON c.guardian3 = l5.id AND l5.deleted = 0
            ";
        $rGetListStudent = $GLOBALS['db']->query($getListStudent);
        $studentList = array();
        while ($r = $GLOBALS['db']->fetchbyAssoc($rGetListStudent)) {
            $studentList[$r['student_id']]['student_id'] = $r['student_id'];
            $studentList[$r['student_id']]['student_name'] = $r['student_name'];
            $studentList[$r['student_id']]['class_id'] = $r['class_id'];
            $studentList[$r['student_id']]['class_name'] = $r['class_name'];
            $studentList[$r['student_id']]['picture'] = $r['picture'];
            $studentList[$r['student_id']]['portal_id'] = $r['portal_id'];
            $studentList[$r['student_id']]['student_phone'] = $r['phone'];
            $studentList[$r['student_id']]['student_type'] = $r['type'];
            $studentList[$r['student_id']]['birthdate'] = $timedate->to_display_date($r['birthdate'], false);
            $studentList[$r['student_id']]['nick_name'] = $r['nick_name'];

            $parent = array();
            if (!empty($r['p1_id'])) {
                $parent[0]['parent_id'] = $r['p1_id'];
                $parent[0]['type'] = $r['p1_type'];
                $parent[0]['last_name'] = $r['p1_last_name'];
                $parent[0]['first_name'] = $r['p1_first_name'];
                $parent[0]['phone_mobile'] = $r['p1_phone_mobile'];
                $parent[0]['picture'] = $r['p1_picture'];
                $parent[0]['birthdate'] = $timedate->to_display_date($r['p1_birthdate'], false);
            }
            if (!empty($r['p2_id'])) {
                $parent[1]['parent_id'] = $r['p2_id'];
                $parent[1]['type'] = $r['p2_type'];
                $parent[1]['last_name'] = $r['p2_last_name'];
                $parent[1]['first_name'] = $r['p2_first_name'];
                $parent[1]['phone_mobile'] = $r['p2_phone_mobile'];
                $parent[1]['picture'] = $r['p2_picture'];
                $parent[1]['birthdate'] = $timedate->to_display_date($r['p2_birthdate'], false);
            }
            if (!empty($r['p3_id'])) {
                $parent[2]['parent_id'] = $r['p3_id'];
                $parent[2]['type'] = $r['p3_type'];
                $parent[2]['last_name'] = $r['p3_last_name'];
                $parent[2]['first_name'] = $r['p3_first_name'];
                $parent[2]['phone_mobile'] = $r['p3_phone_mobile'];
                $parent[2]['picture'] = $r['p3_picture'];
                $parent[2]['birthdate'] = $timedate->to_display_date($r['p3_birthdate'], false);
            }
            $studentList[$r['student_id']]['parent'] = $parent;
        }
        return array(
            'success' => 'true',
            'data' => [
                'student_list' => $studentList
            ],
            'error_info' => throwApiError(1)
        );
    }

    //End move from TeaAPI

    //Move from MobileApi
    function editTeacherAvt(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');

        $id = $args['id'];
        $bean = BeanFactory::getBean('C_Teachers', $id);
        $name = create_guid();
        $file = 'upload/' . $name;
        move_uploaded_file($_FILES['picture']['tmp_name'], $file);

        if ($args['type'] == 'avatar') {
            $q1 = "UPDATE c_teachers
            SET c_teachers.picture = '$name' WHERE c_teachers.id = '$id'";
            $GLOBALS['db']->query($q1);
        } else {
            $q1 = "UPDATE c_teachers
            SET c_teachers.cover_app = '$name' WHERE c_teachers.id = '$id'";
            $GLOBALS['db']->query($q1);
        }

        return array(
            'success' => true,
            'data' => [
                'bean_id' => $bean->id,
            ],
            'error_info' => throwApiError(1)
        );
    }

    function getTeachingHour(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $timedate;
        if (empty($args['begin'])) {
            $now_day = date($timedate->get_db_date_format(), strtotime($timedate->nowDb() . '+7 hours'));
            $begin = $timedate->convert_to_gmt_datetime(date('Y-m-01', strtotime($now_day)));
            $end = $timedate->convert_to_gmt_datetime(date('Y-m-01', strtotime($now_day . '+1 months')));
        } else {
            $begin = $timedate->convert_to_gmt_datetime($args['begin']);
            $end = $timedate->convert_to_gmt_datetime($args['end']);
        }
        $teacher = array();
        include_once("custom/include/_helper/junior_class_utils.php");
        $total_hours = getTeachingHour($begin, $end, $args['teacher_id'], '')[0];
        $sum_late_format = floor($total_hours['late_time'] / 3600) . ":" . floor($total_hours['late_time'] / 60 % 60) . ":" . floor($total_hours['late_time'] % 60);
        $teacher['total_hous'] = (double)$total_hours['max_hour'];
        $teacher['teaching_hours'] = (double)$total_hours['teaching_hour'];
        $teacher['teaching_late_time'] = $sum_late_format;
        $teacher['total_session'] = (double)$total_hours['total_session'];
        $teacher['teaching_session'] = (double)$total_hours['teaching_session'];
        $teacher['teaching_hours_by_class'] = getTeachingHour($begin, $end, $args['teacher_id'], 'class');
        return array(
            'success' => true,
            'data' => [
                'teacher' => $teacher
            ],
            'error_info' => throwApiError(1)
        );
    }

    function getLearningHour(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');
        global $timedate;
        $validate_param = validateParam($args, ['begin', 'end', 'teacher_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $start = $timedate->convertToDBDate($args['begin']);
        $end   = $timedate->convertToDBDate($args['end']);

        $s1 = "SELECT DISTINCT
            IFNULL(l2.id, '') l2_class_id,
            IFNULL(c_teachers.id, '') teacher_id,
            IFNULL(c_teachers.full_teacher_name, '') C_TEACHERS_FULL_TEACHE77CDF3,
            IFNULL(l2.class_code, '') l2_class_code,
            IFNULL(l2.name, '') l2_name,
            IFNULL(l3.name, '') l3_team_name,
            IFNULL(l2.team_id, '') l2_team_id,
            COUNT(DISTINCT c_teachers.id) c_teachers__allcount,
            SUM(l1.total_attended) c_teachers__count,
            SUM(l1.teaching_hour) l1_sum_teaching_hour,
            SUM(IFNULL(l1.total_attended,0) * IFNULL(l1.teaching_hour, 0)) l1_sum_duration_cal
            FROM c_teachers
            INNER JOIN meetings l1 ON c_teachers.id = l1.teacher_id AND l1.deleted = 0
            INNER JOIN j_class l2 ON l1.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN teams l3 ON l2.team_id = l3.id AND l3.deleted = 0
            WHERE (l1.date >= '$start' AND l1.date <= '$end')
            AND (l1.session_status <> 'Cancelled') AND c_teachers.deleted = 0 AND c_teachers.id = '{$args['teacher_id']}'
            GROUP BY teacher_id, l2_class_id";

        $s2 = "SELECT DISTINCT
            IFNULL(l2.id, '') class_id,
            IFNULL(l1.date, '') l1_date,
            IFNULL(l1.teaching_hour, 0) l1_teaching_hour,
            IFNULL(l1.total_attended,0) c_teachers__count,
            IFNULL(l1.total_attended,0) * IFNULL(l1.teaching_hour, 0) l1_duration_cal,
            IFNULL(l1.date_start, '') l1_date_start,
            IFNULL(l1.date_end, '') l1_date_end
            FROM c_teachers
            INNER JOIN meetings l1 ON c_teachers.id = l1.teacher_id AND l1.deleted = 0
            INNER JOIN j_class l2 ON l1.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN teams l3 ON l2.team_id = l3.id AND l3.deleted = 0
            WHERE (l1.date >= '$start' AND l1.date <= '$end')
            AND (l1.session_status <> 'Cancelled') AND c_teachers.deleted = 0 AND c_teachers.id = '{$args['teacher_id']}'
            ORDER BY l1.date_start";

        $r1 = $GLOBALS['db']->query($s1);
        $r2 = $GLOBALS['db']->query($s2);
        $data = array('r1' => array(), 'r2' => array());
        while ($row = $GLOBALS['db']->fetchByAssoc($r1)) $data['r1'][] = $row;
        while ($row = $GLOBALS['db']->fetchByAssoc($r2)) $data['r2'][] = $row;
        return array(
            'success' => 1,
            'data' => [
                'learning_hour' => $data
            ],
            'error_info' => throwApiError(1)
        );
    }
    function getListAttendance(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['class_id', 'lesson_date']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        require_once("custom/include/_helper/junior_class_utils.php");
        $res = json_decode(getListAttendanceStudent($args['class_id'], $args['lesson_date']));
        if ($res->success == '0') {
            return array(
                'success' => false,
                'error_info' => throwApiError(115)
            );
        } else {
            return array(
                'success' => true,
                'data' => [
                    'array_student' => (array)$res->array_student
                ],
                'error_info' => throwApiError(1)
            );
        }
    }

    function saveAttendance(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['session_id', 'class_id', 'student_id', 'attend_id', 'field']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        require_once("custom/include/_helper/junior_class_utils.php");
        $confim = false;//biến xác nhận vào vòng lặp
        $data['session_id'] = $args['session_id'];
        $data['class_id'] = $args['class_id'];
        $data['student_id'] = $args['student_id'];
        $data['attend_id'] = $args['attend_id'];
        foreach ($args['field'] as $field) {
            $confim = true;
            $data['savePos'] = $field['field_name'];
            $data['saveVal'] = $field['field_val'];
            $res = json_decode(saveAttendanceUtils($data));
            if ($res->success == '0') {
                return array(
                    'success' => false,
                    'error_info' => throwApiError(106),
                );
            } else if ($res->success == '2') {
                return array(
                    'success' => false,
                    'error_info' => throwApiError(116),
                );
            }
        }

        if (!$confim)
            return array(
                'success' => false,
                'error_info' => throwApiError(106),
            );

        return array(
            'success' => true,
            'data' => [
                'attend_id' => $data['attend_id']
            ],
            'error_info' => throwApiError(1)
        );
    }

    function saveListAttendance(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['session_id', 'class_id', 'attendance']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        require_once("custom/include/_helper/junior_class_utils.php");
        $confim = false;//biến xác nhận vào vòng lặp
        $attendance_error = array();
        $data['session_id'] = $args['session_id'];
        $data['class_id'] = $args['class_id'];
        foreach ($args['attendance'] as $attendance) {
            $data['student_id'] = $attendance['student_id'];
            $data['attend_id'] = $attendance['attend_id'];
            foreach ($attendance['field'] as $field) {
                $confim = true;
                $data['savePos'] = $field['field_name'];
                $data['saveVal'] = $field['field_val'];
                $res = json_decode(saveAttendanceUtils($data));
                if (($res->success == '0' || $res->success == '2') && !in_array($data['attend_id'], $attendance_error)) {
                    $attendance_error[] = $data['attend_id'];
                }
            }
        }
        if (!$confim) {
            return array(
                'success' => false,
                'error_info' => throwApiError(106),
            );
        }
        if (count($attendance_error) > 0) {
            return array(
                'success' => false,
                'data' => [
                    'attendance_error' => $attendance_error
                ],
                'error_info' => throwApiError(106),
            );
        }
        return array(
            'success' => true,
            'data' => [
                'class_id' => $data['class_id']
            ],
            'error_info' => throwApiError(1)
        );
    }

    function sendAttendance(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['attend_id', 'send_type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        require_once('custom/include/_helper/junior_class_utils.php');
        $res = json_decode(attendenceSendApp($args));
        if ($res->status == 'RECEIVED') {
            return array(
                'success' => true,
                'error_info' => throwApiError(1)
            );
        } else if ($res->status == 'SENT_FAILED') {
            return array(
                'success' => false,
                'error_info' => throwApiError(117),
            );
        }
        return array(
            'success' => false,
            'error_info' => throwApiError(106),
        );
    }
    //End move from MobileApi

    //Api for comment and like
    function likeAction(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id', 'parent_id', 'parent_type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE teacher_id = '{$args['teacher_id']}'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $like_link_map = [
            'C_Gallery' => 'c_gallery_c_teachers_1',
        ];
        $focus = BeanFactory::getBean($args['parent_type'], $args['parent_id']);
        $relationship_link = $like_link_map[$args['parent_type']];
        $focus->load_relationship($relationship_link);
        $focus->$relationship_link->add($args['teacher_id']);
        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }
    function unlikeAction(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id', 'parent_id', 'parent_type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE teacher_id = '{$args['teacher_id']}'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $like_link_map = [
            'C_Gallery' => 'c_gallery_c_teachers_1',
        ];
        $focus = BeanFactory::getBean($args['parent_type'], $args['parent_id']);
        $relationship_link = $like_link_map[$args['parent_type']];
        $focus->load_relationship($relationship_link);
        $focus->$relationship_link->delete($args['parent_id'],$args['teacher_id']);

        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }
    function createComment(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id', 'parent_id', 'parent_type', 'content']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE teacher_id = '{$args['teacher_id']}'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $comment = BeanFactory::newBean('C_Comments');
        $comment->parent_id = $args['parent_id'];
        $comment->parent_type = $args['parent_type'];
        $comment->description = $args['content'];
        $comment->direction = 'inbound';
        $comment->save();

        $teacher = BeanFactory::getBean('C_Teachers', $args['teacher_id']);
        $chatter_name = $teacher->full_teacher_name;
        $picture = $GLOBALS['dotb_config']['site_url'] . '/upload/' . $teacher->picture;

        return [
            'success' => true,
            'data' => [
                'id' => $comment->id,
                'content' => $comment->description,
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $comment->date_entered)),
                'created_by_name' => $chatter_name,
                'created_by_avatar' => $picture,
                'created_by_id' => $args['teacher_id']
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function deleteComment(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['comment_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE teacher_id = '{$args['teacher_id']}'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $comment = BeanFactory::getBean('C_Comments', $args['comment_id'], array('disable_row_level_security' => true));
        $comment->mark_deleted($args['comment_id']);
        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }
    function addNewsView(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['news_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $sqlUpdateNew = "UPDATE c_news SET addon_views = addon_views + 1 WHERE id = '{$args['news_id']}'";
        $GLOBALS['db']->query($sqlUpdateNew);

        $sqlGetNew = "SELECT addon_views FROM c_news WHERE id = '{$args['news_id']}'";
        $views = $GLOBALS['db']->getOne($sqlGetNew);
        return [
            'success' => true,
            'data' => [
                'count_views' => $views == false ? 0 : (int)$views
            ],
            'error_info' => throwApiError(1)
        ];
    }
    public function getGalleryById(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['gallery_id', 'teacher_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        $teacher_id = $args['teacher_id'];
        $gallery_id = $args['gallery_id'];
        $formatted_img_list = $this->getNote($gallery_id, 'C_Gallery');
        $sqlGallery = "SELECT
            IFNULL(gallery.id, '') gallery_id,
            IFNULL(gallery.name, '') gallery_name,
            IFNULL(gallery.gallery_date, '') gallery_date,
            IFNULL(gallery.description, '') gallery_description,
            IFNULL(gallery.gallery_type, '') gallery_type,
            IFNULL(gallery.created_by, '') created_by,
            IFNULL(gallery.date_entered, '') date_entered,
            IFNULL(gallery.date_modified, '') date_modified,
            IFNULL(gallery.visibility_range, '') visibility_range,
            IFNULL(user_creator.picture, '') user_avt,
            IFNULL(teacher.picture, '') teacher_avt,
            IFNULL(user_creator.full_user_name, '') full_user_name,
            IFNULL(gallery.team_id, '') team_id
            FROM  c_gallery gallery
                LEFT JOIN users user_creator ON user_creator.id = gallery.created_by
                LEFT JOIN c_teachers teacher ON teacher.id = user_creator.teacher_id
            WHERE gallery.id = '$gallery_id'";

        $album = $GLOBALS['db']->fetchOne($sqlGallery);
        if(!empty($album)){
            if($album['visibility_range'] == 'class'){
                $sqlClass = "SELECT
                    IFNULL(class.id, '') class_id,
                    IFNULL(class.name, '') class_name
                    FROM j_class class
                    INNER JOIN j_class_c_gallery_1_c class_gallery_link ON class.id = class_gallery_link.j_class_c_gallery_1j_class_ida AND class_gallery_link.deleted = 0
                    INNER JOIN c_gallery gallery ON gallery.id = class_gallery_link.j_class_c_gallery_1c_gallery_idb
                    WHERE gallery.id = '$gallery_id'";
                $class = $GLOBALS['db']->fetchOne($sqlClass);
                $album['class_id'] = $class['class_id'];
                $album['class_name'] = $class['class_name'];
            } else {
                $sqlCenter = "SELECT
                    IFNULL(team.id, '') team_id,
                    IFNULL(team.name, '') team_name
                    FROM teams team
                    WHERE team.id = '{$album['team_id']}'";
                $center = $GLOBALS['db']->fetchOne($sqlCenter);
                $album['team_id'] = $center['team_id'];
                $album['team_name'] = $center['team_name'];
            }
            $data = array(
                'gallery_id' => $album['gallery_id'],
                'class_id' => $album['class_id'],
                'class_name' => $album['class_name'],
                'team_id' => $album['team_id'],
                'team_name' => $album['team_name'],
                'gallery_name' => $album['gallery_name'],
                'visibility_range' => $album['visibility_range'],
                'gallery_description' => $album['gallery_description'],
                'gallery_type' => $album['gallery_type'],
                'gallery_date' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['gallery_date'])),
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered'])),
                'date_modified' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_modified'])),
                'user_avt' => empty($album['user_avt']) ? $album['teacher_avt'] : $album['user_avt'],
                'created_by' => $album['created_by'],
                'full_user_name' => $album['full_user_name'],
                'total_like' => $this->countLikes($album['gallery_id'], 'C_Gallery'),
                'is_liked' => $this->isLiked($album['gallery_id'], 'C_Gallery', $teacher_id),
                'total_comment' => $this->countComments($album['gallery_id'], 'C_Gallery'),
                'comment_list' => $comment_list,
                'image_list' => $formatted_img_list,
            );
            return [
                'success' => true,
                'data' => $data,
                'error_info' => throwApiError(1)
            ];
        } else {
            return [
                'success' => false,
                'error_info' => throwApiError(106)
            ];
        }
    }
    function updateCustomSyllabus(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');
        $validate_param = validateParam($args, ['meeting_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $meeting_id = $args['meeting_id'];
        $meeting = BeanFactory::getBean('Meetings', $meeting_id);
        if($meeting->id){
            $meeting->topic_custom = $args['topic_custom'];
            $meeting->syllabus_custom = $args['syllabus_custom'];
            $meeting->objective_custom = $args['note_for_teacher'];
            $meeting->save();
            return [
                'success' => true,
                'error_info' => throwApiError(1)
            ];
        }
        return [
            'success' => false,
            'error_info' => throwApiError(106)
        ];
    }

    function getUnapprovedGallery(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['teacher_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $teacher_id = $args['teacher_id'];
        global $current_user;
        $current_user = new User();
        $current_user->retrieve_by_string_fields(array('teacher_id' => $teacher_id));

        $sqlAlbum = "SELECT DISTINCT
                IFNULL(gallery.id, '') gallery_id,
                IFNULL(class.id, '') class_id,
                IFNULL(class.name, '') class_name,
                IFNULL(team.id, '') team_id,
                IFNULL(team.name, '') team_name,
                IFNULL(gallery.name, '') gallery_name,
                IFNULL(gallery.gallery_date, '') gallery_date,
                IFNULL(gallery.date_modified, '') date_modified,
                IFNULL(gallery.visibility_range, '') visibility_range,
                IFNULL(gallery.description, '') gallery_description,
                IFNULL(gallery.assigned_user_id, '') created_by,
                IFNULL(gallery.gallery_type, '') gallery_type,
                IFNULL(gallery.date_entered, '') date_entered,
                IFNULL(creator.picture, '') user_avatar,
                IFNULL(teacher.picture, '') teacher_avatar,
                IFNULL(creator.full_user_name, '') full_user_name
            FROM c_teachers teacher
                 INNER JOIN team_sets_teams team_set ON teacher.team_set_id = team_set.team_set_id
                 RIGHT JOIN c_gallery gallery ON team_set.team_id = gallery.team_id
                 LEFT JOIN teams team ON team.id = gallery.team_id AND team.deleted = 0
                 LEFT JOIN j_class_c_gallery_1_c class_gallery ON gallery.id = class_gallery.j_class_c_gallery_1c_gallery_idb AND class_gallery.deleted = 0
                 LEFT JOIN j_class class ON class.id = class_gallery.j_class_c_gallery_1j_class_ida AND gallery.deleted = 0
                 INNER JOIN users creator ON creator.id = gallery.created_by
            WHERE teacher.id = '$teacher_id' AND gallery.created_by = '$current_user->id' AND gallery.deleted = 0 AND gallery.status = 'wait_for_approval'
            ORDER BY date_modified DESC";
        $albums = $GLOBALS['db']->query($sqlAlbum);
        $gls = array();
        while ($album = $GLOBALS['db']->fetchbyAssoc($albums)) {
            $formatted_img_list = $this->getNote($album['gallery_id'], 'C_Gallery');
            $gls[] = array(
                'gallery_id' => $album['gallery_id'],
                'class_id' => $album['class_id'],
                'class_name' => $album['class_name'],
                'team_id' => $album['team_id'],
                'team_name' => $album['team_name'],
                'user_avt' => empty($album['user_avatar']) ? $album['teacher_avatar'] : $album['user_avatar'],
                'created_by' => $album['created_by'],
                'visibility_range' => $album['visibility_range'],
                'full_user_name' => $album['full_user_name'],
                'gallery_name' => $album['gallery_name'],
                'gallery_description' => $album['gallery_description'],
                'gallery_type' => $album['gallery_type'],
                'total_like' => $this->countLikes($album['gallery_id'], 'C_Gallery'),
                'total_comment' => $this->countComments($album['gallery_id'], 'C_Gallery'),
                'gallery_date' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['gallery_date'])),
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered'])),
                'date_modified' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_modified'])),
                'image_list' => $formatted_img_list,
                'is_liked' => $this->isLiked($album['gallery_id'], 'C_Gallery', $teacher_id)
            );
        }
        return array(
            'success' => true,
            'data' => [
                'gallery_list' => $gls
            ],
            'error_info' => throwApiError(1)
        );
    }
    function getNewsById(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');
        $validate_param = validateParam($args, ['news_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        //Get New
        global $timedate;
        $qGetNews = "SELECT DISTINCT IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin, 0) pin,
            IFNULL(c_news.addon_views, 0) addon_views,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.news_content, '') content,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            WHERE c_news.id = '{$args['news_id']}'";

        $result = $GLOBALS['db']->fetchOne($qGetNews);

        $qViewNews = "SELECT IFNULL(SUM(count_read_news) , 0)
            FROM c_news_contacts_1_c
            WHERE deleted = 0 AND c_news_contacts_1c_news_ida = '{$args['news_id']}'";
        $views = $GLOBALS['db']->getOne($qViewNews);
        $srcImg =  strlen($r['thumbnail']) != 36 ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] .  $result['thumbnail'] : $GLOBALS['dotb_config']['site_url'] . '/download_attachment.php?id=s3_storage/' .  $result['thumbnail'];
        //HOT FIX: Remove in future
        $periodPosition = strrpos($r['thumbnail'], '.');
        if ($periodPosition === false) {
            $periodPosition = strlen($result['thumbnail']);
        }
        $startPosition = max(0, $periodPosition - 36);
        $thumbnail_id = substr($result['thumbnail'], $startPosition, $periodPosition - $startPosition);
        //end
        $news = array();
        $news['id'] = $result['primaryid'];
        $news['name'] = $result['name'];
        $news['thumbnail'] = $thumbnail_id; // HOT FIX
        $news['thumbnail_url'] = $srcImg;
        $news['url'] = $result['url'];
        $news['type'] = $result['type'];
        $news['addon_views'] = $result['addon_views'];
        $news['content'] = $result['content'];
        $news['pin'] = $result['pin'];
        $news['description'] = $result['description'];
        $news['type_news'] = $result['type_news'];
        $news['date_entered'] = $timedate->to_display_date_time($result['date_entered']);
        $news['end_date'] = $timedate->to_display_date($result['end_date'], false);
        $news['start_date'] = $timedate->to_display_date($result['start_date'], false);
        $news['views'] = $views;

        return [
            'success' => true,
            'data' => [
                'news' => $news
            ],
            'error_info' => throwApiError(1)
        ];

    }

}
