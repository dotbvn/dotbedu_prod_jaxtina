<?php


class MobileApiV3 extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'load_branding_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','load-branding'),
                'pathVars' => array(''),
                'method' => 'loadBranding',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'post_token_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','post-token'),
                'pathVars' => array(''),
                'method' => 'post_token',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'update_custom_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','update-custom'),
                'pathVars' => array(''),
                'method' => 'updateCustom',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'change_password_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','change-password'),
                'pathVars' => array(''),
                'method' => 'change_password',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'reset_password_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','reset-password'),
                'pathVars' => array(''),
                'method' => 'reset_password',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'save_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','save-comment'),
                'pathVars' => array(''),
                'method' => 'saveComment',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_doc_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','get-doc'),
                'pathVars' => array(''),
                'method' => 'getDoc',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_storage_list_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','get-storage-list'),
                'pathVars' => array(''),
                'method' => 'getStorageList',
                'shortHelp' => '',
                'longHelp' => '',
                'noLoginRequired' => true,
            ),
            'get_link_share_item_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','share-storage-item'),
                'pathVars' => array(''),
                'method' => 'shareStorageItem',
                'shortHelp' => '',
                'longHelp' => '',
                'noLoginRequired' => true,
            ),
            'get_storage_item_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','get-storage-item'),
                'pathVars' => array(''),
                'method' => 'getStorageItem',
                'shortHelp' => '',
                'longHelp' => '',
                'noLoginRequired' => true,
            ),
            'save_gallery_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','system','save-gallery'),
                'pathVars' => array(''),
                'method' => 'saveGallery',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }

    function getStorageList(ServiceBase $api, $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['folder']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $result = $GLOBALS['db']->query("select name,value from config where name in ('onedrive_client_secret','onedrive_client_id','onedrive_username','onedrive_password','onedrive_org_id')");
        $settings = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $settings[$row['name']] = $row['value'];
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://onedrive.s.dotb.vn/msgraph/gets',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'c' => $settings['onedrive_client_id'],
                'o' => $settings['onedrive_org_id'],
                's' => $settings['onedrive_client_secret'],
                'u' => $settings['onedrive_username'],
                'p' => $settings['onedrive_password'],
                'f' => $args['folder']
            )
        ));
        $response = curl_exec($curl);
        $response = json_decode($response, 1);
        curl_close($curl);
        return [
            'success' => true,
            'data' => [
                'respone' => $response
            ],
            'error_info' => throwApiError(1)
        ];
    }

    function shareStorageItem(ServiceBase $api, $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $result = $GLOBALS['db']->query("select name,value from config where name in ('onedrive_client_secret','onedrive_client_id','onedrive_username','onedrive_password','onedrive_org_id')");
        $settings = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $settings[$row['name']] = $row['value'];
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://onedrive.s.dotb.vn/msgraph/share',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'c' => $settings['onedrive_client_id'],
                'o' => $settings['onedrive_org_id'],
                's' => $settings['onedrive_client_secret'],
                'u' => $settings['onedrive_username'],
                'p' => $settings['onedrive_password'],
                'f' => $args['id'],
            ),
        ));
        $response = curl_exec($curl);
        $response = json_decode($response, 1);
        curl_close($curl);
        return [
            'success' => true,
            'data' => [
                'respone' => $response
            ],
            'error_info' => throwApiError(1)
        ];
    }

    function getStorageItem(ServiceBase $api, $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $result = $GLOBALS['db']->query("select name,value from config where name in ('onedrive_client_secret','onedrive_client_id','onedrive_username','onedrive_password','onedrive_org_id')");
        $settings = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $settings[$row['name']] = $row['value'];
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://onedrive.s.dotb.vn/msgraph/get',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'c' => $settings['onedrive_client_id'],
                'o' => $settings['onedrive_org_id'],
                's' => $settings['onedrive_client_secret'],
                'u' => $settings['onedrive_username'],
                'p' => $settings['onedrive_password'],
                'f' => $args['id'],
            ),
        ));
        $response = curl_exec($curl);
        $response = json_decode($response, 1);
        curl_close($curl);
        return [
            'success' => true,
            'data' => [
                'respone' => $response
            ],
            'error_info' => throwApiError(1)
        ];
    }

    function loadBranding(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        global $dotb_config;
        $admin = new Administration();
        $admin->retrieveSettings();
        $color_config = $admin->settings['default_color_config'];
        $primary_color = $admin->settings['default_primary_color'];
        $second_color = $admin->settings['default_second_color'];
        $third_color = $admin->settings['default_third_color'];
        $hotline = substr($admin->settings['default_hotline'], 0, strlen($admin->settings['default_hotline']) - 1);
        $update_in_app = $admin->settings['default_update_in_app'];
        $show_news_views = $admin->settings['default_show_news_views'];
        $storage_method = $admin->settings['default_storage_config'];
        $enable_fees = $admin->settings['default_collect_tuition_fee'];
        $bank_name = $admin->settings['default_bank_name'];
        $account_name = $admin->settings['default_account_name'];
        $account_number = $admin->settings['default_account_number'];
        $vietqr_enable = $admin->settings['vietqr_enable'];
        $vietqr_bank = $admin->settings['vietqr_bank'];
        $vietqr_bank_number = $admin->settings['vietqr_bank_number'];
        $vietqr_bank_name = $admin->settings['vietqr_bank_name'];
        $vietqr_account_name = $admin->settings['vietqr_account_name'];
        $vietqr_bank_logo = $admin->settings['vietqr_bank_logo'];

        if (empty($dotb_config['brand_name']))
            return array(
                'success' => false,
                'error_info' => throwApiError(111)
            );
        else {
            if (empty($dotb_config['brand_color1']) && empty($dotb_config['brand_color2'])) {
                $dotb_config['brand_color1'] = '#10cdfc';
                $dotb_config['brand_color2'] = '#85ee95';
            } elseif (empty($dotb_config['brand_color1']) && !empty($dotb_config['brand_color2']))
                $dotb_config['brand_color1'] = $dotb_config['brand_color2'];
            elseif (!empty($dotb_config['brand_color1']) && empty($dotb_config['brand_color2']))
                $dotb_config['brand_color2'] = $dotb_config['brand_color1'];

            $logo_url = $GLOBALS['db']->getOne("SELECT value from config where category = 'system' and name = 'company_logo'");
            $companyLogo = !empty($logo_url) ? $logo_url : 'custom/include/images/company_logo.png';

            return array(
                'success' => true,
                'data' => [
                    'brand_name' => $dotb_config['brand_name'],
                    'brand_id' => $dotb_config['brand_id'],
                    'brand_logo' => $companyLogo,
                    'color_config' => $color_config,
                    'primary_color' => $primary_color,
                    'second_color' => $second_color,
                    'third_color' => $third_color,
                    'hotline' => $hotline,
                    'update_in_app' => $update_in_app,
                    'show_news_views' => $show_news_views,
                    'storage_method' => $storage_method,
                    'enable_fees' => $enable_fees,
                    'bank_name' => $bank_name,
                    'account_name' => $account_name,
                    'account_number' => $account_number,
                    'vietqr_enable' => $vietqr_enable,
                    'vietqr_bank' => $vietqr_bank,
                    'vietqr_bank_number' => $vietqr_bank_number,
                    'vietqr_bank_name' => $vietqr_bank_name,
                    'vietqr_account_name' => $vietqr_account_name,
                ],
                'error_info' => throwApiError(1)
            );
        }
    }

    function post_token(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['student_id', 'user_id', 'token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $studentId = $args['student_id'];          //Student
        $userId = $args['user_id'];             //Teacher
        $apps_type = $args['apps_type'];
        $token = $args['token'];
        $device_name = $args['device_name'];
        $method = $args['method'];

        if (empty($apps_type) || $apps_type == 'Student') {
            //Update Last Login
            $ext_date = "last_login = '" . $GLOBALS['timedate']->nowDb() . "'";

            //update device name
            $ext_dc = '';
            if (!empty($device_name)) $ext_dc = ", device_name = '$device_name'";

            //Get token
            $content = $GLOBALS['db']->getOne("SELECT IFNULL(portal_app_token, '') portal_app_token FROM contacts WHERE id = '$studentId'");

            $curToken = json_decode(html_entity_decode($content), true);
            $count_change = 0;

            if (!in_array($token, $curToken) && (empty($method) || $method == 'add')) {    //Add Token
                $curToken[] = $token;
                $count_change++;
            }

            if (($key = array_search($token, $curToken)) !== false && $method == 'delete') { //remove Token
                unset($curToken[$key]);
                $count_change++;
            }

            $ext_token = '';
            if ($count_change > 0) $ext_token = ", portal_app_token = '" . json_encode($curToken) . "'";

            //$GLOBALS['db']->query("UPDATE contacts SET $ext_date $ext_dc $ext_token WHERE id = '$studentId'");

            return array(
                'success' => true,
                'error_info' => throwApiError(1)
            );
        } elseif ($apps_type == 'Teacher') {
            //Get token
            $content = $GLOBALS['db']->getOne("SELECT IFNULL(portal_app_token, '') portal_app_token FROM users WHERE id = '$userId'");

            $curToken = json_decode(html_entity_decode($content), true);
            $count_change = 0;

            if (!in_array($token, $curToken) && (empty($method) || $method == 'add')) {    //Add Token
                $curToken[] = $token;
                $count_change++;
            }

            if (($key = array_search($token, $curToken)) !== false && $method == 'delete') { //remove Token
                unset($curToken[$key]);
                $count_change++;
            }

            $ext_token = '';
            if ($count_change > 0) $ext_token = ", portal_app_token = '" . json_encode($curToken) . "'";

            return array(
                'success' => true,
                'error_info' => throwApiError(1)

            );
        } else {
            return array(
                'success' => false,
                'error_info' => throwApiError(112)
            );
        }
    }

    function change_password(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['password', 'object_id', 'new_password']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $current_user;
        $current_user = new User();
        $current_user->retrieve_by_string_fields(array('user_name' => 'apps_admin'));

        $old_password = $args['password'];
        $apps_type = $args['apps_type'];
        $dont_check_old = $args['dont_check_old'];
        $object_id = $args['object_id'];
        $phone_mobile = $args['phone_mobile'];
        $email = $args['email'];

        if (empty($dont_check_old)) $dont_check_old = false;

        // Handle change pass STUDENT
        if (empty($apps_type) || $apps_type == 'Student') {
            $student = new Contact();
            if (!empty($object_id))
                $student->retrieve($object_id);
            elseif (!empty($phone_mobile))
                $student->retrieve_by_string_contains(array('phone_mobile' => $phone_mobile));
            elseif (!empty($email))
                $student->retrieve_by_string_fields(array('email1' => $email));

            if (!empty($student->id)) {
                //Check Old Password
                if (!$dont_check_old) {
                    $pwdCheck = false;
                    if (User::checkPassword($old_password, $student->portal_password) || md5($old_password) == $student->portal_password)
                        $pwdCheck = true;

                    if (!$pwdCheck)
                        return array(
                            'success' => false,
                            'error_info' => throwApiError(101),
                        );
                }

                //update new password
                $student->portal_password = md5($args['new_password']);
                $student->save();
                return array(
                    'success' => true,
                    'error_info' => throwApiError(1)
                );
            } else {
                return array(
                    'success' => false,
                    'error_info' => throwApiError(100),
                );
            }
        } elseif ($apps_type == 'Teacher') {
            // Handle change pass TEACHER
            $userT = new User();
            if (!empty($object_id))
                $userT->retrieve($object_id);
            elseif (!empty($phone_mobile))
                $userT->retrieve_by_string_contains(array('phone_mobile' => $phone_mobile));
            elseif (!empty($email))
                $userT->retrieve_by_string_fields(array('email1' => $email));
            if (!empty($userT->id)) {
                if (!$dont_check_old) {
                    $pwdCheck = false;
                    if (User::checkPassword($old_password, $userT->user_hash) || md5($old_password) == $userT->user_hash)
                        $pwdCheck = true;

                    if (!$pwdCheck)
                        return array(
                            'success' => false,
                            'error_info' => throwApiError(101),
                        );
                }
                //update new password
                $userT->user_hash = md5($args['new_password']);
                $userT->save();
                return array(
                    'success' => true,
                    'error_info' => throwApiError(1)
                );
            }
        } else {
            return array(
                'success' => false,
                'data' => [
                    'apps_type' => $apps_type,
                    'object_id' => $object_id,
                ],
                'error_info' => throwApiError(112)
            );
        }
    }

    function reset_password(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['portal_name']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $dotb_config;

        require_once('modules/C_SMS/SMS/sms.php');

        $apps_type = $args['apps_type'];
        $user_name = $args['portal_name']; //editted

        if (empty($apps_type) || $apps_type == 'Student') {
            //Check Username credential
            $q1 = "SELECT DISTINCT IFNULL(contacts.id, '') primaryid,
            IFNULL(contacts.team_id, '') team_id,
            IFNULL(contacts.phone_mobile, '') phone_mobile,
            IFNULL(l1.email_address, '') email
            FROM contacts
            LEFT JOIN email_addr_bean_rel l1_1 ON contacts.id = l1_1.bean_id
            AND l1_1.deleted = 0
            AND l1_1.bean_module = 'Contacts'
            AND l1_1.primary_address = 1
            LEFT JOIN
            email_addresses l1 ON l1.id = l1_1.email_address_id
            AND l1.deleted = 0
            WHERE (contacts.phone_mobile = '$user_name')
            AND (contacts.portal_active = '1')
            AND contacts.deleted = 0";
            $rows = $GLOBALS['db']->fetchArray($q1);

            $module_name = 'Contacts';

            if (count($rows) < 1) {
                return array(
                    'success' => false,
                    'error_info' => throwApiError(100),
                );
            }
        } elseif ($apps_type == 'Teacher') {
            //Check Username credential
            $q1 = "SELECT DISTINCT
            IFNULL(c_teachers.id, '') primaryid,
            IFNULL(c_teachers.full_teacher_name, '') name,
            IFNULL(c_teachers.first_name, '') first_name,
            IFNULL(c_teachers.team_id, '') team_id,
            IFNULL(c_teachers.phone_mobile, '') phone_mobile
            FROM c_teachers
            INNER JOIN users l1 ON c_teachers.user_id = l1.id AND l1.deleted = 0
            WHERE (((l1.user_name = '$user_name')
            AND (IFNULL(c_teachers.type, '') = 'Teacher')
            AND (IFNULL(l1.status, '') = 'Active')))
            AND c_teachers.deleted = 0";
            $rows = $GLOBALS['db']->fetchArray($q1);

            $module_name = 'C_Teachers';

            if (count($rows) < 1) {
                return array(
                    'success' => false,
                    'error_info' => throwApiError(100),
                );
            }
        }

        $randomid = mt_rand(1000, 9999);
        $message = $dotb_config['brand_name'] . ": DE CAP NHAT MAT KHAU, ma xac minh la $randomid.";
        $message .= "Ma co hieu luc trong 5 phut. KHONG chia se ma nay voi nguoi khac.";

        //Send SMS
        $sms = new sms();
        $result = (int)$sms->send_message($rows[0]['phone_mobile'], $message, $module_name, $rows[0]['primaryid'], '1', $rows[0]['team_id']);
        $status = 'RECEIVED';
        if ($result <= 0) $status = 'FAILED';

        if ($status == 'FAILED') {
            return array(
                'success' => false,
                'data' => [
                    'code' => $randomid,
                    'expired_date' => date('Y-m-d H:i:s', strtotime("+5 minutes " . date('Y-m-d H:i:s'))),
                ],
                'error_info' => throwApiError(113)
            );
        } else {
            return array(
                'success' => true,
                'data' => [
                    'code' => $randomid,
                    'expired_date' => date('Y-m-d H:i:s', strtotime("+5 minutes " . date('Y-m-d H:i:s'))),
                ],
                'error_info' => throwApiError(1)
            );
        }
    }

    function updateCustom(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['module_name', 'module_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $moduleName = $args['module_name'];
        $moduleId = $args['module_id'];
        unset($args['module_id']);
        unset($args['module_name']);

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        if ($moduleName == 'C_Attendance') {
            $moduleId = $GLOBALS['db']->getOne("SELECT IFNULL(c_attendance.id, '') FROM c_attendance
            WHERE c_attendance.meeting_id = '{$args['meeting_id']}'
              AND c_attendance.student_id = '{$args['student_id']}' AND deleted = 0");
        }
        $bean = BeanFactory::getBean($moduleName, $moduleId);
        foreach ($args as $field => $Val)
            $bean->$field = $Val;
        $bean->save();

        return array(
            'success' => true,
            'data' => [
                'bean_id' => $bean->id,
            ],
            'error_info' => throwApiError(1)
        );

    }


    // type = [student, teacher]
    function saveComment(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['value', 'feedback_id', 'type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $value = $args['value'];
        $feedbackId = $args['feedback_id'];
        $userType = $args['type'];
        //lay activities_id
        $q1 = "SELECT activities.id
        FROM activities WHERE activities.deleted = 0
        AND activities.parent_id = '$feedbackId'
        ORDER BY date_entered ASC";
        $parent_id = $GLOBALS['db']->getOne($q1);
        $commentBean = BeanFactory::newBean('Comments');
        $commentBean->parent_id = $parent_id;
        $commentBean->data = json_encode(['type' => $userType, 'value' => $value, 'tags' => array()]);
        $commentBean->save();
        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }

    function getConnection($storage = '')
    {

        require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';
        require_once 'custom/include/GoogleAPI/class.google.php';
        require_once 'custom/include/GoogleAPI/class.drive.php';

        if (empty($storage)) $storage = 'Google Drive';
        if ($storage == 'Google Drive') {
            $admin = new Administration();
            $admin->retrieveSettings();
            $gg = new PHPGoogle();
            if (empty($admin->settings['default_google_api_token'])) return array('success' => 0);
            $gg->setAccessToken($admin->settings['default_google_api_token']);
            $drive = new PHPDrive($gg->client);
            return array(
                'success' => 1,
                'connection' => $drive,
                'storage' => 'Google Drive'
            );
        } elseif ($storage == 'Amazon S3') {
            $AWS = new AWSHelper();
            //check connection S3
            if (!$AWS->getS3()) return array('success' => 0);
            return array(
                'success' => 1,
                'connection' => $AWS,
                'storage' => 'Amazon S3'
            );
        }
    }

    function getDoc(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['storage', 'doc_url']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $resultConnection = $this->getConnection($args['storage']);
        if ($resultConnection['success']) {
            if ($resultConnection['storage'] == 'Google Drive') {
                if (empty($args['doc_url']))
                    return array(
                        'success' => false,
                        'error_info' => throwApiError(114),
                    );
                $drive = $resultConnection['connection'];
                $data = $drive->getFilesInFolder(array("'" . $args['doc_url'] . "' in parents"), 'name_natural asc');
                return array(
                    'success' => true,
                    'data' => [
                        'doc' => $data
                    ],
                    'error_info' => throwApiError(1)
                );
            } elseif ($resultConnection['storage'] == 'Amazon S3') {
                $AWS = $resultConnection['connection'];
                $data = $AWS->getKey($args['doc_url']);
                if ($data['success'])
                    return array(
                        'success' => true,
                        'data' => [
                            'list_key' => $data['list_key']
                        ],
                        'error_info' => throwApiError(1)
                    );
                else return array(
                    'success' => false,
                    'error_info' => throwApiError(203),
                );
            }
        } else return array(
            'success' => false,
            'error_info' => throwApiError(201),
        );
    }

    function saveGallery(ServiceBase $api, $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $id = $args['id'];
        if ($id != '') {
            $bean = BeanFactory::getBean('C_Gallery', $id);
        } else {
            $bean = BeanFactory::getBean('C_Gallery');
        }

        unset($args['id']);

        //delete gallery
        if ($args['deleted'] == 1) {
            $bean->mark_deleted($bean->id);
            return array(
                'success' => true,
                'data' => [
                    'bean_id' => $bean->id,
                    'message' => 'Delete success',
                ],
                'error_info' => throwApiError(1)
            );
        }

        $message = '';
        $args['list_pic_id'] = array();
        if (!empty($_FILES['gallery_picture'])) {
            $count_picture = count($_FILES['gallery_picture']['name']);
            if ($count_picture > 0)
                for ($i = 0; $i < $count_picture; $i++) {
                    $beanNotes = BeanFactory::getBean('Notes');
                    $beanNotes->name = $_FILES['gallery_picture']['name'][$i];
                    $beanNotes->file_mime_type = $_FILES['gallery_picture']['type'][$i];
                    $beanNotes->file_ext = pathinfo($_FILES['gallery_picture']['name'][$i], PATHINFO_EXTENSION);
                    $beanNotes->file_size = $_FILES['gallery_picture']['size'][$i];
                    $beanNotes->filename = $_FILES['gallery_picture']['name'][$i];
                    $beanNotes->parent_type = 'C_Gallery';
                    $beanNotes->parent_id = $bean->id;
                    $beanNotes->save();
                    $savefile = 'upload/' . $beanNotes->id;
                    move_uploaded_file($_FILES['gallery_picture']['tmp_name'][$i], $savefile);
                    $args['list_pic_id'][] = $beanNotes->id;
                }
            else $message = 'No new images are added';
        } else $message = 'No new images are added';

        //delete picture in list remove (delete notes)
        if (!empty($args['list_pic_remove_id'])) {
            $noteIds = implode("','", $args['list_pic_remove_id']);
            $qDeletePicture = "UPDATE notes SET deleted=1 WHERE parent_type='C_Gallery' AND id IN('$noteIds') AND deleted=0";
            $GLOBALS['db']->query($qDeletePicture);
        }

        //save
        foreach ($args as $field => $Val)
            $bean->$field = $Val;
        $bean->save();

        //save tags
        // Getting Tag Field ID
        $tagField = $bean->getTagField();
        // Getting Tag Field Properties
        $tagFieldProperties = $bean->field_defs[$tagField];

        // Preparing the latest Tags to be sync with the record
        // Note: Already attached tags will be automatically removed from the record
        // If you want to keep some of the existing tags then you will need to keep them in the array
        $tags['tag'] = $args['tags'];
        // Building DotbFieldTag instance
        $DotbFieldTag = new DotbFieldTag('tag');
        // Passing the arguments to save the Tags
        $DotbFieldTag->apiSave($bean, $tags, $tagField, $tagFieldProperties);

        return array(
            'success' => true,
            'data' => [
                'bean_id' => $bean->id,
                'message' => $message,
            ],
            'error_info' => throwApiError(1)
        );
    }

}
