<?php


class StudentApiV3 extends DotbApi
{
    function registerApiRest()
    {
        return array(
            //Payment API
            'get_payment_info_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-paymentinfo'),
                'pathVars' => array(''),
                'method' => 'getPaymentInfo',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'request_payment_check_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'request-paymentcheck'),
                'pathVars' => array(''),
                'method' => 'requestPaymentCheck',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //Notifications API
            'list_notifications_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'list-notifications'),
                'pathVars' => array(''),
                'method' => 'listNotifications',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'count_unread_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'count-unread-noti'),
                'pathVars' => array(''),
                'method' => 'countUnreadNoti',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'update_notification_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'update-notification'),
                'pathVars' => array(''),
                'method' => 'updateNotification',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //Login API
            'student_login_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'login'),
                'pathVars' => array(''),
                'method' => 'studentLoginV3',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //Related information
            'feedback_list_v3_new' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-list-feedback'),
                'pathVars' => array(''),
                'method' => 'getListFeedBack',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'class_list_v3_2' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-list-class'),
                'pathVars' => array(''),
                'method' => 'getClassListV2',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'payment_list_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-payment-list'),
                'pathVars' => array(''),
                'method' => 'getPaymentList',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'news_list_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-news-list'),
                'pathVars' => array(''),
                'method' => 'getNewsList',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'gallery_list_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-gallery-list'),
                'pathVars' => array(''),
                'method' => 'getGalleryList',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //Student action API
            'update_student_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'update-info'),
                'pathVars' => array(''),
                'method' => 'updateStudentInfo',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'rate_lesson_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'rate-lesson'),
                'pathVars' => array(''),
                'method' => 'rateLesson',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'edit_student_avt_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'edit-avt'),
                'pathVars' => array(''),
                'method' => 'editStudentAvt',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            //End move from Mobile API
            //API for comment and like
            'create_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'create-comment'),
                'pathVars' => array(''),
                'method' => 'createComment',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'delete_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'delete-comment'),
                'pathVars' => array(''),
                'method' => 'deleteComment',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-comment-list'),
                'pathVars' => array(''),
                'method' => 'getCommentList',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'like_action_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'like'),
                'pathVars' => array(''),
                'method' => 'likeAction',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'unlike_action_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'unlike'),
                'pathVars' => array(''),
                'method' => 'unlikeAction',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'add_view_new_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'update-news-view'),
                'pathVars' => array(''),
                'method' => 'addNewsView',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_gallery_by_id_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-gallery-by-id'),
                'pathVars' => array(''),
                'method' => 'getGalleryById',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_new_by_id_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-news-by-id'),
                'pathVars' => array(''),
                'method' => 'getNewsById',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_payment_by_id_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-payment-by-id'),
                'pathVars' => array(''),
                'method' => 'getPaymentById',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'get_feedback_by_id_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3', 'students', 'get-feedback-by-id'),
                'pathVars' => array(''),
                'method' => 'getFeedbackById',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }
    function getNote($parent_id, $parent_type){
        //Get the image of each gallery
        $sqlNote = "SELECT IFNULL(l1.id, '') pic_id,
                        IFNULL(l1.name, '') name,   
                        IFNULL(l1.upload_id, '') upload_id,
                        IFNULL(l1.file_source, '') file_source,
                        IFNULL(l1.file_mime_type, '') file_mime_type,
                        IFNULL(l1.file_ext, '') file_ext
                        FROM notes l1 
                        WHERE l1.parent_id = '{$parent_id}' AND l1.parent_type = '{$parent_type}' AND l1.deleted = 0";
        $img_list = $GLOBALS['db']->fetchArray($sqlNote);
        $formatted_img_list = [];
        //Format img list
        foreach($img_list as $pic){
            $srcImg = $pic['file_source'] == 'S3'
                ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $pic['upload_id']
                : $GLOBALS['dotb_config']['site_url'] . '/download_attachment.php?path=s3_storage/'.$pic['name'];
            $fileType = explode('/', $pic['file_mime_type'])[0]; //image or video
            $formatted_img_list[] = array(
                'pic_id' => $pic['pic_id'],
                'file_source' => empty($pic['file_source']) ? 'local' : $pic['file_source'],
                'file_type' => $fileType,
                'file_ext' => $pic['file_ext'],
                'picture_url' => $srcImg
            );
        }
        return $formatted_img_list;
    }
    function getListGallery($classIds, $str_student_id = '')
    {
        $ext1 = '';
        $ext2 = '';
        if (!empty($str_student_id)) {
            $ext1 = "INNER JOIN j_studentsituations l4 ON
            j_class.id = l4.ju_class_id AND l4.deleted = 0
            INNER JOIN contacts l5 ON l4.student_id = l5.id
            AND l5.deleted = 0 AND l4.student_type = 'Contacts'";
            $ext2 = "AND (IFNULL(l4.type, '') IN ('OutStanding' , 'Enrolled', 'Demo'))
            AND (l4.start_study <= DATE(l1.gallery_date))
            AND (l4.end_study >= DATE(l1.gallery_date))
            AND l5.id = '$str_student_id'
            AND (IFNULL(l5.status, '') NOT IN ('Cancelled'))";
        }
        $sqlAlbum = "SELECT DISTINCT
        IFNULL(j_class.id, '') class_id,
        IFNULL(j_class.name, '') class_name,
        IFNULL(l1.id, '') gallery_id,
        IFNULL(l1.name, '') gallery_name,
        IFNULL(l1.gallery_date, '') gallery_date,
        IFNULL(l1.description, '') gallery_description,
        IFNULL(l1.gallery_type, '') gallery_type,
        IFNULL(l2.id, '') pic_id,
        IFNULL(l2.name, '') note_name,
        IFNULL(l2.upload_id, '') upload_id,
        IFNULL(l2.file_source, '') file_source,
        IFNULL(l2.file_mime_type, '') file_mime_type,
        IFNULL(l1.date_entered, '') date_entered
        FROM j_class
        INNER JOIN j_class_c_gallery_1_c l1_1 ON j_class.id = l1_1.j_class_c_gallery_1j_class_ida AND l1_1.deleted = 0
        INNER JOIN c_gallery l1 ON l1.id = l1_1.j_class_c_gallery_1c_gallery_idb AND l1.deleted = 0
        INNER JOIN notes l2 ON l1.id = l2.parent_id AND l2.deleted = 0 AND l2.parent_type = 'C_Gallery'
        {$ext1}
        WHERE j_class.id IN ('" . implode("','", $classIds) . "') AND j_class.deleted = 0
        {$ext2}
        GROUP BY pic_id
        ORDER BY date_entered DESC";
        $albums = $GLOBALS['db']->query($sqlAlbum);
        $gls = array();
        while ($album = $GLOBALS['db']->fetchbyAssoc($albums)) {
            if (empty($album['class_id'])) break;
            $srcImg = $album['file_source'] == 'S3'
                ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $album['upload_id']
                : $GLOBALS['dotb_config']['site_url'].'/upload/s3_storage/'.$album['note_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_id'] = $album['gallery_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_id'] = $album['class_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_name'] = $album['class_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_name'] = $album['gallery_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_description'] = $album['gallery_description'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_type'] = $album['gallery_type'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_date'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $album['gallery_date']));
            $gls[$album['class_id']][$album['gallery_id']]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered']));
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['picture_id'] = $album['pic_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['google_id'] = $album['upload_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['google_url'] = $srcImg;
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['file_mime_type'] = $album['file_mime_type'];
            $gls[$album['class_id']][$album['gallery_id']]['images'][] = $album['google_url'];
            if (!empty($album['tag_name']))
                $gls[$album['class_id']][$album['gallery_id']]['tags'] = explode(",", $album['tag_name']);
            else $gls[$album['class_id']][$album['gallery_id']]['tags'] = array();
        }
        foreach ($gls as $class_id => $album) {
            foreach ($album as $gallery_id => $albumVal) {
                foreach ($albumVal['image_list'] as $pic_id => $picVal) {
                    $gls[$class_id][$gallery_id]['image_list'] = array_values($gls[$class_id][$gallery_id]['image_list']);
                }
            }
        }
        return $gls;
    }

    function studentLoginV3(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        global $timedate;
        $validate_param = validateParam($args, ['portal_name', 'password']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $user_name = $args['portal_name'];
        $password = $args['password'];

        //Check Username credential
        $q1 = "SELECT DISTINCT IFNULL(contacts.id, '') primaryid,
        IFNULL(contacts.first_name, '') first_name,
        IFNULL(contacts.last_name, '') last_name,
        IFNULL(contacts.full_student_name, '') name,
        IFNULL(contacts.contact_id, '') student_id,
        contacts.birthdate birthdate,
        IFNULL(contacts.status, '') status,
        IFNULL(contacts.phone_mobile, '') phone_mobile,
        IFNULL(contacts.picture, '') picture,
        IFNULL(contacts.cover_app, '') cover_app,
        IFNULL(l1.email_address, '') email,
        IFNULL(contacts.nick_name, '') nick_name,
        IFNULL(contacts.guardian_name, '') guardian_name,
        IFNULL(contacts.relationship, '') relationship,
        IFNULL(contacts.lead_source, '') lead_source,
        IFNULL(contacts.utm_source, '') utm_source,
        IFNULL(contacts.gender, '') gender,
        IFNULL(contacts.facebook, '') facebook,
        IFNULL(contacts.primary_address_street, '') address,
        IFNULL(contacts.primary_address_postalcode, '') address_postalcode,
        IFNULL(contacts.primary_address_state, '') address_state,
        IFNULL(contacts.primary_address_city, '') address_city,
        IFNULL(contacts.primary_address_country, '') address_country,
        IFNULL(contacts.assigned_user_id, '') assigned_to_user_id,
        IFNULL(contacts.team_id, '') team_id,
        IFNULL(l2.name, '') team_name,
        IFNULL(contacts.team_set_id, '') team_set_id,
        IFNULL(contacts.portal_name, '') portal_name,
        IFNULL(contacts.portal_password, '') portal_password,
        IFNULL(contacts.phone_guardian, '') phone_guardian,
        IFNULL(contacts.guardian_name_2, '') guardian_name_2,
        IFNULL(contacts.relationship2, '') relationship2,
        IFNULL(contacts.other_mobile, '') phone_guardian_2,
        contacts.lasted_view_noti lasted_view_noti
        FROM contacts
        LEFT JOIN email_addr_bean_rel l1_1 ON contacts.id = l1_1.bean_id
        AND l1_1.deleted = 0
        AND l1_1.bean_module = 'Contacts'
        AND l1_1.primary_address = 1
        LEFT JOIN
        email_addresses l1 ON l1.id = l1_1.email_address_id
        AND l1.deleted = 0
        LEFT JOIN teams l2 ON contacts.team_id = l2.id
        AND l2.deleted = 0
        WHERE (contacts.portal_name = '$user_name'
        OR contacts.phone_mobile = '$user_name')
        AND (contacts.portal_active = '1')
        AND contacts.deleted = 0
        ORDER BY first_name ASC";

        $students = $GLOBALS['db']->fetchArray($q1);
        if (count($students) == 0)
            return array(
                'success' => false,
                'error' => throwApiError(100),
            );

        $student_list = array();
        //Check Password credential
        $pwdCheck = false;

        $OauthKeys = new OAuthKey();
        $OauthKeys->getByKey('dotb-sea-v2', 'oauth2');
        $token = new OAuthToken();
        $createToken = false;
        if (empty($args['access_token'])) {
            $createToken = true;
        }
        $access_token_list = [];

        ////////////
        ///
        foreach ($students as $student) {
            if ((User::checkPassword($password, $student['portal_password']) || md5($password) == $student['portal_password']) && !$pwdCheck)
                $pwdCheck = true;
        }
        foreach ($students as $student) {
            if ($createToken && $pwdCheck) {
                $access = $token->createAuthorizedForMobile($OauthKeys, $student['primaryid'], $api_user);
            }

            //add student array
            $student['birthdate'] = $timedate->to_display_date($student['birthdate'], false);//format birthdate
            $student_list[$student['primaryid']] = $student;

            $student_list[$student['primaryid']]['school_name'] = '';
            $student_list[$student['primaryid']]['loyalty_point'] = 0;
            $student_list[$student['primaryid']]['total_payment'] = 0;
            $student_list[$student['primaryid']]['unread_noti'] = 0;
            $student_list[$student['primaryid']]['sms'] = array();
            $student_list[$student['primaryid']]['loyalty'] = array();
            $student_list[$student['primaryid']]['access_token'] = $createToken ? $access->id : $args['access_token'];
            $access_token_list[] = $createToken ? $access->id : $args['access_token'];
        }

        if (!$pwdCheck)
            return array(
                'success' => false,
                'error' => throwApiError(101),
            );

        $str_student_id = implode("','", array_column($students, 'primaryid'));

        //get Student ID login and push array
        $arrStudentId = array_column($students, 'primaryid');

        //get school
        $qgetschool = "SELECT DISTINCT
        IFNULL(contacts.id, '') student_id,
        IFNULL(l0.name, '') school_name
        FROM contacts
        INNER JOIN j_school l0 ON contacts.school_id = l0.id AND l0.deleted = 0
        WHERE (contacts.id IN ('$str_student_id')) AND contacts.deleted = 0";
        $getschoolstudent = $GLOBALS['db']->query($qgetschool);
        while ($res = $GLOBALS['db']->fetchbyAssoc($getschoolstudent)) {
            $student_list[$res['student_id']]['school_name'] = $res['school_name'];
        }

        //Get Loyalty
        $q6 = "SELECT DISTINCT
        IFNULL(l1.id, '') student_id,
        IFNULL(l2.id,'') payment_id,
        IFNULL(l3.id,'') receipt_id,
        IFNULL(l2.name,'') payment_code,
        IFNULL(j_loyalty.id, '') primaryid,
        j_loyalty.input_date date,
        j_loyalty.exp_date exp_date,
        IFNULL(j_loyalty.type, '') type,
        j_loyalty.point point,
        j_loyalty.rate_in_out rate_in_out,
        IFNULL(j_loyalty.name, '') name,
        j_loyalty.description description
        FROM j_loyalty
        INNER JOIN contacts l1 ON j_loyalty.student_id = l1.id AND l1.deleted = 0
        LEFT JOIN  j_payment l2 ON j_loyalty.payment_id=l2.id AND l2.deleted=0
        LEFT JOIN  j_paymentdetail l3 ON l3.id =j_loyalty.paymentdetail_id AND l3.deleted=0
        WHERE
        (((l1.id IN ('$str_student_id'))))
        AND j_loyalty.deleted = 0
        ORDER BY student_id ASC, input_date DESC";
        $rs = $GLOBALS['db']->query($q6);
        $key = 0;
        $runStudent = '####';
        while ($r = $GLOBALS['db']->fetchbyAssoc($rs)) {
            if ($runStudent != $r['student_id']) $key = 0;

            $loyalty = array();
            $loyalty['loyalty_id'] = $r['primaryid'];
            $loyalty['receipt_id'] = $r['receipt_id'];
            $loyalty['payment_id'] = $r['payment_id'];
            $loyalty['payment_code'] = $r['payment_code'];
            $loyalty['date'] = $timedate->to_display_date($r['date'], false);
            $loyalty['exp_date'] = $timedate->to_display_date($r['exp_date'], false);
            $loyalty['type'] = $r['type'];
            $loyalty['point'] = $r['point'];
            $loyalty['amount'] = format_number(abs($r['point'] * $r['rate_in_out']));
            $loyalty['name'] = $r['name'];
            $loyalty['description'] = $r['description'];

            $student_list[$r['student_id']]['loyalty_point'] += $r['point'];
            $student_list[$r['student_id']]['loyalty'][$key] = $loyalty;
            $runStudent = $r['student_id'];
            $key++;
        }
        //handle count notifications Unread - BY Hoàng Huy
        $res = $this::countUnreadNoti($api, array('access_token' => $access_token_list));
        if ($res['success']) {
            foreach ($student_list as $student_id => $student) {
                $student_list[$student_id]['unread_noti'] = intVal($res['data']['unread_noti_list'][$student_id]);
            }
        }
        //END: - BY Hoàng Huy

        $language_options = array('case_status_dom', 'case_sub_type_dom', 'status_class_list', 'attendance_type_list', 'do_homework_list', 'ss_status_list', 'relationship_list', 'address_city_2_options', 'address_state_2_options', 'gallery_type_list', 'reconcile_status_list'); //mảng chứa các options trong app_list_strings
        //Set App_String
        $app_strings = array(
            'en' => parseAppListString('en_us', $language_options),
            'vn' => parseAppListString('vn_vn', $language_options),
        );
        //Cập nhật thời gian mỗi lần gọi api login cho database (last access)
        $dtlastaccess = date('Y-m-d H:i:s', strtotime("-7 hours"));
        foreach ($arrStudentId as $key => $valueid) {
            $qr = "UPDATE contacts
            SET contacts.last_login = '$dtlastaccess' WHERE contacts.id = '$valueid'";
            $GLOBALS['db']->query($qr);
        }

        //Return
        return array(
            'success' => true,
            'data' => [
                'portal_name' => $user_name,
                'password' => $password,
                'student_list' => array_values($student_list),
                'app_strings_en' => $app_strings['en'],
                'app_strings_vn' => $app_strings['vn'],
            ],
            'error_info' => throwApiError(1)
        );
    }

    function updateStudentInfo(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $bean = BeanFactory::getBean('Contacts', $student_id);
        foreach ($args['field'] as $field => $Val){
            $bean->$field = $Val;
        }
        $bean->save();

        return array(
            'success' => true,
            'data' => [
                'student_id' => $bean->id,
            ],
            'error_info' => throwApiError(1)
        );

    }
    function rateLesson(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'meeting_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $session = BeanFactory::getBean('Meetings', $args['meeting_id']);
        if(!empty($session->id)){
            if(empty($args['attendance_id'])){
                $attendanceBean             = new C_Attendance();
                $attendanceBean->name       = 'create|by_manual';
                $attendanceBean->student_id = $student_id;
                $attendanceBean->student_type = 'Contacts';
                $attendanceBean->star_rating = $args['star_rating'];
                $attendanceBean->customer_comment = $args['customer_comment'];
                $attendanceBean->meeting_id = $session->id;
                $attendanceBean->class_id   = $session->ju_class_id;
                $attendanceBean->date_input = $GLOBALS['timedate']->to_display_date($session->date_start);
                $class = BeanFactory::getBean('J_Class', $session->ju_class_id);
                $attendanceBean->team_id = $class->team_id;
                $attendanceBean->team_set_id = $attendanceBean->team_id;
                $attendanceBean->save();
            } else {
                $attendanceBean = BeanFactory::getBean('C_Attendance', $args['attendance_id']);
                $attendanceBean->student_type = 'Contacts';
                $attendanceBean->star_rating = $args['star_rating'];
                $attendanceBean->customer_comment = $args['customer_comment'];
                $attendanceBean->student_id = $student_id;
                $attendanceBean->meeting_id = $session->id;
                $attendanceBean->save();
            }
        }
        return array(
            'success' => true,
            'error_info' => throwApiError(1)
        );
    }

    //Get transfer and payment + VietQR Info:
    function getPaymentInfo(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['receipt_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $receipt = BeanFactory::getBean('J_PaymentDetail', $args['receipt_id'], array('disable_row_level_security' => true));
        if ($receipt->status == 'Unpaid') $vietQR = getVietQR($receipt);
        return array(
            'success' => true,
            'data' => [
                'id' => $receipt->id,
                'payment_amount' => $receipt->payment_amount,
                'status' => $receipt->status,
                'payment_datetime' => $receipt->payment_datetime,
                'payment_date' => $receipt->payment_date,
                'payment_method' => $receipt->payment_method,
                'bank_account' => $receipt->bank_account,
                'audited_amount' => $receipt->audited_amount,
                'reconcile_status' => $receipt->reconcile_status,
                'vietQR' => $vietQR,
            ],
            'error_info' => throwApiError(1)

        );
    }

    //Check payment status
    function requestPaymentCheck(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['receipt_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $receipt = BeanFactory::getBean('J_PaymentDetail', $args['receipt_id'], array('disable_row_level_security' => true));
        //Xử lý realtime check
        if ($receipt->status == 'Unpaid') {
            $receipt->reconcile_status = 'processing';
            $receipt->save();
        }
        return array(
            'success' => true,
            'data' => [
                'id' => $receipt->id,
                'payment_amount' => $receipt->payment_amount,
                'status' => $receipt->status,
                'payment_datetime' => $receipt->payment_datetime,
                'payment_date' => $receipt->payment_date,
                'payment_method' => $receipt->payment_method,
                'bank_account' => $receipt->bank_account,
                'audited_amount' => $receipt->audited_amount,
                'reconcile_status' => $receipt->reconcile_status
            ],
            'error_info' => throwApiError(1)
        );
    }

    //List unread notification divide page - Hoang Huy
    function listNotifications(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['type', 'page', 'access_token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $type_noti = $args['type'];
        $channel_list = array(
            'news' => array('C_News', 'C_Gallery'),
            'chat' => array('Contacts', 'Leads', 'Cases', 'C_Comments', 'Notifications'),
            'payment' => array('J_PaymentDetail', 'J_Payment'),
            'course' => array('C_Attendance', 'J_Class', 'J_Gradebook', 'J_GradebookDetail')
        );
        $ext = "";
        if ($args['type'] != 'all' && !empty($args['type'])) {
            if ($type_noti == 'news') {
                $str_type = 'C_Gallery';
            } else {
                $str_type = implode("','", $channel_list[$type_noti]);
            }
            $ext = "AND nt.parent_type IN ('{$str_type}')";
        }
        //Get the page from the mobile app to set LIMIT
        $limit = (!empty($args['page'])) ? 30 * $args['page'] : 30;
        $limit_bot = $limit - 30;
        $limit_top = $limit;
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }


        $args['update_time'] = empty($args['update_time']) ? "update" : $args['update_time'];
        //When click to the bell we will get the lasted view of students
        if ($limit == 30 && $type_noti == 'all' && $args['update_time'] == "update") { //First Page
            $time = date('Y-m-d H:i:s', strtotime($GLOBALS['timedate']->nowDb()));
            $GLOBALS['db']->query("UPDATE contacts SET lasted_view_noti = '{$time}' WHERE id = '{$student_id}' AND deleted = 0");
        }

        //Query to get the team of students
        $q1 = "SELECT IFNULL(team_id, '') team_id
        FROM contacts ct
        WHERE ct.id = '{$student_id}' AND ct.deleted = 0";
        $team_id = $GLOBALS['db']->getOne($q1);
        //Build query
        //Query notification
        $query2 = "SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        WHERE nt.deleted = 0
        AND nt.assigned_user_id = '{$student_id}'
        {$ext}
        ORDER BY nt.date_entered DESC
        LIMIT $limit_bot, $limit_top";
        //Query c_news
        $query3 = "SELECT DISTINCT
        IFNULL(nt.id, '') primaryid,
        IFNULL(nt.assigned_user_id, '') student_id,
        IFNULL(nt.name, '') title,
        IFNULL(nt.is_read, 0) is_read,
        IFNULL(nt.description, '') body,
        IFNULL(nt.parent_id, '') record_id,
        IFNULL(nt.parent_type, '') module_name,
        IFNULL(nt.created_by, '') created_by,
        nt.date_entered date_entered,
        IFNULL(nt.parent_type, '') parent_type,
        IFNULL(nt.parent_id, '') parent_id
        FROM notifications nt
        WHERE (nt.team_set_id = '{$team_id}' OR nt.team_set_id = '1') AND nt.parent_type = 'C_News' AND nt.assigned_user_id = 'all' AND nt.apps_type LIKE '%Student%' AND nt.deleted = 0
        ORDER BY nt.date_entered DESC
        LIMIT $limit_bot, $limit_top";

        $q4 = "SELECT DISTINCT
        IFNULL(c_news.id, '') news_id,
        IFNULL(l1.id, '') student_id,
        CASE WHEN IFNULL(l1.id, '') = '' THEN '0' ELSE 1 END is_read
        FROM c_news
        LEFT JOIN c_news_contacts_1_c l1_1 ON c_news.id = l1_1.c_news_contacts_1c_news_ida AND l1_1.deleted = 0
        LEFT JOIN contacts l1 ON l1.id = l1_1.c_news_contacts_1contacts_idb AND l1.deleted = 0
        WHERE l1.id = '{$student_id}' AND c_news.deleted = 0
        AND (c_news.type LIKE '%Student%')";

        if ($type_noti == "all" || $type_noti == "news") {
            $q2 = "SELECT nttt.* FROM (($query2) UNION ($query3)) nttt ORDER BY date_entered DESC LIMIT $limit_bot, $limit_top";
            $row1 = $GLOBALS['db']->query($q2);
        } else {
            $row1 = $GLOBALS['db']->query($query2);
        }

        //List of news that the student read
        $read_news = array();
        if ($type_noti == "all" || $type_noti == "news") {
            $row2 = $GLOBALS['db']->fetchArray($q4);
            foreach ($row2 as $_key => $row) $read_news[$row['news_id']] = $row['is_read'];
        }
        //The target of make this read news list is to make sense that which is the notification about news that the student read

        //Loop through all notifications and push it into $notification_list
        $count_noti = 0;
        while ($r = $GLOBALS['db']->fetchByAssoc($row1)) {
            //get notification All
            $nt = array();
            $nt['primaryid'] = $r['primaryid'];
            $nt['title'] = $r['title'];
            $nt['body'] = $r['body'];
            $nt['is_read'] = ($r['parent_type'] == "C_News") ? boolVal($read_news[$r['parent_id']]) : boolVal($r['is_read']);
            $nt['module_name'] = $r['module_name'];
            $nt['record_id'] = $r['record_id'];
            $nt['created_by'] = $r['created_by'];
            $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));
            $notification_list[] = $nt;
            $count_noti++;
        }
        $can_load = true;
        if ($count_noti < 30 || empty($notification_list)) {
            $can_load = false;
        }
        return array(
            'success' => true,
            'data' => [
                'notification_list' => $notification_list,
                'can_load' => $can_load
            ],
            'error_info' => throwApiError(1)

        );
    }

    //Count Unread Notification - Hoang Huy
    function countUnreadNoti(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        $unread_noti_list = array();
        foreach ($args['access_token'] as $access_token) {
            $student_id = $this->getStudentFromAccessToken($access_token);
            if (empty($student_id)) {
                return [
                    'success' => false,
                    'error_info' => throwApiError(105)
                ];
            }
            $q7 = "SELECT COUNT(id) FROM notifications WHERE assigned_user_id = '{$student_id}' AND is_read = 0 AND deleted = 0";
            $unread_noti_list[$student_id] = intVal($GLOBALS['db']->getOne($q7));
        }
        return array(
            'success' => true,
            'data' => [
                'unread_noti_list' => $unread_noti_list,
            ],
            'error_info' => throwApiError(1)
        );
    }

    //Update Notification Read/Unread/Delete  - Hoang Huy
    function updateNotification(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        //Get noti beans
        $validate_param = validateParam($args, ['access_token', 'action_type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $noti = BeanFactory::getBean('Notifications', $args['noti_id']);
        $cnew_id = $noti->parent_type == 'C_News' ? $noti->parent_id : "";
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        if ($noti->parent_type == "C_News" && !empty($cnew_id))
            $new = BeanFactory::getBean('C_News', $cnew_id);
        switch ($args['action_type']) {
            case 'read':
                if ($noti->parent_type == "C_News" && !empty($cnew_id)) {
                    if (!empty($new->id) && $new->load_relationship('c_news_contacts_1')) {
                        //If the student haven't read the news that is the parent_id of notifications we will add the new relation
                        //On the other side we will count the view of new by 1
                        $new->c_news_contacts_1->add($student_id);
                        $GLOBALS['db']->query("UPDATE c_news SET addon_views = addon_views + 1 WHERE deleted = 0 AND id = '{$cnew_id}'");
                    }
                } elseif (!$noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'unread':
                if ($noti->parent_type == "C_News" && !empty($cnew_id)) {
                    //Incase unread we will need to delete the relationship existing
                    if (!empty($new->id) && $new->load_relationship('c_news_contacts_1'))
                        $new->c_news_contacts_1->delete($cnew_id, $student_id);
                } elseif ($noti->is_read)
                    $GLOBALS['db']->query("UPDATE notifications SET is_read = 0 WHERE id = '{$noti->id}' AND deleted = 0");
                break;
            case 'delete':
                //the student only can delete the notification that relate to them and cant delete the notification about news
                if ($noti->parent_type != 'C_News') $noti->mark_deleted($noti->id);
                break;
            case 'markallread':
                //Mark All Read Notification
                $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE assigned_user_id = '$student_id' AND is_read = 0 AND deleted=0");
                //Read all news
                //Step1: get all the news that the students have read
                $q1 = "SELECT IFNULL(c_news_contacts_1c_news_ida, '') news_id FROM c_news_contacts_1_c crl WHERE crl.deleted = 0 AND crl.c_news_contacts_1contacts_idb = '{$student_id}'";
                $row1 = $GLOBALS['db']->fetchArray($q1);

                //Step2: convert $read_news array to string to query get all the news that doesn't exist in our read list
                $str_read_news = (!empty($row1)) ? implode("','", array_column($row1, 'news_id')) : "";
                $q2 = "SELECT IFNULL(id, '') news_id FROM c_news WHERE deleted = 0 AND id NOT IN ('$str_read_news') AND type LIKE '%Student%'";
                $row2 = $GLOBALS['db']->fetchArray($q2);
                $date_modified = $GLOBALS['timedate']->nowDb(); //Get time at now
                foreach ($row2 as $r2) {
                    //Count the relationship that was deleted
                    $countRelationDeleted = $GLOBALS['db']->getOne("SELECT COUNT(id)
                        FROM c_news_contacts_1_c
                        WHERE c_news_contacts_1c_news_ida = '{$r2['news_id']}'
                        AND c_news_contacts_1contacts_idb = '{$student_id}' AND deleted = 1");
                    if ($countRelationDeleted > 0) {
                        //If exist the deleted realtionship we will update deleted field to 1 instead of create new record
                        $qUpdateRelation = "UPDATE c_news_contacts_1_c
                        SET deleted = 0, date_modified ='{$date_modified}'
                        WHERE deleted = 1
                        AND c_news_contacts_1c_news_ida = '{$r2['news_id']}'
                        AND c_news_contacts_1contacts_idb = '{$student_id}'";
                        $GLOBALS['db']->query($qUpdateRelation);
                    } else {
                        //If doesn't exist the deleted relationship we will add new relationship
                        $relation_id = create_guid();
                        $qInsertRelation = "INSERT INTO c_news_contacts_1_c (id, date_modified, deleted, c_news_contacts_1c_news_ida, c_news_contacts_1contacts_idb, count_read_news)
                        VALUES ('{$relation_id}','{$date_modified}', 0,'{$r2['news_id']}','{$student_id}', 0)";
                        $GLOBALS['db']->query($qInsertRelation);
                    }
                }
                break;
        }
        return array(
            'success' => true,
            'error_info' => throwApiError(1)
        );
    }

    function getListFeedBack(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'page']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $limit = (!empty($args['page'])) ? 20 * $args['page'] : 20;
        $limit_bot = $limit - 20;
        $limit_top = $limit;
        //Get Feedback
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $q4 = "SELECT DISTINCT
                IFNULL(cases.id, '') primaryid,
                IFNULL(cases.name, '') subject,
                IFNULL(l3.id, '') student_id, IFNULL(l3.full_student_name, '') student_name,
                IFNULL(l2.id, '') class_id, IFNULL(l2.name, '') class_name,
                IFNULL(cases.case_number, '') case_number,
                IFNULL(cases.type, '') type,
                IFNULL(cases.status, '') case_status,
                IFNULL(cases.rate, '') rate,
                cases.date_entered date_entered,
                IFNULL(cases.description, '') feedback_content,
                IFNULL(l4.id, '') assigned_to_id, IFNULL(l4.full_user_name, '') assigned_to,
                IFNULL(l5.id, '') create_by, IFNULL(l5.full_user_name, '') create_by_name,
                IFNULL(l5.picture, '') created_by_avatar,
                IFNULL(l1.id, '') center_id,
                IFNULL(l1.name, '') center
                FROM cases
                INNER JOIN teams l1 ON cases.team_id = l1.id AND l1.deleted = 0
                LEFT JOIN j_class_cases_1_c l2_1 ON cases.id = l2_1.j_class_cases_1cases_idb AND l2_1.deleted = 0
                LEFT JOIN j_class l2 ON l2.id = l2_1.j_class_cases_1j_class_ida AND l2.deleted = 0
                LEFT JOIN contacts_cases_1_c l3_1 ON cases.id = l3_1.contacts_cases_1cases_idb AND l3_1.deleted = 0
                LEFT JOIN contacts l3 ON l3.id = l3_1.contacts_cases_1contacts_ida AND l3.deleted = 0
                LEFT JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
                LEFT JOIN users l5 ON cases.created_by = l5.id AND l5.deleted = 0
                WHERE (((l3.id = '{$student_id}')))
                AND cases.portal_viewable = 1
                AND cases.deleted = 0
                GROUP BY cases.id
                ORDER BY l3.id ASC, CASE";
        $fb_status = $GLOBALS['app_list_strings']['case_status_dom'];
        $i = 1;
        foreach ($fb_status as $_status => $val)
            $q4 .= " WHEN case_status = '$_status' THEN " . $i++;
        $q4 .= " ELSE $i END ASC, last_comment_date DESC";
        $q4 .= " \nLIMIT $limit_bot, $limit_top";
        $rs4 = $GLOBALS['db']->fetchArray($q4);

        //Get Last Feedback Cmt
        $fbIds = implode("','", array_column($rs4, 'primaryid'));
        $q9 = "SELECT cs.*
                FROM c_comments cs, (SELECT  cc.parent_id parent_id, max(cc.date_entered) max_date_entered
                FROM c_comments cc
                WHERE cc.deleted = 0 AND cc.parent_id IN ('$fbIds') AND cc.parent_type = 'Cases' AND cc.is_unsent = 0
                GROUP BY cc.parent_id) cc_ma
                WHERE (cs.parent_id = cc_ma.parent_id) AND (cs.parent_type = 'Cases')
                AND (cc_ma.max_date_entered = cs.date_entered) AND (cs.parent_id IN ('$fbIds'))
                GROUP BY cs.id";
        $rs9 = $GLOBALS['db']->fetchArray($q9);
        $fb_last = array();
        foreach ($rs9 as $ind => $r9) {
            $row_u = $GLOBALS['db']->fetchOne("SELECT IFNULL(full_user_name, '') full_user_name, IFNULL(picture, '') created_by_avatar FROM users WHERE id = '{$r9['created_by']}'");
            $fb_last[$r9['parent_id']]['lcmt_content'] = $r9['description'];
            $fb_last[$r9['parent_id']]['lcmt_datetime'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r9['date_entered']));
            $fb_last[$r9['parent_id']]['lcmt_created_by_name'] = $row_u['full_user_name'];
            $fb_last[$r9['parent_id']]['lcmt_created_by'] = $r9['created_by'];
            $fb_last[$r9['parent_id']]['lcmt_created_by_avatar'] = $row_u['created_by_avatar'];
            $fb_last[$r9['parent_id']]['lcmt_unread_app'] = (($r9['is_read_inapp'] == 0) ? '1' : '0');
            $fb_last[$r9['parent_id']]['lcmt_unread_ems'] = (($r9['is_read_inems'] == 0) ? '1' : '0');
        }

        $data = [];
        $count_feedback = 0;
        foreach ($rs4 as $_ind => $r4) {
            $feedback = array();
            $feedback['number'] = $r4['case_number'];
            $feedback['feedback_id'] = $r4['primaryid'];
            $feedback['subject'] = $r4['subject'];
            $feedback['class_id'] = $r4['class_id'];
            $feedback['class_name'] = $r4['class_name'];
            $feedback['type'] = $r4['type'];
            $feedback['case_status'] = $r4['case_status'];
            $feedback['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r4['date_entered']));
            $feedback['feedback_content'] = $r4['feedback_content'];
            $feedback['assigned_to_id'] = $r4['assigned_to_id'];
            $feedback['assigned_to'] = $r4['assigned_to'];
            $feedback['create_by'] = $r4['create_by'];
            $feedback['create_by_name'] = $r4['create_by_name'];
            $feedback['created_by_avatar'] = $r4['created_by_avatar'];
            $feedback['center_id'] = $r4['center_id'];
            $feedback['center'] = $r4['center'];
            $feedback['rate'] = $r4['rate'];
            $feedback['last_comment'] = $fb_last[$r4['primaryid']]['lcmt_content'];
            $feedback['last_comment_date'] = $fb_last[$r4['primaryid']]['lcmt_datetime'];
            $feedback['last_comment_depositor'] = $fb_last[$r4['primaryid']]['lcmt_created_by_name'];
            $feedback['count_unread_comment'] = $fb_last[$r4['primaryid']]['lcmt_unread_app'];
            $feedback['lcmt_created_by_name'] = $fb_last[$r4['primaryid']]['lcmt_created_by_name'];
            $feedback['lcmt_created_by'] = $fb_last[$r4['primaryid']]['lcmt_created_by'];
            $feedback['comment_list'] = $this->getFeedbackDetail($r4['primaryid']);
            $data[] = $feedback;
            $count_feedback++;
        }
        $can_load = true;
        if ($count_feedback < 20 || empty($data)) {
            $can_load = false;
        }
        return [
            'success' => true,
            'data' => [
                'feedback_list' => $data,
                'can_load' => $can_load,
                'page' => (int)$args['page']
            ],
            'error_info' => throwApiError(1)
        ];
    }

    function getFeedbackDetail($case_id)
    {
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';

        $r = $GLOBALS['db']->query("SELECT DISTINCT
                IFNULL(feedback.id, '') case_id,
                IFNULL(comment.id, '') primaryid,
                IFNULL(comment.description, '') description,
                IFNULL(comment.direction, '') direction,
                comment.date_entered date_entered,
                IFNULL(user.id, '') user_id,
                IFNULL(user.first_name, '') first_name,
                IFNULL(user.last_name, '') last_name,
                IFNULL(user.picture, '') avatar,
                IFNULL(comment.filename, '') filename,
                IFNULL(comment.file_ext, '') file_ext,
                IFNULL(comment.file_mime_type, '') file_mime_type,
                IFNULL(comment.is_read_inems, 0) is_read_inems,
                IFNULL(comment.is_read_inapp, 0) is_read_inapp,
                IFNULL(comment.is_updated, 0) is_updated
                FROM c_comments comment
                INNER JOIN cases feedback ON feedback.id = comment.parent_id AND comment.parent_type = 'Cases'
                LEFT JOIN users user ON comment.created_by = user.id AND user.deleted = 0
                WHERE feedback.id = '{$case_id}'
                AND comment.is_unsent = 0 AND comment.deleted = 0
                ORDER BY comment.date_entered DESC
                LIMIT 30");

        while ($row = $GLOBALS['db']->fetchByAssoc($r)) {
            $create_by = $row['user_id'];
            if ($row['user_id'] == $api_user) $create_by = 'apps_admin';

            //Get the attachment of each comment
            $sqlNote = "SELECT IFNULL(l1.id, '') attachment_id,
                        IFNULL(l1.name, '') name,
                        IFNULL(l1.upload_id, '') upload_id,
                        IFNULL(l1.filename, '') filename,
                        IFNULL(l1.file_size, '') file_size,
                        IFNULL(l1.file_source, '') file_source,
                        IFNULL(l1.file_mime_type, '') file_mime_type,
                        IFNULL(l1.file_ext, '') file_ext
                        FROM notes l1 
                        WHERE l1.parent_id = '{$row['primaryid']}' AND l1.parent_type = 'C_Comments' AND l1.deleted = 0";
            $attachment_list = $GLOBALS['db']->fetchArray($sqlNote);
            $formatted_attachment_list = [];
            //Format img list
            foreach($attachment_list as $attachment){
                $srcImg = $attachment['file_source'] == 'S3'
                    ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $attachment['upload_id']
                    : $GLOBALS['dotb_config']['site_url'].'/upload/s3_storage/'.$attachment['name'];
                $fileType = explode('/', $attachment['file_mime_type'])[0]; //image or video
                $formatted_attachment_list[] = array(
                    'attachment_id' => $attachment['attachment_id'],
                    'file_source' => empty($attachment['file_source']) ? 'local' : $attachment['file_source'],
                    'file_type' => $fileType,
                    'file_size' => $attachment['file_size'],
                    'file_name' => $attachment['filename'],
                    'file_ext' => $attachment['file_ext'],
                    'file_url' => $srcImg
                );
            }

            $data[] = array(
                'id' => $row['primaryid'],
                'direction' => $row['direction'],
                'description' => $row['description'],
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $row['date_entered'])),
                'created_by' => $create_by,
                'created_by_name' => $GLOBALS['locale']->getLocaleFormattedName($row['first_name'], $row['last_name']),
                'created_by_avatar' => $row['avatar'],
                'attachment' => $formatted_attachment_list,
                'is_read_inems' => $row['is_read_inems'],
                'is_read_inapp' => $row['is_read_inapp'],
                'is_updated' => $row['is_updated'],
            );
        }
        return $data;
    }
    function getFeedbackById(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'feedback_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        //Get Feedback
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $q4 = "SELECT DISTINCT
                IFNULL(cases.id, '') primaryid,
                IFNULL(cases.name, '') subject,
                IFNULL(l3.id, '') student_id, IFNULL(l3.full_student_name, '') student_name,
                IFNULL(l2.id, '') class_id, IFNULL(l2.name, '') class_name,
                IFNULL(cases.case_number, '') case_number,
                IFNULL(cases.type, '') type,
                IFNULL(cases.status, '') case_status,
                IFNULL(cases.rate, '') rate,
                cases.date_entered date_entered,
                IFNULL(cases.description, '') feedback_content,
                IFNULL(l4.id, '') assigned_to_id, IFNULL(l4.full_user_name, '') assigned_to,
                IFNULL(l5.id, '') create_by, IFNULL(l5.full_user_name, '') create_by_name,
                IFNULL(l5.picture, '') created_by_avatar,
                IFNULL(l1.id, '') center_id,
                IFNULL(l1.name, '') center
                FROM cases
                INNER JOIN teams l1 ON cases.team_id = l1.id AND l1.deleted = 0
                LEFT JOIN j_class_cases_1_c l2_1 ON cases.id = l2_1.j_class_cases_1cases_idb AND l2_1.deleted = 0
                LEFT JOIN j_class l2 ON l2.id = l2_1.j_class_cases_1j_class_ida AND l2.deleted = 0
                LEFT JOIN contacts_cases_1_c l3_1 ON cases.id = l3_1.contacts_cases_1cases_idb AND l3_1.deleted = 0
                LEFT JOIN contacts l3 ON l3.id = l3_1.contacts_cases_1contacts_ida AND l3.deleted = 0
                LEFT JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
                LEFT JOIN users l5 ON cases.created_by = l5.id AND l5.deleted = 0
                WHERE cases.id = '{$args['feedback_id']}'";
        $result = $GLOBALS['db']->fetchOne($q4);

        $q9 = "SELECT cs.*
                FROM c_comments cs, (SELECT  cc.parent_id parent_id, max(cc.date_entered) max_date_entered
                FROM c_comments cc
                WHERE cc.deleted = 0 AND cc.parent_id = '{$args['feedback_id']}' AND cc.parent_type = 'Cases' AND cc.is_unsent = 0
                GROUP BY cc.parent_id) cc_ma
                WHERE (cs.parent_id = cc_ma.parent_id) AND (cs.parent_type = 'Cases')
                AND (cc_ma.max_date_entered = cs.date_entered) AND (cs.parent_id = '{$args['feedback_id']}')
                GROUP BY cs.id";
        $rs9 = $GLOBALS['db']->fetchArray($q9);
        foreach ($rs9 as $ind => $r9) {
            $row_u = $GLOBALS['db']->fetchOne("SELECT IFNULL(full_user_name, '') full_user_name, IFNULL(picture, '') created_by_avatar FROM users WHERE id = '{$r9['created_by']}'");
            $fb_last[$r9['parent_id']]['lcmt_content'] = $r9['description'];
            $fb_last[$r9['parent_id']]['lcmt_datetime'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r9['date_entered']));
            $fb_last[$r9['parent_id']]['lcmt_created_by_name'] = $row_u['full_user_name'];
            $fb_last[$r9['parent_id']]['lcmt_created_by'] = $r9['created_by'];
            $fb_last[$r9['parent_id']]['lcmt_created_by_avatar'] = $row_u['created_by_avatar'];
            $fb_last[$r9['parent_id']]['lcmt_unread_app'] = (($r9['is_read_inapp'] == 0) ? '1' : '0');
            $fb_last[$r9['parent_id']]['lcmt_unread_ems'] = (($r9['is_read_inems'] == 0) ? '1' : '0');
        }
        $feedback = array();
        $feedback['number'] = $result['case_number'];
        $feedback['feedback_id'] = $result['primaryid'];
        $feedback['subject'] = $result['subject'];
        $feedback['class_id'] = $result['class_id'];
        $feedback['class_name'] = $result['class_name'];
        $feedback['type'] = $result['type'];
        $feedback['case_status'] = $result['case_status'];
        $feedback['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $result['date_entered']));
        $feedback['feedback_content'] = $result['feedback_content'];
        $feedback['assigned_to_id'] = $result['assigned_to_id'];
        $feedback['assigned_to'] = $result['assigned_to'];
        $feedback['create_by'] = $result['create_by'];
        $feedback['create_by_name'] = $result['create_by_name'];
        $feedback['created_by_avatar'] = $result['created_by_avatar'];
        $feedback['center_id'] = $result['center_id'];
        $feedback['center'] = $result['center'];
        $feedback['rate'] = $result['rate'];
        $feedback['last_comment'] = $fb_last[$result['primaryid']]['lcmt_content'];
        $feedback['last_comment_date'] = $fb_last[$result['primaryid']]['lcmt_datetime'];
        $feedback['last_comment_depositor'] = $fb_last[$result['primaryid']]['lcmt_created_by_name'];
        $feedback['count_unread_comment'] = $fb_last[$result['primaryid']]['lcmt_unread_app'];
        $feedback['lcmt_created_by_name'] = $fb_last[$result['primaryid']]['lcmt_created_by_name'];
        $feedback['lcmt_created_by'] = $fb_last[$result['primaryid']]['lcmt_created_by'];
        $feedback['comment_list'] = $this->getCommentFeedbackList($result['primaryid']);
        return [
            'success' => true,
            'data' => [
                'feedback' => $feedback,
            ],
            'error_info' => throwApiError(1)
        ];
    }

    function getCommentFeedbackList($case_id)
    {
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';

        $r = $GLOBALS['db']->query("SELECT DISTINCT
                IFNULL(l1.id, '') case_id,
                IFNULL(c_comments.id, '') primaryid,
                IFNULL(c_comments.description, '') description,
                IFNULL(c_comments.direction, '') direction,
                c_comments.date_entered date_entered,
                IFNULL(l2.id, '') user_id,
                IFNULL(l2.first_name, '') first_name,
                IFNULL(l2.last_name, '') last_name,
                IFNULL(l2.picture, '') avatar,
                IFNULL(c_comments.filename, '') filename,
                IFNULL(c_comments.file_ext, '') file_ext,
                IFNULL(c_comments.file_mime_type, '') file_mime_type,
                IFNULL(c_comments.is_read_inems, 0) is_read_inems,
                IFNULL(c_comments.is_read_inapp, 0) is_read_inapp,
                IFNULL(c_comments.is_updated, 0) is_updated
                FROM c_comments
                INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases'
                LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
                WHERE (((l1.id = '{$case_id}')))
                AND c_comments.is_unsent = 0 AND c_comments.deleted = 0
                ORDER BY c_comments.date_entered DESC
                LIMIT 30");

        while ($row = $GLOBALS['db']->fetchByAssoc($r)) {
            $create_by = $row['user_id'];
            if ($row['user_id'] == $api_user) $create_by = 'apps_admin';
            $attachment = [];
            if ($row['direction'] == 'inbound') {
                $r1 = $GLOBALS['db']->query("SELECT DISTINCT
                IFNULL(c_uploadimage.id, '') id,
                IFNULL(c_uploadimage.filename, '') filename,
                IFNULL(c_uploadimage.document_name, '') document_name,
                IFNULL(c_uploadimage.file_ext, '') file_ext,
                IFNULL(c_uploadimage.file_mime_type, '') file_mime_type
                FROM c_uploadimage
                WHERE (c_uploadimage.comment_id = '{$row['primaryid']}'AND c_uploadimage.deleted = 0)");
                while ($img = $GLOBALS['db']->fetchByAssoc($r1)) {
                    if ($img['document_name'] == 's3_image') {
                        $attachment[] = array(
                            's3Link' => 'https://dotb-gallery.s3.ap-southeast-1.amazonaws.com/' . $GLOBALS['dotb_config']['unique_key'] . '/comment/' . $img['id'],
                        );
                    } else {
                        $attachment[] = array(
                            'id' => $img['id'],
                            'name' => $img['filename'],
                            'module' => 'c_uploadimage',
                        );
                    }
                }
            } else {
                if ($row['filename'] != '')
                    $attachment[] = array(
                        'id' => $row['primaryid'],
                        'name' => $row['filename'],
                        'module' => 'c_comments',
                    );
            }

            $data[] = array(
                'id' => $row['primaryid'],
                'direction' => $row['direction'],
                'description' => $row['description'],
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $row['date_entered'])),
                'created_by' => $create_by,
                'created_by_name' => $GLOBALS['locale']->getLocaleFormattedName($row['first_name'], $row['last_name']),
                'created_by_avatar' => $row['avatar'],
                'attachment' => $attachment,
                'is_read_inems' => $row['is_read_inems'],
                'is_read_inapp' => $row['is_read_inapp'],
                'is_updated' => $row['is_updated'],
            );
        }
        return $data;
    }
    //END
    function getCommentList(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'parent_id', 'parent_type', 'page']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $limit = (!empty($args['page'])) ? 20 * $args['page'] : 20;
        $limit_bot = $limit - 20;
        $limit_top = $limit + 1;
        $parent_type = $args['parent_type'];
        $parent_id = $args['parent_id'];
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $r = $GLOBALS['db']->query("SELECT DISTINCT
                IFNULL(comment.id, '') primaryid,
                IFNULL(comment.description, '') description,
                comment.date_entered date_entered,
                IFNULL(student.id, '') student_id,
                IFNULL(student.full_student_name, '') full_student_name,
                IFNULL(student.picture, '') student_avatar,
                IFNULL(teacher.id, '') teacher_id,
                IFNULL(teacher.picture, '') teacher_avatar,
                IFNULL(teacher.full_teacher_name, '') full_teacher_name
                FROM c_comments comment
                LEFT JOIN contacts student ON comment.student_id = student.id AND student.deleted = 0
                LEFT JOIN c_teachers teacher ON comment.created_by = teacher.user_id AND teacher.deleted = 0
                WHERE comment.parent_id = '{$parent_id}' AND comment.parent_type = '{$parent_type}' AND comment.deleted = 0
                ORDER BY comment.date_entered DESC
                LIMIT $limit_bot, $limit_top");
        $count_comment = 0;
        $can_load = false;
        while ($row = $GLOBALS['db']->fetchByAssoc($r)) {
            $count_comment++;
            if($count_comment == $limit_top){
                $can_load = true;
                break;
            }
            if(empty($row['student_id'])){
                $chatter_name = $row['full_teacher_name'];
                $picture = $GLOBALS['dotb_config']['site_url'] . '/upload/' .$row['teacher_avatar'];
                $chatter_id = $row['teacher_id'];
            } else {
                $chatter_name = $row['full_student_name'];
                $picture = $GLOBALS['dotb_config']['site_url'] . '/upload/origin/' .$row['student_avatar'];
                $chatter_id = $row['student_id'];
            }
            $data[] = array(
                'id' => $row['primaryid'],
                'content' => $row['description'],
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $row['date_entered'])),
                'created_by_name' => $chatter_name,
                'created_by_avatar' => $picture,
                'created_by_id' => $chatter_id
            );
        }

        return [
            'success' => true,
            'data' => [
                'comment_list' => $data,
                'can_load' => $can_load,
                'page' => (int)$args['page']
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function countComments($parentId, $parentType){
        $sqlCountComment = "SELECT COUNT(*) 
                FROM c_comments 
                WHERE parent_id = '{$parentId}' 
                  AND parent_type = '{$parentType}' 
                  AND deleted = 0
                  ORDER BY c_comments.date_entered DESC";
        $result = $GLOBALS['db']->getOne($sqlCountComment);
        return $result;
    }
    function likeAction(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'parent_id', 'parent_type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $like_link_map = [
            'C_Gallery' => 'c_gallery_contacts_1',
            'C_News' => 'c_news_contacts_2'
        ];
        $focus = BeanFactory::getBean($args['parent_type'], $args['parent_id']);
        $relationship_link = $like_link_map[$args['parent_type']];
        $focus->load_relationship($relationship_link);
        $focus->$relationship_link->add($student_id);

        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }
    function unlikeAction(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'parent_id', 'parent_type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $like_link_map = [
            'C_Gallery' => 'c_gallery_contacts_1',
            'C_News' => 'c_news_contacts_2'
        ];
        $focus = BeanFactory::getBean($args['parent_type'], $args['parent_id']);
        $relationship_link = $like_link_map[$args['parent_type']];
        $focus->load_relationship($relationship_link);
        $focus->$relationship_link->delete($args['parent_id'],$student_id);

        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }
    function createComment(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'parent_id', 'parent_type', 'content']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $comment = BeanFactory::newBean('C_Comments');
        $comment->parent_id = $args['parent_id'];
        $comment->parent_type = $args['parent_type'];
        $comment->description = $args['content'];
        $comment->direction = 'inbound';
        $comment->student_id = $student_id;
        $comment->save();
        $comment_id = $comment->id;

        $student = BeanFactory::getBean('Contacts', $student_id);
        $chatter_name = $student->full_student_name;
        $picture = $GLOBALS['dotb_config']['site_url'] . '/upload/origin/' . $student->picture;

        return [
            'success' => true,
            'data' => [
                'id' => $comment->id,
                'content' => $comment->description,
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $comment->date_entered)),
                'created_by_name' => $chatter_name,
                'created_by_avatar' => $picture,
                'created_by_id' => $student_id
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function deleteComment(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'comment_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
        $comment = BeanFactory::getBean('C_Comments', $args['comment_id'], array('disable_row_level_security' => true));
        $comment->mark_deleted($args['comment_id']);
        return [
            'success' => true,
            'error_info' => throwApiError(1)
        ];
    }
    function getClassListV2(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        global $timedate;
        $now = $timedate->nowDb();
        require_once('include/externalAPI/ClassIn/ExtAPIClassIn.php');
        $ExtApiClassIn = new ExtApiClassIn();
        $schoolId = $ExtApiClassIn->_SID;
        $AppUrl = $ExtApiClassIn->_AppUrl;
        //Get Current Class
        $nowDb = $timedate->nowDb();
        $q2 = "SELECT DISTINCT
            IFNULL(contacts.id, '') student_id,
            IFNULL(l3.id, '') class_id,
            IFNULL(l3.picture, '') picture,
            IFNULL(l3.name, '') class_name,
            IFNULL(l3.class_code, '') class_code,
            l3.start_date start_date,
            l3.end_date end_date,
            l3.date_entered date_entered,
            IFNULL(l3.kind_of_course, '') kind_of_course,
            IFNULL(l3.level, '') level,
            CONCAT(IFNULL(l3.kind_of_course, ''), ' ', IFNULL(l3.level, '')) koc_level,
            l3.hours hours,
            IFNULL(l3.status, '') class_status,
            IFNULL(l2.lesson_number, '') ss_lesson,
            IFNULL(l4.id, '') assigned_to_id,
            IFNULL(l4.full_user_name, '') assigned_to,
            IFNULL(l5.id, '') koc_id,
            IFNULL(l5.name, '') koc_name,
            IFNULL(l6.id, '') room_id,
            IFNULL(l6.name, '') room_name,
            IFNULL(l7.id, '') teacher_id,
            IFNULL(l7.full_teacher_name, '') teacher_name,
            IFNULL(l3.max_size, '') max_size,
            IFNULL(l8.id, '') team_id,
            IFNULL(l8.name, '') team_name,
            IFNULL(l8.code_prefix, '') team_code,
            IFNULL(l2.id, '') ss_id,
            IFNULL(l2.name, '') ss_name,
            IFNULL(l2.week_no, '') ss_week_no,
            IFNULL(l2.observe_score, 0) ss_observe_score,
            IFNULL(l2.syllabus_custom, '') ss_lesson_note,
            IFNULL(l2.syllabus_custom, '') ss_syllabus_custom,
            IFNULL(l2.topic_custom, '') ss_topic_custom,
            IFNULL(l2.syllabus_id, '') ss_syllabus_id,
            IFNULL(l13.name, '') ss_syllabus_topic,
            IFNULL(l13.description, '') ss_syllabus_content,
            IFNULL(l13.homework, '') ss_syllabus_homework,
            l2.till_hour ss_till_hour,
            l2.date_start ss_date_start,
            l2.date_end ss_date_end,
            l2.duration_cal ss_duration_cal,
            IFNULL(l9.id, '') l9_id,
            IFNULL(l9.full_teacher_name, '') ss_ta_name,
            IFNULL(l10.id, '') l10_id,
            IFNULL(l10.full_teacher_name, '') ss_teacher_name,
            IFNULL(l11.id, '') l11_id,
            IFNULL(l11.name, '') ss_room_name,
            IFNULL(l2.type, '') ss_learning_type,
            (CASE
            WHEN DATE_SUB(l2.date_end, INTERVAL 1 HOUR) < '$nowDb' THEN ''
            WHEN l2.type = 'ClassIn' THEN (CONCAT('$AppUrl','enterclass?telephone=',IFNULL(contacts.phone_prefix,'84'),'-',SUBSTR(contacts.phone_mobile,2),'&classId=',l2.external_id,'&courseId=',l3.onl_course_id,'&schoolId=$schoolId'))
            ELSE IFNULL(l2.join_url, '')
            END) ss_displayed_url,
            IFNULL(l12.id, '') attendance_id,
            IFNULL(l12.homework_comment, '') homework_comment,
            l12.homework_score homework_score,
            IFNULL(l12.homework, '') do_homework,
            IFNULL(l12.description, '') teacher_comment,
            IFNULL(l12.care_comment, '') care_comment,
            IFNULL(l12.attended, 0) attended,
            IFNULL(l12.attendance_type, '') attendance_type,
            IFNULL(l12.star_rating, '') star_rating,
            IFNULL(l12.customer_comment, '') customer_comment,
            IFNULL(l12.check_in_time, '') check_in_time,
            IFNULL(l12.check_out_time, '') check_out_time,
            l2.lesson_number lesson_number
            FROM contacts
            INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0
            INNER JOIN meetings_contacts l2_1 ON l1.id = l2_1.situation_id AND l2_1.deleted = 0 AND contacts.id = l2_1.contact_id
            INNER JOIN meetings l2 ON l2.id = l2_1.meeting_id AND l2.deleted = 0
            INNER JOIN j_class l3 ON l2.ju_class_id = l3.id AND l3.deleted = 0
            LEFT JOIN users l4 ON l3.assigned_user_id = l4.id AND l4.deleted = 0
            LEFT JOIN j_kindofcourse l5 ON l3.koc_id = l5.id AND l5.deleted = 0
            LEFT JOIN c_rooms l6 ON l3.room_id = l6.id AND l6.deleted = 0
            LEFT JOIN c_teachers l7 ON l3.teacher_id = l7.id AND l7.deleted = 0
            LEFT JOIN teams l8 ON l3.team_id = l8.id AND l8.deleted = 0
            LEFT JOIN c_teachers l9 ON l2.teacher_cover_id=l9.id AND l9.deleted=0
            LEFT JOIN c_teachers l10 ON l2.teacher_id=l10.id AND l10.deleted=0
            LEFT JOIN c_rooms l11 ON l2.room_id=l11.id AND l11.deleted=0
            LEFT JOIN c_attendance l12 ON l2.id = l12.meeting_id AND l12.deleted = 0 AND l12.student_id = contacts.id
            LEFT JOIN j_syllabus l13 ON l2.syllabus_id = l13.id AND l13.deleted = 0
            WHERE (((contacts.id = '{$student_id}')                
            AND (IFNULL(l2.meeting_type, '') = 'Session')
            AND (IFNULL(l2.session_status, '') <> 'Cancelled' OR (IFNULL(l2.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
            AND (IFNULL(l1.type, '') IN ('Enrolled' , 'OutStanding', 'Demo'))))
            AND contacts.deleted = 0
            ORDER BY student_id, CASE";
        $types = $GLOBALS['app_list_strings']['status_class_list'];
        $count_k = 1;
        foreach ($types as $_type => $val_)
            $q2 .= " WHEN class_status = '$_type' THEN " . $count_k++;
        $q2 .= " ELSE $count_k END ASC,l3.end_date DESC, class_id, l2.date_start";

        $rowS = $GLOBALS['db']->fetchArray($q2);
        //Get Kind Of Course Syllabus - For best perfomance
        $classIds = implode("','", array_unique(array_column($rowS, 'class_id')));
        $classes = array();
        $koc = $this->getKOCofClass($classIds);
        $arrClassFinish = array();
        $data = [];
        $runClass = '####';
        $key = -1;
        //List Class ID
        $classIds = array();
        foreach ($rowS as $_k2 => $r2) {
            if ($runClass != $r2['class_id']) {
                //Đã tham gia
                $count_studied = 0;
                $ss_studied = 0;
                $attended_lesson = 0;
                $attended_session = 0;
                $ss_count = 0;

                $runClass = $r2['class_id'];

                $classes['id'] = $r2['class_id'];
                $classes['class_name'] = $r2['class_name'];
                $classes['picture'] = $r2['picture'];
                $classes['class_code'] = $r2['class_code'];
                $classes['start_date'] = $timedate->to_display_date($r2['start_date'], false);
                $classes['end_date'] = $timedate->to_display_date($r2['end_date'], false);
                $classes['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['date_entered']));
                $classes['kind_of_course'] = $r2['kind_of_course'];
                $classes['level'] = $r2['level'];
                $classes['hours'] = $r2['hours'];
                $classes['assigned_to'] = $r2['assigned_to'];
                $classes['koc_id'] = $r2['koc_id'];
                $classes['koc_name'] = $r2['koc_name'];
                $classes['room_name'] = $r2['room_name'];
                $classes['teacher_name'] = $r2['teacher_name'];
                $classes['max_size'] = $r2['max_size'];
                $classes['team_name'] = $r2['team_name'];
                $classes['team_code'] = $r2['team_code'];
                $classes['status'] = $r2['class_status'];

                //Add new average rating by nnamnguyen
                $count_session_is_rated = 0;
                $rating_score = 0;
                $classes['average_rating'] = '';

                $content = $koc[$r2['koc_id']]['content'];
                foreach ($content as $ct) {
                    if ($ct['levels'] == $r2['level'])
                        $docs = $ct['doc_url'];
                }

                $classes['doc_url'] = $docs;
                $classes['sessions'] = array();
                $classes['albums'] = array();
                $classes['gradebooks'] = array();

                $data[] = $classes;
                $key++;
            }
            //nếu thời gian hiện tại nằm trong thòi gian student situation thì lấy trạng thái lớp là trạng thái lớp hiện tại
            if ($r2['class_status'] != 'Finished' && $classes['status'] != $r2['class_status']) {
                $data[$key]['status'] = $r2['class_status'];
            }

            $session = array();
            $session['ss_id'] = $r2['ss_id'];
            $session['ss_name'] = $r2['ss_name'];
            $session['ss_lesson'] = $r2['ss_lesson'];
            $session['ss_week_no'] = $r2['ss_week_no'];
            $session['ss_week_date'] = date('l', strtotime("+7 hours " . $r2['ss_date_start']));
            $session['ss_till_hour'] = $r2['ss_till_hour'];
            $session['ss_db_date_start'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_start']));
            $session['ss_db_date_end'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_end']));
            $session['ss_date_start'] = $timedate->to_display_date_time($r2['ss_date_start']);
            $session['ss_date_end'] = $timedate->to_display_date_time($r2['ss_date_end']);
            $session['ss_db_date'] = date('Y-m-d', strtotime("+7 hours " . $r2['ss_date_start']));
            $session['ss_learning_type'] = $r2['ss_learning_type'];
            $session['ss_displayed_url'] = $r2['ss_displayed_url'];
            if ($now < $r2['ss_date_start']) {
                $session['ss_status'] = "not_stated";
                if ($lesson_num_prev == (int)$r2['lesson_number']) {
                    $count_studied--;
                }
            } elseif ($now >= $r2['ss_date_start'] && $now <= $r2['ss_date_end']) {
                $session['ss_status'] = "in_progress";
                if ($lesson_num_prev == (int)$r2['lesson_number']) {
                    $count_studied--;
                }
            } elseif ($now > $r2['ss_date_end']) {
                $session['ss_status'] = "finished";
                if ($lesson_num_prev != (int)$r2['lesson_number']) {
                    $count_studied++;
                    $lesson_num_prev = (int)$r2['lesson_number'];
                }
                $ss_studied++;
            }

            $session['ss_duration_cal'] = $r2['ss_duration_cal'];
            $session['ss_ta_name'] = $r2['ss_ta_name'];
            $session['ss_teacher_name'] = $r2['ss_teacher_name'];
            $session['ss_room_name'] = $r2['ss_room_name'];
            $session['ss_observe_score'] = $r2['ss_observe_score'];
            $session['ss_lesson_note'] = $r2['ss_lesson_note'];
            $session['ss_syllabus_custom'] = $r2['ss_syllabus_custom'];
            $session['ss_topic_custom'] = $r2['ss_topic_custom'];
            $session['syllabus_topic'] = $r2['ss_syllabus_topic'];
            $session['syllabus_activities'] = $r2['ss_syllabus_content'];
            $session['syllabus_homework'] = $r2['ss_syllabus_homework'];

            //LAY THONG TIN SO LIEN LAC
            $session['att_id'] = $r2['attendance_id'];
            $session['att_homework_comment'] = $r2['homework_comment'];
            $session['att_homework_score'] = $r2['homework_score'];
            $session['att_do_homework'] = $r2['do_homework'];
            $session['att_teacher_comment'] = $r2['teacher_comment'];
            $session['att_care_comment'] = $r2['care_comment'];
            $session['att_attended'] = $r2['attended'];
            $session['att_attendance_type'] = $r2['attendance_type'];
            $session['att_star_rating'] = $r2['star_rating'];
            $session['att_customer_comment'] = $r2['customer_comment'];
            $session['check_in_time'] = $timedate->to_display_time($r2['check_in_time']);
            $session['check_out_time'] = $timedate->to_display_time($r2['check_out_time']);
            //Custom: $attended_session
            if ($session['ss_status'] === 'finished') {
                if ($session['att_attendance_type'] === 'P' || $session['att_attendance_type'] === 'L') {
                    if ($lesson_num_prev2 != (int)$r2['lesson_number']) {
                        $attended_lesson++;
                        $lesson_num_prev2 = (int)$r2['lesson_number'];
                    }
                    $attended_session++;
                } else {
                    if ($lesson_num_prev2 == (int)$r2['lesson_number']) {
                        $attended_lesson--;
                    }
                }
            }
            $ss_count++;
            $qCountStudied = "SELECT COUNT(DISTINCT l2.lesson_number) 
                    FROM j_class 
                    INNER JOIN meetings l2 ON l2.ju_class_id = j_class.id AND l2.deleted = 0
                    WHERE l2.date_end < '{$now}' AND j_class.id = '{$runClass}'";
            $lesson_studied = $GLOBALS['db']->getOne($qCountStudied);
            $data[$key]['sessions'][] = $session;
            $data[$key]['count_studied_lesson'] = $count_studied;
            $data[$key]['count_studied'] = $ss_studied;
            $data[$key]['attended_session'] = $attended_session;
            $data[$key]['total_session'] = (string)$ss_count;
            $data[$key]['total_lesson'] = (int)$r2['lesson_number'];
            $data[$key]['completed_lesson'] = $lesson_studied;
            $data[$key]['attended_lesson'] = $attended_lesson < 0 ? 0 : $attended_lesson;

            //Custom average rating
            if ($session['att_homework_score'] !== "" && $session['att_homework_score'] !== null) {
                $count_session_is_rated += 1;
                if ($session['att_homework_score'] > 5) {
                    $rating_score += 5;
                } else {
                    $rating_score += $session['att_homework_score'];
                }
                $rating = number_format($rating_score / $count_session_is_rated, 1);
                $data[$key]['average_rating'] = $rating;
            }
            //Custom attended session percent
            if ($data[$key]['count_studied'] !== 0) {
                $attended_session_percent = (($data[$key]['attended_session']) / ($data[$key]['count_studied'])) * 100;
                $attended_session_percent = number_format($attended_session_percent, 0);
            } else {
                $attended_session_percent = 0;
            }
            if ($data[$key]['count_studied_lesson'] !== 0) {
                $attended_lesson_percent = (($data[$key]['attended_lesson']) / ($data[$key]['count_studied_lesson'])) * 100;
                $attended_lesson_percent = number_format($attended_lesson_percent, 0);
            } else {
                $attended_lesson_percent = 0;
            }
            $data[$key]['attended_session_percent'] = $attended_session_percent;
            $data[$key]['attended_lesson_percent'] = $attended_lesson_percent;
            $data[$key]['rank_in_class'] = null;

            //lấy điểm số của tất cả các học sinh trong lớp của học viên đang login để set rank cho học viên đó
            if ($r2['class_status'] == 'Finished') {
                $qClassFinish = "SELECT DISTINCT
                    IFNULL(c.id, '') student_id,
                    IFNULL (l2.id,'') class_id,
                    IFNULL(l5.final_result, '') final_result,
                    @rank:=1 AS rank_in_class
                    FROM contacts c
                    INNER JOIN j_classstudents l1 ON l1.student_id = c.id AND l1.deleted=0
                    INNER JOIN j_class l2 ON l2.id = l1.class_id AND l2.deleted=0 AND l2.status='Finished'
                    INNER JOIN j_class_j_gradebook_1_c l3 ON l3. j_class_j_gradebook_1j_class_ida = l2.id AND l3.deleted=0
                    INNER JOIN j_gradebook l4 ON l4.id = l3. j_class_j_gradebook_1j_gradebook_idb AND l4.deleted=0 AND l4.status='Approved' AND l4.type='Overall'
                    INNER JOIN j_gradebookdetail l5 ON l5.gradebook_id = l4.id AND l5.student_id = c.id AND l5.j_class_id=l2.id AND l5.deleted=0
                    WHERE c.deleted = 0 AND (l2.id IN ('$runClass'))
                    ORDER BY final_result DESC";
                $rowClassFinish = $GLOBALS['db']->fetchArray($qClassFinish);
                $arrClassFinish[$runClass] = $rowClassFinish;
            }
            if (!in_array($runClass, $classIds)) $classIds[] = $runClass;
        }

        $this->setStudentRank($data, $arrClassFinish, $student_id);
        $this->getClassGradeBook($data, $classIds, $student_id);
        //Assign to result
        return [
            'success' => true,
            'data' => [
                'class_list' => $data
            ],
            'error_info' => throwApiError(1)
        ];
    }

    function getGalleryList(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        $limit = (!empty($args['page'])) ? 20 * $args['page'] : 20;
        $limit_bot = $limit - 20;
        $limit_top = $limit + 1;
        //Prepare where clause if class_id or center_id is set
        if (isset($args['class_id'])) {
            $str_class_id = $args['class_id'];
            $class_where_clause = "AND class.id = '$str_class_id'";
            $center_where_clause = 'AND 1 = 0'; // To avoid getting center gallery
        } else if (isset($args['center_id'])) {
            $str_center_id = $args['center_id'];
            $center_where_clause = "AND team.id = '$str_center_id'";
            $class_where_clause = 'AND 1 = 0'; // To avoid getting class gallery
        }
        //Get gallery list of class
        $sql_album_class = "SELECT DISTINCT
            IFNULL(gallery.id, '') gallery_id,
            IFNULL(gallery.name, '') gallery_name,
            IFNULL(gallery.gallery_date, '') gallery_date,
            IFNULL(gallery.description, '') gallery_description,
            IFNULL(gallery.gallery_type, '') gallery_type,
            IFNULL(gallery.created_by, '') created_by,
            IFNULL(gallery.date_entered, '') date_entered,
            IFNULL(gallery.visibility_range, '') visibility_range,
            IFNULL(class.id, '') class_id,
            IFNULL(class.name, '') class_name,
            '' team_id,
            '' team_name,
            IFNULL(creator.picture, '') user_avt,
            IFNULL(teacher.picture, '') teacher_avt,
            IFNULL(creator.full_user_name, '') full_user_name
        FROM contacts student
        INNER JOIN j_studentsituations situation ON student.id = situation.student_id AND situation.deleted = 0
        INNER JOIN j_class class ON class.id = situation.ju_class_id AND class.deleted = 0
        INNER JOIN j_class_c_gallery_1_c class_gallery_link ON class.id = class_gallery_link.j_class_c_gallery_1j_class_ida AND class_gallery_link.deleted = 0
        INNER JOIN c_gallery gallery ON gallery.id = class_gallery_link.j_class_c_gallery_1c_gallery_idb AND gallery.deleted = 0
        INNER JOIN users creator ON creator.id = gallery.created_by
        LEFT JOIN c_teachers teacher ON teacher.id = creator.teacher_id
        WHERE (IFNULL(situation.type, '') IN ('OutStanding', 'Enrolled', 'Demo'))
            AND (situation.start_study <= DATE(gallery.gallery_date))
            AND (situation.end_study >= DATE(gallery.gallery_date))
            AND student.id = '$student_id'
            AND (IFNULL(student.status, '') NOT IN ('Cancelled')) AND gallery.visibility_range = 'class' AND gallery.status = 'approved'
            {$class_where_clause}";
        //Get gallery list of center
        $sql_album_center = "SELECT DISTINCT
            IFNULL(gallery.id, '') gallery_id,
            IFNULL(gallery.name, '') gallery_name,
            IFNULL(gallery.gallery_date, '') gallery_date,
            IFNULL(gallery.description, '') gallery_description,
            IFNULL(gallery.gallery_type, '') gallery_type,
            IFNULL(gallery.created_by, '') created_by,
            IFNULL(gallery.date_entered, '') date_entered,
            IFNULL(gallery.visibility_range, '') visibility_range,
            '' class_id,
            '' class_name,
            IFNULL(team.id, '') team_id,
            IFNULL(team.name, '') team_name,
            IFNULL(user_creator.picture, '') user_avt,
            IFNULL(teacher.picture, '') teacher_avt,
            IFNULL(user_creator.full_user_name, '') full_user_name
        FROM contacts student
            INNER JOIN c_gallery gallery ON gallery.team_id = student.team_id AND gallery.deleted = 0
            INNER JOIN teams team ON team.id = gallery.team_id AND team.deleted = 0
            INNER JOIN users user_creator ON user_creator.id = gallery.created_by
            LEFT JOIN c_teachers teacher ON teacher.id = user_creator.teacher_id
        WHERE student.id = '$student_id'
            AND (IFNULL(student.status, '') NOT IN ('Cancelled')) AND gallery.visibility_range = 'center' AND gallery.status = 'approved'
            {$center_where_clause}";

        $sqlAlbum = "{$sql_album_center} UNION {$sql_album_class} ORDER BY date_entered DESC LIMIT $limit_bot, $limit_top";
        $albums = $GLOBALS['db']->fetchArray($sqlAlbum);
        $gls = array();
        $can_load = false;
        $count_gallery = 0;
        foreach($albums as $album) {
            $count_gallery++;
            if($count_gallery == $limit_top) {
                $can_load = true;
                break;
            }
            $formatted_img_list = $this->getNote($album['gallery_id'], 'C_Gallery');
            $comment_list = $this->getCommentList($api, ['access_token' => $args['access_token'], 'parent_id' => $album['gallery_id'], 'parent_type' => 'C_Gallery', 'page' => 1]);
            if(!empty($comment_list['data']['comment_list'])){
                $comment_list = $comment_list['data']['comment_list'];
            } else {
                $comment_list = [];
            }
            $gls[] = array(
                'gallery_id' => $album['gallery_id'],
                'class_id' => $album['class_id'],
                'class_name' => $album['class_name'],
                'team_id' => $album['team_id'],
                'team_name' => $album['team_name'],
                'gallery_name' => $album['gallery_name'],
                'visibility_range' => $album['visibility_range'],
                'gallery_description' => $album['gallery_description'],
                'gallery_type' => $album['gallery_type'],
                'gallery_date' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['gallery_date'])),
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered'])),
                'user_avt' => empty($album['user_avt']) ? $album['teacher_avt'] : $album['user_avt'],
                'created_by' => $album['created_by'],
                'full_user_name' => $album['full_user_name'],
                'total_like' => $this->countLikes($album['gallery_id'], 'C_Gallery'),
                'is_liked' => $this->isLiked($album['gallery_id'], 'C_Gallery', $student_id),
                'total_comment' => $this->countComments($album['gallery_id'], 'C_Gallery'),
                'comment_list' => $comment_list,
                'image_list' => $formatted_img_list,
            );
        }
        return [
            'success' => true,
            'data' => [
                'gallery_list' => $gls,
                'can_load' => $can_load,
                'page' => (int)$args['page']
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function setStudentRank(&$data, $arrClassFinish, $student_id)
    {
        // Set rank cho học viên theo điều kiện config trên mobile app config
        $admin = new Administration();
        $admin->retrieveSettings();
        $isShowRank = $admin->settings['default_student_ranking'];
        if ($isShowRank == 'true') {
            $rankPercent = $admin->settings['default_percentile_rank'];
            foreach ($arrClassFinish as $key => $studentClass) {
                foreach ($studentClass as $key2 => $infoStudentClass) {
                    if ($infoStudentClass['student_id'] == $student_id) {
                        $arrResultStudent[$key] = $infoStudentClass;
                    }
                }
            }

            foreach ($arrResultStudent as $idStudent => $infoStudentInClass) {
                foreach ($infoStudentInClass as $idClassStudentCheck => $infoStudent) {
                    foreach ($arrClassFinish as $classId => $studentClass) {
                        if ($classId == $idClassStudentCheck) {
                            $number_of_student = count($studentClass);
                            $opRank = 1;
                            $final_result_student_ago = 0;
                            foreach ($studentClass as $key2 => $infoStudentClass) {
                                if ((float)$infoStudentClass['final_result'] > (float)$infoStudent['final_result'] && (float)$infoStudentClass['final_result'] != $final_result_student_ago) {
                                    $opRank += 1;
                                }
                                $final_result_student_ago = (float)$infoStudentClass['final_result'];
                            }
                            $topRank = ($rankPercent / 100) * $number_of_student;
                            $topRank = number_format($topRank, 0);
                            //Nếu nằm trong top rank thì lấy, ngược lại mặc định trả về null
                            if ($opRank <= $topRank) {
                                foreach ($data as $key => $class) {
                                    if ($class['id'] == $idClassStudentCheck) {
                                        $data[$key]['rank_in_class'] = $opRank;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function getKOCofClass($classIds)
    {
        $koc = array();
        $q2_1 = "SELECT DISTINCT
                IFNULL(l1.id, '') koc_id,
                IFNULL(l1.name, '') koc_name,
                IFNULL(l1.kind_of_course, '') kind_of_course,
                IFNULL(l1.content, '') content,
                IFNULL(l1.lms_content, '') lms_content
                FROM j_class INNER JOIN j_kindofcourse l1 ON j_class.koc_id = l1.id  AND l1.deleted = 0
                WHERE (j_class.id IN ('$classIds')) AND j_class.deleted = 0";
        $rowK = $GLOBALS['db']->fetchArray($q2_1);
        foreach ($rowK as $key => $rK) {
            $koc[$rK['koc_id']]['koc_id'] = $rK['koc_id'];
            $koc[$rK['koc_id']]['koc_name'] = $rK['koc_name'];
            $koc[$rK['koc_id']]['kind_of_course'] = $rK['kind_of_course'];
            $koc[$rK['koc_id']]['content'] = ((!empty($rK['content'])) ? json_decode(html_entity_decode($rK['content']), true) : '');
        }
        return $koc;
    }

    function getClassGradeBook(&$data, $classIds, $student_id)
    {
        $q4 = "SELECT DISTINCT
                IFNULL(l2.id,'') student_id,
                IFNULL(l1.id, '') class_id,
                IFNULL(j_gradebook.id, '') gradebook_id,
                IFNULL(j_gradebook.status, '') status,
                IFNULL(j_gradebook.type, '') gradebook_type
                FROM j_gradebook
                INNER JOIN j_class_j_gradebook_1_c l1_1 ON j_gradebook.id = l1_1.j_class_j_gradebook_1j_gradebook_idb AND l1_1.deleted = 0
                INNER JOIN j_class l1 ON l1.id = l1_1.j_class_j_gradebook_1j_class_ida AND l1.deleted = 0
                INNER JOIN  j_classstudents l2_1 ON l1.id=l2_1.class_id AND l2_1.deleted=0
                INNER JOIN  contacts l2 ON l2.id=l2_1.student_id AND l2.deleted=0
                WHERE (((l1.id IN ('" . implode("','", $classIds) . "'))
                AND (l2.id = '{$student_id}')
                AND (IFNULL(j_gradebook.status, '') = 'Approved')))
                AND j_gradebook.deleted = 0
                ORDER BY class_id ASC,
                CASE WHEN
                (j_gradebook.type = '' OR j_gradebook.type IS NULL) THEN 0";
        $types = $GLOBALS['app_list_strings']['gradebook_type_options'];
        $count_k = 1;
        foreach ($types as $_type => $val_)
            $q4 .= " WHEN j_gradebook.type = '$_type' THEN " . $count_k++;
        $q4 .= " ELSE $count_k END ASC";
        $rs4 = $GLOBALS['db']->query($q4);
        while ($r4 = $GLOBALS['db']->fetchbyAssoc($rs4)) {
            $gradebook = BeanFactory::getBean('J_Gradebook', $r4['gradebook_id']);
            $gbArray = $gradebook->getDetailForStudent($r4['student_id']);
            $gbArray['gradebook_id'] = $r4['gradebook_id'];
            $gbArray['rate'] = $r4['rate'];
            foreach ($data as $key => $class) {
                if ($class['id'] == $r4['class_id'])
                    $data[$key]['gradebooks'][] = $gbArray;
            }
        }
    }
    function getPaymentList(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        //Get Payment
        $lastDate = date('Y-m-d', strtotime('last day of this month'));
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        global $timedate;
        $q3 = "SELECT DISTINCT
            IFNULL(j_paymentdetail.id, '') receipt_id,
            IFNULL(l1.id, '') payment_id,
            IFNULL(l2.id, '') student_id,
            IFNULL(j_paymentdetail.name,'') receipt_code,
            j_paymentdetail.expired_date expired_date,
            j_paymentdetail.payment_date receipt_date,
            j_paymentdetail.payment_datetime receipt_datetime,
            j_paymentdetail.payment_amount receipt_amount,
            j_paymentdetail.audited_amount audited_amount,
            IFNULL(j_paymentdetail.status, '') status,
            IFNULL(j_paymentdetail.reconcile_status, '') reconcile_status,
            IFNULL(j_paymentdetail.payment_method, '') method,
            IFNULL(j_paymentdetail.bank_account, '') bank_account,
            IFNULL(j_paymentdetail.description, '') description,
            IFNULL(l4.name, '') course_fee,
            IFNULL(l1.payment_type, '') payment_type,
            IFNULL(l1.kind_of_course, '') kind_of_course,
            IFNULL(l3.name, '') team_name
            FROM j_paymentdetail
            INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
            INNER JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
            INNER JOIN teams l3 ON l1.team_id = l3.id AND l3.deleted = 0
            LEFT JOIN j_coursefee_j_payment_1_c l4_1 ON l1.id = l4_1.j_coursefee_j_payment_1j_payment_idb AND l4_1.deleted = 0
            LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_1j_coursefee_ida AND l4.deleted = 0
            WHERE (((l2.id = '$student_id')
            AND (j_paymentdetail.payment_amount > 0)
            AND ( ((j_paymentdetail.status = 'Unpaid') AND ( (j_paymentdetail.expired_date <= '$lastDate') OR (j_paymentdetail.expired_date IS NULL) OR (j_paymentdetail.expired_date = '') )) OR ((j_paymentdetail.status = 'Paid')))))
            AND j_paymentdetail.deleted = 0
            ORDER BY l2.id ASC,
            CASE WHEN IFNULL(j_paymentdetail.status, '') = 'Paid' THEN 1
            WHEN IFNULL(j_paymentdetail.status, '') = 'Unpaid' THEN 2
            ELSE 3 END DESC, j_paymentdetail.payment_date DESC, j_paymentdetail.expired_date DESC";
        $rs3 = $GLOBALS['db']->query($q3);
        $runStudent = '####';

        while ($r3 = $GLOBALS['db']->fetchbyAssoc($rs3)) {
            if ($runStudent != $r3['student_id']) $key = 0;
            $payment = array();
            $payment['receipt_id'] = $r3['receipt_id'];
            $payment['payment_id'] = $r3['payment_id'];
            $payment['receipt_code'] = $r3['receipt_code'];
            $payment['receipt_date_db'] = $r3['receipt_date'];
            $payment['receipt_date'] = $timedate->to_display_date($r3['receipt_date'], false);
            $payment['expired_date'] = $timedate->to_display_date($r3['expired_date'], false);
            $payment['receipt_datetime'] = $timedate->to_display_date_time($r3['receipt_datetime']);

            $payment['reconcile_status'] = $r3['reconcile_status'];
            $payment['audited_amount'] = $r3['audited_amount'];
            $payment['bank_account'] = $r3['bank_account'];

            $payment['expired_date'] = $timedate->to_display_date($r3['expired_date'], false);
            $payment['receipt_amount'] = format_number($r3['receipt_amount']);
            $payment['status'] = $r3['status'];
            if ($payment['status'] == 'Unpaid') {
                $payment['receipt_date_db'] = '';
                $payment['receipt_date'] = '';
            }
            $payment['method'] = $r3['method'];
            $payment['course_fee'] = $r3['course_fee'];
            if ($r3['payment_type'] == 'Book/Gift' || $r3['payment_type'] == 'Deposit')
                $payment['course_fee'] = $r3['payment_type'];

            $payment['payment_type'] = $r3['payment_type'];
            $payment['description'] = $r3['description'];
            $payment['team_name'] = $r3['team_name'];

            $data[] = $payment;
        }
        return [
            'success' => true,
            'data' => [
                'payment_list' => $data
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function getPaymentById(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'receipt_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        //Get Payment
        $lastDate = date('Y-m-d', strtotime('last day of this month'));
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        global $timedate;
        $q3 = "SELECT DISTINCT
            IFNULL(j_paymentdetail.id, '') receipt_id,
            IFNULL(l1.id, '') payment_id,
            IFNULL(l2.id, '') student_id,
            IFNULL(j_paymentdetail.name,'') receipt_code,
            j_paymentdetail.expired_date expired_date,
            j_paymentdetail.payment_date receipt_date,
            j_paymentdetail.payment_datetime receipt_datetime,
            j_paymentdetail.payment_amount receipt_amount,
            j_paymentdetail.audited_amount audited_amount,
            IFNULL(j_paymentdetail.status, '') status,
            IFNULL(j_paymentdetail.reconcile_status, '') reconcile_status,
            IFNULL(j_paymentdetail.payment_method, '') method,
            IFNULL(j_paymentdetail.bank_account, '') bank_account,
            IFNULL(j_paymentdetail.description, '') description,
            IFNULL(l4.name, '') course_fee,
            IFNULL(l1.payment_type, '') payment_type,
            IFNULL(l1.kind_of_course, '') kind_of_course,
            IFNULL(l3.name, '') team_name
            FROM j_paymentdetail
            INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
            INNER JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
            INNER JOIN teams l3 ON l1.team_id = l3.id AND l3.deleted = 0
            LEFT JOIN j_coursefee_j_payment_1_c l4_1 ON l1.id = l4_1.j_coursefee_j_payment_1j_payment_idb AND l4_1.deleted = 0
            LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_1j_coursefee_ida AND l4.deleted = 0
            WHERE j_paymentdetail.id = '{$args['receipt_id']}'";
        $result = $GLOBALS['db']->fetchOne($q3);
        $payment = array();
        $payment['receipt_id'] = $result['receipt_id'];
        $payment['payment_id'] = $result['payment_id'];
        $payment['receipt_code'] = $result['receipt_code'];
        $payment['receipt_date_db'] = $result['receipt_date'];
        $payment['receipt_date'] = $timedate->to_display_date($result['receipt_date'], false);
        $payment['expired_date'] = $timedate->to_display_date($result['expired_date'], false);
        $payment['receipt_datetime'] = $timedate->to_display_date_time($result['receipt_datetime']);

        $payment['reconcile_status'] = $result['reconcile_status'];
        $payment['audited_amount'] = $result['audited_amount'];
        $payment['bank_account'] = $result['bank_account'];

        $payment['expired_date'] = $timedate->to_display_date($result['expired_date'], false);
        $payment['receipt_amount'] = format_number($result['receipt_amount']);
        $payment['status'] = $result['status'];
        if ($payment['status'] == 'Unpaid') {
            $payment['receipt_date_db'] = '';
            $payment['receipt_date'] = '';
        }
        $payment['method'] = $result['method'];
        $payment['course_fee'] = $result['course_fee'];
        if ($result['payment_type'] == 'Book/Gift' || $result['payment_type'] == 'Deposit')
            $payment['course_fee'] = $result['payment_type'];

        $payment['payment_type'] = $result['payment_type'];
        $payment['description'] = $result['description'];
        $payment['team_name'] = $result['team_name'];

        return [
            'success' => true,
            'data' => [
                'payment' => $payment
            ],
            'error_info' => throwApiError(1)
        ];
    }
    function getNewsList(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');
        $validate_param = validateParam($args, ['access_token']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        //Get New
        global $timedate;
        $qCenterInContact = "SELECT DISTINCT IFNULL(contacts.id, '') student_id,
            IFNULL(l1.id, '') team_id,
            IFNULL(l1.name, '') team_name,
            IFNULL(l1.parent_id, '') parent_team_id
            FROM contacts
            INNER JOIN team_sets_teams l1_1 ON contacts.team_set_id = l1_1.team_set_id AND l1_1.deleted = 0
            INNER JOIN teams l1 ON l1.id = l1_1.team_id AND l1.deleted = 0 AND l1.private = 0
            WHERE contacts.deleted = 0
            AND (contacts.id = '$student_id')";

        $resultCenter = $GLOBALS['db']->query($qCenterInContact);

        while ($r = $GLOBALS['db']->fetchbyAssoc($resultCenter)) {
            $teams = array();
            $teams_id = $r['team_id'];
            // Get parent center
            if (!empty($r['parent_team_id'])) {
                $parentCenterIds = array();
                $parentCenterIds[] = $r['parent_team_id'];
                $parentCenter = BeanFactory::getBean('Teams', $r['parent_team_id']);
                while (!empty($parentCenter->parent_id)) {
                    $parentCenterIds[] = $parentCenter->parent_id;
                    $parentCenter = BeanFactory::getBean('Teams', $parentCenter->parent_id);
                }
            }

            $arrIdCenter[] = $teams_id;
            $arrIdCenter = array_unique(array_merge($arrIdCenter, $parentCenterIds), SORT_REGULAR);
        }

        $qGetNews = "SELECT DISTINCT IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin, 0) pin,
            IFNULL(c_news.addon_views, 0) addon_views,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.news_content, '') content,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            INNER JOIN team_sets_teams l1_1 ON c_news.team_set_id = l1_1.team_set_id AND l1_1.deleted = 0
            INNER JOIN teams l1 ON l1.id = l1_1.team_id AND l1.deleted = 0 AND l1.private = 0
            WHERE (c_news.status NOT IN ('Inactive'))
            AND (c_news.type LIKE '%^Student^%')
            AND c_news.deleted = 0
            AND l1.id IN ('" . implode("', '", $arrIdCenter) . "')
            ORDER BY date_entered DESC";

        $rsGetNews = $GLOBALS['db']->query($qGetNews);

        while ($r = $GLOBALS['db']->fetchbyAssoc($rsGetNews)) {
            $qViewNews = "SELECT IFNULL(SUM(count_read_news) , 0)
                FROM c_news_contacts_1_c
                WHERE deleted = 0 AND c_news_contacts_1c_news_ida = '{$r['primaryid']}'";
            $views = $GLOBALS['db']->getOne($qViewNews);
            $srcImg =  strlen($r['thumbnail']) != 36 ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] .  $r['thumbnail'] : $GLOBALS['dotb_config']['site_url'] . '/download_attachment.php?id=s3_storage/' . $r['thumbnail'];
            //HOT FIX: Remove in future
            $periodPosition = strrpos($r['thumbnail'], '.');
            if ($periodPosition === false) {
                $periodPosition = strlen($r['thumbnail']);
            }
            $startPosition = max(0, $periodPosition - 36);
            $thumbnail_id = substr($r['thumbnail'], $startPosition, $periodPosition - $startPosition);
            //end

            $news = array();
            $news['id'] = $r['primaryid'];
            $news['name'] = $r['name'];
            $news['thumbnail_url'] = $srcImg;
            $news['thumbnail'] = $thumbnail_id; //HOT FIX
            $news['url'] = $r['url'];
            $news['type'] = $r['type'];
            $news['addon_views'] = $r['addon_views'];
            $news['content'] = $r['content'];
            $news['pin'] = $r['pin'];
            $news['description'] = $r['description'];
            $news['type_news'] = $r['type_news'];
            $news['date_entered'] = $timedate->to_display_date_time($r['date_entered']);
            $news['end_date'] = $timedate->to_display_date($r['end_date'], false);
            $news['start_date'] = $timedate->to_display_date($r['start_date'], false);
            $news['views'] = $views;
            $news['db_date_entered'] = $r['db_date_entered'];
            $news['db_end_date'] = $r['db_end_date'];
            $news['db_start_date'] = $r['db_start_date'];
            $new_list[] = $news;
        }

        return [
            'success' => true,
            'data' => [
                'news_list' => $new_list
            ],
            'error_info' => throwApiError(1)
        ];

    }
    function getNewsById(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');
        $validate_param = validateParam($args, ['access_token', 'news_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        //Get New
        global $timedate;
        $qGetNews = "SELECT DISTINCT IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin, 0) pin,
            IFNULL(c_news.addon_views, 0) addon_views,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.news_content, '') content,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            WHERE c_news.id = '{$args['news_id']}'";

        $result = $GLOBALS['db']->fetchOne($qGetNews);

        $qViewNews = "SELECT IFNULL(SUM(count_read_news) , 0)
            FROM c_news_contacts_1_c
            WHERE deleted = 0 AND c_news_contacts_1c_news_ida = '{$args['news_id']}'";
        $views = $GLOBALS['db']->getOne($qViewNews);
        $srcImg =  strlen($result['thumbnail']) != 36 ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] .  $result['thumbnail'] : $GLOBALS['dotb_config']['site_url'].'/upload/s3_storage/'. $result['thumbnail'];
        $news = array();
        //HOT FIX: Remove in future
        $periodPosition = strrpos($r['thumbnail'], '.');
        if ($periodPosition === false) {
            $periodPosition = strlen($result['thumbnail']);
        }
        $startPosition = max(0, $periodPosition - 36);
        $thumbnail_id = substr($result['thumbnail'], $startPosition, $periodPosition - $startPosition);
        //end
        $news['id'] = $result['primaryid'];
        $news['name'] = $result['name'];
        $news['thumbnail'] = $thumbnail_id; // HOT FIX
        $news['thumbnail_url'] = $srcImg;
        $news['url'] = $result['url'];
        $news['type'] = $result['type'];
        $news['addon_views'] = $result['addon_views'];
        $news['content'] = $result['content'];
        $news['pin'] = $result['pin'];
        $news['description'] = $result['description'];
        $news['type_news'] = $result['type_news'];
        $news['date_entered'] = $timedate->to_display_date_time($result['date_entered']);
        $news['end_date'] = $timedate->to_display_date($result['end_date'], false);
        $news['start_date'] = $timedate->to_display_date($result['start_date'], false);
        $news['views'] = $views;

        return [
            'success' => true,
            'data' => [
                'news' => $news
            ],
            'error_info' => throwApiError(1)
        ];

    }

    function isLiked($parent_id, $parent_type, $contact_id){
        $relationship_table_map = [
            'C_Gallery' => 'c_gallery_contacts_1',
            'C_News' => 'c_news_contacts_2'
        ];
        $table = $relationship_table_map[$parent_type] . '_c';
        $fieldA = $relationship_table_map[$parent_type] . strtolower($parent_type) . '_ida';
        $fieldB = $relationship_table_map[$parent_type] . 'contacts_idb';
        $sqlCountLike = "SELECT COUNT(*)
        FROM $table 
        WHERE deleted = 0 AND $fieldA = '{$parent_id}' AND $fieldB = '{$contact_id}'";
        return $GLOBALS['db']->getOne($sqlCountLike) == 1;
    }
    function countLikes($parent_id, $parent_type)
    {
        $relationship_table_map = [
            'C_Gallery' => 'c_gallery_contacts_1',
            'C_News' => 'c_news_contacts_2'
        ];
        $table = $relationship_table_map[$parent_type] . '_c';
        $field = $relationship_table_map[$parent_type] . strtolower($parent_type) . '_ida';
        $sqlCountLike1 = "SELECT COUNT(*)
        FROM $table 
        WHERE deleted = 0 AND $field = '{$parent_id}'";
        $studentLikeAmount =  $GLOBALS['db']->getOne($sqlCountLike1);

        $relationship_table_map = [
            'C_Gallery' => 'c_gallery_c_teachers_1',
        ];
        $table = $relationship_table_map[$parent_type] . '_c';
        $field = $relationship_table_map[$parent_type] . strtolower($parent_type) . '_ida';
        $sqlCountLike2 = "SELECT COUNT(*)
        FROM $table 
        WHERE deleted = 0 AND $field = '{$parent_id}'";
        $teacherLikeAmount =  $GLOBALS['db']->getOne($sqlCountLike2);

        return $teacherLikeAmount + $studentLikeAmount;
    }
    private function getStudentFromAccessToken($access_token)
    {
        $q = "SELECT contact_id FROM oauth_tokens WHERE id = '$access_token'";
        $result = $GLOBALS['db']->fetchOne($q);
        if (!empty($result)) {
            return $result['contact_id'];
        } else {
            return '';
        }
    }
    //Move from Mobile API
    function editStudentAvt(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['id', 'type']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        require_once('include/DotbFields/Fields/Image/ImageHelper.php');

        $id = $args['id'];
        $bean = BeanFactory::getBean('Contacts', $id);
        unset($args['id']);

        $name = create_guid();

        if (!is_dir('upload/origin')) {
            mkdir('upload/origin');
        }
        if (!is_dir('upload/resize')) {
            mkdir('upload/resize');
        }
        $file1 = 'upload/origin/' . $name;
        $file2 = 'upload/resize/' . $name;

        move_uploaded_file($_FILES['picture']['tmp_name'], $file1);

        file_put_contents($file2, file_get_contents($file1));

        $imgOri1 = new ImageHelper($file1);
        $imgOri1->resize(500);
        $imgOri1->save($file1);

        $imgOri2 = new ImageHelper($file2);
        $imgOri2->resize(220, 220);
        $imgOri2->save($file2);

        if ($args['type'] == 'avatar') {
            $q1 = "UPDATE contacts
            SET contacts.picture = '$name' WHERE contacts.id = '$id'";
            $GLOBALS['db']->query($q1);
        } else {
            $q1 = "UPDATE contacts
            SET contacts.cover_app = '$name' WHERE contacts.id = '$id'";
            $GLOBALS['db']->query($q1);
        }

        return array(
            'success' => true,
            'data' => [
                'bean_id' => $bean->id,
            ],
            'error_info' => throwApiError(1)
        );
    }
    //end move from Mobile API
    function addNewsView(ServiceBase $api, $args){
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['news_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $sqlUpdateNew = "UPDATE c_news SET addon_views = addon_views + 1 WHERE id = '{$args['news_id']}'";
        $GLOBALS['db']->query($sqlUpdateNew);

        $sqlGetNew = "SELECT addon_views FROM c_news WHERE id = '{$args['news_id']}'";
        $views = $GLOBALS['db']->getOne($sqlGetNew);
        return [
            'success' => true,
            'data' => [
                'count_views' => $views == false ? 0 : (int)$views
            ],
            'error_info' => throwApiError(1)
        ];
    }

    public function getGalleryById(ServiceBase $api, $args)
    {
        require_once('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['access_token', 'gallery_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }

        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if (empty($student_id)) {
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }

        $gallery_id = $args['gallery_id'];
        $formatted_img_list = $this->getNote($gallery_id, 'C_Gallery');
        $sqlGallery = "SELECT
            IFNULL(gallery.id, '') gallery_id,
            IFNULL(gallery.name, '') gallery_name,
            IFNULL(gallery.gallery_date, '') gallery_date,
            IFNULL(gallery.description, '') gallery_description,
            IFNULL(gallery.gallery_type, '') gallery_type,
            IFNULL(gallery.created_by, '') created_by,
            IFNULL(gallery.date_entered, '') date_entered,
            IFNULL(gallery.visibility_range, '') visibility_range,
            IFNULL(user_creator.picture, '') user_avt,
            IFNULL(teacher.picture, '') teacher_avt,
            IFNULL(user_creator.full_user_name, '') full_user_name,
            IFNULL(gallery.team_id, '') team_id
            FROM  c_gallery gallery
                LEFT JOIN users user_creator ON user_creator.id = gallery.created_by
                LEFT JOIN c_teachers teacher ON teacher.id = user_creator.teacher_id
            WHERE gallery.id = '$gallery_id'";

        $album = $GLOBALS['db']->fetchOne($sqlGallery);
        if(!empty($album)){
            if($album['visibility_range'] == 'class'){
                $sqlClass = "SELECT
                    IFNULL(class.id, '') class_id,
                    IFNULL(class.name, '') class_name
                    FROM j_class class
                    INNER JOIN j_class_c_gallery_1_c class_gallery_link ON class.id = class_gallery_link.j_class_c_gallery_1j_class_ida AND class_gallery_link.deleted = 0
                    INNER JOIN c_gallery gallery ON gallery.id = class_gallery_link.j_class_c_gallery_1c_gallery_idb
                    WHERE gallery.id = '$gallery_id'";
                $class = $GLOBALS['db']->fetchOne($sqlClass);
                $album['class_id'] = $class['class_id'];
                $album['class_name'] = $class['class_name'];
            } else {
                $sqlCenter = "SELECT
                    IFNULL(team.id, '') team_id,
                    IFNULL(team.name, '') team_name
                    FROM teams team
                    WHERE team.id = '{$album['team_id']}'";
                $center = $GLOBALS['db']->fetchOne($sqlCenter);
                $album['team_id'] = $center['team_id'];
                $album['team_name'] = $center['team_name'];
            }
            $data = array(
                'gallery_id' => $album['gallery_id'],
                'class_id' => $album['class_id'],
                'class_name' => $album['class_name'],
                'team_id' => $album['team_id'],
                'team_name' => $album['team_name'],
                'gallery_name' => $album['gallery_name'],
                'visibility_range' => $album['visibility_range'],
                'gallery_description' => $album['gallery_description'],
                'gallery_type' => $album['gallery_type'],
                'gallery_date' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['gallery_date'])),
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered'])),
                'user_avt' => empty($album['user_avt']) ? $album['teacher_avt'] : $album['user_avt'],
                'created_by' => $album['created_by'],
                'full_user_name' => $album['full_user_name'],
                'total_like' => $this->countLikes($album['gallery_id'], 'C_Gallery'),
                'is_liked' => $this->isLiked($album['gallery_id'], 'C_Gallery', $student_id),
                'total_comment' => $this->countComments($album['gallery_id'], 'C_Gallery'),
                'comment_list' => $comment_list,
                'image_list' => $formatted_img_list,
            );
            return [
                'success' => true,
                'data' => $data,
                'error_info' => throwApiError(1)
            ];
        } else {
            return [
                'success' => false,
                'error_info' => throwApiError(106)
            ];
        }
    }
}
