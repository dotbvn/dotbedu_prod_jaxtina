<?php

class DotBIntergrationAPI extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'api3rd-update_lead' => array(
                'reqType' => 'POST',
                'path' => array('api3rd', 'update_lead'),
                'pathVars' => array(),
                'method' => 'updateLead',
                'noLoginRequired' => true,
            ),
            'api3rd-get_teams' => array(
                'reqType' => 'GET',
                'path' => array('api3rd', 'teams'),
                'pathVars' => array(),
                'method' => 'getTeams',
                'noLoginRequired' => true,
            ),
            'api3rd-get_campaigns' => array(
                'reqType' => 'GET',
                'path' => array('api3rd', 'campaigns'),
                'pathVars' => array(),
                'method' => 'getCampaigns',
                'noLoginRequired' => true,
            ),
            'api3rd-get_enum_options' => array(
                'reqType' => 'GET',
                'path' => array('api3rd', 'enum_options'),
                'pathVars' => array(),
                'method' => 'getEnumOptions',
                'noLoginRequired' => true,
            ),
            'api3rd-create_appointment' => array(
                'reqType' => 'POST',
                'path' => array('api3rd', 'appointment'),
                'pathVars' => array(),
                'method' => 'createAppointment',
                'noLoginRequired' => true,
            ),
            'api3rd-delete_appointment' => array(
                'reqType' => 'POST',
                'path' => array('api3rd', 'delete_appointment'),
                'pathVars' => array(),
                'method' => 'deleteAppointment',
                'noLoginRequired' => true,
            )
        );
    }

    function validToken($key, $secret)
    {
        $result = $GLOBALS['db']->query("select id from oauth_consumer where c_key='{$key}' and c_secret='{$secret}' and deleted=0");
        if ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            if (!empty($row['id'])) {
                $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
                if (empty($api_user)) $api_user = '1';
                $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);
                return true;
            }
        }
        return false;
    }

    function deleteAppointment(ServiceBase $api, array $args)
    {
        if ($this->validToken($args['key'], $args['secret'])) {
            $bean = BeanFactory::newBean('Meetings');
            $bean->mark_deleted($args['appointment_id']);
            return array('success' => 1);
        }
        return array('success' => 0);
    }

    function createAppointment(ServiceBase $api, array $args)
    {
        if ($this->validToken($args['key'], $args['secret'])) {
            $person = BeanFactory::getBean('Leads', $args['lead_id']);
            $personType = 'Leads';
            if (empty($person->id)) return array('success' => 0, 'msg' => 'Lead not found');
            if ($person->converted == 1 && !empty($person->contact_id)) {
                $person = BeanFactory::getBean('Contacts', $person->contact_id);
                $personType = 'Contacts';
            }
            $bean = BeanFactory::newBean('Meetings');
            $bean->name = $args['data']['name'];
            $bean->date_start = $args['data']['date_start'];
            $bean->date_end = $args['data']['date_end'];
            $bean->description = $args['data']['description'];
            $bean->team_id = $args['data']['team_id'];
            $bean->team_set_id = $args['data']['team_set_id'];
            $bean->meeting_type = 'Meeting';
            $bean->parent_type = $personType;
            $bean->parent_id = $person->id;
            $bean->save();
            return array('success' => 1, 'data' => 'Meeting created: ' . $bean->id);
        }
        return array('success' => 0);
    }

    function getTeams(ServiceBase $api, array $args)
    {
        if ($this->validToken($args['key'], $args['secret'])) {
            global $db;
            $data = array('1', 'Global', 'GLOBAL');
            $ids = $db->fetchArray("select id from teams where parent_id='1'");
            foreach ($ids as $id) {
                $result = $db->query("select id,name,code_prefix from teams where parent_id='{$id}'");
                while ($row = $db->fetchByAssoc($result)) {
                    $data[] = $row;
                }
            }
            return array('success' => 1, 'data' => $data);
        }
        return array('success' => 0);
    }

    function getCampaigns(ServiceBase $api, array $args)
    {
        if ($this->validToken($args['key'], $args['secret'])) {
            global $db;
            $data = $db->fetchArray("select * from campaigns where deleted=0 order by date_modified DESC");
            return array('success' => 1, 'data' => $data);
        }
        return array('success' => 0);
    }

    function getEnumOptions(ServiceBase $api, array $args)
    {
        if ($this->validToken($args['key'], $args['secret'])) {
            if (empty($args['name'])) return array('success' => 0);
            $vn = return_app_list_strings_language('vn_vn');
            $en = return_app_list_strings_language('en_us');
            $data = array('vn_vn' => array(), 'en_us' => array());
            if (!empty($vn[$args['name']])) $data['vn_vn'] = $vn[$args['name']];
            if (!empty($vn[$args['name']])) $data['en_us'] = $en[$args['name']];
            return array('success' => 1, 'data' => $data);
        }
        return array('success' => 0);
    }

    function updateLead(ServiceBase $api, array $args)
    {
        if ($this->validToken($args['key'], $args['secret'])) {
            $bean = BeanFactory::getBean('Leads', $args['lead_id']);
            if (empty($bean->id)) return array('success' => 0, 'msg' => 'Lead not found');
            if ($bean->converted == 1 && !empty($bean->contact_id)) {
                return array('success' => 0, 'msg' => 'Lead had converted to Student');
            }

            //check status
            if (isset($args['status']) && !in_array($args['status'], array('New', 'In Process', 'Dead'))) {
                return array('success' => 0, 'msg' => 'Invalid input: status');
            }

            //team_id, default global
            if (isset($args['team_id'])) {
                $bean->team_set_id = $args['team_id'];
            } else {
                $bean->team_id = '1';
                $bean->team_set_id = '1';
            }


            foreach ($args['data'] as $key => $value) {
                if (isset($bean->$key)) {
                    $bean->$key = $value;
                }
            }
            $bean->name = trim($bean->last_name . ' ' . $bean->first_name);

            //Check duplicate
            $duplicates = $bean->findDuplicates();
            if (count($duplicates['records']) > 0) {
                return array('success' => 0, 'msg' => 'duplicated found: ' . $duplicates['records'][0]['id']);
            }

            $bean->save();
            return array('success' => 1);
        }
        return array('success' => 0);
    }

}
