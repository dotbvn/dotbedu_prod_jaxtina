<?php

class FeedbackMobileV3 extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'list_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'list-comments'),
                'pathVars' => array(),
                'method' => 'listComments',
                'noLoginRequired' => true,
            ),
            'save_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'save-comment'),
                'pathVars' => array(),
                'method' => 'saveComment',
                'noLoginRequired' => true,
            ),
            'save_feedback_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'save-feedback'),
                'pathVars' => array(),
                'method' => 'saveFeedback',
                'noLoginRequired' => true,
            ),
            'save_feedback_v3_new' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'save-feedback-detail'),
                'pathVars' => array(),
                'method' => 'saveFeedbackDetail',
                'noLoginRequired' => true,
            ),
            'feedback_detail_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'get-feedback-detail'),
                'pathVars' => array(),
                'method' => 'feedbackDetail',
                'noLoginRequired' => true,
            ),
            'feedback_detail_v3_new' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'get-feedback-comment'),
                'pathVars' => array(),
                'method' => 'getFeedbackComment',
                'noLoginRequired' => true,
            ),
            'create_feedback_v3_temp' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'create-feeback'),
                'pathVars' => array(),
                'method' => 'createFeedback',
                'noLoginRequired' => true,
            ),
            'create_feedback_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'create-feedback'),
                'pathVars' => array(),
                'method' => 'createFeedback',
                'noLoginRequired' => true,
            ),
            'delete_feedback_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'delete-feedback'),
                'pathVars' => array(),
                'method' => 'deleteFeedback',
                'noLoginRequired' => true,
            ),
            'read_comment_v3' => array(
                'reqType' => 'POST',
                'path' => array('v3','feedback', 'read-comment'),
                'pathVars' => array(),
                'method' => 'readCommentInFeedback',
                'noLoginRequired' => true,
            ),
        );
    }

    /**
     *
     * @param array $args = data,case_id
     *  'path' => array('chat-message', 'save-comment'),
     */
    function saveComment(ServiceBase $api, array $args)
    {

        require_once 'include/utils/file_utils.php';
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['case_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $timedate, $dotb_config;

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $bean = new C_Comments();
        $bean->id = create_guid();
        $bean->new_with_id = true;
        $bean->description = $args['data'];
        $bean->parent_id = $args['case_id'];
        $bean->parent_type = 'Cases';
        $bean->direction = 'inbound';

        if (!empty($args['attachment'])) {
            $file = base64_decode($args['attachment']['data']);
            $filesize = filesize($file);
            $idUp = $bean->id;
            if ($filesize <= $dotb_config['upload_maxsize']) {
                file_put_contents('upload/' . $bean->id, $file);
                $bean->filename = $args['attachment']['name'];
                $bean->document_name = $args['attachment']['name'];

                $extension = get_file_extension($bean->filename);
                if (!empty($extension)) {
                    $bean->file_ext = $extension;
                    $bean->file_mime_type = get_mime_content_type_from_filename($bean->filename);
                }
                $logUpload = 'uploaded';
            } else {
                $logUpload = 'limited_filesize';
                $idUp = $bean->id;
                $bean->filename = '';
                $bean->file_ext = '';
                $bean->file_mime_type = '';
            }
        }
        if(!empty($_FILES['attachment'] || !empty($args['data']))){
            $bean->save();
            return array(
                'success' => true,
                'data' => array (
                    'id' => $bean->id,
                    'is_read_inems' => $bean->is_read_inems,
                    'is_read_inapp' => $bean->is_read_inapp,
                    'is_updated' => $bean->is_updated,
                    'direction' => $bean->direction,
                    'description' => $bean->description,
                    'date_entered' => $timedate->to_display_date_time($bean->date_entered),
                    'create_by' => $args['api_user_id'],
                    'images' => array(
                        'log_upload' => $logUpload,
                        'id' => $idUp,
                        'name' => $bean->filename,
                        'file_ext' => $bean->file_ext,
                        'file_mime_type' => $bean->file_mime_type,
                    )
                ),
                'error_info' => throwApiError(1)
            );
        } else {
            return array(
                'success' => false,
                'error_info' => throwApiError(118)
            );
        }
    }

    /**
     *
     * @param array $args = case_id
     * 'path' => array('chat-message', 'list-comments'),
     */
    function listComments(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['case_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $timedate, $db, $locale;
        $data = array();

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $r = $db->query("SELECT DISTINCT
                IFNULL(l1.id, '') case_id,
                IFNULL(c_comments.id, '') primaryid,
                IFNULL(c_comments.filename, '') filename,
                IFNULL(c_comments.description, '') description,
                IFNULL(c_comments.direction, '') direction,
                IFNULL(c_comments.file_ext, '') file_ext,
                IFNULL(c_comments.file_mime_type, '') file_mime_type,
                IFNULL(c_comments.is_read_inems, 0) is_read_inems,
                IFNULL(c_comments.is_read_inapp, 0) is_read_inapp,
                c_comments.date_entered date_entered,
                IFNULL(l2.id, '') created_by,
                IFNULL(l2.full_user_name, '') created_by_name,
                IFNULL(l2.picture, '') created_by_picture
                FROM c_comments
                INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases' AND l1.deleted = 0 AND l1.portal_viewable = 1
                LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
                WHERE (((l1.id = '{$args['case_id']}')))
                AND c_comments.deleted = 0
                ORDER BY c_comments.date_entered ASC");
        while ($row = $db->fetchByAssoc($r)) {
            $data[] = array(
                'direction' => $row['direction'],
                'description' => $row['description'],
                'date_entered' => $timedate->to_display_date_time($row['date_entered']),
                'date_entered_db' => date('Y-m-d H:i:s', strtotime("+7 hours " . $row['date_entered'])),
                'created_by' => $row['created_by'],
                'created_by_name' => $row['created_by_name'],
                'created_by_picture' => $row['created_by_picture'],
                'is_read_inems' => $row['is_read_inems'],
                'is_read_inapp' => $row['is_read_inapp'],
                'images' => array(
                    'id' => $row['primaryid'],
                    'name' => $row['filename'],
                    'file_ext' => $row['file_ext'],
                    'file_mime_type' => $row['file_mime_type'],
                )
            );
        }
        return array(
            'success' => true,
            'data' => [
                'list_comment' => $data
            ],
            'error_info' => throwApiError(1)
        );
    }
    function saveFeedback(ServiceBase $api, array $args)
    {
        require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';
        require_once 'include/utils/file_utils.php';
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['case_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        global $timedate;

        $message = 'uploaded';

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        if (!empty($args['message_id'])) {
            // Update message
            $bean = BeanFactory::getBean('C_Comments', $args['message_id']);
            $bean->description = $args['data'];
            $bean->is_updated = 1;

            // Remove old files
            if (!empty($_FILES['attachment'])) {
                $r1 = $GLOBALS['db']->query("SELECT DISTINCT IFNULL(c_uploadimage.id, '') id
                FROM c_uploadimage
                WHERE (c_uploadimage.comment_id = '{$args['message_id']}'AND c_uploadimage.deleted = 0)");
                while ($img = $GLOBALS['db']->fetchByAssoc($r1)) {
                    if (!empty($img['id'])) {
                        $images = BeanFactory::getBean('C_UploadImage', $img['id']);
                        $images->mark_deleted($img['id']);
                    }
                }
            }
        } else {
            // Create new message
            $bean = new C_Comments();
            $bean->id = create_guid();
            $bean->new_with_id = true;
            $bean->description = $args['data'];
            $bean->parent_id = $args['case_id'];
            $bean->parent_type = 'Cases';
            $bean->direction = 'inbound';
        }
        // Create new file
        if (!empty($_FILES['attachment'])) {
            global $dotb_config;
            $AWS = new AWSHelper();
            if (!$AWS->getS3())
                return array(
                    'success' => false,
                    'error_info' => throwApiError(203)
                );
            $count_picture = count($_FILES['attachment']['name']);
            if ($count_picture > 0)
                for ($i = 0; $i < $count_picture; $i++) {
                    $beanImage = BeanFactory::getBean('C_UploadImage');
                    $beanImage->file_mime_type = $_FILES['attachment']['type'][$i];
                    $beanImage->file_ext = pathinfo($_FILES['attachment']['name'][$i], PATHINFO_EXTENSION);
                    $beanImage->filename = $_FILES['attachment']['name'][$i];
                    $beanImage->document_name = 's3_image';
                    $beanImage->comment_id = $bean->id;
                    $beanImage->save();
                    $savefile = 'upload/' . $beanImage->id;
                    move_uploaded_file($_FILES['attachment']['tmp_name'][$i], $savefile);
                    $result = $AWS->uploadAWS($dotb_config['unique_key'].'/comment', $beanImage->id, $savefile);
                    if($result['success']){
                        $images_list[] = [
                            's3Link' => $result['url']
                        ];
                    }
                    unlink($savefile);
                }
            else $message = 'list image null';
        } else $message = 'list image null';

        if(!empty($_FILES['attachment']) || !empty($args['data'])){
            $bean->save();
            return array(
                'success' => true,
                'data' => array (
                    'id' => $bean->id,
                    'is_read_inems' => $bean->is_read_inems,
                    'is_read_inapp' => $bean->is_read_inapp,
                    'is_updated' => $bean->is_updated,
                    'direction' => $bean->direction,
                    'description' => $bean->description,
                    'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $bean->date_entered)),
                    'create_by' => $args['api_user_id'],
                    'attachment' => $images_list,
                    'message' => $message
                ),
                'error_info' => throwApiError(1)
            );
        } else {
            return array(
                'success' => false,
                'error_info' => throwApiError(118)
            );
        }
    }
    function saveFeedbackDetail(ServiceBase $api, array $args)
    {
        require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';
        require_once 'include/utils/file_utils.php';
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['case_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $message = 'uploaded';

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        if (!empty($args['message_id'])) {
            // Update message
            $bean = BeanFactory::getBean('C_Comments', $args['message_id']);
            $bean->description = $args['data'];
            $bean->is_updated = 1;

            // Remove old files
            if (!empty($_FILES['attachment'])) {
                $r1 = $GLOBALS['db']->query("SELECT DISTINCT IFNULL(notes.id, '') id
                FROM notes
                WHERE notes.parent_id = '{$args['message_id']}' 
                       AND notes.parent_type = 'C_Comments' 
                       AND notes.deleted = 0");
                while ($attachment = $GLOBALS['db']->fetchByAssoc($r1)) {
                    if (!empty($attachment['id'])) {
                        $note = BeanFactory::getBean('Notes', $attachment['id']);
                        $note->mark_deleted($attachment['id']);
                    }
                }
            }
        } else {
            // Create new message
            $bean = new C_Comments();
            $bean->id = create_guid();
            $bean->new_with_id = true;
            $bean->description = $args['data'];
            $bean->parent_id = $args['case_id'];
            $bean->parent_type = 'Cases';
            $bean->direction = 'inbound';
            $bean->source = 'mobile';
        }
        // Create new file
        if (!empty($_FILES['attachment'])) {
            $count_picture = count($_FILES['attachment']['name']);
            if ($count_picture > 0){
                for ($i = 0; $i < $count_picture; $i++) {
                    $fileName = $_FILES['attachment']['name'][$i];
                    $fileTmpLoc = $_FILES['attachment']['tmp_name'][$i];
                    $fileType = $_FILES['attachment']['type'][$i];
                    $fileSize = $_FILES['attachment']['size'][$i];

                    $note = BeanFactory::newBean('Notes');
                    $note->id = create_guid();
                    $note->new_with_id = true;
                    $note->name = $note->id . '.' . pathinfo($fileName, PATHINFO_EXTENSION);
                    $note->file_mime_type = $fileType;
                    $note->file_ext = pathinfo($fileName, PATHINFO_EXTENSION);
                    $note->filename = $fileName;
                    $note->file_size = $fileSize;
                    $note->parent_type = 'C_Comments';
                    $note->save();
                    $file_name = $note->name;

                    $sourceFile = 'upload/s3_storage/' . $file_name;
                    if (move_uploaded_file($fileTmpLoc, $sourceFile)) {
                        $attachment_list[] = [
                            'file_id' => $note->id,
                            'file_name' => $fileName,
                            'file_url' => $GLOBALS['dotb_config']['site_url'].'/'.$sourceFile,
                            'file_size' => $note->file_size,
                            'file_type' => $fileType,
                            'file_ext' => $note->file_ext
                        ];
                    }
                }
            } else $message = 'list image null';
        } else $message = 'list image null';

        if(!empty($_FILES['attachment']) || !empty($args['data'])){
            $add_attachment_list = [];
            if (!empty($attachment_list)){
                foreach($attachment_list as $add_attachment) {
                    $add_attachment_list[] = [
                        'id' => $add_attachment['file_id'],
                        'action' => 'add'
                    ];
                }
            }
            $bean->attachment_list = $add_attachment_list;
            $bean->save();
            return array(
                'success' => true,
                'data' => array (
                    'id' => $bean->id,
                    'is_read_inems' => $bean->is_read_inems,
                    'is_read_inapp' => $bean->is_read_inapp,
                    'is_updated' => $bean->is_updated,
                    'direction' => $bean->direction,
                    'description' => $bean->description,
                    'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $bean->date_entered)),
                    'create_by' => $args['api_user_id'],
                    'attachment' => $attachment_list,
                    'message' => $message
                ),
                'error_info' => throwApiError(1)
            );
        } else {
            return array(
                'success' => false,
                'error_info' => throwApiError(118)
            );
        }
    }
    function getFeedbackComment(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['case_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $last_comment_id = $args['last_comment_id'];
        $data = array();
        if(!empty($last_comment_id)){
            $q = "SELECT date_entered FROM c_comments WHERE id = '{$last_comment_id}'";
            $date = $GLOBALS['db']->getOne($q);
            $ext = "AND comment.date_entered < '$date'";
        }

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $r = $GLOBALS['db']->query("SELECT DISTINCT
                IFNULL(feedback.id, '') case_id,
                IFNULL(comment.id, '') primaryid,
                IFNULL(comment.description, '') description,
                IFNULL(comment.direction, '') direction,
                comment.date_entered date_entered,
                IFNULL(user.id, '') user_id,
                IFNULL(user.first_name, '') first_name,
                IFNULL(user.last_name, '') last_name,
                IFNULL(user.picture, '') avatar,
                IFNULL(comment.filename, '') filename,
                IFNULL(comment.file_ext, '') file_ext,
                IFNULL(comment.file_mime_type, '') file_mime_type,
                IFNULL(comment.is_read_inems, 0) is_read_inems,
                IFNULL(comment.is_read_inapp, 0) is_read_inapp,
                IFNULL(comment.is_updated, 0) is_updated
                FROM c_comments comment
                INNER JOIN cases feedback ON feedback.id = comment.parent_id AND comment.parent_type = 'Cases'
                LEFT JOIN users user ON comment.created_by = user.id AND user.deleted = 0
                WHERE (((feedback.id = '{$args['case_id']}')))
                AND comment.is_unsent = 0 AND comment.deleted = 0
                {$ext}
                ORDER BY comment.date_entered DESC
                LIMIT 30");

        $can_load = true;
        $count = 0;
        while ($row = $GLOBALS['db']->fetchByAssoc($r)) {
            $create_by = $row['user_id'];
            if ($row['user_id'] == $api_user) $create_by = 'apps_admin';
            //Get the attachment of each comment
            $sqlNote = "SELECT IFNULL(l1.id, '') attachment_id,
                        IFNULL(l1.name, '') name,
                        IFNULL(l1.upload_id, '') upload_id,
                        IFNULL(l1.filename, '') filename,
                        IFNULL(l1.file_size, '') file_size,
                        IFNULL(l1.file_source, '') file_source,
                        IFNULL(l1.file_mime_type, '') file_mime_type,
                        IFNULL(l1.file_ext, '') file_ext
                        FROM notes l1 
                        WHERE l1.parent_id = '{$row['primaryid']}' AND l1.parent_type = 'C_Comments' AND l1.deleted = 0";
            $attachment_list = $GLOBALS['db']->fetchArray($sqlNote);
            $formatted_attachment_list = [];
            //Format img list
            foreach($attachment_list as $attachment){
                $srcImg = $attachment['file_source'] == 'S3'
                    ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $attachment['upload_id']
                    : $GLOBALS['dotb_config']['site_url'].'/upload/s3_storage/'.$attachment['name'];
                $fileType = explode('/', $attachment['file_mime_type'])[0]; //image or video
                $formatted_attachment_list[] = array(
                    'file_id' => $attachment['attachment_id'],
                    'file_source' => empty($attachment['file_source']) ? 'local' : $attachment['file_source'],
                    'file_type' => $fileType,
                    'file_size' => $attachment['file_size'],
                    'file_name' => $attachment['filename'],
                    'file_ext' => $attachment['file_ext'],
                    'file_url' => $srcImg
                );
            }

            $data[] = array(
                'id' => $row['primaryid'],
                'direction' => $row['direction'],
                'description' => $row['description'],
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $row['date_entered'])),
                'created_by' => $create_by,
                'created_by_name' => $GLOBALS['locale']->getLocaleFormattedName($row['first_name'], $row['last_name']),
                'created_by_avatar' => $row['avatar'],
                'attachment' => $formatted_attachment_list,
                'is_read_inems' => $row['is_read_inems'],
                'is_read_inapp' => $row['is_read_inapp'],
                'is_updated' => $row['is_updated'],
            );
            $count++;
            if($count == 20){
                $check_can_load_id = $row['primaryid'];
            }
        }

        //Update is_read_inapp to 1
        $GLOBALS['db']->query("UPDATE c_comments SET is_read_inapp = 1 WHERE parent_id = '{$args['case_id']}' AND parent_type = 'Cases' AND deleted = 0");

        if($count < 20){
            $can_load = false;
        } else if($count == 20){
            $q = "SELECT date_entered FROM c_comments WHERE id = '{$check_can_load_id}'";
            $date = $GLOBALS['db']->getOne($q);
            $ext = "AND c_comments.date_entered < '$date'";

            $count_comment = "SELECT DISTINCT
                COUNT(c_comments.id) count_comment
                FROM c_comments
                INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases' AND l1.portal_viewable = 1
                LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
                WHERE (((l1.id = '{$args['case_id']}')))
                AND c_comments.is_unsent = 0 AND c_comments.deleted = 0
                $ext";

            if(intVal($GLOBALS['db']->getOne($count_comment)) == 0){
                $can_load = false;
            }
        }
        return array(
            'success' => true,
            'data' => [
                'can_load' => $can_load,
                'list_comment' => $data,
            ],
            'error_info' => throwApiError(1)
        );
    }

    function feedbackDetail(ServiceBase $api, array $args)
    {
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['case_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $last_comment_id = $args['last_comment_id'];
        global $timedate, $db, $locale,$dotb_config;
        $data = array();
        if(!empty($last_comment_id)){
            $q = "SELECT date_entered FROM c_comments WHERE id = '{$last_comment_id}'";
            $date = $GLOBALS['db']->getOne($q);
            $ext = "AND c_comments.date_entered < '$date'";
        }

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $r = $db->query("SELECT DISTINCT
                IFNULL(l1.id, '') case_id,
                IFNULL(c_comments.id, '') primaryid,
                IFNULL(c_comments.description, '') description,
                IFNULL(c_comments.direction, '') direction,
                c_comments.date_entered date_entered,
                IFNULL(l2.id, '') user_id,
                IFNULL(l2.first_name, '') first_name,
                IFNULL(l2.last_name, '') last_name,
                IFNULL(l2.picture, '') avatar,
                IFNULL(c_comments.filename, '') filename,
                IFNULL(c_comments.file_ext, '') file_ext,
                IFNULL(c_comments.file_mime_type, '') file_mime_type,
                IFNULL(c_comments.is_read_inems, 0) is_read_inems,
                IFNULL(c_comments.is_read_inapp, 0) is_read_inapp,
                IFNULL(c_comments.is_updated, 0) is_updated
                FROM c_comments
                INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases'
                LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
                WHERE (((l1.id = '{$args['case_id']}')))
                AND c_comments.is_unsent = 0 AND c_comments.deleted = 0
                {$ext}
                ORDER BY c_comments.date_entered DESC
                LIMIT 30");

        $can_load = true;
        $count = 0;
        while ($row = $db->fetchByAssoc($r)) {
            $create_by = $row['user_id'];
            if ($row['user_id'] == $api_user) $create_by = 'apps_admin';
            $attachment = [];
            if ($row['direction'] == 'inbound') {
                $r1 = $db->query("SELECT DISTINCT
                IFNULL(c_uploadimage.id, '') id,
                IFNULL(c_uploadimage.filename, '') filename,
                IFNULL(c_uploadimage.document_name, '') document_name,
                IFNULL(c_uploadimage.file_ext, '') file_ext,
                IFNULL(c_uploadimage.file_mime_type, '') file_mime_type
                FROM c_uploadimage
                WHERE (c_uploadimage.comment_id = '{$row['primaryid']}'AND c_uploadimage.deleted = 0)");
                while ($img = $db->fetchByAssoc($r1)) {
                    if ($img['document_name'] == 's3_image') {
                        $attachment[] = array(
                            's3Link' => 'https://dotb-gallery.s3.ap-southeast-1.amazonaws.com/'.$dotb_config['unique_key'].'/comment/' . $img['id'],
                        );
                    } else {
                        $attachment[] = array(
                            'id' => $img['id'],
                            'name' => $img['filename'],
                            'module' => 'c_uploadimage',
                        );
                    }
                }
            } else  {
                if ($row['filename'] != '')
                    $attachment[] = array(
                        'id' => $row['primaryid'],
                        'name' => $row['filename'],
                        'module' => 'c_comments',
                    );
            }

            $data[] = array(
                'id' => $row['primaryid'],
                'direction' => $row['direction'],
                'description' => $row['description'],
                'date_entered' => date('Y-m-d H:i:s', strtotime("+7 hours " . $row['date_entered'])),
                'created_by' => $create_by,
                'created_by_name' => $locale->getLocaleFormattedName($row['first_name'], $row['last_name']),
                'created_by_avatar' => $row['avatar'],
                'attachment' => $attachment,
                'is_read_inems' => $row['is_read_inems'],
                'is_read_inapp' => $row['is_read_inapp'],
                'is_updated' => $row['is_updated'],
            );
            $count++;
            if($count == 20){
                $check_can_load_id = $row['primaryid'];
            }
        }

        //Update is_read_inapp to 1
        $GLOBALS['db']->query("UPDATE c_comments SET is_read_inapp = 1 WHERE parent_id = '{$args['case_id']}' AND parent_type = 'Cases' AND deleted = 0");

        if($count < 20){
            $can_load = false;
        } else if($count == 20){
            $q = "SELECT date_entered FROM c_comments WHERE id = '{$check_can_load_id}'";
            $date = $GLOBALS['db']->getOne($q);
            $ext = "AND c_comments.date_entered < '$date'";

            $count_comment = "SELECT DISTINCT
                COUNT(c_comments.id) count_comment
                FROM c_comments
                INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases' AND l1.portal_viewable = 1
                LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
                WHERE (((l1.id = '{$args['case_id']}')))
                AND c_comments.is_unsent = 0 AND c_comments.deleted = 0
                $ext";

            if(intVal($GLOBALS['db']->getOne($count_comment)) == 0){
                $can_load = false;
            }
        }
        return array(
            'success' => true,
            'data' => [
                'can_load' => $can_load,
                'list_comment' => $data,
            ],
            'error_info' => throwApiError(1)
        );
    }
    function readCommentInFeedback(ServiceBase $api, $args){

        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['case_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $GLOBALS['db']->query("UPDATE c_comments SET is_read_inapp = 1 WHERE parent_id = '{$args['case_id']}' AND parent_type = 'Cases' AND deleted = 0 AND is_read_inapp = 0");
        return array(
            'success' => true,
            'error_info' => throwApiError(1)
        );
    }
    function createFeedback(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['type', 'name','team_id','team_set_id','class_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $student_id = $this->getStudentFromAccessToken($args['access_token']);
        if(empty($student_id)){
            return [
                'success' => false,
                'error_info' => throwApiError(105)
            ];
        }
        global $timedate;
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $case = BeanFactory::newBean('Cases');
        $case->type = $args['type'];
        $case->priority = 'P1';
        $case->name = $args['name'];
        $case->status = 'New';
        $case->source = 'Mobile App';
        $case->portal_viewable = '1';
        $case->contacts_cases_1contacts_ida = $student_id;
        $case->description = $args['description'];
        $case->assigned_user_id = !empty($args['assigned_user_id']) ? $args['assigned_user_id'] : $api_user;
        $case->team_id = $args['team_id'];
        $case->team_set_id = $args['team_set_id'];
        $case->j_class_cases_1j_class_ida = $args['class_id'];
        $case->save();

        $q = "SELECT DISTINCT
            IFNULL(l3.id, '') student_id, IFNULL(l3.full_student_name, '') student_name,
            IFNULL(l2.id, '') class_id, IFNULL(l2.name, '') class_name,
            IFNULL(l4.id, '') assigned_to_id, IFNULL(l4.full_user_name, '') assigned_to,
            IFNULL(l5.id, '') create_by, IFNULL(l5.full_user_name, '') create_by_name,
            IFNULL(l5.picture, '') created_by_avatar,
            IFNULL(l1.id, '') center_id,
            IFNULL(l1.name, '') center
            FROM cases
            INNER JOIN teams l1 ON cases.team_id = l1.id AND l1.deleted = 0
            LEFT JOIN j_class_cases_1_c l2_1 ON cases.id = l2_1.j_class_cases_1cases_idb AND l2_1.deleted = 0
            LEFT JOIN j_class l2 ON l2.id = l2_1.j_class_cases_1j_class_ida AND l2.deleted = 0
            LEFT JOIN contacts_cases_1_c l3_1 ON cases.id = l3_1.contacts_cases_1cases_idb AND l3_1.deleted = 0
            LEFT JOIN contacts l3 ON l3.id = l3_1.contacts_cases_1contacts_ida AND l3.deleted = 0
            LEFT JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
            LEFT JOIN users l5 ON cases.created_by = l5.id AND l5.deleted = 0
            WHERE (((cases.id = '{$case->id}')))
            AND cases.portal_viewable = 1
            AND cases.deleted = 0
            GROUP BY cases.id";
        $subData = $GLOBALS['db']->fetchOne($q);
        $mainData = [
            'number' => $case->case_number,
            'feedback_id' => $case->id,
            'subject' => $case->name,
            'description' => $case->status,
            'status' => $case->case_number,
            'date_entered' => $timedate->to_display_date_time($case->data_entered),
            'rate' => $case->rate
        ];
        $returnData = array_merge($mainData, $subData);

        return array(
            'success' => true,
            'data' => [
                'feedback_model' => $returnData
            ],
            'error_info' => throwApiError(1)
        );
    }
    function deleteFeedBack(ServiceBase $api, $args){
        require_once ('custom/include/utils/apiHelper.php');

        $validate_param = validateParam($args, ['feedback_id']);
        if (!$validate_param['valid']) {
            return [
                'success' => false,
                'error_info' => throwApiError($validate_param['error_num'])
            ];
        }
        $bean = BeanFactory::getBean('Cases', $args['feedback_id']);
        if(!empty($bean->id)){
            $bean->portal_viewable = 0;
            $bean->save();
            return [
                'success' => true,
                'error_info' => throwApiError(1)
            ];
        } else {
            return [
                'success' => false,
                'error_info' => throwApiError(202)
            ];
        }
    }
    private function getStudentFromAccessToken($access_token)
    {
        $q = "SELECT contact_id FROM oauth_tokens WHERE id = '$access_token'";
        $result = $GLOBALS['db']->fetchOne($q);
        if (!empty($result)) {
            return $result['contact_id'];
        } else {
            return '';
        }
    }
}
