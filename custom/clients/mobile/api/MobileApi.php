<?php


class MobileApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'student-login' => array(
                'reqType' => 'POST',
                'path' => array('students', 'login'),
                'pathVars' => array(''),
                'method' => 'studentLogin',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'teacher-login' => array(
                'reqType' => 'POST',
                'path' => array('teacher', 'login'),
                'pathVars' => array(''),
                'method' => 'teacherLogin',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'load-branding' => array(
                'reqType' => 'POST',
                'path' => array('loadbranding'),
                'pathVars' => array(''),
                'method' => 'loadBranding',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'post-token' => array(
                'reqType' => 'POST',
                'path' => array('posttoken'),
                'pathVars' => array(''),
                'method' => 'post_token',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'update-custom' => array(
                'reqType' => 'POST',
                'path' => array('updatecustom'),
                'pathVars' => array(''),
                'method' => 'updateCustom',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'update-notification' => array(
                'reqType' => 'POST',
                'path' => array('updatenotification'),
                'pathVars' => array(''),
                'method' => 'updateNotification',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'change-password' => array(
                'reqType' => 'POST',
                'path' => array('changepassword'),
                'pathVars' => array(''),
                'method' => 'change_password',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'reset-password' => array(
                'reqType' => 'POST',
                'path' => array('resetpassword'),
                'pathVars' => array(''),
                'method' => 'reset_password',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'save_comment' => array(
                'reqType' => 'POST',
                'path' => array('save_comment'),
                'pathVars' => array(''),
                'method' => 'saveComment',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'get-doc' => array(
                'reqType' => 'POST',
                'path' => array('getdoc'),
                'pathVars' => array(''),
                'method' => 'getDoc',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'get-storage-list' => array(
                'reqType' => 'POST',
                'path' => array('get_storage_list'),
                'pathVars' => array(''),
                'method' => 'getStorageList',
                'shortHelp' => '',
                'longHelp' => '',
                'noLoginRequired' => true,
            ),

            'get-link-share-item' => array(
                'reqType' => 'POST',
                'path' => array('share_storage_item'),
                'pathVars' => array(''),
                'method' => 'shareStorageItem',
                'shortHelp' => '',
                'longHelp' => '',
                'noLoginRequired' => true,
            ),

            'get_storage_item' => array(
                'reqType' => 'POST',
                'path' => array('get_storage_item'),
                'pathVars' => array(''),
                'method' => 'getStorageItem',
                'shortHelp' => '',
                'longHelp' => '',
                'noLoginRequired' => true,
            ),

            'get-list-attendance' => array(
                'reqType' => 'POST',
                'path' => array('getlistattendance'),
                'pathVars' => array(''),
                'method' => 'getListAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'save-attendance' => array(
                'reqType' => 'POST',
                'path' => array('saveattendance'),
                'pathVars' => array(''),
                'method' => 'saveAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'save-list-attendance' => array(
                'reqType' => 'POST',
                'path' => array('savelistattendance'),
                'pathVars' => array(''),
                'method' => 'saveListAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'send-attendance' => array(
                'reqType' => 'POST',
                'path' => array('sendattendance'),
                'pathVars' => array(''),
                'method' => 'sendAttendance',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'save-gallery' => array(
                'reqType' => 'POST',
                'path' => array('savegallery'),
                'pathVars' => array(''),
                'method' => 'saveGallery',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'edit-student' => array(
                'reqType' => 'POST',
                'path' => array('editstudent'),
                'pathVars' => array(''),
                'method' => 'editStudent',
                'shortHelp' => '',
                'longHelp' => ''
            ),

            'edit-teacher' => array(
                'reqType' => 'POST',
                'path' => array('editteacher'),
                'pathVars' => array(''),
                'method' => 'editTeacher',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'teacher-get-teaching-hour' => array(
                'reqType' => 'POST',
                'path' => array('teacher', 'getteachinghour'),
                'pathVars' => array(''),
                'method' => 'getTeachingHour',
                'shortHelp' => '',
                'longHelp' => ''
            ),
            'teacher-get-learning-hour' => array(
                'reqType' => 'POST',
                'path' => array('teacher', 'getlearninghour'),
                'pathVars' => array(''),
                'method' => 'getLearningHour',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }

    function getListGallery($classIds, $str_student_id = '')
    {
        $ext1 = '';
        $ext2 = '';
        if (!empty($str_student_id)) {
            $ext1 = "INNER JOIN j_studentsituations l4 ON
            j_class.id = l4.ju_class_id AND l4.deleted = 0
            INNER JOIN contacts l5 ON l4.student_id = l5.id
            AND l5.deleted = 0 AND l4.student_type = 'Contacts'";
            $ext2 = "AND (IFNULL(l4.type, '') IN ('OutStanding' , 'Enrolled'))
            AND (l4.start_study <= l1.gallery_date)
            AND (l4.end_study >= l1.gallery_date)
            AND l5.id IN ('{$str_student_id}')
            AND (IFNULL(l5.status, '') NOT IN ('Cancelled'))";
        }
        $sqlAlbum = "SELECT DISTINCT
        IFNULL(j_class.id, '') class_id,
        IFNULL(j_class.name, '') class_name,
        IFNULL(l1.id, '') gallery_id,
        IFNULL(l1.name, '') gallery_name,
        IFNULL(l1.gallery_date, '') gallery_date,
        IFNULL(l1.description, '') gallery_description,
        IFNULL(l1.gallery_type, '') gallery_type,
        IFNULL(l2.id, '') pic_id,
        IFNULL(l2.google_id, '') google_id,
        IFNULL(l2.google_url, '') google_url,
        IFNULL(l1.date_entered, '') date_entered,
        IFNULL(GROUP_CONCAT(l3.name), '') tag_name
        FROM j_class
        INNER JOIN j_class_c_gallery_1_c l1_1 ON j_class.id = l1_1.j_class_c_gallery_1j_class_ida AND l1_1.deleted = 0
        INNER JOIN c_gallery l1 ON l1.id = l1_1.j_class_c_gallery_1c_gallery_idb AND l1.deleted = 0
        INNER JOIN notes l2 ON l1.id = l2.parent_id AND l2.deleted = 0 AND l2.parent_type = 'C_Gallery'
        LEFT JOIN tag_bean_rel l3_1 ON
        l1.id = l3_1.bean_id AND l3_1.deleted = 0 AND l3_1.bean_module = 'C_Gallery'
        LEFT JOIN tags l3 ON
        l3.id = l3_1.tag_id AND l3.deleted = 0
        {$ext1}
        WHERE j_class.id IN ('" . implode("','", $classIds) . "') AND j_class.deleted = 0
        {$ext2}
        GROUP BY pic_id
        ORDER BY date_entered DESC";
        $albums = $GLOBALS['db']->query($sqlAlbum);
        $gls = array();
        while ($album = $GLOBALS['db']->fetchbyAssoc($albums)) {
            if (empty($album['class_id'])) break;
            $gls[$album['class_id']][$album['gallery_id']]['gallery_id'] = $album['gallery_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_id'] = $album['class_id'];
            $gls[$album['class_id']][$album['gallery_id']]['class_name'] = $album['class_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_name'] = $album['gallery_name'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_description'] = $album['gallery_description'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_type'] = $album['gallery_type'];
            $gls[$album['class_id']][$album['gallery_id']]['gallery_date'] = $album['gallery_date'];
            $gls[$album['class_id']][$album['gallery_id']]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $album['date_entered']));
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['picture_id'] = $album['pic_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['picture_id'] = $album['google_id'];
            $gls[$album['class_id']][$album['gallery_id']]['image_list'][$album['pic_id']]['google_url'] = $album['google_url'];
            $gls[$album['class_id']][$album['gallery_id']]['images'][] = $album['google_url'];
            if (!empty($album['tag_name']))
                $gls[$album['class_id']][$album['gallery_id']]['tags'] = explode(",", $album['tag_name']);
            else $gls[$album['class_id']][$album['gallery_id']]['tags'] = array();
        }
        foreach ($gls as $class_id => $album) {
            foreach ($album as $gallery_id => $albumVal) {
                foreach ($albumVal['image_list'] as $pic_id => $picVal) {
                    $gls[$class_id][$gallery_id]['image_list'] = array_values($gls[$class_id][$gallery_id]['image_list']);
                }
            }
        }
        return $gls;
    }

    function getStorageList(ServiceBase $api, $args)
    {
        $result = $GLOBALS['db']->query("select name,value from config where name in ('onedrive_client_secret','onedrive_client_id','onedrive_username','onedrive_password','onedrive_org_id')");
        $settings = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $settings[$row['name']] = $row['value'];
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://onedrive.s.dotb.vn/msgraph/gets',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'c' => $settings['onedrive_client_id'],
                'o' => $settings['onedrive_org_id'],
                's' => $settings['onedrive_client_secret'],
                'u' => $settings['onedrive_username'],
                'p' => $settings['onedrive_password'],
                'f' => $args['folder']
            )
        ));
        $response = curl_exec($curl);
        $response = json_decode($response, 1);
        curl_close($curl);
        return $response;
    }

    function shareStorageItem(ServiceBase $api, $args)
    {
        $result = $GLOBALS['db']->query("select name,value from config where name in ('onedrive_client_secret','onedrive_client_id','onedrive_username','onedrive_password','onedrive_org_id')");
        $settings = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $settings[$row['name']] = $row['value'];
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://onedrive.s.dotb.vn/msgraph/share',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'c' => $settings['onedrive_client_id'],
                'o' => $settings['onedrive_org_id'],
                's' => $settings['onedrive_client_secret'],
                'u' => $settings['onedrive_username'],
                'p' => $settings['onedrive_password'],
                'f' => $args['id'],
            ),
        ));
        $response = curl_exec($curl);
        $response = json_decode($response, 1);
        curl_close($curl);
        return $response;
    }

    function getStorageItem(ServiceBase $api, $args)
    {
        $result = $GLOBALS['db']->query("select name,value from config where name in ('onedrive_client_secret','onedrive_client_id','onedrive_username','onedrive_password','onedrive_org_id')");
        $settings = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $settings[$row['name']] = $row['value'];
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://onedrive.s.dotb.vn/msgraph/get',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'c' => $settings['onedrive_client_id'],
                'o' => $settings['onedrive_org_id'],
                's' => $settings['onedrive_client_secret'],
                'u' => $settings['onedrive_username'],
                'p' => $settings['onedrive_password'],
                'f' => $args['id'],
            ),
        ));
        $response = curl_exec($curl);
        $response = json_decode($response, 1);
        curl_close($curl);
        return $response;
    }

    function studentLogin(ServiceBase $api, $args)
    {
        global $timedate, $current_user, $app_list_strings;
        if (isset($args) && !empty($args)) {
            $now = $timedate->nowDb();
            $user_name = $args['portal_name'];
            $password = $args['password'];
            $module = $args['module'];

            $res = array();

            $nowDate = date('Y-m-d');

            //Check Username credential
            $q1 = "SELECT DISTINCT IFNULL(contacts.id, '') primaryid,
            IFNULL(contacts.first_name, '') first_name,
            IFNULL(contacts.last_name, '') last_name,
            IFNULL(contacts.full_student_name, '') name,
            IFNULL(contacts.contact_id, '') student_id,
            contacts.birthdate birthdate,
            IFNULL(contacts.status, '') status,
            IFNULL(contacts.phone_mobile, '') phone_mobile,
            IFNULL(contacts.picture, '') picture,
            IFNULL(contacts.cover_app, '') cover_app,
            IFNULL(l1.email_address, '') email,
            IFNULL(contacts.nick_name, '') nick_name,
            IFNULL(contacts.guardian_name, '') guardian_name,
            IFNULL(contacts.relationship, '') relationship,
            IFNULL(contacts.lead_source, '') lead_source,
            IFNULL(contacts.utm_source, '') utm_source,
            IFNULL(contacts.gender, '') gender,
            IFNULL(contacts.facebook, '') facebook,
            IFNULL(contacts.primary_address_street, '') address,
            IFNULL(contacts.assigned_user_id, '') assigned_user_id,
            IFNULL(contacts.team_id, '') team_id,
            IFNULL(contacts.team_set_id, '') team_set_id,
            IFNULL(contacts.portal_name, '') portal_name,
            IFNULL(contacts.portal_password, '') portal_password
            FROM contacts
            LEFT JOIN email_addr_bean_rel l1_1 ON contacts.id = l1_1.bean_id
            AND l1_1.deleted = 0
            AND l1_1.bean_module = 'Contacts'
            AND l1_1.primary_address = 1
            LEFT JOIN
            email_addresses l1 ON l1.id = l1_1.email_address_id
            AND l1.deleted = 0
            WHERE (contacts.portal_name = '$user_name'
            OR contacts.phone_mobile = '$user_name')
            AND (contacts.portal_active = '1')
            AND contacts.deleted = 0
            ORDER BY first_name ASC";

            $students = $GLOBALS['db']->fetchArray($q1);
            if (count($students) == 0)
                return array(
                    'success' => false,
                    'error' => "invalid_username",
                );

            $student_list = array();
            //Check Password credential
            $pwdCheck = false;
            $studentIds = array_column($students, 'primaryid');
            foreach ($students as $student) {
                if ((User::checkPassword($password, $student['portal_password']) || md5($password) == $student['portal_password']) && !$pwdCheck)
                    $pwdCheck = true;
                //add student array
                $student['birthdate'] = $timedate->to_display_date($student['birthdate'], false);//format birthdate
                $student_list[$student['primaryid']] = $student;

                $student_list[$student['primaryid']]['loyalty_point'] = 0;
                $student_list[$student['primaryid']]['total_payment'] = 0;
                $student_list[$student['primaryid']]['unread_noti'] = 0;
                $student_list[$student['primaryid']]['notifications'] = array();
                $student_list[$student['primaryid']]['sms'] = array();
                $student_list[$student['primaryid']]['loyalty'] = array();

                if (empty($module) || $module == 'payments')
                    $student_list[$student['primaryid']]['payments'] = array();

                if (empty($module) || $module == 'feedback')
                    $student_list[$student['primaryid']]['feedback'] = array();

                if (empty($module) || $module == 'class_list')
                    $student_list[$student['primaryid']]['class_list'] = array();

            }

            if (!$pwdCheck)
                return array(
                    'success' => false,
                    'error' => "invalid_password",
                );

            $str_student_id = implode("','", array_column($students, 'primaryid'));

            if (empty($module) || $module == 'class_list') {
                require_once('include/externalAPI/ClassIn/ExtAPIClassIn.php');
                $ExtApiClassIn = new ExtApiClassIn();
                $schoolId = $ExtApiClassIn->_SID;
                $AppUrl = $ExtApiClassIn->_AppUrl;

                //Get Current Class
                $nowDb = $timedate->nowDb();
                $q2 = "SELECT DISTINCT
                IFNULL(contacts.id, '') student_id,
                IFNULL(l3.id, '') class_id,
                IFNULL(l3.picture, '') picture,
                IFNULL(l3.name, '') class_name,
                IFNULL(l3.class_code, '') class_code,
                l3.start_date start_date,
                l3.end_date end_date,
                l3.date_entered date_entered,
                IFNULL(l3.kind_of_course, '') kind_of_course,
                IFNULL(l3.level, '') level,
                CONCAT(IFNULL(l3.kind_of_course, ''), ' ', IFNULL(l3.level, '')) koc_level,
                IFNULL(l3.modules, '') modules,
                IFNULL(l3.lms_course_id, '') lms_course_id,
                IFNULL(l3.lms_template_id, '') lms_template_id,
                l3.hours hours,
                IFNULL(l3.status, '') class_status,
                IFNULL(l2.lesson_number, '') ss_lesson,
                IFNULL(l4.id, '') assigned_to_id,
                IFNULL(l4.full_user_name, '') assigned_to,
                IFNULL(l5.id, '') koc_id,
                IFNULL(l5.kind_of_course, '') koc_name,
                IFNULL(l6.id, '') room_id,
                IFNULL(l6.name, '') room_name,
                IFNULL(l7.id, '') teacher_id,
                IFNULL(l7.full_teacher_name, '') teacher_name,
                IFNULL(l3.max_size, '') max_size,
                IFNULL(l8.id, '') team_id,
                IFNULL(l8.name, '') team_name,
                IFNULL(l8.code_prefix, '') team_code,
                IFNULL(l2.id, '') ss_id,
                IFNULL(l2.name, '') ss_name,
                IFNULL(l2.week_no, '') ss_week_no,
                IFNULL(l2.observe_score, 0) ss_observe_score,
                IFNULL(l2.syllabus_custom, '') ss_lesson_note,
                IFNULL(l2.syllabus_custom, '') ss_syllabus_custom,
                IFNULL(l2.topic_custom, '') ss_topic_custom,
                l2.till_hour ss_till_hour,
                l2.date_start ss_date_start,
                l2.date_end ss_date_end,
                l2.duration_cal ss_duration_cal,
                IFNULL(l9.id, '') l9_id,
                IFNULL(l9.full_teacher_name, '') ss_ta_name,
                IFNULL(l10.id, '') l10_id,
                IFNULL(l10.full_teacher_name, '') ss_teacher_name,
                IFNULL(l11.id, '') l11_id,
                IFNULL(l11.name, '') ss_room_name,
                IFNULL(l2.type, '') ss_learning_type,
                (CASE
                WHEN DATE_ADD(l2.date_end, INTERVAL 2 HOUR) < '$nowDb' THEN ''
                WHEN l2.type = 'ClassIn' THEN (CONCAT('$AppUrl','enterclass?telephone=',IFNULL(contacts.phone_prefix,'84'),'-',SUBSTR(contacts.phone_mobile,2),'&classId=',l2.external_id,'&courseId=',l3.onl_course_id,'&schoolId=$schoolId'))
                ELSE IFNULL(l2.join_url, '')
                END) ss_displayed_url,
                IFNULL(l12.id, '') attendance_id,
                IFNULL(l12.homework_comment, '') homework_comment,
                l12.homework_score homework_score,
                IFNULL(l12.homework, '') do_homework,
                IFNULL(l12.description, '') teacher_comment,
                IFNULL(l12.care_comment, '') care_comment,
                IFNULL(l12.attended, 0) attended,
                IFNULL(l12.attendance_type, '') attendance_type,
                IFNULL(l12.star_rating, '') star_rating,
                IFNULL(l12.customer_comment, '') customer_comment,
                IFNULL(l12.check_in_time, '') check_in_time,
                IFNULL(l12.check_out_time, '') check_out_time,
                l12.absent_for_hour absent_for_hour
                FROM contacts
                INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0
                INNER JOIN meetings_contacts l2_1 ON l1.id = l2_1.situation_id AND l2_1.deleted = 0 AND contacts.id = l2_1.contact_id
                INNER JOIN meetings l2 ON l2.id = l2_1.meeting_id AND l2.deleted = 0
                INNER JOIN j_class l3 ON l2.ju_class_id = l3.id AND l3.deleted = 0
                LEFT JOIN users l4 ON l3.assigned_user_id = l4.id AND l4.deleted = 0
                LEFT JOIN j_kindofcourse l5 ON l3.koc_id = l5.id AND l5.deleted = 0
                LEFT JOIN c_rooms l6 ON l3.room_id = l6.id AND l6.deleted = 0
                LEFT JOIN c_teachers l7 ON l3.teacher_id = l7.id AND l7.deleted = 0
                LEFT JOIN teams l8 ON l3.team_id = l8.id AND l8.deleted = 0
                LEFT JOIN c_teachers l9 ON l2.teacher_cover_id=l9.id AND l9.deleted=0
                LEFT JOIN c_teachers l10 ON l2.teacher_id=l10.id AND l10.deleted=0
                LEFT JOIN c_rooms l11 ON l2.room_id=l11.id AND l11.deleted=0
                LEFT JOIN c_attendance l12 ON l2.id = l12.meeting_id AND l12.deleted = 0 AND l12.student_id = contacts.id
                WHERE (((contacts.id IN ('$str_student_id'))
                AND (IFNULL(l2.meeting_type, '') = 'Session')
                AND (IFNULL(l2.session_status, '') <> 'Cancelled' OR (IFNULL(l2.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
                AND (IFNULL(l1.type, '') IN ('Enrolled' , 'OutStanding', 'Moving In'))))
                AND contacts.deleted = 0
                ORDER BY student_id, CASE";
                $types = $GLOBALS['app_list_strings']['status_class_list'];
                $count_k = 1;
                foreach ($types as $_type => $val_)
                    $q2 .= " WHEN class_status = '$_type' THEN " . $count_k++;
                $q2 .= " ELSE $count_k END ASC,l3.end_date DESC, class_id, l2.date_start";
                $rowS = $GLOBALS['db']->fetchArray($q2);
                //Get Kind Of Course Syllabus - For best perfomance
                $classIds = implode("','", array_unique(array_column($rowS, 'class_id')));
                $koc = array();
                $q2_1 = "SELECT DISTINCT
                IFNULL(l1.id, '') koc_id,
                IFNULL(l1.name, '') koc_name,
                IFNULL(l1.kind_of_course, '') kind_of_course,
                IFNULL(l1.syllabus, '') syllabus,
                IFNULL(l1.content, '') content,
                IFNULL(l1.lms_content, '') lms_content
                FROM j_class INNER JOIN j_kindofcourse l1 ON j_class.koc_id = l1.id  AND l1.deleted = 0
                WHERE (j_class.id IN ('$classIds')) AND j_class.deleted = 0";
                $rowK = $GLOBALS['db']->fetchArray($q2_1);
                foreach ($rowK as $key => $rK) {
                    $koc[$rK['koc_id']]['koc_id'] = $rK['koc_id'];
                    $koc[$rK['koc_id']]['koc_name'] = $rK['koc_name'];
                    $koc[$rK['koc_id']]['kind_of_course'] = $rK['kind_of_course'];
                    $koc[$rK['koc_id']]['syllabus'] = ((!empty($rK['syllabus'])) ? json_decode(html_entity_decode($rK['syllabus']), true) : '');
                    $koc[$rK['koc_id']]['content'] = ((!empty($rK['content'])) ? json_decode(html_entity_decode($rK['content']), true) : '');

                }
                $classes = array();

                $runStudent = '####';
                $runClass = '####';
                //List Class ID
                $classIds = array();
                //#ext new
                $ext_new = '';

                foreach ($rowS as $_k2 => $r2) {
                    if ($runStudent != $r2['student_id'] || $runClass != $r2['class_id']) {
                        //Đã tham gia
                        $attended_session = 0;

                        $ss_studied = 0;
                        $ss_count = 0;
                        $runStudent = $r2['student_id'];
                        $runClass = $r2['class_id'];

                        $classes[$runStudent][$runClass]['id'] = $r2['class_id'];
                        $classes[$runStudent][$runClass]['class_name'] = $r2['class_name'];
                        $classes[$runStudent][$runClass]['picture'] = $r2['picture'];
                        $classes[$runStudent][$runClass]['class_code'] = $r2['class_code'];
                        $classes[$runStudent][$runClass]['start_date'] = $timedate->to_display_date($r2['start_date'], false);
                        $classes[$runStudent][$runClass]['end_date'] = $timedate->to_display_date($r2['end_date'], false);
                        $classes[$runStudent][$runClass]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['date_entered']));
                        $classes[$runStudent][$runClass]['kind_of_course'] = $r2['kind_of_course'];
                        $classes[$runStudent][$runClass]['level'] = $r2['level'];
                        $classes[$runStudent][$runClass]['hours'] = $r2['hours'];
                        $classes[$runStudent][$runClass]['assigned_to'] = $r2['assigned_to'];
                        $classes[$runStudent][$runClass]['koc_id'] = $r2['koc_id'];
                        $classes[$runStudent][$runClass]['koc_name'] = $r2['koc_name'];
                        $classes[$runStudent][$runClass]['room_name'] = $r2['room_name'];
                        $classes[$runStudent][$runClass]['teacher_name'] = $r2['teacher_name'];
                        $classes[$runStudent][$runClass]['max_size'] = $r2['max_size'];
                        $classes[$runStudent][$runClass]['team_name'] = $r2['team_name'];
                        $classes[$runStudent][$runClass]['team_code'] = $r2['team_code'];

                        $classes[$runStudent][$runClass]['lms_course_id'] = $r2['lms_course_id'];
                        $classes[$runStudent][$runClass]['lms_template_id'] = $r2['lms_template_id'];

                        $classes[$runStudent][$runClass]['status'] = $r2['class_status'];

                        //Add new average rating by nnamnguyen
                        $count_session_is_rated = 0;
                        $rating_score = 0;
                        $classes[$runStudent][$runClass]['average_rating'] = '';

                        $content = $koc[$r2['koc_id']]['content'];
                        foreach ($content as $ct) {
                            if ($ct['levels'] == $r2['level'])
                                $docs = $ct['doc_url'];
                        }
                        $classes[$runStudent][$runClass]['doc_url'] = $docs;

                        $classes[$runStudent][$runClass]['sessions'] = array();
                        $classes[$runStudent][$runClass]['albums'] = array();
                        $classes[$runStudent][$runClass]['gradebooks'] = array();

                    }

                    //nếu thời gian hiện tại nằm trong thòi gian student situation thì lấy trạng thái lớp là trạng thái lớp hiện tại
                    if ($r2['class_status'] != 'Finished' && $classes[$runStudent][$runClass]['status'] != $r2['class_status']) {
                        $classes[$runStudent][$runClass]['status'] = $r2['class_status'];
                    }

                    $session = array();
                    $session['ss_id'] = $r2['ss_id'];
                    $session['ss_name'] = $r2['ss_name'];
                    $session['ss_lesson'] = $r2['ss_lesson'];
                    $session['ss_week_no'] = $r2['ss_week_no'];
                    $session['ss_week_date'] = date('l', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_till_hour'] = $r2['ss_till_hour'];
                    $session['ss_date_start'] = $timedate->to_display_date_time($r2['ss_date_start']);
                    $session['ss_date_end'] = $timedate->to_display_date_time($r2['ss_date_end']);
                    $session['ss_db_date_start'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_db_date_end'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_end']));
                    $session['ss_db_date'] = date('Y-m-d', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_learning_type'] = $r2['ss_learning_type'];
                    $session['ss_displayed_url'] = $r2['ss_displayed_url'];
                    if ($now < $r2['ss_date_start']) $session['ss_status'] = "not_stated";
                    elseif ($now >= $r2['ss_date_start'] && $now <= $r2['ss_date_end']) $session['ss_status'] = "in_progress";
                    elseif ($now > $r2['ss_date_end']) {
                        $session['ss_status'] = "finished";
                        $ss_studied++;
                    }

                    $session['ss_duration_cal'] = $r2['ss_duration_cal'];
                    $session['ss_ta_name'] = $r2['ss_ta_name'];
                    $session['ss_teacher_name'] = $r2['ss_teacher_name'];
                    $session['ss_room_name'] = $r2['ss_room_name'];
                    $session['ss_observe_score'] = $r2['ss_observe_score'];
                    $session['ss_lesson_note'] = $r2['ss_lesson_note'];
                    $session['ss_syllabus_custom'] = $r2['ss_syllabus_custom'];
                    $session['ss_topic_custom'] = $r2['ss_topic_custom'];

                    //get Syllabus
                    //$J_Obj = json_decode(html_entity_decode($r2['syllabus']), true);
                    //foreach ($J_Obj as $label => $Jvalue)
                    //$session[$label] = $Jvalue;

                    $syll = $koc[$r2['koc_id']]['syllabus'][$r2['level']][$r2['ss_lesson']];
                    if (!empty($syll)) {
                        $session['syllabus_topic'] = $syll['theme'];
                        $session['syllabus_activities'] = $syll['content'];
                        $session['syllabus_homework'] = $syll['homework'];
                    } else {
                        $session['syllabus_topic'] = '';
                        $session['syllabus_activities'] = '';
                        $session['syllabus_homework'] = '';
                    }
                    //LAY THONG TIN SO LIEN LAC
                    $session['att_id'] = $r2['attendance_id'];
                    $session['att_homework_comment'] = $r2['homework_comment'];
                    $session['att_homework_score'] = $r2['homework_score'];
                    $session['att_do_homework'] = $r2['do_homework'];
                    $session['att_teacher_comment'] = $r2['teacher_comment'];
                    $session['att_care_comment'] = $r2['care_comment'];
                    $session['att_attended'] = $r2['attended'];
                    $session['att_attendance_type'] = $r2['attendance_type'];
                    $session['att_absent_for_hour'] = $r2['absent_for_hour'];
                    $session['att_star_rating'] = $r2['star_rating'];
                    $session['att_customer_comment'] = $r2['customer_comment'];
                    $session['check_in_time'] = $timedate->to_display_time($r2['check_in_time']);
                    $session['check_out_time'] = $timedate->to_display_time($r2['check_out_time']);
                    //$attended_session
                    if ($session['ss_status'] === 'finished') {
                        if ($session['att_attendance_type'] === 'P' || $session['att_attendance_type'] === 'L') {
                            $attended_session++;
                        }
                    }
                    $ss_count++;
                    $classes[$runStudent][$runClass]['sessions'][] = $session;
                    $classes[$runStudent][$runClass]['count_studied'] = $ss_studied;
                    $classes[$runStudent][$runClass]['attended_session'] = $attended_session;
                    $classes[$runStudent][$runClass]['count_session'] = $ss_count;

                    //Custom average rating
                    if ($session['att_homework_score'] !== "" && $session['att_homework_score'] !== null) {
                        $count_session_is_rated += 1;
                        if ($session['att_homework_score'] > 5) {
                            $rating_score += 5;
                        } else {
                            $rating_score += $session['att_homework_score'];
                        }
                        $rating = number_format($rating_score / $count_session_is_rated, 1);
                        $classes[$runStudent][$runClass]['average_rating'] = $rating;
                    }

                    //Custom attended session percent
                    if ($classes[$runStudent][$runClass]['count_studied'] !== 0) {
                        $attended_session_percent = (($classes[$runStudent][$runClass]['attended_session']) / ($classes[$runStudent][$runClass]['count_studied'])) * 100;
                        $attended_session_percent = number_format($attended_session_percent, 0);
                    } else {
                        $attended_session_percent = 0;
                    }
                    $classes[$runStudent][$runClass]['attended_session_percent'] = $attended_session_percent;
                    if (!in_array($runClass, $classIds))
                        $classIds[] = $runClass;
                }

                // Get total session of class
                $qSession = "SELECT DISTINCT
                IFNULL(c.id, '') class_id,
                COUNT(l1.id) total_session,
                IFNULL(l3.id,'') student_id
                FROM j_class c
                INNER JOIN meetings l1 ON l1.ju_class_id = c.id AND l1.deleted = 0
                INNER JOIN  j_classstudents l2 ON c.id = l2.class_id AND l2.deleted=0
                INNER JOIN  contacts l3 ON l3.id = l2.student_id AND l3.deleted=0
                WHERE (c.id IN ('" . implode("','", $classIds) . "'))
                AND (l3.id IN ('$str_student_id'))
                AND (IFNULL(l1.meeting_type, '') = 'Session')
                AND (IFNULL(l1.session_status, '') <> 'Cancelled' OR (IFNULL(l1.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
                GROUP BY l3.id, c.id";
                $rSession = $GLOBALS['db']->query($qSession);
                while ($row = $GLOBALS['db']->fetchbyAssoc($rSession)) {
                    $classes[$row['student_id']][$row['class_id']]['total_session'] = $row['total_session'];
                }

                //Add Gallery Image
                $gls = $this->getListGallery($classIds, $str_student_id);
                //Assigned Gallery By Class
                foreach ($classes as $stC => $st_class) {
                    foreach ($st_class as $class_id => $class) {
                        if (array_key_exists($class_id, $gls))
                            $classes[$stC][$class_id]['albums'] = array_values($gls[$class_id]);
                    }
                }

                $q4 = "SELECT DISTINCT
                IFNULL(l2.id,'') student_id,
                IFNULL(l1.id, '') class_id,
                IFNULL(j_gradebook.id, '') gradebook_id,
                IFNULL(j_gradebook.status, '') status,
                IFNULL(j_gradebook.rate, '') rate,
                IFNULL(j_gradebook.type, '') gradebook_type,
                IFNULL(j_gradebook.minitest, '') minitest
                FROM j_gradebook
                INNER JOIN j_class_j_gradebook_1_c l1_1 ON j_gradebook.id = l1_1.j_class_j_gradebook_1j_gradebook_idb AND l1_1.deleted = 0
                INNER JOIN j_class l1 ON l1.id = l1_1.j_class_j_gradebook_1j_class_ida AND l1.deleted = 0
                INNER JOIN  j_classstudents l2_1 ON l1.id=l2_1.class_id AND l2_1.deleted=0
                INNER JOIN  contacts l2 ON l2.id=l2_1.student_id AND l2.deleted=0
                WHERE (((l1.id IN ('" . implode("','", $classIds) . "'))
                AND (l2.id IN ('$str_student_id'))
                AND (IFNULL(j_gradebook.status, '') = 'Approved')))
                AND j_gradebook.deleted = 0
                ORDER BY class_id ASC,
                CASE WHEN
                (j_gradebook.type = '' OR j_gradebook.type IS NULL) THEN 0";
                $types = $GLOBALS['app_list_strings']['gradebook_type_options'];
                $count_k = 1;
                foreach ($types as $_type => $val_)
                    $q4 .= " WHEN j_gradebook.type = '$_type' THEN " . $count_k++;
                $q4 .= " ELSE $count_k END ASC";
                $rs4 = $GLOBALS['db']->query($q4);
                while ($r4 = $GLOBALS['db']->fetchbyAssoc($rs4)) {
                    $gradebook = BeanFactory::getBean('J_Gradebook', $r4['gradebook_id']);
                    $gbArray = $gradebook->getDetailForStudent($r4['student_id']);
                    $gbArray['gradebook_id'] = $r4['gradebook_id'];
                    $gbArray['rate'] = $r4['rate'];
                    $classes[$r4['student_id']][$r4['class_id']]['gradebooks'][] = $gbArray;
                }

                //Assign to result
                foreach ($student_list as $stid => $student)
                    $student_list[$stid]['class_list'] = array_values($classes[$stid]);

                $lastDate = date('Y-m-d', strtotime('last day of this month'));
            }

            if (empty($module) || $module == 'payments') {
                //Get Payment
                $q3 = "SELECT DISTINCT
                IFNULL(j_paymentdetail.id, '') receipt_id,
                IFNULL(l1.id, '') payment_id,
                IFNULL(l2.id, '') student_id,
                IFNULL(j_paymentdetail.name,'') receipt_code,
                j_paymentdetail.expired_date expired_date,
                j_paymentdetail.payment_date receipt_date,
                j_paymentdetail.payment_amount receipt_amount,
                IFNULL(j_paymentdetail.status, '') status,
                IFNULL(j_paymentdetail.payment_method, '') method,
                IFNULL(j_paymentdetail.description, '') description,
                IFNULL(l4.name, '') course_fee,
                IFNULL(l1.payment_type, '') payment_type,
                IFNULL(l1.kind_of_course, '') kind_of_course,
                IFNULL(l3.name, '') team_name
                FROM j_paymentdetail
                INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
                INNER JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
                INNER JOIN teams l3 ON l1.team_id = l3.id AND l3.deleted = 0
                LEFT JOIN j_coursefee_j_payment_1_c l4_1 ON l1.id = l4_1.j_coursefee_j_payment_1j_payment_idb AND l4_1.deleted = 0
                LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_1j_coursefee_ida AND l4.deleted = 0
                WHERE (((l2.id IN ('$str_student_id'))
                AND (j_paymentdetail.payment_amount > 0)
                AND ( ((j_paymentdetail.status = 'Unpaid') AND ( (j_paymentdetail.expired_date <= '$lastDate') OR (j_paymentdetail.expired_date IS NULL) OR (j_paymentdetail.expired_date = '') )) OR ((j_paymentdetail.status = 'Paid'))  )))
                AND j_paymentdetail.deleted = 0
                ORDER BY l2.id ASC, status DESC, j_paymentdetail.payment_date DESC, j_paymentdetail.expired_date DESC";
                $rs3 = $GLOBALS['db']->query($q3);
                $key = 0;
                $runStudent = '####';

                while ($r3 = $GLOBALS['db']->fetchbyAssoc($rs3)) {
                    if ($runStudent != $r3['student_id']) $key = 0;
                    $payment = array();
                    $payment['receipt_id'] = $r3['receipt_id'];
                    $payment['payment_id'] = $r3['payment_id'];
                    $payment['receipt_code'] = $r3['receipt_code'];
                    $payment['receipt_date_db'] = $r3['receipt_date'];
                    $payment['receipt_date'] = $timedate->to_display_date($r3['receipt_date'], false);
                    $payment['expired_date'] = $timedate->to_display_date($r3['expired_date'], false);
                    $payment['receipt_amount'] = format_number($r3['receipt_amount']);
                    $payment['status'] = $r3['status'];
                    if ($payment['status'] == 'Unpaid') {
                        $payment['receipt_date_db'] = '';
                        $payment['receipt_date'] = '';
                    }
                    $payment['method'] = $r3['method'];
                    $payment['course_fee'] = $r3['course_fee'];
                    if ($r3['payment_type'] == 'Book/Gift')
                        $payment['course_fee'] = 'Thanh toán Sách/Quà tặng';
                    if ($r3['payment_type'] == 'Deposit')
                        $payment['course_fee'] = $r3['kind_of_course'];
                    $payment['payment_type'] = $r3['payment_type'];
                    $payment['description'] = $r3['description'];

                    $payment['team_name'] = $r3['team_name'];
                    $student_list[$r3['student_id']]['payments'][$key] = $payment;
                    $student_list[$r3['student_id']]['total_payment'] += $r3['receipt_amount'];
                    $runStudent = $r3['student_id'];
                    $key++;
                }
            }

            if (empty($module) || $module == 'feedback') {
                //Get Feedback
                $q4 = "SELECT DISTINCT
                IFNULL(cases.id, '') primaryid,
                IFNULL(cases.name, '') subject,
                IFNULL(l3.id, '') student_id, IFNULL(l3.full_student_name, '') student_name,
                IFNULL(l2.id, '') class_id, IFNULL(l2.name, '') class_name,
                IFNULL(cases.case_number, '') case_number,
                IFNULL(cases.type, '') type,
                IFNULL(cases.status, '') case_status,
                IFNULL(cases.rate, '') rate,
                cases.date_entered date_entered,
                IFNULL(cases.description, '') feedback_content,
                IFNULL(l4.id, '') assigned_to_id, IFNULL(l4.full_user_name, '') assigned_to,
                IFNULL(l5.id, '') create_by, IFNULL(l5.full_user_name, '') create_by_name,
                IFNULL(l1.id, '') center_id,
                IFNULL(l1.name, '') center
                FROM cases
                INNER JOIN teams l1 ON cases.team_id = l1.id AND l1.deleted = 0
                LEFT JOIN j_class_cases_1_c l2_1 ON cases.id = l2_1.j_class_cases_1cases_idb AND l2_1.deleted = 0
                LEFT JOIN j_class l2 ON l2.id = l2_1.j_class_cases_1j_class_ida AND l2.deleted = 0
                LEFT JOIN contacts_cases_1_c l3_1 ON cases.id = l3_1.contacts_cases_1cases_idb AND l3_1.deleted = 0
                LEFT JOIN contacts l3 ON l3.id = l3_1.contacts_cases_1contacts_ida AND l3.deleted = 0
                LEFT JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
                LEFT JOIN users l5 ON cases.created_by = l5.id AND l5.deleted = 0
                WHERE (((l3.id IN ('$str_student_id'))))
                AND cases.portal_viewable = 1
                AND cases.deleted = 0
                GROUP BY cases.id
                ORDER BY l3.id ASC, CASE";
                $fb_status = $GLOBALS['app_list_strings']['case_status_dom'];
                $i = 1;
                foreach ($fb_status as $_status => $val)
                    $q4 .= " WHEN case_status = '$_status' THEN " . $i++;
                $q4 .= " ELSE $i END ASC, last_comment_date DESC";
                $rs4 = $GLOBALS['db']->fetchArray($q4);

                //Get Last Feedback Cmt
                $fbIds = implode("','", array_column($rs4, 'primaryid'));
                $q9 = "SELECT cs.*
                FROM c_comments cs, (SELECT  cc.parent_id parent_id, max(cc.date_entered) max_date_entered
                FROM c_comments cc
                WHERE cc.deleted = 0 AND cc.parent_id IN ('$fbIds') AND cc.parent_type = 'Cases'
                GROUP BY cc.parent_id) cc_ma
                WHERE (cs.parent_id = cc_ma.parent_id) AND (cs.parent_type = 'Cases')
                AND (cc_ma.max_date_entered = cs.date_entered) AND (cs.parent_id IN ('$fbIds'))
                GROUP BY cs.id";
                $rs9 = $GLOBALS['db']->fetchArray($q9);
                $fb_last = array();
                foreach ($rs9 as $ind => $r9) {
                    $created_by_name = $GLOBALS['db']->getOne("SELECT IFNULL(full_user_name, '') full_user_name FROM users WHERE id = '{$r9['created_by']}'");
                    if (empty($created_by_name)) $created_by_name = '';
                    $fb_last[$r9['parent_id']]['lcmt_content'] = $r9['description'];
                    $fb_last[$r9['parent_id']]['lcmt_datetime'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r9['date_entered']));
                    $fb_last[$r9['parent_id']]['lcmt_created_by_name'] = $created_by_name;
                    $fb_last[$r9['parent_id']]['lcmt_created_by'] = $r9['created_by'];
                    $fb_last[$r9['parent_id']]['lcmt_unread_app'] = (($r9['is_read_inapp'] == 0) ? '1' : '0');
                    $fb_last[$r9['parent_id']]['lcmt_unread_ems'] = (($r9['is_read_inems'] == 0) ? '1' : '0');
                }


                $key = 0;
                $runStudent = '####';
                foreach ($rs4 as $_ind => $r4) {
                    if ($runStudent != $r4['student_id']) $key = 0;
                    $feedback = array();
                    $feedback['number'] = $r4['case_number'];
                    $feedback['feedback_id'] = $r4['primaryid'];
                    $feedback['subject'] = $r4['subject'];
                    $feedback['class_id'] = $r4['class_id'];
                    $feedback['class_name'] = $r4['class_name'];
                    $feedback['type'] = $r4['type'];
                    $feedback['case_status'] = $r4['case_status'];
                    $feedback['date_entered'] = $timedate->to_display_date_time($r4['date_entered']);
                    $feedback['date_entered_db'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r4['date_entered']));
                    $feedback['feedback_content'] = $r4['feedback_content'];
                    $feedback['assigned_to_id'] = $r4['assigned_to_id'];
                    $feedback['assigned_to'] = $r4['assigned_to'];
                    $feedback['create_by'] = $r4['create_by'];
                    $feedback['create_by_name'] = $r4['create_by_name'];
                    $feedback['center_id'] = $r4['center_id'];
                    $feedback['center'] = $r4['center'];
                    $feedback['rate'] = $r4['rate'];
                    $feedback['last_comment'] = $fb_last[$r4['primaryid']]['lcmt_content'];
                    $feedback['last_comment_date'] = $fb_last[$r4['primaryid']]['lcmt_datetime'];
                    $feedback['last_comment_depositor'] = $fb_last[$r4['primaryid']]['lcmt_created_by_name'];
                    $feedback['count_unread_comment'] = $fb_last[$r4['primaryid']]['lcmt_unread_app'];
                    $feedback['lcmt_created_by_name'] = $fb_last[$r4['primaryid']]['lcmt_created_by_name'];
                    $feedback['lcmt_created_by'] = $fb_last[$r4['primaryid']]['lcmt_created_by'];
                    $student_list[$r4['student_id']]['feedback'][$key] = $feedback;
                    $runStudent = $r4['student_id'];
                    $key++;
                }
            }

            //Get Loyalty
            $q6 = "SELECT DISTINCT
            IFNULL(l1.id, '') student_id,
            IFNULL(l2.id,'') payment_id,
            IFNULL(l3.id,'') receipt_id,
            IFNULL(l2.name,'') payment_code,
            IFNULL(j_loyalty.id, '') primaryid,
            j_loyalty.input_date date,
            j_loyalty.exp_date exp_date,
            IFNULL(j_loyalty.type, '') type,
            j_loyalty.point point,
            j_loyalty.rate_in_out rate_in_out,
            IFNULL(j_loyalty.name, '') name,
            j_loyalty.description description
            FROM j_loyalty
            INNER JOIN contacts l1 ON j_loyalty.student_id = l1.id AND l1.deleted = 0
            LEFT JOIN  j_payment l2 ON j_loyalty.payment_id=l2.id AND l2.deleted=0
            LEFT JOIN  j_paymentdetail l3 ON l3.id =j_loyalty.paymentdetail_id AND l3.deleted=0
            WHERE
            (((l1.id IN ('$str_student_id'))))
            AND j_loyalty.deleted = 0
            ORDER BY student_id ASC, exp_date DESC";
            $rs = $GLOBALS['db']->query($q6);
            $key = 0;
            $runStudent = '####';
            while ($r = $GLOBALS['db']->fetchbyAssoc($rs)) {
                if ($runStudent != $r['student_id']) $key = 0;

                $loyalty = array();
                $loyalty['loyalty_id'] = $r['primaryid'];
                $loyalty['receipt_id'] = $r['receipt_id'];
                $loyalty['payment_id'] = $r['payment_id'];
                $loyalty['payment_code'] = $r['payment_code'];
                $loyalty['date'] = $timedate->to_display_date($r['date'], false);
                $loyalty['exp_date'] = $timedate->to_display_date($r['exp_date'], false);
                $loyalty['type'] = $r['type'];
                $loyalty['point'] = $r['point'];
                $loyalty['amount'] = format_number(abs($r['point'] * $r['rate_in_out']));
                $loyalty['name'] = $r['name'];
                $loyalty['description'] = $r['description'];

                $student_list[$r['student_id']]['loyalty_point'] += $r['point'];
                $student_list[$r['student_id']]['loyalty'][$key] = $loyalty;
                $runStudent = $r['student_id'];
                $key++;
            }

            if ($args['dotbfrom'] == 'web') {
                //Get SMS
                $q8 = "SELECT DISTINCT
                IFNULL(c_sms.id, '') primaryid,
                IFNULL(c_sms.phone_number, '') phone_number,
                IFNULL(c_sms.name, '') name,
                IFNULL(c_sms.delivery_status, '')    status,
                c_sms.date_entered date_entered,
                c_sms.date_send date_send,
                c_sms.description description,
                IFNULL(l1.id, '') student_id
                FROM c_sms
                INNER JOIN contacts l1 ON c_sms.parent_id = l1.id AND l1.deleted = 0
                WHERE (((l1.id IN ('$str_student_id'))
                AND (IFNULL(c_sms.delivery_status, '') = 'RECEIVED')))
                AND c_sms.deleted = 0
                ORDER BY student_id ASC, date_entered DESC";
                $rs = $GLOBALS['db']->query($q8);
                $key = 0;
                $runStudent = '####';
                while ($r = $GLOBALS['db']->fetchbyAssoc($rs)) {
                    if ($runStudent != $r['student_id']) $key = 0;

                    $nt = array();
                    $nt['student_id'] = $r['student_id'];
                    $nt['primaryid'] = $r['primaryid'];
                    $nt['phone_number'] = $r['phone_number'];
                    $nt['name'] = $r['name'];
                    $nt['status'] = $r['status'];
                    $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));
                    $nt['date_send'] = $timedate->to_display_date($r['date_send'], false);
                    $nt['description'] = $r['description'];


                    $student_list[$r['student_id']]['sms'][$key] = $nt;
                    $runStudent = $r['student_id'];
                    $key++;
                }
            } else {
                //Get Notification
                $q7 = "SELECT nttt.* FROM (SELECT DISTINCT
                IFNULL(nt.id, '') primaryid,
                CASE nt.student_id WHEN 'all' THEN IFNULL(l1.id, '') ELSE (IFNULL(nt.student_id, '')) END student_id,
                IFNULL(nt.name, '') title,
                IFNULL(nt.description, '') body,
                IFNULL(nt.is_read, 0) is_read,
                IFNULL(nt.parent_id, '') record_id,
                IFNULL(nt.parent_type, '') module_name,
                IFNULL(nt.created_by, '') created_by,
                IFNULL(nt.student_read, '') student_read,
                nt.date_entered date_entered
                FROM notifications nt
                INNER JOIN contacts l1 ON (nt.student_id = l1.id OR (nt.student_id = 'all' AND nt.date_entered >= l1.date_entered) ) AND l1.deleted = 0 AND (nt.student_id IN ('$str_student_id') OR nt.student_id = 'all')
                WHERE nt.deleted = 0 AND (nt.apps_type = 'Student' OR nt.apps_type = '' OR nt.apps_type IS NULL)) nttt WHERE nttt.student_id IN ('$str_student_id')
                ORDER BY nttt.student_id ASC, nttt.date_entered DESC";
                $rs = $GLOBALS['db']->query($q7);
                $runStudent = '####';
                $key = 0;
                while ($r = $GLOBALS['db']->fetchbyAssoc($rs)) {
                    if ($runStudent != $r['student_id']) $key = 0;

                    $nt = array();
                    $nt['primaryid'] = $r['primaryid'];
                    $nt['title'] = $r['title'];
                    $nt['body'] = $r['body'];
                    //get notification All
                    if (!empty($r['student_read']) && $r['student_read'] != '[]') {
                        $student_read = json_decode(html_entity_decode($r['student_read']), true);
                        if (in_array($r['student_id'], $student_read))
                            $r['is_read'] = '1';
                    }
                    $nt['is_read'] = ($r['is_read'] == '1') ? true : false;
                    if ($r['is_read'] != '1') $student_list[$r['student_id']]['unread_noti'] += 1; //Add Number of Notification

                    $nt['module_name'] = $r['module_name'];
                    $nt['record_id'] = $r['record_id'];
                    $nt['created_by'] = $r['created_by'];
                    $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));

                    $student_list[$r['student_id']]['notifications'][$key] = $nt;
                    $runStudent = $r['student_id'];
                    $key++;
                }
            }
            $ext_new = '';
            foreach ($koc as $ind => $kl)
                $ext_new .= " OR (c_news.koc = '{$kl['kind_of_course']}') ";
            //Get New
            $q5 = "SELECT DISTINCT
            IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin, 0) pin,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            WHERE (c_news.end_date >= '$nowDate')
            AND (c_news.type LIKE '%^Student^%')
            AND ((c_news.koc = '') OR (c_news.koc IS NULL)
            $ext_new)
            AND c_news.deleted = 0
            ORDER BY date_entered DESC";
            $rs5 = $GLOBALS['db']->query($q5);
            $news = array();
            while ($r = $GLOBALS['db']->fetchbyAssoc($rs5)) {
                $qViewNews = "SELECT IFNULL(SUM(count_read_news) , 0)
                FROM c_news_contacts_1_c
                WHERE deleted = 0 AND c_news_contacts_1c_news_ida = '{$r['primaryid']}'";
                $views = $GLOBALS['db']->getOne($qViewNews);
                $news[$r['primaryid']] = array(
                    'id' => $r['primaryid'],
                    'name' => $r['name'],
                    'thumbnail' => $r['thumbnail'],
                    'url' => $r['url'],
                    'type' => $r['type'],
                    'pin' => $r['pin'],
                    'description' => $r['description'],
                    'type_news' => $r['type_news'],
                    'date_entered' => $timedate->to_display_date_time($r['date_entered']),
                    'end_date' => $timedate->to_display_date($r['end_date'], false),
                    'start_date' => $timedate->to_display_date($r['start_date'], false),
                    'views' => $views,
                    'db_date_entered' => $r['date_entered'],
                    'db_end_date' => $r['end_date'],
                    'db_start_date' => $r['start_date']
                );
            }

            $language_options = array('case_status_dom', 'case_sub_type_dom', 'status_class_list', 'attendance_type_list', 'do_homework_list', 'ss_status_list', 'gallery_type_list'); //mảng chứa các options trong app_list_strings
            //Set App_String
            $app_strings = array(
                'en' => parseAppListString('en_us', $language_options),
                'vn' => parseAppListString('vn_vn', $language_options),
            );
            if ($args['dotbfrom'] == 'web') {
                $app_strings = array(
                    'en' => array('case_sub_type_dom' => $app_list_strings['full_relate_feedback_list'], 'case_status_dom' => $app_list_strings['case_status_dom']),
                    'vn' => array('case_sub_type_dom' => [
                        'Complaint' => 'Phàn nàn',
                        'Suggestion' => 'Góp ý',
                        'Question' => 'Câu hỏi',
                        'Other' => 'Khác'],
                        'case_status_dom' => [
                            'New' => 'Mới',
                            'Assigned' => 'Đã tiếp nhận',
                            'Pending Input' => 'Đang giải quyết',
                            'Closed' => 'Đã đóng']),
                );
            }

            //Load Canvas LMS config
            require_once("custom/include/lms/canvas/autoload.php");
            $canvas = new CanvasEnvironment;
            $lms_config = array();
            $lms_config['canvas']['enable'] = intval($canvas->enable);
            $lms_config['canvas']['apiHost'] = $canvas->apiHost;
            $lms_config['canvas']['token'] = $canvas->token;
            $lms_config['canvas']['accountId'] = $canvas->accountId;
            $lms_config['canvas']['sso_enable'] = $canvas->sso_enable;
            $lms_config['canvas']['auth_provider_id'] = $canvas->auth_provider_id;


            //Return
            return array(
                'success' => true,
                'portal_name' => $user_name,
                'password' => $password,
                'student_list' => array_values($student_list),
                'news_list' => array_values($news),
                'app_strings_en' => $app_strings['en'],
                'app_strings_vn' => $app_strings['vn'],
                'lms_config' => $lms_config,
            );


        } else
            return array(
                'success' => false,
                'error' => "sending_failed."
            );

    }

    function teacherLogin(ServiceBase $api, array $args)
    {

        global $timedate, $current_user, $app_list_strings;
        if (isset($args) && !empty($args)) {
            $now = $timedate->nowDb();
            $user_name = $args['portal_name'];
            $password = $args['password'];
            $res = array();

            //Check Username credential
            $q1 = "SELECT DISTINCT
            IFNULL(c_teachers.id, '') primaryid,
            IFNULL(c_teachers.picture, '') picture,
            IFNULL(c_teachers.cover_app, '') cover_app,
            IFNULL(c_teachers.teacher_id, '') teacher_id,
            IFNULL(c_teachers.full_teacher_name, '') name,
            IFNULL(c_teachers.first_name, '') first_name,
            IFNULL(c_teachers.last_name, '') last_name,
            IFNULL(c_teachers.title, '') title,
            IFNULL(c_teachers.gender, '') gender,
            IFNULL(c_teachers.status, '') status,
            IFNULL(l1_2.email_address, '') email,
            c_teachers.dob dob,
            IFNULL(c_teachers.phone_mobile, '') phone_mobile,
            IFNULL(l1.id, '') user_id,
            IFNULL(l1.user_name, '') user_name,
            IFNULL(l1.user_hash, '') user_hash,
            IFNULL(l2.id, '') assigned_to_user_id,
            IFNULL(l2.full_user_name, '') assigned_to_user,
            IFNULL(l3.id, '') center_id,
            IFNULL(l3.name, '') center_name
            FROM c_teachers
            LEFT JOIN email_addr_bean_rel l1_1 ON c_teachers.id = l1_1.bean_id
            AND l1_1.deleted = 0
            AND l1_1.bean_module = 'C_Teachers'
            AND l1_1.primary_address = 1
            LEFT JOIN
            email_addresses l1_2 ON l1_2.id = l1_1.email_address_id
            AND l1_2.deleted = 0
            INNER JOIN
            users l1 ON c_teachers.user_id = l1.id
            AND l1.deleted = 0
            INNER JOIN
            users l2 ON c_teachers.assigned_user_id = l2.id
            AND l2.deleted = 0
            LEFT JOIN
            teams l3 ON c_teachers.team_id = l3.id
            AND l3.deleted = 0
            WHERE (((l1.user_name = '$user_name')
            AND (IFNULL(l1.status, '') = 'Active')))
            AND c_teachers.deleted = 0";

            $teachers = $GLOBALS['db']->fetchArray($q1);
            if (count($teachers) == 0)
                return array(
                    'success' => false,
                    'error' => "invalid_username",
                );

            $teacher = array();
            //Check Password credential
            $pwdCheck = false;

            foreach ($teachers as $teacher) {
                if ((User::checkPassword($password, $teacher['user_hash']) || md5($password) == $teacher['user_hash']) && !$pwdCheck)
                    $pwdCheck = true;
                //add student array
                $teacher['dob'] = $timedate->to_display_date($teacher['dob'], false);//format birthdate
                $teacher = $teacher;

                $teacher['class_list'] = array();
                $teacher['unread_noti'] = 0;
                $teacher['notifications'] = array();
            }

            if (!$pwdCheck)
                return array(
                    'success' => false,
                    'error' => "invalid_password",
                );

            $str_teacher_id = implode("','", array_column($teachers, 'primaryid'));
            $str_user_id = implode("','", array_column($teachers, 'user_id'));

            //Get Current Class
            $nowDb = $timedate->nowDb();
            $q2 = "SELECT DISTINCT
            IFNULL(l2.id, '') class_id,
            IFNULL(l2.picture, '') picture,
            IFNULL(l2.name, '') class_name,
            IFNULL(l2.class_code, '') class_code,
            IFNULL(l2.main_schedule, '') main_schedule,
            l2.start_date start_date,
            l2.end_date end_date,
            l2.date_entered date_entered,
            IFNULL(l2.kind_of_course, '') kind_of_course,
            IFNULL(l2.level, '') level,
            IFNULL(l2.max_size, '') max_size,
            IFNULL(l2.hours, 0) hours,
            IFNULL(l2.status, '') status,
            IFNULL(l3.id, '') koc_id,
            IFNULL(l3.name, '') koc_name,
            IFNULL(l4.id, '') center_id,
            IFNULL(l4.name, '') center_name,
            IFNULL(l4.code_prefix, '') center_code,
            IFNULL(l8.name, '') room_name,
            IFNULL(l9.full_teacher_name, '') teacher_name,
            IFNULL(meetings.id, '') ss_id,
            IFNULL(meetings.name, '') ss_name,
            IFNULL(meetings.type, '') ss_learning_type,
            (CASE
            WHEN DATE_SUB(meetings.date_end, INTERVAL 1 HOUR) < '$nowDb' THEN ''
            ELSE IFNULL(meetings.join_url, '')
            END) ss_displayed_url,
            IFNULL(meetings.lesson_number, '') ss_lesson,
            IFNULL(meetings.week_no, '') ss_week_no,
            meetings.till_hour ss_till_hour,
            meetings.date_start ss_date_start,
            meetings.date_end ss_date_end,
            IFNULL(l1.id, '') ss_teacher_id,
            IFNULL(l1.full_teacher_name, '') ss_teacher_name,
            IFNULL(l5.id, '') ss_ta_id,
            IFNULL(l5.full_teacher_name, '') ss_ta_name,
            IFNULL(l6.id, '') ss_room_id,
            IFNULL(l6.name, '') ss_room_name,
            IFNULL(meetings.observe_note, '') ss_observe_note,
            IFNULL(meetings.syllabus_custom, '') ss_syllabus_custom,
            IFNULL(meetings.topic_custom, '') ss_topic_custom,
            IFNULL(meetings.objective_custom, '') ss_note_for_teacher_custom,
            IFNULL(meetings.syllabus_id, '') ss_syllabus_id,
            IFNULL(l10.name, '') ss_syllabus_topic,
            IFNULL(l10.description, '') ss_syllabus_content,
            IFNULL(l10.homework, '') ss_syllabus_homework,
            IFNULL(l10.note_for_teacher, '') ss_syllabus_note_for_teacher,
            meetings.duration_cal ss_duration_cal,
            meetings.observe_score ss_observe_score,
            IFNULL(l7.id,'') assigned_user_id,
            IFNULL(l7.full_user_name,'') assigned_to,
            IFNULL(meetings.total_attended,0) total_attended,
            IFNULL(meetings.total_absent,0) total_absent,
            IFNULL(meetings.total_student,0) total_student
            FROM meetings
            INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
            INNER JOIN c_teachers l1 ON  l1.id IN (meetings.teacher_id, meetings.teacher_cover_id, meetings.sub_teacher_id)  AND l1.deleted = 0
            INNER JOIN j_kindofcourse l3 ON l2.koc_id = l3.id AND l3.deleted = 0
            INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
            LEFT JOIN c_teachers l5 ON meetings.teacher_cover_id = l5.id AND l5.deleted = 0
            LEFT JOIN c_rooms l6 ON meetings.room_id = l6.id AND l6.deleted = 0
            LEFT JOIN users l7 ON l2.assigned_user_id=l7.id AND l7.deleted=0
            LEFT JOIN c_rooms l8 ON l2.room_id=l8.id AND l8.deleted=0
            LEFT JOIN c_teachers l9 ON l2.teacher_id = l9.id AND l9.deleted = 0
            LEFT JOIN j_syllabus l10 ON meetings.syllabus_id = l10.id AND l10.deleted = 0
            WHERE (((l1.id = '$str_teacher_id' )
            AND (IFNULL(meetings.session_status, '') <> 'Cancelled'
            OR (IFNULL(meetings.session_status, '') IS NULL
            AND 'Cancelled' IS NOT NULL))
            AND (IFNULL(meetings.meeting_type, '') = 'Session')))
            AND meetings.deleted = 0
            GROUP BY meetings.id
            ORDER BY status DESC,class_id ASC, meetings.date_start ASC";
            $rowS = $GLOBALS['db']->fetchArray($q2);
            $runClass = '####';
            $classes = array();

            //Get Kind Of Course Syllabus - For best perfomance
            $classIds = implode("','", array_unique(array_column($rowS, 'class_id')));

            $koc = array();
            $q2_1 = "SELECT DISTINCT
            IFNULL(l1.id, '') koc_id,
            IFNULL(l1.name, '') koc_name,
            IFNULL(l1.kind_of_course, '') kind_of_course,
            IFNULL(l1.content, '') content,
            IFNULL(l1.lms_content, '') lms_content
            FROM j_class INNER JOIN j_kindofcourse l1 ON j_class.koc_id = l1.id  AND l1.deleted = 0
            WHERE (j_class.id IN ('$classIds')) AND j_class.deleted = 0";
            $rowK = $GLOBALS['db']->fetchArray($q2_1);
            foreach ($rowK as $key => $rK) {
                $koc[$rK['koc_id']]['koc_id'] = $rK['koc_id'];
                $koc[$rK['koc_id']]['koc_name'] = $rK['koc_name'];
                $koc[$rK['koc_id']]['kind_of_course'] = $rK['kind_of_course'];
                $koc[$rK['koc_id']]['content'] = ((!empty($rK['content'])) ? json_decode(html_entity_decode($rK['content']), true) : '');
            }


            //List Class ID
            $classIds = array();

            foreach ($rowS as $_key => $r2) {
                if ($runClass != $r2['class_id']) {
                    $runClass = $r2['class_id'];
                    $classes[$runClass]['id'] = $r2['class_id'];
                    $classes[$runClass]['picture'] = $r2['picture'];
                    $classes[$runClass]['class_name'] = $r2['class_name'];
                    $classes[$runClass]['class_code'] = $r2['class_code'];

                    $schedule = json_decode(html_entity_decode($r2['main_schedule']), true);
                    $sche = array();
                    foreach ($schedule as $key => $value) {
                        $detail = array();
                        foreach ($value as $k => $val) {
                            $detail[$k]['day'] = $key;
                            $detail[$k]['start_time'] = $val['start_time'];
                            $detail[$k]['end_time'] = $val['end_time'];
                            $detail[$k]['duration_hour'] = $val['duration_hour'];
                            $detail[$k]['revenue_hour'] = $val['revenue_hour'];
                            $detail[$k]['teaching_hour'] = $val['teaching_hour'];
                        }
                        $sche = $detail;
                    }
                    $classes[$runClass]['main_schedule'] = $sche;

                    $classes[$runClass]['start_date'] = $timedate->to_display_date($r2['start_date'], false);
                    $classes[$runClass]['end_date'] = $timedate->to_display_date($r2['end_date'], false);
                    $classes[$runClass]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['date_entered']));
                    $classes[$runClass]['kind_of_course'] = $r2['kind_of_course'];
                    $classes[$runClass]['level'] = $r2['level'];
                    $classes[$runClass]['hours'] = $r2['hours'];
                    $classes[$runClass]['assigned_to'] = $r2['assigned_to'];
                    $classes[$runClass]['koc_id'] = $r2['koc_id'];
                    $classes[$runClass]['koc_name'] = $r2['koc_name'];
                    $classes[$runClass]['room_name'] = $r2['room_name'];
                    $classes[$runClass]['teacher_name'] = $r2['teacher_name'];
                    $classes[$runClass]['max_size'] = $r2['max_size'];
                    $classes[$runClass]['team_name'] = $r2['center_name'];
                    $classes[$runClass]['team_code'] = $r2['center_code'];
                    $classes[$runClass]['status'] = $r2['status'];


                    $content = $koc[$r2['koc_id']]['content'];
                    foreach ($content as $ct) {
                        if ($ct['levels'] == $r2['level'])
                            $docs = $ct['doc_url'];
                    }
                    $classes[$runClass]['doc_url'] = $docs;

                    $classes[$runClass]['sessions'] = array();
                    $classes[$runClass]['albums'] = array();
                    $classes[$runClass]['feedback'] = array();
                    $classes[$runClass]['news'] = array();

                }
                $session = array();
                $session['ss_id'] = $r2['ss_id'];
                $session['ss_name'] = $r2['ss_name'];
                $session['ss_lesson'] = $r2['ss_lesson'];
                $session['ss_week_no'] = $r2['ss_week_no'];
                $session['ss_week_date'] = date('l', strtotime("+7 hours " . $r2['ss_date_start']));
                $session['ss_till_hour'] = $r2['ss_till_hour'];
                $session['ss_date_start'] = $timedate->to_display_date_time($r2['ss_date_start']);
                $session['ss_date_end'] = $timedate->to_display_date_time($r2['ss_date_end']);
                $session['ss_db_date_start'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_start']));
                $session['ss_db_date_end'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_end']));
                $session['ss_db_date'] = date('Y-m-d', strtotime("+7 hours " . $r2['ss_date_start']));

                if ($now < $r2['ss_date_start']) $session['ss_status'] = "not_stated";
                elseif ($now >= $r2['ss_date_start'] && $now <= $r2['ss_date_end']) $session['ss_status'] = "in_progress";
                elseif ($now > $r2['ss_date_end']) $session['ss_status'] = "finished";

                $session['ss_duration_cal'] = $r2['ss_duration_cal'];
                $session['ss_ta_name'] = $r2['ss_ta_name'];
                $session['ss_teacher_name'] = $r2['ss_teacher_name'];
                $session['ss_room_name'] = $r2['ss_room_name'];
                $session['ss_observe_score'] = $r2['ss_observe_score'];
                $session['ss_syllabus_custom'] = $r2['ss_syllabus_custom'];
                $session['ss_topic_custom'] = $r2['ss_topic_custom'];
                $session['ss_note_for_teacher_custom'] = $r2['ss_note_for_teacher_custom'];
                $session['ss_learning_type'] = $r2['ss_learning_type'];
                $session['ss_displayed_url'] = $r2['ss_displayed_url'];
                $session['total_student'] = $r2['total_student'];
                $session['total_attended'] = $r2['total_attended'];
                $session['total_absent'] = $r2['total_absent'];
                $session['syllabus_topic'] = $r2['ss_syllabus_topic'];
                $session['syllabus_activities'] = $r2['ss_syllabus_content'];
                $session['syllabus_homework'] = $r2['ss_syllabus_homework'];
                $session['syllabus_note_for_teacher'] = $r2['ss_syllabus_note_for_teacher'];

                $classes[$runClass]['sessions'][] = $session;

                if (!in_array($runClass, $classIds))
                    $classIds[] = $runClass;
            }

            // Get total session of class
            $qSession = "SELECT DISTINCT
            IFNULL(c.id, '') class_id,
            COUNT(l1.id) total_session
            FROM j_class c
            INNER JOIN meetings l1 ON l1.ju_class_id = c.id AND l1.deleted = 0
            WHERE (c.id IN ('" . implode("','", $classIds) . "'))
            AND (IFNULL(l1.meeting_type, '') = 'Session')
            AND (IFNULL(l1.session_status, '') <> 'Cancelled' OR (IFNULL(l1.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
            GROUP BY c.id";
            $rSession = $GLOBALS['db']->query($qSession);
            while ($row = $GLOBALS['db']->fetchbyAssoc($rSession)) {
                $classes[$row['class_id']]['total_session'] = $row['total_session'];
            }

            //Add Gallery Image
            $gls = $this->getListGallery($classIds);
            //Gallery By Class
            foreach ($classes as $class_id => $class) {
                if (array_key_exists($class_id, $gls))
                    $classes[$class_id]['albums'] = array_values($gls[$class_id]);
            }

            //Get List Student
            $getListStudent = "SELECT p.* FROM (
            SELECT DISTINCT
            IFNULL(c.id, '') student_id,
            IFNULL(c.first_name, '') first_name,
            IFNULL(c.full_student_name, '') student_name,
            IFNULL(c.picture, '') picture,
            IFNULL(c.contact_id, '') portal_id,
            IFNULL(c.phone_mobile, '') student_phone,
            'Contacts' student_type,
            c.birthdate birthdate,
            IFNULL(c.nick_name, '') nick_name,
            c.guardian_name parent_name,
            c.relationship relationship,
            IFNULL(c.phone_guardian, '') parent_phone,
            c.guardian_name_2 parent_name_2,
            c.relationship2 relationship_2,
            IFNULL(c.other_mobile, '') parent_phone_2,
            '' type,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.status, '') class_status
            FROM j_classstudents
            INNER JOIN j_class l1 ON j_classstudents.class_id = l1.id AND l1.deleted = 0
            INNER JOIN contacts c ON j_classstudents.student_id = c.id AND c.deleted = 0 AND j_classstudents.student_type = 'Contacts'
            WHERE (l1.id IN('".implode("','",$classIds)."') ) AND j_classstudents.deleted = 0
            UNION ALL
            SELECT DISTINCT
            IFNULL(l.id, '') student_id,
            IFNULL(l.first_name, '') first_name,
            IFNULL(l.full_lead_name, '') student_name,
            IFNULL(l.picture, '') picture,
            IFNULL(l.contact_id, '') portal_id,
            IFNULL(l.phone_mobile, '') student_phone,
            'Leads' student_type,
            l.birthdate birthdate,
            IFNULL(l.nick_name, '') nick_name,
            l.guardian_name parent_name,
            l.relationship relationship,
            IFNULL(l.phone_guardian, '') parent_phone,
            l.guardian_name_2 parent_name_2,
            l.relationship2 relationship_2,
            IFNULL(l.other_mobile, '') parent_phone_2,
            '' type,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.status, '') class_status
            FROM j_classstudents
            INNER JOIN j_class l1 ON j_classstudents.class_id = l1.id AND l1.deleted = 0
            INNER JOIN leads l ON j_classstudents.student_id = l.id AND l.deleted = 0 AND j_classstudents.student_type = 'Leads'
            WHERE (l1.id IN('".implode("','",$classIds)."') ) AND j_classstudents.deleted = 0) p ORDER BY p.first_name ASC";

            $rGetListStudent = $GLOBALS['db']->query($getListStudent);

            while ($r = $GLOBALS['db']->fetchbyAssoc($rGetListStudent)) {
                $student = array();
                $student['student_id'] = $r['student_id'];
                $student['student_name'] = $r['student_name'];
                $student['picture'] = $r['picture'];
                $student['portal_id'] = $r['portal_id'];
                $student['student_phone'] = $r['student_phone'];
                $student['student_type'] = $r['student_type'];
                $student['birthdate'] = $timedate->to_display_date($r['birthdate'], false);;
                $student['nick_name'] = $r['nick_name'];
                $student['parent_name'] = $r['parent_name'];
                $student['status_in_class'] = $r['status'];
                $student['type_in_class'] = $r['type'];

                $classes[$r['class_id']]['student'][] = $student;
            }

            //Get Feedback
            $q4 = "SELECT DISTINCT
            IFNULL(cases.id, '') primaryid,
            IFNULL(cases.name, '') subject,
            IFNULL(cases.status, '') status,
            IFNULL(cases.case_number, '') case_number,
            IFNULL(cases.type, '') type,
            IFNULL(cases.relate_feedback_list, '') sub_type,
            IFNULL(cases.priority, '') priority,
            cases.date_entered date_entered,
            cases.received_date received_date,
            IFNULL(cases.description, '') feedback_content,
            cases.resolved_date resolved_date,
            IFNULL(cases.resolution, '') response_to_customer,
            IFNULL(l2.id, '') student_id,
            IFNULL(l2.full_student_name, '') student_name,
            IFNULL(l2.contact_id, '') student_code,
            IFNULL(l2.phone_mobile, '') mobile,
            IFNULL(l2.guardian_name, '') parent,
            IFNULL(l1.id, '') class_id,
            IFNULL(l1.name, '') class_name,
            IFNULL(l4.full_user_name, '') assigned_to,
            IFNULL(l3.name,'') center,
            IFNULL(l1.id,'') center_id
            FROM cases
            INNER JOIN j_class_cases_1_c l1_1 ON cases.id = l1_1.j_class_cases_1cases_idb AND l1_1.deleted = 0
            INNER JOIN j_class l1 ON l1.id = l1_1.j_class_cases_1j_class_ida AND l1.deleted = 0
            INNER JOIN contacts_cases_1_c l2_1 ON cases.id = l2_1.contacts_cases_1cases_idb AND l2_1.deleted = 0
            INNER JOIN contacts l2 ON l2.id = l2_1.contacts_cases_1contacts_ida AND l2.deleted = 0
            INNER JOIN teams l3 ON cases.team_id=l3.id AND l3.deleted=0
            INNER JOIN users l4 ON cases.assigned_user_id = l4.id AND l4.deleted = 0
            LEFT JOIN users l5 ON cases.cso_id = l5.id AND l5.deleted = 0 AND (l5.id = '$str_user_id')
            WHERE (l1.id IN ('" . implode("','", $classIds) . "')) AND cases.deleted = 0
            ORDER BY class_id ASC,  date_entered DESC";
            $rs4 = $GLOBALS['db']->query($q4);

            while ($r4 = $GLOBALS['db']->fetchbyAssoc($rs4)) {
                $feedback = array();
                $feedback['number'] = $r4['case_number'];
                $feedback['feedback_id'] = $r4['primaryid'];
                $feedback['subject'] = $r4['subject'];
                $feedback['class_id'] = $r4['class_id'];
                $feedback['class_name'] = $r4['class_name'];
                $feedback['type'] = $r4['type'];
                $feedback['sub_type'] = $r4['sub_type'];
                $feedback['status'] = $r4['status'];
                $feedback['received_date'] = $timedate->to_display_date($r4['received_date'], false);
                $feedback['date_entered'] = $timedate->to_display_date_time($r4['date_entered']);
                $feedback['resolved_date'] = $timedate->to_display_date($r4['resolved_date'], false);
                $feedback['feedback_content'] = $r4['feedback_content'];
                $feedback['response_to_customer'] = $r4['response_to_customer'];
                $feedback['assigned_to'] = $r4['assigned_to'];
                $feedback['center_id'] = $r4['center_id'];
                $feedback['center'] = $r4['center'];
                $feedback['last_comment'] = $r4['last_comment'];
                $feedback['last_comment_date'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r4['last_comment_date']));
                $feedback['last_comment_depositor'] = $r4['last_comment_depositor'];
                $feedback['count_unread_comment'] = $r4['count_unread_comment'];
                $classes[$r4['class_id']]['feedback'][] = $feedback;
            }

            $teacher['class_list'] = array_values($classes);

            //Get New
            $nowDate = date('Y-m-d');
            $q5 = "SELECT DISTINCT
            IFNULL(c_news.id, '') primaryid,
            IFNULL(c_news.name, '') name,
            IFNULL(c_news.picture, '') thumbnail,
            IFNULL(c_news.url, '') url,
            IFNULL(c_news.pin_teacher, 0) pin,
            IFNULL(c_news.description, '') description,
            IFNULL(c_news.type_news, '') type_news,
            c_news.start_date start_date,
            c_news.end_date end_date,
            c_news.date_entered date_entered
            FROM c_news
            WHERE (c_news.end_date >= '$nowDate')
            AND (c_news.type LIKE '%^Teacher^%')
            AND c_news.deleted = 0
            ORDER BY date_entered DESC";
            $rs5 = $GLOBALS['db']->query($q5);
            $news = array();
            while ($r = $GLOBALS['db']->fetchbyAssoc($rs5)) {
                $news[$r['primaryid']] = array(
                    'id' => $r['primaryid'],
                    'name' => $r['name'],
                    'thumbnail' => $r['thumbnail'],
                    'url' => $r['url'],
                    'type' => $r['type'],
                    'pin' => $r['pin'],
                    'description' => $r['description'],
                    'type_news' => $r['type_news'],
                    'date_entered' => $timedate->to_display_date_time($r['date_entered']),
                    'end_date' => $timedate->to_display_date($r['end_date'], false),
                    'start_date' => $timedate->to_display_date($r['start_date'], false),
                );
            }

            //Get Notification
            $q7 = "SELECT nttt.* FROM (SELECT DISTINCT
            IFNULL(nt.id, '') primaryid,
            CASE nt.assigned_user_id WHEN 'all' THEN IFNULL(l1.id, '') ELSE (IFNULL(l1.id, '')) END assigned_user_id,
            IFNULL(nt.name, '') title,
            IFNULL(nt.description, '') body,
            IFNULL(nt.is_read, 0) is_read,
            IFNULL(nt.parent_id, '') record_id,
            IFNULL(nt.parent_type, '') module_name,
            IFNULL(nt.created_by, '') created_by,
            IFNULL(nt.student_read, '') student_read,
            nt.date_entered date_entered
            FROM notifications nt
            INNER JOIN users l1 ON (nt.assigned_user_id = l1.id OR nt.assigned_user_id = 'all') AND l1.deleted = 0 AND (nt.assigned_user_id = '$str_user_id' OR nt.assigned_user_id = 'all')
            WHERE nt.deleted = 0 AND (nt.apps_type = 'Teacher')) nttt WHERE nttt.assigned_user_id = '$str_user_id'
            ORDER BY nttt.assigned_user_id ASC, nttt.date_entered DESC
            LIMIT 50";
            $rs = $GLOBALS['db']->query($q7);
            $key = 0;
            while ($r = $GLOBALS['db']->fetchbyAssoc($rs)) {

                $nt = array();
                $nt['primaryid'] = $r['primaryid'];
                $nt['title'] = $r['title'];
                $nt['body'] = $r['body'];
                //get notification All
                if (!empty($r['student_read']) && $r['student_read'] != '[]') {
                    $student_read = json_decode(html_entity_decode($r['student_read']), true);
                    if (in_array($r['assigned_user_id'], $student_read))
                        $r['is_read'] = '1';
                }
                $nt['is_read'] = ($r['is_read'] == '1') ? true : false;
                if ($r['is_read'] != '1') $teacher['unread_noti'] += 1; //Add Number of Notification

                $nt['module_name'] = $r['module_name'];
                $nt['record_id'] = $r['record_id'];
                $nt['created_by'] = $r['created_by'];
                $nt['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r['date_entered']));

                $teacher['notifications'][$key] = $nt;

                $key++;
            }

            $language_options = array('case_status_dom', 'case_sub_type_dom', 'status_class_list', 'attendance_type_list', 'do_homework_list', 'ss_status_list', 'situation_type_list', 'gallery_type_list'); //mảng chứa các options trong app_list_strings


            //Set App_String
            $app_strings = array(
                'en' => parseAppListString('en_us', $language_options),
                'vn' => parseAppListString('vn_vn', $language_options),
            );

            //list tag
            $tag_list = array();
            $qGetTags = "SELECT IFNULL(id, '') primaryid
            , IFNULL(name, '') tag_name
            , IFNULL(name_lower, '') name_lower
            FROM tags
            WHERE deleted=0";
            $resGetTags = $GLOBALS['db']->query($qGetTags);
            while ($row = $GLOBALS['db']->fetchByAssoc($resGetTags)) {
                $tag_list[$row['primaryid']]['primaryid'] = $row['primaryid'];
                $tag_list[$row['primaryid']]['tag_name'] = $row['tag_name'];
                $tag_list[$row['primaryid']]['name_lower'] = $row['name_lower'];
            }
            // Get list center
            $list_center = $GLOBALS['db']->fetchArray("
                SELECT id,
                name,
                IFNULL(legal_name,'' ) legal_name,
                IFNULL(short_name,'') short_name,
                IFNULL(code_prefix,'') code_prefix ,
                IFNULL(phone_number,'') phone ,
                IFNULL(description,'') description
                FROM teams
            WHERE deleted =0 and private = 0");
            //get Config
            $admin = new Administration();
            $admin->retrieveSettings();
            $config = array(
                'doc_folder' => empty($admin->settings['default_doc_folder']) ? '' : $admin->settings['default_doc_folder'],
                'google_folder' => empty($admin->settings['default_google_folder']) ? '' : $admin->settings['default_google_folder'],
                'hot_line' => empty($admin->settings['default_hotline']) ? '' : substr($admin->settings['default_hotline'], 0, strlen($admin->settings['default_hotline']) - 1),
            );
            //Return
            return array(
                'success' => true,
                'portal_name' => $user_name,
                'password' => $password,
                'teacher' => $teacher,
                'news_list' => array_values($news),
                'tag_list' => array_values($tag_list),
                'center_list' => $list_center,
                'app_strings_en' => $app_strings['en'],
                'app_strings_vn' => $app_strings['vn'],
                'config' => $config
            );


        } else
            return array(
                'success' => false,
                'error' => "sending_failed."
            );

    }

    function loadBranding(ServiceBase $api, array $args)
    {
        global $dotb_config;
        $admin = new Administration();
        $admin->retrieveSettings();
        $color_config = $admin->settings['default_color_config'];
        $primary_color = $admin->settings['default_primary_color'];
        $second_color = $admin->settings['default_second_color'];
        $third_color = $admin->settings['default_third_color'];
        $hotline = substr($admin->settings['default_hotline'], 0, strlen($admin->settings['default_hotline']) - 1);
        $update_in_app = $admin->settings['default_update_in_app'];
        $show_news_views = $admin->settings['default_show_news_views'];
        $storage_method = $admin->settings['default_storage_config'];
        $enable_fees = $admin->settings['default_collect_tuition_fee'];
        $bank_name = $admin->settings['default_bank_name'];
        $account_name = $admin->settings['default_account_name'];
        $account_number = $admin->settings['default_account_number'];
        $vietqr_enable = $admin->settings['vietqr_enable'];
        $vietqr_bank = $admin->settings['vietqr_bank'];
        $vietqr_bank_number = $admin->settings['vietqr_bank_number'];
        $vietqr_bank_name = $admin->settings['vietqr_bank_name'];
        $vietqr_account_name = $admin->settings['vietqr_account_name'];
        $vietqr_bank_logo = $admin->settings['vietqr_bank_logo'];

        if (empty($dotb_config['brand_name']))
            return array(
                'success' => false,
                'error' => "no_info"
            );
        else {
            if (empty($dotb_config['brand_color1']) && empty($dotb_config['brand_color2'])) {
                $dotb_config['brand_color1'] = '#10cdfc';
                $dotb_config['brand_color2'] = '#85ee95';
            } elseif (empty($dotb_config['brand_color1']) && !empty($dotb_config['brand_color2']))
                $dotb_config['brand_color1'] = $dotb_config['brand_color2'];
            elseif (!empty($dotb_config['brand_color1']) && empty($dotb_config['brand_color2']))
                $dotb_config['brand_color2'] = $dotb_config['brand_color1'];


            return array(
                'success' => true,
                'brand_name' => $dotb_config['brand_name'],
                'brand_id' => $dotb_config['brand_id'],
                'brand_logo' => 'custom/themes/default/images/company_logo.png',
                'color_config' => $color_config,
                'primary_color' => $primary_color,
                'second_color' => $second_color,
                'third_color' => $third_color,
                'hotline' => $hotline,
                'update_in_app' => $update_in_app,
                'show_news_views' => $show_news_views,
                'storage_method' => $storage_method,
                'enable_fees' => $enable_fees,
                'bank_name' => $bank_name,
                'account_name' => $account_name,
                'account_number' => $account_number,
                'vietqr_enable' => $vietqr_enable,
                'vietqr_bank' => $vietqr_bank,
                'vietqr_bank_number' => $vietqr_bank_number,
                'vietqr_bank_name' => $vietqr_bank_name,
                'vietqr_account_name' => $vietqr_account_name,
            );
        }
    }

    function post_token(ServiceBase $api, array $args)
    {
        $studentId = $args['student_id'];          //Student
        $userId = $args['user_id'];             //Teacher
        $apps_type = $args['apps_type'];
        $token = $args['token'];
        $device_name = $args['device_name'];
        $method = $args['method'];

        if (empty($apps_type) || $apps_type == 'Student') {
            if (empty($studentId)) return array('success' => false);

            //Update Last Login
            $ext_date = "last_login = '" . $GLOBALS['timedate']->nowDb() . "'";

            //update device name
            $ext_dc = '';
            if (!empty($device_name)) $ext_dc = ", device_name = '$device_name'";

            //Get token
            $content = $GLOBALS['db']->getOne("SELECT IFNULL(portal_app_token, '') portal_app_token FROM contacts WHERE id = '$studentId'");

            $curToken = json_decode(html_entity_decode($content), true);
            $count_change = 0;

            if (!in_array($token, $curToken) && (empty($method) || $method == 'add')) {    //Add Token
                $curToken[] = $token;
                $count_change++;
            }

            if (($key = array_search($token, $curToken)) !== false && $method == 'delete') { //remove Token
                unset($curToken[$key]);
                $count_change++;
            }


            $ext_token = '';
            if ($count_change > 0) $ext_token = ", portal_app_token = '" . json_encode($curToken) . "'";

            //$GLOBALS['db']->query("UPDATE contacts SET $ext_date $ext_dc $ext_token WHERE id = '$studentId'");

            return array(
                'success' => true,
            );
        } elseif ($apps_type == 'Teacher') {
            if (empty($userId)) return array('success' => false);

            //Get token
            $content = $GLOBALS['db']->getOne("SELECT IFNULL(portal_app_token, '') portal_app_token FROM users WHERE id = '$userId'");

            $curToken = json_decode(html_entity_decode($content), true);
            $count_change = 0;

            if (!in_array($token, $curToken) && (empty($method) || $method == 'add')) {    //Add Token
                $curToken[] = $token;
                $count_change++;
            }

            if (($key = array_search($token, $curToken)) !== false && $method == 'delete') { //remove Token
                unset($curToken[$key]);
                $count_change++;
            }

            $ext_token = '';
            if ($count_change > 0) $ext_token = ", portal_app_token = '" . json_encode($curToken) . "'";

            //$GLOBALS['db']->query("UPDATE users SET deleted=0, $ext_token WHERE id = '$userId'");

            return array(
                'success' => true,
            );
        } else {
            return array('success' => false);
        }
    }

    function updateNotification(ServiceBase $api, array $args)
    {
        if (!empty($args)) {
            $bean = BeanFactory::getBean($args['module_name'], $args['module_id']);

            //module = C_News
            if (isset($args['news_id']) && !empty($args['news_id'])) {
                $qCheckRelation = "SELECT COUNT(*)
                FROM c_news_contacts_1_c
                WHERE c_news_contacts_1c_news_ida = '{$args['news_id']}'
                AND c_news_contacts_1contacts_idb = '{$args['student_id']}'";
                $countRelation = $GLOBALS['db']->getOne($qCheckRelation . "AND deleted = 0");
                $date_modified = $GLOBALS['timedate']->nowDb();
                if ($args['is_read'] == 1) {
                    if ($countRelation == 0) {
                        $countRelationDeleted = $GLOBALS['db']->getOne($qCheckRelation . "AND deleted = 1");
                        if ($countRelationDeleted > 0) {
                            $qUpdateRelation = "UPDATE c_news_contacts_1_c
                            SET deleted = 0, date_modified ='{$date_modified}'
                            WHERE deleted = 1
                            AND c_news_contacts_1c_news_ida = '{$args['news_id']}'
                            AND c_news_contacts_1contacts_idb = '{$args['student_id']}'";
                            $GLOBALS['db']->query($qUpdateRelation);
                        } else {
                            $relation_id = create_guid();
                            $qInsertRelation = "INSERT INTO c_news_contacts_1_c (id, date_modified, deleted, c_news_contacts_1c_news_ida, c_news_contacts_1contacts_idb, count_read_news)
                            VALUES ('{$relation_id}','{$date_modified}', 0,'{$args['news_id']}','{$args['student_id']}', 1)";
                            $GLOBALS['db']->query($qInsertRelation);
                        }
                    } else {
                        $qUpdateView = "UPDATE c_news_contacts_1_c
                        SET count_read_news = count_read_news + 1
                        WHERE deleted = 0
                        AND c_news_contacts_1c_news_ida = '{$args['news_id']}'
                        AND c_news_contacts_1contacts_idb = '{$args['student_id']}'";
                        $GLOBALS['db']->query($qUpdateView);
                    }
                } elseif ($args['is_read'] != 1 && $countRelation > 0) {
                    $qUpdateRelation = "UPDATE c_news_contacts_1_c
                    SET deleted = 1, date_modified='{$date_modified}'
                    WHERE deleted=0
                    AND c_news_contacts_1c_news_ida = '{$args['news_id']}'
                    AND c_news_contacts_1contacts_idb = '{$args['student_id']}'";
                    $GLOBALS['db']->query($qUpdateRelation);
                }
            }

            if ($bean->apps_type == 'Student' || empty($bean->apps_type)) {  //Handle Update Student Notification - $args['student_id']
                if ($bean->student_id == 'all') {
                    $curReadList = json_decode(html_entity_decode($bean->student_read), true);
                    $count_change = 0;
                    if ($args['is_read'] == 1) {
                        if (!in_array($args['student_id'], $curReadList)) {
                            $curReadList[] = $args['student_id'];
                            $count_change++;

                        }
                    } else {
                        if (in_array($args['student_id'], $curReadList)) {
                            $curReadList = array_diff($curReadList, [$args['student_id']]);
                            $count_change++;
                        }
                    }
                    if ($count_change > 0) $bean->student_read = json_encode($curReadList);
                } else
                    $bean->is_read = $args['is_read'];
            } elseif ($bean->apps_type == 'Teacher') { //Handle Update Teacher Notification - $args['user_id']
                if ($bean->assigned_user_id == 'all') {
                    $curReadList = json_decode(html_entity_decode($bean->student_read), true);
                    $count_change = 0;
                    if ($args['is_read'] == 1) {
                        if (!in_array($args['user_id'], $curReadList)) {
                            $curReadList[] = $args['user_id'];
                            $count_change++;
                        }
                    } else {
                        if (in_array($args['user_id'], $curReadList)) {
                            $curReadList = array_diff($curReadList, [$args['user_id']]);
                            $count_change++;
                        }
                    }
                    if ($count_change > 0) $bean->student_read = json_encode($curReadList);
                } else
                    $bean->is_read = $args['is_read'];
            }


            $bean->save();

            return array(
                'success' => true,
                'bean_id' => $bean->id,
            );

        } else return array(
            'success' => false,
        );
    }
    function change_password(ServiceBase $api, array $args)
    {
        global $current_user;
        $current_user = new User();
        $current_user->retrieve_by_string_fields(array('user_name' => 'apps_admin'));

        $old_password = $args['password'];
        $apps_type = $args['apps_type'];
        $dont_check_old = $args['dont_check_old'];
        $user_id = $args['user_id'];
        $student_id = $args['student_id'];
        $phone_mobile = $args['phone_mobile'];
        $email = $args['email'];

        if (empty($dont_check_old)) $dont_check_old = false;

        if (empty($args['new_password']))
            return array(
                'success' => false,
                'error' => "invalid_new_password",
            );

        // Handle change pass STUDENT
        if (empty($apps_type) || $apps_type == 'Student') {
            $student = new Contact();
            if (!empty($student_id))
                $student->retrieve($student_id);
            elseif (!empty($phone_mobile))
                $student->retrieve_by_string_contains(array('phone_mobile' => $phone_mobile));
            elseif (!empty($email))
                $student->retrieve_by_string_fields(array('email1' => $email));

            if (!empty($student->id)) {
                //Check Old Password
                if (!$dont_check_old) {
                    $pwdCheck = false;
                    if (User::checkPassword($old_password, $student->portal_password) || md5($old_password) == $student->portal_password)
                        $pwdCheck = true;

                    if (!$pwdCheck)
                        return array(
                            'success' => false,
                            'error' => "invalid_password",
                        );
                }

                //update new password
                $student->portal_password = md5($args['new_password']);
                $student->save();
                return array(
                    'success' => true,
                );
            } else {
                return array(
                    'success' => false,
                    'error' => 'invalid_username',
                );
            }
        } elseif ($apps_type == 'Teacher') {
            // Handle change pass TEACHER
            $userT = new User();
            if (!empty($user_id))
                $userT->retrieve($user_id);
            elseif (!empty($phone_mobile))
                $userT->retrieve_by_string_contains(array('phone_mobile' => $phone_mobile));
            elseif (!empty($email))
                $userT->retrieve_by_string_fields(array('email1' => $email));

            if (!empty($userT->id)) {
                if (!$dont_check_old) {
                    $pwdCheck = false;
                    if (User::checkPassword($old_password, $userT->user_hash) || md5($old_password) == $userT->user_hash)
                        $pwdCheck = true;


                    if (!$pwdCheck)
                        return array(
                            'success' => false,
                            'error' => "invalid_password",
                        );
                }

                //update new password
                $userT->user_hash = md5($args['new_password']);
                $userT->save();
                return array(
                    'success' => true,
                );

            } else {
                return array(
                    'success' => false,
                    'error' => 'invalid_apps_type',
                    'apps_type' => $apps_type,
                    'user_id' => $user_id,
                );
            }
        }


    }

    function reset_password(ServiceBase $api, array $args)
    {
        global $dotb_config;

        require_once('modules/C_SMS/SMS/sms.php');

        $apps_type = $args['apps_type'];
        $user_name = $args['portal_name']; //editted

        if (empty($user_name))
            return array(
                'success' => false,
                'error' => 'invalid_username',
            );

        if (empty($apps_type) || $apps_type == 'Student') {
            //Check Username credential
            $q1 = "SELECT DISTINCT IFNULL(contacts.id, '') primaryid,
            IFNULL(contacts.team_id, '') team_id,
            IFNULL(contacts.phone_mobile, '') phone_mobile,
            IFNULL(l1.email_address, '') email
            FROM contacts
            LEFT JOIN email_addr_bean_rel l1_1 ON contacts.id = l1_1.bean_id
            AND l1_1.deleted = 0
            AND l1_1.bean_module = 'Contacts'
            AND l1_1.primary_address = 1
            LEFT JOIN
            email_addresses l1 ON l1.id = l1_1.email_address_id
            AND l1.deleted = 0
            WHERE (contacts.phone_mobile = '$user_name')
            AND (contacts.portal_active = '1')
            AND contacts.deleted = 0";
            $rows = $GLOBALS['db']->fetchArray($q1);

            $module_name = 'Contacts';

            if (count($rows) < 1) {
                return array(
                    'success' => false,
                    'error' => 'invalid_username',
                );
            }
        } elseif ($apps_type == 'Teacher') {
            //Check Username credential
            $q1 = "SELECT DISTINCT
            IFNULL(c_teachers.id, '') primaryid,
            IFNULL(c_teachers.full_teacher_name, '') name,
            IFNULL(c_teachers.first_name, '') first_name,
            IFNULL(c_teachers.team_id, '') team_id,
            IFNULL(c_teachers.phone_mobile, '') phone_mobile
            FROM c_teachers
            INNER JOIN users l1 ON c_teachers.user_id = l1.id AND l1.deleted = 0
            WHERE (((l1.user_name = '$user_name')
            AND (IFNULL(c_teachers.type, '') = 'Teacher')
            AND (IFNULL(l1.status, '') = 'Active')))
            AND c_teachers.deleted = 0";
            $rows = $GLOBALS['db']->fetchArray($q1);

            $module_name = 'C_Teachers';

            if (count($rows) < 1) {
                return array(
                    'success' => false,
                    'error' => 'invalid_username',
                );
            }
        }

        $randomid = mt_rand(1000, 9999);
        $message = $dotb_config['brand_name'] . ": DE CAP NHAT MAT KHAU, ma xac minh la $randomid.";
        $message .= "Ma co hieu luc trong 5 phut. KHONG chia se ma nay voi nguoi khac.";

        //Send SMS
        $sms = new sms();
        $result = (int)$sms->send_message($rows[0]['phone_mobile'], $message, $module_name, $rows[0]['primaryid'], '1', $rows[0]['team_id']);
        $status = 'RECEIVED';
        if ($result <= 0) $status = 'FAILED';

        if ($status == 'FAILED') {
            return array(
                'success' => false,
                'error' => 'SMS_service_error',
                'code' => $randomid,
                'expired_date' => date('Y-m-d H:i:s', strtotime("+5 minutes " . date('Y-m-d H:i:s'))),
            );
        } else {
            return array(
                'success' => true,
                'code' => $randomid,
                'expired_date' => date('Y-m-d H:i:s', strtotime("+5 minutes " . date('Y-m-d H:i:s'))),
            );
        }
    }

    function updateCustom(ServiceBase $api, array $args)
    {
        $moduleName = $args['module_name'];
        $moduleId = $args['module_id'];
        unset($args['module_id']);
        unset($args['module_name']);

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        if (!empty($args)) {
            if ($moduleName == 'C_Attendance') {
                $moduleId = $GLOBALS['db']->getOne("SELECT IFNULL(c_attendance.id, '') FROM c_attendance
                WHERE c_attendance.meeting_id = '{$args['meeting_id']}'
                  AND c_attendance.student_id = '{$args['student_id']}' AND deleted = 0");
            }
            $bean = BeanFactory::getBean($moduleName, $moduleId);
            foreach ($args as $field => $Val)
                $bean->$field = $Val;
            $bean->save();

            return array(
                'success' => true,
                'bean_id' => $bean->id,
            );

        } else return array(
            'success' => false,
        );
    }


    // type = [student, teacher]
    function saveComment(ServiceBase $api, array $args)
    {
        $value = $args['value'];
        $feedbackId = $args['feedback_id'];
        $userType = $args['type'];
        //lay activities_id
        $q1 = "SELECT activities.id
        FROM activities WHERE activities.deleted = 0
        AND activities.parent_id = '$feedbackId'
        ORDER BY date_entered ASC";
        $parent_id = $GLOBALS['db']->getOne($q1);
        $commentBean = BeanFactory::newBean('Comments');
        $commentBean->parent_id = $parent_id;
        $commentBean->data = json_encode(['type' => $userType, 'value' => $value, 'tags' => array()]);
        $commentBean->save();
    }

    function getConnection($storage = '')
    {
        require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';
        require_once 'custom/include/GoogleAPI/class.google.php';
        require_once 'custom/include/GoogleAPI/class.drive.php';

        if (empty($storage)) $storage = 'Google Drive';
        if ($storage == 'Google Drive') {
            $admin = new Administration();
            $admin->retrieveSettings();
            $gg = new PHPGoogle();
            if (empty($admin->settings['default_google_api_token'])) return array('success' => 0);
            $gg->setAccessToken($admin->settings['default_google_api_token']);
            $drive = new PHPDrive($gg->client);
            return array(
                'success' => 1,
                'connection' => $drive,
                'storage' => 'Google Drive'
            );
        } elseif ($storage == 'Amazon S3') {
            $AWS = new AWSHelper();
            //check connection S3
            if (!$AWS->getS3()) return array('success' => 0);
            return array(
                'success' => 1,
                'connection' => $AWS,
                'storage' => 'Amazon S3'
            );
        }
    }

    function getDoc(ServiceBase $api, array $args)
    {
        $resultConnection = $this->getConnection($args['storage']);
        if ($resultConnection['success']) {
            if ($resultConnection['storage'] == 'Google Drive') {
                if (empty($args['doc_url']))
                    return array(
                        'success' => false,
                        'error' => 'invalid_doc_url',
                    );
                $drive = $resultConnection['connection'];
                $data = $drive->getFilesInFolder(array("'" . $args['doc_url'] . "' in parents"), 'name_natural asc');
                return array(
                    'success' => true,
                    'data' => $data,
                );
            } elseif ($resultConnection['storage'] == 'Amazon S3') {
                $AWS = $resultConnection['connection'];
                $data = $AWS->getKey($args['doc_url']);
                if ($data['success'])
                    return array(
                        'success' => true,
                        'data' => $data['list_key'],
                    );
                else return array(
                    'success' => false,
                    'error' => $data['message'],
                );
            }
        } else return array(
            'success' => false,
            'error' => 'Don\'t connect to storage server',
        );
    }

    function getListAttendance(ServiceBase $api, array $args)
    {
        if (empty($args['class_id']) || empty($args['lesson_date'])) {
            return array(
                'success' => false,
                'error' => 'Invalid class_id or lesson_date',
            );
        } else {
            require_once("custom/include/_helper/junior_class_utils.php");
            $res = json_decode(getListAttendanceStudent($args['class_id'], $args['lesson_date']));
            if ($res->success == '0') {
                return array(
                    'success' => false,
                    'error' => 'No session in' . $args['lesson_date'],
                );
            } else {
                return array(
                    'success' => true,
                    'data' => (array)$res->array_student,
                );
            }
        }
    }

    function saveAttendance(ServiceBase $api, array $args)
    {
        require_once("custom/include/_helper/junior_class_utils.php");
        $confim = false;//biến xác nhận vào vòng lặp
        $data['session_id'] = $args['session_id'];
        $data['class_id'] = $args['class_id'];
        $data['student_id'] = $args['student_id'];
        $data['attend_id'] = $args['attend_id'];
        foreach ($args['field'] as $field) {
            $confim = true;
            $data['savePos'] = $field['field_name'];
            $data['saveVal'] = $field['field_val'];
            $res = json_decode(saveAttendanceUtils($data));
            if ($res->success == '0') {
                return array(
                    'success' => false,
                    'error' => 'Invalid param',
                );
            } else if ($res->success == '2') {
                return array(
                    'success' => false,
                    'error' => 'Invalid field_val of loyalty_point',
                );
            }
        }

        if (!$confim)
            return array(
                'success' => false,
                'error' => 'Invalid param',
            );

        return array(
            'success' => true,
            'data' => $data['attend_id'],
        );
    }

    function saveListAttendance(ServiceBase $api, array $args)
    {
        require_once("custom/include/_helper/junior_class_utils.php");
        $confim = false;//biến xác nhận vào vòng lặp
        $attendance_error = array();
        $data['session_id'] = $args['session_id'];
        $data['class_id'] = $args['class_id'];
        foreach ($args['attendance'] as $attendance) {
            $data['student_id'] = $attendance['student_id'];
            $data['attend_id'] = $attendance['attend_id'];
            foreach ($attendance['field'] as $field) {
                $confim = true;
                $data['savePos'] = $field['field_name'];
                $data['saveVal'] = $field['field_val'];
                $res = json_decode(saveAttendanceUtils($data));
                if (($res->success == '0' || $res->success == '2') && !in_array($data['attend_id'], $attendance_error)) {
                    $attendance_error[] = $data['attend_id'];
                }
            }
        }
        if (!$confim) {
            return array(
                'success' => false,
                'error' => 'Invalid param',
            );
        }
        if (count($attendance_error) > 0) {
            return array(
                'success' => false,
                'error' => 'Invalid attendance param',
                'data' => $attendance_error,
            );
        }
        return array(
            'success' => true,
            'data' => $data['class_id'],
        );
    }

    function sendAttendance(ServiceBase $api, array $args)
    {
        require_once('custom/include/_helper/junior_class_utils.php');
        $res = json_decode(attendenceSendApp($args));
        if ($res->status == 'RECEIVED') {
            return array(
                'success' => true,
            );
        } else if ($res->status == 'SENT_FAILED') {
            return array(
                'success' => false,
                'error' => 'Sent failed',
            );
        }
        return array(
            'success' => false,
            'error' => 'Invalid param',
        );
    }

    function saveGallery(ServiceBase $api, $args)
    {
        if (!empty($args)) {
            $id = $args['id'];
            if ($id != '') {
                $bean = BeanFactory::getBean('C_Gallery', $id);
            } else {
                $bean = BeanFactory::getBean('C_Gallery');
            }
        } else return array(
            'success' => false,
            'message' => '',
        );

        unset($args['id']);

        //delete gallery
        if ($args['deleted'] == 1) {
            $bean->mark_deleted($bean->id);
            return array(
                'success' => true,
                'bean_id' => $bean->id,
                'message' => 'Delete success',
            );
        }

        $message = '';
        $args['list_pic_id'] = array();
        if (!empty($_FILES['gallery_picture'])) {
            $count_picture = count($_FILES['gallery_picture']['name']);
            if ($count_picture > 0)
                for ($i = 0; $i < $count_picture; $i++) {
                    $beanNotes = BeanFactory::getBean('Notes');
                    $beanNotes->name = $_FILES['gallery_picture']['name'][$i];
                    $beanNotes->file_mime_type = $_FILES['gallery_picture']['type'][$i];
                    $beanNotes->file_ext = pathinfo($_FILES['gallery_picture']['name'][$i], PATHINFO_EXTENSION);
                    $beanNotes->file_size = $_FILES['gallery_picture']['size'][$i];
                    $beanNotes->filename = $_FILES['gallery_picture']['name'][$i];
                    $beanNotes->parent_type = 'C_Gallery';
                    $beanNotes->parent_id = $bean->id;
                    $beanNotes->save();
                    $savefile = 'upload/' . $beanNotes->id;
                    move_uploaded_file($_FILES['gallery_picture']['tmp_name'][$i], $savefile);
                    $args['list_pic_id'][] = $beanNotes->id;
                }
            else $message = 'No new images are added';
        } else $message = 'No new images are added';

        //delete picture in list remove (delete notes)
        if (!empty($args['list_pic_remove_id'])) {
            $noteIds = implode("','", $args['list_pic_remove_id']);
            $qDeletePicture = "UPDATE notes SET deleted=1 WHERE parent_type='C_Gallery' AND id IN('$noteIds') AND deleted=0";
            $GLOBALS['db']->query($qDeletePicture);
        }

        //save
        foreach ($args as $field => $Val)
            $bean->$field = $Val;
        $bean->save();

        //save tags
        // Getting Tag Field ID
        $tagField = $bean->getTagField();
        // Getting Tag Field Properties
        $tagFieldProperties = $bean->field_defs[$tagField];

        // Preparing the latest Tags to be sync with the record
        // Note: Already attached tags will be automatically removed from the record
        // If you want to keep some of the existing tags then you will need to keep them in the array
        $tags['tag'] = $args['tags'];
        // Building DotbFieldTag instance
        $DotbFieldTag = new DotbFieldTag('tag');
        // Passing the arguments to save the Tags
        $DotbFieldTag->apiSave($bean, $tags, $tagField, $tagFieldProperties);

        return array(
            'success' => true,
            'bean_id' => $bean->id,
            'message' => $message,
        );
    }

    function editStudent(ServiceBase $api, $args)
    {
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');

        $id = $args['id'];
        $bean = BeanFactory::getBean('Contacts', $id);
        unset($args['id']);

        $name = create_guid();

        if (!is_dir('upload/origin')) {
            mkdir('upload/origin');
        }
        if (!is_dir('upload/resize')) {
            mkdir('upload/resize');
        }
        $file1 = 'upload/origin/' . $name;
        $file2 = 'upload/resize/' . $name;

        move_uploaded_file($_FILES['picture']['tmp_name'], $file1);

        file_put_contents($file2, file_get_contents($file1));

        $imgOri1 = new ImageHelper($file1);
        $imgOri1->resize(500);
        $imgOri1->save($file1);

        $imgOri2 = new ImageHelper($file2);
        $imgOri2->resize(220, 220);
        $imgOri2->save($file2);

        if ($args['type'] == 'avatar') {
            $q1 = "UPDATE contacts
            SET contacts.picture = '$name' WHERE contacts.id = '$id'";
            $GLOBALS['db']->query($q1);
        } else {
            $q1 = "UPDATE contacts
            SET contacts.cover_app = '$name' WHERE contacts.id = '$id'";
            $GLOBALS['db']->query($q1);
        }

        return array(
            'success' => true,
            'bean_id' => $bean->id,
        );
    }

    function editTeacher(ServiceBase $api, $args)
    {
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');

        $id = $args['id'];
        $bean = BeanFactory::getBean('C_Teachers', $id);
        $name = create_guid();

        if (!is_dir('upload/origin')) {
            mkdir('upload/origin');
        }
        if (!is_dir('upload/resize')) {
            mkdir('upload/resize');
        }
        $file1 = 'upload/origin/' . $name;
        $file2 = 'upload/resize/' . $name;

        move_uploaded_file($_FILES['picture']['tmp_name'], $file1);

        file_put_contents($file2, file_get_contents($file1));

        $imgOri1 = new ImageHelper($file1);
        $imgOri1->resize(500);
        $imgOri1->save($file1);

        $imgOri2 = new ImageHelper($file2);
        $imgOri2->resize(220, 220);
        $imgOri2->save($file2);

        if ($args['type'] == 'avatar') {
            $q1 = "UPDATE c_teachers
            SET c_teachers.picture = '$name' WHERE c_teachers.id = '$id'";
            $GLOBALS['db']->query($q1);
        } else {
            $q1 = "UPDATE c_teachers
            SET c_teachers.cover_app = '$name' WHERE c_teachers.id = '$id'";
            $GLOBALS['db']->query($q1);
        }

        return array(
            'success' => true,
            'bean_id' => $bean->id,
        );
    }

    function getTeachingHour(ServiceBase $api, $args)
    {
        global $timedate;
        if (empty($args['begin'])) {
            $now_day = date($timedate->get_db_date_format(), strtotime($timedate->nowDb() . '+7 hours'));
            $begin = $timedate->convert_to_gmt_datetime(date('Y-m-01', strtotime($now_day)));
            $end = $timedate->convert_to_gmt_datetime(date('Y-m-01', strtotime($now_day . '+1 months')));
        } else {
            $begin = $timedate->convert_to_gmt_datetime($args['begin']);
            $end = $timedate->convert_to_gmt_datetime($args['end']);
        }
        $teacher = array();
        include_once("custom/include/_helper/junior_class_utils.php");
        $total_hours = getTeachingHour($begin, $end, $args['teacher_id'], '')[0];
        $sum_late_format = floor($total_hours['late_time'] / 3600) . ":" . floor($total_hours['late_time'] / 60 % 60) . ":" . floor($total_hours['late_time'] % 60);
        $teacher['total_hous'] = (double)$total_hours['max_hour'];
        $teacher['teaching_hours'] = (double)$total_hours['teaching_hour'];
        $teacher['teaching_late_time'] = $sum_late_format;
        $teacher['total_session'] = (double)$total_hours['total_session'];
        $teacher['teaching_session'] = (double)$total_hours['teaching_session'];
        $teacher['teaching_hours_by_class'] = getTeachingHour($begin, $end, $args['teacher_id'], 'class');
        return array(
            'success' => true,
            'data' => $teacher,
        );
    }

    function getLearningHour(ServiceBase $api, $args)
    {
        $d = date("Y-m-d", strtotime($args['begin']) - 86400);
        $s1 = "SELECT DISTINCT IFNULL(c_teachers.type, '')                      c_teachers_type,
                IFNULL(c_teachers.hr_code, '')                   c_teachers_hr_code,
                IFNULL(c_teachers.full_teacher_name, '')         C_TEACHERS_FULL_TEACHE77CDF3,
                IFNULL(l1.teaching_type, '')                     l1_teaching_type,
                IFNULL(l2.class_code, '')                        l2_class_code,
                IFNULL(l2.id, '')                        l2_class_id,
                IFNULL(l2.name, '')                              l2_name,
                IFNULL(l2.kind_of_course, '')                    l2_kind_of_course,
                IFNULL(l2.level, '')                             l2_level,
                GROUP_CONCAT(l1.description)                     l1_location,
                COUNT(l1.id)                                     l1__allcount,
                COUNT(DISTINCT l1.id)                            l1__count,
                IFNULL(l2.team_id, '')                             l2_team_id,
                IFNULL(l3.name, '')                             l3_team_name,
                COUNT(c_teachers.id)                             c_teachers__allcount,
                SUM(l4.count_att)                                c_teachers__count,
                sum(IFNULL(l1.teaching_hour, 0))                 l1_sum_teaching_hour,
                sum(IFNULL(l1.duration_hours * l4.count_att, 0)) l1_sum_duration_cal
FROM c_teachers
         INNER JOIN meetings l1 ON c_teachers.id = l1.teacher_id AND l1.deleted = 0
         INNER JOIN j_class l2 ON l1.ju_class_id = l2.id AND l2.deleted = 0
         INNER JOIN teams l3 ON l2.team_id = l3.id AND l3.deleted = 0
         LEFT JOIN (SELECT mt.id att_meeting_id, COUNT(att.id) count_att
                    FROM c_attendance att
                             INNER JOIN meetings mt ON att.meeting_id = mt.id
                        AND mt.deleted = 0
                        AND (DATE_FORMAT(mt.date_start + INTERVAL 420 MINUTE, '%Y-%m-%d') >= '{$args['begin']}'
                            AND DATE_FORMAT(mt.date_end - INTERVAL 1020 MINUTE, '%Y-%m-%d') <= '{$args['end']}')
                    WHERE att.attendance_type IN ('P', 'L')
                      AND att.deleted = 0
                    GROUP BY att_meeting_id) l4 ON l4.att_meeting_id = l1.id
WHERE c_teachers.id = '{$args['teacher_id']}'
  AND l1.date_start >= '$d 17:00:00'
  AND l1.date_start <= '{$args['end']} 16:59:59'
  AND (IFNULL(l1.session_status, '') <> 'Cancelled' OR (IFNULL(l1.session_status, '') IS NULL))
  AND c_teachers.deleted = 0
GROUP BY IFNULL(c_teachers.type, ''),
         c_teachers.hr_code,
         c_teachers.full_teacher_name,
         l2.class_code,
         l2.name,
         IFNULL(l2.kind_of_course, ''),
         l2.level";

        $s2 = "SELECT DISTINCT l4.count_att                               c_teachers__count,
                                    IFNULL(l1.teaching_hour, 0)             l1_teaching_hour,
                                    IFNULL(l1.duration_hours * l4.count_att, 0) l1_duration_cal,
                                    IFNULL(l1.name, '') l1_name,
                                    IFNULL(l1.date_start, '') l1_date_start,
                                    IFNULL(l1.date_end, '') l1_date_end,
                                    IFNULL(l1.date, '') l1_date,
                                    IFNULL(l2.id, '') class_id,
                                    IFNULL(l2.class_code, '') class_code
                    FROM c_teachers
                             INNER JOIN meetings l1 ON c_teachers.id = l1.teacher_id AND l1.deleted = 0
                             INNER JOIN j_class l2 ON l1.ju_class_id = l2.id AND l2.deleted = 0
                             INNER JOIN teams l3 ON l2.team_id = l3.id AND l3.deleted = 0
                             LEFT JOIN (SELECT mt.id att_meeting_id, COUNT(att.id) count_att
                                        FROM c_attendance att
                                                 INNER JOIN meetings mt ON att.meeting_id = mt.id
                                            AND mt.deleted = 0
                                            AND (DATE_FORMAT(mt.date_start + INTERVAL 420 MINUTE, '%Y-%m-%d') >= '{$args['begin']}'
                                                AND DATE_FORMAT(mt.date_end - INTERVAL 1020 MINUTE, '%Y-%m-%d') <= '{$args['end']}')
                                        WHERE att.attendance_type IN ('P', 'L')
                                          AND att.deleted = 0
                                        GROUP BY att_meeting_id) l4 ON l4.att_meeting_id = l1.id
                    WHERE c_teachers.id = '{$args['teacher_id']}'
                      AND l1.date_start >= '$d 17:00:00'
                      AND l1.date_start <= '{$args['end']} 16:59:59'
                      AND (IFNULL(l1.session_status, '') <> 'Cancelled' OR (IFNULL(l1.session_status, '') IS NULL))
                      AND c_teachers.deleted = 0
                    order by l1.date_start";

        global $db;
        $r1 = $db->query($s1);
        $r2 = $db->query($s2);
        $data = array('r1' => array(), 'r2' => array());
        while ($row = $db->fetchByAssoc($r1)) $data['r1'][] = $row;
        while ($row = $db->fetchByAssoc($r2)) $data['r2'][] = $row;
        return array(
            'success' => 1,
            'data' => $data
        );
    }
}
