<?php


class JaxtinaApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'student-get-info' => array(
                'reqType' => 'POST',
                'path' => array('v1', 'students', 'get_info'),
                'pathVars' => array(''),
                'method' => 'getInfo',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }

    function getInfo(ServiceBase $api, $args){
        //Set current User
        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        global $timedate, $current_user, $app_list_strings;
        if (isset($args) && !empty($args) && (!empty($args['email']) || !empty($args['phone_mobile'])) ) {
            $now          = $timedate->nowDb();

            $email = $args['email'];
            if(!empty($email)) $ext_ .= " (l1.email_address = '$email') OR (l1.email_address_caps = '".mb_strtoupper($email, 'UTF-8')."') ";
            $phone_mobile = $args['phone_mobile'];
            if(!empty($phone_mobile)) $ext_ .= " OR (contacts.phone_mobile = '$phone_mobile') ";
            if(substr($ext_,0,3) == ' OR') $ext_ = str_replace(' OR', '', $ext_);
            $password     = $args['password'];
            $module       = $args['module']; //class_list
            $res          = array();

            //Check Username credential
            $q1 = "SELECT DISTINCT IFNULL(contacts.id, '') primaryid,
            IFNULL(contacts.first_name, '') first_name,
            IFNULL(contacts.last_name, '') last_name,
            IFNULL(contacts.full_student_name, '') name,
            IFNULL(contacts.contact_id, '') student_id,
            contacts.birthdate birthdate,
            IFNULL(contacts.status, '') status,
            IFNULL(contacts.phone_mobile, '') phone_mobile,
            IFNULL(contacts.picture, '') picture,
            IFNULL(l1.email_address, '') email,
            IFNULL(contacts.nick_name, '') nick_name,
            IFNULL(contacts.guardian_name, '') guardian_name,
            IFNULL(contacts.relationship, '') relationship,
            IFNULL(contacts.lead_source, '') lead_source,
            IFNULL(contacts.utm_source, '') utm_source,
            IFNULL(contacts.gender, '') gender,
            IFNULL(contacts.facebook, '') facebook,
            IFNULL(contacts.primary_address_street, '') address,
            IFNULL(contacts.primary_address_postalcode, '') address_postalcode,
            IFNULL(contacts.primary_address_state, '') address_state,
            IFNULL(contacts.primary_address_city, '') address_city,
            IFNULL(contacts.primary_address_country, '') address_country,
            IFNULL(contacts.assigned_user_id, '') assigned_to_user_id,
            IFNULL(contacts.team_id, '') team_id,
            IFNULL(l2.name, '') team_name,
            IFNULL(contacts.team_set_id, '') team_set_id,
            IFNULL(contacts.portal_name, '') portal_name,
            IFNULL(contacts.portal_password, '') portal_password,
            IFNULL(contacts.phone_guardian, '') phone_guardian,
            IFNULL(contacts.guardian_name_2, '') guardian_name_2,
            IFNULL(contacts.relationship2, '') relationship2,
            IFNULL(contacts.other_mobile, '') phone_guardian_2
            FROM contacts
            INNER JOIN email_addr_bean_rel l1_1 ON contacts.id = l1_1.bean_id AND l1_1.deleted = 0 AND l1_1.bean_module = 'Contacts' AND l1_1.primary_address = 1
            INNER JOIN email_addresses l1 ON l1.id = l1_1.email_address_id AND l1.deleted = 0
            LEFT JOIN teams l2 ON contacts.team_id = l2.id AND l2.deleted = 0
            WHERE contacts.deleted = 0 AND ($ext_)
            AND contacts.deleted = 0
            ORDER BY first_name ASC";
            $students = $GLOBALS['db']->fetchArray($q1);
            if (count($students) == 0)
                return array(
                    'success' => false,
                    'error' => "invalid_username");

            $student_list = array();
            $studentIds = array_column($students, 'primaryid');
            foreach ($students as $student) {
                //add student array
                $student['birthdate'] = $timedate->to_display_date($student['birthdate'], false);//format birthdate
                $student_list[$student['primaryid']] = $student;



                if (empty($module) || $module == 'class_list')
                    $student_list[$student['primaryid']]['class_list'] = array();

            }
            $str_student_id = implode("','", array_column($students, 'primaryid'));
            //get Student ID login and push array
            $arrStudentId = array_column($students, 'primaryid');

            //get school
            $qgetschool = "SELECT DISTINCT
            IFNULL(contacts.id, '') student_id,
            IFNULL(l0.name, '') school_name
            FROM contacts
            INNER JOIN j_school l0 ON contacts.school_id = l0.id AND l0.deleted = 0
            WHERE (contacts.id IN ('$str_student_id')) AND contacts.deleted = 0";
            $getschoolstudent = $GLOBALS['db']->query($qgetschool);
            while ($res = $GLOBALS['db']->fetchbyAssoc($getschoolstudent)) {
                $student_list[$res['student_id']]['school_name'] = $res['school_name'];
            }

            if (empty($module) || $module == 'class_list') {
                require_once('include/externalAPI/ClassIn/ExtAPIClassIn.php');
                $ExtApiClassIn  = new ExtApiClassIn();
                $schoolId       = $ExtApiClassIn->_SID;
                $AppUrl         = $ExtApiClassIn->_AppUrl;
                //Get Current Class
                $nowDb = $timedate->nowDb();
                $q2 = "SELECT DISTINCT
                IFNULL(contacts.id, '') student_id,
                IFNULL(l3.id, '') class_id,
                IFNULL(l3.picture, '') picture,
                IFNULL(l3.name, '') class_name,
                IFNULL(l3.class_code, '') class_code,
                l3.start_date start_date,
                l3.end_date end_date,
                l3.date_entered date_entered,
                IFNULL(l3.kind_of_course, '') kind_of_course,
                IFNULL(l3.level, '') level,
                CONCAT(IFNULL(l3.kind_of_course, ''), ' ', IFNULL(l3.level, '')) koc_level,
                IFNULL(l3.modules, '') modules,
                l3.hours hours,
                IFNULL(l3.status, '') class_status,
                IFNULL(l2.lesson_number, '') ss_lesson,
                IFNULL(l4.id, '') assigned_to_id,
                IFNULL(l4.full_user_name, '') assigned_to,
                IFNULL(l5.id, '') koc_id,
                IFNULL(l5.name, '') koc_name,
                IFNULL(l6.id, '') room_id,
                IFNULL(l6.name, '') room_name,
                IFNULL(l7.id, '') teacher_id,
                IFNULL(l7.full_teacher_name, '') teacher_name,
                IFNULL(l3.max_size, '') max_size,
                IFNULL(l8.id, '') team_id,
                IFNULL(l8.name, '') team_name,
                IFNULL(l8.code_prefix, '') team_code,
                IFNULL(l2.id, '') ss_id,
                IFNULL(l2.name, '') ss_name,
                IFNULL(l2.week_no, '') ss_week_no,
                IFNULL(l2.week_date, '') ss_week_date,
                IFNULL(l2.observe_score, 0) ss_observe_score,
                IFNULL(l2.syllabus_custom, '') ss_lesson_note,
                IFNULL(l2.syllabus_custom, '') ss_syllabus_custom,
                IFNULL(l2.topic_custom, '') ss_topic_custom,
                l2.till_hour ss_till_hour,
                l2.date_start ss_date_start,
                l2.date_end ss_date_end,
                l2.duration_cal ss_duration_cal,
                IFNULL(l9.id, '') l9_id,
                IFNULL(l9.full_teacher_name, '') ss_ta_name,
                IFNULL(l10.id, '') l10_id,
                IFNULL(l10.full_teacher_name, '') ss_teacher_name,
                IFNULL(l11.id, '') l11_id,
                IFNULL(l11.name, '') ss_room_name,
                IFNULL(l2.type, '') ss_learning_type,
                IFNULL(l14.status, '') status_in_class,
                l14.start_study start_study,
                l14.end_study end_study,
                (CASE WHEN DATE_SUB(l2.date_end, INTERVAL 1 HOUR) < '$nowDb' THEN ''
                WHEN l2.type = 'ClassIn' THEN (CONCAT('$AppUrl','enterclass?telephone=',IFNULL(contacts.phone_prefix,'84'),'-',SUBSTR(contacts.phone_mobile,2),'&classId=',l2.external_id,'&courseId=',l3.onl_course_id,'&schoolId=$schoolId'))
                ELSE IFNULL(l2.join_url, '')
                END) ss_displayed_url
                FROM contacts
                INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0
                INNER JOIN meetings_contacts l2_1 ON l1.id = l2_1.situation_id AND l2_1.deleted = 0 AND contacts.id = l2_1.contact_id
                INNER JOIN meetings l2 ON l2.id = l2_1.meeting_id AND l2.deleted = 0
                INNER JOIN j_class l3 ON l2.ju_class_id = l3.id AND l3.deleted = 0
                LEFT JOIN users l4 ON l3.assigned_user_id = l4.id AND l4.deleted = 0
                LEFT JOIN j_kindofcourse l5 ON l3.koc_id = l5.id AND l5.deleted = 0
                LEFT JOIN c_rooms l6 ON l3.room_id = l6.id AND l6.deleted = 0
                LEFT JOIN c_teachers l7 ON l3.teacher_id = l7.id AND l7.deleted = 0
                LEFT JOIN teams l8 ON l3.team_id = l8.id AND l8.deleted = 0
                LEFT JOIN c_teachers l9 ON l2.teacher_cover_id=l9.id AND l9.deleted=0
                LEFT JOIN c_teachers l10 ON l2.teacher_id=l10.id AND l10.deleted=0
                LEFT JOIN c_rooms l11 ON l2.room_id=l11.id AND l11.deleted=0
				LEFT JOIN j_classstudents l14 ON l14.class_id = l3.id AND l14.student_id = contacts.id AND l14.deleted = 0
                WHERE (((contacts.id IN ('$str_student_id'))
                AND (IFNULL(l2.meeting_type, '') = 'Session')
                AND (IFNULL(l2.session_status, '') <> 'Cancelled' OR (IFNULL(l2.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
                AND (IFNULL(l1.type, '') IN ('Enrolled' , 'OutStanding', 'Demo'))))
                AND contacts.deleted = 0
                ORDER BY student_id, CASE";
                $types = $GLOBALS['app_list_strings']['status_class_list'];
                $count_k = 1;
                foreach ($types as $_type => $val_)
                    $q2 .= " WHEN class_status = '$_type' THEN " . $count_k++;
                $q2 .= " ELSE $count_k END ASC,l3.end_date DESC, class_id, l2.date_start";
                $rowS = $GLOBALS['db']->fetchArray($q2);
                //Get Kind Of Course Syllabus - For best perfomance
                $classIds = implode("','", array_unique(array_column($rowS, 'class_id')));
                $koc = array();
                $q2_1 = "SELECT DISTINCT
                IFNULL(l1.id, '') koc_id,
                IFNULL(l1.name, '') koc_name,
                IFNULL(l1.kind_of_course, '') kind_of_course,
                IFNULL(l1.syllabus, '') syllabus,
                IFNULL(l1.content, '') content,
                IFNULL(l1.lms_content, '') lms_content
                FROM j_class INNER JOIN j_kindofcourse l1 ON j_class.koc_id = l1.id  AND l1.deleted = 0
                WHERE (j_class.id IN ('$classIds')) AND j_class.deleted = 0";
                $rowK = $GLOBALS['db']->fetchArray($q2_1);
                foreach ($rowK as $key => $rK) {
                    $koc[$rK['koc_id']]['koc_id'] = $rK['koc_id'];
                    $koc[$rK['koc_id']]['koc_name'] = $rK['koc_name'];
                    $koc[$rK['koc_id']]['kind_of_course'] = $rK['kind_of_course'];
                    $koc[$rK['koc_id']]['syllabus'] = ((!empty($rK['syllabus'])) ? json_decode(html_entity_decode($rK['syllabus']), true) : '');
                    $koc[$rK['koc_id']]['content'] = ((!empty($rK['content'])) ? json_decode(html_entity_decode($rK['content']), true) : '');

                }
                $classes = array();

                $arrClassFinish = array();
                $arrResultStudent = array();

                $runStudent = '####';
                $runClass = '####';
                //List Class ID
                $classIds = array();
                foreach ($rowS as $_k2 => $r2) {
                    if ($runStudent != $r2['student_id'] || $runClass != $r2['class_id']) {
                        //Đã tham gia
                        $attended_session = 0;

                        $ss_studied = 0;
                        $ss_count = 0;
                        $runStudent = $r2['student_id'];
                        $runClass = $r2['class_id'];

                        $classes[$runStudent][$runClass]['id'] = $r2['class_id'];
                        $classes[$runStudent][$runClass]['class_name'] = $r2['class_name'];
                        $classes[$runStudent][$runClass]['picture'] = $r2['picture'];
                        $classes[$runStudent][$runClass]['class_code'] = $r2['class_code'];
                        $classes[$runStudent][$runClass]['start_date'] = $timedate->to_display_date($r2['start_date'], false);
                        $classes[$runStudent][$runClass]['end_date'] = $timedate->to_display_date($r2['end_date'], false);
                        $classes[$runStudent][$runClass]['date_entered'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['date_entered']));
                        $classes[$runStudent][$runClass]['kind_of_course'] = $r2['kind_of_course'];
                        $classes[$runStudent][$runClass]['level'] = $r2['level'];
                        $classes[$runStudent][$runClass]['hours'] = $r2['hours'];
                        $classes[$runStudent][$runClass]['assigned_to'] = $r2['assigned_to'];
                        $classes[$runStudent][$runClass]['koc_id'] = $r2['koc_id'];
                        $classes[$runStudent][$runClass]['koc_name'] = $r2['koc_name'];
                        $classes[$runStudent][$runClass]['room_name'] = $r2['room_name'];
                        $classes[$runStudent][$runClass]['teacher_name'] = $r2['teacher_name'];
                        $classes[$runStudent][$runClass]['max_size'] = $r2['max_size'];
                        $classes[$runStudent][$runClass]['team_name'] = $r2['team_name'];
                        $classes[$runStudent][$runClass]['team_code'] = $r2['team_code'];

                        $classes[$runStudent][$runClass]['status'] = $r2['class_status'];
                        $classes[$runStudent][$runClass]['status_in_class'] = $r2['status_in_class'];
                        $classes[$runStudent][$runClass]['start_study'] = $r2['start_study'];
                        $classes[$runStudent][$runClass]['end_study'] = $r2['end_study'];

                        //Add new average rating by nnamnguyen
                        $count_session_is_rated = 0;
                        $rating_score = 0;
                        $classes[$runStudent][$runClass]['average_rating'] = '';

                        $content = $koc[$r2['koc_id']]['content'];
                        foreach ($content as $ct) {
                            if ($ct['levels'] == $r2['level'])
                                $docs = $ct['doc_url'];
                        }
                        $classes[$runStudent][$runClass]['doc_url'] = $docs;
                        $classes[$runStudent][$runClass]['sessions'] = array();

                    }

                    //nếu thời gian hiện tại nằm trong thòi gian student situation thì lấy trạng thái lớp là trạng thái lớp hiện tại
                    if ($r2['class_status'] != 'Finished' && $classes[$runStudent][$runClass]['status'] != $r2['class_status']) {
                        $classes[$runStudent][$runClass]['status'] = $r2['class_status'];
                    }

                    $session = array();
                    $session['ss_id'] = $r2['ss_id'];
                    $session['ss_name'] = $r2['ss_name'];
                    $session['ss_lesson'] = $r2['ss_lesson'];
                    $session['ss_week_no'] = $r2['ss_week_no'];
                    $session['ss_week_date'] = date('l', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_till_hour'] = $r2['ss_till_hour'];
                    $session['ss_db_date_start'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_db_date_end'] = date('Y-m-d H:i:s', strtotime("+7 hours " . $r2['ss_date_end']));
                    $session['ss_date_start'] = $timedate->to_display_date_time($r2['ss_date_start']);
                    $session['ss_date_end'] = $timedate->to_display_date_time($r2['ss_date_end']);
                    $session['ss_db_date'] = date('Y-m-d', strtotime("+7 hours " . $r2['ss_date_start']));
                    $session['ss_learning_type'] = $r2['ss_learning_type'];
                    $session['ss_displayed_url'] = $r2['ss_displayed_url'];
                    if ($now < $r2['ss_date_start']) $session['ss_status'] = "not_stated";
                    elseif ($now >= $r2['ss_date_start'] && $now <= $r2['ss_date_end']) $session['ss_status'] = "in_progress";
                    elseif ($now > $r2['ss_date_end']) {
                        $session['ss_status'] = "finished";
                        $ss_studied++;
                    }

                    $session['ss_duration_cal'] = $r2['ss_duration_cal'];
                    $session['ss_ta_name'] = $r2['ss_ta_name'];
                    $session['ss_teacher_name'] = $r2['ss_teacher_name'];
                    $session['ss_room_name'] = $r2['ss_room_name'];
                    $session['ss_observe_score'] = $r2['ss_observe_score'];
                    $session['ss_lesson_note'] = $r2['ss_lesson_note'];
                    $session['ss_syllabus_custom'] = $r2['ss_syllabus_custom'];
                    $session['ss_topic_custom'] = $r2['ss_topic_custom'];

                    //get Syllabus
                    $syll = $koc[$r2['koc_id']]['syllabus'][$r2['level']][$r2['ss_lesson']];
                    if (!empty($syll)) {
                        $session['syllabus_topic'] = $syll['theme'];
                        $session['syllabus_activities'] = $syll['content'];
                        $session['syllabus_homework'] = $syll['homework'];
                    } else {
                        $session['syllabus_topic'] = '';
                        $session['syllabus_activities'] = '';
                        $session['syllabus_homework'] = '';
                    }

                    $ss_count++;
                    $classes[$runStudent][$runClass]['sessions'][] = $session;
                    $classes[$runStudent][$runClass]['count_studied'] = $ss_studied;
                    $classes[$runStudent][$runClass]['count_session'] = $ss_count;

                    if (!in_array($runClass, $classIds))
                        $classIds[] = $runClass;
                }


                // Get total session of class
                $qSession = "SELECT DISTINCT
                IFNULL(c.id, '') class_id,
                COUNT(l1.id) total_session,
                IFNULL(l3.id,'') student_id
                FROM j_class c
                INNER JOIN meetings l1 ON l1.ju_class_id = c.id AND l1.deleted = 0
                INNER JOIN  j_classstudents l2 ON c.id = l2.class_id AND l2.deleted=0
                INNER JOIN  contacts l3 ON l3.id = l2.student_id AND l3.deleted=0
                WHERE (c.id IN ('" . implode("','", $classIds) . "'))
                AND (l3.id IN ('$str_student_id'))
                AND (IFNULL(l1.meeting_type, '') = 'Session')
                AND (IFNULL(l1.session_status, '') <> 'Cancelled' OR (IFNULL(l1.session_status, '') IS NULL AND 'Cancelled' IS NOT NULL))
                GROUP BY l3.id, c.id";
                $rSession = $GLOBALS['db']->query($qSession);
                while ($row = $GLOBALS['db']->fetchbyAssoc($rSession)) {
                    $classes[$row['student_id']][$row['class_id']]['total_session'] = $row['total_session'];
                }

                //Assign to result
                foreach ($student_list as $stid => $student)
                    $student_list[$stid]['class_list'] = array_values($classes[$stid]);

                //Return
                return array(
                    'success'      => true,
                    'email'        => $email,
                    'phone_mobile' => $phone_mobile,
                    'student_list' => array_values($student_list),
                );
            }else return array(
                'success' => false,
                'error'   => "sending_failed."
                );
        }
    }

}
