<?php

class FeedbackMobile extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'feedbackmobile_listcomment' => array(
                'reqType' => 'POST',
                'path' => array('appchat-message', 'list-comments'),
                'pathVars' => array(),
                'method' => 'listComments',
                'noLoginRequired' => true,
            ),
            'feedbackmobile_savecomment' => array(
                'reqType' => 'POST',
                'path' => array('appchat-message', 'save-comment'),
                'pathVars' => array(),
                'method' => 'saveComment',
                'noLoginRequired' => true,
            ),
            'feedbackmobile_savefeedback' => array(
                'reqType' => 'POST',
                'path' => array('appchat-message', 'save-feedback'),
                'pathVars' => array(),
                'method' => 'saveFeedback',
                'noLoginRequired' => true,
            ),
            'feedbackmobile_feedbackdetail' => array(
                'reqType' => 'POST',
                'path' => array('appchat-message', 'feedback-detail'),
                'pathVars' => array(),
                'method' => 'feedbackDetail',
                'noLoginRequired' => true,
            ),
        );
    }

    /**
     *
     * @param array $args = data,case_id
     *  'path' => array('chat-message', 'save-comment'),
     */
    function saveComment(ServiceBase $api, array $args)
    {

        require_once 'include/utils/file_utils.php';

        global $timedate, $dotb_config;

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $bean = new C_Comments();
        $bean->id = create_guid();
        $bean->new_with_id = true;
        $bean->description = $args['data'];
        $bean->parent_id = $args['case_id'];
        $bean->parent_type = 'Cases';
        $bean->direction = 'inbound';

        if (!empty($args['attachment'])) {
            $file = base64_decode($args['attachment']['data']);
            $filesize = filesize($file);
            $idUp = $bean->id;
            if ($filesize <= $dotb_config['upload_maxsize']) {
                file_put_contents('upload/' . $bean->id, $file);
                $bean->filename = $args['attachment']['name'];
                $bean->document_name = $args['attachment']['name'];

                $extension = get_file_extension($bean->filename);
                if (!empty($extension)) {
                    $bean->file_ext = $extension;
                    $bean->file_mime_type = get_mime_content_type_from_filename($bean->filename);
                }
                $logUpload = 'uploaded';
            } else {
                $logUpload = 'limited_filesize';
                $idUp = $bean->id;
                $bean->filename = '';
                $bean->file_ext = '';
                $bean->file_mime_type = '';
            }
        }

        $bean->save();


        return array(
            'success' => 1,
            'data' => array(
                'direction' => $bean->direction,
                'description' => $bean->description,
                'date_entered' => $timedate->to_display_date_time($bean->date_entered),
                'create_by' => $args['api_user_id'],
                'attachment' => array(
                    'log_upload' => $logUpload,
                    'id' => $idUp,
                    'name' => $bean->filename,
                    'file_ext' => $bean->file_ext,
                    'file_mime_type' => $bean->file_mime_type,
                )
            )
        );
    }

    /**
     *
     * @param array $args = case_id
     * 'path' => array('chat-message', 'list-comments'),
     */
    function listComments(ServiceBase $api, array $args)
    {
        global $timedate, $db, $locale;
        $data = array();

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $r = $db->query("SELECT DISTINCT
                IFNULL(l1.id, '') case_id,
                IFNULL(c_comments.id, '') primaryid,
                IFNULL(c_comments.filename, '') filename,
                IFNULL(c_comments.description, '') description,
                IFNULL(c_comments.direction, '') direction,
                IFNULL(c_comments.file_ext, '') file_ext,
                IFNULL(c_comments.file_mime_type, '') file_mime_type,
                IFNULL(c_comments.is_read_inems, 0) is_read_inems,
                IFNULL(c_comments.is_read_inapp, 0) is_read_inapp,
                c_comments.date_entered date_entered,
                IFNULL(l2.id, '') created_by,
                IFNULL(l2.full_user_name, '') created_by_name,
                IFNULL(l2.picture, '') created_by_picture
                FROM c_comments
                INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases' AND l1.deleted = 0
                LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
                WHERE (((l1.id = '{$args['case_id']}')))
                AND c_comments.deleted = 0
                ORDER BY c_comments.date_entered ASC");
        while ($row = $db->fetchByAssoc($r)) {
            $create_by = $row['user_id'];
            $data[] = array(
                'direction'          => $row['direction'],
                'description'        => $row['description'],
                'date_entered'       => $timedate->to_display_date_time($row['date_entered']),
                'date_entered_db'    => date('Y-m-d H:i:s', strtotime("+7 hours ".$row['date_entered'])),
                'created_by'         => $row['created_by'],
                'created_by_name'    => $row['created_by_name'],
                'created_by_picture' => $row['created_by_picture'],
                'is_read_inems'      => $row['is_read_inems'],
                'is_read_inapp'      => $row['is_read_inapp'],
                'attachment' => array(
                    'id' => $row['primaryid'],
                    'name' => $row['filename'],
                    'file_ext' => $row['file_ext'],
                    'file_mime_type' => $row['file_mime_type'],
                )
            );
        }

        return array('success' => 1, 'data' => $data);
    }

    function saveFeedback(ServiceBase $api, array $args)
    {

        require_once 'include/utils/file_utils.php';

        global $timedate;

        $message = 'uploaded';
        $args['list_id_image'] = array();
        //$cases_id = $args['case_id'];
        //$now = $timedate->nowDb();

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        if (!empty($args['message_id'])) {
            // Update message
            $bean = BeanFactory::getBean('C_Comments', $args['message_id']);
            $bean->description = $args['data'];
            $bean->is_updated = 1;

            // Remove old files
            if (!empty($_FILES['attachment'])) {
                $r1 = $GLOBALS['db']->query("SELECT DISTINCT IFNULL(c_uploadimage.id, '') id
                FROM c_uploadimage
                WHERE (c_uploadimage.comment_id = '{$args['message_id']}'AND c_uploadimage.deleted = 0)");
                while ($img = $GLOBALS['db']->fetchByAssoc($r1)) {
                    if (!empty($img['id'])) {
                        $images = BeanFactory::getBean('C_UploadImage', $img['id']);
                        $images->mark_deleted($img['id']);
                    }
                }
            }
        } else {
            // Create new message
            $bean = new C_Comments();
            $bean->id = create_guid();
            $bean->new_with_id = true;
            $bean->description = $args['data'];
            $bean->parent_id = $args['case_id'];
            $bean->parent_type = 'Cases';
            $bean->direction = 'inbound';
        }
        // Create new file
        if (!empty($_FILES['attachment'])) {
            $count_picture = count($_FILES['attachment']['name']);
            if ($count_picture > 0)
                for ($i = 0; $i < $count_picture; $i++) {
                    $beanImage = BeanFactory::getBean('C_UploadImage');
                    $beanImage->file_mime_type = $_FILES['attachment']['type'][$i];
                    $beanImage->file_ext = pathinfo($_FILES['attachment']['name'][$i], PATHINFO_EXTENSION);
//                        $beanImage->file_size = $_FILES['attachment']['size'][$i];
                    $beanImage->filename = $_FILES['attachment']['name'][$i];
                    $beanImage->document_name = $_FILES['attachment']['name'][$i];
                    $beanImage->comment_id = $bean->id;
                    $beanImage->save();
                    $savefile = 'upload/' . $beanImage->id;
                    move_uploaded_file($_FILES['attachment']['tmp_name'][$i], $savefile);
                    $args['list_id_image'][] = $beanImage->id;
                }
            else $message = 'list image null';
        } else $message = 'list image null';

        $bean->save();

        return array(
            'success' => 1,
            'data' => array(
                'direction' => $bean->direction,
                'description' => $bean->description,
                'date_entered' => $timedate->to_display_date_time($bean->date_entered),
                'create_by' => $args['api_user_id'],
                'attachment' => array(
                    'message' => $message,
                    'list_id_image' => $args['list_id_image'],
                )
            )
        );
    }

    function feedbackDetail(ServiceBase $api, array $args)
    {
        global $timedate, $db, $locale;
        $data = array();

        if (!isset($args['skip_seen']) || !$args['skip_seen']) {
            //Seen
            $GLOBALS['db']->query("UPDATE c_comments SET is_read_inapp=1 WHERE parent_id = '{$args['case_id']}' AND parent_type = 'Cases' AND deleted = 0");
        }

        $api_user = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = 'apps_admin'");
        if (empty($api_user)) $api_user = '1';
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', $api_user);

        $r = $db->query("SELECT DISTINCT
                IFNULL(l1.id, '') case_id,
                IFNULL(c_comments.id, '') primaryid,
                IFNULL(c_comments.description, '') description,
                IFNULL(c_comments.direction, '') direction,
                c_comments.date_entered date_entered,
                IFNULL(l2.id, '') user_id,
                IFNULL(l2.first_name, '') first_name,
                IFNULL(l2.last_name, '') last_name,
                IFNULL(l2.picture, '') avatar,
                IFNULL(c_comments.filename, '') filename,
                IFNULL(c_comments.file_ext, '') file_ext,
                IFNULL(c_comments.file_mime_type, '') file_mime_type,
                IFNULL(c_comments.is_read_inems, 0) is_read_inems,
                IFNULL(c_comments.is_read_inapp, 0) is_read_inapp,
                IFNULL(c_comments.is_updated, 0) is_updated
                FROM c_comments
                INNER JOIN cases l1 ON l1.id = c_comments.parent_id AND c_comments.parent_type = 'Cases'
                LEFT JOIN users l2 ON c_comments.created_by = l2.id AND l2.deleted = 0
                WHERE (((l1.id = '{$args['case_id']}')))
                AND c_comments.is_unsent = 0 AND c_comments.deleted = 0
                ORDER BY c_comments.date_entered ASC");
        while ($row = $db->fetchByAssoc($r)) {
            $create_by = $row['user_id'];
            if ($row['user_id'] == $api_user) $create_by = 'apps_admin';

            $images = array();
            if ($row['direction'] == 'inbound') {
                $r1 = $db->query("SELECT DISTINCT
                IFNULL(c_uploadimage.id, '') id,
                IFNULL(c_uploadimage.filename, '') filename,
                IFNULL(c_uploadimage.file_ext, '') file_ext,
                IFNULL(c_uploadimage.file_mime_type, '') file_mime_type
                FROM c_uploadimage
                WHERE (c_uploadimage.comment_id = '{$row['primaryid']}'AND c_uploadimage.deleted = 0)");
                while ($img = $db->fetchByAssoc($r1)) {
                    $images[] = array(
                        'id' => $img['id'],
                        'name' => $img['filename'],
                        'module' => 'c_uploadimage',
                    );
                }
            } else  {
                if ($row['filename'] != '')
                    $images[] = array(
                        'id' => $row['primaryid'],
                        'name' => $row['filename'],
                        'module' => 'c_comments',
                    );
            }

            $data[] = array(
                'id'                => $row['primaryid'],
                'direction'         => $row['direction'],
                'description'       => $row['description'],
                'date_entered'      => $timedate->to_display_date_time($row['date_entered']),
                'created_by'        => $create_by,
                'created_by_name'   => $locale->getLocaleFormattedName($row['first_name'], $row['last_name']),
                'created_by_avatar' => $row['avatar'],
                'images'            => $images,
                'is_read_inems'     => $row['is_read_inems'],
                'is_read_inapp'     => $row['is_read_inapp'],
                'is_updated'        => $row['is_updated'],
            );
        }

        return array('success' => 'true', 'data' => $data);
    }
}
