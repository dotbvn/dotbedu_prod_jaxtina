<?php


class TeaAPI extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'check_listParent' => array(
                'reqType' => 'POST',
                'path' => array('checkin-out', 'list-parent'),
                'pathVars' => array(),
                'method' => 'listParent',
                'noLoginRequired' => true,
            ),
            'check_listStudent' => array(
                'reqType' => 'POST',
                'path' => array('checkin-out', 'list-student'),
                'pathVars' => array(),
                'method' => 'listStudent',
                'noLoginRequired' => true,
            ),
        );
    }

    /**
     * @param array $args = student_id
     *  'path' => array('checkin-out', 'get-parent'),
     */
    function listParent(ServiceBase $api, array $args)
    {
        global  $timedate;

        $qStudent = "SELECT DISTINCT
                        IFNULL(c.id, '') student_id,
                        IFNULL(c.full_student_name, '') student_name,
                        IFNULL(c.picture, '') picture,
                        IFNULL(c.contact_id, '') portal_id,
                        IFNULL(c.phone_mobile, '') phone,
                        'Contacts' student_type,
                        c.birthdate birthdate,
                        IFNULL(c.nick_name, '') nick_name,
                        IFNULL(c.guardian1, '') p1_id,
                        IFNULL(c.guardian_type_1, '') p1_type,
                        IFNULL(c.guardian2, '') p2_id,
                        IFNULL(c.guardian_type_2, '') p2_type,
                        IFNULL(c.guardian3, '') p3_id,
                        IFNULL(c.guardian_type_3, '') p3_type,
                        IFNULL(l1.last_name, '') p1_last_name,
                        IFNULL(l1.first_name, '') p1_first_name,
                        IFNULL(l1.phone_mobile, '') p1_phone_mobile,
                        IFNULL(l1.picture, '') p1_picture,
                        IFNULL(l1.birthdate, '') p1_birthdate,
                        IFNULL(l2.last_name, '') p2_last_name,
                        IFNULL(l2.first_name, '') p2_first_name,
                        IFNULL(l2.phone_mobile, '') p2_phone_mobile,
                        IFNULL(l2.picture, '') p2_picture,
                        IFNULL(l2.birthdate, '') p2_birthdate,
                        IFNULL(l3.last_name, '') p3_last_name,
                        IFNULL(l3.first_name, '') p3_first_name,
                        IFNULL(l3.phone_mobile, '') p3_phone_mobile,
                        IFNULL(l3.picture, '') p3_picture,
                        IFNULL(l3.birthdate, '') p3_birthdate
                    FROM contacts c
                    LEFT JOIN j_membership l1 ON c.guardian1 = l1.id AND l1.deleted = 0
                    LEFT JOIN j_membership l2 ON c.guardian2 = l2.id AND l2.deleted = 0
                    LEFT JOIN j_membership l3 ON c.guardian3 = l3.id AND l3.deleted = 0
                    WHERE (c.contact_id = '{$args['student_id']}')
                    AND c.deleted = 0";
        $rStudent = $GLOBALS['db']->query($qStudent);
        $student = array();
        $parent = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($rStudent)) {
            $student['student_id']      = $row['student_id'];
            $student['student_name']    = $row['student_name'];
            $student['picture']         = $row['picture'];
            $student['portal_id']       = $row['portal_id'];
            $student['nick_name']       = $row['nick_name'];
            $student['birthdate']       = $timedate->to_display_date($row['birthdate'], false);
            $student['student_phone']   = $row['phone'];
            $student['student_type']    = $row['student_type'];
            if (!empty($row['p1_id'])) {
                $parent[0]['parent_id']     = $row['p1_id'];
                $parent[0]['type']          = $row['p1_type'];
                $parent[0]['last_name']     = $row['p1_last_name'];
                $parent[0]['first_name']    = $row['p1_first_name'];
                $parent[0]['phone_mobile']  = $row['p1_phone_mobile'];
                $parent[0]['picture']       = $row['p1_picture'];
                $parent[0]['birthdate']     = $timedate->to_display_date($row['p1_birthdate'], false);
            }
            if (!empty($row['p2_id'])) {
                $parent[1]['parent_id']     = $row['p2_id'];
                $parent[1]['type']          = $row['p2_type'];
                $parent[1]['last_name']     = $row['p2_last_name'];
                $parent[1]['first_name']    = $row['p2_first_name'];
                $parent[1]['phone_mobile']  = $row['p2_phone_mobile'];
                $parent[1]['picture']       = $row['p2_picture'];
                $parent[1]['birthdate']     = $timedate->to_display_date($row['p2_birthdate'], false);
            }
            if (!empty($row['p3_id'])) {
                $parent[2]['parent_id']     = $row['p3_id'];
                $parent[2]['type']          = $row['p3_type'];
                $parent[2]['last_name']     = $row['p3_last_name'];
                $parent[2]['first_name']    = $row['p3_first_name'];
                $parent[2]['phone_mobile']  = $row['p3_phone_mobile'];
                $parent[2]['picture']       = $row['p3_picture'];
                $parent[2]['birthdate']     = $timedate->to_display_date($row['p3_birthdate'], false);
            }
        }

        $student['parent'] = $parent;

        return  array(
            'success' => 'true',
            'data'    => $student
        );
    }

    /**
     * @param array $args = lesson_date
     *  'path' => array('checkin-out', 'list-student'),
     */
    function listStudent(ServiceBase $api, array $args)
    {
        global $timedate;
        $lessonDateDb = $timedate->convertToDBDate($args['lesson_date'], false);
        $qClass = "SELECT ju_class_id class_id
                    FROM meetings
                    WHERE deleted = 0 AND DATE(CONVERT_TZ(meetings.date_start,'+00:00','+7:00')) = '$lessonDateDb'";
        $classId = array();
        $rClassId = $GLOBALS['db']->query($qClass);
        while ($row = $GLOBALS['db']->fetchByAssoc($rClassId)) {
            $classId[] = $row['class_id'];
        }
        if (empty($classId)) return array('success' => 'false');

        $getListStudent = "SELECT DISTINCT
            IFNULL(c.id, '') student_id,
            IFNULL(c.full_student_name, '') student_name,
            IFNULL(c.picture, '') picture,
            IFNULL(c.contact_id, '') portal_id,
            IFNULL(c.phone_mobile, '') phone,
            'Contacts' type,
            c.birthdate birthdate,
            IFNULL(c.nick_name, '') nick_name,
            IFNULL(l2.id, '') class_id,
            IFNULL(l2.name, '') class_name,
            IFNULL(c.guardian1, '') p1_id,
            IFNULL(c.guardian_type_1, '') p1_type,
            IFNULL(l3.last_name, '') p1_last_name,
            IFNULL(l3.first_name, '') p1_first_name,
            IFNULL(l3.phone_mobile, '') p1_phone_mobile,
            IFNULL(l3.picture, '') p1_picture,
            IFNULL(l3.birthdate, '') p1_birthdate,
            IFNULL(c.guardian2, '') p2_id,
            IFNULL(c.guardian_type_2, '') p2_type,
            IFNULL(l4.last_name, '') p2_last_name,
            IFNULL(l4.first_name, '') p2_first_name,
            IFNULL(l4.phone_mobile, '') p2_phone_mobile,
            IFNULL(l4.picture, '') p2_picture,
            IFNULL(l4.birthdate, '') p2_birthdate,
            IFNULL(c.guardian3, '') p3_id,
            IFNULL(c.guardian_type_3, '') p3_type,
            IFNULL(l5.last_name, '') p3_last_name,
            IFNULL(l5.first_name, '') p3_first_name,
            IFNULL(l5.phone_mobile, '') p3_phone_mobile,
            IFNULL(l5.picture, '') p3_picture,
            IFNULL(l5.birthdate, '') p3_birthdate
            FROM contacts c
            INNER JOIN j_classstudents l1 ON l1.student_id = c.id AND c.deleted = 0
            AND l1.deleted = 0 AND l1.class_id IN ('" . implode("','", $classId) . "')
            INNER JOIN j_class l2 ON l2.id = l1.class_id AND l2.deleted = 0
            LEFT JOIN j_membership l3 ON c.guardian1 = l3.id AND l3.deleted = 0
            LEFT JOIN j_membership l4 ON c.guardian2 = l4.id AND l4.deleted = 0
            LEFT JOIN j_membership l5 ON c.guardian3 = l5.id AND l5.deleted = 0
            ";
        $rGetListStudent = $GLOBALS['db']->query($getListStudent);
        $studentList = array();
        $count = 0;
        while ($r = $GLOBALS['db']->fetchbyAssoc($rGetListStudent)) {
            $studentList[$r['student_id']]['student_id']    = $r['student_id'];
            $studentList[$r['student_id']]['student_name']  = $r['student_name'];
            $studentList[$r['student_id']]['class_id']      = $r['class_id'];
            $studentList[$r['student_id']]['class_name']    = $r['class_name'];
            $studentList[$r['student_id']]['picture']       = $r['picture'];
            $studentList[$r['student_id']]['portal_id']     = $r['portal_id'];
            $studentList[$r['student_id']]['student_phone'] = $r['phone'];
            $studentList[$r['student_id']]['student_type']  = $r['type'];
            $studentList[$r['student_id']]['birthdate']     = $timedate->to_display_date($r['birthdate'], false);
            $studentList[$r['student_id']]['nick_name']     = $r['nick_name'];

            $parent = array();
            if (!empty($r['p1_id'])) {
                $parent[0]['parent_id']     = $r['p1_id'];
                $parent[0]['type']          = $r['p1_type'];
                $parent[0]['last_name']     = $r['p1_last_name'];
                $parent[0]['first_name']    = $r['p1_first_name'];
                $parent[0]['phone_mobile']  = $r['p1_phone_mobile'];
                $parent[0]['picture']       = $r['p1_picture'];
                $parent[0]['birthdate']     = $timedate->to_display_date($r['p1_birthdate'], false);
            }
            if (!empty($r['p2_id'])) {
                $parent[1]['parent_id']     = $r['p2_id'];
                $parent[1]['type']          = $r['p2_type'];
                $parent[1]['last_name']     = $r['p2_last_name'];
                $parent[1]['first_name']    = $r['p2_first_name'];
                $parent[1]['phone_mobile']  = $r['p2_phone_mobile'];
                $parent[1]['picture']       = $r['p2_picture'];
                $parent[1]['birthdate']     = $timedate->to_display_date($r['p2_birthdate'], false);
            }
            if (!empty($r['p3_id'])) {
                $parent[2]['parent_id']     = $r['p3_id'];
                $parent[2]['type']          = $r['p3_type'];
                $parent[2]['last_name']     = $r['p3_last_name'];
                $parent[2]['first_name']    = $r['p3_first_name'];
                $parent[2]['phone_mobile']  = $r['p3_phone_mobile'];
                $parent[2]['picture']       = $r['p3_picture'];
                $parent[2]['birthdate']     = $timedate->to_display_date($r['p3_birthdate'], false);
            }
            $studentList[$r['student_id']]['parent']     = $parent;
            $count++;
        }
        return array(
            'success'   => 'true',
            'data'      => $studentList
        );
    }
}