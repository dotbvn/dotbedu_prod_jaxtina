<?php

class MobileApiV2 extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'edit-picture' => array(
                'reqType' => 'POST',
                'path' => array('v2', 'editpicture'),
                'pathVars' => array(''),
                'method' => 'editPicture',
                'shortHelp' => '',
                'longHelp' => ''
            ),
        );
    }
    //module: Teacher/Student
    function editPicture(ServiceBase $api, $args){
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');

        $id = $args['id'];
        $module = $args['module'];
        $bean = BeanFactory::getBean($module, $id);
        unset($args['id']);

        $name = create_guid();

        if (!is_dir('upload/')) {
            mkdir('upload/');
        }
        if (!is_dir('upload/resize')) {
            mkdir('upload/resize');
        }
        $file1 = 'upload/' . $name;
        $file2 = 'upload/resize/' . $name;

        move_uploaded_file($_FILES['picture']['tmp_name'], $file1);

        file_put_contents($file2, file_get_contents($file1));

        $imgOri1 = new ImageHelper($file1);
        $imgOri1->resize(500);
        $imgOri1->save($file1);

        $imgOri2 = new ImageHelper($file2);
        $imgOri2->resize(220, 220);
        $imgOri2->save($file2);

        if ($args['type'] == 'avatar') {
            $bean->picture = $name;
            $bean->save();
//            $q1 = "UPDATE $module
//            SET $module.picture = '$name' WHERE $module.id = '$id'";
//            $GLOBALS['db']->query($q1);
        } else {
            $q1 = "UPDATE $module
            SET $module.cover_app = '$name' WHERE $module.id = '$id'";
            $GLOBALS['db']->query($q1);
        }

        return array(
            'success' => true,
            'bean_id' => $bean->id,
            'picture_id' => $name,
        );
    }
}