({
    events: {
        'click .close-callbox-ringing': 'close',
        'click .mini-callbox-ringing': 'mini'
    },
    initialize: function (options) {
        this._super('initialize', [options]);
        this.context.set('skipFetch', true);
        var _this = this;
        app.api.call("update", app.api.buildURL('callcenter/getBeans'), {
            phoneNumber: options.data.phoneNumber
        }, {
            success: function (res) {
                if (res.length === 0) {
                    _this.data = [{
                        id: '',
                        name: '',
                        beanName: '',
                        email: ''
                    }];
                } else {
                    _this.timenow = 'callboxringing' + options.data.phoneNumber;
                    _this.data = res;
                    _this.render();
                    $('#footer .btn-toolbar').append(_this.$el);

                    //reset position callbox
                    _this.initPosition(_this.timenow);
                }
            }
        });
    },
    close: function (e) {
        $(e.currentTarget).closest('.callbox-ringing').remove();
    },
    initPosition: function (id) {
        var $this = $('#' + id);
        $this.css('right', $(window).width() - $this.width() - 16);
        $this.css('top', -2 - $this.height());
    },
    mini: function (e) {
        var $thisBox = $(e.currentTarget).closest('.callbox-ringing');
        if ($thisBox.find('.table_info_callbox').is(':visible')) {
            $thisBox.find('.table_info_callbox').hide();
            $thisBox.find('.mini-callbox').removeClass('fa-chevron-down').addClass('fa-chevron-up').attr('data-original-title', 'max');
            this.initPosition($thisBox.attr('id'));
        } else {
            $thisBox.find('.table_info_callbox').show();
            $thisBox.find('.mini-callbox').removeClass('fa-chevron-up').addClass('fa-chevron-down').attr('data-original-title', 'min');
            this.initPosition($thisBox.attr('id'));
        }
    }
})
