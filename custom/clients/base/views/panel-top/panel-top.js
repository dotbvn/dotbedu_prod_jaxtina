({
    extendsFrom: 'PanelTopView',

    /**
    * Addes by HieuPham to create a demo schedule via bwc
    * @param event
    */
    createDemoSchedule: function (event) {
        var id = app.controller.context.attributes.modelId;
        var module = app.controller.context.attributes.module;
        window.location.replace('#bwc/index.php?module=Meetings&action=EditView&return_module=' + module + '&return_action=DetailView&return_id=' + id + '&type=Demo');
    },

    /**
    * Addes by HieuPham to create a PT schedule via bwc
    * @param event
    */
    createPTSchedule: function (event) {
        var id = app.controller.context.attributes.modelId;
        var module = app.controller.context.attributes.module;
        window.location.replace('#bwc/index.php?module=Meetings&action=EditView&return_module=' + module + '&return_action=DetailView&return_id=' + id + '&type=PT');
    },

    /**
    * Addes by HieuPham to open drawer to select a Demo Schedule
    * @param event
    */
    selectDemoSchedule: function (event) {
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
            initial_filter_label: 'Demo',
            initial_filter: 'by_type_demo',
            filter_populate: ['meeting_type']
        });

        app.drawer.open({
            layout: 'selection-list',
            context: {
                fields: ['meeting_type'],
                module: 'Meetings',
                filterOptions: filterOptions.format()
            }
            }, function (model) {
                if (model) {
                    self.setReturnLeadDemo(model);
                }
        });

    },

    /**
    * Addes by HieuPham to open drawer to select a PT Schedule
    * @param event
    */
    selectPTSchedule: function (event) {
        var self = this;
        var filterOptions = new app.utils.FilterOptions().config({
            initial_filter_label: 'Placement Test',
            initial_filter: 'by_type_pt',
            filter_populate: ['meeting_type']
        });

        app.drawer.open({
            layout: 'selection-list',
            context: {
                fields: ['meeting_type'],
                module: 'Meetings',
                filterOptions: filterOptions.format()
            }
            }, function (model) {
                if (model) {
                    self.setReturnLeadPT(model);
                }
        });


    },

    setReturnLeadPT: function (model, filter) {
        var id = app.controller.context.attributes.modelId;
        var module = app.controller.context.attributes.module;
        var self = this;


        if (model.meeting_type != 'Placement Test') {
            self.alertUser( app.lang.get('LBL_ONLY_PT', 'Meetings'), 'error');
            return false;
        }
        app.alert.show('wait', {
            level: 'process',
            title: 'Waiting'
        });

        var preferences = {};
        preferences['meeting_id'] = model.id;
        preferences['parent'] = module;
        preferences['id'] = id;
        preferences['name'] = app.controller.context.attributes.model.get('name');

        DOTB.App.api.call('update', DOTB.App.api.buildURL('Contacts', 'save-pt'), preferences, {
            success: function (data) {
                if (data.success) {
                    self.alertUser(data.message, 'success');
                    $('[data-action=refreshList]').trigger('click');
                    app.controller.context.reloadData();
                } else
                    self.alertUser(data.message, 'error');
                app.alert.dismiss('wait');
            },
            error: function (data) {
                console.log(data);
                self.alertUser(app.lang.get('LBL_ERROR', 'Meetings'), 'error');
                app.alert.dismiss('wait');
            }
        });

    },

    setReturnLeadDemo: function (model, filter) {
        var id = app.controller.context.attributes.modelId;
        var module = app.controller.context.attributes.module;
        var name = app.controller.context.attributes.model.get('name');
        var self = this;


        if (model.meeting_type != 'Demo') {
            self.alertUser( app.lang.get('LBL_ONLY_DEMO', 'Meetings'), 'error');
            return false;
        }

        app.alert.show('wait', {
            level: 'process',
            title: 'Waiting'
        });

        var preferences = {};
        preferences['meeting_id'] = model.id;
        preferences['parent'] = module;
        preferences['id'] = id;
        preferences['name'] = name;

        DOTB.App.api.call('update', DOTB.App.api.buildURL('Contacts', 'save-demo'), preferences, {
            success: function (data) {
                if (data.success) {
                    self.alertUser(data.message, 'success');
                    $('[data-action=refreshList]').trigger('click');
                    app.controller.context.reloadData();
                } else
                    self.alertUser(data.message, 'error');
                app.alert.dismiss('wait');
            },
            error: function (data) {
                console.log(data);
                self.alertUser(app.lang.get('LBL_ERROR', 'Meetings'), 'error');
                app.alert.dismiss('wait');
            }
        });
    },


    /**
    * Addes by Hieu Pham to redirect to create payment
    * @param event
    */
    studentCreatePayment: function (event) {
        var id = app.controller.context.attributes.modelId;
        window.location.replace('#bwc/index.php?module=J_Payment&action=EditView&return_module=J_Payment&return_action=DetailView&payment_type=Cashholder&student_id=' + id);
    },

    /**
    * Addes by Hieu Pham to redirect to create enrollment
    * @param event
    */
    studentCreateBook: function (event) {
        var id = app.controller.context.attributes.modelId;
        window.location.replace('#bwc/index.php?module=J_Payment&action=EditView&return_module=J_Payment&return_action=DetailView&payment_type=Book/Gift&student_id=' + id);
    },

    /**
    * Addes by Hieu Pham to redirect to create payment
    * @param event
    */
    leadCreatePayment: function (event) {
        var id = app.controller.context.attributes.modelId;
        window.location.replace('#bwc/index.php?module=J_Payment&action=EditView&return_module=J_Payment&return_action=DetailView&payment_type=Deposit&lead_id=' + id);
    },

    /**
    * Addes by HieuPham to open drawer select class to add demo
    * @param event
    */
    addDemoToClass: function (event) {
        var self = this;

        app.drawer.open({
            layout: 'selection-list',
            context: {
                fields: ['short_schedule'],
                module: 'J_Class'
            }
            }, function (model) {
                if (model) {
                    self.setReturnClass(model, self);
                }

        });


    },

    addLoyalty: function (event) {

        var student_id = app.controller.context.attributes.modelId;
        var student_name = app.controller.context.attributes.model.get('name');
        window.location.replace('#bwc/index.php?module=J_Loyalty&action=EditView&student_id='+student_id+'&student_name='+student_name);
    },

    setReturnClass: function (model, btself) {
        app.alert.show('wait', {
            level: 'process'
        });

        var id = app.controller.context.attributes.modelId;
        var module = app.controller.context.attributes.module;
        var name = app.controller.context.attributes.model.get('name');
        var self = this;
        var preferences = {};
        preferences['model'] = model;
        preferences['student_type'] = module;
        preferences['student_id'] = id;
        preferences['student_name'] = name;
        preferences['converted'] = app.controller.context.attributes.model.get('converted');

        app.api.call('update', app.api.buildURL('J_Class', 'add-demo-popup'), preferences, {
            success: function (data) {
                if (data) {
                    window.top.$.openPopupLayer({
                        name: "dialog_demo",
                        width: "auto",
                        height: "auto",
                        html: data
                    });

                    options = {
                        format: app.date.toDatepickerFormat(app.user.attributes.preferences.datepref),
                        weekStart: parseInt(app.user.getPreference('first_day_of_week'), 10),
                    };

                    $('input[name="dm_lesson_date"]').datepicker(options);
                    $('input[name="dm_lesson_to_date"]').datepicker(options);
                    $('div.datepicker').css('z-index', '9000');

                    if(typeof(preferences.converted) != 'undefined'
                        && preferences.converted){
                        app.alert.show('message-id', {
                            level: 'warning',
                            messages: app.lang.get('LBL_CONVERTED_LEAD','J_Class'),
                            autoClose: false,
                        });
                        setTimeout(function(){ app.alert.dismiss('message-id'); }, 5000);
                    }
                    $('#btn_add_demo').on('click', function () {
                       $('#btn_add_demo').prop('disabled',true);
                       $('#btn_add_demo').unbind('click');
                       self.addDemo(model.id);
                    });
                    app.alert.dismiss('wait');
                }
            },
            error: function (data) {
                console.log(data);
                self.alertUser(app.lang.get('LBL_ERROR', 'Meetings'), 'error');
                app.alert.dismiss('wait');
            }
        });
    },

    /**
    * Added By HP
    * To Add/Remove a Lead/Student into a Demo Class
    * @param action_demo
    * @param situa_id
    */
    addDemo: function (dm_class_id) {
        var dm_student_id = $('#dm_student_id').val();
        var dm_lesson_date = $('#dm_lesson_date').val();
        var dm_lesson_to_date = $('#dm_lesson_to_date').val();
        var dm_type_student = $('#dm_type_student').val();
        var self = this;

        if( dm_student_id == '' || dm_lesson_date == '' || dm_lesson_to_date == '' || dm_type_student == '')
            return false;

        app.alert.show('wait', {
            level: 'process'
        });

        $.ajax({
            type: "POST",
            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
            data:
            {
                dm_student_id: dm_student_id,
                dm_lesson_date: dm_lesson_date,
                dm_lesson_to_date: dm_lesson_to_date,
                dm_type_student: dm_type_student,
                dm_class_id: dm_class_id,
                type: "addDemo",
            },
            dataType: "json",
            success: function (data) {
                if (data.success == "1"){
                    self.alertUser(data.notify, 'success');
                    if(dm_student_id != data.student_id
                    && dm_type_student != data.student_type){
                    var route = app.router.buildRoute(data.student_type, data.student_id);
                        app.router.redirect(route);
                    }else{
                        if(dm_type_student == 'Contacts') app.controller.context.trigger('subpanel:reload', {links: ['contacts_situations_enrollment','j_studentsituations']});
                        else if(dm_type_student == 'Leads') app.controller.context.trigger('subpanel:reload', {links: ['ju_studentsituations','j_studentsituations']});
                        app.controller.context.reloadData();
                    }
                }else self.alertUser(data.error, 'error');
                app.alert.dismiss('wait');
                window.top.$.closePopupLayer('dialog_demo');
            },
            error: function(data){
                app.alert.dismiss('wait');
                window.top.$.closePopupLayer('dialog_demo');
            }
        });
    },

    /**
    * Added by HP
    * To check date demo have in lession list
    * @param start_study
    * @param end_study
    */
    isInSchedule: function (checking_date) {
        var rs = new Object();
        var json_sessions     = $("#json_sessions").val();
        var flag = false;
        if( checking_date != '' && json_sessions != ''){
            var checking_date = DOTB.util.DateUtils.parse(checking_date,app.user.attributes.preferences.datepref);

            obj = JSON.parse(json_sessions);
            flag = DOTB.util.DateUtils.formatDate(checking_date,false,"Y-m-d") in obj;
            if(!flag)
                this.alertUser(app.lang.get('LBL_DATE_NOT_IN_SCHEDULE', 'J_Class'),'error');
        }
        return flag;
    },



    /**
    * Added by Hieu Pham to show alert
    * @param msg - Label message in Module ( ex: LBL_ERROR)
    * @param level - level of alert (ex: error, success)
    */
    alertUser: function (msg, level) {
        app.alert.show('no-lumia-access', {
            level: level,
            messages: app.lang.get(msg, app.controller.context.attributes.module),
            autoClose: true,
        });
    }
});
