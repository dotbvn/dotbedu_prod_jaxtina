<?php
require_once('custom/include/utils/apiHelper.php');

class TenantApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'refresh_license' =>
            array (
                'reqType' => 'POST',
                'path' => array('tenant', 'license', 'refresh'),
                'pathVars' => array(''),
                'method' => 'refreshLicense',
            ),
            'validate_student' =>
            array (
                'reqType' => 'GET',
                'path' => array('tenant', 'license', 'validate-student'),
                'pathVars' => array(''),
                'method' => 'validateStudent',
            ),
            'validate_user' =>
                array (
                    'reqType' => 'GET',
                    'path' => array('tenant', 'license', 'validate-user'),
                    'pathVars' => array(''),
                    'method' => 'validateUser',
                ),
            'tenant_get_license' =>
            array (
                'reqType' => 'POST',
                'path' => array('tenant', 'license'),
                'pathVars' => array(),
                'method' => 'getLicense',
                'noLoginRequired' => false,
            ),
            'tenant_update_license' =>
            array (
                'reqType' => 'POST',
                'path' => array('tenant', 'license', 'update'),
                'pathVars' => array(),
                'method' => 'updateLicense',
                'noLoginRequired' => false,
            ),
            'sample_db' =>
                array (
                    'reqType' => 'GET',
                    'path' => array('tenant', 'sample-db'),
                    'pathVars' => array(),
                    'method' => 'getSampleDB',
                    'noLoginRequired' => false,
                ),
        );
    }

    function refreshLicense(ServiceBase $api, array $args): array {
        global $dotb_config;
        $site_host = parse_url($dotb_config['site_url'])['host'];
        $tenant_name = ($site_host == 'production.dotb.cloud') ? $_SERVER['SERVER_NAME'] : $site_host;

        // Call API to get new license
        require_once('custom/include/utils/DotbWebhook.php');
        $webhook = new DotbWebhook();
        $response = $webhook->callQuyettamAPI('tenant/get', 'POST', ['tenant_name' => $tenant_name], array('Content-Type: multipart/form-data'));
        $result = $response['data'];
        if ($result['success']) {
            $license = $result['data']['tenant']['license'];
            $admin = new Administration();
            $admin->retrieveSettings();
            // Save new config
            foreach ($license as $key => $value) {
                $admin->saveSetting('license', $key, $value);
            }
            refreshGlobalLicense();
        }
        return array(
            'success' => true,
            'data' => ['tenant_name' => $tenant_name],
            'error_info' => throwApiError(1)
        );
    }

    function validateStudent(ServiceBase $api, array $args): array {
        global $license;
        global $app_strings;
        $current_active_student = $GLOBALS['db']->getOne("SELECT count(id) as total FROM contacts WHERE contacts.ems_active_state = 1 AND contacts.deleted = 0");
        $valid = ((int)$license['lic_subscription'] != 0 && ($current_active_student + 1 <= $license['lic_subscription']));
        $message = sprintf($app_strings["LLI_OVER_LIMIT_SUBSCRIPTION"],
            "{$license['lic_activated_subscription']}/{$license['lic_subscription']}");

        return array(
            'success' => true,
            'data' => [
                'valid' => $valid,
                'message' => ($valid) ? 'Validate successfully!' : $message
            ],
            'error_info' => throwApiError(1)
        );
    }

    function validateUser(ServiceBase $api, array $args): array {
        global $app_strings;
        $licenseBean = new LicenseBean();
        $licenseBean->fromClient();
        $current_active_user = $licenseBean->lic_activated_users;
        $valid = ((int)$licenseBean->lic_users != 0 && ($current_active_user + 1 <= $licenseBean->lic_users));
        $message = sprintf($app_strings["LLI_OVER_LIMIT_USER"],
            "{$licenseBean->lic_activated_users}/{$licenseBean->lic_users}");

        return array(
            'success' => true,
            'data' => [
                'valid' => $valid,
                'message' => ($valid) ? 'Validate successfully!' : $message
            ],
            'error_info' => throwApiError(1)
        );
    }

    function getLicense(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/DotbWebhook.php');
        $webhook = new DotbWebhook();
        $data = [
            'tenant_name' => $args['lic_tenant_name']
        ];

        $result = $webhook->callQuyettamAPI('tenant/get', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }

        if (!empty($result['data']['data']['tenant']['license'])) {
            return array(
                'success' => true,
                'data' => $result['data']['data']['tenant']['license']
            );
        }

        return array(
            'success' => false,
        );
    }

    function validateLicense(ServiceBase $api, array $args)
    {
        require_once('custom/include/utils/DotbWebhook.php');
        $webhook = new DotbWebhook();

        $result = $webhook->callQuyettamAPI('tenant/validate-license', 'POST', $args, array('Content-Type: multipart/form-data'));
        if(empty($result) || !$result['data']['success']) {
            return array(
                'success' => false,
            );
        }

        return array(
            'success' => true,
        );
    }

    function getSampleDB(ServiceBase $api, array $args)
    {
        if ($query = file_get_contents(dotb_cached('include/sample_db.sql'))) {
            /// todo: execute script
            $query = str_replace("\n", "", $query);
            $array_sql = explode(';', $query);

            foreach ($array_sql as $index => $q) {
                if (!empty($q)) {
                    $result = $GLOBALS['db']->query($q);

                    if ($result !== true) {
                        $GLOBALS['log']->fatal('TenantApi/getSampleDB error: ' . $q);
                    }
                }
            }

            return array(
                'success' => true,
            );

        } else {
            require_once('custom/include/utils/DotbWebhook.php');
            global $license;
            $webhook = new DotbWebhook();
            $data = [
                'tenant_id' => $license['lic_tenant_id']
            ];
            $result = $webhook->callQuyettamAPI('tenant/sample-db', 'POST', $data, array('Content-Type: multipart/form-data'));

            if(empty($result) || !$result['data']['success']) {
                return array(
                    'success' => false,
                );
            } else {
                /// create sample_db here
                $encrypt_content = $result['data']['data']['query'];

                if (!empty($encrypt_content)) {
                    $private_key = '2d90170b2e0d1f85';

                    $file_content = decrypt($encrypt_content, $private_key);
                    file_put_contents(dotb_cached('include/sample_db.sql'), $file_content);
                }
            }

            $query = file_get_contents(dotb_cached('include/sample_db.sql'));
            /// todo: execute script
            $array_sql = explode(';', urldecode($query));

            foreach ($array_sql as $index => $q) {
                $result = $GLOBALS['db']->query($q);

                if ($result !== true) {
                    $GLOBALS['log']->fatal('Sample data error: '.$q);
                }
            }

            return array(
                'success' => true,
            );
        }
    }
}