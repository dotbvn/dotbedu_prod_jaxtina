<?php

use Firebase\JWT\JWT;

class OIDCApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'oidc-login' => array(
                'reqType' => 'POST',
                'path' => array('oidc', 'login'),
                'pathVars' => array(),
                'method' => 'login',
                'noLoginRequired' => true,
            ),
            'oidc-set-code' => array(
                'reqType' => 'POST',
                'path' => array('oidc', 'code'),
                'pathVars' => array(),
                'method' => 'code',
                'noLoginRequired' => true,
            ),
            'oidc-token' => array(
                'reqType' => 'POST',
                'path' => array('oidc', 'token'),
                'pathVars' => array(),
                'method' => 'token',
                'noLoginRequired' => true,
            ),
            'oidc-userinfo' => array(
                'reqType' => 'GET',
                'path' => array('oidc', 'userinfo'),
                'pathVars' => array(),
                'method' => 'userinfo',
                'noLoginRequired' => true,
            ),
            'oidc-changepass' => array(
                'reqType' => 'POST',
                'path' => array('oidc', 'changepassword'),
                'pathVars' => array(),
                'method' => 'changepassword',
                'noLoginRequired' => true,
            ),
        );
    }

    function checkToken($args)
    {
        if (empty($args['code'])) {
            return array(
                'status' => 404,
                'message' => 'code is required'
            );
        }
        //check oath
        global $db;
        $result = $db->query("select consumer from oauth_tokens where id='{$args['code']}'");
        if ($result = $db->fetchByAssoc($result)) {
            $isUser = $db->getOne("select id from users where id='{$result['consumer']}'");
            if ($isUser) {
                $GLOBALS['oidc_current_user'] = BeanFactory::getBean('Users', $result['consumer'], array('disable_row_level_security' => true));
                if (empty($GLOBALS['oidc_current_user']->id)) {
                    return array(
                        'status' => 404,
                        'message' => 'code is required'
                    );
                }
            } else {
                $GLOBALS['oidc_current_user'] = BeanFactory::getBean('Contacts', $result['consumer'], array('disable_row_level_security' => true));
                if (empty($GLOBALS['oidc_current_user']->id)) {
                    return array(
                        'status' => 404,
                        'message' => 'code is required'
                    );
                }
            }
        }
        return array(
            'status' => 200,
            'data' => [
                'access_token' => base64_encode($args['client_id'] . ':' . $args['code']),
                'token_type' => "Bearer",
            ]
        );
    }

    function setCurrentUser()
    {
        $headers = getallheaders();
        $oauth = $headers['Authorization'];
        if (empty($oauth)) {
            http_response_code(401);
            return array(
                'status' => 401,
                'message' => 'Headers is missing authorization'
            );
        }
        if (explode(' ', $oauth)[0] != 'Bearer') {
            http_response_code(401);
            return array(
                'status' => 401,
                'message' => 'Authorization type must be OAuth 2.0'
            );
        } else {
            global $db;
            $id = explode(' ', $oauth)[1];
            $result = $db->query("select consumer from oauth_tokens where id='$id'");
            if ($result = $db->fetchByAssoc($result)) {
                $isUser = $db->getOne("select id from users where id='{$result['consumer']}'");
                if ($isUser) {
                    $GLOBALS['oidc_current_user'] = BeanFactory::getBean('Users', $result['consumer'], array('disable_row_level_security' => true));
                    if (empty($GLOBALS['oidc_current_user']->id)) {
                        http_response_code(401);
                        return array(
                            'status' => 401,
                            'message' => 'Invalid authorization'
                        );
                    }
                } else {
                    $GLOBALS['oidc_current_user'] = BeanFactory::getBean('Contacts', $result['consumer'], array('disable_row_level_security' => true));
                    if (empty($GLOBALS['oidc_current_user']->id)) {
                        return array(
                            'status' => 404,
                            'message' => 'code is required'
                        );
                    }
                }
            } else {
                http_response_code(401);
                return array(
                    'status' => 401,
                    'message' => 'Authorization type must be OAuth 2.0'
                );
            }
        }
    }

    function changepassword(ServiceBase $api, array $args)
    {
        $this->setCurrentUser();
        global $db;
        $isUser = $db->getOne("select id from users where id='{$GLOBALS['oidc_current_user']->id}'");
        if ($isUser) {
            $db->query("update users set user_hash='{$args['new_password']}' where id='{$GLOBALS['oidc_current_user']->id}'");
        } else {
            $db->query("update contacts set portal_password='{$args['new_password']}' where id='{$GLOBALS['oidc_current_user']->id}'");
        }
        return ['success' => 1];
    }

    function userinfo(ServiceBase $api, array $args)
    {
        global $db;
        $this->setCurrentUser();
        http_response_code(200);
        $isUser = $db->getOne("select id from users where id='{$GLOBALS['oidc_current_user']->id}'");
        if ($isUser) {
            return array(
                'status' => 200,
                'data' => [
                    'preferred_username' => $GLOBALS['oidc_current_user']->user_name,
                    'family_name' => $GLOBALS['oidc_current_user']->last_name,
                    'given_name' => $GLOBALS['oidc_current_user']->first_name,
                    'email' => $GLOBALS['oidc_current_user']->email1,
                    'id' => $GLOBALS['oidc_current_user']->id
                ]
            );
        }
        return array(
            'status' => 200,
            'data' => [
                'preferred_username' => $GLOBALS['oidc_current_user']->portal_name,
                'family_name' => $GLOBALS['oidc_current_user']->last_name,
                'given_name' => $GLOBALS['oidc_current_user']->first_name,
                'email' => $GLOBALS['oidc_current_user']->email1,
                'id' => $GLOBALS['oidc_current_user']->id
            ]
        );
    }

    function token(ServiceBase $api, array $args)
    {
        $GLOBALS['log']->fatal($args);
        $res = $this->checkToken($args);
        $GLOBALS['log']->fatal($res);
        global $db;
        http_response_code($res['status']);
        $data = $res['data'];
        $isUser = $db->getOne("select id from users where id='{$GLOBALS['oidc_current_user']->id}'");
        $key = "skip_verification";
        $payload = array(
            'preferred_username' => $isUser ? $GLOBALS['oidc_current_user']->user_name : $GLOBALS['oidc_current_user']->portal_name,
            'family_name' => $GLOBALS['oidc_current_user']->last_name,
            'given_name' => $GLOBALS['oidc_current_user']->first_name,
            'email' => $GLOBALS['oidc_current_user']->email1,
            'id' => $GLOBALS['oidc_current_user']->id
        );
        $jwt = JWT::encode($payload, $key);
        $data['id_token'] = $jwt;
        $GLOBALS['log']->fatal($data);
        return $data;
    }

    function login(ServiceBase $api, array $args)
    {
        global $db;
        $result = $db->query("select user_hash,id from users where user_name='{$args['username']}' and deleted=0 and status='Active'");
        $result = $db->fetchByAssoc($result);
        if (User::checkPassword($args['password'], $result['user_hash']) || User::checkPasswordMD5($args['password'], $result['user_hash'])) {
            $code = $this->code($api, ['user_id' => $result['id']]);
            return ['success' => 1, 'code' => $code];
        } else {
            $result = $db->query("select portal_password,id from contacts where portal_name='{$args['username']}' and deleted=0");
            $result = $db->fetchByAssoc($result);
            if (User::checkPassword($args['password'], $result['portal_password']) || User::checkPasswordMD5($args['password'], $result['portal_password'])) {
                $code = $this->code($api, ['user_id' => $result['id']]);
                return ['success' => 1, 'code' => $code];
            } else {
                return ['success' => 0];
            }
        }
    }

    function code(ServiceBase $api, array $args)
    {
        global $db;
        $id = create_guid();
        $db->query("delete from oauth_tokens where secret='{$args['user_id']}'");
        $db->query("insert into oauth_tokens(id,consumer) values('$id','{$args['user_id']}')");
        return $id;
    }
}
