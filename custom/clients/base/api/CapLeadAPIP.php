<?php

class CapLeadAPIP extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'cap_lead_p' => array(
                'reqType' => 'POST',
                'path' => array('captureLead'),
                'pathVars' => array(''),
                'method' => 'capLeadAPI',
                'noLoginRequired' => true
            ),
            'cap_lead_v2' => array(
                'reqType' => 'POST',
                'path' => array('cap_lead_v2'),
                'pathVars' => array(''),
                'method' => 'capLeadAPI',
                'noLoginRequired' => true
            ),
            'get_apps_list_strings' => array(
                'reqType' => 'GET',
                'path' => array('get_dropdown_options'),
                'pathVars' => array(''),
                'method' => 'getAppsListStrings',
                'noLoginRequired' => true
            ),
        );
    }
    function capLeadAPI(ServiceBase $api, array $args){
        $_POST = $args;
        include_once('custom/include/capLead.php');
    }

    function getAppsListStrings(ServiceBase $api, array $args){
        global $app_list_strings;
        $app_string_vn  = parseAppListStringForWeb('vn_vn',explode(",",$args['name']));
        $app_strings_en = parseAppListStringForWeb('en_us',explode(",",$args['name']));

        if(empty($app_strings_en) && empty($app_string_vn)){
            return json_encode(array(
                'success'       =>  0,
            ));
        }else return json_encode(array(
            'success'       =>  1,
            'app_strings_en'=>  $app_strings_en,
            'app_strings_vn' => $app_string_vn,
            ));
    }
}
