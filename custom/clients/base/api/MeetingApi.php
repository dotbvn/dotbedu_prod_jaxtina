<?php

class MeetingApi extends DotbApi {
    function registerApiRest() {
        return array(
            'meeting-get-type' => array(
                'reqType' => 'PUT',
                'path' => array('Meetings', 'get-type'),
                'pathVars' => array(''),
                'method' => 'getType',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'check-lead-in-schedule' => array(
                'reqType' => 'PUT',
                'path' => array('Meetings', 'check-lead-in-schedule'),
                'pathVars' => array(''),
                'method' => 'check_lead_in_schedule',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'meeting-save-select' => array(
                'reqType' => 'GET',
                'path' => array('Meetings', 'save-select'),
                'pathVars' => array(''),
                'method' => 'saveSelect',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'meeting-update-result' => array(
                'reqType' => 'GET',
                'path' => array('Meetings', 'update-result'),
                'pathVars' => array(''),
                'method' => 'updatePTResult',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'meeting-delete-result' => array(
                'reqType' => 'PUT',
                'path' => array('Meetings', 'delete-result'),
                'pathVars' => array(''),
                'method' => 'deletePTResult',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'meeting-save-result' => array(
                'reqType' => 'GET',
                'path' => array('Meetings', 'save-result'),
                'pathVars' => array(''),
                'method' => 'saveResultAll',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'meeting-move-result' => array(
                'reqType' => 'PUT',
                'path' => array('Meetings', 'move-result'),
                'pathVars' => array(''),
                'method' => 'movePTResult',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'meeting-save-demo' => array(
                'reqType' => 'GET',
                'path' => array('Meetings', 'save-demo'),
                'pathVars' => array(''),
                'method' => 'saveDemoResult',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'meeting-save-attended' => array(
                'reqType' => 'GET',
                'path' => array('Meetings', 'save-attended'),
                'pathVars' => array(''),
                'method' => 'saveDemoAttended',
                'shortHelp' => '',
                'longHelp' => '',
            ),

            'meeting-update-ecnote' => array(
                'reqType' => 'GET',
                'path' => array('Meetings', 'update-ecnote'),
                'pathVars' => array(''),
                'method' => 'updateECNote',
                'shortHelp' => '',
                'longHelp' => '',
            ),

        );
    }
    /**
    * Added by Hieu Pham to get type of meeting for routing
    * @param ServiceBase $api
    * @param array $args
    * @return string
    */
    function getType(ServiceBase $api, array $args) {
        $type = 'other';
        if (isset($args) && !empty($args)) {
            if ($args["id"]) {
                $db = DBManagerFactory::getInstance();
                $result = $db->query("SELECT meeting_type FROM meetings WHERE id = '" . $args['id'] . "' AND deleted = 0");
                if ($row = $db->fetchByAssoc($result)) {
                    $type = $row['meeting_type'];
                } else {
                    $type = 'none';
                }
            }
        }
        return $type;

    }
    /**
    * Added by Hieu Pham to check student/lead have exists in PT
    * @param ServiceBase $api
    * @param array $data
    * @return bool
    */
    function check_lead_in_schedule(ServiceBase $api, array $data) {
        //kiem tra neu student or lead do da co trong situation thi bo qua khong them nua
        $sql_check = "
        SELECT count(j_ptresult.id) count
        FROM j_ptresult
        LEFT JOIN leads l1 ON j_ptresult.student_id = l1.id AND l1.deleted = 0 AND j_ptresult.parent = 'Leads'
        LEFT JOIN contacts l2 ON j_ptresult.student_id = l2.id AND l2.deleted = 0 AND j_ptresult.parent = 'Contacts'
        INNER JOIN meetings l3 ON j_ptresult.meeting_id = l3.id AND l3.deleted = 0
        WHERE (l3.id = '{$data['meeting_id']}') AND j_ptresult.deleted = 0
        AND ( l1.id = '{$data['student_id']}' OR l2.id='{$data['student_id']}' ) ";
        $count = $GLOBALS['db']->getOne($sql_check);
        return (($count > 0) ? true : false);
    }

    /**
    * Added by Hieu Pham to save select lead/student in PT/Demo
    * @param ServiceBase $api
    * @param array $data
    * @return mixed
    */
    function saveSelect(ServiceBase $api, array $data) {
        global $timedate;

        $i = 0;
        $meeting = BeanFactory::getBean('Meetings', $data['meeting_id'],array('disable_row_level_security' => true));

        $result = new J_PTResult();
        $result->pt_order = $data['pt_order'][$i];

        $result->team_id = $meeting->team_id;
        $result->team_set_id = $meeting->team_set_id;
        $result->assigned_user_id = $meeting->assigned_user_id;

        $result->attended = $data['check_attended'][$i];


        $result->type_result = $meeting->meeting_type;

        //get Student Info
        $rela_student = BeanFactory::getBean($data['student_type'][$i], $data['pt_id'][$i],array('disable_row_level_security' => true));
        $result->name = $meeting->name . ' - ' . $rela_student->last_name . ' ' . $rela_student->first_name;

        $result->parent = $data['student_type'][$i];
        $result->student_id = $data['pt_id'][$i];
        $result->meeting_id = $meeting->id;
        $result->save();
        return $result->id;
    }

    /**
    * Added by Hieu Pham to update PT result when change score, note, ...
    * @param ServiceBase $api
    * @param array $data
    * @return mixed
    */
    function updatePTResult(ServiceBase $api, array $data) {
        $id_result = $data['id_of_result'][0];
        //Update Lead/Student last_pt_result
		$res_koc = $data['result_koc'][0];
        $parts = explode('-', $res_koc);
        $data['result'][0]     = $res_koc;
        $data['result_koc'][0] = trim($parts[0]);
        $data['result_lvl'][0] = trim($parts[1]);

        if(!empty($id_result)){
            $pt_res = BeanFactory::getBean('J_PTResult', $id_result,array('disable_row_level_security' => true));
            $changed = 0;
            foreach ($data as $field => $val){
                $value =  $data[$field][0];
                if($field == 'pt_order')   continue;


                if($field == 'check_attended') $field = 'attended';

                if(isset($pt_res->$field) &&  $pt_res->$field != $value){
                    $changed++;
                    $pt_res->$field = $value;
                }
            }
            if($changed > 0) $pt_res->save();
        }
        return true;
    }

    /**
    * Added by Hieu Pham to delete PTResult record when delete in the sub-panel
    * @param ServiceBase $api
    * @param array $data
    * @return bool
    */
    function deletePTResult(ServiceBase $api, array $data) {
        $thisResult = new J_PTResult();
        $thisResult->retrieve($data['result_id']);
        if ($thisResult->id) {
            $thisResult->mark_deleted($thisResult->id);
            return true;
        }
        return false;
    }

    /**
    * Added by Hieu Pham to save all result
    * @param ServiceBase $api
    * @param array $data
    * @return bool
    */
    function saveResultAll(ServiceBase $api, array $data) {
        for ($i = 1; $i < count($data['id_of_result']); $i++) {
            $result = new J_PTResult();
            $result->retrieve($data['id_of_result'][$i]);
            if (!empty($result->id)) {
                $GLOBALS['db']->query("UPDATE j_ptresult SET pt_order='{$data['pt_order'][$i]}'  WHERE id='{$result->id}'");
            }
        }
        return true;
    }

    /**
    * Added by Hieu Pham to move result to another Meeting
    * @param ServiceBase $api
    * @param array $data
    * @return bool
    */
    function movePTResult(ServiceBase $api, array $data) {
        $results = $data["results"];
        $meetingId = $data["meeting_id"];

        foreach ($results as $id) {
            $result = BeanFactory::getBean('J_PTResult', $id ,array('disable_row_level_security' => true));
            $meeting = BeanFactory::getBean('Meetings', $meetingId,array('disable_row_level_security' => true));
            $result->team_id = $meeting->team_id;
            $result->team_set_id = $meeting->team_set_id;
            $result->assigned_user_id = $meeting->assigned_user_id;
            //change id
            $result->meeting_id = $meeting->id;

            $result->ec_note = "";
            $result->save();
        }

        return "success";
    }

    /**
    * Added by Hieu Pham to save demo result after add in sub-panel view
    * @param ServiceBase $api
    * @param array $data
    * @return bool
    */
    function saveDemoResult(ServiceBase $api, array $data) {
        global $timedate;
        $meeting = BeanFactory::getBean('Meetings', $data['meeting_id'],array('disable_row_level_security' => true));
        $i = 0;
        $result = new J_PTResult();
        $result->team_id = $meeting->team_id;
        $result->team_set_id = $meeting->team_set_id;
        $result->assigned_user_id = $meeting->assigned_user_id;
        $result->attended = $data['check_attended'][$i];
        $result->parent = $data['student_type'][$i];
        $result->ec_note = $data['ec_note'][$i];
        $result->type_result = "Demo";
        $result->time_start = $meeting->date_start;
        $result->time_end = $meeting->date_end;

        //get Student Info
        $rela_student = BeanFactory::getBean($data['student_type'][$i], $data['pt_id'][$i],array('disable_row_level_security' => true));
        $result->name = $meeting->name . ' - ' . $rela_student->last_name . ' ' . $rela_student->first_name;

        $result->student_id = $data['demo_id'][$i];
        $result->meeting_id = $meeting->id;
        $result->save();
        return $result->id;
    }

    /**
    * Added by Hieu Pham to save demo Attended
    * @param ServiceBase $api
    * @param array $data
    * @return bool
    */
    function saveDemoAttended(ServiceBase $api, array $data) {
        $id_result = $data['id_of_result'][0];
        if(!empty($id_result)){
            $pt_res = BeanFactory::getBean('J_PTResult', $id_result,array('disable_row_level_security' => true));
            $pt_res->attended = $data['check_attended'][0];
            $pt_res->save();
        }
        return true;
    }

    /**
    * Added by Hieu Pham to save update EC Note demo result
    * @param ServiceBase $api
    * @param array $data
    * @return bool
    */
    function updateECNote($data) {
        $sql_update_koc = "UPDATE j_ptresult SET ec_note='{$data['ec_note'][0]}'  WHERE id='{$data['id_of_result'][0]}'";
        return $GLOBALS['db']->query($sql_update_koc);
    }
}