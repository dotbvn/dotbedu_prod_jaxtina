<?php

class CassoApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'casso_webhook_get' => array(
                'reqType' => 'GET',
                'path' => array('casso', 'webhook'),
                'pathVars' => array(),
                'method' => 'cassoWebhook',
                'noLoginRequired' => true
            ),
            'casso_webhook_post' => array(
                'reqType' => 'POST',
                'path' => array('casso', 'webhook'),
                'pathVars' => array(),
                'method' => 'cassoWebhook',
                'noLoginRequired' => true
            )
        );
    }

    function cassoWebhook(ServiceBase $api, array $args){
        global $current_user;
        //Get User apps_admin
        $current_user = new User();
        $current_user->retrieve_by_string_fields(array('user_name' => 'apps_admin'));

        //Get User admin
        if(empty($current_user->id)){
            $current_user = new User();
            $current_user->retrieve_by_string_fields(array('user_name' => 'admin'));
        }

        if(empty($current_user->id)) return array('success' => 0, 'message_code' => 'apps_admin_not_found', 'message' => 'User Apps Admin does not found!');

        if ($args['error'] == 0) {
            $record = new J_BankTrans();
            $record->name               = $args['data'][0]['id'];
            $record->tid                = $args['data'][0]['tid'];
            $record->engine_type        = 'Casso';
            $record->description        = $args['data'][0]['description'];
            $record->amount             = $args['data'][0]['amount'];
            $record->cusum_balance      = $args['data'][0]['cusum_balance'];
            $record->when_entered       = $args['data'][0]['when']; //GMT+7
            $record->bank_sub_acc_id    = $args['data'][0]['bank_sub_acc_id'];
            $record->subaccid           = $args['data'][0]['subAccId'];
            $record->bankname           = $args['data'][0]['bankName'];
            $record->bankabbreviation   = $args['data'][0]['bankAbbreviation'];
            $record->virtualaccount     = $args['data'][0]['virtualAccount'];
            $record->virtualaccountname = $args['data'][0]['virtualAccountName'];
            $record->corresponsivename      = $args['data'][0]['corresponsiveName'];
            $record->corresponsiveaccount   = $args['data'][0]['corresponsiveAccount'];
            $record->corresponsivebankid    = $args['data'][0]['corresponsiveBankId'];
            $record->corresponsivebankname  = $args['data'][0]['corresponsiveBankName'];
            if($record->amount > 0) $record->save();
        }
        return array('success' => 1);
    }
}
