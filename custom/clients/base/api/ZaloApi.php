<?php
require_once('include/utils.php');
require_once('custom/include/utils/parseTemplate.php');

class ZaloApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'zalo_config' => array(
                'reqType' => 'GET',
                'path' => array('zalo', 'config'),
                'pathVars' => array(''),
                'method' => 'config'
            ),
            'zalo_save_config' => array(
                'reqType' => 'PUT',
                'path' => array('zalo', 'saveConfig'),
                'pathVars' => array(''),
                'method' => 'saveConfig'
            ),
        );
    }

    function config(ServiceBase $api, array $args){
        $admin = new Administration();
        $admin->retrieveSettings();
        $data = null;
        if (!empty($admin->settings['zalo_link'])) $data['link'] = (string)$admin->settings['zalo_link'];
        if (!empty($admin->settings['zalo_key'])) $data['key'] = $admin->settings['zalo_key'];
        if (!empty($admin->settings['zalo_supplier'])) $data['supplier'] = $admin->settings['zalo_supplier'];
        if (!empty($admin->settings['zalo_username'])) $data['username'] = $admin->settings['zalo_username'];
        if (!empty($admin->settings['zalo_enable'])) $data['enable'] = $admin->settings['zalo_enable'];
        return array('data' => $data);
    }

    function saveConfig(ServiceBase $api, array $args){
        $admin = new Administration();
        $admin->retrieveSettings();
        $admin->saveSetting('zalo', 'link', $args['link']);
        $admin->saveSetting('zalo', 'key', $args['key']);
        $admin->saveSetting('zalo', 'supplier', $args['supplier']);
        $admin->saveSetting('zalo', 'username', $args['username']);
        $admin->saveSetting('zalo', 'enable', $args['enable']);
        return array("success" => 1);
    }
}
