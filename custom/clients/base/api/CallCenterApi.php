<?php

class CallCenterApi extends DotbApi
{
    const RESULT_NOT_STARTED = ['Busy/No Answer', 'Call Back Later', 'Deny', 'Out of Cover'];
    const RESULT_IN_PROCESS = ['Interested', 'Request More Info'];
    const RESULT_DEAD = ['Not Interested', 'Invalid Number', 'Impossible to Convert', 'Duplicate'];

    function registerApiRest()
    {
        return array(
            'callcenter_fptoncall_make_call' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'fptoncall', 'makecall'),
                'pathVars' => array(),
                'method' => 'fptoncallMakecall'
            ),
            'callcenter_config' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'config'),
                'pathVars' => array(),
                'method' => 'getConfig'
            ),
            'callcenter_webhook_incoming' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'routing_incom'),
                'pathVars' => array(),
                'method' => 'getRoutingIncom',
                'noLoginRequired' => true
            ),
            'callcenter_saveConfig' => array(
                'reqType' => 'PUT',
                'path' => array('callcenter', 'saveConfig'),
                'pathVars' => array(),
                'method' => 'saveConfig'
            ),
            'callcenter-getScript' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'getScript'),
                'pathVars' => array(),
                'method' => 'getScript'
            ),
            'callcenter-getBeans' => array(
                'reqType' => 'PUT',
                'path' => array('callcenter', 'getBeans'),
                'pathVars' => array(),
                'method' => 'getBeans'
            ),
            'callcenter-save' => array(
                'reqType' => 'PUT',
                'path' => array('callcenter', 'save'),
                'pathVars' => array(),
                'method' => 'save'
            ),

            /**
             * oncall
             */
            'callcenter-oncall-clicktocall' => array(
                'reqType' => 'PUT',
                'path' => array('callcenter', 'oncall', 'clicktocall'),
                'pathVars' => array(),
                'method' => 'oncallClickToCall'
            ),
            'callcenter-oncall-webhook-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'oncall', 'webhook'),
                'pathVars' => array(),
                'method' => 'oncallWebhook',
                'noLoginRequired' => true,
            ),
            'callcenter-oncall-webhook-post' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'oncall', 'webhook'),
                'pathVars' => array(),
                'method' => 'oncallWebhook',
                'noLoginRequired' => true,
            ),
            'callcenter-oncall-get-recording' => array(
                'reqType' => 'PUT',
                'path' => array('callcenter', 'oncall', 'recording'),
                'pathVars' => array(),
                'method' => 'oncallGetRecording'
            ),
            'callcenter-voip24h-webhook-callmissed' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'voip24h', 'webhook-callmissed'),
                'pathVars' => array(),
                'method' => 'VOIP24hCallmissed',
                'noLoginRequired' => true,
            ),
            'callcenter-voip24h-webhook-callmissed2' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'voip24h', 'webhook-callmissed'),
                'pathVars' => array(),
                'method' => 'VOIP24hCallmissed',
                'noLoginRequired' => true,
            ),

            /**
             * worldfone
             */
            'callcenter-worldfone-webhook-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'worldfone', 'webhook'),
                'pathVars' => array(),
                'method' => 'worldfoneWebhook',
                'noLoginRequired' => true,
            ),
            'callcenter-worldfone-webhook-post' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'worldfone', 'webhook'),
                'pathVars' => array(),
                'method' => 'worldfoneWebhook',
                'noLoginRequired' => true,
            ),

            /**
             * cloudfone
             */
            'callcenter-cloudfone-webhook-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'cloudfone', 'webhook'),
                'pathVars' => array(),
                'method' => 'cloudfoneWebhook',
                'noLoginRequired' => true,
            ),
            'callcenter-cloudfone-webhook-post' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'cloudfone', 'webhook'),
                'pathVars' => array(),
                'method' => 'cloudfoneWebhook',
                'noLoginRequired' => true,
            ),

            /**
             * voicecloud
             */
            'callcenter-voicecloud-hangup' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'voicecloud', 'hangup'),
                'pathVars' => array(),
                'method' => 'voiceCloudHangup',
                'noLoginRequired' => true,
            ),
            'callcenter-voicecloud-hangup-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'voicecloud', 'hangup'),
                'pathVars' => array(),
                'method' => 'voiceCloudHangup',
                'noLoginRequired' => true,
            ),
            'callcenter-voicecloud-answer' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'voicecloud', 'log'),
                'pathVars' => array(),
                'method' => 'voiceCloudLog',
                'noLoginRequired' => true,
            ),
            'callcenter-voicecloud-answer-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'voicecloud', 'log'),
                'pathVars' => array(),
                'method' => 'voiceCloudLog',
                'noLoginRequired' => true,
            ),
            'callcenter-voicecloud-call' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'voicecloud', 'call'),
                'pathVars' => array(),
                'method' => 'voiceCloudCall',
                'noLoginRequired' => true,
            ),
            'callcenter-voicecloud-callcomming' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'voicecloud', 'callcomming'),
                'pathVars' => array(),
                'method' => 'voiceCloudCallcomming',
                'noLoginRequired' => true,
            ),
            'callcenter-voicecloud-callcomming-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'voicecloud', 'callcomming'),
                'pathVars' => array(),
                'method' => 'voiceCloudCallcomming',
                'noLoginRequired' => true,
            ),
            'callcenter-voicecloud-ext-callcomming-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'voicecloud', 'ext-callcomming'),
                'pathVars' => array(),
                'method' => 'voiceCloudExtCallcomming',
                'noLoginRequired' => true,
            ),
            'callcenter-omicall-log' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'omicall', 'log'),
                'pathVars' => array(),
                'method' => 'omicallLog',
                'noLoginRequired' => true,
            ),
        );
    }

    function fptoncallMakecall(ServiceBase $api, array $args)
    {
        $admin = new Administration();
        $admin->retrieveSettings();
        $callcenter_username = $admin->settings['callcenter_username'];
        $callcenter_password = $admin->settings['callcenter_password'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://fti.oncall.vn:8899/api/account/credentials/verify',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "name": "' . $callcenter_username . '",
                "password": "' . $callcenter_password . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        $token = $response['access_token'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://fti.oncall.vn:8899/api/extensions/call',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "src": "' . $args['src'] . '",
                "to": "' . $args['to'] . '",
                "domain": "' . $args['domain'] . '",
                "extension": "' . $args['extension'] . '",
                "auth": "' . $args['auth'] . '",
                "outbound_caller_id": "' . $args['outbound_caller_id'] . '",
                "sendsdp": true
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'access_token: ' . $token
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }

    function getRoutingIncom(ServiceBase $api, array $args)
    {
        $listBean = $this->getBeans($api, array('phoneNumber' => $args['number']));
        if (count($listBean) > 0) {
            $bean = $listBean[0];
            $team_code = $GLOBALS['db']->getOne("select code_prefix from teams where id='{$bean['team_id']}'");
            if ($bean['beanName'] == 'Contacts') {
                $team_code = $team_code . '-DT';
            } else {
                $team_code = $team_code . '-TV';
            }
            $admin = new Administration();
            $admin->retrieveSettings();
            $callcenter_config = $admin->settings['callcenter_config'];
            foreach ($callcenter_config as $conf) {
                if ($conf['chanel'] == $team_code) {
                    echo $conf['ext'];
                    break;
                }
            }
            $team_code = $GLOBALS['db']->getOne("select code_prefix from teams where id='{$bean['team_id']}'");
            foreach ($callcenter_config as $conf) {
                if ($conf['chanel'] == $team_code) {
                    echo $conf['ext'];
                    break;
                }
            }
        }
        die();
    }

    function voiceCloudHangup(ServiceBase $api, array $args)
    {
        $GLOBALS['log']->fatal($args);
        $admin = new Administration();
        $admin->retrieveSettings();
        $config = $admin->settings['callcenter_config'];
        $curl = curl_init();
        foreach ($config as $c) {
            if ($c['ext'] == $args['extension']) {
                $args['dotb_status'] = 'hangup';
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://php2socket.dotb.cloud',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode(array(
                        'to' => $GLOBALS['dotb_config']['unique_key'] . $c['user'],
                        'data' => $args
                    )),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));
                curl_exec($curl);
            }
        }
        curl_close($curl);

        return array('success' => 1);
    }

    function omicallLog(ServiceBase $api, array $args)
    {
        $admin = new Administration();
        $admin->retrieveSettings();
        $config = $admin->settings['callcenter_config'];
        foreach ($config as $c) {
            $args['dotb_status'] = 'log';
            if ($c['ext'] == $args['sip_user']) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://php2socket.dotb.cloud',
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode(array(
                        'to' => $GLOBALS['dotb_config']['unique_key'] . $c['user'],
                        'data' => $args
                    )),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    )
                ));
                curl_exec($curl);
                curl_close($curl);
                break;
            }
        }

        echo json_encode(array('success' => 1));
        exit();
    }

    function voiceCloudLog(ServiceBase $api, array $args)
    {
//        $log = new C_CallLogs();
//        $log->name='voiceCloudlog';
//        $log->description = json_encode($args);
//        $log->save();

        $admin = new Administration();
        $admin->retrieveSettings();
        $config = $admin->settings['callcenter_config'];
        foreach ($config as $c) {
            $args['dotb_status'] = 'log';
            if ($c['ext'] == $args['extension']) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://php2socket.dotb.cloud',
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode(array(
                        'to' => $GLOBALS['dotb_config']['unique_key'] . $c['user'],
                        'data' => $args
                    )),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    )
                ));
                curl_exec($curl);
                curl_close($curl);
                break;
            }
        }

        echo json_encode(array('success' => 1));
        exit();
    }

    function VOIP24hCallmissed(ServiceBase $api, array $args)
    {
        $GLOBALS['log']->fatal($args);
        if ($args['type'] == 'inbound' && $args['disposition'] == 'MISSED') {
            global $db, $timedate, $app_strings;
            $sql = "SELECT id FROM calls WHERE outlook_id = '{$args['id']}' AND deleted = 0 ORDER BY date_modified DESC LIMIT 1";
            $id = $db->getOne($sql);

            if (empty($id)) $call = BeanFactory::newBean('Calls');
            else $call = BeanFactory::getBean('Calls', $id);

            $call->name = $app_strings['LBL_CALL_MISSED'] . ' ' . $args['src'];
            $call->outlook_id = $args['id'];
            $call->direction = 'Inbound';
            $call->status = 'Held';
            $call->call_missed = 1;

            $listBeanids = $this->getBeanFromPhone($args['src']);
            if (count($listBeanids) > 0) {
                $call->parent_type = $listBeanids[0]['module'];
                $call->parent_id = $listBeanids[0]['id'];
                $CBean = BeanFactory::getBean($call->parent_type, $call->parent_id);
                $call->assigned_user_id = $CBean->assigned_user_id;
                $call->team_id = $CBean->team_id;
            }

            $call->call_source = $args['src'];
            $call->call_destination = $args['dst'];
            $call->call_entrysource = 'VOIP24H';
            $call->date_start = gmdate("Y-m-d H:i:s", strtotime($args['calldate']));
            $call->save();

            $nowDb = $timedate->nowDb();
            if ($call->parent_type == 'Leads') {
                $id = create_guid();
                $db->query("delete from calls_leads where call_id='{$call->id}' and lead_id='{$call->parent_id}'");
                $db->query("insert into calls_leads(id, call_id, lead_id, date_modified) values(uuid(),'{$call->id}','{$call->parent_id}','$nowDb')");
            } elseif ($call->parent_type == 'Contacts') {
                $id = create_guid();
                $db->query("delete from calls_contacts where call_id='{$call->id}' and contact_id='{$call->parent_id}'");
                $db->query("insert into calls_contacts(id, call_id, contact_id, date_modified) values(uuid(),'{$call->id}','{$call->parent_id}','$nowDb')");
            }
        }
        return array('success' => 1);
    }

    function voiceCloudCall(ServiceBase $api, array $args)
    {
        return array('success' => 1);
    }

    function voiceCloudExtCallcomming(ServiceBase $api, array $args)
    {
        $phones = array(
            '02873007638',
            '02873007147',
            '02873002126',
            '02873003226',
            '02873007996'
        );
        if (!in_array($args['did'], $phones)) {
            echo 0;
            die();
        }
        $beans = $this->getBeanFromPhone($args['phone']);
        if (count($beans) == 0) {
            echo 0;
            die();
        }

        foreach ($beans as $b) {
            if ($b['module'] == 'Leads') $bean = BeanFactory::getBean('Leads', $b['id']);
            elseif ($b['module'] == 'Contacts') $bean = BeanFactory::getBean('Contacts', $b['id']);
            elseif ($b['module'] == 'Accounts') $bean = BeanFactory::getBean('Accounts', $b['id']);
            elseif ($b['module'] == 'Prospects') $bean = BeanFactory::getBean('Prospects', $b['id']);
            $admin = new Administration();
            $admin->retrieveSettings();
            $config = $admin->settings['callcenter_config'];

            foreach ($config as $c)
                if ($c['user'] == $bean->assigned_user_id) {
                    echo $c['ext'];
                    die();
                }
        }

        echo 0;
        die();
    }

    function voiceCloudCallcomming(ServiceBase $api, array $args)
    {
        $admin = new Administration();
        $admin->retrieveSettings();
        $config = $admin->settings['callcenter_config'];
        foreach ($config as $c) {
            if ($c['ext'] == $args['extension']) {
                $args['dotb_status'] = 'callin';
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://php2socket.dotb.cloud',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode(array(
                        'to' => $GLOBALS['dotb_config']['unique_key'] . $c['user'],
                        'data' => $args
                    )),
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json'
                    ),
                ));
                curl_exec($curl);
                curl_close($curl);
                break;
            }
        }

        return array('success' => 1);
    }

    function oncallClickToCall(ServiceBase $api, array $args)
    {
        $GLOBALS['log']->fatal(array(
            'success' => 1
        ));
        require_once 'custom/include/guzzlehttp/vendor/autoload.php';
        $client = new GuzzleHttp\Client(['base_uri' => 'http://118.68.169.39:8899']);
        $r = $client->post('api/extensions/call', array(
            'body' => json_encode($args['data'])
        ));
        $GLOBALS['log']->fatal($r);

        return array('success' => 1);
    }

    function cloudfoneWebhook(ServiceBase $api, array $args)
    {
        $GLOBALS['log']->fatal($args);
        $admin = new Administration();
        $admin->retrieveSettings();
        $callcenterSupplier = $admin->settings['callcenter_supplier'];
        $callcenterPort = $admin->settings['callcenter_port'];
        if ($callcenterSupplier != 'Cloudfone') return array('success' => 0);

        include 'custom/include/socket.io.class.php';
        $socket = new SocketIO('localhost', ((int)$callcenterPort - 1));
        $callcenterSettings = $admin->settings['callcenter_config'];

        if (in_array($args['Status'], array('Ringing'))) {
            $args['dotb_direction'] = 'Outbound';
            $args['dotb_call_status'] = 'Waiting';

            foreach ($callcenterSettings as $st) {
                if ($st['ext'] == $args['CallNumber']) {
                    $args['dotb_user'] = $st['user'];
                    break;
                }
            }

            $args['dotb_source'] = 'Cloudfone';

            $socket->send(array(
                'receiver_id' => $args['dotb_user'],
                'data' => $args
            ));
        }
        return array('success' => 1);
    }

    /**
     * @param ServiceBase $api
     * @param array $args : dotb_ext, dotb_phone, dotb_call_status,dotb_direction
     * @return |null
     */
    function oncallWebhook(ServiceBase $api, array $args)
    {
        $admin = new Administration();
        $admin->retrieveSettings();
        $callcenterSupplier = $admin->settings['callcenter_supplier'];
        $callcenterPort = $admin->settings['callcenter_port'];
        if ($callcenterSupplier != 'Oncall') return null;

        include 'custom/include/socket.io.class.php';
        $socket = new SocketIO('localhost', ((int)$callcenterPort - 1));
        $callcenterSettings = $admin->settings['callcenter_config'];

        $args['dotb_ext'] = false;
        if (in_array($args['event_type'], array('call_established', 'call_ended', 'call_start'))) {
            $matchr = array();
            preg_match('/sip:([0-9]+)@/', $args['caller'], $matchr);
            $matche = array();
            preg_match('/sip:([0-9]+)@/', $args['callee'], $matche);

            if (strlen($matchr[1]) > 4) {
                $args['dotb_phone'] = $matchr[1];
                $args['dotb_direction'] = 'Inbound';
            } else {
                $args['dotb_ext'] = $matchr[1];
                $args['dotb_direction'] = 'Outbound';
            }

            if (strlen($matche[1]) > 4) $args['dotb_phone'] = $matche[1];
            else $args['dotb_ext'] = $matche[1];

            foreach ($callcenterSettings as $st) {
                if ($st['ext'] == $args['dotb_ext']) {
                    $args['dotb_user'] = $st['user'];
                    break;
                }
            }

            //call status
            if ($args['event_type'] == 'call_start') $args['dotb_call_status'] = 'Waiting';
            elseif ($args['event_type'] == 'call_established') $args['dotb_call_status'] = 'Connected';
            elseif ($args['event_type'] == 'call_ended') $args['dotb_call_status'] = 'Hangup';

            //source
            $args['dotb_source'] = 'Oncall';

            $socket->send(array(
                'receiver_id' => $args['dotb_user'],
                'data' => $args
            ));
        }
    }

    /**
     * @param ServiceBase $api
     * @param array $args
     * @return |null
     * [
     * {
     * "pbx_customer_code": "C0581",
     * "secret": "854795eea8ca3f432fa4d53fece48c2f",
     * "callstatus": "Dialing",
     * "calluuid": "1595005183.51414",
     * "direction": "outbound",
     * "callernumber": "101",
     * "destinationnumber": "0336634028",
     * "agentname": "101",
     * "starttime": "20200717T235945",
     * "dnis": "2836222703",
     * "calltype": "Outbound_non-ACD",
     * "version": "4"
     * },
     * {
     * "pbx_customer_code": "C0581",
     * "secret": "854795eea8ca3f432fa4d53fece48c2f",
     * "callstatus": "DialAnswer",
     * "calluuid": "1595005183.51414",
     * "childcalluuid": "1595005185.51415",
     * "callernumber": "2836222703",
     * "destinationnumber": "0336634028",
     * "answertime": "20200717T235956",
     * "version": "4",
     * "direction": "outbound"
     * },
     * {
     * "pbx_customer_code": "C0581",
     * "secret": "854795eea8ca3f432fa4d53fece48c2f",
     * "callstatus": "CDR",
     * "calluuid": "1595005183.51414",
     * "starttime": "20200717T235945",
     * "answertime": "20200717T235956",
     * "endtime": "20200718T000005",
     * "billduration": "9",
     * "totalduration": "20",
     * "disposition": "ANSWERED",
     * "monitorfilename": "/mnt/sipcloud9_1/C0581/callout/20200717/SIP/C0581101-0000c61c-101-1595005183.51414.mp3",
     * "direction": "outbound",
     * "hangup_by": "",
     * "version": "4"
     * }
     * ]
     */
    function worldfoneWebhook(ServiceBase $api, array $args)
    {
        $GLOBALS['log']->fatal($args);
        $admin = new Administration();
        $admin->retrieveSettings();
        $callcenterSupplier = $admin->settings['callcenter_supplier'];
        $callcenterPort = $admin->settings['callcenter_port'];
        if ($callcenterSupplier != 'Worldfone') return null;

        include 'custom/include/socket.io.class.php';
        $socket = new SocketIO('localhost', ((int)$callcenterPort - 1));
        $callcenterSettings = $admin->settings['callcenter_config'];

        if (in_array($args['callstatus'], array('Dialing', 'DialAnswer', 'CDR'))) {
            foreach ($callcenterSettings as $st) {
                if ($st['ext'] == $args['callernumber']) {
                    $args['dotb_user'] = $st['user'];
                    break;
                }
            }
            if (empty($args['dotb_user'])) $args['dotb_user'] = 0;

            //call status
            if ($args['callstatus'] == 'Dialing') $args['dotb_call_status'] = 'Waiting';
            elseif ($args['callstatus'] == 'DialAnswer') $args['dotb_call_status'] = 'Connected';
            elseif ($args['callstatus'] == 'CDR') $args['dotb_call_status'] = 'Hangup';

            //source
            $args['dotb_source'] = 'Worldfone';

            $socket->send(array(
                'receiver_id' => $args['dotb_user'],
                'data' => $args
            ));
        }
    }

    function callApi($method, $url, $params = array(), $header = array("Content-Type: application/json"))
    {
        $auth_request = curl_init($url);
        curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($auth_request, CURLOPT_HEADER, false);
        curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
        if ($method == 'POST') {
            curl_setopt($auth_request, CURLOPT_POST, 1);
        } elseif ($method == 'GET') {
            curl_setopt($auth_request, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($auth_request, CURLOPT_HTTPHEADER, $header);

        if ($method != 'GET') {
            if (count($params) > 0) {
                $json_arguments = json_encode($params);
                curl_setopt($auth_request, CURLOPT_POSTFIELDS, $json_arguments);
            }
        } else {
            $tmp = '?';
            foreach ($params as $key => $value) {
                $tmp = $key . '=' . $value . '&';
            }
            $tmp = rtrim($tmp, '&');
            if ($tmp != '?') {
                $auth_request = curl_init($url . $tmp);
                curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
                curl_setopt($auth_request, CURLOPT_HEADER, false);
                curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($auth_request, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
                curl_setopt($auth_request, CURLOPT_HTTPHEADER, $header);
            }
        }

        $oauth2_token_response = curl_exec($auth_request);
        return $oauth2_token_response;
    }

    function getConfig(ServiceBase $api, array $args)
    {
        $data = array();

        //config
        $admin = new Administration();
        $admin->retrieveSettings();
        $callcenterSettings = $admin->settings['callcenter_config'];
        $callcenterSupplier = $admin->settings['callcenter_supplier'];
        $callcenterPort = $admin->settings['callcenter_port'];
        $callcenterUsername = $admin->settings['callcenter_username'];
        $callcentershow_phone_key_board = $admin->settings['callcenter_show_phone_key_board'];
        $callcentermanual_dial = $admin->settings['callcenter_manual_dial'];
        $callcenterPassword = $admin->settings['callcenter_password'];

        if (!empty($callcenterSettings)) {
            $data['config'] = $callcenterSettings;
        } else {
            $data['config'] = array();
        }

        if (!empty($callcenterSupplier)) {
            $data['supplier'] = $callcenterSupplier;
        } else {
            $data['supplier'] = '';
        }

        if (!empty($callcenterPort)) {
            $data['port'] = $callcenterPort;
        } else {
            $data['port'] = '';
        }

        if (!empty($callcenterUsername)) {
            $data['username'] = $callcenterUsername;
        } else {
            $data['username'] = '';
        }

        if (!empty($callcenterPassword)) {
            $data['password'] = $callcenterPassword;
        } else {
            $data['password'] = '';
        }

        if (!empty($callcentershow_phone_key_board)) {
            $data['show_phone_key_board'] = $callcentershow_phone_key_board;
        } else {
            $data['show_phone_key_board'] = '';
        }

        if (!empty($callcentershow_phone_key_board)) {
            $data['manual_dial'] = $callcentermanual_dial;
        } else {
            $data['manual_dial'] = '';
        }

        //users
        $users = array();
        $result = $GLOBALS['db']->query("select id,user_name,concat(first_name,' ',last_name) as full_name from users where deleted=0 and status='active' order by last_name");
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $users[$row['id']] = $row['user_name'] . ' (' . $row['full_name'] . ')';
        }
        $data['users'] = $users;

        return $data;
    }

    function saveConfig(ServiceBase $api, array $args)
    {
        $admin = new Administration();
        $admin->retrieveSettings();
        $admin->saveSetting('callcenter', 'config', json_encode($args['config']));
        $admin->saveSetting('callcenter', 'supplier', $args['supplier']);
        $admin->saveSetting('callcenter', 'port', $args['port']);
        $admin->saveSetting('callcenter', 'username', $args['username']);
        $admin->saveSetting('callcenter', 'password', $args['password']);
        $admin->saveSetting('callcenter', 'show_phone_key_board', $args['show_phone_key_board']);
        $admin->saveSetting('callcenter', 'manual_dial', $args['manual_dial']);

        return array('success' => 1);
    }

    function getBeanFromPhone($phone)
    {
        global $db;
        $listBeanId = array();

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Contacts');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '$phone'";
        $sql = "select id,'Contacts' as module from contacts where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result)) $listBeanId[] = $row;

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Accounts');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '$phone'";
        $sql = "select id,'Accounts' as module from accounts where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result)) $listBeanId[] = $row;

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Leads');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '$phone'";
        $sql = "select id,'Leads' as module from leads where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result)) $listBeanId[] = $row;

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Prospects');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '$phone'";
        $sql = "select id,'Prospects' as module from prospects where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        while ($row = $db->fetchByAssoc($result)) $listBeanId[] = $row;

        return $listBeanId;
    }

    function getBeans(ServiceBase $api, array $args)
    {
        global $locale, $db;
        $listBean = array();
        if (!empty($args['beanName']) && !empty($args['beanId'])) {
            $bean = BeanFactory::getBean($args['beanName'], $args['beanId']);
            $dataBean = array(
                'id' => $bean->id,
                'beanName' => $args['beanName'],
                'name' => in_array($args['beanName'], array('Prospects', 'Leads', 'Contacts')) ? $locale->getLocaleFormattedName($bean->first_name, $bean->last_name) : $bean->name,
                'status' => empty($bean->status) ? '' : $bean->status,
                'phoneNumber' => empty($args['phoneNumber']) ? (empty($bean->phone_mobile) ? '' : $bean->phone_mobile) : $args['phoneNumber'],
                'email' => empty($bean->email1) ? '' : $bean->email1
            );
            if ($args['beanName'] == 'Accounts') {
                $dataBean['address'] = array(
                    'address_street' => $bean->billing_address_street,
                    'address_state' => $bean->billing_address_state,
                    'address_city' => $bean->billing_address_city,
                    'address_country' => $bean->billing_address_country
                );
            } else {
                $dataBean['address'] = array(
                    'address_street' => $bean->primary_address_street,
                    'address_state' => $bean->primary_address_state,
                    'address_city' => $bean->primary_address_city,
                    'address_country' => $bean->primary_address_country
                );
            }
            $listBean[] = $dataBean;
        } else {
            $listBeanId = array();

            $fieldPhone = array();
            $bean = BeanFactory::newBean('Contacts');
            foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$args['phoneNumber']}'";
            $sql = "select id,'Contacts' as module from contacts where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) $listBeanId[$row['id']] = $row;

            $fieldPhone = array();
            $bean = BeanFactory::newBean('Accounts');
            foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$args['phoneNumber']}'";
            $sql = "select id,'Accounts' as module from accounts where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) $listBeanId[$row['id']] = $row;

            $fieldPhone = array();
            $bean = BeanFactory::newBean('Leads');
            foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$args['phoneNumber']}'";
            $sql = "select id,'Leads' as module from leads where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) $listBeanId[$row['id']] = $row;

            $fieldPhone = array();
            $bean = BeanFactory::newBean('Prospects');
            foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$args['phoneNumber']}'";
            $sql = "select id,'Prospects' as module from prospects where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
            $result = $db->query($sql);
            while ($row = $db->fetchByAssoc($result)) $listBeanId[$row['id']] = $row;

            foreach ($listBeanId as $id => $item) {
                $bean = BeanFactory::getBean($item['module'], $id);
                $dataBean = array(
                    'id' => $bean->id,
                    'team_id' => $bean->team_id,
                    'beanName' => $item['module'],
                    'name' => in_array($item['module'], array('Prospects', 'Leads', 'Contacts')) ? $locale->getLocaleFormattedName($bean->first_name, $bean->last_name) : $bean->name,
                    'status' => empty($bean->status) ? '' : $bean->status,
                    'phoneNumber' => empty($args['phoneNumber']) ? (empty($bean->phone_mobile) ? '' : $bean->phone_mobile) : $args['phoneNumber'],
                    'email' => empty($bean->email1) ? '' : $bean->email1
                );
                if ($item['module'] == 'Accounts') {
                    $dataBean['address'] = array(
                        'address_street' => $bean->billing_address_street,
                        'address_state' => $bean->billing_address_state,
                        'address_city' => $bean->billing_address_city,
                        'address_country' => $bean->billing_address_country
                    );
                } else {
                    $dataBean['address'] = array(
                        'address_street' => $bean->primary_address_street,
                        'address_state' => $bean->primary_address_state,
                        'address_city' => $bean->primary_address_city,
                        'address_country' => $bean->primary_address_country
                    );
                }
                $listBean[] = $dataBean;
            }
        }
        return $listBean;
    }

    function save(ServiceBase $api, array $args)
    {
        global $db, $current_user, $timedate, $app_list_strings;
        $sql = "SELECT id FROM calls WHERE outlook_id = '{$args['callId']}' AND deleted = 0 ORDER BY date_modified DESC LIMIT 1";
        $id = $db->getOne($sql);

        if ($args['direction'] == 'Inbound') {
            $callSubject = "Inbound Call from: " . $args['phoneNumber'];
            $sourceNumber = $args['phoneNumber'];
            $destinationNumber = $args['ext'];
        } else {
            $callSubject = "Outbound Call to: " . $args['phoneNumber'];
            $sourceNumber = $args['ext'];
            $destinationNumber = $args['phoneNumber'];
        }

        if (empty($id)) $call = BeanFactory::newBean('Calls');
        else $call = BeanFactory::getBean('Calls', $id);

        $call->name = $callSubject;
        $call->outlook_id = $args['callId'];
        $call->direction = $args['direction'];
        $call->status = 'Held';
        $call->description = $args['description'];
        if (!empty($args['beanName'])) $call->parent_type = $args['beanName'];
        if (!empty($args['id'])) $call->parent_id = $args['id'];
        $call->call_result = $args['callResult'];
        $call->call_purpose = $args['callPurpose'];
        $call->assigned_user_id = $current_user->id;
        $call->team_id = $current_user->team_id;
        $call->move_trash = $args['deadlead'];
        $call->mark_favorite = $args['favorite'];
        $call->call_source = $sourceNumber;
        $call->call_destination = $destinationNumber;
        $call->call_entrysource = $args['source'];
        $call->duration = (int)$args['duration'];
        if (!empty($args['recording'])) {
            $call->call_recording = $args['recording'];
        }
        if (!empty($args['start'])) $call->date_start = gmdate("Y-m-d H:i:s", strtotime($args['start']));
        else $call->date_start = $timedate->nowDb();
        if (!empty($args['end'])) $call->date_end = gmdate("Y-m-d H:i:s", strtotime($args['end']));

        //Add by HoangHvy
        if (empty($call->date_end)) {
            $tmp = strtotime($call->date_start) + $call->duration;
            $call->date_end = gmdate("Y-m-d H:i:s", $tmp);
        }
        //end
        $call->recall = $args['recall'];
        if ($call->recall != 0 && $call->recall != 99999) {
            $call->recall_at = date("Y-m-d H:i:s", $call->recall + strtotime($call->date_start) + $call->duration);
        } elseif ($call->recall == 0) {
            $call->recall_at = null;
        } else {
            $call->recall_at = $args['recall_at'];
        }
		
		//Customize Jaxtina
		$call->jaxtina_mark_l10 = $args['markl10'];
		//END CUSTOM Jaxtina
        $call->save();

        $nowDb = $timedate->nowDb();
        if ($call->parent_type == 'Leads') {
            $id = create_guid();
            $db->query("delete from calls_leads where call_id='{$call->id}' and lead_id='{$call->parent_id}'");
            $db->query("insert into calls_leads(id, call_id, lead_id, date_modified) values('{$id}','{$call->id}','{$call->parent_id}','{$nowDb}')");
        } elseif ($call->parent_type == 'Contacts') {
            $id = create_guid();
            $db->query("delete from calls_contacts where call_id='{$call->id}' and contact_id='{$call->parent_id}'");
            $db->query("insert into calls_contacts(id, call_id, contact_id, date_modified) values('{$id}','{$call->id}','{$call->parent_id}','{$nowDb}')");
        }
        //return result
        return array(
            "success" => 1
        );
    }

    function getScript(ServiceBase $api, array $args)
    {
        global $current_user;

        $admin = new Administration();
        $admin->retrieveSettings();
        $callcenter_config = $admin->settings['callcenter_config'];
        $callcenter_show_phone_key_board = $admin->settings['callcenter_show_phone_key_board'];
        $callcenter_manual_dial = $admin->settings['callcenter_manual_dial'];
        $callcenter_supplier = $admin->settings['callcenter_supplier'];
        $callcenter_port = $admin->settings['callcenter_port'];
        $callcenter_username = $admin->settings['callcenter_username'];
        $callcenter_password = $admin->settings['callcenter_password'];

        if (!empty($callcenter_supplier)) {
            foreach ($callcenter_config as $config) {
                if ($current_user->id == $config['user']) {
                    if ($callcenter_supplier == 'Asterisk' && !empty($config['ip']) && !empty($config['ext']) && !empty($config['chanel']) && !empty($config['context'])) {
                        //Deleted By Lap Nguyen
                    } elseif ($callcenter_supplier == 'Oncall' && !empty($config['ext']) && !empty($config['ip']) && !empty($config['context'])) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'Oncall',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'port' => $callcenter_port,
                            'username' => $callcenter_username,
                            'password' => $callcenter_password,
                            'callcenter_list_ext' => $callcenter_config,
                            'config' => array(
                                'ext' => $config['ext'],
                                'ip' => $config['ip'],
                                'context' => $config['context'],
                                'port' => $callcenter_port
                            )
                        );
                    } elseif ($callcenter_supplier == 'Voip24h' && !empty($config['ext']) && !empty($callcenter_port) && !empty($callcenter_username) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'Voip24h',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'ip' => $callcenter_port,
                            'key' => $callcenter_username,
                            'secret' => $callcenter_password,
                            'callcenter_list_ext' => $callcenter_config,
                            'ext' => $config['ext']
                        );
                    } elseif ($callcenter_supplier == 'RTC' && !empty($config['ext']) && !empty($callcenter_port) && !empty($callcenter_username) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'RTC',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'ip' => $callcenter_port,
                            'key' => $callcenter_username,
                            'secret' => $callcenter_password,
                            'ext' => $config['ext'],
                            'realm' => $config['ip'],
                            'context' => $config['context'],
                            'callcenter_list_ext' => $callcenter_config,
                            'chanel' => $config['chanel']
                        );
                    } elseif ($callcenter_supplier == 'newRTC' && !empty($config['ext']) && !empty($callcenter_port) && !empty($callcenter_username) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'newRTC',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'ip' => $callcenter_port,
                            'key' => $callcenter_username,
                            'secret' => $callcenter_password,
                            'ext' => $config['ext'],
                            'realm' => $config['ip'],
                            'context' => $config['context'],
                            'callcenter_list_ext' => $callcenter_config,
                            'chanel' => $config['chanel']
                        );
                    } elseif ($callcenter_supplier == 'omicall' && !empty($config['ext']) && !empty($callcenter_port) && !empty($callcenter_username) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'omicall',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'ip' => $callcenter_port,
                            'key' => $callcenter_username,
                            'secret' => $callcenter_password,
                            'ext' => $config['ext'],
                            'realm' => $config['ip'],
                            'context' => $config['context'],
                            'callcenter_list_ext' => $callcenter_config,
                            'chanel' => $config['chanel']
                        );
                    } elseif ($callcenter_supplier == 'Worldfone' && !empty($config['ext']) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'Worldfone',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'secret' => $callcenter_password,
                            'ext' => $config['ext'],
                            'port' => $callcenter_port,
                            'callcenter_list_ext' => $callcenter_config,
                        );
                    } elseif ($callcenter_supplier == 'Cloudfone' && !empty($config['ext']) && !empty($config['chanel']) && !empty($callcenter_port) && !empty($callcenter_username) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'Cloudfone',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'key' => $callcenter_username,
                            'secret' => $callcenter_password,
                            'ext' => $config['ext'],
                            'service_name' => $config['chanel'],
                            'port' => $callcenter_port,
                            'callcenter_list_ext' => $callcenter_config,
                        );
                    } elseif ($callcenter_supplier == 'VoiceCloud' && !empty($config['ext']) && !empty($callcenter_port) && !empty($callcenter_username) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'VoiceCloud',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'domain' => $callcenter_username,
                            'key' => $callcenter_password,
                            'ext' => $config['ext'],
                            'callcenter_domain' => $callcenter_port,
                            'callcenter_list_ext' => $callcenter_config,
                        );
                    } elseif ($callcenter_supplier == 'VoiceCloudV2' && !empty($config['ext']) && !empty($callcenter_port) && !empty($callcenter_username) && !empty($callcenter_password)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'VoiceCloudV2',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'domain' => $callcenter_username,
                            'key' => $callcenter_password,
                            'ext' => $config['ext'],
                            'callcenter_domain' => $callcenter_port,
                            'callcenter_list_ext' => $callcenter_config,
                        );
                    } elseif ($callcenter_supplier == 'SouthTelecom' && !empty($config['ext']) && !empty($callcenter_password) && !empty($callcenter_port)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'SouthTelecom',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'secret' => $callcenter_password,
                            'ext' => $config['ext'],
                            'callcenter_domain' => $callcenter_port,
                            'callcenter_list_ext' => $callcenter_config,
                        );
                    } elseif ($callcenter_supplier == 'FPTOncall' && !empty($config['ext']) && !empty($callcenter_password) && !empty($callcenter_port)) {
                        foreach ($callcenter_config as $tmpkey => $tmp) {
                            $callcenter_config[$tmpkey]['user_name'] = $GLOBALS['db']->getOne("select user_name from users where id='{$tmp['user']}'");
                        }
                        return array(
                            'success' => 1,
                            'supplier' => 'FPTOncall',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'ip' => $callcenter_port,
                            'ext' => $config['ext'],
                            'realm' => $config['ip'],
                            'context' => $config['context'],
                            'callcenter_list_ext' => $callcenter_config,
                            'chanel' => $config['chanel']
                        );
                    } elseif ($callcenter_supplier == 'VCS' && !empty($config['ext'])) {
                        return array(
                            'success' => 1,
                            'supplier' => 'VCS',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                            'ext' => $config['ext']
                        );
                    } elseif ($callcenter_supplier == 'VCS3CX' && !empty($config['ext'])) {
                        return array(
                            'success' => 1,
                            'supplier' => 'VCS3CX',
                            'show_phone_key_board' => $callcenter_show_phone_key_board,
                            'manual_dial' => $callcenter_manual_dial,
                        );
                    }
                }
            }
        }
        return array(
            'success' => 0
        );
    }
}
