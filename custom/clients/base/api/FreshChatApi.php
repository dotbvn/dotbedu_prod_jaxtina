<?php
/**
 * @author Tran Le Khanh Hong
 */

class FreshChatAPI extends DotbApi
{
    function registerApiRest(): array
    {
        return array(
            'freshchat-webhook-get' => array(
                'reqType' => 'GET',
                'path' => array('freshchat', 'webhook'),
                'pathVars' => array(),
                'method' => 'freshChatWebhook',
                'noLoginRequired' => true,
            ),
            'freshchat-webhook-post' => array(
                'reqType' => 'POST',
                'path' => array('freshchat', 'webhook'),
                'pathVars' => array(),
                'method' => 'freshChatWebhook',
                'noLoginRequired' => true,
            ),
            'freshchat-webhook-put' => array(
                'reqType' => 'PUT',
                'path' => array('freshchat', 'webhook'),
                'pathVars' => array(),
                'method' => 'freshChatWebhook',
                'noLoginRequired' => true,
            ),
        );
    }

    function freshChatWebhook(ServiceBase $api, array $args): array
    {
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', '1');

        $event = $args['action'];

        switch ($event){
            case 'message_create':
                break;
            case 'conversation_assignment':
                break;
            case 'conversation_resolution':
                $userData = $args['data']['resolve']['user'];
                if (!empty($userData)){
                    $firstName = $userData['first_name'];
                    $lastName = $userData['last_name'];
                    $email = $userData['email'];
                    $phone = $userData['phone'];
                    $userId = $userData['id'];
                    $properties = $userData['properties'];
                    $descriptionProps = $this->getDescriptionProperties($properties, $userId);
                    if (!empty($firstName) && !empty($email) && !empty($phone)){
                       $dupLeadId = $this->checkLeadDuplicate($email, $phone, $firstName, $lastName);
                        if (!empty($dupLeadId)){
                            $beanDup = BeanFactory::getBean('Leads', $dupLeadId);
                            if (!empty($beanDup)){
                                // Update description (properties)
                                $currentDes = $beanDup->description;
                                if (!empty($currentDes)){
                                    $beanDup->description = $currentDes . "\r\n" . $descriptionProps;
                                }else{
                                    $beanDup->description = $descriptionProps;
                                }
                                $beanDup->save();
                            }
                        }else{
                           $this->newLead($email, $phone, $descriptionProps, $firstName, $lastName);
                        }
                    }
                }
                break;
            case 'conversation_reopen':
                break;
            default:

        }

        echo '{"error_info": {"errno": 1,"error": "Success"}}';
        die();
    }

    function checkLeadDuplicate($email, $phone, $firstName, $lastName = ""): string
    {
        $lead = new Lead();
        $lead->disable_row_level_security = true;
        $lead->id = '';
        $lead->email1 = $email;
        $lead->phone_mobile = $phone;
        $lead->first_name = $firstName;
        $lead->last_name = empty($lastName) ? "Unknow" : $lastName;
        $duplicates = $lead->findDuplicates();
        if (count($duplicates['records']) > 0) {
            return $duplicates['records'][0]['id'];
        }else{
            return "";
        }
    }

    function newLead($email, $phone, $description, $firstName, $lastName = "")
    {
        $newBean = BeanFactory::getBean('Leads');
        $newBean->email1 = $email;
        $newBean->phone_mobile = $phone;
        $newBean->first_name = $firstName;
        $newBean->last_name = empty($lastName) ? "Unknow" : $lastName;
        $newBean->description = $description;
        $newBean->save();
    }

    function getDescriptionProperties($properties, $userId): string
    {
        $des = "";
        foreach ($properties as $property){
            if (!empty($des)){
                $des .= ", ";
            }
            $des .= $property['name'] . ': ' . $property['value'];
        }
        $des .= ', user_id: ' . $userId;
        return $des;
    }
}
