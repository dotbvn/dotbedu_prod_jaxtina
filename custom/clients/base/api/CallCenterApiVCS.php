<?php

class CallCenterApiVCS extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'callcenter-vcs-webhook-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'vcs', 'webhook'),
                'pathVars' => array(),
                'method' => 'vcsWebhook',
                'noLoginRequired' => true,
            ),
            'callcenter-vcs-webhook-post' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'vcs', 'webhook'),
                'pathVars' => array(),
                'method' => 'vcsWebhook',
                'noLoginRequired' => true,
            ),
            'callcenter-vcs-click2call' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'vcs', 'click2call'),
                'pathVars' => array(),
                'method' => 'click2call'
            ),
            'callcenter-vcs-webhook-report' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'vcs', 'report'),
                'pathVars' => array(),
                'method' => 'vcsWebhookReport',
                'noLoginRequired' => true,
            ),
            'callcenter-vcs-webhook-report-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'vcs', 'report'),
                'pathVars' => array(),
                'method' => 'vcsWebhookReport',
                'noLoginRequired' => true,
            ),
            'callcenter-vcs-webhook-report_3cx' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'vcs', 'report', '3cx'),
                'pathVars' => array(),
                'method' => 'vcsWebhookReport3CX',
                'noLoginRequired' => true,
            ),
            'callcenter-vcs-webhook-report-get_3cx' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'vcs', 'report', '3cx'),
                'pathVars' => array(),
                'method' => 'vcsWebhookReport3CX',
                'noLoginRequired' => true,
            ),
        );
    }

    function vcsWebhookReport3CX(ServiceBase $api, array $args)
    {
        global $timedate, $db;
        $GLOBALS['log']->fatal($args);

        $id = $GLOBALS['db']->getOne("select id from calls where outlook_id='{$args['CallID']}' and deleted=0");
        if (empty($id)) {
            $call = BeanFactory::newBean('Calls');
            $call->disable_row_level_security = true;
            if ($args['Direction'] != 'Incoming') {
                $direction = 'Outbound';
                $callSubject = "Outbound Call to: " . $args['NumberPhone'];
                $sourceNumber = $args['Extension'];
                $destinationNumber = $args['NumberPhone'];
            } else {
                $callSubject = "Inbound Call from: " . $args['NumberPhone'];
                $direction = 'Inbound';
                $sourceNumber = $args['NumberPhone'];
                $destinationNumber = $args['Extension'];
            }

            $call->name = $callSubject;
            $call->outlook_id = $args['CallID'];
            $call->direction = $direction;
            $call->status = 'Held';
            $call->description = '';

            $relateBean = $this->getBeanFromPhone($args['Extension']);
            if ($relateBean !== false) {
                $call->parent_type = $relateBean['module'];
                $call->parent_id = $relateBean['id'];
            } else {
                return array('success' => 1, 'message' => 'not save log call');
            }

            if ($args['CallStatus'] != 'Answer') $call->call_result = 'Busy/No Answer';
            else $call->call_result = '';

            $call->call_purpose = '';

            $admin = new Administration();
            $admin->retrieveSettings();
            $callcenter_config = $admin->settings['callcenter_config'];
            foreach ($callcenter_config as $config) {
                if ($args['Extension'] == $config['ext']) {
                    $user = BeanFactory::getBean('Users', $config['user']);
                    $call->assigned_user_id = $user->id;
                    $call->team_id = $user->team_id;
                    break;
                }
            }

            $call->move_trash = 0;
            $call->mark_favorite = 0;
            $call->call_source = $sourceNumber;
            $call->call_destination = $destinationNumber;
            $call->call_entrysource = 'VCS3CX';
            if ($args['CallStatus'] == 'Answer') {
                $t = explode(':', $args['TalkingTime']);
                $call->duration = (int)$t[0] * 3600 + (int)$t[1] * 60 + (int)$t[0];
                $call->duration_hours = $t[0];
                $call->duration_minutes = $t[1];
            } else {
                $call->duration = 0;
                $call->duration_hours = 0;
                $call->duration_minutes = 0;
            }
            $call->call_recording = $args['Recording'];
            $call->date_start = $timedate->nowDb();
            $call->date_end = gmdate("Y-m-d H:i:s", strtotime($call->date_start) + $call->duration);
            $call->recall = 0;
            $call->recall_at = null;
            $call->save();

            $nowDb = $timedate->nowDb();
            if ($call->parent_type == 'Leads') {
                $id = create_guid();
                $db->query("delete from calls_leads where call_id='{$call->id}' and lead_id='{$call->parent_id}'");
                $db->query("insert into calls_leads(id, call_id, lead_id, date_modified) values(uuid(),'{$call->id}','{$call->parent_id}','{$nowDb}')");
            } elseif ($call->parent_type == 'Contacts') {
                $id = create_guid();
                $db->query("delete from calls_contacts where call_id='{$call->id}' and contact_id='{$call->parent_id}'");
                $db->query("insert into calls_contacts(id, call_id, contact_id, date_modified) values(uuid(),'{$call->id}','{$call->parent_id}','{$nowDb}')");
            }
        } else {
            $bean = BeanFactory::getBean('Calls', $id);
            $bean->call_recording = $args['Recording'];
            $bean->save();
        }

        return array('success' => 1, 'data' => $args);
    }

    function click2call(ServiceBase $api, array $args)
    {
        shell_exec("bash vcs-click2call.sh " . $args['ext'] . ' ' . $args['phone']);
        return array('success' => 1);
    }

    function vcsWebhook(ServiceBase $api, array $args)
    {
        $GLOBALS['log']->fatal($args);
        include 'custom/include/socket.io.class.php';

        $data = [
            'supplier' => 'VCS',
            'from' => $args['data']['from'],
            'to' => $args['data']['to'],
            'direction' => strlen($args['data']['from']) < 5 ? 'out' : 'in',
            'event' => $args['data']['event'] == 'busy_reject' ? 'busy' : $args['data']['event'],
            'callid' => $args['data']['callid'],
            'time' => $args['data']['time'],
            'recording' => ''
        ];
        $socket = new SocketIO('serv.dotb.cloud', 3008);
        $socket->send(array(
            'data' => $data
        ));

        $socket = new SocketIO('localhost', 3006);
        $socket->send(array(
            'data' => $args
        ));

        return array('success' => 1);
    }

    function getBeanFromPhone($phone)
    {
        global $db;

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Contacts');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$phone}'";
        $sql = "select id,'Contacts' as module from contacts where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        if ($row = $db->fetchByAssoc($result)) return $row;

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Accounts');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$phone}'";
        $sql = "select id,'Accounts' as module from accounts where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        if ($row = $db->fetchByAssoc($result)) return $row;

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Leads');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$phone}'";
        $sql = "select id,'Leads' as module from leads where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        if ($row = $db->fetchByAssoc($result)) return $row;

        $fieldPhone = array();
        $bean = BeanFactory::newBean('Prospects');
        foreach ($bean->field_defs as $field) if ($field['type'] == 'phone') $fieldPhone[] = "{$field['name']} like '{$phone}'";
        $sql = "select id,'Prospects' as module from prospects where deleted=0 and (" . implode(' OR ', $fieldPhone) . ")";
        $result = $db->query($sql);
        if ($row = $db->fetchByAssoc($result)) return $row;

        return false;
    }

    function vcsWebhookReport(ServiceBase $api, array $args)
    {
        global $timedate, $db;
        $GLOBALS['log']->fatal($args);
        if (empty($args['call_id']) || (strlen($args['to']) <= 5 && strlen($args['from']))) {
            return array('success' => 1, 'message' => 'not save log call');
        }

        $id = $GLOBALS['db']->getOne("select id from calls where outlook_id='{$args['call_id']}' and deleted=0");
        if (empty($id)) {
            $call = BeanFactory::newBean('Calls');
            $call->disable_row_level_security = true;
            $sourceNumber = $args['from'];
            $destinationNumber = $args['to'];
            if ($args['direction'] == 'out') {
                $direction = 'Outbound';
                $callSubject = "Outbound Call to: " . $args['to'];
            } else {
                $callSubject = "Inbound Call from: " . $args['from'];
                $direction = 'Inbound';
            }

            $call->name = $callSubject;
            $call->outlook_id = $args['call_id'];
            $call->direction = $direction;
            $call->status = 'Held';
            $call->description = '';

            $relateBean = $this->getBeanFromPhone($sourceNumber);
            if ($relateBean !== false) {
                $call->parent_type = $relateBean['module'];
                $call->parent_id = $relateBean['id'];
            } else {
                return array('success' => 1, 'message' => 'not save log call');
            }

            if ($args['status'] == 'abandon') $call->call_result = 'Busy/No Answer';
            else $call->call_result = '';

            $call->call_purpose = '';

            $admin = new Administration();
            $admin->retrieveSettings();
            $callcenter_config = $admin->settings['callcenter_config'];
            foreach ($callcenter_config as $config) {
                if (($call->direction == 'Outbound' && $sourceNumber == $config['ext'])
                    || ($call->direction == 'Inbound' && $destinationNumber == $config['ext'])) {
                    $user = BeanFactory::getBean('Users', $config['user']);
                    $call->assigned_user_id = $user->id;
                    $call->team_id = $user->team_id;
                    break;
                }
            }

            $call->move_trash = 0;
            $call->mark_favorite = 0;
            $call->call_source = $sourceNumber;
            $call->call_destination = $destinationNumber;

            $call->call_entrysource = 'VCS';
            if ($args['call_duration'] == '00:00:00' || $args['end_time'] == 0) {
                $call->duration = 0;
            } else {
                $call->duration = (int)$args['end_time'] / 1000 - (int)$args['start_time'] / 1000;
            }
            $call->call_recording = '';

            if (!empty($args['start_time'])) $call->date_start = gmdate("Y-m-d H:i:s", (int)$args['start_time'] / 1000);
            else $call->date_start = $timedate->nowDb();

            $call->date_end = gmdate("Y-m-d H:i:s", (int)$args['start_time'] / 1000 + $call->duration);

            $call->call_duration = $call->duration;
            $call->duration_hours = (int)date("H", $call->duration);
            $call->duration_minutes = (int)date("i", $call->duration);
            $call->recall = 0;
            $call->recall_at = null;

            $call->save();

            $nowDb = $timedate->nowDb();
            if ($call->parent_type == 'Leads') {
                $id = create_guid();
                $db->query("delete from calls_leads where call_id='{$call->id}' and lead_id='{$call->parent_id}'");
                $db->query("insert into calls_leads(id, call_id, lead_id, date_modified) values('{$id}','{$call->id}','{$call->parent_id}','{$nowDb}')");
            } elseif ($call->parent_type == 'Contacts') {
                $id = create_guid();
                $db->query("delete from calls_contacts where call_id='{$call->id}' and contact_id='{$call->parent_id}'");
                $db->query("insert into calls_contacts(id, call_id, contact_id, date_modified) values('{$id}','{$call->id}','{$call->parent_id}','{$nowDb}')");
            }
        }

        return array('success' => 1, 'data' => $args);
    }
}
