<?php

class RandomNewsViewsApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'c_admin_config_update_random_news_views' => array(
                'reqType' => 'POST',
                'path' => array('c_admin_config', 'update_random_news_views'),
                'pathVars' => array(''),
                'method' => 'updateRandomNewsViews'
            ),
        );
    }

    function updateRandomNewsViews(ServiceBase $api, array $data)
    {
        $sql_update_views = "UPDATE c_news 
                            SET addon_views=FLOOR(RAND()*({$data['random_number_to']}-{$data['random_number_from']}+1)+{$data['random_number_from']}) 
                            WHERE deleted=0";
        $GLOBALS['db']->query($sql_update_views);

        $arrResultViews = array();

        $qContactsViewNews = "SELECT c_news_contacts_1c_news_ida,SUM(count_read_news) AS totalContactViews FROM c_news_contacts_1_c WHERE deleted = 0 GROUP BY c_news_contacts_1c_news_ida";
        $rContactsViews = $GLOBALS['db']->fetchArray($qContactsViewNews);

        $qAddonViewNews = "SELECT id,addon_views FROM c_news WHERE deleted = 0";
        $rAddonViews = $GLOBALS['db']->fetchArray($qAddonViewNews);

        foreach ($rAddonViews as $key => $rAddonViewsValue) {
            foreach ($rContactsViews as $keyCV => $rContactsViewsValue) {
                if ($rAddonViewsValue['id'] == $rContactsViewsValue['c_news_contacts_1c_news_ida']) {
                    $totalViews = $rAddonViewsValue['addon_views'] + $rContactsViewsValue['totalContactViews'];
                    $arrResultViews[$rAddonViewsValue['id']] = $totalViews;
                }
            }
        }
        foreach ($arrResultViews as $keyIdNews => $rTotalNewsViews) {
            $GLOBALS['db']->query("update c_news set views='{$rTotalNewsViews}' where id='{$keyIdNews}'");
        }

        return array('success' => 1);
    }
}
