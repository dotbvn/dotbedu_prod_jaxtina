<?php
class CallCenterApiFocus extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'callcenter-st-webhook-get' => array(
                'reqType' => 'GET',
                'path' => array('callcenter', 'st', 'webhook'),
                'pathVars' => array(),
                'method' => 'stWebhook',
                'noLoginRequired' => true,
            ),
            'callcenter-st-webhook-post' => array(
                'reqType' => 'POST',
                'path' => array('callcenter', 'st', 'webhook'),
                'pathVars' => array(),
                'method' => 'stWebhook',
                'noLoginRequired' => true,
            )
        );
    }

    function stWebhook(ServiceBase $api, array $args)
    {
        //$GLOBALS['log']->fatal($args);

        include 'custom/include/socket.io.class.php';
        $socket = new SocketIO('localhost', 3004);
        $socket->send(array(
            'data' => $args
        ));

        return array('success' => 1);
    }
}
