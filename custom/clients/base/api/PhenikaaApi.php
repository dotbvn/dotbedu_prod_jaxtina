<?php
//Hanlde Phenikaa API - Hoang Hvy


class PhenikaaApi extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'phenikaa_webhook_get' => array(
                'reqType' => 'GET',
                'path' => array('phenikaa', 'webhook'),
                'pathVars' => array(),
                'method' => 'phenikaaWebhook',
                'noLoginRequired' => true
            ),
            'phenikaa_webhook_post' => array(
                'reqType' => 'POST',
                'path' => array('phenikaa', 'webhook'),
                'pathVars' => array(),
                'method' => 'phenikaaWebhook',
                'noLoginRequired' => true
            )
        );
    }

    function phenikaaWebhook(ServiceBase $api, array $args)
    {
        global $current_user;
        //Get User apps_admin
        $current_user = new User();
        $current_user->retrieve_by_string_fields(array('user_name' => 'apps_admin'));

        //Get User admin
        if (empty($current_user->id)) {
            $current_user = new User();
            $current_user->retrieve_by_string_fields(array('user_name' => 'admin'));
        }

        if (empty($current_user->id)) return array('success' => 0, 'message_code' => 'apps_admin_not_found', 'message' => 'User Apps Admin does not found!');

        if ($args['error'] == 0) {

                $record = BeanFactory::getBean('J_CheckInOut');
                $record->verify_status = $args['type'] == "StrangerCap" ? -1 : 0;
                $record->name = $args['data']['RecordID'];
                $record->student_id = $args['data']['customId'];
                $q = "SELECT IFNULL(id,'') id, IFNULL(team_id, '') team_id FROM j_attdevice WHERE device_id = '{$args['data']['facesluiceId']}' AND deleted = 0";
                $result = $GLOBALS['db']->fetchOne($q);
                $record->device_id = $result['id'];
                $record->team_id = $result['team_id'];
                $record->time_check = date('Y-m-d H:i:s', strtotime($args['data']['time'] . '-7 hours'));
                $record->similarity_percent = round($args['data']['similarity1'], 2);
                $base64 ='';
                $q1 = "SELECT
                        IFNULL(contacts.full_student_name, '') full_student_name
                        FROM contacts WHERE contacts.id = '{$record->student_id}' AND contacts.deleted = 0";
                $row1 = $GLOBALS['db']->getOne($q1);
                $record->student_name = $row1;
                if (strpos($args['data']['pic'], 'data:image/') !== false) {
                    $base64 = explode(',', $args['data']['pic'], 2)[1];
                }
                $this->saveImage($record, $base64);
                $record->save();

        }
        return array('success' => 1);
    }

    function saveImage(&$bean, $base64){;
        if (!empty($base64)) {
            $bean->pic_id = create_guid();
            $upload_file = new UploadFile('pic_id');
            $image_data = base64_decode($base64);
            $upload_file->set_for_soap($bean->pic_id, $image_data);
            $des = 'FaceID/CheckInOutImage/'.$bean->pic_id;
            $upload_file->final_move($des);
        }
    }
}
