<?php

class copyDashboad extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'copy_dashboard' => array(
                'reqType' => 'POST',
                'path' => array('copy_dashboard'),
                'pathVars' => array(''),
                'method' => 'copyUser',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'set_dashboard_id' => array(
                'reqType' => 'POST',
                'path' => array('set_dashboard_id'),
                'pathVars' => array(''),
                'method' => 'setDashboardId',
                'shortHelp' => '',
                'longHelp' => '',
            ),
            'check_dashboard' => array(
                'reqType' => 'POST',
                'path' => array('check_dashboard'),
                'pathVars' => array(''),
                'method' => 'checkDashboard',
                'shortHelp' => '',
                'longHelp' => '',
            ),

        );
    }

    function copyUser(ServiceBase $api, array $args)
    {
        global $db;
        $listUserId = [];
        if ($args['moduleName'] == "Teams") {
            $bean = BeanFactory::retrieveBean($args['moduleName'], $args['model']['id']);
            $users = $bean->get_team_members();
            foreach ($users as $user) {
                if ($user->id) {
                    $listUserId[] = $user->id;
                }
            }
        } elseif ($args['moduleName'] == "ACLRoles") {
            $stringId = '';
            foreach ($args['model'] as $role) {
                $stringId .= "'" . $role['id'] . "', ";
            }
            $sql = "Select user_id from acl_roles_users where role_id in(" . $stringId . "'') and deleted = 0";
            $result = $db->fetchArray($sql);
            foreach ($result as $user) {
                $listUserId[] = $user['user_id'];
            }
        } else {
            foreach ($args['model'] as $user) {
                $listUserId[] = $user['id'];
            }
        }
        if (!empty($listUserId)) {
            foreach($listUserId as $userId){
                $user = BeanFactory::retrieveBean('Users', $userId);

                $dash = BeanFactory::getBean('Dashboards', $args['idDashboard']);
                $dupDash = BeanFactory::newBean('Dashboards');
                $dupDash->name = $dash->name;
                $dupDash->description = $dash->description;
                $dupDash->dashboard_module = $dash->dashboard_module;
                $dupDash->view_name = $dash->view_name;
                $dupDash->metadata = $dash->metadata;
                $dupDash->default_dashboard = $dash->default_dashboard;
                $dupDash->team_id = $user->default_team;
                $dupDash->team_set_id = $user->team_set_id;
                $dupDash->assigned_user_id = $user->id;
                $dupDash->copy_from_id = $dash->id;
                $dupDash->save();

                //Set deafult old dashboard to 0
                $dash->default_dashboard = 0;
                $dash->save();

                $user->setPreference('dashboard_id', $dupDash->id, $nosession = 0, $category = 'global');
                $user->savePreferencesToDB();
            }
        }
    }
    function setDashboardId(ServiceBase $api, array $args){
        $user = BeanFactory::retrieveBean('Users', $args['idUser']);
        $user->setPreference('dashboard_id', $args['idDashboard'], $nosession = 0, $category = 'global');
        $user->savePreferencesToDB();
    }



}
