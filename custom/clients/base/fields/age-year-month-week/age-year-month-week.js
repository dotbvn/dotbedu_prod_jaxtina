({
    ageYearMonthWeekValue: "No Data",
    initialize: function (options) {
        this._super('initialize', [options]);
        var ageYearMonthWeek = setInterval(()=>{
            if (this.model.attributes.birthdate !== undefined){
                const diff = new Date(new Date() - new Date(this.model.attributes.birthdate));
                const year = (diff.getFullYear() - 1970).toString() + ' ' + App.lang.getAppString('LBL_YEAR');
                const month = diff.getUTCMonth().toString() + ' ' + App.lang.getAppString('LBL_MONTH');
                const week = Math.floor((diff.getUTCDate() - 1) / 7).toString() + ' ' + App.lang.getAppString('LBL_WEEK');
                this.ageYearMonthWeekValue = year + ' ' + month + ' ' + week;
                console.log(this.ageYearMonthWeekValue);
                this.render();
                clearInterval(ageYearMonthWeek);
            }
        }, 1000);
    }
})
