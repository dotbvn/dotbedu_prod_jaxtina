({
    extendsFrom: 'FieldsetField',

    events: {
        'click .customIcon': 'displayMap',
    },

    currentFocus : -1,

    initialize: function (options) {
        this._super('initialize', [options]);
        this.fields = [];

        var inlineTag = this.def.inline ? '-inline' : '';
        this.def.css_class = (this.def.css_class ? this.def.css_class + ' fieldset' :
            'fieldset') + inlineTag;

        if (this.def.equal_spacing && this.def.inline) {
            this.def.css_class += ' fieldset-equal';
        }
    },

    _getFallbackTemplate: function (viewName) {
        if (_.contains(this.fallbackActions, viewName)) {
            return viewName;
        }
        if (app.template.get('f.' + this.type + '.' + this.view.fallbackFieldTemplate)) {
            return this.view.fallbackFieldTemplate;
        }
        return 'detail';
    },

    _loadTemplate: function () {
        this._super('_loadTemplate');

        if ((this.view.name === 'record' || this.view.name === 'create'
                || this.view.name === 'create-nodupecheck' || this.view.name === 'pmse-case')
            && this.type === 'fieldset' && !_.contains(this.fallbackActions, this.action)) {

            this.template = app.template.getField('fieldset', 'record-detail', this.model.module);
        }
    },
    showNoData: function () {

        if (!this.def.readonly) {
            return false;
        }

        return !_.some(this.fields, function (field) {
            return field.name && field.model.has(field.name);
        });
    },

    _render: function () {

        var fields = this._getChildFields();
        _.each(fields, function (field) {
            field.placeholder = field.getPlaceholder();
        }, this);

        this.focusIndex = 0;

        this._super('_render');

        this._renderFields(fields);

        /// Lê Minh custom
        /// Start
        let self = this;

        if (this.action === "detail" && fields.length >= 4 && this.def.css_class === 'address') {
            if (fields[4]["def"]["css_class"] === "address_street") {
                if (fields[4].value !== null) {
                    var checkExistDateModified = setInterval(function () {
                        if ($("div[data-name='date_entered'] span div").prop("title")) {
                            if (fields[4].$el != null) {
                                if (fields[4].$el.find("div").length !== 0) {
                                    valuePara = fields[4].value.long;
                                    outHTML =
                                        '<span class="" style="position: absolute;" tabindex="-1">\n' +
                                        '<a class="customIcon" data-address="' + valuePara + '">' +
                                        '<i style="cursor: pointer;font-size:20px; color: #193059; transition: all .2s ease-in-out;" class="customIcon far fa-map-marked-alt"></i>' +
                                        '</a>\n' +
                                        '</span>';
                                    fields[fields.length - 1].$el.parent().append(outHTML);
                                }
                            }
                            clearInterval(checkExistDateModified);
                        }
                    }, 100);

                }
            }
        }

        if (this["action"] === "edit" && fields.length !== 0 && fields[0]["def"]["group"] === "primary_address") {
            this.getJsonArray();

            // keyboard down listener
            this.suggestList("primary_address_postalcode");
            this.suggestList("primary_address_state");
            this.suggestList("primary_address_city");

            this.closeListOnClick();
        }
        /// End
        return this;
    },

    closeListOnClick: function () {
        let self = this;

        let wardElm = $('input[name="primary_address_postalcode"]');
        let districtElm = $('input[name="primary_address_state"]');
        let provinceElm = $('input[name="primary_address_city"]');

        wardElm.blur(function () {
            self.clearList();
        })

        districtElm.focusout(function () {
            self.clearList();
        })

        provinceElm.focusout(function () {
            self.clearList();
        })
    },

    displayMap: function (evt) {
        var address = this.$(evt.currentTarget).data('address');
        if (address !== undefined) {
            var url = 'https://maps.google.it/maps/place/' + address;
            window.open(url, '', 500, 500)
        }
    },

    getJsonArray: function () {
        let self = this;
        DOTB.App.api.call('update', DOTB.App.api.buildURL('leads/vietnam-location'), null,
            {
                success: function (data) {
                    self.vietNamJson = JSON.parse(data["data"]);
                    /// init list provinces
                    self.listProvince = [];
                    _.each(self.vietNamJson, function (province) {
                        self.listProvince.push({"Id": province["Id"], "Name": province["Name"] + ", Việt Nam"});
                    })
                },
                error: function (result) {
                    console.log('Error getting');
                }
            });
    },

    suggestList: function (name) {
        let wardInput = 'primary_address_postalcode';
        let districtInput = 'primary_address_state';
        let cityInput = 'primary_address_city';

        if (name !== wardInput && name !== districtInput && name !== cityInput) {
            return;
        }

        let self = this;
        var fields = this._getChildFields();
        _.each(fields, function (field) {
            field.placeholder = field.getPlaceholder();
        }, this);

        let timeout = name === wardInput ? 300 : 0;

        /// clear all suggestions when user reload page
        self.clearList();

        $('input[name="' + name + '"]').keyup(self.delay(function (e) {

            if (e.keyCode === 27) {
                self.clearList();
                return;
            }

            let validKeycode = (
                e.keyCode === 8 || e.keyCode === 32
                || (e.keyCode >= 45 && e.keyCode <= 90)
                || (e.keyCode >= 96 && e.keyCode <= 111)
                || (e.keyCode >= 169 && e.keyCode <= 173)
                || (e.keyCode >= 186 && e.keyCode <= 223)
            );

            if (!validKeycode) {
                return;
            }

            self.clearList();

            let val = this.value;

            if (val.length > 2) {
                let list = [];

                /// check which list uses for input
                switch (name) {
                    case wardInput:
                        /// for all searching location
                        list = self.searchTreeArray(self.vietNamJson, val, 3).slice(0, 5);
                        break;
                    case districtInput:
                        list = self.searchTreeArray(self.vietNamJson, val, 2).slice(0, 5);
                        break;
                    case cityInput:
                        list = self.searchTreeArray(self.vietNamJson, val).slice(0, 5);
                        break;
                }

                /// clear all suggestions every keyboard down
                self.clearList();

                /// init a element uses for appending suggestion element
                let listSuggestion = document.createElement("DIV");

                listSuggestion.setAttribute("class", `list-suggestion`);
                listSuggestion.setAttribute("data-name", `dropdown-content`);

                this.parentNode.appendChild(listSuggestion);

                _.each(list, function (value, index) {

                    /// create a element to display a suggestion
                    let suggestion = document.createElement("a");

                    /// add display value
                    /// split string
                    let unsignedName = self.unsigned(value["Name"]).split(self.unsigned(val))[0];

                    let searchText = value["Name"].substring(unsignedName.length, unsignedName.length + val.length);

                    let listString = value["Name"].split(searchText);

                    // add icon
                    suggestion.innerHTML += "<i class='fas fa-map-marker-alt'>" + "&nbsp";

                    // update html
                    _.each(listString, function (string, index) {
                        if (index < listString.length - 1) {
                            suggestion.innerHTML += string + "<strong>" + searchText + "</strong>";
                        } else {
                            suggestion.innerHTML += string;
                        }
                    })

                    /// add class attr
                    suggestion.setAttribute("class", `address-line`);
                    suggestion.setAttribute("data-name", "suggestion");
                    //suggestion.setAttribute("style", "display: block; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;");

                    /// on click
                    suggestion.addEventListener('mousedown', function (_) {
                        /// fill data
                        self.autocomplete(value["Name"]);

                        /// clear list suggestion after choosing
                        self.clearList();
                    });

                    /// append to list suggestion element
                    listSuggestion.appendChild(suggestion);

                    /// up down action
                });
            }
        }, timeout));
        self.keyboardAction(name);
    },

    keyboardAction: function (name) {
        let self = this;
        $('input[name="' + name + '"]').keyup(function (e){
            var textLength;
            var text;
            var addressLines = document.getElementsByClassName(`address-line`);
            if (addressLines.length > 0) {
                if (e.keyCode === 40) {

                    self.currentFocus++;

                    self.removeActiveLine(addressLines);
                    if (self.currentFocus >= addressLines.length) self.currentFocus = 0;
                    if (self.currentFocus < 0) self.currentFocus = (addressLines.length - 1);

                    addressLines[self.currentFocus].classList.add("address-active");

                    textLength = addressLines[self.currentFocus].innerText.length;
                    text = addressLines[self.currentFocus].innerText.substring(1, textLength + 1);

                    self.autocomplete(text);

                } else if (e.keyCode === 38) {

                    self.currentFocus--;

                    self.removeActiveLine(addressLines);
                    if (self.currentFocus >= addressLines.length) self.currentFocus = 0;
                    if (self.currentFocus < 0) self.currentFocus = (addressLines.length - 1);

                    addressLines[self.currentFocus].classList.add("address-active");

                    textLength = addressLines[self.currentFocus].innerText.length;
                    text = addressLines[self.currentFocus].innerText.substring(1, textLength + 1);

                    self.autocomplete(text);

                } else if (e.keyCode === 13) {

                    e.preventDefault();
                    self.clearList();
                }
            }
        });
    },

    removeActiveLine: function (elms) {
        for (let i = 0; i < elms.length; i++) {
            elms[i].classList.remove("address-active");
        }
    },

    searchTreeArray: function (array, text, level = 1, parentAddress = "Việt Nam") {
        /// level 1 = provinces
        /// level 2 = districts
        /// level 3 = wards

        let self = this;
        let _listLocation = [];
        let addressName = parentAddress;

        _.each(array, function (location) {
            addressName = location["Name"] + ", " + parentAddress;
            if (self.unsigned(location["Name"]) !== undefined && self.unsigned(location["Name"]).includes(self.unsigned(text))) {
                _listLocation.push({"Id": location["Id"], "Name": addressName});
            }

            if (location["Districts"] !== undefined && level > 1) {
                let parentAdd = location["Name"] + ", " + parentAddress;
                let result = self.searchTreeArray(location["Districts"], text, level, parentAdd);
                _.each(result, function (item) {
                    _listLocation.push(item);
                });
            }

            if (location["Wards"] !== undefined && level > 2) {
                let parentAdd = location["Name"] + ", " + parentAddress;
                let result = self.searchTreeArray(location["Wards"], text, level, parentAdd)
                _.each(result, function (item) {
                    _listLocation.push(item);
                });
            }
        })

        return _listLocation;
    },

    clearList: function () {
        var listSuggestion = document.getElementsByClassName(`list-suggestion`);

        if (listSuggestion.length > 0) {
            for (let i = 0; i < listSuggestion.length; i++) {
                listSuggestion[i].remove();
            }
        }

        this.currentFocus = -1;
    },

    unsigned: function (text, upperCase = true) {
        let _vietnameseRegex = [
            ['à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ'],
            ['À', 'Á', 'Ạ', 'Ả', 'Ã', 'Â', 'Ầ', 'Ấ', 'Ậ', 'Ẩ', 'Ẫ', 'Ă', 'Ằ', 'Ắ', 'Ặ', 'Ẳ', 'Ẵ'],
            ['è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ'],
            ['È', 'É', 'Ẹ', 'Ẻ', 'Ẽ', 'Ê', 'Ề', 'Ế', 'Ệ', 'Ể', 'Ễ'],
            ['ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ'],
            ['Ò', 'Ó', 'Ọ', 'Ỏ', 'Õ', 'Ô', 'Ồ', 'Ố', 'Ộ', 'Ổ', 'Ỗ', 'Ơ', 'Ờ', 'Ớ', 'Ợ', 'Ở', 'Ỡ'],
            ['ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ'],
            ['Ù', 'Ú', 'Ụ', 'Ủ', 'Ũ', 'Ư', 'Ừ', 'Ứ', 'Ự', 'Ử', 'Ữ'],
            ['ì', 'í', 'ị', 'ỉ', 'ĩ'],
            ['Ì', 'Í', 'Ị', 'Ỉ', 'Ĩ'],
            ['đ'],
            ['Đ'],
            ['ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ'],
            ['Ỳ', 'Ý', 'Ỵ', 'Ỷ', 'Ỹ']
        ];
        const _replaceChars = 'aAeEoOuUiIdDyY';

        function unsignedString(text, upperCase = true) {
            if (text === undefined) return text;

            let result = text;
            for (let i = 0; i < _replaceChars.length; ++i) {
                for (let j = 0; j < _vietnameseRegex[i].length; j++) {
                    result = result.replaceAll(_vietnameseRegex[i][j], _replaceChars[i]);
                }
            }

            return upperCase ? result.toUpperCase() : result;
        }

        return unsignedString(text);
    },

    delay: function (fn, ms) {
        let timer = 0
        return function(...args) {
            clearTimeout(timer)
            timer = setTimeout(fn.bind(this, ...args), ms || 0)
        }
    },

    autocomplete: function (value) {
        var fields = this._getChildFields();
        _.each(fields, function (field) {
            field.placeholder = field.getPlaceholder();
        }, this);

        // update UI
        let wardElm = $('input[name="primary_address_postalcode"]');
        let districtElm = $('input[name="primary_address_state"]');
        let provinceElm = $('input[name="primary_address_city"]');
        let countryElm = $('input[name="primary_address_country"]');
        let streetElm = $('textarea[name="primary_address_street"]');
        // address format "{ward}, {district}, {province}, {country}"
        let listAddress = value.split(", ");

        let listLength = listAddress.length;

        let countryName = listLength >= 1 ? listAddress[listLength - 1] : '';
        let provinceName = listLength >= 2 ? listAddress[listLength - 2] : '';
        let districtName = listLength >= 3 ? listAddress[listLength - 3] : '';
        let wardName = listLength >= 4 ? listAddress[listLength - 4] : '';

        // update val
        streetElm.val(value).trigger("change");
        wardElm.val(wardName).trigger("change");
        districtElm.val(districtName).trigger("change");
        provinceElm.val(provinceName).trigger("change");
        countryElm.val(countryName).trigger("change");
    }
})
