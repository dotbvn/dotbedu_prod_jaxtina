({
    ageYearMonthWeekValue: "No Data",
    initialize: function (options) {
        this._super('initialize', [options]);
        var ageYearMonthWeek = setInterval(()=>{
            if (this.model.attributes.birthdate !== undefined){
                const diff = new Date(new Date() - new Date(this.model.attributes.birthdate));
                const year = (diff.getFullYear() - 1970);
                const month = diff.getUTCMonth();
                const week = Math.floor((diff.getUTCDate() - 1) / 7);
                this.ageYearMonthWeekValue = year + '.' + month + '.' + week;
                clearInterval(ageYearMonthWeek);
                this.render();
            }
        }, 1000);
    }
})
