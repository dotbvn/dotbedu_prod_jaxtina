({
    extendsFrom: 'BadgeSelectField',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.statusClasses = {
            'Not Held': 'label-important',
            'Planned': 'textbg_green',
            'Overdue' : 'textbg_red',
            'New': 'textbg_green',
            'In Process': 'textbg_blue',
            'In Progress': 'textbg_blue',
            'Converted': 'textbg_red',
            'Waiting payment': 'textbg_crimson',
            'Delayed': 'textbg_black',
            'Finished': 'textbg_crimson',
            'OutStanding': 'textbg_orange',
            'Recycled': 'textbg_orange',
            'Ready to PT': 'textbg_bluelight',
            'Ready to Demo': 'textbg_bluelight',
            'Appointment': 'textbg_bluelight',
            'Assigned': 'textbg_bluelight',
            'PT/Demo': 'textbg_violet',
            "Deposit": "textbg_crimson",
            'Dead': 'textbg_black',
            'Held': 'label-pending',
            'Not Started':'label-pending',
            'Completed':'textbg_green',
            'Pending Input':'textbg_blue',
            'Deferred':'label-important',
            'Not Applicable':'textbg_black',
            'Activated' :'textbg_green',
            'Inactive':'textbg_orange',
            'Expired':'textbg_crimson',
            'Active': 'textbg_green',


            //hed_Expense
            'Not Approved': 'textbg_orange',
            "Approved": "textbg_green",

        };

        this.type = 'badge-select';
    },

    _loadTemplate: function () {
        var action = this.action || this.view.action;
        if (action === 'edit') {
            this.type = 'enum';
        }

        this._super('_loadTemplate');
        this.type = 'badge-select';
    }
})
