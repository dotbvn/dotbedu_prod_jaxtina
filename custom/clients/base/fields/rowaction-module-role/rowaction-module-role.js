({
    extendsFrom: 'ButtonField',
    hiddenEdit: true,
    initialize: function (options) {
        options.def.events = _.extend({}, options.def.events, {
            'click .rowaction': 'rowActionSelect'
        });
        this._super('initialize', [options]);
        this.hiddenEdit = !app.acl.hasAccess(options.def.acl_action, options.def.acl_module, {field: options.def.acl_field})
    },
    rowActionSelect: function (evt) {
        var eventName = $(evt.currentTarget).data('event') || this.def.event;
        if (eventName) {
            this.getTarget().trigger(eventName, this.model, this, evt);
        }
    },
    getTarget: function () {
        var target;
        switch (this.def.target) {
            case 'view':
                target = this.view;
                break;
            case 'layout':
                target = this.view.layout;
                break;
            default:
                target = this.view.context;
        }
        return target;
    }
})
