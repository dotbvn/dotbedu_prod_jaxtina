<?php
 // created: 2020-07-23 17:36:56
$app_list_strings['lead_status_dom']=array (
    'New' => 'L1 - New',
    'In Process' => 'L2 - In Process',
    'Ready to PT' => 'L3.1 - Ready to PT',
    'Ready to Demo' => 'L3.2 - Ready to Demo',
    'PT/Demo' => 'L6 - PT/Demo',
    'Dead' => 'Dead',
    'Deposit' => 'L7 - Deposit',
    'Converted' => 'L8 - Completed',
    'Unchangeable'=>'L10 - Unchangeable'
);
