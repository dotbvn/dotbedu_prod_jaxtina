<?php
 // created: 2019-04-08 14:00:40

$app_list_strings['lead_source_dom']=array (
  '' => '',
  'Cold Call' => 'Cold Call',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Employee',
  'Partner' => 'Partner',
  'Conference' => 'Conference',
  'Web Site' => 'Web Site',
  'Word of mouth' => 'Word of mouth',
  'Email' => 'Any Email',
  'Campaign' => 'Campaign',
  'Google' => 'Google',
  'Facebook' => 'Facebook',
  'Other' => 'Other',
);