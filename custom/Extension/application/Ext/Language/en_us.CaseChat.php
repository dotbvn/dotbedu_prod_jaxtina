<?php
$app_list_strings['license_type_list'] = array(
    'outfitters' => 'Outfitters',
    'exsitec' => 'Exsitec',
);

$app_strings['ERROR_LICENSE_KEY_USER_LIMIT_REACHED'] = 'User Limit for CaseChat Reached';
$app_strings['ERROR_INVALID_LICENSE_KEY'] = 'Invalid CaseChat License Key';
$app_strings['ERROR_INVALID_APPLICATION_LICENSE_KEY'] = 'Invalid CaseChat Application';
$app_strings['ERROR_MISSING_LICENSE_KEY'] = 'Invalid CaseChat Application';
$app_strings['ERROR_MISSING_VALIDATION_KEY'] = 'Missing CaseChat Validation Key';
$app_strings['ERROR_CORRUPT_VALIDATION_KEY'] = 'Corrupt CaseChat Validation Key';
$app_strings['ERROR_LICENSE_KEY_EXPIRED'] = 'CaseChat License Expired';
$app_strings['ERROR_LICENSE_KEY_MISSING_INFO'] = 'CaseChat Missing Info';

$app_strings['LBL_CASECHAT_INVALID_LICENSE'] = 'CaseChat Invalid License';

$app_strings['LBL_CASECHAT_LICENSE_ABOUT_TO_EXPIRE_NAME'] = 'CaseChat license is about to expire';
$app_list_strings['product_list_dom'] = array(
    'ems'   => 'DOTB EMS',
    'mobile' => 'Mobile Apps',
    'portal' => 'Student Portal',
    'other' => 'Others',
);
$app_list_strings['severity_dom'] = array(
    '1' => 'High',
    '2' => 'Medium',
    '3' => 'Low',
);
$app_list_strings['status_case_support_dom'] = array(
    'open' => 'OPEN',
    'assigned' => 'ASSIGNED',
    'inprogress' => 'IN PROGRESS',
    'resolved' => 'RESOLVED',
    'reopen' => 'RE-OPEN',
    'closed' => 'CLOSED',
);
$app_list_strings['category_case_support_dom'] = array(
    'technical' => 'Technical',
    'quotation' => 'Quotations',
    'helpdesk' => 'Helpdesk',
    'others' => 'Others',
);
$app_strings['LBL_RESOLVED_TICKET'] = 'This case has been resolved';
$app_strings['LBL_CLOSED_TICKET'] = 'This case has been closed';
$app_strings['LBL_RATE_US'] = 'Rate us';
$app_strings['LBL_REOPEN'] = 'Re-open';
$app_strings['LBL_OR'] = 'or';
$app_strings['LBL_TYPE_MESSAGE'] = 'Type a message...';
$app_strings['LBL_EDIT_MESSAGE'] = 'Edit message';
$app_strings['LBL_ALERT_RATE'] = 'Select a rate to continue';
$app_strings['LBL_ALERT_FINISH_UPLOADING'] = 'Please wait for the file to finish uploading';
$app_strings['LBL_ALERT_EDIT_MSG_SUCCESS'] = 'Message edited successfully';
$app_strings['LBL_ALERT_DEL_MSG_SUCCESS'] = 'Message deleted successfully';
$app_strings['LBL_ALERT_EDIT_MSG_FAILED'] = 'Error editing message';
$app_strings['LBL_ALERT_DEL_MSG_FAILED'] = 'Error deleting message';
$app_strings['LBL_ALERT_DEL_MSG_CONFIRM'] = 'Are you sure you want to delete this message?';
$app_strings['LBL_SENT'] = 'Sent';
$app_strings['LBL_SENT_FAILED'] = 'Sent failed';
$app_strings['LBL_SENDING'] = 'Sending';

