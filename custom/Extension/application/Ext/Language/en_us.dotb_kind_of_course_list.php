<?php
 // created: 2020-07-23 17:36:56

$app_list_strings['kind_of_course_list']=array (
  '' => '-none-',
  '4SKILL COURSES' => '4SKILL COURSES',
  'IELTS' => 'IELTS',
  'HAPPY SEED' => 'HAPPY SEED',
  'GREEN TREE' => 'GREEN TREE',
  'BLOOMING' => 'BLOOMING',
  'PREMIUM' => 'PREMIUM',
  'SHORT COURSE' => 'SHORT COURSE',
  'English Pronunciation' => 'English Pronunciation',
  'Outing Trip' => 'Outing Trip',
  'DOANH NGHIỆP' => 'DOANH NGHIỆP',
  'THEO YÊU CẦU' => 'THEO YÊU CẦU',
  'Other' => 'Other',
  'ENGLISH COMMUNICATION' => 'ENGLISH COMMUNICATION',
  'Free Offline' => 'Free Offline',
  'TOEIC' => 'TOEIC',
  'ESP' => 'ESP',
  'OE' => 'OE',
  'Free Online' => 'Free Online',
);