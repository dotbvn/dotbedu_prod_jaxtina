<?php
$app_list_strings['check_attended_list'] = array(
    '' => '-empty-',
    'Yes' => 'Yes',
    'No' => 'No',
    'No result' => 'Không test/Không có điểm',
);
$app_list_strings['lead_eng_level_list'] = array (
    '' => '',
    'Mất gốc' => 'Mất gốc',
    'Cơ bản' => 'Cơ bản',
    'Nâng cao' => 'Nâng cao',
    'KH không rõ trình độ' => 'KH không rõ trình độ',
    '> TOEIC 500' => '> TOEIC 500',
    '< TOEIC 500' => '< TOEIC 500',
    '> IELTS 5.0' => '> IELTS 5.0',
    '< IELTS 5.0' => '< IELTS 5.0',
);
$app_list_strings['lead_object_list']=array (
    '' => '',
    'Người đi làm' => 'Người đi làm',
    'Tân sinh viên' => 'Tân sinh viên',
    'Sinh viên' => 'Sinh viên',
    'PH hỏi cho con cấp 1,2' => 'PH hỏi cho con cấp 1,2',
    'PH hỏi cho con cấp 3' => 'PH hỏi cho con cấp 3',
    'Học sinh cấp 2' => 'Học sinh cấp 2',
    'Học sinh cấp 3' => 'Học sinh cấp 3',
    'Chưa xác định' => 'Chưa xác định',
);
$app_list_strings['lead_target_list']=array (
    '' => '',
    'TOEIC' => 'TOEIC',
    'IELTS' => 'IELTS',
    'Giao tiếp' => 'Nâng cao',
    '4 kỹ năng' => '4 kỹ năng',
    'KH không rõ mục tiêu' => 'KH không rõ mục tiêu',
);
$app_list_strings['case_absent_reason_list']=array (
    '' => '',
    'Ốm, nghỉ 1-2 buổi' => 'Ốm, nghỉ 1-2 buổi',
    'Bệnh nặng, cần nghỉ dài hạn' => 'Bệnh nặng, cần nghỉ dài hạn',
    'HV Delay' => 'HV Delay',
    'Đi công tác' => 'Đi công tác',
    'Bận việc cá nhân đột xuất' => 'Bận việc cá nhân đột xuất',
    'Quên lịch học' => 'Quên lịch học',
    'Không liên lạc được' => 'Không liên lạc được',
    'Sai số' => 'Sai số',
    'HV Hủy khóa học' => 'HV Hủy khóa học',
    'Chất lượng đào tạo' => 'Chất lượng đào tạo',
    'HV đang xử lý sự vụ' => 'HV đang xử lý sự vụ',
    'Bận không sắp xếp học được' => 'Bận không sắp xếp học được',
    'Nguyên nhân khác' => 'Nguyên nhân khác',
);


$app_list_strings['option_j_teacherqe_type'] = array(
    'Class Observation'   => 'Dự giờ',
    'Collaborate AC' => 'Phối hợp với AC',
    'Student Evaluation' => 'HV đánh giá',
);

$app_list_strings['late_time_list'] = array(
    '' => '-none-',
    'on_time' => 'On time',
    'less_5m' => 'Late < 5 minutes',
    'less_15m' => 'Late < 15 minutes',
    'greater_15m' => 'Late > 15 minutes',
    'leave_early' => 'Leave early',
);
$app_strings['LBL_MARK_L10'] = 'Mark unchangeable';