<?php


$app_list_strings['j_gradebook_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['j_gradebook_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['j_gradebook_category_dom']['Sales'] = 'Sales';
$app_list_strings['j_gradebook_category_dom'][''] = '';
$app_list_strings['j_gradebook_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['j_gradebook_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['j_gradebook_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['j_gradebook_subcategory_dom'][''] = '';
$app_list_strings['j_gradebook_status_dom']['Active'] = 'Active';
$app_list_strings['j_gradebook_status_dom']['Draft'] = 'Draft';
$app_list_strings['j_gradebook_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['j_gradebook_status_dom']['Expired'] = 'Expired';
$app_list_strings['j_gradebook_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['j_gradebook_status_dom']['Pending'] = 'Pending';
$app_list_strings['moduleList']['J_Gradebook'] = 'Gradebooks';
$app_list_strings['moduleList']['J_Budget'] = 'Center Expense';
$app_list_strings['moduleListSingular']['J_Gradebook'] = 'Gradebook';
$app_list_strings['moduleListSingular']['J_Budget'] = 'Budget Plan';
