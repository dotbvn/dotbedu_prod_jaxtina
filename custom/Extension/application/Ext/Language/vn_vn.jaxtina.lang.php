<?php

$app_list_strings['case_type_dom'] = array(
    '' => '-không có-',
    'Tutoring' => 'Dạy kèm',
    'LMS/Apps' => 'Hệ thống Apps/LMS',
    'Nationality' => 'Quốc tịch',
    'Qualification' => 'Năng lực',
    'Performance In Class' => 'Hiệu quả trong lớp',
    'Administration' => 'Quản lý',
    'Performance Level' => 'Mức độ hiệu quả',
    'Behavior' => 'Hành vi',
    'Education Quality'=>'Chất lượng đào tạo',
    'Room' => 'Phòng',
    'Request/Complaint'=>'Yêu cầu/ Khiếu nại',
    'Teacher'=>'Giáo viên',
    'Facility' => 'Cơ sở vật chất',
    'Safety' => 'An toàn',
    'Leaving' => 'Xin nghỉ',
    'Schedule' => 'Lịch học',
    'Service Quality'=>'Chất lượng dịch vụ',
    'Fee' => 'Phí',
);

$app_list_strings['case_status_dom'] = array(
    'New' => 'Mới',
    'Assigned' => 'Đã tiếp nhận',
    'Overdue' => 'Xử lý quá hạn',
    'Closed' => 'Xử lý đúng tiến độ',
);

$app_list_strings['case_status_teacher'] = array(
    'Required'=>'Bắt buộc',
    'Teacher hour' => 'Giờ của giáo viên',
    'Teaching Standard' => 'Tiêu chuẩn giảng dạ',
    'Behavior' => 'Hành vi',
);

$app_list_strings['case_status_request'] = array(
    'Refund'=>'Hoàn phí',
    'Transfer'=>'Chuyển nhượng',
    'Class transfer'=>'Chuyển lớp',
    'Class arrangement'=>'Xếp lớp',
    'Center transfer'=>'Chuyển cơ sở',
    'Counseling'=>'Tư vấn',
    'Deferment'=>'Bảo lưu',
    'Make-up lesson'=>'Học bổ trợ/Học bù',
    'Retake the curriculum'=>'Học lại giáo trình',
    'Other' => 'Other',
);

$app_list_strings['option_contract_info'] = array(
    'Part time' => 'Bán thời gian',
    'Full time' => 'Toàn thời gian',
);

