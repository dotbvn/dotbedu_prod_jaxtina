<?php
 // created: 2022-03-30 15:33:46

$app_list_strings['lead_source_list']=array (
  '' => '',
  'Walk in' => 'Walk in',
  'Call in' => 'Call in',
  'Facebook' => 'Facebook',
  'Search' => 'Search',
  'Adnet' => 'Adnet',
  'Socialmedia' => 'Socialmedia',
  'Organic' => 'Organic',
  'Tools' => 'Tools',
  'Local Data' => 'Local Data',
  'Referral' => 'Referral',
  'Sponsorship' => 'Sponsorship',
  'Enterprise' => 'Enterprise',
  'Others' => 'Others',
  'TT.Facebook' => 'TT.Facebook',
  'TT.Search' => 'TT.Search',
  'TT.Socialmedia' => 'TT.Socialmedia',
  'TT.Organic' => 'TT.Organic',
  'Online.HNQ' => 'Online.HNQ',
  'HS10' => 'HS10',
  'HS12' => 'HS12',
  'Chuachoncoso' => 'Chuachoncoso',
  'TikTok' => 'TikTok',
);