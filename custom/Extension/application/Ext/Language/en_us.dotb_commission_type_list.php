<?php
 // created: 2020-07-23 17:36:56

$app_list_strings['commission_type_list']=array (
    'First Assigned To' => 'First Assigned To',
    'Closed Sale' => 'Closed Sale',
    'PT/Demo by User' => 'PT/Demo by User',
);
$app_list_strings['commission_default_list']=array (
    '' => '-none-',
    'First Assigned To' => '50',
    'Closed Sale' => '40',
    'PT/Demo by User' => '10',
);