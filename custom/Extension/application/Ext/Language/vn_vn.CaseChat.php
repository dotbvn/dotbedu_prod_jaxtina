<?php
$app_list_strings['product_list_dom'] = array(
    'ems'   => 'DOTB EMS',
    'mobile' => 'Ứng dụng di động',
    'portal' => 'Cổng thông tin học sinh',
    'other' => 'Khác',
);
$app_list_strings['status_case_support_dom'] = array(
    'open' => 'Mở',
    'assigned' => 'Đã tiếp nhận',
    'inprogress' => 'Đang xử lý',
    'resolved' => 'Đã giải quyết',
    'reopen' => 'Mở lại',
    'closed' => 'Đóng',
);
$app_list_strings['category_case_support_dom'] = array(
    'technical' => 'Kỹ thuật',
    'quotation' => 'Báo giá',
    'helpdesk' => 'Hỗ trợ',
    'others' => 'Khác',
);
$app_list_strings['severity_dom'] = array(
    '1' => 'Cao',
    '2' => 'Vừa',
    '3' => 'Thấp',
);
$app_strings['LBL_RESOLVED_TICKET'] = 'Trường hợp này đã được giải quyết';
$app_strings['LBL_CLOSED_TICKET'] = 'Trường hợp này đã được đóng';
$app_strings['LBL_RATE_US'] = 'Đánh giá';
$app_strings['LBL_REOPEN'] = 'Mở lại';
$app_strings['LBL_OR'] = 'hoặc';
$app_strings['LBL_TYPE_MESSAGE'] = 'Nhập tin nhắn...';
$app_strings['LBL_EDIT_MESSAGE'] = 'Sửa tin nhắn';
$app_strings['LBL_ALERT_RATE'] = 'Chọn một đánh giá để tiếp tục';
$app_strings['LBL_ALERT_FINISH_UPLOADING'] = 'Vui lòng chờ tệp tải lên hoàn tất';
$app_strings['LBL_ALERT_EDIT_MSG_SUCCESS'] = 'Tin nhắn đã được sửa thành công';
$app_strings['LBL_ALERT_DEL_MSG_SUCCESS'] = 'Tin nhắn đã được xóa thành công';
$app_strings['LBL_ALERT_EDIT_MSG_FAILED'] = 'Lỗi khi sửa tin nhắn';
$app_strings['LBL_ALERT_DEL_MSG_FAILED'] = 'Lỗi khi xóa tin nhắn';
$app_strings['LBL_ALERT_DEL_MSG_CONFIRM'] = 'Bạn có chắc chắn muốn xóa tin nhắn này?';
$app_strings['LBL_SENT'] = 'Đã gửi';
$app_strings['LBL_SENT_FAILED'] = 'Gửi thất bại';
$app_strings['LBL_SENDING'] = 'Đang gửi';

