<?php
// created: 2024-06-18 10:31:20
$app_list_strings['moduleListSingular']['Home'] = 'Trang chủ';
$app_list_strings['moduleListSingular']['Contacts'] = 'Học viên';
$app_list_strings['moduleListSingular']['Accounts'] = 'Công ty';
$app_list_strings['moduleListSingular']['Opportunities'] = 'Cơ hội';
$app_list_strings['moduleListSingular']['Cases'] = 'Vụ việc';
$app_list_strings['moduleListSingular']['Notes'] = 'Ghi chú';
$app_list_strings['moduleListSingular']['Calls'] = 'Cuộc gọi';
$app_list_strings['moduleListSingular']['Emails'] = 'Email';
$app_list_strings['moduleListSingular']['Meetings'] = 'Cuộc họp';
$app_list_strings['moduleListSingular']['Tasks'] = 'Nhiệm vụ';
$app_list_strings['moduleListSingular']['Calendar'] = 'Lịch';
$app_list_strings['moduleListSingular']['Leads'] = 'HV Tiềm năng';
$app_list_strings['moduleListSingular']['Contracts'] = 'Hợp đồng';
$app_list_strings['moduleListSingular']['Quotes'] = 'Báo giá';
$app_list_strings['moduleListSingular']['Products'] = 'Mục hàng được báo giá';
$app_list_strings['moduleListSingular']['ProductCategories'] = 'Danh mục sản phẩm';
$app_list_strings['moduleListSingular']['ProductTypes'] = 'Loại sản phẩm';
$app_list_strings['moduleListSingular']['ProductTemplates'] = 'Mẫu sản phẩm';
$app_list_strings['moduleListSingular']['Reports'] = 'Báo cáo';
$app_list_strings['moduleListSingular']['Forecasts'] = 'Dự báo';
$app_list_strings['moduleListSingular']['Bugs'] = 'Hỗ trợ EMS';
$app_list_strings['moduleListSingular']['Campaigns'] = 'Chiến dịch';
$app_list_strings['moduleListSingular']['Documents'] = 'Tài liệu';
$app_list_strings['moduleListSingular']['pmse_Inbox'] = 'Quá trình';
$app_list_strings['moduleListSingular']['pmse_Project'] = 'Định nghĩa quy trình';
$app_list_strings['moduleListSingular']['pmse_Business_Rules'] = 'Quy tắc kinh doanh xử lý';
$app_list_strings['moduleListSingular']['pmse_Emails_Templates'] = 'Xử lý mẫu email';
$app_list_strings['moduleListSingular']['Prospects'] = 'KH Mục tiêu';
$app_list_strings['moduleListSingular']['ProspectLists'] = 'Danh sách mục tiêu';
$app_list_strings['moduleListSingular']['Tags'] = 'Thẻ';
$app_list_strings['moduleListSingular']['OutboundEmail'] = 'Cài đặt email';
$app_list_strings['moduleListSingular']['DataPrivacy'] = 'Bảo mật dữ liệu';
$app_list_strings['moduleListSingular']['KBContents'] = 'Bài viết cơ sở kiến ​​thức';
$app_list_strings['moduleListSingular']['fte_UsageTracking'] = 'UsageTracking';
$app_list_strings['moduleListSingular']['J_BankTrans'] = 'Bank Transactions';
$app_list_strings['moduleListSingular']['C_Comments'] = 'Comments';
$app_list_strings['moduleListSingular']['C_SMS'] = 'SMS';
$app_list_strings['moduleListSingular']['C_Contacts'] = 'Parent';
$app_list_strings['moduleListSingular']['C_Rooms'] = 'Room';
$app_list_strings['moduleListSingular']['C_Teachers'] = 'Teacher';
$app_list_strings['moduleListSingular']['C_Timesheet'] = 'Admin Hours';
$app_list_strings['moduleListSingular']['J_Class'] = 'Class';
$app_list_strings['moduleListSingular']['J_Coursefee'] = 'Course Fee';
$app_list_strings['moduleListSingular']['J_Discount'] = 'Discount';
$app_list_strings['moduleListSingular']['J_GradebookConfig'] = 'Gradebook Setting';
$app_list_strings['moduleListSingular']['J_GradebookDetail'] = 'Gradebook Detail';
$app_list_strings['moduleListSingular']['J_Inventory'] = 'Inventory';
$app_list_strings['moduleListSingular']['J_Invoice'] = 'Invoice';
$app_list_strings['moduleListSingular']['J_Kindofcourse'] = 'Kind Of Course';
$app_list_strings['moduleListSingular']['J_PaymentDetail'] = 'Receipts';
$app_list_strings['moduleListSingular']['J_School'] = 'School';
$app_list_strings['moduleListSingular']['J_Targetconfig'] = 'KPI Settings';
$app_list_strings['moduleListSingular']['J_Teachercontract'] = 'Teacher Contract';
$app_list_strings['moduleListSingular']['J_Voucher'] = 'Voucher';
$app_list_strings['moduleListSingular']['DRI_Workflows'] = 'Customer Journey';
$app_list_strings['moduleListSingular']['J_Gradebook'] = 'Gradebook';
$app_list_strings['moduleListSingular']['J_Budget'] = 'Budget Plan';
$app_list_strings['moduleListSingular']['J_Loyalty'] = 'Loyalty';
$app_list_strings['moduleListSingular']['C_News'] = 'News';
$app_list_strings['moduleListSingular']['RevenueLineItems'] = 'Mục hàng doanh thu';
$app_list_strings['moduleListSingular']['J_Payment'] = 'Payment';
$app_list_strings['moduleListSingular']['J_TeacherQE'] = 'Teaching Performance';
$app_list_strings['moduleListSingular']['Holidays'] = 'Holidays';
$app_list_strings['moduleListSingular']['DRI_SubWorkflows'] = 'Customer Journey Stage';
$app_list_strings['moduleListSingular']['C_Gallery'] = 'Gallery';
$app_list_strings['moduleListSingular']['J_GradebookSettingGroup'] = 'Gradebook Setting Group';
$app_list_strings['moduleListSingular']['J_Membership'] = 'Membership';
$app_list_strings['moduleListSingular']['J_LessonPlan'] = 'Giáo án';
$app_list_strings['moduleListSingular']['J_GroupUnit'] = 'Group Unit';
$app_list_strings['moduleListSingular']['J_Unit'] = 'Unit';
$app_list_strings['moduleListSingular']['J_Workpermit'] = 'Work permit';
