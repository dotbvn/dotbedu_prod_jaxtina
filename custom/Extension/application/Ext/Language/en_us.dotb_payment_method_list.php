<?php
 // created: 2020-07-23 17:36:56
$app_list_strings['payment_method_list']=array (
    '' => '-none-',
    'Cash' => 'CASH',
    'Card' => 'CREDIT/DEBIT CARD',
    'Trả góp' => 'TRẢ GÓP',
    'VNPay' => 'VNPay',
    'Bank Transfer' => 'BANK TRANSFER',
    'Other' => 'OTHER',
);