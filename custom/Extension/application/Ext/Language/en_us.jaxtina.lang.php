<?php

$app_list_strings['case_type_dom'] = array(
    '' => '-none-',
    'Tutoring' => 'Tutoring - Dạy kèm',
    'LMS/Apps' => 'LMS/Apps - Hệ thống LMS/Apps',
    'Nationality' => 'Nationality - Quốc tịch',
    'Qualification' => 'Qualification - Năng lực',
    'Performance In Class' => 'Performance In Class - Hiệu quả trong lớp',
    'Administration' => 'Administration - Quản lý',
    'Performance Level' => 'Performance Level - Mức độ hiệu quả',
    'Behavior' => 'Behavior - Hành vi',
    'Education Quality'=>'Education Quality - Chất lượng đào tạo',
    'Room' => 'Room - Phòng',
    'Request/Complaint'=>'Request/Complaint - Yêu cầu/Khiếu nại',
    'Teacher'=>'Teacher - Giáo viên',
    'Facility' => 'Facility - Cơ sở vật chất',
    'Safety' => 'Safety - An toàn',
    'Leaving' => 'Leaving - xin nghỉ',
    'Schedule' => 'Schedule - Lịch học',
    'Service Quality'=>'Service Quality - Chất lượng dịch vụ',
    'Fee' => 'Fee - Phí',
);

$app_list_strings['case_status_dom'] = array(
    'New' => 'New',
    'Assigned' => 'Assigned',
    'Overdue' => 'Overdue',
    'Closed' => 'Closed',
);

$app_list_strings['case_status_teacher'] = array(
    'Teacher hour' => 'Teacher hour',
    'Teaching Standard' => 'Teaching Standard',
    'Behavior' => 'Behavior',
);

$app_list_strings['case_status_request'] = array(
    'Refund'=>'Refund - Hoàn phí',
    'Transfer'=>'Transfer - Chuyển nhượng',
    'Class transfer'=>'Class transfer - Chuyển lớp',
    'Class arrangement'=>'Class arrangement - Xếp lớp',
    'Center transfer'=>'Center transfer - Chuyển cơ sở',
    'Counseling'=>'Counseling - Tư vấn',
    'Deferment'=>'Deferment - Bảo lưu',
    'Make-up lesson'=>'Make-up lesson - Học bổ trợ/Học bù',
    'Retake the curriculum'=>' Retake the curriculum - Học lại giáo trình',
    'Other' => 'Other - Khác',
);

$app_list_strings['case_type_options'] = array(
   'Teacher' => 'case_status_teacher',
   'Request/Complaint' => 'case_status_request',
);

$app_list_strings['case_specified_type_options'] = array(
    'Teacher hour' => 'Teacher hour',
    'Teaching Standard' => 'Teaching Standard',
    'Behavior' => 'Behavior',
    'Refund'=>'Refund - Hoàn phí',
    'Transfer'=>'Transfer - Chuyển nhượng',
    'Class transfer'=>'Class transfer - Chuyển lớp',
    'Class arrangement'=>'Class arrangement - Xếp lớp',
    'Center transfer'=>'Center transfer - Chuyển cơ sở',
    'Counseling'=>'Counseling - Tư vấn',
    'Deferment'=>'Deferment - Bảo lưu',
    'Make-up lesson'=>'Make-up lesson - Học bổ trợ/Học bù',
    'Retake the curriculum'=>' Retake the curriculum - Học lại giáo trình',
    'Other' => 'Other - Khác',
);

$app_list_strings['option_contract_info'] = array(
    'Part time' => 'Part time',
    'Full time' => 'Full time',
);