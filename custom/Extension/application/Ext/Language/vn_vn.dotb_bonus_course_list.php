<?php
 // created: 2021-11-24 14:02:37
$app_list_strings['bonus_course_list']=array (
  'IPA' => 'App Phát Âm',
  'CompleteEnglish' => 'App Sách Complete English',
  'PreS'=> 'App Khóa PreS' ,
  'S' => 'App Khóa S',
  'TC' => 'App Khóa TC',
  'MTC' => 'App Khóa MTC',
  'Toeic650' => 'App Khóa TOEIC 650+',
  'AcadamicEnglish' => 'App Khóa Academic English',
  'IeltsIntensive'=> 'App Khóa IELTS Intensive',
  'IeltsFocus' => 'App Khóa IELTS Focus',
);