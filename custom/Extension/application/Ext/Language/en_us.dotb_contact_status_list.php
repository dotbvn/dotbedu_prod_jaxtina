<?php
 // created: 2020-07-23 17:36:56

$app_list_strings['contact_status_list']=array (
    'Converted'              => 'S0 - Converted',
    'Deposit'                => 'S1 - Deposit',
    'Waiting for class'      => 'S2 - Waiting for class',
    'Demo'                   => 'S3.1 -In class (Demo)',
    'In Progress'            => 'S3.2 - In class (Enrolled)',
    'OutStanding'            => 'S4 - OutStanding',
    'Delayed'                => 'Delayed',
    'Finished'               => 'Finished',
);