<?php
// created: 2020-07-23 17:36:56
$app_list_strings['payment_process_list'] = array(
    'None' => '-None-',
    'Unpaid' => 'Chưa thanh toán',
    'Part Paid' => 'Đã trả 1 phần',
    'Full Paid' => 'Đã trả đủ',
);