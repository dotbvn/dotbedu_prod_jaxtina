<?php
// created: 2020-07-23 17:36:56
$app_list_strings['payment_process_list'] = array(
    'None' => '-None-',
    'Unpaid' => 'Unpaid',
    'Part Paid' => 'Part Paid',
    'Full Paid' => 'Full Paid',
);