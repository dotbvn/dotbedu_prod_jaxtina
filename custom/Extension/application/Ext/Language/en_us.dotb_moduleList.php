<?php
// created: 2024-04-23 10:42:06
$app_list_strings['moduleList']['Home'] = 'Home';
$app_list_strings['moduleList']['Contacts'] = 'Students';
$app_list_strings['moduleList']['Accounts'] = 'Corporates';
$app_list_strings['moduleList']['Opportunities'] = 'Opportunities';
$app_list_strings['moduleList']['Cases'] = 'Feedback';
$app_list_strings['moduleList']['Notes'] = 'Notes';
$app_list_strings['moduleList']['Calls'] = 'Calls';
$app_list_strings['moduleList']['Emails'] = 'Emails';
$app_list_strings['moduleList']['Meetings'] = 'Schedules';
$app_list_strings['moduleList']['Tasks'] = 'Tasks';
$app_list_strings['moduleList']['Calendar'] = 'Calendar';
$app_list_strings['moduleList']['Leads'] = 'Leads';
$app_list_strings['moduleList']['Contracts'] = 'Contracts';
$app_list_strings['moduleList']['Quotes'] = 'Quotes';
$app_list_strings['moduleList']['Products'] = 'Quoted Line Items';
$app_list_strings['moduleList']['ProductCategories'] = 'Product Categories';
$app_list_strings['moduleList']['ProductTypes'] = 'Product Types';
$app_list_strings['moduleList']['ProductTemplates'] = 'Product Catalog';
$app_list_strings['moduleList']['Reports'] = 'Reports';
$app_list_strings['moduleList']['Forecasts'] = 'Forecasts';
$app_list_strings['moduleList']['Bugs'] = 'EMS Support';
$app_list_strings['moduleList']['Campaigns'] = 'Campaigns';
$app_list_strings['moduleList']['Documents'] = 'Documents';
$app_list_strings['moduleList']['pmse_Inbox'] = 'Processes';
$app_list_strings['moduleList']['pmse_Project'] = 'Process Definitions';
$app_list_strings['moduleList']['pmse_Business_Rules'] = 'Process Business Rules';
$app_list_strings['moduleList']['pmse_Emails_Templates'] = 'Process Email Templates';
$app_list_strings['moduleList']['Prospects'] = 'Targets';
$app_list_strings['moduleList']['ProspectLists'] = 'Target Lists';
$app_list_strings['moduleList']['Tags'] = 'Tags';
$app_list_strings['moduleList']['OutboundEmail'] = 'Email Settings';
$app_list_strings['moduleList']['DataPrivacy'] = 'Data Privacy';
$app_list_strings['moduleList']['KBContents'] = 'News Article';
$app_list_strings['moduleList']['fte_UsageTracking'] = 'UsageTracking';
$app_list_strings['moduleList']['J_BankTrans'] = 'Bank Transactions';
$app_list_strings['moduleList']['C_Comments'] = 'Comments';
$app_list_strings['moduleList']['C_SMS'] = 'SMS';
$app_list_strings['moduleList']['C_Contacts'] = 'Parents';
$app_list_strings['moduleList']['C_Rooms'] = 'Rooms';
$app_list_strings['moduleList']['C_Teachers'] = 'Teachers';
$app_list_strings['moduleList']['C_Timesheet'] = 'Admin Hours';
$app_list_strings['moduleList']['J_Class'] = 'Classes';
$app_list_strings['moduleList']['J_Coursefee'] = 'Course Fees';
$app_list_strings['moduleList']['J_Discount'] = 'Discounts';
$app_list_strings['moduleList']['J_GradebookConfig'] = 'Gradebook Settings';
$app_list_strings['moduleList']['J_GradebookDetail'] = 'Gradebook Details';
$app_list_strings['moduleList']['J_Inventory'] = 'Inventory';
$app_list_strings['moduleList']['J_Invoice'] = 'Invoices';
$app_list_strings['moduleList']['J_Kindofcourse'] = 'Kind Of Courses';
$app_list_strings['moduleList']['J_PaymentDetail'] = 'Receipts';
$app_list_strings['moduleList']['J_School'] = 'Schools';
$app_list_strings['moduleList']['J_Targetconfig'] = 'KPI Settings';
$app_list_strings['moduleList']['J_Teachercontract'] = 'Teacher Contracts';
$app_list_strings['moduleList']['J_Voucher'] = 'Vouchers';
$app_list_strings['moduleList']['DRI_Workflows'] = 'Customer Journeys';
$app_list_strings['moduleList']['J_Gradebook'] = 'Gradebooks';
$app_list_strings['moduleList']['J_Budget'] = 'Expense';
$app_list_strings['moduleList']['J_Loyalty'] = 'Loyalty';
$app_list_strings['moduleList']['C_News'] = 'News';
$app_list_strings['moduleList']['RevenueLineItems'] = 'Revenue Line Items';
$app_list_strings['moduleList']['J_Payment'] = 'Payments';
$app_list_strings['moduleList']['J_TeacherQE'] = 'Teaching Performance';
$app_list_strings['moduleList']['Holidays'] = 'Holidays';
$app_list_strings['moduleList']['DRI_SubWorkflows'] = 'Customer Journey Stages';
$app_list_strings['moduleList']['C_Gallery'] = 'Gallery';
$app_list_strings['moduleList']['J_GradebookSettingGroup'] = 'Gradebook Setting Groups';
$app_list_strings['moduleList']['J_Membership'] = 'Membership';
$app_list_strings['moduleList']['J_LessonPlan'] = 'Lesson Plans';
$app_list_strings['moduleList']['J_GroupUnit'] = 'Group Units';
$app_list_strings['moduleList']['J_Unit'] = 'Units';
$app_list_strings['moduleList']['J_Workpermit'] = 'Work permit';
