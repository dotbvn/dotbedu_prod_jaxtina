<?php

use DRI_Workflow_Task_Templates\Activity\ActivityHandlerFactory;

/**
 * Check if there are any Customer Journeys related to the Lead/Student
 * @return boolean
 */
function hasInProgressCJ($parent_type, $parent_id) {
    $parent_type_field = ($parent_type === 'Leads') ? 'lead_id' : 'contact_id';
    $q1 = "SELECT IFNULL(id, '') cj_id FROM dri_workflows
    WHERE dri_workflows.deleted = 0 AND dri_workflows.state NOT IN ('completed')
    AND ". $parent_type_field ."= '" . $parent_id ."'";
    $not_completed_cj = $GLOBALS['db']->getOne($q1);

    return (bool)$not_completed_cj;
}

/**
 * Auto-complete tasks that meet the completion criteria
 * @return void
 */
function autoCompleteCJTask($parentBean, $table_name = "") {
    if (empty($table_name)) $table_name = $parentBean->table_name;
    switch($parentBean->module_dir) {
        case 'Contacts':
            $parent_type = $parentBean->module_dir;
            $parent_id = $parentBean->id;
            break;
        case 'Leads':
            $parent_type = ($parentBean->status == 'Converted') ? 'Contacts' : 'Leads';
            $parent_id = ($parentBean->status == 'Converted') ? $parentBean->contact_id : $parentBean->id;
            break;
        case 'J_PTResult':
            $parent_type = $parentBean->parent;
            $parent_id = $parentBean->student_id;
            break;
        case 'J_StudentSituations':
        case 'C_Attendance':
            $parent_type = $parentBean->student_type;
            $parent_id = $parentBean->student_id;
            break;
        default:
            $parent_type = $parentBean->parent_type;
            $parent_id = $parentBean->parent_id;
    }

    $q = "SELECT IFNULL(tasks.id, '') task_id, l1.completed_by_key completed_key
    FROM tasks INNER JOIN dri_workflow_task_templates l1 ON l1.id = tasks.dri_workflow_task_template_id AND l1.completed_by_module = '$table_name' AND l1.deleted = 0
    WHERE tasks.is_customer_journey_activity = 1 AND tasks.parent_type = '$parent_type' AND tasks.parent_id = '$parent_id'
    AND tasks.status IN ('Not Started', 'Not Applicable') AND tasks.deleted = 0
    ORDER BY tasks.cj_actual_sort_order";

    $rows = $GLOBALS['db']->fetchArray($q);
    foreach ($rows as $row) {
        $criteria = generateCriteriaArray($row['completed_key']);
        if (count($criteria) > 0) {
            $task = BeanFactory::getBean('Tasks', $row['task_id']);
            if (isCompleted($parent_type, $parent_id, $table_name, $criteria)) {
                $task->status = 'Completed';
                $task->assigned_user_id = (!empty($parentBean->assigned_user_id)) ? $parentBean->assigned_user_id : $parentBean->modified_user_id;
                $task->save();

                setNotApplicable($parent_type, $parent_id, $task);
            }
        }
    }
}

/**
 * Check if the Customer Journey Task is Completed or Not
 * @return boolean
 */
function isCompleted($parent_type, $parent_id, $table_name, $extWhere): bool {
    switch($table_name) {
        case 'contacts':
        case 'leads':
            $parent_id_field = ($parent_type == 'Contacts') ? "contact_id" : "id";
            $ext_parent_type_query = "";
            break;
        case 'j_ptresult':
            $parent_type_field = "parent";
            $parent_id_field = "student_id";
            $ext_parent_type_query = " AND $table_name." .$parent_type_field. "= '".$parent_type."'";
            break;
        case 'j_studentsitations':
        case 'c_attendance':
            $parent_type_field = 'student_type';
            $parent_id_field = 'student_id';
            $ext_parent_type_query = " AND $table_name." .$parent_type_field. "= '".$parent_type."'";
            break;
        default:
            $parent_type_field = "parent_type";
            $parent_id_field = "parent_id";
            $ext_parent_type_query = " AND $table_name." .$parent_type_field. "= '".$parent_type."'";
    }
    $q = "SELECT IFNULL($table_name.id, '') primary_id FROM $table_name
    WHERE $table_name." .$parent_id_field. "= '".$parent_id."' AND $table_name.deleted = 0" . $ext_parent_type_query;
    $q .= generateSQLQuery($extWhere);
    $result = $GLOBALS['db']->getOne($q);

    return (bool)$result;
}

/**
 * Transform given criteria from string to array
 * @return array
 */
function generateCriteriaArray($completed_key) {
    $result = array();
    $criteria = explode(',', str_replace('^', '', $completed_key));
    foreach ($criteria as $value) {
        $valueSplit = explode(':', $value);
        $result[$valueSplit[0]][] = $valueSplit[1];
    }
    return $result;
}

/**
 * Generates SQL query based on the given criteria
 * @return string
 */
function generateSQLQuery($conditions) {
    $specialOperators = ['is_null', 'is_not_null'];
    $ext1 = $ext2 = $ext3 = "";
    foreach ($conditions as $field => $values) {
        foreach ($values as $value) {
            if (!in_array($value, $specialOperators) && !strpos($value, '|field')) continue;
            switch ($value) {
                case 'is_null':
                    $ext1 .= " AND (" . $field . " IS NULL OR " . $field . " = '')";
                    break;
                case 'is_not_null':
                    $ext1 .= " AND " . $field . " IS NOT NULL";
                    break;
                default:
            }
            if (strpos($value, '|field')) {
                $specialOperators[] = $value;
                $ext3 = " AND " . $field . " = " . str_replace('|field', '', $value);
            }
        }
        $normalValues = array_diff($values, $specialOperators);
        $operator = (empty($ext1)) ? " AND " : " OR ";
        if (count($normalValues) > 0) {
            $ext2 .= $operator . $field . " IN ('" . implode("','", $normalValues) . "')";
        }
    }
    return $ext1.$ext2.$ext3;
}

function completeAutomaticTask($parent_type, $parent_id, $process_key, $table_name) {
    $q = "SELECT IFNULL(tasks.id, '') task_id, l1.completed_by_key completed_key
    FROM tasks INNER JOIN dri_workflow_task_templates l1 ON l1.id = tasks.dri_workflow_task_template_id AND l1.deleted = 0
    WHERE tasks.is_customer_journey_activity = 1 AND tasks.customer_journey_type = 'automatic_task'
    AND tasks.cj_completed_by_module = '$table_name' AND tasks.status NOT IN ('Completed', 'Deferred', 'Not Applicable')
    AND tasks.parent_type = '$parent_type' AND tasks.parent_id = '$parent_id' AND tasks.deleted = 0
    ORDER BY tasks.cj_actual_sort_order";

    $rows = $GLOBALS['db']->fetchArray($q);
    foreach ($rows as $row) {
        $criteria = generateCriteriaArray($row['completed_key']);
        if (count($criteria) > 0 && (in_array($process_key['after'], $criteria['process_key']))) {
            $task = BeanFactory::getBean('Tasks', $row['task_id']);
            $task->status = 'Completed';
            $task->save();
            setNotApplicable($parent_type, $parent_id, $task);
        }
    }
}

/**
 * Set Not Applicable for all activities that have not been started (status = "Not Started") in previous CJ sSage
 * @return void
 */
function setNotApplicable($parent_type, $parent_id, $task) {
    $parent_field = ($parent_type == 'Leads') ? 'lead_id' : 'contact_id';

    $cj_sub_workflow = BeanFactory::getBean('DRI_SubWorkflows', $task->dri_subworkflow_id);
    $cj_workflow = BeanFactory::getBean('DRI_Workflows', $cj_sub_workflow->dri_workflow_id);

    $q1 = "SELECT IFNULL(l2.id, '') activity_id, l2.cj_actual_sort_order activity_sort_order
    FROM dri_workflows l1
    INNER JOIN tasks l2 ON l2.dri_workflow_template_id = l1.dri_workflow_template_id AND l2.parent_id = l1.". $parent_field ." AND l2.deleted = 0
    AND l2.status NOT IN ('Completed', 'Not Applicable', 'Deferred') AND l2.cj_actual_sort_order < '$task->cj_actual_sort_order' AND l2.dri_subworkflow_id != '$task->dri_subworkflow_id'
    WHERE l1.". $parent_field ." = '$parent_id' AND l1.deleted = 0 AND l1.id = '$cj_workflow->id'
    ORDER BY activity_sort_order";

    $rows1 = $GLOBALS['db']->fetchArray($q1);
    foreach ($rows1 as $row1) {
        $activity = BeanFactory::getBean('Tasks', $row1['activity_id']);
        $activity->status = 'Not Applicable';
        $activity->save();
    }
}

function markDeadLead($lead)
{
    if ($lead->load_relationship('dri_workflows')) {
        foreach ($lead->dri_workflows->getBeans() as $cycle) {
            $cycle->retrieve();
            if ($cycle->state == 'completed') continue;
            foreach ($cycle->getStages() as $stage) {
                if ($stage->state == 'completed') continue;
                foreach ($stage->getActivities() as $activity) {
                    if (!in_array($activity->status, $GLOBALS['app_list_strings']['cj_tasks_completed_status_list'])) {
                        $handler = ActivityHandlerFactory::factory('Tasks');
                        $handler->setStatus($activity, 'Not Applicable');
                        $activity->save();
                    }
                }
            }
        }
    }
}