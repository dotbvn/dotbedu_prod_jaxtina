<?php

//Update Class Attendance
function updateClassAttendance($class_id = ''){
    if(!empty($class_id)){

        //handle Attendance Average
        $att_config = $GLOBALS['app_list_strings']['att_config_options'];
        foreach($att_config as $aType => $aVal) $extAvgAtt .= " WHEN (IFNULL(att.attendance_type,'')='$aType') THEN $aVal ";
        $extAvgAtt = !empty($extAvgAtt) ? "ROUND(AVG(CASE $extAvgAtt ELSE NULL END),6) avg_attendance" : "null avg_attendance";

        $qAttended = "UPDATE meetings
        INNER JOIN (SELECT DISTINCT IFNULL(mtt.id, '') meeting_id,
        SUM(CASE WHEN att.attended=1 THEN 1 ELSE 0 END) total_attended,
        SUM(CASE WHEN att.attendance_type IN ('A','E') THEN 1 ELSE 0 END) total_absent,
        SUM(CASE WHEN att.datetime_notify_sent IS NOT NULL THEN 1 ELSE 0 END) total_sent,
        $extAvgAtt,
        COUNT(DISTINCT l1.id) total_student
        FROM meetings mtt
        INNER JOIN meetings_contacts l1_1 ON mtt.id = l1_1.meeting_id AND l1_1.deleted = 0
        INNER JOIN contacts l1 ON l1.id = l1_1.contact_id AND l1.deleted=0
        LEFT JOIN c_attendance att ON att.student_id = l1.id AND att.meeting_id = mtt.id AND att.deleted = 0
        WHERE mtt.ju_class_id = '$class_id' AND mtt.session_status <> 'Cancelled'
        AND mtt.deleted = 0 GROUP BY mtt.id) u1 ON u1.meeting_id = meetings.id
        SET meetings.total_attended = u1.total_attended,
        meetings.total_absent = u1.total_absent,
        meetings.total_sent = u1.total_sent,
        meetings.total_student = u1.total_student,
        meetings.attendance_status = (CASE WHEN (u1.total_absent + u1.total_attended) > 0 THEN 1 ELSE 0 END),
        meetings.avg_attendance = u1.avg_attendance
        WHERE meetings.ju_class_id = '$class_id' AND (meetings.session_status <> 'Cancelled')
        AND meetings.deleted = 0";
        return $GLOBALS['db']->query($qAttended);
    }else return false;
}

//Update Meeeting Attendance
function updateMeetingAttendance($meeting_id = ''){
    if(!empty($meeting_id)){
        //handle Attendance Average
        $att_config = $GLOBALS['app_list_strings']['att_config_options'];
        foreach($att_config as $aType => $aVal) $extAvgAtt .= " WHEN (IFNULL(att.attendance_type,'')='$aType') THEN $aVal ";
        $extAvgAtt = !empty($extAvgAtt) ? "ROUND(AVG(CASE $extAvgAtt ELSE NULL END),6) avg_attendance" : "null avg_attendance";

        $qAttended = "UPDATE meetings
        INNER JOIN (SELECT DISTINCT IFNULL(meetings.id, '') meeting_id,
        SUM(CASE WHEN att.attended=1 THEN 1 ELSE 0 END) total_attended,
        SUM(CASE WHEN att.attendance_type IN ('A','E') THEN 1 ELSE 0 END) total_absent,
        SUM(CASE WHEN att.datetime_notify_sent IS NOT NULL THEN 1 ELSE 0 END) total_sent,
        COUNT(DISTINCT l1.id) total_student,
        $extAvgAtt
        FROM meetings
        INNER JOIN meetings_contacts l1_1 ON meetings.id = l1_1.meeting_id AND l1_1.deleted = 0
        INNER JOIN contacts l1 ON l1.id = l1_1.contact_id AND l1.deleted=0
        LEFT JOIN c_attendance att ON att.student_id = l1.id AND att.meeting_id = '$meeting_id' AND att.deleted = 0
        WHERE meetings.id = '$meeting_id' AND meetings.session_status <> 'Cancelled'
        AND meetings.deleted = 0 GROUP BY meetings.id) u1 ON u1.meeting_id = meetings.id
        SET meetings.total_attended = u1.total_attended,
        meetings.total_absent = u1.total_absent,
        meetings.total_sent = u1.total_sent,
        meetings.total_student = u1.total_student,
        meetings.attendance_status = (CASE WHEN (u1.total_absent+u1.total_attended)>0 THEN 1 ELSE 0 END),
        meetings.avg_attendance = u1.avg_attendance
        WHERE meetings.id = '$meeting_id'";
        return $GLOBALS['db']->query($qAttended);
    }else return false;
}

//biuld query update attendance
function buildQueryAttendance($classId = '', $student_id = ''){
    if(empty($classId)) return false;
    if(!empty($student_id)) $ext_student = "AND (l1.student_id = '$student_id')";
    //handle Attendance Average
    $att_config = $GLOBALS['app_list_strings']['att_config_options'];
    foreach($att_config as $aType => $aVal) $extAvgAtt .= " WHEN (IFNULL(att.attendance_type,'')='$aType') THEN $aVal ";
    $extAvgAtt = !empty($extAvgAtt) ? "ROUND(AVG(CASE $extAvgAtt ELSE NULL END),6) avg_attendance" : "null avg_attendance";

    return "SELECT DISTINCT
    IFNULL(l1.id, '') jsc_id,
    IFNULL(cl.id, '') class_id,
    IFNULL(l1.student_id, '') student_id,
    SUM(CASE WHEN (att.attended=1) THEN 1 ELSE 0 END) total_attended,
    SUM(CASE WHEN (att.attendance_type IN ('A','E')) THEN 1 ELSE 0 END) total_absent,
    SUM(CASE WHEN (IFNULL(att.homework,'') = 'yes') THEN 1 ELSE 0 END) total_dohw,
    $extAvgAtt
    FROM j_class cl INNER JOIN j_classstudents l1 ON cl.id = l1.class_id $ext_student AND l1.deleted = 0
    INNER JOIN meetings mt ON cl.id = mt.ju_class_id AND mt.deleted = 0 AND (IFNULL(mt.session_status, '') <> 'Cancelled')
    LEFT JOIN c_attendance att ON att.meeting_id = mt.id AND att.student_id = l1.student_id AND att.class_id = l1.class_id AND att.deleted = 0
    WHERE cl.deleted = 0 AND cl.id = '$classId'
    GROUP BY student_id, class_id";
}

//Update ClassStudent - ALL
function updateClassStudent($classId = '', $student_id = ''){
    $datetime = $GLOBALS['timedate']->nowDb();
    $date     = $GLOBALS['timedate']->nowDbDate();
    if(empty($classId)) return false;
    if(!empty($student_id)){
        $ext_student1 = "AND jsc.student_id = '$student_id'";
        $ext_student3 = "AND jt1.student_id = '$student_id'";
        $ext_student4 = "AND jt2.student_id = '$student_id'";
        $ext_student5 = "AND jst.student_id = '$student_id'";
    }

    $rows = $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(jsc.id, '') jsc_id, IFNULL(jsc.class_id, '') class_id,
        IFNULL(jsc.student_id, '') student_id, u0.total_attended total_attended,
        u0.total_absent total_absent, u0.avg_attendance avg_attendance,
        u0.total_dohw total_dohw, u2.total_amount total_amount,
        u2.total_hour total_hour, u2.start_study start_study,
        u2.end_study end_study, u3.total_hc total_hc,
        u3.total_ssc total_ssc, u3.count_ost count_ost,
        u3.count_ern count_ern, u3.count_demo count_demo,
        u3.total_ss_registered total_ss_registered,
        (CASE WHEN ('$date' < u2.start_study) THEN 'Not Started' WHEN ('$date' >= u2.start_study AND '$date' <= u2.end_study) THEN 'In Progress' WHEN ('$date' > u2.end_study AND (u1.end_delay IS NOT NULL) AND (u2.end_study < u1.end_delay)) THEN 'Delayed' ELSE 'Finished' END) status,
        (CASE WHEN (u2.jst_ost > 0) THEN 'OutStanding' WHEN (u2.jst_enrolled > 0) THEN 'Enrolled' WHEN (u2.jst_demo > 0) THEN 'Demo' ELSE '' END) type
        FROM j_classstudents jsc
        LEFT JOIN (".buildQueryAttendance($classId,$student_id).") u0 ON u0.student_id = jsc.student_id AND u0.class_id = jsc.class_id
        LEFT JOIN (SELECT DISTINCT IFNULL(jt1.student_id, '') student_id, MIN(jt1.start_study) start_delay, MAX(jt1.end_study) end_delay
        FROM j_studentsituations jt1 WHERE jt1.ju_class_id = '$classId' $ext_student3 AND (jt1.type='Delayed') AND jt1.deleted = 0 GROUP BY jt1.student_id) u1 ON u1.student_id = jsc.student_id
        LEFT JOIN (SELECT DISTINCT IFNULL(jt2.student_id, '') student_id, MIN(jt2.start_study) start_study, MAX(jt2.end_study) end_study, SUM(jt2.total_amount) total_amount, MAX(jt2.total_hour) total_hour,
        SUM(CASE WHEN (jt2.type='OutStanding') THEN 1 ELSE 0 END) jst_ost, SUM(CASE WHEN (jt2.type='Enrolled') THEN 1 ELSE 0 END) jst_enrolled, SUM(CASE WHEN (jt2.type='Demo') THEN 1 ELSE 0 END) jst_demo
        FROM j_studentsituations jt2 WHERE jt2.ju_class_id = '$classId' $ext_student4 AND (jt2.type IN ('OutStanding', 'Enrolled', 'Demo')) AND jt2.deleted = 0 GROUP BY jt2.student_id) u2 ON u2.student_id = jsc.student_id
        LEFT JOIN (SELECT DISTINCT IFNULL(jst.student_id, '') student_id, IFNULL(jst.ju_class_id, '') class_id,
        ROUND(SUM(CASE WHEN (mt.date_end <= '$datetime') THEN mt.duration_hours+(mt.duration_minutes/60) ELSE 0 END),2) total_hc,
        SUM(CASE WHEN (mt.date_end <= '$datetime') THEN 1 ELSE 0 END) total_ssc,
        SUM(CASE WHEN (mt.date_end <= '$datetime' AND jst.type = 'OutStanding') THEN 1 ELSE 0 END) count_ost,
        SUM(CASE WHEN (mt.date_end <= '$datetime' AND jst.type = 'Enrolled') THEN 1 ELSE 0 END) count_ern,
        SUM(CASE WHEN (mt.date_end <= '$datetime' AND jst.type = 'Demo') THEN 1 ELSE 0 END) count_demo,
        COUNT(DISTINCT mt.id) total_ss_registered
        FROM j_studentsituations jst
        INNER JOIN meetings_contacts mt_1 ON jst.id = mt_1.situation_id AND mt_1.deleted = 0
        INNER JOIN meetings mt ON mt.id = mt_1.meeting_id AND mt.deleted = 0
        WHERE (jst.type IN ('OutStanding','Enrolled','Demo'))
        AND jst.ju_class_id = '$classId' $ext_student5 AND jst.student_type='Contacts' AND jst.deleted = 0
        GROUP BY jst.student_id, jst.ju_class_id
        UNION ALL SELECT DISTINCT IFNULL(jst.student_id, '') student_id, IFNULL(jst.ju_class_id, '') class_id,
        ROUND(SUM(CASE WHEN (mt.date_end <= '$datetime') THEN mt.duration_hours+(mt.duration_minutes/60) ELSE 0 END),2) total_hc,
        SUM(CASE WHEN (mt.date_end <= '$datetime') THEN 1 ELSE 0 END) total_ssc,
        0 count_ost,0 count_ern, SUM(CASE WHEN (mt.date_end <= '$datetime' AND jst.type = 'Demo') THEN 1 ELSE 0 END) count_demo,
        COUNT(DISTINCT mt.id) total_ss_registered
        FROM j_studentsituations jst
        INNER JOIN meetings_leads mt_1 ON jst.id = mt_1.situation_id AND mt_1.deleted = 0
        INNER JOIN meetings mt ON mt.id = mt_1.meeting_id AND mt.deleted = 0
        WHERE jst.type ='Demo' AND jst.ju_class_id = '$classId' $ext_student5 AND jst.student_type='Leads' AND jst.deleted = 0
        GROUP BY jst.student_id, jst.ju_class_id) u3 ON u3.student_id = jsc.student_id AND u3.class_id = jsc.class_id
        WHERE jsc.class_id = '$classId' $ext_student1 AND jsc.deleted = 0
        GROUP BY class_id ,student_id");

    foreach ($rows as $key => $jst) {
        $qcst = "UPDATE j_classstudents jc
        SET jc.total_attended = ".($jst['total_attended'] ?? 'null').",
        jc.total_absent = ".($jst['total_absent'] ?? 'null').", jc.avg_attendance = ".($jst['avg_attendance'] ?? 'null').",
        jc.total_dohw = ".($jst['total_dohw'] ?? 'null').", jc.total_ssc = ".($jst['total_ssc'] ?? 'null').",
        jc.total_ss_registered = ".($jst['total_ss_registered'] ?? 'null').", jc.total_hc = ".($jst['total_hc'] ?? 'null').",
        jc.count_ost = ".($jst['count_ost'] ?? 'null').", jc.count_ern = ".($jst['count_ern'] ?? 'null').",
        jc.count_demo = ".($jst['count_demo'] ?? 'null').", jc.start_study = '{$jst['start_study']}',
        jc.end_study = '{$jst['end_study']}', jc.total_amount = ".($jst['total_amount'] ?? 'null').",
        jc.total_hrs = ".($jst['total_hour'] ?? 'null').", jc.status = '{$jst['status']}', jc.type = '{$jst['type']}'
        WHERE jc.id = '{$jst['jsc_id']}' AND jc.deleted = 0";
        $GLOBALS['db']->query($qcst);
    }

    //Update Count Students - Class
    $q2 = "UPDATE j_class cl
    INNER JOIN (SELECT DISTINCT IFNULL(j_class.id, '') class_id, COUNT(DISTINCT l1.id) number_of_student,
    ROUND(AVG(l1.avg_attendance),6) avg_attendance
    FROM j_class LEFT JOIN j_classstudents l1 ON j_class.id = l1.class_id AND l1.deleted = 0
    WHERE (j_class.id = '$classId') AND j_class.deleted = 0
    GROUP BY j_class.id) u1 ON cl.id = u1.class_id
    SET cl.number_of_student = u1.number_of_student,
    cl.avg_attendance = u1.avg_attendance
    WHERE cl.id = '$classId'";
    $GLOBALS['db']->query($q2);
    return true;
}
//Update ClassStudent - Attendance Only
function updateClassStudent_ATT($classId = '', $student_id = ''){
    if(empty($classId)) return false;
    $rows = $GLOBALS['db']->fetchArray(buildQueryAttendance($classId, $student_id));
    foreach ($rows as $key => $jst) {
        $qcst = "UPDATE j_classstudents jc
        SET jc.total_attended = ".($jst['total_attended'] ?? 'null').",
        jc.total_absent = ".($jst['total_absent'] ?? 'null').",
        jc.avg_attendance = ".($jst['avg_attendance'] ?? 'null').",
        jc.total_dohw = ".($jst['total_dohw'] ?? 'null')."
        WHERE jc.id = '{$jst['jsc_id']}' AND jc.deleted = 0";
        $GLOBALS['db']->query($qcst);
    }
    return true;
}

//return label attendance overall
function labelAttOverall($att_score, $tool_tip = ''){
    //Overall attendance
    switch (true) {
        case ($att_score === null || $att_score === ''):
            $label = "label-none";
            break;
        case ($att_score>=90):
            $label = "label-perfect";
            break;
        case ($att_score>=80):
            $label = "label-good";
            break;
        case ($att_score>=70):
            $label = "label-qualified";
            break;
        case ($att_score>=50):
            $label = "label-average";
            break;
        case ($att_score>=0):
            $label = "label-below";
            break;
    }
    if(!empty($tool_tip)){
        $label.= " simptip-position-top simptip-movable simptip-multiline";
        $extTT = "data-tooltip='$tool_tip'";
    }
    //remove useless zero digits from decimals
    if($att_score != null){
        $numDec = strlen(substr(strrchr(round($att_score,2), "."), 1));
        $att_text = format_number($att_score,$numDec,$numDec).'%';
    }else $att_text = '--';
    return "<div class='att-level $label' $extTT >$att_text</div>";
}

//return label attendance overall for export excel
function labelAttOverallForExcel($att_score){
    //Overall attendance
    switch (true) {
        case ($att_score === null || $att_score === ''):
            $color = "FFFFFF";
            break;
        case ($att_score>=90):
            $color = "42E780";
            break;
        case ($att_score>=80):
            $color = "CDEE4B";
            break;
        case ($att_score>=70):
            $color = "FFE500";
            break;
        case ($att_score>=50):
            $color = "F3960B";
            break;
        case ($att_score>=0):
            $color = "FF3B30";
            break;
    }
    //remove useless zero digits from decimals
    if($att_score != null){
        $numDec = strlen(substr(strrchr(round($att_score,2), "."), 1));
        $att_text = format_number($att_score,$numDec,$numDec).'%';
    }else $att_text = '--';
    return array(
        'color' => $color,
        'text'  => $att_text,
    );
}