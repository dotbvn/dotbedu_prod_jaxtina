<?php
//Update cached carry amount
function load_carry_forward($student_ids, $update_cf = false){
    if(!empty($student_ids)){
        if(is_array($student_ids))  $studentIds = $student_ids;
        else $studentIds = [$student_ids];
        if(count($studentIds) == 1) $ext_student = "AND (contacts.id = '{$studentIds[0]}')";
        else $ext_student = "AND (contacts.id IN ('".implode("','", $studentIds)."'))";
    }

    $student_list = array();
    foreach($studentIds as $student_id) $student_list[$student_id] = array();

    //Remain, sum_paid, sum_unpaid
    $query = "SELECT DISTINCT IFNULL(contacts.id, '') student_id,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type IN ('Cashholder','Deposit')) THEN l1.sum_paid ELSE 0 END) sum_paid,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type IN ('Cashholder')) THEN ((l1.sum_paid + l1.paid_amount + l1.deposit_amount)/((l1.payment_amount + l1.deposit_amount+ l1.paid_amount) / l1.total_hours)) ELSE 0 END) sum_paid_hours,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type IN ('Cashholder','Deposit')) THEN l1.sum_unpaid ELSE 0 END) sum_unpaid,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type IN ('Cashholder') AND l1.sum_unpaid > 0) THEN (l1.sum_unpaid/((l1.payment_amount + l1.deposit_amount) / l1.total_hours)) ELSE 0 END) sum_unpaid_hours,
    SUM(l1.remain_amount) remain_amount,
    SUM(l1.remain_hours) remain_hours,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type IN ('Cashholder', 'Deposit')) THEN 1 ELSE 0 END) count_payment,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type = 'Refund') THEN l1.payment_amount ELSE 0 END) refund_amount,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type = 'Transfer In') THEN l1.payment_amount ELSE 0 END) tf_in_amount,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type = 'Transfer In') THEN l1.total_hours ELSE 0 END) tf_in_hours,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type = 'Transfer Out') THEN l1.payment_amount ELSE 0 END) tf_out_amount,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type = 'Transfer Out') THEN l1.total_hours ELSE 0 END) tf_out_hours,
    SUM(CASE WHEN (l1.is_old=1) THEN l1.old_remain_amount ELSE 0 END) migrate_amount,
    SUM(CASE WHEN (l1.is_old=1) THEN l1.total_hours ELSE 0 END) migrate_hours
    FROM contacts
    INNER JOIN j_payment l1 ON contacts.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l1.deleted = 0
    WHERE contacts.deleted = 0 $ext_student
    GROUP BY contacts.id";
    $rows = $GLOBALS['db']->fetchArray($query);
    foreach($rows as $row){foreach ($row as $kField => $aVal) $student_list[$row['student_id']][$kField] = $aVal;}

    //Enrolled
    $query = "SELECT DISTINCT
    IFNULL(contacts.id, '') student_id,
    SUM(CASE WHEN (l2.type = 'Enrolled') THEN l2.total_amount ELSE 0 END) enrolled_amount,
    SUM(CASE WHEN (l2.type = 'Enrolled') THEN (l2.total_minute/60) ELSE 0 END) enrolled_hours,
    MIN(l2.start_study) min_start_study,
    MAX(l2.end_study) max_end_study,
    SUM(CASE WHEN (l2.type = 'Enrolled') THEN (l2.total_minute/60) ELSE 0 END) enrolled_hours,
    SUM(CASE WHEN (l2.type = 'OutStanding') THEN (l2.total_minute/60) ELSE 0 END) ost_hours
    FROM contacts
    INNER JOIN j_studentsituations l2 ON contacts.id = l2.student_id AND l2.student_type = 'Contacts' AND l2.deleted = 0 AND l2.type IN ('Enrolled','OutStanding')
    WHERE contacts.deleted = 0 $ext_student
    GROUP BY contacts.id";
    $rows = $GLOBALS['db']->fetchArray($query);
    foreach($rows as $row){foreach ($row as $kField => $aVal) $student_list[$row['student_id']][$kField] = $aVal;}

    //Studied
    $query = "SELECT DISTINCT
    IFNULL(contacts.id, '') student_id,
    SUM(CASE WHEN (l1.type = 'OutStanding') THEN ROUND(l2.duration_hours+(l2.duration_minutes/60),6) ELSE 0 END) sum_ost_hours,
    SUM(CASE WHEN (l1.type = 'Enrolled') THEN ROUND(l2.duration_hours+(l2.duration_minutes/60),6) ELSE 0 END) sum_enrolled_hours,
    SUM(CASE WHEN (l1.type = 'Enrolled') THEN (ROUND(l2.duration_hours+(l2.duration_minutes/60),6)*ROUND(l1.total_amount/(l1.total_minute/60),6)) ELSE 0 END) sum_enrolled_amount
    FROM contacts
    INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0 AND l1.student_type = 'Contacts'
    INNER JOIN meetings_contacts l2_1 ON l1.id = l2_1.situation_id AND l2_1.deleted = 0
    INNER JOIN meetings l2 ON l2.id = l2_1.meeting_id AND l2.deleted = 0
    WHERE(l2.date_start <= '{$GLOBALS['timedate']->nowDb()}')
    AND (l2.session_status <> 'Cancelled')
    AND contacts.deleted = 0 $ext_student
    GROUP BY contacts.id";
    $rows = $GLOBALS['db']->fetchArray($query);
    foreach($rows as $row){foreach ($row as $kField => $aVal) $student_list[$row['student_id']][$kField] = $aVal;}

    //Drop revenue
    $query = "SELECT DISTINCT
    IFNULL(contacts.id, '') student_id,
    SUM(IFNULL(c_deliveryrevenue.amount, 0)) drop_amount,
    SUM(IFNULL(c_deliveryrevenue.duration, 0)) drop_hours
    FROM c_deliveryrevenue
    INNER JOIN contacts ON c_deliveryrevenue.student_id = contacts.id AND contacts.deleted = 0
    INNER JOIN j_payment l2 ON c_deliveryrevenue.ju_payment_id = l2.id AND l2.deleted = 0
    WHERE c_deliveryrevenue.deleted = 0 $ext_student
    AND (c_deliveryrevenue.date_input <= '{$GLOBALS['timedate']->nowDbDate()}')
    GROUP BY contacts.id";
    $rows = $GLOBALS['db']->fetchArray($query);
    foreach($rows as $row){foreach ($row as $kField => $aVal) $student_list[$row['student_id']][$kField] = $aVal;}

    //Tính carry forward
    foreach($student_list as $stdId => $student){
        $student_list[$stdId]['carry_amount'] =  $student['carry_amount'] = ($student['sum_paid']+$student['migrate_amount']+$student['tf_in_amount'])
        - $student['sum_enrolled_amount'] - $student['tf_out_amount']
        - $student['drop_amount'] - $student['refund_amount'];

        $student_list[$stdId]['carry_hours'] = $student['carry_hours'] = ($student['sum_paid_hours']+$student['migrate_hours']+$student['tf_in_hours'])
        - $student['sum_enrolled_hours'] - $student['tf_out_hours']
        - $student['drop_hours'];

        if($update_cf) $GLOBALS['db']->query("UPDATE contacts SET
            paid_amount = ".round(floatval($student['sum_paid'])).",
            unpaid_amount = ".round(floatval($student['sum_unpaid'])).",
            remain_amount = ".round(floatval($student['remain_amount'])).",
            count_payment = ".round(floatval($student['count_payment'])).",
            total_ost_hours = ".round(floatval($student['ost_hours']),2).",
            ost_hours = ".round(floatval($student['sum_ost_hours']),2).",
            enrolled_hours = ".round(floatval($student['sum_enrolled_hours']),2).",
            enrolled_amount = ".round(floatval($student['sum_enrolled_amount'])).",
            min_start_study = " . (!empty($student['min_start_study']) ? "'" . $student['min_start_study'] . "'" : "NULL") . ",
            max_end_study = " . (!empty($student['max_end_study']) ? "'" . $student['max_end_study'] . "'" : "NULL") . ",
            remain_carry = ".round(floatval($student['carry_amount']))."
            WHERE id = '$stdId' AND deleted = 0");
    }
    return $student_list;
}

//Load Payment Course Fees
function paymentCourseFees($payB=[]){
    if(empty($payB)) return false;
    $cfOjb = json_decode(html_entity_decode($payB->coursefee_list), true);
    $q = "SELECT DISTINCT IFNULL(jsf.id, '') id, IFNULL(jsf.name, '') name, IFNULL(jsf.status, '') status,
    IFNULL(jsf.type, '') type, jsf.unit_price unit_price, jsf.type_of_course_fee type_of_course_fee
    FROM j_coursefee jsf WHERE (jsf.id IN ('".implode("','",array_keys($cfOjb))."')) AND jsf.deleted = 0";
    $rows = $GLOBALS['db']->fetchArray($q);
    foreach($rows as $key => $r) {
        $Cf          = $cfOjb[$r['id']];
        $Cf['name']  = $r['name'];
        $Cf['name_'] = '<a href="index.php?module=J_Coursefee&&record='.$r['id'].'">'.$r['name'].'</a>';
        $Cf['type_'] = $GLOBALS['app_list_strings']['type_coursefee_list'][$Cf['type']];
        switch ($Cf['type']) {
            case 'Hours':
                $Cf['quantity'] = $Cf['hours'];
                break;
            case 'Sessions':
            case 'Days':
            case 'Hour/Month':
            case 'Hour/Week':
                $Cf['quantity'] = (int)(!empty($Cf['custom']) ? $Cf['custom'] : $Cf['select']);
                break;
            default: break;
        }
        $Cf['subtotal']    = $Cf['amount'];
        $Cf['dis_amount']  = ($Cf['subtotal']/$payB->amount_bef_discount)*($payB->loyalty_amount + $payB->discount_amount + $payB->final_sponsor);
        $Cf['net_amount']  = ($Cf['subtotal'] - $Cf['dis_amount']);
        if(!is_numeric($Cf['subtotal']) || $Cf['subtotal'] < 0) $Cf['subtotal'] = 0;
        if(!is_numeric($Cf['net_amount']) || $Cf['net_amount'] < 0) $Cf['net_amount'] = 0;
        if(!is_numeric($Cf['dis_amount']) || $Cf['dis_amount'] < 0) $Cf['dis_amount'] = 0;

        $Cf['subtotal_']   = format_number($Cf['subtotal']);
        $Cf['dis_amount_'] = format_number($Cf['dis_amount']);
        $Cf['net_amount_'] = format_number($Cf['net_amount']);
        //Unset value
        unset($Cf['amount']);
        unset($Cf['custom']);
        unset($Cf['select']);
        unset($Cf['hours']);

        $cfOjb[$r['id']]   = $Cf;
    }
    return $cfOjb;
}



?>
