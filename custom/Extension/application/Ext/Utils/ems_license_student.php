<?php
function updateActiveStudent($student_id = ''){
    global $timedate;
    //Chung format st = contacts
    $today = $timedate->nowDbDate();
    if (!empty($student_id)) $ext = " AND (st.id='$student_id')";

    //Update last last_payment_date, last_receipt_paid_date
    $qud = "UPDATE contacts st LEFT JOIN (SELECT DISTINCT IFNULL(st.id, '') primaryid, MAX(l1.payment_date) last_payment_date
        FROM contacts st INNER JOIN j_payment l1 ON st.id = l1.parent_id AND l1.deleted = 0
        WHERE st.deleted = 0 $ext GROUP BY st.id) u1 ON u1.primaryid = st.id
        LEFT JOIN (SELECT DISTINCT IFNULL(st.id, '') primaryid, MAX(l2.payment_date) last_receipt_paid_date
        FROM contacts st INNER JOIN j_paymentdetail l2 ON st.id = l2.parent_id AND l2.deleted = 0 AND (IFNULL(l2.status,'') = 'Paid')
        WHERE st.deleted = 0 $ext GROUP BY st.id) u2 ON u2.primaryid = st.id
        SET st.last_payment_date = u1.last_payment_date, st.last_receipt_paid_date = u2.last_receipt_paid_date
        WHERE st.deleted = 0 $ext";
    $GLOBALS['db']->query($qud);

    $q0 = "SELECT DISTINCT IFNULL(st.id, '') student_id,
    IFNULL(st.last_payment_date, '') last_payment_date,
    IFNULL(st.date_entered, '') date_entered,
    IFNULL(st.max_end_study, '') max_end_study
    FROM contacts st WHERE st.deleted = 0 $ext";
    $rs = $GLOBALS['db']->query($q0);
    $students = array();
    while ($r = $GLOBALS['db']->fetchByAssoc($rs)) {
        $students[$r['student_id']]['max_end_study']    = $r['max_end_study'];
        $students[$r['student_id']]['last_payment_date']= $r['last_payment_date'];
        $students[$r['student_id']]['date_entered']     = $r['date_entered'];
    }
    //Handle cache fields active lifetime
    foreach ($students as $stId => $s) {
        $s['ems_end_date'] = $s['ems_end_field'] ='';
        foreach(array('date_entered','max_end_study','last_payment_date') as $field){
            if(!empty($s[$field])){
                $s[$field] = $timedate->convertToDBDate($s[$field]);
                if( empty($s['ems_end_date']) || $s[$field] >= $s['ems_end_date']){
                    $s['ems_end_date'] = $s[$field];
                    $s['ems_end_field']= $field;
                }
            }
        }
        if(empty($s['ems_end_date'])) $s['ems_end_date'] = $today;
        $s['ems_expired_date'] = date('Y-m-d', strtotime("+30 days ".$s['ems_end_date']));
        $s['ems_active_state'] = ($today > $s['ems_expired_date']) ? 0 : 1;
        $s['ems_active_update'] = $timedate->nowDb();
        //END: Handle cache fields active lifetime

        $GLOBALS['db']->query("UPDATE contacts SET
            ems_expired_date=NULLIF('{$s['ems_expired_date']}',''),
            ems_end_date=NULLIF('{$s['ems_end_date']}',''),
            ems_end_field=NULLIF('{$s['ems_end_field']}',''),
            ems_active_update=NULLIF('{$s['ems_active_update']}',''),
            ems_active_state={$s['ems_active_state']}
            WHERE id = '$stId'");
    }
    return $students;
}
