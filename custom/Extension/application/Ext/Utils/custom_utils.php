<?php
/**
* Auto-generate Increment Code @by Lap Nguyen
*
* @param string $bean
* @param bool $param <code_prefix> <date_field> <separator> <padding> <code_field>
*/
function IncrementCode($bean, $param){
    //Get Prefix
    $table      = $bean->table_name;
    $code_field = $param['code_field'];
    $prefix     = $param['code_prefix'];
    $date_field = $param['date_field'];
    $date_fm    = (!empty($param['date_format'])) ? $param['date_format'] : 'Y';
    $dateOption = (!empty($date_field) && !empty($bean->$date_field)) ? date($date_fm, strtotime('+ 7hours '. $bean->$date_field)) : '';
    $separator  = (!empty($param['separator'])) ? $param['separator'] : '' ;
    $str_code   = $prefix.$dateOption.$separator;

    $padding    = $param['padding'];
    if(!empty($str_code) && !empty($table) && !empty($code_field)){
        //Check Overwrite padding
        $query   = "SELECT IFNULL(id, '') id, IFNULL(padding, '') padding, date_entered FROM c_configid WHERE deleted = 0 AND tf_id = '$table-$code_field' AND prefix_str = '$str_code' ORDER BY date_entered DESC LIMIT 1";
        $owr     = $GLOBALS['db']->fetchOne($query);
        $ext_owr = '';
        if(!empty($owr)){
            $padding    = $owr['padding'];
            $ext_owr    = "AND date_entered >= '{$owr['date_entered']}' AND LENGTH($code_field) = ".(strlen($str_code)+$padding);
        }

        $query = "SELECT $code_field FROM $table WHERE deleted = 0 $ext_owr AND ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, ".strlen($str_code).") = '$str_code') ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";
        $result = $GLOBALS['db']->query($query);

        if($row = $GLOBALS['db']->fetchByAssoc($result))
            $last_code = $row[$code_field];
        else{
            //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
            $last_code = $str_code. str_pad('',$padding,'0',STR_PAD_LEFT); //first pad

            if(!empty($owr)){
                //TH: xoá mất dòng số 1000 => tạo lại mã 1000
                $new_code = $str_code. str_pad('1', $padding,'0',STR_PAD_RIGHT);
                return $new_code;
            }
        }

        $num = substr($last_code, -$padding, $padding);
        $num++;
        $pads = $padding - strlen($num);
        if($pads == -1){ //next number is 1000, 10000 ... continue
            $GLOBALS['db']->query("INSERT INTO c_configid (id, tf_id, prefix_str, name, padding, date_entered, date_modified, deleted, modified_user_id, created_by, assigned_user_id) VALUES (UUID(),'$table-$code_field','$str_code', '{$bean->id}','".($padding+1)."','{$bean->date_entered}','{$bean->date_modified}',0,'{$bean->created_by}','{$bean->created_by}','{$bean->created_by}');");
            $new_code = $str_code.$num;
        }else{
            $new_code = $str_code;
            //preform the lead padding 0
            for($i=0; $i < $pads; $i++) $new_code .= "0";
            $new_code .= $num;
        }
    }
    //return
    return $new_code;
}

/**
* Convert vietnamese name to no marks
*/
function viToEn($str)
{
    $str = html_entity_decode_utf8($str);
    //Convert Unicode Dung San
    $str = preg_replace('/(à|á|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ)/', 'a', $str);
    $str = preg_replace('/(Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ)/', 'A', $str);

    $str = preg_replace("/(é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ)/", 'e', $str);
    $str = preg_replace("/(É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ)/", 'E', $str);

    $str = preg_replace("/(í|ì|ỉ|ị|ĩ)/", 'i', $str);
    $str = preg_replace("/(Í|Ì|Ỉ|Ĩ|Ị)/", 'i', $str);

    $str = preg_replace("/(ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ)/", 'o', $str);
    $str = preg_replace("/(Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ)/", 'O', $str);

    $str = preg_replace("/(ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự)/", 'u', $str);
    $str = preg_replace("/(Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự)/", 'U', $str);

    $str = preg_replace("/(ý|ỳ|ỷ|ỹ|ỵ)/", 'y', $str);
    $str = preg_replace("/(Ý|Ỳ|Ỷ|Ỹ|Ỵ)/", 'Y', $str);

    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);


    //Convert Unicode To Hop
    $str = preg_replace('/(à|á|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ)/', 'a', $str);
    $str = preg_replace('/(Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ)/', 'A', $str);

    $str = preg_replace("/(é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ)/", 'e', $str);
    $str = preg_replace("/(É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ)/", 'E', $str);

    $str = preg_replace("/(í|ì|ỉ|ị|ĩ)/", 'i', $str);
    $str = preg_replace("/(Í|Ì|Ỉ|Ĩ|Ị)/", 'i', $str);

    $str = preg_replace("/(ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ)/", 'o', $str);
    $str = preg_replace("/(Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ)/", 'O', $str);

    $str = preg_replace("/(ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự)/", 'u', $str);
    $str = preg_replace("/(Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự)/", 'U', $str);

    $str = preg_replace("/(ý|ỳ|ỷ|ỹ|ỵ)/", 'y', $str);
    $str = preg_replace("/(Ý|Ỳ|Ỷ|Ỹ|Ỵ)/", 'Y', $str);

    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    $str = preg_replace("/(`)/", '', $str);
    return $str;
}

function getParentTeamName($team_id)
{
    return $GLOBALS['db']->getOne("SELECT l1.name
        FROM teams
        LEFT JOIN teams l1 ON l1.id = teams.parent_id AND l1.deleted <> 1
        WHERE teams.id = '$team_id' AND teams.deleted = 0");
}

/**
* Generate an array of string dates between 2 dates
*
* @param string $start Start date
* @param string $end End date
* @param string $format Output format (Default: Y-m-d)
*
* @return array
*/
function getDatesFromRange($start, $end, $format = 'Y-m-d')
{
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach ($period as $date) {
        $array[] = $date->format($format);
    }

    return $array;
}

function get_string_between($string, $start = "'", $end = "'")
{
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $end_pos = strpos($string, $end, $ini);
    $len = $end_pos - $ini;
    $resStr = substr($string, $ini, $len);

    if (!empty($resStr)) return $resStr;
    else {
        $string = substr($string, $end_pos + strlen($end));

        $ini = strpos($string, $start);
        $ini += strlen($start);
        $end_pos = strpos($string, $end, $ini);
        $len = $end_pos - $ini;
        $resStr = substr($string, $ini, $len);
        return $resStr;
    }
}
/**
* Check Lock-Dated
* Return TRUE - Có quyền chỉnh sửa / FALSE: Không có quyền chỉnh sửa
* @by Lap Nguyen
*
* @param string $date display date
*/
function checkDataLockDate($input_date, $lock_type = ''){
    require_once('custom/include/_helper/bs_rule_helper.php');
    $lock_info  = loadDataLockConfig();
    if(empty($input_date)) return true;

    //Overwrite lock customize
    if(!empty($lock_type)){
        $lock_info['lock_type'] = $lock_type;
        $lock_info['enable'] = true;
    }

    if ($lock_info['enable']) {
        global $timedate;
        $type = $lock_info['lock_type'];
        $lock_back  = $lock_info['lock_back'];
        $lock_date  = $lock_info['lock_date'];
        $splited    = explode('-', $lock_info['lock_date']);
        $date       = intval($splited[0]); //date lock
        $hour       = intval($splited[1]); //hour lock
        $input_date = $timedate->convertToDBDate($input_date);
        switch ($type) {
            case 'day_after':
                $tl_date    = $timedate->asDb($timedate->getNow(true)->setTime(0,0,0)->modify("-$lock_back days"),false);
                $check_date = $input_date;
                break;
            case 'last_month':
                $check_date = $timedate->asDb($timedate->fromDbDate($input_date)->setTime(0,0,0)->modify("first day of next month")->modify("+".($date-1)." days +$hour hours"));
                $tl_date    =  $timedate->asDb($timedate->getNow(true),false);
                break;
            case 'this_month':
                $check_date = $timedate->asDb($timedate->getNow(true)->setTime(0,0,0)->modify("first day of this month")->modify("+".($date-1)." days +$hour hours"));
                $last_month = $timedate->asDb($timedate->getNow(true)->setTime(0,0,0)->modify("first day of last month")->modify("+".($date-1)." days +$hour hours"));
                if($input_date < $last_month) //Xét ngày của tháng trước
                    return false;
                elseif($input_date > $check_date) // TH ngày chọn hiện tại lớn hơn ngày chốt thì vẫn cho chỉnh sửa
                    return true;
                $tl_date    =  $timedate->asDb($timedate->getNow(true),false);
                break;
            default:
                return false;
        }
        return ($tl_date > $check_date) ? false : true;
    } else return true;
}
//Get List Level By Kind Of Course
function get_list_level(){
    $sql = "SELECT DISTINCT
    IFNULL(j_kindofcourse.id, '') primaryid,
    IFNULL(j_kindofcourse.kind_of_course, '') kind_of_course,
    IFNULL(j_kindofcourse.content, '') content
    FROM j_kindofcourse
    WHERE j_kindofcourse.deleted = 0
    ORDER BY CASE WHEN (j_kindofcourse.kind_of_course = '' OR j_kindofcourse.kind_of_course IS NULL) THEN 0";
    $kocc = $GLOBALS['app_list_strings']['kind_of_course_list'];
    $count_koc = 1;
    foreach($kocc as $koc => $value) $sql .= " WHEN j_kindofcourse.kind_of_course = '$koc' THEN ".$count_koc++;
    $sql .= " ELSE $count_koc END ASC, date_entered ASC";
    $query = $GLOBALS['db']->query($sql);
    $lvl = array('' => '-none-');
    while ($row = $GLOBALS['db']->fetchByAssoc($query)) {
        $options = json_decode(html_entity_decode($row['content']),true);
        foreach ($options as $key => $option) {
            $lvl[$option['levels']] = $option['levels'];
        }
    }
    return $lvl;
}

function getTeacher()
{
    $options = array();
    $sql = "SELECT id,full_teacher_name FROM c_teachers WHERE deleted = 0";
    $result = $GLOBALS['db']->query($sql);
    while ($teacher = $GLOBALS['db']->fetchByAssoc($result)) {
        $options[$teacher['id']] = $teacher['full_teacher_name'];
    }
    return $options;
}

function getRoom()
{
    $options = array();
    $sql = "SELECT id,name FROM c_rooms WHERE deleted = 0";
    $result = $GLOBALS['db']->query($sql);
    while ($teacher = $GLOBALS['db']->fetchByAssoc($result)) {
        $options[$teacher['id']] = $teacher['name'];
    }
    return $options;
}

//Add Commission By Payment Detial
function addCommisson($bean, $types = '')
{
    if (empty($types))
        $types = $GLOBALS['app_list_strings']['commission_type_list'];

    $rateDef = $GLOBALS['app_list_strings']['commission_default_list'];
    //User get com
    $rs7 = $GLOBALS['db']->query("SELECT DISTINCT
        IFNULL(id, '') primaryId,
        IFNULL(assigned_user_id, '') assigned_user_id,
        IFNULL(user_closed_sale_id, '') user_closed_sale_id,
        IFNULL(user_pt_demo_id, '') user_pt_demo_id
        FROM j_payment WHERE id = '{$bean->payment_id}' AND deleted = 0");
    $r7 = $GLOBALS['db']->fetchByAssoc($rs7);
    $GLOBALS['db']->query("UPDATE c_commission SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE pmd_id='{$bean->id}' AND type IN ('" . implode("','", array_keys($types)) . "') AND deleted=0");

    foreach ($types as $type => $value) {
        if (!empty($type)) {
            $com = new C_Commission();
            $com->name = $bean->name . '-' . $type;
            $com->amount = $bean->payment_amount * ($rateDef[$type] / 100);
            $com->type = $type;

            $com->pmd_id = $bean->id;
            $com->payment_id = $bean->payment_id;
            $com->input_date = $bean->payment_date;
            if ($type == 'First Assigned To')
                $com->assigned_user_id = $r7['assigned_user_id'];
            elseif ($type == 'Closed Sale')
                $com->assigned_user_id = $r7['user_closed_sale_id'];
            elseif ($type == 'PT/Demo by User')
                $com->assigned_user_id = $r7['user_pt_demo_id'];

            //get Teamdefaut Users
            $assigned_user = BeanFactory::getBean('Users', $com->assigned_user_id);
            $com->team_id = $assigned_user->default_team;
            $com->team_set_id = $com->team_id;

            $com->save();
        }
    }
}

function getCenterList()
{
    global $current_user;
    $qr = "";
    if (!$current_user->isAdmin()) {
        $sql_get_my_team = "SELECT DISTINCT
        rel.team_id
        FROM
        team_memberships rel
        RIGHT JOIN teams ON (rel.team_id = teams.id)
        WHERE rel.user_id = '" . $current_user->id . "' AND teams.private = 0
        AND rel.deleted = 0 AND teams.deleted = 0";
        $result = $GLOBALS['db']->query($sql_get_my_team);

        $teamIds = array();
        while ($row = $GLOBALS['db']->fetchByAssoc($result))
            $teamIds[] = $row['team_id'];

        $qr = " AND t1.id IN ('" . implode("','", $teamIds) . "') ";
    }


    $sql_get_team = "SELECT DISTINCT IFNULL(t1.id, '') id, IFNULL(t1.name, '') name, IFNULL(t1.code_prefix, '') center_code, IFNULL(t1.short_name, '') short_name
    FROM teams t1
    INNER JOIN teams t2 ON t1.parent_id = t2.id AND t2.deleted = 0
    $qr AND t1.deleted = 0 AND t1.id NOT IN (SELECT DISTINCT tt.parent_id FROM teams tt WHERE tt.private = 0 AND tt.deleted = 0 AND (tt.parent_id <> '' AND tt.parent_id IS NOT NULL))
    ORDER BY t1.name";
    $result = $GLOBALS['db']->query($sql_get_team);
    $teams = array();
    while ($row = $GLOBALS['db']->fetchByAssoc($result)){
        $teams[$row['id']]['id']          = $row['id'];
        $teams[$row['id']]['name']        = $row['name'];
        $teams[$row['id']]['center_code'] = $row['center_code'];
        $teams[$row['id']]['short_name']  = $row['short_name'];
    }

    return $teams;
}

/*
*function split name field Họ Tên Bé  lưu trong Dotb là Firstname và Lastname.
*/
function split_fullname($fullname, $t = 1)
{

    $parts = explode(" ", $fullname);
    $lastname = array_pop($parts);
    $firstname = implode(" ", $parts);
    if ($t == 1)
        return $firstname;
    else
        return $lastname;

}

function getlastAtivities($bean)
{
    $st_id = $bean->id;
    $activities = '';
    $limit = 200;

    //Call, Task, meeting
    $call_query = "SELECT * FROM ((SELECT
    name, description, assigned_user_id, date_modified, 'call' type
    FROM calls
    WHERE parent_id = '$st_id' AND deleted = 0
    ORDER BY date_modified DESC
    LIMIT 3)
    UNION (SELECT
    name, description, assigned_user_id, date_modified, 'task' type
    FROM tasks
    WHERE parent_id = '$st_id' AND deleted = 0
    ORDER BY date_modified DESC
    LIMIT 3)
    UNION (SELECT
    name, description, assigned_user_id, date_modified, 'meeting' type
    FROM meetings
    WHERE parent_id = '$st_id' AND deleted = 0 AND meeting_type = 'Meeting'
    ORDER BY date_modified DESC
    LIMIT 3))
    ORDER BY date_modified DESC
    LIMIT 3";
    $rows = $GLOBALS['db']->fetchArray($call_query);
    $activities = '<div class="ellipsis_inline">';
    foreach ($rows as $row) {
        $activities .= "<a href='#' class='nav-link'><i class='icon-stack2'></i><span></span></a>";
    }
    $activities .= '</div>';
    if (strlen(trim($bean->description)) > $limit)
        $activities .= mb_substr($bean->description, 0, $limit, 'UTF-8') . ' ...';
    return $description;
}

/*
*function get payment remain
*/
function getPaymentRemain($id = ''){
    if(empty($id)) return false;

    $q1 = "SELECT DISTINCT
    IFNULL(jj.id, '') primaryid,
    IFNULL(jj.payment_type, '') payment_type,
    IFNULL(jj.payment_amount,'') payment_amount,
    IFNULL(lt.id, '') student_id,
    IFNULL(jj.is_old, 0) is_old,
    IFNULL((IFNULL(jj.payment_amount, 0) + IFNULL(jj.deposit_amount, 0) + IFNULL(jj.paid_amount, 0)) / IFNULL(jj.total_hours, 0),0) price,
    IFNULL(p1.collected_amount, 0) - IFNULL(p2.spent_amount, 0) - IFNULL(p4.revenue_amount, 0) + SUM(IFNULL(p3.given_amount, 0)) remain_amount,
    IFNULL(p1.collected_hours, 0) - IFNULL(p2.spent_hours, 0) + SUM(IFNULL(p3.given_hours, 0)) remain_hours,
    IFNULL(p1.sum_paid, 0) sum_paid,
    IFNULL(p1.sum_unpaid, 0) sum_unpaid
    FROM j_payment jj
    LEFT JOIN (SELECT DISTINCT
    IFNULL(j_payment.id, '') primaryid,
    (CASE WHEN (IFNULL(j_payment.is_old,0)=1) THEN (j_payment.old_remain_amount + SUM(CASE WHEN (l1.status = 'Paid' AND IFNULL(l1.is_old,0) = 0) THEN l1.payment_amount ELSE 0 END))
    ELSE SUM(CASE WHEN (l1.status = 'Paid') THEN l1.payment_amount ELSE 0 END) END) collected_amount,
    IFNULL(j_payment.total_hours, 0) collected_hours,
    SUM(CASE WHEN (l1.status = 'Paid') THEN l1.payment_amount ELSE 0 END) sum_paid,
    SUM(CASE WHEN (l1.status = 'Unpaid') THEN l1.payment_amount ELSE 0 END) sum_unpaid
    FROM j_payment
    LEFT JOIN j_paymentdetail l1 ON j_payment.id = l1.payment_id AND l1.deleted = 0 AND (l1.status <> 'Cancelled')
    WHERE (j_payment.id IN ('$id')) AND j_payment.deleted = 0
    GROUP BY j_payment.id) p1 ON p1.primaryid = jj.id
    LEFT JOIN
    (SELECT DISTINCT
    IFNULL(j_payment.id, '') primaryid,
    SUM(IFNULL(l2_1.amount, 0)) spent_amount,
    SUM(IFNULL(l2_1.hours, 0)) spent_hours
    FROM j_payment LEFT JOIN j_payment_j_payment_1_c l2_1 ON j_payment.id = l2_1.payment_idb AND l2_1.deleted = 0
    INNER JOIN j_payment l5 ON l5.id = l2_1.payment_ida AND l5.deleted = 0
    WHERE (j_payment.id IN ('$id')) AND j_payment.deleted = 0
    GROUP BY j_payment.id) p2 ON p2.primaryid = jj.id
    LEFT JOIN
    (SELECT DISTINCT
    IFNULL(j_payment.id, '') primaryid,
    SUM(IFNULL(l2_2.amount, 0)) given_amount,
    SUM(IFNULL(l2_2.hours, 0)) given_hours
    FROM j_payment LEFT JOIN j_payment_j_payment_1_c l2_2 ON j_payment.id = l2_2.payment_ida AND l2_2.deleted = 0
    INNER JOIN j_payment l4 ON l4.id = l2_2.payment_idb AND l4.deleted = 0
    WHERE (j_payment.id IN ('$id')) AND j_payment.deleted = 0
    GROUP BY j_payment.id) p3 ON p3.primaryid = jj.id
    LEFT JOIN
    (SELECT DISTINCT
    IFNULL(j_payment.id, '') primaryid,
    SUM(IFNULL(l3.amount, 0)) revenue_amount,
    SUM(IFNULL(l3.duration, 0)) revenue_hour
    FROM j_payment LEFT JOIN c_deliveryrevenue l3 ON j_payment.id = l3.ju_payment_id AND l3.deleted = 0
    WHERE (j_payment.id IN ('$id')) AND j_payment.deleted = 0
    GROUP BY j_payment.id) p4 ON p4.primaryid = jj.id
    LEFT JOIN contacts lt ON lt.id = jj.id AND jj.parent_type = 'Contacts' AND lt.deleted = 0
    WHERE (jj.id IN('$id')) AND jj.deleted = 0
    GROUP BY jj.id";
    $result = $GLOBALS['db']->query($q1);
    $pa_list = array();
    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
        if (!empty($row['primaryid'])) {
            //Loai bo cac type ko dung
            $pa_type = array('Cashholder','Deposit','Delay','Transfer In','Moving In');
            $pa                 = array();
            $pa['id']           = $row['primaryid'];
            $pa['student_id']   = $row['student_id'];
            $pa['payment_type'] = $row['payment_type'];

            $pa['sum_paid']     = $row['sum_paid'];
            $pa['sum_unpaid']   = $row['sum_unpaid'];

            if (in_array($pa['payment_type'], $pa_type)) {
                $pa['remain_amount'] = $row['remain_amount'];
                //FOC
                if($row['price'] == 0
                    && $row['remain_amount'] == 0
                    && $row['remain_hours'] > 0) $pa['remain_hours'] = $row['remain_hours'];
                //END - FOC type
                else $pa['remain_hours'] = $row['remain_amount'] / $row['price'];
            } else {
                $pa['remain_amount'] = 0;
                $pa['remain_hours'] = 0;
            }
            if (empty($pa['remain_hours']) || is_nan($pa['remain_hours']) || is_infinite($pa['remain_hours']) || $pa['remain_hours'] < '0.1') $pa['remain_hours'] = 0;
            if (empty($pa['remain_amount']) || is_nan($pa['remain_amount']) || is_infinite($pa['remain_amount']) || $pa['remain_amount'] < 0) $pa['remain_amount'] = 0;

            if ($pa['sum_paid'] < 0)   $pa['sum_paid'] = 0;
            if ($pa['sum_unpaid'] < 0) $pa['sum_unpaid'] = 0;

            $GLOBALS['db']->query("UPDATE j_payment SET
                remain_amount = {$pa['remain_amount']},
                remain_hours = {$pa['remain_hours']},
                sum_paid = {$pa['sum_paid']},
                sum_unpaid = {$pa['sum_unpaid']},
                date_modified='{$GLOBALS['timedate']->nowDb()}',
                modified_user_id='{$GLOBALS['current_user']->id}'
                WHERE id = '{$pa['id']}' AND deleted = 0");
            $pa_list[] = $pa;
        }
    }
    return $pa_list;
}

function parseAppListString($language, array $appStrings)
{
    global $app_list_strings;
    $app_list_strings = return_app_list_strings_language($language);
    $return_value = array();
    foreach ($appStrings as $appString) {
        $array = array();
        $array['name'] = $appString;
        $list = array();

        foreach ($app_list_strings[$appString] as $key => $value) {
            $list_array = array();
            $list_array['key'] = $key;
            $list_array['value'] = $value;
            array_push($list, $list_array);
        }
        $array['list'] = $list;
        array_push($return_value, $array);
    }
    return $return_value;
}

function parseAppListStringForWeb($language, array $appStrings){
    $app_list_strings = return_app_list_strings_language($language);
    $return_value = array();
    foreach ($appStrings as $appString) {
        $return_value[$appString] = $app_list_strings[$appString];
    }
    return $return_value;
}

function formatPhoneNumber($phone){
    if(substr($phone,0 , 1) === '+') return '+'.preg_replace("/[^0-9]/", "",$phone);
    $phone = preg_replace("/[^0-9]/", "",$phone);
    if(empty($phone)) return '';
    if(substr($phone,0 , 4) == '0000') $phone = substr_replace($phone,'0',0,4);
    if(substr($phone,0 , 3) == '000') $phone = substr_replace($phone,'0',0,3);
    if(substr($phone,0 , 2) == '00') $phone = substr_replace($phone,'0',0,2);
    if(substr($phone,0 , 1) !== '0') $phone = '0'.$phone;
    return $phone;
}

function array_flip_with_key($array, $key){
    $list_index = array_column($array,$key);
    $new_array = array();
    foreach($array as $ind => $value)
        $new_array[$list_index[$ind]] = $value;
    return $new_array;
}

function checkParentTeam($team_id){
    $count = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM teams WHERE parent_id = '$team_id' AND deleted = 0 AND private = 0");
    if($count > 0) return true;
    else return false;
}

if (!function_exists('split_fullname')) {
    function split_fullname($fullname, $t = 1)
    {
        $parts = explode(" ", $fullname);
        $lastname = array_pop($parts);
        $firstname = implode(" ", $parts);
        if ($t == 1)
            return $firstname;
        else
            return $lastname;

    }
}

//Save ReferenceLog
function saveReference(&$bean){
    //Tạm dùng chức năng - Do dulicate
    //return '';
    if(empty($bean->reference_logs )) return array(
        'success' => '0',
        'error'  => 'no_reference',
        );

    $logs = json_decode($bean->reference_logs,true);
    $log = BeanFactory::newBean('J_ReferenceLog');
    $log->parent_id   = $bean->id;
    $log->parent_type = $bean->module_name;
    foreach($logs as $field => $value)
        $log->$field = $value;
    $log->hits        = 1;
    $log->save();
    return array(
        'success' => '1',
        'log_id'  => $log->id,
        'parent_type'  => $log->parent_type,
        'parent_id'  => $log->parent_id,
        'hits'    => $log->hits,
    );
}

//durationToText
function durationToText($hours, $minutes){
    $duration_text = '';
    if($hours == 1) $duration_text .= $hours.' '.translate('LBL_HR','J_Class').' ';
    if($hours > 1) $duration_text .= $hours.' '.translate('LBL_HRS','J_Class').' ';
    if($minutes == 1) $duration_text .= $minutes.' '.translate('LBL_MIN','J_Class');
    if($minutes > 1) $duration_text .= $minutes.' '.translate('LBL_MINS','J_Class');
    return $duration_text;
}

// -------GET KEY PATH----------------------------\\
function getkeypath($arr, $lookup){
    if (array_key_exists($lookup, $arr))
        return array($lookup);
    else{
        foreach ($arr as $key => $subarr){
            if (is_array($subarr)){
                $ret = getkeypath($subarr, $lookup);
                if (!empty($ret))
                    return $ret;
            }
        }
    }
    return null;
}

//Tính lịch chẳn lẽ
function checkScheduleBy(string $schedule_by, array $param): bool{
    $first_dow = (date('D', $param[0]) === 'Mon') ? $param[0] : strtotime('last monday ', $param[0]);
    $d_c = floor(abs($param[1] - $first_dow)/ 604800)+1;
    if($schedule_by == '') return true;
    if($schedule_by == 'even_week' && $d_c%2 == 0) return true;
    if($schedule_by == 'odd_week' && $d_c%2 == 1) return true;
    if($schedule_by == 'every_3w3' && $d_c%3 == 0 ) return true;
    if($schedule_by == 'every_3w1' && ($d_c-1)%3 == 0 ) return true;
    return false;
}

/**
* Get list holiday in date range
*
*/
function getPublicHolidays($start_date = '', $end_date = ''){
    global $timedate;
    //Get list Public Holiday
    if(!empty($start_date)) $ext_start = "AND holiday_date >= '{$timedate->convertToDBDate($start_date,false)}'";
    if(!empty($end_date)) $ext_end = "AND holiday_date <= '{$timedate->convertToDBDate($end_date,false)}'";

    $q1 = "SELECT IFNULL(id, '') id, holiday_date, IFNULL(description, '') description
    FROM holidays WHERE deleted = 0 AND type = 'Public Holiday' $ext_start $ext_end AND apply_for = 'All'";
    $rs1 = $GLOBALS['db']->query($q1);

    $holiday_list   = array();
    while($row = $GLOBALS['db']->fetchByAssoc($rs1)) $holiday_list[$row['holiday_date']] = $row['description'];
    return $holiday_list;
}

//Xử lý Huỷ Buổi học
function handleCancelledSS_p1($data){
    $thisSS = BeanFactory::getBean('Meetings', $data['session_id'], array('disable_row_level_security'=>true));
    $class = BeanFactory::getBean('J_Class', $thisSS->ju_class_id, array('disable_row_level_security'=>true));

    //XỬ LÝ TRƯỚC - TRÁNH TÌNH TRẠNG nhiều người Cancelled 1 buổi
    $q10 = "UPDATE meetings SET session_status='Cancelled' WHERE id='{$thisSS->id}'";
    $GLOBALS['db']->query($q10);

    $newSS = BeanFactory::newBean('Meetings');
    //set value for new session
    $unset_array = array('id', 'join_url', 'external_id', 'creator', 'date_entered', 'date_modified', 'modified_user_id', 'modified_by_name', 'created_by', 'created_by_name','created_by_link', 'modified_user_link', 'syllabus_id');
    foreach ($thisSS as $key => $val){
        if (!in_array($key, $unset_array)) $newSS->$key = $val;
    }
    $start_date = $data['date'].' '.$data['start'];
    $end_date   = $data['date'].' '.$data['end'];
    $newSS->room_id        = $data['room'];
    $newSS->teacher_id     = $data['teacher'];
    $newSS->date_start     = $start_date;
    $newSS->date_end       = $end_date;
    $newSS->teaching_type  = $data['cancel_teaching_type'];
    $newSS->save();

    //update old session
    $q10 = "UPDATE meetings SET
    session_status='Cancelled',
    cancel_by='{$data['cancel_by']}',
    cancel_reason='{$data['cancel_reason']}',
    makeup_session_id='{$newSS->id}',
    lesson_number=''
    WHERE id='{$thisSS->id}'";
    $GLOBALS['db']->query($q10);

    //Xoá kết quả điểm danh & loyalty
    $GLOBALS['db']->query("UPDATE c_attendance SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE meeting_id = '{$thisSS->id}' AND deleted=0");
    $GLOBALS['db']->query("UPDATE j_loyalty SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE meeting_id = '{$thisSS->id}' AND deleted=0");

}

//Tạo lịch lớp thu gọn
function generateSmartSchedule($sss = []){
    require_once('custom/include/_helper/junior_revenue_utils.php');
    $schedule   = array();
    for($i = 0; $i < count($sss); $i++){
        $this_start = strtotime('+ 7hour '.$sss[$i]['date_start']);
        $this_end   = strtotime('+ 7hour '.$sss[$i]['date_end']);

        $this_date  = date('Y-m-d',$this_start);
        $week_date  = date('D',$this_start);
        $time       = date('g:i',$this_start).' - '.date('g:ia',$this_end);

        $schedule[$week_date.' '.$time][] = $this_date;
    }
    foreach($schedule as $key => $value){
        $first  = reset($value);
        $last   = end($value);
        $schedule_obj[$key]       = "$first|$last";
    }
    if(!empty($schedule_obj)) return json_encode($schedule_obj);
    else return '';
}
function convertMonthKeyToEnglish($arr) {
    $current_year = ' '.date('Y');
    $monthMap = [
        "Tháng 1".$current_year => "January".$current_year,
        "Tháng 2".$current_year => "February".$current_year,
        "Tháng 3".$current_year => "March".$current_year,
        "Tháng 4".$current_year => "April".$current_year,
        "Tháng 5".$current_year => "May".$current_year,
        "Tháng 6".$current_year => "June".$current_year,
        "Tháng 7".$current_year => "July".$current_year,
        "Tháng 8".$current_year => "August".$current_year,
        "Tháng 9".$current_year => "September".$current_year,
        "Tháng 10".$current_year => "October".$current_year,
        "Tháng 11".$current_year => "November".$current_year,
        "Tháng 12".$current_year => "December".$current_year
    ];

    if(is_array($arr)){
        $newArr = [];
        foreach ($arr as $key => $value) {
            $newKey = isset($monthMap[$key]) ? $monthMap[$key] : $key;
            $newArr[$newKey] = $value;
        }
        return $newArr;
    } else {
        foreach($monthMap as $key => $value){
            if($value == $arr){
                return $key;
            }
        }
    }

}
function thousandCurrencyFormat($number, $precision = 2)
{
    if ($number < 1000) {
        $n_format = number_format($number, $precision);
        $suffix = '';
    } else if ($number < 1000000) {
        $n_format = number_format($number / 1000, $precision);
        $suffix = 'K';
    } else if ($number < 1000000000) {
        // 0.9m-850m
        $n_format = number_format($number / 1000000, $precision);
        $suffix = 'M';
    } else if ($number < 1000000000000) {
        $n_format = number_format($number / 1000000000, $precision);
        $suffix = 'B';
    } else {
        $n_format = number_format($number / 1000000000000, $precision);
        $suffix = 'T';
    }

    if ($precision > 0) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }

    return $n_format . $suffix;
}

/**
* Get sum class
*/
function getSumClassInfo($class_id = ''){
    $q1 = "SELECT DISTINCT IFNULL(l2.id, '') class_id,
    l2.start_date class_start_date, l2.end_date class_end_date, l2.hours class_hour,
    IFNULL(meetings.id, '') primaryid, meetings.lesson_number lesson_number,
    meetings.date_start date_start, meetings.date_end date_end
    FROM meetings INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
    WHERE meetings.deleted = 0 AND (l2.id = '$class_id')
    ORDER BY date_start ASC";
    return md5(json_encode($GLOBALS['db']->fetchArray($q1)));
}

//Update Class Status
function updateClassStatusUtil($class_id = ''){
    $today = $GLOBALS['timedate']->nowDbDate();
    if(!empty($class_id)) $extClass = " AND (cl.id = '$class_id')";

    //UPDATE CLASS Planning
    $q1 = "SELECT DISTINCT IFNULL(cl.id, '') class_id, IFNULL(cl.status, '') status
    FROM j_class cl WHERE (cl.status IN ('In Progress', 'Finished')) AND (cl.start_date > '$today') $extClass AND cl.deleted = 0";
    $rowsNotStarted = $GLOBALS['db']->fetchArray($q1);
    if (!empty($rowsNotStarted)) {
        $u1 = "UPDATE j_class SET status='Planning' WHERE id IN ('".implode("','",array_column($rowsNotStarted, 'class_id'))."')";
        $GLOBALS['db']->query($u1);
    }

    //UPDATE CLASS In Progress
    $q2 = "SELECT DISTINCT IFNULL(cl.id, '') class_id, IFNULL(cl.status, '') status
    FROM j_class cl WHERE (cl.status IN ('Planning', 'Finished')) AND (cl.start_date <= '$today' AND cl.end_date >= '$today') $extClass AND cl.deleted = 0";
    $rowInProgress = $GLOBALS['db']->fetchArray($q2);
    if (!empty($rowInProgress)) {
        $u1 = "UPDATE j_class SET status='In Progress' WHERE id IN ('".implode("','",array_column($rowInProgress, 'class_id'))."')";
        $GLOBALS['db']->query($u1);
    }

    //UPDATE CLASS Finished
    $q3 = "SELECT DISTINCT IFNULL(cl.id, '') class_id, IFNULL(cl.status, '') status, IFNULL(cl.onl_course_id, '') onl_course_id, IFNULL(cl.onl_status, '') onl_status
    FROM j_class cl WHERE (cl.status IN ('Planning', 'In Progress')) AND (cl.end_date < '$today') $extClass AND cl.deleted = 0";
    $rowsFinished = $GLOBALS['db']->fetchArray($q3);
    if (!empty($rowsFinished)) {
        $u1 = "UPDATE j_class SET status='Finished' WHERE id IN ('".implode("','",array_column($rowsFinished, 'class_id'))."')";
        $GLOBALS['db']->query($u1);
    }


    if(!empty($class_id)){
        //Update Main Teacher
        $q4 = "SELECT DISTINCT IFNULL(l2.id, '') teacher_id, COUNT(l2.id) allcount
        FROM j_class INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
        INNER JOIN c_teachers l2 ON l1.teacher_id = l2.id AND l2.deleted = 0
        WHERE (j_class.id = '$class_id') AND j_class.deleted = 0
        GROUP BY teacher_id ORDER BY allcount DESC LIMIT 1";
        $tea = $GLOBALS['db']->fetchOne($q4);
        if (!empty($tea['teacher_id'])) $GLOBALS['db']->query("UPDATE j_class SET teacher_id='{$tea['teacher_id']}' WHERE id = '$class_id'");
        //Update TA 1
        $q4 = "SELECT DISTINCT IFNULL(l2.id, '') teacher_id, COUNT(l2.id) allcount
        FROM j_class INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
        INNER JOIN c_teachers l2 ON l1.teacher_cover_id = l2.id AND l2.deleted = 0
        WHERE (j_class.id = '$class_id') AND j_class.deleted = 0
        GROUP BY teacher_id ORDER BY allcount DESC LIMIT 1";
        $tea = $GLOBALS['db']->fetchOne($q4);
        if (!empty($tea['teacher_id'])) $GLOBALS['db']->query("UPDATE j_class SET ta1_id='{$tea['teacher_id']}' WHERE id = '$class_id'");
        //Update TA 2
        $q4 = "SELECT DISTINCT IFNULL(l2.id, '') teacher_id, COUNT(l2.id) allcount
        FROM j_class INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
        INNER JOIN c_teachers l2 ON l1.sub_teacher_id = l2.id AND l2.deleted = 0
        WHERE (j_class.id = '$class_id') AND j_class.deleted = 0
        GROUP BY teacher_id ORDER BY allcount DESC LIMIT 1";
        $tea = $GLOBALS['db']->fetchOne($q4);
        if (!empty($tea['teacher_id'])) $GLOBALS['db']->query("UPDATE j_class SET ta2_id='{$tea['teacher_id']}' WHERE id = '$class_id'");
    }


    /////////////////////////////////////////////////CLASSIN//////////////////////////////////////////////////////////
    //Nếu lớp đó có lớp học online bên ClassIn => Cũng update course đó bên ClassIn thành finished
    require_once("include/externalAPI/ClassIn/utils.php");
    foreach ($rowsFinished as $class){
        if(!empty($class['onl_course_id'])
            && $class['onl_status'] == '1'
            && ExtAPIClassIn::hasAPIConfig())
            endTheCourse($class['onl_course_id']);
    }
    //END: CLASSIN
}


?>
