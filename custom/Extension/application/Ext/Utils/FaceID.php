<?php
function RegisterFaceID($student, $url, $img_file, $action, $secret_key, $att_id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_CUSTOMREQUEST => $action,
        CURLOPT_POSTFIELDS => json_encode(array(
            "customId" => $student['id'],
            "name" => $student['full_student_name'],
            "picURI" => $img_file
        )),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'SecretKey: ' . $secret_key
        )
    ));
    $respone = curl_exec($curl);
    if ($respone == true) {
        $http_info = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($http_info == 200) {
            $result = json_decode($respone, true);
            if ($result['info']['AddSucNum'] == 1 && $result['info']['AddErrNum'] == 0) {
                curl_close($curl);
                $q = "UPDATE j_attdevice SET synced_number_students = synced_number_students + 1 WHERE id = '{$att_id}'";
                $GLOBALS['db']->query($q);
                return array ( 'success' => 1);
            }
        }
    }
    curl_close($curl);
    return array ( 'success' => 0);
}

function DeleteFaceID($url, $secret_key, $att_id){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => 'DELETE',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'SecretKey: ' . $secret_key
        )
    ));
    $respone = curl_exec($curl);
    if ($respone == true) {
        $http_info = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($http_info == 200) {
            $result = json_decode($respone, true);
            if ($result['info']['DelSucNum'] == 1 && $result['info']['DelErrNum'] == 0) {
                $q = "UPDATE j_attdevice SET synced_number_students = synced_number_students - 1 WHERE id = '{$att_id}'";
                $GLOBALS['db']->query($q);
                return array('success' => 1);
            }
        }
    }
    curl_close($curl);

}