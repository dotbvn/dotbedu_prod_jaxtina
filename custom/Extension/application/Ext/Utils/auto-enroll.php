<?php
function addEMSEnrollment($student = array()){
    require_once("custom/include/_helper/junior_class_utils.php");
    require_once("custom/include/_helper/junior_revenue_utils.php");
    global $timedate;
    //Enrollment config
    $ernCf = loadEnrollmentConfig();
    if(empty($student)) return array('success' => 0); //Student enrollment not found
    if(empty($student['type'])) $student['type'] = 'Contacts';
    if(empty($student['ost_type'])){
        if($student['type'] == 'Contacts') $student['ost_type'] = 'OutStanding';
        if($student['type'] == 'Leads') $student['ost_type'] = 'Demo';
    }
    $student['start'] = $timedate->convertToDBDate($student['start']);
    $student['end']   = $timedate->convertToDBDate($student['end']);

    //Check student in present class
    $q21 = "SELECT COUNT(IFNULL(mt.id, '')) count_sessions
    FROM meetings mt INNER JOIN meetings_".strtolower($student['type'])." l2_1 ON mt.id = l2_1.meeting_id
    AND (l2_1.".strtolower(rtrim($student['type'],'s'))."_id IN ('{$student['id']}')) AND l2_1.deleted = 0
    INNER JOIN j_studentsituations l3 ON l3.id = l2_1.situation_id AND (l3.type IN ('Enrolled','Demo')) AND l3.deleted = 0
    WHERE mt.deleted = 0 AND (mt.ju_class_id = '{$student['class_id']}') AND mt.id IN ('".implode("','", $student['enroll_list'])."')
    GROUP BY mt.ju_class_id";
    $count_sessions = $GLOBALS['db']->getOne($q21);
    if($count_sessions > 0) return array(
        'success' => 0,
        'errorLabel' => translate('LBL_EXISTING_IN_CLASS', 'J_Class'));

    if($student['type'] == 'Contacts'){
        //Check Max size
        $res = getClassSize($student['class_id']);
        if(!in_array($student['id'], $res['student_list'])){
            if(!empty($res['max_size'])){
                if($res['current_size'] <= $res['max_size'] ) $res['current_size']++;
                if($res['current_size'] > $res['max_size'])
                    return array(
                        'success'    => 0,
                        'errorLabel' => translate('LBL_OVER_CLASS_SIZE', 'J_Class'));
            }
        }
    }
    $sss = get_list_lesson_by_class($student['class_id'], $student['start'], $student['end'], $student['enroll_list']);
    if(empty($sss)) return array('success' => 0, 'errorLabel' => translate('LBL_ERR_NO_SESSION', 'J_Class')); //Class not found

    //Kiểm tra số dư
    if(!empty($student['pmId_sel'])){
        $q1 = "SELECT DISTINCT IFNULL(l2.id, '') student_id, IFNULL(l2.full_student_name, '') full_student_name,
        IFNULL(pm.modified_user_id, '') modified_user_id, IFNULL(pm.team_id, '') team_id, IFNULL(pm.payment_type, '') payment_type,
        IFNULL(pm.id, '') id, pm.remain_amount remain_amount, pm.payment_expired payment_expired,
        ((pm.deposit_amount+pm.paid_amount+pm.payment_amount)/(pm.total_hours)) price,
        pm.sum_paid sum_paid, pm.sum_unpaid sum_unpaid, pm.payment_amount payment_amount, pm.is_old is_old,
        pm.remain_hours remain_hours, IFNULL(pm.name, '') payment_name, IFNULL(pm.kind_of_course, '') kind_of_course
        FROM j_payment pm
        INNER JOIN contacts l2 ON pm.parent_id = l2.id AND l2.deleted = 0 AND pm.parent_type = 'Contacts'
        WHERE (pm.payment_type IN ('Cashholder' , 'Delay', 'Transfer In', 'Moving In'))
        AND (pm.id IN ('".implode("','", $student['pmId_sel'])."')) AND (pm.remain_hours > 0)
        AND (pm.use_type = 'Hour') AND pm.deleted = 0
        ORDER BY pm.remain_hours ASC, pm.payment_date ASC";
        $rowsP = $GLOBALS['db']->fetchArray($q1);
        $today = $timedate->nowDbDate();
        foreach($rowsP as $key => $rowP){
            //Kiểm tra loại bỏ các payment không đủ số dư
            if($rowP['remain_hours'] >= round($sss[0]['delivery_hour'],2)){
                $student['pm_selected'][$rowP['id']]['remain_amount'] = $rowP['remain_amount'];
                $student['pm_selected'][$rowP['id']]['remain_hours']  = $rowP['remain_hours'];
                $student['pm_selected'][$rowP['id']]['price']         = $rowP['price'];
                $student['pm_selected'][$rowP['id']]['team_id']       = $rowP['team_id'];
            }
            //Xét Rule Payment quá hạn sử dụng
            if(!$ernCf['allow_enroll_expired'] && ($today > $rowP['payment_expired'])){
                return array(
                    'success'    => 0,
                    'errorLabel' => translate('LBL_ERR_EXPIRED_PAYMENT', 'J_Class'));
            }
            if(!$ernCf['payment_type'] == 'Cashholder'){
                //Xét Rule full payment mới cho Enroll
                if($ernCf['allow_full_payment']
                && $rowP['payment_amount'] > 0
                && $rowP['sum_unpaid'] > 0
                && $rowP['is_old'] == 0){
                    return array(
                        'success'    => 0,
                        'errorLabel' => translate('LBL_ERR_FULL_PAYMENT', 'J_Class'));
                }
            }

            //Xét Chương trình học payment và class
            $kind_of_course = unencodeMultienum($rowP['kind_of_course']);
            if(!$ernCf['allow_enroll_diff_koc'] && !empty($kind_of_course) && !in_array($student['kind_of_course'], $kind_of_course)){
                return array(
                    'success'    => 0,
                    'errorLabel' => translate('LBL_ERR_DIFF_KOC', 'J_Class'));
            }
        }
    }
    //Xóa các OutStanding trong khoảng đã tạo
    $q3 = "SELECT DISTINCT IFNULL(jst.id, '') situation_id
    FROM j_studentsituations jst
    INNER JOIN j_class l2 ON jst.ju_class_id = l2.id AND l2.deleted = 0
    WHERE jst.deleted = 0
    AND (l2.id = '{$student['class_id']}')
    AND (jst.student_id = '{$student['id']}')
    AND (jst.type IN ('OutStanding'))
    AND ((jst.start_study < '{$student['end']}' AND jst.end_study > '{$student['end']}')
    OR (jst.start_study < '{$student['start']}' AND jst.end_study > '{$student['start']}')
    OR (jst.start_study >= '{$student['start']}' AND jst.end_study <= '{$student['end']}')
    OR (jst.start_study > '{$student['start']}' AND jst.end_study < '{$student['end']}')
    OR (jst.start_study = '{$student['start']}') OR (jst.start_study = '{$student['end']}')
    OR (jst.end_study = '{$student['start']}') OR (jst.end_study = '{$student['end']}'))";
    foreach($GLOBALS['db']->fetchArray($q3) as $key => $val){
        $situa = BeanFactory::getBean('J_StudentSituations',$val['situation_id'],array('disable_row_level_security'=>true));
        //Giữ lại ID Outstanding
        if(!empty($situa->payment_id)) $ost_pm_id = $situa->payment_id;
        if(!empty($student['pm_selected'])) $situa->no_cache = true;
        $description = $situa->description;
        $situa->mark_deleted($situa->id);
    }

    //Xóa các Delayed đã tạo trong lớp
    $q4 = "SELECT DISTINCT IFNULL(jst.id, '') situation_id
    FROM j_studentsituations jst
    INNER JOIN j_class l2 ON jst.ju_class_id = l2.id
    AND l2.deleted = 0
    WHERE jst.deleted = 0
    AND (l2.id = '{$student['class_id']}')
    AND (jst.student_id = '{$student['id']}')
    AND (jst.type = 'Delayed')";
    foreach($GLOBALS['db']->fetchArray($q4) as $key => $val){
        $situa = BeanFactory::getBean('J_StudentSituations',$val['situation_id'],array('disable_row_level_security'=>true));
        $description = $situa->description;
        $situa->mark_deleted($situa->id);
    }
    //--------------------------------------------------------------------------------
    $enrollment_id = $GLOBALS['db']->getOne("SELECT IFNULL(j_payment.id, '') FROM j_payment WHERE payment_type = 'Enrollment' AND ju_class_id = '{$student['class_id']}' AND parent_id = '{$student['id']}' AND parent_type = 'Contacts' AND deleted = 0 LIMIT 1");
    $enr = !empty($enrollment_id) ? BeanFactory::getBean('J_Payment', $enrollment_id, array('disable_row_level_security'=>true)) : BeanFactory::newBean('J_Payment');
    if(!empty($enrollment_id)) $enr = BeanFactory::getBean('J_Payment', $enrollment_id, array('disable_row_level_security'=>true));
    else{
        $enr = BeanFactory::newBean('J_Payment');
        $enr->id = create_guid();
        $enr->new_with_id = true;
    }
    $countSS = 0;
    foreach($student['pm_selected'] as $pmId => $payment){
        $enroll = BeanFactory::newBean('J_StudentSituations');
        $enroll->id = create_guid();
        $enroll->new_with_id = true;
        //Create Enroll
        $runSS = $enroll_hours = 0;
        $ruin_hours = $payment['remain_hours'];
        for($i = $countSS; $i < count($sss); $i++){
            $ruin_hours -= $sss[$i]['delivery_hour'];
            if(round($ruin_hours,2) >= 0){
                //Handle Add to Session
                if($runSS == 0){
                    $enroll->start_study = $sss[$i]['date'];
                    $enroll->start_hour  = $sss[$i]['till_hour'] - $sss[$i]['delivery_hour'];
                    if($enroll->start_hour <= '0.2') $enroll->start_hour = 0;
                }
                $enroll->end_study  = $sss[$i]['date'];
                addJunToSession($student['id'], $student['type'], $enroll->id, $sss[$i]['primaryid']);
                $enroll_hours += $sss[$i]['delivery_hour'];
                $countSS++;
                $runSS++;
            }else break;

        }
        $enroll_hours = round($enroll_hours,2);
        $enroll_hours -= $payment['remain_hours'];
        if(abs($enroll_hours) <= '0.1') $enroll_hours = 0; //Fix lỗi tròn số quá nhỏ: Lưu ý số $enroll_hours < 0 chạy logic ELSE
        if($enroll_hours == 0 ){
            $enroll->total_amount = $payment['remain_amount'];
            $enroll->total_hour   = $payment['remain_hours'];
        }else{
            $used_hrs = $payment['remain_hours'] + $enroll_hours;
            $used_amt = $used_hrs * $payment['price'];
            $enroll->total_amount = $used_amt;
            $enroll->total_hour   = $used_hrs;
        }
        $enroll->name         = $student['name'];
        $enroll->student_type = 'Contacts';
        $enroll->student_id   = $student['id'];
        $enroll->ju_class_id  = $student['class_id'];
        $enroll->type         = 'Enrolled';
        $enroll->settle_date  = !empty($student['settle_date']) ? ($GLOBALS['timedate']->convertToDBDate($student['settle_date'])) : date('Y-m-d');
        $enroll->team_id      = $payment['team_id'];
        $enroll->team_set_id  = $enroll->team_id;
        $enroll->description  = $description;
        $enroll->payment_id   = $enr->id;
        //Start total_hour
        if($enroll->total_hour > 0){
            $enroll->save();
            addRelatedPayment($enr->id, $pmId, $enroll->settle_date, $enroll->total_amount, $enroll->total_hour, $enroll->id);
            $enr->paid_amount  += $enroll->total_amount;
            $enr->paid_hours   += $enroll->total_hour;
            $enr->sessions     += $countSS;
        }
        //Update start_study/end_study Receipt
        if(!empty($student['pmd_id'])){
            $GLOBALS['db']->query("UPDATE j_paymentdetail SET
                start_study = '{$enroll->start_study}',
                end_study = '{$enroll->end_study}'
                WHERE id = '{$student['pmd_id']}' AND deleted = 0");
        }
    }
    //Handle Enrollment
    if($countSS > 0){
        $enr->payment_type  = 'Enrollment';
        $enr->parent_id     = $student['id'];
        $enr->is_processing = 1;
        $enr->ju_class_id   = $student['class_id'];
        $enr->kind_of_course= $student['kind_of_course'];
        $enr->level_string  = $student['level'];
        $enr->parent_type   = 'Contacts';
        $enr->start_study   = (empty($enr->start_study) || ($enroll->start_study < $enr->start_study)) ? $enroll->start_study : $enr->start_study;
        $enr->end_study     = (empty($enr->end_study) || ($enroll->end_study > $enr->end_study)) ? $enroll->end_study : $enr->end_study;
        $enr->payment_expired   = date('Y-m-d',strtotime("+3 months ".$enr->end_study));
        $enr->assigned_user_id  = $GLOBALS['current_user']->id;
        $enr->team_id           = $enroll->team_id;
        $enr->team_set_id       = $enroll->team_id;
        $enr->save();
        //Tính lại số dư
        foreach($student['pm_selected'] as $pmId => $payment) getPaymentRemain($pmId);
    }

    //Create OutStanding & Demo
    if($student['is_allow_ost'] && $countSS < count($sss)){
        $ost = BeanFactory::newBean('J_StudentSituations');
        $ost->id = create_guid();
        $ost->new_with_id = true;
        $runSS = $ost_hours = 0;
        for($i = $countSS; $i < count($sss); $i++){
            if($runSS == 0){
                $ost->start_study = $sss[$i]['date'];
                $ost->start_hour  = $sss[$i]['till_hour'] - $sss[$i]['delivery_hour'];
                if($ost->start_hour <= '0.2') $ost->start_hour = 0;
            }
            $ost->end_study  = $sss[$i]['date'];
            $ost_hours += $sss[$i]['delivery_hour'];
            addJunToSession($student['id'], $student['type'], $ost->id, $sss[$i]['primaryid']);
            $runSS++;
            $countSS++;
        }
        $ost->name         = $student['name'];
        $ost->student_type = $student['type'];
        $ost->student_id   = $student['id'];
        $ost->ju_class_id  = $student['class_id'];
        $ost->total_hour   = format_number($ost_hours,2,2);
        $ost->type         = $student['ost_type'];
        $ost->settle_date  = !empty($student['settle_date']) ? ($GLOBALS['timedate']->convertToDBDate($student['settle_date'])) : date('Y-m-d');
        $ost->team_id      = $student['team_id'];
        $ost->team_set_id  = $ost->team_id;
        $ost->description  = $description;
        //add related Cashholder to OutStanding - Verified OutStanding
        if(!empty($student['pmId_sel'][0])) $ost->payment_id  = $student['pmId_sel'][0];
        if(empty($ost->payment_id)) $ost->payment_id = $ost_pm_id;
        $ost->save();
    }
    //Checking Class Student
    $qStd = $GLOBALS['db']->query("UPDATE j_studentsituations SET deleted=1 WHERE
    total_hour <= 0.2
    AND type IN ('Enrolled', 'Demo', 'OutStanding')
    AND ju_class_id = '{$student['class_id']}' AND student_id = '{$student['id']}'
    AND deleted = 0");

    $countStd = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM j_studentsituations WHERE type IN ('Enrolled', 'OutStanding', 'Demo') AND deleted = 0 AND ju_class_id = '{$student['class_id']}' AND student_id = '{$student['id']}'");
    if(!empty($countStd)){
        $relcs_id = $GLOBALS['db']->getOne("SELECT IFNULL(id, '') FROM j_classstudents WHERE class_id = '{$student['class_id']}' AND student_id = '{$student['id']}' AND deleted = 0 LIMIT 1");
        if(empty($relcs_id)){
            $relcs = BeanFactory::newBean('J_ClassStudents');
            $relcs->student_type = $student['type'];
            $relcs->student_id   = $student['id'];
            $relcs->class_id     = $student['class_id'];
            $relcs->save();
        }
        //Set Count Class Number
        if($student['type'] == 'Contacts'){
            updateClassAttendance($student['class_id']);
            updateStudentStatus($student['id']);
        }
        updateClassStudent($student['class_id'], $student['id']);

        return array('success' => 1);
    }else{
        $GLOBALS['db']->query("UPDATE j_classstudents SET deleted=1 WHERE class_id='{$student['class_id']}' AND student_id='{$student['id']}' AND deleted = 0");
        if($student['type'] == 'Contacts') updateStudentStatus($student['id']);
        return array(
            'success'    => 0,
            'errorLabel' => translate('LBL_ERR_ENROLL_HOURS', 'J_Class'));
    }
}

function processAutoEnroll($payment, $class){
    //Process Auto-Enroll
    $student['class_id']       = $class->id;
    $student['settle_date']    = $GLOBALS['timedate']->nowDbDate();
    $student['team_id']        = $class->team_id;
    $student['kind_of_course'] = $class->kind_of_course;
    $student['level']          = $class->level;
    $student['id']             = $payment->parent_id;
    $student['name']           = $payment->parent_name;
    $student['type']           = $payment->parent_type;
    $student['start']          = $payment->start_study;

    $student['enroll_hours']   = ($payment->is_allow_ost) ? $payment->total_hours : $payment->remain_hours;
    $student['pmd_id']         = $payment->pmd_id;
    $student['ost_type']       = 'OutStanding';
    $student['is_allow_ost']   = $payment->is_allow_ost;
    $student['pmId_sel']       = array(0 => $payment->id);
    if(empty($student['class_id'])
        || $student['enroll_hours'] <= 0
        || empty($student['start']))
        return array('success' => 0,
            'alert' => $student['name']." ".translate('LBL_ERR_FULLY_ENROLLED', 'J_Payment'));

    //Tìm khoảng trống để ghi danh
    $q2 = "SELECT DISTINCT IFNULL(meetings.id,'') primaryid, meetings.date date,
    ROUND(meetings.duration_hours+(meetings.duration_minutes/60),9) delivery_hour,
    IFNULL(l1.type,'') situation_type, IFNULL(l1.id,'') situation_id
    FROM meetings
    INNER JOIN j_class l2 ON meetings.ju_class_id=l2.id AND l2.deleted=0
    LEFT JOIN meetings_contacts l1_1 ON meetings.id=l1_1.meeting_id AND l1_1.contact_id='{$student['id']}' AND l1_1.deleted=0
    LEFT JOIN j_studentsituations l1 ON l1.student_id='{$student['id']}' AND l1.id=l1_1.situation_id AND l1.deleted=0
    WHERE (l2.id='{$student['class_id']}') AND (meetings.date>='{$student['start']}') AND meetings.deleted=0 AND meetings.session_status <> 'Cancelled'
    GROUP BY primaryid ORDER BY meetings.date_start ASC, primaryid ASC";
    $rows = $GLOBALS['db']->fetchArray($q2);
    $sss = array();
    $enroll_hours = $student['enroll_hours'];
    foreach($rows as $ind => $row){
        if((empty($row['situation_id']) || $row['situation_type'] == 'OutStanding') && (empty($sss) || $flag_enroll)){
            $flag_enroll = true;
            $enroll_hours -= $row['delivery_hour'];
            if(round($enroll_hours,2) > '-0.2'){
                $sss[] = array(
                    'id'     => $row['primaryid'],
                    'date'   => $row['date'],
                    'd_hour' => $row['delivery_hour'],
                );
            }else break;
        }else $flag_enroll = false;
    }
    if(empty($sss)) return array('success' => 0, 'alert' => $student['name'].translate('LBL_ADD_FAIL', 'J_Class'). '<br>'.translate('LBL_ERR_FULLY_ENROLLED', 'J_Class'));
    //Update
    $student['end']         = $sss[count($sss)-1]['date'];
    $student['enroll_list'] = array_column($sss,'id');
    if($student['end'] != $payment->end_study) $GLOBALS['db']->query("UPDATE j_payment SET end_study='{$student['end']}' WHERE id='{$payment->id}' AND deleted = 0");
    $res = addEMSEnrollment($student);
    if($res['success']) return array('success' => 1, 'alert' => $student['name'].translate('LBL_ADD_SUCCESSFULLY', 'J_Class'));
    else return array('success' => 0, 'alert' => $student['name'].translate('LBL_ADD_FAIL', 'J_Class').'<br>'.$res['errorLabel']);
}
//Load Enrollment Config
function loadEnrollmentConfig(){
    //Enrollment config
    $ernCf = $GLOBALS['dotb_config']['enrollment_config'];
    if(!isset($ernCf['allow_outstanding'])) $ernCf['allow_outstanding'] = true;
    if(!isset($ernCf['allow_enroll_diff_koc'])) $ernCf['allow_enroll_diff_koc'] = true;
    if(!isset($ernCf['allow_enroll_expired'])) $ernCf['allow_enroll_expired'] = true;
    if(!isset($ernCf['allow_settle'])) $ernCf['allow_settle'] = true;
    if(!isset($ernCf['allow_full_payment'])) $ernCf['allow_full_payment'] = false;
    return $ernCf;
}

//Tính toán số dư Delay
function calculateDelay($student_id='', $student_type='', $class_id='', $from_date='', $to_date=''){
    require_once("custom/include/_helper/junior_revenue_utils.php");
    global $timedate;

    if(empty($student_id) || empty($student_type) || empty($class_id) || empty($from_date))
        return array('success' => 0,'error' => translate('LBL_ERR_CAN_NOT_SITUATION', 'J_Class'));

    //Lấy số revenue đã học
    $bf_start   = date('Y-m-d', strtotime("-1 day " . $timedate->convertToDBDate($from_date, false)));
    $revenues = get_total_revenue($student_id, $student_type, $class_id, '', $bf_start);
    foreach($revenues as $rev){
        if($rev['type'] == 'Enrolled'){
            $bf_enrolled_hours  += $rev['total_hours'];
            $bf_enrolled_amount += $rev['total_revenue'];
        }
        if($last_study < $rev['end_study']) $last_study = $rev['end_study'];
        if(in_array($rev['type'],['Demo','OutStanding'])) $bf_ost_hours += $rev['total_hours'];
    }

    if(empty($last_study)) $last_study = '-none-';
    else $last_study = $timedate->to_display_date($last_study, true);

    //Lấy giờ Delay
    $del_arr = get_remain_af_change($student_id, $student_type, $class_id, $from_date, $to_date);
    foreach($del_arr as $delay){
        if($delay['type'] == 'Enrolled'){
            $delay_amount   += $delay['amount'];
            $delay_hours    += $delay['hours'];
            $af_enrolled_amount += $delay['amount'];
            $af_enrolled_hours  += $delay['hours'];
        }elseif(in_array($delay['type'],['Demo','OutStanding']))
            $af_ost_hours   += $delay['hours'];
    }
    if($delay_amount < '1000') $delay_amount = 0;
    if($delay_hours < '0.2') $delay_hours = 0;
    $enrollment_id = $GLOBALS['db']->getOne("SELECT IFNULL(j_payment.id, '')
        FROM j_payment
        WHERE payment_type = 'Enrollment'
        AND ju_class_id = '{$class_id}'
        AND parent_id = '{$student_id}'
        AND parent_type = 'Contacts'
        AND deleted = 0 LIMIT 1");
    if(!empty($enrollment_id)){
        //Check TH delay rule cũ
        //Cal delayed Amount
        $q3 = "SELECT DISTINCT
        IFNULL(l1.id, '') enrollment_id,
        sum(IFNULL(l1_1.amount, 0)) delayed_amount,
        sum(IFNULL(l1_1.hours, 0)) delayed_hours,
        IFNULL(j_payment.id, '') pm_delay_id
        FROM j_payment
        INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
        INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0
        WHERE (l1.id = '$enrollment_id') AND j_payment.deleted = 0
        GROUP BY l1.id";
        $rows = $GLOBALS['db']->fetchArray($q3);
        foreach($rows as $dls_id => $dls){
            return array(
                'success' => 0,
                'error' => 'TH: Học viên đã phát sinh Payment Delay trong lớp. Chức năng này sẽ không hoạt động!!!. <br>Để Xóa học viên khỏi lớp, vui lòng thực hiện thao tác xóa Payment Delay sau đó làm Delay lại HOẶC liên hệ IT Admin để được trợ giúp !');
        }
    }
    return array(
        'success' => 1,
        'bf_enrolled_hours' => $bf_enrolled_hours,
        'bf_ost_hours'      => $bf_ost_hours,
        'af_enrolled_hours' => $af_enrolled_hours,
        'af_ost_hours'      => $af_ost_hours,
        'delay_amount'      => $delay_amount,
        'delay_hours'       => $delay_hours,
        'reversed_hours'    => $delay_hours + $af_ost_hours,
        'last_study'        => $last_study,
        'delay_array'       => $del_arr,
        'enrollment_id'     => $enrollment_id,
    );
}

//Handle Delay Student
function handleDelay($student_id='', $student_type='', $class_id='', $from_date='', $to_date='', $dl_date='', $dl_reason='', $dl_type='', $rvn_case='revert_delay', $dl_reason_for){
    require_once("custom/include/_helper/junior_revenue_utils.php");
    require_once("custom/include/_helper/junior_class_utils.php");
    global $timedate, $current_user;

    if(empty($dl_date)) $dl_date = $timedate->nowDbDate();
    $res = calculateDelay($student_id, $student_type, $class_id, $from_date);
    if($res['success'] == 0) return array('success' => 0);
    $delays = $res['delay_array'];

    // force_rm_from_class: BẮT BUỘC xoá hv khỏi Lớp trong 1 số TH mất thanh toán gốc
    if(empty($delays) && ($dl_type == 'Delay')) return array('success' => 0, 'errorLabel' => translate('LBL_PAYMENT_RELATED_NOT_FOUND', 'J_Class'));

    //Xử lý TH xoá khỏi lớp
    if(!empty($res['enrollment_id'])){
        $enr = BeanFactory::getBean('J_Payment', $res['enrollment_id'], array('disable_row_level_security'=>true));
        $enr->paid_hours  -= round($res['delay_hours'],2);
        $enr->paid_amount -= $res['delay_amount'];
        if($enr->paid_hours <= 0.2 || ($dl_type == 'Remove')) $enr->mark_deleted($enr->id);
        else $enr->save();
    }
    //Handle remove sessions changed situation
    $ss_remove    = get_list_revenue($student_id, $student_type, $class_id, $from_date); //$to_date bỏ do deplay lun đến cuối lớp
    $resits = array();
    foreach($ss_remove as $key => $ss) $resits[$ss['situation_id']][] = $ss['primaryid'];
    foreach($resits as $sit => $sss) removeJunFromSessions($sit, $student_type, $sss);//Tăng tốc xử lý

    //Xử lý thay đổi Situation
    $revenues  = get_total_revenue($student_id, $student_type, $class_id);
    foreach($delays as $dls_id => $dls){
        $sitChange = BeanFactory::getBean('J_StudentSituations', $dls_id, array('disable_row_level_security'=>true));
        if(empty($sitChange->id)) continue;
        $sitChange->dl_reason_for = $dl_reason_for;
        $sitChange->total_amount -= $dls['amount'];
        $sitChange->total_hour = (($sitChange->total_minute/60))- $dls['hours'];  //Tính theo phút để ko bị sai
        if($sitChange->total_amount < '1000') $sitChange->total_amount = 0;
        if($sitChange->total_hour < '0.2') $sitChange->total_hour = 0;
        if($sitChange->total_hour <= 0 || ($dl_type == 'Remove'))
            $sitChange->mark_deleted($sitChange->id);
        else{
            //Edit Situation
            foreach($revenues as $curRevenue){
                if($sitChange->id == $curRevenue['situation_id']){
                    $sitChange->start_study   = $timedate->to_display_date($curRevenue['start_study'], true);
                    $sitChange->end_study     = $timedate->to_display_date($curRevenue['end_study'], true);
                }
            }
            $sitChange->save();
        }
    }
    //TH: force_rm_from_class:  BẮT BUỘC xoá hv khỏi Lớp trong
    //Tìm tất cả Situation còn sót lại để xoá lun
    if($dl_type == 'Remove'){
        $q1 = "SELECT IFNULL(id,'') situation_id FROM j_studentsituations WHERE deleted = 0 AND ju_class_id = '$class_id' AND student_id = '$student_id'";
        $rs1 = $GLOBALS['db']->query($q1);
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1)){
            $sitChange = BeanFactory::getBean('J_StudentSituations', $row['situation_id'], array('disable_row_level_security'=>true));
            $sitChange->mark_deleted($sitChange->id);
        }
    }

    if($rvn_case == 'revert_delay'){
        $delay_hours = $res['delay_hours'];
        if($delay_hours > 0){
            foreach($delays as $dls_id => $dls){
                $dls_hours  = $dls['hours'];
                $dls_amount = $dls['amount'];
                foreach($dls['payment'] as $pmId => $relPay){
                    //TH delay có tiền
                    if($dls_amount > 0 && $relPay['ttp_amount'] > 0){
                        $used_amount = $relPay['amount'] - $dls_amount;
                        if(abs($used_amount) < 1000) $used_amount = 0;
                        if($used_amount > 0){
                            $dl_amount = $dls_amount;
                            $dl_hours  = $dls_amount/$relPay['price'];
                            $GLOBALS['db']->query("UPDATE j_payment_j_payment_1_c SET hours=hours - $dl_hours,amount=amount - $dl_amount, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE situation_id='$dls_id' AND payment_idb='$pmId' AND deleted = 0");
                            getPaymentRemain($relPay['id']);
                            $dls_amount = 0;
                        }elseif($used_amount <= 0){
                            $dl_amount = $relPay['amount'];
                            $dl_hours  = $relPay['amount']/$relPay['price'];
                            $GLOBALS['db']->query("UPDATE j_payment_j_payment_1_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE situation_id='$dls_id' AND payment_idb='$pmId' AND deleted = 0");
                            getPaymentRemain($relPay['id']);
                            $dls_amount  = abs($used_amount);
                        }
                        if($dls_amount <= 0) break;
                    }elseif($dls_hours > 0){
                        $used_hours = $relPay['hours'] - $dls_hours;
                        if(abs($used_hours) < '0.2') $used_hours = 0;
                        if($used_hours > 0){
                            $dl_hours  = $dls_hours;
                            $dl_amount = ($dls_hours * $relPay['price']);
                            $GLOBALS['db']->query("UPDATE j_payment_j_payment_1_c SET hours=hours - $dl_hours,amount=amount - $dl_amount, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE situation_id='$dls_id' AND payment_idb='$pmId' AND deleted = 0");
                            getPaymentRemain($relPay['id']);
                            $dls_hours = 0;
                        }elseif($used_hours <= 0){
                            $dl_hours  = $relPay['hours'];
                            $dl_amount = $relPay['amount'];
                            $GLOBALS['db']->query("UPDATE j_payment_j_payment_1_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE situation_id='$dls_id' AND payment_idb='$pmId' AND deleted = 0");
                            getPaymentRemain($relPay['id']);
                            $dls_hours  = abs($used_hours);
                        }
                        if($dls_hours <= 0) break;
                    }
                }
            }
        }

        //Tạo Situation Delayed
        if($res['reversed_hours'] > 0 && $dl_type == 'Delay'){
            $first = reset($ss_remove);
            $end   = end($ss_remove);
            $stu_delay = new J_StudentSituations();
            $stu_delay->name        = $situa->name;
            $stu_delay->student_type= $student_type;
            $stu_delay->student_id  = $student_id;
            $stu_delay->ju_class_id = $class_id;
            $stu_delay->payment_id  = $enr->id; //enrollment_id
            $stu_delay->total_hour  = $res['reversed_hours'];
            $stu_delay->total_amount= $res['delay_amount'];
            $stu_delay->start_study = $from_date;
            $stu_delay->end_study   = $to_date;
            $stu_delay->settle_date = $dl_date;
            $stu_delay->description = $dl_reason;
            $stu_delay->dl_reason_for = $dl_reason_for;
            $stu_delay->start_hour = format_number($first['till_hour'] - $first['delivery_hour'], 2, 2);
            $stu_delay->type = 'Delayed';
            $stu_delay->assigned_user_id = $current_user->id;
            $stu_delay->team_id          = $situa->team_id;
            $stu_delay->team_set_id      = $situa->team_set_id;
            $stu_delay->save();
        }
    }

    if($rvn_case == 'drop_revenue'){
        if(empty($enr->deleted) && ($res['delay_amount'] > 0 || $res['delay_hours'] > 0)){
            //Drop revenue
            $delivery               = new C_DeliveryRevenue();
            $delivery->name         = 'Drop revenue from enrollment id '.$enr->name;
            $delivery->student_id   = $student_id;
            $delivery->ju_payment_id = $enr->id;
            $delivery->amount       = $res['delay_amount'];
            $delivery->duration     = $res['delay_hours'];
            $delivery->date_input   = $from_date;
            $delivery->team_id      = $enr->team_id;
            $delivery->team_set_id  = $enr->team_id;
            $delivery->assigned_user_id = $current_user->id;
            $delivery->created_by       = $current_user->id;
            $delivery->modified_user_id = $current_user->id;
            $delivery->revenue_type     = 'Drop';
            $delivery->save();
        }
    }

    //save delay note
    if(!empty($dl_reason))  $GLOBALS['db']->query("UPDATE j_classstudents SET description='$dl_reason', dl_reason_for='$dl_reason_for' WHERE student_id='$student_id' AND student_type='$student_type' AND class_id='$class_id' AND deleted=0");
    updateClassStudent($class_id, $student_id);

    //Double check ClassStudent

    $qStd = $GLOBALS['db']->query("UPDATE j_studentsituations SET deleted=1 WHERE
    total_hour <= 0.2
    AND type IN ('Enrolled', 'Demo', 'OutStanding')
    AND ju_class_id = '$class_id' AND student_id = '$student_id'
    AND deleted = 0");

    $countStd = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM j_studentsituations WHERE type IN ('Enrolled', 'OutStanding', 'Demo') AND deleted = 0 AND ju_class_id = '$class_id' AND student_id = '$student_id'");
    if(empty($countStd)){
        $rowsrelcs = $GLOBALS['db']->fetchArray("SELECT IFNULL(id, '') id FROM j_classstudents WHERE class_id='$class_id' AND student_id='$student_id' AND deleted = 0");
        foreach($rowsrelcs as $key=> $rel) $GLOBALS['db']->query("UPDATE j_classstudents SET deleted=1 WHERE id='{$rel['id']}'");
    }

    //Update Status Student
    if($student_type == 'Contacts' && !empty($student_id)){
        updateStudentStatus($student_id);
        $GLOBALS['db']->query("UPDATE j_payment SET is_auto_enroll=0 WHERE ju_class_id = '$class_id' AND parent_type = 'Contacts' AND parent_id = '$student_id' AND payment_type IN ('Cashholder', 'Moving In', 'Delay', 'Transfer In') AND deleted = 0 AND is_auto_enroll = 1");
    }

    return array('success' => 1);
}

