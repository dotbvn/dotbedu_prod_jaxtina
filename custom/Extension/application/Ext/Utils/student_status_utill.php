<?php
function updateStudentStatus($studentIds = ''){
	
    load_carry_forward($studentIds,true); //update student remain
    updateActiveStudent($studentIds); //Update ActiveStudent
	
    $today = date('Y-m-d');
    //Chung format st = contacts
    if (!empty($studentIds)) $ext = " AND (st.id IN ('$studentIds'))";
    $q0 = "SELECT DISTINCT IFNULL(st.id, '') student_id,
    IFNULL(st.current_class_id, '') current_class_id,
    IFNULL(st.max_end_study, '') max_end_study,
    IFNULL(st.min_start_study, '') min_start_study,
    IFNULL(st.status, '') status,
    IFNULL(st.ost_hours, 0) ost_hours,
    IFNULL(st.count_payment, 0) count_payment
    FROM contacts st WHERE st.deleted = 0 $ext";
    $rs = $GLOBALS['db']->query($q0);
    $students = array();
    while ($r = $GLOBALS['db']->fetchByAssoc($rs)) {
        $students[$r['student_id']]['o_status']           = $r['status'];
        $students[$r['student_id']]['o_current_class_id'] = $r['current_class_id'];
        $students[$r['student_id']]['o_payment_process']  = $r['payment_process'];
        $students[$r['student_id']]['o_min_start_study']  = $r['min_start_study'];
        $students[$r['student_id']]['o_max_end_study']    = $r['max_end_study'];
        $students[$r['student_id']]['ost_hours']          = $r['ost_hours'];
        if(!empty($r['old_student_id']) && empty($r['count_payment']))
            $students[$r['student_id']]['status'] = 'Finished';  // TH học viên cũ migrate
    }

    //Status Deposit
    $q3 = "SELECT DISTINCT
    IFNULL(st.id, '') student_id,
    COUNT(pm.id) count_pm,
    COUNT(DISTINCT pm.id) AS count_deposit
    FROM j_payment pm
    INNER JOIN contacts st ON st.id = pm.parent_id AND pm.parent_type = 'Contacts' AND st.deleted = 0
    WHERE (pm.deleted = 0) AND (pm.payment_type IN ('Deposit'))
    AND ((pm.sum_paid+pm.paid_amount+pm.deposit_amount) > 0) $ext
    GROUP BY student_id
    HAVING count_deposit = count_pm";
    $row3 = $GLOBALS['db']->fetchArray($q3);
    foreach ($row3 as $key => $val) {
        $students[$val['student_id']]['status'] = 'Deposit';
        $students[$val['student_id']]['payment_process'] = 'Part Paid';
    }


    //Waiting for class
    $q3 = "SELECT DISTINCT
    IFNULL(st.id, '') student_id,
    COUNT(DISTINCT pm.id) count_new_sale
    FROM j_payment pm
    INNER JOIN contacts st ON st.id = pm.parent_id AND pm.parent_type = 'Contacts' AND st.deleted = 0
    WHERE (pm.deleted = 0) AND (pm.payment_type IN ('Cashholder'))
    AND ((pm.sum_paid+pm.paid_amount+pm.deposit_amount) > 0) $ext
    GROUP BY student_id
    HAVING count_new_sale > 0";
    $row3 = $GLOBALS['db']->fetchArray($q3);
    foreach ($row3 as $key => $val) {
        $students[$val['student_id']]['status'] = 'Waiting for class';
    }

    $q1 = "SELECT DISTINCT
    IFNULL(st.id, '') student_id, IFNULL(jst.type, '') type,
    jst.start_study start_study, jst.end_study end_study, jst.ju_class_id class_id
    FROM j_studentsituations jst
    INNER JOIN contacts st ON jst.student_id = st.id AND st.deleted = 0 AND jst.student_type = 'Contacts'
    WHERE jst.deleted = 0 $ext
    ORDER BY student_id,
    CASE WHEN IFNULL(jst.type, '') = 'Enrolled' THEN 1
    WHEN IFNULL(jst.type, '') = 'Demo' THEN 2
    WHEN IFNULL(jst.type, '') = 'OutStanding' THEN 3
    WHEN IFNULL(jst.type, '') = 'Delayed' THEN 4
    ELSE 5 END ASC, start_study DESC";
    $r1 = $GLOBALS['db']->fetchArray($q1);
    foreach($r1 as $row){
        $stId = $row['student_id'];
        if(in_array($row['type'],['OutStanding','Enrolled','Demo'])){
            //Kiểm lớp học hiện tại
            if ($today >= $row['start_study']
                && $today <= $row['end_study'])
                $students[$stId]['current_class_id'] = $row['class_id'];
        }

        //Enrolled
        if(in_array($row['type'],['Enrolled'])){
            //Tính min_start_study và max_end_study
            if($students[$stId]['min_start_study'] > $row['start_study']
                || empty($students[$stId]['min_start_study']))
                $students[$stId]['min_start_study'] = $row['start_study'];

            if($students[$stId]['max_end_study'] < $row['end_study']
            || empty($students[$stId]['max_end_study'])){
                $students[$stId]['max_end_study'] = $row['end_study'];
                $students[$stId]['last_class_id'] = $row['class_id'];
            }
        }

        //Demo
        if(in_array($row['type'],['Demo'])
            && ($today >= $row['start_study'] && $today <= $row['end_study']))
            $students[$stId]['status'] = 'Demo';

        //OutStanding
        if(in_array($row['type'],['OutStanding'])
            && ($today >= $row['start_study'] && $today <= $row['end_study']))
            $students[$stId]['status'] = 'OutStanding';

        //Delayed
        if(in_array($row['type'],['Delayed'])
            && $students[$stId]['status'] != 'OutStanding'
            && (($today >= $row['start_study'] && $today <= $row['end_study'])))
            $students[$stId]['status'] = 'Delayed';
    }


    //Check full-paid/Unpaid
    $query = "SELECT DISTINCT IFNULL(st.id, '') student_id,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type IN ('Cashholder','Deposit')) THEN l1.sum_paid ELSE 0 END) sum_paid,
    SUM(CASE WHEN (l1.is_old=0 AND l1.payment_type IN ('Cashholder','Deposit')) THEN l1.sum_unpaid ELSE 0 END) sum_unpaid
    FROM contacts st
    INNER JOIN j_payment l1 ON st.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l1.deleted = 0
    WHERE st.deleted = 0 $ext
    GROUP BY st.id";
    $row3 = $GLOBALS['db']->fetchArray($query);
    foreach ($row3 as $key => $val) {
        if($val['sum_paid'] == 0 && $val['sum_unpaid'] > 0)
            $students[$val['student_id']]['payment_process'] = 'Unpaid';

        if($val['sum_paid'] > 0 && $val['sum_unpaid'] == 0
            && $students[$val['student_id']]['status'] <> 'Deposit' )
            $students[$val['student_id']]['payment_process'] = 'Full Paid';

        if($val['sum_paid'] > 0 && $val['sum_unpaid'] > 0)
            $students[$val['student_id']]['payment_process'] = 'Part Paid';
    }

    foreach ($students as $stId => $s) {
        if(empty($s['min_start_study']) && empty($s['max_end_study'])){
            //            if(empty($students[$stId]['status']))
            //                $students[$stId]['status'] = 'Waiting for class'; //Waiting for class
        }else{
            if(($today < $s['min_start_study']
                || ($today >= $s['min_start_study'] && $today <= $s['max_end_study']))
                && ($students[$stId]['status'] != 'OutStanding'
                    && $students[$stId]['status'] != 'Demo'))
                $students[$stId]['status'] = 'In Progress';  //In Progress
            if($today > $s['max_end_study']
            && !in_array($students[$stId]['status'],['OutStanding','In Progress','Delayed'])){
                if(empty($students[$stId]['current_class_id']))
                    $students[$stId]['current_class_id'] = $s['last_class_id']; //Lớp sau cùng là Last Class nếu học viên Finished
                $students[$stId]['status'] = 'Finished'; //Finished
            }
            //Kêt thúc Outstanding vẫn là Outstanding
            if($students[$stId]['status'] == 'Finished' && $students[$stId]['ost_hours'] > 0 )
                $students[$stId]['status'] = 'OutStanding'; //OutStanding
        }
    }

    //End: By Lap nguyen
    foreach ($students as $stId => $s) {
        if(empty($s['status'])) $s['status'] = 'Converted';
        if(empty($s['payment_process'])) $s['payment_process'] = 'None';
        if(($s['status']                != $s['o_status'])
            || ($s['current_class_id']  != $s['o_current_class_id'])
            || ($s['payment_process']   != $s['o_payment_process'])
            || ($s['max_end_study']     != $s['o_max_end_study'])
            || ($s['min_start_study']   != $s['o_min_start_study']))
            $GLOBALS['db']->query("UPDATE contacts SET status='{$s['status']}',
                current_class_id='{$s['current_class_id']}',
                payment_process='{$s['payment_process']}',
                max_end_study=NULLIF('{$s['max_end_study']}',''),
                min_start_study=NULLIF('{$s['min_start_study']}','')
                WHERE id = '$stId'");
    }
    return $students;
}