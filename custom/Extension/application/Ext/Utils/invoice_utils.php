<?php
//Copy row excel
function copyRowFull(&$ws_from, &$ws_to, $row_from, $row_to) {
    $ws_to->getRowDimension($row_to)->setRowHeight($ws_from->getRowDimension($row_from)->getRowHeight());
    $lastColumn = $ws_from->getHighestColumn();
    ++$lastColumn;
    for ($c = 'A'; $c != $lastColumn; ++$c) {
        $cell_from = $ws_from->getCell($c.$row_from);
        $cell_to = $ws_to->getCell($c.$row_to);
        $cell_to->setXfIndex($cell_from->getXfIndex()); // black magic here
        $cell_to->setValue($cell_from->getValue());
    }
    //Xu ly merge cell
    $merged_list = $ws_from->getMergeCells();
    $max_re = $row_to - $row_from;
    foreach($merged_list as $merged => $strMedged){
        $part = explode(":",$strMedged);
        $r1 = $ws_from->getCell($part[0])->getRow();
        $c1 = $ws_from->getCell($part[0])->getColumn();

        $r2 = $ws_from->getCell($part[1])->getRow();
        $c2 = $ws_from->getCell($part[1])->getColumn();

        $ws_to->mergeCells($c1.strval($r1+$max_re).':'.$c2.strval($r2+$max_re));
    }
}

function getReceiptData($id) {
    $sql = "SELECT DISTINCT
    IFNULL(l1.id, '') payment_id,
    IFNULL(l1.name, '') l1_name,
    IFNULL(l1.kind_of_course, '') kind_of_course,
    IFNULL(l9.id, '') company_id,
    IFNULL(l9.name, '') company_name,
    IFNULL(l9.billing_address_street, '') company_address,
    IFNULL(l9.tax_code, '') tax_code,
    IFNULL(l4.name, '') course_fee_name,
    IFNULL(l4.extend_vat, '') extend_vat,
    l1.final_sponsor_percent final_sponsor_percent,
    l4.type_of_course_fee type_of_course_fee,
    l1.tuition_fee payment_tuition_fee,
    l1.tuition_hours payment_tuition_hours,
    IFNULL(j_paymentdetail.id, '') primaryid,
    IFNULL(j_paymentdetail.name, '') name,
    j_paymentdetail.payment_date payment_date,
    j_paymentdetail.printed_date printed_date,
    IFNULL(j_paymentdetail.payment_method, '') payment_method,
    j_paymentdetail.before_discount before_discount,
    j_paymentdetail.discount_amount discount_amount,
    j_paymentdetail.sponsor_amount sponsor_amount,
    j_paymentdetail.payment_no payment_no,
    j_paymentdetail.payment_amount payment_amount,
    IFNULL(j_paymentdetail.pos_code, '') pos_code,
    IFNULL(j_paymentdetail.inv_code, '') inv_code,
    l1.tuition_hours tuition_hours,
    l1.deposit_amount deposit_amount,
    l1.paid_amount paid_amount,
    l1.payment_type payment_type,
    l1.total_after_discount total_after_discount,
    l1.parent_type parent_type,
    l1.description description,
    j_paymentdetail.reference_document reference_document,
    j_paymentdetail.reference_number reference_number,
    j_paymentdetail.description detail_description,
    j_paymentdetail.is_discount is_discount,
    IFNULL(l2.id, '') l2_id,
    IFNULL(l5.name, '') team_name,
    IFNULL(l5.description, '') team_address,
    IFNULL(l3.contact_id, '') student_id,
    IFNULL(l6.name, '') invoice_no,
    IFNULL(l6.invoice_date, '') invoice_date,
    IFNULL(l2.full_user_name, '') assigned_user_name,
    IFNULL(l7.full_user_name, '') created_by_name,
    IFNULL(l3.id, '') l3_id,
    IFNULL(l3.full_student_name, '') student_name,
    l3.primary_address_street student_address,
    IFNULL(l3.phone_mobile, '') student_phone,
    IFNULL(la.full_lead_name, '') lead_name,
    la.primary_address_street lead_address,
    IFNULL(la.phone_mobile, '') lead_phone,
    IFNULL(group_concat(l11.name separator ','), '') class_name
    FROM j_paymentdetail
    LEFT JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
    LEFT JOIN j_studentsituations l10 ON l1.id=l10.payment_id AND l10.deleted=0
    LEFT JOIN j_class l11 ON l10.ju_class_id=l11.id AND l11.deleted=0
    LEFT JOIN users l2 ON j_paymentdetail.assigned_user_id = l2.id AND l2.deleted = 0
    LEFT JOIN users l7 ON j_paymentdetail.created_by = l7.id AND l7.deleted = 0
    LEFT JOIN contacts l3 ON l3.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l3.deleted = 0
    LEFT JOIN leads la ON la.id = l1.parent_id AND l1.parent_type = 'Leads' AND la.deleted = 0
    LEFT JOIN j_coursefee_j_payment_1_c l4_1 ON l1.id = l4_1.j_coursefee_j_payment_1j_payment_idb AND l4_1.deleted = 0
    LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_1j_coursefee_ida AND l4.deleted = 0
    INNER JOIN teams l5 ON l1.team_id = l5.id AND l5.deleted = 0
    LEFT JOIN j_invoice l6 ON j_paymentdetail.invoice_id = l6.id AND l6.deleted = 0 AND l6.status <> 'Cancelled'
    LEFT JOIN accounts l9 ON l1.account_id = l9.id AND l9.deleted = 0
    WHERE (j_paymentdetail.id = '$id') AND j_paymentdetail.deleted = 0
    GROUP BY primaryid";
    $res     = $GLOBALS['db']->query($sql);
    $r       = $GLOBALS['db']->fetchByAssoc($res);
    return $r;
}

function parseInvField($module, $id, $content, $string) {
    $product = BeanFactory::getBean($module , $id);
    foreach ($product->field_defs as $keyField => $aFieldName) {
        if (in_array($aFieldName['type'], ['varchar', 'id', 'currency', 'decimal', 'datetime', 'date', 'int'])) {
            if ($aFieldName['type'] == 'currency')
                $product->$keyField = format_number(abs($product->$keyField));
            if ($aFieldName['type'] == 'decimal')
                $product->$keyField = format_number(abs($product->$keyField), $aFieldName['precision'], $aFieldName['precision']);
            if ($aFieldName['type'] == 'int')
                $product->$keyField = format_number(abs($product->$keyField) , 0,0);
            $content = str_replace( $string . $keyField . '}', $product->$keyField, $content);
        }
    }
    return $content;
}

//Kiểm tra sản phẩm thuộc Product type -No invoice- ==> Không xuất hóa đơn
function noInvoiceCheck($paymentId) {
    $q3 = "SELECT DISTINCT
    COUNT(DISTINCT l4.id) count_tax_no_inv
    FROM j_inventorydetail
    INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id AND l1.deleted = 0
    INNER JOIN j_payment_j_inventory_1_c l2_1 ON l1.id = l2_1.j_payment_j_inventory_1j_inventory_idb AND l2_1.deleted = 0
    INNER JOIN j_payment l2 ON l2.id = l2_1.j_payment_j_inventory_1j_payment_ida AND l2.deleted = 0
    INNER JOIN product_templates l3 ON j_inventorydetail.book_id = l3.id  AND l3.deleted = 0
    INNER JOIN taxrates l4 ON l3.taxrate_id = l4.id  AND l4.deleted = 0 AND l4.name = '-No invoice-'
    WHERE (((l2.id = '$paymentId')))
    AND j_inventorydetail.deleted = 0";
    $rowTax = $GLOBALS['db']->fetchOne($q3);
    if($rowTax['count_tax_no_inv'] > 0)  return false;

    $q4 = "SELECT DISTINCT
    COUNT(DISTINCT j_payment.id) count_p
    FROM j_payment
    INNER JOIN j_coursefee_j_payment_1_c l1_1 ON j_payment.id = l1_1.j_coursefee_j_payment_1j_payment_idb AND l1_1.deleted = 0
    INNER JOIN j_coursefee l1 ON l1.id = l1_1.j_coursefee_j_payment_1j_coursefee_ida AND l1.deleted = 0
    INNER JOIN taxrates l4 ON l1.taxrate_id = l4.id  AND l4.deleted = 0 AND l4.name = '-No invoice-'
    WHERE (j_payment.id = '$paymentId') AND j_payment.deleted = 0";
    $rowTax = $GLOBALS['db']->fetchOne($q4);
    if($rowTax['count_p'] > 0)  return false;
    return true;
}

?>
