<?php
//GET VietQR CONFIG
function getVietQRConfig($param = array()){
    global $dotb_config;
    //populate param
    if(!empty($param['team_id'])){
        $teamSetList = array_column($GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(team_set_id, '') team_set_id FROM team_sets_teams WHERE team_id = '{$param['team_id']}' AND deleted = 0"),'team_set_id');
        $extW .= "AND ((vc.team_id = '{$param['team_id']}') OR (vc.team_set_id IN('".implode("','",$teamSetList)."'))) ";
    }
    if(!empty($param['bank_id'])) $extW .= "AND vc.bank_id = '{$param['bank_id']}' ";
    if(!empty($param['account_number'])) $extW .= "AND vc.account_number = '{$param['account_number']}' ";

    $q_p = "SELECT DISTINCT
    IFNULL(vc.id,'') primaryId,
    IFNULL(vc.picture,'') logo,
    IFNULL(vc.bank_id,'') bank_id,
    IFNULL(vc.account_holder_name,'') account_holder_name,
    IFNULL(vc.account_number,'') account_number,
    IFNULL(vc.pattern,'')  pattern,
    IFNULL(vc.ext_content,'') ext_content,
    IFNULL(vc.len_rid,8) len_rid,
    IFNULL(vc.team_id,'') team_id,
    IFNULL(vc.engine_type,'') engine_type,
    IFNULL(vc.api_key,'') api_key,
    IFNULL(vc.team_set_id,'') team_set_id,
    IFNULL(l1.id,'') team_id,
    IFNULL(l1.code_prefix,'') team_code
    FROM j_vietqrconfig vc
    INNER JOIN teams l1 ON l1.id = vc.team_id AND l1.deleted = 0
    WHERE vc.deleted = 0 $extW AND vc.status = 'Enable'
    ORDER BY vc.date_entered DESC LIMIT 1";
    $row = $GLOBALS['db']->fetchOne($q_p);
    if(empty($row)) return '';
    //format data
    $row['pattern']         = htmlspecialchars_decode($row['pattern']);
    $row['account_holder_name']= htmlspecialchars_decode($row['account_holder_name']);
    $row['bank_name']       = htmlspecialchars_decode($GLOBALS['app_list_strings']['vietqr_bank_list'][$row['bank_id']]);
    $row['bank_name_short'] = get_string_between($row['bank_name'],'(',')');
    $row['len_rid']         = (int)$row['len_rid'];
    return $row;
}

function formatQRPrefix($string){
    return strtoupper(preg_replace('/[^a-zA-Z0-9]/','',viToEn(trim($string))));
}

//GENERATE VietQR IMAGE
function getVietQR($receipt, $template = 'compact'){ //Template: qr_only, compact , compact2, print
    global $dotb_config;
    //load VietQR Config
    $row = getVietQRConfig(['team_id'=>$receipt->team_id]);
    if(empty($row)) return array('success' => 0);
    $patterns = unencodeMultienum($row['pattern']);
    $content    = '';
    foreach($patterns as $_key => $pattern){
        switch ($pattern){
            case 'BRANDNAME':
                $content .= formatQRPrefix($dotb_config['brand_id']);
                break;
            case 'CENTER_CODE':
                $content  .= formatQRPrefix($GLOBALS['db']->getOne("SELECT IFNULL(code_prefix,'') FROM teams WHERE id = '{$receipt->team_id}'"));
                break;
            case 'RECEIPT_ID':
                $content .= formatQRPrefix($receipt->name);
                break;
            case 'USER_ASSIGNED_TO':
                $payment = BeanFactory::getBean('J_Payment', $receipt->payment_id, array('disable_row_level_security' => true));
                $content .= ' '.$payment->assigned_user_name;
                break;
            case 'STUDENT_NAME':
                if(!empty($receipt->parent_name)) $content .= ' '.$receipt->parent_name;
                break;
        }
    }
    if(!empty($row['ext_content'])) $content .= ' '.$row['ext_content'];
    $content   =  strtoupper(preg_replace('/[^a-zA-Z0-9 ]/','', viToEn($content))); //has space
    if(!empty($row['logo'])){
        $logo_QR = '"logo": "'.$dotb_config['site_url'].'/upload/resize/'.$row['logo'].'"';
        $logo    = $dotb_config['site_url'].'/upload/resize/'.$row['logo'];
    }
    $curl      = curl_init();
    $param     = '{
    "accountNo": "'.$row['account_number'].'",
    "acqId": '.$row['bank_id'].',
    "accountName": "'.$row['account_holder_name'].'",
    "amount": '.$receipt->payment_amount.',
    "addInfo": "'.$content.'",
    "format": "text",
    "template": "'.$template.'"
    }';
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.vietqr.io/v2/generate',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $param,
        CURLOPT_HTTPHEADER => array(
            'x-client-id: df0f6840-f7ef-4226-93eb-2d58035ef25d',
            'x-api-key: 130721cc-93f2-4dcf-a4af-c0866390fa7c',
            'Content-Type: application/json'
        ),
    ));
    $response = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($response, 1);
    if ($response['code'] == '00') {
        //Xoá bớt files
        $_dir = 'upload/vietqr';
        if (!file_exists($_dir)) mkdir($_dir, 0777, true);
        $fi = new FilesystemIterator($_dir, FilesystemIterator::SKIP_DOTS);
        if(iterator_count($fi) > 10) array_map('unlink', glob("$_dir/*"));
        $img = $_dir.'/'.create_guid().'.png';
        file_put_contents($img, base64_decode(preg_replace('#^data:image/\w+;base64,#i','',$response['data']['qrDataURL'])));
    }
    return array(
        'success'            => 1,
        'img_path'           => $img,
        'account_number'     => $row['account_number'],
        'bank_id'            => $row['bank_id'],
        'logo'               => $logo,
        'bank_name'          => $row['bank_name'],
        'bank_name_short'    => $row['bank_name_short'],
        'account_holder_name'=> $row['account_holder_name'],
        'payment_amount'     => $receipt->payment_amount,
        'content'            => $content,
    );
}
/**
* force new transaction sync right now
* Đồng bộ tức thì thanh toán
*/
function forceReconcile($receipt){
    //load VietQR Config
    $row = getVietQRConfig(['team_id' => $receipt->team_id]);
    if(empty($row)) return array('success' => 0);
    if($row['engine_type'] == 'Casso'
    && !empty($row['api_key'])){
        $curl  = curl_init();
        $postdata = json_encode(array('bank_acc_id' => $row['account_number']));
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://oauth.casso.vn/v2/sync',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $postdata,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Apikey '.$row['api_key'],
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, 1);
        if($response['message']=='success') return array('success'=>1);
    }
    return array('success' => 0);
}


//Check reconcile status
function reconcileState($amount1, $amount2){
    $reconcile_status = '';
    if($amount1 <= 0 ) $reconcile_status = '';
    elseif($amount1 == $amount2) $reconcile_status = 'fullpaid';
    elseif($amount1 < $amount2) $reconcile_status = 'underpaid';
    elseif($amount1 > $amount2) $reconcile_status = 'overpaid';
    return $reconcile_status;
}
?>
