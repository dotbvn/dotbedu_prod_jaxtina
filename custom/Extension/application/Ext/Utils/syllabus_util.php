<?php
//Update Get Syllabus
function getSyllabus($lessonplan_id) {
    $q = "SELECT IFNULL(sb.id, '') primary_id,
    IFNULL(lp.name, '') lessonplan_name,
    IFNULL(sb.lesson, '') lesson,
    IFNULL(sb.lesson_type, '') type,
    IFNULL(sb.learning_type, '') learning_type,
    IFNULL(sb.name, '') theme,
    IFNULL(sb.homework, '') homework,
    IFNULL(sb.note_for_teacher, '') objective,
    IFNULL(sb.description, '') content
    FROM j_syllabus sb INNER JOIN j_lessonplan lp ON lp.id = sb.lessonplan_id AND lp.deleted = 0
    WHERE lp.id = '$lessonplan_id' AND sb.deleted = 0
    ORDER BY CAST(lesson AS DECIMAL)";
    return $GLOBALS['db']->fetchArray($q);
}
//Update Class Syllabus
function updateClassSyllabus($class_id, $lessonplan_id = '') {
    if(!empty($class_id) && empty($lessonplan_id)) $lessonplan_id = $GLOBALS['db']->getOne("SELECT IFNULL(lessonplan_id, '') FROM j_class WHERE id='$class_id' AND deleted=0");
    if (!empty($lessonplan_id) && !empty($class_id)) {
        $updateQuery = "UPDATE meetings
        LEFT JOIN (SELECT IFNULL(l2.id, '') primary_id, IFNULL(l2.lesson, '') lesson, IFNULL(l2.learning_type, '') learning_type
        FROM j_lessonplan l1 INNER JOIN j_syllabus l2 ON l2.lessonplan_id = l1.id AND l2.deleted = 0
        WHERE l1.id = '$lessonplan_id' AND l1.deleted = 0) u1 ON (meetings.lesson_number = u1.lesson) AND (meetings.session_status <> 'Cancelled')
        SET meetings.syllabus_id = u1.primary_id, meetings.type = u1.learning_type
        WHERE (meetings.deleted = 0) AND (meetings.ju_class_id = '$class_id') AND (meetings.session_status <> 'Cancelled')";
        $GLOBALS['db']->query($updateQuery);
        return true;
    }else return false;
}