<?php
 //WARNING: The contents of this file are auto-generated
$beanList['J_Class'] = 'J_Class';
$beanFiles['J_Class'] = 'modules/J_Class/J_Class.php';
$moduleList[] = 'J_Class';

$beanList['J_Coursefee'] = 'J_Coursefee';
$beanFiles['J_Coursefee'] = 'modules/J_Coursefee/J_Coursefee.php';
$moduleList[] = 'J_Coursefee';

$beanList['J_Targetconfig'] = 'J_Targetconfig';
$beanFiles['J_Targetconfig'] = 'modules/J_Targetconfig/J_Targetconfig.php';
$moduleList[] = 'J_Targetconfig';
$bwcModules[] = 'J_Targetconfig';


$beanList['J_Inventory'] = 'J_Inventory';
$beanFiles['J_Inventory'] = 'modules/J_Inventory/J_Inventory.php';
$moduleList[] = 'J_Inventory';

$beanList['J_Inventorydetail'] = 'J_Inventorydetail';
$beanFiles['J_Inventorydetail'] = 'modules/J_Inventorydetail/J_Inventorydetail.php';
$report_include_modules['J_Inventorydetail'] = 'J_Inventorydetail';
$modInvisList[] = 'J_Inventorydetail';

$beanList['J_Invoice'] = 'J_Invoice';
$beanFiles['J_Invoice'] = 'modules/J_Invoice/J_Invoice.php';
$moduleList[] = 'J_Invoice';
$beanList['J_Kindofcourse'] = 'J_Kindofcourse';
$beanFiles['J_Kindofcourse'] = 'modules/J_Kindofcourse/J_Kindofcourse.php';
$moduleList[] = 'J_Kindofcourse';


$beanList['J_PaymentDetail'] = 'J_PaymentDetail';
$beanFiles['J_PaymentDetail'] = 'modules/J_PaymentDetail/J_PaymentDetail.php';
$moduleList[] = 'J_PaymentDetail';


$beanList['J_PTResult'] = 'J_PTResult';
$beanFiles['J_PTResult'] = 'modules/J_PTResult/J_PTResult.php';
$modInvisList[] = 'J_PTResult';
$report_include_modules['J_PTResult'] = 'J_PTResult';

$beanList['J_School'] = 'J_School';
$beanFiles['J_School'] = 'modules/J_School/J_School.php';
$moduleList[] = 'J_School';


$beanList['J_Sponsor'] = 'J_Sponsor';
$beanFiles['J_Sponsor'] = 'modules/J_Sponsor/J_Sponsor.php';
$report_include_modules['J_Sponsor'] = 'J_Sponsor';
$modInvisList[] = 'J_Sponsor';

$beanList['J_Teachercontract'] = 'J_Teachercontract';
$beanFiles['J_Teachercontract'] = 'modules/J_Teachercontract/J_Teachercontract.php';
$moduleList[] = 'J_Teachercontract';
$beanList['J_Voucher'] = 'J_Voucher';
$beanFiles['J_Voucher'] = 'modules/J_Voucher/J_Voucher.php';
$moduleList[] = 'J_Voucher';

?>