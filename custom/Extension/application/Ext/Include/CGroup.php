<?php
 //WARNING: The contents of this file are auto-generated
$beanList['C_SMS'] = 'C_SMS';
$beanFiles['C_SMS'] = 'modules/C_SMS/C_SMS.php';
$moduleList[] = 'C_SMS';

//An module tren Role và Menu
$beanList['C_Attendance'] = 'C_Attendance';
$beanFiles['C_Attendance'] = 'modules/C_Attendance/C_Attendance.php';
$report_include_modules['C_Attendance'] = 'C_Attendance';
$modInvisList[] = 'C_Attendance';

$beanList['C_Commission'] = 'C_Commission';
$beanFiles['C_Commission'] = 'modules/C_Commission/C_Commission.php';
$report_include_modules['C_Commission'] = 'C_Commission';
$modInvisList[] = 'C_Commission';

$beanList['C_Contacts'] = 'C_Contacts';
$beanFiles['C_Contacts'] = 'modules/C_Contacts/C_Contacts.php';
$moduleList[] = 'C_Contacts';

$beanList['C_DeliveryRevenue'] = 'C_DeliveryRevenue';
$beanFiles['C_DeliveryRevenue'] = 'modules/C_DeliveryRevenue/C_DeliveryRevenue.php';
$report_include_modules['C_DeliveryRevenue'] = 'C_DeliveryRevenue';
$modInvisList[] = 'C_DeliveryRevenue';

$beanList['C_Rooms'] = 'C_Rooms';
$beanFiles['C_Rooms'] = 'modules/C_Rooms/C_Rooms.php';
$moduleList[] = 'C_Rooms';

$beanList['C_Teachers'] = 'C_Teachers';
$beanFiles['C_Teachers'] = 'modules/C_Teachers/C_Teachers.php';
$moduleList[] = 'C_Teachers';

$beanList['C_Timesheet'] = 'C_Timesheet';
$beanFiles['C_Timesheet'] = 'modules/C_Timesheet/C_Timesheet.php';
$moduleList[] = 'C_Timesheet';
$bwcModules[] = 'C_Timesheet';

?>