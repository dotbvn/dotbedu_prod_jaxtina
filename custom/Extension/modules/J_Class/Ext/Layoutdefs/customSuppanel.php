<?php
$layout_defs["J_Class"]["subpanel_setup"]["class_attendances"] = array (
    'order' => 30,
    'module' => 'Meetings',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_SUPPANEL_STUDENT_ATTENDANCES',
    'sort_order' => 'asc',
    'sort_by' => 'date_start',
    'get_subpanel_data' => 'ju_meetings',
    'top_buttons' =>
        array (
            0 =>
                array (
                    'widget_class' => 'SubPanelAttendances',
                ),

        ),
);

$layout_defs["J_Class"]["subpanel_setup"]["j_class_meetings"] = array (
    'order' => 30,
    'module' => 'Meetings',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_SUPPANEL_SESSION',
    'sort_order' => 'asc',
    'sort_by' => 'date_start',
    'get_subpanel_data' => 'ju_meetings',
    'top_buttons' =>
    array (
        0 =>
        array (
            'widget_class' => 'SubPanelClass',
        ),
    ),
);

$layout_defs["J_Class"]["subpanel_setup"]["j_class_meetings_syllabus"] = array (
    'order' => 50,
    'module' => 'Meetings',
    'subpanel_name' => 'J_Class_subpanel_j_class_meetings_syllabus',
    'title_key' => 'LBL_RECORD_OF_WORK',
    'sort_order' => 'asc',
    'sort_by' => 'date_start',
    'get_subpanel_data' => 'ju_meetings',
    'top_buttons' =>
    array (
        0 =>
        array (
            'widget_class' => 'SubPabelSyllabus',
        ),
    ),
);
$layout_defs["J_Class"]["subpanel_setup"]["j_class_studentsituations"] = array (
    'order' => 20,
    'module' => 'J_StudentSituations',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_SUPPANEL_STUDENT_SITUATION',
    'sort_order' => '',
    'sort_by' => '',
    'get_subpanel_data' => 'function:getSubSituation',
    'function_parameters' => array(
        'import_function_file' => 'custom/modules/J_Class/customSubPanel.php',
        'class_id' => $this->_focus->id,
        'return_as_array' => 'true'
    ),
    'top_buttons' =>
    array (
        0 =>
        array (
            'widget_class' => 'SubPanelDateSituation',
        ),

    ),
);

$layout_defs["J_Class"]["subpanel_setup"]["class_ac_valuation"] = array (
    'order' => 100,
    'module' => 'Meetings',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_SUPPANEL_AC_VALUATION',
    'sort_order' => 'asc',
    'sort_by' => 'date_start',
    'get_subpanel_data' => 'ju_meetings',
    'top_buttons' =>
        array (

        ),
);
?>
