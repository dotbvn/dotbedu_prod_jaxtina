<?php
 // created: 2020-09-29 10:36:07
$layout_defs["J_Class"]["subpanel_setup"]['j_teacherqe_j_class'] = array (
  'order' => 100,
  'module' => 'J_TeacherQE',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_TEACHERQE_TITLE',
  'get_subpanel_data' => 'j_teacherqe_j_class',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
//    1 =>
//    array (
//      'widget_class' => 'SubPanelTopSelectButton',
//      'mode' => 'MultiSelect',
//    ),
  ),
);
