<?php
// created: 2019-07-06 00:05:50
$dictionary["J_Class"]["fields"]["j_class_tasks_1"] = array (
  'name' => 'j_class_tasks_1',
  'type' => 'link',
  'relationship' => 'j_class_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'vname' => 'LBL_J_CLASS_TASKS_1_FROM_J_CLASS_TITLE',
  'id_name' => 'j_class_tasks_1j_class_ida',
  'link-type' => 'many',
  'side' => 'left',
);
