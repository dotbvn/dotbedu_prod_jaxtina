<?php
$dictionary['J_Class']['fields']['lessonplan_name'] = array(
    'name' => 'lessonplan_name',
    'vname' => 'LBL_LESSONPLAN_NAME',
    'type' => 'relate',
    'dbType' => 'varchar',
    'rname' => 'name',
    'id_name' => 'lessonplan_id',
    'join_name' => 'j_lessonplan',
    'link' => 'lessonplan_link',
    'table' => 'j_lessonplan',
    'isnull' => 'true',
    'module' => 'J_LessonPlan',
    'source' => 'non-db',
);
$dictionary['J_Class']['fields']['lessonplan_id'] = array(
    'name' => 'lessonplan_id',
    'rname' => 'id',
    'vname' => 'LBL_LESSONPLAN_ID',
    'type' => 'id',
    'table' => 'j_lessonplan',
    'isnull' => 'true',
    'module' => 'J_LessonPlan',
    'dbType' => 'id',
    'audited' => true,
);
$dictionary['J_Class']['fields']['lessonplan_link'] = array(
    'name' => 'lessonplan_link',
    'type' => 'link',
    'relationship' => 'j_lessonplan_j_class',
    'module' => 'J_LessonPlan',
    'bean_name' => 'J_LessonPlan',
    'source' => 'non-db',
    'vname' => 'LBL_LESSONPLAN_NAME',
);
$dictionary['J_Class']['fields']['schedule_text'] = array(
    'name' => 'schedule_text',
    'type' => 'varchar',
    'len' => 100,
    'source' => 'non-db',
);