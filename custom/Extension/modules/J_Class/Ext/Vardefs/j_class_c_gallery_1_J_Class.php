<?php
// created: 2019-08-31 09:36:22
$dictionary["J_Class"]["fields"]["j_class_c_gallery_1"] = array (
  'name' => 'j_class_c_gallery_1',
  'type' => 'link',
  'relationship' => 'j_class_c_gallery_1',
  'source' => 'non-db',
  'module' => 'C_Gallery',
  'bean_name' => 'C_Gallery',
  'vname' => 'LBL_J_CLASS_C_GALLERY_1_FROM_J_CLASS_TITLE',
  'id_name' => 'j_class_c_gallery_1j_class_ida',
  'link-type' => 'many',
  'side' => 'left',
);
