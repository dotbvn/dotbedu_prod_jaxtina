<?php
// created: 2019-03-20 17:12:55
$dictionary["J_Class"]["fields"]["j_class_cases_1"] = array (
  'name' => 'j_class_cases_1',
  'type' => 'link',
  'relationship' => 'j_class_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_J_CLASS_CASES_1_FROM_J_CLASS_TITLE',
  'id_name' => 'j_class_cases_1j_class_ida',
  'link-type' => 'many',
  'side' => 'left',
);
