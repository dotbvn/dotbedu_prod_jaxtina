<?php
// created: 2020-09-29 10:36:07
$dictionary["J_Class"]["fields"]["j_teacherqe_j_class"] = array (
  'name' => 'j_teacherqe_j_class',
  'type' => 'link',
  'relationship' => 'j_teacherqe_j_class',
  'source' => 'non-db',
  'module' => 'J_TeacherQE',
  'bean_name' => false,
  'vname' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_CLASS_TITLE',
  'id_name' => 'j_teacherqe_j_classj_class_ida',
  'link-type' => 'many',
  'side' => 'left',
);
