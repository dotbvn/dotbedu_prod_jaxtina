<?php
// created: 2019-09-14 10:42:45
$dictionary["J_Class"]["fields"]["j_class_j_loyalty_1"] = array (
  'name' => 'j_class_j_loyalty_1',
  'type' => 'link',
  'relationship' => 'j_class_j_loyalty_1',
  'source' => 'non-db',
  'module' => 'J_Loyalty',
  'bean_name' => 'J_Loyalty',
  'vname' => 'LBL_J_CLASS_J_LOYALTY_1_FROM_J_CLASS_TITLE',
  'id_name' => 'j_class_j_loyalty_1j_class_ida',
  'link-type' => 'many',
  'side' => 'left',
);
