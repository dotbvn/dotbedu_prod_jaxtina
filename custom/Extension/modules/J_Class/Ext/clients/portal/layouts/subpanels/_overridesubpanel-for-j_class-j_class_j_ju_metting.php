<?php
$viewdefs['J_Class']['portal']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_RECORD_OF_WORK',
    'context' =>
        array (
            'link' => 'ju_meetings',
        ),
    'override_subpanel_list_view' => 'subpanel-for-j_class-j_class_meetings',
);
