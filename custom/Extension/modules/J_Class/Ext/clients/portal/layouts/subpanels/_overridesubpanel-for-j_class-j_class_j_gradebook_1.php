<?php
$viewdefs['J_Class']['portal']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_CLASS_TITLE',
    'context' =>
        array (
            'link' => 'j_class_j_gradebook_1',
        ),
    'override_subpanel_list_view' => array (
        'link' => 'j_class_j_gradebook_1',
        'view' => 'subpanel-for-j_class-j_class_j_gradebook_1',
    ),
);
