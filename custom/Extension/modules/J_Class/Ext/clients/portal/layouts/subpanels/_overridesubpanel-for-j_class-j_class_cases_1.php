<?php
//auto-generated file DO NOT EDIT
$viewdefs['J_Class']['portal']['layout']['subpanels']['components'][] = array(
        'override_paneltop_view' => 'panel-top-for-j_class_cases_1',
        'layout' => 'subpanel',
        'label' => 'LBL_J_CLASS_CASES_1_FROM_J_CLASS_TITLE',
        'context' => array(
                'link' => 'j_class_cases_1',
            ),
        'override_subpanel_list_view' => array (
            'link' => 'j_class_cases_1',
            'view' => 'subpanel-for-j_class-j_class_cases_1',
        ),
    );
