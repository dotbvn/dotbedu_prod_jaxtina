<?php
$mod_strings['LBL_HOMEWORK_SCORE'] = 'Active Learning';
$mod_strings['LBL_CARE_COMMENT_1'] = 'Class Note';
$mod_strings['LBL_FLIPPED'] = 'Flipped';
$mod_strings['LBL_HOMEWORK_NUM'] = 'Homework';
$mod_strings['LBL_TEACHER_COMMENT_NUM'] = 'Tương tác trên lớp';
$mod_strings['LBL_WARNING_JAX'] = 'Giá trị tối đa cho phép là ';

$mod_strings['LBL_WARNING_MISSING'] = 'Các trường Flipped, Homework, Tương tác trên lớp là bắt buộc';