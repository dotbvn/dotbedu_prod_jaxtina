<?php
$mod_strings['LBL_HOMEWORK_SCORE'] = 'Active Learning';
$mod_strings['LBL_CARE_COMMENT_1'] = 'Class Note';
$mod_strings['LBL_FLIPPED'] = 'Flipped';
$mod_strings['LBL_HOMEWORK_NUM'] = 'Homework';
$mod_strings['LBL_TEACHER_COMMENT_NUM'] = 'Student\'s Interaction';
$mod_strings['LBL_WARNING_JAX'] = 'Maximum value is ';

$mod_strings['LBL_WARNING_MISSING'] = 'Flipped, Homework, Student\'s Interaction are required';
$mod_strings['LBL_NOTE_FOR_TEA'] = 'GV ghi nội dung BTVN buổi học (bao gồm flipped và HW)';
$mod_strings['LBL_SHOW_HELP_JAX'] = '<p> <strong>Lưu ý:</strong> Points bằng tổng điểm 3 phần: Giao tiếp và lắng nghe (max 5 điểm), Tham gia hoạt động nhóm (max 5 điểm), Tập trung lớp học (max 5 điểm)</p> <br>' .
    '<table style=&quot;width: 100%; border-collapse: collapse &quot;;>' .
    '<thead>' .
    '<tr>' .
    '<th style=&quot;border: solid 1pt black; padding: 3pt&quot;;> </th>' .
    '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Excellent <br>5</th>' .
    '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Very Good <br>4</th>' .
    '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Good <br>3</th>' .
    '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Fair <br>2</th>' .
    '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Need Improvement <br>1</th>' .
    '</tr> ' .
    '</thead> ' .
    '<tbody> ' .
    '<tr>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Giao tiếp và lắng nghe</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Luôn luôn trả lời câu hỏi và chủ động đặt câu hỏi</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thường xuyên trả lời câu hỏi và chủ động đặt câu hỏi</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Trả lời câu hỏi nhưng hiếm khi chủ động đặt câu hỏi</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thỉnh thoảng trả lời câu hỏi và hiếm khi chủ động đặt câu hỏi</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Chỉ trả lời câu hỏi khi thực sự cần thiết/ bắt buộc</td>' .
    '</tr>' .
    '<tr>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Tham gia hoạt động nhóm</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Luôn luôn chủ động tham gia đầy đủ các hoạt động  nhóm</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thường xuyên chủ động tham gia đầy đủ các hoạt động  nhóm</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Tham gia hoạt động  nhóm nhưng chưa chủ động</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thỉnh thoảng tham gia hoạt động nhóm</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Chỉ tham gia khi thực sự cần thiết/ bắt buộc</td>' .
    '</tr>' .
    '<tr>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Tập trung trong lớp học</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Luôn luôn tập trung vào giáo viên và chủ động thực hiện nhiệm vụ / bài tập.</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thường xuyên tập trung vào giáo viên và chủ động thực hiện nhiệm vụ / bài tập. </td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Đôi khi mất tập trung nhưng vẫn thực hiện đủ nhiệm vụ / bài tập.</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thỉnh thoảng tập trung và cần được nhắc nhở thực hiện nhiệm vụ/ bài tập</td>' .
    '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Không tập trung và chỉ thực hiện nhiệm vụ / bài tập khi được nhắc nhở thường xuyên</td>' .
    '</tr>' .
    '</tbody>' .
    '</table>';