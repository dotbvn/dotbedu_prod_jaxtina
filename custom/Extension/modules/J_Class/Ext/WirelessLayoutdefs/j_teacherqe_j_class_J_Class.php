<?php
 // created: 2020-09-29 10:36:07
$layout_defs["J_Class"]["subpanel_setup"]['j_teacherqe_j_class'] = array (
  'order' => 100,
  'module' => 'J_TeacherQE',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_TEACHERQE_TITLE',
  'get_subpanel_data' => 'j_teacherqe_j_class',
);
