<?php
$mod_strings['LBL_CONFIG_TITLE'] = 'Case Chat Settings';
$mod_strings['LBL_EXTERNAL_DEFAULT_SUBJECT'] = 'Standard subject for chat with external users in portal';
$mod_strings['LBL_INTERNAL_DEFAULT_SUBJECT'] = 'Standard subject for internal chat';
$mod_strings['LBL_LICENSE_KEY'] = 'License Key';
$mod_strings['LBL_VALIDATION_KEY'] = 'Validation Key';
$mod_strings['LBL_LICENSE_TYPE'] = 'License Type';