<?php
$dictionary['J_Membership']['fields']['birthdate'] = array(
    'name' => 'birthdate',
    'vname' => 'LBL_BIRTHDATE',
    'massupdate' => false,
    'type' => 'date',
);

$dictionary['J_Membership']['fields']['first_name']['required'] = true;

//Add Relationship Membership - Contacts (1 - n) (guardian1)
$dictionary['J_Membership']['fields']['guardian1_link'] = array(
    'name' => 'guardian1_link',
    'type' => 'link',
    'relationship' => 'contacts_guardian1',
    'module' => 'Contacts',
    'bean_name' => 'Contacts',
    'source' => 'non-db',
    'vname' => 'LBL_GUARDIAN',
);

$dictionary['J_Membership']['relationships']['contacts_guardian1'] = array(
    'lhs_module' => 'J_Membership',
    'lhs_table' => 'j_membership',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'guardian1',
    'relationship_type' => 'one-to-many'
);
//Add Relationship Membership - Contacts (1 - n) (guardian2)
$dictionary['J_Membership']['fields']['guardian2_link'] = array(
    'name' => 'guardian2_link',
    'type' => 'link',
    'relationship' => 'contacts_guardian2',
    'module' => 'Contacts',
    'bean_name' => 'Contacts',
    'source' => 'non-db',
    'vname' => 'LBL_GUARDIAN',
);

$dictionary['J_Membership']['relationships']['contacts_guardian2'] = array(
    'lhs_module' => 'J_Membership',
    'lhs_table' => 'j_membership',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'guardian2',
    'relationship_type' => 'one-to-many'
);
//Add Relationship Membership - Contacts (1 - n) (guardian3)
$dictionary['J_Membership']['fields']['guardian3_link'] = array(
    'name' => 'guardian3_link',
    'type' => 'link',
    'relationship' => 'contacts_guardian3',
    'module' => 'Contacts',
    'bean_name' => 'Contacts',
    'source' => 'non-db',
    'vname' => 'LBL_GUARDIAN',
);

$dictionary['J_Membership']['relationships']['contacts_guardian3'] = array(
    'lhs_module' => 'J_Membership',
    'lhs_table' => 'j_membership',
    'lhs_key' => 'id',
    'rhs_module' => 'Contacts',
    'rhs_table' => 'contacts',
    'rhs_key' => 'guardian3',
    'relationship_type' => 'one-to-many'
);
