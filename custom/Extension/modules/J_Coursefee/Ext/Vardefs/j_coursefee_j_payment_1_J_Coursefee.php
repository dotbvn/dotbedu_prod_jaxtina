<?php
// created: 2019-03-20 17:24:00
$dictionary["J_Coursefee"]["fields"]["j_coursefee_j_payment_1"] = array (
  'name' => 'j_coursefee_j_payment_1',
  'type' => 'link',
  'relationship' => 'j_coursefee_j_payment_1',
  'source' => 'non-db',
  'module' => 'J_Payment',
  'bean_name' => 'J_Payment',
  'vname' => 'LBL_J_COURSEFEE_J_PAYMENT_1_FROM_J_COURSEFEE_TITLE',
  'id_name' => 'j_coursefee_j_payment_1j_coursefee_ida',
  'link-type' => 'many',
  'side' => 'left',
);
