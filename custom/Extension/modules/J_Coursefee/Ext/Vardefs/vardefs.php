<?php
$dictionary['J_Coursefee']['fields']['description']['required'] = false;
$dictionary['J_Coursefee']['fields']['ext_content_1'] = array (
    'name' => 'ext_content_1',
    'vname' => 'LBL_EXT_CONTENT_1',
    'type' => 'varchar',
    'len'=>255
);
$dictionary['J_Coursefee']['fields']['ext_content_2'] = array (
    'name' => 'ext_content_2',
    'vname' => 'LBL_EXT_CONTENT_2',
    'type' => 'varchar',
    'len'=>255
);
$dictionary['J_Coursefee']['fields']['tag']['massupdate'] = false;
$dictionary['J_Coursefee']['fields']['is_external_accumulate'] = array (
    'name' => 'is_external_accumulate',
    'vname' => 'LBL_IS_EXTERNAL_ACCUMULATE',
    'type' => 'bool',
    'defauft' => false,
);

