<?php
$dictionary['J_Coursefee']['fields']['bonus_course'] = array (
    'required' => false,
    'name' => 'bonus_course',
    'vname' => 'LBL_BONUS_COURSE',
    'type' => 'multienum',
    'isMultiSelect' => true,
    'massupdate' => 1,
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'len' => 255,
    'size' => '20',
    'options' => 'bonus_course_list',
    'studio' => 'visible',
    'dependency' => false,
    'dbType' => 'varchar',
);
?>
