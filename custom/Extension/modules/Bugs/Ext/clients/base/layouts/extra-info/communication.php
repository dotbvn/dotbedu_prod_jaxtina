<?php

$panel = array(
    'layout' => 'communication',
    'label' => 'LBL_COMMUNICATION',
);

if (isset($viewdefs['Bugs']['base']['layout']['extra-info'])) {
    $viewdefs['Bugs']['base']['layout']['extra-info']['components'][] = $panel;
} else {
    $viewdefs['Bugs']['base']['layout']['extra-info'] = array(
        'components' => array($panel),
        'type' => 'simple',
        'span' => 12,
    );
}
