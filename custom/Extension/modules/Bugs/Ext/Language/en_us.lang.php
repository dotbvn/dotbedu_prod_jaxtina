<?php
// WARNING: The contents of this file are auto-generated.
$mod_strings['LNK_NEW_BUG'] = 'Report EMS Support';
$mod_strings['LNK_CREATE'] = 'Report EMS Support';
$mod_strings['LBL_MODULE_NAME'] = 'EMS Support';
$mod_strings['LBL_MODULE_NAME_SINGULAR'] = 'EMS Support';
$mod_strings['LBL_NEW_FORM_TITLE'] = 'New EMS Support';
$mod_strings['LNK_BUG_LIST'] = 'View EMS Support';
$mod_strings['LNK_BUG_REPORTS'] = 'View EMS Support Reports';
$mod_strings['LNK_IMPORT_BUGS'] = 'Import EMS Support';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'EMS Support List';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'EMS Support Search';
$mod_strings['LBL_LIST_MY_BUGS'] = 'My Assigned EMS Support';
