<?php
// AUTO-COMPLETED - Add by phgiahannn
$dictionary['DRI_Workflow_Task_Template']['fields']['is_adhoc'] = array(
    'name' => 'is_adhoc',
    'vname' => 'LBL_IS_ADHOC',
    'type' => 'bool',
    'default' => '0',
    'dependency' => 'equal($is_parent, "0")',
);
$dictionary['DRI_Workflow_Task_Template']['fields']['completed_by_module'] = array(
    'name' => 'completed_by_module',
    'vname' => 'LBL_COMPLETED_BY_MODULE',
    'reportable' => true,
    'options' => 'cj_completed_by_module_options',
    'type' => 'enum',
    'dbType' => 'varchar',
    'default' => '',
);
$dictionary['DRI_Workflow_Task_Template']['fields']['completed_by_key'] = array(
    'name' => 'completed_by_key',
    'vname' => 'LBL_COMPLETED_BY_KEY',
    'reportable' => true,
    'options' => 'cj_completed_by_key_options',
    'type' => 'multienum',
    'isMultiSelect' => true,
    'dbType' => 'varchar',
    'default' => '',
);
$dictionary['DRI_Workflow_Task_Template']['fields']['remind_email'] = array(
    'name' => 'remind_email',
    'vname' => 'LBL_REMINDER_EMAIL',
    'type' => 'enum',
    'options' => 'reminder_time_options'
);
$dictionary['DRI_Workflow_Task_Template']['fields']['remind_popup'] = array(
    'name' => 'remind_popup',
    'vname' => 'LBL_REMINDER_POPUP',
    'type' => 'enum',
    'options' => 'reminder_time_options',
    'default' => 900,
);