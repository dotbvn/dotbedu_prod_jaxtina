<?php
$dependencies['DRI_Workflow_Task_Templates']['set_task_created_condition_value_options'] = array(
    'hooks' => array("edit","save"),
    'trigger' => 'true',
    'triggerFields' => array('task_created_condition_field'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'task_created_condition_value',
                'keys' => 'getDropdownKeySet(getDropdownValue("task_created_condition_field_value_mapping", $task_created_condition_field))',
                'labels' => 'getDropdownValueSet(getDropdownValue("task_created_condition_field_value_mapping", $task_created_condition_field))'
            ),
        ),
    ),
);
$dependencies['DRI_Workflow_Task_Templates']['set_completed_key_options'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('completed_by_module'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'completed_by_key',
                'keys' => 'getDropdownKeySet(getDropdownValue("cj_completed_module_key_options", $completed_by_module))',
                'labels' => 'getDropdownValueSet(getDropdownValue("cj_completed_module_key_options", $completed_by_module))'
            ),
        ),
    ),
);