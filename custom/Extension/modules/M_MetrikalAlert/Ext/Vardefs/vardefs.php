<?php
$dictionary['M_MetrikalAlert']['fields']['target'] = array(
    'name' => 'target',
    'vname' => 'LBL_TARGET',
    'type' => 'decimal',
    'len' => '20',
    'precision' => '2',
)
;$dictionary['M_MetrikalAlert']['fields']['report_id'] = array(
    'name' => 'report_id',
    'vname' => 'LBL_REPORT_ID',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
);
;$dictionary['M_MetrikalAlert']['fields']['report_name'] = array(
    'name' => 'report_name',
    'vname' => 'LBL_REPORT_NAME',
    'type' => 'varchar',
    'len' => '100',
    'required' => false,
    'reportable' => false,
);