<?php
//DOTB METRIKCAL APPS
$dictionary['SavedReport']['fields']['metric_link'] = array(
    'name' => 'metric_link',
    'type' => 'link',
    'relationship' => 'metrics_report_rel',
    'module' => 'J_Metric',
    'bean_name' => 'J_Metric',
    'source' => 'non-db',
    'vname' => 'DOTB Metrikal',
);
$dictionary['SavedReport']['relationships']['metrics_report_rel'] = array(
    'lhs_module' => 'Reports',
    'lhs_table' => 'saved_reports',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Metric',
    'rhs_table' => 'j_metric',
    'rhs_key' => 'report_id',
    'relationship_type' => 'one-to-many'
);
//Add by HoangHvy to manage Efficency
$dictionary['SavedReport']['fields']['stats_type'] = array(
    'name' => 'stats_type',
    'type' => 'varchar',
    'len' => '20'
);
$dictionary['SavedReport']['fields']['relate_effi_id'] = array(
    'name' => 'relate_effi_id',
    'type' => 'id',
    'massupdate' => false,
    'reportable' => false,
    'required' => false
);