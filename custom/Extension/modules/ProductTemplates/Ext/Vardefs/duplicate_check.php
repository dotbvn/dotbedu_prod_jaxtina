<?php

$dictionary['ProductTemplate']['duplicate_check'] = array(
    'enabled' => true,
    'FilterDuplicateCheck' => array(
        'filter_template' => array(
            array(
                'code' => array('$equals' => '$code'),
            ),
        ),
        'ranking_fields' => array(
            array('in_field_name' => 'code', 'dupe_field_name' => 'code'),
        )
    ),
);