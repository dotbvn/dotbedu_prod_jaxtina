<?php
$dictionary['ProductTemplate']['fields']['code'] = array(
    'name' => 'code',
    'vname' => 'LBL_CODE',
    'dbType' => 'varchar',
    'type' => 'varchar',
    'len' => '50',
    'comment' => 'code of the product',
    'importable' => 'required',
    'required' => true,
    'studio' => 'visible',
);
$dictionary['ProductTemplate']['fields']['status2'] = array(
    'name' => 'status2',
    'vname' => 'LBL_STATUS_2',
    'type' => 'enum',
    'options' => 'status_ProductTemplates_list',
    'len' => 100,
);
$dictionary['ProductTemplate']['fields']['unit'] = array(
    'name' => 'unit',
    'vname' => 'LBL_UNIT',
    'type' => 'enum',
    'options' => 'unit_ProductTemplates_list',
    'len' => 100,
    'studio' => 'visible',
);
$dictionary['ProductTemplate']['fields']['inventorydetail_link'] =array(
    'name' => 'inventorydetail_link',
    'type' => 'link',
    'relationship' => 'book_inventorydetails',
    'module' => 'J_Inventorydetail',
    'bean_name' => 'J_Inventorydetail',
    'source' => 'non-db',
    'vname' => 'LBL_INVENTORYDETAIL',
);
$dictionary['ProductTemplate']['relationships']['book_inventorydetails'] = array(
    'lhs_module'        => 'ProductTemplates',
    'lhs_table'            => 'product_templates',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_Inventorydetail',
    'rhs_table'            => 'j_inventorydetail',
    'rhs_key'            => 'book_id',
    'relationship_type'    => 'one-to-many',
);

//Add Relationship Tax Rate - Book/Gift
$dictionary['ProductTemplate']['fields']['taxrate_id'] = array(
    'name' => 'taxrate_id',
    'vname' => 'LBL_TAXRATE_ID',
    'type' => 'id',
    'required'=>false,
    'reportable'=>false,
    'comment' => ''
);
$dictionary['ProductTemplate']['fields']['taxrate_name'] = array(
    'name' => 'taxrate_name',
    'rname' => 'name',
    'id_name' => 'taxrate_id',
    'vname' => 'LBL_TAXRATE_NAME',
    'type' => 'relate',
    'link' => 'taxrate_link',
    'table' => 'taxrates',
    'isnull' => 'true',
    'module' => 'TaxRates',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable'=>true,
    'source' => 'non-db',
);
$dictionary['ProductTemplate']['fields']['taxrate_link'] = array(
    'name' => 'taxrate_link',
    'type' => 'link',
    'relationship' => 'taxrate_producttemplates',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_TAXRATE_NAME',
);
$dictionary['ProductTemplate']['fields']['name']['exportable'] = true;
$dictionary['ProductTemplate']['fields']['code']['exportable'] = true;
$dictionary['ProductTemplate']['fields']['list_price']['exportable'] = true;
$dictionary['ProductTemplate']['fields']['unit']['exportable'] = true;
$dictionary['ProductTemplate']['fields']['category_name']['exportable'] = true;
$dictionary['ProductTemplate']['fields']['status2']['exportable'] = true;
$dictionary['ProductTemplate']['fields']['date_available']['exportable'] = true;
$dictionary['ProductTemplate']['fields']['description']['exportable'] = true;

$dictionary['ProductTemplate']['fields']['discount_price']['calculated'] = true;
$dictionary['ProductTemplate']['fields']['discount_price']['enforced'] = false;
$dictionary['ProductTemplate']['fields']['discount_price']['formula'] = 'ifElse(isNumeric($list_price), $list_price, 0)';
