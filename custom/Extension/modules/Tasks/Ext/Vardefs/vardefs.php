<?php
$dictionary['Task']['fields']['priority']['default']='Medium';
$dictionary['Task']['fields']['status']['duplicate_on_record_copy']  ='no';
$dictionary['Task']['fields']['date_start']['display_default']='now';
$dictionary["Task"]["fields"]["date_due"]["calculated"] = true;
$dictionary["Task"]["fields"]["date_due"]["formula"] = 'addDays($date_start,1)';

$dictionary['Task']['fields']['task_id'] = array(
    'name' => 'task_id',
    'type' => 'varchar',
    'label' => 'LBL_TASK_ID',
    'len' => 100,
    'readonly' => true,
    'duplicate_on_record_copy' => 'no',
);