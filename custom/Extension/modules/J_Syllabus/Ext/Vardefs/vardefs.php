<?php
$dictionary['J_Syllabus']['fields']['j_syllabus_meetings'] = array(
    'name' => 'j_syllabus_meetings',
    'vname' => 'LBL_J_SYLLABUS_MEETINGS',
    'type' => 'link',
    'source' => 'non-db',
    'relationship' => 'syllabus_sessions',
    'module' => 'Meetings',
    'bean_name' => 'Meetings',
);
$dictionary['J_Syllabus']['relationships']['syllabus_sessions'] = array(
    'lhs_module' => 'J_Syllabus',
    'lhs_table' => 'j_syllabus',
    'lhs_key' => 'id',
    'rhs_module' => 'Meetings',
    'rhs_table' => 'meetings',
    'rhs_key' => 'syllabus_id',
    'relationship_type' => 'one-to-many'
);