<?php
 // created: 2022-06-14 10:18:07

$dictionary['J_Payment']['fields']['assigned_user_name']['audited']=true;
$dictionary['J_Payment']['fields']['user_closed_sale']['audited']=true;
$dictionary['J_Payment']['fields']['user_pt_demo']['audited']=true;
$dictionary['J_Payment']['fields']['parent_name']['audited']=true;

$dictionary['J_Payment']['fields']['description']['audited']=true;
$dictionary['J_Payment']['fields']['amount_bef_discount']['audited']=true;
$dictionary['J_Payment']['fields']['discount_amount']['audited']=true;
$dictionary['J_Payment']['fields']['discount_percent']['audited']=true;
$dictionary['J_Payment']['fields']['total_after_discount']['audited']=true;
$dictionary['J_Payment']['fields']['paid_amount']['audited']=true;
$dictionary['J_Payment']['fields']['paid_hours']['audited']=true;
$dictionary['J_Payment']['fields']['total_hours']['audited']=true;
$dictionary['J_Payment']['fields']['payment_amount']['audited']=true;
$dictionary['J_Payment']['fields']['is_auto_enroll']['audited']=true;
$dictionary['J_Payment']['fields']['number_of_payment']['audited']=true;
$dictionary['J_Payment']['fields']['payment_date']['audited']=true;

 ?>