<?php
// created: 2019-07-03 12:11:05
$dictionary["J_Payment"]["fields"]["j_payment_j_inventory_1"] = array (
  'name' => 'j_payment_j_inventory_1',
  'type' => 'link',
  'relationship' => 'j_payment_j_inventory_1',
  'source' => 'non-db',
  'module' => 'J_Inventory',
  'bean_name' => 'J_Inventory',
  'vname' => 'LBL_J_PAYMENT_J_INVENTORY_1_FROM_J_PAYMENT_TITLE',
  'id_name' => 'j_payment_j_inventory_1j_payment_ida',
  'link-type' => 'many',
  'side' => 'left',
);
