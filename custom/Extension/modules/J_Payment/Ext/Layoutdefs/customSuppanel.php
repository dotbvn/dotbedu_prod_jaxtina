<?php
// created: 2019-06-23 12:15:49
$layout_defs["J_Payment"]["subpanel_setup"]['j_payment_j_discount_1'] = array (
    'order' => 10,
    'module' => 'J_Discount',
    'subpanel_name' => 'default',
    'sort_order' => 'asc',
    'sort_by' => 'name',
    'title_key' => 'LBL_J_PAYMENT_J_DISCOUNT_1_FROM_J_DISCOUNT_TITLE',
    'get_subpanel_data' => 'j_payment_j_discount_1',
    'top_buttons' =>
    array (),
);
$layout_defs["J_Payment"]["subpanel_setup"]["j_payment_j_sponsor"] = array (
    'order' => 20,
    'module' => 'J_Sponsor',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_SPONSOR',
    'sort_order' => 'asc',
    'sort_by' => 'name',
    'get_subpanel_data' => 'ju_sponsor',
    'top_buttons' =>
    array (),
);
$layout_defs["J_Payment"]["subpanel_setup"]["payment_loyaltys"] = array (
    'order' => 30,
    'module' => 'J_Loyalty',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_LOYALTY',
    'sort_order' => 'asc',
    'sort_by' => 'input_date',
    'get_subpanel_data' => 'loyalty_link',
    'top_buttons' =>
    array (),
);

//Payment book/gift
$layout_defs["J_Payment"]["subpanel_setup"]['j_payment_j_payment_2'] = array (
    'order' => 31,
    'module' => 'J_Payment',
    'subpanel_name' => 'default',
    'sort_order' => 'desc',
    'sort_by' => 'payment_date',
    'title_key' => 'LBL_PAYMENT_RELATED_BOOK',
    'get_subpanel_data' => 'j_payment_j_payment_2',
    'top_buttons' =>
    array (
        0 =>
        array (
            'widget_class' => 'SubPanelTopCreateBookGift',
        ),
    ),
);

$layout_defs["J_Payment"]["subpanel_setup"]['j_payment_j_payment_1'] = array (
    'order' => 40,
    'module' => 'J_Payment',
    'subpanel_name' => 'default',
    'sort_order' => 'desc',
    'sort_by' => 'payment_date',
    'title_key' => 'LBL_J_PAYMENT_J_PAYMENT_1_FROM_J_PAYMENT_R_TITLE',
    'get_subpanel_data' => 'j_payment_j_payment_1',
    'top_buttons' =>
    array (),
);

$layout_defs["J_Payment"]["subpanel_setup"]["j_payment_j_payment_1_right"] = array (
    'order' => 41,
    'module' => 'J_Payment',
    'subpanel_name' => 'default',
    'sort_order' => 'desc',
    'sort_by' => 'payment_date',
    'title_key' => 'LBL_J_PAYMENT_J_PAYMENT_1_FROM_J_PAYMENT_L_TITLE',
    'get_subpanel_data' => 'j_payment_j_payment_1_right',
    'top_buttons' =>
    array (),
);

$layout_defs["J_Payment"]["subpanel_setup"]["j_payment_studentsituations"] = array (
    'order' => 45,
    'module' => 'J_StudentSituations',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_STUDENT_SITUATION_SUPPANEL',
    'sort_order' => 'asc',
    'sort_by' => 'type',
    'get_subpanel_data' => 'ju_studentsituations',
    'top_buttons' =>
    array (
    ),
);


$layout_defs["J_Payment"]["subpanel_setup"]['j_invoice_j_payment_1'] = array (
    'order' => 50,
    'module' => 'J_Invoice',
    'subpanel_name' => 'default',
    'sort_order' => 'desc',
    'sort_by' => 'invoice_date',
    'title_key' => 'LBL_INVOICE',
    'get_subpanel_data' => 'j_invoice_j_payment_1',
    'top_buttons' =>
    array (
        0 =>
        array (
            'widget_class' => 'SubPanelTopButtonQuickCreate',
            'title'=>'LBL_MANUAL_INVOICE',
            'form_value'=>'LBL_MANUAL_INVOICE',
        ),
    ),
);

$layout_defs["J_Payment"]["subpanel_setup"]["payment_paymentdetails"] = array (
    'order' => 70,
    'module' => 'J_PaymentDetail',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_PAYMENT_DETAIL',
    'sort_order' => 'asc',
    'sort_by' => 'payment_no',
    'get_subpanel_data' => 'paymentdetail_link',
    'top_buttons' =>
    array (
        0 =>
        array (
            'widget_class' => 'SubPanelAddEVat',
        ),
    ),
);


//$layout_defs["J_Payment"]["subpanel_setup"]['j_coursefee_j_payment_2'] = array (
//  'order' => 100,
//  'module' => 'J_Coursefee',
//  'subpanel_name' => 'default',
//  'sort_order' => 'asc',
//  'sort_by' => 'id',
//  'title_key' => 'LBL_J_COURSEFEE_J_PAYMENT_2_FROM_J_COURSEFEE_TITLE',
//  'get_subpanel_data' => 'j_coursefee_j_payment_2',
//  'top_buttons' =>
//  array (
//    0 =>
//    array (
//      'widget_class' => 'SubPanelTopButtonQuickCreate',
//    ),
//    1 =>
//    array (
//      'widget_class' => 'SubPanelTopSelectButton',
//      'mode' => 'MultiSelect',
//    ),
//  ),
//);
// // created: 2019-07-03 12:11:05
//$layout_defs["J_Payment"]["subpanel_setup"]['j_payment_j_inventory_1'] = array (
//  'order' => 100,
//  'module' => 'J_Inventory',
//  'subpanel_name' => 'default',
//  'sort_order' => 'asc',
//  'sort_by' => 'id',
//  'title_key' => 'LBL_J_PAYMENT_J_INVENTORY_1_FROM_J_INVENTORY_TITLE',
//  'get_subpanel_data' => 'j_payment_j_inventory_1',
//  'top_buttons' =>
//  array (
//    0 =>
//    array (
//      'widget_class' => 'SubPanelTopButtonQuickCreate',
//    ),
//    1 =>
//    array (
//      'widget_class' => 'SubPanelTopSelectButton',
//      'mode' => 'MultiSelect',
//    ),
//  ),
//);
?>