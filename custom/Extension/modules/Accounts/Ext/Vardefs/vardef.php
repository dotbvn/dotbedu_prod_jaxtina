<?php
$dictionary['Account']['fields']['billing_address_street']['required'] = true;
$dictionary['Account']['fields']['phone_office']['required'] = true;
$dictionary['Account']['fields']['tax_code']['required'] = true;

//Add Relationship Accounts - J_Invoice
$dictionary['Account']['fields']['invoice_link'] = array(
        'name' => 'paymentdetail_link',
        'type' => 'link',
        'relationship' => 'accounts_j_invoice',
        'module' => 'J_Invoice',
        'bean_name' => 'J_Invoice',
        'source' => 'non-db',
        'vname' => 'LBL_J_INVOICE',
);

$dictionary['Account']['relationships']['accounts_j_invoice'] = array(
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Invoice',
    'rhs_table' => 'j_invoice',
    'rhs_key' => 'account_id',
    'relationship_type' => 'one-to-many'
);
