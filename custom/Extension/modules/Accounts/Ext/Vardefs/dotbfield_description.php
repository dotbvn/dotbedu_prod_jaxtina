<?php
 // created: 2019-04-18 13:50:47
$dictionary['Account']['fields']['description']['audited']=false;
$dictionary['Account']['fields']['description']['massupdate']=false;
$dictionary['Account']['fields']['description']['comments']='Full text of the note';
$dictionary['Account']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Account']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Account']['fields']['description']['merge_filter']='disabled';
$dictionary['Account']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.72',
  'searchable' => true,
);
$dictionary['Account']['fields']['description']['calculated']=false;
$dictionary['Account']['fields']['description']['rows']='3';
$dictionary['Account']['fields']['description']['cols']='40';

 ?>