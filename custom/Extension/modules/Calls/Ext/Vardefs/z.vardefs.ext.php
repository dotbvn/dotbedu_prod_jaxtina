<?php
$dictionary['Call']['fields']['api_sync'] = array(
    'name' => 'api_sync',
    'vname' => 'LBL_API_SYNC',
    'type' => 'varchar',
    'len' => '255',
);

$dictionary['Call']['fields']['voip_status'] = array(
    'name' => 'voip_status',
    'vname' => 'LBL_VOIP_STATUS',
    'type' => 'varchar',
    'len' => '100',
);
