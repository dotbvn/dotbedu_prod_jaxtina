<?php
 // created: 2024-02-15 13:56:27
$layout_defs["C_News"]["subpanel_setup"]['c_news_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_C_NEWS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'c_news_contacts_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
