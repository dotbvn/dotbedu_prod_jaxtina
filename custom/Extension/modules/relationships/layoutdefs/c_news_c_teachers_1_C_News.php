<?php
 // created: 2023-10-17 11:11:16
$layout_defs["C_News"]["subpanel_setup"]['c_news_c_teachers_1'] = array (
  'order' => 100,
  'module' => 'C_Teachers',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_C_NEWS_C_TEACHERS_1_FROM_C_TEACHERS_TITLE',
  'get_subpanel_data' => 'c_news_c_teachers_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
