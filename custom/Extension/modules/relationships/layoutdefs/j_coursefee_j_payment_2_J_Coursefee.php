<?php
 // created: 2019-06-23 12:15:49
$layout_defs["J_Coursefee"]["subpanel_setup"]['j_coursefee_j_payment_2'] = array (
  'order' => 100,
  'module' => 'J_Payment',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_J_COURSEFEE_J_PAYMENT_2_FROM_J_PAYMENT_TITLE',
  'get_subpanel_data' => 'j_coursefee_j_payment_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
