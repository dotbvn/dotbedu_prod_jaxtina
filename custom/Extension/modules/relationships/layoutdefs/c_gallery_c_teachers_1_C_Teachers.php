<?php
 // created: 2024-03-01 10:13:26
$layout_defs["C_Teachers"]["subpanel_setup"]['c_gallery_c_teachers_1'] = array (
  'order' => 100,
  'module' => 'C_Gallery',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_C_GALLERY_C_TEACHERS_1_FROM_C_GALLERY_TITLE',
  'get_subpanel_data' => 'c_gallery_c_teachers_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
