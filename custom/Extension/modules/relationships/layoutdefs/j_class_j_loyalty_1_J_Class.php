<?php
 // created: 2019-09-14 10:42:45
$layout_defs["J_Class"]["subpanel_setup"]['j_class_j_loyalty_1'] = array (
  'order' => 100,
  'module' => 'J_Loyalty',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_J_CLASS_J_LOYALTY_1_FROM_J_LOYALTY_TITLE',
  'get_subpanel_data' => 'j_class_j_loyalty_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
