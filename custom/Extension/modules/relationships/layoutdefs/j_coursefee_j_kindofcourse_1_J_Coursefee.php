<?php
 // created: 2022-03-28 15:31:32
$layout_defs["J_Coursefee"]["subpanel_setup"]['j_coursefee_j_kindofcourse_1'] = array (
  'order' => 100,
  'module' => 'J_Kindofcourse',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_COURSEFEE_KINDOFCOURSE_TITLE',
  'get_subpanel_data' => 'j_coursefee_j_kindofcourse_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
