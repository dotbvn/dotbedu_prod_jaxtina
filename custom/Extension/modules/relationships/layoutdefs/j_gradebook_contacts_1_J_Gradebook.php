<?php
 // created: 2021-02-01 09:34:27
$layout_defs["J_Gradebook"]["subpanel_setup"]['j_gradebook_contacts_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_J_GRADEBOOK_CONTACTS_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'j_gradebook_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
