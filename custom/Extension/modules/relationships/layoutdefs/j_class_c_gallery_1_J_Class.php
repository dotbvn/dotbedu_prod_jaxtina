<?php
 // created: 2019-08-31 09:36:22
$layout_defs["J_Class"]["subpanel_setup"]['j_class_c_gallery_1'] = array (
  'order' => 100,
  'module' => 'C_Gallery',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_J_CLASS_C_GALLERY_1_FROM_C_GALLERY_TITLE',
  'get_subpanel_data' => 'j_class_c_gallery_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),

  ),
);
