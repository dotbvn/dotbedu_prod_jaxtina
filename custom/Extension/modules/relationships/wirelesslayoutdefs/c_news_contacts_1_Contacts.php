<?php
 // created: 2023-05-08 18:01:46
$layout_defs["Contacts"]["subpanel_setup"]['c_news_contacts_1'] = array (
  'order' => 100,
  'module' => 'C_News',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_C_NEWS_CONTACTS_1_FROM_C_NEWS_TITLE',
  'get_subpanel_data' => 'c_news_contacts_1',
);
