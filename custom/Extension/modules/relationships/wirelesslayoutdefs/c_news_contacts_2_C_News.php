<?php
 // created: 2024-02-15 13:56:27
$layout_defs["C_News"]["subpanel_setup"]['c_news_contacts_2'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_C_NEWS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'c_news_contacts_2',
);
