<?php
 // created: 2021-02-01 09:34:27
$layout_defs["Contacts"]["subpanel_setup"]['j_gradebook_contacts_1'] = array (
  'order' => 100,
  'module' => 'J_Gradebook',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_J_GRADEBOOK_CONTACTS_1_FROM_J_GRADEBOOK_TITLE',
  'get_subpanel_data' => 'j_gradebook_contacts_1',
);
