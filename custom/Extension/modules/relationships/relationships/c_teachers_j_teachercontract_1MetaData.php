<?php
// created: 2019-03-22 11:35:20
$dictionary["c_teachers_j_teachercontract_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'c_teachers_j_teachercontract_1' => 
    array (
      'lhs_module' => 'C_Teachers',
      'lhs_table' => 'c_teachers',
      'lhs_key' => 'id',
      'rhs_module' => 'J_Teachercontract',
      'rhs_table' => 'j_teachercontract',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'c_teachers_j_teachercontract_1_c',
      'join_key_lhs' => 'c_teachers_j_teachercontract_1c_teachers_ida',
      'join_key_rhs' => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
    ),
  ),
  'table' => 'c_teachers_j_teachercontract_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'c_teachers_j_teachercontract_1c_teachers_ida' => 
    array (
      'name' => 'c_teachers_j_teachercontract_1c_teachers_ida',
      'type' => 'id',
    ),
    'c_teachers_j_teachercontract_1j_teachercontract_idb' => 
    array (
      'name' => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_c_teachers_j_teachercontract_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_c_teachers_j_teachercontract_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_teachers_j_teachercontract_1c_teachers_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_c_teachers_j_teachercontract_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'c_teachers_j_teachercontract_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'c_teachers_j_teachercontract_1j_teachercontract_idb',
      ),
    ),
  ),
);