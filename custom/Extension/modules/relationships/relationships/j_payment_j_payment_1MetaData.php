<?php
// created: 2019-03-20 17:40:04
$dictionary["j_payment_j_payment_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'j_payment_j_payment_1' => 
    array (
      'lhs_module' => 'J_Payment',
      'lhs_table' => 'j_payment',
      'lhs_key' => 'id',
      'rhs_module' => 'J_Payment',
      'rhs_table' => 'j_payment',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_payment_j_payment_1_c',
      'join_key_lhs' => 'payment_ida',
      'join_key_rhs' => 'payment_idb',
    ),
  ),
  'table' => 'j_payment_j_payment_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'payment_ida' => 
    array (
      'name' => 'payment_ida',
      'type' => 'id',
    ),
    'payment_idb' => 
    array (
      'name' => 'payment_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_payment_j_payment_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_payment_ida_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'payment_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_payment_idb_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'payment_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_payment_j_payment_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'payment_idb',
      ),
    ),
  ),
);