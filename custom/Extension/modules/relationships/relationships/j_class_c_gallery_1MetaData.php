<?php
// created: 2019-08-31 09:36:22
$dictionary["j_class_c_gallery_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'j_class_c_gallery_1' => 
    array (
      'lhs_module' => 'J_Class',
      'lhs_table' => 'j_class',
      'lhs_key' => 'id',
      'rhs_module' => 'C_Gallery',
      'rhs_table' => 'c_gallery',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_class_c_gallery_1_c',
      'join_key_lhs' => 'j_class_c_gallery_1j_class_ida',
      'join_key_rhs' => 'j_class_c_gallery_1c_gallery_idb',
    ),
  ),
  'table' => 'j_class_c_gallery_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'j_class_c_gallery_1j_class_ida' => 
    array (
      'name' => 'j_class_c_gallery_1j_class_ida',
      'type' => 'id',
    ),
    'j_class_c_gallery_1c_gallery_idb' => 
    array (
      'name' => 'j_class_c_gallery_1c_gallery_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_class_c_gallery_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_j_class_c_gallery_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_class_c_gallery_1j_class_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_j_class_c_gallery_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_class_c_gallery_1c_gallery_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_class_c_gallery_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'j_class_c_gallery_1c_gallery_idb',
      ),
    ),
  ),
);