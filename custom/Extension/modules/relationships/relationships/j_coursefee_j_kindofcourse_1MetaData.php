<?php
// created: 2022-03-28 15:31:32
$dictionary["j_coursefee_j_kindofcourse_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'j_coursefee_j_kindofcourse_1' => 
    array (
      'lhs_module' => 'J_Coursefee',
      'lhs_table' => 'j_coursefee',
      'lhs_key' => 'id',
      'rhs_module' => 'J_Kindofcourse',
      'rhs_table' => 'j_kindofcourse',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_coursefee_j_kindofcourse_1_c',
      'join_key_lhs' => 'coursefee_ida',
      'join_key_rhs' => 'kindofcourse_idb',
    ),
  ),
  'table' => 'j_coursefee_j_kindofcourse_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'coursefee_ida' => 
    array (
      'name' => 'coursefee_ida',
      'type' => 'id',
    ),
    'kindofcourse_idb' => 
    array (
      'name' => 'kindofcourse_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_coursefee_j_kindofcourse_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_j_coursefee_j_kindofcourse_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'coursefee_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_j_coursefee_j_kindofcourse_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'kindofcourse_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_coursefee_j_kindofcourse_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'coursefee_ida',
        1 => 'kindofcourse_idb',
      ),
    ),
  ),
);