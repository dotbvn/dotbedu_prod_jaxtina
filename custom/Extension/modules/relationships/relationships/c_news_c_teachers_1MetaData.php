<?php
// created: 2023-10-17 11:11:16
$dictionary["c_news_c_teachers_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'c_news_c_teachers_1' => 
    array (
      'lhs_module' => 'C_News',
      'lhs_table' => 'c_news',
      'lhs_key' => 'id',
      'rhs_module' => 'C_Teachers',
      'rhs_table' => 'c_teachers',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'c_news_c_teachers_1_c',
      'join_key_lhs' => 'c_news_c_teachers_1c_news_ida',
      'join_key_rhs' => 'c_news_c_teachers_1c_teachers_idb',
    ),
  ),
  'table' => 'c_news_c_teachers_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'c_news_c_teachers_1c_news_ida' => 
    array (
      'name' => 'c_news_c_teachers_1c_news_ida',
      'type' => 'id',
    ),
    'c_news_c_teachers_1c_teachers_idb' => 
    array (
      'name' => 'c_news_c_teachers_1c_teachers_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_c_news_c_teachers_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_c_news_c_teachers_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_news_c_teachers_1c_news_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_c_news_c_teachers_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_news_c_teachers_1c_teachers_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'c_news_c_teachers_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'c_news_c_teachers_1c_news_ida',
        1 => 'c_news_c_teachers_1c_teachers_idb',
      ),
    ),
  ),
);