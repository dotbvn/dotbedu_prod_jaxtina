<?php
// created: 2020-08-24 09:10:44
$dictionary["J_Coursefee"]["fields"]["j_coursefee_j_discount_1"] = array (
  'name' => 'j_coursefee_j_discount_1',
  'type' => 'link',
  'relationship' => 'j_coursefee_j_discount_1',
  'source' => 'non-db',
  'module' => 'J_Discount',
  'bean_name' => 'J_Discount',
  'vname' => 'LBL_J_COURSEFEE_J_DISCOUNT_1_FROM_J_DISCOUNT_TITLE',
  'id_name' => 'j_coursefee_j_discount_1j_discount_idb',
);
