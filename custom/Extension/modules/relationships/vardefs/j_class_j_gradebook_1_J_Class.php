<?php
// created: 2019-03-20 17:13:28
$dictionary["J_Class"]["fields"]["j_class_j_gradebook_1"] = array (
  'name' => 'j_class_j_gradebook_1',
  'type' => 'link',
  'relationship' => 'j_class_j_gradebook_1',
  'source' => 'non-db',
  'module' => 'J_Gradebook',
  'bean_name' => 'J_Gradebook',
  'vname' => 'LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_CLASS_TITLE',
  'id_name' => 'j_class_j_gradebook_1j_class_ida',
  'link-type' => 'many',
  'side' => 'left',
);
