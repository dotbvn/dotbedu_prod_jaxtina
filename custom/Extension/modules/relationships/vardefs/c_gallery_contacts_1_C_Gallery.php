<?php
// created: 2024-02-15 13:54:26
$dictionary["C_Gallery"]["fields"]["c_gallery_contacts_1"] = array (
  'name' => 'c_gallery_contacts_1',
  'type' => 'link',
  'relationship' => 'c_gallery_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_C_GALLERY_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'c_gallery_contacts_1contacts_idb',
);
