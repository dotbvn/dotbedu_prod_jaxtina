<?php
// created: 2019-06-23 12:15:49
$dictionary["J_Coursefee"]["fields"]["j_coursefee_j_payment_2"] = array (
  'name' => 'j_coursefee_j_payment_2',
  'type' => 'link',
  'relationship' => 'j_coursefee_j_payment_2',
  'source' => 'non-db',
  'module' => 'J_Payment',
  'bean_name' => 'J_Payment',
  'vname' => 'LBL_J_COURSEFEE_J_PAYMENT_2_FROM_J_PAYMENT_TITLE',
  'id_name' => 'j_coursefee_j_payment_2j_payment_idb',
);
