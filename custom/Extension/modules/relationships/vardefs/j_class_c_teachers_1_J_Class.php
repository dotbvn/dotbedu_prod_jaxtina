<?php
// created: 2019-03-20 17:10:45
$dictionary["J_Class"]["fields"]["j_class_c_teachers_1"] = array (
  'name' => 'j_class_c_teachers_1',
  'type' => 'link',
  'relationship' => 'j_class_c_teachers_1',
  'source' => 'non-db',
  'module' => 'C_Teachers',
  'bean_name' => 'C_Teachers',
  'vname' => 'LBL_J_CLASS_C_TEACHERS_1_FROM_C_TEACHERS_TITLE',
  'id_name' => 'j_class_c_teachers_1c_teachers_idb',
);
