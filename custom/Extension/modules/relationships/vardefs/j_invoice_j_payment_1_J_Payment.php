<?php
// created: 2022-05-25 11:05:23
$dictionary["J_Payment"]["fields"]["j_invoice_j_payment_1"] = array (
  'name' => 'j_invoice_j_payment_1',
  'type' => 'link',
  'relationship' => 'j_invoice_j_payment_1',
  'source' => 'non-db',
  'module' => 'J_Invoice',
  'bean_name' => 'J_Invoice',
  'vname' => 'LBL_INVOICE',
  'id_name' => 'invoice_id',
);
