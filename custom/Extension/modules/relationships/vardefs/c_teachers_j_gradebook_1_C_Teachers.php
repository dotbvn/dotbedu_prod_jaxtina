<?php
// created: 2019-03-22 11:34:15
$dictionary["C_Teachers"]["fields"]["c_teachers_j_gradebook_1"] = array (
  'name' => 'c_teachers_j_gradebook_1',
  'type' => 'link',
  'relationship' => 'c_teachers_j_gradebook_1',
  'source' => 'non-db',
  'module' => 'J_Gradebook',
  'bean_name' => 'J_Gradebook',
  'vname' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_C_TEACHERS_TITLE',
  'id_name' => 'c_teachers_j_gradebook_1c_teachers_ida',
  'link-type' => 'many',
  'side' => 'left',
);
