<?php
// created: 2021-02-01 09:34:27
$dictionary["J_Gradebook"]["fields"]["j_gradebook_contacts_1"] = array (
  'name' => 'j_gradebook_contacts_1',
  'type' => 'link',
  'relationship' => 'j_gradebook_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_J_GRADEBOOK_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'j_gradebook_contacts_1contacts_idb',
);
