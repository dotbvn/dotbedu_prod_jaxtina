<?php
// created: 2023-10-17 11:11:16
$dictionary["C_News"]["fields"]["c_news_c_teachers_1"] = array (
  'name' => 'c_news_c_teachers_1',
  'type' => 'link',
  'relationship' => 'c_news_c_teachers_1',
  'source' => 'non-db',
  'module' => 'C_Teachers',
  'bean_name' => 'C_Teachers',
  'vname' => 'LBL_C_NEWS_C_TEACHERS_1_FROM_C_TEACHERS_TITLE',
  'id_name' => 'c_news_c_teachers_1c_teachers_idb',
);
