<?php
// created: 2019-03-20 17:10:45
$dictionary["C_Teachers"]["fields"]["j_class_c_teachers_1"] = array (
  'name' => 'j_class_c_teachers_1',
  'type' => 'link',
  'relationship' => 'j_class_c_teachers_1',
  'source' => 'non-db',
  'module' => 'J_Class',
  'bean_name' => 'J_Class',
  'vname' => 'LBL_J_CLASS_C_TEACHERS_1_FROM_J_CLASS_TITLE',
  'id_name' => 'j_class_c_teachers_1j_class_ida',
);
