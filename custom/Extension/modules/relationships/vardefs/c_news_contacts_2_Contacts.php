<?php
// created: 2024-02-15 13:56:27
$dictionary["Contact"]["fields"]["c_news_contacts_2"] = array (
  'name' => 'c_news_contacts_2',
  'type' => 'link',
  'relationship' => 'c_news_contacts_2',
  'source' => 'non-db',
  'module' => 'C_News',
  'bean_name' => 'C_News',
  'vname' => 'LBL_C_NEWS_CONTACTS_2_FROM_C_NEWS_TITLE',
  'id_name' => 'c_news_contacts_2c_news_ida',
);
