<?php
// created: 2023-04-18 18:12:14
$dictionary["J_BankTrans"]["fields"]["j_banktrans_j_paymentdetail_1"] = array (
  'name' => 'j_banktrans_j_paymentdetail_1',
  'type' => 'link',
  'relationship' => 'j_banktrans_j_paymentdetail_1',
  'source' => 'non-db',
  'module' => 'J_PaymentDetail',
  'bean_name' => 'J_PaymentDetail',
  'vname' => 'LBL_J_BANKTRANS_J_PAYMENTDETAIL_1_FROM_J_PAYMENTDETAIL_TITLE',
  'id_name' => 'receipt_id',
);
