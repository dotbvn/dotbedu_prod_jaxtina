<?php
// created: 2023-05-08 18:01:46
$dictionary["Contact"]["fields"]["c_news_contacts_1"] = array (
  'name' => 'c_news_contacts_1',
  'type' => 'link',
  'relationship' => 'c_news_contacts_1',
  'source' => 'non-db',
  'module' => 'C_News',
  'bean_name' => 'C_News',
  'vname' => 'LBL_C_NEWS_CONTACTS_1_FROM_C_NEWS_TITLE',
  'id_name' => 'c_news_contacts_1c_news_ida',
);
