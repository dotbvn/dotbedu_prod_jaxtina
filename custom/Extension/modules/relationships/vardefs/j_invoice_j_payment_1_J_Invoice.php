<?php
// created: 2022-05-25 11:05:23
$dictionary["J_Invoice"]["fields"]["j_invoice_j_payment_1"] = array (
  'name' => 'j_invoice_j_payment_1',
  'type' => 'link',
  'relationship' => 'j_invoice_j_payment_1',
  'source' => 'non-db',
  'module' => 'J_Payment',
  'bean_name' => 'J_Payment',
  'vname' => 'LBL_PAYMENT',
  'id_name' => 'payment_id',
);
