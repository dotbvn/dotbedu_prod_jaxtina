<?php
// created: 2022-03-28 15:31:32
$dictionary["J_Coursefee"]["fields"]["j_coursefee_j_kindofcourse_1"] = array (
  'name' => 'j_coursefee_j_kindofcourse_1',
  'type' => 'link',
  'relationship' => 'j_coursefee_j_kindofcourse_1',
  'source' => 'non-db',
  'module' => 'J_Kindofcourse',
  'bean_name' => 'J_Kindofcourse',
  'vname' => 'LBL_COURSEFEE_KINDOFCOURSE_TITLE',
  'id_name' => 'kindofcourse_idb',
);
