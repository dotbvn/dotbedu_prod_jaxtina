<?php
// created: 2021-02-01 09:34:27
$dictionary["Contact"]["fields"]["j_gradebook_contacts_1"] = array (
  'name' => 'j_gradebook_contacts_1',
  'type' => 'link',
  'relationship' => 'j_gradebook_contacts_1',
  'source' => 'non-db',
  'module' => 'J_Gradebook',
  'bean_name' => 'J_Gradebook',
  'vname' => 'LBL_J_GRADEBOOK_CONTACTS_1_FROM_J_GRADEBOOK_TITLE',
  'id_name' => 'j_gradebook_contacts_1j_gradebook_ida',
);
