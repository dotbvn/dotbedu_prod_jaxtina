<?php
// created: 2024-03-01 10:13:26
$dictionary["C_Teachers"]["fields"]["c_gallery_c_teachers_1"] = array (
  'name' => 'c_gallery_c_teachers_1',
  'type' => 'link',
  'relationship' => 'c_gallery_c_teachers_1',
  'source' => 'non-db',
  'module' => 'C_Gallery',
  'bean_name' => 'C_Gallery',
  'vname' => 'LBL_C_GALLERY_C_TEACHERS_1_FROM_C_GALLERY_TITLE',
  'id_name' => 'c_gallery_c_teachers_1c_gallery_ida',
);
