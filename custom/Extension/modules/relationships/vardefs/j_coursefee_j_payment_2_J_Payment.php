<?php
// created: 2019-06-23 12:15:49
$dictionary["J_Payment"]["fields"]["j_coursefee_j_payment_2"] = array (
  'name' => 'j_coursefee_j_payment_2',
  'type' => 'link',
  'relationship' => 'j_coursefee_j_payment_2',
  'source' => 'non-db',
  'module' => 'J_Coursefee',
  'bean_name' => 'J_Coursefee',
  'vname' => 'LBL_J_COURSEFEE_J_PAYMENT_2_FROM_J_COURSEFEE_TITLE',
  'id_name' => 'j_coursefee_j_payment_2j_coursefee_ida',
);
