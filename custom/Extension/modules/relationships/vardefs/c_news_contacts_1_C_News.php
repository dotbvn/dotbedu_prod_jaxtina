<?php
// created: 2023-05-08 18:01:46
$dictionary["C_News"]["fields"]["c_news_contacts_1"] = array (
  'name' => 'c_news_contacts_1',
  'type' => 'link',
  'relationship' => 'c_news_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_C_NEWS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'c_news_contacts_1contacts_idb',
);
