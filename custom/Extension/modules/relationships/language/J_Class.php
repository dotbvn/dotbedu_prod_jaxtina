<?php
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_J_CLASS_CONTACTS_1_FROM_CONTACTS_TITLE'] = 'Students';
$mod_strings['LBL_J_CLASS_CONTACTS_1_FROM_J_CLASS_TITLE'] = 'Students';
$mod_strings['LBL_J_CLASS_C_TEACHERS_1_FROM_C_TEACHERS_TITLE'] = 'Teachers';
$mod_strings['LBL_J_CLASS_C_TEACHERS_1_FROM_J_CLASS_TITLE'] = 'Teachers';
$mod_strings['LBL_J_CLASS_J_CLASS_1_FROM_J_CLASS_R_TITLE'] = 'Classes';
$mod_strings['LBL_J_CLASS_J_CLASS_1_FROM_J_CLASS_L_TITLE'] = 'Classes';
$mod_strings['LBL_J_CLASS_CASES_1_FROM_CASES_TITLE'] = 'Feedback';
$mod_strings['LBL_J_CLASS_CASES_1_FROM_J_CLASS_TITLE'] = 'Feedback';
$mod_strings['LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_GRADEBOOK_TITLE'] = 'Gradebooks';
$mod_strings['LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_CLASS_TITLE'] = 'Gradebooks';
$mod_strings['LBL_J_CLASS_LEADS_1_FROM_LEADS_TITLE'] = 'Leads';
$mod_strings['LBL_J_CLASS_LEADS_1_FROM_J_CLASS_TITLE'] = 'Leads';
$mod_strings['LBL_J_COURSEFEE_J_CLASS_1_FROM_J_COURSEFEE_TITLE'] = 'Course Fees';
$mod_strings['LBL_J_COURSEFEE_J_CLASS_1_FROM_J_CLASS_TITLE'] = 'Course Fees';
$mod_strings['LBL_J_CLASS_J_INVENTORY_1_FROM_J_INVENTORY_TITLE'] = 'Inventory';
$mod_strings['LBL_J_CLASS_J_INVENTORY_1_FROM_J_CLASS_TITLE'] = 'Inventory';
$mod_strings['LBL_J_CLASS_TASKS_1_FROM_TASKS_TITLE'] = 'Tasks';
$mod_strings['LBL_J_CLASS_TASKS_1_FROM_J_CLASS_TITLE'] = 'Tasks';
$mod_strings['LBL_C_NEWS_J_CLASS_1_FROM_C_NEWS_TITLE'] = 'News';
$mod_strings['LBL_C_NEWS_J_CLASS_1_FROM_J_CLASS_TITLE'] = 'News';
$mod_strings['LBL_J_CLASS_C_NEWS_1_FROM_C_NEWS_TITLE'] = 'News';
$mod_strings['LBL_J_CLASS_C_NEWS_1_FROM_J_CLASS_TITLE'] = 'News';
$mod_strings['LBL_J_CLASS_C_GALLERY_1_FROM_C_GALLERY_TITLE'] = 'Gallery';
$mod_strings['LBL_J_CLASS_C_GALLERY_1_FROM_J_CLASS_TITLE'] = 'Gallery';
$mod_strings['LBL_J_CLASS_J_LOYALTY_1_FROM_J_LOYALTY_TITLE'] = 'Loyalty';
$mod_strings['LBL_J_CLASS_J_LOYALTY_1_FROM_J_CLASS_TITLE'] = 'Loyalty';
