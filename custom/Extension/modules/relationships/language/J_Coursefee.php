<?php
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_J_COURSEFEE_J_CLASS_1_FROM_J_CLASS_TITLE'] = 'Classes';
$mod_strings['LBL_J_COURSEFEE_J_CLASS_1_FROM_J_COURSEFEE_TITLE'] = 'Classes';
$mod_strings['LBL_J_COURSEFEE_J_PAYMENT_1_FROM_J_PAYMENT_TITLE'] = 'Payments';
$mod_strings['LBL_J_COURSEFEE_J_PAYMENT_1_FROM_J_COURSEFEE_TITLE'] = 'Payments';
$mod_strings['LBL_J_COURSEFEE_J_PAYMENT_2_FROM_J_PAYMENT_TITLE'] = 'Payments';
$mod_strings['LBL_J_COURSEFEE_J_PAYMENT_2_FROM_J_COURSEFEE_TITLE'] = 'Payments';
$mod_strings['LBL_J_COURSEFEE_J_DISCOUNT_1_FROM_J_DISCOUNT_TITLE'] = 'Discounts';
$mod_strings['LBL_J_COURSEFEE_J_DISCOUNT_1_FROM_J_COURSEFEE_TITLE'] = 'Discounts';
$mod_strings['LBL_COURSEFEE_KINDOFCOURSE_TITLE'] = 'Apply with Kind of Courses';
$mod_strings['LBL_KINDOFCOURSE_COURSEFEE_TITLE'] = 'Apply with Kind of Courses';
