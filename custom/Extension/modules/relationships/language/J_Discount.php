<?php
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_J_DISCOUNT_J_DISCOUNT_1_FROM_J_DISCOUNT_R_TITLE'] = 'Do not Apply with Discounts';
$mod_strings['LBL_J_DISCOUNT_J_DISCOUNT_1_FROM_J_DISCOUNT_L_TITLE'] = 'Do not Apply with Discounts';
$mod_strings['LBL_J_PAYMENT_J_DISCOUNT_1_FROM_J_PAYMENT_TITLE'] = 'Payments';
$mod_strings['LBL_J_PAYMENT_J_DISCOUNT_1_FROM_J_DISCOUNT_TITLE'] = 'Payments';
$mod_strings['LBL_J_COURSEFEE_J_DISCOUNT_1_FROM_J_COURSEFEE_TITLE'] = 'Course Fees';
$mod_strings['LBL_J_COURSEFEE_J_DISCOUNT_1_FROM_J_DISCOUNT_TITLE'] = 'Course Fees';
