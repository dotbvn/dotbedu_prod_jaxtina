<?php

    $layout_defs["J_GradebookSettingGroup"]["subpanel_setup"]["gbsettinggroup_gbconfig"] = array (
        'order' => 1,
        'module' => 'J_GradebookConfig',
        'subpanel_name' => 'default',
        'title_key' => 'LBL_GBCONFIG',
        'sort_order' => 'asc',
        'sort_by' => 'name',
        'get_subpanel_data' => 'gbconfig_link',
        'top_buttons' =>
        array (
            0 =>
            array (
                'widget_class' => 'SubPanelTopButtonQuickCreate',
            ),
            1 =>
            array (
                'widget_class' => 'SubPanelTopSelectButton',
                'mode' => 'MultiSelect',
            ),
        ),
    );
