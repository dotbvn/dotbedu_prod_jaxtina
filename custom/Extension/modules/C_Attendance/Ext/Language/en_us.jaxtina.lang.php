<?php
$mod_strings['LBL_FLIPPED'] = 'Flipped';
$mod_strings['LBL_HOMEWORK_NUM'] = 'Homework';
$mod_strings['LBL_TEACHER_COMMENT_NUM'] = 'Student\'s Interaction (Point)';
$mod_strings['LBL_HOMEWORK_SCORE'] = 'Active Learning';

$mod_strings['LBL_HOMEWORK_COMMENT']='Homework Comment';
$mod_strings['LBL_TEACHER_COMMENT_PERCENT'] = 'Student\'s Interaction (%)';

$mod_strings['LBL_TOTAL_TIME'] = 'Online Time';
$mod_strings['LBL_TOTAL_SCORE'] = 'Total Score';
$mod_strings['LBL_NUMBER_PART'] = 'Number of Parts';
$mod_strings['LBL_NUMBER_QUESTION'] = 'Number of Questions';
$mod_strings['LBL_NUMBER_RIGHT'] = 'Number of Right Answers';
$mod_strings['LBL_NUMBER_WRONG'] = 'Number of Wrong Answers';
$mod_strings['LBL_WRONG_ANS_LIST'] = 'Wrong Answers List';