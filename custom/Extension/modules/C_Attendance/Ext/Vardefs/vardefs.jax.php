<?php
//Custom field Attendance Page
$dictionary['C_Attendance']['fields']['flipped'] = array(
    'name' => 'flipped',
    'vname' => 'LBL_FLIPPED',
    'type' => 'decimal',
    'len' => 10,
    'precision' => 2,
);
$dictionary['C_Attendance']['fields']['homework_num'] = array(
    'name' => 'homework_num',
    'vname' => 'LBL_HOMEWORK_NUM',
    'type' => 'decimal',
    'len' => 10,
    'precision' => 2,
);
$dictionary['C_Attendance']['fields']['teacher_comment_num'] = array(
    'name' => 'teacher_comment_num',
    'vname' => 'LBL_TEACHER_COMMENT_NUM',
    'type' => 'decimal',
    'len' => 10,
    'precision' => 2,
);

//END

$dictionary['C_Attendance']['fields']['total_time'] = array(
    'name' => 'total_time',
    'vname' => 'LBL_TOTAL_TIME',
    'type' => 'decimal',
    'len' => 10,
    'precision' => 2,
);

$dictionary['C_Attendance']['fields']['total_score'] = array(
    'name' => 'total_score',
    'vname' => 'LBL_TOTAL_SCORE',
    'type' => 'decimal',
    'len' => 10,
    'precision' => 2,
);


$dictionary['C_Attendance']['fields']['number_of_part'] = array (
    'name' => 'number_of_part',
    'vname' => 'LBL_NUMBER_PART',
    'type' => 'int',
    'len' => 5,
);

$dictionary['C_Attendance']['fields']['number_of_question'] = array (
    'name' => 'number_of_question',
    'vname' => 'LBL_NUMBER_QUESTION',
    'type' => 'int',
    'len' => 5,
);

$dictionary['C_Attendance']['fields']['number_of_right'] = array (
    'name' => 'number_of_right',
    'vname' => 'LBL_NUMBER_RIGHT',
    'type' => 'int',
    'len' => 5,
);

$dictionary['C_Attendance']['fields']['number_of_wrong'] = array (
    'name' => 'number_of_wrong',
    'vname' => 'LBL_NUMBER_WRONG',
    'type' => 'int',
    'len' => 5,
);


$dictionary['C_Attendance']['fields']['wrong_ans_list'] = array (
    'name' => 'wrong_ans_list',
    'vname' => 'LBL_WRONG_ANS_LIST',
    'type' => 'text',
);

$dictionary['C_Attendance']['fields']['teacher_comment_num_per'] = array(
    'name' => 'teacher_comment_num_per',
    'vname' => 'LBL_TEACHER_COMMENT_PERCENT',
    'type' => 'decimal',
    'len' => 10,
    'precision' => 2,
);
