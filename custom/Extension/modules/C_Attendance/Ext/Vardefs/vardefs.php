<?php
$dictionary['C_Attendance']['fields']['datetime_notify_sent'] = array (
    'name' => 'datetime_notify_sent',
    'vname' => 'LBL_DATETIME_NOTIFY_SENT',
    'type' => 'datetime',
);
$dictionary['C_Attendance']['fields']['check_in_time'] = array (
    'name' => 'check_in_time',
    'vname' => 'LBL_CHECK_IN_TIME',
    'type' => 'datetime',
    'required' => false,
    'reportable' => true,
    'massupdate' => false,
    'readonly' => true,
);
$dictionary['C_Attendance']['fields']['check_out_time'] = array (
    'name' => 'check_out_time',
    'vname' => 'LBL_CHECK_OUT_TIME',
    'type' => 'datetime',
    'required' => false,
    'reportable' => true,
    'massupdate' => false,
    'readonly' => true,
);
//Khai bao field quan he
$dictionary['C_Attendance']['fields']['check_in_id'] = array(
    'name' => 'check_in_id',
    'vname' => 'LBL_CHECK_IN_ID',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['C_Attendance']['fields']['check_in_name'] = array(
    'name' => 'check_in_name',
    'rname' => 'name',
    'id_name' => 'check_in_id',
    'vname' => 'LBL_CHECK_IN_NAME',
    'type' => 'relate',
    'link' => 'check_in_link',
    'table' => 'j_checkinout',
    'module' => 'J_CheckInOut',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['C_Attendance']['fields']['check_in_link'] = array(
    'name' => 'check_in_link',
    'type' => 'link',
    'relationship' => 'checkin_attendance',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_CHECK_IN_NAME',
);
$dictionary['C_Attendance']['fields']['check_out_id'] = array(
    'name' => 'check_out_id',
    'vname' => 'LBL_CHECK_OUT_ID',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['C_Attendance']['fields']['check_out_name'] = array(
    'name' => 'check_out_name',
    'rname' => 'name',
    'id_name' => 'check_out_id',
    'vname' => 'LBL_CHECK_OUT_NAME',
    'type' => 'relate',
    'link' => 'check_out_link',
    'table' => 'j_checkinout',
    'module' => 'J_CheckInOut',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['C_Attendance']['fields']['check_out_link'] = array(
    'name' => 'check_out_link',
    'type' => 'link',
    'relationship' => 'checkout_attendance',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_CHECK_OUT_NAME',
);
