<?php
$dictionary["C_Comments"]["fields"]["image_link"] = array (
    'name' => 'image_link',
    'type' => 'link',
    'relationship' => 'comment_images',
    'module' => 'C_UploadImage',
    'bean_name' => 'C_UploadImage',
    'source' => 'non-db',
    'vname' => 'LBL_IMAGES',
);

$dictionary["C_Comments"]["relationships"]["comment_images"] = array (
    'lhs_module' => 'C_Comments',
    'lhs_table' => 'c_comments',
    'lhs_key' => 'id',
    'rhs_module' => 'C_UploadImage',
    'rhs_table' => 'c_uploadimage',
    'rhs_key' => 'comment_id',
    'relationship_type' => 'one-to-many'
);

//HIDE FIELD: STUDIO _ PROCESS
$dictionary['C_Comments']['fields']['description']['required'] = true;
$dictionary['C_Comments']['fields']['description']['rows'] = 4;
$dictionary['C_Comments']['fields']['description']['cols'] = 90;

$dictionary['C_Comments']['fields']['direction']['studio'] = 'visible';
$dictionary['C_Comments']['fields']['filename']['studio'] = 'visible';
$dictionary['C_Comments']['fields']['filename']['parent_name'] = 'visible';
$dictionary['C_Comments']['fields']['filename']['parent_id'] = 'visible';



$dictionary['C_Comments']['fields']['status_id']['studio'] = false;
$dictionary['C_Comments']['fields']['file_ext']['studio'] = false;
$dictionary['C_Comments']['fields']['document_name']['studio'] = false;
$dictionary['C_Comments']['fields']['active_date']['studio'] = false;
$dictionary['C_Comments']['fields']['exp_date']['studio'] = false;
$dictionary['C_Comments']['fields']['category_id']['studio'] = false;
$dictionary['C_Comments']['fields']['subcategory_id']['studio'] = false;
$dictionary['C_Comments']['fields']['file_mime_type']['studio'] = false;

$dictionary['C_Comments']['fields']['description']['audited'] = true;
$dictionary['C_Comments']['fields']['is_unsent'] = array(
    'name'          => 'is_unsent',
    'vname'         => 'LBL_IS_UNSENT',
    'type'          => 'bool',
    'no_default'    => false,
    'default'       => 0,
);
$dictionary['C_Comments']['fields']['is_updated'] = array(
    'name'          => 'is_updated',
    'vname'         => 'LBL_IS_UPDATED',
    'type'          => 'bool',
    'no_default'    => false,
    'default'       => 0,
);
//Field for identify student (use to comment in gallery and new)
$dictionary['C_Comments']['fields']['student_id'] = array(
    'name' => 'student_id',
    'vname' => 'LBL_STUDENT_ID',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['C_Comments']['fields']['student_name'] = array(
    'name' => 'student_name',
    'rname' => 'name',
    'id_name' => 'student_id',
    'vname' => 'LBL_STUDENT_NAME',
    'type' => 'relate',
    'link' => 'student_link',
    'table' => 'contacts',
    'module' => 'Contacts',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['C_Comments']['fields']['student_link'] = array(
    'name' => 'student_link',
    'type' => 'link',
    'relationship' => 'c_comments_contacts',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_STUDENT_NAME',
);
$dictionary['C_Comments']['fields']['attachment_list'] = array(
    'name' => 'attachment_list',
    'type' => 's3-multi',
    'source' => 'non-db',
    'vname' => 'LBL_ATTACHMENTS',
    'duplicate_on_record_copy' => 'no',
    'studio' => false,
    'group' => 'attachments',
);