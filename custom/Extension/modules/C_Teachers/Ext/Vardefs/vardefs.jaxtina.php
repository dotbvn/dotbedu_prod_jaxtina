<?php
$dictionary['C_Teachers']['relationships']['teacher_cases'] = array (
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'Cases',
    'rhs_table' => 'cases',
    'rhs_key' => 'teacher_id',
    'relationship_type' => 'one-to-many',
);

$dictionary['C_Teachers']['fields']['teacher_entrance_score'] = array (
    'name' => 'teacher_entrance_score',
    'vname' => 'LBL_ENTRANCE_SCORE',
    'type' => 'text',
    'reportable' => true,
    'studio' => 'visible',
);

$dictionary['C_Teachers']['fields']['teacher_contract_info'] = array (
    'name' => 'teacher_contract_info',
    'vname' => 'LBL_CONTRACT_INFO',
    'required' => true,
    'type' => 'enum',
    'options'=>'option_contract_info'
);
