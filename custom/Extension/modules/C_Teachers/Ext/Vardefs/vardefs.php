<?php
//Add Required
$dictionary['C_Teachers']['fields']['phone_mobile']['required']=true;
$dictionary['C_Teachers']['fields']['email1']['required']=true;
$dictionary['C_Teachers']['fields']['type']['massupdate']=true;
$dictionary['C_Teachers']['fields']['type']['importable']='true';

$dictionary["C_Teachers"]["fields"]["c_teachers_j_gradebook_1"] = array (
  'name' => 'c_teachers_j_gradebook_1',
  'type' => 'link',
  'relationship' => 'c_teachers_j_gradebook_1',
  'source' => 'non-db',
  'module' => 'J_Gradebook',
  'bean_name' => 'J_Gradebook',
  'vname' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_C_TEACHERS_TITLE',
  'id_name' => 'c_teachers_j_gradebook_1c_teachers_ida',
  'link-type' => 'many',
  'side' => 'left',
);

$dictionary["C_Teachers"]["fields"]["c_teachers_j_teachercontract_1"] = array (
  'name' => 'c_teachers_j_teachercontract_1',
  'type' => 'link',
  'relationship' => 'c_teachers_j_teachercontract_1',
  'source' => 'non-db',
  'module' => 'J_Teachercontract',
  'bean_name' => 'J_Teachercontract',
  'vname' => 'LBL_C_TEACHERS_J_TEACHERCONTRACT_1_FROM_C_TEACHERS_TITLE',
  'id_name' => 'c_teachers_j_teachercontract_1c_teachers_ida',
  'link-type' => 'many',
  'side' => 'left',
);

$dictionary["C_Teachers"]["fields"]["j_teacherqe_c_teachers"] = array (
  'name' => 'j_teacherqe_c_teachers',
  'type' => 'link',
  'relationship' => 'j_teacherqe_c_teachers',
  'source' => 'non-db',
  'module' => 'J_TeacherQE',
  'bean_name' => false,
  'vname' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_C_TEACHERS_TITLE',
  'id_name' => 'j_teacherqe_c_teachersc_teachers_ida',
  'link-type' => 'many',
  'side' => 'left',
);
$dictionary["C_Teachers"]["fields"]["j_class_c_teachers_1"] = array (
  'name' => 'j_class_c_teachers_1',
  'type' => 'link',
  'relationship' => 'j_class_c_teachers_1',
  'source' => 'non-db',
  'module' => 'J_Class',
  'bean_name' => 'J_Class',
  'vname' => 'LBL_J_CLASS_C_TEACHERS_1_FROM_J_CLASS_TITLE',
  'id_name' => 'j_class_c_teachers_1j_class_ida',
);

$dictionary['C_Teachers']['fields']['cover_app'] = array(
    'name' => 'cover_app',
    'vname' => 'LBL_COVER_APP',
    'type' => 'varchar',
    'len' => '255',
);
$dictionary['C_Teachers']['fields']['gender'] = array(
    'name' => 'gender',
    'required' => false,
    'vname' => 'LBL_GENDER',
    'type' => 'enum',
    'massupdate' => true,
    'len' => 20,
    'options' => 'gender_list',
    'default' => '',
    'importable' => true,
);

$dictionary['C_Teachers']['fields']['lasted_view_noti'] = array(
    'name' => 'lasted_view_noti',
    'vname' => 'LBL_LASTED_VIEW_NOTI',
    'type' => 'datetime',
    'required' => false,
    'massupdate' => 0,
    'duplicate_on_record_copy' => 'no',
);

//Expense
$dictionary['C_Teachers']['fields']['j_budget_link'] = array(
    'name' => 'j_budget_link',
    'type' => 'link',
    'relationship' => 'teachers_j_budget',
    'module' => 'J_Budget',
    'bean_name' => 'J_Budget',
    'source' => 'non-db',
    'vname' => 'LBL_EXPENSE',
    'reportable' => false
    );

$dictionary['C_Teachers']['relationships']['teachers_j_budget'] = array(
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Budget',
    'rhs_table' => 'j_budget',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Teachers',
);