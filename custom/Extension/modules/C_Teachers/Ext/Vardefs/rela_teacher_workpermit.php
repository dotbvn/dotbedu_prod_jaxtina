<?php
//Add Relationship teacher - Work permit
$dictionary['C_Teachers']['relationships']['teachers_workpermit'] = array(
    'lhs_module' => 'C_Teachers',
    'lhs_table' => 'c_teachers',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Workpermit',
    'rhs_table' => 'j_workpermit',
    'rhs_key' => 'teacher_id',
    'relationship_type' => 'one-to-many'
);

//Add Relationship teacher - Work permit
$dictionary['C_Teachers']['fields']['work_permit_link'] = array(
    'name' => 'work_permit_link',
    'type' => 'link',
    'relationship' => 'teachers_workpermit',
    'module' => 'J_Workpermit',
    'bean_name' => 'J_Workpermit',
    'source' => 'non-db',
    'vname' => 'LBL_WORK_PERMIT',
);
