<?php
// created: 2024-03-01 10:13:26
$viewdefs['C_Teachers']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_C_GALLERY_C_TEACHERS_1_FROM_C_GALLERY_TITLE',
  'context' => 
  array (
    'link' => 'c_gallery_c_teachers_1',
  ),
);