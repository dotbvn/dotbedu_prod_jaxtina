<?php
$layout_defs["C_Teachers"]["subpanel_setup"]["teacher_meetings"] = array (
    'order' => 300,
    'module' => 'Meetings',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_MEETING',
    'sort_order' => 'asc',
    'sort_by' => 'date_start',
    'get_subpanel_data' => 'function:getSessions',
    'function_parameters' => array(
        'import_function_file' => 'custom/modules/C_Teachers/customSubPanel.php',
        'teacher_id' => $this->_focus->id,
        'teacher_type' => $this->_focus->type,
        'return_as_array' => 'true'
    ),
    'top_buttons' =>
        array (
        ),
);

?>
