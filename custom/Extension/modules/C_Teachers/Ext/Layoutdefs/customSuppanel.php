<?php
$layout_defs["C_Teachers"]["subpanel_setup"]["teacher_holidays"] = array (
    'order' => 30,
    'module' => 'Holidays',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_HOLIDAYS',
    'sort_order' => 'desc',
    'sort_by' => 'date_entered',
    'get_subpanel_data' => 'holiday_link',
    'top_buttons' =>
    array (
        0 =>
        array (
            'widget_class' => 'SubPanelTopButtonQuickCreate',
        ),

    ),
);
$layout_defs["C_Teachers"]["subpanel_setup"]["teachers_workpermit"] = array (
    'order' => 21,
    'module' => 'J_Workpermit',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_WORK_PERMIT',
    'get_subpanel_data' => 'work_permit_link',
    'top_buttons' =>
        array (
            0 =>
                array (
                    'widget_class' => 'SubPanelTopButtonQuickCreate',
                ),
        ),
);

?>
