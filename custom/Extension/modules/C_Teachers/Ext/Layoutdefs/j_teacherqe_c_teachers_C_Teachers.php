<?php
 // created: 2020-09-29 10:36:07
$layout_defs["C_Teachers"]["subpanel_setup"]['j_teacherqe_c_teachers'] = array (
  'order' => 100,
  'module' => 'J_TeacherQE',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_J_TEACHERQE_TITLE',
  'get_subpanel_data' => 'j_teacherqe_c_teachers',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
