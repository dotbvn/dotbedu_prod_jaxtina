<?php
 // created: 2023-10-17 11:11:16
$layout_defs["C_Teachers"]["subpanel_setup"]['c_news_c_teachers_1'] = array (
  'order' => 100,
  'module' => 'C_News',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_C_NEWS_C_TEACHERS_1_FROM_C_NEWS_TITLE',
  'get_subpanel_data' => 'c_news_c_teachers_1',
);
