<?php
// created: 2019-03-22 11:34:15
$dictionary["J_Gradebook"]["fields"]["c_teachers_j_gradebook_1"] = array (
  'name' => 'c_teachers_j_gradebook_1',
  'type' => 'link',
  'relationship' => 'c_teachers_j_gradebook_1',
  'source' => 'non-db',
  'module' => 'C_Teachers',
  'bean_name' => 'C_Teachers',
  'side' => 'right',
  'vname' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_J_GRADEBOOK_TITLE',
  'id_name' => 'c_teachers_j_gradebook_1c_teachers_ida',
  'link-type' => 'one',
);

$dictionary["J_Gradebook"]["fields"]["c_teachers_j_gradebook_1_name"] = array (
  'name' => 'c_teachers_j_gradebook_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_C_TEACHERS_TITLE',
  'save' => true,
  'id_name' => 'c_teachers_j_gradebook_1c_teachers_ida',
  'link' => 'c_teachers_j_gradebook_1',
  'table' => 'c_teachers',
  'module' => 'C_Teachers',
  'rname' => 'full_teacher_name',
);

$dictionary["J_Gradebook"]["fields"]["c_teachers_j_gradebook_1c_teachers_ida"] = array (
  'name' => 'c_teachers_j_gradebook_1c_teachers_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_J_GRADEBOOK_TITLE_ID',
  'id_name' => 'c_teachers_j_gradebook_1c_teachers_ida',
  'link' => 'c_teachers_j_gradebook_1',
  'table' => 'c_teachers',
  'module' => 'C_Teachers',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

// created: 2019-03-20 17:13:28
$dictionary["J_Gradebook"]["fields"]["j_class_j_gradebook_1"] = array (
  'name' => 'j_class_j_gradebook_1',
  'type' => 'link',
  'relationship' => 'j_class_j_gradebook_1',
  'source' => 'non-db',
  'module' => 'J_Class',
  'bean_name' => 'J_Class',
  'side' => 'right',
  'vname' => 'LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_GRADEBOOK_TITLE',
  'id_name' => 'j_class_j_gradebook_1j_class_ida',
  'link-type' => 'one',
);
$dictionary["J_Gradebook"]["fields"]["j_class_j_gradebook_1_name"] = array (
  'name' => 'j_class_j_gradebook_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_CLASS_TITLE',
  'save' => true,
  'id_name' => 'j_class_j_gradebook_1j_class_ida',
  'link' => 'j_class_j_gradebook_1',
  'table' => 'j_class',
  'module' => 'J_Class',
  'rname' => 'name',
);
$dictionary["J_Gradebook"]["fields"]["j_class_j_gradebook_1j_class_ida"] = array (
  'name' => 'j_class_j_gradebook_1j_class_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_GRADEBOOK_TITLE_ID',
  'id_name' => 'j_class_j_gradebook_1j_class_ida',
  'link' => 'j_class_j_gradebook_1',
  'table' => 'j_class',
  'module' => 'J_Class',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);


// created: 2021-02-01 09:34:27
$dictionary["J_Gradebook"]["fields"]["j_gradebook_contacts_1"] = array (
  'name' => 'j_gradebook_contacts_1',
  'type' => 'link',
  'relationship' => 'j_gradebook_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_J_GRADEBOOK_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'j_gradebook_contacts_1contacts_idb',
);

