<?php
$dictionary["J_CheckInOut"]["fields"]["time_check"] = array(
    'name' => 'time_check',
    'vname' => 'LBL_TIME',
    'type' => 'datetime',
    'importable' => 'false',
    'massupdate' => false,
);
//Khai bao field quan he
$dictionary['J_CheckInOut']['fields']['student_id'] = array(
    'name' => 'student_id',
    'vname' => 'LBL_STUDENT_ID',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['J_CheckInOut']['fields']['student_name'] = array(
    'name' => 'student_name',
    'rname' => 'name',
    'id_name' => 'student_id',
    'vname' => 'LBL_STUDENT_NAME',
    'type' => 'relate',
    'link' => 'student_link',
    'table' => 'contacts',
    'module' => 'Contacts',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['J_CheckInOut']['fields']['student_link'] = array(
    'name' => 'student_link',
    'type' => 'link',
    'relationship' => 'checkinout_contacts',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_STUDENT_NAME',
);
//Khai bao field quan he
$dictionary['J_CheckInOut']['fields']['device_id'] = array(
    'name' => 'device_id',
    'vname' => 'LBL_DEVICE_ID',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['J_CheckInOut']['fields']['device_name'] = array(
    'name' => 'device_name',
    'rname' => 'name',
    'id_name' => 'device_id',
    'vname' => 'LBL_DEVICE_NAME',
    'type' => 'relate',
    'link' => 'device_link',
    'table' => 'j_attdevice',
    'module' => 'J_AttDevice',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['J_CheckInOut']['fields']['device_link'] = array(
    'name' => 'device_link',
    'type' => 'link',
    'relationship' => 'attdevice_checkinout',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_DEVICE_NAME',
);

$dictionary["J_CheckInOut"]["fields"]["verify_status"] = array(
    'name' => 'verify_status',
    'vname' => 'LBL_VERIFY_STATUS',
    'type' => 'varchar',
    'importable' => 'false',
    'massupdate' => false,
    'len' => 30
);
$dictionary["J_CheckInOut"]["fields"]["similarity_percent"] = array(
    'name' => 'similarity_percent',
    'vname' => 'LBL_SIMILARITY_PERCENT',
    'type' => 'decimal',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 26,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);

$dictionary["J_CheckInOut"]["fields"]["pic_id"] = array(
    'name' => 'pic_id',
    'vname' => 'LBL_PIC_ID',
    'type' => 'image',
    'dbtype' => 'varchar',
    'massupdate' => false,
    'reportable' => false,
    'comment' => 'Avatar',
    'len' => '255',
    'width' => '42',
    'height' => '42',
    'border' => '',
);


//Add relationship with C_Attendance
$dictionary['J_CheckInOut']['relationships']['checkin_attendance'] = array(
    'lhs_module' => 'J_CheckInOut',
    'lhs_table' => 'j_checkinout',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Attendance',
    'rhs_table' => 'c_attendance',
    'rhs_key' => 'check_in_id',
    'relationship_type' => 'one-to-many'
);
$dictionary['J_CheckInOut']['relationships']['checkout_attendance'] = array(
    'lhs_module' => 'J_CheckInOut',
    'lhs_table' => 'j_checkinout',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Attendance',
    'rhs_table' => 'c_attendance',
    'rhs_key' => 'check_out_id',
    'relationship_type' => 'one-to-many'
);