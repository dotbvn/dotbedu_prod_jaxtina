<?php

$dictionary['Meeting']['fields']['lock_status'] = array(
    'name' => 'lock_status',
    'vname' => 'LBL_LOCK_STATUS',
    'help' => 'LBL_LOCK_STATUS_DES',
    'type' => 'enum',
    'len' => 20,
    'size' => '20',
    'default' => 'Unlocked',
    'options' => 'lock_status_list',
    'studio' => 'visible',
    'dependency' => false,
);