<?php
/**
* Create By: Hiếu Phạm
* DateTime: 10:38 AM 09/04/2019
* To: override option parent_name
*/
$dictionary['Meeting']['fields']['parent_name']['options'] = 'meeting_parent_type_display';
$dictionary['Meeting']['fields']['relationships']['meetings_loyalty_link'] = array(
    'lhs_module'        => 'Meetings',
    'lhs_table'            => 'meetings',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_Loyalty',
    'rhs_table'            => 'j_loyalty',
    'rhs_key'            => 'meeting_id',
    'relationship_type'    => 'one-to-many',
);

$dictionary['Meeting']['fields']['attended_label'] = array(
    'name' => 'attended_label',
    'vname' => 'LBL_ATTENDED',
    'type' => 'varchar',
    'len' => '1',
    'studio' => 'visible',
    'source' => 'non-db',
    'studio' => 'false',
);
$dictionary['Meeting']['fields']['sent_app_label'] = array(
    'name' => 'sent_app_label',
    'vname' => 'LBL_SENDING_APPS_STATUS',
    'type' => 'varchar',
    'len' => '1',
    'studio' => 'visible',
    'source' => 'non-db',
    'studio' => 'false',
);
$dictionary['Meeting']['fields']['subpanel_btn_edit'] = array(
    'name' => 'subpanel_btn_edit',
    'type' => 'varchar',
    'len' => '1',
    'studio' => 'visible',
    'source' => 'non-db',
    'studio' => 'false',
);

$dictionary['Meeting']['fields']['subpanel_btn3'] = array(
    'name' => 'subpanel_btn3',
    'type' => 'varchar',
    'len' => '1',
    'studio' => 'visible',
    'source' => 'non-db',
    'studio' => 'false',
);

$dictionary['Meeting']['fields']['late_time'] = array(
    'name' => 'late_time',
    'vname' => 'LBL_LATE_TIME',
    'type' => 'enum',
    'len' => 50,
    'comment' => 'Teacher Late time',
    'options' => 'late_time_list',
    'massupdate' => false,
    'studio' => 'false',
);
$dictionary['Meeting']['fields']['late_time_ta1'] = array(
    'name' => 'late_time_ta1',
    'vname' => 'LBL_LATE_TIME_TA1',
    'type' => 'enum',
    'len' => 50,
    'comment' => 'TA1 Late time',
    'options' => 'late_time_list',
    'massupdate' => false,
    'studio' => 'false',
);
$dictionary['Meeting']['fields']['late_time_ta2'] = array(
    'name' => 'late_time_ta2',
    'vname' => 'LBL_LATE_TIME_TA2',
    'type' => 'enum',
    'len' => 50,
    'comment' => 'TA2 Late time',
    'options' => 'late_time_list',
    'massupdate' => false,
    'studio' => 'false',
);
$dictionary['Meeting']['fields']['planned_absence'] = array(
    'name' => 'planned_absence',
    'vname' => 'LBL_PLANNED_ABSENCE',
    'type' => 'enum',
    'len' => 100,
    'options' => 'planned_absence_list',
    'massupdate' => false,
    'studio' => 'false',
);

//0verride default value of session_status
$dictionary['Meeting']['fields']['session_status']['default'] = 'Not Started';


// Syllabus (1 - n) Meeting
$dictionary['Meeting']['fields']['syllabus_name'] = array(
    'name' => 'syllabus_name',
    'vname' => 'LBL_SYL_SYLLABUS',
    'type' => 'relate',
    'dbType' => 'varchar',
    'rname' => 'name',
    'id_name' => 'syllabus_id',
    'join_name' => 'j_syllabus',
    'link' => 'syllabus_link',
    'table' => 'j_syllabus',
    'isnull' => 'true',
    'module' => 'J_Syllabus',
    'source' => 'non-db',
);
$dictionary['Meeting']['fields']['syllabus_id'] = array(
    'name' => 'syllabus_id',
    'rname' => 'id',
    'vname' => 'LBL_SYL_SYLLABUS',
    'type' => 'id',
    'table' => 'j_syllabus',
    'isnull' => 'true',
    'module' => 'J_Syllabus',
    'dbType' => 'id',
);
$dictionary['Meeting']['fields']['syllabus_link'] = array(
    'name' => 'syllabus_link',
    'type' => 'link',
    'relationship' => 'syllabus_sessions',
    'module' => 'J_Syllabus',
    'bean_name' => 'J_Syllabus',
    'source' => 'non-db',
    'vname' => 'LBL_SYL_SYLLABUS',
    'reportable' => true,
);