<?php
$dictionary['C_Timesheet']['fields']['date_start'] = array (
    'name' => 'date_start',
    'vname' => 'LBL_CALENDAR_START_DATE',
    'type' => 'datetimecombo',
    'dbType' => 'datetime',
    'comment' => 'Date of start of timesheet',
    'importable' => 'required',
    'required' => true,
    'massupdate' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom'
);

$dictionary['C_Timesheet']['fields']['date_end'] = array (
    'name' => 'date_end',
    'vname' => 'LBL_CALENDAR_END_DATE',
    'type' => 'datetimecombo',
    'dbType' => 'datetime',
    'comment' => 'Date of end of timesheet',
    'importable' => 'required',
    'required' => true,
    'massupdate' => false,
    'enable_range_search' => true,
    'options' => 'date_range_search_dom'
);

$dictionary['C_Timesheet']['fields']['duration_hours'] = array (
    'name' => 'duration_hours',
    'vname' => 'LBL_DURATION_HOURS',
    'type' => 'int',
    'comment' => 'Duration (hours)',
    'importable' => 'required',
    'required' => true,
    'massupdate' => false,
    'studio' => false,
    'processes' => true,
    'default' => 0,
    'group'=>'end_date',
    'group_label' => 'LBL_DATE_END',
);
$dictionary['C_Timesheet']['fields']['duration_minutes'] = array (
    'name' => 'duration_minutes',
    'vname' => 'LBL_DURATION_MINUTES',
    'type' => 'enum',
    'dbType' => 'int',
    'options' => 'duration_intervals',
    'group'=>'end_date',
    'group_label' => 'LBL_DATE_END',
    'len' => '2',
    'comment' => 'Duration (minutes)',
    'required' => true,
    'massupdate' => false,
    'studio' => false,
    'processes' => true,
    'default' => 0,
);
$dictionary['C_Timesheet']['fields']['meeting_type'] = array (
    'name' => 'meeting_type',
    'vname' => 'LBL_MEETING_TYPE',
    'type' => 'enum',
    'len' => 100,
    'comment' => '',
    'options' => 'admin_task_list',
    'default'    => '',
    'massupdate' => false
);

//Custom Relationship. Teacher - Timesheet (1-n)
$dictionary['C_Timesheet']['fields']['teacher_name'] = array (
    'required'  => false,
    'source'    => 'non-db',
    'name'      => 'teacher_name',
    'vname'     => 'LBL_TEACHER_NAME',
    'type'      => 'relate',
    'rname'     => 'full_teacher_name',
    'id_name'   => 'teacher_id',
    'join_name' => 'c_teachers',
    'link'      => 'c_teachers',
    'table'     => 'c_teachers',
    'isnull'    => 'true',
    'module'    => 'C_Teachers',
);
$dictionary['C_Timesheet']['fields']['teacher_id'] = array (
    'name'              => 'teacher_id',
    'rname'             => 'id',
    'vname'             => 'LBL_TEACHER',
    'type'              => 'id',
    'table'             => 'c_teachers',
    'isnull'            => 'true',
    'module'            => 'C_Teachers',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
);
$dictionary['C_Timesheet']['fields']['c_teachers_link'] = array (
    'name'          => 'c_teachers_link',
    'type'          => 'link',
    'relationship'  => 'timesheet_teacher',
    'module'        => 'C_Teachers',
    'bean_name'     => 'C_Teachers',
    'source'        => 'non-db',
    'vname'         => 'LBL_TEACHER',
);
//End custom Relationship. Teacher - Timesheet (1-n)

$dictionary['C_Timesheet']['fields']['duration_cal'] = array (
    'required' => false,
    'name' => 'duration_cal',
    'vname' => 'LBL_DURATION',
    'type' => 'decimal',
    'len' => '13',
    'precision' => '4',
    'enable_range_search' => true,
    'options' => 'numeric_range_search_dom',
);

//chú ý teacher_type chỉ để phục vụ đồng bộ pt/demo
$dictionary['C_Timesheet']['fields']['teacher_type'] = array (
    'name' => 'teacher_type',
    'vname' => 'LBL_DURATION',
    'type' => 'enum',
    'options' => 'teacher_type_list',
);
