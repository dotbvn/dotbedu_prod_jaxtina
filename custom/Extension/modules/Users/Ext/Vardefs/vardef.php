<?php
$dictionary['User']['fields']['asterisk_phone_extension'] = array (
    'name' => 'asterisk_phone_extension',
    'vname' => 'LBL_ASTERISK_PHONE_EXTENSION',
    'type' => 'varchar',
    'len'=>255
);
$dictionary['User']['fields']['asterisk_ip'] = array (
    'name' => 'asterisk_ip',
    'vname' => 'LBL_ASTERISK_IP',
    'type' => 'varchar',
    'len'=>255
);
$dictionary['User']['fields']['asterisk_chanel'] = array (
    'name' => 'asterisk_chanel',
    'vname' => 'LBL_ASTERISK_CHANEL',
    'type' => 'varchar',
    'len'=>255
);
$dictionary['User']['fields']['asterisk_context'] = array (
    'name' => 'asterisk_context',
    'vname' => 'LBL_ASTERISK_CONTEXT',
    'type' => 'varchar',
    'len'=>255
);

// Relationships
$dictionary["User"]["relationships"]["user_j_targetconfig"] = array (
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Targetconfig',
    'rhs_table' => 'j_targetconfig',
    'rhs_key' => 'tg_user_id',
    'relationship_type' => 'one-to-many'
);

//Expense
$dictionary['User']['fields']['j_budget_link'] = array(
    'name' => 'j_budget_link',
    'type' => 'link',
    'relationship' => 'users_j_budget',
    'module' => 'Users',
    'bean_name' => 'Users',
    'source' => 'non-db',
    'vname' => 'LBL_EXPENSE',
    'reportable' => false
    );

$dictionary['User']['relationships']['users_j_budget'] = array(
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Budget',
    'rhs_table' => 'j_budget',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Users',
);