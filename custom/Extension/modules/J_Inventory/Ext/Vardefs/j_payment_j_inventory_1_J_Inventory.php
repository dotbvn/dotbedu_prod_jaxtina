<?php
// created: 2019-07-03 12:11:05
$dictionary["J_Inventory"]["fields"]["j_payment_j_inventory_1"] = array (
  'name' => 'j_payment_j_inventory_1',
  'type' => 'link',
  'relationship' => 'j_payment_j_inventory_1',
  'source' => 'non-db',
  'module' => 'J_Payment',
  'bean_name' => 'J_Payment',
  'side' => 'right',
  'vname' => 'LBL_J_PAYMENT_J_INVENTORY_1_FROM_J_INVENTORY_TITLE',
  'id_name' => 'j_payment_j_inventory_1j_payment_ida',
  'link-type' => 'one',
);
$dictionary["J_Inventory"]["fields"]["j_payment_j_inventory_1_name"] = array (
  'name' => 'j_payment_j_inventory_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_J_PAYMENT_J_INVENTORY_1_FROM_J_PAYMENT_TITLE',
  'save' => true,
  'id_name' => 'j_payment_j_inventory_1j_payment_ida',
  'link' => 'j_payment_j_inventory_1',
  'table' => 'j_payment',
  'module' => 'J_Payment',
  'rname' => 'name',
);
$dictionary["J_Inventory"]["fields"]["j_payment_j_inventory_1j_payment_ida"] = array (
  'name' => 'j_payment_j_inventory_1j_payment_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_J_PAYMENT_J_INVENTORY_1_FROM_J_INVENTORY_TITLE_ID',
  'id_name' => 'j_payment_j_inventory_1j_payment_ida',
  'link' => 'j_payment_j_inventory_1',
  'table' => 'j_payment',
  'module' => 'J_Payment',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
