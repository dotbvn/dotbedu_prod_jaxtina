<?php
$dictionary["J_Inventory"]["fields"]["amount_bef_discount"] = array(
    'required' => false,
    'name' => 'amount_bef_discount',
    'vname' => 'LBL_AMOUNT_BEF_DISCOUNT',
    'type' => 'currency',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'reportable' => true,
    'merge_filter' => 'disabled',
    'default' => 0.0,
    'len' => 26,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);

$dictionary["J_Inventory"]["fields"]["import_type"] = array(
    'name' => 'import_type',
    'vname' => 'LBL_IMPORT_TYPE',
    'type' => 'enum',
    'len' => '20',
    'options' => 'inventory_import_type_list'
);
