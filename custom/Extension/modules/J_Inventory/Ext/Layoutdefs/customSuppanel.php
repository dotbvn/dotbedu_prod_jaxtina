<?php
$layout_defs["J_Inventory"]["subpanel_setup"]["inventory_inventorydetails"] = array (
    'order' => 50,
    'module' => 'J_Inventorydetail',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_INVENTORYDETAIL',
    'sort_order' => 'asc',
    'sort_by' => 'id',
    'get_subpanel_data' => 'inventorydetail_link',
    'top_buttons' =>
    array (

    ),
);
?>
