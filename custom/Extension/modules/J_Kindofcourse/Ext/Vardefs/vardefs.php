<?php
$dictionary['J_Kindofcourse']['fields']['tag']['massupdate'] = false;
$dictionary['J_Kindofcourse']['fields']['max_size'] = array (
    'required' => false,
    'name' => 'max_size',
    'vname' => 'LBL_MAX_SIZE',
    'type' => 'int',
    'len' => '10',
    'size' => '20',
    'enable_range_search' => false,
    'disable_num_format' => '',
    'min' => false,
    'max' => '10000',
);
