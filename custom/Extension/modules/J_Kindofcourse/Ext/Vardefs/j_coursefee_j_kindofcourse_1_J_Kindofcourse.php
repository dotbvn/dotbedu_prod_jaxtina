<?php
// created: 2022-03-28 15:31:32
$dictionary["J_Kindofcourse"]["fields"]["j_coursefee_j_kindofcourse_1"] = array (
  'name' => 'j_coursefee_j_kindofcourse_1',
  'type' => 'link',
  'relationship' => 'j_coursefee_j_kindofcourse_1',
  'source' => 'non-db',
  'module' => 'J_Coursefee',
  'bean_name' => 'J_Coursefee',
  'vname' => 'LBL_KINDOFCOURSE_COURSEFEE_TITLE',
  'id_name' => 'coursefee_ida',
);
