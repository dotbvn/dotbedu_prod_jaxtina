<?php
$layout_defs["J_Kindofcourse"]["subpanel_setup"]["kindofcourse_class"] = array (
    'order' => 30,
    'module' => 'J_Class',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_SUPPANEL_CLASS',
    'sort_order' => 'asc',
    'sort_by' => 'name',
    'get_subpanel_data' => 'kindofcourse_class',
    'top_buttons' =>
    array (
    ),
);
?>
