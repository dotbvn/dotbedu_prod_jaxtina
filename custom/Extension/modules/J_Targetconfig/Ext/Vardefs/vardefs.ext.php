<?php
$dictionary["J_Targetconfig"]["fields"]['type']['options'] = 'kpi_type_list';
// Relationship: User (1 - n) J_TargetConfig
$dictionary["J_Targetconfig"]["fields"]['tg_user_name'] = array(
    'importable' => 'true',
    'source'     => 'non-db',
    'name'       => 'tg_user_name',
    'vname'      => 'LBL_TG_USER_NAME',
    'type'       => 'relate',
    'rname'      => 'full_user_name',
    'id_name'    => 'tg_user_id',
    'join_name'  => 'Users',
    'link'       => 'tg_user_link',
    'table'      => 'users',
    'isnull'     => 'true',
    'module'     => 'Users',
);
$dictionary["J_Targetconfig"]["fields"]['tg_user_id'] = array(
    'reportable' => false,
    'name'   => 'tg_user_id',
    'rname'  => 'id',
    'vname'  => 'LBL_TG_USER_ID',
    'type'   => 'id',
    'table'  => 'users',
    'isnull' => 'true',
    'module' => 'Users',
    'dbType' => 'id',
);
$dictionary["J_Targetconfig"]["fields"]['tg_user_link'] = array(
    'name'          => 'tg_user_link',
    'type'          => 'link',
    'relationship'  => 'user_j_targetconfig',
    'module'        => 'Users',
    'bean_name'     => 'User',
    'source'        => 'non-db',
    'vname'         => 'LBL_TG_USER_NAME',
);

// Relationship: Team (1 - n) J_TargetConfig
$dictionary["J_Targetconfig"]["fields"]['tg_team_name'] = array(
    'importable' => 'true',
    'source'     => 'non-db',
    'name'       => 'tg_team_name',
    'vname'      => 'LBL_TG_TEAM_NAME',
    'type'       => 'relate',
    'rname'      => 'name',
    'id_name'    => 'tg_team_id',
    'join_name'  => 'Teams',
    'link'       => 'tg_team_link',
    'table'      => 'teams',
    'isnull'     => 'true',
    'module'     => 'Teams',
);
$dictionary["J_Targetconfig"]["fields"]['tg_team_id'] = array(
    'reportable' => false,
    'name'   => 'tg_team_id',
    'rname'  => 'id',
    'vname'  => 'LBL_TG_TEAM_ID',
    'type'   => 'id',
    'table'  => 'teams',
    'isnull' => 'true',
    'module' => 'Teams',
    'dbType' => 'id',
);
$dictionary["J_Targetconfig"]["fields"]['tg_team_link'] = array(
    'name'         => 'tg_team_link',
    'type'         => 'link',
    'relationship' => 'team_j_targetconfig',
    'module'       => 'Teams',
    'bean_name'    => 'Team',
    'source'       => 'non-db',
    'vname'        => 'LBL_TG_TEAM_NAME',
);