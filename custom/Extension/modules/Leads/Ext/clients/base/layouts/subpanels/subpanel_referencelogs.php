<?php
$viewdefs['Leads']['base']['layout']['subpanels']['components'][3] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_REFERENCE_LOG',
    'context' =>
    array (
        'link' => 'referencelog_link',
    ),
);

//siblings
//auto-generated file DO NOT EDIT
$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_SIBLINGS',
    'override_paneltop_view' => 'panel-top-for-sibling',
    'context' =>
        array (
            'link' => 'link_primary_siblings',
        ),
);
?>
