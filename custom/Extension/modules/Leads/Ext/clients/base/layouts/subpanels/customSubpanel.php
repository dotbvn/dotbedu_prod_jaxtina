<?php
/*$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_PAYMENT',
    'override_paneltop_view' => 'panel-top-for-leads',
    'context' =>
    array(
        'link' => 'new_payments',
    ),
);*/

$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_LEAD_SITUATION',
    'override_paneltop_view' => 'paneltop-for-class-demo',
    'override_subpanel_list_view' => 'subpanel-list-for-demo-class',
    'context' =>
    array(
        'link' => 'ju_studentsituations',
    ),
);


$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_DEMO_RESULT',
    'override_subpanel_list_view' => 'subpanel-for-demo-result',
    'override_paneltop_view' => 'panel-top-for-demo-result',
    'context' =>
        array(
            'link' => 'j_ptresults_demo',
        ),
);

$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_PT_RESULT',
    'override_subpanel_list_view' => 'subpanel-for-pt-result',
    'override_paneltop_view' => 'panel-top-for-pt-result',
    'context' =>
        array(
            'link' => 'j_ptresults_pt',
        ),
);

$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_LEAD_SMS',
    'context' =>
        array (
            'link' => 'leads_sms',
        ),
);

// Opportunities
$viewdefs['Leads']['base']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_OPPORTUNITIES',
    'context' =>
    array (
        'link' => 'opportunities_parent',
    ),
);