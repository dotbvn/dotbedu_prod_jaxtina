<?php
/**
 * Created by PhpStorm.
 * User: Tuan Anh
 * Date: 3/24/2019
 * Time: 8:18 PM
 */

$viewdefs['Leads']['base']['view']['panel-top']['buttons'] = array(
    array(
       'type' => 'actiondropdown',
            'name' => 'panel_dropdown', 'notCustomButton' => true,
        'css_class' => 'pull-right',
        'buttons' => array(
            array(
                'type' => 'link-action',
                'icon' => 'fa-link',
                'name' => 'select_button',
                'label' => ' ',
                'tooltip' => 'LBL_ASSOC_RELATED_RECORD',
            ),
        ),
    ),
);
