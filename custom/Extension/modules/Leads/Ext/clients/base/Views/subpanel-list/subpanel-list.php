<?php
/**
 * Created by PhpStorm.
 * User: Tuan Anh
 * Date: 3/24/2019
 * Time: 8:18 PM
 */

$viewdefs['Leads']['base']['view']['subpanel-list']['rowactions'] = array(
    'actions' => array(
        array(
            'type' => 'rowaction',
            'css_class' => 'btn',
            'tooltip' => 'LBL_PREVIEW',
            'event' => 'list:preview:fire',
            'icon' => 'fa-search-plus',
            'acl_action' => 'view',
        ),

    ),
);