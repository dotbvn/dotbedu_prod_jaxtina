<?php
//MKT fields
$dictionary['Lead']['fields']['reference'] = array (
    'name' => 'reference',
    'vname' => 'LBL_REFERENCE',
    'type' => 'url',
    'len' => 255,
    'importable' => true,
);
$dictionary['Lead']['fields']['utm_source'] = array(
    'name' => 'utm_source',
    'vname' => 'LBL_UTM_SOURCE',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Lead']['fields']['utm_medium'] = array(
    'name' => 'utm_medium',
    'vname' => 'LBL_UTM_MEDIUM',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Lead']['fields']['utm_content'] = array(
    'name' => 'utm_content',
    'vname' => 'LBL_UTM_CONTENT',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Lead']['fields']['utm_term'] = array(
    'name' => 'utm_term',
    'vname' => 'LBL_UTM_TERM',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Lead']['fields']['channel'] = array(
    'name' => 'channel',
    'vname' => 'LBL_CHANNEL',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'required' => false,
    'importable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 150,
    'options' => 'utm_source_list',
    'studio' => 'visible',
    'massupdate' => true,
    'dependency' => false,
);
//Add Agent user
$dictionary['Lead']['fields']['utm_agent_id'] = array(
    'name' => 'utm_agent_id',
    'vname' => 'LBL_UTM_AGENT',
    'type' => 'id',
    'required'=>false,
    'reportable'=>false,
    'comment' => ''
);

$dictionary['Lead']['fields']['utm_agent'] = array(
    'name' => 'utm_agent',
    'rname' => 'name',
    'id_name' => 'utm_agent_id',
    'vname' => 'LBL_UTM_AGENT',
    'type' => 'relate',
    'link' => 'utm_agent_link',
    'table' => 'users',
    'isnull' => 'true',
    'module' => 'Users',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable'=>true,
    'source' => 'non-db',
    'massupdate' => true,
);

$dictionary['Lead']['fields']['utm_agent_link'] = array(
    'name' => 'utm_agent_link',
    'type' => 'link',
    'relationship' => 'utm_agent_rel_l',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_UTM_AGENT',
);

//End MKT Fields
$dictionary["Lead"]["fields"]["source_description"] = array(
    'name' => 'source_description',
    'type' => 'text',
    'rows' => '1',
    'cols' => '30',
    'studio' => 'visible',
    'vname' => 'LBL_SOURCE_DESCRIPTION',
);


//Add fields School
$dictionary['Lead']['fields']['school_name'] = array (
    'required'  => false,
    'source'    => 'non-db',
    'name'      => 'school_name',
    'vname'     => 'LBL_SCHOOL_NAME',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'school_id',
    'massupdate' => false,
    'join_name' => 'j_school',
    'link'      => 'j_school_link',
    'table'     => 'j_school',
    'isnull'    => 'true',
    'module'    => 'J_School',
);

$dictionary['Lead']['fields']['school_id'] = array (
    'name'              => 'school_id',
    'rname'             => 'id',
    'vname'             => 'LBL_SCHOOL_NAME',
    'type'              => 'id',
    'table'             => 'j_school',
    'isnull'            => 'true',
    'module'            => 'J_School',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
    'importable' => false,
);

$dictionary['Lead']['fields']['j_school_link'] = array (
    'name'          => 'j_school_link',
    'type'          => 'link',
    'relationship'  => 'j_school_lead',
    'module'        => 'J_School',
    'bean_name'     => 'J_School',
    'source'        => 'non-db',
    'vname'         => 'LBL_SCHOOL_NAME',
);

$dictionary['Lead']['indices']['idx_old_student_id'] = array (
    'name' => 'idx_old_student_id',
    'type' => 'index',
    'fields' =>
    array (
        0 => 'old_student_id',
    ),
);
  $dictionary['Lead']['fields']['team_name_text'] = array(
    'name' => 'team_name_text',
    'vname' => 'Team Name (Text)',
    'type' => 'varchar',
    'len' => '255',
    'importable' => 'true',
);
?>
