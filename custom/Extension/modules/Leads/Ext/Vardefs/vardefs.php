<?php

//Custom field - By HP
$dictionary['Lead']['fields']['phone_mobile']['enableSMS'] = true;
$dictionary['Lead']['fields']['phone_work']['enableSMS'] = true;

//Custom Relationship. Leads - SMS  By HP
$dictionary['Lead']['fields']['leads_sms'] = array(
    'name' => 'leads_sms',
    'type' => 'link',
    'relationship' => 'lead_smses',
    'module' => 'C_SMS',
    'bean_name' => 'C_SMS',
    'source' => 'non-db',
    'vname' => 'LBL_LEAD_SMS',
);
$dictionary['Lead']['relationships']['lead_smses'] = array(
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'C_SMS',
    'rhs_table' => 'c_sms',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
);
// Add by Tuan Anh
$dictionary['Lead']['fields']['gender'] = array(
    'name' => 'gender',
    'required' => false,
    'vname' => 'LBL_GENDER',
    'type' => 'enum',
    'massupdate' => true,
    'len' => 20,
    'options' => 'gender_list',
    'default' => '',
    'importable' => true,
);


$dictionary['Lead']['fields']['guardian_name'] = array(
    'name' => 'guardian_name',
    'vname' => 'LBL_GUARDIAN_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);
$dictionary['Lead']['fields']['guardian_name_2'] = array(
    'name' => 'guardian_name_2',
    'vname' => 'LBL_GUARDIAN_NAME_2',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);

$dictionary['Lead']['fields']['nick_name'] = array(
    'name' => 'nick_name',
    'vname' => 'LBL_NICK_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);
$dictionary['Lead']['fields']['other_mobile'] = array(
    'name' => 'other_mobile',
    'vname' => 'LBL_OTHER_MOBILE',
    'type' => 'phone',
    'dbType' => 'varchar',
    'len' => '50',
    'required' => 'false',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);

$dictionary['Lead']['fields']['relationship'] = array(
    'name' => 'relationship',
    'vname' => 'LBL_RELATIONSHIP',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'relationship_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

// Duplicate field relationship by HP
$dictionary['Lead']['fields']['relationship2'] = array(
    'name' => 'relationship2',
    'vname' => 'LBL_RELATIONSHIP2',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'relationship_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary["Lead"]["fields"]["grade"] = array(
    'required' => false,
    'name' => 'grade',
    'vname' => 'LBL_GRADE',
    'type' => 'varchar',
    'massupdate' => 1,
    'no_default' => false,
    'importable' => true,
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'len' => '255',
    'size' => '20',
    'studio' => 'visible',
);

$dictionary["Lead"]["fields"]["phone_guardian"] = array(
    'name' => 'phone_guardian',
    'vname' => 'LBL_PHONE_GUARDIAN',
    'type' => 'phone',
    'dbType' => 'varchar',
    'len' => '100',
    'comments' => '',
    'help' => '',
    'default' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
    'merge_filter' => 'disabled',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['Lead']['fields']['prefer_level'] = array(
    'name' => 'prefer_level',
    'vname' => 'LBL_PREFER_LEVEL',
    'type' => 'multienum',
    'dbType' => 'varchar',
    'default' => '',
    'comments' => 'comment',
    'importable' => 'true',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'audited' => true,
    'reportable' => true,
    'len' => 255,
    'size' => '20',
    'options' => 'kind_of_course_list',
    'studio' => 'visible',
    'dependency' => false,
    'massupdate' => false,
    'isMultiSelect' => true,
);
$dictionary["Lead"]["fields"]["age"] = array(
    'name' => 'age',
    'type' => 'int',
    'len' => '10',
    'enable_range_search' => true,
    'options' => 'numeric_range_search_dom',
    'studio' => 'visible',
    'vname' => 'LBL_AGE',
    'readonly'  => true,
);
$dictionary['Lead']['fields']['full_lead_name'] = array(
    'name' => 'full_lead_name',
    'vname' => 'LBL_FULL_LEAD_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
);
//Add by Tuan Anh modified mass update
$dictionary['Lead']['fields']['potential'] = array(
    'name' => 'potential',
    'vname' => 'LBL_POTENTIAL',
    'type' => 'enum',
    'comments' => '',
    'help' => '',
    'default' => 'Interested',
    'massupdate' => false,
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'massupdate' => true,
    'len' => 20,
    'size' => '20',
    'options' => 'level_lead_list',
    'studio' => 'visible',
);
//Custom Relationship JUNIOR. Student - StudentSituation
$dictionary['Lead']['fields']['ju_studentsituations'] = array (
    'name' => 'ju_studentsituations',
    'type' => 'link',
    'relationship' => 'lead_studentsituations',
    'module' => 'J_StudentSituations',
    'bean_name' => 'J_StudentSituations',
    'source' => 'non-db',
    'vname' => 'LBL_LEAD_SITUATION',
);
$dictionary['Lead']['relationships']['lead_studentsituations'] = array (
    'lhs_module'        => 'Leads',
    'lhs_table'            => 'leads',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_StudentSituations',
    'rhs_table'            => 'j_studentsituations',
    'rhs_key'            => 'student_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Leads'
);
//Custom Relationship. Student - PT/Demo  By Lap Nguyen
$dictionary['Lead']['fields']['j_ptresults'] = array(
    'name' => 'j_ptresults',
    'type' => 'link',
    'relationship' => 'lead_ptresults',
    'module' => 'J_PTResult',
    'bean_name' => 'J_PTResult',
    'source' => 'non-db',
    'vname' => 'LBL_PTRESULT',
);
$dictionary['Lead']['relationships']['lead_ptresults'] = array(
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'J_PTResult',
    'rhs_table' => 'j_ptresult',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent',
    'relationship_role_column_value' => 'Leads'
);
$dictionary["Lead"]["fields"]["j_ptresults_demo"] = array(
    'name' => 'j_ptresults_demo',
    'type' => 'link',
    'link_file' => 'modules/J_PTResult/DemoLink.php',
    'link_class' => 'DemoLink',
    'relationship' => 'lead_ptresults',
    'source' => 'non-db',
    'module' => 'J_PTResult',
    'bean_name' => 'J_PTResult',
    'vname' => 'LBL_DEMO_RESULT',
    'id_name' => 'student_id',
    'link-type' => 'many',
    'side' => 'left',
);

$dictionary["Lead"]["fields"]["j_ptresults_pt"] = array(
    'name' => 'j_ptresults_pt',
    'type' => 'link',
    'link_file' => 'modules/J_PTResult/PTLink.php',
    'link_class' => 'PTLink',
    'relationship' => 'lead_ptresults',
    'source' => 'non-db',
    'module' => 'J_PTResult',
    'bean_name' => 'J_PTResult',
    'vname' => 'LBL_PT_RESULT',
    'id_name' => 'student_id',
    'link-type' => 'many',
    'side' => 'left',
);

$dictionary["Lead"]["fields"]['old_student_id'] = array(
    'name' => 'old_student_id',
    'vname' => 'LBL_OLD_STUDENT_ID',
    'type' => 'varchar',
    'len' => '150',
    'importable' => 'true',
);

$dictionary['Lead']['fields']['dp_business_purpose']['massupdate'] = false;
$dictionary['Lead']['fields']['do_not_call']['massupdate'] = false;
$dictionary['Lead']['fields']['dp_consent_last_updated']['massupdate'] = false;
$dictionary['Lead']['fields']['tag']['massupdate'] = false;
$dictionary['Lead']['fields']['school_name']['massupdate'] = true;
$dictionary['Lead']['fields']['dri_workflow_template_id']['massupdate'] = false;
$dictionary['Lead']['fields']['mkto_sync']['massupdate'] = false;
$dictionary['Lead']['fields']['preferred_language']['massupdate'] = false;
$dictionary['Lead']['fields']['converted_opp_name']['massupdate'] = false;
$dictionary['Lead']['fields']['dp_consent_last_updated']['massupdate'] = false;
$dictionary['Lead']['fields']['dp_business_purpose']['massupdate'] = false;
$dictionary['Lead']['fields']['phone_mobile']['required'] = true;

$dictionary['Lead']['fields']['first_name']['required'] = true;
$dictionary['Lead']['fields']['birthdate']['required'] = false;
$dictionary['Lead']['fields']['phone_mobile']['required'] = true;
$dictionary['Lead']['fields']['lead_source']['required'] = true;
$dictionary['Lead']['fields']['guardian_name']['audited'] = true;
$dictionary['Lead']['duplicate_check'] = array(
    'enabled' => true,
    'FilterDuplicateCheck' => array(
         'filter_template' => array(
            array(
                '$or' => array(
                    array(
                        '$and' => array(
                            array(
                                '$or' => array(
                                    array('phone_mobile' => array('$contains' => '$phone_mobile')),
                                    array('phone_mobile' => array('$contains' => '$phone_guardian')),
                                    array('phone_mobile' => array('$contains' => '$other_mobile')),

                                    array('other_mobile' => array('$contains' => '$phone_mobile')),
                                    array('other_mobile' => array('$contains' => '$phone_guardian')),
                                    array('other_mobile' => array('$contains' => '$other_mobile')),

                                    array('phone_guardian' => array('$contains' => '$other_mobile')),
                                    array('phone_guardian' => array('$contains' => '$phone_mobile')),
                                    array('phone_guardian' => array('$contains' => '$phone_guardian')),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
        'ranking_fields' => array(
            array('in_field_name' => 'full_lead_name', 'dupe_field_name' => 'full_lead_name'),
            array('in_field_name' => 'phone_mobile', 'dupe_field_name' => 'phone_mobile'),
        ),
    ),
);
$dictionary['Lead']['fields']['facebook']['type'] = 'url';
$dictionary['Lead']['fields']['guardian_name']['audited'] = true;
$dictionary['Lead']['fields']['salutation']['audited'] = false;
$dictionary['Lead']['fields']['first_name']['audited'] = false;
$dictionary['Lead']['fields']['last_name']['audited'] = false;
$dictionary['Lead']['fields']['title']['audited'] = false;
$dictionary['Lead']['fields']['twitter']['audited'] = false;
$dictionary['Lead']['fields']['office_phone']['audited'] = false;
$dictionary['Lead']['fields']['fax_phone']['audited'] = false;
$dictionary['Lead']['fields']['primary_address_street_2']['audited'] = false;
$dictionary['Lead']['fields']['primary_address_street_3']['audited'] = false;
$dictionary['Lead']['fields']['primary_address_city']['audited'] = false;
$dictionary['Lead']['fields']['primary_address_state']['audited'] = false;
$dictionary['Lead']['fields']['primary_address_postalcode']['audited'] = false;
$dictionary['Lead']['fields']['primary_address_country']['audited'] = false;
$dictionary['Lead']['fields']['alt_address_street']['audited'] = false;
$dictionary['Lead']['fields']['alt_address_street_2']['audited'] = false;
$dictionary['Lead']['fields']['alt_address_street_3']['audited'] = false;
$dictionary['Lead']['fields']['alt_address_city']['audited'] = false;
$dictionary['Lead']['fields']['alt_address_state']['audited'] = false;
$dictionary['Lead']['fields']['alt_address_postalcode']['audited'] = false;
$dictionary['Lead']['fields']['alt_address_country']['audited'] = false;
$dictionary['Lead']['fields']['dp_business_purpose']['audited'] = false;
$dictionary['Lead']['fields']['dp_consent_last_updated']['audited'] = false;
$dictionary['Lead']['fields']['assistant']['audited'] = false;
$dictionary['Lead']['fields']['full_lead_name']['audited'] = true;
$dictionary['Lead']['fields']['mkto_sync']['audited'] = false;
$dictionary['Lead']['fields']['dri_workflow_template_id']['audited'] = false;
$dictionary['Lead']['fields']['dri_workflow_template_name']['audited'] = false;
$dictionary['Lead']['fields']['dri_workflow_template_link']['audited'] = false;
$dictionary['Lead']['fields']['prefer_level']['audited'] = false;
$dictionary['Lead']['fields']['do_not_call']['audited'] = false;
$dictionary['Lead']['fields']['assistant_phone']['audited'] = false;
$dictionary['Lead']['fields']['mkto_id']['audited'] = false;
$dictionary['Lead']['fields']['mkto_lead_score']['audited'] = false;
$dictionary['Lead']['fields']['prefer_level']['audited'] = false;
$dictionary['Lead']['fields']['phone_guardian']['audited'] = false;
$dictionary['Lead']['fields']['lead_source']['audited'] = true;
$dictionary['Lead']['fields']['team_set_id']['audited'] = false;
$dictionary['Lead']['fields']['acl_team_set_id']['audited'] = false;
$dictionary['Lead']['fields']['phone_work']['audited'] = false;
$dictionary['Lead']['fields']['phone_other']['audited'] = false;
$dictionary['Lead']['fields']['phone_fax']['audited'] = false;
$dictionary['Lead']['fields']['phone_home']['audited'] = false;
$dictionary['Lead']['fields']['facebook']['audited'] = false;
$dictionary['Lead']['fields']['email']['audited'] = false;
$dictionary['Lead']['fields']['phone_guardian']['audited'] = true;

// Relationship Lead ( 1 - n ) Attendance - Dinh Tao
$dictionary['Lead']['relationships']['lead_attendances'] = array(
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Attendance',
    'rhs_table' => 'c_attendance',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    //Them 2 dong nay de tao quan he kieu related
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Leads',
);
$dictionary['Lead']['fields']['attendances_parent'] = array(
    'name' => 'attendances_parent',
    'type' => 'link',
    'relationship' => 'lead_attendances',
    'source' => 'non-db',
    'vname' => 'LBL_ATTENDANCES',
    'reportable' => false,
);
$dictionary['Lead']['fields']['class_activity'] = array (
    'name' => 'class_activity',
    'vname' => 'LBL_CLASS_ACTIVITY',
    'type' => 'html',
    'source' => 'non-db',
    'studio' => 'visible',
);
$dictionary['Lead']['fields']['career'] = array(
    'name' => 'career',
    'vname' => 'LBL_CAREER',
    'type' => 'enum',
    'dbType' => 'varchar',
    'required' => false,
    'reportable' => true,
    'importable' => true,
    'mass_update' => true,
    'len' => 200,
    'duplicate_merge' => false,
    'options' => 'careers_list',
);

// massupdate

$dictionary['Lead']['fields']['campaign_name']['massupdate'] = true;
$dictionary['Lead']['fields']['source_description']['massupdate'] = true;
$dictionary['Lead']['fields']['prefer_level']['massupdate'] = true;
$dictionary['Lead']['fields']['status']['massupdate']=true;

$dictionary['Lead']['fields']['birth_day'] = array(
    'name' => 'birth_day',
    'vname' => 'LBL_BIRTH_DAY',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' =>  'disabled',
    'len' => 50,
    'default' => '',
    'size' => '20',
    'options' => 'birth_day_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['Lead']['fields']['birth_month'] = array(
    'name' => 'birth_month',
    'vname' => 'LBL_BIRTH_MONTH',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'default' => '',
    'merge_filter' =>  'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'birth_month_list',
    'studio' => 'visible',
    'massupdate' => 0,
);
