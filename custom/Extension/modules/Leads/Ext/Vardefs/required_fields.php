<?php
 // created: 2021-09-15 13:55:46
$dictionary['Lead']['fields']['last_name']['importable']='true';
$dictionary['Lead']['fields']['phone_mobile']['importable']='required';
$dictionary['Lead']['fields']['lead_source']['importable']='true';
 // created: 2019-03-18 08:41:42
$dictionary['Lead']['fields']['lead_source']['len']=100;
$dictionary['Lead']['fields']['lead_source']['massupdate']=true;
$dictionary['Lead']['fields']['lead_source']['options']='lead_source_list';
$dictionary['Lead']['fields']['lead_source']['comments']='Lead source (ex: Web, print)';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge']='enabled';
$dictionary['Lead']['fields']['lead_source']['duplicate_merge_dom_value']='1';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';
$dictionary['Lead']['fields']['lead_source']['calculated']=false;
$dictionary['Lead']['fields']['lead_source']['dependency']=false;
VardefManager::addTemplate('Leads', 'Lead', 'customer_journey_parent', 'Lead', true);

# This flag disables population of the template id when converting the lead.
# This logic should be managed by \Leads_LogicHook_DRICustomerJourney::convertLead
$dictionary['Lead']['fields']['dri_workflow_template_id']['duplicate_on_record_copy'] = 'no';


 ?>