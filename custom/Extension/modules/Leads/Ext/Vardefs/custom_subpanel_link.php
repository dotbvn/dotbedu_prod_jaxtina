<?php
//Payments
$dictionary["Lead"]["fields"]["new_payments"] = array (
    'name' => 'new_payments',
    'type' => 'link',
    'relationship' => 'lead_payments',
    'source' => 'non-db',
    'module' => 'J_Payment',
    'bean_name' => 'J_Payment',
    'vname' => 'LBL_PAYMENT',
    'id_name' => 'parent_id',
    'link-type' => 'many',
    'side' => 'left',
    'reportable' => false,
);