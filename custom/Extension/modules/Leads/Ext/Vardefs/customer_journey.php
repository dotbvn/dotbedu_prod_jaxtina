<?php
// Add by phgiahannn
$dictionary['Lead']['fields']['cj_process_key'] = array(
    'name' => 'cj_process_key',
    'vname' => 'LBL_CJ_PROCESS_KEY',
    'type' => 'varchar',
    'len' => '100',
);

// Custom link for subpanel
$dictionary["Lead"]["fields"]["normal_tasks_link"] = array (
    'name' => 'normal_tasks_link',
    'type' => 'link',
    'relationship' => 'lead_tasks',
    'source' => 'non-db',
    'module' => 'Tasks',
    'bean_name' => 'Task',
    'vname' => 'LBL_TASKS_SUBPANEL_TITLE',
    'id_name' => 'parent_id',
    'link-type' => 'many',
    'side' => 'left',
    'link_file' => 'custom/modules/Leads/normalTaskLink.php',
    'link_class' => 'normalTaskLink',
);