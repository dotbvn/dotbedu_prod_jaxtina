<?php
//Custom Relationship  - NEW PAYMENT
$dictionary['Lead']['fields']['payment_link'] = array (
    'name' => 'payment_link',
    'type' => 'link',
    'relationship' => 'lead_payments',
    'module' => 'J_Payment',
    'bean_name' => 'J_Payment',
    'source' => 'non-db',
    'vname' => 'LBL_PAYMENT',
);
$dictionary['Lead']['relationships']['lead_payments'] = array (
    'lhs_module'        => 'Leads',
    'lhs_table'            => 'leads',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_Payment',
    'rhs_table'            => 'j_payment',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads'
);

//Custom Relationship  - ClassStudents
$dictionary['Lead']['fields']['class_leads_link'] = array (
    'name' => 'class_leads_link',
    'type' => 'link',
    'relationship' => 'class_leads',
    'module' => 'J_ClassStudents',
    'bean_name' => 'J_ClassStudents',
    'source' => 'non-db',
    'vname' => 'LBL_CLASSSTUDENTS',
);
$dictionary['Lead']['relationships']['class_leads'] = array (
    'lhs_module'        => 'Leads',
    'lhs_table'            => 'leads',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_ClassStudents',
    'rhs_table'            => 'j_classstudents',
    'rhs_key'            => 'student_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Leads'
);

//Custom Relationship  - NEW PAYMENT DETAIL
$dictionary['Lead']['fields']['paymentdetail_link'] = array (
    'name' => 'paymentdetail_link',
    'type' => 'link',
    'relationship' => 'lead_receipts',
    'module' => 'J_PaymentDetail',
    'bean_name' => 'J_PaymentDetail',
    'source' => 'non-db',
    'vname' => 'LBL_PAYMENT_DETAIL',
);
$dictionary['Lead']['relationships']['lead_receipts'] = array (
    'lhs_module'        => 'Leads',
    'lhs_table'            => 'leads',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_PaymentDetail',
    'rhs_table'            => 'j_paymentdetail',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads'
);
//Custom Relationship  - NEW BANK TRANSACTION
$dictionary['Lead']['fields']['banktrans_link'] = array (
    'name' => 'banktrans_link',
    'type' => 'link',
    'relationship' => 'lead_banktrans',
    'module' => 'J_BankTrans',
    'bean_name' => 'J_BankTrans',
    'source' => 'non-db',
    'vname' => 'LBL_BANKTRANS',
);
$dictionary['Lead']['relationships']['lead_banktrans'] = array (
    'lhs_module'        => 'Leads',
    'lhs_table'            => 'leads',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_BankTrans',
    'rhs_table'            => 'j_banktrans',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads'
);