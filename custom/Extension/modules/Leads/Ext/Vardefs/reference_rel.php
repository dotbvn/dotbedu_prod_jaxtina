<?php

$dictionary['Lead']['fields']['referencelog_link'] = array(
    'name' => 'referencelog_link',
    'type' => 'link',
    'relationship' => 'lead_referencelog',
    'module' => 'J_ReferenceLog',
    'bean_name' => 'J_ReferenceLog',
    'source' => 'non-db',
    'vname' => 'LBL_REFERENCE_LOG',
    'reportable' => false
);

$dictionary['Lead']['fields']['reference_logs'] = array(
    'name' => 'reference_logs',
    'type' => 'text',
    'source' => 'non-db',
    'vname' => 'LBL_REFERENCE_LOG',
);

$dictionary['Lead']['relationships']['lead_referencelog'] = array(
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'J_ReferenceLog',
    'rhs_table' => 'j_referencelog',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
);