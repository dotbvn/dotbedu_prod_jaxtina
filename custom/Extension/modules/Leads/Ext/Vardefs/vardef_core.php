<?php

$dictionary['Lead']['fields']['opportunities_parent'] = array(
    'name' => 'opportunities_parent',
    'type' => 'link',
    'relationship' => 'lead_opportunities',
    'source' => 'non-db',
    'vname' => 'LBL_OPPORTUNITIES',
    'reportable' => false,
);
$dictionary['Lead']['relationships']['lead_opportunities'] = array(
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'Opportunities',
    'rhs_table' => 'opportunities',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads'
);

$dictionary['Lead']['fields']['j_budget_link'] = array(
    'name' => 'j_budget_link',
    'type' => 'link',
    'relationship' => 'leads_j_budget',
    'module' => 'J_Budget',
    'bean_name' => 'J_Budget',
    'source' => 'non-db',
    'vname' => 'LBL_EXPENSE',
    'reportable' => false
    );

$dictionary['Lead']['relationships']['leads_j_budget'] = array(
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Budget',
    'rhs_table' => 'j_budget',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Leads',
);