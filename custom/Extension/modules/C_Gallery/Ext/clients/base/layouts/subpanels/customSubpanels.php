<?php
// Comments
$viewdefs['C_Gallery']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_COMMENT_LINK',
   'override_paneltop_view' => 'panel-top-for-comments_link_2',
    'context' =>
    array(
        'link' => 'comments_link',
    ),
);