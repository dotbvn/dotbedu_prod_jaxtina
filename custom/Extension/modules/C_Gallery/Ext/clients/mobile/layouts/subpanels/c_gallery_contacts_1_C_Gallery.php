<?php
// created: 2024-02-15 13:54:26
$viewdefs['C_Gallery']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_C_GALLERY_CONTACTS_1_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'c_gallery_contacts_1',
  ),
);