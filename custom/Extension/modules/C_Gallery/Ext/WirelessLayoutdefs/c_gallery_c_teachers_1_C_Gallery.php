<?php
 // created: 2024-03-01 10:13:26
$layout_defs["C_Gallery"]["subpanel_setup"]['c_gallery_c_teachers_1'] = array (
  'order' => 100,
  'module' => 'C_Teachers',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_C_GALLERY_C_TEACHERS_1_FROM_C_TEACHERS_TITLE',
  'get_subpanel_data' => 'c_gallery_c_teachers_1',
);
