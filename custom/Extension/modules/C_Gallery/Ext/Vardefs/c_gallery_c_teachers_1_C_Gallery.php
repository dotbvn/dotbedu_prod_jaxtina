<?php
// created: 2024-03-01 10:13:26
$dictionary["C_Gallery"]["fields"]["c_gallery_c_teachers_1"] = array (
  'name' => 'c_gallery_c_teachers_1',
  'type' => 'link',
  'relationship' => 'c_gallery_c_teachers_1',
  'source' => 'non-db',
  'module' => 'C_Teachers',
  'bean_name' => 'C_Teachers',
  'vname' => 'LBL_C_GALLERY_C_TEACHERS_1_FROM_C_TEACHERS_TITLE',
  'id_name' => 'c_gallery_c_teachers_1c_teachers_idb',
);
