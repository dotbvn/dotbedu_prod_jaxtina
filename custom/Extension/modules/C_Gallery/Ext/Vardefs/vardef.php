<?php
$dictionary["C_Gallery"]["fields"]["gallery_date"] = array (
    'name' => 'gallery_date',
    'vname' => 'LBL_GALLERY_DATE',
    'type' => 'datetime',
    'required' => true,
);
$dictionary["C_Gallery"]["fields"]["send_notification"] = array (
    'name' => 'send_notification',
    'vname' => 'LBL_SEND_NOTIFICATION',
    'type' => 'bool',
    "source"=>"non-db",
);
$dictionary["C_Gallery"]["fields"]["visibility_range"] = array (
    'name' => 'visibility_range',
    'vname' => 'LBL_VISIBILITY_RANGE',
    'type' => 'enum',
    'options' => 'visibility_range_list',
    'default' => 'class'
);
$dictionary["C_Gallery"]["fields"]["team_name"] = array (
    'name' => 'team_name',
    'db_concat_fields' =>
        array (
            0 => 'name',
            1 => 'name_2',
        ),
    'sort_on' => 'tj.name',
    'join_name' => 'tj',
    'rname' => 'name',
    'id_name' => 'team_id',
    'vname' => 'LBL_TEAMS',
    'type' => 'relate',
    'required' => 'true',
    'table' => 'teams',
    'isnull' => 'true',
    'module' => 'Teams',
    'link' => 'team_link',
    'massupdate' => true,
    'dbType' => 'varchar',
    'source' => 'non-db',
    'len' => 36,
    'custom_type' => 'teamset',
    'studio' =>
        array (
            'portallistview' => false,
            'portalrecordview' => false,
        ),
    'duplicate_on_record_copy' => 'always',
    'exportable' => true,
    'fields' =>
        array (
            0 => 'acl_team_set_id',
        ),
    'dependency'        => 'equal($visibility_range, "center")',
);
$dictionary['C_Gallery']['fields']['comments_link'] = array (
    'name' => 'comments_link',
    'type' => 'link',
    'relationship' => 'comments_c_gallery',
    'module' => 'C_Comments',
    'bean_name' => 'C_Comments',
    'source' => 'non-db',
    'vname' => 'LBL_COMMENT_LINK',
);

$dictionary['C_Gallery']['relationships']['comments_c_gallery'] = array (
    'lhs_module'        => 'C_Gallery',
    'lhs_table'            => 'c_gallery',
    'lhs_key'            => 'id',
    'rhs_module'        => 'C_Comments',
    'rhs_table'            => 'c_comments',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_Gallery'
);
$dictionary["C_Gallery"]["fields"]["total_like"] = array (
    'name' => 'total_like',
    'vname' => 'LBL_TOTAL_LIKE',
    'type' => 'int',
    "source"=>"non-db",
);
$dictionary["C_Gallery"]["fields"]["status"] = array (
    'name' => 'status',
    'vname' => 'LBL_STATUS',
    'type' => 'enum',
    'options' => 'gallery_status_list',
    'default' => 'approved',
);