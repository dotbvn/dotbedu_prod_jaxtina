<?php

$dependencies['Prospects']['readonly_fields'] = array(
    'hooks' => array("edit"),

    'onload' => true,
    //Actions is a list of actions to fire when the trigger is true
    // You could list multiple fields here each in their own array under 'actions'
    'actions' => array(
        array(
            'name' => 'ReadOnly',
            //The parameters passed in will depend on the action type set in 'name'
            'params' => array(
                'target' => 'status',
                'value' => 'true',
            ),
        ),
    ),
);
