<?php
//Custom Relationship  - NEW PAYMENT
$dictionary['Prospect']['fields']['link_siblings'] = array (
    'name' => 'link_siblings',
    'type' => 'link',
    'relationship' => 'siblings_prospects',
    'module' => 'C_Siblings',
    'bean_name' => 'C_Siblings',
    'source' => 'non-db',
    'vname' => 'LBL_RELATIONSHIP_TO',
);
$dictionary['Prospect']['relationships']['siblings_prospects'] = array (
    'lhs_module'        => 'Prospects',
    'lhs_table'            => 'prospects',
    'lhs_key'            => 'id',
    'rhs_module'        => 'C_Siblings',
    'rhs_table'            => 'c_siblings',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Prospects'
);


$dictionary['Prospect']['relationships']['rel_siblings_prospects'] = array(
    'lhs_module' => 'Prospects',
    'lhs_table' => 'prospects',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Siblings',
    'rhs_table' => 'c_siblings',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
);

$dictionary['Prospect']['fields']['link_primary_siblings'] = array(
    'name' => 'link_primary_siblings',
    'type' => 'link',
    'relationship' => 'rel_siblings_prospects',
    'module' => 'C_Siblings',
    'bean_name' => 'C_Siblings',
    'source' => 'non-db',
    'vname' => 'LBL_SIBLINGS',
);