<?php
  //MKT fields
$dictionary['Prospect']['fields']['reference'] = array (
    'name' => 'reference',
    'vname' => 'LBL_REFERENCE',
    'type' => 'url',
    'len' => 255,
    'importable' => true,
);
$dictionary['Prospect']['fields']['utm_source'] = array(
    'name' => 'utm_source',
    'vname' => 'LBL_UTM_SOURCE',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Prospect']['fields']['utm_medium'] = array(
    'name' => 'utm_medium',
    'vname' => 'LBL_UTM_MEDIUM',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Prospect']['fields']['utm_content'] = array(
    'name' => 'utm_content',
    'vname' => 'LBL_UTM_CONTENT',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Prospect']['fields']['utm_term'] = array(
    'name' => 'utm_term',
    'vname' => 'LBL_UTM_TERM',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Prospect']['fields']['channel'] = array(
    'name' => 'channel',
    'vname' => 'LBL_CHANNEL',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'required' => false,
    'importable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 150,
    'options' => 'utm_source_list',
    'studio' => 'visible',
    'massupdate' => true,
    'dependency' => false,
);
//Add Agent user
$dictionary['Prospect']['fields']['utm_agent_id'] = array(
    'name' => 'utm_agent_id',
    'vname' => 'LBL_UTM_AGENT',
    'type' => 'id',
    'required'=>false,
    'reportable'=>false,
    'comment' => ''
);

$dictionary['Prospect']['fields']['utm_agent'] = array(
    'name' => 'utm_agent',
    'rname' => 'name',
    'id_name' => 'utm_agent_id',
    'vname' => 'LBL_UTM_AGENT',
    'type' => 'relate',
    'link' => 'utm_agent_link',
    'table' => 'users',
    'isnull' => 'true',
    'module' => 'Users',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable'=>true,
    'source' => 'non-db',
    'massupdate' => true,
);

$dictionary['Prospect']['fields']['utm_agent_link'] = array(
    'name' => 'utm_agent_link',
    'type' => 'link',
    'relationship' => 'utm_agent_rel_p',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_UTM_AGENT',
);

//End MKT Fields

//Add fields School
$dictionary['Prospect']['fields']['school_name'] = array (
    'required'  => false,
    'source'    => 'non-db',
    'name'      => 'school_name',
    'vname'     => 'LBL_SCHOOL_NAME',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'school_id',
    'massupdate' => false,
    'join_name' => 'j_school',
    'link'      => 'j_school_link',
    'table'     => 'j_school',
    'isnull'    => 'true',
    'module'    => 'J_School',
);

$dictionary['Prospect']['fields']['school_id'] = array (
    'name'              => 'school_id',
    'rname'             => 'id',
    'vname'             => 'LBL_SCHOOL_NAME',
    'type'              => 'id',
    'table'             => 'j_school',
    'isnull'            => 'true',
    'module'            => 'J_School',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
    'importable' => false,
);

$dictionary['Prospect']['fields']['j_school_link'] = array (
    'name'          => 'j_school_link',
    'type'          => 'link',
    'relationship'  => 'j_school_prospect',
    'module'        => 'J_School',
    'bean_name'     => 'J_School',
    'source'        => 'non-db',
    'vname'         => 'LBL_SCHOOL_NAME',
);
?>
