<?php
 // created: 2019-03-18 07:06:27
$dictionary['Prospect']['fields']['phone_mobile']['len']='100';
$dictionary['Prospect']['fields']['phone_mobile']['required']=true;
$dictionary['Prospect']['fields']['phone_mobile']['massupdate']=false;
$dictionary['Prospect']['fields']['phone_mobile']['comments']='Mobile phone number of the contact';
$dictionary['Prospect']['fields']['phone_mobile']['duplicate_merge']='enabled';
$dictionary['Prospect']['fields']['phone_mobile']['duplicate_merge_dom_value']='1';
$dictionary['Prospect']['fields']['phone_mobile']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['phone_mobile']['unified_search']=false;
$dictionary['Prospect']['fields']['phone_mobile']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.88',
  'searchable' => true,
);
$dictionary['Prospect']['fields']['phone_mobile']['calculated']=false;

 ?>