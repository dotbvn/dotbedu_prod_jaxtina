<?php
$dictionary['Prospect']['fields']['eng_level'] = array(
    'name' => 'eng_level',
    'vname' => 'LBL_ENG_LEVEL',
    'type' => 'enum',
    'options' => 'lead_eng_level_list',
    'len' => 100,
    'massupdate' => true,
    'importable' => true,
);
$dictionary['Prospect']['fields']['target'] = array(
    'name' => 'target',
    'vname' => 'LBL_TARGET',
    'type' => 'enum',
    'options' => 'lead_target_list',
    'len' => 100,
    'massupdate' => true,
    'importable' => true,
);
$dictionary['Prospect']['fields']['object'] = array(
    'name' => 'object',
    'vname' => 'LBL_OBJECT',
    'type' => 'enum',
    'options' => 'lead_object_list',
    'len' => 100,
    'massupdate' => true,
    'importable' => true,
);