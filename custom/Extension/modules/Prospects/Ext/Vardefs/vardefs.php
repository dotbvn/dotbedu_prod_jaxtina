<?php
// WARNING: The contents of this file are auto-generated.
?>
<?php
// Merged from custom/Extension/modules/Teams/Ext/Vardefs/vardefs.php

$dictionary['Prospect']['fields']['lead_source'] = array(
    'name' => 'lead_source',
    'vname' => 'LBL_LEAD_SOURCE',
    'type' => 'enum',
    'options' => 'lead_source_list',
    'len' => '100',
    'massupdate' => true,
    'required' => true,
    'importable' => 'required',
);
$dictionary['Prospect']['fields']['status'] = array(
    'name' => 'status',
    'vname' => 'LBL_STATUS',
    'type' => 'enum',
    'len' => '100',
    'options' => 'target_status_dom',
    'default' => 'New',
    'audited' => true,
    'comment' => 'Status of the target',
    'massupdate' => false,
);

$dictionary['Prospect']['fields']['gender'] = array(
    'name' => 'gender',
    'required' => false,
    'vname' => 'LBL_GENDER',
    'type' => 'enum',
    'massupdate' => true,
    'len' => 20,
    'options' => 'gender_list',
    'default' => '',
    'importable' => true,
);

$dictionary['Prospect']['fields']['guardian_name'] = array(
    'name' => 'guardian_name',
    'vname' => 'LBL_GUARDIAN_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);

$dictionary['Prospect']['fields']['potential'] = array(
    'name' => 'potential',
    'vname' => 'LBL_POTENTIAL',
    'type' => 'enum',
    'comments' => '',
    'help' => '',
    'default' => 'Low',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'massupdate' => false,
    'merge_filter' => 'disabled',
    'len' => 20,
    'size' => '20',
    'options' => 'level_lead_list',
    'studio' => 'visible',
);
$dictionary["Prospect"]["fields"]["age"] = array(
    'name' => 'age',
    'type' => 'int',
    'len' => '10',
    'enable_range_search' => true,
    'options' => 'numeric_range_search_dom',
    'studio' => 'visible',
    'vname' => 'LBL_AGE',
    'readonly'  => true,
);
$dictionary['Prospect']['fields']['full_target_name'] = array(
    'name' => 'full_target_name',
    'vname' => 'LBL_FULL_PROSPECT_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
);
//Add by Tuan Anh modified mass update
$dictionary['Prospect']['fields']['dp_business_purpose']['massupdate'] = false;
$dictionary['Prospect']['fields']['do_not_call']['massupdate'] = false;
$dictionary['Prospect']['fields']['dp_consent_last_updated']['massupdate'] = false;
$dictionary['Prospect']['fields']['tag']['massupdate'] = false;
$dictionary['Prospect']['fields']['school_name']['massupdate'] = true;
$dictionary['Prospect']['fields']['status']['massupdate'] = true;
$dictionary['Prospect']['fields']['converted'] = array(
    'name' => 'converted',
    'vname' => 'LBL_CONVERTED',
    'type' => 'bool',
    'default' => '',
    'comment' => 'Has Lead been converted to a Contact (and other Dotb objects)',
    'massupdate' => true,
);

$dictionary['Prospect']['fields']['facebook']['type'] = 'url';
$dictionary["Prospect"]["fields"]["source_description"] = array(
    'name' => 'source_description',
    'type' => 'text',
    'rows' => '1',
    'cols' => '30',
    'studio' => 'visible',
    'vname' => 'LBL_SOURCE_DESCRIPTION',
);

$dictionary['Prospect']['fields']['first_name']['required'] = true;
$dictionary['Prospect']['fields']['phone_mobile']['required'] = true;
$dictionary['Prospect']['fields']['lead_source']['required'] = true;

$dictionary['Prospect']['duplicate_check'] = array(
    'enabled' => true,
    'FilterDuplicateCheck' => array(
        'filter_template' => array(
            array(
                '$and' => array(
                    array('full_target_name' => array('$equals' => '$name')),
                    array(
                        '$or' => array(
                            array('phone_mobile' => array('$contains' => '$phone_mobile')),
                            array('phone_mobile' => array('$contains' => '$phone_guardian')),
                            array('phone_mobile' => array('$contains' => '$other_mobile')),

                            array('other_mobile' => array('$contains' => '$phone_mobile')),
                            array('other_mobile' => array('$contains' => '$phone_guardian')),
                            array('other_mobile' => array('$contains' => '$other_mobile')),

                            array('phone_guardian' => array('$contains' => '$other_mobile')),
                            array('phone_guardian' => array('$contains' => '$phone_mobile')),
                            array('phone_guardian' => array('$contains' => '$phone_guardian')),
                        ),
                    ),
                )
            ),
        ),
        'ranking_fields' => array(
            array('in_field_name' => 'full_target_name', 'dupe_field_name' => 'full_target_name'),
            array('in_field_name' => 'phone_mobile', 'dupe_field_name' => 'phone_mobile'),

        ),
    ),
);
$dictionary['Prospect']['fields']['guardian_name']['audited'] = true;
$dictionary['Prospect']['fields']['salutation']['audited'] = false;
$dictionary['Prospect']['fields']['first_name']['audited'] = false;
$dictionary['Prospect']['fields']['last_name']['audited'] = false;
$dictionary['Prospect']['fields']['title']['audited'] = false;
$dictionary['Prospect']['fields']['twitter']['audited'] = false;
$dictionary['Prospect']['fields']['office_phone']['audited'] = false;
$dictionary['Prospect']['fields']['fax_phone']['audited'] = false;
$dictionary['Prospect']['fields']['primary_address_street_2']['audited'] = false;
$dictionary['Prospect']['fields']['primary_address_street_3']['audited'] = false;
$dictionary['Prospect']['fields']['primary_address_city']['audited'] = false;
$dictionary['Prospect']['fields']['primary_address_state']['audited'] = false;
$dictionary['Prospect']['fields']['primary_address_postalcode']['audited'] = false;
$dictionary['Prospect']['fields']['primary_address_country']['audited'] = false;
$dictionary['Prospect']['fields']['alt_address_street']['audited'] = false;
$dictionary['Prospect']['fields']['alt_address_street_2']['audited'] = false;
$dictionary['Prospect']['fields']['alt_address_street_3']['audited'] = false;
$dictionary['Prospect']['fields']['alt_address_city']['audited'] = false;
$dictionary['Prospect']['fields']['alt_address_state']['audited'] = false;
$dictionary['Prospect']['fields']['alt_address_postalcode']['audited'] = false;
$dictionary['Prospect']['fields']['alt_address_country']['audited'] = false;
$dictionary['Prospect']['fields']['dp_business_purpose']['audited'] = false;
$dictionary['Prospect']['fields']['dp_consent_last_updated']['audited'] = false;
$dictionary['Prospect']['fields']['assistant']['audited'] = false;
$dictionary['Prospect']['fields']['full_target_name']['audited'] = true;
$dictionary['Prospect']['fields']['lead_source']['audited'] = true;
$dictionary['Prospect']['fields']['team_set_id']['audited'] = false;
$dictionary['Prospect']['fields']['acl_team_set_id']['audited'] = false;
$dictionary['Prospect']['fields']['phone_work']['audited'] = false;
$dictionary['Prospect']['fields']['phone_other']['audited'] = false;
$dictionary['Prospect']['fields']['phone_fax']['audited'] = false;
$dictionary['Prospect']['fields']['phone_home']['audited'] = false;
$dictionary['Prospect']['fields']['facebook']['audited'] = false;
$dictionary['Prospect']['fields']['email']['audited'] = false;



$dictionary['Prospect']['fields']['guardian_name_2'] = array(
    'name' => 'guardian_name_2',
    'vname' => 'LBL_GUARDIAN_NAME_2',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);

$dictionary['Prospect']['fields']['relationship'] = array(
    'name' => 'relationship',
    'vname' => 'LBL_RELATIONSHIP',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'relationship_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['Prospect']['fields']['relationship2'] = array(
    'name' => 'relationship2',
    'vname' => 'LBL_RELATIONSHIP2',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'relationship_list',
    'studio' => 'visible',
    'massupdate' => 0,
);
$dictionary['Prospect']['fields']['other_mobile'] = array(
    'name' => 'other_mobile',
    'vname' => 'LBL_OTHER_MOBILE',
    'type' => 'phone',
    'dbType' => 'varchar',
    'len' => '50',
    'required' => 'false',
    'audited' => false,
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);
$dictionary["Prospect"]["fields"]["phone_guardian"] = array(
    'name' => 'phone_guardian',
    'vname' => 'LBL_PHONE_GUARDIAN',
    'type' => 'phone',
    'dbType' => 'varchar',
    'len' => '100',
    'comments' => '',
    'help' => '',
    'default' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
    'merge_filter' => 'disabled',
    'studio' => 'visible',
    'massupdate' => 0,
);
$dictionary['Prospect']['fields']['nick_name'] = array(
    'name' => 'nick_name',
    'vname' => 'LBL_NICK_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => ''
);

$dictionary["Prospect"]["fields"]['old_student_id'] = array(
    'name' => 'old_student_id',
    'vname' => 'LBL_OLD_STUDENT_ID',
    'type' => 'varchar',
    'len' => '150',
    'importable' => 'true',
);

// massupdate
$dictionary['Prospect']['fields']['campaign_name']['massupdate'] = true;
$dictionary['Prospect']['fields']['source_description']['massupdate'] = true;
$dictionary['Prospect']['fields']['prefer_level']['massupdate'] = true;
$dictionary['Prospect']['fields']['grade']['massupdate'] = true;
$dictionary['Prospect']['fields']['status']['massupdate']=true;

$dictionary['Prospect']['fields']['birth_day'] = array(
    'name' => 'birth_day',
    'vname' => 'LBL_BIRTH_DAY',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' =>  'disabled',
    'len' => 50,
    'default' => '',
    'size' => '20',
    'options' => 'birth_day_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['Prospect']['fields']['birth_month'] = array(
    'name' => 'birth_month',
    'vname' => 'LBL_BIRTH_MONTH',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'default' => '',
    'merge_filter' =>  'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'birth_month_list',
    'studio' => 'visible',
    'massupdate' => 0,
);
