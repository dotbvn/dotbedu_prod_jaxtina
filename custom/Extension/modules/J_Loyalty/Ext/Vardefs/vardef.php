<?php
$dictionary['J_Loyalty']['fields']['meeting_id'] = array(
    'name'              => 'meeting_id',
    'rname'             => 'id',
    'vname'             => 'LBL_METTING_NAME',
    'type'              => 'id',
    'table'             => 'meetings',
    'isnull'            => 'true',
    'module'            => 'Meetings',
    'dbType'            => 'id',
    'reportable'        => false,
    'importable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
);

$dictionary['J_Loyalty']['fields']['meeting_name'] = array(
    'required'  => false,
    'source'    => 'non-db',
    'name'      => 'meeting_name',
    'vname'     => 'LBL_MEETING_NAME',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'meeting_id',
    'join_name' => 'meetings',
    'link'      => 'meetings_loyalty_link',
    'table'     => 'meetings',
    'isnull'    => 'true',
    'importable'        => false,
    'module'    => 'Meetings',
);

$dictionary['J_Loyalty']['fields']['name']['importable'] = false;

$dictionary['J_Loyalty']['fields']['meetings_loyalty_link'] = array(
    'name'          => 'meetings_loyalty_link',
    'type'          => 'link',
    'relationship'  => 'meetings_loyalty_link',
    'module'        => 'Meetings',
    'bean_name'     => 'Meetings',
    'source'        => 'non-db',
    'vname'         => 'LBL_MEETING_NAME',
);