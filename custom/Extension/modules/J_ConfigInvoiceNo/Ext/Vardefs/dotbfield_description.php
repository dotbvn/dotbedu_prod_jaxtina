<?php
 // created: 2022-06-18 01:16:07
$dictionary['J_ConfigInvoiceNo']['fields']['description']['audited']=false;
$dictionary['J_ConfigInvoiceNo']['fields']['description']['massupdate']=false;
$dictionary['J_ConfigInvoiceNo']['fields']['description']['comments']='Full text of the note';
$dictionary['J_ConfigInvoiceNo']['fields']['description']['duplicate_merge']='enabled';
$dictionary['J_ConfigInvoiceNo']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['J_ConfigInvoiceNo']['fields']['description']['merge_filter']='disabled';
$dictionary['J_ConfigInvoiceNo']['fields']['description']['unified_search']=false;
$dictionary['J_ConfigInvoiceNo']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.5',
  'searchable' => true,
);
$dictionary['J_ConfigInvoiceNo']['fields']['description']['calculated']=false;
$dictionary['J_ConfigInvoiceNo']['fields']['description']['rows']='3';
$dictionary['J_ConfigInvoiceNo']['fields']['description']['cols']='60';

 ?>