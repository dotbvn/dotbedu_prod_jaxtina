<?php

$dictionary["C_UploadImage"]["fields"]["comment_id"] = array(
    'name' => 'comment_id',
    'vname' => 'LBL_COMMENT_ID',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
);

$dictionary["C_UploadImage"]["fields"]["comment_name"] = array(
    'name' => 'comment_name',
    'rname' => 'name',
    'id_name' => 'comment_id',
    'vname' => 'LBL_COMMENT_NAME',
    'type' => 'relate',
    'link' => 'comment_link',
    'table' => 'c_comments',
    'isnull' => 'true',
    'module' => 'C_Comments',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable'=>true,
    'source' => 'non-db',
);

$dictionary["C_UploadImage"]["fields"]["comment_link"] = array(
    'name' => 'comment_link',
    'type' => 'link',
    'relationship' => 'comment_images',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_COMMENT_NAME',
);