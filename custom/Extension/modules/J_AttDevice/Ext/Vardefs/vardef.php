<?php
$dictionary["J_AttDevice"]["fields"]["device_id"] = array(
    'required' => true,
    'name' => 'device_id',
    'vname' => 'LBL_DEVICE_ID',
    'type' => 'varchar',
    'len' => '30',
);
$dictionary["J_AttDevice"]["fields"]["secret_key"] = array(
    'required' => true,
    'name' => 'secret_key',
    'vname' => 'LBL_SECRET_KEY',
    'type' => 'varchar',
    'len' => '50',
);

$dictionary["J_AttDevice"]["fields"]["device_type"] = array(
    'required' => true,
    'name' => 'device_type',
    'vname' => 'LBL_DEVICE_TYPE',
    'type' => 'enum',
    'options' => 'device_att_type',
);
$dictionary["J_AttDevice"]["fields"]["synced_number_students"] = array(
    'name' => 'synced_number_students',
    'vname' => 'LBL_SYNCED_NUMBER_STUDENTS',
    'type' => 'int',
    'len' => 20,
    'default' => 0
);
$dictionary["J_AttDevice"]["fields"]["synced_number_students_display"] = array(
    'name' => 'synced_number_students_display',
    'vname' => 'LBL_SYNCED_NUMBER_STUDENTS_DISPLAY',
    'type' => 'varchar',
    'source' => 'non-db',
    'duplicate_on_record_copy' => 'no',
);
$dictionary["J_AttDevice"]["fields"]["device_volume"] = array(
    'name' => 'device_volume',
    'vname' => 'LBL_DEVICE_VOLUME',
    'type' => 'int',
    'size' => 20,
    'no_default' => false,
    'default' => 100,
    'validation' =>
        array (
            'type' => 'range',
            'min' => '0',
            'max' => '100',
        ),
    'help' => 'LBL_HELP_VOULUME_ATT_DEVICE'
);

//Dua center thanh enum de chon 1

//Add relationship with J_CheckInOut
$dictionary['J_AttDevice']['relationships']['attdevice_checkinout'] = array(
    'lhs_module' => 'J_AttDevice',
    'lhs_table' => 'j_attdevice',
    'lhs_key' => 'id',
    'rhs_module' => 'J_CheckInOut',
    'rhs_table' => 'j_checkinout',
    'rhs_key' => 'device_id',
    'relationship_type' => 'one-to-many'
);
$dictionary['J_AttDevice']['fields']['team_id'] = array(
    'name'              => 'team_id',
    'rname'             => 'id',
    'vname'             => 'LBL_TEAM',
    'type' => 'enum',
    'dbType' => 'id',
    'massupdate' => true,
    'function' => 'getTeamList',
    'table'             => 'teams',
    'isnull'            => 'true',
    'module'            => 'Teams',
    'reportable'        => false,
);