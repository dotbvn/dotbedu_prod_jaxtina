<?php
$dependencies['Cases']['set_specified_type_options'] = array(
    'hooks' => array("all"),
    'trigger' => 'true',
    'triggerFields' => array('type'),
    'onload' => true,
    'actions' => array(
        array(
            'name' => 'SetOptions',
            'params' => array(
                'target' => 'specified_type',
                'keys' => 'getDropdownKeySet(getDropdownValue("case_type_options", $type))',
                'labels' => 'getDropdownValueSet(getDropdownValue("case_type_options", $type))'
            ),
        ),
    ),
);