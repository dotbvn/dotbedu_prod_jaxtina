<?php

$panel = array(
    'layout' => 'communication',
    'label' => 'LBL_COMMUNICATION',
);

if (isset($viewdefs['Cases']['base']['layout']['extra-info'])) {
    $viewdefs['Cases']['base']['layout']['extra-info']['components'][] = $panel;
} else {
    $viewdefs['Cases']['base']['layout']['extra-info'] = array(
        'components' => array($panel),
        'type' => 'simple',
        'span' => 12,
    );
}
