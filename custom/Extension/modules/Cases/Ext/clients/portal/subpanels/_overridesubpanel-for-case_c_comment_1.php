<?php
//auto-generated file DO NOT EDIT
$viewdefs['Case']['portal']['layout']['subpanels']['components'][] = array(
        'override_paneltop_view' => 'panel-top-for-comments_link',
        'layout' => 'subpanel',
        'label' => 'LBL_COMMENT_LINK',
        'context' => array(
                'link' => 'comments_link',
            ),
        'override_subpanel_list_view' => array (
            'link' => 'comments_link',
            'view' => 'subpanel-for-cases-comments_link',
        ),
    );
