<?php
//Custom Relationship  - NEW PAYMENT DETAIL
$dictionary['Case']['fields']['comments_link'] = array (
    'name' => 'comments_link',
    'type' => 'link',
    'relationship' => 'comments_cases',
    'module' => 'C_Comments',
    'bean_name' => 'C_Comments',
    'source' => 'non-db',
    'vname' => 'LBL_COMMENT_LINK',
);

$dictionary['Case']['relationships']['comments_cases'] = array (
    'lhs_module'        => 'Cases',
    'lhs_table'            => 'cases',
    'lhs_key'            => 'id',
    'rhs_module'        => 'C_Comments',
    'rhs_table'            => 'c_comments',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Cases'
);