<?php
$dictionary['Case']['fields']['absent_reason'] = array (
    'name' => 'absent_reason',
    'vname' => 'LBL_ABSENT_REASON',
    'type' => 'enum',
    'options' => 'case_absent_reason_list',
    'len' => 100,
    'audited' => true,
    'massupdate' => true,
);

