<?php

$dictionary['Case']['fields']['deadline'] = array (
    'name' => 'deadline',
    'vname' => 'LBL_DEADLINE',
    'type' => 'datetime',
);

$dictionary['Case']['fields']['j_class_cases_1_name']['required'] = true;
$dictionary['Case']['fields']['type']['required'] = true;

$dictionary['Case']['fields']['teacher_id'] = array (
    'name' => 'teacher_id',
    'vname' => 'LBL_MAIN_TEACHER_NAME',
    'type' => 'id',
    'reportable' => true,
    'studio' => 'visible',
);

$dictionary['Case']['fields']['teacher_name'] = array (
    'name' => 'teacher_name',
    'vname' => 'LBL_MAIN_TEACHER_NAME',
    'type' => 'relate',
    'required'=>true,
    'source' => 'non-db',
    'rname' => 'full_teacher_name',
    'id_name' => 'teacher_id',
    'join_name' => 'c_teachers',
    'link' => 'c_teachers',
    'table' => 'c_teachers',
    'isnull' => 'true',
    'module' => 'C_Teachers',
);

$dictionary['Case']['fields']['c_teachers'] = array (
    'name' => 'c_teachers',
    'vname' => 'LBL_MAIN_TEACHER_NAME',
    'type' => 'link',
    'relationship' => 'teacher_cases',
    'module' => 'C_Teachers',
    'bean_name' => 'C_Teachers',
    'source' => 'non-db',
);

$dictionary['Case']['fields']['specified_type'] = array (
    'name' => 'specified_type',
    'vname' => 'LBL_SPECIFIED_TYPE',
    'required' => true,
    'type' => 'enum',
    'options'=>'case_specified_type_options',
    'dependency' => 'isInList($type, createList("Teacher", "Request/Complaint"))',
);


