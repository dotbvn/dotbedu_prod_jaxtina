<?php
 // created: 2023-08-31 14:48:05
$dictionary['Case']['fields']['description']['audited']=false;
$dictionary['Case']['fields']['description']['massupdate']=false;
$dictionary['Case']['fields']['description']['comments']='Full text of the note';
$dictionary['Case']['fields']['description']['duplicate_merge']='enabled';
$dictionary['Case']['fields']['description']['duplicate_merge_dom_value']='1';
$dictionary['Case']['fields']['description']['merge_filter']='disabled';
$dictionary['Case']['fields']['description']['full_text_search']=array (
  'enabled' => true,
  'boost' => '0.66',
  'searchable' => true,
);
$dictionary['Case']['fields']['description']['calculated']=false;
$dictionary['Case']['fields']['description']['rows']='6';
$dictionary['Case']['fields']['description']['cols']='20';
$dictionary['Case']['fields']['description']['required']=true;

 ?>