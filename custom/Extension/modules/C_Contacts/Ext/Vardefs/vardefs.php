<?php

$dictionary["C_Contacts"]["fields"]["birthdate"] = array (
    'name' => 'birthdate',
    'type' => 'date',
    'vname' => 'LBL_BIRTHDATE',
    'massupdate' => 0,
    'no_default' => false,
    'comments' => '',
    'help' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'size' => '20',
    'enable_range_search' => false,
);

$dictionary['C_Contacts']['fields']['birth_day'] = array(
    'name' => 'birth_day',
    'vname' => 'LBL_BIRTH_DAY',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' =>  'disabled',
    'len' => 50,
    'default' => '',
    'size' => '20',
    'options' => 'birth_day_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['C_Contacts']['fields']['birth_month'] = array(
    'name' => 'birth_month',
    'vname' => 'LBL_BIRTH_MONTH',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'default' => '',
    'merge_filter' =>  'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'birth_month_list',
    'studio' => 'visible',
    'massupdate' => 0,
);
