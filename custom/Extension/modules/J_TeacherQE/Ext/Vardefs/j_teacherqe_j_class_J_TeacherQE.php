<?php
// created: 2020-09-29 10:36:07
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_j_class"] = array (
  'name' => 'j_teacherqe_j_class',
  'type' => 'link',
  'relationship' => 'j_teacherqe_j_class',
  'source' => 'non-db',
  'module' => 'J_Class',
  'bean_name' => 'J_Class',
  'side' => 'right',
  'vname' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_TEACHERQE_TITLE',
  'id_name' => 'j_teacherqe_j_classj_class_ida',
  'link-type' => 'one',
);
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_j_class_name"] = array (
  'name' => 'j_teacherqe_j_class_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_CLASS_TITLE',
  'save' => true,
  'id_name' => 'j_teacherqe_j_classj_class_ida',
  'link' => 'j_teacherqe_j_class',
  'table' => 'j_class',
  'module' => 'J_Class',
  'rname' => 'name',
);
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_j_classj_class_ida"] = array (
  'name' => 'j_teacherqe_j_classj_class_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_TEACHERQE_TITLE_ID',
  'id_name' => 'j_teacherqe_j_classj_class_ida',
  'link' => 'j_teacherqe_j_class',
  'table' => 'j_class',
  'module' => 'J_Class',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
