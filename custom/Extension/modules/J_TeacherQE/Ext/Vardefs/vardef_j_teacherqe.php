<?php

$dictionary['J_TeacherQE']['fields']['j_teacherqe_type'] = array (
    'name' => 'j_teacherqe_type',
    'vname' => 'LBL_J_TEACHERQE_TYPE',
    'required' => true,
    'type' => 'enum',
    'options'=>'option_j_teacherqe_type'
);