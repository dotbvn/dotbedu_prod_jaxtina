<?php
$dictionary['J_TeacherQE']['fields']['times'] =array(
    'name' => 'times',
    'type' => 'int',
    'len' => '10',
    'vname' => 'LBL_TIMES',
);
$dictionary['J_TeacherQE']['fields']['class_observation_date'] =array(
    'name' => 'class_observation_date',
    'type' => 'date',
    'vname' => 'LBL_CLASS_OBSERVATION_DATE',
    'display_default' => 'now',
);
$dictionary['J_TeacherQE']['fields']['student_comment'] =array(
    'name' => 'student_comment',
    'type' => 'text',
    'vname' => 'LBL_STUDENT_COMMENT',

);
$dictionary['J_TeacherQE']['fields']['teacher_comment'] =array(
    'name' => 'teacher_comment',
    'type' => 'text',
    'vname' => 'LBL_TEACHER_COMMENT',

);
$dictionary['J_TeacherQE']['fields']['teacher_mark'] =array(
    'name' => 'teacher_mark',
    'type' => 'decimal',
    'len' => '10',
    'precision' => '2',
    'vname' => 'LBL_TEACHER_MARK',
);
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_c_teachers_name"]['required'] = true;
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_j_class_name"]['required'] = true;

