<?php
// created: 2020-09-29 10:36:07
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_c_teachers"] = array (
  'name' => 'j_teacherqe_c_teachers',
  'type' => 'link',
  'relationship' => 'j_teacherqe_c_teachers',
  'source' => 'non-db',
  'module' => 'C_Teachers',
  'bean_name' => 'C_Teachers',
  'side' => 'right',
  'vname' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_J_TEACHERQE_TITLE',
  'id_name' => 'j_teacherqe_c_teachersc_teachers_ida',
  'link-type' => 'one',
);
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_c_teachers_name"] = array (
  'name' => 'j_teacherqe_c_teachers_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_C_TEACHERS_TITLE',
  'save' => true,
  'id_name' => 'j_teacherqe_c_teachersc_teachers_ida',
  'link' => 'j_teacherqe_c_teachers',
  'table' => 'c_teachers',
  'module' => 'C_Teachers',
  'rname' => 'full_name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["J_TeacherQE"]["fields"]["j_teacherqe_c_teachersc_teachers_ida"] = array (
  'name' => 'j_teacherqe_c_teachersc_teachers_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_J_TEACHERQE_TITLE_ID',
  'id_name' => 'j_teacherqe_c_teachersc_teachers_ida',
  'link' => 'j_teacherqe_c_teachers',
  'table' => 'c_teachers',
  'module' => 'C_Teachers',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);
