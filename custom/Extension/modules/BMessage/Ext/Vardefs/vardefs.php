<?php
$dictionary['BMessage']['fields']['is_customer_journey_activity'] = array(
    'name' => 'is_customer_journey_activity',
    'vname' => 'LBL_IS_CUSTOMER_JOURNEY_ACTIVITY',
    'type' => 'bool',
    'default' => '0',
    'processes' => true,
);
$dictionary['BMessage']['fields']['date_start'] = array(
    'name' => 'date_start',
    'vname' => 'LBL_START_DATE',
    'type' => 'datetimecombo',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'size' => '20',
    'enable_range_search' => false,
    'dbType' => 'datetime',
    'processes' => true,
);
$dictionary['BMessage']['fields']['customer_journey_type'] = array(
    'name' => 'customer_journey_type',
    'vname' => 'LBL_CUSTOMER_JOURNEY_TYPE',
    'type' => 'enum',
    'options' => 'dri_workflow_task_templates_type_list',
    'default' => 'customer_task',
    'processes' => true,
);