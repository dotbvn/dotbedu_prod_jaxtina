<?php
$layout_defs['Contacts']['subpanel_setup']['meetings']['override_subpanel_name'] = 'Contact_subpanel_meetings';
$layout_defs['Contacts']['subpanel_setup']['loyalty_link']['override_subpanel_name'] = 'Contact_subpanel_loyalty_link';
$layout_defs['Contacts']['subpanel_setup']['leads']['override_subpanel_name'] = 'Contact_subpanel_leads';
$layout_defs['Contacts']['subpanel_setup']['ju_studentsituations']['override_subpanel_name'] = 'Contact_subpanel_ju_studentsituations';
$layout_defs['Contacts']['subpanel_setup']['contacts_situations_enrollment']['override_subpanel_name'] = 'Contact_subpanel_contacts_situations_enrollment';
$layout_defs['Contacts']['subpanel_setup']['contacts_situations_delay']['override_subpanel_name'] = 'Contact_subpanel_contacts_situations_delay';
$layout_defs['Contacts']['subpanel_setup']['contacts_cases_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_cases_1';
$layout_defs['Contacts']['subpanel_setup']['calls']['override_subpanel_name'] = 'Contact_subpanel_calls';
$layout_defs['Contacts']['subpanel_setup']['attendances_parent']['override_subpanel_name'] = 'Contact_subpanel_attendances_parent';
$layout_defs['Contacts']['subpanel_setup']['all_tasks']['override_subpanel_name'] = 'Contact_subpanel_all_tasks';
$layout_defs['Contacts']['subpanel_setup']['new_payments']['override_subpanel_name'] = 'Contact_subpanel_new_payments';
$layout_defs['Contacts']['subpanel_setup']['bookgift_link']['override_subpanel_name'] = 'Contact_subpanel_bookgift_link';
$layout_defs['Contacts']['subpanel_setup']['j_budget']['override_subpanel_name'] = 'Contact_subpanel_j_budget';

 // created: 2019-03-20 17:09:12
$layout_defs["Contacts"]["subpanel_setup"]['contacts_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'contacts_cases_1',
  'top_buttons' =>
  array (
    0 =>
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
$layout_defs["Contacts"]["subpanel_setup"]['c_news_contacts_1'] = array (
  'order' => 100,
  'module' => 'C_News',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_C_NEWS_CONTACTS_1_FROM_C_NEWS_TITLE',
  'get_subpanel_data' => 'c_news_contacts_1',
  'top_buttons' =>
  array (
    0 =>
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 =>
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
