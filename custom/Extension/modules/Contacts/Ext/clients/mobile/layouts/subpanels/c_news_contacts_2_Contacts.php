<?php
// created: 2024-02-15 13:56:27
$viewdefs['Contacts']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_C_NEWS_CONTACTS_2_FROM_C_NEWS_TITLE',
  'context' => 
  array (
    'link' => 'c_news_contacts_2',
  ),
);