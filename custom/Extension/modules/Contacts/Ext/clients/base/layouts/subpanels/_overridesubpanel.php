<?php
//Task
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'all_tasks',
  'view' => 'subpanel-for-contacts-all_tasks',
);


//Meeting
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'meetings',
  'view' => 'subpanel-for-contacts-meetings',
);

//Loyaty
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'loyalty_link',
  'view' => 'subpanel-for-contacts-loyalty_link',
);

//Lead
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'leads',
  'view' => 'subpanel-for-contacts-leads',
);

//Situation
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'ju_studentsituations',
  'view' => 'subpanel-for-contacts-ju_studentsituations',
);

//Enrollment
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'contacts_situations_enrollment',
  'view' => 'subpanel-for-contacts-contacts_situations_enrollment',
);

//Delay
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'contacts_situations_delay',
  'view' => 'subpanel-for-contacts-contacts_situations_delay',
);



//Payment: Tuition fee
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'new_payments',
  'view' => 'subpanel-for-contacts-new_payments',
);

//Payment: Book
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'bookgift_link',
  'view' => 'subpanel-for-contacts-bookgift_link',
);

////Expense
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'j_budget',
  'view' => 'subpanel-for-contacts-j_budget',
);

//Case
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'contacts_cases_1',
  'view' => 'subpanel-for-contacts-contacts_cases_1',
);

//Call
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'calls',
  'view' => 'subpanel-for-contacts-calls',
);

//Attendance
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][]['override_subpanel_list_view'] = array (
  'link' => 'attendances_parent',
  'view' => 'subpanel-for-contacts-attendances_parent',
);