<?php
//Case (Feedback)
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_CASES_SUBPANEL_TITLE',
    'context' =>
    array (
        'link' => 'contacts_cases_1',
    ),
);

//Payment: Tuition Fee
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_PAYMENT_TUITION_FEE',
    'override_paneltop_view' => 'panel-top-for-students_payment',
    'context' =>
    array(
        'link' => 'new_payments',
    ),
);

//Payment: Book/Gift
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_PAYMENT_BOOKGIFT',
    'override_paneltop_view' => 'panel-top-for-students_book',
    'context' =>
    array(
        'link' => 'bookgift_link',
    ),
);

// Enrollment
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_ENROLLMENT_TITLE',
    'override_paneltop_view' => 'paneltop-for-class-demo',
    'context' =>
    array(
        'link' => 'contacts_situations_enrollment',
    ),
);

//Delay
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_DELAY_TITLE',
    'context' =>
    array(
        'link' => 'contacts_situations_delay',
    ),
);

//Expense
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_EXPENSE',
    'override_paneltop_view' => 'panel-top-for-expense',
    'context' =>
    array(
        'link' => 'j_budget',
    ),
);

//Attendance
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_ATTENDANCES',
    'context' =>
    array(
        'link' => 'attendances_parent',
    ),
);

//SMS
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_STUDENT_SMS',
    'context' =>
    array (
        'link' => 'contacts_sms',
    ),
);

// Demo
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_DEMO_RESULT',
    'override_subpanel_list_view' => 'subpanel-for-demo-result',
    'override_paneltop_view' => 'panel-top-for-demo-result',
    'context' =>
    array(
        'link' => 'j_ptresults_demo',
    ),
);
//PT
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_PT_RESULT',
    'override_subpanel_list_view' => 'subpanel-for-pt-result',
    'override_paneltop_view' => 'panel-top-for-pt-result',
    'context' =>
    array(
        'link' => 'j_ptresults_pt',
    ),
);

////Sitation
//$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
//    'layout' => 'subpanel',
//    'label' => 'LBL_STUDENT_SITUATION',
//    'override_paneltop_view' => 'paneltop-for-class-demo',
//    'override_subpanel_list_view' => 'subpanel-list-for-demo-class',
//    'context' =>
//    array(
//        'link' => 'ju_studentsituations',
//    ),
//);
//
//

//Add Subpanel Loyalty
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array(
    'layout' => 'subpanel',
    'label' => 'LBL_LOYALTY',
    'override_paneltop_view' => 'panel-top-for-loyalty_link',
    'context' =>
    array(
        'link' => 'loyalty_link',
    ),
);


//News
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_C_NEWS_CONTACTS_1_FROM_C_NEWS_TITLE',
    'context' =>
    array (
        'link' => 'c_news_contacts_1',
    ),
);

// Opportunities
$viewdefs['Contacts']['base']['layout']['subpanels']['components'][] = array (
    'layout' => 'subpanel',
    'label' => 'LBL_OPPORTUNITIES',
    'context' =>
    array (
      'link' => 'opportunities_parent',
    ),
);
