<?php
 // created: 2024-02-15 13:56:27
$layout_defs["Contacts"]["subpanel_setup"]['c_news_contacts_2'] = array (
  'order' => 100,
  'module' => 'C_News',
  'subpanel_name' => 'default',
  'title_key' => 'LBL_C_NEWS_CONTACTS_2_FROM_C_NEWS_TITLE',
  'get_subpanel_data' => 'c_news_contacts_2',
);
