<?php
$dictionary['Contact']['fields']['eng_level'] = array(
    'name' => 'eng_level',
    'vname' => 'LBL_ENG_LEVEL',
    'type' => 'enum',
    'options' => 'lead_eng_level_list',
    'len' => 100,
    'merge_filter' => 'enabled',
    'importable' => 'required',
);
$dictionary['Contact']['fields']['target'] = array(
    'name' => 'target',
    'vname' => 'LBL_TARGET',
    'type' => 'enum',
    'options' => 'lead_target_list',
    'len' => 100,
    'merge_filter' => 'enabled',
    'importable' => 'required',
);
$dictionary['Contact']['fields']['object'] = array(
    'name' => 'object',
    'vname' => 'LBL_OBJECT',
    'type' => 'enum',
    'options' => 'lead_object_list',
    'len' => 100,
    'merge_filter' => 'enabled',
    'importable' => 'required',
);