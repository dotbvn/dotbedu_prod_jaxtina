<?php
// created: 2021-02-01 09:34:27
$dictionary["Contact"]["fields"]["j_gradebook_contacts_1"] = array (
  'name' => 'j_gradebook_contacts_1',
  'type' => 'link',
  'relationship' => 'j_gradebook_contacts_1',
  'source' => 'non-db',
  'module' => 'J_Gradebook',
  'bean_name' => 'J_Gradebook',
  'vname' => 'LBL_J_GRADEBOOK_CONTACTS_1_FROM_J_GRADEBOOK_TITLE',
  'id_name' => 'j_gradebook_contacts_1j_gradebook_ida',
);

$dictionary['Contact']['fields']['j_budget'] = array(
    'name' => 'j_budget',
    'type' => 'link',
    'relationship' => 'contacts_j_budget',
    'module' => 'J_Budget',
    'bean_name' => 'J_Budget',
    'source' => 'non-db',
    'vname' => 'LBL_EXPENSE',
    'reportable' => false
    );

$dictionary['Contact']['relationships']['contacts_j_budget'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Budget',
    'rhs_table' => 'j_budget',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
);

$dictionary["Contact"]["fields"]["c_news_contacts_1"] = array (
  'name' => 'c_news_contacts_1',
  'type' => 'link',
  'relationship' => 'c_news_contacts_1',
  'source' => 'non-db',
  'module' => 'C_News',
  'bean_name' => 'C_News',
  'vname' => 'LBL_C_NEWS_CONTACTS_1_FROM_C_NEWS_TITLE',
  'id_name' => 'c_news_contacts_1c_news_ida',
);

// created: 2019-04-18 17:57:03
$dictionary["Contact"]["fields"]["contacts_cases_1"] = array (
  'name' => 'contacts_cases_1',
  'type' => 'link',
  'relationship' => 'contacts_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_cases_1contacts_ida',
  'link-type' => 'many',
  'side' => 'left',
);

// created: 2019-03-20 17:04:05
$dictionary["Contact"]["fields"]["c_contacts_contacts_1"] = array (
  'name' => 'c_contacts_contacts_1',
  'type' => 'link',
  'relationship' => 'c_contacts_contacts_1',
  'source' => 'non-db',
  'module' => 'C_Contacts',
  'bean_name' => 'C_Contacts',
  'side' => 'right',
  'vname' => 'LBL_C_CONTACTS_CONTACTS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'c_contacts_contacts_1c_contacts_ida',
  'link-type' => 'one',
);
$dictionary["Contact"]["fields"]["c_contacts_contacts_1_name"] = array (
  'name' => 'c_contacts_contacts_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_C_CONTACTS_CONTACTS_1_FROM_C_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'c_contacts_contacts_1c_contacts_ida',
  'link' => 'c_contacts_contacts_1',
  'table' => 'c_contacts',
  'module' => 'C_Contacts',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["c_contacts_contacts_1c_contacts_ida"] = array (
  'name' => 'c_contacts_contacts_1c_contacts_ida',
  'type' => 'id',
  'source' => 'non-db',
  'vname' => 'LBL_C_CONTACTS_CONTACTS_1_FROM_CONTACTS_TITLE_ID',
  'id_name' => 'c_contacts_contacts_1c_contacts_ida',
  'link' => 'c_contacts_contacts_1',
  'table' => 'c_contacts',
  'module' => 'C_Contacts',
  'rname' => 'id',
  'reportable' => false,
  'side' => 'right',
  'massupdate' => false,
  'duplicate_merge' => 'disabled',
  'hideacl' => true,
);

// created: 2021-08-18 17:15:49

//Custom Relationship  - NEW PAYMENT
$dictionary['Contact']['fields']['payment_link'] = array (
    'name' => 'payment_link',
    'type' => 'link',
    'relationship' => 'student_payments',
    'module' => 'J_Payment',
    'bean_name' => 'J_Payment',
    'source' => 'non-db',
    'vname' => 'LBL_PAYMENT',
);
$dictionary['Contact']['relationships']['student_payments'] = array (
    'lhs_module'        => 'Contacts',
    'lhs_table'            => 'contacts',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_Payment',
    'rhs_table'            => 'j_payment',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts'
);

//Custom Relationship  - ClassStudents
$dictionary['Contact']['fields']['class_students_link'] = array (
    'name' => 'class_students_link',
    'type' => 'link',
    'relationship' => 'class_students',
    'module' => 'J_ClassStudents',
    'bean_name' => 'J_ClassStudents',
    'source' => 'non-db',
    'vname' => 'LBL_CLASSSTUDENTS',
);
$dictionary['Contact']['relationships']['class_students'] = array (
    'lhs_module'        => 'Contacts',
    'lhs_table'            => 'contacts',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_ClassStudents',
    'rhs_table'            => 'j_classstudents',
    'rhs_key'            => 'student_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Contacts'
);

//Custom Relationship  - NEW PAYMENT DETAIL
$dictionary['Contact']['fields']['paymentdetail_link'] = array (
    'name' => 'paymentdetail_link',
    'type' => 'link',
    'relationship' => 'student_receipts',
    'module' => 'J_PaymentDetail',
    'bean_name' => 'J_PaymentDetail',
    'source' => 'non-db',
    'vname' => 'LBL_PAYMENT_DETAIL',
);
$dictionary['Contact']['relationships']['student_receipts'] = array (
    'lhs_module'        => 'Contacts',
    'lhs_table'            => 'contacts',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_PaymentDetail',
    'rhs_table'            => 'j_paymentdetail',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts'
);

//Custom Relationship  - NEW BANK TRANSACTION
$dictionary['Contact']['fields']['banktrans_link'] = array (
    'name' => 'banktrans_link',
    'type' => 'link',
    'relationship' => 'student_banktrans',
    'module' => 'J_BankTrans',
    'bean_name' => 'J_BankTrans',
    'source' => 'non-db',
    'vname' => 'LBL_BANKTRANS',
);
$dictionary['Contact']['relationships']['student_banktrans'] = array (
    'lhs_module'        => 'Contacts',
    'lhs_table'            => 'Contacts',
    'lhs_key'            => 'id',
    'rhs_module'        => 'J_BankTrans',
    'rhs_table'            => 'j_banktrans',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts'
);