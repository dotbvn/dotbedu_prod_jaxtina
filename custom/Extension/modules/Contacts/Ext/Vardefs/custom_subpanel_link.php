<?php
//Payments
$dictionary["Contact"]["fields"]["new_payments"] = array (
    'name' => 'new_payments',
    'type' => 'link',
    'relationship' => 'student_payments',
    'source' => 'non-db',
    'module' => 'J_Payment',
    'bean_name' => 'J_Payment',
    'vname' => 'LBL_PAYMENT_TUITION_FEE',
    'id_name' => 'parent_id',
    'link-type' => 'many',
    'side' => 'left',
    'link_file' => 'custom/modules/J_Payment/PaymentLink.php',
    'link_class' => 'PaymentLink',
    'reportable' => false,
);

//Payments - Book/Gift
$dictionary["Contact"]["fields"]["bookgift_link"] = array (
    'name' => 'bookgift_link',
    'type' => 'link',
    'relationship' => 'student_payments',
    'source' => 'non-db',
    'module' => 'J_Payment',
    'bean_name' => 'J_Payment',
    'vname' => 'LBL_PAYMENT_BOOKGIFT',
    'id_name' => 'parent_id',
    'link-type' => 'many',
    'side' => 'left',
    'link_file' => 'custom/modules/J_Payment/PaymentLink.php',
    'link_class' => 'BookGiftLink',
    'reportable' => false,
);

//Enrollment
$dictionary["Contact"]["fields"]["contacts_situations_enrollment"] = array (
    'name' => 'contacts_situations_enrollment',
    'type' => 'link',
    'relationship' => 'contact_studentsituations',
    'source' => 'non-db',
    'module' => 'J_StudentSituations',
    'bean_name' => 'J_StudentSituations',
    'vname' => 'LBL_ENROLLMENT_TITLE',
    'id_name' => 'student_id',
    'link-type' => 'many',
    'side' => 'left',
    'link_file' => 'custom/modules/J_StudentSituations/SituationLink.php',
    'link_class' => 'EnrollmentLink',
    'reportable' => false,
);

//Delay Link
$dictionary["Contact"]["fields"]["contacts_situations_delay"] = array (
    'name' => 'contacts_situations_delay',
    'type' => 'link',
    'relationship' => 'contact_studentsituations',
    'source' => 'non-db',
    'module' => 'J_StudentSituations',
    'bean_name' => 'J_StudentSituations',
    'vname' => 'LBL_DELAY_TITLE',
    'id_name' => 'student_id',
    'link-type' => 'many',
    'side' => 'left',
    'link_file' => 'custom/modules/J_StudentSituations/SituationLink.php',
    'link_class' => 'DelayLink',
    'reportable' => false,
);