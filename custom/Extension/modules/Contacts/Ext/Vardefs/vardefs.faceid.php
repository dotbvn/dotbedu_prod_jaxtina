<?php
// FaceID Field
$dictionary['Contact']['fields']['status_face_id'] = array(
    'name' => 'status_face_id',
    'vname' => 'LBL_STATUS_FACE_ID',
    'type' => 'varchar',
    'source' => 'non-db',
    'len' => '255',
    'required' => false,
    'importable' => false,
    'massupdate' => false,
    'readonly' => true,
    'duplicate_on_record_copy' => 'no',
);
$dictionary['Contact']['fields']['frc_device_id_list'] = array(
    'name' => 'frc_device_id_list',
    'vname' => 'LBL_DEVICE_ID_LIST',
    'type' => 'multienum',
    'isMultiSelect' => true,
    'dbType' => 'varchar',
    'required' => false,
    'audited' => true,
    'importable' => false,
    'massupdate' => false,
    'readonly' => true,
    'function' => 'getDeviceID'
);
$dictionary['Contact']['fields']['frc_face_id_image'] = array(
    'name' => 'frc_face_id_image',
    'vname' => 'LBL_FACEID_IMAGE',
    'type' => 'image',
    'dbType' => 'varchar',
    'required' => false,
    'massupdate' => false,
    'reportable' => false,
    'len' => '255',
    'width' => '42',
    'height' => '42',
    'border' => '',
    'help' => 'LBL_HELP_FACEID_IMAGE'
);
//Add relationship with J_CheckInOut
$dictionary['Contact']['relationships']['checkinout_contacts'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_CheckInOut',
    'rhs_table' => 'j_checkinout',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many'
);