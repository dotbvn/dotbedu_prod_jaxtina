<?php
//Custom Relationship. Contact - SMS  By Hieu Pham
$dictionary['Contact']['fields']['contacts_sms'] = array (
    'name' => 'contacts_sms',
    'type' => 'link',
    'relationship' => 'contact_smses',
    'module' => 'C_SMS',
    'bean_name' => 'C_SMS',
    'source' => 'non-db',
    'vname' => 'LBL_STUDENT_SMS',
);
$dictionary['Contact']['relationships']['contact_smses'] = array (
    'lhs_module'        => 'Contacts',
    'lhs_table'            => 'contacts',
    'lhs_key'            => 'id',
    'rhs_module'        => 'C_SMS',
    'rhs_table'            => 'c_sms',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
);
//Custom field - By HP
$dictionary['Contact']['fields']['relationship2'] = array(
    'name' => 'relationship2',
    'vname' => 'LBL_RELATIONSHIP2',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'relationship_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary["Contact"]["fields"]["source_description"] = array(
    'name' => 'source_description',
    'type' => 'text',
    'rows' => '1',
    'cols' => '30',
    'studio' => 'visible',
    'vname' => 'LBL_SOURCE_DESCRIPTION',
);


//Custom Relationship. Contact - SMS  By HP
$dictionary['Contact']['fields']['contacts_sms'] = array(
    'name' => 'contacts_sms',
    'type' => 'link',
    'relationship' => 'contact_smses',
    'module' => 'C_SMS',
    'bean_name' => 'C_SMS',
    'source' => 'non-db',
    'vname' => 'LBL_STUDENT_SMS',
);
$dictionary['Contact']['relationships']['contact_smses'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_SMS',
    'rhs_table' => 'c_sms',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
);


// Add by Tuan Anh
$dictionary['Contact']['fields']['gender'] = array(
    'name' => 'gender',
    'required' => true,
    'vname' => 'LBL_GENDER',
    'type' => 'enum',
    'massupdate' => true,
    'len' => 20,
    'options' => 'gender_list',
    'default' => '',
    'importable' => true,
);
$dictionary['Contact']['fields']['member_card'] = array(
    'name' => 'member_card',
    'vname' => 'LBL_RFID_CARD',
    'type' => 'varchar',
    'len' => '150',
    'comment' => '',
);

$dictionary['Contact']['fields']['contact_id'] = array(
    'name' => 'contact_id',
    'vname' => 'LBL_CONTACT_ID',
    'type' => 'varchar',
    'len' => '255',
    'comment' => '',
    // 'default' => 'Auto generate',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
    'readonly'  => true,
    'duplicate_on_record_copy' => 'no',
);
$dictionary['Contact']['fields']['guardian_name'] = array(
    'name' => 'guardian_name',
    'vname' => 'LBL_GUARDIAN_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);
$dictionary['Contact']['fields']['guardian_name_2'] = array(
    'name' => 'guardian_name_2',
    'vname' => 'LBL_GUARDIAN_NAME_2',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);


$dictionary['Contact']['fields']['nick_name'] = array(
    'name' => 'nick_name',
    'vname' => 'LBL_NICK_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);
$dictionary['Contact']['fields']['other_mobile'] = array(
    'name' => 'other_mobile',
    'vname' => 'LBL_OTHER_MOBILE',
    'type' => 'phone',
    'dbType' => 'varchar',
    'len' => '50',
    'required' => 'false',
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);

$dictionary['Contact']['fields']['relationship'] = array(
    'name' => 'relationship',
    'vname' => 'LBL_RELATIONSHIP',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' =>  'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'relationship_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['Contact']["fields"]["grade"] = array(
    'required' => false,
    'name' => 'grade',
    'vname' => 'LBL_GRADE',
    'type' => 'varchar',
    'massupdate' => 0,
    'no_default' => false,
    'importable' => true,
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'len' => '255',
    'size' => '20',
    'studio' => 'visible',
);

$dictionary['Contact']["fields"]["phone_guardian"] = array(
    'name' => 'phone_guardian',
    'vname' => 'LBL_PHONE_GUARDIAN',
    'type' => 'phone',
    'dbType' => 'varchar',
    'len' => '100',
    'comments' => '',
    'help' => '',
    'default' => '',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'studio' => 'visible',
    'massupdate' => 0,
    'unified_search' => true,
    'full_text_search' => array('enabled' => true, 'searchable' => true, 'boost' => 0.94),
);



$dictionary['Contact']['fields']['prefer_level'] = array(
    'name' => 'prefer_level',
    'vname' => 'LBL_PREFER_LEVEL',
    'type' => 'multienum',
    'dbType' => 'varchar',
    'default' => '',
    'comments' => 'comment',
    'importable' => 'true',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'audited' => true,
    'reportable' => true,
    'len' => 255,
    'size' => '20',
    'options' => 'kind_of_course_list',
    'studio' => 'visible',
    'dependency' => false,
    'massupdate' => true,
    'isMultiSelect' => true,
);

//CUSTOM RELATIONSHIP - Lap Nguyen

// Relationship Student ( 1 - n ) Attendance - Lap Nguyen
$dictionary['Contact']['fields']['attendances_parent'] = array(
    'name' => 'attendances_parent',
    'type' => 'link',
    'relationship' => 'student_attendances',
    'module' => 'C_Attendance',
    'bean_name' => 'C_Attendance',
    'source' => 'non-db',
    'vname' => 'LBL_ATTENDANCES',
);
$dictionary['Contact']['relationships']['student_attendances'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Attendance',
    'rhs_table' => 'c_attendance',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Contacts'
);

// Relationship Student ( 1 - n ) Delivery Revenue
$dictionary['Contact']['relationships']['student_revenue'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_DeliveryRevenue',
    'rhs_table' => 'c_deliveryrevenue',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
);

$dictionary['Contact']['fields']['student_revenue'] = array(
    'name' => 'student_revenue',
    'type' => 'link',
    'relationship' => 'student_revenue',
    'module' => 'C_DeliveryRevenue',
    'bean_name' => 'C_DeliveryRevenue',
    'source' => 'non-db',
    'vname' => 'LBL_DELIVERY_REVENUE',
);

//Custom Relationship. Student - StudentSituation  By Lap Nguyen
$dictionary['Contact']['fields']['ju_studentsituations'] = array(
    'name' => 'ju_studentsituations',
    'type' => 'link',
    'relationship' => 'contact_studentsituations',
    'module' => 'J_StudentSituations',
    'bean_name' => 'J_StudentSituations',
    'source' => 'non-db',
    'vname' => 'LBL_STUDENT_SITUATION',
);
$dictionary['Contact']['relationships']['contact_studentsituations'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_StudentSituations',
    'rhs_table' => 'j_studentsituations',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'student_type',
    'relationship_role_column_value' => 'Contacts'
);

//Custom Relationship. Student - PT/Demo  By Lap Nguyen
$dictionary['Contact']['fields']['j_ptresults'] = array(
    'name' => 'j_ptresults',
    'type' => 'link',
    'relationship' => 'contact_ptresults',
    'module' => 'J_PTResult',
    'bean_name' => 'J_PTResult',
    'source' => 'non-db',
    'vname' => 'LBL_PTRESULT',
);
$dictionary['Contact']['relationships']['contact_ptresults'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_PTResult',
    'rhs_table' => 'j_ptresult',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent',
    'relationship_role_column_value' => 'Contacts'
);
$dictionary["Contact"]["fields"]["j_ptresults_demo"] = array(
    'name' => 'j_ptresults_demo',
    'type' => 'link',
    'link_file' => 'modules/J_PTResult/DemoLink.php',
    'link_class' => 'DemoLink',
    'relationship' => 'contact_ptresults',
    'source' => 'non-db',
    'module' => 'J_PTResult',
    'bean_name' => 'J_PTResult',
    'vname' => 'LBL_DEMO_RESULT',
    'id_name' => 'student_id',
    'link-type' => 'many',
    'side' => 'left',
);

$dictionary["Contact"]["fields"]["j_ptresults_pt"] = array(
    'name' => 'j_ptresults_pt',
    'type' => 'link',
    'link_file' => 'modules/J_PTResult/PTLink.php',
    'link_class' => 'PTLink',
    'relationship' => 'contact_ptresults',
    'source' => 'non-db',
    'module' => 'J_PTResult',
    'bean_name' => 'J_PTResult',
    'vname' => 'LBL_PT_RESULT',
    'id_name' => 'student_id',
    'link-type' => 'many',
    'side' => 'left',
);

$dictionary["Contact"]["fields"]["referral_status"] = array(
    'required' => false,
    'name' => 'referral_status',
    'vname' => 'LBL_REFERRAL_STATUS',
    'type' => 'enum',
    'massupdate' => false,
    'default' => 'Inactive',
    'comments' => '',
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'len' => 10,
    'size' => '20',
    'options' => 'voucher_status_dom',
    'studio' => 'visible',
    'dependency' => false,
);

// Relationship Gradebook Detail (1 - n)
$dictionary['Contact']['fields']['student_j_gradebookdetail'] = array(
    'name' => 'student_j_gradebookdetail',
    'type' => 'link',
    'relationship' => 'student_j_gradebookdetail',
    'module' => 'J_GradebookDetail',
    'bean_name' => 'J_GradebookDetail',
    'source' => 'non-db',
    'vname' => 'LBL_GRADEBOOK_DETAIL',
);
$dictionary['Contact']['relationships']['student_j_gradebookdetail'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_GradebookDetail',
    'rhs_table' => 'j_gradebookdetail',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
);

//Custom Relationship. Student - Voucher (1 - n) By Lap Nguyen
$dictionary['Contact']['fields']['voucher_name'] = array(
    'required'  => false,
    'source'    => 'non-db',
    'name'      => 'voucher_name',
    'vname'     => 'LBL_VOUCHER',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'voucher_id',
    'join_name' => 'j_voucher',
    'link'      => 'ju_vouchers',
    'table'     => 'j_voucher',
    'isnull'    => 'true',
    'module'    => 'J_Voucher',
    'module'    => 'J_Voucher',
    'readonly'  => true,
);

$dictionary['Contact']['fields']['voucher_id'] = array(
    'name'              => 'voucher_id',
    'rname'             => 'id',
    'vname'             => 'LBL_VOUCHER',
    'type'              => 'id',
    'table'             => 'j_voucher',
    'isnull'            => 'true',
    'module'            => 'J_Voucher',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
);
$dictionary['Contact']['fields']['ju_vouchers'] = array(
    'name' => 'ju_vouchers',
    'type' => 'link',
    'relationship' => 'contact_vouchers',
    'module' => 'J_Voucher',
    'bean_name' => 'J_Voucher',
    'source' => 'non-db',
    'vname' => 'LBL_VOUCHER',
);


//Custom Relationship. Student - Loyalty (1 - n) By Lap Nguyen
$dictionary['Contact']['fields']['loyalty_link'] = array(
    'name' => 'loyalty_link',
    'type' => 'link',
    'relationship' => 'student_loyaltys',
    'module' => 'J_Loyalty',
    'bean_name' => 'J_Loyalty',
    'source' => 'non-db',
    'vname' => 'LBL_LOYALTY',
);
$dictionary['Contact']['relationships']['student_loyaltys'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Loyalty',
    'rhs_table' => 'j_loyalty',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many'
);


//Custom Relationship. Student - Sponsor (1 - n) By Lap Nguyen
$dictionary['Contact']['fields']['sponsor_link'] = array(
    'name' => 'sponsor_link',
    'type' => 'link',
    'relationship' => 'student_sponsor',
    'module' => 'J_Sponsor',
    'bean_name' => 'J_Sponsor',
    'source' => 'non-db',
    'vname' => 'LBL_SPONSOR',
);
$dictionary['Contact']['relationships']['student_sponsor'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Sponsor',
    'rhs_table' => 'j_sponsor',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many'
);

////Custom Relationship. Student - Notification (1 - n) By Lap Nguyen
//$dictionary['Contact']['fields']['notify_link'] = array(
//    'name' => 'notify_link',
//    'type' => 'link',
//    'relationship' => 'student_notifications',
//    'module' => 'Notifications',
//    'bean_name' => 'Notifications',
//    'source' => 'non-db',
//    'vname' => 'LBL_NOTIFY',
//);
//$dictionary['Contact']['relationships']['student_notifications'] = array(
//    'lhs_module' => 'Contacts',
//    'lhs_table' => 'contacts',
//    'lhs_key' => 'id',
//    'rhs_module' => 'Notifications',
//    'rhs_table' => 'notifications',
//    'rhs_key' => 'student_id',
//    'relationship_type' => 'one-to-many'
//);

$dictionary["Contact"]["fields"]["age"] = array(
    'name' => 'age',
    'type' => 'int',
    'len' => '10',
    'enable_range_search' => true,
    'options' => 'numeric_range_search_dom',
    'studio' => 'visible',
    'vname' => 'LBL_AGE',
    'readonly'  => true,
);
$dictionary['Contact']['fields']['full_student_name'] = array(
    'name' => 'full_student_name',
    'vname' => 'LBL_FULL_CONTACT_NAME',
    'type' => 'varchar',
    'len' => '100',
    'comment' => '',
    'merge_filter' => 'disabled',
);
$dictionary['Contact']['fields']['status'] = array(
    'name' => 'status',
    'vname' => 'LBL_STATUS',
    'type' => 'enum',
    'len' => '100',
    'audited' => true,
    'options' => 'contact_status_list',
    'default' => 'Waiting for class',
    'duplicate_on_record_copy' => 'no',
    'massupdate' => false,
);
//Add by Tuan Anh modified mass update

$dictionary["Contact"]["fields"]["sync_contact"] = array(
    'massupdate' => true,
    'name' => 'sync_contact',
    'vname' => 'LBL_SYNC_CONTACT',
    'type' => 'bool',
    'source' => 'non-db',
    'comment' => 'Synch to outlook?  (Meta-Data only)',
    'studio' => 'true',
);

$dictionary['Contact']['fields']['potential'] = array(
    'name' => 'potential',
    'vname' => 'LBL_POTENTIAL',
    'type' => 'enum',
    'comments' => '',
    'help' => '',
    'massupdate' => true,
    'default' => 'Interested',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 20,
    'size' => '20',
    'options' => 'level_lead_list',
    'studio' => 'visible',
);

//**IMPORT FIELD !!!***
$dictionary["Contact"]["fields"]['old_student_id'] = array(
    'name' => 'old_student_id',
    'vname' => 'LBL_OLD_STUDENT_ID',
    'type' => 'varchar',
    'len' => '150',
    'importable' => 'true',
);
//**END: IMPORT FIELD !!!***
$dictionary['Contact']['duplicate_check'] = array(
    'enabled' => true,
    'FilterDuplicateCheck' => array(
        'filter_template' => array(
            array(
                '$and' => array(
                    array('full_student_name' => array('$equals' => '$name')),
                    array(
                        '$or' => array(
                            array('phone_mobile' => array('$contains' => '$phone_mobile')),
                            array('phone_mobile' => array('$contains' => '$phone_guardian')),
                            array('phone_mobile' => array('$contains' => '$other_mobile')),

                            array('other_mobile' => array('$contains' => '$phone_mobile')),
                            array('other_mobile' => array('$contains' => '$phone_guardian')),
                            array('other_mobile' => array('$contains' => '$other_mobile')),

                            array('phone_guardian' => array('$contains' => '$other_mobile')),
                            array('phone_guardian' => array('$contains' => '$phone_mobile')),
                            array('phone_guardian' => array('$contains' => '$phone_guardian')),
                        ),
                    ),
                ),
            ),
        ),
        'ranking_fields' => array(
            array('in_field_name' => 'full_student_name', 'dupe_field_name' => 'full_student_name'),
            array('in_field_name' => 'phone_mobile', 'dupe_field_name' => 'phone_mobile'),
        ),
    ),
);
$dictionary["Contact"]["fields"]["is_converted_from_lead"] = array(
    'name' => 'is_converted_from_lead',
    'vname' => 'LBL_IS_CONVERTED_FROM_LEAD',
    'type' => 'bool',
    'massupdate' => true,
    'default' => '1',
);
$dictionary['Contact']['fields']['class_activity'] = array (
    'name' => 'class_activity',
    'vname' => 'LBL_CLASS_ACTIVITY',
    'type' => 'text',
    'source' => 'non-db',
);

$dictionary['Contact']['fields']['date_issue'] = array(
    'name' => 'date_issue',
    'vname' => 'LBL_DATE_ISSUE',
    'type' => 'date',
    'required' => false,
    'display_default' => 'now',
);

$dictionary['Contact']['fields']['guardian_type_1'] = array(
    'name' => 'guardian_type_1',
    'vname' => 'LBL_GUARDIAN_TYPE_1',
    'type' => 'enum',
    'options' => 'guardian_type_dom',
    'dependency' => 'not(equal($guardian1, ""))',
    'required' => false,
    'len' => 100,
);
$dictionary['Contact']['fields']['guardian_type_2'] = array(
    'name' => 'guardian_type_2',
    'vname' => 'LBL_GUARDIAN_TYPE_2',
    'type' => 'enum',
    'options' => 'guardian_type_dom',
    'dependency' => 'not(equal($guardian2, ""))',
    'required' => false,
    'len' => 100,
);
$dictionary['Contact']['fields']['guardian_type_3'] = array(
    'name' => 'guardian_type_3',
    'vname' => 'LBL_GUARDIAN_TYPE_3',
    'type' => 'enum',
    'options' => 'guardian_type_dom',
    'dependency' => 'not(equal($guardian3, ""))',
    'required' => false,
    'len' => 100,
);

//Add Relationship Membership - Contacts (1 - n) (guardian1)
$dictionary['Contact']['fields']['guardian1'] = array(
    'name' => 'guardian1',
    'vname' => 'LBL_GUARDIAN1',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['Contact']['fields']['guardian1_name'] = array(
    'name' => 'guardian1_name',
    'rname' => 'name',
    'id_name' => 'guardian1',
    'vname' => 'LBL_GUARDIAN1_NAME',
    'type' => 'relate',
    'link' => 'guardian1_link',
    'table' => 'j_membership',
    'isnull' => 'true',
    'module' => 'J_Membership',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['Contact']['fields']['guardian1_link'] = array(
    'name' => 'guardian1_link',
    'type' => 'link',
    'relationship' => 'contacts_guardian1',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_GUARDIAN1_NAME',
);
//Add Relationship Membership - Contacts (1 - n) (guardian2)
$dictionary['Contact']['fields']['guardian2'] = array(
    'name' => 'guardian2',
    'vname' => 'LBL_GUARDIAN2',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['Contact']['fields']['guardian2_name'] = array(
    'name' => 'guardian2_name',
    'rname' => 'name',
    'id_name' => 'guardian2',
    'vname' => 'LBL_GUARDIAN2_NAME',
    'type' => 'relate',
    'link' => 'guardian2_link',
    'table' => 'j_membership',
    'isnull' => 'true',
    'module' => 'J_Membership',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['Contact']['fields']['guardian2_link'] = array(
    'name' => 'guardian2_link',
    'type' => 'link',
    'relationship' => 'contacts_guardian2',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_GUARDIAN2_NAME',
);
//Add Relationship Membership - Contacts (1 - n) (guardian3)
$dictionary['Contact']['fields']['guardian3'] = array(
    'name' => 'guardian3',
    'vname' => 'LBL_GUARDIAN3',
    'type' => 'id',
    'required' => false,
    'reportable' => false,
    'comment' => '',
    'audit' => true,
);

$dictionary['Contact']['fields']['guardian3_name'] = array(
    'name' => 'guardian3_name',
    'rname' => 'name',
    'id_name' => 'guardian3',
    'vname' => 'LBL_GUARDIAN3_NAME',
    'type' => 'relate',
    'link' => 'guardian3_link',
    'table' => 'j_membership',
    'isnull' => 'true',
    'module' => 'J_Membership',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable' => true,
    'source' => 'non-db',
    'required' => false,
);

$dictionary['Contact']['fields']['guardian3_link'] = array(
    'name' => 'guardian3_link',
    'type' => 'link',
    'relationship' => 'contacts_guardian3',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_GUARDIAN3_NAME',
);

$dictionary['Contact']['fields']['external_connection'] = array(
    'required' => false,
    'name' => 'external_connection',
    'vname' => 'LBL_EXTERNAL_CONNECTION',
    'type' => 'text',
);


$dictionary['Contact']['fields']['current_class_id'] = array(
    'name'              => 'current_class_id',
    'rname'             => 'name',
    'vname'             => 'LBL_CURRENT_CLASS',
    'type'              => 'id',
    'table'             => 'j_class',
    'isnull'            => 'true',
    'module'            => 'J_Class',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
);

$dictionary['Contact']['fields']['current_class_name'] = array(
    'source'    => 'non-db',
    'name'      => 'current_class_name',
    'vname'     => 'LBL_CURRENT_CLASS',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'current_class_id',
    'join_name' => 'j_class',
    'link'      => 'cr_class_link',
    'table'     => 'j_class',
    'isnull'    => 'true',
    'module'    => 'J_Class',
);

$dictionary['Contact']['fields']['cr_class_link'] = array(
    'name'          => 'cr_class_link',
    'type'          => 'link',
    'relationship'  => 'student_cur_class',
    'module'        => 'J_Class',
    'bean_name'     => 'J_Class',
    'source'        => 'non-db',
    'vname'         => 'LBL_CURRENT_CLASS',
);

$dictionary['Contact']['fields']['cover_app'] = array(
    'name' => 'cover_app',
    'vname' => 'LBL_COVER_APP',
    'type' => 'varchar',
    'len' => '255',
);

$dictionary['Contact']['fields']['view_news'] = array (
    'name' => 'view_news',
    'type' => 'int',
    'source' => 'non-db',
    'vname' => 'LBL_VIEW_NEWS',
);
$dictionary['Contact']['fields']['overview'] = array(
    'name' => 'overview',
    'vname' => 'LBL_OV_OVERVIEW',
    'type' => 'varchar',
    'source' => 'non-db',
    'readonly' => true,
);

$dictionary['Contact']['fields']['birth_day'] = array(
    'name' => 'birth_day',
    'vname' => 'LBL_BIRTH_DAY',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' =>  'disabled',
    'len' => 50,
    'default' => '',
    'size' => '20',
    'options' => 'birth_day_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['Contact']['fields']['birth_month'] = array(
    'name' => 'birth_month',
    'vname' => 'LBL_BIRTH_MONTH',
    'type' => 'enum',
    'audited' => false,
    'unified_search' => false,
    'default' => '',
    'merge_filter' =>  'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'birth_month_list',
    'studio' => 'visible',
    'massupdate' => 0,
);

$dictionary['Contact']['fields']['lasted_view_noti'] = array(
    'name' => 'lasted_view_noti',
    'vname' => 'LASTED_VIEW_NOTI',
    'type' => 'datetime',
    'required' => false,
    'massupdate' => 0,
    'duplicate_on_record_copy' => 'no',
);
$dictionary['Contact']['fields']['opportunities_parent'] = array(
    'name' => 'opportunities_parent',
    'type' => 'link',
    'relationship' => 'contact_opportunities',
    'source' => 'non-db',
    'vname' => 'LBL_OPPORTUNITIES',
    'reportable' => false,
);
$dictionary['Contact']['relationships']['contact_opportunities'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'Opportunities',
    'rhs_table' => 'opportunities',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts'
);

$dictionary['Contact']['fields']['payment_process'] = array(
    'name' => 'payment_process',
    'vname' => 'LBL_PAYMENT_PROCESS',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 50,
    'size' => '20',
    'options' => 'payment_process_list',
    'studio' => 'visible',
    'massupdate' => 0,
);
//Add by HoangHvy to create relationship between Contact and Comments
$dictionary['Contact']['relationships']['c_comments_contacts'] = array(
    'lhs_module' => 'Contact',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Comments',
    'rhs_table' => 'c_comments',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many'
);