<?php
// CJ: Custom link Contact - Tasks - Add by phgiahannn
$dictionary["Contact"]["fields"]["custom_tasks_link"] = array (
    'name' => 'custom_tasks_link',
    'type' => 'link',
    'relationship' => 'contact_tasks',
    'source' => 'non-db',
    'module' => 'Tasks',
    'bean_name' => 'Task',
    'vname' => 'LBL_TASKS_SUBPANEL_TITLE',
    'id_name' => 'parent_id',
    'link-type' => 'many',
    'side' => 'left',
    'link_file' => 'custom/modules/Contacts/customTaskLink.php',
    'link_class' => 'customTaskLink',
);