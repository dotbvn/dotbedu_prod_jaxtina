<?php
$dictionary['Contact']['fields']['career'] = array(
    'name' => 'career',
    'vname' => 'LBL_CAREER',
    'type' => 'enum',
    'dbType' => 'varchar',
    'required' => false,
    'reportable' => true,
    'importable' => true,
    'mass_update' => true,
    'len' => 200,
    'duplicate_merge' => false,
    'options' => 'careers_list',
);