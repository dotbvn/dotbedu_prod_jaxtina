<?php
  //MKT fields
$dictionary['Contact']['fields']['reference'] = array (
    'name' => 'reference',
    'vname' => 'LBL_REFERENCE',
    'type' => 'url',
    'len' => 255,
    'importable' => true,
);
$dictionary['Contact']['fields']['utm_source'] = array(
    'name' => 'utm_source',
    'vname' => 'LBL_UTM_SOURCE',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Contact']['fields']['utm_medium'] = array(
    'name' => 'utm_medium',
    'vname' => 'LBL_UTM_MEDIUM',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Contact']['fields']['utm_content'] = array(
    'name' => 'utm_content',
    'vname' => 'LBL_UTM_CONTENT',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Contact']['fields']['utm_term'] = array(
    'name' => 'utm_term',
    'vname' => 'LBL_UTM_TERM',
    'type' => 'varchar',
    'len' => '150',
);
$dictionary['Contact']['fields']['channel'] = array(
    'name' => 'channel',
    'vname' => 'LBL_CHANNEL',
    'type' => 'enum',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'required' => false,
    'importable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'len' => 150,
    'options' => 'utm_source_list',
    'studio' => 'visible',
    'massupdate' => true,
    'dependency' => false,
);
//Add Agent user
$dictionary['Contact']['fields']['utm_agent_id'] = array(
    'name' => 'utm_agent_id',
    'vname' => 'LBL_UTM_AGENT',
    'type' => 'id',
    'required'=>false,
    'reportable'=>false,
    'comment' => ''
);

$dictionary['Contact']['fields']['utm_agent'] = array(
    'name' => 'utm_agent',
    'rname' => 'name',
    'id_name' => 'utm_agent_id',
    'vname' => 'LBL_UTM_AGENT',
    'type' => 'relate',
    'link' => 'utm_agent_link',
    'table' => 'users',
    'isnull' => 'true',
    'module' => 'Users',
    'dbType' => 'varchar',
    'len' => 'id',
    'reportable'=>true,
    'source' => 'non-db',
    'massupdate' => true,
);

$dictionary['Contact']['fields']['utm_agent_link'] = array(
    'name' => 'utm_agent_link',
    'type' => 'link',
    'relationship' => 'utm_agent_rel_c',
    'link_type' => 'one',
    'side' => 'right',
    'source' => 'non-db',
    'vname' => 'LBL_UTM_AGENT',
);

//End MKT Fields

//Add fields School
$dictionary['Contact']['fields']['school_name'] = array (
    'required'  => false,
    'source'    => 'non-db',
    'name'      => 'school_name',
    'vname'     => 'LBL_SCHOOL_NAME',
    'type'      => 'relate',
    'rname'     => 'name',
    'id_name'   => 'school_id',
    'massupdate' => false,
    'join_name' => 'j_school',
    'link'      => 'j_school_link',
    'table'     => 'j_school',
    'isnull'    => 'true',
    'module'    => 'J_School',
);

$dictionary['Contact']['fields']['school_id'] = array (
    'name'              => 'school_id',
    'rname'             => 'id',
    'vname'             => 'LBL_SCHOOL_NAME',
    'type'              => 'id',
    'table'             => 'j_school',
    'isnull'            => 'true',
    'module'            => 'J_School',
    'dbType'            => 'id',
    'reportable'        => false,
    'massupdate'        => false,
    'duplicate_merge'   => 'disabled',
    'importable' => false,
);

$dictionary['Contact']['fields']['j_school_link'] = array (
    'name'          => 'j_school_link',
    'type'          => 'link',
    'relationship'  => 'j_school_contact',
    'module'        => 'J_School',
    'bean_name'     => 'J_School',
    'source'        => 'non-db',
    'vname'         => 'LBL_SCHOOL_NAME',
);

$dictionary['Contact']['indices']['idx_old_student_id'] = array (
    'name' => 'idx_old_student_id',
    'type' => 'index',
    'fields' =>
    array (
        0 => 'old_student_id',
    ),
);


  $dictionary['Contact']['fields']['team_name_text'] = array(
    'name' => 'team_name_text',
    'vname' => 'Team Name (Text)',
    'type' => 'varchar',
    'len' => '255',
    'importable' => 'true',
);

$dictionary['Contact']['fields']['referencelog_link'] = array(
    'name' => 'referencelog_link',
    'type' => 'link',
    'relationship' => 'contact_referencelog',
    'module' => 'J_ReferenceLog',
    'bean_name' => 'J_ReferenceLog',
    'source' => 'non-db',
    'vname' => 'LBL_REFERENCE_LOG',
    'reportable' => false
);

$dictionary['Contact']['fields']['reference_logs'] = array(
    'name' => 'reference_logs',
    'type' => 'text',
    'source' => 'non-db',
    'vname' => 'LBL_REFERENCE_LOG',
);

$dictionary['Contact']['relationships']['contact_referencelog'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'J_ReferenceLog',
    'rhs_table' => 'j_referencelog',
    'rhs_key' => 'parent_id',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts',
);
