<?php
// created: 2021-09-15 13:55:46
$dictionary['Contact']['fields']['facebook']['type'] = 'url';
$dictionary['Contact']['fields']['dp_business_purpose']['massupdate'] = false;
$dictionary['Contact']['fields']['do_not_call']['massupdate'] = false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['massupdate'] = false;
$dictionary['Contact']['fields']['tag']['massupdate'] = false;
$dictionary['Contact']['fields']['school_name']['massupdate'] = true;
$dictionary['Contact']['fields']['c_contacts_contacts_1_name']['massupdate'] = false;
$dictionary['Contact']['fields']['dri_workflow_template_id']['massupdate'] = false;
$dictionary['Contact']['fields']['mkto_sync']['massupdate'] = false;
$dictionary['Contact']['fields']['preferred_language']['massupdate'] = false;
$dictionary['Contact']['fields']['converted_opp_name']['massupdate'] = false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['massupdate'] = false;
$dictionary['Contact']['fields']['dp_business_purpose']['massupdate'] = false;
$dictionary['Contact']['fields']['report_to_name']['massupdate'] = false;
$dictionary['Contact']['fields']['sync_contact']['massupdate'] = false;
$dictionary['Contact']['fields']['campaign_name']['massupdate'] = false;
$dictionary['Contact']['fields']['account_name']['massupdate'] = false;


$dictionary['Contact']['fields']['first_name']['required'] = true;
$dictionary['Contact']['fields']['birthdate']['required'] = true;
$dictionary['Contact']['fields']['phone_mobile']['required'] = true;
$dictionary['Contact']['fields']['primary_address_street']['required'] = true;
$dictionary['Contact']['fields']['lead_source']['required'] = true;
$dictionary['Contact']['fields']['lead_source']['options'] = 'lead_source_list';

$dictionary['Contact']['fields']['guardian_name']['audited'] = true;

$dictionary['Contact']['fields']['guardian_name']['audited'] = true;
$dictionary['Contact']['fields']['salutation']['audited'] = false;
$dictionary['Contact']['fields']['first_name']['audited'] = false;
$dictionary['Contact']['fields']['last_name']['audited'] = false;
$dictionary['Contact']['fields']['title']['audited'] = false;
$dictionary['Contact']['fields']['twitter']['audited'] = false;
$dictionary['Contact']['fields']['office_phone']['audited'] = false;
$dictionary['Contact']['fields']['fax_phone']['audited'] = false;
$dictionary['Contact']['fields']['primary_address_street_2']['audited'] = false;
$dictionary['Contact']['fields']['primary_address_street_3']['audited'] = false;
$dictionary['Contact']['fields']['primary_address_city']['audited'] = false;
$dictionary['Contact']['fields']['primary_address_state']['audited'] = false;
$dictionary['Contact']['fields']['primary_address_postalcode']['audited'] = false;
$dictionary['Contact']['fields']['primary_address_country']['audited'] = false;
$dictionary['Contact']['fields']['alt_address_street']['audited'] = false;
$dictionary['Contact']['fields']['alt_address_street_2']['audited'] = false;
$dictionary['Contact']['fields']['alt_address_street_3']['audited'] = false;
$dictionary['Contact']['fields']['alt_address_city']['audited'] = false;
$dictionary['Contact']['fields']['alt_address_state']['audited'] = false;
$dictionary['Contact']['fields']['alt_address_postalcode']['audited'] = false;
$dictionary['Contact']['fields']['alt_address_country']['audited'] = false;
$dictionary['Contact']['fields']['dp_business_purpose']['audited'] = false;
$dictionary['Contact']['fields']['dp_consent_last_updated']['audited'] = false;
$dictionary['Contact']['fields']['assistant']['audited'] = false;
$dictionary['Contact']['fields']['full_student_name']['audited'] = true;
$dictionary['Contact']['fields']['mkto_sync']['audited'] = false;
$dictionary['Contact']['fields']['dri_workflow_template_id']['audited'] = false;
$dictionary['Contact']['fields']['dri_workflow_template_name']['audited'] = false;
$dictionary['Contact']['fields']['dri_workflow_template_link']['audited'] = false;
$dictionary['Contact']['fields']['prefer_level']['audited'] = false;
$dictionary['Contact']['fields']['do_not_call']['audited'] = false;
$dictionary['Contact']['fields']['assistant_phone']['audited'] = false;
$dictionary['Contact']['fields']['mkto_id']['audited'] = false;
$dictionary['Contact']['fields']['mkto_lead_score']['audited'] = false;
$dictionary['Contact']['fields']['prefer_level']['audited'] = false;
$dictionary['Contact']['fields']['phone_guardian']['audited'] = false;
$dictionary['Contact']['fields']['lead_source']['audited'] = true;
$dictionary['Contact']['fields']['team_set_id']['audited'] = false;
$dictionary['Contact']['fields']['acl_team_set_id']['audited'] = false;
$dictionary['Contact']['fields']['phone_work']['audited'] = false;
$dictionary['Contact']['fields']['phone_other']['audited'] = false;
$dictionary['Contact']['fields']['phone_fax']['audited'] = false;
$dictionary['Contact']['fields']['phone_home']['audited'] = false;
$dictionary['Contact']['fields']['facebook']['audited'] = false;
$dictionary['Contact']['fields']['phone_guardian']['audited'] = true;

$dictionary['Contact']['fields']['primary_address_city']['required'] = true;
$dictionary['Contact']['fields']['primary_address_state']['required'] = true;
$dictionary['Contact']['fields']['primary_address_country']['required'] = true;
$dictionary['Contact']['fields']['primary_address_country']['default'] = '';

$dictionary['Contact']['fields']['phone_mobile']['enableSMS']= true;
$dictionary['Contact']['fields']['phone_work']['enableSMS']= true;
$dictionary['Contact']['fields']['assistant_phone']['enableSMS']= true;



VardefManager::addTemplate('Contacts', 'Contact', 'customer_journey_parent', 'Contact', true);
$dictionary['Contact']['fields']['lead_decription_c']['labelValue']='Source Description';
$dictionary['Contact']['fields']['lead_decription_c']['full_text_search']=array (
    'enabled' => '0',
    'boost' => '1',
    'searchable' => false,
);
$dictionary['Contact']['fields']['lead_decription_c']['enforced']='';
$dictionary['Contact']['fields']['lead_decription_c']['dependency']='';
$dictionary['Contact']['fields']['contact_id']['default']='Auto generate';
$dictionary['Contact']['fields']['contact_id']['audited']=false;
$dictionary['Contact']['fields']['contact_id']['massupdate']=false;
$dictionary['Contact']['fields']['contact_id']['importable']='false';
$dictionary['Contact']['fields']['contact_id']['duplicate_merge']='disabled';
$dictionary['Contact']['fields']['contact_id']['duplicate_merge_dom_value']='0';
$dictionary['Contact']['fields']['contact_id']['full_text_search']=array (
    'enabled' => '0',
    'boost' => '1',
    'searchable' => false,
);
$dictionary['Contact']['fields']['contact_id']['calculated']=false;
?>

