<?php
//Custom Relationship  - NEW PAYMENT
$dictionary['Contact']['fields']['link_siblings'] = array (
    'name' => 'link_siblings',
    'type' => 'link',
    'relationship' => 'siblings_contacts',
    'module' => 'C_Siblings',
    'bean_name' => 'C_Siblings',
    'source' => 'non-db',
    'vname' => 'LBL_RELATIONSHIP_TO',
);
$dictionary['Contact']['relationships']['siblings_contacts'] = array (
    'lhs_module'        => 'Contacts',
    'lhs_table'            => 'contacts',
    'lhs_key'            => 'id',
    'rhs_module'        => 'C_Siblings',
    'rhs_table'            => 'c_siblings',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'Contacts'
);


$dictionary['Contact']['relationships']['rel_siblings_contacts'] = array(
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'C_Siblings',
    'rhs_table' => 'c_siblings',
    'rhs_key' => 'student_id',
    'relationship_type' => 'one-to-many',
);

$dictionary['Contact']['fields']['link_primary_siblings'] = array(
    'name' => 'link_primary_siblings',
    'type' => 'link',
    'relationship' => 'rel_siblings_contacts',
    'module' => 'C_Siblings',
    'bean_name' => 'C_Siblings',
    'source' => 'non-db',
    'vname' => 'LBL_SIBLINGS',
);