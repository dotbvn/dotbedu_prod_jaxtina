<?php
//LMS fields
$dictionary['Contact']['fields']['lms_active'] = array(
    'name' => 'lms_active',
    'vname' => 'LBL_LMS_ACTIVE',
    'type' => 'bool',
    'studio' => 'true',
    'massupdate' => true,
    'default' => '1',
    'duplicate_on_record_copy' => 'no',
    'group' => 'portal',
);
$dictionary['Contact']['fields']['lms_user_id'] = array(
    'name' => 'lms_user_id',
    'vname' => 'LBL_LMS_USER_ID',
    'type' => 'varchar',
    'audited' => false,
    'reportable' => true,
    'importable' => false,
    'len' => 36,
    'readonly'  => true,
    'duplicate_on_record_copy' => 'no',
    'group' => 'portal',
);
$dictionary['Contact']['fields']['lms_user_name'] = array (
    'name' => 'lms_user_name',
    'vname' => 'LBL_LMS_USER_NAME',
    'type' => 'varchar',
    'len' => '255',
    'readonly'  => true,
    'duplicate_on_record_copy' => 'no',
    'group' => 'portal',
);
$dictionary['Contact']['fields']['lms_user_password'] = array (
    'name' => 'lms_user_password',
    'vname' => 'LBL_LMS_USER_PASSWORD',
    'type' => 'varchar',
    'len' => '100',
    'readonly'  => true,
    'duplicate_on_record_copy' => 'no',
    'group' => 'portal',
);
$dictionary['Contact']['fields']['lms_content'] = array(
    'name' => 'lms_content',
    'vname' => 'LMS Content',
    'type' => 'text',
    'audited' => false,
    'reportable' => true,
    'importable' => false,
    'readonly'  => true,
    'duplicate_on_record_copy' => 'no',
    'group' => 'portal',
);


