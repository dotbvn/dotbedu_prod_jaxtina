<?php
// created: 2024-02-15 13:54:26
$dictionary["Contact"]["fields"]["c_gallery_contacts_1"] = array (
  'name' => 'c_gallery_contacts_1',
  'type' => 'link',
  'relationship' => 'c_gallery_contacts_1',
  'source' => 'non-db',
  'module' => 'C_Gallery',
  'bean_name' => 'C_Gallery',
  'vname' => 'LBL_C_GALLERY_CONTACTS_1_FROM_C_GALLERY_TITLE',
  'id_name' => 'c_gallery_contacts_1c_gallery_ida',
);
