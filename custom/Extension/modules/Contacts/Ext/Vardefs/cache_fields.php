<?php

$dictionary['Contact']['fields']['paid_amount'] = array(
    'required' => false,
    'name' => 'paid_amount',
    'vname' => 'LBL_PAID_AMOUNT',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 26,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);

$dictionary['Contact']['fields']['unpaid_amount'] = array(
    'required' => false,
    'name' => 'unpaid_amount',
    'vname' => 'LBL_UNPAID_AMOUNT',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 26,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);


$dictionary['Contact']['fields']['ost_hours'] = array(
    'required' => false,
    'name' => 'ost_hours',
    'vname' => 'LBL_OST_HOURS',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 13,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);

$dictionary['Contact']['fields']['remain_amount'] = array(
    'required' => false,
    'name' => 'remain_amount',
    'vname' => 'LBL_REMAIN_AMOUNT',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 26,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);



$dictionary['Contact']['fields']['total_ost_hours'] = array(
    'required' => false,
    'name' => 'total_ost_hours',
    'vname' => 'LBL_TOTAL_OST_HOURS',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 13,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);

$dictionary['Contact']['fields']['enrolled_hours'] = array(
    'required' => false,
    'name' => 'enrolled_hours',
    'vname' => 'LBL_ENROLLED_HOURS',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 13,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);

$dictionary['Contact']['fields']['enrolled_amount'] = array(
    'required' => false,
    'name' => 'enrolled_amount',
    'vname' => 'LBL_ENROLLED_AMOUNT',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 26,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);


$dictionary['Contact']['fields']['remain_carry'] = array(
    'required' => false,
    'name' => 'remain_carry',
    'vname' => 'LBL_REMAIN_CARRY',
    'type' => 'decimal',
    'massupdate' => false,
    'no_default' => false,
    'importable' => 'false',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => false,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'pii' => false,
    'default' => 0,
    'calculated' => false,
    'len' => 26,
    'size' => '20',
    'enable_range_search' => false,
    'precision' => 2,
);

$dictionary["Contact"]["fields"]["count_payment"] = array(
    'name' => 'count_payment',
    'type' => 'int',
    'len' => '10',
    'enable_range_search' => true,
    'options' => 'numeric_range_search_dom',
    'studio' => 'visible',
    'vname' => 'LBL_COUNT_PAYMENT',
    'default' => 0,
);

//Date, datetime fields
//LICENSE: Theses fields affects the student active lifetime
$dictionary['Contact']['fields']['last_payment_date'] = array(
    'name' => 'last_payment_date',
    'vname' => 'LBL_LAST_PAYMENT_DATE',
    'massupdate' => false,
    'type' => 'date',
);

$dictionary['Contact']['fields']['last_receipt_paid_date'] = array(
    'name' => 'last_receipt_paid_date',
    'vname' => 'LBL_LAST_RECEIPT_PAID_DATE',
    'massupdate' => false,
    'type' => 'date',
);
$dictionary['Contact']['fields']['max_end_study'] = array(
    'name' => 'max_end_study',
    'vname' => 'LBL_MAX_END_STUDY',
    'massupdate' => false,
    'type' => 'date',
);
$dictionary['Contact']['fields']['min_start_study'] = array(
    'name' => 'min_start_study',
    'vname' => 'LBL_MIN_START_STUDY',
    'massupdate' => false,
    'type' => 'date',
);
$dictionary['Contact']['fields']['stop_date'] = array(
    'name' => 'stop_date',
    'vname' => 'LBL_STOP_DATE',
    'massupdate' => false,
    'audited' => true,
    'type' => 'date',
    'dependency'=>'isInList("Stopped",createList($status))',
);
//END


