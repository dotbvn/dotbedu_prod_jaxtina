<?php

$dictionary['Campaign']['fields']['referencelog_link'] = array(
    'name' => 'referencelog_link',
    'type' => 'link',
    'relationship' => 'campaign_referencelog',
    'module' => 'J_ReferenceLog',
    'bean_name' => 'J_ReferenceLog',
    'source' => 'non-db',
    'vname' => 'LBL_REFERENCE_LOG',
    'reportable' => false
);

$dictionary['Campaign']['relationships']['campaign_referencelog'] = array(
    'lhs_module' => 'Campaigns',
    'lhs_table' => 'campaigns',
    'lhs_key' => 'id',
    'rhs_module' => 'J_ReferenceLog',
    'rhs_table' => 'j_referencelog',
    'rhs_key' => 'campaign_id',
    'relationship_type' => 'one-to-many',
);
