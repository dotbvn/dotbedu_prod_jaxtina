<?php
// created: 2023-04-18 18:12:13
$viewdefs['J_PaymentDetail']['base']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_J_BANKTRANS_J_PAYMENTDETAIL_1_FROM_J_BANKTRANS_TITLE',
  'context' => 
  array (
    'link' => 'j_banktrans_j_paymentdetail_1',
  ),
);