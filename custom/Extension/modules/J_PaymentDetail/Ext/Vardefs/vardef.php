<?php
// $dictionary['J_PaymentDetail']['fields']['e_invoice_token'] =array(
//     'name' => 'e_invoice_token',
//     'type' => 'varchar',
//     'label' => 'LBL_E_INVOICE_TOKEN',
//     'len' => '100',
// );
// $dictionary['J_PaymentDetail']['fields']['transaction_id'] =array(
//     'name' => 'transaction_id',
//     'type' => 'varchar',
//     'label' => 'LBL_TRANSACTION_ID',
//     'len' => '100',
// );
//API: TAPTAP
$dictionary['J_PaymentDetail']['fields']['send_inv_status'] = array(
    'required' => false,
    'name' => 'send_inv_status',
    'vname' => 'LBL_SEND_INV_STATUS',
    'type' => 'enum',
    'options' => 'send_inv_status_dom',
);
$dictionary['J_PaymentDetail']['fields']['external_connection'] = array(
    'required' => false,
    'name' => 'external_connection',
    'vname' => 'LBL_EXTERNAL_CONNECTION',
    'type' => 'text',
);
//END API TAPTAP
$dictionary['J_PaymentDetail']['fields']['text_currency'] =array(
    'name' => 'text_currency',
    'type' => 'varchar',
    'label' => 'LBL_TEXT_CURRENCY',
    'len' => '100',
    'source' => 'non-db',
);
//Đối soát tự động thông qua công thanh toán của Casso
$dictionary['J_PaymentDetail']['fields']['audited_amount'] = array(
    'required' => false,
    'name' => 'audited_amount',
    'vname' => 'LBL_AUDITED_AMOUNT',
    'type' => 'currency',
    'len' => 26,
    'size' => '20',
    'enable_range_search' => true,
    'options' => 'numeric_range_search_dom',
    'precision' => 6,
);
$dictionary['J_PaymentDetail']['fields']['audited_amount_text'] = array(
    'name' => 'audited_amount_text',
    'vname' => 'audited_amount_text',
    'type' => 'varchar',
    'len' => '1',
    'studio' => 'visible',
    'source' => 'non-db',
);

$dictionary['J_PaymentDetail']['fields']['reconcile_status'] = array(
    'required' => false,
    'name' => 'reconcile_status',
    'vname' => 'LBL_RECONCILE_STATUS',
    'type' => 'enum',
    'massupdate' => 0,
    'no_default' => false,
    'importable' => 'true',
    'duplicate_merge' => 'disabled',
    'duplicate_merge_dom_value' => '0',
    'audited' => true,
    'reportable' => true,
    'unified_search' => false,
    'merge_filter' => 'disabled',
    'calculated' => false,
    'len' => 100,
    'size' => '20',
    'options' => 'reconcile_status_list',
    'studio' => 'visible',
    'dependency' => false,
);

