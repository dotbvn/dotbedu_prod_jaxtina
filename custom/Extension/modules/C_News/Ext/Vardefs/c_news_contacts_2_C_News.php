<?php
// created: 2024-02-15 13:56:27
$dictionary["C_News"]["fields"]["c_news_contacts_2"] = array (
  'name' => 'c_news_contacts_2',
  'type' => 'link',
  'relationship' => 'c_news_contacts_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_C_NEWS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'c_news_contacts_2contacts_idb',
);
