<?php

$dictionary['C_News']['fields']['type'] = array(
    'required' => true,
    'name' => 'type',
    'vname' => 'LBL_TYPE',
    'type' => 'multienum',
    'dbType' => 'varchar',
    'default' => '^Student^,^Teacher^',
    'comments' => 'comment',
    'importable' => 'true',
    'duplicate_merge' => 'enabled',
    'duplicate_merge_dom_value' => '1',
    'audited' => true,
    'reportable' => true,
    'len' => 255,
    'size' => '20',
    'options' => 'apps_type_dom',
    'studio' => 'visible',
    'massupdate' => false,
    'isMultiSelect' => true,
);
$dictionary['C_News']['fields']['pin'] = array(
    'name' => 'pin',
    'vname' => 'LBL_PIN',
    'type' => 'bool',
    'default' => 0,
);
$dictionary['C_News']['fields']['pin_teacher'] = array(
    'name' => 'pin_teacher',
    'vname' => 'LBL_PIN_TEACHER',
    'type' => 'bool',
    'default' => 0,
);

$dictionary['C_News']['fields']['type_news'] = array(
    'name' => 'type_news',
    'vname' => 'LBL_TYPE_NEWS',
    'type' => 'enum',
    'options' => 'type_news_dom',
);

$dictionary['C_News']['fields']['send_notification'] = array(
    'name' => 'send_notification',
    'vname' => 'LBL_SEND_NOTIFICATION',
    'type' => 'bool',
    'default' => 0,
    'source' => 'non-db',
    'duplicate_on_record_copy' => 'no',
);

$dictionary['C_News']['fields']['views'] = array(
    'name' => 'views',
    'vname' => 'LBL_VIEWS',
    'type' => 'int',
    'default' => 0,
    'source' => 'non-db',
    'readonly' => true,
    'duplicate_on_record_copy' => 'no',
);

$dictionary['C_News']['fields']['addon_views'] = array(
    'name' => 'addon_views',
    'vname' => 'LBL_ADDON_VIEWS',
    'type' => 'int',
    'default' => 0,
//    'source' => 'non-db',
    'readonly' => true,
);
$dictionary['C_News']['fields']['status'] = array(
    'name'    => 'status',
    'vname'   => 'LBL_STATUS',
    'type'    => 'enum',
    'len'     => 100,
    'options' => 'c_new_status_list',
    'default' => 'Active',
);
$dictionary['C_News']['fields']['start_date']['dependency'] = 'equal($pin, 1)';
$dictionary['C_News']['fields']['end_date']['dependency'] = 'equal($pin, 1)';
$dictionary['C_News']['fields']['end_date']['required'] = false;
$dictionary['C_News']['fields']['end_date']['required'] = false;

$dictionary['C_News']['fields']['comments_link'] = array (
    'name' => 'comments_link',
    'type' => 'link',
    'relationship' => 'comments_c_news',
    'module' => 'C_Comments',
    'bean_name' => 'C_Comments',
    'source' => 'non-db',
    'vname' => 'LBL_COMMENT_LINK',
);

$dictionary['C_News']['relationships']['comments_c_news'] = array (
    'lhs_module'        => 'C_News',
    'lhs_table'            => 'c_news',
    'lhs_key'            => 'id',
    'rhs_module'        => 'C_Comments',
    'rhs_table'            => 'c_comments',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
    'relationship_role_column' => 'parent_type',
    'relationship_role_column_value' => 'C_News'
);

$dictionary["C_News"]["fields"]["total_like"] = array (
    'name' => 'total_like',
    'vname' => 'LBL_TOTAL_LIKE',
    'type' => 'int',
    "source"=>"non-db",
);
$dictionary["C_News"]["fields"]["news_content"] = array (
    'name' => 'news_content',
    'vname' => 'LBL_NEWS_CONTENT',
    'dbType' => 'longtext',
    'type' => 'htmleditable_tinymce',
);