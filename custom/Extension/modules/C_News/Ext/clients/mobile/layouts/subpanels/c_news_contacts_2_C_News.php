<?php
// created: 2024-02-15 13:56:27
$viewdefs['C_News']['mobile']['layout']['subpanels']['components'][] = array (
  'layout' => 'subpanel',
  'label' => 'LBL_C_NEWS_CONTACTS_2_FROM_CONTACTS_TITLE',
  'context' => 
  array (
    'link' => 'c_news_contacts_2',
  ),
);