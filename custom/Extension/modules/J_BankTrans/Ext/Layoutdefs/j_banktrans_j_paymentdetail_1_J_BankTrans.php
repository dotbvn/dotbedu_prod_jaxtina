<?php
 // created: 2023-04-18 18:12:13
$layout_defs["J_BankTrans"]["subpanel_setup"]['j_banktrans_j_paymentdetail_1'] = array (
  'order' => 100,
  'module' => 'J_PaymentDetail',
  'subpanel_name' => 'ForJ_paymentPayment_paymentdetails',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_J_BANKTRANS_J_PAYMENTDETAIL_1_FROM_J_PAYMENTDETAIL_TITLE',
  'get_subpanel_data' => 'j_banktrans_j_paymentdetail_1',
  'top_buttons' =>
  array (

  ),
);
