<?php

$dictionary['RevenueLineItem']['fields']['parent_type'] = array(
    'name' => 'parent_type',
    'vname' => 'LBL_PARENT_TYPE',
    'type' => 'parent_type',
    'dbType' => 'varchar',
    'required' => false,
    'group' => 'parent_name',
    'options' => 'parent_type_display',
    'len' => 255,
    'studio' => array('wirelesslistview' => false),
    'comment' => 'The Dotb object to which the call is related'
);
$dictionary['RevenueLineItem']['fields']['parent_id'] = array(
    'name' => 'parent_id',
    'vname' => 'LBL_PARENT_ID',
    'type' => 'id',
    'group' => 'parent_name',
    'reportable' => false,
    'comment' => 'The ID of the parent Dotb object identified by parent_type',
    'required' => true
);
$dictionary['RevenueLineItem']['fields']['parent_name'] = array(
    'name' => 'parent_name',
    'parent_type' => 'record_type_display',
    'type_name' => 'parent_type',
    'id_name' => 'parent_id',
    'vname' => 'LBL_PARENT_NAME',
    'type' => 'parent',
    'group' => 'parent_name',
    'source' => 'non-db',
    'options' => 'parent_type_display',
    'studio' => true,
    'required' => true
);
$dictionary['RevenueLineItem']['fields']['opportunity_name']['populate_list'] = array(
    'parent_id' => 'parent_id',
    'parent_name' => 'parent_name'
);
$dictionary['RevenueLineItem']['fields']['total_amount']['populate_list'] = array(
    'parent_id' => 'parent_id',
    'parent_name' => 'parent_name'
);
$dictionary['RevenueLineItem']['fields']['best_case']['formula'] = 'string($total_amount)';
$dictionary['RevenueLineItem']['fields']['likely_case']['formula'] = 'string($total_amount)';
$dictionary['RevenueLineItem']['fields']['worst_case']['formula'] = 'string($total_amount)';