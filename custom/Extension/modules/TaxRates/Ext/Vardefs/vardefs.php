<?php
//Add Relationship Tax Rate - Book/gift
$dictionary['TaxRate']['fields']['producttemplate_link'] = array(
    'name' => 'producttemplate_link',
    'type' => 'link',
    'relationship' => 'taxrate_producttemplates',
    'module' => 'ProductTemplates',
    'bean_name' => 'ProductTemplate',
    'source' => 'non-db',
    'vname' => 'LBL_PRODUCT_TEMPLATE',
);
$dictionary['TaxRate']['relationships']['taxrate_producttemplates'] = array(
    'lhs_module' => 'TaxRates',
    'lhs_table' => 'taxrates',
    'lhs_key' => 'id',
    'rhs_module' => 'ProductTemplates',
    'rhs_table' => 'product_templates',
    'rhs_key' => 'taxrate_id',
    'relationship_type' => 'one-to-many'
);
