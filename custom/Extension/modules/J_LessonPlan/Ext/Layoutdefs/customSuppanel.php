<?php

$layout_defs["J_LessonPlan"]["subpanel_setup"]["j_lessonplan_classes"] = array (
    'order' => 1,
    'module' => 'J_Class',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_J_LESSONPLAN_CLASSES',
    'sort_order' => 'asc',
    'sort_by' => 'start_date',
    'get_subpanel_data' => 'j_lessonplan_classes',
    'top_buttons' => array (),
);
$layout_defs["J_LessonPlan"]["subpanel_setup"]["j_lessonplan_koc"] = array (
    'order' => 2,
    'module' => 'J_Kindofcourse',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_J_LESSONPLAN_KOC',
    'sort_order' => '',
    'sort_by' => '',
    'get_subpanel_data' => 'function:getKindOfCourse',
    'function_parameters' => array(
        'import_function_file' => 'custom/modules/J_LessonPlan/customSubPanel.php',
        'lessonplan_id' => $this->_focus->id,
        'return_as_array' => 'true'
    ),
    'top_buttons' => array (),
);