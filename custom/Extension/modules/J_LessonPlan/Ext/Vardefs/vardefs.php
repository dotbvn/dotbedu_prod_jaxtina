<?php
$dictionary["J_LessonPlan"]["fields"]["syllabus_config"] = array(
    'name' => 'syllabus_config',
    'vname' => 'LBL_SYLLABUS_CONFIG',
    'type' => 'varchar',
    'source' => 'non-db',
    'readonly' => true,
    'duplicate_on_record_copy' => 'no',
);
$dictionary['J_LessonPlan']['fields']['short_syllabus'] = array(
    'name' => 'short_syllabus',
    'vname' => 'LBL_SHORT_SYLLABUS',
    'type' => 'varchar',
    'source' => 'non-db',
    'readonly' => true,
    'duplicate_on_record_copy' => 'no',
);
$dictionary['J_LessonPlan']['fields']['is_copy_from_current_lp'] = array(
    'name' => 'is_copy_from_current_lp',
    'vname' => 'LBL_COPY_FROM_CURRENT_LP',
    'type' => 'varchar',
    'source' => 'non-db',
    'duplicate_on_record_copy' => 'no',
);
$dictionary['J_LessonPlan']['fields']['j_lessonplan_classes'] = array(
    'name' => 'j_lessonplan_classes',
    'vname' => 'LBL_J_LESSONPLAN_CLASSES',
    'type' => 'link',
    'source' => 'non-db',
    'relationship' => 'j_lessonplan_j_class',
    'module' => 'J_Class',
    'bean_name' => 'J_Class',
);

// Relationships
$dictionary['J_LessonPlan']['relationships']['j_lessonplan_j_class'] = array(
    'lhs_module' => 'J_LessonPlan',
    'lhs_table' => 'j_lessonplan',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Class',
    'rhs_table' => 'j_class',
    'rhs_key' => 'lessonplan_id',
    'relationship_type' => 'one-to-many'
);