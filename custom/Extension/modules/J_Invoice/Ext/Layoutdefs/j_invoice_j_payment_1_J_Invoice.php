<?php
 // created: 2022-05-25 11:05:23
$layout_defs["J_Invoice"]["subpanel_setup"]['j_invoice_j_payment_1'] = array (
  'order' => 100,
  'module' => 'J_Payment',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_PAYMENT',
  'get_subpanel_data' => 'j_invoice_j_payment_1',
  'top_buttons' =>
  array (

  ),
);

$layout_defs["J_Invoice"]["subpanel_setup"]["invoice_paymentdetail"] = array (
    'order' => 101,
    'module' => 'J_PaymentDetail',
    'subpanel_name' => 'default',
    'title_key' => 'LBL_PAYMENT_DETAIL',
    'sort_order' => 'asc',
    'sort_by' => 'payment_no',
    'get_subpanel_data' => 'paymentdetail_link',
    'top_buttons' =>
    array (

    ),
);
