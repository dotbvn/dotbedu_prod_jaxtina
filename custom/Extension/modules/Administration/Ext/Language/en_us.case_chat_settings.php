<?php
$mod_strings['LBL_EXS_CASE_CHAT_SETTINGS_TITLE'] = 'Case Chat';
$mod_strings['LBL_EXS_CASE_CHAT_SETTINGS_DESC'] = 'Configurations For Exsitec Case Chat Plugin';
$mod_strings['LBL_EXS_CASE_CHAT_SETTINGS_LINK_NAME'] = 'Settings';
$mod_strings['LBL_EXS_CASE_CHAT_SETTINGS_LINK_DESC'] = 'Change Settings';