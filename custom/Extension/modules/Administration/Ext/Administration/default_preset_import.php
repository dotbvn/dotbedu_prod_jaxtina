<?php
//add by TKT 27-2-2019
$admin_option_defs = array();
$admin_option_defs['Administration']['default_preset_import'] = array(
    'Administration',
    'LBL_LINK_AUTO_SET_PRESET_IMPORT',
    'LBL_LINK_AUTO_SET_PRESET_IMPORT',
    'javascript:void(parent.DOTB.App.router.navigate("EMS_Settings/layout/default-preset-import", {trigger: true}));',
);
$admin_option_defs['Administration']['google_api'] = array(
    'Administration',
    'LBL_LINK_GOOGLE_API_CONFIG',
    'LBL_LINK_GOOGLE_API_CONFIG',
    'javascript:void(parent.DOTB.App.router.navigate("EMS_Settings/layout/google_api_config", {trigger: true}));',
);
$admin_group_header[] = array('LBL_SECTION_GENERAL_SETTING', '', false, $admin_option_defs, 'LBL_SECTION_GENERAL_SETTING');