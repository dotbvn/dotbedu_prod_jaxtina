<?php

$admin_option_defs = array();
$admin_option_defs['Administration']['exs_case_chat_settings'] = array(
    'Administration',
    'LBL_EXS_CASE_CHAT_SETTINGS_LINK_NAME',
    'LBL_EXS_CASE_CHAT_SETTINGS_LINK_DESC',
    'javascript:parent.DOTB.App.router.navigate("Home/layout/case-chat-settings", {trigger: true});',
);

$admin_group_header[] = array(
    'LBL_EXS_CASE_CHAT_SETTINGS_TITLE',
    '',
    false,
    $admin_option_defs,
    'LBL_EXS_CASE_CHAT_SETTINGS_DESC'
);