<?php
// created: 2023-04-18 18:12:13
$dictionary["j_banktrans_j_paymentdetail_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'j_banktrans_j_paymentdetail_1' => 
    array (
      'lhs_module' => 'J_BankTrans',
      'lhs_table' => 'j_banktrans',
      'lhs_key' => 'id',
      'rhs_module' => 'J_PaymentDetail',
      'rhs_table' => 'j_paymentdetail',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_banktrans_j_paymentdetail_1_c',
      'join_key_lhs' => 'banktrans_id',
      'join_key_rhs' => 'receipt_id',
    ),
  ),
  'table' => 'j_banktrans_j_paymentdetail_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'banktrans_id' => 
    array (
      'name' => 'banktrans_id',
      'type' => 'id',
    ),
    'receipt_id' => 
    array (
      'name' => 'receipt_id',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_banktrans_j_paymentdetail_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_j_banktrans_j_paymentdetail_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'banktrans_id',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_j_banktrans_j_paymentdetail_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'receipt_id',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_banktrans_j_paymentdetail_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'banktrans_id',
        1 => 'receipt_id',
      ),
    ),
  ),
);