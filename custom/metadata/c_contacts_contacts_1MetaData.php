<?php
// created: 2019-03-20 17:04:05
$dictionary["c_contacts_contacts_1"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'c_contacts_contacts_1' => 
    array (
      'lhs_module' => 'C_Contacts',
      'lhs_table' => 'c_contacts',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'c_contacts_contacts_1_c',
      'join_key_lhs' => 'c_contacts_contacts_1c_contacts_ida',
      'join_key_rhs' => 'c_contacts_contacts_1contacts_idb',
    ),
  ),
  'table' => 'c_contacts_contacts_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'c_contacts_contacts_1c_contacts_ida' => 
    array (
      'name' => 'c_contacts_contacts_1c_contacts_ida',
      'type' => 'id',
    ),
    'c_contacts_contacts_1contacts_idb' => 
    array (
      'name' => 'c_contacts_contacts_1contacts_idb',
      'type' => 'id',
    ),
    'parent_type' =>
    array (
      'name' => 'parent_type',
      'type' => 'varchar',
      'len'  => 10,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_c_contacts_contacts_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_c_contacts_contacts_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_contacts_contacts_1c_contacts_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_c_contacts_contacts_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_contacts_contacts_1contacts_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'c_contacts_contacts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'c_contacts_contacts_1contacts_idb',
      ),
    ),
  ),
);