<?php
// created: 2020-09-29 10:36:07
$dictionary["j_teacherqe_j_class"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'j_teacherqe_j_class' => 
    array (
      'lhs_module' => 'J_Class',
      'lhs_table' => 'j_class',
      'lhs_key' => 'id',
      'rhs_module' => 'J_TeacherQE',
      'rhs_table' => 'j_teacherqe',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_teacherqe_j_class_c',
      'join_key_lhs' => 'j_teacherqe_j_classj_class_ida',
      'join_key_rhs' => 'j_teacherqe_j_classj_teacherqe_idb',
    ),
  ),
  'table' => 'j_teacherqe_j_class_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'j_teacherqe_j_classj_class_ida' => 
    array (
      'name' => 'j_teacherqe_j_classj_class_ida',
      'type' => 'id',
    ),
    'j_teacherqe_j_classj_teacherqe_idb' => 
    array (
      'name' => 'j_teacherqe_j_classj_teacherqe_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_teacherqe_j_class_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_j_teacherqe_j_class_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_teacherqe_j_classj_class_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_j_teacherqe_j_class_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_teacherqe_j_classj_teacherqe_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_teacherqe_j_class_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'j_teacherqe_j_classj_teacherqe_idb',
      ),
    ),
  ),
);