<?php
// created: 2022-05-25 11:05:23
$dictionary["j_invoice_j_payment_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'j_invoice_j_payment_1' => 
    array (
      'lhs_module' => 'J_Invoice',
      'lhs_table' => 'j_invoice',
      'lhs_key' => 'id',
      'rhs_module' => 'J_Payment',
      'rhs_table' => 'j_payment',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_invoice_j_payment_1_c',
      'join_key_lhs' => 'invoice_id',
      'join_key_rhs' => 'payment_id',
    ),
  ),
  'table' => 'j_invoice_j_payment_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'invoice_id' => 
    array (
      'name' => 'invoice_id',
      'type' => 'id',
    ),
    'payment_id' => 
    array (
      'name' => 'payment_id',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_invoice_j_payment_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_j_invoice_j_payment_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'invoice_id',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_j_invoice_j_payment_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'payment_id',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_invoice_j_payment_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'invoice_id',
        1 => 'payment_id',
      ),
    ),
  ),
);