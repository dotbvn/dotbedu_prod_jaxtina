<?php
// created: 2020-09-29 10:36:07
$dictionary["j_teacherqe_c_teachers"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'j_teacherqe_c_teachers' => 
    array (
      'lhs_module' => 'C_Teachers',
      'lhs_table' => 'c_teachers',
      'lhs_key' => 'id',
      'rhs_module' => 'J_TeacherQE',
      'rhs_table' => 'j_teacherqe',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_teacherqe_c_teachers_c',
      'join_key_lhs' => 'j_teacherqe_c_teachersc_teachers_ida',
      'join_key_rhs' => 'j_teacherqe_c_teachersj_teacherqe_idb',
    ),
  ),
  'table' => 'j_teacherqe_c_teachers_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'j_teacherqe_c_teachersc_teachers_ida' => 
    array (
      'name' => 'j_teacherqe_c_teachersc_teachers_ida',
      'type' => 'id',
    ),
    'j_teacherqe_c_teachersj_teacherqe_idb' => 
    array (
      'name' => 'j_teacherqe_c_teachersj_teacherqe_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_teacherqe_c_teachers_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_j_teacherqe_c_teachers_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_teacherqe_c_teachersc_teachers_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_j_teacherqe_c_teachers_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_teacherqe_c_teachersj_teacherqe_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_teacherqe_c_teachers_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'j_teacherqe_c_teachersj_teacherqe_idb',
      ),
    ),
  ),
);