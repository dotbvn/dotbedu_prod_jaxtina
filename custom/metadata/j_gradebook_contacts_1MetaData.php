<?php
// created: 2021-02-01 09:34:27
$dictionary["j_gradebook_contacts_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'j_gradebook_contacts_1' => 
    array (
      'lhs_module' => 'J_Gradebook',
      'lhs_table' => 'j_gradebook',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'j_gradebook_contacts_1_c',
      'join_key_lhs' => 'j_gradebook_contacts_1j_gradebook_ida',
      'join_key_rhs' => 'j_gradebook_contacts_1contacts_idb',
    ),
  ),
  'table' => 'j_gradebook_contacts_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'j_gradebook_contacts_1j_gradebook_ida' => 
    array (
      'name' => 'j_gradebook_contacts_1j_gradebook_ida',
      'type' => 'id',
    ),
    'j_gradebook_contacts_1contacts_idb' => 
    array (
      'name' => 'j_gradebook_contacts_1contacts_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_j_gradebook_contacts_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_j_gradebook_contacts_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_gradebook_contacts_1j_gradebook_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_j_gradebook_contacts_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'j_gradebook_contacts_1contacts_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'j_gradebook_contacts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'j_gradebook_contacts_1j_gradebook_ida',
        1 => 'j_gradebook_contacts_1contacts_idb',
      ),
    ),
  ),
);