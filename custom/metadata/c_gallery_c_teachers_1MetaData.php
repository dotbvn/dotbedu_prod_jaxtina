<?php
// created: 2024-03-01 10:13:26
$dictionary["c_gallery_c_teachers_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'c_gallery_c_teachers_1' => 
    array (
      'lhs_module' => 'C_Gallery',
      'lhs_table' => 'c_gallery',
      'lhs_key' => 'id',
      'rhs_module' => 'C_Teachers',
      'rhs_table' => 'c_teachers',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'c_gallery_c_teachers_1_c',
      'join_key_lhs' => 'c_gallery_c_teachers_1c_gallery_ida',
      'join_key_rhs' => 'c_gallery_c_teachers_1c_teachers_idb',
    ),
  ),
  'table' => 'c_gallery_c_teachers_1_c',
  'fields' => 
  array (
    'id' => 
    array (
      'name' => 'id',
      'type' => 'id',
    ),
    'date_modified' => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    'deleted' => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'default' => 0,
    ),
    'c_gallery_c_teachers_1c_gallery_ida' => 
    array (
      'name' => 'c_gallery_c_teachers_1c_gallery_ida',
      'type' => 'id',
    ),
    'c_gallery_c_teachers_1c_teachers_idb' => 
    array (
      'name' => 'c_gallery_c_teachers_1c_teachers_idb',
      'type' => 'id',
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'idx_c_gallery_c_teachers_1_pk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'idx_c_gallery_c_teachers_1_ida1_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_gallery_c_teachers_1c_gallery_ida',
        1 => 'deleted',
      ),
    ),
    2 => 
    array (
      'name' => 'idx_c_gallery_c_teachers_1_idb2_deleted',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'c_gallery_c_teachers_1c_teachers_idb',
        1 => 'deleted',
      ),
    ),
    3 => 
    array (
      'name' => 'c_gallery_c_teachers_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'c_gallery_c_teachers_1c_gallery_ida',
        1 => 'c_gallery_c_teachers_1c_teachers_idb',
      ),
    ),
  ),
);