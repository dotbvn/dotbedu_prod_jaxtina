<?php

class LicenseStatus {
    private $_trial = 'ACTIVE-TRIAL';
    private $_onDemand = 'ACTIVE-ON-DEMAND';
    private $_expired = 'EXPIRED';

    public function noLicense() {
        return null;
    }

    public function trial() {
        return $this->_trial;
    }

    public function onDemand() {
        return $this->_onDemand;
    }

    public function expired() {
        return $this->_expired;
    }

    public function isActive($status) : bool {
        if (isset($status) && in_array($status, array($this->_trial, $this->onDemand()))) {
            return true;
        }

        return  false;
    }
}