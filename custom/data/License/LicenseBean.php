<?php

class LicenseBean
{
    /// public attribute
    public $lic_tenant_id;
    public $lic_tenant_name;
    public $lic_status;
    public $lic_start_date;
    public $lic_expired_date;
    public $lic_users;
    public $lic_subscription;
    public $lic_plan_id;
    public $lic_billing_cycle;
    public $lic_last_validation_success;
    public $lic_currency;
    public $lic_country;

    /// Attributes that usually change value
    public $lic_activated_users;
    public $lic_activated_subscription;
    /// private attribute
    private $license_map;

    private $ignoredModule;

    public function __construct()
    {
        $this->license_map = array(
            "lic_tenant_id",
            "lic_tenant_name",
            "lic_status",
            "lic_start_date",
            "lic_expired_date",
            "lic_users",
            "lic_subscription",
            "lic_plan_id",
            "lic_billing_cycle",
            "lic_country",
            "lic_currency",
            "lic_activated_users",
            "lic_activated_subscription",
            "lic_last_validation_success"
        );

        $this->ignoredModule = array(
            'bulk',
            'Notifications',
            'Calls',
            'Meetings',
            'me',
            'callcenter',
            'Dashboards',
            'Filters',
            'metadata',
            'lang',
            'connectors',
            'Bugs',
            'Meetings',
            'PdfManager',
            'Emails',
            'oauth2'
        );
    }

    public function fromJson($arg, $fromAdminSetting = false): void
    {
        $tag = '';

        if ($fromAdminSetting) {
            $tag = 'license_';
        }

        $index = 0;
        while ($index < count($this->license_map)) {
            $key = $this->license_map[$index];
            $this->$key = $arg[$tag.$key];
            $index++;
        }
    }

    public function toJson(): array
    {
        $temp = array();

        $index = 0;
        while ($index < count($this->license_map)) {
            $key = $this->license_map[$index];
            $temp[$key] = $this->$key;
            $index++;
        }

        return $temp;
    }

        function save(): void
        {
        $admin = new Administration();
        $index = 0;
        while ($index < count($this->license_map)) {
            $key = $this->license_map[$index];
            if (isset($this->$key)) {
                $admin->saveSetting('license', $key, $this->$key);
            }
            $index++;
        }
        $admin->saveConfig();
    }

    public function isActivated() : bool //// Hàm này nên được dùng cho model license được trả về từ Quyettam
    {
        if (isset($this->lic_start_date) && isset($this->lic_expired_date)) {
            $valid_active_date = date_diff(date_create(date('Y-m-d H:i')), date_create($this->lic_start_date))->invert;
            $valid_expired_date = date_diff(date_create($this->lic_expired_date), date_create(date('Y-m-d H:i')))->invert;

            $status = new LicenseStatus();
            if ($valid_active_date
                && $valid_expired_date
                && ($this->lic_status === $status->trial() || $this->lic_status === $status->onDemand())) {
                return true;
            }
        }

        return false;
    }

    public function fromClient(): void
    {
        $sql = sprintf("SELECT %s FROM %s WHERE category = '%s'",
            'category, name, value',
            'config',
            'license'
        );

        $result = $GLOBALS['db']->fetchArray($sql);
        $json = array();

        if (!empty($result)) {
            foreach ($result as $key) {
                $json[$key['name']] = $key['value'];
            }
        }

        $this->fromJson($json);
    }

    public function fromSession() {
        if (isset($_SESSION['license']))
        {
            $this->fromJson($_SESSION['license']);
        } else {
            $this->fromClient();
        }
    }

    static function getLicenseRemainingDays($expired_date) : int
    {
        $date_diff = date_diff(date_create($expired_date), date_create(date('Y-m-d H:i:s')));

        if ($date_diff->invert) {
            return $date_diff->days;
        } else {
            return 0;
        }
    }

    public function countTotalSubscription(): void
    {
        $sql = sprintf("SELECT count(%s) as %s FROM %s WHERE %s = %s AND %s = %s",
            'id',
            'total_active',
            'contacts',
            'contacts.ems_active_state',
            1,
            'contacts.deleted',
            0
        );

        $result = $GLOBALS['db']->fetchOne($sql);

        if (isset($result['total_active'])) {
            $this->lic_activated_subscription = (int)$result['total_active'];
        }
    }

    public function countTotalUser(): void
    {
        $sql = "SELECT 
                    COUNT(DISTINCT id) as total_active
                FROM
                    users
                WHERE
                    user_name NOT IN ('admin' , 'app_admin')
                        AND (teacher_id IS NULL
                        OR teacher_id = '')
                        AND status = 'Active'
                        AND (user_name IS NOT NULL AND user_name <> '')
                        AND deleted = 0";

        $result = $GLOBALS['db']->fetchOne($sql);

        if (isset($result['total_active'])) {
            $this->lic_activated_users = (int)$result['total_active'];
        }
    }

    public function getIgnoreModules() {
        return $this->ignoredModule;
    }
}

require_once 'LicenseStatus.php';