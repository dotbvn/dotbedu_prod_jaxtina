<?php

/// check session lifespan
function checkLicenseSession() {
    if (isset($_SESSION['license']['lic_latest_refresh'])) {
        $time_pass = (strtotime("now") - strtotime($_SESSION['license']['lic_latest_refresh'])) / 60; /// to minutes
        $valid_session = ($time_pass < $GLOBALS['dotb_config']['license_session_duration'] ?? 30) /// 30 minutes as default
            && $time_pass > 0;

        if (!$valid_session) {
            refreshLicenseSession();
        }

    } else {
        refreshLicenseSession();
    }
}

//// Full refresh session
function refreshLicenseSession(): void
{
    $licenseBean = new LicenseBean();
    $licenseBean->fromClient();
    $licenseBean->countTotalSubscription();
    $licenseBean->countTotalUser();
    $remaining_days = LicenseBean::getLicenseRemainingDays($licenseBean->lic_expired_date);
    $_SESSION['license'] = $licenseBean->toJson();
    if (isset($_SESSION['license']['tenant_id'])) {
        $_SESSION['license']['remaining_days'] = $remaining_days;
        $_SESSION['license']['latest_refresh'] = date('Y-m-d H:i:s');
    }
    $licenseBean->save();
    unset($license);
}

//// create global license
/// after check session
function globalLicenseFromSession(): void
{
    global $license;

    $licenseBean = new LicenseBean();
    $licenseBean->fromSession();
    $remaining_days = LicenseBean::getLicenseRemainingDays($licenseBean->lic_expired_date);

    /// update total student if null
    if (empty($licenseBean->lic_activated_subscription)) {
        $licenseBean->countTotalSubscription();
    }

    /// update total student if null
    if (empty($licenseBean->lic_activated_users)) {
        $licenseBean->countTotalUser();
    }

    $license = $licenseBean->toJson();
    $license['lic_remaining_days'] = $remaining_days;
}

function refreshGlobalLicense(): void
{
    if ( isset($_REQUEST['__dotb_url']) && $_REQUEST['__dotb_url'] === 'v11_3/bulk') {
        return;
    }

    global $license;
    $license = new LicenseBean();
    $license->fromClient();
    $remaining_days = LicenseBean::getLicenseRemainingDays($license->lic_expired_date);

    /// update total student if null
    if (empty($license->lic_activated_subscription)) {
        $license->countTotalSubscription();
        $license->save();
    }

    /// update total student if null
    if (empty($license->lic_activated_users)) {
        $license->countTotalUser();
        $license->save();
    }

    $license = $license->toJson();
    $license['lic_remaining_days'] = $remaining_days;

    /// change to maintenance mode if license is not activated
    $status = new LicenseStatus();
    if (!in_array($license['lic_status'], array($status->trial(), $status->onDemand()))) {
        $GLOBALS['dotb_config']['maintenanceMode'] = true;
    }
}

function checkLicenseBWC() {
    global $license;

    /// validate license
    if (isset($GLOBALS['current_user']->id)){
        if (($_REQUEST['module'] == 'EMS_Settings' && $_REQUEST['action'] == 'subscription') || ($_REQUEST['module'] == 'Administration' && in_array($_REQUEST['action'], ['repair', 'Upgrade'])))
            return;
        $status = new LicenseStatus();
        if (!$status->isActive($license['lic_status'])) {
            header("Location: index.php?module=EMS_Settings&action=subscription");
            echo "<script>
                         app.alert.show('warning', {
                             level: 'warning',
                             title: app.lang.get('LIL_SUBSCRIPTION_PLAN_LIMITED', 'EMS_Settings'),
                             messages: app.lang.get('LIL_SUBSCRIPTION_LIMITED_MESSAGES', 'EMS_Settings')
                         });
                       </script>";
        }
    }
}