<?php
/**
* Kiểm tra giáo viên đã dạy session nào trong khoảng thời gian truyển vào hay không
*
* @param id         : teacher id
* @param db_start   : yyyy-mm-dd hh:mm:ss
* @param db_end     : yyyy-mm-dd hh:mm:ss
*
* @return if teacher is free: true || if busy: false
*/
function checkTeacherInDateime($id= '', $db_start= '', $db_end= '', $session_id = ''){
    $sql = "SELECT DISTINCT id FROM meetings
    WHERE teacher_id = '$id'
    AND ((('$db_start' >= date_start) AND ('$db_start' < date_end))
    OR (('$db_end' > date_start) AND ('$db_end' <= date_end))
    OR (('$db_start' < date_start) AND ('$db_end' > date_end)))
    AND deleted=0
    AND (timesheet_id = '' OR timesheet_id IS NULL)
    AND session_status <> 'Cancelled' AND id <> '$session_id' ";
    $id = $GLOBALS['db']->getOne($sql);

    if(!empty($id)) return false;
    else return true;
}
/**
* Kiểm tra xem giáo viên đã dạy session nào trùng thời gian với session truyển vào hay không
*
* @return if teacher is free: true || if busy: false
*/
function checkTeacherInSession($teacherId= '', $sessionId= ''){
    $sqlGetSession = "SELECT date_start, date_end
    FROM meetings
    WHERE id = '{$sessionId}' AND session_status <> 'Cancelled'";
    $rs = $GLOBALS['db']->query($sqlGetSession);
    $row = $GLOBALS['db']->fetchByAssoc($rs);

    return checkTeacherInDateime($teacherId,$row['date_start'],$row['date_end']);
}

/**
* Check priority of teacher
*
* @param teacherId  : teacher id
* @param classId    : class id
* @param dayOff     : teacher day off: array("Monday", "Tuesday",...)
* @param dayOfWeek  : day of session in week:  array("Monday", "Tuesday",...)
* @param dayOfWoeek : array("Mon","Tue")
*
* @return 1 or 2 or 3
*/
function checkTeacherPriority($teacherId= '', $teaFromPrev= '', $dayOff= '', $dayOfWeek= '', $holidays= ''){
    //Xet Day offf
    $days_name = array();
    foreach($dayOfWeek as $key => $value)
        $days_name[] =  trim(substr($value,0, strpos($value, '|') - 1));

    foreach ($dayOff as $value){
        $day_off = date('l',strtotime($value));
        if (in_array($day_off,$days_name))
            return "3";
    }
    if($holidays > 0) return "3";


    //Xet uu tien teacher day lop truoc
    if(!empty($teaFromPrev)){
        if(in_array($teacherId,$teaFromPrev))
            return "1";
    }

    //Teacher muc do trung binh
    return "2";
}

/**
* check teacher holidays in a list of session
*
* @param teacherId  : teacher id
* @param sessions   : array of session
*
* @return number of holidays
*/
function checkTeacherHolidays($teacherId= '', $session_date= ''){
    global $timedate;
    $sql = "SELECT IFNULL(holiday_date, '') holiday_date
    FROM holidays
    WHERE teacher_id = '$teacherId'
    AND holiday_date in ($session_date)
    AND deleted=0";
    $res = $GLOBALS['db']->query($sql);
    $listHoliday = array();
    while ($row = $GLOBALS['db']->fetchByAssoc($res)){
        $listHoliday[] = date_format(date_create($row['holiday_date']), 'D') . ' - ' . $timedate->to_display_date($row['holiday_date']);
    }
    return $listHoliday;
}

function sortTeacherListByTaughtHour($teacherList= ''){
    $hourList = array();
    foreach($teacherList as $value) {
        $hour = $value["total_hour"];
        if (!in_array($hour,$hourList)) $hourList[] = $hour;
    }
    sort($hourList,1);

    $result = array();
    foreach($hourList as $hourItem) {
        foreach($teacherList as $teacherId => $teacher) {
            if ($teacher["total_hour"] == $hourItem) $result[$teacherId] = $teacher;
        }
    }
    return $result;
}

/**
* Get available teachers for a class
*
* @param classId    : class id
* @param date_start : display format ex:dd/mm/yyyy
* @param date_end   : display format ex:dd/mm/yyyy
* @param dayOfWoeek : array("Monday","Tuesday")
*
* @return array of teachers info
*/
function getClassSession($classId= '', $date_start= '', $date_end= '', $dayOfWeek= ''){
    global $timedate;

    if(!empty($date_start)){
        $start_tz     = date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($date_start,false)." 00:00:00"));
        $ext_start = "AND (meetings.date_start >= '$start_tz')";
    }
    if(!empty($date_end)){
        $end_tz     = date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($date_end,false)." 23:59:59"));
        $ext_end = "AND (meetings.date_end <= '$end_tz')";
    }

    $ext_date  = $schedules = array();
    foreach($dayOfWeek as $key => $value){
        $part_      = explode("|", str_replace( ' ', '', $value));
        $day_name   = $part_[0];
        $time_slot  = explode("-", $part_[1]);
        $scheduleBy = $part_[2];

        $schedules[] =  array('week_date'  => $part_[0],'time_slot'  => $part_[1],'schedule_by'=> $part_[2],);
        if($time_slot[0] == 'select_all' || empty($time_slot[0]) || empty($time_slot[1]))
            $ext_date[] = "DAYNAME(CONVERT_TZ(meetings.date_start,'+0:00','+7:00')) = '$day_name'";
        else
            $ext_date[] = "DAYNAME(CONVERT_TZ(meetings.date_start,'+0:00','+7:00')) = '$day_name'
            AND DATE_FORMAT(CONVERT_TZ(meetings.date_start,'+0:00','+7:00'), '%H:%i') = '{$time_slot[0]}'
            AND DATE_FORMAT(CONVERT_TZ(meetings.date_end,'+0:00','+7:00'), '%H:%i') = '{$time_slot[1]}'";
    }

    $ext_date_string = "AND ((".implode(") OR (",$ext_date)."))";

    $sqlGetSessionList = "SELECT DISTINCT IFNULL(meetings.id, '') meeting_id,
    IFNULL(meetings.external_id, '') external_id, IFNULL(meetings.join_url, '') join_url,
    IFNULL(meetings.type, '') type, IFNULL(meetings.teacher_id, '') teacher_id, IFNULL(meetings.lesson_number,0) lesson_number,
    IFNULL(meetings.teacher_cover_id, '') ta1_id, IFNULL(meetings.sub_teacher_id, '') ta2_id, l1.start_date class_start,
    IFNULL(meetings.room_id, '') room_id, meetings.date_start time_start, meetings.date_end time_end,
    DATE_FORMAT(CONVERT_TZ(meetings.date_start,'+0:00','+7:00'),'%Y-%m-%d') start_date,
    DATE_FORMAT(CONVERT_TZ(meetings.date_start,'+0:00','+7:00'),'%W') week_date
    FROM meetings INNER JOIN j_class l1 ON l1.id = meetings.ju_class_id AND l1.deleted = 0 AND l1.id = '$classId'
    WHERE meetings.deleted = 0 $ext_start $ext_end $ext_date_string
    AND meetings.session_status <> 'Cancelled'
    ORDER BY time_start";
    $list = $GLOBALS['db']->fetchArray($sqlGetSessionList);
    $first_list = reset($list);
    //Xét lịch tuần chẵn/ tuần lẻ / buổi chẵn / buổi lẽ
    foreach ($list as $ind_ => $row){
        foreach($schedules as $key => $value){
            if($value['week_date'] == $row['week_date'] && !empty($value['schedule_by'])){
                $wStart1 = strtotime($first_list['start_date']);
                $wStart2 = (int)$row['lesson_number'];
                $wRun    = strtotime($row['start_date']);
                $res     = 0;
                if(in_array($value['schedule_by'], ['even_lesson','odd_lesson'])){
                    if($value['schedule_by'] == 'even_lesson' && $wStart2%2 == 0) $res=1;
                    if($value['schedule_by'] == 'odd_lesson' && $wStart2%2 == 1) $res=1;
                }elseif(in_array($value['schedule_by'], ['even_week','odd_week']))
                    $res = checkScheduleBy($value['schedule_by'],[$wStart1,$wRun]);
                if(!$res) unset($list[$ind_]);
            }
        }
    }
    return $list;
}
//Get list Online-learning Session In Class
function getOnlineSession($classId= ''){
    $sqlGetSessionList = "SELECT DISTINCT IFNULL(id, '') meeting_id,
    IFNULL(external_id, '') external_id, IFNULL(join_url, '') join_url,
    IFNULL(type, '') type, IFNULL(teacher_id, '') teacher_id,
    IFNULL(teacher_cover_id, '') ta1_id, IFNULL(sub_teacher_id, '') ta2_id,
    IFNULL(room_id, '') room_id,  date_start time_start, date_end time_end,
    DATE_FORMAT(convert_tz(date_start,'+0:00','+7:00'),'%Y-%m-%d') start_date
    FROM meetings WHERE ju_class_id = '$classId' AND deleted = 0
    AND session_status <> 'Cancelled' AND (external_id IS NOT NULL) AND (external_id <> '')
    ORDER BY time_start";
    return $GLOBALS['db']->fetchArray($sqlGetSessionList);
}

/**
* Get available teachers for a class
*
* @param classId    : class id
* @param date_start : display format ex:dd/mm/yyyy
* @param date_end   : display format ex:dd/mm/yyyy
* @param dayOfWoeek : array("Monday","Tuesday")
*
* @return array of teachers info
*/
function checkTeacherInClass($classId= '', $date_start= '', $date_end= '', $dayOfWeek= '', $available = 1, $show_another = 0, $sc_type = 'Teacher'){
    global $timedate;
    $start_date     = $timedate->convertToDBDate($date_start,false);
    $end_date       = $timedate->convertToDBDate($date_end,false);
    $classBean      = BeanFactory::getBean("J_Class", $classId);
    $type = ($sc_type == 'Teacher') ? $type ='Teacher' : $type ='TA';
    if($show_another) $sql_team = " AND ts.id NOT IN ('{$classBean->team_id}') ";
    else $sql_team = " AND ts.id = '{$classBean->team_id}' ";

    $teacherList = array();
    $q1 = "SELECT DISTINCT IFNULL(teacher.id, '') teacher_id, teacher.date_entered date_entered
    FROM c_teachers teacher
    INNER JOIN team_sets_teams tst ON tst.team_set_id = teacher.team_set_id AND tst.deleted = 0
    INNER JOIN teams ts ON ts.id = tst.team_id AND ts.deleted = 0 $sql_team
    WHERE IFNULL(teacher.type, '') LIKE '%{$type}%' AND IFNULL(teacher.status, '') = 'Active' AND teacher.deleted = 0
    ORDER BY date_entered DESC";
    $teacher_list = $GLOBALS['db']->fetchArray($q1);
    $arr_teacher_id = array_column($teacher_list,'teacher_id');
    $str_teacher_id = implode("','",$arr_teacher_id);
    $arr_condition = array();

    $sessionList    = getClassSession($classId, $date_start, $date_end, $dayOfWeek);
    foreach($sessionList as $key => $value)
        $arr_condition[] = "date_start < '{$value['time_end']}' AND date_end > '{$value['time_start']}'";
    $str_condition = "(".implode(") OR (", $arr_condition).")";
    $sql_teacher_work = "(SELECT DISTINCT teacher_id teacher_id
    FROM meetings
    WHERE deleted = 0 AND ($str_condition)
    AND teacher_id IN ('$str_teacher_id')
    AND ju_class_id <> '$classId'
    AND meeting_type = 'Session'
    AND session_status <> 'Cancelled')
    UNION DISTINCT
    (SELECT DISTINCT sub_teacher_id teacher_id
    FROM meetings
    WHERE deleted = 0 AND ($str_condition)
    AND sub_teacher_id IN ('$str_teacher_id')
    AND ju_class_id <> '$classId'
    AND meeting_type = 'Session'
    AND session_status <> 'Cancelled')
    UNION DISTINCT
    (SELECT DISTINCT teacher_cover_id teacher_id
    FROM meetings
    WHERE deleted = 0 AND ($str_condition)
    AND teacher_cover_id IN ('$str_teacher_id')
    AND ju_class_id <> '$classId'
    AND meeting_type = 'Session'
    AND session_status <> 'Cancelled')";
    $row_teacher = $GLOBALS['db']->fetchArray($sql_teacher_work);

    if($available){
        //Lọc để không xếp trùng TA1, TA2 cùng 1 thời điểm trong lớp
        if(!$show_another && ($sc_type == 'TA' || $sc_type == 'Sub Teacher')){
            if($sc_type == 'TA') $sqlTA ="sub_teacher_id";
            if($sc_type == 'Sub Teacher') $sqlTA ="teacher_cover_id";
            $sql_check_TA = "SELECT DISTINCT $sqlTA teacher_id
            FROM meetings WHERE ju_class_id = '$classId' AND deleted = 0
            AND ($str_condition) AND meeting_type = 'Session' AND session_status <> 'Cancelled'";
            $row_TA = $GLOBALS['db']->fetchArray($sql_check_TA);
            $arr_teacher_id = array_diff($arr_teacher_id, array_column($row_TA,'teacher_id'));

        }
        //remove teacher busy
        $arr_teacher_id = array_diff($arr_teacher_id, array_column($row_teacher,'teacher_id'));
        $str_teacher_id = implode("','",$arr_teacher_id);

        $ext_this_month = " AND (date_start >= '".date('Y-m-d H:i:s',strtotime("-7 hours ".date('Y-m-01',strtotime($start_date))." 00:00:00"))."' AND date_end <= '".date('Y-m-d H:i:s',strtotime("-7 hours ".date('Y-m-t',strtotime($start_date))." 23:59:59"))."')";

        $sqlTeacherList = "SELECT DISTINCT IFNULL(teacher.id, '') teacher_id, IFNULL(teacher.full_teacher_name, '') full_teacher_name,
        IFNULL(teacher.description, '') note, IFNULL(teacher.phone_mobile, '') phone,
        IFNULL(ts.name, '') center, SUM(IFNULL(mh.sum_hour,0)) taught_hours,
        IFNULL(teacher.kind_of_course, '') kind_of_course,
        IFNULL(l1.id, '') contract_id, IFNULL(l1.contract_date, '') contract_date,
        IFNULL(l1.contract_until, '') contract_until, IFNULL(l1.day_off, '') day_off,
        IFNULL(l1.contract_type, '') contract_type, IFNULL(l1.working_hours_monthly, '') working_hours_monthly
        FROM c_teachers teacher
        LEFT JOIN c_teachers_j_teachercontract_1_c l1_1 ON teacher.id = l1_1.c_teachers_j_teachercontract_1c_teachers_ida AND l1_1.deleted = 0
        LEFT JOIN j_teachercontract l1 ON l1_1.c_teachers_j_teachercontract_1j_teachercontract_idb = l1.id AND l1.deleted=0 AND l1.status = 'Active'
        INNER JOIN team_sets_teams tst ON
        tst.team_set_id = teacher.team_set_id AND tst.deleted = 0
        INNER JOIN teams ts ON
        ts.id = tst.team_id AND ts.deleted = 0 $sql_team
        LEFT OUTER JOIN(SELECT teacher_id,
        SUM(ROUND(duration_hours+(duration_minutes/60),9)) sum_hour
        FROM meetings
        WHERE LENGTH(ju_class_id) > 30 AND LENGTH(teacher_id) > 30
        $ext_this_month AND status <> 'Cancelled' AND deleted = 0
        AND teacher_id IN('$str_teacher_id')
        GROUP BY teacher_id
        UNION DISTINCT (SELECT sub_teacher_id teacher_id,
        SUM(ROUND(duration_hours+(duration_minutes/60),9)) sum_hour
        FROM meetings
        WHERE LENGTH(ju_class_id) > 30 AND LENGTH(sub_teacher_id) > 30
        $ext_this_month
        AND status <> 'Cancelled' AND deleted = 0
        AND sub_teacher_id IN('$str_teacher_id')
        GROUP BY sub_teacher_id)
        UNION DISTINCT (SELECT teacher_cover_id teacher_id,
        SUM(ROUND(duration_hours+(duration_minutes/60),9)) sum_hour
        FROM meetings
        WHERE LENGTH(ju_class_id) > 30 AND LENGTH(teacher_cover_id) > 30
        $ext_this_month
        AND status <> 'Cancelled' AND deleted = 0
        AND teacher_cover_id IN('$str_teacher_id')
        GROUP BY teacher_cover_id)) mh ON mh.teacher_id = teacher.id
        WHERE teacher.id IN('$str_teacher_id') AND teacher.deleted = 0
        GROUP BY teacher_id, teacher.date_entered DESC";

        $rsTeacherList = $GLOBALS['db']->query($sqlTeacherList);
        //Get Info class upgrade From
        $teaFromPrev = array();
        if(!empty($classBean->j_class_j_class_1j_class_ida)){
            $qss = "SELECT DISTINCT IFNULL(teacher_id, '') teacher_id
            FROM meetings WHERE deleted = 0
            AND (ju_class_id = '{$classBean->j_class_j_class_1j_class_ida}') AND (session_status <> 'Cancelled') AND (teacher_id IS NOT NULL) AND (teacher_id <> '')";
            $teaFromPrev = $GLOBALS['db']->fetchArray($qss);
        }

        $arr_session_date = array_column($sessionList,'start_date');
        $str_session_date = "'" . implode("','", $arr_session_date) . "'";
        while($rowTeacher = $GLOBALS['db']->fetchByAssoc($rsTeacherList)){
            $holidays       = checkTeacherHolidays($rowTeacher["teacher_id"], $str_session_date);
            $holidays       = implode("<br>", $holidays);
            $dayOff         = unencodeMultienum($rowTeacher["day_off"]);
            $kind_of_course = unencodeMultienum($rowTeacher["kind_of_course"]);
            $priority       = checkTeacherPriority($rowTeacher["teacher_id"], $teaFromPrev, $dayOff, $dayOfWeek, $holidays);
            $alertContractUtil = $timedate->to_display_date($rowTeacher["contract_until"],true);
            if($rowTeacher["contract_until"] < $end_date)
                $alertContractUtil = '<span class="error">'.$alertContractUtil.'</span>';
            $day_of_week = implode($dayOfWeek);
            foreach ($dayOff as $key=>$value){
                if (strpos($day_of_week, $value) !== false)
                    $dayOff[$key] = '<span class="error">'.$value.'</span>';
            }
            $dayOff = implode(", ", $dayOff);


            $koc_str = array();
            if(in_array($classBean->kind_of_course, $kind_of_course)){
                $koc_str[0] = '<b style="color: blue;">'.$classBean->kind_of_course.'</b>';;
                foreach($kind_of_course as $_key => $_val)
                    if($classBean->kind_of_course != $_val ) $koc_str[] = $_val;
            }else $koc_str = $kind_of_course;

            $koc_str = implode(", ", $koc_str);

            //check array value is empty
            if(count($kind_of_course) == 1 && empty(reset($kind_of_course)))
                $kind_of_course = '';

            //Xét chương trình học của lớp và giáo viên
            //if(!empty($kind_of_course) && !in_array($classBean->kind_of_course, $kind_of_course) && !$show_another)
            //   continue;

            $teacherList[$rowTeacher["teacher_id"]] = array(
                "center"          => $rowTeacher["center"],
                "teacher_id"      => $rowTeacher["teacher_id"],
                "teacher_name"    => $rowTeacher["full_teacher_name"],
                "phone"           => $rowTeacher["phone"],
                "contract_id"     => $rowTeacher["contract_id"],
                "contract_type"   => $GLOBALS['app_list_strings']['type_teacher_contract_list'][$rowTeacher["contract_type"]],
                "require_hours"   => format_number($rowTeacher["working_hours_monthly"],2,2),
                "total_hour"      => format_number($rowTeacher['taught_hours'],2,2),
                "contract_until"  => $timedate->to_display_date($rowTeacher["contract_until"],true),
                "contract_until_span"  => $alertContractUtil,
                "day_off"         => $dayOff,
                "kind_of_course"  => $koc_str,
                "note"            => $rowTeacher["note"],
                "holiday"         => $holidays,
                "priority"        => $priority,
            );
        }
    }else{
        $arr_teacher_id = array_column($row_teacher,'teacher_id');
        $str_teacher_id = implode("','",$arr_teacher_id);
        $ext_this_month = " AND (date_start >= '".date('Y-m-d H:i:s',strtotime($start_date." 00:00:00"))."' AND date_end <= '".date('Y-m-d H:i:s',strtotime($end_date." 23:59:59"))."')";

        $sqlTeacherList = "SELECT DISTINCT m.id,
        IFNULL(teacher.id, '') teacher_id, IFNULL(teacher.full_teacher_name, '') full_teacher_name,
        IFNULL(class.name, '') class_name, IFNULL(class.id, '') class_id,
        IFNULL(m.date_start, '') date_start, IFNULL(m.date_end, '') date_end
        FROM meetings m LEFT JOIN  c_teachers teacher";
        if($type =='Teacher')
            $sqlTeacherList .= " ON teacher.id = m.teacher_id AND teacher.deleted = 0
            LEFT JOIN  j_class class ON m.ju_class_id = class.id AND class.deleted = 0
            WHERE m.teacher_id IN('$str_teacher_id')";
        else $sqlTeacherList .= " ON (teacher.id = m.teacher_cover_id OR teacher.id = m.sub_teacher_id ) AND teacher.deleted = 0
            LEFT JOIN  j_class class ON m.ju_class_id = class.id AND class.deleted = 0
            WHERE (m.teacher_cover_id IN('$str_teacher_id') OR m.sub_teacher_id IN('$str_teacher_id'))";
        $sqlTeacherList .= " AND m.deleted = 0 AND m.status <> 'Cancelled' AND m.meeting_type = 'Session' AND ($str_condition) ORDER BY m.date_start ASC";
        $rsTeacherList = $GLOBALS['db']->query($sqlTeacherList);
        while($rowTeacher = $GLOBALS['db']->fetchByAssoc($rsTeacherList)){
            if(!empty($rowTeacher["teacher_id"])){
                if (!in_array($rowTeacher["teacher_id"], array_keys($teacherList))) {
                    $teacherList[$rowTeacher["teacher_id"]]["teacher_id"] = $rowTeacher["teacher_id"];
                    $teacherList[$rowTeacher["teacher_id"]]["teacher_name"] = $rowTeacher["full_teacher_name"];
                    $teacherList[$rowTeacher["teacher_id"]]["class_teaching"] = array();
                }
                if (!in_array($rowTeacher["class_id"], array_keys($teacherList[$rowTeacher["teacher_id"]]["class_teaching"]))) {
                    $teacherList[$rowTeacher["teacher_id"]]["class_teaching"][$rowTeacher["class_id"]]["class_id"] = $rowTeacher["class_id"];
                    $teacherList[$rowTeacher["teacher_id"]]["class_teaching"][$rowTeacher["class_id"]]["class_name"] = $rowTeacher["class_name"];
                    $teacherList[$rowTeacher["teacher_id"]]["class_teaching"][$rowTeacher["class_id"]]["schedule"] = array();
                }
                $schedule = " ".date('d/m - D H:i',strtotime("+7 hours " .$rowTeacher["date_start"]))." -";
                $schedule .= " ".date('H:i',strtotime("+7 hours " .$rowTeacher["date_end"]));
                $teacherList[$rowTeacher["teacher_id"]]["class_teaching"][$rowTeacher["class_id"]]["schedule"][] = $schedule;
            }
        }
    }
    $teacherList = sortTeacherListByTaughtHour($teacherList);

    return $teacherList;
}

function checkRoomInClass($classId= '', $date_start= '', $date_end= '', $dayOfWeek= '', $available = 1){
    global $timedate;
    $start_date = $timedate->convertToDBDate($date_start,false);
    $end_date = $timedate->convertToDBDate($date_end,false);
    $classBean = BeanFactory::getBean("J_Class", $classId);

    $session_date = $arr_condition = $roomList = $team_list = $arr_room_id = $room_in_work =  array();

    $sql_class_teams = "SELECT team_id FROM team_sets_teams WHERE team_set_id = '{$classBean->team_set_id}'";
    $sql_class_teams_result = $GLOBALS['db']->query($sql_class_teams);
    while($row=$GLOBALS['db']->fetchByAssoc($sql_class_teams_result))
        $team_list[] = $row['team_id'];


    $sql_room_id = "SELECT DISTINCT room.id room_id, room.date_entered date_entered
    FROM c_rooms room INNER JOIN team_sets_teams tst ON tst.team_set_id = room.team_set_id AND tst.deleted = 0
    INNER JOIN teams ts ON ts.id = tst.team_id AND ts.deleted = 0 AND ts.id IN ('".implode("','",$team_list)."')
    WHERE room.deleted = 0 AND room.status = 'Active' AND (room.type = 'offline' OR (room.type = 'online' AND room.classin_user_type <> 'Regular User'))
    UNION SELECT DISTINCT room.id room_id, room.date_entered date_entered
    FROM c_rooms room WHERE room.deleted = 0 AND room.status = 'Active' AND room.team_id = '1'";

    $room_list = $GLOBALS['db']->fetchArray($sql_room_id);
    foreach($room_list as $key => $value) $arr_room_id[] = $value['room_id'];

    $str_room_id =   "'".implode("','",$arr_room_id)."'";

    $sessionList = getClassSession($classId, $date_start, $date_end, $dayOfWeek);
    foreach($sessionList as $key => $value){
        $session_date[] = $value['start_date'];
        $arr_condition[] = "date_start < '{$value['time_end']}'
        AND date_end > '{$value['time_start']}'";
    }
    $str_session_date =   "'".implode("','",$session_date)."'";
    $str_condition = "(".implode(") OR (", $arr_condition).")";
    $sql_room_in_work = "SELECT DISTINCT room_id FROM meetings WHERE ($str_condition)
    AND room_id in ($str_room_id) AND ju_class_id <> '$classId' AND meeting_type = 'Session' AND session_status <> 'Cancelled' AND deleted = 0";
    $row_room = $GLOBALS['db']->fetchArray($sql_room_in_work);
    foreach($row_room as $key => $value)
        $room_in_work[] = $value['room_id'];

    $arr_room_id = array_diff($arr_room_id, $room_in_work);
    $str_room_id  =   "'".implode("','",$arr_room_id)."'";

    $sql_room = "SELECT DISTINCT IFNULL(id,'') room_id,
    IFNULL(name,'') room_name, IFNULL(capacity,'') capacity,
    IFNULL(description,'') description, IFNULL(status,'') status
    FROM c_rooms WHERE id IN ($str_room_id)";
    $rsRoomList = $GLOBALS['db']->query($sql_room);

    while($rowRoom = $GLOBALS['db']->fetchByAssoc($rsRoomList)){
        $roomList[$rowRoom["room_id"]] = array(
            "teacher_id"      => $rowRoom["room_id"],
            "teacher_name"    => $rowRoom["room_name"],
            "contract_id"     => '-none-',
            "contract_type"   => '',
            "require_hours"   => 'Room Size: '.$rowRoom["capacity"],
            "total_hour"      => '',
            "contract_until"  => '',
            "contract_until_span"  => '',
            "day_off"         => '',
            "note"         => $rowRoom["description"],
            "holiday"         => '-none-',
            "priority"        => '2',
        );
    }
    return $roomList;
}

/**
* get all teacher of Centers
*
* @param array center id $center_id
*
* @author Lap Nguyen
*/
function getTeacherOfCenter($center_id = array(), $type = '') {
    if (!in_array($center_id)) $center_id = array($center_id) ;
    $teacher = array();
    global $locale;
    $t_type = '';
    if($type != '')
        $t_type = " AND t.type LIKE '%$type%' ";
    $sql = "SELECT t.id, t.first_name, t.last_name
    FROM c_teachers t
    INNER JOIN team_sets_teams tst ON tst.team_set_id = t.team_set_id AND tst.deleted = 0
    INNER JOIN teams ts ON ts.id = tst.team_id AND ts.deleted = 0 AND ts.id IN ('".implode("','",$center_id)."')
    WHERE t.deleted = 0 AND t.status = 'Active' $t_type";
    $result = $GLOBALS['db']->query($sql);
    while($row = $GLOBALS['db']->fetchByAssoc($result)) {
        $row['name'] = $locale->getLocaleFormattedName($row['first_name'], $row['last_name'],'');
        $teacher[$row['id']] = $row['name'];
    }
    return $teacher;
}

/**
* get room of center
*
* @param mixed $center_id
*
* @author Lap Nguyen
*/
function getRoomOfCenter($center_id = array()) {
    if (!in_array($center_id)) $center_id = array($center_id) ;
    $room = array();
    $sql = "SELECT t.id, t.name
    FROM c_rooms t
    INNER JOIN team_sets_teams tst ON tst.team_set_id = t.team_set_id AND tst.deleted = 0
    INNER JOIN teams ts ON ts.id = tst.team_id AND ts.deleted = 0 AND ts.id IN ('".implode("','",$center_id)."')
    WHERE t.deleted = 0";
    $result = $GLOBALS['db']->query($sql);
    while($row = $GLOBALS['db']->fetchByAssoc($result)) {
        $room[$row['id']] = $row['name'];
    }
    return $room;
}


function checkRoomInDateime($id= '', $db_start= '', $db_end= '', $session_id = ""){
    $sql = "SELECT count(id) FROM meetings
    WHERE room_id = '$id'
    AND ((('$db_start' >= date_start) AND ('$db_start' < date_end))
    OR (('$db_end' > date_start) AND ('$db_end' <= date_end))
    OR (('$db_start' < date_start) AND ('$db_end' > date_end)))
    AND deleted=0  AND id <> '$session_id'
    AND session_status <> 'Cancelled'";

    return ($GLOBALS['db']->getOne($sql)?false:true);
}

/**
* function check teacher is working
*
* @param mixed $teacher_id
* @param mixed $start_time
* @param mixed $end_time
*/
function checkTeacherWorking($teacher_id= '',$start_time= '', $end_time= '', $return = 'bool') {
    $ext_start = '';
    if(!empty($start_time))
        $ext_start = "AND tc.contract_date <= '$start_time'";

    $ext_end = '';
    if(!empty($end_time))
        $ext_end = "AND tc.contract_until >= '$end_time'";

    $sqlTeacherList = "SELECT tc.id
    FROM
    j_teachercontract tc
    INNER JOIN c_teachers_j_teachercontract_1_c l1_1 ON tc.id = l1_1.c_teachers_j_teachercontract_1j_teachercontract_idb AND l1_1.deleted = 0
    INNER JOIN c_teachers t ON t.id = l1_1.c_teachers_j_teachercontract_1c_teachers_ida AND t.deleted = 0
    WHERE  tc.deleted <> 1 AND tc.status = 'Active'
    $ext_start $ext_end
    AND t.id = '$teacher_id'
    ORDER BY tc.contract_until DESC ";
    $teacher_contract_id = $GLOBALS['db']->getOne($sqlTeacherList);
    if(empty($teacher_contract_id))
        $teacher_contract_id = '';
    if ($return == 'id') return $teacher_contract_id;
    return !empty($teacher_contract_id) ? true : false;
}

function checkExistTeacherInClass($class_id= '', $teacher_id= '') {
    $sql = "SELECT count(id)
    FROM meetings
    WHERE ju_class_id = '$class_id' AND teacher_id = '$teacher_id'
    AND deleted = 0 AND session_status <> 'Cancelled' ";
    return $GLOBALS['db']->getOne($sql);
}

/**
* Get array week days from date to date DB Format
*
* @param display date $start_date. eg:1/12/2013
* @param display date $end_date. eg:30/12/2013
* @param day of week $weekdate. eg:Tue
* @return array eg: : array = 0: string = "03/12/2013"  1: string = "10/12/2013"
*/
function get_array_weekdates_db($start_date= '', $end_date= '', $weekdate= ''){
    global $timedate;
    date_default_timezone_set("Asia/Bangkok");
    // $start_date = $start_date.' 00:00:00';
    //  $end_date = $end_date.' 23:59:59';
    $days = array();
    $i = 0;
    $start = strtotime($timedate->convertToDBDate($start_date,false));
    $end = strtotime($timedate->convertToDBDate($end_date,false));
    $end = strtotime('+ 23 hours', $end);
    while($start <= $end){
        if (array_key_exists(date('D', $start), $weekdate)){
            $days[$i]=date('Y-m-d', $start);
            $i++;
        }
        $start = strtotime('+1 day', $start);
    }
    return $days;
}
?>
