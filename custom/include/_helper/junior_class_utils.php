<?php
/**
* add Student To Session
*/
function addJunToSession($student_id, $student_type , $situation_id , $meeting_id){
    if(empty($student_id) || empty($situation_id) || empty($student_type) || empty($meeting_id))
        return false;
    //Check duplicate
    $id = $GLOBALS['db']->getOne("SELECT IFNULL(id,'') id
        FROM meetings_".strtolower($student_type)."
        WHERE meeting_id = '$meeting_id'
        AND ".strtolower(rtrim($student_type,'s'))."_id = '$student_id'
        AND situation_id = '$situation_id'
        AND deleted = 0");
    if(empty($id)){
        //Them hoc vien vao session
        $GLOBALS['db']->query("DELETE FROM meetings_".strtolower($student_type)." WHERE ".strtolower(rtrim($student_type,'s'))."_id = '$student_id' AND meeting_id = '$meeting_id' AND deleted = 0");

        $q = "INSERT INTO meetings_".strtolower($student_type)."
        (id, meeting_id, ".strtolower(rtrim($student_type,'s'))."_id, required, accept_status, date_modified, deleted, situation_id) VALUES
        ('".create_guid()."', '$meeting_id', '$student_id', '1', 'none', '".$GLOBALS['timedate']->nowDb()."', '0', '$situation_id')";
        $GLOBALS['db']->query($q);
    }
}

/**
* Xóa học viên ra khỏi session
*/
function removeJunFromSession($situation_id, $student_type, $meeting_id = ''){
    if(empty($situation_id) || empty($student_type)) return false;
    if(!empty($meeting_id)) $ext_meeting = "AND meeting_id = '$meeting_id'";
    $GLOBALS['db']->query("DELETE FROM meetings_".strtolower($student_type)." WHERE situation_id = '$situation_id' $ext_meeting $ext_id");
}
function removeJunFromSessions($situation_id, $student_type, $meeting_id = array()){
    if(empty($situation_id) || empty($student_type)) return false;
    if(!empty($meeting_id)) $ext_meeting = "AND meeting_id IN ('".implode("','",$meeting_id)."')";
    $GLOBALS['db']->query("DELETE FROM meetings_".strtolower($student_type)." WHERE situation_id = '$situation_id' $ext_meeting $ext_id");
}


/* functiond Lấy danh sách student kèm thông tin giờ học trong khoảng thời gian
*/
function GetStudentsProcessInClass($class_id, $start_change = ''){
    require_once("custom/include/_helper/junior_revenue_utils.php");
    global $timedate;
    //Lấy tất cả Situation có liên quan đến change lịch
    if(!empty($start_change))
        $ext_start_change = "AND (jst.end_study >= '{$timedate->convertToDBDate($start_change,false)}')";

    $q1 = "SELECT DISTINCT
    IFNULL(jst.id, '') situation_id, IFNULL(l1.id, '') class_id,
    IFNULL(jst.student_id, '') student_id, IFNULL(jst.name, '') student_name,
    IFNULL(jst.student_type, '') student_type, IFNULL(jst.type, '') type,
    IFNULL(jst.start_hour, 0) start_hour, IFNULL(jst.total_hour, 0) total_hour
    FROM j_studentsituations jst
    INNER JOIN j_class l1 ON jst.ju_class_id = l1.id AND l1.deleted = 0
    WHERE (((l1.id = '$class_id') AND (jst.type IN ('Enrolled', 'OutStanding', 'Demo')) $ext_start_change
    AND (jst.total_hour > 0))) AND jst.deleted = 0
    ORDER BY student_id, start_hour ASC";
    $rs1 = $GLOBALS['db']->query($q1);
    $res = array();
    while($st = $GLOBALS['db']->fetchByAssoc($rs1)){
        $res[$st['student_id']][$st['situation_id']]['start_hour']     = $st['start_hour'];
        $res[$st['student_id']][$st['situation_id']]['total_hour']     = $st['total_hour'];
        $res[$st['student_id']][$st['situation_id']]['situa_type']     = $st['type'];
        $res[$st['student_id']][$st['situation_id']]['student_id']     = $st['student_id'];
        $res[$st['student_id']][$st['situation_id']]['student_name']   = $st['student_name'];
        $res[$st['student_id']][$st['situation_id']]['student_type']   = $st['student_type'];
        $res[$st['student_id']][$st['situation_id']]['class_id']       = $st['class_id'];
    }
    return $res;
}

function addStudentToNewSessions($situationArr = '', $class_id = '', $start_change = '1970-01-01'){
    require_once("custom/include/_helper/junior_revenue_utils.php");
    global $timedate;

    if(empty($situationArr)) return false;

    //Get list New Session by Class
    $ss = get_list_lesson_by_class($class_id);
    //render session by date
    $ss_ = array();
    if(!empty($start_change)) $start_change = $timedate->convertToDBDate($start_change,false);

    foreach($ss as $ind=>$value){
        $ss_[$value['primaryid']]['primaryid'][]    =  $value['primaryid'];
        $ss_[$value['primaryid']]['date']           =  $value['date'];
        $ss_[$value['primaryid']]['till_hour']      =  $value['till_hour'];
        $ss_[$value['primaryid']]['delivery_hour'] +=  $value['delivery_hour'];
    }

    foreach($situationArr as $student_id => $situation_list){
        foreach($situation_list as $si_id => $si_value){
            //TODO: Sau khi thay đổi lịch một số học viên sẽ tịnh tiến buổi học ==> Xoá các buổi trước 'start_hour' ==> Xoá hết add lại
            removeJunFromSession($si_id, $si_value['student_type']);
            //CHECK LẠI TH: GIỜ LẺ
            foreach($ss_ as $ss_id => $ss_value){
                $start_till = round($ss_value['till_hour'] - $ss_value['delivery_hour'],2);
                if($si_value['start_hour'] <= $start_till){
                    //Caculate First - Last Session
                    $si_value['total_hour'] -= $ss_value['delivery_hour'];
                    $rounded = round($si_value['total_hour'],2);
                    if((int)$rounded == $rounded) $si_value['total_hour'] = $rounded;//làm tròn số để ko bị lẻ giờ
                    if($rounded >= 0) addJunToSession($si_value['student_id'], $si_value['student_type'], $si_id, $ss_id);
                    elseif($rounded < -1) break; //Tối ưu hiệu xuất không loop thêm Nếu thấy quá số giờ
                }
            }
            $uTz = $timedate->getUserTimeZone()['gmt'];
            //Update Situation Start-End
            $GLOBALS['db']->query("UPDATE j_studentsituations jst
                INNER JOIN (SELECT DISTINCT
                IFNULL(mtc.situation_id, '') situation_id,
                DATE(convert_tz(MIN(l1.date_start),'+0:00','$uTz')) min_date_start,
                DATE(convert_tz(MAX(l1.date_end),'+0:00','$uTz')) max_date_end
                FROM meetings_".strtolower($si_value['student_type'])." mtc
                INNER JOIN meetings l1 ON mtc.meeting_id = l1.id AND l1.deleted = 0
                WHERE mtc.deleted = 0 AND l1.session_status <> 'Cancelled' AND mtc.situation_id = '$si_id'
                GROUP BY mtc.situation_id) mms ON mms.situation_id = jst.id
                SET jst.start_study = mms.min_date_start, jst.end_study = mms.max_date_end WHERE jst.id = '$si_id'");

        }
    }
}

/**
* ham lay ngay ke tiep buoi cuoi cung cua lop hoc khi cancel sesion
*
* @param mixed $class_id
*/
function getEndNextTimeSession($class) {
    $run_datetime = $GLOBALS['db']->getOne("SELECT
        CONVERT_TZ(MAX(mt.date_start),'+00:00','+7:00') date_start
        FROM meetings mt
        INNER JOIN j_class jj ON mt.ju_class_id = jj.id AND jj.deleted = 0
        WHERE jj.id = '{$class->id}' AND mt.session_status <> 'Cancelled' AND mt.deleted = 0");
    if(empty($run_datetime)) return '';

    $run_date = $run_begin_date = date('Y-m-d',strtotime($run_datetime));
    $run_time = date('H:i:s',strtotime($run_datetime));
    $holiday_list = getPublicHolidays($class->start_date);

    $json     = json_decode(html_entity_decode($class->content),true);
    $schedule = $json['schedule'];
    $_loop = $_found = 0;
    if(!empty($schedule)){
        while(!$_found || $_loop == 1000){
            $_loop++;
            $chck_day   = date('D',strtotime($run_date));
            if (array_key_exists($chck_day, $schedule) && !array_key_exists($run_date, $holiday_list)){
                foreach($schedule[$chck_day] as $key => $sche){
                    if(($run_date == $run_begin_date)){
                        if($sche['start_time'] > $run_time){
                            $run_time = $sche['start_time'];
                            $_found++;
                            break;
                        }elseif($key == (count($schedule[$chck_day]) - 1))
                            $run_date   = date('Y-m-d',strtotime('+1 day'. $run_date));
                    }else{
                        $run_time = $sche['start_time'];
                        $_found++;
                        break;
                    }
                }
            }else $run_date = date('Y-m-d',strtotime('+1 day'. $run_date));
        }
    }

    return $run_date.' '.$run_time;
}

function addRelatedPayment($payment_id, $get_from_payment_id, $date, $amount='', $hour='', $situation_id=''){
    if(empty($amount)) $amount = 0;
    if(empty($hour)) $hour = 0;
    if(!empty($situation_id)){
        $ext1 = ', situation_id';
        $ext2 = ", '$situation_id'";
    }
    $sql2 = "INSERT INTO j_payment_j_payment_1_c
    (id, date_modified, deleted, payment_ida, payment_idb, issue_date, hours, amount $ext1) VALUES
    ('".create_guid()."','".$GLOBALS['timedate']->nowDb()."',0, '$payment_id', '$get_from_payment_id', '$date',  $hour, $amount $ext2)";
    $GLOBALS['db']->query($sql2);
}

function removeRelatedPayment($payment_id, $get_from_payment_id = ''){
    if(!empty($get_from_payment_id)){
        $sql = "UPDATE j_payment_j_payment_1_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE payment_ida='$payment_id' AND payment_idb='$get_from_payment_id' AND deleted = 0";
        $GLOBALS['db']->query($sql);
        getPaymentRemain($get_from_payment_id);
    }else{
        $q1 = "SELECT payment_idb payment_id FROM j_payment_j_payment_1_c WHERE payment_ida='$payment_id' AND deleted = 0
        UNION DISTINCT (SELECT payment_ida payment_id FROM j_payment_j_payment_1_c WHERE payment_idb='$payment_id' AND deleted = 0)";
        $row_p = $GLOBALS['db']->fetchArray($q1);

        $sql = "UPDATE j_payment_j_payment_1_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE payment_ida='$payment_id' AND deleted = 0";
        $GLOBALS['db']->query($sql);
        $sql = "UPDATE j_payment_j_payment_1_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE payment_idb='$payment_id' AND deleted = 0";
        $GLOBALS['db']->query($sql);

        foreach($row_p as $key => $pay)
            getPaymentRemain($pay['payment_id']);

    }
}

function getUnpaidPaymentByClass($class_id){
    $q1 = "SELECT DISTINCT
    IFNULL(l2.id, '') l2_id
    FROM j_studentsituations
    INNER JOIN j_class l1 ON j_studentsituations.ju_class_id = l1.id AND l1.deleted = 0
    INNER JOIN j_payment l2 ON j_studentsituations.payment_id = l2.id AND l2.deleted = 0
    INNER JOIN j_paymentdetail l3 ON l2.id = l3.payment_id AND l3.deleted = 0
    WHERE (((l1.id = '$class_id')
    AND (l3.status = 'Unpaid')
    AND (j_studentsituations.type IN ('Enrolled' , 'Moving In'))))
    AND j_studentsituations.deleted = 0";
    $rs1 = $GLOBALS['db']->query($q1);
    $unpaidList = array();
    while($row= $GLOBALS['db']->fetchByAssoc($rs1))
        $unpaidList[] = $row['l2_id'];
    return $unpaidList;
}

function getListAttendanceStudent($classId = '', $lessonDateDb = '', $ss_id = ''){
    global $timedate, $current_user;
    $ext1 = '';
    $ext2 = '';
    if(!empty($lessonDateDb)) $ext1 = "AND DATE(CONVERT_TZ(date_start,'+00:00','+7:00')) = '{$timedate->convertToDBDate($lessonDateDb, false)}'";
    if(!empty($ss_id)) $ext2 = "AND id = '{$ss_id}'";

    if(empty($ss_id) && empty($lessonDateDb))
        return json_encode(
            array(
                "success" => "0",
                "date_start" => $lessonDateDb,
            )
        );

    //check session in date
    $sqlCheckSes = "SELECT id, description, duration_cal FROM meetings
    WHERE ju_class_id = '{$classId}'
    AND deleted = 0 $ext1 $ext2
    AND session_status <> 'Cancelled'
    LIMIT 1";
    $result = $GLOBALS['db']->query($sqlCheckSes);
    $rowSession = $GLOBALS['db']->fetchByAssoc($result);

    $sessionId = $rowSession['id'];
    $description = $rowSession['description'];

    // $sessionId = $GLOBALS['db']->getOne($sqlCheckSes);

    $session = BeanFactory::getBean('Meetings', $sessionId,array('disable_row_level_security' => true));
    $class = BeanFactory::getBean("J_Class",$session->ju_class_id,array('disable_row_level_security' => true));

    if (empty($session->id) || empty($class->id)) {
        return json_encode(
            array(
                "success" => "0",
                "date_start" => $lessonDateDb,
            )
        );
    }

    //Get All student have relationship with this class
    $sqlAllStudent = "SELECT rss.* FROM ((SELECT DISTINCT
    IFNULL(c.id, '') student_id, IFNULL(c.full_student_name, '') student_name,
    IFNULL(c.first_name, '') first_name, IFNULL(c.picture, '') picture,
    IFNULL(c.contact_id, '') portal_id, IFNULL(c.phone_mobile, '') student_phone,
    'Contacts' student_type, c.birthdate birthdate,
    IFNULL(c.nick_name, '') nick_name, c.guardian_name parent_name,
    mc.meeting_id session_id, IFNULL(ss.type, '') situation_type,
    IFNULL(a.id, '') attend_id, IFNULL(a.attended, 0) attended,
    IFNULL(a.attendance_type, '') attendance_type, IFNULL(a.homework, 0) homework,
    IFNULL(a.homework_comment, '') homework_comment, IFNULL(a.care_comment, '') care_comment,
    IFNULL(a.homework_score, '') homework_score, IFNULL(a.sms_content, '') sms_content, a.description description,
    b.point loyalty_point,

    -- JAXTINA CUSTOM
    a.flipped flipped,
    a.homework_num homework_num,
    a.teacher_comment_num teacher_comment_num,
    a.teacher_comment_num_per teacher_comment_num_per,
    IFNULL(a.total_score, '') total_score,
    IFNULL(a.number_of_part, '') number_of_part,
    IFNULL(a.number_of_question, '') number_of_question,
    IFNULL(a.number_of_right, '') number_of_right,
    IFNULL(a.number_of_wrong, '') number_of_wrong,
    IFNULL(a.total_time, '') total_time,
    IFNULL(a.wrong_ans_list, '') wrong_ans_list,
    IFNULL(cc.avg_attendance, '') avg_attendance,
    IFNULL(cc.total_attended, '') total_attended,
    IFNULL(cc.total_absent, '') total_absent
    -- END: JAXTINA CUSTOM

    FROM contacts c INNER JOIN j_classstudents cc ON cc.student_id = c.id AND c.deleted = 0 AND cc.deleted = 0 AND cc.class_id = '$classId'
    LEFT JOIN meetings_contacts mc ON mc.contact_id = c.id AND mc.deleted = 0 AND mc.meeting_id = '$sessionId'
    INNER JOIN j_studentsituations ss ON mc.situation_id = ss.id AND ss.deleted=0
    LEFT JOIN c_attendance a ON a.student_id = c.id AND a.meeting_id = '$sessionId' AND a.deleted = 0
    LEFT JOIN j_loyalty b ON b.student_id = c.id AND b.meeting_id = '$sessionId' AND b.deleted = 0)
    UNION ALL
    (SELECT DISTINCT IFNULL(c.id, '') student_id,
    IFNULL(c.full_lead_name, '') student_name, IFNULL(c.first_name, '') first_name,
    IFNULL(c.picture, '') picture, IFNULL(c.contact_id, '') portal_id,
    IFNULL(c.phone_mobile, '') student_phone, 'Leads' student_type,
    c.birthdate birthdate, IFNULL(c.nick_name, '') nick_name,
    c.guardian_name parent_name, mc.meeting_id session_id,
    IFNULL(ss.type, '') situation_type, IFNULL(a.id, '') attend_id,
    IFNULL(a.attended, 0) attended, IFNULL(a.attendance_type, '') attendance_type,
    IFNULL(a.homework, 0) homework, IFNULL(a.homework_comment, '') homework_comment,
    IFNULL(a.care_comment, '') care_comment, IFNULL(a.homework_score, '') homework_score,
    IFNULL(a.sms_content, '') sms_content, a.description description,
    b.point loyalty_point,

    -- JAXTINA CUSTOM
    a.flipped flipped,
    a.homework_num homework_num,
    a.teacher_comment_num teacher_comment_num,
    a.teacher_comment_num_per teacher_comment_num_per,
    IFNULL(a.total_score, '') total_score,
    IFNULL(a.number_of_part, '') number_of_part,
    IFNULL(a.number_of_question, '') number_of_question,
    IFNULL(a.number_of_right, '') number_of_right,
    IFNULL(a.number_of_wrong, '') number_of_wrong,
    IFNULL(a.total_time, '') total_time,
    IFNULL(a.wrong_ans_list, '') wrong_ans_list,
    '' avg_attendance,
    0 total_attended,
    0 total_absent
    -- END: JAXTINA CUSTOM

    FROM leads c
    INNER JOIN j_studentsituations ss ON c.id = ss.student_id AND ss.type = 'Demo' AND ss.ju_class_id ='$classId'
    LEFT JOIN meetings_leads mc ON mc.lead_id = c.id AND mc.deleted = 0 AND mc.meeting_id = '$sessionId'
    LEFT JOIN c_attendance a ON a.student_id = c.id AND a.meeting_id = '$sessionId' AND a.deleted = 0
    LEFT JOIN j_loyalty b ON b.student_id = c.id AND b.meeting_id = '$sessionId' AND b.deleted = 0)) rss
    ORDER BY rss.first_name ASC, rss.student_id";
    $rsAllStudent = $GLOBALS['db']->query($sqlAllStudent);

    $runStudentId   = '###';
    $count_dup      = 0;
    $count_error    = 0;

    $count = 1;
    $array_student = array();

    //CUSTOM JAXTINA
    $lmsRes = loadClassLMSResult($classId);
    $array_field = ['total_score' => 'totalScores', 'number_of_part' => 'numberPart', 'number_of_question' => 'totalQuestions', 'number_of_right' => 'rightAns', 'total_time' => 'timeOnline', 'wrong_ans_list' => 'wrongAnsDetail', 'number_of_wrong' => 'wrongAns'];
    //END

    while ($row = $GLOBALS['db']->fetchByAssoc($rsAllStudent)) {
        if (empty($row['attend_id']) && !empty($row['session_id'])) {
            $attendanceBean                 = new C_Attendance();
            $attendanceBean->name           = 'create|by_manual';
            $attendanceBean->student_id     = $row["student_id"];
            $attendanceBean->student_type   = $row["student_type"];
            $attendanceBean->meeting_id     = $session->id;
            $attendanceBean->class_id       = $class->id;
            $attendanceBean->date_input     = $timedate->to_display_date($session->date_start);
            $attendanceBean->team_id        = $class->team_id;
            $attendanceBean->team_set_id    = $attendanceBean->team_id;
            $attendanceBean->save();
            $attend_id = $attendanceBean->id;
            $row['attend_id'] = $attendanceBean->id;
        } else $attend_id = $row['attend_id'];
        /*
        * 'attend_id' sử dung bên Apps nên không được thay đổi param
        */
        //Fix loi duplicate Student
        if($runStudentId == $row["student_id"]) $count_dup++;
        else $count_dup = 0;

        if($count_dup > 0){
            //Delete Duplicated
            $GLOBALS['db']->query("DELETE FROM c_attendance WHERE id = '$attend_id'");
            $count_error++;
            continue;
        }
        //CUSTOM: assign param
        //CUSTOM JAXTINA
        //LOAD total_loyalty_point
        $total_loyalty_point = array();
        $ttl = $GLOBALS['db']->fetchArray("SELECT j_loyalty.student_id student_id, SUM(j_loyalty.point) point
            FROM j_loyalty INNER JOIN j_class_j_loyalty_1_c l1_1 ON j_loyalty.id = l1_1.j_class_j_loyalty_1j_loyalty_idb AND l1_1.deleted = 0
            INNER JOIN j_class l1 ON l1.id = l1_1.j_class_j_loyalty_1j_class_ida AND l1.deleted = 0
            WHERE (l1.id = '{$class->id}') AND j_loyalty.type='Sticker' AND j_loyalty.deleted = 0
            GROUP BY l1.id, j_loyalty.student_id");
        foreach($ttl as $key => $ft) $total_loyalty_point[$ft['student_id']] = $ft['point'];


        $attendBean = BeanFactory::getBean('C_Attendance', $attend_id);
        foreach($row as $field => $value){
            // Nếu field thuộc các field trong kết quả trả về của API
            // + học viên có email trong kết quả trả về, gắn lại kết quả vào bean
            // và gán lại kết quả của field vào array_student để đưa lên giao diện
            if(in_array($field, ['total_score', 'number_of_part', 'number_of_question', 'number_of_right', 'number_of_wrong', 'total_time', 'wrong_ans_list']) && array_key_exists($array_student[$count]['email_address'], $lmsRes)) {
                $attendBean->$field = $array_student[$count][$field] = $lmsRes[$array_student[$count]['email_address']][$numberLesson][$array_field[$field]];
            }else{
                $array_student[$count][$field] = $value;
            }
            $array_student[$count]['total_loyalty_point'] = $total_loyalty_point[$attendBean->student_id];
        }
        $attendBean->flipped = empty($attendBean->number_of_question) ? 0 : (round(($attendBean->number_of_right / $attendBean->number_of_question)*100));
        $attendBean->homework_score = ($attendBean->flipped * 0.4) + ($attendBean->homework_num * 0.4) + ($attendBean->teacher_comment_num_per * 0.2);
        $array_student[$count]['homework_score'] = $attendBean->homework_score;
        $attendBean->no_cache = 1;
        $attendBean->save();
        //END: CUSTOM JAXTINA
        $count++;
        $runStudentId = $row['student_id'];
    }

    return json_encode(
        array(
            "success"               => "1",
            "session_id"            => $sessionId,
            "session_description"   => $description,
            "array_student"         => $array_student,
            "lock_status"           => $session->lock_status,
        )
    );
}

//Load json session time
function getJsonSession($session_id = '', $class_id = '' , $date = ''){
    global $timedate;
    if(!empty($session_id)){
        $row = $GLOBALS['db']->fetchOne("SELECT DISTINCT IFNULL(l1.id, '') class_id, meetings.date session_date
            FROM meetings INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
            WHERE (meetings.id = '$session_id') AND meetings.deleted = 0");
        $date     = $row['session_date'];
        $class_id = $row['class_id'];
    }
    if(empty($class_id) && empty($date)) return false;

    $dateDB   = $timedate->convertToDBDate($date);
    $start_tz = date('Y-m-d H:i:s',strtotime("-7 hours ".$dateDB." 00:00:00"));
    $end_tz   = date('Y-m-d H:i:s',strtotime("-7 hours ".$dateDB." 23:59:59"));

    $q1 = "SELECT DISTINCT IFNULL(mt.id, '') primaryid, IFNULL(l1.id, '') class_id, IFNULL(l1.name, '') class_name,
    IFNULL(mt.session_status, '') session_status, mt.date_start date_start, mt.date_end date_end, mt.date session_date
    FROM meetings mt INNER JOIN j_class l1 ON mt.ju_class_id = l1.id AND l1.deleted = 0
    WHERE (((l1.id = '$class_id') AND (mt.date_start >= '$start_tz' AND mt.date_start <= '$end_tz'))) AND mt.deleted = 0";
    $rows = $GLOBALS['db']->fetchArray($q1);
    $arr = array();
    foreach($rows as $key=>$ss){
        $DATE_START            = $timedate->to_display_date_time($ss['date_start']);
        $DATE_END              = $timedate->to_display_date_time($ss['date_end']);
        $arr[$key]['id']       = $ss['primaryid'];
        $arr[$key]['status']   = $ss['session_status'];
        $arr[$key]['time']     = substr($DATE_START, 11,5).' - '.substr($DATE_END, 11);
        $arr[$key]['selected'] = ($session_id == $ss['primaryid'] ? 'selected' : '');
    }
    return array(
        'class_id'   => $ss['class_id'],
        'class_name' => $ss['class_name'],
        'date'       => $ss['session_date'],
        'json_session' => json_encode($arr));
}

function saveAttendanceUtils($data = ''){
    global $timedate;

    if(!empty($data['session_id']) && !empty($data['class_id'])){
        $s1 =  $GLOBALS['db']->query("SELECT id, duration_cal, ju_class_id, team_id, date_start, date FROM meetings WHERE deleted = 0 AND id = '{$data['session_id']}' AND ju_class_id = '{$data['class_id']}' AND session_status <> 'Cancelled'");
        $session = $GLOBALS['db']->fetchByAssoc($s1);
    }else return json_encode(array( "success" => "0"));

    $field_name =  $data['savePos'];
    $field_val  =  $data['saveVal'];

    if(!empty($data['attend_id']) && !empty($field_name)){
        $params = array('disable_row_level_security' => true);
        $att = BeanFactory::getBean('C_Attendance',$data['attend_id'],$params);
        $att->no_cache = $data['no_cache'];//Bổ sung cache trigger tối ưu performance
        if(empty($att->id)) return json_encode(array( "success" => "0"));

        //Customize Jaxtina
        $jaxtina_fields = ['flipped',  'homework_num' , 'teacher_comment_num', 'teacher_comment_num_per','homework_score'];
        if(in_array($field_name, $jaxtina_fields)){
            $max_value = ($field_name == 'teacher_comment_num') ? 15 : 100;
            //validate fields
            if(($field_val < 0 || $field_val > $max_value))
                return json_encode(array( "success" => 9,
                    'old_value' => ((!empty($att->$field_name)) ? format_number($att->$field_name) : ''),
                    'message' => translate('LBL_WARNING_JAX','J_Class').$max_value));

            //prepare & save
            $att->$field_name = $field_val;
            $att->teacher_comment_num_per = round(($att->teacher_comment_num * 100) / 15);
            $att->homework_score = ($att->flipped * 0.4) + ($att->homework_num * 0.4) + ($att->teacher_comment_num_per * 0.2);
            foreach($jaxtina_fields as $field){if(empty($att->$field) || !is_numeric( $att->$field )) $att->$field = '';}
            $att->name = 'update|by_manual';
            $att->save();
            //set return
            $res['success'] = 10;
            $return_fields = [$field_name, 'teacher_comment_num_per','homework_score'];
            foreach($return_fields as $field) {
                $res[$field] = (!empty($att->$field)) ? format_number($att->$field) : '';
            }
            return json_encode($res);
        }elseif($field_name != 'loyalty_point') {
            $att->$field_name = $field_val;
            $att->name        = 'update|by_manual';
            $att->meeting_id  = $session['id'];
            $att->team_id     = $session['team_id'];
            $att->class_id    = $session['ju_class_id'];
            $att->date_input  = $timedate->to_display_date($session['date_start']);
            $att->save();
            //Get AVG Attendance  AVG_ATTENDANCE _ CUSTOM JAXTINA
            $row1 = $GLOBALS['db']->fetchOne("SELECT IFNULL(avg_attendance, '') avg_attendance, IFNULL(total_attended, 0) total_attended,IFNULL(total_absent, 0) total_absent FROM j_classstudents WHERE class_id='{$att->class_id}' AND student_id='{$att->student_id}' AND deleted=0");
            $tool_tip = translate('LBL_TOTAL_ATTENDED', 'J_ClassStudents') . ": " . $row1['total_attended'] . ' / ' . ($row1['total_absent'] + $row1['total_attended']);
            $tool_tip .= "\n" . translate('LBL_TOTAL_ABSENT', 'J_ClassStudents') . ": " . $row1['total_absent'] . ' / ' . ($row1['total_absent'] + $row1['total_attended']);
            $avg_attendance = labelAttOverall($row1['avg_attendance'],$tool_tip);
        }else{
            //New loyalty
            $max_point = 1000;//Set max loyalty point
            if(empty($field_val)) $GLOBALS['db']->query("DELETE FROM j_loyalty WHERE meeting_id ='{$session['id']}' AND student_id ='{$att->student_id}' AND deleted = 0 ");
            elseif(!empty($field_val) && $field_val >= 1 && $field_val <= $max_point) {
                //delete old loyalty
                $GLOBALS['db']->query("DELETE FROM j_loyalty WHERE meeting_id ='{$session['id']}' AND student_id ='{$att->student_id}' AND deleted = 0 ");
                // Create new loyalty
                $loyaltybean = BeanFactory::newBean('J_Loyalty');
                $loyaltybean->meeting_id    = $session['id'];
                $loyaltybean->point         = $field_val * 1 ; //Custom Jaxtina
                $loyaltybean->team_set_id   = $session['team_id'];
                $loyaltybean->team_id       = $session['team_id'];
                $loyaltybean->rate_in_out   = '1000';
                $loyaltybean->type          = 'Sticker';//Custom Jaxtina
                $loyaltybean->student_id    = $att->student_id;
                $loyaltybean->input_date    = $session['date'];
                $loyaltybean->j_class_j_loyalty_1j_class_ida = $session['ju_class_id'];
                $loyaltybean->description   = 'Tặng điểm thưởng Sticker buổi học ngày '.date("d-m-Y",strtotime($session['date']));
                $loyaltybean->save();

                $total_loyalty = $GLOBALS['db']->getOne("SELECT SUM(j_loyalty.point) point
                    FROM j_loyalty INNER JOIN j_class_j_loyalty_1_c l1_1 ON j_loyalty.id = l1_1.j_class_j_loyalty_1j_loyalty_idb AND l1_1.deleted = 0
                    INNER JOIN j_class l1 ON l1.id = l1_1.j_class_j_loyalty_1j_class_ida AND l1.deleted = 0
                    WHERE (l1.id = '{$session['ju_class_id']}') AND j_loyalty.type='Sticker' AND (j_loyalty.student_id = '{$att->student_id}') AND j_loyalty.deleted = 0
                    GROUP BY l1.id");
            }
            else{
                $point = $GLOBALS['db']->getOne("SELECT point FROM j_loyalty WHERE deleted =0 AND meeting_id ='{$session['id']}' AND student_id ='{$att->student_id}'");
                if(empty($point)) $point = '';
                return json_encode(array( "success" => 2, 'point' => $point, 'max_point' => $max_point));
            }
        }
        return json_encode(array( "success" => 1, 'attend_id' => $data['attend_id'] , 'avg_attendance' => $avg_attendance , 'total_loyalty' => $total_loyalty));

    }else return json_encode(array( "success" => 0, 'attend_id' => $data['attend_id']));
}

function attendenceSendApp(array $args){
    require_once('custom/clients/mobile/helper/NotificationHelper.php');
    $attend = BeanFactory::getBean('C_Attendance', $args['attend_id']);
    $status = 'SENT_FAILED';
    if(!empty($attend->id)){
        if($args['send_type'] == 'send_att'){
            $attend->notificated = 1;
            if(isset($args['no_cache'])) $attend->no_cache = $args['no_cache'];
            $attend->save();
            $status = 'RECEIVED';
        }elseif($args['send_type'] == 'send_mes'){
            $notify = new NotificationMobile();
            $status = 'RECEIVED';
            if(!empty($attend->student_id)){
                if(!$notify->pushNotification('Bạn có thông báo mới!!', $args['msg'], 'Notifications', '', $attend->student_id)) $status = 'SENT_FAILED';
            }else $status = 'SENT_FAILED';
        }
    }
    return json_encode(array("status" => $status));
}

//Get Students Info for Enrollment
function getStudentsForEnroll($students_id = '', $beanClass = '', $selectedId = ''){
    global $timedate;

}


//get Payment Remain for Enrollment
function getRemainForEnroll($students_id = '', $beanClass = '', $selectedId = ''){
    $ext1 = '';
    if(!empty($selectedId))
        $ext1 = " OR (pmm.id IN ('".implode("','",$selectedId)."')) ";
    $q4 = "SELECT DISTINCT IFNULL(stt.id, '') student_id,
    IFNULL(pmm.id, '') id, IFNULL(pmm.name, '') name,
    IFNULL(pmm.payment_type, '') type, IFNULL(pmm.payment_date, '') date,
    IFNULL(pmm.payment_expired, '') expired, IFNULL(pmm.kind_of_course, '') kind_of_course,
    IFNULL(l4.id, '') course_fee_id, IFNULL(l4.name, '') course_fee_name,
    IFNULL(l2.id, '') team_id, IFNULL(l2.name, '') team_name,
    IFNULL(pmm.payment_amount + pmm.paid_amount + pmm.deposit_amount , 0) payment_amount,
    (IFNULL(pmm.payment_amount + pmm.paid_amount + pmm.deposit_amount , 0) /IFNULL(pmm.total_hours,0)) price,
    IFNULL(pmm.total_hours, 0) total_hours, IFNULL(pmm.remain_amount, 0) remain_amount, IFNULL(pmm.remain_hours, 0) remain_hours
    FROM contacts stt
    INNER JOIN j_payment pmm ON stt.id = pmm.parent_id AND pmm.parent_type = 'Contacts' AND pmm.deleted = 0
    INNER JOIN teams l2 ON pmm.team_id = l2.id AND l2.deleted = 0
    LEFT JOIN j_coursefee_j_payment_2_c l4_1 ON pmm.id = l4_1.j_coursefee_j_payment_2j_payment_idb AND l4_1.deleted = 0
    LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_2j_coursefee_ida AND l4.deleted = 0
    WHERE (stt.id IN ('".implode("','",$students_id)."'))
    AND (((pmm.team_id IN (SELECT tst.team_id FROM team_sets_teams tst WHERE tst.team_set_id = '{$beanClass->team_set_id}'))
    AND (pmm.remain_hours > 0)
    AND (pmm.use_type = 'Hour')) $ext1)
    AND stt.deleted = 0
    GROUP BY IFNULL(stt.id, ''), IFNULL(pmm.id, '')
    ORDER BY IFNULL(stt.id, ''), CASE WHEN (l2.id = stt.team_id) THEN 0 ELSE 1 END ASC,
    CASE WHEN (pmm.kind_of_course LIKE '%{$beanClass->kind_of_course}%') THEN 0 ELSE 1 END ASC,
    pmm.remain_hours ASC, pmm.payment_date ASC";

    return $GLOBALS['db']->fetchArray($q4);

}

function buildHTMLPaidList($prefix = '', $row){
    global $timedate;
    //handle dropdown paid list
    $paid_list = "<select id='".$prefix.$row['student_id']."' student_id = '".$row['student_id']."' class='".$prefix."paid_list' multiple name='".$prefix."paid_list[]'>";
    $first_opt  = true;
    $previous   = '';
    foreach($row['paid_list'] as $key => $pay){
        if ($pay['team_id'] != $previous){
            if(!$first_opt) $paid_list .= '</optgroup>';
            else $first_opt = false;

            $paid_list  .= '<optgroup label="'.translate('LBL_CENTER_C','J_Class').': ' .$pay['team_name'].'">';
            $previous   = $pay['team_id'];
        }

        switch ($pay['type']) {
            case "Deposit":
                $color = "textbg_blue";
                break;
            case "Cashholder":
                $color = "textbg_bluelight";
                break;
            case "Delay":
                $color = "textbg_blood";
                break;
            case "Transfer In":
            case "Moving In":
                $color = "textbg_yellow_light";
                break;
        }
        $today = $timedate->nowDbDate();
        $paid_list .= "<option sub_text='<small> <span class=\"$color\">". $GLOBALS['app_list_strings']['payment_type_list'][$pay['type']] ."</span>";
        $paid_list .= "<br>".translate('LBL_TOTAL_C','J_Class')." : ".format_number($pay['payment_amount'])." - ".format_number($pay['total_hours'],2,2)." ".translate('LBL_HOURS_C','J_Class')."";
        $paid_list .= "<br>".translate('LBL_REMAIN_C','J_Class')." : ".format_number($pay['remain_amount'])." - <b>".format_number($pay['remain_hours'],2,2)." ".translate('LBL_HOURS_C','J_Class')."</b>";
        $paid_list .= "<br>".translate('LBL_COURSE_C','J_Class')." : [".(empty($pay['kind_of_course']) ? '-none-' : decodeMultienum($pay['kind_of_course'])).']'. ((!empty($pay['course_fee_name'])) ? " - {$pay['course_fee_name']}" : "");
        $paid_list .= "<br>".translate('LBL_DATE_C','J_Class')." : ".$timedate->to_display_date($pay['date'],true)." - ".translate('LBL_EXPIRED_C','J_Class')." : ".(($today > $pay['expired']) ? "<span class=\"error\">" : "<b>") .$timedate->to_display_date($pay['expired'],true)."</span></small>' ";

        $paid_list .= "student_id='{$row['student_id']}' ";
        $paid_list .= "value='{$pay['id']}' ";
        $paid_list .= "remain_amount='{$pay['remain_amount']}' ";
        $paid_list .= "team_name='{$pay['team_name']}' ";
        $paid_list .= "team_id='{$pay['team_id']}' ";
        $paid_list .= "remain_hours='{$pay['remain_hours']}' ";
        $paid_list .= "expired='{$pay['expired']}' ";
        $paid_list .= "payment_name='{$pay['name']}' ";
        $paid_list .= "kind_of_course='".json_encode(array_unique(unencodeMultienum($pay['kind_of_course'])))."' ";
        $paid_list .= (($pay['selected']) ? ' selected ' : '');
        $paid_list .= " >{$pay['name']}</option>";
    }
    $paid_list .= '</optgroup>';
    $paid_list .= '</select>';
    return $paid_list;

}

//Lấy ra danh sách lớp hiện tại
function getClassSize($class_id){
    //Check Max size
    $q1 = "SELECT DISTINCT IFNULL(l2.id, '') student_id, IFNULL(l1.max_size, '') max_size
    FROM j_studentsituations jst
    INNER JOIN j_class l1 ON jst.ju_class_id = l1.id AND l1.deleted = 0
    INNER JOIN contacts l2 ON jst.student_id = l2.id AND l2.deleted = 0 AND jst.student_type = 'Contacts'
    WHERE (l1.id = '$class_id') AND (jst.type IN ('Enrolled','OutStanding')) AND jst.deleted = 0";
    $rs1 = $GLOBALS['db']->query($q1);
    $studentIds = array();
    while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
        $studentIds[] = $row['student_id'];
        $currentSize++;
        $maxSize = $row['max_size'];
    }
    return array(
        'student_list' => $studentIds,
        'current_size' => $currentSize,
        'max_size' => $maxSize,
    );
}
?>
