<?php
/**
* GET LIST: DOANH THU THEO LỚP THEO HỌC VIÊN
* @param start_display    	: Ngày bắt đầu trên hệ CRM
* @param end_display		: Ngày kết thúc trên CRM
* @param class_id   		: ID lớp
* @param student_id       	: ID Học viên
* @param situation_type     : Loại doanh thu cần lấy 'Enrolled', 'OutStanding'
*/
require_once('custom/include/_helper/junior_schedule.php');
function get_list_revenue($student_id, $student_type, $class_id='', $start_display='', $end_display='', $situation_type = array()){
    global $timedate;
    if(!empty($start_display)) $ext_start = "AND (meetings.date_start >= '".date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($start_display)." 00:00:00"))."')";
    if(!empty($end_display)) $ext_end = "AND (meetings.date_end <= '".date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($end_display)." 23:59:59"))."')";
    if(!empty($class_id)) $ext_class = "AND (l2.id = '$class_id')";
    if(!empty($situation_type)) $ext_situation = "AND l3.type IN('".implode("','",$situation_type)."')";

    $q1 = "SELECT DISTINCT IFNULL(l3.id, '') situation_id, IFNULL(l3.type, '') type,
    IFNULL(meetings.id, '') primaryid, IFNULL(l2.id, '') class_id,
    IFNULL(meetings.till_hour, 0) till_hour, ROUND(meetings.duration_hours+(meetings.duration_minutes/60),9) delivery_hour
    FROM meetings
    INNER JOIN meetings_".strtolower($student_type)." l1_1 ON meetings.id = l1_1.meeting_id AND l1_1.deleted = 0
    INNER JOIN ".strtolower($student_type)." l1 ON l1.id = l1_1.".strtolower(rtrim($student_type,'s'))."_id AND l1.deleted = 0 AND (l1.id = '$student_id')
    INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
    INNER JOIN j_studentsituations l3 ON l1_1.situation_id = l3.id AND l3.deleted = 0
    WHERE ((meetings.deleted = 0) AND (meetings.session_status <> 'Cancelled') $ext_class $ext_start $ext_end $ext_situation)
    GROUP BY primaryid, situation_id
    ORDER BY date_start ASC";
    return $GLOBALS['db']->fetchArray($q1);
}

/**
* GET LIST: DOANH THU THEO LỚP THEO SITUATION THEO HỌC VIÊN ...
* @param start_display    	: Ngày bắt đầu trên hệ CRM
* @param end_display		: Ngày kết thúc trên CRM
* @param class_id   		: ID lớp
* @param student_id       	: ID Học viên
* @param situation_type     : Loại doanh thu cần lấy 'Enrolled', 'OutStanding'
*/
function get_total_revenue($student_id, $student_type, $class_id = '', $start_display = '', $end_display = '', $situation_type = array()){
    global $timedate;
    if(!empty($class_id)) $ext_class = "AND (l2.id = '$class_id')";
    if(!empty($situation_type)) $ext_situation = "AND l3.type IN('".implode("','",$situation_type)."')";
    if(!empty($start_display)) $ext_start = "AND (mt.date_start >= '".date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($start_display,false)." 00:00:00"))."')";
    if(!empty($end_display)) $ext_end = "AND (mt.date_end <= '".date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($end_display    ,false)." 23:59:59"))."')";
    $q1 = "SELECT DISTINCT IFNULL(l3.id, '') situation_id, IFNULL(l3.type, '') type,
    ROUND(l3.total_amount/(l3.total_minute/60),9) price,
    SUM(ROUND(mt.duration_hours+(mt.duration_minutes/60),9)) total_hours,
    ROUND((l3.total_minute/60),9) total_hours_jst,
    l3.total_amount total_amount_jst,
    MIN(mt.date_start) start_study, MAX(mt.date_end) end_study,
    (SUM(ROUND(mt.duration_hours+(mt.duration_minutes/60),9))*ROUND(l3.total_amount/(l3.total_minute/60),9)) total_revenue,
    COUNT(mt.id) count_sessions
    FROM meetings mt
    INNER JOIN meetings_".strtolower($student_type)." l1_1 ON mt.id = l1_1.meeting_id AND l1_1.deleted = 0
    INNER JOIN ".strtolower($student_type)." l1 ON l1.id = l1_1.".strtolower(rtrim($student_type,'s'))."_id AND l1.deleted = 0 AND (l1.id = '$student_id')
    INNER JOIN j_class l2 ON mt.ju_class_id = l2.id AND l2.deleted = 0 $ext_class
    INNER JOIN j_studentsituations l3 ON l1_1.situation_id = l3.id AND l3.deleted = 0 $ext_situation
    WHERE mt.deleted = 0 $ext_start $ext_end AND (mt.session_status <> 'Cancelled')
    GROUP BY situation_id
    ORDER BY start_study ASC";
    return $GLOBALS['db']->fetchArray($q1);
}

/**
* GET LIST: TỔNG SỐ GIỜ DẠY THEO LỚP
* @param start_display        : Ngày bắt đầu trên hệ CRM
* @param end_display        : Ngày kết thúc trên CRM
* @param class_id           : ID lớp
*/
function get_list_lesson_by_class($class_id = '', $start_display = '', $end_display = '', $session_list = ''){
    global $timedate;
    $ext_class = "AND (l2.id = '$class_id')";

    $ext_start = '';
    if(!empty($start_display)){
        $start_tz  = date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($start_display,false)." 00:00:00"));
        $ext_start = "AND (meetings.date_start >= '$start_tz')";
    }

    $ext_end = '';
    if(!empty($end_display)){
        $end_tz   = date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($end_display,false)." 23:59:59"));
        $ext_end  = "AND (meetings.date_end <= '$end_tz')";
    }

    if(!empty($session_list)) $ext_ss = "AND meetings.id IN ('".implode("','",$session_list)."')";

    $uTz = $timedate->getUserTimeZone()['gmt'];

    $q1 = "SELECT DISTINCT
    IFNULL(l2.id, '') class_id, IFNULL(l2.class_code, '') class_code,
    IFNULL(l2.name, '') class_name, l2.start_date class_start_date,
    l2.end_date class_end_date, l2.hours class_hour, meetings.week_no week_no,
    IFNULL(meetings.id, '') primaryid, meetings.date_modified date_modified,
    meetings.date_start date_start, meetings.date_end date_end, meetings.date date,
    CONVERT_TZ(meetings.date_start,'+00:00','$uTz') date_start_tz, CONVERT_TZ(meetings.date_end,'+00:00','$uTz') date_end_tz,
    meetings.lesson_number lesson_number, IFNULL(meetings.till_hour, 0) till_hour,
    IFNULL(meetings.external_id, '') external_id, IFNULL(meetings.type, '') type, IFNULL(meetings.creator, '') creator, IFNULL(meetings.join_url, '') join_url,
    IFNULL(meetings.duration_cal, 0) duration_cal, ROUND(meetings.duration_hours+(meetings.duration_minutes/60),9) delivery_hour,
    meetings.duration_hours duration_hours, meetings.duration_minutes duration_minutes,
    meetings.session_status session_status, IFNULL(meetings.teacher_id, '') teacher_id, IFNULL(meetings.teaching_type, '') teaching_type,
    IFNULL(meetings.sub_teacher_id, '') sub_teacher_id, IFNULL(meetings.teacher_cover_id, '') teacher_cover_id, IFNULL(meetings.room_id, '') room_id
    FROM meetings INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
    WHERE ((meetings.deleted = 0 $ext_class $ext_start $ext_end $ext_ss AND (meetings.session_status <> 'Cancelled')))
    ORDER BY date_start ASC, primaryid ASC";
    return $GLOBALS['db']->fetchArray($q1);
}
//Tín số giờ dư - Delay
function get_remain_af_change($student_id, $student_type, $class_id, $from_date = '', $to_date = ''){
    $res = array();
    //Kiếm tra TH treo giờ do đổi lịch
    $revs = get_total_revenue($student_id, $student_type, $class_id);
    foreach($revs as $key => $s){
        $hours  = ($s['total_hours_jst']-$s['total_hours']);
        $amount = ($s['total_amount_jst']-$s['total_revenue']);
        if($hours < '0.1') $hours = 0;
        if($amount < '1000') $amount = 0;
        if($hours > 0){
            $res[$s['situation_id']]['type']    = $s['type'];
            $res[$s['situation_id']]['hours']   = $hours;
            $res[$s['situation_id']]['amount']  = $amount;
        }
    }
    //END: Treo Hours
    //Lấy số giờ DELAY
    $revenues = get_total_revenue($student_id, $student_type, $class_id ,$from_date, $to_date);
    foreach($revenues as $rev){
        $res[$rev['situation_id']]['type']    = $rev['type'];
        $res[$rev['situation_id']]['hours']  += $rev['total_hours'];
        $res[$rev['situation_id']]['amount'] += $rev['total_revenue'];
    }
    //END: Lấy số giờ DELAY
    //Lấy Payment List
    if($student_type == 'Contacts'){
        $qc1 = "SELECT DISTINCT IFNULL(jst.id, '') situation_id, IFNULL(l3.id, '') payment_id,
        IFNULL(jst.student_id,'') student_id, IFNULL(jst.type, '') type,
        l3_1.hours hours, l3_1.amount amount, IFNULL(l3.payment_type, '') payment_type,
        (l3.payment_amount + l3.paid_amount + l3.deposit_amount) ttp_amount
        FROM j_studentsituations jst
        INNER JOIN j_class l1 ON jst.ju_class_id = l1.id AND l1.deleted = 0 AND (l1.id = '{$class_id}')
        INNER JOIN j_payment_j_payment_1_c l3_1 ON jst.id = l3_1.situation_id AND l3_1.deleted = 0
        INNER JOIN j_payment l3 ON l3.id = l3_1.payment_idb AND l3.deleted = 0
        WHERE (jst.type = 'Enrolled')
        AND (jst.student_id = '{$student_id}')
        AND (jst.id IN ('".implode("','",array_keys($res))."'))
        AND jst.deleted = 0
        ORDER BY ttp_amount DESC"; //ưu tiên nhả lại payment nhiều tiền hơn
        $r1 = $GLOBALS['db']->query($qc1);
        while($row = $GLOBALS['db']->fetchByAssoc($r1)){
            $price = $row['amount'] / $row['hours'];
            if(is_nan($price) || empty($price)) $price = 0;
            $res[$row['situation_id']]['payment'][$row['payment_id']] = array(
                'id'     => $row['payment_id'],
                'type'   => $row['payment_type'],
                'hours'  => $row['hours'],
                'amount' => $row['amount'],
                'price'  => $price,
                'ttp_amount' => $row['ttp_amount'],
            );
        }
    }
    return $res;
}

/**
* GET: TỔNG SỐ GIỜ LỚP
* @param start_display    	: Ngày bắt đầu trên hệ CRM
* @param end_display		: Ngày kết thúc trên CRM
* @param class_id   		: ID lớp
*/
function get_total_lesson_by_class($class_id = '', $start_display = '', $end_display = '', $not_status = 'Cancelled'){
    global $timedate;

    $ext_start = '';
    if(!empty($start_display)){
        $start_tz 	= date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($start_display,false)." 00:00:00"));
        $ext_start = "AND (meetings.date_start >= '$start_tz')";
    }
    $ext_end = '';
    if(!empty($end_display)){
        $end_tz 	= date('Y-m-d H:i:s',strtotime("-7 hours ".$timedate->convertToDBDate($end_display	,false)." 23:59:59"));
        $ext_end = "AND (meetings.date_end <= '$end_tz')";
    }
    $ext_class = "AND (l2.id IN ('$class_id'))";

    $ext_status = "AND (meetings.session_status <> 'Cancelled')";
    if(empty($not_status))
        $ext_status = "";

    $q1 = "SELECT DISTINCT IFNULL(l2.id, '') class_id, IFNULL(l2.name, '') class_name,
    l2.start_date start_date, l2.end_date end_date, COUNT(meetings.id) count_session,
    SUM(ROUND(meetings.duration_hours+(meetings.duration_minutes/60),9)) sum_hours
    FROM meetings INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
    WHERE ((meetings.deleted = 0 $ext_class $ext_start $ext_end $ext_status))
    GROUP BY class_id";
    return $GLOBALS['db']->fetchArray($q1);
}


/**
* KIỂM TRA HỌC VIÊN CÓ TỒN TẠI TRONG KHOẢNG THỜI GIAN CỦA LỚP HAY KO
* @param start_display    	: Ngày bắt đầu trên hệ CRM
* @param end_display		: Ngày kết thúc trên CRM
* @param class_id   		: ID lớp
*/
function is_exist_in_class($student_id, $start_display = '', $end_display = '', $class_id = ''){
    $ses_list = get_list_revenue($student_id, 'Contacts', $class_id, $start_display, $end_display);
    if(count($ses_list) > 0)
        return true;
    else return false;
}

/**
* KIỂM TRA LEAD CÓ TỒN TẠI TRONG KHOẢNG THỜI GIAN CỦA LỚP HAY KO
* @param Lead ID    		: Ngày bắt đầu trên hệ CRM
* @param class_id   		: ID lớp
*/
function is_exist_lead_in_class($lead_id = '', $class_id = ''){
    $res = get_lead_in_class($lead_id, $class_id);
    if(count($res) > 0)
        return true;
    else return false;
}

/**
* GET LIST: Lead trong lớp
* @param Lead ID    		: Ngày bắt đầu trên hệ CRM
* @param class_id   		: ID lớp
*/
function get_lead_in_class($lead_id = '', $class_id = ''){
    $ext_lead_id = '';
    if(!empty($lead_id))
        $ext_lead_id = "AND (l1.id = '$lead_id')";

    $ext_class_id = '';
    if(!empty($class_id))
        $ext_class_id = "AND (l2.id = '$class_id')";

    $q1 = "SELECT DISTINCT
    IFNULL(meetings.id, '') primaryid,
    IFNULL(l1_1.id, '') rel_id, IFNULL(meetings.name, '') meetings_name,
    meetings.date_start meetings_date_start, meetings.date_end meetings_date_end
    FROM meetings
    INNER JOIN meetings_leads l1_1 ON meetings.id = l1_1.meeting_id AND l1_1.deleted = 0
    INNER JOIN leads l1 ON l1.id = l1_1.lead_id AND l1.deleted = 0
    INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
    WHERE ((meetings.deleted = 0 $ext_lead_id $ext_class_id))";
    return $GLOBALS['db']->fetchArray($q1);
}

/**
* GET LIST: Receipt
*/
function get_list_payment_detail($payment_id = '', $team_id = '', $student_id = '',  $start_db = '', $end_db = '', $status = 'Paid', $type = ''){
    $ext_pay = '';
    if(!empty($payment_id))
        $ext_pay = "AND (l1.id = '$payment_id')";

    $ext_team = '';
    if(!empty($team_id))
        $ext_team = "AND (l2.id = '$team_id')";

    $ext_stu = '';
    if(!empty($student_id))
        $ext_stu = "AND (l3.id = '$student_id')";

    $ext_start = '';
    if(!empty($start_db))
        $ext_start = "AND (j_paymentdetail.payment_date >= '$start_db')";

    $ext_end = '';
    if(!empty($end_db))
        $ext_end = "AND (j_paymentdetail.payment_date <= '$end_db')";

    $ext_status = '';
    if(!empty($status))
        $ext_status = "AND (j_paymentdetail.status = '$status')";

    $ext_type = '';

    $q1 = "SELECT DISTINCT
    IFNULL(j_paymentdetail.id, '') primaryid,
    IFNULL(j_paymentdetail.name, '') name,
    j_paymentdetail.payment_no payment_no,
    IFNULL(j_paymentdetail.payment_method, '') payment_method,
    j_paymentdetail.before_discount before_discount,
    j_paymentdetail.discount_amount discount_amount,
    j_paymentdetail.sponsor_amount sponsor_amount,
    j_paymentdetail.payment_amount payment_amount,
    j_paymentdetail.payment_method_fee payment_method_fee,
    j_paymentdetail.payment_date payment_date,
    IFNULL(l1.id, '') payment_id,
    l1.payment_amount payment_payment_amount,
    IFNULL(l2.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    l3.full_student_name student_name
    FROM j_paymentdetail
    INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
    INNER JOIN teams l2 ON j_paymentdetail.team_id = l2.id AND l2.deleted = 0
    LEFT JOIN contacts l3 ON l3.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l3.deleted = 0
    WHERE (((j_paymentdetail.deleted = 0)
    $ext_pay
    $ext_team
    $ext_stu
    $ext_start
    $ext_end
    $ext_status
    $ext_type))";
    return $GLOBALS['db']->fetchArray($q1);
}

/**
* update lesson number for class
* Update Start End Date for class
*
* @param mixed $class_id
*
* @author Lap Nguyen
*/
function updateClassSession($class) {
    global $timedate;
    //Update lesson no
    $q1 = "SELECT DISTINCT IFNULL(id, '') primaryid,
    IFNULL(duration_hours, 0) duration_hours,
    IFNULL(duration_minutes, 0) duration_minutes, IFNULL(external_id, '') external_id,
    IFNULL(syllabus_id, '') syllabus_id, IFNULL(type, '') learning_type,
    date_start date_start, date_end date_end
    FROM meetings WHERE ((deleted = 0 AND (session_status <> 'Cancelled') AND (ju_class_id = '{$class->id}')))
    ORDER BY date_start ASC";
    $ss_list = $GLOBALS['db']->fetchArray($q1);
    $till = $week_no = 0;

    $start_change = $timedate->convertToDBDate($_POST['change_date_from'],false);
    $end_change   = $timedate->convertToDBDate($_POST['change_date_to'],false);

    // Add by phgiahannn
    $syllabus = array();
    $rows = getSyllabus($class->lessonplan_id);
    foreach ($rows as $row) {
        $syllabus[$row['lesson']] = $row;
    }

    if(empty($class->start_lesson)) $class->start_lesson = 1;
    $lesson = $class->start_lesson;
    $countSS = count($ss_list);
    for($i = 0; $i < $countSS; $i++){
        $current_date    = date('Y-m-d',strtotime("+7 hours ".$ss_list[$i]['date_start']));
        $current_week_no = (int)date('W',strtotime($current_date));
        if($last_week_no != $current_week_no){
            $week_no++;
            $last_week_no = $current_week_no;
        }

        $ext_sylla = '';
        //Set syllabus
        $syllabus_id = $syllabus[$lesson]['primary_id'];
        if($syllabus_id != $ss_list[$i]['syllabus_id'])
            $ext_sylla .= ", syllabus_id= '$syllabus_id'";

        //GENERATE Online Learning ==> ONLY FOR CREATE CLASS
        $learning_type = $syllabus[$lesson]['learning_type'];
        //TH đã tạo lịch online thì phải dùng chức năng Unlink hệ thống ko update learning_type
        if(empty($ss_list[$i]['external_id']) && $learning_type != $ss_list[$i]['learning_type'])
            $ext_sylla .= ", type = '$learning_type'";


        //Update till hours
        $till += ($ss_list[$i]['duration_hours'] + ($ss_list[$i]['duration_minutes'] / 60));

        //update fields
        $u1 = "UPDATE meetings SET till_hour = ROUND($till,2), lesson_number= '$lesson', week_no='W$week_no' $ext_sylla WHERE id='{$ss_list[$i]['primaryid']}'";
        $GLOBALS['db']->query($u1);

        if($class->syllabus_by == '2'){ //By Day - qua 1 ngày hệ thống mới tính 1 lesson
            $next_date   = date('Y-m-d',strtotime("+7 hours ".$ss_list[$i+1]['date_start']));
            if(empty($_date)) $_date = $current_date;
            if( $_date != $next_date ){
                $lesson++;
                $_date = $next_date;
            }
        }else{
            $lesson++;
            //Xử lý Start Lesson quay lại; 1 ,2 ,3. Lưu ý: Chỉ áp dụng với Syllabus By Session
            //            if($i == ($countSS - $class->start_lesson)) $lesson = 1;
            //            else $lesson++;
            //By session - mỗi buổi là 1 lesson
        }
    }

    //Return DB class start - end date
    $rsClass = array();
    $first = reset($ss_list);
    $start_date = date('Y-m-d',strtotime("+7 hours ".$first['date_start']));

    $last = end($ss_list);
    $end_date = date('Y-m-d',strtotime("+7 hours ".$last['date_start']));

    return array(
        'start_date' => $start_date,
        'end_date'   => $end_date,
        'short_schedule'   => generateSmartSchedule($ss_list), //generate short schedule
    );
}

//////////////// CHECK NEW SALE -  BY LAP NGUYEN \\\\\\\\\\\\\\\\\\\\\\\\\
function revokeSaleType($payment_id = ''){
    if (empty($payment_id)) return false;
    //Lọc tất cả Payment liên quan - Check  lại New Sale
    $q1 = "SELECT DISTINCT
    IFNULL(l2.id, '') rel_pmd_id
    FROM j_payment
    INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
    INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0
    INNER JOIN j_paymentdetail l2 ON l2.payment_id = l1.id AND l2.deleted = 0 AND l2.status = 'Paid'
    WHERE (j_payment.id = '$payment_id')
    AND (l1.payment_type IN ('Deposit', 'Cashholder'))
    AND j_payment.deleted = 0";
    $rs1 = $GLOBALS['db']->query($q1);
    while($row = $GLOBALS['db']->fetchByAssoc($rs1))
        checkSaleType($row['rel_pmd_id'], true);
}

function checkSaleType($primaryId = '', $revokeSale = false){
    if (empty($primaryId)) return false;

    $retention_period = '6 months'; // Thời gian retention đc tính là New Sale
    $new_sale_range   = 0;  //Thời gian chốt sale được tính là New sale -  0: Không xét Chốt, bao lâu tùy ý - ĐVT: Days
    $except_koc       = ['']; // Không xét các chương trình học
    $except_type      = ['Transfer Fee', 'Delay Fee', 'Placement Test', 'Book/Gift','Other'];  // Không xét các loại thanh toán
    $moving_transfer  = 0; // Có/Không TRUE/FALSE xét rule sử dụng Moving/Transfer
    $target_deposit   = 0;  // Tổng số tiền Deposit xét được xét Sale  - 0: Không xét Deposit
    $saleSetBy        = 'Payment'; //Xét sale trên Payment/Receipt

    //return 'New Sale';//SET tạm
    $q1 = "SELECT DISTINCT
    IFNULL(l1.id, '') payment_id, IFNULL(jj.id, '') primaryId,
    l1.payment_date payment_date,
    l1.payment_amount payment_amount,
    jj.payment_amount receipt_amount,
    jj.payment_date receipt_date,
    l1.old_student_id old_student_id,
    IFNULL(l1.payment_type, '') payment_type,
    IFNULL(".(($saleSetBy == 'Receipt') ? 'jj' : 'l1').".sale_type, '') sale_type,
    IFNULL(jj.status, '') status,
    IFNULL(l1.kind_of_course, '') kind_of_course,
    IFNULL(l1.date_entered, '') date_entered,
    SUM(IFNULL(l3.payment_amount,0)) total_paid,
    MAX(l3.payment_date) max_receipt_date,
    GROUP_CONCAT(IFNULL(l4.id, '')) rel_payment_list,
    IFNULL(l2.id, '') student_id
    FROM j_paymentdetail jj
    INNER JOIN j_payment l1 ON l1.id = jj.payment_id AND l1.deleted = 0
    INNER JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
    LEFT JOIN j_paymentdetail l3 ON l3.payment_id = l1.id AND l3.deleted = 0 AND l3.status = 'Paid'
    LEFT JOIN j_payment_j_payment_1_c l4_1 ON l1.id = l4_1.payment_ida AND l4_1.deleted = 0
    LEFT JOIN j_payment l4 ON l4.id = l4_1.payment_idb AND l4.deleted = 0
    WHERE (jj.id = '$primaryId') AND l1.deleted = 0
    GROUP BY l1.id";
    $row = $GLOBALS['db']->fetchOne($q1);

    if(empty($row['max_receipt_date'])) $row['max_receipt_date'] = $row['payment_date'];  // Max receipt date
    if(empty($row['student_id']) || empty($row['payment_id']) || empty($row['max_receipt_date'])) return false;
    if(!empty($row['old_student_id'])) return false; //Bỏ qua các Payment Import
    //Bỏ qua các KOC theo ds ko xét
    foreach(unencodeMultienum($row['kind_of_course']) as $koc)
        if(!empty($koc) && $row['payment_type'] != 'Deposit' && in_array($koc , $except_koc)) return false;
        if(in_array($row['payment_type'],$except_type)) return false; //Bỏ qua các KOC theo ds ko xét
    //Xet Deposit - TH đã set set type rồi thì ko set lại nữa
    if($row['payment_type'] == 'Deposit' && !$revokeSale && (in_array($row['sale_type'], ['New Sale', 'Retention']))) return false;
    if($row['payment_type'] == 'Book/Gift') return false;

    if( (($row['payment_type'] == 'Deposit') && (empty($target_deposit) || ($row['receipt_amount'] < $target_deposit))) //Xet receipt - đủ số tiền deposit tối thiểu
        || (($saleSetBy == 'Payment') && ($row['total_paid'] < $row['payment_amount'])) //Xet TH chưa đóng full phí - $saleSetBy = 'Payment
        || (($saleSetBy == 'Receipt') && ($row['status'] != 'Paid')) ) //Xét TH Unpaid - $saleSetBy = 'Payment
        $sale_type =  'Not set';

    if(empty($sale_type)){
        $basic_rule = newsaleBasicRule($row, $retention_period, $moving_transfer);
        //Update các Payment Liên quan
        $basic_rule = newsaleRelPay($row, $basic_rule, $new_sale_range, $saleSetBy);
        $sale_type = ($basic_rule) ? 'New Sale' : 'Retention';
    }

    // UPDATE SALE_TYPE
    if($row['sale_type'] != $sale_type){
        $GLOBALS['db']->query("UPDATE ".(($saleSetBy == 'Receipt') ? 'j_paymentdetail' : 'j_payment')." SET sale_type = '$sale_type', sale_type_date='{$row['max_receipt_date']}' WHERE id = '".(($saleSetBy == 'Receipt') ? $row['primaryId'] : $row['payment_id'])."'");
        if($saleSetBy == 'Receipt') //Cache Sale Type
            $GLOBALS['db']->query("UPDATE j_payment SET sale_type = '$sale_type', sale_type_date='{$row['max_receipt_date']}' WHERE id = '{$row['payment_id']}'");
    }

    return true;
}

//Rule 1: Học viên không có lesson học trong 6tháng hoặc ko có đóng full phí trong 6 tháng trước
function newsaleBasicRule($payment = '', $duration = 0, $moving_transfer = false){

    //Get last study date: Học viên không có lesson học trong $duration
    if(!empty($duration)){
        $q1 = "SELECT DISTINCT
        IFNULL(contacts.id, '') primaryid,
        MAX(l1.end_study) last_end_study
        FROM contacts
        INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0 AND l1.student_type = 'Contacts'
        WHERE (contacts.id = '{$payment['student_id']}') AND (l1.type = 'Enrolled') AND (l1.payment_id <> '{$payment['payment_id']}')
        AND (l1.date_entered < '{$payment['date_entered']}') AND contacts.deleted = 0
        GROUP BY contacts.id";
        $rule1 = $GLOBALS['db']->fetchOne($q1);
        if(!empty($rule1)){
            $last = date("Y-m-d", strtotime($rule1['last_end_study'] . "+ $duration"));
            if($last > $payment['max_receipt_date']) return false;
        }
        //Tìm Payment Import sau cùng
        $q2 = "SELECT DISTINCT
        IFNULL(l1.id, '') primaryid,
        MAX(j_payment.payment_date) last_end_study
        FROM j_payment INNER JOIN contacts l1 ON j_payment.parent_id = l1.id AND l1.deleted = 0 AND j_payment.parent_type = 'Contacts'
        WHERE (j_payment.is_old = 1) AND (l1.id = '{$payment['student_id']}') AND j_payment.deleted = 0
        GROUP BY l1.id";
        $rule2 = $GLOBALS['db']->fetchOne($q2);
        if(!empty($rule2)){
            $last = date("Y-m-d", strtotime($rule2['last_end_study'] . "+ $duration"));
            if($last > $payment['max_receipt_date']) return false;
        }
    }

    if($moving_transfer){
        //Moving/Transfer used: Có sử dụng tiền Moving/Transfer
        $q3 = "SELECT DISTINCT
        COUNT(IFNULL(l1.id, '')) count
        FROM j_payment
        INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
        INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0
        WHERE (j_payment.id = '{$payment['payment_id']}')
        AND (l1.payment_type IN ('Transfer In', 'Moving In'))
        AND j_payment.deleted = 0";
        $count = $GLOBALS['db']->getOne($q3);
        if ($count > 0) return false;
    }

    return true;
}

//Rule 2: Xet Payment Relate trong 30 ngay
function newsaleRelPay($payment = '', $basic_rule = true, $new_sale_range = 0, $saleSetBy = 'Payment'){

    $max_receipt_date = $payment['max_receipt_date'];
    if(empty($new_sale_range)) $new_sale_range = 9999;  //Chốt bao lâu tuỳ ý
    if(!empty($new_sale_range)){
        //Xác định ngày đóng tiền đầu tiên của gói này
        $q1 = "SELECT DISTINCT
        IFNULL(jj.id, '') primaryId,
        IFNULL(jj.payment_id, '') payment_id,
        jj.payment_date receipt_date,
        IFNULL(".(($saleSetBy == 'Receipt') ? 'jj' : 'l1').".sale_type, '') sale_type
        FROM j_paymentdetail jj
        INNER JOIN j_payment l1 ON jj.payment_id = l1.id AND l1.deleted = 0
        WHERE (l1.id IN ('".implode("','",(explode(",",$payment['rel_payment_list'])))."','{$payment['payment_id']}'))
        AND jj.status = 'Paid' AND jj.deleted = 0
        ORDER BY receipt_date ASC";
        $rows = $GLOBALS['db']->fetchArray($q1);
        $min_receipt_date = $payment['max_receipt_date'];
        foreach($rows as $key => $r)
            if($min_receipt_date > $r['receipt_date']) $min_receipt_date = $r['receipt_date'];

            $due_date = date("Y-m-d",strtotime("+$new_sale_range days ".$min_receipt_date));
        foreach($rows as $key => $r){
            if(empty($r['sale_type']) || $r['sale_type'] == 'Not set'){
                if($r['receipt_date'] >= $min_receipt_date && $r['receipt_date'] <= $due_date){
                    $sale_type = ($basic_rule) ? 'New Sale' : 'Retention'; // xet theo basic_rule
                }else{
                    $sale_type = 'New Sale'; //Case trước ==> New
                    $basic_rule =  false;    // Case hiện tại ==> Retention
                }
                if($r['payment_id'] != $payment['payment_id']){
                    $GLOBALS['db']->query("UPDATE ".(($saleSetBy == 'Receipt') ? 'j_paymentdetail' : 'j_payment')." SET sale_type = '$sale_type', sale_type_date = '$max_receipt_date' WHERE id = '".(($saleSetBy == 'Receipt') ? $r['primaryId'] : $r['payment_id'])."'");
                    if($saleSetBy == 'Receipt') //Cache Sale Type
                        $GLOBALS['db']->query("UPDATE j_payment SET sale_type = '$sale_type', sale_type_date='$max_receipt_date' WHERE id = '{$r['payment_id']}'");
                }
            }
        }
    }

    return $basic_rule;
}
// END CHECK NEW SALE

function getLoyaltyPoint($student_id= '', $not_payment_id='', $payment_date = ''){
    global $timedate;
    if(empty($student_id))
        return 0;

    $ext_not_pm_id = '';
    if(!empty($not_payment_id)){
        $ext_not_pm_id1 = "AND (IFNULL(l4.payment_id,'') <> '$not_payment_id')";
        $ext_not_pm_id2 = "AND (IFNULL(lt.payment_id,'') <> '$not_payment_id')";
    }

    if(!empty($payment_date)){
        $payment_date =  $timedate->convertToDBDate($payment_date, false);
        $ext_payment_date1 = "AND (lt.input_date <= '$payment_date')";

    }

    $loyalty_rank = $GLOBALS['app_list_strings']['loyalty_rank_list'];
    $today = date('Y-m-d');
    $q4 = "SELECT DISTINCT
    IFNULL(contacts.id, '') primaryid,
    IFNULL(contacts.contact_id, '') student_id,
    IFNULL(contacts.full_student_name, '') student_name,
    IFNULL(SUM(l4.payment_amount), 0) net_amount,
    IFNULL(p.total_point, 0) points,
    IFNULL(p.exp_date, '') exp_date
    FROM contacts
    LEFT JOIN j_paymentdetail l4 ON contacts.id = l4.parent_id AND l4.deleted = 0 AND (l4.status = 'Paid') AND (l4.payment_amount > 0)
    LEFT JOIN (SELECT lt.student_id student_id, SUM(lt.point) total_point, MAX(lt.exp_date) exp_date
    FROM j_loyalty lt
    WHERE lt.deleted = 0 AND (lt.student_id = '$student_id' $ext_not_pm_id2 $ext_payment_date1) GROUP BY lt.student_id) p  ON p.student_id = contacts.id
    WHERE ((contacts.id = '$student_id') $ext_not_pm_id1) AND contacts.deleted = 0";
    $rs4 = $GLOBALS['db']->query($q4);
    $row4 = $GLOBALS['db']->fetchByAssoc($rs4);
    //result
    $net_amount = 0 + $current_payment['gross_amount'];
    if(!empty($row4))
        $net_amount = $row4['net_amount'] + $current_payment['gross_amount'];

    if($net_amount < $loyalty_rank['Blue']){
        $level        = 'N/A';
        $next_level   = 'Blue';
        $current_rate = format_number((($net_amount - $loyalty_rank[$level]) / ($loyalty_rank[$next_level] - $loyalty_rank[$level])) * 100,0,0);
    }
    if($net_amount >= $loyalty_rank['Blue']){
        $level        = 'Blue';
        $next_level   = 'Gold';
        $current_rate = format_number((($net_amount - $loyalty_rank[$level]) / ($loyalty_rank[$next_level] - $loyalty_rank[$level])) * 100,0,0);
    }
    if($net_amount >= $loyalty_rank['Gold']){
        $level        = 'Gold';
        $next_level   = 'Platinum';
        $current_rate = format_number((($net_amount - $loyalty_rank[$level]) / ($loyalty_rank[$next_level] - $loyalty_rank[$level])) * 100,0,0);
    }
    if($net_amount >= $loyalty_rank['Platinum']){
        $level        = 'Platinum';
        $next_level   = '';
        $current_rate = '100';
    }

    //Get Loyalty Point
    $getday = date('yy-m-d');
    $q5 = "SELECT SUM(point) as points FROM j_loyalty WHERE student_id ='$student_id' AND exp_date >'$getday'";
    $rs5 = $GLOBALS['db']->query($q5);
    $row5 = $GLOBALS['db']->fetchByAssoc($rs5);

    return array(
        'student_id'=> $row4['primaryid'],
        'code'      => $row4['code'],
        'level'     => $level,
        'next_level'=> $next_level,
        'current_rate'=> $current_rate,
        'points'    => $row4['points'],
        'net_amount'=> $row4['net_amount'],
    );
}
function getLoyaltyRateOut($mem_level= '', $team_id = '', $year = ''){

    $redemp_rate = array('id' => '','value' => $GLOBALS['app_list_strings']['default_loyalty_rate']['Conversion Rate']);
    return $redemp_rate;
}
?>
