<?php
class TapTapHelper{
    private $keycloak_host;
    private $gateway_host;
    private $grant_type;
    private $client_id;
    private $client_secret;
    private $token;

    function __construct() {
        $admin = new Administration();
        $admin->retrieveSettings();
        $this->keycloak_host = (!empty($admin->settings['taptap_config_keycloak_host']) ? $admin->settings['taptap_config_keycloak_host'] : '');
        $this->gateway_host = (!empty($admin->settings['taptap_config_gateway_host']) ? $admin->settings['taptap_config_gateway_host'] : '');
        $this->grant_type = (!empty($admin->settings['taptap_config_grant_type']) ? $admin->settings['taptap_config_grant_type'] : '');
        $this->client_id = (!empty($admin->settings['taptap_config_client_id']) ? $admin->settings['taptap_config_client_id'] : '');
        $this->client_secret = (!empty($admin->settings['taptap_config_client_secret']) ? $admin->settings['taptap_config_client_secret'] : '');
    }

    function requestToken(){
        if(empty($this->keycloak_host)) return array(
            'success' => false,
            'message' => 'Error: TAPTAP config not found'
        );
        $url = "{$this->keycloak_host}/auth/realms/mobile/protocol/openid-connect/token";
        $param = "grant_type=" . $this->grant_type;
        $param .= "&client_id=" . $this->client_id;
        $param .= "&client_secret=" . $this->client_secret;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err) {
            $data = json_decode($response);
            if(!empty($data->access_token)){
                $this->token = $data->access_token;
                return array(
                    'success'=>true,
                    'message'=>'Success'
                );
            }else return array(
                'success' =>false,
                'message' =>  'Error: ' . $data->error
            );
        }else return array(
            'success' =>false,
            'message' => 'Error: ' . $err
        );
    }

    function addCustomer($post){
        $url = "{$this->gateway_host}/v1/customers";

        $authorization = "Authorization: Bearer " . $this->token;

        $curl = curl_init($url); // Initialise cURL
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization)); // Inject the token into the header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $response = curl_exec($curl); // Execute the cURL statement
        $err = curl_error($curl);
        curl_close($curl); // Close the cURL connection
        if (!$err) {
            $data = json_decode($response);
            if($data->response->status->success == 'true'){
                return array(
                    'success' => true,
                    'message' => $data->response->status->message,
                    'data' => array(
                        'first_name' => $data->response->customers->customer[0]->firstname,
                        'last_name' => $data->response->customers->customer[0]->lastname,
                        'phone_mobile' => $data->response->customers->customer[0]->mobile,
                    ),
                );
            }else return array(
                'success' => false,
                'message' => 'Error: ' . $data->response->customers->customer[0]->item_status->message . ' - ' 
                . $data->response->customers->customer[0]->item_status->code
            );
        }else return array(
            'success' => false,
            'message' => 'Error: ' . $err
        );

    }

    function requestTransaction($post){
        $url = "{$this->gateway_host}/v1/transactions";

        $authorization = "Authorization: Bearer " . $this->token;

        $curl = curl_init($url); // Initialise cURL
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization)); // Inject the token into the header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $response = curl_exec($curl); // Execute the cURL statement
        $err = curl_error($curl);
        curl_close($curl); // Close the cURL connection
        if (!$err) {
            $data = json_decode($response);
            if($data->response->status->success == 'true'){
                return array(
                    'success' => true,
                    'message' => $data->response->status->message,
                    'data' => array(
                        'phone_mobile' => $data->response->data->mobile,
                    ),
                );
            }else return array(
                'success' => false,
                'message' => 'Error: ' . $data->response->status->message . '_'
                . $data->response->status->code
            );
        }else return array(
            'success' => false,
            'message' => 'Error: ' . $err
        );
    }

    function searchCustomer($mobile){
        $url = "{$this->gateway_host}/v1/customers?mobile={$mobile}";
        $authorization = "Authorization: Bearer " . $this->token;

        $curl = curl_init($url); // Initialise cURL
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization)); // Inject the token into the header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($curl); // Execute the cURL statement
        $err = curl_error($curl);
        curl_close($curl); // Close the cURL connection
        if (!$err) {
            $data = json_decode($response);
            if($data->response->customers->customer[0]->item_status->success == 'true'){
                return array(
                    'success' => true,
                    'message' => $data->response->customers->customer[0]->item_status->message,
                    'data' => array(
                        'first_name' => $data->response->customers->customer[0]->firstname,
                        'last_name' => $data->response->customers->customer[0]->lastname,
                        'phone_mobile' => $data->response->customers->customer[0]->mobile,
                    ),
                );
            } else return array(
                'success' => false,
                'message' => 'Error: ' . $data->response->customers->customer[0]->item_status->message
            );
        }else return array(
            'success' => false,
            'message' => 'Error: ' . $err
        );
    }

    function validateVoucher($code, $mobile, $store_code){
        $url = "{$this->gateway_host}/v1/vouchers/validate?code={$code}&mobile={$mobile}&store_code={$store_code}";
        $authorization = "Authorization: Bearer " . $this->token;

        $curl = curl_init($url); // Initialise cURL
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization)); // Inject the token into the header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($curl); // Execute the cURL statement
        $err = curl_error($curl);
        curl_close($curl); // Close the cURL connection
        if (!$err) {
            $data = json_decode($response);
            if($data->response->coupons->redeemable->item_status->success == 'true'){
                if($data->response->coupons->redeemable->series_info->discount_type == 'ABS'){
                    $discount_amount = $data->response->coupons->redeemable->series_info->discount_value;
                } elseif($data->response->coupons->redeemable->series_info->discount_type == 'PERC'){
                    $discount_percent = $data->response->coupons->redeemable->series_info->discount_value;
                }
                $voucher_info = array(
                    'code'=> $data->response->coupons->redeemable->code,
                    'valid_till'=> $data->response->coupons->redeemable->series_info->valid_till,
                    'discount_amount'=> empty($discount_amount) ? '' : $discount_amount,
                    'discount_percent'=> empty($discount_percent) ? '' : $discount_percent,
                    'description'=> $data->response->coupons->redeemable->series_info->description,
                );
                return array(
                    'success' => true,
                    'message' => $data->response->coupons->redeemable->item_status->message,
                    'data'    => $voucher_info
                );
            } elseif ($data->response->coupons->redeemable->item_status->code == 711) {
                $voucher_info = array(
                    'code'=> $data->response->coupons->redeemable->code,
                    'valid_till'=> '',
                    'discount_amount'=> '',
                    'discount_percent'=> '',
                    'description'=> $data->response->coupons->redeemable->item_status->message,
                );
                return array(
                    'success' => true,
                    'message' => 'Error: ' . $data->response->coupons->redeemable->item_status->message,
                    'data'    => $voucher_info
                );
            }
            else  return array(
                'success' => false,
                'message' => 'Error: ' . $data->response->coupons->redeemable->item_status->message
            );
        }else return array(
            'success' => false,
            'message' => 'Error: ' . $err
        );
    }

    function redeemVoucher($post){
        $url = "{$this->gateway_host}/v1/vouchers/redeem";

        $authorization = "Authorization: Bearer " . $this->token;

        $curl = curl_init($url); // Initialise cURL
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization)); // Inject the token into the header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1); // Specify the request method as POST
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post); // Set the posted fields
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // This will follow any redirects
        $response = curl_exec($curl); // Execute the cURL statement
        $err = curl_error($curl);
        curl_close($curl); // Close the cURL connection
        if (!$err) {
            $data = json_decode($response);
            if($data->response->coupons->coupon->item_status->success == 'true'){
                return array(
                    'success' => true,
                    'message' => $data->response->coupons->coupon->item_status->message
                );
            }else return array(
                'success' => false,
                'message' => 'Error: ' . $data->response->coupons->coupon->item_status->message
            );
        }else return array(
            'success' => false,
            'message' => 'Error: ' . $err
        );
    }

    function createCustomer($bean){//param $bean contact
        //get mobile
        $phone_mobile = $bean->phone_mobile;
        if($bean->phone_mobile[0] == '0'){
            $phone_mobile = "84" . ltrim($bean->phone_mobile, "0");
        }
        $responeSearchCustomer = $this->searchCustomer($phone_mobile);
        if (!$responeSearchCustomer['success']) {
            //get team code
            $q1 = "SELECT code_prefix FROM teams WHERE id = '{$bean->team_id}'";
            $team_code = $GLOBALS['db']->getOne($q1);
            
            $param = array(
                'firstname' => $bean->first_name,
                'lastname' => $bean->last_name,
                'mobile' => $phone_mobile,
                'external_id' => $bean->id,
                'dob' => $bean->birthdate,
                'registered_store' => $team_code,
                'registered_date' => $bean->date_entered,
                'address' => $bean->primary_address_street
            );
            $post = json_encode($param, JSON_UNESCAPED_UNICODE);

            $responeAddCustomer = $this->addCustomer($post);
            if ($responeAddCustomer['success']) {
                return array(
                    'status' => 'Success',
                    'message' => 'Create new customer',
                    'data_sent' => $param,
                    'result' => $responeAddCustomer['data'],
                );
            }else {
                return array(
                    'status' => 'Fail',
                    'message' => $responeAddCustomer['message'],
                    'data_sent' => $param,
                );
            }
        } else {
            return array(
                'status' => 'Success',
                'message' => 'Data Exist',
                'data_sent' => array('mobile' => $phone_mobile),
                'result' => $responeSearchCustomer['data'],
            );
        }
    }

    function addTransaction($bean, $payment_type){
        //check customer TAPTAP
        $qCheck = "SELECT external_connection FROM contacts WHERE id='{$bean->parent_id}' AND deleted = 0";
        $customer = html_entity_decode($GLOBALS['db']->getOne($qCheck));
        $customerObject = json_decode($customer);
        if(empty($customerObject->external_accumulation->TAPTAP->status)){
            $beanContact = BeanFactory::getBean('Contacts', $bean->parent_id);
            if(!empty($beanContact->id)){
                $resultCustomer = $this->createCustomer($beanContact);
                $customerObject->external_accumulation->TAPTAP = json_decode(json_encode($resultCustomer, JSON_UNESCAPED_UNICODE));
                $customer = json_encode($customerObject, JSON_UNESCAPED_UNICODE);
                $GLOBALS['db']->query("UPDATE contacts SET external_connection = '{$customer}' WHERE id = '{$beanContact->id}'");
            }
        }
        $customer = $customerObject;
        // check empty $customer TH $bean->student_id rỗng
        if(empty($customer) || $customer->external_accumulation->TAPTAP->status != "Success" 
        || empty($customer->external_accumulation->TAPTAP->result->phone_mobile)){
            return array(
                'status' => 'Fail',
                'message' => 'Customer not found',
            );
        }

        //get param transaction
        $phone_mobile = $customer->external_accumulation->TAPTAP->result->phone_mobile;
        switch ($bean->payment_method) {
            case 'Cash' :
                $payment_mode = 'CASH';
                break;
            case 'Card' :
                $payment_mode = 'CARD';
                break;
            case 'Bank Transfer' :
                $payment_mode = 'ONLINE ORDER';
                break;
            case 'Payoo' :
                $payment_mode = 'WALLETS';
                break;
            case 'Other' :
            default:
                $payment_mode = 'OTHERS';
        }
        $payment_info = array(
            'mode' => $payment_mode,
            'value' => $bean->payment_amount,
        );
        $post = $this->getParamTransaction($bean, $payment_type, $phone_mobile);
        $post['payments'] = array($payment_info);
        $post['type'] = 1;

        //add transaction taptap
        $responeTransaction = $this->requestTransaction(json_encode($post, JSON_UNESCAPED_UNICODE));
        if ($responeTransaction['success']) {
            return array(
                'status' => 'Success',
                'message' => 'Add transaction',
                'data_sent' => $post,
                'result' => $responeTransaction['data'],
            );
        } else {
            return array(
                'status' => 'Fail',
                'message' => $responeTransaction['message'],
                'data_sent' => $post,
            );
        }
    }

    function returnTransaction($bean, $payment_type){
        //check transaction (giao dich dc add truoc do)
        $external_connection = json_decode(html_entity_decode($bean->external_connection));
        if($external_connection->external_accumulation->TAPTAP->add_transaction->status != "Success"
        || empty($external_connection->external_accumulation->TAPTAP->add_transaction->result->phone_mobile)){
            return array(
                'status' => 'Fail',
                'message' => 'Transaction not found',
            );
        }

        //get param transaction
        $phone_mobile = $external_connection->external_accumulation->TAPTAP->add_transaction->result->phone_mobile;
        $post = $this->getParamTransaction($bean, $payment_type, $phone_mobile);
        $post['payments'] = array();
        $post['type'] = 5;

        //add transaction taptap
        $responeTransaction = $this->requestTransaction(json_encode($post, JSON_UNESCAPED_UNICODE));
        if ($responeTransaction['success']) {
            return array(
                'status' => 'Success',
                'message' => 'Return transaction',
                'data_sent' => $post,
                'result' => $responeTransaction['data'],
            );
        } else {
            return array(
                'status' => 'Fail',
                'message' => $responeTransaction['message'],
                'data_sent' => $post,
            );
        }
    }

    function getParamTransaction($beanPmdt, $payment_type, $phone_mobile){
        //get store code
        $qGetCenterCode = "SELECT code_prefix FROM teams WHERE id = '{$beanPmdt->team_id}'";
        $store_code = $GLOBALS['db']->getOne($qGetCenterCode);
        //get order_code
        $qGetInfo = "SELECT DISTINCT IFNULL(l1.name, '') order_code
            FROM j_paymentdetail
            INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
            WHERE j_paymentdetail.id = '{$beanPmdt->id}' AND j_paymentdetail.deleted = 0";
        $order_code = $GLOBALS['db']->getOne($qGetInfo);

        //get course name
        if($payment_type == 'Cashholder'){
            $qGetCourseFee = "SELECT DISTINCT
                GROUP_CONCAT(IFNULL(l2.name, '')) AS name
            FROM j_paymentdetail
            INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
            INNER JOIN j_coursefee_j_payment_2_c l2_1 ON l1.id = l2_1.j_coursefee_j_payment_2j_payment_idb AND l2_1.deleted = 0
            INNER JOIN j_coursefee l2 ON l2.id = l2_1.j_coursefee_j_payment_2j_coursefee_ida AND l2.deleted = 0
            WHERE j_paymentdetail.id = '{$beanPmdt->id}' 
                AND j_paymentdetail.deleted = 0";
        $course_name = $GLOBALS['db']->getOne($qGetCourseFee);
        } elseif($payment_type == "Deposit") {
            $course_name = "Deposit for " . trim($beanPmdt->kind_of_course, "^") . " course";
        }

        return array(
            'additional_discount' => $beanPmdt->discount_amount,
            'amount' => $beanPmdt->payment_amount,
            'number' => 'sylvan_' . $order_code . '_' . $beanPmdt->name,
            'billing_time' => $beanPmdt->payment_date,
            'store_code' => $store_code,
            'discount' => $beanPmdt->sponsor_amount,
            'gross_amount' => $beanPmdt->before_discount,
            'mobile' => $phone_mobile,
            'course_name' => $course_name,
            'order_code' => $order_code,
            'order_id' => $order_code,
        );
    }

    function checkIsExternalAccumulate($pmdtId){
        $qCheck = "SELECT DISTINCT
            COUNT(l2.is_external_accumulate) AS is_not_external_accumulate,
                IFNULL(l1.payment_type, '') payment_type 
            FROM j_paymentdetail
            INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
            INNER JOIN j_coursefee_j_payment_2_c l2_1 ON l1.id = l2_1.j_coursefee_j_payment_2j_payment_idb AND l2_1.deleted = 0
            INNER JOIN j_coursefee l2 ON l2.id = l2_1.j_coursefee_j_payment_2j_coursefee_ida AND l2.deleted = 0
            WHERE j_paymentdetail.id = '{$pmdtId}' AND j_paymentdetail.deleted = 0 
                AND(l2.is_external_accumulate = 0 OR l2.is_external_accumulate IS NULL)";
        $resCheck = $GLOBALS['db']->fetchOne($qCheck);
        return $resCheck;
    }
}
