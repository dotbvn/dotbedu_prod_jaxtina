<?php
//Load Data Lock Config
function loadDataLockConfig(){
    global $dotb_config, $timedate;
    $lock_info = $dotb_config['lock_info'];
    $lock_info['enable'] = false;
    //Lock back-dated field  - ACL_ROLE
    $q2 = "SELECT acl_roles.id id, acl_roles.name name,  IFNULL(acl_roles.is_lock_dated,0) is_lock_dated
    FROM acl_roles
    INNER JOIN acl_roles_users ON acl_roles_users.user_id = '{$GLOBALS['current_user']->id}'
    AND acl_roles_users.role_id = acl_roles.id
    AND acl_roles_users.deleted = 0
    WHERE acl_roles.deleted = 0";
    $rs2 = $GLOBALS['db']->query($q2);
    while($row = $GLOBALS['db']->fetchByAssoc($rs2) ){
        if($row['is_lock_dated']){
            $lock_info['enable'] = true;
            break;
        }else $lock_info['enable'] = false;
    }

    //except lock user admin
    if($GLOBALS['current_user']->isAdmin()) $lock_info['enable'] = false;
    //except lock list
    if (!empty($lock_info['except_lock_for_user_name'])){
        $exceptList = explode(",", $lock_info['except_lock_for_user_name']);
        if (in_array($GLOBALS['current_user']->user_name, $exceptList)) $lock_info['enable'] = false;
    }

    if (strpos($lock_info['lock_type'], 'month') !== false) $lock_info['lock_type'] = 'last_month';//this_month đang bị sai, không xài
    if(empty($lock_info['lock_type'])) $lock_info['lock_type'] = 'last_month';
    if(empty(intval($lock_info['lock_back']))) $lock_info['lock_back'] = 1; //days
    else $lock_info['lock_back'] = intval($lock_info['lock_back']);
    if(empty($lock_info['lock_date'])) $lock_info['lock_date'] = '01-00';

    return array (
        'enable' => $lock_info['enable'],
        'lock_type' => $lock_info['lock_type'],
        'lock_date' => $lock_info['lock_date'],
        'lock_back' => $lock_info['lock_back'],
        'except_lock_for_user_name' => $lock_info['except_lock_for_user_name'],
    );
}