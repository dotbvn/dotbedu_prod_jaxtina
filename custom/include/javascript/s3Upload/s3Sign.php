<?php

// You will need to set these constants or variables somewhere in your configuration
define('S3_BUCKET_NAME', $GLOBALS['dotb_config']['storage_service']['bucket_name']);
define('S3_SECRET_KEY', $GLOBALS['dotb_config']['storage_service']['api_secret']);
define('S3_ACCESS_KEY', $GLOBALS['dotb_config']['storage_service']['api_key']);
define('S3_URL', $GLOBALS['dotb_config']['storage_service']['host_url']);

// Assuming this endpoint is called with GET or POST method
if ($_SERVER['REQUEST_METHOD'] === 'GET' || $_SERVER['REQUEST_METHOD'] === 'POST') {
    $objectName = $_GET['s3_object_name'] ?? $_POST['s3_object_name'];
    $mimeType = $_GET['s3_object_type'] ?? $_POST['s3_object_type'];
    $expires = time() + 100; // PUT request to S3 must start within 100 seconds

    $amzHeaders = "x-amz-acl:public-read"; // set the public read permission on the uploaded file
    $stringToSign = "PUT\n\n{$mimeType}\n{$expires}\n{$amzHeaders}\n/" . S3_BUCKET_NAME . "/{$objectName}";

    $signature = base64_encode(hash_hmac('sha1', $stringToSign, S3_SECRET_KEY, true));
    $signedRequest = S3_URL . "/{$objectName}?AWSAccessKeyId=" . S3_ACCESS_KEY . "&Expires={$expires}&Signature={$signature}";

    $response = [
        'signed_request' => $signedRequest,
        'url' => S3_URL . "/{$objectName}"
    ];

    echo json_encode($response);
}

