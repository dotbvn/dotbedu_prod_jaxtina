(function() {
  window.S3Upload = (function() {
    S3Upload.prototype.s3_sign_put_url = '/signS3put';

    S3Upload.prototype.file_dom_selector = '#file_upload';
    
    S3Upload.prototype.acl_value = 'public-read'

    S3Upload.prototype.onFinishS3Put = function(public_url, file) {
      return console.log('base.onFinishS3Put()', public_url, file);
    };

    S3Upload.prototype.onProgress = function(percent, status, public_url, file) {
      return console.log('base.onProgress()', percent, status, public_url, file);
    };

    S3Upload.prototype.onError = function(status, file) {
      return console.log('base.onError()', status, file);
    };

    function S3Upload(options) {
      if (options == null) {
        options = {};
      }
      _.extend(this, options);
      if (this.file_dom_selector) {
        debugger
        this.handleFileSelect($(this.file_dom_selector).get(0));
      }
    }

    S3Upload.prototype.handleFileSelect = function(file_element) {
      var files;
      files = file_element.files;
      return this.uploadFiles(files);
    };

    S3Upload.prototype.uploadFiles = function(files) {
      var f, _i, _len, _results;
      this.onProgress(0, 'Upload started.');
      _results = [];
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        f = files[_i];
        _results.push(this.uploadFile(f));
      }
      return _results;
    };

    S3Upload.prototype.createCORSRequest = function(method, url) {
      var xhr;
      xhr = new XMLHttpRequest();
      if (xhr.withCredentials != null) {
        xhr.open(method, url, true);
      } else if (typeof XDomainRequest !== "undefined") {
        xhr = new XDomainRequest();
        xhr.open(method, url);
      } else {
        xhr = null;
      }
      return xhr;
    };

    S3Upload.prototype.executeOnSignedUrl = function(file, callback, opts) {
      debugger
      var name, type;
      name = opts && opts.name || file.name;
      if(file.name) name = App.config.brand_id + '/chat/' + this.generateUUID() + '.' + file.name.split('.').pop();
      type = opts && opts.type || file.type || "application/octet-stream";
      let this_s3upload = this;

      var formData = new FormData();
      formData.append('s3_object_type', type);
      formData.append('s3_object_name', name);
      formData.append('acl_value', this.acl_value);

      $.ajax({
        url: this.s3_sign_put_url,
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
          debugger
          var result;
          try {
            result = JSON.parse(response);
            callback(result.signed_request, result.url); // Assuming callback is defined and handles these parameters
          } catch (error) {
            this_s3upload.onError('Signing server returned some ugly/empty JSON: "' + response + '"');
          }
        },
        error: function(xhr, status, error) {
          this_s3upload.onError('Could not contact request signing server. Status = ' + xhr.status);
        }
      });
    };
    S3Upload.prototype.generateUUID = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    }
    S3Upload.prototype.uploadToS3 = function(file, url, public_url, opts) {
      debugger
      var type, this_s3upload = this;
      type = opts && opts.type || file.type || "application/octet-stream";
      var method = 'PUT';
      $.ajax({
        url: url, // The URL to which you are sending the PUT request
        type: method,
        data: file, // The file object to be uploaded
        processData: false, // Tell jQuery not to process the data
        contentType: type, // Set the content type header
        headers: {
          'x-amz-acl': this.acl_value // Additional headers can be set like this
        },
        xhr: function() {
          debugger
          var xhr = new window.XMLHttpRequest();
          if ("withCredentials" in xhr) {
            xhr.open(method, url, true);
          } else if (typeof XDomainRequest !== "undefined") { // Fallback for IE9 and older
            xhr = new XDomainRequest();
            xhr.open(method, url);
          } else { // CORS not supported by the browser
            xhr = null;
          }
          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = Math.round((evt.loaded / evt.total) * 100);
              // Call the onProgress function
              this_s3upload.onProgress(percentComplete, (percentComplete === 100 ? 'Finalizing.' : 'Uploading.'), public_url, file);
            }
          }, false);
          return xhr;
        },
        success: function(data, textStatus, xhr) {
          if (xhr.status === 200) {
            debugger
            // Call onFinishS3Put when upload is completed
            this_s3upload.onProgress(100, 'Upload completed.', public_url, file);
            this_s3upload.onFinishS3Put(public_url, file);
          }
        },
        error: function(xhr, textStatus, errorThrown) {
          debugger
          if (xhr.status !== 200) {
            this_s3upload.onError('Upload error: ' + xhr.status, file);
          }
        }
      });
    };

    S3Upload.prototype.validate = function(file) {
      return null;
    };

    S3Upload.prototype.uploadFile = function(file, opts) {
      var error, this_s3upload;
      error = this.validate(file);
      if (error) {
        this.onError(error, file);
        return null;
      }
      this_s3upload = this;
      return this.executeOnSignedUrl(file, function(signedURL, publicURL) {
        return this_s3upload.uploadToS3(file, signedURL, publicURL, opts);
      }, opts);
    };

    return S3Upload;

  })();

}).call(this);
