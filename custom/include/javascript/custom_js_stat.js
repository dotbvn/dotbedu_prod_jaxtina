$(document).ready(function(){
    $('.stats_item_list, .stats_item_grid, .stats_item_grid_none_compare').click(function(){
        window.location.href = '#bwc/index.php?module=Reports&action=DetailView&record='+this.id;
    });
    $(".sortable-list, .grid-container").sortable({
        placeholder: "ui-state-highlight",
        start: function(event, ui) {
            // Capture the original index
            originalIndex = ui.item.index();
        },
        update: function(event, ui) {
            const newIndex = ui.item.index();
            if(newIndex !== originalIndex){
                let data = {
                    idDashlet: this.attributes["data-dashlet-id"].nodeValue,
                    oldIndex: originalIndex,
                    newIndex: newIndex,
                    idDashboard: this.attributes["data-dashboard-id"].nodeValue
                }
                $.ajax({
                    url:'index.php?module=Dashboards&action=HandleAjaxAction&dotb_body_only=true' ,
                    type: 'POST',
                    dataType: "json", // Expected data type from the server
                    data: {
                        type: 'reArrangeItem',
                        indexData:data
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    },
                    success: function (res) {
                        console.log('Arrange Success')
                    }
                });
            }

        }
    });
});

