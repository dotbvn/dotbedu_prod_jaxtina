<?php
if(!empty($_POST['action'])){
    switch ($_POST['action']){
        case 'checkConnect':
            checkConnect();
        case 'deleteNote':
            deleteNote();
            break;
        case 'createNote':
            createNote();
            break;
        case 'getNote':
            getNote();
            break;
        case 'downloadNotes':
            downloadNotes();
            break;
        case 'downloadZip':
            downloadZip();
            break;
    }
}
function checkConnect(){
    $AWS = new AWSHelper();
    if (!$AWS->getS3()) {
        echo json_encode([
            'success' => false,
            'error_num' => 101,
            'message' => 'Error connecting to Amazon S3'
        ]);
    } else {
        echo json_encode([
            'success' => true,
            'message' => 'Connect to Amazon S3 successfully'
        ]);
    }
    exit();
}
function createNote(){
    $fileName = $_FILES["file"]["name"]; // The file name
    $fileTmpLoc = $_FILES["file"]["tmp_name"]; // File in the PHP tmp folder
    $fileType = $_FILES["file"]["type"]; // The type of file it is
    $fileSize = $_FILES["file"]["size"]; // File size in bytes
    $fileErrorMsg = $_FILES["file"]["error"]; // 0 for false... and 1 for true

    require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';


    if (!$fileTmpLoc) { // if file not chosen
        echo "ERROR: Please browse for a file before clicking the upload button.";
        exit();
    }
    $user_id = $_POST['userId'];
    $GLOBALS['current_user'] = BeanFactory::getBean('Users', $user_id);
    $ext = pathinfo($fileName, PATHINFO_EXTENSION) == 'png' ? 'jpeg' : pathinfo($fileName, PATHINFO_EXTENSION);
    $parent_module = $_POST['parentModule'];
    $note = BeanFactory::newBean('Notes');
    $note->id = create_guid();
    $note->new_with_id = true;
    $note->name = $note->id . '.' . $ext;
    $note->file_mime_type = $fileType;
    $note->file_ext = pathinfo($fileName, PATHINFO_EXTENSION);
    $note->filename = $fileName;
    $note->file_size = $fileSize;
    $note->modified_user_id = $user_id;
    $note->created_by = $user_id;
    $note->team_set_id = $GLOBALS['current_user']->team_set_id;
    $note->team_id = $GLOBALS['current_user']->team_id;
    $note->parent_type = $parent_module;
    $note->save();
    $file_name = $note->name;
    $_dir = 'upload/tmp';
    if (!file_exists($_dir)) mkdir($_dir, 0777, true);
    $sourceFile = 'upload/tmp/' . $file_name;
    if (move_uploaded_file($fileTmpLoc, $sourceFile)) {
        if(strstr($note->file_mime_type, 'image')){
            require_once('include/DotbFields/Fields/Image/ImageHelper.php');
            $image = new ImageHelper($sourceFile);
            $sourceFile = $image->convertToJpeg();
            $image->save($sourceFile, 80);
            $GLOBALS['db']->query('UPDATE notes SET file_size = ' . filesize($sourceFile) . ' WHERE id = "' . $note->id . '"');
        }
        echo json_encode([
            'success' => true,
            'id' => $note->id
        ]);
    } else {
        echo json_encode([
            'success' => false,
            'error_num' => 102,
            'message' => 'Error uploading file.'
        ]);
    }
    exit();
}
function deleteNote(){
    $note_id = $_POST['noteId'];
    $note = BeanFactory::getBean('Notes', $note_id);
    $source_file = 'upload/tmp/' . $note->name;
    unlink($source_file);
    echo json_encode([
        'success' => true,
        'message' => 'Delete note successfully'
    ]);
    exit();
}
function getNote(){
    if(!empty($_POST['parentModule']) && !empty($_POST['parentId'])){
        $parent_type = $_POST['parentModule'];
        $parent_id = $_POST['parentId'];
        $sqlNote = "SELECT IFNULL(id,'') id, 
        IFNULL(name,'') notes_name,  
        IFNULL(upload_id,'') upload_id,  
        IFNULL(file_source,'') file_source,  
        IFNULL(filename,'') name,  
        IFNULL(file_mime_type,'') type,  
        IFNULL(file_ext,'') file_ext,  
        IFNULL(file_size,'') size 
        FROM notes 
        WHERE parent_id = '$parent_id' 
          AND parent_type = '$parent_type'
          AND deleted = 0";

        $result = $GLOBALS['db']->fetchArray($sqlNote);
        foreach($result as $key => $value){
            $file_name = $value['notes_name'];
            $srcImg = $value['file_source'] == 'S3' ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $value['upload_id'] : 'download_attachment.php?path=s3_storage/' . $file_name;
            $result[$key]['src'] = $srcImg;
        }
        echo json_encode([
            'success' => true,
            'data' => [
                'note_list' => $result
            ]
        ]);
    }
    exit();
}
function downloadNotes(){
    if(!empty($_POST['noteId'])) {
        $note_id = $_POST['noteId'];
        $note = BeanFactory::getBean('Notes', $note_id);
        $sourceFile = 'cache/' . $note->name;
        // This part is used to cache the video file from S3 to local
        if ($note->file_source == 'S3') {
            if (!file_exists($sourceFile)) {
                $fileContent = file_get_contents($GLOBALS['dotb_config']['storage_service']['host_url'] . $note->upload_id);
                if ($fileContent) file_put_contents($sourceFile, $fileContent);
            }
        } else {
            if (!file_exists($sourceFile)) {
                $fileContent = file_get_contents('upload/s3_storage/' . $note->name);
                if ($fileContent) file_put_contents($sourceFile, $fileContent);
            }
        }
        echo json_encode([
            'success' => true,
            'data' => [
                'file_source' => $sourceFile
            ]
        ]);
    }
}
function downloadZip(){
    if(!empty($_POST['parentId']) && !empty($_POST['parentModule'])) {
        $parent_id = $_POST['parentId'];
        $parent_type = $_POST['parentModule'];

        $sqlNote = "SELECT IFNULL(id,'') id, 
        IFNULL(name,'') notes_name,  
        IFNULL(upload_id,'') upload_id,  
        IFNULL(file_source,'') file_source,  
        IFNULL(filename,'') file_name,  
        IFNULL(file_mime_type,'') type,  
        IFNULL(file_ext,'') file_ext,  
        IFNULL(file_size,'') size 
        FROM notes 
        WHERE parent_id = '$parent_id' 
          AND parent_type = '$parent_type'
          AND deleted = 0";

        $result = $GLOBALS['db']->fetchArray($sqlNote);
        foreach($result as $key => $value){
            $sourceFile = '/upload/s3_storage/' . $value['notes_name'];
            if ($value['file_source'] == 'S3') {
                if (!file_exists($sourceFile)) {
                    $fileContent = file_get_contents($GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $value['upload_id']);
                    if ($fileContent) file_put_contents($sourceFile, $fileContent);
                }
            }
            $note_list[] = [
                'file_name' => $value['file_name'],
                'file_source' => $GLOBALS['dotb_config']['site_url'] . $sourceFile
            ];
        }
        echo json_encode([
            'success' => true,
            'data' => [
                'file_list' => $note_list,
            ]
        ]);
    }
}
