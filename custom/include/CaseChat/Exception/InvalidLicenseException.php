<?php

namespace CaseChat\Exception;

/**
 * @author Exsitec AB
 */
class InvalidLicenseException extends \DotbApiException
{
    public $errorLabel = 'invalid_license';
    public $messageLabel = 'ERROR_INVALID_LICENSE';
    public $httpCode = 403;
}
