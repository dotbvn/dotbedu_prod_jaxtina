<?php

namespace CaseChat;

/**
 * @author Exsitec AB
 */
class Config
{
    const LICENCE_TYPE_EXSITEC = 'exsitec';
    const LICENCE_TYPE_OUTFITTERS = 'outfitters';
    
    /**
     * @var array
     */
    private static $availableKeys = array(
        'cc_license_type',
        'cc_license_key',
        'cc_validation_key'
    );

    /**
     * @var array|null
     */
    private $properties;

    /**
     * @return array
     */
    public function load()
    {
        if (is_null($this->properties)) {
            $admin = \Administration::getSettings('Cases');

            foreach (self::$availableKeys as $availableKey) {
                $key = 'Cases_'.$availableKey;

                if (isset($admin->settings[$key])) {
                    $this->properties[$availableKey] = $admin->settings[$key];
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getLicenseType()
    {
        return $this->getValue('cc_license_type');
    }

    /**
     * @return string
     */
    public function getLicenseKey()
    {
        return $this->getValue('cc_license_key');
    }

    /**
     * @return string
     */
    public function getValidationKey()
    {
        return $this->getValue('cc_validation_key');
    }

    /**
     * @param string $validationKey
     */
    public function setValidationKey($validationKey)
    {
        $this->load();
        $this->properties['cc_validation_key'] = $validationKey;
    }

    /**
     * @param string $licenseKey
     */
    public function setLicenseKey($licenseKey)
    {
        $this->load();
        $this->properties['cc_license_key'] = $licenseKey;
    }

    /**
     * @param string $name
     * @param null $default
     *
     * @return null
     */
    private function getValue($name, $default = null)
    {
        $this->load();
        return isset($this->properties[$name]) ? $this->properties[$name] : $default;
    }
}
