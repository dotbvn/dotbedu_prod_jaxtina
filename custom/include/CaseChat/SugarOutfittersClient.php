<?php

namespace CaseChat;

require_once 'modules/Administration/Administration.php';

use CaseChat\Exception\InvalidLicenseException;
use Dotbcrm\Dotbcrm\Security\HttpClient\ExternalResourceClient;

/**
 * @author Exsitec AB
 */
class DotbOutfittersClient
{
    const PARAM_LICENSE_KEY = 'key';
    const PARAM_PUBLIC_KEY  = 'public_key';
//    const PARAM_USER_COUNT  = 'user_count';

    const CACHE_LAST_RAN    = 'last_ran';
    const CACHE_LAST_RESULT = 'last_result';
    const CACHE_PARAMS      = 'params';

    /**
     * @var string
     */
    private $url = 'https://www.dotboutfitters.com/api/v1';

    /**
     * @var string
     */
    private $validationFrequency = '+ 1 week';

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @param string $validationFrequency
     */
    public function setValidationFrequency($validationFrequency)
    {
        $this->validationFrequency = $validationFrequency;
    }

    /**
     * @param string $name
     * @param array  $data
     * @return array
     * @throws \DotbApiExceptionRequestMethodFailure
     */
    public function validate($name, array $data)
    {
        $result = $this->getLastValidation($name, $data);

        // force revalidation if the cached result is invalid
        if (null !== $result && !$this->isValid($data, $result)) {
            $result = null;
        }

        if (null === $result) {
            $result = $this->doValidate($name, $data);
        }

        if (!$this->isValid($data, $result)) {
            throw new InvalidLicenseException('ERROR_INVALID_LICENSE_KEY');
        }

        return $result;
    }

    /**
     * @param string $name
     * @return array|null
     */
    private function getLastValidation($name, array $data)
    {
        // retrieve checked check
        $administration = $this->getAdmin();

        $key = 'DotbOutfitters_' . $name;
        if (!isset($administration->settings[$key])) {
            return null;
        }

        $lastValidation = $administration->settings[$key];

        if (!is_array($lastValidation)) {
            $lastValidation = json_decode($lastValidation, true);
        }

        $params = $lastValidation[self::CACHE_PARAMS];

        // have the license key changed?
        if ($params[self::PARAM_LICENSE_KEY] !== $data[self::PARAM_LICENSE_KEY]) {
            return null;
        }

        // have the public key changed?
        if ($params[self::PARAM_PUBLIC_KEY] !== $data[self::PARAM_PUBLIC_KEY]) {
            return null;
        }

//        if (isset($data[self::PARAM_USER_COUNT]) && $params[self::PARAM_USER_COUNT] != $data[self::PARAM_USER_COUNT]) {
//            return null;
//        }

        $lastRun = new \DateTime($lastValidation[self::CACHE_LAST_RAN]);
        $lastRun->modify($this->validationFrequency);

        $now = new \DateTime();

        // is the cached check still valid?
        if ($lastRun < $now) {
            return null;
        }

        return $lastValidation[self::CACHE_LAST_RESULT];
    }

    /**
     * @param string $name
     * @param array  $data
     * @return array
     * @throws \InvalidLicenseException
     */
    private function doValidate($name, array $data)
    {
        // build request
        $url = "{$this->url}/key/validate?" . http_build_query($data);

        if (class_exists('Dotbcrm\Dotbcrm\Security\HttpClient\ExternalResourceClient')) {
            $client = new ExternalResourceClient(10, 10);
            $response = $client->get($url);
            $result = !empty($response) ? json_decode($response->getBody()->getContents(), true) : null;
            $httpCode = $response->getStatusCode();
        } else {
            return array(
                'validated' => true,
                'success' => true,
                'result' => ''
            );
        }

        // check for errors
        if ($httpCode === 0) {
            $GLOBALS['log']->fatal(__METHOD__ . ': Unable to validate license.');
            throw new InvalidLicenseException('ERROR_INVALID_LICENSE_KEY');
        } else if ($httpCode !== 200) {
            $GLOBALS['log']->fatal(__METHOD__ . ': HTTP Request failed.' . print_r($httpCode, true));
            throw new InvalidLicenseException('ERROR_INVALID_LICENSE_KEY');
        }

        if (JSON_ERROR_NONE !== json_last_error()) {
            $GLOBALS['log']->fatal(__METHOD__.': HTTP Request failed. json error: '.json_last_error_msg());
            throw new InvalidLicenseException('ERROR_INVALID_LICENSE_KEY');
        }

        // only save valid checks
        if ($this->isValid($data, $result)) {
            $now = new \DateTime();
            $store = array(
                self::CACHE_LAST_RAN => $now->format(\DateTime::ISO8601),
                self::CACHE_LAST_RESULT => $result,
                self::CACHE_PARAMS => $data,
            );

            $serialized = json_encode($store);
            $administration = $this->getAdmin();
            $administration->saveSetting('DotbOutfitters', $name, $serialized);
        }

        return $result;
    }

    /**
     * @return \Administration
     */
    private function getAdmin()
    {
        $administration = new \Administration();
        $administration->retrieveSettings();
        return $administration;
    }

    /**
     * @param array $data
     * @param $result
     * @return array
     */
    private function isValid(array $data, $result)
    {
        if (!$result['validated']) {
            return false;
        }

//        if (isset($data[self::PARAM_USER_COUNT]) && !$result['validated_users']) {
//            return false;
//        }

        return true;
    }
}
