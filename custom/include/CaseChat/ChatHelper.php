<?php
class ChatHelper {

    private function getFileExtension($filename) {
        // Check if the filename includes 'zip' directly
        if (strpos($filename, 'zip') !== false) {
            return 'zip';
        }
        // Use pathinfo to get extension for other cases
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        return $extension;
    }
    private function getFileIcon($fileName, $fileType) {
        $iconList = [
            'docx' => 'fa-file-word-o',
            'txt' => 'fa-file-text',
            'excel' => 'fa-file-excel-o',
            'video' => 'fa-file-video-o',
            'pptx' => 'fa-file-powerpoint-o',
            'audio' => 'fa-file-audio-o',
            'pdf' => 'fa-file-pdf-o',
            'zip' => 'fa-file-archive-o',
            'others' => 'fa-file'
        ];

        $fileIcon = 'others';
        if (strpos($fileType, "video") === 0) {
            $fileIcon = 'video';
        }

        $extension = $this->getFileExtension($fileName); // Assuming getFileExtension is defined elsewhere

        switch ($extension) {
            case 'csv':
            case 'xls':
            case 'xlsx':
                $fileIcon = 'excel';
                break;
            case 'doc':
            case 'docx':
                $fileIcon = 'docx';
                break;
            case 'txt':
                $fileIcon = 'txt';
                break;
            case 'pptx':
                $fileIcon = 'pptx';
                break;
            case 'pdf':
                $fileIcon = 'pdf';
                break;
            case 'zip':
            case 'rar':
                $fileIcon = 'zip';
                break;
        }
        return $iconList[$fileIcon];
    }

    public function getStudentInfo($caseId){
        $sqlContacts = "SELECT IFNULL(contacts.id, '') id, IFNULL(full_student_name,'') full_student_name, IFNULL(picture, '') picture 
                                    FROM contacts 
                                    INNER JOIN contacts_cases_1_c ON contacts.id = contacts_cases_1_c.contacts_cases_1contacts_ida
                                    WHERE contacts_cases_1_c.contacts_cases_1cases_idb = '{$caseId}' AND contacts.deleted = 0";
        $result = $GLOBALS['db']->fetchOne($sqlContacts);
        $personName = $result['full_student_name'];
        $avt_url = "rest/v11_3/Contacts/{$result['id']}/file/picture?format=dotb-html-json&platform=base&_hash=/resize/{$result['picture']}";
        return [
            'personName' => $personName,
            'avt_url' => $avt_url,
        ];
    }
    public function getNote($parent_id){
        //Get the attachment of each comment
        $sqlNote = "SELECT IFNULL(l1.id, '') pic_id,
                        IFNULL(l1.name, '') name,
                        IFNULL(l1.upload_id, '') upload_id,
                        IFNULL(l1.filename, '') filename,
                        IFNULL(l1.file_size, '') file_size,
                        IFNULL(l1.file_source, '') file_source,
                        IFNULL(l1.file_mime_type, '') file_mime_type,
                        IFNULL(l1.file_ext, '') file_ext
                        FROM notes l1 
                        WHERE l1.parent_id = '$parent_id' AND l1.parent_type = 'C_Comments' AND l1.deleted = 0";
        $attachment_list = $GLOBALS['db']->fetchArray($sqlNote);
        $formatted_attachment_list = [];
        //Format img list
        foreach($attachment_list as $attachment){
            $srcImg = $attachment['file_source'] == 'S3'
                ? $GLOBALS['dotb_config']['storage_service']['cloudfront_url'] . $attachment['upload_id']
                : $GLOBALS['dotb_config']['site_url'].'/upload/s3_storage/'.$attachment['name'];
            if($_SESSION['platform'] == 'portal'){
                $srcImg = '../'.$srcImg;
            }
            $fileType = explode('/', $attachment['file_mime_type'])[0]; //image or video
            $icon = $this->getFileIcon($attachment['filename'], $fileType);
            if (strpos($fileType, "image") === 0) {
                $fileType = 'image';
                $preview = '<img class="img-preview show-image" src="' . htmlspecialchars($srcImg) . '" alt="' . htmlspecialchars($attachment['filename']) . '" height="30">';
            } else {
                $preview = '<i class="fa ' . htmlspecialchars($icon) . '"></i>';
            }
            $formatted_attachment_list[] = array(
                'attachment_id' => $attachment['pic_id'],
                'fileSource' => empty($attachment['file_source']) ? 'local' : $attachment['file_source'],
                'fileType' => $fileType,
                'isImage' => strpos($fileType, "image") === 0,
                'fileSize' => round(intVal($attachment['file_size']) / 1000, 2),
                'fileName' => $attachment['filename'],
                'fileExt' => $attachment['file_ext'],
                'fileUrl' => $srcImg,
                'preview' => $preview,
                's3Uploaded' => $attachment['file_source'] == 'S3',
            );
        }
        return $formatted_attachment_list;
    }
    public function fetchComment($page, $caseId)
    {
        $limit = (!empty($page)) ? 20 * $page : 20;
        $limit_bot = $limit - 20;
        $limit_top = $limit + 1;

        $messages = array();
        $sqlComment = "SELECT DISTINCT
                IFNULL(comment.id, '') primaryid,
                IFNULL(comment.description, '') description,
                IFNULL(comment.direction, '') direction,
                IFNULL(comment.created_by, '') created_by,
                comment.date_entered date_entered,
                IFNULL(comment.is_read_inems, 0) is_read_inems,
                IFNULL(comment.is_read_inapp, 0) is_read_inapp,
                IFNULL(comment.is_updated, 0) is_updated
                FROM c_comments comment
                WHERE comment.parent_id = '{$caseId}' AND comment.parent_type = 'Cases'
                AND comment.is_unsent = 0 AND comment.deleted = 0
                ORDER BY comment.date_entered DESC
                LIMIT $limit_bot, $limit_top";
        $result = $GLOBALS['db']->query($sqlComment);
        $countComment = 0;
        $can_load = false;
        $student = $this->getStudentInfo($caseId);
        $previous_user = '';
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $countComment++;
            if($countComment == 21){
                $can_load = true;
                break;
            }
            $avt_url = '';
            if($row['direction'] == 'outbound'){
                if($previous_user != $row['created_by']){
                    $sqlUserName = "SELECT IFNULL(full_user_name,'') full_user_name FROM users WHERE id = '{$row['created_by']}'";
                    $personName = $GLOBALS['db']->getOne($sqlUserName);
                    $previous_user = $row['created_by'];
                    $previous_user_name = $personName;
                }
                $personName = $previous_user_name;
                $direction = 'to';
            } else {
                $personName = $student['personName'];
                $avt_url = $student['avt_url'];
                $direction = 'from';
            }

            if($_SESSION['platform'] == 'portal'){
                if($direction == 'to'){
                    $direction = 'from';
                } else{
                    $direction = 'to';
                }
            }
            $dateEntered = TimeDate::getInstance()->fromDb($row['date_entered']);

            $message = array(
                'id' => $row['primaryid'],
                'message' => nl2br($row['description']),
                'direction' => $direction,
                'person_name' => $personName,
                'avt' => $avt_url,
                'date_entered' => TimeDate::getInstance()->asUser($dateEntered),
                'attachments' => $this->getNote($row['primaryid']),
            );
            array_unshift($messages, $message);
        }
        return [
            'messageList' => $messages,
            'canLoad' => $can_load,
            'currentPage' => $page ?? 1,
        ];
    }

}