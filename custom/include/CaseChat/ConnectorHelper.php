<?php

namespace CaseChat;

use CaseChat\DotbOutfittersClient;

/**
 * @author Exsitec AB
 */
class ConnectorHelper
{
    /**
     * @var Config
     */
    private $config;

    /**
     * ConnectorHelper constructor.
     */
    public function __construct()
    {
        $this->config = new Config();
    }

    /**
     * Fetches the config instace
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Fetches the license type
     * @return string
     */
    public function getLicenseType()
    {
        return $this->config->getLicenseType();
    }

    /**
     * Fetches the CC license key
     * @return string
     */
    public function getLicenceKey()
    {
        return $this->config->getLicenseKey();
    }

    /**
     * Fetches the CC validation key
     * @return string
     */
    public function getValidationKey()
    {
        return $this->config->getValidationKey();
    }

    /**
     * Validates license (and validation if Exsitec license)
     *
     */
    public function checkLicense()
    {
        return; // bypass license check
        // skip this check in the Studio modules as it breaks these parts of the application
        if (isset($_REQUEST['module']) && $_REQUEST['module'] === 'ModuleBuilder') {
            return;
        }

        $validator = new LicenseValidator();


        if (self::getLicenseType() === Config::LICENCE_TYPE_OUTFITTERS) {
            $data   = array(
                DotbOutfittersClient::PARAM_LICENSE_KEY => self::getLicenceKey(),
                DotbOutfittersClient::PARAM_PUBLIC_KEY => 'e4f629f965e83684390d1caf950e4785,bb3332b1126a7befca13d66f2c8dd420',
            );
            $client = new DotbOutfittersClient();
            $client->validate('case_chat', $data);
        } else {
            $validator->validateKey(
                'case_chat', self::getLicenceKey(), self::getValidationKey()
            );
        }
    }
}