<?php
// Run:$ composer require aws/aws-sdk-php
//require_once dirname(__FILE__) . '/vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class AWSHelper
{
    // AWS Info
    private $bucketName;
    private $IAM_KEY;
    private $IAM_SECRET;
    private $region;
    private $pathInS3;
    private $s3;

    function __construct()
    {
        $this->bucketName = $GLOBALS['dotb_config']['storage_service']['bucket_name'];
        $this->IAM_KEY = $GLOBALS['dotb_config']['storage_service']['api_key'];
        $this->IAM_SECRET = $GLOBALS['dotb_config']['storage_service']['api_secret'];
        $this->region = $GLOBALS['dotb_config']['storage_service']['region'];
        $this->pathInS3 = 'https://s3.' . $this->region . '.amazonaws.com/' . $this->bucketName;
        // Connect to AWS
        $this->s3 = $this->connectAWS();
    }

    function getS3(){
        return $this->s3;
    }

    function connectAWS()
    {
        // Connect to AWS
        try {
            // You may need to change the region. It will say in the URL when the bucket is open
            // and on creation.
            $s3 = S3Client::factory(
                array(
                    'credentials' => array(
                        'key' => $this->IAM_KEY,
                        'secret' => $this->IAM_SECRET
                    ),
                    'version' => 'latest',
                    'region' => $this->region
                )
            );
            return $s3;
        } catch (Exception $e) {
            // We use a die, so if this fails. It stops here. Typically this is a REST call so this would
            // return a json object.
//            die("Error: " . $e->getMessage());
            return false;
        }
    }

    function uploadAWS($path, $fileName, $sourceFile)
    {
        // For this, I would generate a unqiue random string for the key name. But you can do whatever.
        $keyName = $path . '/' . $fileName;

        $checkIssetPath = $this->s3->doesObjectExist($this->bucketName, $keyName);

        if ($checkIssetPath) {
            return array(
                'success' => false,
                'message' => 'Isset path.'
            );
        }
        // Add it to S3
        try {
            $result = $this->s3->putObject(
                array(
                    'Bucket'        => $this->bucketName,
                    'Key'           => $keyName,
                    'SourceFile'    => $sourceFile,
                    'StorageClass'  => 'REDUCED_REDUNDANCY',
                    'ACL'           => 'public-read'
                )
            );

        } catch (S3Exception $e) {
            return array(
                'success' => false,
                'message' => $e->getMessage()
            );
        } catch (Exception $e) {
            return array(
                'success' => false,
                'message' => $e->getMessage()
            );
        }

        return array(
            'success' => true,
            'key_name' => $keyName,
            'url' => $result->get('ObjectURL')
        );
    }

    function downloadAWS($keyPath)
    {
        // Get file
        try {
            $result = $this->s3->getObject(array(
                'Bucket' => $this->bucketName,
                'Key' => $keyPath
            ));

            // Display it in the browser
            header("Content-Type: {$result['ContentType']}");
            header('Content-Disposition: filename="' . basename($keyPath) . '"');
            echo $result['Body'];

        } catch (Exception $e) {
            die("Error: " . $e->getMessage());
        }
    }

    function getKey($path = '')
    {
        // Use the high-level iterators (returns ALL of your objects).
        try {
            $results = $this->s3->getPaginator('ListObjects', [
                'Bucket' => $this->bucketName,
                //Limits the response to keys that begin with the specified prefix.
//                'Prefix' => 'test_example'
            ]);

            $listKey = array();
            foreach ($results as $result) {
                //key filter: JMESPath Expressions in the SDK
                $filtered = $result->search("Contents[?starts_with(Key, '" . $path . "') == `true`]");
//                foreach ($result['Contents'] as $object) {
//                    $listKey[] = $object['Key'];
//                }
                foreach ($filtered as $object) {
                    $key['id'] = $object['Key'];
                    $key['name'] = basename($object['Key']);
                    array_push($listKey, $key);
                }
            }
        } catch (S3Exception $e) {
            return array(
                'success' => false,
                'message' => $e->getMessage()
            );
        }

        if(empty($path)){
            array_shift($listKey);
        }
        return array(
            'success' => true,
            'list_key' => $listKey
        );
    }

    function delete($listObjectsKey)
    {
        foreach ($listObjectsKey as $key) {
            $objects[] = array('Key' => $key);
        }
        try {
            $result = $this->s3->deleteObjects([
                'Bucket' => $this->bucketName,
                'Delete' => [
                    'Objects' => $objects
                ]
            ]);
            $listRemove = array_column($result['Deleted'], 'Key');
        } catch (Exception $e) {
            return array(
                'success' => false,
                'message' => $e->getMessage()
            );
        }

        return array(
            'success' => true,
            'list_remove' => $listRemove
        );
    }
}









