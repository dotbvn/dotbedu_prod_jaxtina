<?php
    //Create Delete all session button
    if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

    class DotbWidgetSubPabelSyllabus extends DotbWidgetSubPanelTopSelectButton
    {
        //This default function is used to create the HTML for a simple button
        function display(&$widget_data){
            $button2 = '<style>a:hover{text-decoration: underline;}</style>';
            $button2 .= '<div style="display: flex;">';
            $button2 .= '<input type="button" class="button" style="font-size:12px;max-height:30px;margin-top:6px;" id="btn_change_lp"value="'.translate('LBL_CHANGE_LP').'"/>';
            $button2 .= '<div style="padding: 2px 0 2px 10px;">';
            $button2 .= '<span style="font-size: 11px;">'. translate('LBL_CURRENT_LESSONPLAN') . ': </span><br>';
            $button2 .= '<a href="index.php?module=J_LessonPlan&action=DetailView&record='.$this->parent_bean->lessonplan_id.'" style="color:#0679c8; font-size:12px;">'.$this->parent_bean->lessonplan_name.'</a>';
            $button2 .= '</div></div>';

            return $button2;
        }
    }
