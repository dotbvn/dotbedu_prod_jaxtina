<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class DotbWidgetSubPanelGradebook extends DotbWidgetSubPanelTopSelectButton
{
    //This default function is used to create the HTML for a simple button
    function display(&$widget_data){

        $html = '';
        if (ACLController::checkAccess('J_Gradebook', 'import', true)){
            $html .= '<input style="margin-left:10px;cursor:context-menu;" disabled type="button" name="reloadconfig_gradebook_btn" id="reloadconfig_gradebook_btn" class="button btn-secondary" title="'.translate('LBL_LOADNEWCONFIG','J_Gradebook').'" value="'.translate('LBL_LOADNEWCONFIG','J_Gradebook').'">';
        }
        return $html;
    }
}
