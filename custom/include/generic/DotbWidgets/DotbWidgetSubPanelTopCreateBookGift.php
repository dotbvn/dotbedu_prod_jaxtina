<?php
    //Create Delete all session button in Sessions subpanel by Lap Nguyen
    if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

    class DotbWidgetSubPanelTopCreateBookGift extends DotbWidgetSubPanelTopSelectButton
    {
        //This default function is used to create the HTML for a simple button
        function display(&$widget_data){

//            $button2 = '';
//            if (ACLController::checkAccess('J_Class', 'import', true))
                $button2 = '<input type="button" id="book_gift" value="'.translate('LBL_CREATE_BOOK_GIFT').'" onclick="created_book_gift($(this));"/>';
            return $button2;
        }
    }
