<?php
    //Create Delete all session button
    if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
    require_once('include/generic/DotbWidgets/DotbWidgetSubPanelTopButton.php');

    class DotbWidgetSubPabelCustomCreateButtonPaymentVoucher extends DotbWidgetSubPanelTopButton
    {    
        //This default function is used to create the HTML for a simple button
    public  function display($defines, $additionalFormFields = null, $nonbutton = null){

            global $app_strings;
            $this->title = $app_strings['LBL_BUTTON_CREATE'];
            $this->form_value = $app_strings['LBL_BUTTON_CREATE'];
            $this->module = $defines['module'];
            //$this->module_name = 'Hợp Đồng TVDH';
            $additionalFormFields['contacts_id'] = $defines['focus']->contacts_c_contract_1contacts_ida;
            $additionalFormFields['contacts_name'] = $defines['focus']->contacts_c_contract_1_name;
            return parent::display($defines, $additionalFormFields, $nonbutton);
        }
    }
