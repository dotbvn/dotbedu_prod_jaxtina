<?php
//Create Delete all session button in Sessions subpanel by Lap Nguyen
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class DotbWidgetSubPanelClass extends DotbWidgetSubPanelTopSelectButton
{
    //This default function is used to create the HTML for a simple button
    function display(&$widget_data){

        $button2 = '';
        if (ACLController::checkAccess('J_Class', 'import', true))
            $button2 .= '<input type="button" id="schedule_teacher" class="button primary" value="'.translate('LBL_SCHEDULE_TEACHER').'" onclick="schedule_teacher($(this));"/>';

        //$button2 .= '<input style="margin-left: 10px;" type="button" id="btn_send_message" class="button" value="'.translate('LBL_SEND_APP_MESSAGE').'" onclick="generateAppMess(\''.$this->parent_bean->module_name.'\',\''.$this->parent_bean->id.'\');"/>';

        $button2 .= '<input style="margin-left: 10px;" type="button" id="btn_view_on_calendar" class="button" value="'.translate('LNK_VIEW_CALENDAR','J_Class').'" onclick="window.top.DOTB.App.router.redirect(\'#bwc/index.php?module=Calendar&action=index&view=week&class_id='.$this->parent_bean->id.'\');"/>';
        return $button2;
    }
}
