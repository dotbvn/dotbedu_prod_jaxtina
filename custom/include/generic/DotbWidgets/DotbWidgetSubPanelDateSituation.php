<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
//
class DotbWidgetSubPanelDateSituation extends DotbWidgetSubPanelTopSelectButton
{
    //This default function is used to create the HTML for a simple button
    function display($layout_def)
    {
        global $timedate, $current_user;
        $html = '';
        $html1 = '';
        $class_id = $layout_def['focus']->id;
        if(ACLController::checkAccess('J_StudentSituations', 'edit', true) || ($current_user->isAdmin())){
            $html = '<table style="float: left;"><tbody><tr>';
            $html .='<td><select style="margin-left: 5px;" id="parent_demo">'.get_select_options($GLOBALS['app_list_strings']['student_type_list'],'').'</td>
            <td><input style="margin-left: 5px;" type="button" id="btn_open_popup_demo" value="'.translate('LBL_CLASS_DEMO').'" onclick="open_popup_demo($(this));"/></td>';
            $html .= '<td><input type="button" name="add_student_btn" id="add_student_btn" class="button primary" onclick="open_popup_add_students();" title="'.translate('LBL_STUDENT_FROM_PREVIOUS').'" value="'.translate('LBL_ENROLL_STUDENTS').'"></td>';

            $onClick = "window.top.DOTB.App.router.redirect('#bwc/index.php?module=J_Payment&action=EditView&payment_type=Cashholder&class_id=$class_id')";
            $html .= "<td><input type='button' name='btn_create_payment' id='btn_create_payment' class='button btn-success' onClick=$onClick title='".translate('LBL_CREATE_NEW_PAYMENT_DES')."' value='".translate('LBL_CREATE_NEW_PAYMENT')."'></td>";
            $html .= '</tr></tbody></table>';
        }

        $q1 = "SELECT DISTINCT IFNULL(l1.id, '') class_id,
        COUNT(jsc.id) count_students,
        SUM(CASE WHEN (jsc.status='Not Started') THEN 1 ELSE 0 END) sts_notstarted,
        SUM(CASE WHEN (jsc.status='In Progress') THEN 1 ELSE 0 END) sts_inprogress,
        SUM(CASE WHEN (jsc.status='Delayed') THEN 1 ELSE 0 END) sts_delayed,
        SUM(CASE WHEN (jsc.status='Finished') THEN 1 ELSE 0 END) sts_finished,
        SUM(CASE WHEN (jsc.type='OutStanding') THEN 1 ELSE 0 END) type_ost,
        SUM(CASE WHEN (jsc.type='Enrolled') THEN 1 ELSE 0 END) type_enrolled,
        SUM(CASE WHEN (jsc.type='Demo') THEN 1 ELSE 0 END) type_demo,
        SUM(CASE WHEN (jsc.student_type='Leads') THEN 1 ELSE 0 END) count_lead,
        SUM(CASE WHEN (jsc.student_type='Contacts') THEN 1 ELSE 0 END) count_student
        FROM j_classstudents jsc INNER JOIN j_class l1 ON jsc.class_id = l1.id AND l1.deleted = 0
        WHERE (l1.id = '$class_id') AND jsc.deleted = 0
        GROUP BY l1.id";
        $rows = $GLOBALS['db']->fetchOne($q1);
        if(!empty($rows)){
            $MOD1 = $GLOBALS['app_list_strings']['situation_status_list'];
            $MOD2 = $GLOBALS['app_list_strings']['situation_type_list'];
            $MOD3 = $GLOBALS['app_list_strings']['student_type_list'];
            $html1 .= "<table style='float: left;margin-left: 10px; border-left: 2px dashed #cdd0d4;'><tbody><tr><td colspan='100%' style='font-size: 15px;font-weight:600;'>".translate('LBL_TOTAL_STUDENT','J_Class').": ".($rows['count_students']). "</td></tr>";
            $html1 .= "<tr>";
            if($rows['type_enrolled'] > 0) $html1 .= "<td style='color:#11734B;'>{$MOD2['Enrolled']}: {$rows['type_enrolled']}</td>";
            if($rows['type_ost'] > 0) $html1 .= "<td style='color:#FF8C00;'>{$MOD2['OutStanding']}: {$rows['type_ost']}</td>";
            if($rows['type_demo'] > 0) $html1 .= "<td style='color:#00a5ff;'>{$MOD2['Demo']}: {$rows['type_demo']}</td>";
            $html1 .= "</tr></tbody></table>";

            $extS = ($rows['count_student'] > 0) ? $rows['count_student'].' '.mb_strtolower($MOD3['Contacts'], 'UTF-8'): '';
            $extL = ($rows['count_lead'] > 0) ? $rows['count_lead'].' '.mb_strtolower($MOD3['Leads'], 'UTF-8'): '';
            if($GLOBALS['current_language'] == 'en_us'){
                if($rows['count_student'] > 1) $extS.='s';
                if($rows['count_lead'] > 1) $extL.='s';
            }
            $html1 .= "<table style='float: left;margin-left: 10px; border-left: 2px dashed #cdd0d4;'><tbody><tr><td colspan='100%' style='font-size: 15px;'>".(!empty($extL) ? "$extS + $extL" : $extS)."</td></tr>";
            $html1 .= "<tr>";
            if($rows['sts_notstarted'] > 0) $html1 .= "<td style='color:#11734B;'>{$MOD1['Not Started']}: {$rows['sts_notstarted']}</td>";
            if($rows['sts_inprogress'] > 0) $html1 .= "<td style='color:#0a53a8;'>{$MOD1['In Progress']}: {$rows['sts_inprogress']}</td>";
            if($rows['sts_delayed'] > 0) $html1 .= "<td style='color:#343434;'>{$MOD1['Delayed']}: {$rows['sts_delayed']}</td>";
            if($rows['sts_finished'] > 0) $html1 .= "<td style='color:#bb0e1b;'>{$MOD1['Finished']}: {$rows['sts_finished']}</td>";
            $html1 .= "</tr></tbody></table>";
        }

        return  $html.$html1;
    }
}
?>
