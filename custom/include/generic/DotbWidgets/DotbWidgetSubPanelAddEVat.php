<?php
//Create Delete all session button in Sessions subpanel by Lap Nguyen
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class DotbWidgetSubPanelAddEVat extends DotbWidgetSubPanelTopSelectButton{
    //This default function is used to create the HTML for a simple button
    function display(&$widget_data){
        $paymentId = $_REQUEST['record'];
        $payment = BeanFactory::getBean('J_Payment',$paymentId);
        $checkConfigEvat = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM j_configinvoiceno WHERE active = 1 AND team_id = '{$payment->team_id}' AND deleted = 0");
        if(empty($checkConfigEvat)) return false;
        //if(!in_array($payment->payment_type,['Cashholder','Book/Gift'])) return false;

        $admin = new Administration();
        $admin->retrieveSettings();
        if (!empty($admin->settings['einvoice_setting_export_einvoice_type']))
            $exportIvoiceType = $admin->settings['einvoice_setting_export_einvoice_type'];
        else $exportIvoiceType = "Payment";
        if($exportIvoiceType != "Payment") return false; //Xuất Hóa đơn Receipt ko xuất ở đây

        $button2 = '';
        //Kiểm tra có hóa đơn nào đã xuất chưa
        $q2 = "SELECT DISTINCT
        IFNULL(j_payment.id, '') primaryid,
        IFNULL(l1.id, '') invoice_id,
        IFNULL(l1.name, '') invoice_number,
        l1.invoice_date invoice_date
        FROM j_payment INNER JOIN j_invoice_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_id AND l1_1.deleted = 0
        INNER JOIN j_invoice l1 ON l1.id = l1_1.invoice_id AND l1.deleted = 0
        WHERE (IFNULL(l1.status, '') = 'Paid') AND j_payment.deleted = 0 AND j_payment.id = '$paymentId'
        LIMIT 1";
        $rowInv= $GLOBALS['db']->fetchOne($q2);
        if(!empty($rowInv['invoice_number'])){
            $button2 .= '<input class="button primary" type="button" payment_id="'.$payment->id.'" invoice_id="'.$rowInv['invoice_id'].'" onclick="ex_invoice_pdf(this);" value="'.translate('LBL_EXPORT_INVOICE','J_Payment').'"/>';
            if(checkDataLockDate($rowInv['invoice_date']) && ACLController::checkAccess('J_Invoice', 'delete', true))
                $button2 .= '<input class="button cancel_invoice" type="button" payment_id="'.$payment->id.'" onclick = \'cancel_invoice( "'.$rowInv['invoice_id'].'" ,"'.$payment->id.'")\' value="'.translate('LBL_VOID_INVOICE','J_Payment').'"/>';
        }elseif(ACLController::checkAccess('J_Invoice', 'view', true)){
            //Kiểm tra tổng tiền - Full Payment thì mới xuất hóa đơn
            $q1 = "(SELECT DISTINCT
            IFNULL(l1.id, '') rel_payment_id,
            (l1.discount_amount + l1.final_sponsor + l1.loyalty_amount) discount_amount,
            GROUP_CONCAT(l2.id) rel_pmd_ids,
            SUM(l2.payment_amount) total_amount
            FROM j_payment INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
            INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0
            INNER JOIN j_paymentdetail l2 ON l2.payment_id = l1.id AND l2.deleted = 0 AND l2.status = 'Paid' AND (l2.invoice_id IS NULL OR l2.invoice_id = '')
            WHERE (j_payment.id = '{$payment->id}') AND j_payment.deleted = 0
            GROUP BY IFNULL(l1.id, ''))
            UNION (SELECT DISTINCT
            IFNULL(l1.id, '') rel_payment_id,
            (l1.discount_amount + l1.final_sponsor + l1.loyalty_amount) discount_amount,
            GROUP_CONCAT(l2.id) rel_pmd_ids,
            SUM(l2.payment_amount) total_amount
            FROM j_payment l1
            INNER JOIN j_paymentdetail l2 ON l2.payment_id = l1.id AND l2.deleted = 0 AND l2.status = 'Paid'
            WHERE (l1.id = '{$payment->id}') AND l1.deleted = 0 AND (l2.invoice_id IS NULL OR l2.invoice_id = '')
            GROUP BY IFNULL(l1.id, ''))";
            $rs1 = $GLOBALS['db']->query($q1);
            $totalAmount = 0;
            while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) $totalAmount += $row['total_amount'];

            //Kiểm tra tổng tiền - Full Payment thì mới xuất hóa đơn
            $q1 = "SELECT IFNULL(l1.id,'') payment_id,
            SUM(pmd.payment_amount) sum_pmd_amount
            FROM j_paymentdetail pmd
            INNER JOIN j_payment l1 ON pmd.payment_id = l1.id AND l1.deleted = 0
            WHERE l1.id = '{$payment->id}'  AND pmd.deleted = 0 AND pmd.status = 'Paid'
            GROUP BY l1.id";
            $rowPay= $GLOBALS['db']->fetchOne($q1);

            //Kiểm tra sản phẩm thuộc Product type -No invoice- ==> Không xuất hóa đơn
            $res = noInvoiceCheck($paymentId);
            if(!$res) return false;

            //THanh toán Book/Gift thì số tiền thu phải lớn hơn 0 mới xuất hóa đơn
            if(($payment->payment_type == 'Book/Gift' && $payment->payment_amount > 0 && $rowPay['sum_pmd_amount'] == $payment->payment_amount)
                || ($payment->payment_type == 'Cashholder' && $totalAmount > 0 &&  $rowPay['sum_pmd_amount'] == $payment->payment_amount)
                || (!in_array($payment->payment_type,['Book/Gift', 'Cashholder'])))
                $button2 .= '<input class="button primary" id="btn_get_evat" type="button" payment_id="'.$payment->id.'" value="'.translate('LBL_GET_INVOICE_NO', 'J_Payment').'" onclick="get_invoice_no(this);"/>';
        }

        return $button2;
    }
}
?>
