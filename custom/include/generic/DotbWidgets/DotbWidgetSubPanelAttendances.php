<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
//
class DotbWidgetSubPanelAttendances extends DotbWidgetSubPanelTopSelectButton
{
    //This default function is used to create the HTML for a simple button
    function display($layout_def)
    {
        $html1 = "<table width='100%' style='min-width:900px'><tbody><tr>";
        $html1 .= "<td width='10%'>";
        $html1 .= '<input type="button" class="button" id="export_attendance" name="export_attendance" value="'.translate('BTN_TOP_EXPORT_ATTENDANCE','J_Class').'" onclick="showDialogExportAttendance();"/>';
        $html1 .= "</td>";
        $class_id = $layout_def['focus']->id;

        $q1 = "SELECT DISTINCT IFNULL(l1.id, '') class_id,
        AVG(jst.avg_attendance) avg_attendance,
        SUM(CASE WHEN (jst.avg_attendance >= 100) THEN 1 ELSE 0 END) count_perfect,
        COUNT(DISTINCT jst.id) count_std,
        SUM(CASE WHEN (jst.avg_attendance >= 50) THEN 1 ELSE 0 END) count_average,
        SUM(CASE WHEN (jst.avg_attendance < 50) THEN 1 ELSE 0 END) count_below
        FROM j_classstudents jst INNER JOIN j_class l1 ON jst.class_id = l1.id AND l1.deleted = 0
        WHERE (l1.id = '$class_id') AND jst.deleted = 0
        GROUP BY l1.id";
        $row = $GLOBALS['db']->fetchOne($q1);
        if(ACLController::checkAccess('C_Attendance', 'view', true)){
            $html1 .= "<td width='15%'>";
            $html1 .= "<table><tbody><tr>";
            $html1 .= "<td width='30%'>".labelAttOverall($row['avg_attendance'])."</td>";
            $html1 .= "<td width='70%' style='font-weight: bold;'>".translate('LBL_AVG_ATTENDANCE','J_Class')."</td>";
            $html1 .= "</tr></tbody></table>";
            $html1 .= "</td>";
            $html1 .= "<td width='75%'>";
            $html1 .= "<table class='att-sumary'><tbody><tr>";
            foreach(['perfect', 'average', 'below'] as $val){
                $row['count_'.$val] = empty($row['count_'.$val]) ?  0 : $row['count_'.$val];
                $row['count_std']   = empty($row['count_std']) ?  0 : $row['count_std'];
                $num_  = (!empty($row['count_'.$val]) && !empty($row['count_std'])) ?  format_number(($row['count_'.$val]/$row['count_std'])*100) : 0;
                $text_ = str_replace('{percent}',$num_,translate('LBL_AVG_ATTENDANCE_'.strtoupper($val),'J_Class'));
                $html1.= "<td width='5%'>{$row['count_'.$val]}</td><td width='28%'>$text_</td>";
            }
            $html1 .= "</tr></tbody></table>";
            $html1 .= "</td>";
            $html1 .= "</tr></tbody></table>";
        }


        return $html1;
    }
}
?>
