<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');


class DotbWidgetSubPanelCheckAllTeacher extends DotbWidgetField{
	function displayHeaderCell(&$layout_def){
        return '<input type="checkbox" id="checkall_teacher" onClick="toggleCheckAllTeacher(this)" />';
    }
}
?>
