<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');


class DotbWidgetSubPanelCheckAll extends DotbWidgetField
{
	function displayHeaderCell(&$layout_def)
    {
        return '<input type="checkbox" id="checkall" onClick="toggleCheckAll(this)" />';
    }
}
?>
