<?php
    //Create Delete all session button in Sessions subpanel  by Lap Nguyen
    if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
          
    class DotbWidgetSubPanelTeacherAllButton extends DotbWidgetSubPanelTopSelectButton
    {    
        //This default function is used to create the HTML for a simple button
        function display(&$widget_data)
        {   
            $bt_t = '
            <input type="submit" name="send_sms_teacher" id="send_sms_teacher" class="button" 
            onclick= ajaxSendSMSTeacher()            
            title="'.$GLOBALS['mod_strings']['LBL_SEND_SMS_FREETEXT'].'" value="'.$GLOBALS['mod_strings']['LBL_SEND_SMS_FREETEXT'].'">';
            return $bt_t;
        }
    }
?>
