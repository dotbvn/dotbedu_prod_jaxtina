<?php
use Uncgits\CanvasApi\CanvasApiConfig;

class CanvasEnvironment extends CanvasApiConfig
{
    public function __construct()
    {
        //Set config Canvas LMS - By Lap Nguyen
        $q1     = "SELECT * FROM config WHERE category = 'lms_canvas'";
        $rowC   = $GLOBALS['db']->fetchArray($q1);
        $config = array();
        foreach($rowC as $key => $row) $config[$row['name']] = $row['value'];

        $this->setEnable($config['enable']);
        if($this->enable){
            $this->setApiHost($config['domain']);
            $this->setToken($config['token']);
            $this->setAccount($config['account_id']);
            $this->setSSO($config['sso_enable'], $config['auth_provider_id']);
        }
    }
}
?>
