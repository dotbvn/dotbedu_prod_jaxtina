<?php

require_once('include/externalAPI/Google/ExtAPIGoogle.php');

function createGM(&$meeting){
    global $timedate;
    if(!empty($meeting->id)){
        $listAttendees = getAttendees($meeting->id);
        if(count($listAttendees['teachers']) == 0){
            $meeting->join_url      = '';
            $meeting->external_id   = '';
            $meeting->creator       = "LBL_API_ERR_CREATE1";
            $res['success'] = 0;
        }else{
            $param['attendees']     = array_merge($listAttendees['teachers'],$listAttendees['assistants'],$listAttendees['students']);
            $param['host_user_id']  = $listAttendees['teachers'][0]['host_user_id']; //Set Host
            $ExtAPIGoogle = new ExtAPIGoogle();
            $res = $ExtAPIGoogle->createEvent($meeting, $param);
            if($res['success']){
                //Assign param
                $meeting->join_url    = $res['hangoutLink'];
                $meeting->external_id = $res['external_id'];
                $meeting->creator    = '';
            }else{
                //Assign param
                $meeting->join_url      = '';
                $meeting->external_id   = '';
                $meeting->creator       = $res['errorCode'];
            }
        }

        //Update bằng query để hạn chế rủi ro
        $GLOBALS['db']->query("UPDATE meetings SET
            type='{$meeting->type}',
            external_id='{$meeting->external_id}',
            join_url='{$meeting->join_url}',
            creator='{$meeting->creator}',
            date_modified='{$timedate->nowDb()}' WHERE id = '{$meeting->id}'");
        return $res;
    }
}

function updateGM(&$meeting){
    global $timedate;
    if(!empty($meeting->id)){
        $listAttendees = getAttendees($meeting->id);
        $param['attendees'] = array_merge($listAttendees['teachers'],$listAttendees['assistants'],$listAttendees['students']);
        $param['host_user_id']  = $listAttendees['teachers'][0]['host_user_id']; //Set Host
        $ExtAPIGoogle = new ExtAPIGoogle();
        $res = $ExtAPIGoogle->updateEvent($meeting,$param);
        if($res['success']){
            //Assign param
            $meeting->join_url    = $res['hangoutLink'];
            $meeting->external_id = $res['external_id'];
            $meeting->creator     = '';
        }else{
            //Assign param
            $meeting->join_url      = '';
            $meeting->external_id   = '';
            $meeting->creator       = $res['errorCode'];
        }

        //Update bằng query để hạn chế rủi ro
        $GLOBALS['db']->query("UPDATE meetings SET
        type='{$meeting->type}',
        external_id='{$meeting->external_id}',
        join_url='{$meeting->join_url}',
        creator='{$meeting->creator}',
        date_modified='{$timedate->nowDb()}' WHERE id = '{$meeting->id}'");
        return $res;
    }
}


function deleteGM(&$meeting){
    global $timedate;
    $ExtAPIGoogle = new ExtAPIGoogle();
    $res = $ExtAPIGoogle->deleteEvent($meeting);
    if($res['success']){
        $meeting->external_id = '';
        $meeting->join_url    = '';
        $meeting->creator     = $res['errorCode'];
    }else{
        //Assign param - Xóa ko thành công thì giữ lại ID để lần sau xóa tiếp
        $meeting->creator     = $res['errorCode'];
    }

    //Update bằng query để hạn chế rủi ro
    $GLOBALS['db']->query("UPDATE meetings SET
        type='{$meeting->type}',
        external_id='{$meeting->external_id}',
        join_url='{$meeting->join_url}',
        creator='{$meeting->creator}',
        date_modified='{$timedate->nowDb()}' WHERE id = '{$meeting->id}'");
    return $res;
}














//===++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//get list student, teacher, ta class
function getAttendees($meeting_id){
    $q1 = "SELECT DISTINCT IFNULL(l2.id, '') student_id,
    IFNULL(l2.full_student_name, '') student_name,
    IFNULL(l3.email_address, '') student_email,
    IFNULL(l5.id, '') teacher_id,
    IFNULL(l5.full_teacher_name, '') teacher_name,
    l6.email_address teacher_email,
    IFNULL(l11.id, '') host_user_id,
    IFNULL(l7.id, '') ta1_id,
    IFNULL(l7.full_teacher_name, '') ta1_name,
    IFNULL(l8.email_address, '') ta1_email,
    IFNULL(l9.id, '') ta2_id,
    IFNULL(l9.full_teacher_name, '') ta2_name,
    IFNULL(l10.email_address, '') ta2_email
    FROM meetings
    LEFT JOIN meetings_contacts l1_1 ON meetings.id = l1_1.meeting_id AND l1_1.deleted = 0
    LEFT JOIN j_studentsituations l1 ON l1.id = l1_1.situation_id AND l1.deleted = 0
    LEFT JOIN contacts l2 ON l1.student_id = l2.id AND l2.deleted = 0 AND l1.student_type = 'Contacts'
    LEFT JOIN email_addr_bean_rel l3_1 ON l2.id = l3_1.bean_id AND l3_1.deleted = 0 AND l3_1.bean_module = 'Contacts' AND l3_1.primary_address = 1
    LEFT JOIN email_addresses l3 ON l3.id = l3_1.email_address_id AND l3.deleted = 0
    INNER JOIN j_class l4 ON meetings.ju_class_id = l4.id AND l4.deleted = 0
    INNER JOIN c_teachers l5 ON meetings.teacher_id = l5.id AND l5.deleted = 0 AND l5.status = 'Active'
    INNER JOIN email_addr_bean_rel l6_1 ON l5.id = l6_1.bean_id AND l6_1.deleted = 0 AND l6_1.bean_module = 'C_Teachers' AND l6_1.primary_address = 1
    INNER JOIN email_addresses l6 ON l6.id = l6_1.email_address_id AND l6.deleted = 0
    INNER JOIN users l11 ON l5.user_id = l11.id AND l11.deleted = 0
    LEFT JOIN c_teachers l7 ON meetings.teacher_cover_id = l7.id AND l7.deleted = 0
    LEFT JOIN email_addr_bean_rel l8_1 ON l7.id = l8_1.bean_id AND l8_1.deleted = 0 AND l8_1.bean_module = 'C_Teachers' AND l8_1.primary_address = 1
    LEFT JOIN email_addresses l8 ON l8.id = l8_1.email_address_id AND l8.deleted = 0
    LEFT JOIN c_teachers l9 ON meetings.sub_teacher_id = l9.id AND l9.deleted = 0
    LEFT JOIN email_addr_bean_rel l10_1 ON l9.id = l10_1.bean_id AND l10_1.deleted = 0 AND l10_1.bean_module = 'C_Teachers' AND l10_1.primary_address = 1
    LEFT JOIN email_addresses l10 ON l10.id = l10_1.email_address_id AND l10.deleted = 0
    WHERE (((meetings.id = '$meeting_id') AND (meetings.session_status <> 'Cancelled'))) AND meetings.deleted = 0";
    $attendees = $GLOBALS['db']->fetchArray($q1);
    $students   = array();
    $teachers   = array();
    $assistants = array();
    //add teachers
    if(!empty($attendees[0])){
        if($attendees[0]['teacher_email'])
            $teachers[] = [
                'host_user_id' => $attendees[0]['host_user_id'],
                'id' => $attendees[0]['teacher_id'],
                'email' => $attendees[0]['teacher_email'],
                'name' => $attendees[0]['teacher_name'],
                'comment' => 'Primary Teacher',
            ];
        if($attendees[0]['ta1_email'])
            $assistants[] = [
                'id' => $attendees[0]['ta1_id'],
                'email' => $attendees[0]['ta1_email'],
                'name' => $attendees[0]['ta1_name'],
                'comment' => 'Teacher Assistant 1',
            ];
        if($attendees[0]['ta2_email'])
            $assistants[] = [
                'id' => $attendees[0]['ta2_id'],
                'email' => $attendees[0]['ta2_email'],
                'name' => $attendees[0]['ta2_name'],
                'comment' => 'Teacher Assistant 2',
            ];
    }
    //add students
    foreach($attendees as $key => $attendee){
        if($attendee['student_email'])
            $students[] = [
                'id' => $attendee['student_id'],
                'email' => $attendee['student_email'],
                'name' => $attendee['student_name'],
                'comment' => 'Student',
            ];
    }
    return array(
        'teachers'      => $teachers,
        'assistants'    => $assistants,
        'students'      => $students,
    );
}