<?php

class ViettelEInvoice
{
    private $token;
    public $evat;

    function __construct($evat)
    {
        $this->evat = $evat;
    }

    function getToken()
    {
        if (empty($this->token)) {
            $res = $this->callApi($this->evat->host . '/auth/login', array(
                'username' => $this->evat->user_name,
                'password' => $this->evat->password
            ));
            $res = json_decode($res, true);
            if (empty($res)) return null;
            $this->token = $res['access_token'];
        }
        return $this->token;
    }

    function callApi($url, $params)
    {
        $auth_request = curl_init($url);
        curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($auth_request, CURLOPT_HEADER, false);
        curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($auth_request, CURLOPT_POST, 1);
        curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
        if (!empty($this->token)) {
            curl_setopt($auth_request, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json"
            ));
        } else {
            curl_setopt($auth_request, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "Cookie: access_token=" . $this->token
            ));
        }

        $json_arguments = json_encode($params);
        curl_setopt($auth_request, CURLOPT_POSTFIELDS, $json_arguments);
        $oauth2_token_response = curl_exec($auth_request);

        return $oauth2_token_response;
    }
}
