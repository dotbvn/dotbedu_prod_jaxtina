<?php
//Capture lead
$args = $_POST;

//user create
// Thêm giá trị mặc định nếu không truyền tham số vào
if(empty($args['api_user'])) $args['api_user'] = 'apps_admin';
$args['api_user_id'] = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = '{$args['api_user']}'");
if(empty($args['api_user_id'])) $args['api_user_id'] = '1';

// Dùng BeanFactory để gọi thông tin về user admin
$GLOBALS['current_user'] = BeanFactory::getBean('Users', $args['api_user_id']);

//get Team ID
if(empty($args['team_id']) && !empty($args['center'])){
    $teamId = $GLOBALS['db']->getOne("SELECT id FROM teams WHERE description LIKE '%{$args['center']}%' AND private = 0 AND deleted = 0 LIMIT 1");
    $args['team_id'] = $teamId;
}
if(!empty($args['team_name']) && empty($args['team_id'])){
    $teamId = $GLOBALS['db']->getOne("SELECT id FROM teams WHERE name='{$args['team_name']}' AND private = 0 AND deleted = 0 LIMIT 1");
    $args['team_id'] = $teamId;
}

if (empty($args['team_id'])) $args['team_id'] = '1';

//get utm_agent
$mkt_user = $args['utm_agent'];
if (empty($mkt_user)) {
    $exploded = explode('_', $args['campaign_name']);
    $mkt_user = end($exploded);
}
if (!empty($mkt_user)) $args['utm_agent_id'] = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = '$mkt_user'");

//Check campaign_name
if (!empty($args['campaign_name'])) {
    $args['campaign_id'] = $GLOBALS['db']->getOne("SELECT DISTINCT IFNULL(campaigns.id, '') primaryid FROM campaigns WHERE (campaigns.name LIKE '%{$args['campaign_name']}%') AND campaigns.deleted = 0");
    if (empty($args['campaign_id'])) {
        $cam = new Campaign();
        $cam->name = $args['campaign_name'];
        $cam->team_id = $args['team_id'];
        $cam->team_set_id = $args['team_id'];
        $cam->save();
        $args['campaign_id'] = $cam->id;
    }
}
//Check channel
if (empty($args['channel'])) $args['channel'] = $args['utm_source'];
//get Assigned to
$args['assigned_user_id'] = $GLOBALS['db']->getOne("SELECT IFNULL(manager_user_id, '') manager_user_id FROM teams WHERE id='{$args['team_id']}'");
// End gán giá trị mặc định
// ==========START FUNCTION================
// Lấy dữ liệu được truyền vào từ API
if(empty($args['phone_mobile']))
    $args['phone_mobile'] = $args['phone'];

if(empty($args['phone_mobile']))
    $args['phone_mobile'] = $args['mobile_phone'];

// Kiểm tra nếu có truyền vào name mà ko có truyền vào first name thì dùng hàm split để lấy first name từ full name
if(!empty($args['name']) && empty($args['first_name']))
    $args['first_name']  = split_fullname($args['name'],0);
if(!empty($args['name']) && empty($args['last_name']))
    $args['last_name']  = split_fullname($args['name']);

// Lấy tên module cần đổ dữ liệu (module Lead)
if(strtolower($args['type']) == 'lead' || strtolower($args['type']) == 'leads')
    $module_name = 'Lead';
else $module_name = 'Prospect';

// tạo mới record Lead/Target (Prospect) tương ứng với dữ liệu truyền vào từ module name
$lead = new $module_name();
$lead->disable_row_level_security =true;

foreach ($lead->getFieldDefinitions() as $field => $definition){
    // Chạy vòng lặp qua từng field có dữ liệu được truyển vào từ API để tạo mới record
    if (!isset($args[$field]))
        // Nếu field không có dữ liệu thì bỏ qua
        // (VD: tổng là 10 field mà API chỉ truyền vào 7 field thì 3 field ko đc truyền dữ liệu sẽ được chạy qua)
        continue;
    $lead->$field = $args[$field];
}


//trigger fields email template & Duplicate
// Nối last name với first name để tạo ra name
$lead->name = trim($lead->last_name . ' ' . $lead->first_name);
$lead->id = '';
//Set giá trị field mặc định là ngày hiện tại nếu k có dữ liệu truyền vào
if(!empty($lead->birthdate))
    $lead->birthdate = $GLOBALS['timedate']->convertToDBDate($lead->birthdate);

if(!empty($args['parent_name1']))
    $lead->guardian_name   = $args['parent_name1'];
if(!empty($args['parent_name2']))
    $lead->guardian_name_2   = $args['parent_name2'];
$lead->email1          = $args['email'];
if(empty($lead->email1)) $lead->email1 = $args['email1'];
$lead->team_id          = $args['team_id'];
$lead->team_set_id      = $args['team_id'];

if(empty($lead->name) && empty($lead->phone_mobile) && empty($lead->email1)){
    ob_clean();
    // Trả về lỗi của hệ thống nếu xảy ra sự cố không lưu được
    die(json_encode(array(
        'success'   => 0,
        'messages'  => 'invalid_input',
    )));
}

////reference_logs
/// Chuẩn bị Data để trả về
$reference_logs = json_encode(array(
    'lead_source'        => $lead->lead_source,
    'source_description' => $lead->source_description,
    'description'        => $lead->description,
    'reference'          => $lead->reference,
    'channel'            => $lead->channel,
    'campaign_id'        => $lead->campaign_id,
    'utm_source'         => $lead->utm_source,
    'utm_medium'         => $lead->utm_medium,
    'utm_content'        => $lead->utm_content,
    'utm_term'           => $lead->utm_term,
));

//Check duplicate
$duplicates = $lead->findDuplicates('2');
if(count($duplicates['records']) > 0){
    //Update date modified by
    $dupId = $duplicates['records'][0]['id'];
    $beanName = $module_name.'s';
    $beanDup = BeanFactory::getBean($beanName, $dupId);
    if(!empty($beanDup->id)){
        if(empty($beanDup->email1)) $beanDup->email1 = $lead->email1;
        if(empty($beanDup->description)) $beanDup->description = $lead->description;
        else $beanDup->description .= "\n ".$lead->description;
        $beanDup->reference_logs = $reference_logs;
        $beanDup->save();
    }
    ob_clean();
    die(json_encode(array(
        'success'   => 0,
        'messages'  => 'duplicated_found',
        'record'    => $duplicates['records'][0]['id'],
    )));
}
$lead->save();

ob_clean();
// Trả về thông báo thành công nếu tạo mới record thành công
die(json_encode(array(
    'success'   => 1,
    'messages'  => 'created_new_'.$module_name,
    'record'    => $lead->id,
)));


