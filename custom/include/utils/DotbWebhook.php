<?php


class DotbWebhook
{
    public function buildQuyettamUrl($des){
        return $GLOBALS['dotb_config']['saas_url'] .'/rest/v11_3/'. $des;
    }
    public function callQuyettamAPI($des, $method, $post_fields = '', $header = array('Content-Type: application/json')): array
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->buildQuyettamUrl($des),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $header,
        ));
        if(!empty($post_fields) && in_array('Content-Type: multipart/form-data', $header))
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
        elseif(!empty($post_fields))
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post_fields));
        $response = curl_exec($curl);
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return array(
            'status_code' => $http_code,
            'data' => json_decode($response, 1)
        );
    }
}