<?php
function throwApiError($error_num){
    $error_msg = [
        1 => 'Normal execution',
        //User side
        100 => 'Invalid username',
        101 => 'Wrong password',
        103 => 'Missing parameters',
        104 => 'Empty arguments',
        105 => 'Invalid Access Token',
        106 => 'Invalid param',
        //Specific case
        111 => 'No info',
        112 => 'Invalid app type',
        113 => 'SMS serivec error',
        114 => 'Invalid doc url',
        115 => 'No session',
        116 => 'Invalid field_val of loyalty_point',
        117 => 'Sent failed',
        118 => 'Empty comment',
        119 => 'Reach targets limit',
        120 => 'Reach targets limit of stats',
        //Server side
        201 => 'AmazonS3 is not connect',
        202 => 'Failed to retrieve data',
        203 => 'Failed to retrieve AmazonS3',
    ];

    return [
        'error_num' => $error_num,
        'error_msg' => $error_msg[$error_num]
    ];
}
function validateParam($args,array $require_field){
    if (!isset($args) || empty($args)) {
        return [
            'valid' => false,
            'error_num' => 104
        ];
    }
    if(!empty($require_field)){
        foreach($require_field as $value){
            if(empty($args[$value]) || !isset($args[$value])){
                return [
                    'valid' => false,
                    'error_num' => 103
                ];
            }
        }
    }

    return [
        'valid' => true,
    ];
}