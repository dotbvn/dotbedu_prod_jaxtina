<?php

function uploadFileToS3FromUpload($path, $name, $parent_module){
    $savefile = $path;
    if(file_exists($path)){
        $dir_list = [
            'C_Comments' => 'comment',
            'C_News' => 'news',
            'C_Gallery' => 'gallery',
        ];
        require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';

        $AWS = new AWSHelper();
        if (!$AWS->getS3()) return false;

        $result = $AWS->uploadAWS($GLOBALS['dotb_config']['unique_key'].'/'.$dir_list[$parent_module], $name, $savefile);
        if($result['success']){
            return [
                'success' => true,
                's3_url' => $result['url'],
                'upload_id' => $result['key_name'],
                'cloud_storage' => 'Amazon S3'
            ];
        } else {
            return [
                'success' => false,
                'error' => 'Upload Fail'
            ];
        }
    }
    return [
        'success' => false,
        'error' => 'File not exist'
    ];
}