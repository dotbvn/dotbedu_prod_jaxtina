<?php
//$app_list_strings['bmes_parent_type']
class bmes{
    const bmes_parent_type = array(
        'Contacts'  => 'Student',
        'Leads'     => 'Lead',
        'Prospects' => 'Target',
        'Meetings'  => 'Session',
        'Cases'     => 'Feedback',
        'J_Class'   => 'Class',
        'J_PTResult'  => 'TestResult',
        'J_StudentSituations'  => 'StudentSituations',
        'J_ClassStudents'  => 'ClassStudents',
        'C_Attendance'  => 'Attendance',
        'J_Gradebook'   => 'Gradebook',
        'J_GradebookDetail'    => 'GradebookDetail',
        'J_Payment'     => 'Payment',
        'J_PaymentDetail' => 'Receipt',
        'C_Gallery'       => 'Gallery',
        'C_Comments'    => 'Chats',
        'C_Teachers'    => 'Teacher',
        'J_Teachercontract'    => 'TeacherContract',
        'Calls'    => 'Call',
        'Tasks'    => 'Task',
        'J_ReferenceLog'    => 'ReferenceLog',
        'J_BankTrans'    => 'Bank Transaction',
        'J_Metric'    => 'DOTB Metrikal',
    );
}
