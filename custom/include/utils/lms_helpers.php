<?php
require_once("custom/include/lms/canvas/autoload.php");
//Get LMS CONFIG
function getLMSConfig(){
    $config = new CanvasEnvironment;
    return $config;
}

//Get List LMS Canvan Course - By Lap nguyen
function getBlueprintCourse($use_cache = true){
    $lms = array();
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        if ($use_cache) {
            // Load cache từ config blueprint_course_list
            $admin = new Administration();
            $admin->retrieveSettings();
            $lms['enable']  = true;
            $lms['options'] = $admin->settings['lms_canvas_blueprint_course_list'];
        } else {
            // pass as array to API class
            $api = new \Uncgits\CanvasApi\CanvasApi([
                'client' => new \Uncgits\CanvasApi\Clients\Accounts,//instantiate a client
                'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
                'config' => $config,
            ]);
            // API class will use Accounts client.
            $result = $api
                ->addParameters(['blueprint' => true])
                ->setPerPage(9999)->listActiveCoursesInAccount($config->accountId);

            if ($result->getStatus() == 'success') {
                $lms['enable'] = true;
                $lms['type'] = 'canvas-lms';
                $lms['courses'] = $result->getContent();
                //genenerate dropdown
                $lms['options'] = array();
                $lms['options'][''] = '-none-';
                foreach ($lms['courses'] as $key => $val) {
                    $lms['options'][$val->id] = $val->name;
                    //parse key = course id
                    unset($lms['courses'][$key]);
                    $lms['courses'][$val->id] = $val;
                }
                $lms['content'] = json_encode($lms);

                // Save new course template config
                $admin = new Administration();
                $admin->retrieveSettings();
                $admin->saveSetting('lms_canvas', 'blueprint_course_list', json_encode($lms['options']));
            }
        }
    }
    return $lms;
}

function createNewCourse($class_bean){
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Courses,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        // API create Course
        $param = [    'course' => [
            'name' => $class_bean->name,
            'course_code' => $class_bean->class_code,
            'start_at' => $timedate->timestamp_to_iso8601(strtotime($class_bean->start_date)),
            'end_at' => $timedate->timestamp_to_iso8601(strtotime($class_bean->end_date)),
            //'sis_course_id' => $class_bean->id,
            'event' => 'offer',
            //'integration_id' => $class_bean->id,
            /*'public_description' => '',
            'allow_student_wiki_edits' => false,
            'allow_wiki_comments' => true,

            'open_enrollment' => false,
            'self_enrollment' => false,
            'license' => 'private',
            'is_public' => true,
            'is_public_to_auth_users' => true,
            'public_syllabus' => true,
            'public_syllabus_to_auth' => true,

            'term_id' => $class_bean->team_id,
            'hide_final_grades' => false,
            'default_view' => 'modules',
            'time_zone' => '',
            'enroll_me' => '',
            'syllabus_body' => '',
            'grading_standard_id' => '',
            'grade_passback_setting' => '',
            'course_format' => 'online',
            'enable_sis_reactivation' => '',*/
        ] ];

        $result = $api
        ->addParameters($param)
        ->createCourse($config->accountId);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['course']    = $result->getContent();
            $lms['course_id'] = $lms['course']->id;
        }else
            $lms['error'] = $result->getContent();
        return $lms;
    }
}

function updateCourseEvent($class_bean, $event = 'offer'){
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Courses,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        // API create Course
        $param = ['course' => ['event' => $event]];
        $result = $api
        ->addParameters($param)
        ->updateCourse($class_bean->lms_course_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['course']    = $result->getContent();
            $lms['course_id'] = $lms['course']->id;
        }else
            $lms['error'] = $result->getContent();
        return $lms;
    }
}

function updateCourse($class_bean){
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Courses,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        // API create Course
        $param = ['course' => [
            'name' => $class_bean->name,
            'course_code' => $class_bean->class_code,
            'start_at' => $timedate->timestamp_to_iso8601(strtotime($class_bean->start_date)),
            'end_at' => $timedate->timestamp_to_iso8601(strtotime($class_bean->end_date)),
        ]];
        $result = $api
        ->addParameters($param)
        ->updateCourse($class_bean->lms_course_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['course']    = $result->getContent();
            $lms['course_id'] = $lms['course']->id;
        }else
            $lms['error'] = $result->getContent();

        return $lms;
    }
}

function deleteCourse($class_bean){
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Courses,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        $result = $api
        ->addParameters(['event' => 'delete'])
        ->deleteOrConcludeCourse($class_bean->lms_course_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        return $lms;
    }
}

function updateAssociatedBlueprint($course_id, $template_id){
    if(empty($course_id)||empty($template_id) ) return false;
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Courses,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        //        // API create Course
        //        $param = [            'course' => [
        //            'course_ids_to_add' => [$course_id]
        //        ]  ];

        $result = $api->updateAssociatedCourses($template_id, $course_id);
        //Sync Data
        $result2 = $api->addParameters(['publish_after_initial_sync' => 'true'])
        ->migrateAssociatedCourses($template_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['status2'] = $result2->getStatus();
        $lms['message'] = $result->getMessage();
        $lms['message2'] = $result2->getMessage();
        if($lms['status'] == 'success')
            $lms['template_id'] = $template_id;

        return $lms;
    }
}

function removeAssociatedBlueprint($course_id, $template_id){
    if(empty($course_id)||empty($template_id) ) return false;
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Courses,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);

        $result = $api->removeAssociatedCourses($template_id, $course_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success')
            $lms['template_id'] = '';

        return $lms;
    }
}

function changeAssociatedBlueprint($course_id, $change_from_template_id, $change_to_template_id){
    if(empty($course_id)||empty($change_from_template_id) ) return false;
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Courses,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        //remove from old course
        $result = $api->removeAssociatedCourses($change_from_template_id, $course_id);
        //add to new course
        $result = $api->updateAssociatedCourses($change_to_template_id, $course_id);
        //Sync Data
        $result2 = $api
        ->addParameters(['publish_after_initial_sync' => true])
        ->migrateAssociatedCourses($change_to_template_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success')
            $lms['template_id'] = $change_to_template_id;

        return $lms;
    }
}

function createUser($_bean){
    global $timedate, $dotb_config;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Users,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);

        if($_bean->generate_password == 'defaut' || empty($_bean->generate_password)){
            $password = ($_bean->module_name == 'C_Teachers') ? $GLOBALS['db']->getOne("SELECT IFNULL(value, '') value FROM config WHERE name = 'defaultteacherpwd' AND category = 'pwd'") : $GLOBALS['db']->getOne("SELECT IFNULL(value, '') value FROM config WHERE name = 'defaultportalpwd' AND category = 'pwd'");
            if(empty($password)) $password = strtolower(str_replace(' ', '', $dotb_config['brand_name'])).'123';
        }

        if($_bean->generate_password == 'auto')
            $password = User::generatePassword();

        if($_bean->generate_password == 'manual')
            $password = $_POST['user_passwordd'];

        // API create Normal User
        $lms = array();
        if(empty($_bean->email1)){
            $lms['status'] = 'failed';
            $lms['error'] = 'This student doesnt have an email, cant create an account';
        }
        $param = [
            'user' => [
                'name'          => $_bean->name,
                'short_name'    => $_bean->name,
                'sortable_name' => $_bean->name,
                'skip_registration' => true,
            ],
            'pseudonym' => [
                'unique_id' => $_bean->email1,
                'password' => $password,
                'send_confirmation' => false,
            ],
        ];

        $result = $api
        ->addParameters($param)
        ->createUser($config->accountId);

        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['user']          = $result->getContent();
            $lms['user_id']       = $lms['user']->id;
            $lms['user_name']     = $lms['user']->login_id;
            $lms['user_password'] = $password;
        }else $lms['error'] = $result->getContent();
        return $lms;
    }else return false;
}

function listUserLogins($lms_user_id){
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Accounts,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        // API create Normal User

        $param = [
            'login' => [
                'unique_id' => $_bean->email1,
                'password' => $_bean->lms_user_password,
            ],
        ];

        $result = $api
        ->addParameters($param)
        ->editUserLogin($config->accountId, $_bean->lms_user_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['user']          = $result->getContent();
            $lms['user_id']       = $lms['user']->id;
            $lms['user_name']     = $lms['user']->login_id;
            $lms['user_password'] = $_bean->lms_user_password;
        }else
            $lms['error'] = $result->getContent();
        return $lms;
    }
}

function updateUserLogin($_bean, $type = 'unique_id'){
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Accounts,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);

        //Get List User Login
        $res1   = $api->getListUserLogin($config->accountId, $_bean->lms_user_id);
        $status = $res1->getStatus();
        if($status != 'success') return false;

        $login  = $res1->getContent()[0];
        // API Update Login
        if($type == 'unique_id')
            $param = [
                'login' => [
                    'unique_id' => $_bean->email1,
                    'account_id'=> $config->accountId,
                ],
            ];
        if($type == 'password')
            $param = [
                'login' => [
                    'unique_id' => $_bean->email1,
                    'account_id'=> $config->accountId,
                    'password'  => $_bean->lms_user_password,
                ],
            ];

        $result = $api
        ->addParameters($param)
        ->editUserLogin($config->accountId, $login->id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['user']          = $result->getContent();
            $lms['user_id']       = $lms['user']->id;
            $lms['user_name']     = $lms['user']->unique_id;
            $lms['user_password'] = $_bean->lms_user_password;
        }else
            $lms['error'] = $result->getContent();
        return $lms;
    }
}


function updateUser($_bean, $type = 'name'){
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Users,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        // API create Normal User
        if($type == 'name'){
            $param = [
                'user' => [
                    'name'          => $_bean->name,
                    'short_name'    => $_bean->name,
                    'sortable_name' => $_bean->name,
                ]
            ];
        }
        if(empty($param)) return false;

        $result = $api
        ->addParameters($param)
        ->updateUser($_bean->lms_user_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['user']          = $result->getContent();
            $lms['user_id']       = $lms['user']->id;
            $lms['user_name']     = $lms['user']->login_id;
            $lms['user_password'] = $_bean->lms_user_password;
        }else
            $lms['error'] = $result->getContent();
        return $lms;
    }
}

function deleteUser($lms_user_id){
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Accounts,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        $result = $api
        ->deleteUserFromRootAccount($config->accountId, $lms_user_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        return $lms;
    }
}

function enrollUser($user_id, $course_id, $role = 'StudentEnrollment'){
    if(empty($user_id)||empty($course_id) ) return false;
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Enrollments,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        // API create Course
        $param = [    'enrollment' => [
            //$role: StudentEnrollment, TeacherEnrollment, TaEnrollment, ObserverEnrollment, DesignerEnrollment , AccountMembership
            'user_id' => $user_id,
            'type' => $role,
        ]];

        $result = $api
        ->addParameters($param)
        ->enrollUserInCourse($course_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['enrollment']    = $result->getContent();
            $lms['enrollment_id'] = $lms['enrollment']->id;
        }else
            $lms['error'] = $result->getContent();
        return $lms;
    }
}

function deleteEnroll($course_id, $enroll_id){
    if(empty($course_id)||empty($enroll_id) ) return false;
    global $timedate;
    $config = new CanvasEnvironment;
    if(isset($config->token)){
        // pass as array to API class
        $api = new \Uncgits\CanvasApi\CanvasApi([
            'client'  => new \Uncgits\CanvasApi\Clients\Enrollments,//instantiate a client
            'adapter' => new \Uncgits\CanvasApi\Adapters\Guzzle,//instantiate a Guzzle
            'config'  => $config,
        ]);
        $result = $api
        ->concludeDeactivateOrDeleteEnrollment($course_id, $enroll_id);

        $lms = array();
        $lms['status'] = $result->getStatus();
        $lms['message'] = $result->getMessage();
        if($lms['status'] == 'success'){
            $lms['enrollment']    = $result->getContent();
            $lms['enrollment_id'] = $lms['user']->id;
        }else
            $lms['error'] = $result->getContent();
        return $lms;
    }
}

//----------------CLASS LMS FUNCTION - HELPER-----------------
function syncEnrollment($class_id){
    $bean = BeanFactory::getBean('J_Class', $class_id, array('disable_row_level_security'=>true));
    //Sycn Student
    $relStudents = $GLOBALS['db']->fetchArray("SELECT IFNULL(clt.id, '') primaryId,
        IFNULL(ct.id, '') student_id
        FROM j_classstudents clt
        INNER JOIN contacts ct ON ct.id = clt.student_id AND ct.deleted = 0
        WHERE clt.class_id = '{$bean->id}' AND clt.deleted = 0 AND (clt.lms_enrollment_id IS NULL OR lms_enrollment_id = '')");
    foreach($relStudents as $key => $row){
        $student = BeanFactory::getBean('Contacts', $row['student_id']);
        if(empty($student->lms_user_id)){
            $result = createUser($student);
            if($result['status'] == 'success'){
                $student->lms_user_id       = $result['user_id'];
                $student->lms_user_name     = $result['user_name'];
                $student->lms_user_password = $result['user_password'];
                $student->lms_content       = json_encode($result['user']);
            }else $student->lms_user_id = $student->lms_user_password = $student->lms_user_id = $student->lms_content ='';
            $student->save();
        }
        //Enroll to LMS
        if(!empty($student->lms_user_id)){
            $result2 = enrollUser($student->lms_user_id, $bean->lms_course_id);
            if($result2['status'] == 'success'){
                $err_id = $result2['enrollment_id'];
                $GLOBALS['db']->query("UPDATE j_classstudents SET lms_enrollment_id = '$err_id' WHERE id = '{$row['primaryId']}'");
            }
        }
    }
    //Sync Teacher/TA
    $relTea = $GLOBALS['db']->fetchArray("SELECT IFNULL(clt.id, '') primaryId,
        IFNULL(ct.id, '') teacher_id
        FROM j_class_c_teachers_1_c clt
        INNER JOIN c_teachers ct ON ct.id = clt.j_class_c_teachers_1c_teachers_idb AND ct.deleted = 0
        WHERE clt.j_class_c_teachers_1j_class_ida = '{$bean->id}' AND clt.deleted = 0 AND (clt.lms_enrollment_id IS NULL OR lms_enrollment_id = '')");
    foreach($relTea as $key => $row){
        $Tea = BeanFactory::getBean('C_Teachers', $row['teacher_id']);
        if(empty($Tea->lms_user_id)){
            $result = createUser($Tea);
            if($result['status'] == 'success'){
                $Tea->lms_user_id       = $result['user_id'];
                $Tea->lms_user_name     = $result['user_name'];
                $Tea->lms_user_password = $result['user_password'];
                $Tea->lms_content       = json_encode($result['user']);
            }else $Tea->lms_user_id = $Tea->lms_user_password = $Tea->lms_user_id = $Tea->lms_content ='';
            $Tea->save();
        }
        //Enroll to LMS
        if(!empty($Tea->lms_user_id)){
            if($Tea->type == 'Teacher')
                $result2 = enrollUser($Tea->lms_user_id, $bean->lms_course_id, 'TeacherEnrollment');
            if($Tea->type == 'TA')
                $result2 = enrollUser($Tea->lms_user_id, $bean->lms_course_id, 'TaEnrollment');
            if($result2['status'] == 'success'){
                $err_id = $result2['enrollment_id'];
                $GLOBALS['db']->query("UPDATE j_class_c_teachers_1_c SET lms_enrollment_id = '$err_id' WHERE id = '{$row['primaryId']}'");
            }
        }
    }
    return true;
}


