<?php
    $entry_point_registry['quickEditAdmin'] = array('file' => 'custom/include/utils/quickEditAdmin.php' , 'auth' => true);

    $entry_point_registry['CallRecording'] = array('file' => 'custom/clients/base/api/CallRecording.php' , 'auth' => true);

    $entry_point_registry['Monitor'] = array('file' => 'custom/modules/C_Attendance/attendanceMonitor.php' , 'auth' => '0');
    $entry_point_registry['AjaxMonitor'] = array('file' => 'custom/modules/C_Attendance/ajax_post_code.php' , 'auth' => '0');

    $entry_point_registry['capProspect'] = array('file' => 'custom/include/capLead.php' , 'auth' => '0');
    $entry_point_registry['capLead'] = array('file' => 'custom/include/capLead.php' , 'auth' => '0');

    $entry_point_registry['generateGoogleMeet'] = array('file' => 'custom/include/lms/alovip-ggc/generateGoogleMeet.php' , 'auth' => '0');
    $entry_point_registry['uploadFile'] = array('file' => 'custom/include/javascript/file_upload/file_upload_parser.php' , 'auth' => '0');
    $entry_point_registry['signS3'] = array('file' => 'custom/include/javascript/s3Upload/s3Sign.php' , 'auth' => '0');
    $entry_point_registry['viewSubscriptionPdf'] = array('file' => 'custom/modules/EMS_Settings/view_pdf.php' , 'auth' => '0');
    ?>
