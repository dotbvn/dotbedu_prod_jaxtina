<?php
$subpanel_layout['list_fields'] = array (
    'holiday_date' =>
        array (
            'vname' => 'LBL_HOLIDAY_DATE',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '21',
            'default' => true,
        ),
    'description' =>
        array (
            'vname' => 'LBL_DESCRIPTION',
            'width' => '75',
            'sortable' => false,
            'default' => true,
        ),
//    'edit_button' =>
//        array (
//            'vname' => 'LBL_EDIT_BUTTON',
//            'widget_class' => 'SubPanelEditButton',
//            'width' => '2',
//            'default' => true,
//        ),
);
