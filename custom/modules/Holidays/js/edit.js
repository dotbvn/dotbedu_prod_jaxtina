({
    extendsFrom: 'CreateView',
    save: function () {
        debugger;

        self = this;
        var url = App.api.buildURL('gallery/check_config');
        App.api.call('create', url, {modules: 'C_Gallery'}, {
            success: function (data) {
                if (data.success) {
                    self.saveAndClose();
                } else {
                    App.alert.show('message-error', {
                        level: 'error',
                        messages: 'Please config Google API first!',
                        autoClose: true
                    });
                }
            }
        });
    },
})