<?php

$viewdefs['Holidays']['base']['view']['selection-list'] = array(
    'panels' => array(
        array(
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                0 =>
                array (
                    'name' => 'holiday_date',
                    'label' => 'LBL_HOLIDAY_DATE',
                    'enabled' => true,
                    'default' => true,
                    'type' => 'html',
                    'width' => 'large',
                ),
                1 =>
                array (
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'default' => true,
                    'enabled' => true,
                ),
                3 =>
                array (
                    'name' => 'date_modified',
                    'vname' => 'LBL_DATE_MODIFIED',
                    'default' => true,
                    'enabled' => true,
                ),
            ),
        ),
    ),
);
