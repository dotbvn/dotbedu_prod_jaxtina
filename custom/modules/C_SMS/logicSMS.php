<?php
class logicSMS {
    function handleSendSMS(&$bean, $event, $arguments){
        global $timedate, $current_user;
        //Xử lý: Gom các quan hệ
        if($bean->parent_type == 'J_PTResult'){
            $result = BeanFactory::getBean($bean->parent_type, $parent_id);
            $bean->parent_id    = $result->student_id;
            $bean->parent_type  = $result->parent;
        }
        if ($bean->parent_type == "J_StudentSituations"){
            $situa = BeanFactory::getBean($bean->parent_type, $parent_id);
            $bean->parent_id    = $situa->student_id;
            $bean->parent_type  = $situa->student_type;
        }

        //Xử lý: Logic gửi SMS
        if(empty($bean->delivery_status)){
            require_once("modules/C_SMS/SMS/SMSSender.php");
            require_once("custom/include/utils/parseTemplate.php");

            $parentBean = BeanFactory::getBean($bean->parent_type, $bean->parent_id);
            if(empty($bean->phone_number)) $bean->phone_number = $parentBean->phone_mobile;
            if(empty($bean->date_in_content)) $bean->date_in_content = $timedate->nowDbDate();
            if(empty($bean->assigned_user_id)) $bean->assigned_user_id = $GLOBALS['current_user']->id;
            $bean->team_id = $parentBean->team_id;
            if(empty($bean->team_id)) $bean->team_id = $current_user->team_id;
            $bean->team_set_id = $bean->team_id;
            $message    = parse_template_SMS($bean->template_id, $bean->parent_type, $bean->parent_id, $bean->description);
            $message    = viToEn($message);

            $bean->name = 'Send to '.$parentBean->name.' - '.$bean->phone_number;
            $bean->description = $message;
            $bean->supplier    = getSMSSupplier($bean->phone_number);
            $bean->message_count  = countSms($message);

            if(!empty($bean->phone_number) && !empty($bean->description)){
                //Sent $phone_number = '', $content = '', $team_id = '', $smsId = ''
                $result = SMSSender::sendSMS($bean->phone_number, $bean->description, $bean->team_id, $bean->id);
                $return = (int)$result['success'];
                $status = 'RECEIVED';
                if($return <= 0) $status = 'FAILED';
                elseif ($return == 2) $status = 'DELIVERED';
                $bean->delivery_status = $status;

            }else $bean->delivery_status = 'FAILED';
        }
    }
}
?>
