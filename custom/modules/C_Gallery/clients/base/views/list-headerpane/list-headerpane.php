<?php


$viewdefs['C_Gallery']['base']['view']['list-headerpane'] = array(

    'title' => 'LBL_C_GALLERY',
    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_GALLERY',
			'tooltip' => 'LNK_NEW_J_GALLERY',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'C_Gallery',
            'route'=>'#C_Gallery/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_GALLERY_REPORTS',
			'tooltip' => 'LNK_J_GALLERY_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=C_Gallery',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);