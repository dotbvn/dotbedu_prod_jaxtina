<?php
$module_name = 'C_Gallery';
$viewdefs[$module_name] =
    array(
        'base' =>
            array(
                'view' =>
                    array(
                        'list' =>
                            array(
                                'panels' =>
                                    array(
                                        0 =>
                                            array(
                                                'label' => 'LBL_PANEL_1',
                                                'fields' =>
                                                    array(
                                                        0 =>
                                                            array(
                                                                'name' => 'name',
                                                                'label' => 'LBL_NAME',
                                                                'default' => true,
                                                                'enabled' => true,
                                                                'link' => true,
                                                            ),
                                                        1 =>
                                                            array(
                                                                'name' => 'status',
                                                                'label' => 'LBL_STATUS',
                                                                'type' => 'status-field',
                                                                'readonly' => true,
                                                                'colorClass' => [
                                                                    'approved' => 'label-success',
                                                                    'rejected' => 'label-important',
                                                                    'wait_for_approval' => 'label-warning',
                                                                ]
                                                            ),
                                                        2 =>
                                                            array(
                                                                'name' => 'j_class_c_gallery_1_name',
                                                                'label' => 'LBL_J_CLASS_C_GALLERY_1_FROM_J_CLASS_TITLE',
                                                                'enabled' => true,
                                                                'id' => 'J_CLASS_C_GALLERY_1J_CLASS_IDA',
                                                                'link' => true,
                                                                'sortable' => false,
                                                                'default' => true,
                                                            ),
                                                        3 =>
                                                            array(
                                                                'name' => 'description',
                                                                'label' => 'LBL_DESCRIPTION',
                                                                'enabled' => true,
                                                                'sortable' => false,
                                                                'default' => true,
                                                            ),
                                                        4 =>
                                                            array(
                                                                'name' => 'team_name',
                                                                'label' => 'LBL_TEAM',
                                                                'default' => true,
                                                                'enabled' => true,
                                                            ),
                                                        5 =>
                                                            array(
                                                                'name' => 'date_modified',
                                                                'enabled' => true,
                                                                'default' => true,
                                                            ),
                                                        6 =>
                                                            array(
                                                                'name' => 'date_entered',
                                                                'enabled' => true,
                                                                'default' => true,
                                                            ),
                                                        7 =>
                                                            array(
                                                                'name' => 'assigned_user_name',
                                                                'label' => 'LBL_ASSIGNED_TO_NAME',
                                                                'default' => false,
                                                                'enabled' => true,
                                                                'link' => true,
                                                            ),
                                                    ),
                                            ),
                                    ),
                                'orderBy' =>
                                    array(
                                        'field' => 'date_modified',
                                        'direction' => 'desc',
                                    ),
                            ),
                    ),
            ),
    );
