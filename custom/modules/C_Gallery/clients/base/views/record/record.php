<?php
$module_name = 'C_Gallery';
$viewdefs[$module_name] =
    array(
        'base' =>
            array(
                'view' =>
                    array(
                        'record' =>
                            array(
                                'buttons' =>
                                    array(
                                        0 =>
                                            array(
                                                'type' => 'button',
                                                'name' => 'cancel_button',
                                                'label' => 'LBL_CANCEL_BUTTON_LABEL',
                                                'css_class' => 'btn',
                                                'showOn' => 'edit',
                                                'tooltip' => 'LBL_CANCEL_BUTTON_LABEL',
                                                'icon' => 'fa-times',
                                                'customButton' => true,
                                                'dismiss_label' => true,
                                                'events' =>
                                                    array(
                                                        'click' => 'button:cancel_button:click',
                                                    ),
                                            ),
                                        1 =>
                                            array(
                                                'type' => 'rowaction',
                                                'event' => 'button:save_button:click',
                                                'name' => 'save_button',
                                                'label' => 'LBL_SAVE_BUTTON_LABEL',
                                                'css_class' => 'btn btn-primary',
                                                'showOn' => 'edit',
                                                'tooltip' => 'LBL_SAVE_BUTTON_LABEL',
                                                'icon' => 'fa-save',
                                                'customButton' => true,
                                                'acl_action' => 'edit',
                                            ),
                                        2 =>
                                            array(
                                                'type' => 'actiondropdown',
                                                'name' => 'main_dropdown',
                                                'primary' => true,
                                                'showOn' => 'view',
                                                'buttons' =>
                                                    array(
                                                        0 =>
                                                            array(
                                                                'type' => 'rowaction',
                                                                'event' => 'button:edit_button:click',
                                                                'name' => 'edit_button',
                                                                'label' => 'LBL_EDIT_BUTTON_LABEL',
                                                                'tooltip' => 'LBL_EDIT_BUTTON_LABEL',
                                                                'icon' => 'fa-pencil',
                                                                'customButton' => true,
                                                                'acl_action' => 'edit',
                                                            ),
                                                        1 =>
                                                            array (
                                                                'type' => 'closebutton',
                                                                'name' => 'record-close',
                                                                'label' => 'LBL_MARK_APPROVED_BUTTON_TITLE',
                                                                'closed_status' => 'approved',
                                                                'acl_action' => 'edit',
                                                            ),
                                                        2 =>
                                                            array (
                                                                'type' => 'closebutton',
                                                                'name' => 'record-close',
                                                                'label' => 'LBL_MARK_REJECT_BUTTON_TITLE',
                                                                'closed_status' => 'rejected',
                                                                'acl_action' => 'edit',
                                                            ),
                                                        4 =>
                                                            array(
                                                                'type' => 'divider',
                                                            ),
                                                        5 =>
                                                            array(
                                                                'type' => 'rowaction',
                                                                'event' => 'button:send_notification:click',
                                                                'name' => 'send_notification',
                                                                'label' => 'LBL_SENT_NOTIFICATION',
                                                                'acl_action' => 'edit',
                                                                'acl_module' => 'C_Gallery',
                                                            ),
                                                        6 =>
                                                            array(
                                                                'type' => 'rowaction',
                                                                'event' => 'button:find_duplicates_button:click',
                                                                'name' => 'find_duplicates_button',
                                                                'label' => 'LBL_DUP_MERGE',
                                                                'acl_action' => 'edit',
                                                            ),
                                                        7 =>
                                                            array(
                                                                'type' => 'rowaction',
                                                                'event' => 'button:duplicate_button:click',
                                                                'name' => 'duplicate_button',
                                                                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                                                                'acl_module' => 'C_Gallery',
                                                                'acl_action' => 'create',
                                                            ),
                                                        8 =>
                                                            array(
                                                                'type' => 'rowaction',
                                                                'event' => 'button:audit_button:click',
                                                                'name' => 'audit_button',
                                                                'label' => 'LNK_VIEW_CHANGE_LOG',
                                                                'acl_action' => 'view',
                                                            ),
                                                        9 =>
                                                            array(
                                                                'type' => 'divider',
                                                            ),
                                                        10 =>
                                                            array(
                                                                'type' => 'rowaction',
                                                                'event' => 'button:delete_button:click',
                                                                'name' => 'delete_button',
                                                                'label' => 'LBL_DELETE_BUTTON_LABEL',
                                                                'acl_action' => 'delete',
                                                            ),
                                                    ),
                                            ),
                                        3 =>
                                            array(
                                                'name' => 'sidebar_toggle',
                                                'type' => 'sidebartoggle',
                                            ),
                                    ),
                                'panels' =>
                                    array(
                                        0 =>
                                            array(
                                                'name' => 'panel_header',
                                                'label' => 'LBL_RECORD_HEADER',
                                                'header' => true,
                                                'fields' =>
                                                    array(
                                                        0 =>
                                                            array(
                                                                'name' => 'picture',
                                                                'type' => 'avatar',
                                                                'width' => 42,
                                                                'height' => 42,
                                                                'dismiss_label' => true,
                                                                'readonly' => true,
                                                            ),
                                                        1 => 'name',
                                                        2 =>
                                                            array (
                                                                'name' => 'status',
                                                                'label' => 'LBL_STATUS',
                                                                'type' => 'status-field',
                                                                'readonly' => true,
                                                                'colorClass' => [
                                                                    'approved' => 'label-success',
                                                                    'rejected' => 'label-important',
                                                                    'wait_for_approval' => 'label-warning',
                                                                ]
                                                            ),
                                                    ),
                                            ),
                                        1 =>
                                            array(
                                                'name' => 'panel_body',
                                                'label' => 'LBL_RECORD_BODY',
                                                'columns' => 2,
                                                'labelsOnTop' => true,
                                                'placeholders' => true,
                                                'newTab' => false,
                                                'panelDefault' => 'expanded',
                                                'fields' =>
                                                    array(
                                                        0 => array(
                                                            'name' => 'visibility_range'
                                                        ),
                                                        1 =>
                                                            array(
                                                                'name' => 'tag',
                                                            ),
                                                        2 =>
                                                            array(
                                                                'name' => 'j_class_c_gallery_1_name',
                                                            ),

                                                        3 =>
                                                            array(
                                                                'name' => 'gallery_date',
                                                            ),
                                                        4 =>
                                                            array(
                                                                'name' => 'team_name',
                                                                'label' => 'LBL_LIST_TEAM',
                                                            ),
                                                        5 =>
                                                            array(
                                                                'name' => 'description',
                                                                'span' => 12,
                                                            ),
                                                        6 =>
                                                            array(
                                                                'name' => 'date_modified_by',
                                                                'readonly' => true,
                                                                'inline' => true,
                                                                'type' => 'fieldset',
                                                                'label' => 'LBL_DATE_MODIFIED',
                                                                'fields' =>
                                                                    array(
                                                                        0 =>
                                                                            array(
                                                                                'name' => 'date_modified',
                                                                            ),
                                                                        1 =>
                                                                            array(
                                                                                'type' => 'label',
                                                                                'default_value' => 'LBL_BY',
                                                                            ),
                                                                        2 =>
                                                                            array(
                                                                                'name' => 'modified_by_name',
                                                                            ),
                                                                    ),
                                                            ),
                                                        7 =>
                                                            array(
                                                                'name' => 'date_entered_by',
                                                                'readonly' => true,
                                                                'inline' => true,
                                                                'type' => 'fieldset',
                                                                'label' => 'LBL_DATE_ENTERED',
                                                                'fields' =>
                                                                    array(
                                                                        0 =>
                                                                            array(
                                                                                'name' => 'date_entered',
                                                                            ),
                                                                        1 =>
                                                                            array(
                                                                                'type' => 'label',
                                                                                'default_value' => 'LBL_BY',
                                                                            ),
                                                                        2 =>
                                                                            array(
                                                                                'name' => 'created_by_name',
                                                                            ),
                                                                    ),
                                                            ),
                                                        8 =>
                                                            array (
                                                                'name' => 'attachment_list',
                                                                'label' => 'LBL_ATTACHMENTS',
                                                                'type' => 's3-multi',
                                                                'span' => 12
                                                            )
                                                    ),
                                            ),
                                    ),
                                'templateMeta' =>
                                    array(
                                        'useTabs' => false,
                                    ),
                            ),
                    ),
            ),
    );
