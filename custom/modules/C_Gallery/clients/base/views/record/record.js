({
    extendsFrom: 'RecordView',

    initialize: function (options) {
        this._super('initialize', [options]);

        this.events = _.extend({}, this.events, {
            'click [name=send_notification]': 'sendNotification',
        });
    },

    sendNotification: function () {
        var id = app.controller.context.attributes.modelId;
        $.ajax({
            type: "POST",
            url: "index.php?module=C_Gallery&action=handleAjaxGallery&dotb_body_only=true",
            data:  {
                type            : 'ajaxSendNotification',
                gallery_id      : id,
            },
            success: function(data){
                data = JSON.parse(data)
                if(data != null && data.success == '1'){
                    app.alert.show('send_notification_success', {
                        level: 'success',
                        autoClose: true,
                        messages: 'Sent Notification!'
                    }); 
                }else {
                    app.alert.show('send_notification_error', {
                        level: 'error',
                        autoClose: true,
                        messages: 'Send Notification Error!'
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.alert.show('send_notification_error', {
                    level: 'error',
                    autoClose: true,
                    messages: 'Send Notification Error!'
                });
            },
        });
    },
});
