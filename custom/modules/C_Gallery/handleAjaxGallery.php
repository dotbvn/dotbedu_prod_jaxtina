<?php
switch ($_POST['type']) {
    case 'ajaxSendNotification':
        $result = ajaxSendNotification($_POST['gallery_id']);
        echo $result;
        break;
}

function ajaxSendNotification($gallery_id){
    $gallery = BeanFactory::getBean('C_Gallery', $gallery_id);
    $gallery->send_notification = 1;
    $gallery->save();
    return json_encode(array(
        "success" => "1",
    ));
}