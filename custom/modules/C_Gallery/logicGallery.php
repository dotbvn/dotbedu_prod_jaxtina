<?php
require_once 'custom/include/guzzlehttp/vendor/autoload.php';
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicGallery
{
    public function handleAfterSave(&$bean, $event, $arguments)
    {
        if ((isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) || $bean->send_notification) {
            require_once("custom/clients/mobile/helper/NotificationHelper.php");
            $notify = new NotificationMobile();

            //convert datetime (gallery_date) to date
            $gallery_date = date("Y-m-d", strtotime($bean->gallery_date));

            $qStudent = "SELECT DISTINCT
                IFNULL(contacts.id, '') student_id
            FROM contacts
            INNER JOIN j_studentsituations l1 ON
                contacts.id = l1.student_id AND l1.deleted = 0 AND l1.student_type = 'Contacts'
            WHERE l1.ju_class_id = '{$bean->j_class_c_gallery_1j_class_ida}' AND contacts.deleted = 0
                AND (IFNULL(l1.type, '') IN ('OutStanding' , 'Enrolled'))
                AND (l1.start_study <= '{$gallery_date}')
                AND (l1.end_study >= '{$gallery_date}')
                AND (IFNULL(contacts.status, '') NOT IN ('Cancelled'))";

            $resultStudent = $GLOBALS['db']->query($qStudent);

            while ($row = $GLOBALS['db']->fetchByAssoc($resultStudent)) {
                $notify->pushNotification('Album ' . $bean->name . ' vừa mới được cập nhật!', 'Nhấn vào xem chi tiết.', 'C_Gallery', $bean->id, $row['student_id'], 'Student');
            }
        }
    }

    public function handleBeforeDelete($bean, $event, $arguments)
    {
        //delete notification
        $q1 = "UPDATE notifications
        SET deleted = 1
        WHERE deleted = 0
        AND parent_id = '{$bean->id}'
        AND parent_type = 'C_Gallery'";
        $GLOBALS['db']->query($q1);
    }
}
