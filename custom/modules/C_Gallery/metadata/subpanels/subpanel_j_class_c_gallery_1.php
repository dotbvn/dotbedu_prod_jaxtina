<?php
// created: 2019-07-11 21:27:23
$subpanel_layout['list_fields'] = array(

    'name' =>
        array(
            'vname' => 'LBL_NAME',
            'widget_class' => 'SubPanelDetailViewLink',

            'default' => true,
        ),
    'description' =>
        array(
            'vname' => 'LBL_DESCRIPTION',
            'widget_class' => 'SubPanelDetailViewLink',

            'default' => true,
        ),

    'date_modified'=> array(
        'vname' => 'LBL_DATE_MODIFIED',
        'widget_class' => 'SubPanelDetailViewLink',

        'default' => true,
    ),

    'date_entered'=> array(
        'vname' => 'LBL_DATE_ENTERED',
        'widget_class' => 'SubPanelDetailViewLink',

        'default' => true,
    ),

);