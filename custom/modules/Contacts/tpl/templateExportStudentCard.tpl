<link  rel="stylesheet" type="text/css" href="custom/modules/Contacts/tpl/card.css">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div class="card">
    <img id="student_avatar"
         src="{$PICTURE}"
         alt="{$STUDENT_NAME}" style="">
    <p id="label_student">STUDENT CARD</p>
    <p id="student_name">{$STUDENT_NAME}</p>

    <div id="card_info">
        <p>{$DAY}.{$MONTH}.{$YEAR}</p>
        <p>{$MEMBER_ID}</p>
    </div>

    <div id="bar_code">
        <img src="{$QRCODE}">
    </div>

</div>
