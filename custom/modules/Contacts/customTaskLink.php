<?php
class customTaskLink extends Link2 {
    public function buildJoinDotbQuery($dotb_query, $option = array()) {
        $dotb_query->where()->queryOr()
            ->notEquals('is_customer_journey_activity', '1')
            ->queryAnd()->equals('is_customer_journey_activity', '1')->notEquals('status', 'Not Started');
        return $this->relationship->buildJoinDotbQuery($this, $dotb_query, $option);
    }
}