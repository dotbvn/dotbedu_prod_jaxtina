<?php
/**
 * Add by Tuan Anh
 */
$hook_version = 2;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1, 'Update Total Subscription For License', 'custom/modules/Contacts/checkLicense.php', 'logicCheckLicense', 'updateTotalSubscription');
$hook_array['before_save'][] = Array(2, 'Save full name', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'saveFullName');

$hook_array['before_save'][] = Array(5, 'Add student to LMS', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'handleLMSUser');
$hook_array['before_save'][] = Array(6, 'FaceID', 'custom/modules/Contacts/logicRegisterFaceID.php', 'FaceID', 'HandleSaveFaceId');
$hook_array['before_save'][] = Array(7, 'Remove Access Token', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'handleAccessToken');
$hook_array['before_save'][] = Array(8, 'Handle before save', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'handleBfSave');

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(1, 'Decrease current total subscription', 'custom/modules/Contacts/checkLicense.php', 'logicCheckLicense', 'decreaseCurrentTotalSubscription');
$hook_array['before_delete'][] = Array(2, 'Handle delete', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'beforeDeleteStudent');

$hook_array['after_retrieve'] = Array();
$hook_array['after_retrieve'][] = Array(1, '', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'handleAfterRetrieve');
$hook_array['after_retrieve'][] = Array(2, 'Handle FaceID Retrieve', 'custom/modules/Contacts/logicRegisterFaceID.php', 'FaceID', 'HandleRetrieveFaceID');
$hook_array['after_retrieve'][] = Array(100, 'Load License Status', 'custom/modules/Contacts/logicContact.php','logicContact', 'loadLicenseStatus');

$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1, 'Update Student Status', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'handleAfterSave');
$hook_array['after_save'][] = Array(100, 'Add student id', 'custom/modules/Contacts/logicContact.php', 'logicContact', 'addCode');

$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(1, 'Color Status',      'custom/modules/Contacts/logicContact.php','logicContact', 'listviewcolor');
$hook_array['process_record'][] = Array(2, 'Contact Activity',  'custom/modules/Contacts/logicContact.php','logicContact', 'contactActivity');

$hook_array['before_import'] = Array();
$hook_array['before_import'][] = Array(1, 'Handle Before Import', 'custom/modules/Contacts/logicStudentImport.php','logicStudentImport', 'handleBeforeImport');

//Custom Jaxtina
$hook_array['before_save'][] = Array(11, 'Create LMS Account ', 'custom/modules/Contacts/logicJaxLMS.php', 'logicJaxLMS', 'handleAfterLMS');
