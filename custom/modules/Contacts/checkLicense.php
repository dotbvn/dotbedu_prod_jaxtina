<?php

if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicCheckLicense
{
    /// for before_save
    /**
     * @throws DotbApiExceptionMaintenance
     */
    public function updateTotalSubscription(&$bean, $event, $arguments)
    {
        if ($GLOBALS['service']->action === 'save' && $arguments['isUpdate'] !== true) { /// add record
            $licenseBean = new LicenseBean();
            $licenseBean->fromClient();
            $totalSubscription = (int)$licenseBean->lic_activated_subscription + 1;
            if ($totalSubscription > (int)$licenseBean->lic_subscription) {
                /// over subscription
                $this->redirectExceedingLimitation();
            } else {
                $licenseBean->lic_activated_subscription = $totalSubscription;
                $licenseBean->save();
            }
            unset($licenseBean);

            return;
        }

        if ($GLOBALS['service']->action === 'save' && $arguments['isUpdate'] === true) { /// edit record
            $oldRecord = BeanFactory::getBean('Contacts', $bean->id);

            if ($oldRecord->ems_active_state !== $bean->ems_active_state) {
                $licenseBean = new LicenseBean();
                $licenseBean->fromClient();

                switch ($bean->ems_active_state) {
                    case 1: //// change from deactivate to activate
                        $totalSubscription = (int)$licenseBean->lic_activated_subscription + 1;
                        if ($totalSubscription > (int)$licenseBean->lic_subscription) {
                            /// over subscription
                            $this->redirectExceedingLimitation();
                        } else {
                            $licenseBean->lic_activated_subscription = $totalSubscription;
                            $licenseBean->save();
                        }
                        break;
                    case 0: //// change from activate to deactivate
                        $totalSubscription = (int)$licenseBean->lic_activated_subscription -1;
                        $licenseBean->lic_activated_subscription = $totalSubscription;
                        $licenseBean->save();
                        break;
                    default: break;
                }

                unset($licenseBean);
            }
        }
    }

    /// before_delete
    public function decreaseCurrentTotalSubscription(&$bean, $event, $arguments)
    {
        if ($event === 'before_delete') {
            $licenseBean = new LicenseBean();
            $licenseBean->fromClient();

            switch ($bean->ems_active_state) {
                case 1: //// is activated
                    $totalSubscription = (int)$licenseBean->lic_activated_subscription - 1;
                    $licenseBean->lic_activated_subscription = $totalSubscription;
                    $licenseBean->save();
                    break;
                default: break;
            }

            unset($licenseBean);
        }
    }
    /**
     * @throws DotbApiExceptionMaintenance
     */
    function redirectExceedingLimitation() {
        $e = new DotbApiExceptionMaintenance(
            'EXCEPTION_MAINTENANCE',
            null,
            null,
            0,
            'maintenance'
        );
        $e->setExtraData("url", '#bwc/index.php?module=EMS_Settings&action=subscription&bwcRedirect=1');

        throw $e;
    }
}
