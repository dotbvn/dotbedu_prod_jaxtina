<?php
require_once 'custom/include/QRCode/phpqrcode.php';
global $db, $dotb_config;
$q = "SELECT IFNULL(l1.id, '') contact_id
        , IFNULL(l1.picture, '') picture
        , IFNULL(l1.full_student_name, '') full_student_name
        , IFNULL(l1.date_issue, '') date_issue
        , IFNULL(l1.contact_id, '') member_id
        FROM contacts l1
        WHERE l1.deleted=0
        AND l1.id = '{$_GET['id']}'";
$row = $db->fetchOne($q);

if (empty($row['picture'])) {
    $picture = 'custom/images/no_avata.png';
} elseif (file_exists('upload/image3x4/' . $row['picture'])) {
    $picture = 'upload/image3x4/' . $row['picture'];
} else {
    require_once('include/DotbFields/Fields/Image/ImageHelper.php');
    if (!is_dir('upload/image3x4')) {
        mkdir('upload/image3x4');
    }
    $pictureOri = 'upload/origin/' . $row['picture'];
    if (file_exists($pictureOri)) {
        $imgOri = new ImageHelper($pictureOri);
        $imgOri->resize(220, 293.3);
        $imgOri->save('upload/image3x4/' . $row['picture']);
        $picture = 'upload/image3x4/' . $row['picture'];
    } else $picture = 'custom/images/no_avata.png';
}
$picture = 'data:image/png;base64,' . base64_encode(file_get_contents($picture));

$date_issue = explode("-", $row['date_issue']);
$member_id = $row['member_id'];

if (empty($row['member_id'])) {
    $qrContent = file_get_contents('custom/images/no_qrcode.png');
    $qr = base64_encode($qrContent);
} else {
    QRcode::png($row['member_id'], dotb_cached('qrCode.png'));
    $qrContent = file_get_contents(dotb_cached('qrCode.png'));
    unlink(dotb_cached('qrCode.png'));
    $qr = base64_encode($qrContent);
}

$link_css = "../custom/modules/Contacts/tpl/exportCard.css";

$smarty = new Dotb_Smarty();
$smarty->assign('LINK_CSS', $link_css);
$smarty->assign('PICTURE', $picture);
$smarty->assign('STUDENT_NAME', $row['full_student_name']);
$smarty->assign('DAY', $date_issue[2]);
$smarty->assign('MONTH', $date_issue[1]);
$smarty->assign('YEAR', $date_issue[0]);
$smarty->assign('MEMBER_ID', $member_id);
$smarty->assign('QRCODE', 'data:image/png;base64,' . $qr);
$body = $smarty->fetch('custom/modules/Contacts/tpl/templateStudentCard.tpl');

$filehtml = dotb_cached(time() . '.html');
file_put_contents($filehtml, $body);
$filepdf = dotb_cached(time() . '.pdf');
shell_exec('xvfb-run wkhtmltopdf ' . $filehtml . ' ' . $filepdf);
$filepdfname = $row['member_id'] . '.pdf';
ob_clean();
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename="' . $filepdfname . '"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($filepdf));
readfile($filepdf);

unlink($filehtml);
unlink($filepdf);
exit;

?>
