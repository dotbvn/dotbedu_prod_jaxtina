<?php
class FaceID {
    public function HandleSaveFaceId(&$bean, $event, $arguments)
    {
        global $dotb_config;
        if($dotb_config['enable_face_id']){
            $check_count = strlen($dotb_config['site_url']);
            $site = str_replace('https://', '',$dotb_config['site_url']);
            if($check_count == strlen($site)){
                $site = str_replace('http://', '',$dotb_config['site_url']);
            }
            $device_list = $GLOBALS['db']->getOne("SELECT frc_device_id_list FROM contacts WHERE id = '{$bean->id}'");
            $bean->frc_device_id_list = $device_list;
            if($bean->getFieldValue('frc_face_id_image') === false){
                $bean->frc_face_id_image = $bean->fetched_row['frc_face_id_image'];
                return;
            }
            if ($bean->fetched_row['frc_face_id_image'] != $bean->getFieldValue('frc_face_id_image')){
                $bean->frc_device_id_list = '';
            }
            if($bean->frc_device_id_list == '0'){
                return;
            }
            $q = "SELECT IFNULL(secret_key,'') secret_key,
                     IFNULL(device_id, '') device_id,
                     IFNULL(l1.id, '') att_id
                FROM contacts l2
                INNER JOIN team_sets_teams l3 ON l2.team_set_id = l3.team_set_id AND l3.deleted = 0
                INNER JOIN j_attdevice l1 ON l1.team_id = l3.team_id AND l1.deleted = 0
                WHERE l2.id = '{$bean->id}'";
            $device_data = $GLOBALS['db']->fetchArray($q);
            if (!empty($device_data)) {
                $index = 0;
                foreach ($device_data as $data) {
                    $url = 'http://cam-api.bsmart.city/v2/cam/person/' . $data['device_id'];
                    $student = array();
                    $student['id'] = $bean->id;
                    $student['full_student_name'] = $bean->full_student_name;
                    $pic_id = create_guid();
                    $uploadFile = new UploadFile();
                    if (!empty($bean->frc_face_id_image)) {
                        if ($bean->fetched_row['frc_face_id_image'] != $bean->frc_face_id_image) {
                            $img_file = 'http://cam-api.bsmart.city/v2/cam/proxy/' . $site . '/upload/origin/' . $bean->frc_face_id_image;
                            if (!$this->isConnectToDevice($device_list, $data['att_id'])) {
                                $action = 'POST';
                            } else {
                                $action = 'PUT';
                            }
                            $respone = RegisterFaceID($student, $url, $img_file, $action, $data['secret_key'], $data['att_id']);
                            if($respone['success'] == 1){
                                $bean->frc_device_id_list .= ($index == 0) ? '^'.$data['att_id'].'^' : ',^'.$data['att_id'].'^';
                                $index++;
                            }
                            $uploadFile->duplicate_file('origin/'.$bean->frc_face_id_image,'FaceID/RegisterImage/' .$pic_id);
                            $bean->frc_face_id_image = $pic_id;
                            $uploadFile->unlink_file('origin/' . $bean->frc_face_id_image);
                        }
                    } else {
                        if(!empty($bean->picture) && empty($bean->fetched_row['frc_face_id_image']) && $bean->fetched_row['frc_device_id_list'] != '0'){
                            $img_file = 'http://cam-api.bsmart.city/v2/cam/proxy/' . $site . '/upload/origin/' . $bean->picture;
                            $respone = RegisterFaceID($student, $url, $img_file, 'POST', $data['secret_key'], $data['att_id']);
                            if($respone['success'] == 1){
                                $pic_id = create_guid();
                                $uploadFile = new UploadFile();
                                $uploadFile->duplicate_file('origin/'.$bean->picture,'FaceID/RegisterImage/' .$pic_id);
                                $bean->frc_face_id_image = $pic_id;
                                $bean->frc_device_id_list .= ($index == 0) ? '^'.$data['att_id'].'^' : ',^'.$data['att_id'].'^';
                                $index++;
                            }
                        }
                        if ($bean->fetched_row['frc_face_id_image'] != "" && $bean->frc_face_id_image == "") {
                            $url = 'http://cam-api.bsmart.city/v2/cam/person/' . $data['device_id'] . '/' . $bean->id;
                            DeleteFaceID($url,$data['secret_key'], $data['att_id']);
                            $bean->frc_device_id_list = '0';
                        }
                    }
                }
            }
        }
    }
    public function isConnectToDevice($device_list, $device_id){
        return in_array($device_id, unencodeMultienum($device_list));
    }
    public function HandleRetrieveFaceID(&$bean){
        global $dotb_config;
        if($dotb_config['enable_face_id']) {
            $not_connect = $GLOBALS['app_list_strings']['status_faceid']['LBL_NOT_CONNECT_FACEID'];
            $connect = $GLOBALS['app_list_strings']['status_faceid']['LBL_CONNECT_FACEID'];
            if (empty($bean->frc_device_id_list) || $bean->frc_device_id_list == '0') {
                $bean->status_face_id = $not_connect;
            } else {
                $array_device_id = unencodeMultienum($bean->frc_device_id_list);
                $bean->status_face_id = '<div>';
                foreach ($array_device_id as $data) {
                    $q1 = "SELECT IFNULL(l1.device_id, '') device_id, IFNULL(teams.name, '') team_name FROM j_attdevice l1 INNER JOIN teams ON teams.id = l1.team_id WHERE l1.id = '{$data}' and l1.deleted = 0";
                    $result = $GLOBALS['db']->fetchOne($q1);
                    $bean->status_face_id .= '<span>' . $connect . ' ' . '<a href="#J_AttDevice/' . $data . '">' . $result['device_id'] . '</a> - ' . $result['team_name'] . '</span><br>';
                }
                $bean->status_face_id .= '</div>';
            }
        }
    }

}