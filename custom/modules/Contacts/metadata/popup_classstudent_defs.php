<?php
$popupMeta = array (
    'moduleMain' => 'Contact',
    'varName' => 'CONTACT',
    'orderBy' => 'contacts.first_name, contacts.last_name',

    //Custom filter - chỉ lọc các học viên trong lớp này
    'whereStatement' => "contacts.id IN ('{$_REQUEST['advanced_filter_class_id']}')",

    'whereClauses' => array (
        'contact_id' => 'contacts.contact_id',
        'full_student_name' => 'contacts.full_student_name',
        'phone_mobile' => 'contacts.phone_mobile',
        'guardian_name' => 'contacts.guardian_name',
        'email' => 'contacts.email',
        'date_entered' => 'contacts.date_entered',
        'assigned_user_id' => 'contacts.assigned_user_id',
        'team_name' => 'contacts.team_name',
        'grade' => 'contacts.grade',
        'prefer_level' => 'contacts.prefer_level',
    ),
    'searchInputs' => array (
        3 => 'email',
        4 => 'contact_id',
        5 => 'full_student_name',
        6 => 'phone_mobile',
        8 => 'guardian_name',
        10 => 'date_entered',
        11 => 'assigned_user_id',
        12 => 'team_name',
        14 => 'grade',
        15 => 'prefer_level',
    ),
    'create' => array (
    ),
    'searchdefs' => array (
        'contact_id' =>
        array (
            'type' => 'varchar',
            'label' => 'LBL_CONTACT_ID',
            'width' => '10',
            'name' => 'contact_id',
        ),
        'full_student_name' =>
        array (
            'type' => 'varchar',
            'label' => 'LBL_FULL_CONTACT_NAME',
            'width' => '10',
            'name' => 'full_student_name',
        ),
        'phone_mobile' =>
        array (
            'type' => 'phone',
            'label' => 'LBL_MOBILE_PHONE',
            'width' => '10',
            'name' => 'phone_mobile',
        ),
        'guardian_name' =>
        array (
            'type' => 'varchar',
            'label' => 'LBL_GUARDIAN_NAME',
            'width' => '10',
            'name' => 'guardian_name',
        ),
        'grade' =>
        array (
            'type' => 'varchar',
            'studio' => 'visible',
            'label' => 'LBL_GRADE',
            'width' => 10,
            'name' => 'grade',
        ),
        'prefer_level' =>
        array (
            'type' => 'enum',
            'label' => 'LBL_PREFER_LEVEL',
            'width' => 10,
            'name' => 'prefer_level',
        ),
        'email' =>
        array (
            'name' => 'email',
            'width' => '10',
        ),
        'date_entered' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_ENTERED',
            'width' => '10',
            'name' => 'date_entered',
        ),
        'assigned_user_id' =>
        array (
            'name' => 'assigned_user_id',
            'type' => 'enum',
            'label' => 'LBL_ASSIGNED_TO',
            'function' =>
            array (
                'name' => 'get_user_array',
                'params' =>
                array (
                    0 => false,
                ),
            ),
            'width' => '10',
        ),
        'team_name' =>
        array (
            'type' => 'relate',
            'link' => true,
            'studio' =>
            array (
                'portallistview' => false,
                'portalrecordview' => false,
            ),
            'label' => 'LBL_TEAMS',
            'id' => 'TEAM_ID',
            'width' => '10',
            'name' => 'team_name',
        ),
    ),
    'listviewdefs' => array (
        'CONTACT_ID' =>
        array (
            'type' => 'varchar',
            'default' => true,
            'label' => 'LBL_CONTACT_ID',
            'width' => 10,
        ),
        'NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_LIST_NAME',
            'link' => true,
            'default' => true,
            'related_fields' =>
            array (
                0 => 'first_name',
                1 => 'last_name',
                2 => 'salutation',
                3 => 'account_name',
                4 => 'account_id',
            ),
            'name' => 'name',
        ),
        'GUARDIAN_NAME' =>
        array (
            'type' => 'varchar',
            'label' => 'LBL_GUARDIAN_NAME',
            'width' => 10,
            'default' => true,
        ),
        'STATUS' =>
        array (
            'type' => 'enum',
            'default' => true,
            'label' => 'LBL_STATUS',
            'width' => 10,
        ),
        'BIRTHDATE' =>
        array (
            'type' => 'date',
            'label' => 'LBL_BIRTHDATE',
            'width' => 10,
            'default' => true,
        ),
        'PHONE_MOBILE' =>
        array (
            'type' => 'phone',
            'label' => 'LBL_MOBILE_PHONE',
            'width' => 7,
            'default' => true,
        ),
        'LEAD_SOURCE' =>
        array (
            'width' => 10,
            'label' => 'LBL_LEAD_SOURCE',
            'default' => true,
            'name' => 'lead_source',
        ),
        'ASSIGNED_USER_NAME' =>
        array (
            'link' => true,
            'type' => 'relate',
            'label' => 'LBL_ASSIGNED_TO',
            'id' => 'ASSIGNED_USER_ID',
            'width' => 10,
            'default' => true,
        ),
        'TEAM_NAME' =>
        array (
            'type' => 'relate',
            'link' => true,
            'studio' =>
            array (
                'portallistview' => false,
                'portalrecordview' => false,
            ),
            'label' => 'LBL_TEAMS',
            'id' => 'TEAM_ID',
            'width' => 10,
            'default' => true,
        ),
        'DATE_ENTERED' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_ENTERED',
            'width' => 10,
            'default' => true,
        ),
    ),
);
//Custom giữ lại filter
if(!empty($_REQUEST['advanced_filter_class_id']))
$popupMeta['customInput']['advanced_filter_class_id'] =  $_REQUEST['advanced_filter_class_id'];
else unset($popupMeta['whereStatement']);
//filter meta_data
$popupMeta['customInput']['metadata'] =  $_REQUEST['advanced_filter_class_id'];

