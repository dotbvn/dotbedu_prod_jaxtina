<?php
// created: 2023-10-16 15:35:01
$viewdefs['Contacts']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'full_student_name' => 
    array (
    ),
    'contact_id' => 
    array (
    ),
    'birthdate' => 
    array (
    ),
    'birth_day' => 
    array (
    ),
    'birth_month' => 
    array (
    ),
    'age' => 
    array (
    ),
    'nick_name' => 
    array (
    ),
    'gender' => 
    array (
    ),
    'grade' => 
    array (
    ),
    'guardian_name' => 
    array (
    ),
    'frc_device_id_list' => 
    array (
    ),
    'relationship' => 
    array (
    ),
    'guardian_name_2' => 
    array (
    ),
    'primary_address_street' => 
    array (
    ),
    'primary_address_postalcode' => 
    array (
    ),
    'primary_address_city' => 
    array (
    ),
    'primary_address_state' => 
    array (
    ),
    'primary_address_country' => 
    array (
    ),
    'phone' => 
    array (
      'dbFields' => 
      array (
        0 => 'phone_mobile',
        1 => 'phone_guardian',
        2 => 'other_mobile',
      ),
      'type' => 'phone',
      'vname' => 'LBL_PHONE',
    ),
    'phone_mobile' => 
    array (
      'type' => 'varchar',
      'vname' => 'LBL_PHONE_MOBILE',
    ),
    'prefer_level' => 
    array (
    ),
    'status' => 
    array (
    ),
    'payment_process' => 
    array (
    ),
    'lead_source' => 
    array (
    ),
    'channel' => 
    array (
    ),
    'description' => 
    array (
    ),
    'source_description' => 
    array (
    ),
    'utm_source' => 
    array (
    ),
    'utm_medium' => 
    array (
    ),
    'utm_content' => 
    array (
    ),
    'utm_agent' => 
    array (
    ),
    'campaign_name' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'date_performed' => 
    array (
    ),
    'performed_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'last_call_status' => 
    array (
    ),
    'last_call_date' => 
    array (
    ),
    'last_call_result' => 
    array (
    ),
    'lms_user_id' => 
    array (
    ),
    'lms_user_name' => 
    array (
    ),
    'lms_active' => 
    array (
    ),
    'is_converted_from_lead' => 
    array (
    ),
    'remain_amount' => 
    array (
    ),
    'unpaid_amount' => 
    array (
    ),
    'paid_amount' => 
    array (
    ),
    'min_start_study' => 
    array (
    ),
    'max_end_study' => 
    array (
    ),
    'count_payment' => 
    array (
    ),
    'current_class_name' => 
    array (
    ),
    'ost_hours' => 
    array (
    ),
    'enrolled_hours' => 
    array (
    ),
    'enrolled_amount' => 
    array (
    ),
    'remain_carry' => 
    array (
    ),
    'portal_active' => 
    array (
    ),
  ),
  'quicksearch_field' => 
  array (
    0 => 'contact_id',
    1 => 'full_student_name',
    2 => 'email1',
    3 => 'phone_mobile',
    4 => 'phone_guardian',
    5 => 'other_mobile',
    6 => 'nick_name',
    7 => 'assistant',
  ),
  'quicksearch_priority' => 2,
);