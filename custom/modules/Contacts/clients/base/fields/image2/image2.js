
({
    extendsFrom: 'BaseImageField',
    plugins: ['File', 'FieldDuplicate'],


    initialize: function(options) {
        this._super('initialize', [options]);
        this.def.width = 100;
        this.def.height = 100;
    },
    /**
     * @override
     * @param value
     * @return value
     */
    format: function (value) {
        if(value && this.name === 'frc_face_id_image' && !_.isEmpty(this.model.attributes.frc_face_id_image)){
            var str = window.location.pathname
            if (str.slice(-1) === '/') {
                str = str.slice(0, -1);
            }
            var site = window.location.origin + str
            value = site + '/upload/FaceID/RegisterImage/' + this.model.attributes.frc_face_id_image;
        }
        return value;
    },
    _render: function () {
        this._super("_render");
        if (!_.isEmpty(this.value)) {
            this.$('.image_field').addClass('image_rounded');
        }
        if(this.name === 'frc_face_id_image'){
            this.$el.css('max-height', '100px');
        }
        return this;
    },

})
