/**
 * EventStatusField is a field for Meetings/Calls that show the status field of the model as a badge field.
 *
 * FIXME: This component will be moved out of clients/base folder as part of MAR-2274 and SC-3593
 *
 * @class View.Fields.Base.EventStatusField
 * @alias DOTB.App.view.fields.BaseEventStatusField
 * @extends View.Fields.Base.BadgeSelectField
 */
({
    extendsFrom: 'BadgeSelectField',

    /**
     * @inheritdoc
     */
    initialize: function (options) {
        this._super('initialize', [options]);

        /**
         * An object where its keys map to specific status and color to matching
         * CSS classes.
         */
        this.statusClasses = {
            //Tuan Anh custom color label base on label
			'In Progress': 'textbg_blue',
            'Converted': 'textbg_yellow',
            'Waiting for class': 'textbg_green',
            'Ready to class': 'textbg_bluelight',
            'Demo': 'textbg_bluelight',
            'Deposit': 'textbg_bluelight',
            'Registered': 'textbg_bluelight',
            'Delayed': 'textbg_blood',
            'Finished': 'textbg_crimson',
            'OutStanding': 'textbg_orange',
            'Stopped': 'textbg_black',
            'Planning': 'textbg_bluelight',
            'Observation': 'textbg_yellow',
			'To pay': 'textbg_yellow',
            //end
        };

        this.type = 'badge-select';
    },

    /**
     * @inheritdoc
     */
    _loadTemplate: function () {
        var action = this.action || this.view.action;

        if (action == "disabled" && this.def.name === "status")
            this.action = "visible";


        if (action === 'edit') {
            this.type = 'enum';
        }

        this._super('_loadTemplate');
        this.type = 'badge-select';
    }
})
