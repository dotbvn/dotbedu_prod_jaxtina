<?php

$viewdefs['Contacts']['base']['view']['subpanel-for-prospectlists'] = array(
    'type' => 'subpanel-list',
    'panels' =>
        array(
            array(
                'name' => 'panel_header',
                'label' => 'LBL_PANEL_1',
                'fields' => array(
                    array (
                        'name' => 'contact_id',
                        'label' => 'LBL_CONTACT_ID',
                        'enabled' => true,
                        'default' => true,
                    ),
                    array(
                        'name' => 'name',
                        'label' => 'LBL_NAME',
                        'enabled' => true,
                        'default' => true,
                        'link' => true,
                    ),
                    array (
                        'name' => 'status',
                        'label' => 'LBL_STATUS',
                        'type' => 'event-status',
                        'link' => false,
                        'default' => true,
                        'enabled' => true,
                        'css_class' => 'full-width',
                    ),
                    array (
                        'name' => 'phone_mobile',
                        'label' => 'LBL_MOBILE_PHONE',
                        'enabled' => true,
                        'default' => true,
                    ),
                    array (
                        'name' => 'email',
                        'label' => 'LBL_LIST_EMAIL_ADDRESS',
                        'enabled' => true,
                        'default' => true,
                    ),
                ),
            ),
        ),
    'rowactions' =>
        array (
            'actions' => array(
                1 => array(
                    'type' => 'unlink-action',
                    'icon' => 'fa-chain-broken',
                    'tooltip' => 'LBL_UNLINK_BUTTON',
                ),
            ),
        ),
);
