<?php
$viewdefs['Contacts'] =
    array(
        'base' =>
            array(
                'view' =>
                    array(
                        'dupecheck-list' =>
                            array(
                                'panels' =>
                                    array(

                                        array(
                                            'label' => 'LBL_PANEL_1',
                                            'fields' =>
                                                array(

                                                    array(
                                                        'name' => 'name',
                                                        'type' => 'fullname',
                                                        'fields' =>
                                                            array(
                                                                0 => 'salutation',
                                                                1 => 'first_name',
                                                                2 => 'last_name',
                                                            ),
                                                        'link' => true,
                                                        'label' => 'LBL_LIST_NAME',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'birthdate',
                                                        'label' => 'LBL_BIRTHDATE',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'phone_mobile',
                                                        'label' => 'LBL_MOBILE_PHONE',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'assigned_user_name',
                                                        'label' => 'LBL_LIST_ASSIGNED_USER',
                                                        'id' => 'ASSIGNED_USER_ID',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'team_name',
                                                        'label' => 'LBL_TEAMS',
                                                        'enabled' => true,
                                                        'id' => 'TEAM_ID',
                                                        'link' => true,
                                                        'sortable' => false,
                                                        'default' => true,
                                                    ),
                                                ),
                                        ),
                                    ),
                            ),
                    ),
            ),
    );
