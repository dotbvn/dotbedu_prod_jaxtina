<?php
// created: 2021-03-05 16:48:23
$viewdefs['Contacts']['base']['view']['subpanel-for-c_news-c_news_contacts_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'contact_id',
          'label' => 'LBL_CONTACT_ID',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'name',
          'type' => 'fullname',
          'fields' => 
          array (
            0 => 'salutation',
            1 => 'first_name',
            2 => 'last_name',
          ),
          'link' => true,
          'label' => 'LBL_LIST_NAME',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'type' => 'event-status',
          'link' => false,
          'default' => true,
          'enabled' => true,
          'css_class' => 'full-width',
        ),
        3 => 
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_MOBILE_PHONE',
          'enabled' => true,
          'default' => true,
          'width' => 'large',
        ),
        4 => 
        array (
          'name' => 'email',
          'enabled' => true,
          'default' => true,
        ),
          5 =>
              array (
                  'name' => 'view_news',
                  'enabled' => true,
                  'default' => true,
              ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);