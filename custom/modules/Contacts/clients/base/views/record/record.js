({
    extendsFrom: 'RecordView',
    timeCompare: 259200000,
    initialize: function (options) {
        this._super('initialize', [options]);

        this.events = _.extend({}, this.events, {
            'click [name=moving_center]': 'movingCenter',
            'click [name=transfer_fee]': 'transferFee',
            'click [name=refund]': 'refund',
            'click [name=export_member_card]': 'exportMemberCard',
			 //Custom Jaxtina
            'click [name=reset_lms_password]': 'resetLmsPassword',
        });
    },
    /**
     * Added by HP to redirect to bwc link
     * @param event
     */
    movingCenter: function (event) {
        var id = app.controller.context.attributes.modelId;
        window.location.replace('#bwc/index.php?module=J_Payment&action=EditView&return_module=J_Payment&return_action=DetailView&payment_type=Moving%20Out&student_id=' + id);
    },

    /**
     * Added by HP to redirect to bwc link
     * @param event
     */
    transferFee: function (event) {
        var id = app.controller.context.attributes.modelId;
        window.location.replace('#bwc/index.php?module=J_Payment&action=EditView&return_module=J_Payment&return_action=DetailView&payment_type=Transfer%20Out&student_id=' + id);
    },

    /**
     * Added by HP to redirect to bwc link
     * @param event
     */
    refund: function (event) {
        var id = app.controller.context.attributes.modelId;
        window.location.replace('#bwc/index.php?module=J_Payment&action=EditView&return_module=J_Payment&return_action=DetailView&payment_type=Refund&student_id=' + id);
    },

    exportMemberCard: function (event) {
        var id = app.controller.context.attributes.modelId;
        $.ajax({
            url: "index.php?module=Contacts&action=handleAjaxContact&dotb_body_only=true",
            type: "POST",
            async: true,
            data: {
                type: 'ajaxGetStudentCard',
                membership_id: id,
            },
            dataType: "json",
            success: function (res) {
                $.confirm({
                    boxWidth: '35%',
                    title: app.lang.get('LBL_STUDENT_CARD_TIT', 'Contacts'),
                    content: res.body,
                    buttons: {
                        "Export to PDF": {
                            text  : app.lang.get('LBL_EXPORT_PDF', 'Contacts'),
                            btnClass: 'btn-blue',
                            action: function () {
                                window.location.replace('index.php?module=Contacts&action=downloadStudentCard&id=' + id);
                            }
                        },
                        "Cancel": {
                            text  : app.lang.get('LBL_CANCEL_BUTTON_LABEL'),
                            action: function () {

                            }
                        },
                    }
                });
            },
        });
    },
	
	//Customize Jaxtina
    resetLmsPassword: function (event) {
        var id = app.controller.context.attributes.modelId;
        $.ajax({
            url: "index.php?module=Contacts&action=handleAjaxContact&dotb_body_only=true",
            type: "POST",
            async: true,
            data: {
                type: 'resetLmsPassword',
                student_id: id,
            },
            dataType: "json",
            success: function (res) {
                if (res.success) {
                    App.alert.show('saved', {
                        level: 'success',
                        messages: res.message,
                        autoClose: true
                    });
                } else {
                    App.alert.show('errored', {
                        level: 'error',
                        messages: res.message,
                        autoClose: false
                    });
                }
            },
        });
    },

});
