<?php
$viewdefs['Contacts'] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'picture',
                'label' => 'LBL_PICTURE_FILE',
                'enabled' => true,
                'sortable' => false,
                'width' => '42',
                'default' => true,
              ),
              1 =>
              array (
                'name' => 'contact_id',
                'label' => 'LBL_CONTACT_ID',
                'enabled' => true,
                'default' => true,
              ),
              2 =>
              array (
                'name' => 'name',
                'type' => 'fullname',
                'fields' =>
                array (
                  0 => 'salutation',
                  1 => 'first_name',
                  2 => 'last_name',
                ),
                'link' => true,
                'label' => 'LBL_LIST_NAME',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
              ),
              3 =>
              array (
                'name' => 'phone_mobile',
                'label' => 'LBL_MOBILE_PHONE',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
              ),
              4 =>
              array (
                'name' => 'class_activity',
                'label' => 'LBL_CLASS_ACTIVITY',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
                'width' => 'xlarge',
              ),
              5 =>
              array (
                'name' => 'birthdate',
                'label' => 'LBL_BIRTHDATE',
                'enabled' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'email',
                'enabled' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'type' => 'event-status',
                'link' => false,
                'default' => true,
                'enabled' => true,
                'css_class' => 'full-width',
                'width' => '116',
              ),
              8 =>
              array (
                'name' => 'payment_process',
                'label' => 'LBL_PAYMENT_PROCESS',
                'enabled' => true,
                'default' => true,
              ),
              9 =>
              array (
                'name' => 'lead_source',
                'label' => 'LBL_LEAD_SOURCE',
                'enabled' => true,
                'default' => true,
              ),
              10 =>
              array (
                'name' => 'current_class_name',
                'label' => 'LBL_CURRENT_CLASS',
                'enabled' => true,
                'id' => 'CURRENT_CLASS_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
                'width' => 'large',
              ),
              11 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_LIST_ASSIGNED_USER',
                'id' => 'ASSIGNED_USER_ID',
                'enabled' => true,
                'default' => true,
              ),
              12 =>
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAMS',
                'enabled' => true,
                'id' => 'TEAM_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              13 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              14 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              15 =>
              array (
                'name' => 'channel',
                'label' => 'LBL_CHANNEL',
                'enabled' => true,
                'default' => false,
              ),
              16 =>
              array (
                'name' => 'school_name',
                'label' => 'LBL_SCHOOL_NAME',
                'enabled' => true,
                'id' => 'SCHOOL_ID',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              17 =>
              array (
                'name' => 'grade',
                'label' => 'LBL_GRADE',
                'enabled' => true,
                'default' => false,
              ),
              18 =>
              array (
                'name' => 'date_performed',
                'label' => 'LBL_DATE_PERFORMED',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
              19 =>
              array (
                'name' => 'performed_by_name',
                'label' => 'LBL_PERFORMED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'PERFORMED_USER_ID',
                'link' => true,
                'default' => false,
              ),
              20 =>
              array (
                'name' => 'nick_name',
                'label' => 'LBL_NICK_NAME',
                'enabled' => true,
                'default' => false,
              ),
              21 =>
              array (
                'name' => 'campaign_name',
                'label' => 'LBL_CAMPAIGN',
                'enabled' => true,
                'id' => 'CAMPAIGN_ID',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              22 =>
              array (
                'name' => 'utm_medium',
                'label' => 'LBL_UTM_MEDIUM',
                'enabled' => true,
                'default' => false,
              ),
              23 =>
              array (
                'name' => 'reference',
                'label' => 'LBL_REFERENCE',
                'enabled' => true,
                'default' => false,
              ),
              24 =>
              array (
                'name' => 'phone_guardian',
                'label' => 'LBL_PHONE_GUARDIAN',
                'enabled' => true,
                'default' => false,
              ),
              25 =>
              array (
                'name' => 'phone_other',
                'label' => 'LBL_OTHER_PHONE',
                'enabled' => true,
                'default' => false,
              ),
              26 =>
              array (
                'name' => 'facebook',
                'label' => 'LBL_FACEBOOK',
                'enabled' => true,
                'default' => false,
              ),
              27 =>
              array (
                'name' => 'guardian_name',
                'label' => 'LBL_GUARDIAN_NAME',
                'enabled' => true,
                'default' => false,
              ),
              28 =>
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => false,
              ),
              29 =>
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => false,
              ),
              30 =>
              array (
                'name' => 'eng_level',
                'label' => 'LBL_ENG_LEVEL',
                'enabled' => true,
                'default' => false,
              ),
              31 =>
              array (
                'name' => 'target',
                'label' => 'LBL_TARGET',
                'enabled' => true,
                'default' => false,
              ),
              32 =>
              array (
                'name' => 'object',
                'label' => 'LBL_OBJECT',
                'enabled' => true,
                'default' => false,
              ),
              33 =>
              array (
                'name' => 'object',
                'label' => 'LBL_OBJECT',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
