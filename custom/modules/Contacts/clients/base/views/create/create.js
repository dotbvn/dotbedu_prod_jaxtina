
({
    extendsFrom: 'CreateView',

    subscriptionIsOverflowing : false,

    message : '',
    /**
     * @inherit
     */
    initialize: function(options) {
        this._super('initialize', [options]);
        let self = this;
        app.api.call("read", app.api.buildURL('tenant/license/validate-student'), null, {
            success: function (response) {
                if (response['data']['valid'] === false) {
                    self.subscriptionIsOverflowing = true;
                    self.message = response['data']['message'];
                    app.alert.show('invalid', {
                        level: 'warning',
                        messages: self.message,
                        autoClose: false,
                    });
                }
            },
        });
    },

    /**
     * @override
     */
    save: function() {
        let self = this;
        if (this.subscriptionIsOverflowing) {
            app.alert.show('invalid', {
                level: 'confirmation',
                messages: self.message,
                autoClose: false,
                onConfirm: function () {
                    window.top.App.router.redirect('#bwc/index.php?module=EMS_Settings&action=subscription');
                },
                onCancel: function () {
                    window.top.App.router.redirect('#Contacts');
                }
            });
            return false;
        } else {
            this._super('save');
        }
    }
})