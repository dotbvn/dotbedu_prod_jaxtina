<?php
require_once 'custom/include/QRCode/phpqrcode.php';
switch ($_POST['type']) {
    case 'ajaxGetStudentCard':
        $result = ajaxGetStudentCard($_POST['membership_id']);
        echo $result;
        break;
    case 'resetLmsPassword':
        $result = resetLmsPassword($_POST['student_id']);
        echo $result;
        break;
}
function ajaxGetStudentCard($id = '')
{
    if (empty($id)) {
        return json_encode(array(
            "success" => "0",
            "message" => "error",
        ));
    }
    global $db;
    $q = "SELECT IFNULL(l1.id, '') contact_id
    , IFNULL(l1.picture, '') picture
    , IFNULL(l1.full_student_name, '') full_student_name
    , IFNULL(l1.date_issue, '') date_issue
    , IFNULL(l1.contact_id, '') member_id
    FROM contacts l1
    WHERE l1.deleted=0
    AND l1.id = '{$id}'";
    $row = $db->fetchOne($q);

    if (empty($row['picture'])) {
        $picture = 'custom/images/no_avata.png';
    } elseif (file_exists('upload/image3x4/' . $row['picture'])) {
        $picture = 'rest/v11_3/Contacts/' . $row['contact_id'] . '/file/picture?format=dotb-html-json&platform=base&_hash=/image3x4/' . $row['picture'];
    } else {
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');
        if (!is_dir('upload/image3x4')) {
            mkdir('upload/image3x4');
        }
        $pictureOri = 'upload/origin/' . $row['picture'];
        if (file_exists($pictureOri)) {
            $imgOri = new ImageHelper($pictureOri);
            $imgOri->resize(220, 293.3);
            $imgOri->save('upload/image3x4/' . $row['picture']);
            $picture = 'rest/v11_3/Contacts/' . $row['contact_id'] . '/file/picture?format=dotb-html-json&platform=base&_hash=/image3x4/' . $row['picture'];
        } else $picture = 'custom/images/no_avata.png';
    }

    $date_issue = explode("-", $row['date_issue']);
    $member_id = $row['member_id'];

    if (empty($row['member_id'])) {
        $qrContent = file_get_contents('custom/images/no_qrcode.png');
        $qr = base64_encode($qrContent);
    } else {
        QRcode::png($row['member_id'], dotb_cached('qrCode.png'));
        $qrContent = file_get_contents(dotb_cached('qrCode.png'));
        unlink(dotb_cached('qrCode.png'));
        $qr = base64_encode($qrContent);
    }

    $link_css = "custom/modules/Contacts/tpl/card.css";

    $smarty = new Dotb_Smarty();
    $smarty->assign('LINK_CSS', $link_css);
    $smarty->assign('PICTURE', $picture);
    $smarty->assign('STUDENT_NAME', $row['full_student_name']);
    $smarty->assign('DAY', $date_issue[2]);
    $smarty->assign('MONTH', $date_issue[1]);
    $smarty->assign('YEAR', $date_issue[0]);
    $smarty->assign('MEMBER_ID', $member_id);
    $smarty->assign('QRCODE', 'data:image/png;base64,' . $qr);
    $body = $smarty->fetch('custom/modules/Contacts/tpl/templateStudentCard.tpl');

    return json_encode(array(
        "success" => "1",
        "body" => $body,
    ));
}

//custom Jaxtina
function resetLmsPassword($id = '')
{
    $student = BeanFactory::getBean('Contacts', $id);
    if(!empty($student->lms_user_name)){
        require_once "custom/modules/Schedulers/callcenter.php";
        // goi api lay ket qua
        $data = array(
            'email' => $student->lms_user_name,
            'secretKey' => 'jsnjgndjhbh3bjeb894hbfbsu94389h4uhjdv9834ß'
        );
        $res = callApi('POST', 'https://app.jaxtina.com/api/user/reset-default-password', $data);
        $res_des = json_decode($res,true);
        if($res_des['error'] === 0){
            $student->lms_user_password = '-default PW-';
            $student->save();
            return json_encode(array(
                "success" => 1,
                "message" => "LMS password reset successfully.",
            ));
        }
    }
    return json_encode(array(
        "success" => 0,
        "message" => "LMS password reset failed. The student has not activated LMS.",
    ));
}



?>
