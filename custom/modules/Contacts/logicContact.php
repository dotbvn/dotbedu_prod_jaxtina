<?php

require_once ('include/externalAPI/ClassIn/utils.php');

class logicContact
{
    /*AFTER_SAVE*/
    public function addCode(&$bean, $event, $arguments){
        $code_field = 'contact_id';
        if (($bean->$code_field === "Auto generate") || empty($bean->$code_field)) {
            //AFTER_SAVE: Repeat 10 times to avoid duplicate codes
            for ($x = 0; $x < 10; $x++) {
                //Get Prefix
                $res = $GLOBALS['db']->query("SELECT teams.code_prefix code FROM teams WHERE id = '{$bean->team_id}'");
                $row = $GLOBALS['db']->fetchByAssoc($res);
                $prefix = $row['code'];
                $year = date('y', strtotime('+ 7hours' . (!empty($bean->date_entered) ? $bean->date_entered : $bean->fetched_row['date_entered'])));
                $table = $bean->table_name;
                $sep = '-';
                $first_pad = '00000';
                $padding = 5;
                $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, " . strlen($prefix . $year) . ") = '" . $prefix . $year . "') AND deleted = 0 ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";

                $result = $GLOBALS['db']->query($query);
                if ($row = $GLOBALS['db']->fetchByAssoc($result))
                    $last_code = $row[$code_field];
                else
                    //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $last_code = $prefix . $year . $sep . $first_pad;


                $num = substr($last_code, -$padding, $padding);
                $num++;
                $pads = $padding - strlen($num);
                $new_code = $prefix . $year . $sep;

                //preform the lead padding 0
                for ($i = 0; $i < $pads; $i++) $new_code .= "0";
                $new_code .= $num;

                //check duplicate code
                $countDup = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM $table WHERE $code_field = '$new_code' AND deleted = 0");
                if(empty($countDup)){
                    //write to database - Logic: Before Save
                    $GLOBALS['db']->query("UPDATE $table SET $code_field = '$new_code' WHERE id='{$bean->id}' AND deleted = 0");
                    break;
                }
            }
        }
    }

    public function saveFullName(&$bean, $event, $arguments){
        global $timedate, $dotb_config;
        //save full name
        $bean->last_name  = mb_convert_case(trim(preg_replace('/\s+/', ' ',str_replace(['ㅤ','ᅠ'], '',$bean->last_name))), MB_CASE_TITLE, "UTF-8");
        $bean->first_name  = mb_convert_case(trim(preg_replace('/\s+/', ' ',str_replace(['ㅤ','ᅠ'], '',$bean->first_name))), MB_CASE_TITLE, "UTF-8");
        $bean->full_student_name = trim($bean->last_name . ' ' . $bean->first_name);
        $bean->phone_mobile = formatPhoneNumber($bean->phone_mobile);
        $bean->phone_guardian = formatPhoneNumber($bean->phone_guardian);
        $bean->other_mobile = formatPhoneNumber($bean->other_mobile);
        //save age and update day-month filter
        if (!empty($bean->birthdate)) {
            $diff = date_diff(date_create($bean->birthdate), date_create(date("Y-m-d")));
            $bean->age = $diff->format('%y');
            $bean->birth_day = date('d', strtotime($bean->birthdate));
            $bean->birth_month = date('m', strtotime($bean->birthdate));
        }
        $bean->assistant = viToEn($bean->full_student_name);
        //handle add (Text)
        $bean->team_name_text = $GLOBALS['db']->getOne("SELECT name FROM teams WHERE id='{$bean->team_id}'");

        //Xu ly Import user_name
        if ($_POST['module'] == 'Import') {
            $user_id = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = '{$bean->assigned_user_name}' AND deleted = 0");
            if (!empty($user_id)) {
                $bean->assigned_user_id = $user_id;
            }
            if (!empty($bean->contact_id)) $bean->old_student_id = $bean->contact_id;
        }

        //Save Parent
        if (!empty($bean->phone_mobile)) {
            $sql = "SELECT id FROM c_contacts WHERE mobile_phone = '{$bean->phone_mobile}' AND mobile_phone <> '' AND mobile_phone IS NOT NULL AND deleted = 0";
            $parent_id = $GLOBALS['db']->getOne($sql);
            if (!empty($parent_id))
                $contacts = BeanFactory::getBean('C_Contacts', $parent_id);
            else
                $contacts = new C_Contacts();

            if (!empty($parent_id) && empty($bean->guardian_name)) $bean->guardian_name = $contacts->name;

            $contacts->name = $bean->guardian_name;
            $contacts->address = $bean->primary_address_street;
            $contacts->mobile_phone = $bean->phone_mobile;
            $contacts->email1 = $bean->email1;
            $contacts->team_id = '1';
            $contacts->team_set_id = '1';
            $contacts->assigned_user_id = $bean->assigned_user_id;
            if (!empty($contacts->name) && !empty($contacts->mobile_phone)) {
                $contacts->save();
            }

            //Mặc định luôn add Rel
            $bean->load_relationship('c_contacts_contacts_1');
            $bean->c_contacts_contacts_1->add($contacts->id);
        }

        //Generate Portal Name
        if($bean->portal_name != 'demo') $bean->portal_name = $bean->phone_mobile;
        if(empty($bean->portal_password) || empty($bean->fetched_row)){
            $password = $GLOBALS['db']->getOne("SELECT IFNULL(value, '') value FROM config WHERE name = 'defaultportalpwd' AND category = 'pwd'");
            if(empty($password)) $password = strtolower(str_replace(' ', '', $dotb_config['brand_name'])).'123';
            $bean->portal_password = User::getPasswordHash($password);
        }



        /*        //Create Voucher Code
        if(empty($bean->voucher_id)){
        for($i = 0; $i <= 10; $i++){
        $vouName    = strtoupper(create_guid_section(6));
        $countDup   = $GLOBALS['db']->getOne("SELECT count(id) FROM j_voucher WHERE name = '$vouName' AND deleted = 0");
        if($countDup == 0) break;
        }
        $vou                       = new J_Voucher();
        $vou->name                 = $vouName;
        $vou->discount_amount      = 0;
        $vou->discount_percent     = 0;
        $vou->amount_per_used      = 0;
        $vou->status               = 'Activated';
        $vou->foc_type             = 'Referral';
        $vou->use_time             = 'N';
        $vou->team_id              = '1';
        $vou->team_set_id          = '1';
        $vou->student_id           = $bean->id;
        $vou->start_date           = $timedate->nowDate();
        $vou->end_date             = '';
        $vou->description          = 'Mã giới thiệu bạn bè.';
        $vou->assigned_user_id     = $bean->assigned_user_id;
        $vou->save();
        $bean->voucher_id = $vou->id;
        } */
        //xử lí bàn giao
        if ($bean->fetched_row['assigned_user_id'] != $bean->assigned_user_id) {
            $bean->performed_user_id = $bean->modified_user_id;
            $bean->date_performed = $bean->date_modified;
        }

        //save reference
        if($bean->modified_by_name == 'apps_admin')   //apps_admin
            saveReference($bean);
    }

    public function addCustomerTapTap(&$bean, $event, $arguments)
    {
        //phone_mobile la unique, thay doi phone_mobile taptap se tao tai khoan moi
        if ($bean->fetched_row['phone_mobile'] != $bean->phone_mobile) {
            require_once "custom/include/_helper/connectTapTapApi.php";
            $tapTap = new TapTapHelper();
            $responeToken = $tapTap->requestToken();
            if ($responeToken['success']) {
                $result = $tapTap->createCustomer($bean);
                $external_connection->external_accumulation->TAPTAP = (object)$result;
                $bean->external_connection = json_encode($external_connection, JSON_UNESCAPED_UNICODE);
            }
        }
    }

    public function handleLMSUser(&$bean, $event, $arguments){
        //Student LMS user
        require_once('custom/include/utils/lms_helpers.php');
        //Enroll tạo tài khoản luôn
        if(empty($bean->lms_user_id) && !empty($bean->email1)) {
            $result = createUser($bean);
            if($result['status'] == 'success'){
                $bean->lms_user_id       = $result['user_id'];
                $bean->lms_user_name     = $result['user_name'];
                $bean->lms_user_password = $result['user_password'];
                $bean->lms_content       = json_encode($result['user']);
            }else $bean->lms_user_id = $bean->lms_user_password = $bean->lms_user_id = $bean->lms_content ='';
        }

        //Chỉnh sửa hồ sơ học viên
        if(!empty($bean->lms_user_id) && !empty($bean->fetched_row)){
            //Thay đổi Tên, số điện thoại
            if($bean->fetched_row['full_student_name'] != $bean->full_student_name)
                $result = updateUser($bean);

            //Thay đổi Email xóa tài khoản tạo lại -TESST
            if(trim(strtolower($bean->lms_user_name)) != trim(strtolower($bean->email1)) && !empty(trim(strtolower($bean->email1)))){
                $result = updateUserLogin($bean, 'unique_id');
                if($result['status'] == 'success'){
                    $bean->lms_content       = json_encode($result['user']);
                    $bean->lms_user_id       = $result['user_id'];
                    $bean->lms_user_name     = $result['user_name'];
                    if(!empty($result['user_password']))
                        $bean->lms_user_password = $result['user_password'];
                }
            }
        }


    }
    function beforeDeleteStudent(&$bean, $event, $arguments){
        $count_rel = $GLOBALS['db']->getOne("SELECT DISTINCT
            COUNT(IFNULL(j_payment.id, '')) count_rel
            FROM j_payment
            INNER JOIN contacts l1 ON l1.id = j_payment.parent_id AND j_payment.parent_type = 'Contacts' AND l1.deleted = 0
            WHERE (((l1.id = '{$bean->id}')))
            AND j_payment.deleted = 0");

        if ($count_rel > 0) {
            die('You can not delete this student!. Payments of this student still exists.');
        } else {
            //revert leads
            if ($bean->load_relationship('leads')) {
                $lead = $bean->leads->getBeans();
                $lead = reset($lead);
                if (!empty($lead)) {
                    $lead_id = $lead->id;
                    $lead->status = 'In Process';
                    $lead->converted = 0;
                    $lead->contact_id = '';
                    $lead->save();

                    //revert Contact --> Lead
                    //Update call
                    $GLOBALS['db']->query("UPDATE calls SET parent_type = 'Leads', parent_id = '{$lead_id}' WHERE parent_id = '{$bean->id}' AND deleted = 0");
                    //Add calls_leads
                    $GLOBALS['db']->query("INSERT INTO calls_leads (id, call_id, lead_id, required, accept_status, date_modified, deleted)
                        SELECT id, id, '{$lead_id}', 1, 'none', date_modified, deleted FROM calls WHERE parent_id = '{$lead_id}' AND deleted = 0;");
                    //Delete calls_contacts
                    $GLOBALS['db']->query("DELETE FROM calls_contacts WHERE contact_id = '{$bean->id}' AND deleted = 0");

                    //update session demo
                    $GLOBALS['db']->query("INSERT INTO meetings_leads (id, meeting_id, lead_id, required, accept_status, date_modified, deleted, situation_id)
                        SELECT UUID() id, mt.meeting_id, '{$lead_id}' lead_id, mt.required, mt.accept_status, mt.date_modified, mt.deleted, mt.situation_id FROM meetings_contacts mt
                        INNER JOIN j_studentsituations st ON mt.situation_id = st.id AND st.deleted = 0 AND st.type = 'Demo'
                        WHERE mt.contact_id = '{$bean->id}' AND mt.deleted = 0");

                    //update situation Demo
                    $GLOBALS['db']->query("UPDATE j_studentsituations SET student_type='Leads', student_id = '$lead_id' WHERE student_id = '{$bean->id}' AND deleted = 0 AND type = 'Demo'");
                    //update j_classstudents
                    $GLOBALS['db']->query("UPDATE j_classstudents SET student_type='Leads', student_id = '$lead_id' WHERE student_id = '{$bean->id}' AND deleted = 0");

                    $GLOBALS['db']->query("DELETE FROM meetings_leads WHERE lead_id ='{$bean->id}'");

                    //Copy Meeting
                    $GLOBALS['db']->query("UPDATE meetings SET parent_id='{$bean->id}', parent_type = 'Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0 AND meeting_type = 'Meeting'");

                    //Delete meetings_contacts
                    $GLOBALS['db']->query("DELETE FROM meetings_contacts WHERE contact_id = '{$bean->id}'");

                    //Update Task
                    $GLOBALS['db']->query("UPDATE tasks SET parent_type = 'Leads' , parent_id = '{$lead_id}' WHERE parent_id ='{$bean->id}' AND deleted = 0");

                    //update pt demo
                    $bean->load_relationship('j_ptresults');
                    $pt_id = $bean->j_ptresults->get();
                    for ($i = 0; $i < count($pt_id); $i++) {
                        $pt = BeanFactory::getBean('J_PTResult', $pt_id[$i]);
                        if ($pt->id) {
                            $pt->parent = "Leads";
                            $pt->student_id = $lead_id;
                            $pt->save();
                        }
                    }

                    //Siblings
                    $GLOBALS['db']->query("UPDATE c_siblings SET parent_id = '$lead_id', parent_type = 'Leads' WHERE parent_id = '{$bean->id}' AND deleted = 0");
                    $GLOBALS['db']->query("UPDATE c_siblings SET student_id = '$lead_id' WHERE student_id = '{$bean->id}' AND deleted = 0");

                    // Delete Customer Journey
                    $GLOBALS['db']->query("DELETE FROM dri_workflows WHERE contact_id = '{$bean->id}' AND deleted = 0");
                }

                $GLOBALS['db']->query("UPDATE leads SET converted = 0,contact_id = '', status='In Process' WHERE contact_id='{$bean->id}'");
                $GLOBALS['db']->query("UPDATE j_voucher SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE id='{$bean->voucher_id}'");
                //Remove notification Mobile
                require_once("custom/clients/mobile/helper/NotificationHelper.php");
                $notify = new NotificationMobile();
                $notify->deleteNotification($bean->id, 'Student');
            }

            //Delete all remaining situations
            $q1 = "SELECT DISTINCT IFNULL(jst.id, '') primaryid
            FROM j_studentsituations jst
            INNER JOIN contacts l1 ON jst.student_id = l1.id AND l1.deleted = 0 AND jst.student_type = 'Contacts'
            WHERE (l1.id = '{$bean->id}') AND jst.deleted = 0";
            $rows = $GLOBALS['db']->fetchArray($q1);
            foreach($rows as $_id => $row){
                $sitChange = BeanFactory::getBean('J_StudentSituations', $row['primaryid'], array('disable_row_level_security'=>true));
                $sitChange->mark_deleted($sitChange->id);
            }
        }
        //Delete LMS Account
        if(!empty($bean->lms_user_id)){
            require_once('custom/include/utils/lms_helpers.php');
            $result = deleteUser($bean->lms_user_id);
        }
        //        //Remove student from ClassIn School
        //        if (ExtAPIClassIn::hasAPIConfig() == true){
        //
        //        }
    }

    function listViewColor(&$bean, $event, $arguments){
        if (isset($_GET["action"]) && $_GET["action"] == "Popup") {
            $contactColor = '';
            switch ($bean->status) {
                case 'Waiting for class' :
                    $contactColor = 'textbg_green';
                    break;
                case 'In Progress' :
                    $contactColor = 'textbg_blue';
                    break;
                case 'Registered' :
                case 'Planning' :
                case 'Demo' :
                    $contactColor = 'textbg_bluelight';
                    break;
                case 'Delayed' :
                    $contactColor = 'textbg_blood';
                    break;
                case 'OutStanding' :
                    $contactColor = 'textbg_orange';
                    break;
                case 'Finished' :
                    $contactColor = 'textbg_crimson';
                    break;
                case 'Converted' :
                case 'Observation' :
                    $contactColor = 'textbg_yellow';
                    break;
                case 'Stopped' :
                case 'Cancelled' :
                    $contactColor = 'textbg_black';
                    break;
                default :
                    $contactColor = 'No_color';
            }
            $tmp_status = translate('contact_status_list', '', $bean->status);
            $bean->status = "<span class=$contactColor >$tmp_status</span>";
        }
        //get list last call, meeting, task
        //$bean->description = getlastAtivities($bean);
    }

    function handleAfterRetrieve(&$bean, $event, $arguments)
    {
        global $timedate;
        //HANDLE OVERVIEW
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[sizeof($arrayUrl) - 2];
        }
        if(isset($_REQUEST['view']) && $_REQUEST['view'] == 'record'
        && $url_module == $bean->module_name){
            //Remain, sum_paid, sum_unpaid
            $res = load_carry_forward($bean->id);
            $row = $res[$bean->id];
            //            if(!empty((float)$row['sum_unpaid']))
            //                $ost0 = '<tr>
            //                <td width="30%">'.translate('LBL_OV_UNPAID','Contacts').':</td>
            //                <td width="25%"><span style="font-weight:bold; color:#ff7700">'.format_number($row['sum_unpaid']).'</span></td>
            //                <td width="25%">'.translate('LBL_OV_HOURS','Contacts').':</td>
            //                <td width="20%"><span style="font-weight:bold; color:#ff7700">'.format_number($row['sum_unpaid_hours'],2,2).'</span></td>
            //                </tr>';

            if(!empty((float)$row['ost_hours']))
                $ost1 = '<tr><td colspan="3">'.translate('LBL_OV_OUTSTANDING_HOURS','Contacts').':</td>
                <td><span style="font-weight:bold; color:#555">'.format_number($row['ost_hours'],2,2).'</span></td>
                </tr>';

            if(!empty((float)$row['sum_ost_hours']))
                $ost2 = '<tr><td colspan="3">'.translate('LBL_OV_STUDIED_OST','Contacts').':</td>
                <td><span style="font-weight:bold; color:#555">'.(!empty($row['sum_ost_hours']) ? format_number($row['sum_ost_hours'],2,2) : '').'</span></td>
                </tr>';

            if(!empty((float)$row['drop_amount']))
                $ost4 = '<tr><td>'.translate('LBL_OV_DROPREVENUE','Contacts').':</td>
                <td><span style="font-weight:bold; color:#555">'.format_number($row['drop_amount']).'</span></td>
                <td>'.translate('LBL_OV_HOURS','Contacts').':</td>
                <td><span style="font-weight:bold; color:#555">'.format_number($row['drop_hours'],2,2).'</span></td>
                </tr>';

            if(!empty((float)$row['refund_amount']))
                $ost5 = '<tr><td>'.translate('LBL_OV_REFUND','Contacts').':</td>
                <td><span style="font-weight:bold; color:#555">'.format_number($row['refund_amount']).'</span></td>
                <td></td>
                <td></td>
                </tr>';

            if(!empty((float)$row['migrate_amount']))
                $img1 = '<img src="custom/images/import.png" style="width: 24px;" rel="tooltip" data-original-title="Data Import: '.format_number($row['migrate_amount']).' - hours: '.format_number($row['migrate_hours'],2,2).'">';

            if(!empty((float)$row['tf_out_amount']))
                $img2 = '<img src="custom/images/transfer-out.png" style="width: 24px;" rel="tooltip" data-original-title="Transfer Out: '.format_number($row['tf_out_amount']).' - '.format_number($row['tf_out_hours'],2,2).' hrs">';

            if(!empty((float)$row['tf_in_amount']))
                $img3 = '<img src="custom/images/transfer-in.png" style="width: 24px;" rel="tooltip" data-original-title="Transfer In: '.format_number($row['tf_in_amount']).' - : '.format_number($row['tf_in_hours'],2,2).' hrs">';




            //            //Carry
            //            $ost6 = '            <tr>
            //            <td>'.translate('LBL_OV_CARRY_AMOUNT','Contacts').':</td>
            //            <td><span style="font-weight:bold; color:#810955">'.format_number($row['carry_amount']).'</span></td>
            //            <td>'.translate('LBL_OV_HOURS','Contacts').':</td>
            //            <td><span style="font-weight:bold; color:#810955">'.format_number($row['carry_hours'],2,2).'</span></td>
            //            </tr>';

            //CUSTOM JAXTINA TOTAL LOYLATY
            $rowL = $GLOBALS['db']->fetchOne("SELECT SUM(j_loyalty.point) sum_point,
                ABS(SUM(CASE WHEN (j_loyalty.point < 0) THEN j_loyalty.point ELSE 0 END)) sum_used
                FROM j_loyalty WHERE (j_loyalty.student_id = '{$bean->id}') AND j_loyalty.deleted = 0");

            //Overview
            $ext_od .= $timedate->now().'
            <div style="margin-left: -40%;margin-top: 5px;position: absolute; z-index: 1000;">
            <table class="table"><tbody>
            <tr>
            <td width="30%">'.translate('LBL_OV_PAID','Contacts').':</td>
            <td width="30%"><span style="font-weight:bold; color:#468931">'.format_number($row['sum_paid']+$row['migrate_amount']+$row['tf_in_amount']-$row['tf_out_amount']).' '.$img1.''.$img2.''.$img3.'</span></td>
            <td width="20%">'.translate('LBL_OV_HOURS','Contacts').':</td>
            <td width="20%"><span style="font-weight:bold; color:#468931">'.format_number($row['sum_paid_hours']+$row['migrate_hours']+$row['tf_in_hours']-$row['tf_out_hours'],2,2).'</span></td>
            </tr>
            '.$ost0.'
            <tr>
            <td>'.translate('LBL_OV_ENROLLED','Contacts').':</td>
            <td><span style="font-weight:bold; color:#555">'.format_number($row['enrolled_amount']).'</span></td>
            <td>'.translate('LBL_OV_HOURS','Contacts').':</td>
            <td><span style="font-weight:bold; color:#555">'.format_number($row['enrolled_hours'],2,2).'</span></td>
            </tr>
            '.$ost1.'
            <tr>
            <td>'.translate('LBL_OV_REMAIN','Contacts').':</td>
            <td><span style="font-weight:bold; color:#115CAB">'.format_number($row['remain_amount']).'</span></td>
            <td>'.translate('LBL_OV_HOURS','Contacts').':</td>
            <td><span style="font-weight:bold; color:#115CAB">'.format_number($row['remain_hours'],2,2).'</span></td>
            </tr>

            <tr>
            <td>'.translate('LBL_OV_STUDIED_AMOUNT','Contacts').':</td>
            <td><span style="font-weight:bold; color:#555">'.format_number($row['sum_enrolled_amount']).'</span></td>
            <td>'.translate('LBL_OV_HOURS','Contacts').':</td>
            <td><span style="font-weight:bold; color:#555">'.format_number($row['sum_enrolled_hours'],2,2).'</span></td>
            </tr>
            '.$ost2.'
            '.$ost4.'
            '.$ost5.'
            '.$ost6.'
            <tr>
            <td>'.translate('LBL_TOTAL_LOYALTY','Contacts').':</td>
            <td><span style="font-weight:bold;">'.$rowL['sum_point'].'</span></td>
            <td>'.translate('LBL_TOTAL_USED_LOYALTY','Contacts').':</td>
            <td><span style="font-weight:bold;">'.$rowL['sum_used'].'</span></td>
            </tr>
            </tbody></table>
            </div>';
            $bean->overview = $ext_od;
        }



    }

    function handleAfterSave(&$bean, $event, $arguments){
        // Register ClassIn account
        if (!empty($bean->onl_uid)
        && ExtAPIClassIn::hasAPIConfig() == true
        && $arguments['isUpdate']){
            //Đã có đăng ký classin rồi thì SDT có được update sẽ đăng ký lại SDT mới
            $phone_mobile = !empty($arguments['dataChanges']['phone_mobile']) ? $arguments['dataChanges']['phone_mobile'] : '';
            $team_id = !empty($arguments['dataChanges']['team_id']) ? $arguments['dataChanges']['team_id'] : '';
            $full_student_name = !empty($arguments['dataChanges']['full_student_name']) ? $arguments['dataChanges']['full_student_name'] : '';

            //Thay đổi SDT
            if (!empty($phone_mobile) && $phone_mobile['before'] !== $phone_mobile['after']){
                //stored old classin uid
                $oldClassInUid = $bean->onl_uid;

                // create new ClassIn account with new phone mobile number
                $bean->classin_nickname = formatCINickName($bean);
                $r = registerClassInStudent($bean);

                // xử lý cập nhật thay đổi sdt của học sinh trong các course ClassIn
                // remove sdt cũ khỏi course, đồng thời thêm sdt mới vào course
                // remove sdt cũ khỏi các lesson đã tạo ClassIn, đồng thời thêm sdt mới vào các lesson đó
                if ($r){
                    //Lấy ClassIn uid mới bằng câu query
                    $newClassInUid = $GLOBALS['db']->getOne("SELECT onl_uid FROM contacts WHERE id = '{$bean->id}'");
                    $bean->classin_nickname = formatCINickName($bean);
                    updateStudentPhoneInClassIn($bean, $newClassInUid, $oldClassInUid);
                }

                return;
            }

            //Thay đổi team hoặc tên học sinh thì chỉ cần update nickname
            if (!empty($team_id) || !empty($full_student_name)){
                $nickname = formatCINickName($bean);
                editStudentName($bean->onl_uid, $nickname);
            }
        }
        //update Student Status
        updateStudentStatus($bean->id);
    }

    function contactActivity(&$bean, $event, $arguments){
        global $timedate;

        // kiểm tra list view có field class_activity không
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[sizeof($arrayUrl) - 1];
        }
        //optimize performance - only load when in list view
        if(isset($_REQUEST['view']) && $_REQUEST['view'] == 'list' && $url_module == $bean->module_name){
            $path_list = 'custom/modules/' . $bean->module_name . '/clients/base/views/list/list.php';
            require $path_list;
            $list_field = $viewdefs[$bean->module_name]['base']['view']['list']['panels'][0]['fields'];
            $cont = 0;
            foreach ($list_field as $value) {
                if ($value['name'] == 'class_activity' && $value['default']) {
                    $cont = 1;
                    break;
                }
            }
            if ($cont) {
                $ext_pt_demo = "UNION (SELECT
                CONCAT(DATE_FORMAT(convert_tz(l1.date_start,'+0:00','+7:00'), '%d/%m/%Y %h:%i %p'), ' - ', l1.name, ' - ', IFNULL(l.last_name, ''), ' ', IFNULL(l.first_name, '')) name,
                IFNULL(l1.id, '') id,
                CONCAT(
                'A:',j_ptresult.attended,' - ',
                'L:',j_ptresult.listening,' ',
                'S:',j_ptresult.speaking,' ',
                'R:',j_ptresult.reading,' ',
                'W:',j_ptresult.writing,' ',
                'S:',j_ptresult.score,' ',
                '- Result: ',j_ptresult.result_koc,' | ',
                IFNULL(uu.user_name, '')) description,
                IFNULL(uu.user_name, '') full_user_name,
                '' date_entered,
                '' missed,
                'pt_result' module
                FROM j_ptresult
                INNER JOIN meetings l1 ON j_ptresult.meeting_id = l1.id AND l1.deleted = 0
                LEFT JOIN users uu ON uu.id = j_ptresult.assigned_user_id AND uu.deleted = 0
                LEFT JOIN leads l ON j_ptresult.student_id = l.id AND l.deleted = 0
                WHERE j_ptresult.student_id = '{$bean->id}' AND j_ptresult.parent = '{$bean->table_name}' AND j_ptresult.deleted = 0
                ORDER BY date_start DESC)";

                if ($bean->module_name == 'Contacts') $ext_pt_demo = '';
                $query_ = "
                SELECT * FROM ((SELECT
                IFNULL(calls.name, '') name,
                IFNULL(calls.id, '') id,
                calls.description description,
                IFNULL(uu.user_name, '') full_user_name,
                calls.date_entered date_entered,
                IFNULL(calls.call_missed, 0) missed,
                'calls' module
                FROM calls
                LEFT JOIN users uu ON uu.id = calls.assigned_user_id AND uu.deleted = 0
                WHERE calls.parent_id = '{$bean->id}' AND calls.parent_type = '{$bean->table_name}' AND calls.deleted = 0
                ORDER BY date_entered DESC)
                UNION (SELECT
                IFNULL(tasks.name, '') name,
                IFNULL(tasks.id, '') id,
                tasks.description description,
                IFNULL(uu.user_name, '') full_user_name,
                tasks.date_entered date_entered,
                '' missed,
                'tasks' module
                FROM tasks
                LEFT JOIN users uu ON uu.id = tasks.assigned_user_id AND uu.deleted = 0
                WHERE tasks.parent_id = '{$bean->id}' AND tasks.parent_type = '{$bean->table_name}' AND tasks.deleted = 0
                ORDER BY date_entered DESC)
                $ext_pt_demo
                UNION (SELECT DISTINCT
                IFNULL(emails.name, '') name,
                IFNULL(emails.id, '') id,
                '' description,
                IFNULL(l2.user_name, '') full_user_name,
                l2.date_entered date_entered,
                '' missed,
                'emails' module
                FROM emails
                INNER JOIN emails_beans l1_1 ON emails.id = l1_1.email_id AND l1_1.deleted = 0 AND l1_1.bean_module = '{$bean->table_name}'
                INNER JOIN {$bean->table_name} l1 ON l1.id = l1_1.bean_id AND l1.deleted = 0
                LEFT JOIN users l2 ON emails.assigned_user_id = l2.id AND l2.deleted = 0
                WHERE (l1.id = '{$bean->id}') AND emails.status = 'sent' AND emails.deleted = 0)
                UNION (SELECT DISTINCT
                DATE_FORMAT(convert_tz(j_referencelog.date_modified,'+0:00','+7:00'), '%d/%m/%Y %h:%i %p') name,
                IFNULL(j_referencelog.id, '') id,
                CONCAT(IFNULL(l2.name, ''),' - ',
                IFNULL(j_referencelog.lead_source, ''),' ',
                IFNULL(j_referencelog.channel, '')) description,
                '' full_user_name,
                j_referencelog.date_entered date_entered,
                '' missed,
                'reference_log' module
                FROM j_referencelog
                LEFT JOIN campaigns l2 ON j_referencelog.campaign_id = l2.id AND l2.deleted = 0
                WHERE j_referencelog.parent_id = '{$bean->id}' AND j_referencelog.deleted = 0
                ORDER BY j_referencelog.date_modified DESC)) jjj
                ORDER BY jjj.date_entered DESC";

                $result_ = $GLOBALS['db']->fetchArray($query_);
                $arrayCol = array(
                    'reference_log' => array(
                        'module' => 'J_ReferenceLog',
                        'color' => '#31aaff',
                        'icon' => 'fa-link',
                    ),
                    'calls' => array(
                        'module' => 'Calls',
                        'color' => '#48c3cc',
                        'icon' => 'fa-phone-alt',
                    ),
                    'tasks' => array(
                        'module' => 'Tasks',
                        'color' => '#4bc076',
                        'icon' => 'fa-tasks',
                    ),
                    'pt_result' => array(
                        'module' => '#Meetings',
                        'color' => '#eb7092',
                        'icon' => 'fa-calendar-alt',
                    ),
                    'emails' => array(
                        'module' => 'Emails',
                        'color' => '#95aec5',
                        'icon' => 'fa-envelope',
                    ),
                );
                $last24h = date('Y-m-d H:i:s', strtotime("-24 hours " . $timedate->nowDb()));
                foreach ($arrayCol as $gr => $config) {
                    $arrayCol[$gr]['gr_img'] = "<i style='color: white; margin-right: 2px;' class='fa " . $config['icon'] . "'></i>";
                    foreach ($result_ as $key => $r) {
                        if ($r['module'] == $gr) {
                            if($r['missed']){
                                if($last24h <= $r['date_entered']) $arrayCol[$gr]['count_missed']++;
                                continue;
                            }
                            $arrayCol[$gr]['count']++;
                            $dateFormat = (!empty($r['date_entered'])) ? date('g:ia | j/n/y', strtotime("+7 hours " . $r['date_entered'])) : '';
                            if ($arrayCol[$gr]['count'] == 1) {
                                $span = (!empty($dateFormat)) ? "<span>" . $dateFormat . ' - ' . $r['full_user_name'] . "</span>" : '';
                                $title = (!empty($dateFormat)) ? "$dateFormat - {$r['name']}" : $r['name'];

                                $arrayCol[$gr]['title'] = "<a rel='tooltip' data-original-title='$title {$r['description']}' href='#" . $config['module'] . "/" . $r['id'] . "'>" . $r['name'] . "</a>
                                $span
                                <br><i>" . $r['description'] . "</i></p>";

                            } elseif ($arrayCol[$gr]['count'] <= 3) {
                                $arrayCol[$gr]['tooltip3'] .= $dateFormat . ' ' . $r['name'] . ': ' . $r['description'] . "\n";
                            } elseif ($arrayCol[$gr]['count'] == 4) {
                                $arrayCol[$gr]['tooltip3'] .= '...';
                            }
                        }
                    }
                }
                $bean->class_activity .= '<div style="padding-top: 4px;" class="last_comment">';
                foreach ($arrayCol as $gr => $config) {
                    if ($config['count'] > 0) {
                        $bean->class_activity .= '<p><span rel="tooltip" data-original-title="' . $config['tooltip3'] . '" style="background:' . $config['color'] . '" class="label">' . $config['gr_img'];
                        $bean->class_activity .= '<font style="vertical-align: inherit;">' . $config['count'] . '</font></span> ' . $config['title'];
                    }
                    //Set missed call count
                    if($config['count_missed'] > 0 && $gr == 'calls') $bean->class_activity .= '<i style="color: red;font-size: 15px;" class="fa fa-times"></i><b>'.$config['count_missed'].' '.translate('LBL_MISSED_CALL_ALERT','Calls').'</b><br><br>';
                }
                $bean->class_activity .= '</div>';
            }
        }
    }

    function getViewNews(&$bean, $event, $arguments){
        if ($_REQUEST['view'] == 'subpanel-for-c_news-c_news_contacts_1') {
            if ($bean->load_relationship('c_news_contacts_1')) {
                foreach ($bean->c_news_contacts_1->getBeans() as $relate) {
                    if (strpos($_REQUEST['__dotb_url'], $relate->id) != false) {
                        $sql = "SELECT count_read_news FROM c_news_contacts_1_c WHERE deleted=0 AND  c_news_contacts_1contacts_idb='{$bean->id}' AND  c_news_contacts_1c_news_ida='{$relate->id}'";
                        $result = $GLOBALS['db']->getOne($sql);
                        $bean->view_news = $result;
                        break;
                    }
                }
            }
        }
    }
    function handleAccessToken(&$bean, $event, $arguments){
        //Logic handle to remove access token when unactive student mobile app or change password
        if((!empty($bean->portal_password) && $bean->fetched_row['portal_password'] != $bean->portal_password) || $bean->portal_active == 0){
            $sqlToken = "DELETE FROM oauth_tokens WHERE contact_id = '{$bean->id}'";
            $GLOBALS['db']->query($sqlToken);
        }
    }

    function handleBfSave(&$bean, $event, $arguments){
        // Update stop date automatically when status is settled to Stopped
        if ($bean->fetched_row['status'] != 'Stopped' && $bean->status == 'Stopped') {
            $bean->stop_date = $GLOBALS['timedate']->nowDbDate();
        }
    }

    //Load License EMS Status
    function loadLicenseStatus(&$bean, $event, $arguments){
        //optimize performance - only load when in record view
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[sizeof($arrayUrl) - 2];
        }

        if(isset($_REQUEST['view'])
        && ($_REQUEST['view'] == 'record')
        && ($url_module == $bean->module_name)){
            global $timedate;
            if($bean->ems_active_state){
                $text_ems    = $GLOBALS['app_list_strings']['dom_cal_month_long'][date('n',strtotime($bean->ems_expired_date))].', '.date('Y',strtotime($bean->ems_expired_date)) .' # '. translate('LBL_EMS_ACTIVE','Contacts');
                $tooltip_ems = translate('LBL_EMS_ACTIVE_TEXT','Contacts').$timedate->to_display_date($bean->ems_expired_date)." (".translate('LBL_EMS_END_FIELD','Contacts')." ".mb_strtolower(str_replace('*','',translate('LBL_'.strtoupper($bean->ems_end_field),'Contacts'))." {$timedate->to_display_date($bean->ems_end_date,false)} +30 ".$GLOBALS['app_strings']['LBL_LOGIN_LOGIN_TIME_DAYS'],'UTF-8').")";
                $bean->ems_active_status =  "<span class='label-success label ellipsis_inline' rel='tooltip' data-placement='bottom' data-original-title='$tooltip_ems'>$text_ems</span>";
            }else{
                $bean->ems_expired_date = date('Y-m-d',strtotime("+1 days ".$bean->ems_expired_date));
                $text_ems    = $GLOBALS['app_list_strings']['dom_cal_month_long'][date('n',strtotime($bean->ems_expired_date))].', '.date('Y',strtotime($bean->ems_expired_date)) .' # '. translate('LBL_EMS_INACTIVE','Contacts');
                $tooltip_ems = translate('LBL_EMS_INACTIVE_TEXT','Contacts').$timedate->to_display_date($bean->ems_expired_date)." (".translate('LBL_EMS_END_FIELD','Contacts')." ".mb_strtolower(str_replace('*','',translate('LBL_'.strtoupper($bean->ems_end_field),'Contacts'))." {$timedate->to_display_date($bean->ems_end_date,false)} +30 ".$GLOBALS['app_strings']['LBL_LOGIN_LOGIN_TIME_DAYS'],'UTF-8').")";
                $bean->ems_active_status =  "<span class='label-important label ellipsis_inline' rel='tooltip' data-placement='bottom' data-original-title='$tooltip_ems'>$text_ems</span>";
            }
        }
    }
}
