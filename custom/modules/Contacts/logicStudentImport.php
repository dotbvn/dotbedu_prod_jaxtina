<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

    class logicStudentImport{
        //Import Payment
        function handleImport(&$bean, $event, $arguments){
//            global $timedate;
//            require_once("custom/include/_helper/junior_class_utils.php");
//
//            if($_POST['module'] == 'Import'){
//                //Check duplicate Student
//                $part_name = array_map('trim', explode('-', $bean->first_name));
//                if(count($part_name) > 1){
//                    $name = $part_name[0];
//                }else
//                    $name = $bean->first_name;
//
//                $pfname   =  self::split_name($name);
//                $bean->last_name  = trim($pfname[0]);
//                $bean->first_name = trim($pfname[1]);
//                $bean->full_student_name = trim($bean->last_name . ' ' . $bean->first_name);
//                $studenIds = $GLOBALS['db']->fetchArray("SELECT
//                    DISTINCT IFNULL(contacts.id,'') primaryid
//                    FROM contacts
//                    WHERE (((contacts.full_student_name = '{$bean->full_student_name}') AND (contacts.phone_mobile = '{$bean->phone_mobile}'))) AND  contacts.deleted=0 ");
//                if(count($studenIds) > 0){
//                    $studenId = $studenIds[0]['primaryid'];
//                    $bean->deleted = 1;
//                    // $GLOBALS['db']->query("UPDATE contacts SET deleted = 1 WHERE id='{$studenId}'");
//                }
//                else $studenId = $bean->id;
//                //$studenId = $bean->id;
//                $classes_name = explode('|', trim($bean->class_name));
//
//
//                if(!empty($classes_name) && !empty($bean->class_name)){
//
//                    //Create Payment
//                    $payment = BeanFactory::newBean('J_Payment');
//                    $payment->id = create_guid();
//                    $payment->new_with_id = true;
//                    $payment->tuition_fee          = $bean->payment_amount;
//                    $payment->amount_bef_discount  = $bean->payment_amount;
//                    $payment->total_after_discount = $bean->payment_amount;
//                    $payment->payment_amount       = $bean->payment_amount;
//
//
//
//                    $payment->payment_date         = $bean->payment_date;
//                    $payment->payment_expired      = $bean->payment_date;
//                    $payment->old_student_id       = 'import_date';
//
//                    $payment->sale_type            = '';
//                    $payment->sale_type_date       = $payment->payment_date;
//                    $payment->status               = 'Success';
//                    $payment->payment_type         = 'Enrollment';
//
//                    $payment->team_id          = $bean->team_id;
//                    $payment->team_set_id      = $bean->team_id;
//
//                    //Add TO Class
//
//                    $payment->parent_id = $studenId;
//                    $payment->parent_type = 'Contacts';
//
//                    //get class
//                    foreach($classes_name as $class_name){
//                        $class = new J_Class();
//                        $class->retrieve_by_string_fields(array('name' => trim($class_name)));
//                        if(!empty($class->id)){
//                            $class_hour += $class->hours;
//                            $kind_of_course = $class->kind_of_course;
//                            if(empty($start_date))
//                                $start_date = $class->start_date;
//                            elseif($start_date > $class->start_date) $start_date = $class->start_date;
//
//                            if(empty($end_date))
//                                $end_date = $class->end_date;
//                            elseif($end_date < $class->end_date) $end_date = $class->end_date;
//
//                            $arrCon[$class->id] = array(
//                                'class_id' => $class->id,
//                                'total_hour' => $class->hours,
//                                'start_study' => $start_date,
//                                'end_study' => $end_date,
//                            );
//
//                        }
//                    }
//                    $payment->kind_of_course= $kind_of_course;
//                    $payment->tuition_hours        = $class_hour;
//                    $payment->total_hours          = $payment->tuition_hours;
//
//
//
//                    //Create Receipt
//                    $pmd = BeanFactory::newBean('J_PaymentDetail');
//                    $pmd->payment_no        = 1;
//                    $pmd->name              = $payment->name."-1";
//                    $pmd->is_discount       = 1;
//                    $pmd->before_discount   = $payment->payment_amount;
//                    $pmd->discount_amount   = 0;
//                    $pmd->payment_method    = 'Other';
//                    $pmd->payment_date      = $bean->payment_date;
//                    $pmd->status            = 'Paid';
//                    $pmd->payment_amount    = $payment->payment_amount;
//
//                    $pmd->payment_id            = $payment->id;
//                    $pmd->student_id            = $studenId;
//                    $pmd->assigned_user_id      = $payment->assigned_user_id;
//                    $pmd->modified_user_id      = $payment->assigned_user_id;
//                    $pmd->created_by            = $payment->assigned_user_id;
//                    $pmd->date_entered          = $payment->date_entered;
//                    $pmd->date_modified         = $payment->date_modified;
//                    $pmd->team_id               = $payment->team_id;
//                    $pmd->team_set_id           = $payment->team_id;
//                    $pmd->save();
//
//
//
//                }
//
//
//            }
        }

        function split_name($name) {
            $name = trim($name);
            $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
            $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
            return array($first_name, $last_name);
        }

        public function handleBeforeImport(&$bean, $event, $arguments) {
            //save full name
            $bean->last_name  = mb_convert_case(trim(preg_replace('/\s+/', ' ',str_replace(['ㅤ','ᅠ'], '',$bean->last_name))), MB_CASE_TITLE, "UTF-8");
            $bean->first_name  = mb_convert_case(trim(preg_replace('/\s+/', ' ',str_replace(['ㅤ','ᅠ'], '',$bean->first_name))), MB_CASE_TITLE, "UTF-8");
            $bean->name = trim($bean->last_name . ' ' . $bean->first_name);
        }
    }

?>
