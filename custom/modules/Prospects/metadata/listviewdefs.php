<?php
$listViewDefs['Prospects'] =
array (
  'name' =>
  array (
    'width' => '13%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'orderBy' => 'name',
    'default' => true,
    'related_fields' =>
    array (
      0 => 'last_name',
      1 => 'first_name',
      2 => 'salutation',
    ),
  ),
  'birthdate' =>
  array (
    'type' => 'date',
    'label' => 'LBL_BIRTHDATE',
    'width' => '7%',
    'default' => true,
  ),
  'phone_mobile' =>
  array (
    'width' => '7%',
    'label' => 'LBL_MOBILE_PHONE',
    'default' => true,
  ),
  'description' =>
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '14%',
    'default' => true,
  ),
  'status' =>
  array (
    'width' => '7%',
    'label' => 'LBL_STATUS',
    'default' => true,
  ),
  'lead_source' =>
  array (
    'width' => '7%',
    'label' => 'LBL_LEAD_SOURCE',
    'default' => true,
  ),
  'campaign_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CAMPAIGN',
    'id' => 'CAMPAIGN_ID',
    'width' => '7%',
    'default' => true,
  ),
  'assigned_user_name' =>
  array (
    'width' => '8%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'date_entered' =>
  array (
    'width' => '8%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
    'orderBy' => 'date_entered',
    'sortOrder' => 'desc',
  ),
  'team_name' =>
  array (
    'width' => '7%',
    'label' => 'LBL_LIST_TEAM',
    'default' => true,
  ),
  'guardian_name' =>
  array (
    'type' => 'varchar',
    'label' => 'LBL_GUARDIAN_NAME',
    'width' => '7%',
    'default' => false,
  ),
  'phone_guardian' =>
  array (
    'type' => 'phone',
    'label' => 'LBL_PHONE_GUARDIAN',
    'width' => '7%',
    'default' => false,
  ),
  'guardian_name_2' =>
  array (
    'type' => 'varchar',
    'label' => 'LBL_GUARDIAN_NAME_2',
    'width' => '10%',
    'default' => false,
  ),
   'other_mobile' =>
  array (
    'type' => 'phone',
    'label' => 'LBL_OTHER_MOBILE',
    'width' => '7%',
    'default' => false,
  ),
  'gender' =>
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_GENDER',
    'width' => '10%',
  ),
  'created_by' =>
  array (
    'width' => '10%',
    'label' => 'LBL_CREATED',
    'default' => false,
  ),
);
