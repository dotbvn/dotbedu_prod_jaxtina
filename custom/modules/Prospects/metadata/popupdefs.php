<?php
$popupMeta = array (
    'moduleMain' => 'Prospect',
    'varName' => 'PROSPECT',
    'orderBy' => 'prospects.last_name, prospects.first_name',
    'whereClauses' => array (
  'first_name' => 'prospects.first_name',
  'last_name' => 'prospects.last_name',
),
    'searchInputs' => array (
  0 => 'first_name',
  1 => 'last_name',
),
    'create' => array (
  'formBase' => 'ProspectFormBase.php',
  'formBaseClass' => 'ProspectFormBase',
  'getFormBodyParams' => 
  array (
    0 => '',
    1 => '',
    2 => 'ProspectSave',
  ),
  'createButton' => 'LNK_NEW_PROSPECT',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'orderBy' => 'name',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'last_name',
      1 => 'first_name',
      2 => 'salutation',
    ),
    'name' => 'name',
  ),
  'GUARDIAN_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_GUARDIAN_NAME',
    'width' => 10,
    'default' => true,
    'name' => 'guardian_name',
  ),
  'PHONE_MOBILE' => 
  array (
    'width' => 10,
    'label' => 'LBL_MOBILE_PHONE',
    'default' => true,
    'name' => 'phone_mobile',
  ),
  'STATUS' => 
  array (
    'width' => 10,
    'label' => 'LBL_STATUS',
    'default' => true,
    'name' => 'status',
  ),
  'BIRTHDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BIRTHDATE',
    'width' => 10,
    'default' => true,
    'name' => 'birthdate',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'TEAM_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
  'DATE_ENTERED' => 
  array (
    'width' => 10,
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
    'orderBy' => 'date_entered',
    'sortOrder' => 'desc',
    'name' => 'date_entered',
  ),
),
);
