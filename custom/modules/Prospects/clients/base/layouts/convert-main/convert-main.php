<?php
// created: 2019-03-17 13:31:35
$viewdefs = array (
  'Prospects' =>
  array (
    'base' =>
    array (
      'layout' =>
      array (
        'convert-main' =>
        array (
          'modules' =>
          array (
            0 =>
            array (
              'copyData' => true,
              'required' => true,
              'moduleName' => 'Leads',
              'module' => 'Leads',
              'duplicateCheckOnStart' => true,
            ),
          ),
        ),
      ),
    ),
  ),
);