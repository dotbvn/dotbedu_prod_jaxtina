<?php
$viewdefs['Prospects'] =
    array(
        'base' =>
            array(
                'view' =>
                    array(
                        'dupecheck-list' =>
                            array(
                                'panels' =>
                                    array(

                                        array(
                                            'label' => 'LBL_PANEL_DEFAULT',
                                            'fields' =>
                                                array(

                                                    array(
                                                        'name' => 'name',
                                                        'type' => 'fullname',
                                                        'fields' =>
                                                            array(
                                                                0 => 'salutation',
                                                                1 => 'first_name',
                                                                2 => 'last_name',
                                                            ),
                                                        'link' => true,
                                                        'css_class' => 'full-name',
                                                        'label' => 'LBL_LIST_NAME',
                                                        'enabled' => true,
                                                        'default' => true,
                                                        'width' => 'large',
                                                    ),

                                                    array(
                                                        'name' => 'birthdate',
                                                        'label' => 'LBL_BIRTHDATE',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'phone_mobile',
                                                        'label' => 'LBL_MOBILE_PHONE',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'assigned_user_name',
                                                        'label' => 'LBL_ASSIGNED_TO',
                                                        'enabled' => true,
                                                        'id' => 'ASSIGNED_USER_ID',
                                                        'link' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'team_name',
                                                        'label' => 'LBL_TEAMS',
                                                        'enabled' => true,
                                                        'id' => 'TEAM_ID',
                                                        'link' => true,
                                                        'sortable' => false,
                                                        'default' => true,
                                                    ),

                                                ),
                                        ),
                                    ),
                            ),
                    ),
            ),
    );
