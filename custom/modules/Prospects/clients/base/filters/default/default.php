<?php
// created: 2021-11-21 10:18:05
$viewdefs['Prospects']['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' =>
        array(
            'full_target_name' =>
                array(),
            'birthdate' =>
                array(),
            'birth_day' =>
                array(),
            'birth_month' =>
                array(),
            'nick_name' =>
                array(),
            'gender' =>
                array(),
            'guardian_name' =>
                array(),
            'relationship' =>
                array(),
            'guardian_name_2' =>
                array(),
            'primary_address_street' =>
                array(),
            'primary_address_postalcode' =>
                array(),
            'primary_address_city' =>
                array(),
            'primary_address_state' =>
                array(),
            'primary_address_country' =>
                array(),
            'school_name' =>
                array(),
            'phone' =>
                array(
                    'dbFields' =>
                        array(
                            0 => 'phone_mobile',
                            1 => 'phone_guardian',
                            2 => 'other_mobile',
                        ),
                    'type' => 'phone',
                    'vname' => 'LBL_PHONE',
                ),
            'phone_mobile' => array(
                'type' => 'varchar',
                'vname' => 'LBL_PHONE_MOBILE',
            ),
            'email1' =>array (),
            'potential' =>
                array(),
            'status' =>
                array(),
            'lead_source' =>
                array(),
            'channel' =>
                array(),
            'description' =>
                array(),
            'source_description' =>
                array(),
            'utm_source' =>
                array(),
            'utm_medium' =>
                array(),
            'utm_content' =>
                array(),
            'utm_agent' =>
                array(),
            'campaign_name' =>
                array(),
            'team_name' =>
                array(),
            'assigned_user_name' =>
                array(),
            '$owner' =>
                array(
                    'predefined_filter' => true,
                    'vname' => 'LBL_CURRENT_USER_FILTER',
                ),
            '$favorite' =>
                array(
                    'predefined_filter' => true,
                    'vname' => 'LBL_FAVORITES_FILTER',
                ),
            'date_performed' =>
                array(),
            'performed_by_name' =>
                array(),
            'age' =>
                array(),
            'utm_agent' =>
                array(),
            'utm_content' =>
                array(),
            'date_entered' =>
                array(),
            'date_modified' =>
                array(),
            'modified_by_name' =>
                array(),
            'created_by_name' =>
                array(),
            'last_call_status' =>
                array(),
            'last_call_date' =>
                array(),
            'last_call_result' =>
                array(),
            'grade' =>
                array(),
            'eng_level' =>
                array(),
            'target' =>
                array(),
            'object' =>
                array(),
        ),
    'quicksearch_field' =>
        array(
            0 => 'full_target_name',
            1 => 'email1',
            2 => 'phone_mobile',
            3 => 'phone_guardian',
            4 => 'other_mobile',
            5 => 'assistant',
        ),
    'quicksearch_priority' => 2,
);
