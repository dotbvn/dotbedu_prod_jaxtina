<?php
/**
 * Add by Tuan Anh
 */
$hook_version = 1;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1, 'Save full name', 'custom/modules/Prospects/logicProspect.php', 'logicProspect', 'beforeSaveProspects');

$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(2, 'Contact Activity', 'custom/modules/Contacts/logicContact.php','logicContact', 'contactActivity');

$hook_array['before_import'] = Array();
$hook_array['before_import'][] = Array(1, 'Handle Before Import', 'custom/modules/Prospects/logicProspectImport.php','logicProspectImport', 'handleBeforeImport');