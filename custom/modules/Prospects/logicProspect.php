<?php
class logicProspect
{

    /**
    * @param $bean
    * @param $event
    * @param $arguments
    */
    public function beforeSaveProspects(&$bean, $event, $arguments)
    {
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $action = $arrayUrl[sizeof($arrayUrl) - 1];
            $module = $arrayUrl[sizeof($arrayUrl) - 2];
        }
        //save full name
        $bean->last_name  = mb_convert_case(trim(preg_replace('/\s+/', ' ',str_replace(['ㅤ','ᅠ'], '',$bean->last_name))), MB_CASE_TITLE, "UTF-8");
        $bean->first_name  = mb_convert_case(trim(preg_replace('/\s+/', ' ',str_replace(['ㅤ','ᅠ'], '',$bean->first_name))), MB_CASE_TITLE, "UTF-8");
        $bean->full_target_name = trim($bean->last_name . ' ' . $bean->first_name);
        $bean->phone_mobile   = formatPhoneNumber($bean->phone_mobile);
        $bean->phone_guardian   = formatPhoneNumber($bean->phone_guardian);
        $bean->other_mobile   = formatPhoneNumber($bean->other_mobile);
        //save age and update date&month filter
        if (!empty($bean->birthdate)) {
            $diff = date_diff(date_create($bean->birthdate), date_create(date("Y-m-d")));
            $bean->age = $diff->format('%y');
            $bean->birth_day = date('d', strtotime($bean->birthdate));
            $bean->birth_month = date('m', strtotime($bean->birthdate));
        }
        $bean->assistant = viToEn($bean->full_target_name);

        //Xu ly Import user_name
        if ($_POST['module'] == 'Import') {
            $user_id = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = '{$bean->assigned_user_name}' AND deleted = 0");
            if (!empty($user_id)){
                $bean->assigned_user_id = $user_id;
            }
        }

        //Auto-set status when duplicate
        if(empty($bean->status)
        || !in_array($bean->status, array_keys($GLOBALS['app_list_strings']['target_status_dom']))
        || (empty($bean->fetched_row) && $bean->status == 'Converted')){
            $bean->lead_id = '';
            $bean->status = 'New';
            $bean->converted = 0;
        }

        //Mass Update - Auto convert To Lead
        if ( $bean->converted ) {
            $lead = new Lead();
            foreach ($lead->field_defs as $keyField => $aFieldName)
                $lead->$keyField = $bean->$keyField;
            $lead->status = 'New';
            $lead->id = '';
            $lead->date_entered = '';
            $lead->converted = 0;

            //check duplicate
            $duplicates = $lead->findDuplicates();
            if(count($duplicates['records']) > 0)
                $bean->lead_id = $duplicates['records'][0]['id'];
            else{
                $lead->save();
                $bean->lead_id = $lead->id;
            }
        }

        // update converted
        if (!empty($bean->lead_id)) {
            $bean->status = 'Converted';
            $bean->converted = 1;

            //Copy Log Call
            $GLOBALS['db']->query("UPDATE calls SET parent_id = '{$bean->lead_id}', parent_type = 'Leads' WHERE parent_id = '{$bean->id}' AND deleted = 0");

            $GLOBALS['db']->query("INSERT INTO calls_leads (id, call_id, lead_id, required, accept_status, date_modified, deleted)
                SELECT id, id, '{$bean->lead_id}', 1, 'none', date_modified, deleted FROM calls WHERE parent_id = '{$bean->lead_id}' AND deleted = 0;");

            //Copy Task
            $GLOBALS['db']->query("UPDATE tasks SET parent_id = '{$bean->lead_id}', parent_type = 'Leads' WHERE parent_id = '{$bean->id}' AND deleted = 0");

            //Copy Meeting
            $GLOBALS['db']->query("UPDATE meetings SET parent_id = '{$bean->lead_id}', parent_type = 'Leads' WHERE parent_id = '{$bean->id}' AND deleted = 0 AND meeting_type = 'Meeting'");

            $GLOBALS['db']->query("INSERT INTO meetings_leads (id, meeting_id, lead_id, required, accept_status, date_modified, deleted)
                SELECT id, id, '{$bean->lead_id}', 1, 'none', date_modified, deleted FROM meetings WHERE parent_id = '{$bean->lead_id}' AND deleted = 0;");

            //Copy Reference
            $GLOBALS['db']->query("UPDATE j_referencelog SET parent_id = '{$bean->lead_id}', parent_type = 'Leads' WHERE parent_id = '{$bean->id}' AND deleted = 0");

            //Siblings
            $GLOBALS['db']->query("UPDATE c_siblings SET parent_id = '{$bean->lead_id}', parent_type = 'Leads' WHERE parent_id = '{$bean->id}' AND deleted = 0");
            $GLOBALS['db']->query("UPDATE c_siblings SET student_id = '{$bean->lead_id}' WHERE student_id = '{$bean->id}' AND deleted = 0");

        }

        //xử lí bàn giao
        if($bean->fetched_row['assigned_user_id'] != $bean->assigned_user_id){
            $bean->performed_user_id = $bean->modified_user_id;
            $bean->date_performed = $bean->date_modified;
        }

        if($bean->modified_by_name == 'apps_admin')   //apps_admin
            saveReference($bean);

    }
}
