<?php
switch ($_POST['type']) {
    case 'ajaxSendNotification':
        $result = ajaxSendNotification($_POST['new_id']);
        echo $result;
        break;
}

function ajaxSendNotification($new_id){
    $c_new = BeanFactory::getBean('C_News', $new_id);
    $c_new->send_notification = 1;
    $c_new->save();
    return json_encode(array(
        "success" => "1",
    ));
}