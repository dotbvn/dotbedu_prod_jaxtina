<?php
/**
 * Add by Tuan Anh
 */
$hook_version = 2;
$hook_array = Array();
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1, 'Notification News', 'custom/modules/C_News/logicNews.php', 'logicNews', 'after_save');

$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1, 'Handler before save', 'custom/modules/C_News/logicNews.php', 'logicNews', 'handleBeforeSave');

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(10, 'Delete Notification', 'custom/modules/C_News/logicNews.php', 'logicNews', 'deleteMobileNotification');

$hook_array['after_retrieve'] = Array();
$hook_array['after_retrieve'][] = Array(1, '', 'custom/modules/C_News/logicNews.php', 'logicNews', 'handleAfterRetrieve');