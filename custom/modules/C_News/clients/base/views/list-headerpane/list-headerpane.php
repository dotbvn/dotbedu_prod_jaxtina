<?php


$viewdefs['C_News']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_C_NEWS',
			'tooltip' => 'LNK_NEW_C_NEWS',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'C_News',
            'route'=>'#C_News/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_C_NEWS_REPORTS',
			'tooltip' => 'LNK_C_NEWS_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=C_News',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);