<?php
$module_name = 'C_News';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'label' => 'LBL_PICTURE_FILE',
                'enabled' => true,
                'sortable' => false,
                'width' => '60',
                'default' => true,
              ),
              1 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              2 => 
              array (
                'name' => 'url',
                'label' => 'LBL_URL',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'type',
                'label' => 'LBL_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              4 =>
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'type' => 'event-status',
                'enabled' => true,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'pin',
                'label' => 'LBL_PIN',
                'enabled' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'pin_teacher',
                'label' => 'LBL_PIN_TEACHER',
                'enabled' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'start_date',
                'label' => 'LBL_START_DATE',
                'enabled' => true,
                'default' => true,
              ),
              8 =>
              array (
                'name' => 'end_date',
                'label' => 'LBL_END_DATE',
                'enabled' => true,
                'default' => true,
              ),
              9 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              10 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              11 =>
              array (
                'name' => 'views',
                'label' => 'LBL_VIEWS',
                'enabled' => true,
                'default' => true,
              ),
              12 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              13 =>
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              14 =>
              array (
                'name' => 'tag',
                'label' => 'LBL_TAGS',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
