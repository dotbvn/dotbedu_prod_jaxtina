/**
 * @class View.Views.Base.CreateView
 * @alias DOTB.App.view.views.CreateView
 * @extends View.Views.Base.RecordView
 */
({
    extendsFrom: 'CreateView',
    initialize: function (options) {
        options.meta = _.extend({}, app.metadata.getView(null, 'create'), options.meta);
        this._super("initialize", [options]);
    },
    validateModelWaterfall: function (callback) {
        var fieldsToValidate = this.getFields(this.module, this.model);
        //Add by HoangHvy to check if content is empty then url is required
        if (this.validateEditorContent()) {
            fieldsToValidate['url']['required'] = true
        } else {
            fieldsToValidate['url']['required'] = false
        }
        this.model.doValidate(fieldsToValidate, function (isValid) {
            callback(!isValid);
        });
    },
    validateEditorContent: function (){
        const iframe = document.querySelector('iframe');
        const iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
        const bodyElement = iframeDocument.querySelector('#tinymce');
        return _.isEmpty(bodyElement.textContent)
    }
})
