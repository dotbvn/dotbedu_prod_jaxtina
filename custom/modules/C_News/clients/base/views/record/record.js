({
    extendsFrom: 'RecordView',

    initialize: function (options) {
        this._super('initialize', [options]);

        this.events = _.extend({}, this.events, {
            'click [name=send_notification]': 'sendNotification',
        });
    },

    sendNotification: function () {
        var id = app.controller.context.attributes.modelId;
        $.ajax({
            type: "POST",
            url: "index.php?module=C_News&action=handleAjaxNews&dotb_body_only=true",
            data:  {
                type            : 'ajaxSendNotification',
                new_id          : id,
            },  
            success: function(data){
                data = JSON.parse(data)
                if(data != null && data.success == '1'){
                    app.alert.show('send_notification_success', {
                        level: 'success',
                        autoClose: true,
                        messages: 'Sent Notification!'
                    }); 
                }else {
                    app.alert.show('send_notification_error', {
                        level: 'error',
                        autoClose: true,
                        messages: 'Send Notification Error!'
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                app.alert.show('send_notification_error', {
                    level: 'error',
                    autoClose: true,
                    messages: 'Send Notification Error!'
                });
            },
        });
    },
    saveClicked: function () {
        // Disable the action buttons.
        this.toggleButtons(false);
        var allFields = this.getFields(this.module, this.model);
        var fieldsToValidate = {};
        var erasedFields = this.model.get('_erased_fields');
        for (var fieldKey in allFields) {
            if (app.acl.hasAccessToModel('edit', this.model, fieldKey) &&
                (!_.contains(erasedFields, fieldKey) || this.model.get(fieldKey) || allFields[fieldKey].id_name)) {
                _.extend(fieldsToValidate, _.pick(allFields, fieldKey));
            }
        }
        //Add by HoangHvy to check if content is empty then url is required
        if (this.validateEditorContent()) {
            fieldsToValidate['url'] = allFields['url'];
            fieldsToValidate['url']['required'] = true
        } else {
            fieldsToValidate['url']['required'] = false
        }
        this.model.doValidate(fieldsToValidate, _.bind(this.validationComplete, this));
    },
    validateEditorContent: function (){
        const iframe = document.querySelector('iframe');
        const iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
        const bodyElement = iframeDocument.querySelector('#tinymce');
        return _.isEmpty(bodyElement.textContent)
    }
});
