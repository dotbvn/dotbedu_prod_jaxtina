<?php

class logicNews{
    public function handleBeforeSave(&$bean, $event, $arguments){
        $_POST['team_set_id'] = $bean->fetched_row['team_set_id'];
        /// Regex to match URLs that are not inside <a> tags
        $pattern = '#(?<!href="|">|<\/a>)\bhttps?://[^\s<]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))(?![^<>]*>)#';

        $bean->news_content = preg_replace_callback($pattern, function($matches) {
            $url = $matches[0];
            return '<a href="' . $url . '" target="_blank">' . $url . '</a>';
        }, $bean->news_content);

        $bean->news_content =  nl2br($bean->news_content);
    }

    public function after_save(&$bean, $event, $arguments)
    {
        require_once("custom/clients/mobile/helper/NotificationHelper.php");
        $notify = new NotificationMobile();

        $team_list = array();
        // Get all team set
        $teamSetBean = new TeamSet();
        $teams = $teamSetBean->getTeams($bean->team_set_id);
        // Add all team set to  $team_list
        foreach ($teams as $key => $value) {
            $team_list[] = $key;
        }
        //Handle C_News send notification
        if ((isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) || $bean->send_notification == 1) {
            $arrType = unencodeMultienum($bean->type);
            //If team is global we just push without to_id else we will loop through the team_id and send to each team
            if($bean->team_id == 1) {
                if (in_array('Student', $arrType)) {
                    $notify->pushNotification('Tin tức mới: ' . $bean->name, $bean->description, $bean->module_name, $bean->id, '', 'Student', 1);
                }
                if (in_array('Teacher', $arrType)) {
                    $notify->pushNotification('Tin tức mới: ' . $bean->name, $bean->description, $bean->module_name, $bean->id, '', 'Teacher', 1);
                }
            } else {
                foreach ($team_list as $team_id) {
                    if (in_array('Student', $arrType)) {
                        $notify->pushNotification('Tin tức mới: ' . $bean->name, $bean->description, $bean->module_name, $bean->id, $team_id, 'Student', $team_id);
                    }
                    if (in_array('Teacher', $arrType)) {
                        $notify->pushNotification('Tin tức mới: ' . $bean->name, $bean->description, $bean->module_name, $bean->id, $team_id, 'Teacher', $team_id);
                    }
                }
            }
        } else {
            if ($_POST['team_set_id'] != $bean->team_set_id) {
                $GLOBALS['db']->query("UPDATE notifications SET team_set_id = '{$bean->team_set_id}' WHERE deleted = 0 AND parent_id = '{$bean->id}'");
            }
        }

    }
    public function deleteMobileNotification($bean, $event, $arguments){
        if (!empty($bean->id)) $GLOBALS['db']->query("DELETE FROM notifications WHERE parent_id = '{$bean->id}' AND parent_type = '{$bean->module_name}'");

        //Handle s3 image
        $sqlNote = "UPDATE notes SET deleted = 1 WHERE parent_id = '{$bean->id}' AND parent_type = '{$bean->module_name}' AND deleted = 0";
        $GLOBALS['db']->query($sqlNote);
    }
    public function handleAfterRetrieve($bean, $event, $arguments){
        $qContactsViewNews = "SELECT SUM(count_read_news)
        FROM c_news_contacts_1_c
        WHERE deleted = 0 AND c_news_contacts_1c_news_ida = '{$bean->id}'";
        $rContactViews = $GLOBALS['db']->getOne($qContactsViewNews);
        $qAddonViewNews = "SELECT addon_views
        FROM c_news
        WHERE deleted = 0 AND id = '{$bean->id}'";
        $rAddonViews = $GLOBALS['db']->getOne($qAddonViewNews);
        $bean->views = $rContactViews + $rAddonViews;
    }

}
