<?php
switch ($_POST['type']) {
    case 'reset_all_pwd':
        $result = reset_all_pwd($_POST['password'], $_POST['user_type']);
        echo $result;
        break;
}

// ----------------------------------------------------------------------------------------------------------\\
//Reset all Student Password
function reset_all_pwd($pwd, $user_type){
    if(!empty($pwd)){
        $pwd_hash = User::getPasswordHash($pwd);
        if($user_type == 'Student'){
            $query = "UPDATE contacts SET portal_password='$pwd_hash' WHERE deleted = 0";
            $GLOBALS['db']->query($query);
        }elseif($user_type == 'Teacher'){
            $GLOBALS['db']->query("UPDATE c_teachers SET user_password='$pwd' WHERE deleted = 0");
            $query = "UPDATE users
            SET user_hash='$pwd_hash',
            pwd_last_changed='{$GLOBALS['timedate']->nowDb()}'
            WHERE deleted = 0 AND is_admin = 0 AND status = 'Active'
            AND user_name NOT IN ('admin','apps_admin')
            AND (IFNULL(teacher_id, '') <> '')";
            $GLOBALS['db']->query($query);
        }elseif($user_type == 'User'){
            $query = "UPDATE users
            SET user_hash='$pwd_hash',
            pwd_last_changed='{$GLOBALS['timedate']->nowDb()}'
            WHERE deleted = 0 AND is_admin = 0 AND status = 'Active'
            AND user_name NOT IN ('admin','apps_admin')
            AND (IFNULL(teacher_id, '') = '')";
            $GLOBALS['db']->query($query);
        }
        return json_encode(array("success" => 1,'label' => translate('LBL_APPS_PWD_SAVED_SUCESS', 'Administration')));
    }else return json_encode(array("success" => 0,'label' => translate('LBL_APPS_PWD_SAVED_ERROR', 'Administration')));
}