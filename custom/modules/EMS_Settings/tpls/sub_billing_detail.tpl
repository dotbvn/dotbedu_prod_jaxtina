<div class="sub_billing_detail">
    <div class="row pt-4 px4">
        <div class="col-md-4" style="border-right: 1px solid lightgray">
            <div class="dotb-logo mb-5"><img src="{$company_logo}" width="200px" alt="logo"></div>
            <span>{$MOD.LBL_BILLING_NOTES}</span>
        </div>
        <div class="col-md-8">
            <div class="form-body">
                <div class="form-group col-md-6">
                    <div class="input-group company">
                        <input type="text" class="form-control billing-field-input" autocomplete="off" name="account_name" id="account_name" value="{$billing_info.account_name}" required>
                        <label for="account_name" class="floating-label">
                            <span class="content-name">
                                <i class="fas fa-building"></i> {$MOD.LBL_ACCOUNT_NAME}*
                            </span>
                        </label>
                        <span class="help-block" data-error-msg="{$MOD.LBL_MSG_REQUIRED_FIELD}"
                                    data-default-msg="{$MOD.LBL_HELP_ACCOUNT_NAME}">
                            {$MOD.LBL_HELP_ACCOUNT_NAME}
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="input-group company">
                        <input type="text" class="form-control billing-field-input" autocomplete="off" name="tax_code" id="tax_code" value="{$billing_info.tax_code}" required>
                        <label for="tax_code" class="floating-label">
                            <span class="content-name">
                                <i class="fas fa-file-alt"></i> {$MOD.LBL_TAX_CODE}
                            </span>
                        </label>
                        <span class="help-block" data-error-msg="{$MOD.LBL_MSG_REQUIRED_FIELD}"></span>
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <div class="input-group company">
                        <input type="text" class="form-control billing-field-input" autocomplete="off" name="contact_name" id="contact_name" value="{$billing_info.contact_name}" required>
                        <label for="contact_name" class="floating-label">
                            <span class="content-name">
                                <i class="fas fa-user"></i> {$MOD.LBL_CONTACT_NAME}*
                            </span>
                        </label>
                        <span class="help-block" data-error-msg="{$MOD.LBL_MSG_REQUIRED_FIELD}"></span>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="input-group company">
                        <input type="text" class="form-control billing-field-input" autocomplete="off" name="billing_email" id="billing_email" value="{$billing_info.billing_email}" required>
                        <label for="billing_email" class="floating-label">
                            <span class="content-name">
                                <i class="fas fa-envelope"></i> {$MOD.LBL_EMAIL_ADDRESS}*
                            </span>
                        </label>
                        <span class="help-block" data-error-msg="{$MOD.LBL_MSG_REQUIRED_FIELD}"
                              data-default-msg="{$MOD.LBL_HELP_EMAIL}"
                              data-error-format="{$MOD.LBL_INVALID_EMAIL}">
                            {$MOD.LBL_HELP_EMAIL}
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <div class="input-group company">
                        <input type="text" class="form-control billing-field-input" autocomplete="off" name="phone_mobile" id="phone_mobile" value="{$billing_info.phone_mobile}" required>
                        <label for="phone_mobile" class="floating-label">
                            <span class="content-name">
                                <i class="fas fa-phone"></i> {$MOD.LBL_PHONE}*
                            </span>
                        </label>
                        <span class="help-block" data-error-msg="{$MOD.LBL_MSG_REQUIRED_FIELD}"></span>
                    </div>
                </div>
                <div class="form-group col-md-8">
                    <div class="input-group company">
                        <input type="text" class="form-control edited billing-field-input" autocomplete="off" name="billing_address" id="billing_address" value="{$billing_info.billing_address}" required>
                        <label for="billing_address" class="floating-label">
                            <span class="content-name">
                                <i class="fas fa-home"></i> {$MOD.LBL_PRIMARY_ADDRESS}
                            </span>
                        </label>
                        <span class="help-block" data-error-msg="{$MOD.LBL_MSG_REQUIRED_FIELD}"
                              data-default-msg="{$MOD.LBL_HELP_ADDRESS}">
                            {$MOD.LBL_HELP_ADDRESS}
                        </span>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="input-group" style="height: 59px;">
                        <label for="billing_country" class="floating-label" style="color: grey;">
                            <span class="content-name" style="bottom: 42px; font-size:12px;">
                                <i class="far fa-globe"></i> {$MOD.LBL_COUNTRY}
                            </span>
                        </label>
                        {html_options name="billing_country" id="billing_country" class="form-control billing-field-input" options=$APP_LIST.countries_dom selected=$billing_info.billing_country}
                        <span class="help-block" data-error-msg="{$MOD.LBL_MSG_REQUIRED_FIELD}"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>