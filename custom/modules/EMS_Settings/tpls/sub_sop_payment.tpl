<div id="slide-out-panel-payment" class="slide-out-panel">
    <section class="slide-panel-content" style="height: calc(100% - 45px);">
        {if !$isTrial}
            {include file='custom/modules/EMS_Settings/tpls/sub_payment.tpl'}
        {/if}
    </section>
    <footer class="slide-out-footer py-2" style="border-top: 1px solid lightgray;">
        <div class="form-actions">
            <div class="row">
                <div class="text-end">
                    <button type="button"
                            class="btn btn-light close-slide-out-panel"
                            style="font-size: 1.4rem; margin-right: 2.5rem;"
                            data-id="slide-out-panel-payment">
                        <i class="fas fa-times" style="margin-right: 7px"></i>
                        {$MOD.LBL_BTN_PAY_LATER}
                    </button>
                </div>
            </div>
        </div>
    </footer>
</div>