<div class="sub_change">
    <div class="form-group">
        <div class="col-md-3 hidden-xs">
            <div class="company-logo"><img src="{$company_logo}" width="200px"></div>
            <h4 style="padding-left: 0">{$MOD.LBL_SUB_PAYMENT_METHOD}</h4>{$MOD.LBL_SUB_PAYMENT_METHOD_NOTE}
        </div>
        <div class="col-md-6" style="border-left: 1px solid lightgray;">
            <div style="display: flex; flex-direction: column;">
                <div class="service-duration-container">
                    <label>
                        <input name="service-duration" data-duration="year" type="radio" checked {if $license.lic_status == 'ACTIVE-ON-DEMAND'}disabled{/if}>
                        <span>{$MOD.LBL_ANNUALLY}</span>
                    </label>
                    <label>
                        <input name="service-duration" data-duration="month" type="radio" {if $license.lic_status == 'ACTIVE-ON-DEMAND'}disabled{/if}>
                        <span>{$MOD.LBL_MONTHLY}</span>
                    </label>

                    <span class="selection"></span>
                </div>

                <div class="quantity-input-container">
                    <label class="text-success">{$MOD.LBL_SUB_BUY_MORE_NOTE}</label>
                    <div>
                        <span class="quantity-decrement" onclick="handleChangeQuantity($(this))">–</span>
                        <input id="quantity" class="quantity" type="text"
                               value="0" min="0" onchange="checkQuantity($(this))">
                        <span class="quantity-increment" onclick="handleChangeQuantity($(this))">+</span>
                        <br>
                        <span id="quantity_error" class="help-block"></span>
                    </div>
                </div>
            </div>
            <br>
            <table>
                <tbody>
                <tr>
                    <td style="width: 41%">{$MOD.LBL_LICENSE_STUDENTS}: </td>
                    <td>{$license.lic_subscription} {$MOD.LBL_STUDENT}</td>
                </tr>
                <tr>
                    <td>{$MOD.LBL_EXPIRATION_DATE}: </td>
                    <td>{$license.lic_expired_date} ({$MOD.LBL_REMAINING_DAYS})</td>
                </tr>
                </tbody>
            </table>
            <br>
            <div class="list-group">
                <div class="card product"
                     data-product-name="{$product_templates[0].name}"
                     data-product-id="{$product_templates[0].primary_id}">
                    <div class="card-header">
                        <span class="product-checkmark"></span>
                        <strong>{$product_templates[0].name}</strong>
                    </div>
                    <div class="card-body">
                        <p class="card-title">
                            <span class="list-price bold" data-list-price="{$product_templates[0].list_price}">
                                {$product_templates[0].list_price}</span>
                            <span class="current-price bold"></span>
                            {$CURRENCY} / {$MOD.LBL_STUDENT}
                        </p>
                        <p class="card-text">{$MOD.LBL_SUB_PACKAGE_NOTE}</p>
                    </div>
                </div>

                <div class="card product current-select"
                     data-product-name="{$product_templates[1].name}"
                     data-product-id="{$product_templates[1].primary_id}">
                    <div class="card-header">
                        <span class="product-checkmark product-checked"></span>
                        <strong>{$product_templates[1].name}</strong>
                    </div>
                    <div class="card-body">
                        <p class="card-title">
                            <span class="list-price bold" data-list-price="{$product_templates[1].list_price}">
                                {$product_templates[1].list_price}</span>
                            <span class="current-price bold"></span>
                            {$CURRENCY} / {$MOD.LBL_STUDENT}
                        </p>
                        <p class="card-text">{$MOD.LBL_SUB_PACKAGE_NOTE}</p>
                    </div>
                </div>
                <div class="card product"
                     data-product-name="{$product_templates[2].name}"
                     data-product-id="{$product_templates[2].primary_id}">
                    <div class="card-header">
                        <span class="product-checkmark"></span>
                        <strong>{$product_templates[2].name}</strong>
                    </div>
                    <div class="card-body">
                        <p class="card-title">
                            <span class="list-price bold" data-list-price="{$product_templates[2].list_price}">
                                {$product_templates[2].list_price}</span>
                            <span class="current-price bold"></span>
                            {$CURRENCY} / {$MOD.LBL_STUDENT}
                        </p>
                        <p class="card-text">{$MOD.LBL_SUB_PACKAGE_NOTE}</p>
                    </div>
                </div>

            </div>
            <p style="font-style: italic;font-size: 1.3rem;">{$MOD.LBL_SUB_PRICE_WITHOUT_VAT} </p>
        </div>
    </div>
</div>
