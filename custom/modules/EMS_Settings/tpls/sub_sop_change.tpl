<div id="slide-out-panel-change-subscription" class="slide-out-panel">
    <header>
        <div class="caption" style="border: none;">
            <strong>
                {if $license.lic_status == 'ACTIVE-ON-DEMAND'}{$MOD.LBL_BTN_UPGRADE_PLAN}
                {else}{$MOD.LBL_BTN_CHANGE_PLAN}{/if}
            </strong>
        </div>
    </header>
    <section class="slide-panel-content">
        {if !$isTrial}
            {include file='custom/modules/EMS_Settings/tpls/sub_change.tpl'}
        {/if}
    </section>
    <footer class="slide-out-footer">
        <div class="form-actions">
            <div class="row" style="border-top: 1px solid lightgray;">
                <div class="col-md-offset-4 col-md-8" style="margin-top: 10px;">
                    <button class="btn green btn-outline compare-plans">{$MOD.LBL_BTN_COMPARE_PLAN}</button>
                    <button type="button" class="btn btn-success" style="display: inline-block; font-size: 14px;" id="sub_change"
                            onclick="{if $license.lic_status == 'ACTIVE-ON-DEMAND'}
                            upgradeSubscription(){else}updateSubscription(){/if}">
                        {if $license.lic_status == 'ACTIVE-ON-DEMAND'}
                            {$MOD.LBL_BTN_NEXT}
                            <i class="fas fa-arrow-right" style="margin-left: 7px;"></i>
                        {else}
                            <i class="fas fa-check"></i>
                            {$MOD.LBL_BUTTON_SAVE}
                        {/if}
                    </button>
                    <button style="margin-right: 2rem" type="button"
                            class="btn pull-right close-slide-out-panel"
                            data-id="slide-out-panel-change-subscription">
                        <i class="fas fa-times" style="margin-right: 7px"></i> {$MOD.LBL_BTN_CLOSE}
                    </button>
                </div>
            </div>
        </div>
    </footer>
</div>