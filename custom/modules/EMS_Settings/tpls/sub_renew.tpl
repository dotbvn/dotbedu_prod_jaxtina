<div id="slide-out-panel-renew" class="slide-out-panel" style="text-align: unset;">
    <header>
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="col-xs-offset-1 stepwizard-step col-xs-2">
                    <p data-step="1" style="margin-top: -1px;" class="step-index current-step">1</p>
                    <span>{$MOD.LBL_SELECT_PACKAGE}</span>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <p data-step="2" style="margin-top: -1px;" class="step-index">2</p>
                    <span>{$MOD.LBL_SUBSCRIPTION_DETAILS}</span>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <p data-step="3" style="margin-top: -1px;" class="step-index">3</p>
                    <span>{$MOD.LBL_TITLE_BILLING_INFO}</span>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <p data-step="4" style="margin-top: -1px;" class="step-index">4</p>
                    <span>{$MOD.LBL_CONFIRM_ORDER}</span>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <p data-step="5" style="margin-top: -1px;" class="step-index">5</p>
                    <span>{$MOD.LBL_PAYMENT}</span>
                </div>
            </div>
        </div>
    </header>
    <section class="slide-panel-content" style="height: calc(94% - 120px);">
        {if $current_language == 'en_us'}
            {include file='custom/modules/EMS_Settings/tpls/pricing_table/pricing_table.en_us.tpl'}
        {else}
            {include file='custom/modules/EMS_Settings/tpls/pricing_table/pricing_table.vn_vn.tpl'}
        {/if}
        {include file='custom/modules/EMS_Settings/tpls/sub_change.tpl'}
        {include file='custom/modules/EMS_Settings/tpls/sub_billing_detail.tpl'}
        {include file='custom/modules/EMS_Settings/tpls/sub_inv_detail.tpl'}
        {include file='custom/modules/EMS_Settings/tpls/sub_payment.tpl'}
    </section>
    <footer class="slide-out-footer">
        <div class="form-actions">
            <div class="row" style="border-top: 1px solid lightgray;">
                <div class="col-md-offset-5 col-md-1" style="margin-top: 10px;">
                    <button id="btn_previous" type="button" onclick="return previousStep()"
                            class="btn btn-secondary"><i class="far fa-arrow-left" style="margin-right: 7px;"></i>{$MOD.LBL_BTN_PREVIOUS}</button>
                </div>
                <div class="col-md-5" style="margin-top: 10px;">
                    <button id="btn_next" type="button" onclick="nextStep($(this))" class="btn btn-primary">{$MOD.LBL_BTN_NEXT}<i class="far fa-arrow-right" style="margin-left: 7px;"></i></button>
                </div>
                <div class="col-md-1" style="margin-top: 10px;">
                    <button id="close-sop-renew" style="margin-right: 2rem;" type="button"
                            class="btn pull-right close-slide-out-panel"
                            data-id="slide-out-panel-renew">
                        <i class="fas fa-times" style="margin-right: 7px"></i> {$MOD.LBL_BTN_CLOSE}
                    </button>
                </div>
            </div>
        </div>
    </footer>
</div>
