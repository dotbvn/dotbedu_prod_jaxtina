/*
 * Note:
 * pos (Position): derived from the position of the tpl file within the subscription.tpl file
 */

/**
 * Initialize global variable.
 */
const license_status = $('#license_status').val(),
    isTrial = ($('#slide-out-panel-renew').length > 0),
    saas_url = $('#saas_url').val() + '/',
    currency = $('#currency').val(),
    datepref = parent.DOTB.App.user.get('preferences').datepref;

var $sopBillingInfo = $('#slide-out-panel-billing-detail'),
    $sopInvoiceDetail = $('#slide-out-panel-invoice-detail'),
    $sopPayment = $('#slide-out-panel-payment'),
    $sopComparePlan = $('#slide-out-panel-compare-plan'),
    $sopChangeSubscription = $('#slide-out-panel-change-subscription'),
    $sopUpgradePlan = $('#slide-out-panel-upgrade-plan'),
    $sopRenew = $('#slide-out-panel-renew'),
    defaultSopOptions = {
        closeBtn: '',
        offsetTop: '50px',
        slideFrom: 'top',
        transitionDuration: '0.3s',
        screenClose: false,
    }
var sop_billing_info = $sopBillingInfo.SlideOutPanel(defaultSopOptions),
    sop_invoice_detail = $sopInvoiceDetail.SlideOutPanel(defaultSopOptions),
    sop_compare_plan = $sopComparePlan.SlideOutPanel({
        ...defaultSopOptions,
        beforeOpen() {
            $('.sub_compare_plan').show();
        }
    }),
    sop_upgrade_plan = $sopUpgradePlan.length > 0 ? $sopUpgradePlan.SlideOutPanel({
        ...defaultSopOptions,
        beforeOpen() {
            let service_duration = $('#subscription_service_duration').val();
            $(`input[name="service-duration"][data-duration="${service_duration}"]`).prop('checked', true);
            $('#btn_next, .product').show();
            changeDiscountTier($('#quantity'));
            updatePrice($('input[name="service-duration"]:checked'));
            hiddenProduct();
        },
        afterClosed() {
            sop_upgrade_plan.requestReload && location.reload();
        }
    }) : null,
    sop_payment = $sopPayment.SlideOutPanel({
        ...defaultSopOptions,
        afterClosed() {
            const urlParams = new URLSearchParams(window.location.search);
            if (urlParams.get('invoice_id'))
                parent.App.router.navigate(parent.App.router.buildRoute('EMS_Settings', null, 'layout/subscription'), {trigger:true});
            else
                sop_payment.requestReload;
        }
    }),
    sop_change_subscription = $sopChangeSubscription.length > 0 ? $sopChangeSubscription.SlideOutPanel({
        ...defaultSopOptions,
        beforeOpen() {
            changeDiscountTier($('#quantity'));
            updatePrice($('input[name="service-duration"]:checked'));
            $('.product').show();
        }
    }) : null,
    sop_renew = (isTrial) ? $('#slide-out-panel-renew').SlideOutPanel({
        ...defaultSopOptions,
        beforeClosed() {
            sop_renew.requestReload && location.reload();
        }
    }) : null;
/**
 * Display a success alert message.
 * @param {string} message - The message to display.
 */
function showSuccessAlert(message = '') {
    app.alert.show('success', {level: 'success', messages: message, autoClose: true});
}

/**
 * Display an error alert message.
 * @param {string} message - The message to display.
 */
function showErrorAlert(message = '') {
    app.alert.show('error', {level: 'error', messages: message, autoClose: true});
}

/**
 * Initialize datatable.
 */
const table_lang = JSON.parse($('#datatable_lang').val());
const table = $('#datatable_invoice').DataTable({
    paging: true,
    lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
    language: table_lang,
    order: [2, "desc"],
});

/**
 * Activate trial subscription.
 */
$('.activate-trial').on('click', () => {
    $.confirm({
        title: app.lang.get('LBL_TITLE_ACTIVATE_TRIAL', 'EMS_Settings'),
        content: app.lang.get('LBL_CONTENT_ACTIVATE_TRIAL', 'EMS_Settings'),
        buttons: {
            "Start Trial": {
                text: app.lang.get('LBL_BTN_START_TRIAL', 'EMS_Settings'),
                btnClass: 'btn-blue',
                action: () => {
                    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});
                    $.ajax({
                        type: 'POST',
                        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
                        data: {type: 'activateTrial'},
                        error: () => {
                            app.alert.dismiss('loading');
                            showErrorAlert();
                        }
                    }).then((response) => {
                        app.alert.dismiss('loading');
                        response = JSON.parse(response);
                        if (response.success) {
                            showSuccessAlert();
                            location.reload();
                        } else showErrorAlert(response.message)
                    });
                }
            },
            "Cancel": {},
        },
    });
});

function updatePrice(element) {
    let duration = $(element).data('duration');
    $('.list-price').each(function () {
        let listPrice = Numeric.parse($(this).data('list-price')),
            discount =  Numeric.parse($(this).closest('.card-body').find('.discount-plus').text());
        $(this).text(duration === 'year' ? Numeric.toFloat(listPrice * 12) : Numeric.toFloat(listPrice))
        if(discount) {
            $(this).siblings('.current-price').text(() => {
                listPrice = Math.round(listPrice * (1 - discount / 100) / 100) * 100;
                listPrice *= duration === 'year' ? 12 : 1;
                return Numeric.toFloat(listPrice)
            })
        }
    })
}

$('input[name="service-duration"]').on('change', function () {
    updatePrice($(this))
})

/**
 * Event handler for subscription change button.
 */
$('#subscription_change').on('click', () => {
    if (isTrial && license_status !== 'ACTIVE-ON-DEMAND') {
        let currentStepIndex = $('.stepwizard-step p.current-step').data('step');
        showStep(currentStepIndex);
        sop_renew.open();
    } else if(license_status === 'ACTIVE-ON-DEMAND') {
        showStep(1, 'sop-upgrade-plan');
        sop_upgrade_plan.open();
    }
    else sop_change_subscription.open();
});

/**
 * Event handler for subscription renew button.
 */

$('#subscription_renew').on('click', () => {
    let duration = ($('#lic_billing_cycle').val() === 'month') ? 30 : 365,
        licExpiredDate = new Date($('#lic_expired_date').val()),
        startDate =  new Date(),
        endDate = new Date();
    if ($('#lic_status').val()=== 'ACTIVE-ON-DEMAND') {
        startDate = licExpiredDate;
        startDate.setDate(licExpiredDate.getDate() + 1);
    }
    endDate.setDate(startDate.getDate() + duration - 1);

    let renewContent = app.lang.get('LBL_CONTENT_RENEW', 'EMS_Settings').replace('{start_date}', startDate.toLocaleDateString());
    renewContent = renewContent.replace('{expired_date}', endDate.toLocaleDateString());
    $.confirm({
        title: app.lang.get('LBL_TITLE_RENEW', 'EMS_Settings'),
        content: renewContent,
        buttons: {
            "Renew Now": {
                text: 'OK',
                btnClass: 'btn-blue',
                action: () => {
                    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});
                    $.ajax({
                        type: 'POST',
                        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
                        data: {type: 'renewSubscription'},
                        error: () => {
                            app.alert.dismiss('loading');
                            showErrorAlert();
                        }
                    }).then((response) => {
                        app.alert.dismiss('loading');
                        response = JSON.parse(response);

                        if (response.success) {
                            generatePaymentDetail(response.data.invoice_details, ':last');
                            sop_payment.open();
                            if (response.data.invoice_details.payment.status === 'Unpaid')
                                sop_payment.requestReload = true;
                        } else showErrorAlert(response.message);
                    })
                }
            },
            "Cancel": {},
        }
    });
});
/**
 * Open the compare plans slide-out-panel.
 */
$('.compare-plans').on('click', () => sop_compare_plan.open());


/**
 * Event handler for focusing on billing field inputs.
 */
$('input.billing-field-input').on('focus', function () {
    let defaultMsg = $(this).siblings('.help-block').data('default-msg') || '';
    $(this).siblings('.help-block').removeClass('has-error').text(defaultMsg);
});

/**
 * Show billing information.
 */
$('#billing_info').on('click', () => sop_billing_info.open());

/**
 * Update billing information.
 * @param {object} billing_data - An object containing billing information.
 */
async function updateBillingInfo(billing_data) {
    let success = false;
    await $.ajax({
        type: 'POST',
        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
        data: {type: 'updateBillingInfo', ...billing_data},
        success: (response) => {
            response = JSON.parse(response);
            success = response.success;
        }
    })
    return success;
}

$('#save_billing_info').on('click', async () => {
    if (!validateBillingInfo()) return;

    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});

    let billing_data = {
        account_name: $sopBillingInfo.find('input[name="account_name"]').val(),
        tax_code: $sopBillingInfo.find('input[name="tax_code"]').val(),
        contact_name: $sopBillingInfo.find('input[name="contact_name"]').val(),
        billing_email: $sopBillingInfo.find('input[name="billing_email"]').val(),
        phone_mobile: $sopBillingInfo.find('input[name="phone_mobile"]').val(),
        billing_address: $sopBillingInfo.find('input[name="billing_address"]').val(),
        billing_country: $sopBillingInfo.find('select[name="billing_country"]').val()
    }

    let response = await updateBillingInfo(billing_data);
    app.alert.dismiss('loading');
    if (response) {
        showSuccessAlert();
        sop_billing_info.close();
        location.reload();
    } else showErrorAlert();
})

/**
 * Hidden the product selection based on the selected service name.
 */
function hiddenProduct() {
    let service_name = $('#subscription_service_name').val().trim();
    $('.product-checkmark').removeClass('product-checked');
    $('.card.product')
        .hide().removeClass('current-select')
        .filter(function () {
            return $(this).data('product-name').trim() === service_name;
        })
        .addClass('current-select').show()
        .find('.product-checkmark').addClass('product-checked')
        .end().nextAll('.card.product').show();
}

/**
 * Updates the discount tier based on the selected quantity.
 * @param {jQuery} element - The input element for the quantity field that triggers the change event.
 */
function changeDiscountTier(element) {
    let quantity = parseInt($(element).val()),
        tier_list = JSON.parse($('#tier_list').val()),
        number_registered = parseInt($('#number_registered').val());

    quantity = (isNaN(quantity) || quantity < 0) ? 0 : quantity;
    let number_registering = number_registered + quantity;

    if (Object.keys(tier_list).length > 0) {
        let keys = Object.keys(tier_list);
        delete tier_list[keys[0]];
        let range = Object.keys(tier_list).sort((a, b) => a - b).find(key => parseInt(key) > number_registering),
            rangeIndex = (range !== undefined) ? keys.indexOf(range) : keys.length,
            current_discount_value = (rangeIndex > 1) && parseFloat(tier_list[keys[rangeIndex - 1]])

        if(current_discount_value) {
            $('.list-price').each(function() {
                let listPrice = Numeric.parse($(this).data('list-price'));
                $(this)
                    .addClass('text-decoration-line-through pe-3 record-label').css('font-size', '1.2rem')
                    .siblings('span.current-price').text(() => {
                        let duration = $('input[name="service-duration"]:checked').data('duration');
                        listPrice = Math.round(listPrice * (1 - current_discount_value / 100) / 100) * 100;
                        listPrice *= duration === 'year' ? 12 : 1;
                        return Numeric.toFloat(listPrice);
                })
            })
            $('.card-text').show().html(app.lang.get('LBL_SUB_PACKAGE_NOTE', 'EMS_Settings'));
            $('.quantity-plus').html(keys[rangeIndex - 1]);
            $('.discount-plus').html(current_discount_value);
        } else {
            $('.list-price').each(function() {
                $(this).css('font-size', '1.5rem')
                    .removeClass('text-decoration-line-through pe-3 record-label')
                    .siblings('span.current-price').text('');
            })
            $('.quantity-plus').html('0');
            $('.discount-plus').html('0');
            $('.card-text').hide();
        }
    }
}

/**
 * Changes the quantity value based on the element clicked (increment or decrement button).
 * @param {jQuery} element - The clicked button element.
 */
function handleChangeQuantity(element) {
    let $quantity = $('#quantity'),
        quantity_value = parseInt($quantity.val());

    if ($('#quantity_error').is(":visible")) {
        $quantity.val($quantity.attr('min'));
        eventValidateQuantity(false);
    } else {
        if (element.hasClass('quantity-increment'))
            $quantity.val(quantity_value + 1);
        else if (element.hasClass('quantity-decrement') && quantity_value > parseInt($quantity.attr('min')))
            $quantity.val(quantity_value - 1);
    }

    changeDiscountTier($quantity);
}

/**
 * Displays or hides the quantity error message based on validity.
 * @param {boolean} isValid - Indicates whether the quantity is valid or not.
 */
function eventValidateQuantity(isValid) {
    let $quantity = $('#quantity'),
        errorMessage = app.lang.get('LBL_SUB_NUM_ERROR', 'EMS_Settings') + ' ' + $quantity.attr('min');

    $('#quantity_error')
        .toggle(isValid)
        .text(errorMessage);
    $quantity
        .add($('.quantity-decrement'))
        .add($('.quantity-increment'))
        .css('border-color', isValid ? '#a94442' : '');
}

/**
 * Checks the validity of the quantity entered.
 * @param {jQuery} element - The input element for the quantity field that triggers the change event.
 */
function checkQuantity(element) {
    let value = element.val();
    changeDiscountTier(element);

    if (!/^\d+$/.test(value) || parseInt(value) < parseInt(element.attr('min'))) {
        eventValidateQuantity(true);
        return false;
    } else {
        eventValidateQuantity(false);
        return true;
    }
}

/**
 * Handles product selection by toggling classes.
 */
$('.product').on('click', function () {
    $('.product').removeClass('current-select').find('.product-checkmark').removeClass('product-checked');
    $(this).addClass('current-select').find('.product-checkmark').addClass('product-checked');
})

/**
 * Change the subscription.
 */
async function updateSubscription() {
    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});

    let success = false,
        service_duration = $('input[name="service-duration"]:checked').data('duration'),
        product_id = $(".product.current-select").data("product-id"),
        quantity = $('#quantity').val(),
        pos = ':first';

    if (isTrial) {
        let billing_data = {
            account_name: $('input[name="account_name"]' + pos).val(),
            contact_name: $('input[name="contact_name"]' + pos).val(),
            tax_code: $('input[name="tax_code"]' + pos).val(),
            billing_email: $('input[name="billing_email"]' + pos).val(),
            phone_mobile: $('input[name="phone_mobile"]' + pos).val(),
            billing_address: $('input[name="billing_address"]' + pos).val(),
            billing_country: $('select[name="billing_country"]' + pos).val()
        }
        await updateBillingInfo(billing_data);
    }

    await $.ajax({
        type: 'POST',
        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
        data: {type: 'updateSubscription', product_id, quantity, service_duration},
        error: () => {
            app.alert.dismiss('loading');
            showErrorAlert();
        }
    }).then((response) => {
        app.alert.dismiss('loading');
        response = JSON.parse(response);

        if (response.success) {
            success = true;
            if (isTrial || (!isTrial && license_status === 'ACTIVE-ON-DEMAND')) {
                let invoice_details = response.data.invoice_details;
                $('#btn_next').attr('data-invoice-id', invoice_details.quote_id);
                generateInvoiceDetail(invoice_details, pos);
            } else {
                showSuccessAlert();
                sop_change_subscription.close();
                location.reload();
            }
        } else showErrorAlert(response.message);
    })
    return success;
}

/**
 * Generate invoice draft
 * @param {object} invoice_details - Response from invoice detail.
 * @param {string} pos - Position display.
 */
function generateInvoiceDetail(invoice_details, pos = ':first') {
    $('.inv-no' + pos).text(invoice_details.quote_name);
    $('.inv-date' + pos).text(app.date.format(new Date(invoice_details.issue_date), datepref));
    $('.inv-due-date' + pos).text(app.date.format(new Date(invoice_details.expired_date), datepref));
    $('.inv-number-of-payment' + pos).text(invoice_details.number_of_payment);

    $('.inv-view-pdf' + pos).attr('data-invoice-id', invoice_details.quote_id);
    $('.icon-view-payments' + pos).removeAttr('data-invoice-id').removeData('invoice-id')
    $('.icon-view-payments' + pos).attr('data-invoice-id', invoice_details.quote_id);
    let $inv_payment = $('.inv-payment' + pos);
    (invoice_details.status === 'Sent')
        ? $inv_payment.show().attr('data-invoice-id', invoice_details.quote_id)
        : $inv_payment.hide()

    let billing_info = invoice_details.billing_info;
    $('.inv-payer-contact-name' + pos).text(billing_info.contact_name);
    $('.inv-payer-tax-code' + pos).text(billing_info.tax_code);
    $('.inv-payer-billing-email' + pos).text(billing_info.billing_email);
    $('.inv-payer-phone-mobile' + pos).text(billing_info.phone_mobile);
    $('.inv-payer-billing-address' + pos).text(billing_info.billing_address
        + (billing_info.billing_country !== '' ? (', ' + billing_info.billing_country) : ''));

    $('.table-detail-invoice' + pos + ' tbody tr').remove();
    generateListProduct($('.table-detail-invoice' + pos + ' tbody'), invoice_details);

    let $voucher_content = $('.voucher-code' + pos);
    if (invoice_details.status === 'Sent' || invoice_details.status === 'Draft') {
        $voucher_content
            .attr('data-invoice-id', invoice_details.quote_id)
            .attr('data-invoice-status', invoice_details.status)
            .attr('data-product-ids', () => {
                let product_ids = [];
                $.each(invoice_details.products, (index, product) => {
                    product_ids.push(product.product_template_id);
                })
                return JSON.stringify(product_ids);
            })
    }
    generateUseSponsor($voucher_content, invoice_details);

    $('.inv-subtotal' + pos).text(Numeric.toFloat(invoice_details.total_after_discount));
    $('.inv-vat-value' + pos).text(Numeric.toFloat(invoice_details.tax_amount));
    $('.inv-total' + pos).text(Numeric.toFloat(invoice_details.total));
}

function generatePaymentDetail(invoice_details, pos = ':first') {
    let billing_info = invoice_details.billing_info,
        payment = invoice_details.payment;

    $('.payment_name' + pos).text(invoice_details.quote_name);
    $('.payment_date' + pos).text(app.date.format(new Date(invoice_details.issue_date), datepref));
    $('.payment_due_date' + pos).text(app.date.format(new Date(invoice_details.expired_date), datepref));
    $('.payment_view_pdf' + pos).attr('data-invoice-id', invoice_details.quote_id);
    $('.number_of_payment' + pos).text(invoice_details.number_of_payment);

    $('.payment_contact_name' + pos).text(billing_info.contact_name);
    $('.payment_tax_code' + pos).text(billing_info.tax_code);
    $('.payment_billing_address' + pos).text(billing_info.billing_address
        + (billing_info.billing_country !== '' ? (', ' + billing_info.billing_country) : ''));
    $('.payment_billing_email' + pos).text(billing_info.billing_email);
    $('.payment_phone_mobile' + pos).text(billing_info.phone_mobile);

    $('.qr-payment' + pos).attr('src', saas_url + payment.vietQR.img_path);
    $('.p-transfer-account-name' + pos).text(payment.vietQR.account_holder_name);
    $('.p-transfer-account-number' + pos).text(payment.vietQR.account_number);
    $('.p-transfer-bank' + pos).text(payment.vietQR.bank_name);
    $('.p-transfer-amount' + pos).text(Numeric.toFloat(payment.vietQR.payment_amount));
    $('.p-transfer-content' + pos).text(payment.vietQR.content);

    $('.table-detail-payment' + pos + ' tbody tr').remove();
    generateListProduct($('.table-detail-payment' + pos), invoice_details);

    if (invoice_details.voucher.id) {
        $('.p-sponsor-name' + pos).html(invoice_details.voucher.name);
        $('.p-sponsor-value' + pos).html(Numeric.toFloat(invoice_details.order_sponsor_amount));
    }

    $('.p-subtotal' + pos).text(Numeric.toFloat(invoice_details.total_after_discount));
    $('.p-taxrate-value' + pos).text(Numeric.toFloat(invoice_details.tax_amount));
    $('.p-total' + pos).text(Numeric.toFloat(invoice_details.total));
}

/**
 * Upgrade the subscription.
 */
async function upgradeSubscription(element) {
    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});

    let success = false,
        product_id = $(".product.current-select").data("product-id"),
        quantity = $('#quantity').val();

    await $.ajax({
        type: 'POST',
        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
        data: {type: 'upgradeSubscription', product_id, quantity},
        error: () => {
            app.alert.dismiss('loading');
            showErrorAlert();
        }
    }).then((response) => {
        app.alert.dismiss('loading');
        response = JSON.parse(response);

        if (response.success) {
            success = true;
            let invoice_details = response.data.invoice_details;
            $(element).attr('data-invoice-id', invoice_details.quote_id);
            generateInvoiceDetail(invoice_details);
        } else showErrorAlert(response.message);
    })
    return success;
}

/**
 * Opens the PDF Term/Invoice.
 * @param {jQuery} element - The clicked element containing the PDF data.
 */
function getPDF(element) {
    let type_pdf = element.data('type-pdf'),
        url = '';
    if (type_pdf === 'terms') url = saas_url + 'index.php?entryPoint=getTermsPdf&document_name=terms';
    else {
        let invoice_id = element.data('invoice-id'),
            urlParams = $.param({
                'module': 'EMS_Settings',
                'action': 'view_pdf',
                'pdf_module': 'Quotes',
                'record': invoice_id,
                'dotbpdf': type_pdf,
            });
        url = '?' + urlParams;
    }
    window.open(url);
}
$('body').click(function (event)
{
    if(!$(event.target).closest('#modalHistory').length && !$(event.target).is('#modalHistory')) {
        $('.modal').removeClass('show');
        $('.modal').modal('hide');
    }
});
function getReceipts(element, filter = '') {
    let invoice_id = (filter) ? element.closest('tr').data('invoice-id') : element.data('invoice-id');
    $.ajax({
        type: 'POST',
        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
        data: {
            type: 'getReceipts',
            invoice_id: invoice_id,
            filter: filter,
        },
        error: () => {
            app.alert.dismiss('loading');
            showErrorAlert();
        }
    }).then((response) => {
        let res = JSON.parse(response);
        if (res.success) {
            renderModalBody(res.data.payments);
        } else showErrorAlert(response.message);
    });
}

function renderModalBody(data) {
    let modal = $('.modal'),
        modalBody = $('#historyPayment'),
        html = '',
        statusClass = {
            'Unpaid': 'label-orange',
            'Paid': 'label-success',
        };
    modal.modal().modal('show');
    app.$contentEl.attr('aria-hidden', true);
    $('.modal-backdrop').insertAfter(modal);
    $('.modal-backdrop').css('z-index', '99999');
    modalBody.html('');
    if (data.length === 0) {
        html = `<tr><td colspan="6" style="text-align: center;">
            <img src = "custom/images/no_payment.svg" alt="No payment found!" width="64" style="margin-top: .8rem;"/><br>
            <span style="margin-top: .8rem;">` + app.lang.get('LBL_NO_PAYMENT', 'EMS_Settings') +`</span>
        </td></tr>`;
    } else {
        data.forEach((item, index) => {
            html += `<tr>
                <td>${item.payment_no}</td>
                <td>${item.name}</td>
                <td>${formatDate(parseDate(item.payment_date))}</td>
                <td><span class="label ellipsis_inline ${statusClass[item.status]} modal-status-badge" rel="tooltip" data-placement="bottom" title="" data-original-title="${item.status}">${item.status}</span></td>
                <td>${item.payment_method}</td>
                <td>${Numeric.toFloat(item.payment_amount)}</td></tr>`;
        });
    }
    modalBody.append(html);
    $('#modalHistory').css('z-index', '999999');
    $('#modalHistory').addClass('show');
}
function parseDate(dateString) {
    const [year, month, day] = dateString.split('-').map(Number);
    return new Date(year, month - 1, day);
}
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return `${day}/${month}/${year}`;
}

/**
 * Generates a list of products for the invoice table.
 * @param {jQuery} $table - The jQuery object representing the invoice table element.
 * @param {Object} invoice_details - The object containing details of the invoice.
 */
function generateListProduct($table, invoice_details) {
    let html = '';
    $.each(invoice_details.products, (index, product) => {
        let productNameExt = '',
            serviceDuration = '-';
        if (product.service == '1') {
            productNameExt = ' - ' + product.service_duration_value + ' ' + product.service_duration_unit;
            serviceDuration = app.date.format(new Date(product.service_start_date), datepref) + ' - ' + app.date.format(new Date(product.service_end_date), datepref)
        }
        html +=
            '<tr>' +
            '<td>' + product.line_num + '</td>' +
            '<td>' + product.product_name + productNameExt + '</td>' +
            '<td style="text-align: center;">' + Numeric.parse(product.quantity) + ' ' + parent.DOTB.App.lang.getAppListStrings('subscription_unit_dom')[product.unit_name] + '</td>' +
            '<td style="text-align: center;">' + Numeric.toFloat(product.subtotal/product.quantity) + '</td>' +
            '<td nowrap style="text-align: center;">' + serviceDuration + '</td>' +
            '<td style="text-align: right;">' + Numeric.parse(product.discount_value) + '</td>' +
            '<td style="text-align: right;">' + Numeric.toFloat(product.sponsor_amount) + '</td>' +
            '<td style="text-align: right;">' + Numeric.toFloat(product.net_total) + '</td>' +
            '</tr>';
    });
    $table.append(html);
}

/**
 * Display invoice details on the invoice-detail slide-out-panel.
 * @param {jQuery} element - The clicked element containing the invoice ID data.
 */
function getInvoiceDetail(element) {
    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});

    let invoice_id = element.closest('tr').data('invoice-id');

    $.ajax({
        type: 'POST',
        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
        data: {type: 'getInvoiceDetail', invoice_id},
        error: () => {
            app.alert.dismiss('loading');
            showErrorAlert();
        }
    }).then((response) => {
        app.alert.dismiss('loading');
        response = JSON.parse(response);

        if (response.success) {
            let invoice_details = response.data.invoice_details,
                pos = (isTrial || license_status === 'ACTIVE-ON-DEMAND') ? ':last' : '';

            generateInvoiceDetail(invoice_details, pos)
            sop_invoice_detail.open();
        } else showErrorAlert(response.message);
    });
}


/**
 * Generates HTML content for displaying voucher information.
 * @param {Object} voucher - The voucher object containing discount details.
 * @returns {string} - HTML content for displaying voucher information.
 */
function generateSponsorContent(voucher) {
    let discount_value = Number(voucher.discount_value),
        amount = Number.isInteger(discount_value) ? discount_value : app.utils.formatNumber(voucher.discount_value, 0, 1),
        discount_html = '<span class="voucher-discount-title">' + app.lang.get('LBL_DISCOUNT', 'EMS_Settings').toUpperCase() + '</span>&nbsp;' +
            '<b class="voucher-discount-amount">' + amount + '%</b>',
        description_html = (voucher.description) ? '<span>' + voucher.description + '</span>' : '',
        voucher_html = '<div class="voucher-info-container"><div>' +
            '<b class="voucher-info-title">' + voucher.name + '</b></div>' +
            '<b class="voucher-info-used_time">' + voucher.used_time + '</b></div>' + description_html +
            '<span>' + app.lang.get('LBL_PROMOTION_PERIOD', 'EMS_Settings') + ': <b>' + voucher.promotion_period + '</b></span>';

    return '<div class="voucher-container">' +
        '<div class="voucher-discount">' + discount_html + '</div>' +
        '<div class="voucher-info">' + voucher_html + '</div></div>';
}

function generateSponsorContainer(voucher, view_only, discount_value, discount_total) {
    let sponsorHtml = '<div class="sponsor-container"><span class="sponsor-value">' + voucher.name + '</span>';
    if (!view_only)
        sponsorHtml += '<i class="fa fa-times icon_deleted" onclick="deleteSponsor()"></i>';

    sponsorHtml += '</div><span class="bold">' + Numeric.toFloat(discount_total) + '</span>';

    return sponsorHtml;
}

/**
 * Generates HTML content for displaying the use of sponsor information.
 * @param {jQuery} $voucher_content - The jQuery object representing the container for voucher content.
 * @param {Object} [invoice_details={}] - Invoice details.
 */
async function generateUseSponsor($voucher_content, invoice_details = {}) {
    let view_only = (invoice_details.status === 'Paid in Full'),
        voucher = invoice_details.voucher;
    if (!voucher.id) {
        view_only
            ? $voucher_content.html('<div></div> <span>0</span>')
            : generateInputVoucherCode($voucher_content);
        return;
    }

    if (typeof invoice_details.order_sponsor_percent == 'undefined') {
        let pos = $sopInvoiceDetail.hasClass('open') ? ':last' : ':first';
        $voucher_content.attr('data-sponsor-id', voucher.id);

        await updateInvoice(invoice_details.quote_id, invoice_details.status, 'invoice', pos);
        showSuccessAlert(app.lang.get('LBL_SPONSOR_APPLIED', 'EMS_Settings'));
    } else {
        let discount_value = Numeric.parse(invoice_details.order_sponsor_percent),
            discount_total = invoice_details.order_sponsor_amount;

        $voucher_content.html(generateSponsorContainer(voucher, view_only, discount_value, discount_total));
    }
}

function generateInputVoucherCode($voucher_content) {
    $voucher_content.html(`<div class="d-flex">
        <input type="text" class="input-voucher-code h-100">
        <button class="btn btn-primary search-voucher-code" type="button" onclick="checkVoucher($(this))">
        <i class="fas fa-search"></i></button></div>
        <span class="mt-auto mb-auto"><strong>0</strong></span>`);
}

/**
 * Deletes the sponsor HTML to enable user input for a new sponsor.
 */
async function deleteSponsor() {
    let pos = $sopInvoiceDetail.hasClass('open') ? ':last' : ':first',
        $voucher_content = $('.voucher-code' + pos).removeAttr('data-sponsor-id').removeData('sponsor-id'),
        invoice_id = $voucher_content.data('invoice-id'),
        invoice_status = $voucher_content.data('invoice-status');
    await updateInvoice(invoice_id, invoice_status, 'invoice', pos);
}

/**
 * Checks the validity of a voucher code when the Enter key is pressed.
 */
$(document).on('keydown', '.input-voucher-code', (event) => {
    if (event.key === 'Enter') {
        checkVoucher($(event.target).next());
    }
});


/**
 * Checks the validity of a voucher code when the search button is clicked.
 */
function checkVoucher(element) {
    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});

    let voucher_code = element.prev('.input-voucher-code').val().trim().toUpperCase(),
        $voucher_content = element.closest('.voucher-code'),
        product_id = JSON.stringify($voucher_content.data('product-ids'));

    if (!voucher_code) {
        app.alert.dismiss('loading');
        $.alert({
            title: app.lang.get('LBL_ERROR'),
            content: app.lang.get('LBL_ERR_EMPTY_VOUCHER_CODE', 'EMS_Settings'),
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            buttons: {
                "tryAgain": {
                    text: app.lang.get('LBL_TRY_AGAIN', 'EMS_Settings'),
                    btnClass: 'btn-red',
                },
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
            data: {type: 'checkVoucher', voucher_code, product_id},
            error: () => {
                app.alert.dismiss('loading');
                showErrorAlert();
            }
        }).then((response) => {
            app.alert.dismiss('loading');
            response = JSON.parse(response);

            if (response.success) {
                $.confirm({
                    title: app.lang.get('LBL_VOUCHER_INFO', 'EMS_Settings'),
                    content: generateSponsorContent(response.data.voucher),
                    buttons: {
                        "Use": {
                            text: app.lang.get('LBL_APPLY', 'EMS_Settings'),
                            btnClass: 'btn-blue',
                            action: () => {
                                let invoice_details = {
                                    voucher: response.data.voucher,
                                    quote_id: $voucher_content.data('invoice-id'),
                                    status: $voucher_content.data('invoice-status')
                                }
                                generateUseSponsor($voucher_content, invoice_details)
                            }
                        },
                        "Cancel": {}
                    }
                });
            } else {
                $.alert({
                    title: app.lang.get('LBL_ERROR'),
                    content: response.message,
                    type: 'red',
                    typeAnimated: true,
                    boxWidth: '30%',
                    useBootstrap: false,
                    buttons: {
                        "tryAgain": {
                            text: app.lang.get('LBL_TRY_AGAIN', 'EMS_Settings'),
                            btnClass: 'btn-red',
                        },
                    }
                })
            }
        })
    }
}

/**
 * Displays payment information associated on payment slide-out-panel.
 */
function showPaymentInfo(element) {
    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});

    let invoice_id = element.data('invoice-id');

    $.ajax({
        type: 'POST',
        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
        data: {type: 'getInvoiceDetail', invoice_id},
        error: () => {
            app.alert.dismiss('loading');
            showErrorAlert();
        }
    }).then((response) => {
        app.alert.dismiss('loading');
        response = JSON.parse(response);

        if (response.success) {
            generatePaymentDetail(response.data.invoice_details, ':last');
            sop_payment.open();
            if(response.data.invoice_details.payment.status === 'Unpaid')
                sop_payment.requestReload = true;
        } else showErrorAlert(response.message);
    })
}

/**
 * Handles product selection and panel display during the subscription trial process.
 */
$('.btn-select').on('click', function () {
    let $product_card = $('.product'),
        product_name = $(this).data('product-name');

    $product_card.removeClass('current-select');
    $('.product-checkmark').removeClass('product-checked');

    $('.product[data-product-name="' + product_name + '"]').addClass('current-select')
        .find('.product-checkmark').addClass('product-checked');

    if (isTrial) {
        if ($('.sub_compare_plan').last().is(':visible')) {
            $('.stepwizard-step p').removeClass('current-step step-completed')
                .html(function () {
                    return $(this).data('step');
                }).first().addClass('current-step');
            sop_compare_plan.close();
            sop_renew.open();
        }
        $product_card.each(function () {
            $(this).toggle($(this).hasClass('current-select'));
        });
        nextStep();
        return;
    }
    sop_compare_plan.close();
    sop_change_subscription.open();
})

/**
 * Displays the specified step in the subscription trial process.
 */
function showStep(stepIndex, view = '') {
    let steps = ['.sub_compare_plan', '.sub_change', '.sub_billing_detail', '.sub_inv_detail', '.sub_payment'];
    if(view === 'sop-upgrade-plan')
        steps = ['.sub_change', '.sub_inv_detail', '.sub_payment'];
    $.each(steps, (index, step) => $(step).first().css('display', index === stepIndex - 1 ? 'block' : 'none'));
}

function disableNavigationButtons(isDisabled) {
    let $btnPrevious = $('#btn_previous'),
        $btnNext = $('#btn_next');
    $btnPrevious.add($btnNext).prop('disabled', isDisabled);
}

/**
 * Handles navigation to the next step in the subscription trial process or upgrade plan process.
 */
async function nextStep(element) {
    let currentStep = $('.stepwizard-step p.current-step'),
        currentStepIndex = currentStep.data('step'),
        $btnPrevious = $('#btn_previous'),
        $quantity = $('#quantity');
    disableNavigationButtons(true);
    if(isTrial) {
        switch (currentStepIndex) {
            case 1:
                changeDiscountTier($quantity);
                updatePrice($('input[name="service-duration"]:checked'));
                break;
            case 2:
                if (!checkQuantity($quantity)) {
                    disableNavigationButtons(false);
                    return;
                }
                break;
            case 3:
                if (!validateBillingInfo()) {
                    disableNavigationButtons(false);
                    return;
                }
                if(!await updateSubscription()) {
                    disableNavigationButtons(false);
                    return;
                }
                element.html('<i class="far fa-check" style="margin-right: 7px;"></i>' + app.lang.get('LBL_BTN_CONFIRM', 'EMS_Settings'));
                break;
            case 4:
                if(!(await updateInvoice(element.data('invoice-id'), 'Sent', 'payment', ':first'))) {
                    disableNavigationButtons(false);
                    return;
                }
                element.add($btnPrevious).hide();
                sop_renew.requestReload = true;
                break;
            default:
                break;
        }
    } else {
        switch (currentStepIndex) {
            case 1:
                if (!checkQuantity($quantity)) {
                    disableNavigationButtons(false);
                    return;
                }
               if(!(await upgradeSubscription(element))) {
                   disableNavigationButtons(false);
                   return;
               }
                element.html('<i class="far fa-check" style="margin-right: 7px;"></i>' + app.lang.get('LBL_BTN_CONFIRM', 'EMS_Settings'));
                break;
            case 2:
                if(!(await updateInvoice(element.data('invoice-id'), 'Sent', 'payment', ':first'))) {
                    disableNavigationButtons(false);
                    return;
                }
                element.add($btnPrevious).hide();
                sop_upgrade_plan.requestReload = true;
                break;
            default:
                break;
        }
    }
    disableNavigationButtons(false);

    let $nextStep = currentStep.parent().next().find('p');
    if ($nextStep.length > 0) {
        currentStep.html('<i class="fa fa-check"></i>').removeClass('current-step').addClass('step-completed');
        $nextStep.addClass('current-step');
        showStep(currentStepIndex + 1, !isTrial ? 'sop-upgrade-plan' : '');
    }

    if ($nextStep.parent().next().length) {
        element ? element.show() : $('#btn_next').show();
        $btnPrevious.show();
    }
}

/**
 * Handles navigation to the previous step in the subscription trial process.
 */
function previousStep() {
    let $currentStep = $('.stepwizard-step p.current-step'),
        $prevStep = $currentStep.parent().prev().find('p'),
        prevStepIndex = $prevStep.data('step');

    if ($prevStep.length > 0) {
        $currentStep.removeClass('current-step');
        $prevStep.html(prevStepIndex).removeClass('step-completed').addClass('current-step');
        showStep(prevStepIndex, !isTrial ? 'sop-upgrade-plan' : '');
    }

    $prevStep.parent().prev().find('p').length || $('#btn_previous').hide();
    $('#btn_next').html(app.lang.get('LBL_BTN_NEXT', 'EMS_Settings')
        + '<i class="far fa-arrow-right" style="margin-left: 7px;"></i>');
}

/**
 * Validates the billing information entered
 * @returns {boolean} - Validity status of billing information.
 */
function validateBillingInfo() {
    let fieldsToCheck = ['account_name', 'contact_name', 'billing_email', 'phone_mobile'],
        isValid = true,
        emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        position = !isTrial ? '' : ($sopBillingInfo.hasClass('open') ? ':last' : ':first');

    fieldsToCheck.forEach(fieldName => {
        let $field = $('input[name="' + fieldName + '"]' + position),
            fieldValue = $field.val().trim();

        if (fieldValue === '') {
            let errorMsg = $field.siblings('.help-block').data('error-msg');
            $field.siblings('.help-block').addClass('has-error').text(errorMsg);
            isValid = false;
        } else if (fieldName === 'billing_email' && !emailRegex.test(fieldValue)) {
            let errorMsg = $field.siblings('.help-block').data('error-format');
            $field.siblings('.help-block').addClass('has-error').text(errorMsg);
            isValid = false;
        }
    });
    return isValid;
}

/**
 * Displays payment information. Displayed at the final step of the trial process.
 * @param {string} invoice_id - The ID of the invoice to be updated.
 * @param {string} status - Status of invoice.
 * @param {string} view - Invoice detail / Payment info (invoice/payment).
 * @param {string} pos - The position of the element to be updated.
 */
async function updateInvoice(invoice_id, status = 'Draft', view, pos) {
    app.alert.show('loading', {level: 'process', title: app.lang.get('LBL_LOADING')});
    let success = false,
        $voucher_content = $('.voucher-code' + pos),
        voucher_id = $voucher_content.data('sponsor-id');

    await $.ajax({
        type: 'POST',
        url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
        data: {type: 'updateInvoice', invoice_id, status, voucher_id},
        error: () => {
            app.alert.dismiss('loading');
            showErrorAlert();
        }
    }).then((response) => {
        app.alert.dismiss('loading');
        response = JSON.parse(response);

        if (response.success) {
            success = true;
            let invoice_details = response.data.invoice_details;
            if (view === 'invoice')
                generateInvoiceDetail(invoice_details, pos);
            else if (view === 'payment')
                generatePaymentDetail(invoice_details);
        } else showErrorAlert(response.message);
    })
    return success;
}

$(document).ready(function() {
    let urlParams = new URLSearchParams(window.location.search);
    let invoice_id = urlParams.get('invoice_id');
    if (invoice_id) {
        $.ajax({
            type: 'POST',
            url: 'index.php?module=EMS_Settings&action=handleSubscription&dotb_body_only=true',
            data: {type: 'getInvoiceDetail', invoice_id},
            error: () => {
                app.alert.dismiss('loading');
                showErrorAlert();
            }
        }).then((response) => {
            app.alert.dismiss('loading');
            response = JSON.parse(response);
            if (response.success) {
                generatePaymentDetail(response.data.invoice_details, ':last');
                sop_payment.open();
                if(response.data.invoice_details.payment.status === 'Unpaid')
                    sop_payment.requestReload = true;
            } else showErrorAlert(response.message);
        })
    }

    var socketURL = "https://socket.dotb.cloud/";
    var socket = parent.io.connect(socketURL, {"path": "", "transports": ["websocket"], "reconnection": true});
    socket.on('connect', _.bind(function () {
        console.log('Socket server is live!!!!');
        socket.emit('join', parent.App.config.lic_tenant_name + '/load-license');
    }, this));
    socket.on('error', function () {
        console.log('Cannot connect to socket server!')
    })
    socket.on('event-phenikaa', function (response) {
        let panels = [$sopPayment, $sopRenew, $sopUpgradePlan];
        let openPanels = panels.filter((e) => $(e.get(0)).hasClass('open'))
        openPanels.forEach(function(e) {
            let msg = parent.DOTB.App.lang.get('LBL_PAYMENT_SUCCESSFUL',  'EMS_Settings');
            parent.toastr.success(msg.replace("{{quote_name}}", response.quote_name).replace("{{amount}}", response.audited_amount));
            setTimeout(() => e.close(), 3000)
        })
    });
});