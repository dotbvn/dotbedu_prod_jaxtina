<div id="slide-out-panel-billing-detail" class="slide-out-panel">
    <header  style="height: 8%;">
        <h3 id="labelChangeBillingDetail"><b>{$MOD.LBL_TITLE_BILLING_INFO}</b></h3>
    </header>
    <section class="slide-panel-content" style="height: 82%">
        {include file='custom/modules/EMS_Settings/tpls/sub_billing_detail.tpl'}
    </section>
    <footer class="slide-out-footer"  style="border-top: 1px solid lightgray; height: 10%">
        <div class="row form-actions">
            <div class="text-end">
                <button type="button"
                        class="btn close-slide-out-panel"
                        style="margin-right: 1rem"
                        data-id="slide-out-panel-billing-detail">
                    <i class="fas fa-times" style="margin-right: 7px;"></i>
                    {$MOD.LBL_BTN_CLOSE}
                </button>
                <button type="button"
                        class="btn btn-primary"
                        style="font-size: 1.4rem; margin-right: 2.5rem;"
                        id="save_billing_info">
                    <i class="fas fa-check" style="margin-right: 7px;"></i>
                    {$MOD.LBL_BTN_SAVE}
                </button>
            </div>
        </div>
    </footer>
</div>