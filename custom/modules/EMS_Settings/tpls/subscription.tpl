{*CSS*}
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/Bootstrap/bootstrap.min.css'}">
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/DataTables/css/jquery.dataTables.min.css'}">
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/Bootstrap/bootstrap.min.5_3.css'}">
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/modules/EMS_Settings/tpls/css/slide-out-panel.css'}">
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/modules/EMS_Settings/tpls/css/subscription.css'}">
<link rel='stylesheet' href='{dotb_getjspath file="themes/RacerX/css/custom3.css"}'/>

<div class="main_container">
    <input type="hidden" value="{$saas_url}" id="saas_url">
    <input type="hidden" value="{$CURRENCY}" id="currency">
    <input type="hidden" value="{$license.lic_status}" id="license_status">
    <input type="hidden" value="{$license.lic_billing_cycle}" id="lic_billing_cycle">
    <input type="hidden" value="{$license.lic_expired_date_db}" id="lic_expired_date">
    <input type="hidden" value="{$license.lic_subscription}" id="number_registered">
    <input type="hidden" value="{$subscription.plan_name}" id="subscription_service_name">
    <input type="hidden" value="{$subscription.billing_cycle}" id="subscription_service_duration">
    <textarea hidden id="tier_list">{$tier_list}</textarea>
    <div class="main-subscription">
        <div class="main_header text-left">
            <h3><b>{$MOD.LBL_PANEL_SUBSCRIPTION}</b></h3>
        </div>
        <div class="main_content">
            <form action="#" class="form-horizontal">
                <div class="form-group d-flex align-items-stretch">
                    <div class="license-info col-md-4">
                        <div class="row pb-3">
                            <div class="col-md-12">
                                <h4 class="no-padding" style="font-weight: bold;">{$MOD.LBL_TITLE_LICENSE_INFO}</h4>
                            </div>
                        </div>
                        <div class="row pb-3">
                            <div class="col-md-4 record-label">{$MOD.LBL_LICENSE_STATUS}:</div>
                            <div class="col-md-8">
                                {if $license.lic_status == 'ACTIVE-TRIAL' || $license.lic_status == 'ACTIVE-ON-DEMAND'}
                                    <h4 class="no-padding text-success"><strong>{$license_status}</strong></h4>
                                    <h5 class="no-padding text-success">{$MOD.LBL_REMAINING_DAYS}</h5>
                                {elseif $license.lic_status == 'EXPIRED'}
                                    <h4 class="no-padding text-danger"><strong>{$license_status}</strong></h4>
                                    <h5 class="no-padding text-danger">{$MOD.LBL_REMAINING_DAYS}</h5>
                                {else}
                                    <h4 class="no-padding text-muted"><strong>{$license_status}</strong></h4>
                                    <h5 class="no-padding">{$MOD.LBL_NO_LICENSE_DESC}</h5>
                                {/if}
                            </div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_EXPIRATION_DATE}:</div>
                            <div class="col-md-8 {if $license.lic_status == 'EXPIRED'} text-danger{/if} record">
                                {if $hasLicense}{$license.lic_expired_date}{else} - {/if}
                            </div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_SUBSCRIPTION_PLAN}:</div>
                            <div class="col-md-8 record">
                                {if $hasLicense}{$subscription.plan_name}{else} - {/if}
                            </div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_BILLING_CYCLE}:</div>
                            <div class="col-md-8 record">
                                {if $hasLicense}{$billing_cycle}{else} - {/if}
                            </div>
                        </div>
                        <div class="row pt-2 pb-3 mb-0 align-items-center">
                            <div class="col-md-12 record-label">{$MOD.LBL_SUBSCRIPTION_DETAILS}:</div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-1 record-label text-right" style="padding: 0">&#8226; </div>
                            <div class="col-md-4 record-label">{$MOD.LBL_UNIT_PRICE}:</div>
                            <div class="col-md-7 record">
                                {if $hasLicense}
                                    {$subscription.unit_price} {$CURRENCY} / {if $license.lic_users lt 1000000}{$MOD.LBL_UNIT_USER}{else}{$MOD.LBL_STUDENT}{/if} / {$MOD.LBL_MONTH}
                                {else} - {/if}
                            </div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-1 record-label text-right" style="padding: 0">&#8226; </div>
                            <div class="col-md-4 record-label">{$MOD.LBL_LICENSE_STUDENTS}:</div>
                            <div class="col-md-7 record">
                                {if $hasLicense}
                                    {if $license.lic_users lt 1000000}{$license.lic_users} {$MOD.LBL_UNIT_USER}{else}{if $license.lic_subscription lt 1000000}{$license.lic_subscription} {$MOD.LBL_STUDENT}{else}{$MOD.LBL_UNLIMITED}{/if}{/if}
                                {else} - {/if}
                            </div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-1 record-label text-right" style="padding: 0">&#8226; </div>
                            <div class="col-md-4 record-label">{$MOD.LBL_ACTIVE_STUDENTS}:
                            </div>
                            <div class="col-md-7 record">
                                {if $hasLicense}
                                    {if $license.lic_users lt 1000000}{$license.lic_activated_users} {$MOD.LBL_UNIT_USER}{else}{$license.lic_activated_subscription} {$MOD.LBL_STUDENT}{/if}
                                {else} - {/if}
                            </div>
                        </div>
                        <br>
                        {if $license.lic_status == 'EXPIRED'}
                            <div class="row pb-3 align-items-center">
                                <div class="col-md-12">
                                    <h5 class="no-padding text-danger" style="text-align: justify">{$MOD.LBL_LICENSE_EXPIRED_MSG}</h5>
                                </div>
                            </div>
                        {/if}
                        {if $isAdmin}
                            {if $license.lic_status == 'NO-LICENSE'}
                                <button data-toggle="tab" class="main_button btn btn-primary activate-trial">
                                    <i class="far fa-star" tabindex="-1" style="margin-right: 7px;"></i>{$MOD.LBL_BTN_FREE_TRIAL}
                                </button>
                            {elseif $license.lic_status == 'ACTIVE-TRIAL'}
                                <button data-toggle="tab" class="main_button btn btn-primary" id="subscription_change">
                                    <i class="far fa-exchange" tabindex="-1" style="margin-right: 7px;"></i>{$MOD.LBL_BTN_CHANGE_PLAN}
                                </button>
                            {elseif $license.lic_status == 'EXPIRED'}
                                <button data-toggle="tab" class="main_button btn btn-renew" id="subscription_renew">
                                    <i class="far fa-star" tabindex="-1" style="margin-right: 7px"></i>{$MOD.LBL_BTN_RENEW}
                                </button>
                            {elseif $license.lic_status == 'ACTIVE-ON-DEMAND'}
                                <button data-toggle="tab" class="main_button btn btn-primary" id="subscription_change">
                                    <i class="far fa-rocket" tabindex="-1" style="margin-right: 7px;"></i>{$MOD.LBL_BTN_UPGRADE_PLAN}
                                </button>
                                {if $subscription.remaining_days lt 30}
                                    <button data-toggle="tab" class="main_button btn btn-renew" id="subscription_renew">
                                        <i class="far fa-star" tabindex="-1" style="margin-right: 7px"></i>{$MOD.LBL_BTN_RENEW}
                                    </button>
                                {/if}
                            {/if}
                        {/if}
                    </div>
                    <div class="billing-info col-md-4">
                        <div class="row pb-3">
                            <div class="col-md-12">
                                <h4 class="no-padding" style="font-weight: bold;">{$MOD.LBL_TITLE_BILLING_INFO}</h4>
                            </div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_CONTACT_NAME}:</div>
                            <div class="col-md-8 record">{if $billing_info.contact_name != ''}{$billing_info.contact_name}{else} - {/if}</div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_ACCOUNT_NAME}:</div>
                            <div class="col-md-8 record">{if $billing_info.account_name != ''}{$billing_info.account_name}{else} - {/if}</div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_TAX_CODE}:</div>
                            <div class="col-md-8 record">{if $billing_info.tax_code != ''}{$billing_info.tax_code}{else} - {/if}</div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_EMAIL_ADDRESS}:</div>
                            <div class="col-md-8 record">{if $billing_info.billing_email != ''}{$billing_info.billing_email}{else} - {/if}</div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_PHONE}:</div>
                            <div class="col-md-8 record">{if $billing_info.phone_mobile != ''}{$billing_info.phone_mobile}{else} - {/if}</div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-4 record-label">{$MOD.LBL_PRIMARY_ADDRESS}:</div>
                            <div class="col-md-8 record">{if $billing_info.billing_address != ''}
                                    {$billing_info.billing_address}
                                    {if $billing_info.billing_country != ''}, {$billing_info.billing_country}{/if}
                                {else} - {/if}</div>
                        </div>
                        <br>
                        {if $isAdmin}
                        <button data-toggle="tab" class="main_button btn btn-primary" id="billing_info">
                            <i class="far fa-pencil" tabindex="-1" style="margin-right: 7px;"></i>{$MOD.LBL_BTN_EDIT}
                        </button>
                        {/if}
                    </div>
                    <div class="payment-terms col-md-4">
                        <div class="row pb-3">
                            <div class="col-md-12">
                                <h4 class="no-padding" style="font-weight: bold;">{$MOD.LBL_TITLE_PAYMENT_TERMS_INFO}</h4>
                            </div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-12 record-label">{$MOD.LBL_ACCEPTED_BY}</div>
                        </div>
                        <div class="row pb-3 align-items-center">
                            <div class="col-md-12 record-label">{$MOD.LBL_ACCEPTANCE_DATE}</div>
                        </div>
                        <br>
                        {if $isAdmin}
                            {if $hasLicense}
                            <button data-toggle="tab" class="main_button btn btn-success" data-type-pdf="terms"
                                    onclick="getPDF($(this))">
                                <i class="far fa-file-pdf" tabindex="-1" style="margin-right: 7px;"></i>{$MOD.LBL_BTN_EXPORT_PDF}
                            </button>
                            {/if}
                        {/if}
                    </div>
                </div>
            </form>
        </div>
    </div>
    {if $isAdmin}
    <div class="main-invoices">
        <textarea hidden id="datatable_lang">{$datatable_lang}</textarea>
        <div class="main_header header_container">
            <h3><b>{$MOD.LBL_PANEL_INVOICES}</b></h3>
            <button class="btn green btn-outline pull-right compare-plans" id="compare_plans">
                <i class="far fa-search" tabindex="-1" style="margin-right: 7px;"></i>
                {$MOD.LBL_BTN_COMPARE_PLAN}
            </button>
        </div>
        <div class="main_content">
            {include file='custom/modules/EMS_Settings/tpls/sub_inv.tpl'}
        </div>
    </div>
    {/if}
</div>
<br>

{*Subscription renew*}
{if $isTrial}
    {include file='custom/modules/EMS_Settings/tpls/sub_renew.tpl'}
{/if}

{if $license.lic_status == 'ACTIVE-ON-DEMAND'}
    {include file='custom/modules/EMS_Settings/tpls/sub_upgrade_plan.tpl'}
{/if}

{*Subscription change*}
{if $license.lic_status != 'ACTIVE-ON-DEMAND'}
    {include file='custom/modules/EMS_Settings/tpls/sub_sop_change.tpl'}
{/if}

{*Subscription billing detail*}
{include file='custom/modules/EMS_Settings/tpls/sub_sop_billing_detail.tpl'}

{*Subscription compare plans*}
{if $current_language == 'en_us'}
    {include file='custom/modules/EMS_Settings/tpls/pricing_table/sop_pricing_table.en_us.tpl'}
{else}
    {include file='custom/modules/EMS_Settings/tpls/pricing_table/sop_pricing_table.vn_vn.tpl'}
{/if}

{*Subscription invoice detail*}
{include file='custom/modules/EMS_Settings/tpls/sub_sop_inv_detail.tpl'}

{*Subscription payment*}
{include file='custom/modules/EMS_Settings/tpls/sub_sop_payment.tpl'}


{*JS*}
{dotb_getscript file="custom/include/javascript/DataTables/jquery.dataTables.min.js"}
{dotb_getscript file="custom/include/javascript/Bootstrap/bootstrap.min.js"}
{dotb_getscript file="custom/modules/EMS_Settings/tpls/js/slide-out-panel.js"}
{dotb_getscript file="custom/modules/EMS_Settings/tpls/js/subscription.js"}
