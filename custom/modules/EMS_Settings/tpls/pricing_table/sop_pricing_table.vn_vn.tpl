<div id="slide-out-panel-compare-plan" class="slide-out-panel">
    <header>
        <div class="caption" style="border: none;">
            <h3><b>So sánh gói</b></h3>
        </div>
    </header>
    <section class="slide-panel-content">
        {include file='custom/modules/EMS_Settings/tpls/pricing_table/pricing_table.vn_vn.tpl'}
    </section>
    <footer class="slide-out-footer">
        <div class="row form-actions" style="border-top: 1px solid lightgray;">
            <div class="mt-4 text-end">
                <button style="margin-right: 2rem;" type="button"
                        class="btn pull-right close-slide-out-panel"
                        data-id="slide-out-panel-compare-plan">
                    <i class="fas fa-times" style="margin-right: 7px"></i> Đóng
                </button>
            </div>
        </div>
    </footer>
</div>