
<div class="sub_compare_plan" style="border-top: none;">
  <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs no-padding">
      <div class="text-center">
        <div class="pricing-header">
          <div style="margin-top: 30px;">
            <p>Chọn gói dịch vụ phù hợp với bạn</p>
            <p style="margin-top: 10px;">So sánh cẩn thận các ưu đãi của các nền tảng giáo dục.
              <br><span style="color: #2BAF53; font-weight: bold;">TỰ LỰA CHỌN</span>
              và phân tích phản hồi từ người dùng thực tế.
            </p>
            <p style="margin-top: 10px;">
              Hệ thống tính phí dựa trên số lượng học viên của trường học / trung tâm<br> (không bao gồm người dùng, giáo viên và trợ giảng)<br>
              Hợp đồng <b>tối thiểu 100 học viên</b> và trong <b>1 năm</b>
            </p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
      <div class="pricing-container text-left">
        <div class="row">
          <div class="col-4 border-right">
            <h4 class="pricing-title text-center">{$product_templates[0].name}</h4>
          </div>
          <div class="col-4 border-right">
            <h4 class="pricing-title text-center">{$product_templates[1].name}</h4>
          </div>
          <div class="col-4">
            <h4 class="pricing-title text-center">{$product_templates[2].name}</h4>
          </div>
        </div>

        <div class="row">
          <div class="col-4 border-right">
            <div class="package-note text-center">
              Bộ CRM đơn giản với tiếp thị.
            </div>
          </div>
          <div class="col-4 border-right">
            <div class="package-note text-center">
              Bộ CRM chuyên nghiệp với tiếp thị.}
            </div>
          </div>
          <div class="col-4">
            <div class="package-note text-center">
              Bộ CRM không giới hạn với tiếp thị.
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-4 border-right">
            <div class="text-center">
              <span class="package-price">{$product_templates[0].list_price} {$CURRENCY}</span>
            </div>
          </div>
          <div class="col-4 border-right">
            <div class="text-center">
              <span class="package-price">{$product_templates[1].list_price} {$CURRENCY}</span>
            </div>
          </div>
          <div class="col-4">
            <div class="text-center">
              <span class="package-price">{$product_templates[2].list_price} {$CURRENCY}</span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-4 border-right">
            <div class="text-center">
              <span class="small">học viên / tháng</span>
            </div>
          </div>
          <div class="col-4 border-right">
            <div class="text-center">
              <span class="small">học viên / tháng</span>
            </div>
          </div>
          <div class="col-4">
            <div class="text-center">
              <span class="small">học viên / tháng</span>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-4 border-right">
            <div class="package-info-help">
              <p>
                <span class="bold">Số giờ đào tạo ONLINE: </span>6 giờ
              </p>
              <p>
                <b>Kênh hỗ trợ: </b>Hỗ trợ qua kênh nhắn tin (Zalo/Telegram),
                gọi trực tuyến (GG Meets, MS Team,...) trong vòng 3 giờ ngày hành chính
                kể từ lúc tiếp nhận.
              </p>
            </div>
          </div>
          <div class="col-4 border-right">
            <div class="package-info-help">
              <p>
                <span class="bold">Số giờ đào tạo ONLINE: </span>9 giờ
              </p>
              <p>
                <b>Kênh hỗ trợ: </b>Hỗ trợ qua kênh nhắn tin (Zalo/Telegram),
                gọi trực tuyến (GG Meets, MS Team,...) trong vòng 30 phút trong ngày hành chính
                kể từ lúc tiếp nhận.
              </p>
            </div>
          </div>
          <div class="col-4">
            <div class="package-info-help">
              <p>
                <span class="bold">Số giờ đào tạo ONLINE: </span>18 giờ
              </p>
              <p>
                <b>Kênh hỗ trợ: </b>Hỗ trợ tức thời 24/7 thông qua Call,
                kênh nhắn tin, gọi trực tuyến.
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-4 border-right">
            {if $hasLicense}
              <button class="btn btn-block green btn-outline btn-select"
                      type="button" data-product-name="{$product_templates[0].name}">
                Chọn gói
              </button>
            {/if}
          </div>
          <div class="col-4 border-right">
            {if $hasLicense}
              <button class="btn btn-block green btn-outline btn-select"
                      type="button" data-product-name="{$product_templates[1].name}">
                Chọn gói
              </button>
            {/if}
          </div>
          <div class="col-4">
            {if $hasLicense}
              <button class="btn btn-block green btn-outline btn-select"
                      type="button" data-product-name="{$product_templates[2].name}">
                Chọn gói
              </button>
            {/if}
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="group-feature">
      <details class="dt-group-feature default" open>
        <summary>Chi tiết Dịch vụ</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Mô hình phân quyền ĐA CHI NHÁNH. <br>Tối ưu mọi mô hình kinh doanh của các Tổ chức Giáo dục.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Sử dụng domain riêng của khách hàng.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Thay đổi nhận diện thương hiệu của khách hàng.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">TÙY CHỈNH PHẦN MỀM THEO YÊU CẦU và TÍCH HỢP VỚI CÁC HỆ THỐNG BÊN THỨ 3 (có phát sinh phí customize).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Cài đặt sổ liên lạc điện tử cho Phụ huynh / Học viên <strong>DOTB SEA</strong> (Mobile Apps / Portal Web).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>

      <details class="dt-group-feature default">
        <summary>Nhóm tính năng tối ưu hóa quản lý TUYỂN SINH</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý phễu học viên tiềm năng.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý 360 độ thông tin học viên.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý thông tin phụ huynh.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý kết quả thi đầu vào (Placement Test).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý xếp lịch học thử – Demo.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>

      <details class="dt-group-feature default">
        <summary>Nhóm tính năng quản lý ĐĂNG KÝ HỌC - THU TIỀN</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý thanh toán.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý đơn giá.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý phiếu thu.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý chiết khấu, khuyến mãi.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Báo cáo Tư vấn - Tuyển sinh (10c).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>

      <details class="dt-group-feature default">
        <summary>Nhóm tính năng quản lý Giáo vụ</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý ghi danh học viên vào lớp.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý lớp học, buổi học, giờ học.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý thời khóa biểu, xếp lịch, phân công giảng dạy.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý giáo viên, thỉnh giảng, trợ giảng.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý bảo lưu (Delay) việc học.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý hoàn phí (Refund).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý khung chương trình học (Course Outline / Kind of Course).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý chuyển Trung tâm (Moving), chuyển phí giữa các học viên (Transfer).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý khung chương trình học (Lession Plan / Syllabus).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý hợp đồng giảng dạy.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý chấm công giờ dạy / giờ admin.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý báo nghỉ, báo bù, thay đổi lịch dạy.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý bảng điểm, cấu hình bảng điểm.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Báo cáo cho bộ phận Giáo Vụ (10c).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>

      <details class="dt-group-feature default">
        <summary>Nhóm tính năng Chăm sóc Học viên</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý điểm danh, đánh giá hoạt động trên lớp, bài tập về nhà.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý phản hồi (feedback), trợ giúp học viên.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý kết quả học tập.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý học viên sắp hết phí.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý thư viện hình ảnh qua ứng dụng Mobile Apps của Học viên / phụ huynh.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý trao đổi, xin phép vắng, trợ giúp thông tin qua ứng dụng Mobile Apps của Học viên / phụ huynh.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Tự động báo kết quả học tập, thông báo điểm danh, thông báo kết quả mỗi buổi học qua SMS, Ứng dụng Mobile trên điện thoại.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Báo cáo chăm sóc học viên.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>

      <details class="dt-group-feature default">
        <summary>Nhóm tính năng Tối ưu hóa TIẾP THỊ (MARKETING)</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý kho dữ liệu khách hàng thô của Marketing (Target, Taget List).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý tích luỹ điểm (Loyalty Points), Mã người giới thiệu (Referal Code).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý chiến dịch Email Marketing.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">API đồng bộ học viên tiềm năng từ form website / landing page về CRM.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý Voucher / Loyalty.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>

      <details class="dt-group-feature default">
        <summary>Nhóm tính năng quản lý tài chính trung tâm</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý doanh thu phân bổ / Doanh thu thực hiện theo Học viên.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý doanh thu phân bổ / Doanh thu thực hiện theo Chi nhánh.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý doanh thu phân bổ / Doanh thu thực hiện theo Chương trình học.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý doanh thu bán hàng cho khách hàng mới, khách hàng cũ (New Sale / Retention Sale).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý học viên nợ phí.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý tỉ lệ học viên tái tục.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý Kho (Sách / Quà tặng).</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Quản lý phiếu chi.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>

      <details class="dt-group-feature default">
        <summary>Tiện ích ĐẶC QUYỀN KHÔNG GIỚI HẠN - UNLIMITED</summary>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[0].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[1].name}</div>
          </div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
            <div class="product-name">{$product_templates[2].name}</div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">MIỄN PHÍ Chuyên gia hỗ trợ xuyên suốt quá trình chuyển đổi sử dụng hệ thống mới, đảm bảo thành công dự án.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">MIỄN PHÍ chuyển dữ liệu lên hệ thống mới.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Cài đặt Bộ 40 Quy trình tự động chăm sóc khách hàng theo quy trình.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Cài đặt Bộ 30 báo cáo chuyên sâu cho lĩnh vực đào tạo.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">MIỄN PHÍ cài đặt sổ liên lạc điện tử theo thương hiệu riêng.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">MIỄN PHÍ sử dụng Metrikal Apps.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">MIỄN PHÍ tích hợp với tất cả các bên thứ 3 là đối tác của DOTB.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">MIỄN PHÍ tất cả các sự kiện, hội thảo, webinar, đào tạo do DOTB tổ chức.</div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
          <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
        </div>
      </details>
    </div>
  </div>
</div>
