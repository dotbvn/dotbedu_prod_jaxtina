
<div class="sub_compare_plan" style="border-top: none;">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs no-padding">
            <div class="text-center">
                <div class="pricing-header">
                    <div style="margin-top: 30px;">
                        <p>Choose the package that fits you</p>
                        <p style="margin-top: 10px;">Carefully compare the offer of educational platforms.<br>
                            <span style="color: #2BAF53; font-weight: bold;">CHOOSE ON YOUR OWN</span>
                            and analyze the feedback of real users.
                        </p>
                        <p style="margin-top: 10px;">
                            The fee system is based on the number of students enrolled in the school / center
                            <br>(excluding users, teachers, and assistants).
                            <br>The contract must have at <b>least 5 users</b>
                            and be for <b>a period of 1 year</b>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <div class="pricing-container text-left">
                <div class="row">
                    <div class="col-4 border-right">
                        <h4 class="pricing-title text-center">{$product_templates[0].name}</h4>
                    </div>
                    <div class="col-4 border-right">
                        <h4 class="pricing-title text-center">{$product_templates[1].name}</h4>
                    </div>
                    <div class="col-4">
                        <h4 class="pricing-title text-center">{$product_templates[2].name}</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4 border-right">
                        <div class="package-note text-center">
                            Simple CRM with marketing.
                        </div>
                    </div>
                    <div class="col-4 border-right">
                        <div class="package-note text-center">
                            Professional CRM with marketing.
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="package-note text-center">
                            Unlimited CRM with marketing.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4 border-right">
                        <div class="text-center">
                            <span class="package-price">{$product_templates[0].list_price} {$CURRENCY}</span>
                        </div>
                    </div>
                    <div class="col-4 border-right">
                        <div class="text-center">
                            <span class="package-price">{$product_templates[1].list_price} {$CURRENCY}</span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="text-center">
                            <span class="package-price">{$product_templates[2].list_price} {$CURRENCY}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4 border-right">
                        <div class="text-center">
                            <span class="small">student / month</span>
                        </div>
                    </div>
                    <div class="col-4 border-right">
                        <div class="text-center">
                            <span class="small">student / month</span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="text-center">
                            <span class="small">student / month</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4 border-right">
                        <div class="package-info-help">
                            <p><span class="bold">ONLINE Training Hours: </span>6 hours</p>
                            <p>
                                <b>Support channels: </b>Support via messaging apps (Zalo / Telegram),
                                online calls (Google Meet, MS Teams, etc.) within 3 hours on business days
                                from the time of receipt.
                            </p>
                        </div>
                    </div>
                    <div class="col-4 border-right">
                        <div class="package-info-help">
                            <p><span class="bold">ONLINE Training Hours: </span>9 hours</p>
                            <p>
                                <b>Support channels: </b>Support via messaging apps (Zalo / Telegram),
                                online calls (Google Meet, MS Teams, etc.) within 30 minutes on business days
                                from the time of receipt.
                            </p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="package-info-help">
                            <p><span class="bold">ONLINE Training Hours: </span>18 hours</p>
                            <p>
                                <b>Support channels: </b>Immediate 24/7 support via phone,
                                messaging apps, online calls.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4 border-right">
                        {if $hasLicense}
                            <button class="btn btn-block green btn-outline btn-select"
                                    type="button" data-product-name="{$product_templates[0].name}">
                                Select package
                            </button>
                        {/if}
                    </div>
                    <div class="col-4 border-right">
                        {if $hasLicense}
                            <button class="btn btn-block green btn-outline btn-select"
                                    type="button" data-product-name="{$product_templates[1].name}">
                                Select package
                            </button>
                        {/if}
                    </div>
                    <div class="col-4">
                        {if $hasLicense}
                            <button class="btn btn-block green btn-outline btn-select"
                                    type="button" data-product-name="{$product_templates[2].name}">
                                Select package
                            </button>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="group-feature">
            <details class="dt-group-feature default" open>
                <summary>Service details</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">MULTI-BRANCH authorization model. <span class="small">Optimizing every business model for Educational Organizations.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Use customer's own domain.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Customize customer's brand identity.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">CUSTOMIZE SOFTWARE ACCORDING TO REQUIREMENTS and INTEGRATE WITH THIRD-PARTY SYSTEMS (customization fees may apply).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Installation of electronic contact books for Parents / Students of <b>DOTB SEA</b> (Mobile Apps / Web Portal).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>

            <details class="dt-group-feature default">
                <summary>The feature set for optimized ADMISSIONS management team</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage potential student funnel.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage 360-degree student information.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage parent information.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Management of placement test results (Placement Test).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage trial classes - Demo.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>

            <details class="dt-group-feature default">
                <summary>The feature set for managing ENROLLMENT - FEE COLLECTION</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Payment management.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Price management.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Receipt management.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Discount and promotion management.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Advisory - Admissions Report (10r).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>

            <details class="dt-group-feature default">
                <summary>The feature set for managing ACADEMIC AFFAIRS</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage student enrollment in classes.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage classes, sessions, hours of study.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage schedules, timetables, teaching assignments.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage teachers, guest lecturers, teaching assistants.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage delays in learning.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage refunds (Refund).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage course framework (Course Outline / Kind of Course).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage Center transfers (Moving), transfer fees between students (Transfer).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage course framework (Lession Plan / Syllabus).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage teaching contracts.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage teaching / admin hours attendance.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage leave requests, make-up requests, schedule changes.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage scoreboards, score configuration.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Report to the Academic Affairs department.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>

            <details class="dt-group-feature default">
                <summary>Student Care Feature Group</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage attendance, assess classroom activities, homework.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage feedback, student assistance.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage learning outcomes.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage students nearing payment due.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage image library through Mobile Apps of students / parents.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage exchanges, request for leave, support information through Mobile Apps of students / parents.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Automatically notify learning outcomes, attendance notifications, results notifications after each class via SMS, Mobile App on phones.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Student Care Report (10r).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>

            <details class="dt-group-feature default">
                <summary>Marketing Optimization Feature Group</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage Marketing\'s raw customer data (Target, Target List).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage loyalty points, Referral Code.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Email Marketing Campaign Management.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">API synchronize potential students from website / landing page to CRM.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage Vouchers / Loyalty.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>

            <details class="dt-group-feature default">
                <summary>Financial Management Feature Group</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage revenue allocation / actual revenue by student.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage revenue allocation / actual revenue by branch.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage revenue allocation / actual revenue by program.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage sales revenue for new customers, old customers (New Sale / Retention Sale).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage student fee arrears.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage student retention rate.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage Inventory (Books / Gifts).</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Manage expense vouchers.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>

            <details class="dt-group-feature default">
                <summary>UNLIMITED Exclusive Benefits</summary>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs"></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[0].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[1].name}</div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center">
                        <div class="product-name">{$product_templates[2].name}</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">FREE Expert support throughout the transition to the new system, ensuring project success.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">FREE data migration to the new system.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Installation of 40 automated customer care processes.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">Installation of 30 in-depth training field reports.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">FREE installation of branded email templates.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">FREE use of Metrikal Apps.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">FREE integration with all third-party partners of DOTB.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs border-right">FREE attendance to all events, seminars, webinars, and training organized by DOTB.</div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-minus"></i></div>
                    <div class="col-lg-2 col-md-2 col-4 d-flex justify-content-center align-items-center border-right"><i class="fa fa-check-circle"></i></div>
                </div>
            </details>
        </div>
    </div>
</div>
