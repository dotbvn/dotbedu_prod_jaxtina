<table class="table table-bordered table-hover no-footer dtr-inline"
       role="grid" id="datatable_invoice" style="border-bottom: none;">
    <thead>
    <tr role="row">
        <th style="width: 10%;border-bottom: none; text-align: center;" data-order="desc">{$MOD.LBL_INVOICE_NUMBER}</th>
        <th style="width: 30%;border-bottom: none;">{$MOD.LBL_PRODUCT_NAME}</th>
        <th style="width: 10%;border-bottom: none; text-align: center;">{$MOD.LBL_INVOICE_DATE}</th>
        <th style="width: 10%;border-bottom: none; text-align: center;">{$MOD.LBL_PAYMENT_DUE_DATE}</th>
        <th style="width: 10%;border-bottom: none; text-align: center;">{$MOD.LBL_GRAND_TOTAL}</th>
        <th style="width: 10%;border-bottom: none; text-align: center;">{$MOD.LBL_PAID_AMOUNT}</th>
        <th style="width: 12%;border-bottom: none; text-align: center;">{$MOD.LBL_STATUS}</th>
        <th style="width: 8%;border-bottom: none;" data-orderable="false"></th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$invoices key=quote_id item=invoice}
        <tr data-invoice-id="{$quote_id}">
            <td style="text-align: center;"><a href="#" onclick="return getInvoiceDetail($(this))">{$invoice.quote_name}</a></td>
            <td>
                {foreach from=$invoice.products item=product}
                <b style="color: #c00000;">{$product.name}</b>
                <span>&#8226; {$product.description}</span>
                {/foreach}
            </td>
            <td style="text-align: center;">{$invoice.issue_date}</td>
            <td style="text-align: center;">{$invoice.expired_date}</td>
            <td style="text-align: right;">{$invoice.total} {$CURRENCY}</td>
            <td style="text-align: right;">{$invoice.paid_amount} {$CURRENCY}</td>
            <td>
                {if $invoice.status == 'Sent'}
                    <span class="label-orange">{$MOD.LBL_IN_PROCESS}</span>
                {elseif  $invoice.status == 'Partial Paid'}
                    <span class="label-success">{$MOD.LBL_PARTIAL_PAID}</span>
                {elseif  $invoice.status == 'Paid in Full'}
                    <span class="label-success">{$MOD.LBL_PAID_IN_FULL}</span>
                {elseif  $invoice.status == 'Overdue'}
                    <span class="label-info">{$MOD.LBL_OVERDUE}</span>
                {/if}
            </td>
            <td style="text-align: center;">
                <div class="btn-group">
                    <button class="btn btn-xs btn-droption-action" type="button" data-toggle="dropdown">
                        {$MOD.LBL_DRL_ACTIONS} <i class="fas fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        {if $invoice.status != 'Paid in Full'}
                            <li>
                                <a onclick="return showPaymentInfo($(this))" href="#" data-invoice-id="{$quote_id}">
                                    <i class="fas fa-credit-card"></i> {$MOD.LBL_DRL_PAY_ONLINE}
                                </a>
                            </li>
                            <li class="divider"></li>
                        {/if}
                        <li><a onclick="return getInvoiceDetail($(this))" href="#">
                                <i class="fas fa-eye"></i> {$MOD.LBL_DRL_VIEW_DETAILS}</a>
                        </li>
                        <li><a href="#" onclick="return getPDF($(this))"
                               data-invoice-id="{$quote_id}" data-type-pdf="quote">
                                <i class="fas fa-file-pdf"></i>&nbsp;&nbsp;{$MOD.LBL_DRL_EXPORT_PDF}</a>
                        </li>
                        <li class="divider" style="margin-bottom: 4px"></li>
                        <li><a onclick="getReceipts($(this), 'status_paid')" href="#">
                                <i class="fas fa-clock"></i>&nbsp;{$MOD.LBL_BTN_PAYMENT_HISTORY}</a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
<div id="modalHistory" class="modal quick-create" style="height: fit-content">
    <div class="modal-container">
        <div class="modal-container-header">
            <div class="modal-header-title">
                <h4 class="modal-container-title">{$MOD.LBL_RECEIPT_LIST}</h4>
            </div>
        </div>
        <div class="modal-container-body rtf">
            <table class="edit table table-bordered table-hover no-footer dtr-inline table-border" role="grid" id="datatable_receipts" style="margin-bottom: 1.5rem;">
                <thead>
                    <tr>
                        <th width="5%">{$MOD.LBL_RECEIPT_NO}</th>
                        <th width="15%">{$MOD.LBL_RECEIPT_NAME}</th>
                        <th width="20%">{$MOD.LBL_RECEIPT_DATE}</th>
                        <th width="20%">{$MOD.LBL_RECEIPT_STATUS}</th>
                        <th width="20%">{$MOD.LBL_METHOD}</th>
                        <th width="20%">{$MOD.LBL_RECEIPT_AMOUNT}</th>
                    </tr>
                </thead>
                <tbody id="historyPayment"></tbody>
            </table>
        </div>
        <div class="modal-container-footer">
            <button id="exit" class="btn btn-light" style="font-size: 1.3rem;" onclick="$('.modal').removeClass('show');$('.modal').modal('hide');">Close</button>
        </div>
    </div>
</div>