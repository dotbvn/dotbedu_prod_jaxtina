<div class="sub_payment px-5 mx-2">
    <div class="row pb-4 align-items-center">
        <div class="col-md-8">
            <h3 class="bold font-blue-dark uppercase">{$MOD.LBL_INVOICE}</h3>
        </div>
        <div class="col-md-4">
            <div class="text-end">
                <button class="btn btn-success payment_view_pdf" style="margin-top: 11px" onclick="return getPDF($(this))" data-type-pdf="quote">
                    <i class="fas fa-download"></i> {$MOD.LBL_BTN_EXPORT_PDF}
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <div class="row pb-2 align-items-center">
                        <div class="col-md-3">
                            <div class="row align-items-center">
                                <div class="col-md-9">
                                    <div class="record-label pb-2" style="font-size: 1.25rem">{$MOD.LBL_INVOICE_NUMBER}</div>
                                    <div class="record" style="font-size: 1.5rem"><strong><span class="payment_name"></span></strong></div>
                                </div>
                                <div class="col-md-3 vertical-divider"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row align-items-center">
                                <div class="col-md-9" style="text-align: center;">
                                    <div class="record-label pb-2" style="font-size: 1.25rem">{$MOD.LBL_INVOICE_DATE}</div>
                                    <div class="record" style="font-size: 1.5rem"><strong><span class="payment_date"></span></strong></div>
                                </div>
                                <div class="col-md-3 vertical-divider"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row align-items-center">
                                <div class="col-md-9" style="text-align: center;">
                                    <div class="record-label pb-2" style="font-size: 1.25rem">{$MOD.LBL_PAYMENT_DUE_DATE}</div>
                                    <div class="record" style="font-size: 1.5rem"><strong><span class="payment_due_date"></span></strong></div>
                                </div>
                                <div class="col-md-3 vertical-divider"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row align-items-center">
                                <div class="col-md-9" style="text-align: center;">
                                    <div class="record-label pb-2" style="font-size: 1.25rem">{$MOD.LBL_NUMBER_OF_PAYMENT}</div>
                                    <div class="record" style="font-size: 1.5rem"><strong><span class="number_of_payment"></span></strong></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-4">
                <div class="col-md-12">
                    <div class="row pb-2 align-items-center">
                        <div class="pb-3">
                            <span class="px-3 py-2 bold" style="font-size: 1.3rem; border-radius: 5px; background-color: #F2FAF3; color: #00954F;">{$MOD.LBL_PAYER}</span>
                        </div>
                        <h3 class="bold payment_contact_name"></h3>
                    </div>
                    <div class="row pb-3 align-items-center">
                        <div class="col-md-2 record-label">{$MOD.LBL_TAX_CODE}:</div>
                        <div class="col-md-9 record payment_tax_code"></div>
                    </div>
                    <div class="row pb-3 align-items-center">
                        <div class="col-md-2 record-label">{$MOD.LBL_EMAIL_ADDRESS}:</div>
                        <div class="col-md-9 record payment_billing_email"></div>
                    </div>
                    <div class="row pb-3 align-items-center">
                        <div class="col-md-2 record-label">{$MOD.LBL_PHONE}:</div>
                        <div class="col-md-9 record payment_phone_mobile"></div>
                    </div>
                    <div class="row pb-3">
                        <div class="col-md-2 record-label">{$MOD.LBL_PRIMARY_ADDRESS}:</div>
                        <div class="col-md-9 record payment_billing_address"></div>
                    </div>
                </div>
            </div>
            <div class="row pt-1">
                <span class="text-end pb-2 record" style="font-size: 10pt; font-style: italic;">({$MOD.LBL_UNIT}: {$CURRENCY})</span>
                <table class="table table-bordered table-detail-payment" role="grid">
                    <thead>
                    <tr role="row" class="table-light">
                        <th style="width: 3%;" class="center">#</th>
                        <th style="width: 22%;">{$MOD.LBL_PRODUCT_NAME}</th>
                        <th style="width: 15%;">{$MOD.LBL_QUANTITY}</th>
                        <th style="width: 10%;">{$MOD.LBL_UNIT_PRICE}</th>
                        <th style="width: 20%;">{$MOD.LBL_SERVICE_DURATION}</th>
                        <th style="width: 10%;">{$MOD.LBL_DISCOUNT}</th>
                        <th style="width: 10%;">{$MOD.LBL_SPONSOR_AMOUNT}</th>
                        <th style="width: 10%;">{$MOD.LBL_NET_AMOUNT}</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="row justify-content-end">
                <div class="col-md-7">
                    <div class="row pb-3 align-items-center text-end">
                        <div class="col-md-5 record-label">{$MOD.LBL_SPONSOR_AMOUNT}:</div>
                        <div class="col-md-7 record bold d-flex justify-content-between">
                            <strong class="p-sponsor-name"></strong>
                            <span class="p-sponsor-value">0</span>
                        </div>
                    </div>
                    <div class="row pb-3 align-items-center text-end">
                        <div class="col-md-5 record-label">{$MOD.LBL_SUBTOTAL}:</div>
                        <div class="col-md-7 record bold">
                            <span class="p-subtotal"></span>
                        </div>
                    </div>
                    <div class="row pb-3 align-items-center text-end">
                        <div class="col-md-5 record-label">{$MOD.LBL_VAT_AMOUNT}:</div>
                        <div class="col-md-7 record bold">
                            <span class="p-taxrate-value">0</span>
                        </div>
                    </div>
                    <div class="row pb-3 align-items-center text-end">
                        <div class="col-md-5 record-label">{$MOD.LBL_GRAND_TOTAL}:</div>
                        <div class="col-md-7 record bold">
                            <span class="p-total"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="flex: 0 0 auto; width: 3%"></div>
        <div style="flex: 0 0 auto; width: 30%">
            <div class="row align-items-center text-center">
                <h5>{$MOD.LBL_QR_DESCRIPTION}</h5>
            </div>
            <div class="row pt-4 justify-content-center align-items-center">
                <img class="qr-payment" style="width: 240px;" alt="" src="">
            </div>
            <div class="row pt-4 align-items-center">
                <table class="table table-bordered mx-auto bank-info">
                    <tr>
                        <td style="width: 40%"><b>{$MOD.LBL_ACCOUNT_HOLDER_NAME}:</td>
                        <td><span class="p-transfer-account-name"></span></td>
                    </tr>
                    <tr>
                        <td><b>{$MOD.LBL_ACCOUNT_NUMBER}:</b></td>
                        <td><span class="p-transfer-account-number"></span></td>
                    </tr>
                    <tr>
                        <td><b>{$MOD.LBL_BANK_NAME}:</b></td>
                        <td><span class="p-transfer-bank"></span></td>
                    </tr>
                    <tr>
                        <td><b>{$MOD.LBL_PAYMENT_AMOUNT}:</b></td>
                        <td><span class="p-transfer-amount"></span> {$CURRENCY}</td>
                    </tr>
                    <tr>
                        <td><b>{$MOD.LBL_CONTENT}:</b></td>
                        <td><span class="p-transfer-content"></span></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>