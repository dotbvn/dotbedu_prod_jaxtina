<div class="sub_inv_detail px-5 mx-2">
    <div class="row">
        <div class="col-md-6">
            <div class="row pb-4 align-items-center">
                <h3 class="bold font-blue-dark uppercase">{$MOD.LBL_INVOICE}</h3>
            </div>
            <div class="row pb-2 align-items-center">
                <div class="col-md-3">
                    <div class="row align-items-center">
                        <div class="col-md-9">
                            <div class="record-label pb-2" style="font-size: 1.25rem">{$MOD.LBL_INVOICE_NUMBER}</div>
                            <div class="record" style="font-size: 1.5rem"><strong><span class="inv-no"></span></strong></div>
                        </div>
                        <div class="col-md-3 vertical-divider"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row align-items-center">
                        <div class="col-md-9" style="text-align: center;">
                            <div class="record-label pb-2" style="font-size: 1.25rem">{$MOD.LBL_INVOICE_DATE}</div>
                            <div class="record" style="font-size: 1.5rem"><strong><span class="inv-date"></span></strong></div>
                        </div>
                        <div class="col-md-3 vertical-divider"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row align-items-center">
                        <div class="col-md-9" style="text-align: center;">
                            <div class="record-label pb-2" style="font-size: 1.25rem">{$MOD.LBL_PAYMENT_DUE_DATE}</div>
                            <div class="record" style="font-size: 1.5rem"><strong><span class="inv-due-date"></span></strong></div>
                        </div>
                        <div class="col-md-3 vertical-divider"></div>
                    </div>
                </div>
                <div class="col-md-3" style="text-align: center;">
                    <div class="row align-items-center">
                        <div class="col-md-9">
                            <div class="record-label pb-2" style="font-size: 1.25rem; padding:0">{$MOD.LBL_NUMBER_OF_PAYMENT}</div>
                            <div class="record" style="font-size: 1.5rem"><strong><span class="inv-number-of-payment"></span></strong></div>
                        </div>
                        <div class="col-md-1 icon-view-payments" style="align-self: flex-start; padding:0;" onclick="getReceipts($(this))">
                            <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="View payments" aria-hidden="true"
                               style="vertical-align: top;"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 text-end">
            <button class="btn btn-primary payment inv-payment" onclick="showPaymentInfo($(this))">
                <i class="fas fa-credit-card"></i> {$MOD.LBL_DRL_PAY_ONLINE}
            </button>
            <button class="btn btn-success inv-view-pdf" onclick="getPDF($(this))" data-type-pdf="quote">
                <i class="fas fa-file-pdf"></i> {$MOD.LBL_DRL_EXPORT_PDF}
            </button>
        </div>
    </div>
    <div class="row pt-5">
        <div class="col-md-5">
            <div class="row pb-2 align-items-center">
                <div class="d-flex pb-3" style="gap: .8rem;">
                    <span class="px-3 py-2 bold" style="font-size: 1.3rem; border-radius: 5px; background-color: #F9F6FF; color: #854EEB;">{$MOD.LBL_PAYEE}</span>
                    <h3 class="bold">Công ty Cổ phần Công nghệ Giáo dục DotB</h3>
                </div>
            </div>
            <div class="row pb-3 align-items-center">
                <div class="col-md-3 record-label">{$MOD.LBL_TAX_CODE}:</div>
                <div class="col-md-9 record">0315359346</div>
            </div>
            <div class="row pb-3 align-items-center">
                <div class="col-md-3 record-label">{$MOD.LBL_EMAIL_ADDRESS}:</div>
                <div class="col-md-9 record">contact@dotb.vn</div>
            </div>
            <div class="row pb-3 align-items-center">
                <div class="col-md-3 record-label">{$MOD.LBL_PHONE}:</div>
                <div class="col-md-9 record">0961269091</div>
            </div>
            <div class="row pb-3">
                <div class="col-md-3 record-label">{$MOD.LBL_PRIMARY_ADDRESS}:</div>
                <div class="col-md-9 record">Phòng 102, Tòa Nhà A, Khu Công Nghệ Phần Mềm ĐHQG-HCM, Đường Võ Trường Toản, Khu Phố 6, Phường Linh Trung, Thành phố Thủ Đức, Thành phố Hồ Chí Minh, Việt Nam.</div>
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5">
            <div class="row pb-2 align-items-center">
                <div class="d-flex pb-3" style="gap: .8rem;">
                    <span class="px-3 py-2 bold" style="font-size: 1.3rem; border-radius: 5px; background-color: #F2FAF3; color: #00954F;">{$MOD.LBL_PAYER}</span>
                    <h3 class="bold inv-payer-contact-name">{$billing_info.contact_name}</h3>
                </div>
            </div>
            <div class="row pb-3 align-items-center">
                <div class="col-md-3 record-label">{$MOD.LBL_TAX_CODE}:</div>
                <div class="col-md-9 record inv-payer-tax-code">{$billing_info.tax_code}</div>
            </div>
            <div class="row pb-3 align-items-center">
                <div class="col-md-3 record-label">{$MOD.LBL_EMAIL_ADDRESS}:</div>
                <div class="col-md-9 record inv-payer-billing-email">{$billing_info.billing_email}</div>
            </div>
            <div class="row pb-3 align-items-center">
                <div class="col-md-3 record-label">{$MOD.LBL_PHONE}:</div>
                <div class="col-md-9 record inv-payer-phone-mobile">{$billing_info.phone_mobile}</div>
            </div>
            <div class="row pb-3">
                <div class="col-md-3 record-label">{$MOD.LBL_PRIMARY_ADDRESS}:</div>
                <div class="col-md-9 record inv-payer-billing-address">{$billing_info.billing_address}</div>
            </div>
        </div>
    </div>
    <div class="row pt-4">
        <span class="text-end pb-2 record" style="font-size: 10pt; font-style: italic;">({$MOD.LBL_UNIT}: {$CURRENCY})</span>
        <table class="table table-bordered table-detail-invoice" role="grid">
            <thead>
                <tr role="row" class="table-light">
                    <th style="width: 2%;" class="center">#</th>
                    <th style="width: 28%;">{$MOD.LBL_PRODUCT_NAME}</th>
                    <th style="width: 10%;">{$MOD.LBL_QUANTITY}</th>
                    <th style="width: 10%;">{$MOD.LBL_UNIT_PRICE}</th>
                    <th style="width: 17%;">{$MOD.LBL_SERVICE_DURATION}</th>
                    <th style="width: 11%;">{$MOD.LBL_DISCOUNT}</th>
                    <th style="width: 11%;">{$MOD.LBL_SPONSOR_AMOUNT}</th>
                    <th style="width: 11%;">{$MOD.LBL_NET_AMOUNT}</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <div class="row justify-content-end">
        <div class="col-md-6">
            <div class="row pb-3 align-items-center text-end">
                <div class="col-md-6 record-label">{$MOD.LBL_SPONSOR_AMOUNT}:</div>
                <div class="col-md-6 record voucher-code bold d-flex justify-content-between">
                    <div class="d-flex">
                        <input type="text" class="input-voucher-code h-100">
                        <button class="btn btn-primary search-voucher-code" type="button"
                                onclick="checkVoucher($(this))"><i class="fas fa-search"></i>
                        </button>
                    </div>
                    <span class="mt-auto mb-auto record bold">0</span>
                </div>
            </div>
            <div class="row pb-3 align-items-center text-end">
                <div class="col-md-6 record-label">{$MOD.LBL_SUBTOTAL}:</div>
                <div class="col-md-6 record bold">
                    <span class="inv-subtotal"></span>
                </div>
            </div>
            <div class="row pb-3 align-items-center text-end">
                <div class="col-md-6 record-label">{$MOD.LBL_VAT_AMOUNT}:</div>
                <div class="col-md-6 record bold">
                    <span class="inv-vat-value">0</span>
                </div>
            </div>
            <div class="row pb-3 align-items-center text-end">
                <div class="col-md-6 record-label">{$MOD.LBL_GRAND_TOTAL}:</div>
                <div class="col-md-6 record bold">
                    <span class="inv-total"></span>
                </div>
            </div>
        </div>
    </div>
</div>