<div id="slide-out-panel-invoice-detail" class="slide-out-panel" >
    <section class="slide-panel-content" style="height: 90%;">
        {include file='custom/modules/EMS_Settings/tpls/sub_inv_detail.tpl'}
    </section>
    <footer class="slide-out-footer" style="border-top: 1px solid lightgray;">
        <div class="form-actions">
            <div class="row">
                <div class="text-end">
                    <button type="button"
                            class="btn close-slide-out-panel"
                            style="margin-right: 2.5rem;"
                            data-id="slide-out-panel-invoice-detail">
                        <i class="fas fa-times" style="margin-right: 7px;"></i>
                        {$MOD.LBL_BTN_CLOSE}
                    </button>
                </div>
            </div>
        </div>
    </footer>
</div>