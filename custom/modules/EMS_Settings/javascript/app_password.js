$(document).ready(function () {
    $('.app_pwd').each(function( index, element ) {
        validatePwd($(this))
    });

    $('.apply_new_pwd').click(function(){
        reset_all_pwd($(this));
    });
});
function validatePwd(_this){
    var pwd = _this.val();
    var pwd_length   = _this.closest('td').find('.pwd_length');
    var pwd_strength = _this.closest('td').find('.pwd_strength');
    var number = /([0-9])/;
    var alphabets = /([a-zA-Z])/;
    var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
    if(pwd.length == 0){
        pwd_length.text('').css("color", "#444444");
        pwd_strength.hide();
    }
    else if (pwd.length < 6) {
        pwd_length.text(app.lang.get('LBL_APPS_PWD_SHORD', 'Administration')).css("color", "#444444");
        pwd_strength.show();
    } else {
        if ((pwd.match(number) && pwd.match(alphabets)) || (pwd.match(special_characters) && pwd.match(alphabets))) {
            pwd_length.text(app.lang.get('LBL_APPS_PWD_STRONG', 'Administration')).css("color", "#009600");
            pwd_strength.show();
        } else {
            pwd_length.text(app.lang.get('LBL_APPS_PWD_GOOD', 'Administration')).css("color", "#009600");
            pwd_strength.show();
        }
    }
}

function reset_all_pwd(_tbt){
    var user_type = _tbt.attr('user_type');
    var password  = _tbt.closest('tr').find('.app_pwd');
    if(password.val().length < 6){
         toastr.error(app.lang.get('LBL_APPS_ERR_PWD_LENGTH', 'Administration'));
         password.effect("highlight", {color: '#ff9933'}, 1000);
         return false;
    }
    $.confirm({
        title: app.lang.get('LBL_APPS_PWD_CONFIRM', 'Administration'),
        content: app.lang.get('LBL_APPS_PWD_CONFIRM_DES', 'Administration').replace("{user_type}", app.lang.get('LBL_APPS_PWD_'+user_type.toUpperCase(), 'Administration')),
        buttons: {
            "OK": {
                btnClass: 'btn-blue',
                action: function(){
                    $.ajax({
                        type: "POST",
                        url: "index.php?module=EMS_Settings&action=handle_ajax&dotb_body_only=true",
                        data: {
                            type      : "reset_all_pwd",
                            user_type : user_type,
                            password  : password.val(),
                        },
                        dataType: "json",
                        success: function (data) {
                            if(data.success == 1){
                                toastr.success(data.label);
                                password.val('').trigger('keyup');
                            }
                            else toastr.error(data.label);
                        },
                    });
                }
            },
            "Cancel": {
                text  : app.lang.get('LBL_CANCEL_BUTTON_LABEL'),
                action: function(){

                }
            },
        }
    });

}