<?php

require_once('custom/include/utils/DotbWebhook.php');

class subscriptionHelper {

    function getSubscription($tenant_id = '') {
        $webhook = new DotbWebhook();
        $data = ['tenant_id' => $tenant_id];
        $result = $webhook->callQuyettamAPI('tenant/subscription/get', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }

        if(empty($data['data']['billing_info']['billing_address']) || empty($data['data']['billing_info']['billing_country'])) {
            //get defaut Courtry by API
            $client_ip = self::getVisIPAddr();
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=$client_ip"));
            $data['data']['billing_info']['billing_country'] = empty($data['data']['billing_info']['billing_country'])
                ? strtoupper($ipdat->geoplugin_countryName)
                : $data['data']['billing_info']['billing_country'];
            $data['data']['billing_info']['billing_address'] = empty($data['data']['billing_info']['billing_address'])
                ? $ipdat->geoplugin_city
                : $data['data']['billing_info']['billing_address'];
        }

        //format number & date
        global $timedate;
        if(!empty($data['data']['subscription'])) {
            $data['data']['subscription']['unit_price'] = format_number($data['data']['subscription']['unit_price']);
            $data['data']['subscription']['total_after_discount'] = format_number($data['data']['subscription']['total_after_discount']);
        }
        if(!empty($data['data']['invoices'])) {
            foreach ($data['data']['invoices'] as &$invoice) {
                $invoice['total'] = format_number($invoice['total']);
                $invoice['paid_amount'] = format_number($invoice['paid_amount']);
                $invoice['issue_date'] = $timedate->to_display_date($invoice['issue_date'], false);
                $invoice['expired_date'] = $timedate->to_display_date($invoice['expired_date'], false);
            }
        }

        return $data;
    }

    function getProductTemplate() {
        $webhook = new DotbWebhook();
        $result = $webhook->callQuyettamAPI('tenant/product-templates/get', 'GET');
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }

        //format number
        foreach ($data['data']['product_templates'] as &$product) {
            $product['default_discount'] = ($product['pricing_formula'] == 'PercentageDiscount') ? round($product['pricing_factor'], 1) . '%' : '';
            $product['list_price'] = format_number($product['list_price']);
            $product['discount_price'] = format_number($product['discount_price']);
        }

        return $data;
    }

    function getInvoiceDetails($tenant_id = '', $quote_id = '') {
        $webhook = new DotbWebhook();
        $data = [
            'tenant_id' => $tenant_id,
            'quote_id' => $quote_id
        ];
        $result = $webhook->callQuyettamAPI('tenant/invoice/get', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }
        return $data;
    }

    function updateBillingInfo($tenant_id = '', $data = array()) {
        $webhook = new DotbWebhook();
        $data = [
            'tenant_id' => $tenant_id,
            'contact_name' => $data['contact_name'],
            'phone_mobile' => $data['phone_mobile'],
            'billing_email' => $data['billing_email'],
            'billing_address' => $data['billing_address'],
            'billing_country' => $data['billing_country'],
            'account_name' => $data['account_name'],
            'tax_code' => $data['tax_code']
        ];
        $result = $webhook->callQuyettamAPI('tenant/billing-info/update', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }
        return $data;
    }

    function exportPdfTemplate($tenant_id = '', $module = '', $record = '', $dotbpdf = '') {
        $webhook = new DotbWebhook();
        $data = [
            'tenant_id' => $tenant_id,
            'module' => $module,
            'record' => $record,
            'dotbpdf' => $dotbpdf
        ];
        $result = $webhook->callQuyettamAPI('tenant/export-pdf-template', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }
        return $data;
    }

    function updateSubscription($tenant_id = '', $action = '', $product_id = '', $quantity = '', $service_duration_unit = '') {
        $webhook = new DotbWebhook();
        $data = [
            'tenant_id' => $tenant_id,
            'action' => $action,
            'product_id' => $product_id,
            'quantity' => $quantity,
            'service_duration_unit' => $service_duration_unit
        ];
        $result = $webhook->callQuyettamAPI('tenant/subscription/update', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }

        if($action == 'activate-trial')
            loadLicense();

        return $data;
    }

    function checkVoucher($tenant_id = '', $code = '', $product_template_ids = '') {
        $webhook = new DotbWebhook();
        $data = [
            'tenant_id' => $tenant_id,
            'code' => $code,
            'product_template_ids' => $product_template_ids
        ];
        $result = $webhook->callQuyettamAPI('voucher/validate', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }
        return $data;
    }

    function updateInvoice($tenant_id = '', $quote_id = '', $status = '', $voucher_id = '') {
        $webhook = new DotbWebhook();
        $data = [
            'tenant_id' => $tenant_id,
            'quote_id' => $quote_id,
            'status' => $status,
            'voucher_id' => empty($voucher_id) ? '' : $voucher_id
        ];
        $result = $webhook->callQuyettamAPI('tenant/invoice/update', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }
        return $data;
    }

    //PHP code to extract IP
    function getVisIpAddr() {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])) $ip= $_SERVER['HTTP_CLIENT_IP'];

        elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip= $_SERVER['HTTP_X_FORWARDED_FOR'];

        else $ip= $_SERVER['REMOTE_ADDR'];

        if(empty($ip) || $ip == '::1') $ip = '123.30.53.15'; //sample ip Vietnam

        return $ip;
    }

    function getReceipts($tenant_id, $quote_id, $filter) {
        $webhook = new DotbWebhook();
        $data = [
            'tenant_id' => $tenant_id,
            'quote_id' => $quote_id,
            'filter' => $filter,
        ];
        $result = $webhook->callQuyettamAPI('tenant/invoice/get-receipts', 'POST', $data, array('Content-Type: multipart/form-data'));
        $data = $result['data'];
        if(empty($data) || !$data['success']) {
            return array(
                'success' => false,
                'message' => $data['error_info']['error_msg']
            );
        }
        return $data;
    }
}

function loadLicense() {
    global $license;
    $webhook = new DotbWebhook();
    $data = [
        'tenant_name' => $license['lic_tenant_name']
    ];
    $result = $webhook->callQuyettamAPI('tenant/get', 'POST', $data, array('Content-Type: multipart/form-data'));
    $data = $result['data'];
    if(empty($data) || !$data['success']) {
        return array(
            'success' => false,
            'message' => $data['error_info']['error_msg']
        );
    }

    $admin = new Administration();
    $admin->retrieveSettings();
    foreach ($data['data']['tenant']['license'] as $key => $value) {
        $admin->saveSetting('license', $key, $value);
    }
    refreshGlobalLicense();
}