<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

global $license;
$tenant_id = $license['lic_tenant_id'] ?? $_GET['tenant_id'];
$module = $_GET['pdf_module'];
$record = $_GET['record'];
$dotbpdf = $_GET['dotbpdf'];

// Call API from quyettam
$client = new Client();
$url = $GLOBALS['dotb_config']['saas_url'] . '/rest/v11_3/tenant/pdf/get';
$headers = [
    'Content-Type' => 'application/json'
];
$body = '{
      "tenant_id": "' .$tenant_id .'",
      "module": "' .$module .'",
      "record": "'. $record .'",
      "dotbpdf": "'. $dotbpdf .'"
    }';
$request = new Request('POST', $url, $headers, $body);
$res = $client->sendAsync($request)->wait();
$response = json_decode($res->getBody(), true);

if ($response['success']) {
    $file_url = $response['data']['file_url'];
    echo '<embed src="'.$file_url.'" type="application/pdf" width="100%" height="100%" src="about:blank" style="position:absolute; left: 0; top: 0;" />';
}