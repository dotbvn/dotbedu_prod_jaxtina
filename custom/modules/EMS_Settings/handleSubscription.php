<?php
require_once 'custom/modules/EMS_Settings/subscriptionHelper.php';

switch ($_POST['type']) {
    case 'activateTrial':
        $result = json_encode(activateTrial());
        echo $result;
        break;
    case 'updateSubscription':
        $result = json_encode(updateSubscription($_POST['product_id'], $_POST['quantity'], $_POST['service_duration']));
        echo $result;
        break;
    case 'upgradeSubscription':
        $result = json_encode(upgradeSubscription($_POST['product_id'], $_POST['quantity']));
        echo $result;
        break;
    case 'renewSubscription':
        $result = json_encode(renewSubscription());
        echo $result;
        break;
    case 'checkVoucher':
        $result = json_encode(checkVoucher($_POST['voucher_code'], $_POST['product_id']));
        echo $result;
        break;
    case 'getPDF':
        $result = json_encode(getPDF($_POST['module_name'], $_POST['record_id'], $_POST['action_name']));
        echo $result;
        break;
    case 'getInvoiceDetail':
        $result = json_encode(getInvoiceDetail($_POST['invoice_id']));
        echo $result;
        break;
    case 'updateInvoice':
        $result = json_encode(updateInvoice($_POST['invoice_id'], $_POST['status'], $_POST['voucher_id']));
        echo $result;
        break;
    case 'updateBillingInfo':
        $result = json_encode(updateBillingInfo($_POST['contact_name'], $_POST['phone_mobile'], $_POST['billing_email'], $_POST['billing_address'], $_POST['billing_country'], $_POST['account_name'], $_POST['tax_code']));
        echo $result;
        break;
    case 'getReceipts':
        $result = json_encode(getReceipts($_POST['invoice_id'], $_POST['filter']));
        echo $result;
        break;
    default:
}

function activateTrial() {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->updateSubscription($license['lic_tenant_id'], 'activate-trial');
}

function updateSubscription($product_id = '', $quantity = '', $service_duration_unit = '') {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->updateSubscription($license['lic_tenant_id'], 'change', $product_id, $quantity, $service_duration_unit);
}

function upgradeSubscription($product_id = '', $quantity = '') {
    global $license;
    if($quantity == 0 && $license['lic_plan_id'] == $product_id) {
        return array('status' => false, 'message' => translate("LBL_NOTHING_CHANGED", "EMS_Settings"));
    }
    $_Helper = new subscriptionHelper();
    return $_Helper->updateSubscription($license['lic_tenant_id'], 'upgrade', $product_id, $quantity);
}

function renewSubscription() {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->updateSubscription($license['lic_tenant_id'], 'renew');
}

function checkVoucher($code = '', $product_template_ids = '') {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->checkVoucher($license['lic_tenant_id'], $code, $product_template_ids);
}

function getPDF($module = '', $record = '', $dotbpdf = '') {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->exportPdfTemplate($license['lic_tenant_id'], $module, $record, $dotbpdf);
}

function getInvoiceDetail($quote_id = '') {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->getInvoiceDetails($license['lic_tenant_id'], $quote_id);
}

function updateInvoice($quote_id = '', $status = '', $voucher_id = '') {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->updateInvoice($license['lic_tenant_id'], $quote_id, $status, $voucher_id);
}

function updateBillingInfo($contact_name = '', $phone_mobile = '', $billing_email = '', $billing_address = '', $billing_country = '', $account_name = '', $tax_code = '') {
    global $license;
    $_Helper = new subscriptionHelper();
    $data = array(
        'contact_name' => $contact_name,
        'phone_mobile' => $phone_mobile,
        'billing_email' => $billing_email,
        'billing_address' => $billing_address,
        'billing_country' => $billing_country,
        'account_name' => $account_name,
        'tax_code' => $tax_code,
    );
    return $_Helper->updateBillingInfo($license['lic_tenant_id'], $data);
}

function isEmpty(...$variables) {
    foreach ($variables as $variable) {
        if (empty($variable)) {
            return true;
        }
    }
    return false;
}

function getReceipts($quote_id, $filter) {
    global $license;
    $_Helper = new subscriptionHelper();
    return $_Helper->getReceipts($license['lic_tenant_id'], $quote_id, $filter);
}