<?php
require_once 'custom/modules/EMS_Settings/subscriptionHelper.php';

echo showSubscription();

function showSubscription()
{
    global $dotb_config, $current_user, $current_language, $mod_strings, $app_list_strings, $license, $timedate;
    $_Helper = new subscriptionHelper();
    $ss = new Dotb_Smarty();

    $product_templates = $_Helper->getProductTemplate();

    $subscription = $_Helper->getSubscription($license['lic_tenant_id']);
    if(!$subscription['success']) die('Subscription information not found');

    $isTrial = (count($subscription['data']['invoices']) < 2 && !in_array($license['lic_status'], ['ACTIVE-ON-DEMAND', 'EXPIRED']));
    $hasLicense = $license['lic_status'] != 'NO-LICENSE';
    $license['lic_expired_date_db'] = $license['lic_expired_date'];
    $license['lic_expired_date'] = ($current_language != 'vn_vn') ? date("F jS, Y", strtotime($license['lic_expired_date'])) : $timedate->to_display_date($license['lic_expired_date'], false);

    $ss->assign('isAdmin', $current_user->isAdmin());
    $ss->assign('license', $license);
    $ss->assign('hasLicense', $hasLicense);
    $ss->assign('license_status', $app_list_strings['licenses_status_lang'][$license['lic_status']]);
    $ss->assign('billing_cycle', $app_list_strings['billing_cycle_lang'][$license['lic_billing_cycle']]);
    $ss->assign('product_templates', $product_templates['data']['product_templates']);
    $ss->assign('tier_list', json_encode($subscription['data']['tier_list']));
    $ss->assign('isTrial', $isTrial);
    $ss->assign('subscription', $subscription['data']['subscription']);
    $ss->assign('invoices', $subscription['data']['invoices']);
    $ss->assign('billing_info', $subscription['data']['billing_info']);
    $ss->assign('CURRENCY', $license['lic_currency']);
    $ss->assign('current_language', $current_language);
    $ss->assign('saas_url', $dotb_config['saas_url']);

    $mod_strings['LBL_REMAINING_DAYS'] = str_replace("{remain}", $subscription['data']['subscription']['remaining_days'], $mod_strings['LBL_REMAINING_DAYS']);

    $ss->assign('MOD', $mod_strings);
    $ss->assign('APP_LIST', $app_list_strings);
    $ss->assign('company_logo', DotbThemeRegistry::current()->getImageURL('company_logo.png', true, true));


    //set language for dataTable
    $datatable_lang = json_encode(array(
        'emptyTable' => $mod_strings['LBL_DT_EMPTYTABLE'],
        'info' => $mod_strings['LBL_DT_INFO'],
        'infoEmpty' => $mod_strings['LBL_DT_INFOEMPTY'],
        'lengthMenu' => $mod_strings['LBL_DT_LENGTHMENU'],
        'search' => $mod_strings['LBL_DT_SEARCH'],
        'zeroRecords' => $mod_strings['LBL_DT_ZERORECORDS'],
        'paginate' => array(
            'first' => $mod_strings['LBL_DT_PAGINATE_FIRST'],
            'last' => $mod_strings['LBL_DT_PAGINATE_LAST'],
            'next' => $mod_strings['LBL_DT_PAGINATE_NEXT'],
            'previous' => $mod_strings['LBL_DT_PAGINATE_PREVIOUS'],
        ),
    ));
    $ss->assign('datatable_lang', $datatable_lang);

    return $ss->fetch('custom/modules/EMS_Settings/tpls/subscription.tpl');
}