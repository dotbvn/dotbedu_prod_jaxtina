<?php

$rows = $GLOBALS['db']->fetchArray($this->query);
$ssids = implode("','",array_column($rows,'primaryid'));
$student_ids = implode("','",array_column($rows,'student_id'));

//lay cac ket qua dau tien
$q2 = "SELECT DISTINCT
IFNULL(l1.student_id, '') student_id,
IFNULL(l1.id, '') re_id,
l2.date_start date_time
FROM j_ptresult l1
INNER JOIN meetings l2 ON l2.id = l1.meeting_id AND l2.deleted = 0
WHERE (l1.student_id IN ('$student_ids')) AND l1.attended = 1
ORDER BY student_id, l2.date_start ASC";
$rosS = $GLOBALS['db']->fetchArray($q2);
$flag_student = '';
foreach( $rosS as $key => $ros){
    if($flag_student != $ros['student_id']){
        $flag_student = $ros['student_id'];
        $firstResId[] = $ros['re_id'];
    }
}
$this->query =str_replace('$hehehe', implode("','",$firstResId), $this->query);

//echo $this->query;

$filter = str_replace(' ','',$this->where);
$parts  = explode("AND", $filter);

for($i = 0; $i < count($parts); $i++){
    if(strpos($parts[$i], "l1.date_start>='") !== FALSE) $start_date = get_string_between($parts[$i]);
    if(strpos($parts[$i], "l1.date_start<='") !== FALSE) $end_date     = get_string_between($parts[$i]);
    if(strpos($parts[$i], "l1.date_start='") !== FALSE){
        $start_date = get_string_between($parts[$i]);
        $end_date   = $start_date;
    }
}

//Lay hoc vien trong vong 60 ngay
$start_date = date('Y-m-d',strtotime("-30 days ".$start_date));
$end_date   = date('Y-m-d',strtotime("+60 days ".$end_date));

$q1 = "SELECT DISTINCT
IFNULL(l1.id, '') schedule_id,
DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%d/%m/%Y') date,
IFNULL(l1.meeting_type, '') AS type,
DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%H:%i %p') time,
IFNULL(l3.full_teacher_name, '') teacher,
GROUP_CONCAT(j_ptresult.student_id) student_ids,
GROUP_CONCAT(j_ptresult.id) j_ptresult_ids,
COUNT(j_ptresult.id) number_of_registers,
SUM(CASE WHEN ((j_ptresult.attended LIKE 'Yes' OR j_ptresult.attended = '1')) THEN 1 ELSE 0 END) number_of_takers,
COUNT(p1.student_id) number_of_new_sales
FROM j_ptresult
INNER JOIN meetings l1 ON l1.id = j_ptresult.meeting_id AND l1.deleted = 0
INNER JOIN teams l2 ON j_ptresult.team_id = l2.id AND l2.deleted = 0
LEFT JOIN c_teachers l3 ON l1.teacher_id = l3.id AND l3.deleted = 0
LEFT JOIN (SELECT DISTINCT IFNULL(l1.id, '') student_id
FROM j_paymentdetail jjp
INNER JOIN contacts l1 ON l1.id = jjp.parent_id AND l1.deleted = 0
WHERE jjp.deleted = 0
AND (jjp.payment_date >= '$start_date'
AND jjp.payment_date <= '$end_date') AND (jjp.payment_amount > 0) AND (jjp.status = 'Paid')) p1 ON p1.student_id = j_ptresult.student_id
WHERE j_ptresult.id IN ('$ssids')
GROUP BY l1.date_start, l1.id
ORDER BY date ASC, type ASC, time ASC, teacher ASC";
//GET First Result ID
$result1 = $GLOBALS['db']->query($q1);
$report_detail = array();
$index = 1;
while($row = $GLOBALS['db']->fetchByAssoc($result1)){
    $report_detail[$index]['type']      = "<a href='/index.php?module=Meetings&action=DetailView&record={$row['schedule_id']}'>{$row['type']}</a>";
    $report_detail[$index]['date']      = $row['date'];
    $report_detail[$index]['time']      = $row['time'];
    $report_detail[$index]['teacher']   = $row['teacher'];
    $report_detail[$index]['number_of_registers']   = $row['number_of_registers'];
    $report_detail[$index]['number_of_takers']      = $row['number_of_takers'];
    $report_detail[$index]['number_of_new_takers']      = 0;
    //get new taker
    $j_ptresult_ids =  explode(",",$row['j_ptresult_ids']);
    foreach($j_ptresult_ids as $kk => $jjs){
        if(in_array($jjs, $firstResId))
            $report_detail[$index]['number_of_new_takers'] += 1;
    }

    $report_detail[$index]['number_of_new_sales']   = $row['number_of_new_sales'];
    $report_detail[$index]['taker_register']        = format_number(($row['number_of_takers']/$row['number_of_registers']) * 100,0,0) . "%";
    $report_detail[$index]['new_sales_register']    = format_number(($report_detail[$index]['number_of_new_sales']/$row['number_of_registers']) * 100,0,0) . "%";
    $index++;
}
$html = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><thead>';

$html .= '<tr><td colspan="100%"><h3><span>SUMARY</span></h3></td></tr>';
$html .= '<tr><th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">No.</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Type</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Date</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Time</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Teacher</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Number of registers</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Number of takers</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Number of new takers</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Number of new sales</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Taker / Register (%)</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">New sales / register (%)</th></tr>';
$html .= '</thead>';



$html .= "<tbody>";
foreach ($report_detail as $key => $value){
    $html .= "<tr>";
    $html .= "<td valign='TOP' class='oddListRowS1'>$key</td>";
    foreach ($value as $val)
        $html .= "<td valign='TOP' class='oddListRowS1'>$val</td>";

    $html .= "</tr>";
}
$html .= "</tbody></table><br><br>";
echo $html;
