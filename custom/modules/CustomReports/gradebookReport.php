<?php
//Xử lý Filter
$gbs = $GLOBALS['db']->fetchArray($this->query);

//Nhóm thông tin theo id class
$listClass = array();
foreach($gbs as $key => $gb){
    $listClass[$gb['l2_id']][]=$gb;
}

$html = '';
foreach ($listClass as $l2_id => $value) {
    // Tìm tên lớp từ mảng $gbs bằng id class
    $l2_name = '';
    foreach ($gbs as $gb) {
        if ($gb['l2_id'] == $l2_id) {
            $l2_name = $gb['l2_name'];
            break;
        }
    }

    $html .= "<table style='width: 100%; border-collapse: collapse;'>";
    $html .= "<tr>";
    $html .= "<td style='background: #dcdcdc; text-align: center; font-weight: bold; border: 1px solid #333; color:#3a3427; text-transform: uppercase; padding: 8px; font-size:13px;' colspan='2'>$l2_name</td>";
    $html .= "</tr>";
//lấy ra bảng điểm của từng class
    foreach ($value as $key_vl => $vl) {
        $html .= "<tr>";
        $html .= "<td style='text-align: left; font-weight: bold; border: 1px solid #333; color:#3a3427; text-transform: uppercase; padding: 8px; font-size:13px;' colspan='2'>{$vl['j_gradebook_name']}</td>";
        $html .= "</tr>";
        $bean_gb = BeanFactory::getBean('J_Gradebook', $vl['primaryid']);
        $html .= "<tr>";
        $html .= "<td colspan='2'>" . $bean_gb->getGradebookReport() . "</td>";
        $html .= "</tr>";
    }
    $html .= "</table>";
    $html .= "<br>";

}
echo $html;
die();
//Draw table


