<?php
global $timedate;

$filter = str_replace(' ', '', $this->where);
$parts = explode("AND", $filter);

for ($i = 0; $i < count($parts); $i++) {
    if (strpos($parts[$i], "meetings.date_start>='") !== FALSE) $start_date = get_string_between($parts[$i]);
    if (strpos($parts[$i], "meetings.date_start<='") !== FALSE) $end_date = get_string_between($parts[$i]);
    if (strpos($parts[$i], "l2.id='") !== FALSE) $team_id = get_string_between($parts[$i]);
    if (strpos($parts[$i], "meetings.date_start='") !== FALSE) {
        $start_date = get_string_between($parts[$i]);
        $end_date = $start_date;
    }
}

$from_to = 'From: '.$timedate->to_display_date($start_date) .' - To: '. $timedate->to_display_date($end_date);
$class_list   = array();
$schedules    = array();
$cls_list     = array();
$upgrade_list = array();
$koc_list     = array();
$rows = $GLOBALS['db']->fetchArray($this->query);
foreach ($rows as $key => $row) {
    $koc        = $row['l1_kind_of_course'];
    $schedules[]= $row['primaryid'];
    if (!in_array($row['l1_id'], $cls_list)) {
        $cls_list[] = $row['l1_id'];
        $koc_list[$koc] += 1;
        $class_list[$koc][$row['l1_id']]['class_id'] = $row['l1_id'];
        $class_list[$koc][$row['l1_id']]['koc'] = $koc;
        $class_list[$koc][$row['l1_id']]['level'] = $row['l1_level'];
        $class_list[$koc][$row['l1_id']]['start_date'] = $timedate->to_display_date($row['l1_start_date'], false);
        $class_list[$koc][$row['l1_id']]['end_date'] = $timedate->to_display_date($row['l1_end_date'], false);
        $class_list[$koc][$row['l1_id']]['assigned_to'] = $row['l3_full_user_name'];
        $class_list[$koc][$row['l1_id']]['class_name'] = $row['l1_name'];
        $class_list[$koc][$row['l1_id']]['class_code'] = $row['l1_class_code'];

        $class_list[$koc][$row['l1_id']]['status'] = $row['l1_status'];
        $class_list[$koc][$row['l1_id']]['last_ss_time'] = $row['meetings_date_start'];
        $class_list[$koc][$row['l1_id']]['last_ss_lesson'] = $row['meetings_lesson_number'];
        $class_list[$koc][$row['l1_id']]['upgrade_to_class_id'] = $row['l4_id'];
        $class_list[$koc][$row['l1_id']]['upgrade_to_class_name'] = $row['l4_name'];
        $class_list[$koc][$row['l1_id']]['upgrade_to_date'] = (!empty($row['l4_start_date'])) ? $timedate->to_display_date($row['l4_start_date']) : $timedate->to_display_date_time($row['l4_date_entered']);
        if (!empty($class_list[$koc][$row['l1_id']]['upgrade_to_class_id']))
            $upgrade_list[] = $class_list[$koc][$row['l1_id']]['upgrade_to_class_id'];
    }
    $class_list[$koc][$row['l1_id']]['hour_learned']    += $row['meetings_delivery_hour'];
    $class_list[$koc][$row['l1_id']]['count_ss'] += 1;

    //Schedule
    $this_start = strtotime('+ 7hour ' . $row['meetings_date_start']);
    $this_end = strtotime('+ 7hour ' . $row['meetings_date_end']);
    $week_date = date('D', $this_start);
    $time = $week_date . ": " . date('g:i', $this_start) . '-' . date('g:ia', $this_end);

    if (strpos($class_list[$koc][$row['l1_id']]['schedule'], $time) === false)
        $class_list[$koc][$row['l1_id']]['schedule'] .= $time . "<br style=\"mso-data-placement:same-cell;\"/> " . "\n";

    //Teacher Handle
    $posTea = strpos($class_list[$koc][$row['l1_id']]['teacher'], $row['l5_full_teacher_name']);
    $trim_  = trim($row['l5_full_teacher_name']);
    if (!empty($trim_)) {
        if ($posTea === false)
            $class_list[$koc][$row['l1_id']]['teacher'] .= $row['l5_full_teacher_name'] . " ($week_date)\n" . "<br style=\"mso-data-placement:same-cell;\"/> " . "\n";
        else {
            $reSche     = strpos($class_list[$koc][$row['l1_id']]['teacher'], '(', $posTea);
            $posSche    = strpos($class_list[$koc][$row['l1_id']]['teacher'], ')', $posTea);
            $sche       = substr($class_list[$koc][$row['l1_id']]['teacher'], $reSche, $posSche - $reSche);
            if (strpos($sche, $week_date) === false) {
                $newstr = substr_replace($class_list[$koc][$row['l1_id']]['teacher'], ',' . $week_date, $posSche, 0);
                $class_list[$koc][$row['l1_id']]['teacher'] = $newstr;
            }
        }
        $class_list[$koc][$row['l1_id']]['count_teacher'] += 1;
    }

}
$q2 = "SELECT DISTINCT IFNULL(l1.kind_of_course, '') kind_of_course,
IFNULL(l1.id, '') class_id, IFNULL(l2.full_user_name, '') cso_name,
RTRIM(CONCAT(IFNULL(l3.name_2, ''),' ',IFNULL(l3.name, ''))) team_name,
COUNT(IFNULL(meetings.id, '')) count_ss
FROM meetings
INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
LEFT JOIN users l2 ON l1.cso_id = l2.id AND l2.deleted = 0
INNER JOIN teams l3 ON l1.team_id = l3.id AND l3.deleted = 0
WHERE (meetings.session_status <> 'Cancelled') AND (meetings.deleted = 0)
AND (l1.id IN ('".implode("','",$cls_list)."'))
GROUP BY kind_of_course, class_id";
$rs2 = $GLOBALS['db']->query($q2);
$ssClass = array();
$cr_class = '';
while ($ss = $GLOBALS['db']->fetchByAssoc($rs2)){
    $class_list[$ss['kind_of_course']][$ss['class_id']]['team_name'] = $ss['team_name'];
    $class_list[$ss['kind_of_course']][$ss['class_id']]['total_lesson'] = $ss['count_ss'];
    $class_list[$ss['kind_of_course']][$ss['class_id']]['cso_name'] = $ss['cso_name'];
}


//Count Student
$q2 = "SELECT DISTINCT
IFNULL(l1.kind_of_course, '') kind_of_course, IFNULL(l1.id, '') class_id, IFNULL(l2.student_id, '') student_id, IFNULL(l2.type, '') type
FROM meetings
INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
INNER JOIN meetings_contacts l2_1 ON meetings.id = l2_1.meeting_id AND l2_1.deleted = 0
INNER JOIN j_studentsituations l2 ON l2.id = l2_1.situation_id AND l2.deleted = 0
WHERE (meetings.id IN ('".implode("','",$schedules)."')) AND meetings.deleted = 0
ORDER BY CASE WHEN (IFNULL(l2.type, '') = '' OR IFNULL(l2.type, '') IS NULL) THEN 0
WHEN IFNULL(l2.type, '') = 'OutStanding' THEN 1
WHEN IFNULL(l2.type, '') = 'Enrolled' THEN 2
WHEN IFNULL(l2.type, '') = 'Delayed' THEN 3
WHEN IFNULL(l2.type, '') = 'Demo' THEN 4
ELSE 5 END ASC";
$rs2 = $GLOBALS['db']->query($q2);
$ssClass = array();
$cr_class = '';
$students = array();
while ($ss = $GLOBALS['db']->fetchByAssoc($rs2)){
    if (!in_array($ss['student_id'], $class_list[$ss['kind_of_course']][$ss['class_id']]['student_id'])){
        $class_list[$ss['kind_of_course']][$ss['class_id']]['student_id'][]    = $ss['student_id'];
        $students[] = $ss['student_id'];
        $class_list[$ss['kind_of_course']][$ss['class_id']][$ss['type']]      += 1;
        $class_list[$ss['kind_of_course']][$ss['class_id']]['count_student']  += 1;
    }
}

//Count Student in upgrade class
$q3 = "SELECT DISTINCT IFNULL(l1.id, '') class_id, COUNT(DISTINCT(l2.student_id)) count_student
FROM j_class l1
INNER JOIN j_studentsituations l2 ON l2.ju_class_id = l1.id AND l2.deleted = 0
WHERE (l1.id IN ('".implode("','",$upgrade_list)."')) AND IFNULL(l2.type, '') IN ('OutStanding', 'Enrolled')
GROUP BY class_id ";
$rs3 = $GLOBALS['db']->query($q3);
while ($r3 = $GLOBALS['db']->fetchByAssoc($rs3))
    $upgrade_list[$r3['class_id']] = $r3['count_student'];

//Tính toán các con số TOTAL
$course = array();
foreach ($class_list as $koc => $class_obj)
    foreach ($class_obj as $class_id => $val){
        $course[$koc]['hour_learned']   += $val['hour_learned'];
        $course[$koc]['count_ss']       += $val['count_ss'];
        $course[$koc]['Enrolled']       += $val['Enrolled'];
        $course[$koc]['OutStanding']    += $val['OutStanding'];
        $course[$koc]['Demo']           += $val['Demo'];
        $course[$koc]['count_student']  += $val['count_student'];
        $course[$koc]['count_class']    += 1;
        //Total
        $hour_learned += $val['hour_learned'];
        $count_ss += $val['count_ss'];
        $Enrolled += $val['Enrolled'];
        $OutStanding += $val['OutStanding'];
        $Demo += $val['Demo'];
        $count_student += $val['count_student'];
        $count_class += 1;
}

$kocc_list = array();
$html = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody>';
foreach ($class_list as $koc => $class_obj){
    $no   = 1;
    $html .= get_table_head($koc, $from_to);
    foreach ($class_obj as $class_id => $val){
        $html .= "<tr><td valign='TOP' class='oddListRowS1'>".$no++."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['team_name']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['koc'].' '.$val['level']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>" .'<a href=index.php?module=J_Class&action=DetailView&record=' . $val['class_id'] . '>' . $val['class_name'] . '</a>' . "</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>{$val['class_code']}</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>{$val['status']}</td>";
        $html .= "<td valign='TOP' style='mso-number-format:\"dd/mm/yyyy\";text-align: left;' class='oddListRowS1'>{$val['start_date']}</td>";
        $html .= "<td valign='TOP' style='mso-number-format:\"dd/mm/yyyy\";text-align: left;' class='oddListRowS1'>{$val['end_date']}</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>{$val['assigned_to']}</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>{$val['cso_name']}</td>";
        $html .= "<td valign='TOP' style='mso-number-format:\@;text-align: center;' class='oddListRowS1'>" . $val['last_ss_lesson'] . ' / ' . $val['total_lesson'] . " (" . format_number(($val['last_ss_lesson'] / $val['total_lesson']) * 100, 0, 0) . "%)</td>";

        $html .= "<td valign='TOP' style='mso-number-format:\@;' class='oddListRowS1'>{$val['schedule']}</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>" . rtrim($val['teacher'], '; ') . "</td>";
        $html .= "<td valign='TOP' style='text-align: center;' class='oddListRowS1'>" . format_number($val['hour_learned'], 2, 2) . "</td>";
        $html .= "<td valign='TOP' style='text-align: center;' class='oddListRowS1'>{$val['count_ss']}</td>";


        $html .= "<td valign='TOP' style='text-align: center;' class='oddListRowS1'>{$val['Enrolled']}</td>";
        $html .= "<td valign='TOP' style='text-align: center;' class='oddListRowS1'>{$val['OutStanding']}</td>";
        $html .= "<td valign='TOP' style='text-align: center;' class='oddListRowS1'>{$val['Demo']}</td>";
        $html .= "<td valign='TOP' style='text-align: center;' class='oddListRowS1'>{$val['count_student']}</td>";

        $html .= "<td valign='TOP' class='oddListRowS1'>" . '<a href=index.php?module=J_Class&action=DetailView&record=' . $val['upgrade_to_class_id'] . '>' . $val['upgrade_to_class_name'] . '</a>' . "</td>";
        $html .= "<td valign='TOP' class='oddListRowS1' style='mso-number-format:\"dd/mm/yyyy h:mm\";text-align: left;'>{$val['upgrade_to_date']}</td>";
        $html .= "<td valign='TOP' style='text-align: center;' class='oddListRowS1'>{$upgrade_list[$val['upgrade_to_class_id']]}</td>";

        if ($no == (count($class_obj) +1) ) {
            $html .= "<tr>";
            $html .= "<td colspan='13' style='text-align: right;'><b></b></td>";
            $html .= "<td><b>" . format_number($course[$koc]['hour_learned'],2, 2) . "</b></td>";
            $html .= "<td><b>" . format_number($course[$koc]['count_ss']) . "</b></td>";
            $html .= "<td><b>" . format_number($course[$koc]['Enrolled']) . "</b></td>";
            $html .= "<td><b>" . format_number($course[$koc]['OutStanding']) . "</b></td>";
            $html .= "<td><b>" . format_number($course[$koc]['Demo']) . "</b></td>";
            $html .= "<td><b>" . format_number($course[$koc]['count_student']) . "</b></td>";
            $html .= "<td colspan='3'><b>Average Class Size (ACS): ". format_number($course[$koc]['count_student'] / $course[$koc]['count_class'],2, 2)."</b></td>";
            $html .= "</tr>";
        }
    }
}

if (count($class_list) == 0)
    $html .= "<td colspan='100%'>No Results!!</td>";
$html .= "</tbody></table>";

$html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0" class="formHeader h3Row"><tbody><tr><td nowrap=""><h3><span>Grand Total</span></h3></td><td width="100%"><img height="1" width="1" src="themes/default/images/blank.gif" alt=""></td></tr></tbody></table>';
$html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="list view">
<tbody><tr height="20">
<td scope="col" align="center" valign="middle" nowrap="">Total Hours</td>
<td scope="col" align="center" valign="middle" nowrap="">Count Session</td>
<td scope="col" align="center" valign="middle" nowrap="">Total Enrolled</td>
<td scope="col" align="center" valign="middle" nowrap="">Total OutStanding</td>
<td scope="col" align="center" valign="middle" nowrap="">Total Demo</td>
<td scope="col" align="center" valign="middle" nowrap="">Total Students</td>
<td scope="col" align="center" valign="middle" nowrap="">Count Class</td>
<td scope="col" align="center" valign="middle" nowrap="">Average Class Size (ACS)</td>

</tr>
<tr height="20" class="oddListRowS1">
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($hour_learned,2,2).'</td>
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($count_ss).'</td>
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($Enrolled).'</td>
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($OutStanding).'</td>
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($Demo).'</td>
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($count_student).'</td>
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($count_class).'</td>
<td width="%" valign="TOP" class="oddListRowS1" bgcolor="" scope="row">'.format_number($count_student / $count_class,2, 2).'</td>

</tr>
</tbody></table>';

echo $html;
die();
//##############------------------------------------######################-----------------------


function get_table_head($koc = '', $from_to = '')
{
    $html = "<tr><td colspan='100%' style='text-align: left;'><h3 style='color: #000 !important;'>" .mb_strtoupper($koc, 'UTF-8'). " CLASSES</h3></td></tr>";
    $html .= '<tr><th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">No.</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Primary Center</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Level</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Class Name</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Class Code</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Status</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Start Date</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">End Date</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Assigned To</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">CSO Name</th>';
    $html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle">Progress</th>';

    $html .= '<th colspan="4" align="center" class="reportlistViewThS1" valign="middle" nowrap="">'.$from_to.'</th>';

    $html .= '<th colspan="4" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Number of Student</th>';
    $html .= '<th colspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Upgrade Class</th>';
    $html .= '</tr>';

    $html .= '<tr>';
    $html .= '<th align="center" style="width: 150px;" valign="middle" nowrap="">Schedule</th>';
    $html .= '<th align="center" style="width: 150px;" valign="middle" nowrap="">Teacher</th>';
    $html .= '<th align="center" valign="middle">Total Hours</th>';
    $html .= '<th align="center" valign="middle">Count Session</th>';

    $html .= '<th align="center" valign="middle" nowrap="">Enrolled</th>';
    $html .= '<th align="center" valign="middle">OutStanding</th>';
    $html .= '<th align="center" valign="middle">Demo</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Total<br style="mso-data-placement:same-cell;"/>students</th>';

    $html .= '<th align="center" valign="middle">Upgrade<br style="mso-data-placement:same-cell;"/>Class</th>';
    $html .= '<th align="center" valign="middle">Upgrade<br style="mso-data-placement:same-cell;"/>Start</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Total<br style="mso-data-placement:same-cell;"/>students</th>';

    $html .= '</tr>';
    return $html;
}
