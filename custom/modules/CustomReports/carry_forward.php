<?php
global $timedate, $current_user;
$filter = str_replace(' ', '', $this->where);
$parts  = explode("AND", $filter);
for ($i = 0; $i < count($parts); $i++) {
    if (strpos($parts[$i], "j_payment.payment_date>=") !== FALSE) $start = get_string_between($parts[$i],"payment_date>='");
    if (strpos($parts[$i], "j_payment.payment_date<=") !== FALSE) $end = get_string_between($parts[$i]);
    if (strpos($parts[$i], "j_payment.payment_date=") !== FALSE)  $end = $start = get_string_between($parts[$i]);
    if (strpos($parts[$i], "l1") !== FALSE) $_filterTeam = str_replace('((((','(',$parts[$i]);
    if (strpos($parts[$i], "l2") !== FALSE) $_fiterStudent = str_replace(')))',')',$parts[$i]);
}

//Student Name
$q1 = "SELECT DISTINCT IFNULL(l2.id, '') student_id,
CONCAT(IFNULL(l2.last_name, ''),' ',IFNULL(l2.first_name, '')) student_name,
IFNULL(l2.contact_id, '') student_code
FROM contacts l2 WHERE l2.deleted = 0";
//Team ID
$q2 = "SELECT DISTINCT IFNULL(l1.id, '') team_id, IFNULL(l1.name, '') team_name, IFNULL(l1.code_prefix, '') team_code, IFNULL(l1.parent_id, '') parent_id FROM teams l1 WHERE l1.private = 0 AND l1.deleted = 0";
if(!$current_user->isAdmin()){
    //Filter ALL - Get all students
    $q1 = "$q1 AND l2.team_set_id IN (SELECT tst.team_set_id
    FROM team_sets_teams tst
    INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
    AND team_memberships.user_id = '".$current_user->id."' AND team_memberships.deleted = 0)";
    //Filter ALL - Get all teams
    $q2 = "$q2 AND l1.id IN (SELECT DISTINCT IFNULL(l0.id, '') team_id
    FROM users INNER JOIN team_memberships l0_1 ON users.id = l0_1.user_id AND l0_1.deleted = 0
    INNER JOIN teams l0 ON l0.id = l0_1.team_id AND l0.deleted = 0
    WHERE ((users.id = '{$current_user->id}') AND l0.private = 0) AND users.deleted = 0)";
}

//Build query
if(empty($_fiterStudent) && empty($_filterTeam)){}else{
    //Check Filter Teams, Student
    if(!empty($_fiterStudent)) $q1 = "$q1 AND $_fiterStudent";
    if(!empty($_filterTeam)) $q2 = "$q2 AND $_filterTeam";
}
$q1 = "$q1 ORDER BY first_name ASC";
$q2 = "$q2 ORDER BY parent_id DESC, team_name ASC";
$studentList = $GLOBALS['db']->fetchArray($q1);
$teamList = $GLOBALS['db']->fetchArray($q2);
if(!empty($studentList) && !empty($teamList)){
    //Biuld students list
    foreach($studentList as $k_ => $student) {
        foreach ($teamList as $k__ => $team) {
            $students[$team['team_id']][$student['student_id']]['center_id']   = $team['team_id'];
            $students[$team['team_id']][$student['student_id']]['center_name'] = $team['team_name'];
            $students[$team['team_id']][$student['student_id']]['center_code'] = $team['team_code'];
            $students[$team['team_id']][$student['student_id']]['student_id']  = $student['student_id'];
            $students[$team['team_id']][$student['student_id']]['student_code']= $student['student_code'];
            $students[$team['team_id']][$student['student_id']]['student_name']= $student['student_name'];
        }
    }
    require_once("custom/modules/CustomReports/helper_cf_report.php");
    $stIDs = array_column($studentList,'student_id');
    if(count($stIDs)>10) $stIDs = '';
    $array_cf     =  calculate_carry($start , $end, $stIDs, array_column($teamList,'team_id'));
    $student_list = array_replace_recursive($students, $array_cf);
}

//Draw table Student
$html = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody><tr>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">No.</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Center</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Center Code</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Student ID</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Full Name</th>';
$html .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Opening balance</th>';
$html .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Collected</th>';
$html .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Moving/Transfer In</th>';
$html .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Moving/Transfer Out</th>';
$html .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Delivery Revenue</th>';
$html .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Drop Revenue</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Refund Amount</th>';
$html .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Closing balance</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Notes balance</th>';
$html .= '</tr>';
$html .= '<tr>';
$html .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html .= '</tr>';

//Draw table Team
$html1 = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody><tr>';
$html1 .= '<th rowspan="2" colspan="5" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Center name</th>';
$html1 .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Opening balance</th>';
$html1 .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Collected</th>';
$html1 .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Moving/Transfer In</th>';
$html1 .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Moving/Transfer Out</th>';
$html1 .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Delivery Revenue</th>';
$html1 .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Drop Revenue</th>';
$html1 .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Refund Amount</th>';
$html1 .= '<th colspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Closing balance</th>';
$html1 .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Notes balance</th>';
$html1 .= '</tr>';

$html1 .= '<tr>';
$html1 .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Amount</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Hours</th>';
$html1 .= '</tr>';


foreach($student_list as $team_id => $team){
    foreach($team as $student_id => $student){
        foreach($student as $variable => $amount){
            //Ẩn các số âm giờ
            if(strpos($variable, '_hour') !== false && $amount <= '0.1')
                $student_list[$team_id][$student_id][$variable] = 0;

            //Ẩn các số tiền >0 & < 2k
            if(strpos($variable, '_amount') !== false && abs($amount) <= 2000)
                $student_list[$team_id][$student_id][$variable] = 0;
        }

    }
}

//Unset zero student
foreach($student_list as $team_id => $team){
    foreach($team as $student_id => $student){
        //Check All zeros!
        if($student['begin_amount']     == 0
            && $student['end_amount']       == 0
            && $student['col_amount']       == 0
            && $student['motr_in_amount']   == 0
            && $student['motr_out_amount']  == 0
            && $student['revenue_amount']   == 0
            && $student['drop_revenue_amount']   == 0
            && $student['refund_amount']    == 0)
            unset($student_list[$team_id][$student_id]);
    }
}

//Nếu team ko còn dòng nào thì xóa team
foreach($student_list as $team_id => $team){
    if(!count($team)) unset($student_list[$team_id]);
}


$teamId = '@@@';
$teamS  = array();
$count  = 1;
if(!count($student_list)){
    $html .= "<tr><td valign='TOP' class='oddListRowS1' colspan=\"100%\">No results!</td></tr>";
    $html1 .= "<tr><td valign='TOP' class='oddListRowS1' colspan=\"100%\">No results!</td></tr>";
}else{
    foreach($student_list as $team_id => $team){
        if($team_id !== $teamId && $teamId !== '@@@'){
            $sumTeam = sumTeam($student_list, $teamId, $student);
            $html .= $sumTeam;
            $html1 .= $sumTeam;
            $count = 1;
        }
        foreach($team as $student_id => $student){
            $unallocated_revenue = $student['revenue_last'];
            $remain_amount       =  $student['end_amount'] - $student['revenue_last'];
            if(abs($remain_amount) < 10000 ) $remain_amount = 0;
            if(abs($unallocated_revenue) < 10000 ) $unallocated_revenue = 0;
            $html .= '<tr>';
            $html .= "<td valign='TOP' class='oddListRowS1'>".$count++."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".$student['center_name']."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".$student['center_code']."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".$student['student_code']."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'><a href='#bwc/index.php?module=Contacts&action=DetailView&record={$student['student_id']}' >{$student['student_name']}</a> </td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['begin_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['begin_hours'],2,2)."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['col_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['col_hours'],2,2)."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['motr_in_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['motr_in_hours'],2,2)."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['motr_out_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['motr_out_hours'],2,2)."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['revenue_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['revenue_hours'],2,2)."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['drop_revenue_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['drop_revenue_hours'],2,2)."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['refund_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['end_amount'])."</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($student['end_hours'],2,2)."</td>";
            if($unallocated_revenue || $remain_amount)
                $html .= "<td valign='TOP' class='oddListRowS1'>
                Unallocated revenue: ".format_number($unallocated_revenue)."
                <br>Remain Amount: ".format_number($remain_amount)."
                </td>";
            else $html .= "<td valign='TOP' class='oddListRowS1'></td>";

            $html .= '</tr>';
        }
        $teamId = $team_id;
    }

    $sumTeam = sumTeam($student_list, $teamId, $student);
    $html .= $sumTeam;
    $html1 .= $sumTeam;
    $sumTeamTotal = sumTeam($student_list, '', array('center_name' => 'TOTAL'));
    $html .= $sumTeamTotal;
    $html1 .= $sumTeamTotal;
}
$html .= '</tbody></table>';
$html1 .= '</tbody></table>';
echo "<h1>SUMMARY: Carry Forward by Center</h1><br>".$html1."<br><br><h1>SUMMARY: Carry Forward by Student</h1><br>".$html;
die();


//-(*(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)

function sumTeam($student_list, $teamId, $student){
    if(empty($teamId)){
        foreach($student_list as $team_id => $team)
            foreach($team as $stud_id => $stud)
                foreach($stud as $vari => $value)
                    $teamS[$vari] += $value;

    }else
        foreach($student_list[$teamId] as $stud_id => $stud)
            foreach($stud as $vari => $value)
                $teamS[$vari] += $value;
    $unallocated_revenue = $teamS['revenue_last'];
    $remain_amount       = $teamS['end_amount'] - $teamS['revenue_last'];
    if(abs($remain_amount) < 10000 ) $remain_amount = 0;
    if(abs($unallocated_revenue) < 10000 ) $unallocated_revenue = 0;
    $html = "<tr><td style='border-right: none;'></td><td style='border-right: none;'></td><td style='border-right: none;'></td><td style='border-right: none;'></td><td valign='TOP' class='oddListRowS1' ><b>".$student['center_name']."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['begin_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['begin_hours'],2,2)."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['col_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['col_hours'],2,2)."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['motr_in_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['motr_in_hours'],2,2)."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['motr_out_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['motr_out_hours'],2,2)."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['revenue_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['revenue_hours'],2,2)."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['drop_revenue_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['drop_revenue_hours'],2,2)."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['refund_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['end_amount'])."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><b>".format_number($teamS['end_hours'],2,2)."</b></td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>
    Unallocated revenue: ".format_number($unallocated_revenue)."
    <br>Remain Amount: ".format_number($remain_amount)."
    </td>";
    $html .= "</tr>";
    return $html;
}