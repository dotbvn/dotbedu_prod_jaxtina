<?php
//Report Re-Enroll theo ngày Kết thúc học của học viên


require_once("custom/modules/CustomReports/helper_cf_report.php");
global $timedate, $current_user;
$filter = str_replace(' ', '', $this->where);
$parts  = explode("AND", $filter);
for ($i = 0; $i < count($parts); $i++) {
    if(strpos($parts[$i], "j_studentsituations.end_study>='") !== FALSE) $start = get_string_between($parts[$i]);
    if(strpos($parts[$i], "j_studentsituations.end_study<='") !== FALSE) $end = get_string_between($parts[$i]);
    if(strpos($parts[$i], "j_studentsituations.end_study='") !== FALSE) $start = $end = get_string_between($parts[$i]);
    if(strpos($parts[$i], "j_studentsituations.before_amount") !== FALSE) $epp_amount = (int)filter_var($parts[$i],FILTER_SANITIZE_NUMBER_INT);
    if(strpos($parts[$i], "l1.id=") !== FALSE) $team_id = get_string_between($parts[$i]);
    if(strpos($parts[$i], "l1.idIN(") !== FALSE) $team_id = get_string_between($parts[$i],"('","')");
}
//Get Team for user
if(empty($epp_amount)) $epp_amount = 2000000;
$ext_team_c = "AND l1.team_id IN (SELECT DISTINCT IFNULL(l2.id, '') team_id
FROM users INNER JOIN team_memberships l2_1 ON users.id = l2_1.user_id AND l2_1.deleted = 0
INNER JOIN teams l2 ON l2.id = l2_1.team_id AND l2.deleted = 0
WHERE ((users.id = '{$current_user->id}') AND l2.private = 0) AND users.deleted = 0)";
if($current_user->isAdmin()) $ext_team_c = '';
if(!empty($team_id)) $ext_team_c .= " AND l1.team_id IN ('$team_id') ";

//Lấy danh sách Student Finished
$q1 = "SELECT DISTINCT
IFNULL(l2.id, '') center_id,
IFNULL(l2.name, '') center_name,
IFNULL(l2.code_prefix, '') center_code,
IFNULL(l1.id, '') student_id,
IFNULL(l1.contact_id, '') student_code,
IFNULL(l1.full_student_name, '') student_name,
IFNULL(l1.guardian_name, '') guardian_name,
IFNULL(l1.phone_mobile, '') phone_mobile,
IFNULL(l3.id, '') class_id,
IFNULL(l3.name, '') class_name,
IFNULL(l3.kind_of_course, '') kind_of_course,
IFNULL(l3.level, '') level,
MIN(jsc.start_study) start_study,
MAX(jsc.end_study) end_study,
MIN(nxte.start_study) next_enroll_date,
(CASE WHEN (IFNULL(l5.description,'')<>'') THEN IFNULL(l5.description,'') ELSE IFNULL(jsc.description,'') END) description,
IFNULL(l4.full_user_name, '') assigned_to_name
FROM j_studentsituations jsc
INNER JOIN contacts l1 ON jsc.student_id = l1.id AND l1.deleted = 0 AND jsc.student_type = 'Contacts' $ext_team_c
INNER JOIN teams l2 ON l1.team_id = l2.id AND l2.deleted = 0
INNER JOIN j_class l3 ON jsc.ju_class_id = l3.id AND l3.deleted = 0
INNER JOIN users l4 ON l1.assigned_user_id = l4.id AND l4.deleted = 0
INNER JOIN j_classstudents l5 ON l5.student_id = l1.id AND l5.class_id = l3.id AND l5.deleted = 0
LEFT JOIN j_studentsituations nxte ON (l1.id = nxte.student_id)
AND (nxte.start_study > l3.end_date) AND (nxte.type = 'Enrolled') AND nxte.deleted=0
WHERE (jsc.type = 'Enrolled') AND (jsc.end_study = l3.end_date)
AND (l3.end_date >= '$start') AND (l3.end_date <= '$end')
AND (jsc.total_amount > 0) AND jsc.deleted = 0
GROUP BY l2.name ASC, l3.name ASC, l1.first_name ASC, l1.id, l3.id";
$rows = $GLOBALS['db']->fetchArray($q1);
$students = $student_cf = $teamS = array();
foreach ($rows as $key => $row) {
    if(!empty($row['next_enroll_date'])
        && (date('Y-m-d',strtotime("-6 months ".$row['next_enroll_date'])) >= $row['start_study']))
        continue;
    $students[$row['student_id']]['team_id']         = $row['center_id'];
    $students[$row['student_id']]['team_name']       = $row['center_name'];
    $students[$row['student_id']]['team_code']       = $row['center_code'];
    $students[$row['student_id']]['student_id']      = $row['student_id'];
    $students[$row['student_id']]['student_code']    = $row['student_code'];
    $students[$row['student_id']]['student_name']    = $row['student_name'];
    $students[$row['student_id']]['guardian_name']   = $row['guardian_name'];
    $students[$row['student_id']]['phone_mobile' ]   = $row['phone_mobile'];
    $students[$row['student_id']]['class_id']        = $row['class_id'];
    $students[$row['student_id']]['class_name']      = $row['class_name'];
    $students[$row['student_id']]['kind_of_course']  = $row['kind_of_course'];
    $students[$row['student_id']]['level']           = $row['level'];
    $students[$row['student_id']]['start_study']     = $timedate->to_display_date($row['start_study'],false);
    $students[$row['student_id']]['end_study']       = $timedate->to_display_date($row['end_study'],false);
    $students[$row['student_id']]['description']     = $row['description'];
    $students[$row['student_id']]['assigned_to']     = $row['assigned_to_name'];
    $students[$row['student_id']]['epp_type']        = 'Loss';
    $students[$row['student_id']]['re_enroll_date']  = '';
    $students[$row['student_id']]['re_enroll_amount']= '';
    if(!empty($row['next_enroll_date'])){
        $students[$row['student_id']]['epp_type']       = 'Re-enroll';
        $students[$row['student_id']]['re_enroll_date'] = $row['next_enroll_date'];
        $q1 = "SELECT jsc.total_amount enroll_amount,
            IFNULL(l3.id, '') class_id, IFNULL(l3.name, '') class_name
            FROM j_studentsituations jsc
            INNER JOIN j_class l3 ON jsc.ju_class_id = l3.id AND l3.deleted = 0
            WHERE jsc.start_study='{$row['next_enroll_date']}'
            AND jsc.student_id='{$row['student_id']}' AND jsc.deleted = 0";
        $_return = $GLOBALS['db']->fetchOne($q1);
        $students[$row['student_id']]['re_enroll_amount'] = $_return['enroll_amount'];
        $students[$row['student_id']]['class_return_name']= $_return['class_name'];
        $students[$row['student_id']]['class_return_id']  = $_return['class_id'];
    }
}

//Draw table Student
$html = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody><tr>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">No.</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Center</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Center Code</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Student ID</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Student Name</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Parent</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Mobile</th>';
$html .= '<th colspan="5" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Class Info</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Return Date</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Return Amount</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Class Return</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Assigned To</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Notes</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Type</th>';
$html .= '</tr>';

$html .= '<tr>';
$html .= '<th align="center" valign="middle" nowrap="">Class Name</th>';
$html .= '<th align="center" valign="middle" nowrap="">Course</th>';
$html .= '<th align="center" valign="middle" nowrap="">Level</th>';
$html .= '<th align="center" valign="middle" nowrap="">Start Study</th>';
$html .= '<th align="center" valign="middle" nowrap="">End Study</th>';
$html .= '</tr>';

$count = 0;
foreach($students as $student_id => $student){
    $count++;
    $html .= '<tr>';
    $html .= "<td valign='TOP' class='oddListRowS1'>".$count."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['team_name']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['team_code']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['student_code']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><a href='#bwc/index.php?module=Contacts&action=DetailView&record={$student['student_id']}' >{$student['student_name']}</a> </td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['guardian_name']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['phone_mobile']."</td>";

    $html .= "<td valign='TOP' class='oddListRowS1'><a href='#bwc/index.php?module=J_Class&action=DetailView&record={$student['class_id']}' >{$student['class_name']}</a> </td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['kind_of_course']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['level']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['start_study']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['end_study']."</td>";

    $html .= "<td valign='TOP' class='oddListRowS1'>".$timedate->to_display_date($student['re_enroll_date'],false)."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".($student['re_enroll_amount'] > 1000 ? format_number($student['re_enroll_amount']) : '')."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'><a href='#bwc/index.php?module=J_Class&action=DetailView&record={$student['class_return_id']}' >{$student['class_return_name']}</a> </td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['assigned_to']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['description']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$student['epp_type']."</td>";

    $html .= '</tr>';
    //Handle count team
    $teamS[$student['team_id']]['team_id'] = $student['team_id'];
    $teamS[$student['team_id']]['team_name'] = $student['team_name'];
    $teamS[$student['team_id']]['team_code'] = $student['team_code'];
    $teamS[$student['team_id']]['epp_count'] ++;
    if($student['epp_type'] == 'Re-enroll')
        $teamS[$student['team_id']]['re_count'] ++;
    if($student['epp_type'] == 'Loss')
        $teamS[$student['team_id']]['loss_count'] ++;
}
if(empty($count)) $html .= '<tr><td colspan="100%">No Student Avaiable</td></tr>';
$html .= '</tbody></table>';

//Draw table Team
$html1 = '<table width="70%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody><tr>';
$html1 .= '<th align="center" rowspan="2" class="reportlistViewThS1" valign="middle" nowrap="">Center Name</th>';
$html1 .= '<th align="center" rowspan="2" class="reportlistViewThS1" valign="middle" nowrap="">Center Code</th>';
$html1 .= '<th colspan="5" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Renewal Students</th>';
$html1 .= '</tr>';
$html1 .= '<tr>';
$html1 .= '<th align="center" valign="middle" nowrap="">Total EPP</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Total Re-enroll</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Total Loss</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Loss (%)</th>';
$html1 .= '<th align="center" valign="middle" nowrap="">Re-enroll (%)</th>';
$html1 .= '</tr>';
$countT = 0;
foreach($teamS as $team_id => $team){
    $countT++;
    $html1 .= '<tr>';
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".$team['team_name']."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".$team['team_code']."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number($team['epp_count'])."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number($team['re_count'])."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number($team['loss_count'])."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number(($team['loss_count']/$team['epp_count']) * 100)."%</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number((1 - ($team['loss_count']/$team['epp_count'])) * 100)."%</td>";
    $html1 .= '</tr>';
    $epp_count  += $team['epp_count'];
    $re_count   += $team['re_count'];
    $loss_count += $team['loss_count'];
}
if(empty($countT))
    $html1 .= '<tr><td colspan="100%">No Student Avaiable</td></tr>';
else{
    $html1 .= "<tr style='font-weight:bold;'>";
    $html1 .= "<td style='border-right: none;'></td>";
    $html1 .= "<td style='text-align:left;'><b>Total</b></td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number($epp_count)."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number($re_count)."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number($loss_count)."</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number(($loss_count/$epp_count) * 100)."%</td>";
    $html1 .= "<td valign='TOP' class='oddListRowS1'>".format_number( (1- ($loss_count/$epp_count) ) * 100)."%</td>";
    $html1 .= '</tr>';
}


$html1 .= '</tbody></table>';
echo "
<h1>SUMMARY: Renewal by Center</h1><br>".$html1."<br><br>
<h1>Renewal Student List</h1><br>".$html;



//-(*(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)

$js ="<script>
DOTB.util.doWhen(
function() {
return $('#rowid1').find('td').eq(3).length == 1;
},
function() {
$('#filters_top').find('tr#rowid1').find('td:eq(1)').html('&nbsp;&nbsp;&nbsp;<b>Renewal</b>');
$('#filters_top').find('tr#rowid1').find('td:eq(3)').html('&nbsp;&gt;&nbsp;EPP Date');
});
</script>";
echo $js;

die();