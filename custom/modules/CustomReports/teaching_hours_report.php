<?php

$query =str_replace("DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%a %d/%m') l1_day_date_start,",
    "IFNULL(l3.id,'') team_id,
    IFNULL(l3.name,'') team_name,
    IFNULL(c_teachers.id,'') primaryId,
    IFNULL(c_teachers.hr_code,'') teacher_code,
    IFNULL(c_teachers.teacher_id,'') teacher_id,
    REPLACE(IFNULL(c_teachers.type,''), '^', '') type,
    IFNULL(c_teachers.full_teacher_name,'') full_teacher_name,
    IFNULL(l4.name,'') course,
    IFNULL(l2.level,'')level,
    SUM(IFNULL(l1.late_time, 0)) l1_sum_late_time,
    (CASE WHEN c_teachers.id = l1.teacher_id THEN 'Teaching Hours' ELSE 'TA Hours' END) hour_type,
    COUNT(DISTINCT l1.id) count_ss,",
    $this->$query_name);

$query =substr($query, 0, strpos($query,'GROUP BY'));   //remove Group By
$query =" $query GROUP BY IFNULL(c_teachers.id,'') ASC, (CASE WHEN c_teachers.id = l1.teacher_id THEN 'Teaching Hours' ELSE 'TA Hours' END) DESC,IFNULL(c_teachers.team_id,''), IFNULL(l4.name,''), IFNULL(l2.level,'')";

$rows = $GLOBALS['db']->fetchArray($query);

$arrHour = array();
foreach($rows as $row){
    $arrHour[$row['hour_type'].$row['primaryId']]['id']            = $row['primaryId'];
    $arrHour[$row['hour_type'].$row['primaryId']]['name']          = $row['full_teacher_name'];
    $arrHour[$row['hour_type'].$row['primaryId']]['teacher_id']    = $row['teacher_id'];
    $arrHour[$row['hour_type'].$row['primaryId']]['teacher_code']  = $row['teacher_code'];
    $arrHour[$row['hour_type'].$row['primaryId']]['teacher_type']  = $row['type'];
    $arrHour[$row['hour_type'].$row['primaryId']]['hour_type']     = $row['hour_type'];
    $arrHour[$row['hour_type'].$row['primaryId']]['count_row']    += 1;
    $arrHour[$row['hour_type'].$row['primaryId']]['total_hours']  += $row['l1_sum_duration_cal'];
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['count_koc']                            += 1; //Count Row KOC
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['sum_duration_koc']                     += $row['l1_sum_duration_cal'];
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['sum_session_koc']                      += $row['count_ss'];
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['name']                                  = $row['course'];
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['levels'][$row['level']]['name']         = $row['level'];
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['levels'][$row['level']]['sum_duration']+= $row['l1_sum_duration_cal'];
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['levels'][$row['level']]['sum_late']    += $row['l1_sum_late_time'];
    $arrHour[$row['hour_type'].$row['primaryId']]['courses'][$row['course']]['levels'][$row['level']]['sum_session'] += $row['count_ss'];
}
$html = '<h3>SUMMARY HOURS:</h3><br>';

$html .= '<table width="80%" border="0" cellpadding="0" cellspacing="0" class="reportlistView">';

$html .= '<thead><tr>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">No.</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Teacher ID</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Teacher Code</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Teacher Name</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Hour Type</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Course</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Level</th>';
$html .= '<th align="center" class="reportlistViewThS1" valign="middle">Sum: Hours</th>';
$html .= '<th align="center" class="reportlistViewThS1" valign="middle">Sum: Late</th>';
$html .= '<th align="center" class="reportlistViewThS1" valign="middle">Count: Session</th>';
$html .= '<th align="center" class="reportlistViewThS1" valign="middle" colspan="4" width="15%">Note</th></tr>';
$html .= '</thead>';

$html .= "<tbody>";
$count= 0;
foreach ($arrHour as $tea_id => $tea){
    $total_hour = $total_late = $countss = 0;
    $count++;
    $tt_row = $tea['count_row']+1; //+1 dòng SUM
    $html .= "<tr>";
    $html .= "<td valign='middle' class='oddListRowS1' rowspan='".$tt_row."'>$count</td>";
    $html .= "<td valign='middle' class='oddListRowS1' rowspan='".$tt_row."'>{$tea['teacher_id']}</td>";
    $html .= "<td valign='middle' class='oddListRowS1' rowspan='".$tt_row."'>{$tea['teacher_code']}</td>";
    $html .= "<td valign='middle' class='oddListRowS1' rowspan='".$tt_row."'>{$tea['name']}</td>";
    $html .= "<td valign='middle' class='oddListRowS1' rowspan='".$tt_row."'>{$tea['hour_type']}</td>";


    $count_c = 0;
    foreach ($tea['courses'] as $koc => $kocOptions){
        $count_c++;
        if($count_c > 1)  $html .= "<tr>";
        //level
        $count_l = 0;
        foreach($kocOptions['levels'] as $lvl => $lvlOptions){
            $count_l++;
            if($count_l == 1) $html .= "<td valign='middle' class='oddListRowS1' rowspan='".$kocOptions['count_koc']."'>{$kocOptions['name']}</td>";
            if($count_l > 1)  $html .= "<tr>";
            $html .= "<td valign='middle' class='oddListRowS1''>{$lvlOptions['name']}</td>";
            $html .= "<td valign='middle' class='oddListRowS1''>".format_number($lvlOptions['sum_duration'],4,4)."</td>";
            $html .= "<td valign='middle' class='oddListRowS1''>".format_number($lvlOptions['sum_late'],4,4)."</td>";
            $html .= "<td valign='middle' class='oddListRowS1''>".format_number($lvlOptions['sum_session'])."</td>";

            if($count_c == 1 && $count_l == 1){
                $html .= "<td valign='middle' class='oddListRowS1' colspan='3' rowspan='".$tt_row."'></td>";
                $html .= "</tr>";

            }
            $total_hour += $lvlOptions['sum_duration'];
            $total_late += $lvlOptions['sum_late'];
            $countss    += $lvlOptions['sum_session'];
        }
        if($count_c > 1) $html .= "</tr>";
    }
    $html .= "<tr>";
    $html .= "<td valign='middle' colspan='2' class='oddListRowS1'>SUM </td>";
    $html .= "<td valign='middle' class='oddListRowS1'>".format_number($total_hour,4,4)."</td>";
    $html .= "<td valign='middle' class='oddListRowS1'>".format_number($total_late,4,4)."</td>";
    $html .= "<td valign='middle' class='oddListRowS1'>".format_number($countss)."</td>";
    $html .= "</tr>";
}
$html .= "</tbody></table>";
$html .= "<br><br>";
$html .= "<h3>DETAIL:</h3>";

echo $html;
?>
