<?php
global $db;

$rows = $GLOBALS['db']->fetchArray($this->query);
$ssids = implode("','",array_column($rows,'primaryid'));

$filter = $this->where;
$parts = explode("AND", $filter);

for($i = 0; $i < count($parts); $i++){
    if(strpos($parts[$i], "l1.date_start >= '") !== FALSE) $start_date = get_string_between($parts[$i]);
    if(strpos($parts[$i], "l1.date_start <= '") !== FALSE) $end_date     = get_string_between($parts[$i]);
    if(strpos($parts[$i], "l1.date_start='") !== FALSE){
        $start_date = get_string_between($parts[$i]);
        $end_date   = $start_date;
    }
}

$q1 = "SELECT DISTINCT
IFNULL(l1.name, '') center_name,
IFNULL(leads.lead_source, '') AS source,
COUNT(leads.id) total_new_leads,
SUM(IFNULL(leads.converted, 0)) lead_converted
FROM
leads
INNER JOIN teams l1 ON
leads.team_id = l1.id AND l1.deleted = 0
WHERE
leads.id IN ('{$ssids}')
GROUP BY l1.id, IFNULL(leads.lead_source, ''), IFNULL(leads.utm_source, '')
ORDER BY center_name ASC, IFNULL(leads.lead_source, '') ASC";

$result1 = $db->query($q1);
$report_detail = array();
while ($row = $db->fetchByAssoc($result1)){
    $report_detail[$row['center_name']][$row['source']]['total_new_leads'] = $row['total_new_leads'];
    $report_detail[$row['center_name']][$row['source']]['pt_registration'] = 0;
    $report_detail[$row['center_name']][$row['source']]['pt_taker'] = 0;
    $report_detail[$row['center_name']][$row['source']]['lead_converted'] = $row['lead_converted'];
}

$q2 = "SELECT DISTINCT
IFNULL(l1.name, '') center_name,
IFNULL(leads.lead_source, '') AS source,
COUNT(l2.id) pt_registration,
SUM(CASE WHEN ((l2.attended LIKE 'Yes' OR l2.attended = '1')) THEN 1 ELSE 0 END) pt_taker
FROM leads
INNER JOIN teams l1 ON leads.team_id = l1.id AND l1.deleted = 0
LEFT JOIN j_ptresult l2 ON l2.student_id = leads.id AND l2.deleted = 0 AND IFNULL(l2.type_result, '') = 'Placement Test'
WHERE leads.id IN ('{$ssids}')
GROUP BY l1.id, IFNULL(leads.lead_source, ''), IFNULL(leads.utm_source, '')
ORDER BY center_name ASC, IFNULL(leads.lead_source, '') ASC, IFNULL(leads.utm_source, '') ASC";
$result2 = $db->query($q2);
while($row = $db->fetchByAssoc($result2)){
    $report_detail[$row['center_name']][$row['source']]['pt_registration'] = $row['pt_registration'];
    $report_detail[$row['center_name']][$row['source']]['pt_taker'] = $row['pt_taker'];
}

$html = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><thead>';

$html .= '<tr><th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Center Name</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Source</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Total New Leads</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">PT Registration</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">PT Taker</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Number of new students base on leads</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Enrol/Leads (%)</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">PT Reg/Leads (%)</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">PT Taker/PT Reg (%)</th>';
$html .= '<th rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Enrol/PT taker (%)</th></tr>';
$html .= '</thead>';

$html .= "<tbody>";
foreach ($report_detail as $center_name => $arr_source){
    foreach ($arr_source as $source => $arr_val){
            $html .= "<tr>";
            $html .= "<td valign='TOP' class='oddListRowS1'>$center_name</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>$source</td>";
            foreach ($arr_val as $val){
                $html .= "<td valign='TOP' class='oddListRowS1'>$val</td>";
            }
            $html .= "<td valign='TOP' class='oddListRowS1'>" . number_format($arr_val['lead_converted']/$arr_val['total_new_leads'], 2) . "%</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>" . number_format($arr_val['pt_registration']/$arr_val['total_new_leads'], 2) . "%</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>" . number_format($arr_val['pt_taker']/$arr_val['pt_registration'], 2) . "%</td>";
            $html .= "<td valign='TOP' class='oddListRowS1'>" . number_format($arr_val['lead_converted']/$arr_val['pt_taker'], 2) . "%</td>";
            $html .= "</tr>";
    }
}
$html .= "</tbody></table>";

echo $html;
die();




