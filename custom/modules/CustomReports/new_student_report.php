<?php
//add student id
$query =str_replace("SELECT DISTINCT", "SELECT DISTINCT IFNULL(l2.id, '') student_id,", $this->$query_name);
$query =str_replace("GROUP BY", "GROUP BY l2.id,", $query);

$rows = $GLOBALS['db']->fetchArray($query);
$student_ids = implode("','",array_column($rows,'student_id'));

$filter = str_replace(' ', '', $this->where);
$parts  = explode("AND", $filter);
for ($i = 0; $i < count($parts); $i++) {
    if (strpos($parts[$i], "IFNULL(j_payment.payment_type,'')IN") !== FALSE) $payment_type = get_string_between($parts[$i],"IN(",")");

    if(strpos($parts[$i], "j_payment.kind_of_courseNOTLIKE") !== FALSE) $koc_not_like[] = get_string_between($parts[$i],"NOTLIKE'","'");
    if(strpos($parts[$i], "j_payment.kind_of_courseLIKE") !== FALSE) $koc_like = explode("ORj_payment.kind_of_courseLIKE",get_string_between($parts[$i],"kind_of_courseLIKE'","'))"));
}
//Custom KOC
if(!empty($koc_not_like)){
  foreach($koc_not_like as $vue) {$ext_koc .= "AND l1.kind_of_course NOT LIKE '$vue' ";}
  if(!empty($ext_koc)) $ext_koc = "AND ((".substr($ext_koc,4).") OR (IFNULL(l1.kind_of_course,'')='') )";
}

if(!empty($koc_like)){
  foreach($koc_like as $vue) {$ext_koc .= "OR l1.kind_of_course LIKE '".str_replace("'",'',$vue)."' ";}
  if(!empty($ext_koc)) $ext_koc = "AND (".substr($ext_koc,3).")";
}


//PAYMENT
$sql = "SELECT DISTINCT
IFNULL(j_paymentdetail.id, '') primaryid,
IFNULL(j_paymentdetail.payment_id, '') payment_id,
j_paymentdetail.payment_date receipt_date,
IFNULL(l2.id, '') student_id
FROM j_paymentdetail
INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
INNER JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
WHERE (j_paymentdetail.status = 'Paid')
AND (j_paymentdetail.payment_amount > 0)
AND (l1.payment_type IN ($payment_type))
AND (l2.id IN ('$student_ids')) $ext_koc
AND j_paymentdetail.deleted = 0
ORDER BY student_id, j_paymentdetail.payment_datetime ASC";
$row_payment = $GLOBALS['db']->fetchArray($sql);
$f_paymentDetail = array();
$flag_ = '###';
foreach( $row_payment as $key => $row_p){
    if($flag_ != $row_p['student_id']){
        $flag_ = $row_p['student_id'];
        $f_paymentDetail[] = $row_p['primaryid'];
    }
}

$str0 = implode("','",$f_paymentDetail);
$this->str0 = "j_payment.deleted=0 AND l3.id IN ('{$str0}')";
$this->$query_name =str_replace('j_payment.deleted=0', $this->str0, $this->$query_name);
//echo $this->$query_name;


?>
