<?php
$querya = $this->$query_name;
$querya = str_replace('sum(IFNULL(l1.duration_cal,0))', "IFNULL(c_teachers.id,'') primaryId, IFNULL(c_teachers.full_teacher_name,'') full_teacher_name, REPLACE(IFNULL(IFNULL(c_teachers.type,''),''), '^', '') teacher_type, IFNULL(c_teachers.teacher_id,'') teacher_id, SUM(IFNULL(l1.duration_cal,0))", $querya);

$rows = $GLOBALS['db']->fetchArray($querya);

$arrHour = array();
foreach($rows as $row){
    $arrHour[$row['primaryId']]['teacher_id']     = $row['teacher_id'];
    $arrHour[$row['primaryId']]['teacher_name']   = $row['full_teacher_name'];
    $arrHour[$row['primaryId']]['teacher_type']   = $row['teacher_type'];
    $arrHour[$row['primaryId']]['sum_duration']  += $row['l1_sum_duration_cal'];
    $arrHour[$row['primaryId']]['count']         += $row['count'];
}
$html = '<h3>SUMMARY:</h3><br>';

$html .= '<table width="60%" border="0" cellpadding="0" cellspacing="0" class="reportlistView">';

$html .= '<thead><tr>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">No.</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Teacher ID</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Teacher Name</th>
<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Teacher Type</th>';
$html .= '<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Sum: Duration</th>';
$html .= '<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Count: Session</th>';
$html .= '<th align="center" class="reportlistViewThS1" valign="middle" nowrap="">Note</th></tr>';
$html .= '</thead>';

$html .= "<tbody>";
$count= 1;
foreach ($arrHour as $teacher_id => $value){
    $html .= "<tr>";
    $html .= "<td valign='TOP' class='oddListRowS1'>$count</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>{$value['teacher_id']}</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>{$value['teacher_name']}</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>{$value['teacher_type']}</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($value['sum_duration'],4,4)."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($value['count'])."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1' style='width: 200px;'></td>";
    $html .= "</tr>";
    $count++;
}
$html .= "</tbody></table>";
$html .= "<br><br>";
$html .= "<h3>DETAIL:</h3>";
echo $html;
?>
