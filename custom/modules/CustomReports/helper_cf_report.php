<?php
//Tính toán số dư cuối kỳ theo center
function calculate_carry($start , $end, $studentIds = array(), $teamIds = array()){
    if(!empty($studentIds)) $ext_st = "AND l3.id IN ('".implode("','", $studentIds)."')"; else $ext_st = '';
    if(!empty($teamIds)) $ext_teams = "AND l4.id IN ('".implode("','", $teamIds)."')"; else $ext_teams = '';

    $student_list = array();
    $q_collected    = Qcollected($start, $end, $ext_st, $ext_teams);
    $q_mov_tran_in  = Qmov_tran_in($start, $end, $ext_st, '' , $ext_teams);
    $q_mov_tran_out = Qmov_tran_out($start, $end,$ext_st, '' , $ext_teams);
    $q_revenue      = Qrevenue($start, $end, $ext_st, $ext_teams);
    $q_refund       = Qrefund($start, $end, $ext_st, $ext_teams);
    //Diễn giải số dư cuối kỳ
    $start_new = date('Y-m-d',strtotime("+1 day ".$end));
    $revenue_last = Qrevenue($start_new, '2100-01-01', $ext_st, $ext_teams);
    $revenue_last[0] = str_replace("revenue' ","revenue_last' ", $revenue_last[0]);
    $revenue_last[1] = str_replace("revenue' ","revenue_last' ", $revenue_last[1]);
    //query Union
    $q = "({$q_collected[0]})
    UNION ALL ({$q_collected[1]})
    UNION ALL ($q_mov_tran_in)
    UNION ALL ($q_mov_tran_out)
    UNION ALL ({$q_revenue[0]})
    UNION ALL ({$q_revenue[1]})
    UNION ALL ($q_refund)
    UNION ALL ({$revenue_last[0]})
    UNION ALL ({$revenue_last[1]})";
    $rs = $GLOBALS['db']->query($q);
    while($row = $GLOBALS['db']->fetchbyAssoc($rs)){
        switch ($row['type']) {
            case "collected":    //Get collected
                $student_list[$row['team_id']][$row['student_id']]['col_amount']  += $row['this_amount'];
                $student_list[$row['team_id']][$row['student_id']]['col_hours']   += $row['this_hours'];
                break;
            case "mov_tran_in":  //Get moving/trans in
                $student_list[$row['team_id']][$row['student_id']]['motr_in_amount']  += $row['this_amount'];
                $student_list[$row['team_id']][$row['student_id']]['motr_in_hours']   += $row['this_hours'];
                break;
            case "mov_tran_out":  //Get moving/trans Out
                $student_list[$row['team_id']][$row['student_id']]['motr_out_amount']  += $row['this_amount'];
                $student_list[$row['team_id']][$row['student_id']]['motr_out_hours']   += $row['this_hours'];
                break;
            case "revenue":  //Revenue & Drop revenue
                $student_list[$row['team_id']][$row['student_id']]['revenue_amount']  += $row['this_amount'];
                $student_list[$row['team_id']][$row['student_id']]['revenue_hours']   += $row['this_hours'];
                break;
            case "drop_revenue":  //Revenue & Drop revenue
                $student_list[$row['team_id']][$row['student_id']]['drop_revenue_amount']  += $row['this_amount'];
                $student_list[$row['team_id']][$row['student_id']]['drop_revenue_hours']   += $row['this_hours'];
                break;
            case "refund":  //get refund
                $student_list[$row['team_id']][$row['student_id']]['refund_amount']  += $row['this_amount'];
                $student_list[$row['team_id']][$row['student_id']]['refund_hours']   += $row['this_hours'];
                break;
            case "drop_revenue_last":
            case "revenue_last":
                $student_list[$row['team_id']][$row['student_id']]['revenue_last']  += $row['this_amount'];
                $student_list[$row['team_id']][$row['student_id']]['revenue_last']   += $row['this_hours'];
                break;
        }
        //calculate begin/end +
        if(in_array($row['type'],['collected','mov_tran_in'])){
            $student_list[$row['team_id']][$row['student_id']]['begin_amount']+= $row['befo_amount'];
            $student_list[$row['team_id']][$row['student_id']]['begin_hours'] += $row['befo_hours'];
            $student_list[$row['team_id']][$row['student_id']]['end_amount']  += ($row['befo_amount'] + $row['this_amount']);
            $student_list[$row['team_id']][$row['student_id']]['end_hours']   += ($row['befo_hours'] + $row['this_hours']);
        }
        //calculate begin/end -
        if(in_array($row['type'],['revenue','drop_revenue','refund','mov_tran_out'])){
            $student_list[$row['team_id']][$row['student_id']]['begin_amount']-= $row['befo_amount'];
            $student_list[$row['team_id']][$row['student_id']]['begin_hours'] -= $row['befo_hours'];
            $student_list[$row['team_id']][$row['student_id']]['end_amount']  -= ($row['befo_amount'] + $row['this_amount']);
            $student_list[$row['team_id']][$row['student_id']]['end_hours']   -= ($row['befo_hours'] + $row['this_hours']);
        }
    }
    return $student_list;
}
function Qcollected($start, $end, $ext_st, $ext_teams = '', $group_by = 'center'){
    $ext_g = "GROUP BY IFNULL(l4.id, ''), IFNULL(l3.id, '')";
    if($group_by == 'student') $ext_g = "GROUP BY IFNULL(l3.id, '')";

    $ext_team_c .= $ext_teams;

    $q_collected1 = "SELECT DISTINCT
    IFNULL(l4.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    'collected' type,
    SUM(CASE WHEN j_paymentdetail.payment_date < '$start' THEN IFNULL(j_paymentdetail.payment_amount, 0) ELSE 0 END) AS befo_amount,
    SUM(CASE WHEN j_paymentdetail.payment_date >= '$start' THEN IFNULL(j_paymentdetail.payment_amount, 0) ELSE 0 END) AS this_amount,
    SUM(CASE WHEN j_paymentdetail.payment_date < '$start' THEN ((IFNULL(j_paymentdetail.payment_amount, 0) + IFNULL(l2.deposit_amount * (j_paymentdetail.payment_amount/l2.payment_amount), 0))   / (IFNULL(l2.payment_amount + l2.paid_amount + l2.deposit_amount,0) / IFNULL(l2.total_hours, 0))) ELSE 0 END) AS befo_hours,
    SUM(CASE WHEN j_paymentdetail.payment_date >= '$start' THEN ((IFNULL(j_paymentdetail.payment_amount, 0) + IFNULL(l2.deposit_amount * (j_paymentdetail.payment_amount/l2.payment_amount), 0))  / (IFNULL(l2.payment_amount + l2.paid_amount + l2.deposit_amount,0) / IFNULL(l2.total_hours, 0))) ELSE 0 END) AS this_hours
    FROM j_paymentdetail
    INNER JOIN j_payment l2 ON j_paymentdetail.payment_id = l2.id AND l2.deleted = 0
    INNER JOIN contacts l3 ON l3.id = l2.parent_id AND l2.parent_type = 'Contacts' AND l3.deleted = 0
    INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
    WHERE j_paymentdetail.deleted = 0 $ext_st $ext_team_c
    AND j_paymentdetail.payment_date <= '$end' AND (j_paymentdetail.status = 'Paid' AND IFNULL(j_paymentdetail.is_old,0)=0)
    AND l2.payment_type IN ('Deposit', 'Cashholder')
    $ext_g
    ORDER BY student_id";

    $q_collected2 = "SELECT DISTINCT
    IFNULL(l4.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    'collected' type,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.old_remain_amount, 0) ELSE 0 END) AS befo_amount,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.old_remain_amount, 0) ELSE 0 END) AS this_amount,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS befo_hours,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS this_hours
    FROM j_payment l2
    INNER JOIN contacts l3 ON l3.id = l2.parent_id AND l2.parent_type = 'Contacts' AND l3.deleted = 0
    INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
    WHERE l2.deleted = 0 $ext_st $ext_team_c AND l2.payment_date <= '$end'
    AND (l2.is_old = 1)
    $ext_g
    ORDER BY student_id";
    return array($q_collected1, $q_collected2);
}

function Qmov_tran_in($start, $end, $ext_st, $type = '', $ext_teams = '', $group_by = 'center'){
    if(!empty($type)) $type = "'$type'";
    else $type = "'Moving In', 'Transfer In'";

    $ext_g = "GROUP BY IFNULL(l4.id, ''), IFNULL(l3.id, '')";
    if($group_by == 'student') $ext_g = "GROUP BY IFNULL(l3.id, '')";

    $ext_team_c .= $ext_teams;

    $q_mov_tran_in = "SELECT DISTINCT
    IFNULL(l4.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    'mov_tran_in' type,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.payment_amount, 0) ELSE 0 END) AS befo_amount,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.payment_amount, 0) ELSE 0 END) AS this_amount,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS befo_hours,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS this_hours
    FROM j_payment l2
    INNER JOIN contacts l3 ON l3.id = l2.parent_id AND l2.parent_type = 'Contacts' AND l3.deleted = 0
    INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
    WHERE l2.deleted = 0 $ext_st $ext_team_c
    AND l2.payment_date <= '$end' AND l2.payment_type IN ($type)
    $ext_g
    ORDER BY student_id";
    return $q_mov_tran_in;
}

function Qmov_tran_out($start, $end, $ext_st, $type = '', $ext_teams = '', $group_by = 'center'){
    if(!empty($type)) $type = "'$type'";
    else $type = "'Moving Out', 'Transfer Out'";

    $ext_g = "GROUP BY IFNULL(l4.id, ''), IFNULL(l3.id, '')";
    if($group_by == 'student') $ext_g = "GROUP BY IFNULL(l3.id, '')";

    $ext_team_c .= $ext_teams;

    $q_mov_tran_out = "SELECT DISTINCT
    IFNULL(l4.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    'mov_tran_out' type,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.payment_amount, 0) ELSE 0 END) AS befo_amount,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.payment_amount, 0) ELSE 0 END) AS this_amount,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS befo_hours,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS this_hours
    FROM j_payment l2
    INNER JOIN contacts l3 ON l3.id = l2.parent_id AND l2.parent_type = 'Contacts' AND l3.deleted = 0
    INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
    WHERE l2.deleted = 0 $ext_st $ext_team_c
    AND l2.payment_date <= '$end' AND l2.payment_type IN ($type)
    $ext_g
    ORDER BY student_id";
    return $q_mov_tran_out;
}

function Qrevenue($start, $end, $ext_st, $ext_teams = '', $group_by = 'center'){
    $ernCf = loadEnrollmentConfig();
    if($ernCf['allow_settle']){
        $ext_cp1 = "AND (jst.settle_date < '$start' OR DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') >= DATE_FORMAT(jst.settle_date,'%Y-%m-01'))";
        $ext_cp2 = "OR (jst.settle_date >= '$start' AND DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') < DATE_FORMAT(jst.settle_date,'%Y-%m-01'))";
    }
    $ext_g = "GROUP BY IFNULL(l4.id, ''), IFNULL(l3.id, '')";
    if($group_by == 'student') $ext_g = "GROUP BY IFNULL(l3.id, '')";

    $ext_team_c .= $ext_teams;

    $q_revenue = "SELECT DISTINCT
    IFNULL(l4.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    'revenue' type,
    SUM(CASE WHEN ((DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') < '$start') $ext_cp1) THEN ROUND(l1.duration_hours+(l1.duration_minutes/60),9) * ROUND(jst.total_amount/(jst.total_minute/60),9) ELSE 0 END) befo_amount,
    SUM(CASE WHEN ((DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') >= '$start') $ext_cp2) THEN ROUND(l1.duration_hours+(l1.duration_minutes/60),9) * ROUND(jst.total_amount/(jst.total_minute/60),9) ELSE 0 END) this_amount,
    SUM(CASE WHEN ((DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') < '$start') $ext_cp1) THEN ROUND(l1.duration_hours+(l1.duration_minutes/60),9) ELSE 0 END) befo_hours,
    SUM(CASE WHEN ((DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') >= '$start') $ext_cp2) THEN ROUND(l1.duration_hours+(l1.duration_minutes/60),9) ELSE 0 END) this_hours
    FROM meetings l1
    INNER JOIN meetings_contacts l1_1 ON l1.id = l1_1.meeting_id AND l1_1.deleted = 0
    INNER JOIN j_studentsituations jst ON jst.id = l1_1.situation_id AND jst.deleted = 0
    INNER JOIN j_class cls ON l1.ju_class_id = cls.id AND cls.deleted = 0
    INNER JOIN contacts l3 ON jst.student_id = l3.id AND l3.deleted = 0 AND jst.student_type = 'Contacts'
    INNER JOIN j_payment l2 ON jst.payment_id = l2.id AND l2.deleted = 0
    INNER JOIN teams l4 ON jst.team_id = l4.id AND l4.deleted = 0
    WHERE l1.session_status <> 'Cancelled'
    AND (DATE_FORMAT(l1.date_start + INTERVAL 420 MINUTE,'%Y-%m-%d') <= '$end')
    AND (jst.settle_date <= '$end')
    AND jst.deleted = 0 $ext_st $ext_team_c
    $ext_g
    ORDER BY student_id ASC";

    $q_drop_revenue = "SELECT DISTINCT
    IFNULL(l4.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    'drop_revenue' type,
    SUM(CASE WHEN l1.date_input < '$start' THEN IFNULL(l1.amount, 0) ELSE 0 END) AS befo_amount,
    SUM(CASE WHEN l1.date_input >= '$start' THEN IFNULL(l1.amount, 0) ELSE 0 END) AS this_amount,
    SUM(CASE WHEN l1.date_input < '$start' THEN IFNULL(l1.duration, 0) ELSE 0 END) AS befo_hours,
    SUM(CASE WHEN l1.date_input >= '$start' THEN IFNULL(l1.duration, 0) ELSE 0 END) AS this_hours
    FROM c_deliveryrevenue l1
    INNER JOIN j_payment l2 ON l1.ju_payment_id = l2.id AND l2.deleted = 0
    INNER JOIN contacts l3 ON l3.id = l2.parent_id AND l2.parent_type = 'Contacts' AND l3.deleted = 0
    INNER JOIN teams l4 ON l1.team_id = l4.id AND l4.deleted = 0
    WHERE l1.deleted = 0 $ext_st $ext_team_c AND l1.date_input <= '$end'
    $ext_g
    ORDER BY student_id";
    return array($q_revenue, $q_drop_revenue);
}

function Qrefund($start, $end, $ext_st, $ext_teams = '', $group_by = 'center'){
    $ext_g = "GROUP BY IFNULL(l4.id, ''), IFNULL(l3.id, '')";
    if($group_by == 'student') $ext_g = "GROUP BY IFNULL(l3.id, '')";

    $ext_team_c .= $ext_teams;

    $q_refund = "SELECT DISTINCT
    IFNULL(l4.id, '') team_id,
    IFNULL(l3.id, '') student_id,
    'refund' type,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.payment_amount, 0) ELSE 0 END) AS befo_amount,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.payment_amount, 0) ELSE 0 END) AS this_amount,
    SUM(CASE WHEN l2.payment_date < '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS befo_hours,
    SUM(CASE WHEN l2.payment_date >= '$start' THEN IFNULL(l2.total_hours, 0) ELSE 0 END) AS this_hours
    FROM j_payment l2
    INNER JOIN contacts l3 ON l3.id = l2.parent_id AND l2.parent_type = 'Contacts' AND l3.deleted = 0
    INNER JOIN teams l4 ON l2.team_id = l4.id AND l4.deleted = 0
    WHERE l2.deleted = 0 $ext_st $ext_team_c
    AND l2.payment_date <= '$end' AND l2.payment_type IN ('Refund')
    $ext_g
    ORDER BY student_id";
    return $q_refund;
}
?>
