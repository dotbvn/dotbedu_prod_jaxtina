<?php
global $timedate, $current_user;

$r0 = $GLOBALS['db']->fetchArray($this->query);
$lead_ids =  implode("','", array_unique(array_column($r0,'primaryid')));
$template_filter_arr = array_unique(array_column($r0,'l2_id'));

$_filterNullJourneyId = $_filterEmpty = false;
$_filterJourneyId = "";
if (($key = array_search("", $template_filter_arr)) !== false) {
    unset($template_filter_arr[$key]);
    $_filterNullJourneyId = true;
}

$filter = str_replace(' ', '', $this->where);
$parts  = explode("AND", $filter);
for ($i = 0; $i < count($parts); $i++) {
    if (strpos($parts[$i], "l2.id") !== FALSE) {
        $_filterJourneyId = get_string_between($parts[$i]);
    }
    if (strpos($parts[$i], "(((coalesce(LENGTH(l2.name),0)=0))") !== FALSE) {
        $_filterEmpty = true;
    }
}

$ext = "";
$templateIds = implode("','", $template_filter_arr);

if ($_filterEmpty) $ext = "AND l4.id IS NULL";
elseif ($_filterNullJourneyId) $ext = "AND (l4.id IN ('$templateIds') OR l4.id IS NULL)";
else $ext = "AND l4.id IN ('$templateIds')";

$q = "SELECT DISTINCT
    IFNULL(l4.name, '') dri_workflow_templates_name,
    IFNULL(l4.id, '') dri_workflow_templates_id,
    RTRIM(CONCAT(IFNULL(l1.name_2, ''), ' ', IFNULL(l1.name, ''))) team_name,
    COUNT(leads.id) cnt_total_leads,
    SUM(CASE WHEN leads.status = 'Dead' THEN 1 ELSE 0 END) cnt_dead,
    (SUM(CASE WHEN leads.status = 'Dead' THEN 1 ELSE 0 END)/COUNT(leads.id))*100 dead_rate,
    SUM(CASE WHEN leads.status = 'Converted' THEN 1 ELSE 0 END) cnt_converted,
    (SUM(CASE WHEN leads.status = 'Converted' THEN 1 ELSE 0 END)/COUNT(leads.id))*100 converted_rate,
    MIN(CASE WHEN leads.status = 'Converted' THEN DATEDIFF(l2.date_entered, leads.date_entered) END) min_conversion_time,
    MAX(CASE WHEN leads.status = 'Converted' THEN DATEDIFF(l2.date_entered, leads.date_entered) END) max_conversion_time,
    AVG(CASE WHEN leads.status = 'Converted' THEN DATEDIFF(l2.date_entered, leads.date_entered) END) avg_conversion_time
FROM leads
INNER JOIN teams l1 ON leads.team_id = l1.id AND l1.deleted = 0
LEFT JOIN contacts l2 ON leads.contact_id = l2.id AND l2.deleted = 0
LEFT JOIN dri_workflows l3 ON leads.id = l3.lead_id AND l3.deleted = 0
LEFT JOIN dri_workflow_templates l4 ON l3.dri_workflow_template_id = l4.id AND l4.deleted = 0
WHERE leads.deleted = 0 {$ext} AND leads.id IN ('$lead_ids')
GROUP BY dri_workflow_templates_name , team_name";
$rows = $GLOBALS['db']->fetchArray($q);

$templates = array();
foreach ($rows as $row) {
    if (!array_key_exists($row['dri_workflow_templates_name'], $templates)) {
        $templates[$row['dri_workflow_templates_name']]['id'] = $row['dri_workflow_templates_id'];
        $templates[$row['dri_workflow_templates_name']]['name'] = empty($row['dri_workflow_templates_name']) ? "-none-" : $row['dri_workflow_templates_name'];
        $templates[$row['dri_workflow_templates_name']]['centers'] = array();
    }
    if (!array_key_exists($row['team_name'], $templates[$row['dri_workflow_templates_name']]['centers'])) {
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['team_name'] = $row['team_name'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['cnt_total_leads'] = $row['cnt_total_leads'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['cnt_dead'] = $row['cnt_dead'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['dead_rate'] = $row['dead_rate'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['cnt_converted'] = $row['cnt_converted'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['converted_rate'] = $row['converted_rate'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['min_conversion_time'] = empty($row['min_conversion_time']) ? "-" : $row['min_conversion_time'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['max_conversion_time'] = empty($row['max_conversion_time']) ? "-" : $row['max_conversion_time'];
        $templates[$row['dri_workflow_templates_name']]['centers'][$row['team_name']]['avg_conversion_time'] = $row['avg_conversion_time'];
    }
}

// Get all customer journey templates
$qCenter = "SELECT DISTINCT IFNULL(teams.id, '') primaryid, RTRIM(CONCAT(IFNULL(teams.name_2, ''), ' ', IFNULL(teams.name, ''))) teams_name
FROM teams WHERE teams.private = 0 AND teams.deleted = 0 ";
$rCenter = $GLOBALS['db']->fetchArray($qCenter);
$center_arr = array_unique(array_column($rCenter,'teams_name'));

//Draw table
$html = "";
$html_table_space = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportGroupBySpaceTableView"><tbody>';
$html_table_space .= '<tr height="1"><td width="3%">&nbsp;</td></tr></tbody></table>';

if (!$_filterJourneyId) {
    if (!$_filterEmpty) {
        // Filter Template: Select all
        $qTemplate = "SELECT DISTINCT IFNULL(dri_workflow_templates.id, '') primaryid, IFNULL(dri_workflow_templates.name, '') dri_workflow_templates_name
        FROM dri_workflow_templates WHERE dri_workflow_templates.deleted = 0 ORDER BY dri_workflow_templates_name";
        $rTemplates = $GLOBALS['db']->fetchArray($qTemplate);
        foreach ($rTemplates as $template) {
            $html .= renderBody($template['primaryid'], $template['dri_workflow_templates_name'], $templates[$template['dri_workflow_templates_name']], $center_arr);
            $html .= $html_table_space;
        }
    }
    // Filter Template: Is Empty
    $html .= renderBody("", "-none-", $templates[""], $center_arr);
    $html .= $html_table_space;
} else {
    // Filter Template: Is {Template Name}
    $templateBean = BeanFactory::getBean('DRI_Workflow_Templates', $_filterJourneyId);
    $html .= renderBody($_filterJourneyId, $templateBean->name, $templates[$templateBean->name], $center_arr);
    $html .= $html_table_space;
}

$html .= '</tbody></table>';
$html .= '<script type="text/javascript">
  function toggleTable(elementId) {
    let element = $("." + elementId);
    let hidden = element.attr("hidden");

    if (hidden) {
       element.removeAttr("hidden");
       $("#img_" + elementId + " img").attr("src", "themes/default/images/basic_search.gif?v=ayapz0KWkQgC2uc1iKv6Eg");
    } else {
       element.attr("hidden", "hidden");
       $("#img_" + elementId + " img").attr("src", "themes/default/images/advanced_search.gif?v=ayapz0KWkQgC2uc1iKv6Eg");
    }
  }
</script>';

echo "<h1>Customer Journey Efficiency Assessment by Center Conversion Rate</h1><br>".$html;
die();

function renderHeader($template_name, $template_order, $count_total_applied) {
    $html_header = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody>';
    $html_header .= '<tr style="line-height: 20px;"><th colspan="10" style="text-align: left; background: LightGrey; font-size: 13px;">';
    $html_header .= '<span id="img_combo_summary_div_'.$template_order.'"><a href="javascript:void(0)" onclick="toggleTable(\'combo_summary_div_'.$template_order.'\');"><img width="8" height="8" border="0" absmiddle="" alt="Show" src="themes/default/images/basic_search.gif?v=ayapz0KWkQgC2uc1iKv6Eg"></a></span>';
    $html_header .= ' Customer Journey Template: '.$template_name.', Total: '.$count_total_applied.'</th></tr>';
    $html_header .= '<tr class="combo_summary_div_' . $template_order . '" style="font-weight:bold;">';
    $html_header .= '<td rowspan="2" align="center" valign="middle" nowrap="">No.</td>';
    $html_header .= '<td width="18%" rowspan="2" align="center" valign="middle" nowrap="" style="text-align: left;">Center</td>';
    $html_header .= '<td width="10%" rowspan="2" align="center" valign="middle" nowrap="">Total Records</td>';
    $html_header .= '<td colspan="2" align="center" valign="middle" nowrap="">Dead</td>';
    $html_header .= '<td colspan="2" align="center" valign="middle" nowrap="">Converted</td>';
    $html_header .= '<td colspan="3" align="center" valign="middle" nowrap="">Time to Conversion (Day)</td></tr>';
    $html_header .= '<tr class="combo_summary_div_' . $template_order . '" style="font-weight:bold;">';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Count</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Percent (%)</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Count</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Percent (%)</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Min</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Max</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Average</td>';
    $html_header .= '</tr>';
    return $html_header;
}

function renderRow($data) {
    $html_row = "<td valign='TOP' class='oddListRowS1' style='text-align: left;'>".$data['team_name']."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>".$data['cnt_total_leads']."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>".$data['cnt_dead']."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>".format_number($data['dead_rate'], 2,2)."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>".$data['cnt_converted']."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>".format_number($data['converted_rate'], 2,2)."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>".$data['min_conversion_time']."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>".$data['max_conversion_time']."</td>";
    $html_row .= "<td valign='TOP' class='oddListRowS1'>". (($data['cnt_converted'] == 0) ? "-" : format_number($data['avg_conversion_time'], 2,2))."</td>";
    return $html_row;
}

function renderBody($template_id, $template_name, $template, $center_arr)
{
    $count = 1;
    $count_total_applied = 0;
    $html = $html_body = "";
    foreach ($center_arr as $center_name) {
        $html_body .= "<tr class='combo_summary_div_" . $template_id . "'>";
        $html_body .= "<td valign='TOP' class='oddListRowS1'>" . $count++ . "</td>";
        if (!array_key_exists($center_name, $template['centers'])) {
            $data['team_name'] = $center_name;
            $data['cnt_total_leads'] = $data['cnt_dead'] = $data['cnt_converted'] = $data['dead_rate'] = $data['converted_rate'] = 0;
            $data['min_conversion_time'] = $data['max_conversion_time'] = '-';
            $html_body .= renderRow($data);
        } else {
            $data = $template['centers'][$center_name];
            $count_total_applied += $data['cnt_total_leads'];
            $html_body .= renderRow($data);
        }
        $html_body .= '</tr>';
    }
    $html .= renderHeader($template_name, $template_id, $count_total_applied) . $html_body;
    $html .= '</tbody></table>';
    return $html;
}

//-(*(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)