<?php

$pc_list = array();
$teacher=array();
$teacher_list = array();
$tch_list=array();

$rows = $GLOBALS['db']->fetchArray($this->query);
foreach ($rows as $key => $row) {
    $primary_center = $row['l1_name']; //center name
    $teacher[]= $row['primaryid']; //teacher_id
    if (!in_array($row['primaryid'], $tch_list)) {
        $tch_list[] = $row['primaryid'];
        $pc_list[$primary_center] += 1;
        $teacher_list[$primary_center][$row['primaryid']]['full_teacher_name'] = $row['c_teachers_last_name'] . ' ' . $row['c_teachers_first_name'];
        $teacher_list[$primary_center][$row['primaryid']]['primary_center'] = $primary_center;
        $teacher_list[$primary_center][$row['primaryid']]['class_name'] = $row['l2_name'];
        $teacher_list[$primary_center][$row['primaryid']]['level'] = $row['l2_level'];
        $teacher_list[$primary_center][$row['primaryid']]['start_date'] = $row['l2_start_date'];
        $teacher_list[$primary_center][$row['primaryid']]['end_date'] = $row['l2_end_date'];
        $teacher_list[$primary_center][$row['primaryid']]['status'] = $row['c_teachers_status'];
    }
}

$kocc_list = array();
$html='';
foreach ($teacher_list as $primary_center => $class_obj) {
    $no   = 1;
    $html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody>';
    $html .= get_table_head($primary_center);
    foreach ($class_obj as $class_id => $val) {
        if (!empty($val['end_date'])) {
            $month_end = date('m', strtotime($val['end_date']));
        } else {$month_end = '';}

        $html .= "<tr><td valign='TOP' class='oddListRowS1'>".$no++."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['full_teacher_name']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['contract_info']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['class_name']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['level']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['start_date']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['end_date']."</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>$month_end</td>";
        $html .= "<td valign='TOP' class='oddListRowS1'>".$val['status']."</td>";

    }
    $html .= "</tbody></table>";
    $html .= "<br>";
}
echo $html;
die();

//Header
function get_table_head($primary_center = '')
{
    $html = "<tr><td colspan='100%' style='text-align: left;'><h3 style='color: #000 !important;'>Primary Center =" . strtoupper($primary_center) . "</h3></td></tr>";
    $html .= '<tr><th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">No.</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Giáo viên</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Full time/Part time</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Lớp</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Dòng lớp</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Ngày khai giảng chính thức</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Ngày kết thúc</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Tháng kết thúc</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Trạng thái (GV)</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Phần trăm GV đảm nhận lớp học</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Điểm đánh giá đầu vào</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Tổng điểm (100%)</th>';
    $html .= '<th rowspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Đánh giá</th>';
    $html .= '<th colspan="6" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Điểm sự vụ GV</th>';
    $html .= '<th colspan="3" rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Mức độ hài lòng của học viên</th>';
    $html .= '<th colspan="3" rowspan="2" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Tỉ lệ HV đạt chuẩn/ vượt chuẩn đầu ra khóa học</th>';
    $html .= '<th colspan="6" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Nhận xét của AC/ nhân sự khác</th>';
    $html .= '</tr>';

    $html .= '<tr>';
    $html .= '<th colspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Tỉ lệ chuyên cần của GV</th>';
    $html .= '<th colspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Feedback của HV</th>';

    $html .= '<th colspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Điểm dự giờ lớp học</th>';
    $html .= '<th colspan="3" align="center" class="reportlistViewThS1" valign="middle" nowrap="">Phối hợp với AC</th>';
    $html .= '</tr>';

    $html .= '<tr>';
    $html .= '<th align="center" valign="middle" nowrap="">Giá trị (100)</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Tỉ trọng</th>';
    $html .= '<th align="center" valign="middle">Thực đạt</th>';

    $html .= '<th align="center" valign="middle" nowrap="">Giá trị (100)</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Tỉ trọng</th>';
    $html .= '<th align="center" valign="middle">Thực đạt</th>';

    $html .= '<th align="center" valign="middle" nowrap="">Giá trị (100)</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Tỉ trọng</th>';
    $html .= '<th align="center" valign="middle">Thực đạt</th>';

    $html .= '<th align="center" valign="middle" nowrap="">Giá trị (100)</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Tỉ trọng</th>';
    $html .= '<th align="center" valign="middle">Thực đạt</th>';

    $html .= '<th align="center" valign="middle" nowrap="">Giá trị (100)</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Tỉ trọng</th>';
    $html .= '<th align="center" valign="middle">Thực đạt</th>';

    $html .= '<th align="center" valign="middle" nowrap="">Giá trị (100)</th>';
    $html .= '<th align="center" valign="middle" nowrap="">Tỉ trọng</th>';
    $html .= '<th align="center" valign="middle">Thực đạt</th>';
    $html .= '</tr>';
    return $html;
}
