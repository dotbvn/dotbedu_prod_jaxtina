<?php
global $timedate, $current_user;

$r0 = $GLOBALS['db']->fetchArray($this->query);
$task_ids =  implode("','", array_unique(array_column($r0,'primaryid')));
$template_ids = array_unique(array_column($r0,'l1_id'));

$_filterJourneyId = "";
$filter = str_replace(' ', '', $this->where);
$parts  = explode("AND", $filter);
for ($i = 0; $i < count($parts); $i++) {
    if (strpos($parts[$i], "l1.id=") !== FALSE) {
        $_filterJourneyId = get_string_between($parts[$i]);
    }
}

$q = "SELECT DISTINCT
    IFNULL(l3.id, '') dri_workflow_templates_id,
    IFNULL(l3.name, '') dri_workflow_templates_name,
	l4.sort_order stage_sort_order,
    IFNULL(l4.id, '') stage_id,
    IFNULL(l4.name, '') stage_name,
    IFNULL(l5.sort_order, '') cj_sort_order,
	IFNULL(l5.name, '') cj_task_template_name,
	IFNULL(tasks.name, '') task_name,
	COUNT(DISTINCT l2.id) total_records,
	SUM(CASE WHEN tasks.status = 'Completed' THEN 1 ELSE 0 END) cnt_completed,
	SUM(CASE WHEN tasks.status = 'Completed' THEN 1 ELSE 0 END)/COUNT(DISTINCT l2.id)*100 completed_rate,
	SUM(CASE WHEN tasks.status = 'Not Applicable' THEN 1 ELSE 0 END) cnt_not_applicable,
	SUM(CASE WHEN tasks.status = 'Not Applicable' THEN 1 ELSE 0 END)/COUNT(DISTINCT l2.id)*100 not_applicable_rate,
    l5.momentum_points cj_template_momentum_points,
	l5.points cj_template_progress_points,
    AVG(tasks.customer_journey_score) avg_progress_score,
    AVG(tasks.cj_momentum_score) avg_moment_score
FROM tasks
INNER JOIN dri_subworkflows l1 ON l1.id = tasks.dri_subworkflow_id AND l1.deleted = 0
INNER JOIN dri_workflows l2 ON l2.id = l1.dri_workflow_id AND l2.deleted = 0
INNER JOIN dri_workflow_templates l3 ON l2.dri_workflow_template_id = l3.id AND l3.deleted = 0
INNER JOIN dri_subworkflow_templates l4 ON l3.id = l4.dri_workflow_template_id AND l4.deleted = 0
INNER JOIN dri_workflow_task_templates l5 ON l4.id = l5.dri_subworkflow_template_id AND l5.deleted = 0 AND l5.id = tasks.dri_workflow_task_template_id
WHERE tasks.id IN ('$task_ids') AND tasks.deleted = 0
GROUP BY dri_workflow_templates_name ASC , stage_sort_order ASC , l5.sort_order ASC, l5.name ASC, task_name";
$rows = $GLOBALS['db']->fetchArray($q);

$templates = array();
foreach ($rows as $row) {
    if (!array_key_exists($row['dri_workflow_templates_name'], $templates)) {
        $templates[$row['dri_workflow_templates_name']]['id'] = $row['dri_workflow_templates_id'];
        $templates[$row['dri_workflow_templates_name']]['name'] = $row['dri_workflow_templates_name'];
        $templates[$row['dri_workflow_templates_name']]['total_records'] = $row['total_records'];
        $templates[$row['dri_workflow_templates_name']]['stages'] = array();
    }
    if (!array_key_exists($row['stage_name'], $templates[$row['dri_workflow_templates_name']]['stages'])) {
        $templates[$row['dri_workflow_templates_name']]['stages'][$row['stage_name']]['name'] = $row['stage_name'];
        $templates[$row['dri_workflow_templates_name']]['stages'][$row['stage_name']]['activities'] = array();
    }

    $activities = array();
    $activities['name'] = $row['cj_task_template_name'];
    $activities['cnt_completed'] = $row['cnt_completed'];
    $activities['completed_rate'] = $row['completed_rate'];
    $activities['cnt_not_applicable'] = $row['cnt_not_applicable'];
    $activities['not_applicable_rate'] = $row['not_applicable_rate'];
    $activities['progress_points'] = $row['cj_template_progress_points'];
    $activities['momentum_points'] = $row['cj_template_momentum_points'];
    $activities['avg_progress_score'] = $row['avg_progress_score'];
    $activities['avg_moment_score'] = $row['avg_moment_score'];
    $templates[$row['dri_workflow_templates_name']]['stages'][$row['stage_name']]['activities'][] = $activities;
}

$html = $html_header = "";

$html_table_space = '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tbody>';
$html_table_space .= '<tr height="1"><td width="3%">&nbsp;</td></tr></tbody></table>';

if (!$_filterJourneyId) {
    // Filter: Select all
    $qTemplate = "SELECT DISTINCT IFNULL(dri_workflow_templates.id, '') primaryid, IFNULL(dri_workflow_templates.name, '') dri_workflow_templates_name
    FROM dri_workflow_templates WHERE dri_workflow_templates.deleted = 0 ORDER BY dri_workflow_templates_name";
    $rTemplates = $GLOBALS['db']->fetchArray($qTemplate);

    foreach ($rTemplates as $rTemplate) {
        $template = $templates[$rTemplate['dri_workflow_templates_name']];
        if (empty($template)) {
            $template['id'] = $rTemplate['primaryid'];
            $template['name'] = $rTemplate['dri_workflow_templates_name'];
            $template['total_records'] = 0;
            $template['stages'] = array();
        }
        $html .= renderBody($template['id'], $template['name'], $template['total_records'], $template['stages']);
        $html .= $html_table_space;
    }
} else {
    // Filter: Is <CJ Template name>
    $templateBean = BeanFactory::getBean('DRI_Workflow_Templates', $_filterJourneyId);
    $template = $templates[$templateBean->name];
    if (empty($template)) {
        $template['id'] = $templateBean->id;
        $template['name'] = $templateBean->name;
        $template['total_records'] = 0;
        $template['stages'] = array();
    }
    $html .= renderBody($template['id'], $template['name'], $template['total_records'], $template['stages']);
    $html .= $html_table_space;
}

$html .= '<script type="text/javascript">
  function toggleTable(elementId) {
    let element = $("." + elementId);
    let hidden = element.attr("hidden");

    if (hidden) {
       element.removeAttr("hidden");
       $("#img_" + elementId + " img").attr("src", "themes/default/images/basic_search.gif?v=ayapz0KWkQgC2uc1iKv6Eg");
    } else {
       element.attr("hidden", "hidden");
       $("#img_" + elementId + " img").attr("src", "themes/default/images/advanced_search.gif?v=ayapz0KWkQgC2uc1iKv6Eg");
    }
  }
</script>';
echo "<h1>Customer Journey Efficiency Assessment by Activities</h1><br>".$html;
die();

function renderHeader($template_id, $stage_name, $count_stage) {
    $html_header = '<tr style="line-height: 20px;" class="combo_summary_div_'.$template_id.'">';
    $html_header .= '<th colspan="10" style="text-align: left;">Stage '.$count_stage.': '.$stage_name.'</th></tr>';
    $html_header .= '<tr class="combo_summary_div_'.$template_id.'" style="font-weight:bold;">';
    $html_header .= '<td rowspan="2" align="center" valign="middle" nowrap="">No.</td>';
    $html_header .= '<td width="18%" rowspan="2" align="center" valign="middle" nowrap="" style="text-align: left;">Activity Name</td>';
    $html_header .= '<td colspan="2" align="center" valign="middle" nowrap="">Completed</td>';
    $html_header .= '<td colspan="2" align="center" valign="middle" nowrap="">Not Applicable</td>';
    $html_header .= '<td width="10%" colspan="2" align="center" valign="middle" nowrap="">Progress</td>';
    $html_header .= '<td width="10%" colspan="2" align="center" valign="middle" nowrap="">Momentum</td></tr>';
    $html_header .= '<tr class="combo_summary_div_'.$template_id.'" style="font-weight:bold;">';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Count</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Percent (%)</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Count</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Percent (%)</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Points</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">AVG Score</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">Points</td>';
    $html_header .= '<td width="10%" align="center" valign="middle" nowrap="">AVG Score</td>';
    $html_header .= '</tr>';

    return $html_header;
}

function renderRow($template_id, $count_activity, $activity) {
    $html = "<tr class='combo_summary_div_".$template_id."'>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$count_activity."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1' style='text-align: left;'>".$activity['name']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$activity['cnt_completed']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($activity['completed_rate'], 2,2)."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$activity['cnt_not_applicable']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($activity['not_applicable_rate'], 2,2)."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$activity['progress_points']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($activity['avg_progress_score'], 2,2)."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".$activity['momentum_points']."</td>";
    $html .= "<td valign='TOP' class='oddListRowS1'>".format_number($activity['avg_moment_score'], 2,2)."</td>";
    $html .= "</tr>";

    return $html;
}

function renderBody($template_id, $template_name, $cnt_total_records, $stages) {
    $html = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody>';
    $html .= '<tr style="line-height: 20px;"><th colspan="10" style="text-align: left; background: LightGrey; font-size: 13px;">';
    $html .= '<span id="img_combo_summary_div_'.$template_id.'"><a href="javascript:void(0)" onclick="toggleTable(\'combo_summary_div_'.$template_id.'\');"><img width="8" height="8" border="0" absmiddle="" alt="Show" src="themes/default/images/basic_search.gif?v=ayapz0KWkQgC2uc1iKv6Eg"></a></span>';
    $html .= ' Customer Journey Template: '.$template_name.', Total Applied: '.$cnt_total_records.'</th></tr>';

    $qStages = "SELECT DISTINCT IFNULL(dri_subworkflow_templates.id, '') primaryid, IFNULL(dri_subworkflow_templates.name, '') stage_name
    FROM dri_subworkflow_templates WHERE dri_subworkflow_templates.dri_workflow_template_id = '$template_id' AND dri_subworkflow_templates.deleted = 0
    ORDER BY dri_subworkflow_templates.sort_order";
    $rStages = $GLOBALS['db']->fetchArray($qStages);

    $idx_stage = 1;
    foreach ($rStages as $rStage) {
        $html .= renderHeader($template_id, $rStage['stage_name'], $idx_stage++);
        $idx_activity = 1;
        $stage = $stages[$rStage['stage_name']];
        if (count($stage['activities']) == 0) {
            $qTasks = "SELECT DISTINCT IFNULL(dri_workflow_task_templates.name, '') task_name
            FROM dri_workflow_task_templates
            WHERE dri_workflow_task_templates.dri_subworkflow_template_id = '".$rStage['primaryid']."' AND dri_workflow_task_templates.deleted = 0
            ORDER BY dri_workflow_task_templates.sort_order";
            $rTasks = $GLOBALS['db']->fetchArray($qTasks);
            foreach ($rTasks as $rTask) {
                $task = array();
                $task['name'] = $rTask['task_name'];
                $task['cnt_completed'] = $task['cnt_not_applicable'] = $task['progress_points'] = $task['momentum_points'] = 0;
                $html .= renderRow($template_id, $idx_activity++, $task);
            }
        } else {
            foreach ($stage['activities'] as $activity) {
                $html .= renderRow($template_id, $idx_activity++, $activity);
            }
        }
    }
    $html .= '</tbody></table>';

    return $html;
}

//-(*(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)