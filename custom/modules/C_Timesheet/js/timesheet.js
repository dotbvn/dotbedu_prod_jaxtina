var round = 0
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,'');
}
var month_names = new Array ( );
month_names[0] = 'January';
month_names[1] = 'February';
month_names[2] = 'March';
month_names[3] = 'April';
month_names[4] = 'May';
month_names[5] = 'June';
month_names[6] = 'July';
month_names[7] = 'August';
month_names[8] = 'September';
month_names[9] = 'October';
month_names[10] = 'November';
month_names[11] = 'December';

var day_names = new Array ( );
day_names[0] = 'Sunday';
day_names[1] = 'Monday';
day_names[2] = 'Tuesday';
day_names[3] = 'Wednesday';
day_names[4] = 'Thursday';
day_names[5] = 'Friday';
day_names[6] = 'Saturday';

//config time slot (timepicker)
var minTime = '6:00am';
var maxTime = '9:30pm';
var step             = 15;
var disableTextInput = true;
var showOn           = ["click","focus"];
var defaultTimeDelta = 90;
var TimeDelta = defaultTimeDelta * 60000;

$(document).ready(function(){
    var lang = window.top.App.lang.getLanguage().substr(0, 2) === 'vn' ? 'vi' : window.top.App.lang.getLanguage().substr(0, 2);
    $.tdtpicker.setLocale(lang);
    $('#datetimepicker2').tdtpicker({
        format: window.top.App.user.getPreference('datepref'),
        dayOfWeekStart: parseInt(window.top.App.user.getPreference('first_day_of_week'), 10),
        step: 15,
        inline: true,
        timepicker: false,
        onSelectDate:function(ct,$i){
            $('#dis_apply_date').html(
                day_names[ct.getDay()] + ', ' +
                ct.getDate() + ' ' +  month_names[ct.getMonth()] + ', ' + ct.getFullYear()
            );
            //DB format
            $('#date_add').val(DOTB.util.DateUtils.formatDate(ct,'H:i','Y-m-d'));
            ajaxLoadTimesheet();
        },
        onChangeMonth:function(current_time,$input){
            ajaxLoadTimesheet();
        },
        onChangeYear:function(current_time,$input){
            ajaxLoadTimesheet();
        }
    });

    //set time picker
    var timeOnly = $('#tmpTimeOnly')
    setTimePicker(timeOnly);

    //set current date for apply_date
    var date = new Date();
    $('#dis_apply_date').html(
        day_names[date.getDay()] + ', ' +
        date.getDate() + ' ' +  month_names[date.getMonth()] + ', ' + date.getFullYear()
    );

    $('.btnAddRow').live('click',function(){
        var teacher_ids = $("select[name=tmp_teacher_id]").val();
        var task_name = $('select[name=tmp_task_name]').val();
        var start_time = $('#tmp_start_time').val().trim();
        var end_time = $('#tmp_end_time').val().trim();
        var duration_cal = $('#tmp_duration_cal').val();
        var start_time_format = $('#tmp_start_time_format').val();
        var description = $('#tmp_description').val().trim();
        window.team_list = [];

        if(teacher_ids != null){
            $('.select2-container').removeClass('error');
            $('#validate_teacher_id').hide();

            $.ajax({
                url: "index.php?module=C_Timesheet&action=handleAjaxTimesheet&dotb_body_only=true",
                type: "POST",
                async: true,
                data:  {
                    type:'ajaxGetTeacherTeamSet',
                    teacher_id:teacher_ids,
                },
                dataType: "json",
                success: function(data){
                    if(data.success == "1"){
                        window.team_list = data.data;
                        $.each(teacher_ids, function(index ,teacher_id) {
                            if(validateAddRow(task_name,start_time,description)){
                                insertNewRow(teacher_id,task_name,start_time,end_time,start_time_format,duration_cal,description, window.team_list[teacher_id].teams);
                                $('.timeOnly').each(function (){
                                    setTimePicker($(this));
                                });
                                clear();
                            }
                        });
                        var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
                        bwcComponent.rewriteLinks();
                        calcuHour();
                    }else{
                        toastr.error('Some errors occurred! Load data center failed, please check again!');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    toastr.error('Some errors occurred! Load data center failed, please check again!');
                },
            });
        }else{
            $('.select2-container').addClass('error');
            $('#validate_teacher_id').show();
        }
    });

    $('.btnDelRow').live('click',function(){
        $(this).closest('tr').remove();
        calcuHour();
    });

    $('#btnSave').live('click',function(){
        ajaxSave();
    });

    $('#btnSyncPTDemo').live('click', function (){
        ajaxSyncPTDemo();
    });

    ajaxLoadTimesheet();
});

function setTimePicker(timeOnly) {
    timeOnly.unbind();
    timeOnly.find('.time').eq(0).timepicker({
        'minTime': minTime,
        'maxTime': maxTime,
        'showDuration': false,
        'timeFormat': DOTB.expressions.userPrefs.timef,
        'step': step,
//        'disableTextInput': disableTextInput,
//        'disableTouchKeyboard': disableTextInput,
    });
    timeOnly.find('.time').eq(1).timepicker({
        'showDuration': true,
        'timeFormat': DOTB.expressions.userPrefs.timef,
        'step': step,
//        'disableTextInput': disableTextInput,
//        'disableTouchKeyboard': disableTextInput,
        'showOn': showOn,
    });
    var settingDatePair = new Datepair(timeOnly[0], {
        'defaultTimeDelta': TimeDelta,
    });

    timeOnly.on('rangeSelected', function () {
        var js_start = DOTB.util.DateUtils.parse($(this).find(".start").val(), DOTB.expressions.userPrefs.timef);
        var js_end = DOTB.util.DateUtils.parse($(this).find(".end").val(), DOTB.expressions.userPrefs.timef);
        var duration = parseFloat((js_end - js_start) / 3600000);
        if(duration <= 0 || isNaN(duration)){
             toastr.error('Invalid range');
             $(this).find(".start_time_format").val('');
             $(this).find(".duration_cal").val('');
             return ;
        }
        $(this).find(".start_time_format").val($(this).find(".start").val());
        $(this).find(".duration_cal").val(Numeric.toFloat(duration, 4, 4));

    }).on('rangeError', function(){
        $(this).find(".start_time_format").val('');
        $(this).find(".duration_cal").val('');
        toastr.error('Invalid range');
        return ;
    });
}

function validateAddRow(task_name,start_time,description){
    var flag = true;
    if(task_name == ''){
        $('select[name=tmp_task_name]').addClass('error');
        $('#validate_task').show();
        flag = false;
    }else{
        $('select[name=tmp_task_name]').removeClass('error');
        $('#validate_task').hide();
    }

    if(start_time == ''){
        $('select[name=validate_time_slot]').addClass('error');
        $('#validate_time_slot').show();
        flag = false;
    }else{
        $('select[name=validate_time_slot]').removeClass('error');
        $('#validate_time_slot').hide();
    }

    if(description == ''){
        $('#tmp_description').addClass('error');
        $('#validate_description').show();
        flag = false;
    }else{
        $('#tmp_description').removeClass('error');
        $('#validate_description').hide();
    }
    return flag;

}

function validateSave(){
    var flag = true;
    $('input.start_time').each(function(){
        if($(this).val() == ''){
            $(this).addClass('error');
            flag = false;
        }else{
            $(this).removeClass('error');
        }
    });

    $('input#description').each(function(){
        var check_description = $(this).val().trim();
        if(check_description == ''){
            $(this).addClass('error');
            flag = false;
        }else{
            $(this).removeClass('error');
        }
    });

    var rowCount = $('#timesheets >tbody >tr').length;
    if(rowCount < 0){
        flag = false;
    }
    return flag;
}

function insertNewRow(teacher_id, task_name, start_time, end_time, start_time_format, duration_cal, description, team_list){
    var current_user_id     = $('#current_user_id').val();
    var current_user_name   = $('#current_user_name').val();
    var current_team_id     = $('#current_team_id').val();
    html_TR = '';
    html_TR += '<tr class="new_timesheet">';

    html_TR += '<td valign="bottom" align="left">';
    html_TR += '<input type="hidden" id="" name="c_timesheet_id[]" value=""/>';
    html_TR += '<input type="hidden" id="" name="meeting_id[]" value=""/>';
    html_TR += '<input type="hidden" id="" name="teacher_id[]" value="'+teacher_id+'"/>';
    html_TR += '<a  style="text-decoration: none; vertical-align: -webkit-baseline-middle; font-weight: bold;" href="#bwc/index.php?module=C_Teachers&action=DetailView&record='+teacher_id+'"> '+$("#tmp_teacher_id option[value='"+teacher_id+"']").text()+' </a>';
    html_TR += '</td>';

    html_TR += '<td>';
    html_TR += '<input type="text" id="" name="task_text[]" value="'+$("#tmp_task_name option[value='"+task_name+"']").text()+'" readonly="true" style="width:150px;" class="disable" />';
    html_TR += '<input type="hidden" id="" name="task_name[]" value="'+task_name+'"/>';
    html_TR += '</td>';

    html_TR += '<td align="center">';
    html_TR += '<p class="timeOnly">';
    html_TR += '<input type="text" class="start_time time start ui-timepicker-input" style="width: 65px; text-align: center;" autocomplete="off" size="2" maxlength="2" value="'+start_time +'"/> to ';
    html_TR += '<input type="text" class="end_time time end ui-timepicker-input" style="width: 65px; text-align: center;" autocomplete="off" size="2" maxlength="2" value="'+end_time +'" /> ';
    html_TR += '<input type="text" class="duration_cal" name="duration_cal[]" style="display: none" size="2" maxlength="2" value="' + duration_cal + '"/> ';
    html_TR += '<input type="text" class="start_time_format" name="start_time_format[]" style="display: none" size="2" maxlength="2" value="' + start_time_format + '"/> ';
    html_TR += '</p>';
    html_TR += '</td>';

    html_TR += '<td>';
    html_TR += '<input type="text" id="" name="description[]"  size="20" value="'+description+'"/>';
    html_TR += '</td>';

    html_TR += '<td align="center">';
    html_TR += '<a  style="text-decoration: none; vertical-align: -webkit-baseline-middle; font-weight: bold;" href="#bwc/index.php?module=Employees&action=DetailView&record='+current_user_id+'">'+current_user_name+'</a><input type="hidden" name="created_by[]" value="'+current_user_id+'"/>';
    html_TR += '</td>';

    html_TR += '<td><select id="" name="team_id[]" style="width:120px;">';
    $.each(team_list, function( index, team ) {
        var select = '';
        if(current_team_id == team.id) select = 'selected';
        html_TR += '<option value="' + team.id + '" ' + select + '>' + team.name + '</option>';
    });
    html_TR += '</select></td>';

    html_TR += '<td>';
    html_TR += '<input type="button" class="btnDelRow" value="Delete" id="btnDel"/>';
    html_TR += '</td>';

    html_TR += '</tr>';

    $('#timesheets').find('tbody').append(html_TR);
}

function calcuHour(){
    var hours = 0;
    var i = 1;
    $('input.duration_cal').each(function(){
        if(i > 1)
            hours += $(this).val() * 1.0;
        i++;
    });
    $('#total_day').text(hours.toFixed(2));
}

function ajaxSave(){
    if(validateSave()){
        //prepare
        calcuHour();
        var form_ = $('#Editview');
        //loading icon
        $("#result").html('<img src="themes/default/images/loading.gif" align="absmiddle" width="16">&nbsp;Waiting data...');
        $("#btnSave, #btnSyncPTDemo, .btnAddRow, .btnDelRow").prop('disabled',true);
        $.ajax({  //Make the Ajax Request
            url: "index.php?module=C_Timesheet&action=handleAjaxTimesheet&dotb_body_only=true",
            type: "POST",
            async: true,
            data: form_.serialize(),
            dataType: "json",
            error: function () {
                toastr.error('Some errors occurred! Data can not save, please check again!');
                $("#btnSave, #btnSyncPTDemo, .btnAddRow, .btnDelRow").prop('disabled',false);
            },
            success: function (data) {
                if (data.success) {
                    $("#result").html('Save successfully !');
                    ajaxLoadTimesheet();
                    drawColor();
                } else {
                    toastr.error("Some errors occurred! Data can not save, please check again!");
                    $("#result").html('Load data fail, Please reload page !!');
                }
                $("#btnSave, #btnSyncPTDemo, .btnAddRow, .btnDelRow").prop('disabled',false);
            }
        });
    }else
        toastr.error('Some errors occurred! Please, check again.');
}

function ajaxLoadTimesheet(html = ''){
    //loading icon
    if($('#date_add').val() != ''){
        $("#result").html('<img src="themes/default/images/loading.gif" align="absmiddle" width="16">&nbsp;Loading data...');
        $.ajax({
            url: "index.php?module=C_Timesheet&action=handleAjaxTimesheet&dotb_body_only=true",
            type: "POST",
            async: true,
            data:  {
                date_add: $('#date_add').val(),
                type    : 'load_timesheet'
            },
            dataType: "json",
            success: function(data){
                if(data.success == "1"){
                    data.count == 0 ? $("#result").html('No Data !') : $("#result").html('Load successfully !');
                    $('#list_meeting').val(data.list_meeting);
                    $('#list_date_add').val(data.list_date_add);
                    $('#timesheets').find('tbody').html(data.html);
                    $('#timesheets').find('tbody').append(html);
                    $('.timeOnly').each(function (){
                        setTimePicker($(this));
                    });
                    drawColor();
                    calcuHour();
                    $("#btnSave, #btnSyncPTDemo, .btnAddRow, .btnDelRow").prop('disabled',false);
                    var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
                    bwcComponent.rewriteLinks();
                }else{
                    $("#result").html('Load data fail, Please reload page !!');
                }
            },
        });
    }else{
        $('#date_add').val(DOTB.util.DateUtils.formatDate(new Date(),'H:i','Y-m-d'));
        ajaxLoadTimesheet();
    }
}

function ajaxSyncPTDemo(){
    //loading icon
    $("#result").html('<img src="themes/default/images/loading.gif" align="absmiddle" width="16">&nbsp;Waiting data...');
    $("#btnSave, #btnSyncPTDemo, .btnAddRow, .btnDelRow").prop('disabled',true);
    //prepare
    $.ajax({
        url: "index.php?module=C_Timesheet&action=handleAjaxTimesheet&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type        :'ajaxSyncPTDemo',
            date_sync   : $('#date_add').val(),
        },
        dataType: "json",
        success: function(data){
            if(data.success == "1"){
                var html = '';
                $('.new_timesheet').each(function (){
                    html += '<tr class="new_timesheet">' + $(this).html() + '</tr>';
                });
                ajaxLoadTimesheet(html);
                drawColor();
                $("#result_sync").html(data.total + ' record is synchronized!');
                $("#result_sync").show();
                setTimeout ('$("#result_sync").hide()', 5000);
            }else{
                toastr.error('Some errors occurred! Data can not save, please check again!');
            }
        },
    });
}

function drawColor() {
    var list_date = $('#list_date_add').val();
    var date_array = list_date.split(',');
    for (var i = 0; i < date_array.length; i++) {
        var this_day = date_array[i].substring(8, 10);
        this_day = Number(this_day).toString();
        var this_month = date_array[i].substring(5, 7)
        this_month = (Number(this_month) - 1).toString();
        var this_year = date_array[i].substring(0, 4);
        $('td [data-date="' + this_day + '"][data-month="' + this_month + '"][data-year="' + this_year + '"]').addClass('add_entried');
    }
}

function clear() {
    $("select[name=tmp_teacher_id]").select2('val','');
    $('#tmp_hours').val('0');
    $('#tmp_description').val('');
    $('#tmp_task_name').val('');
}
