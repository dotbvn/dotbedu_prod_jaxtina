<script type='text/javascript' src='include/javascript/datetimepicker/jquery.datetimepicker.full.min.js'></script>
<link rel='stylesheet' type='text/css' href='include/javascript/datetimepicker/jquery.datetimepicker.min.css'>
<script type='text/javascript' src='custom/include/javascript/Select2/select2.min.js'></script>
<link rel='stylesheet' type='text/css' href='custom/include/javascript/Select2/select2.css'>
<link rel='stylesheet' type='text/css' href='custom/modules/C_Timesheet/css/custom.css'>
<link rel='stylesheet' type='text/css' href='custom/include/javascript/Timepicker/css/jquery.timepicker.css'>
<script type='text/javascript' src='custom/include/javascript/Timepicker/js/jquery.timepicker.min.js'></script>
<script type='text/javascript' src='custom/include/javascript/Timepicker/js/datepair.min.js'></script>
<script type='text/javascript' src='custom/modules/C_Timesheet/js/timesheet.js'></script>

<table width="100%" border="0" cellspacing="1" cellpadding="0" id="Default_C_TimeSheets_Subpanel" class="yui3-skin-sam edit view panelContainer">
    <tbody>
        <tr>
            <td valign="top" width="100%" colspan="3">
                <table width="99%" border="1" class="main_timesheet">
                    <tbody>
                        <tr>
                            <td>
                                Legends: <br>
                                <label><span class="timesheet_day_legend">&nbsp;&nbsp;&nbsp;x&nbsp;&nbsp;&nbsp;</span> Added entries</label> <br>
                                <label><span class="today_c_legend">&nbsp;&nbsp;&nbsp;x&nbsp;&nbsp;&nbsp;</span> Today</label><br>
                                <h3>DatePicker</h3>
                                <input type="text" id="datetimepicker2"></input>
                            </td>
                            <td>
                                <h2 id="dis_apply_date" name="dis_apply_date" class="highlight1"></h2>
                                <br>
                                <table width="100%">
                                    <tbody><tr>
                                            <th width="30%">Teacher <font color="red">*</font></th>
                                            <th width="20%">Task <font color="red">*</font></th>
                                            <th width="28%">Time Slot <font color="red">*</font></th>
                                            <th width="22%">Description <font color="red">*</font></th>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td>
                                                {$TEACHER_OPTION}
                                                <br><label name="validate_teacher_id" id="validate_teacher_id" style="display:none; color:red;">Missing required field: Teacher</label>
                                            </td>
                                            <td>
                                                <select name="tmp_task_name" id="tmp_task_name" style="width: 200px;">
                                                    {$TASK_OPTION}
                                                </select>
                                                <br><label name="validate_task" id="validate_task" style="display:none; color:red;">Missing required field: Task</label>
                                            </td>
                                            <td align="center">
                                                <p id="tmpTimeOnly">
                                                    <input id="tmp_start_time" class="time start ui-timepicker-input" type="text" style="width: 70px; text-align: center;" autocomplete="off"> to
                                                    <input id="tmp_end_time" class="time end ui-timepicker-input" type="text" style="width: 70px; text-align: center;" autocomplete="off">
                                                    <input id="tmp_duration_cal" class="duration_cal" type="text" style="display: none">
                                                    <input id="tmp_start_time_format" class="start_time_format" type="text" style="display: none">
                                                </p>
                                                <label name="validate_time_slot" id="validate_time_slot" style="display:none; color:red;">Wrong input</label>
                                            </td>
                                            <td><textarea id="tmp_description" name="tmp_description" maxlength="150" rows="2" cols="30"></textarea>
                                                <br><label name="validate_description" id="validate_description" style="display:none; color:red;">Missing required field: Description</label>
                                            </td>
                                            <td>{if $addPermission}<input type="button" class="btnAddRow" name="btnAddRow" value="Add">{/if}</td>
                                        </tr>
                                    </tbody></table>
                                <br><br>
                                <h2 class="title">Added entries</h2>
                                <div id="added_entries" style="">
                                    <form action="index.php" method="POST" name="Editview" id="Editview">
                                        <input type="hidden" id="type" name="type" value="save_timesheet">
                                        <input type="hidden" id="list_meeting" name="list_meeting" value="">
                                        <input type="hidden" id="list_date_add" name="list_date_add" value="">

                                        <input type="hidden" id="date_add" name="date_add" value="{$DATE_ADD}">
                                        <input type="hidden" id="current_user_name" name="current_user_name" value="{$current_user_name}" >
                                        <input type="hidden" id="current_user_id" name="current_user_id" value="{$current_user_id}" >
                                        <input type="hidden" id="current_team_name" name="current_team_name" value="{$current_team_name}" >
                                        <input type="hidden" id="current_team_id" name="current_team_id" value="{$current_team_id}" >
                                        <table class="timesheets" id="timesheets" border="0" cellspacing="0" cellpadding="2" width="100%">
                                            <thead>
                                                <tr>
                                                    <th width='15%' style='text-align: center;'>Teacher</th>
                                                    <th width='15%' style='text-align: center;'>Task</th>
                                                    <th width='28%'>Time Slot</th>
                                                    <th width='20%'>Description</th>
                                                    <th width='10%'>Created by</th>
                                                    <th width='15%'>Center</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table></form><p align="right"><b>Total: <span id="total_day">0</span> hours</b></p>
                                </div>
                                {if $addPermission}
                                <input type="button" class="button primary" name="btnSave" id="btnSave" value="Save" style="float: right;margin: 5px 5px 0 0;padding: 3px 20px;">
                                <input type="button" class="button" name="btnSyncPTDemo" id="btnSyncPTDemo" value="Sync PT/Demo" style="float: right;margin: 5px 5px 0 0;padding: 3px 20px;">
                                {/if}
                                <div><div id="result">No data!</div>
                                <div style="padding: 10px;background-color: #F7E6E6;display: none" id="result_sync"></div></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<div id="runscript"></div>
