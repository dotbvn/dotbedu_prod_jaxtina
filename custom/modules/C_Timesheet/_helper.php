<?php
function getTeamByTeamSet($teamSetId){
    $team_list = array();
    // Get all team set
    $teamSetBean = new TeamSet();
    $teams = $teamSetBean->getTeams($teamSetId);
    // Add all team set to  $team_list
    foreach ($teams as $key => $value) {
        $team_list[$key]['id'] = $key;
        $team_list[$key]['name'] = $value->name;
    }
    // Add children of team set to $team_list
    foreach ($teams as $key => $value) {
        // Get children of team
        $qTeam1 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$key}'";
        $rs1 = $GLOBALS['db']->query($qTeam1);

        while($row1 = $GLOBALS['db']->fetchByAssoc($rs1)){
            if (!isset($teams[$row1['id']])) {
                $team_list[$row1['id']]['id'] = $row1['id'];
                $team_list[$row1['id']]['name'] = $row1['name'];
            }
            $qTeam2 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$row1['id']}'";
            $rs2 = $GLOBALS['db']->query($qTeam2);
            while($row2 = $GLOBALS['db']->fetchByAssoc($rs2))
                if (!isset($teams[$row1['id']])) {
                    $team_list[$row2['id']]['id'] = $row2['id'];
                    $team_list[$row2['id']]['name'] = $row2['name'];
                }
        }
    }
    return $team_list;
}
