<?php
// create by Lap Nguyen - checkRoom controller
function loadTimeSheet(){
    global $current_user;
    $ss = new Dotb_Smarty();
    $ss->assign("MOD", $GLOBALS['mod_strings']);
    $ext_team_c = "AND c_teachers.team_set_id IN
    (SELECT
    tst.team_set_id
    FROM
    team_sets_teams tst
    INNER JOIN
    team_memberships team_memberships ON tst.team_id = team_memberships.team_id
    AND team_memberships.user_id = '".$current_user->id."'
    AND team_memberships.deleted = 0)";
    if($current_user->isAdmin())
        $ext_team_c = '';

    //Get Teacher Option
    $sql = "SELECT teacher_id ,id, full_teacher_name, team_id, title, phone_mobile, team_set_id, type
    FROM c_teachers
    WHERE deleted = 0 $ext_team_c
    ORDER BY type ASC";
    $result = $GLOBALS['db']->query($sql);

    $c_teacher_type = '';
    $teacher_option ="<select multiple name='tmp_teacher_id' id='tmp_teacher_id' ><optgroup>";
    while($row = $GLOBALS['db']->fetchByAssoc($result)) {
        if ($row['type'] != $c_teacher_type) {
            $c_teacher_type = $row['type'];
            $teacher_option .= "</optgroup><optgroup label='" . $GLOBALS['app_list_strings']['teacher_type_list'][$c_teacher_type] . "'>";

        }
        $teacher_option .= "<option value='" . $row['id'] . "'>" . $row['full_teacher_name'] . "</option>";
    }
    $teacher_option .= "</optgroup>";

    $teacher_option .= "</select>";
    $js = <<<EOQ
    <script>
        $("select[name=tmp_teacher_id]").select2({ width: '100%' });
    </script>
EOQ;

    $ss->assign("TEACHER_OPTION", $teacher_option.$js);
    $ss->assign("DATE_ADD", date('Y-m-d'));
    $ss->assign("current_user_name", $current_user->last_name.' '.$current_user->first_name);
    $ss->assign("current_user_id", $current_user->id);
    $ss->assign("current_team_name", $current_user->team_name);
    $ss->assign("current_team_id", $current_user->team_id);
    $ss->assign("TASK_OPTION", get_select_options($GLOBALS['app_list_strings']['admin_task_list'],''));
    $ss->assign("MINUTES_OPTION", get_select_options($GLOBALS['app_list_strings']['timesheet_minutes_list'],'20'));
    if(ACLController::checkAccess('C_Timesheet', 'edit', true) || ($current_user->isAdmin()))
        $addPermission = true;
    else $addPermission = false;
    $ss->assign("addPermission", $addPermission);
    return $ss->fetch('custom/modules/C_Timesheet/tpls/timesheet.tpl');

}
echo  loadTimeSheet();
