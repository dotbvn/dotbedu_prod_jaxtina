<?php
switch ($_POST['type']) {
    case 'ajaxGetTeacherTeamSet':
        echo ajaxGetTeacherTeamSet($_POST['teacher_id']);
        break;
    case 'ajaxSyncPTDemo':
        echo ajaxSyncPTDemo($_POST['date_sync']);
        break;
    case 'load_timesheet':
        echo load_timesheet();
        break;
    case 'save_timesheet':
        echo save_timesheet();
        break;
    default:
        echo false;
        die;
}

function ajaxGetTeacherTeamSet($array_teacher_id = array()){
    require_once "custom/modules/C_Timesheet/_helper.php";
    if(empty($array_teacher_id)){
        return json_encode(array(
            'success' => false,
            'message' => 'Invalid param',
        ));
    }

    $teacher_id = implode("','", $array_teacher_id);
    $q1 = "SELECT IFNULL(id, '')primaryid
    , IFNULL(team_set_id, '') team_set_id
    FROM c_teachers
    WHERE id IN('{$teacher_id}')";
    $res1 = $GLOBALS['db']->query($q1);
    while ($row1 = $GLOBALS['db']->fetchByAssoc($res1)){
        $team_list = getTeamByTeamSet($row1['team_set_id']);
        $data[$row1['primaryid']]['teams'] = $team_list;
    }

    return json_encode(array(
        'success' => true,
        'data' => $data,
    ));
}

function ajaxSyncPTDemo($date_sync){
    if(empty($date_sync))
        return json_encode(array(
            'success'   => 0,
        ));

    global $timedate , $current_user;
    $date_syncDB = $timedate->convertToDBDate($date_sync);
    $start = date('Y-m-d H:i:s',strtotime("-7 hours ".$date_syncDB." 00:00:00"));
    $end = date('Y-m-d H:i:s',strtotime("-7 hours ".$date_syncDB." 23:59:59"));

    $ext_team = "AND meetings.team_set_id IN
    (SELECT
    tst.team_set_id
    FROM
    team_sets_teams tst
    INNER JOIN
    team_memberships team_memberships ON tst.team_id = team_memberships.team_id
    AND team_memberships.user_id = '{$current_user->id}'
    AND team_memberships.deleted = 0)";
    if($current_user->isAdmin())
        $ext_team = '';

    $q1 = "SELECT DISTINCT
    IFNULL(meetings.id, '') meeting_id,
    IFNULL(l1.id, '') teacher_id,
    meetings.date_start date_start,
    CONCAT(l1.full_teacher_name,' - ', meetings.meeting_type,': ', ' -Synchronized') name,
    IFNULL(meetings.meeting_type, '') meeting_type,
    IFNULL(ct.id, '') timesheet_id,
    IFNULL(meetings.duration_minutes, '') duration_minutes,
    meetings.duration_hours duration_hours,
    meetings.duration_cal duration_cal,
    meetings.duration_cal delivery_hour,
    meetings.duration_cal teaching_hour,
    CONCAT('Sync from: ', meetings.name) description,
    IFNULL(l3.id, '') assigned_user_id,
    IFNULL(l2.id, '') team_id
    FROM meetings
    INNER JOIN c_teachers l1 ON (l1.id=meetings.teacher_id OR l1.id=meetings.teacher_cover_id OR l1.id=meetings.sub_teacher_id) AND l1.deleted = 0
    LEFT JOIN teams l2 ON meetings.team_id = l2.id AND l2.deleted = 0
    LEFT JOIN users l3 ON meetings.assigned_user_id = l3.id AND l3.deleted = 0
    LEFT JOIN c_timesheet ct ON ct.meeting_id = meetings.id AND l1.id = ct.teacher_id AND ct.deleted = 0
    WHERE (meetings.meeting_type IN ('Placement Test' , 'Demo'))
    AND (meetings.date_start >= '$start' AND meetings.date_start <= '$end')
    $ext_team
    AND meetings.deleted = 0";
    $rows = $GLOBALS['db']->fetchArray($q1);
    $count = 0;
    foreach ($rows as $row){
        if (!empty($row['timesheet_id'])){
            $c_timesheet = BeanFactory::getBean('C_Timesheet', $row['timesheet_id']);
            if(is_null($c_timesheet)) $c_timesheet = BeanFactory::getBean('C_Timesheet');
        }else
            $c_timesheet = BeanFactory::getBean('C_Timesheet');

        if(empty($c_timesheet->id)) $count++;

        //Save
        foreach ($row as $key => $value)
            $c_timesheet->$key = $value;
        $c_timesheet->save();
    }
    return json_encode(array(
        'success'   => true,
        'total'     => $count,
    ));
}

function load_timesheet(){
    if(isset($_POST['date_add'])){
        require_once "custom/modules/C_Timesheet/_helper.php";
        global $current_user , $timedate;

        $date_add_start = $timedate->to_db($timedate->to_display_date($_REQUEST['date_add'], false));
        $date_add_end = date('Y-m-d H:i:s', strtotime("+1day $date_add_start"));
        $ext_team = "AND c_timesheet.team_set_id IN
        (SELECT
        tst.team_set_id
        FROM
        team_sets_teams tst
        INNER JOIN
        team_memberships team_memberships ON tst.team_id = team_memberships.team_id
        AND team_memberships.user_id = '{$current_user->id}'
        AND team_memberships.deleted = 0)";
        if($current_user->isAdmin())
            $ext_team = '';

        $q1 = "SELECT DISTINCT
        IFNULL(c_timesheet.id, '') primaryid,
        IFNULL(c_timesheet.name, '') name,
        IFNULL(c_timesheet.description, '') description,
        IFNULL(c_timesheet.date_start, '') date_start,
        IFNULL(c_timesheet.date_start + INTERVAL (c_timesheet.duration_minutes ) MINUTE + INTERVAL (c_timesheet.duration_hours) HOUR, '') date_end,
        (DATE_FORMAT(c_timesheet.date_start, '%Y-%m-%d')) date_add,
        IFNULL(l1.full_teacher_name, '') full_teacher_name,
        IFNULL(l1.id, '') teacher_id,
        IFNULL(c_timesheet.meeting_type, '') task_name,
        IFNULL(c_timesheet.meeting_id, '') meeting_id,
        c_timesheet.duration_cal duration_cal,
        c_timesheet.duration_hours duration_hours,
        IFNULL(c_timesheet.duration_minutes, '') duration_minutes,
        IFNULL(l2.full_user_name, '') assigned_to,
        IFNULL(l2.id, '') assigned_to_id,
        l2.date_entered date_entered,
        IFNULL(l3.id, '') created_by,
        IFNULL(l3.full_user_name, '') user_full_name,
        IFNULL(l1.team_set_id, '') teacher_team_set_id,
        IFNULL(l4.id, '') team_id,
        IFNULL(l4.name, '') team_name
        FROM c_timesheet
        INNER JOIN c_teachers l1 ON c_timesheet.teacher_id = l1.id AND l1.deleted = 0
        LEFT JOIN users l2 ON c_timesheet.assigned_user_id = l2.id AND l2.deleted = 0
        LEFT JOIN users l3 ON c_timesheet.created_by = l3.id AND l3.deleted = 0
        INNER JOIN teams l4 ON c_timesheet.team_id = l4.id AND l4.deleted = 0
        WHERE (DATE_FORMAT(c_timesheet.date_start, '%Y-%m-%d %H:%m:%i') >= '$date_add_start' AND
                DATE_FORMAT(c_timesheet.date_start, '%Y-%m-%d %H:%m:%i') <= '$date_add_end' )
        $ext_team
        AND c_timesheet.deleted = 0";
        $rs1 = $GLOBALS['db']->query($q1);
        $html = '';
        $count = 0;
        $list_meeting = array();
        $user_time_format = $current_user->getPreference('timef');

        //Select center
        $q2 = "SELECT DISTINCT
        IFNULL(teams.id, '') id,
        IFNULL(teams.name, '') name
        FROM teams
        WHERE (teams.private IS NULL OR teams.private = '0') AND teams.deleted = 0
        ORDER BY name";
        $rsT = $GLOBALS['db']->query($q2);
        $teams = array();
        while ($rowT = $GLOBALS['db']->fetchByAssoc($rsT))
            $teams[$rowT['id']] = $rowT['name'];


        while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
            if(!in_array($row['primaryid'], $list_meeting))
                $list_meeting[] = $row['primaryid'];

            $html .= '<tr>';
            $html .= '<td valign="bottom" align="left">';
            $html .= '<input type="hidden" id="" name="c_timesheet_id[]" value="'.$row['primaryid'].'"/>';
            $html .= '<input type="hidden" id="" name="meeting_id[]" value="'.$row['meeting_id'].'"/>';
            $html .= '<input type="hidden" id="" name="teacher_id[]" value="'.$row['teacher_id'].'"/>';
            $html .= '<a  style="text-decoration: none; vertical-align: -webkit-baseline-middle; font-weight: bold;" href="#bwc/index.php?module=C_Teachers&action=DetailView&record='.$row['teacher_id'].'"> '.$row['full_teacher_name'].' </a>';
            $html .= '</td>';

            $html .= '<td>';
            $html .= '<input type="text" id="" name="task_text[]" value="'.$GLOBALS['app_list_strings']['admin_task_list'][$row['task_name']].'" readonly="true" style="width:150px;" class="disable" />';
            $html .= '<input type="hidden" id="task_name" name="task_name[]" value="'.$row['task_name'].'"/>';
            $html .= '</td>';

            $end = $row['duration_hours'] + 7;
            $html .= '<td align="center">';
            $html .= '<p class="timeOnly">';
            $html .= '<input type="text" class="start_time time start ui-timepicker-input" style="width: 65px; text-align: center;" autocomplete="off" size="2" maxlength="2" value="' . $timedate->to_display_time($row['date_start']) . '"/> to ';
            $html .= '<input type="text" class="end_time time end ui-timepicker-input" style="width: 65px; text-align: center;" autocomplete="off" size="2" maxlength="2" value="' . $timedate->to_display_time($row['date_end']). '" /> ';
            $html .= '<input type="text" class="duration_cal" name="duration_cal[]" style="display: none" size="2" maxlength="2" value="' . format_number($row['duration_cal'],4,4). '"/> ';
            $html .= '<input type="text" class="start_time_format" name="start_time_format[]" style="display: none" size="2" maxlength="2" value="' . $timedate->to_display_time($row['date_start']) . '"/> ';
            $html .= '</p>';
            $html .= '</td>';

            $html .= '<td>';
            $html .= '<input type="text" id="" name="description[]"  size="20" value="'.$row['description'].'"/>';
            $html .= '</td>';

            $html .= '<td align="center"><a  style="text-decoration: none; vertical-align: -webkit-baseline-middle; font-weight: bold;" href="#bwc/index.php?module=Employees&action=DetailView&record='.$row['assigned_to_id'].'">'.$row['assigned_to'].'</a><input type="hidden" name="created_by[]" value="'.$row['assigned_to_id'].'"/></td>';
            $html .= '<td><select id="" name="team_id[]" style="width:120px;">';

            $html .= get_select_options_with_id($teams,$row['team_id']);
            $html .= '</select><td>';

            if(ACLController::checkAccess('C_Timesheet', 'delete', true) || ($current_user->isAdmin()))
                $html .= '<input type="button" class="btnDelRow" value="Delete" id="btnDel"/>';
            $html .= '</td>';

            $html .= '</tr>';
            $count++;
        }

        $start_last_month   =  date('Y-m-d', strtotime('first day of last month ' . $date_add_start));
        $end_next_month     =  date('Y-m-d', strtotime('last day of next month ' . $date_add_end));

        // getListDateAdd
        $getDate = "SELECT DISTINCT
        date_start date_add
        FROM c_timesheet
        WHERE deleted = 0 AND (date_start >= '$start_last_month' AND date_start <= '$end_next_month')
        $ext_team";
        $list_date_add = $GLOBALS['db']->fetchArray($getDate);
        foreach ($list_date_add as $key => $value){
            $list_date_add[$key]['date_add'] = $timedate->convertToDBDate($timedate->to_display_date($value['date_add']));
        }

        $list_date_add = implode(",",array_column($list_date_add,'date_add'));

        return json_encode(array(
            "success"       => "1",
            "count"         => $count,
            "html"          => $html,
            "list_meeting"  => json_encode($list_meeting),
            "list_date_add" => $list_date_add
        ));
    }else{
        return json_encode(array(
            "success" => "0",
        ));
    }
}

function save_timesheet(){
    if(isset($_REQUEST['date_add'])){
        global $timedate , $current_user;

        //
        $list_c_timesheet = json_decode(html_entity_decode($_REQUEST['list_meeting']));
        $sqlDelete = "UPDATE c_timesheet
        SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}'
        WHERE id IN ('".implode("','",$list_c_timesheet)."') AND deleted=0";

        $GLOBALS['db']->query($sqlDelete);
        //=====

        //Insert new Timesheet
        $count = 0;
        for($i = 0; $i < count($_REQUEST['teacher_id']) ; $i++){
            $count++;

            $c_timesheet = BeanFactory::getBean('C_Timesheet');

            $q2 = "SELECT id, full_teacher_name, team_id FROM c_teachers WHERE id = '{$_REQUEST['teacher_id'][$i]}'";
            $rs2 = $GLOBALS['db']->query($q2);
            $row = $GLOBALS['db']->fetchByAssoc($rs2);

            $name = $row['full_teacher_name'].' - '.$_REQUEST['task_name'][$i].': '.$_REQUEST['description'][$i];

            $date_start = $timedate->to_db($timedate->to_display_date($_REQUEST['date_add'], false) . ' ' . $_REQUEST['start_time_format'][$i]);

            $c_timesheet->name           = $name;
            $c_timesheet->date_start     = $date_start;

            $c_timesheet->duration_hours    = intval($_REQUEST['duration_cal'][$i]);
            $c_timesheet->duration_minutes  = ($_REQUEST['duration_cal'][$i] - $c_timesheet->duration_hours) * 60;

            $minutes = round($_REQUEST['duration_cal'][$i] * 60,2);
            $end_time = date('Y-m-d H:i:s', strtotime("+$minutes minutes $date_start"));
            $c_timesheet->date_end          = $end_time;

            $c_timesheet->duration_cal   = $_REQUEST['duration_cal'][$i];
            $c_timesheet->meeting_type   = $_REQUEST['task_name'][$i];
            $c_timesheet->meeting_id     = $_REQUEST['meeting_id'][$i];
            $c_timesheet->teacher_id     = $_REQUEST['teacher_id'][$i];
            $c_timesheet->teaching_hour  = $_REQUEST['duration_cal'][$i];
            $c_timesheet->delivery_hour  = $c_timesheet->teaching_hour;
            $c_timesheet->description    = $_REQUEST['description'][$i];
            $c_timesheet->team_id        = $_REQUEST['team_id'][$i];
            $c_timesheet->team_set_id    = $_REQUEST['team_id'][$i];
            $c_timesheet->assigned_user_id = $_REQUEST['created_by'][$i];
            $c_timesheet->save();
        }
        return json_encode(array(
            "success" => "1",
            "count" => $count,
        ));
    }else{
        return json_encode(array(
            "success" => "0",
        ));
    }

}
