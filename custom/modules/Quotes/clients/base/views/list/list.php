<?php
// created: 2018-10-15 09:49:10
$viewdefs['Quotes']['base']['view']['list'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_LIST_QUOTE_NUM',
          'enabled' => true,
          'default' => true,
          'name' => 'quote_num',
        ),
        1 => 
        array (
          'label' => 'LBL_LIST_QUOTE_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        2 => 
        array (
          'target_record_key' => 'billing_account_id',
          'target_module' => 'Accounts',
          'label' => 'LBL_LIST_ACCOUNT_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'billing_account_name',
          'sortable' => false,
        ),
        3 => 
        array (
          'label' => 'LBL_QUOTE_STAGE',
          'enabled' => true,
          'default' => true,
          'name' => 'quote_stage',
        ),
        4 => 
        array (
          'label' => 'LBL_TOTAL',
          'enabled' => true,
          'default' => true,
          'name' => 'total',
          'related_fields' => 
          array (
            0 => 'currency_id',
          ),
        ),
        5 => 
        array (
          'label' => 'LBL_LIST_AMOUNT_USDOLLAR',
          'enabled' => true,
          'default' => true,
          'name' => 'total_usdollar',
        ),
        6 => 
        array (
          'name' => 'date_quote_expected_closed',
          'label' => 'LBL_LIST_DATE_QUOTE_EXPECTED_CLOSED',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'assigned_user_name',
          'target_record_key' => 'assigned_user_id',
          'target_module' => 'Employees',
          'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
          'enabled' => true,
          'default' => true,
          'sortable' => false,
        ),
        8 => 
        array (
          'name' => 'date_modified',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'modified_by_name',
          'label' => 'LBL_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'MODIFIED_USER_ID',
          'link' => true,
          'default' => false,
        ),
        10 => 
        array (
          'name' => 'discount',
          'label' => 'LBL_DISCOUNT_TOTAL',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        11 => 
        array (
          'name' => 'tax',
          'label' => 'LBL_TAX',
          'enabled' => true,
          'related_fields' => 
          array (
            0 => 'currency_id',
            1 => 'base_rate',
            2 => 'taxrate_value',
            3 => 'taxable_subtotal',
          ),
          'currency_format' => true,
          'default' => false,
        ),
        12 => 
        array (
          'name' => 'shipping_address_country',
          'label' => 'LBL_SHIPPING_ADDRESS_COUNTRY',
          'enabled' => true,
          'default' => false,
        ),
        13 => 
        array (
          'name' => 'base_rate',
          'label' => 'LBL_CURRENCY_RATE',
          'enabled' => true,
          'sortable' => false,
          'default' => false,
        ),
        14 => 
        array (
          'name' => 'created_by_name',
          'label' => 'LBL_CREATED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'CREATED_BY',
          'link' => true,
          'default' => false,
        ),
        15 => 
        array (
          'name' => 'date_entered',
          'enabled' => true,
          'default' => false,
        ),
      ),
    ),
  ),
);