<?php

class logicDashboard{
    function handleBeforeSave(&$bean, $event, $arguments){
        //Set lại default = 0 cho tất cả các dashboard của user, chỉ set default = 1 cho dashboard vừa edit
        if ($bean->default_dashboard == 1){
            global $db;
            $db->query("UPDATE dashboards SET default_dashboard = 0 WHERE assigned_user_id = '{$bean->assigned_user_id}' AND id <> '{$bean->id}'  ");
            $db->query("UPDATE dashboards SET default_dashboard = 1 WHERE  id = '{$bean->id}'  ");
        }
    }
    function addFilter(&$bean, $event, $args){
        $args[0]->where()->notEquals('is_mobile_dashboard', 1);
        if(method_exists($args[1]['id_query'],'where'))
            $args[1]['id_query']->where()->notEquals('is_mobile_dashboard', 1);
    }
}
