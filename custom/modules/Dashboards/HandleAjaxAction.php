<?php
switch ($_POST['type']) {
    case 'reArrangeItem':
        $result = ajaxReArrangeItem($_POST['indexData']);
        echo $result;
        break;
}
function ajaxReArrangeItem($args){
    if(isset($args) && !empty($args)){
        $newIndex = $args['newIndex'];
        $oldIndex = $args['oldIndex'];
        $q = "SELECT metadata FROM dashboards WHERE id = '{$args['idDashboard']}'";
        $result = json_decode(str_replace('&quot;', '"', $GLOBALS['db']->fetchOne($q))['metadata'], true);
        // Store the element at oldIndex in a temporary variable
        foreach($result['components'][0]['rows'] as $key1 => $row){
            foreach($row as $key2 => $item){
                if($item["view"]["type"] == 'stats-report'){
                    if($item["view"]["id_dashlet"] == $args["idDashlet"]) {
                        $stats = $item["view"]["display_stats"];
                        $temp = $stats[$args['oldIndex']];

                        // Shift elements to make space for the new position
                        if ($oldIndex < $newIndex) {
                            for ($i = $oldIndex; $i < $newIndex; $i++) {
                                $stats[$i] = $stats[$i + 1];
                            }
                        } else {
                            for ($i = $oldIndex; $i > $newIndex; $i--) {
                                $stats[$i] = $stats[$i - 1];
                            }
                        }

                        $stats[$newIndex] = $temp;
                        $json_stats = json_encode($stats);
                        $idDashboard = $args['idDashboard'];
                        $query = "UPDATE dashboards
                                  SET metadata = JSON_SET(metadata, '$.components[0].rows[$key1][$key2].view.display_stats', CAST('$json_stats' AS JSON))
                                  WHERE id = '$idDashboard'";
                        $GLOBALS['db']->query($query);
                        return json_encode(['success' => 1]);
                    }
                }
            }
        }
        return json_encode(['success' => 0]);
    }
}

