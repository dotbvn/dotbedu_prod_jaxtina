<?php

if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicUser
{
    public function users_update(&$bean, $event, $arguments){
        $bean->full_user_name = $bean->last_name . ' ' . $bean->first_name;

        //Update Commission team_id
        if(!empty($bean->fetched_row) && ($bean->fetched_row['default_team'] != $bean->default_team)){
            $GLOBALS['db']->query("UPDATE c_commission SET team_id = '{$bean->default_team}', team_set_id = '{$bean->default_team}' WHERE assigned_user_id = '{$bean->id}' AND deleted=0");
        }
    }
}
