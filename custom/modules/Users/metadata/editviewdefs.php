<?php
$viewdefs['Users'] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'form' =>
            array (
                'headerTpl' => 'modules/Users/tpls/EditViewHeader.tpl',
                'footerTpl' => 'modules/Users/tpls/EditViewFooter.tpl',
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_USER_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EMPLOYEE_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array (
            'LBL_USER_INFORMATION' =>
            array(
                array (
                    array (
                        'name' => 'user_name',
                        'displayParams' => array ('required' => true)
                    ),
                    'first_name'
                ),
                array (
                    array (
                        'customLabel' => '{$MOD.LBL_STATUS}',
                        'displayParams' => array ('required' => true),
                        'customCode' => '{if $IS_TEACHER}{$USER_STATUS}{else}{html_options name="status" id="status" options=$fields.status.options selected=$fields.status.value}{/if}',
                    ),
                    'last_name'
                ),
                array (
                    array (
                        'name' => 'UserType',
                        'customCode' => '{if $IS_ADMIN}{$USER_TYPE_DROPDOWN}{else}{$USER_TYPE_READONLY}{/if}',
                    ),
                ),
                array ('picture'),
            ),
            'LBL_EMPLOYEE_INFORMATION' =>
            array (
                array ('employee_status','show_on_employees'),
                array ('title','phone_work'),
                array ('department','phone_mobile'),
                array ('reports_to_name','phone_other'),
                array ('phone_home','phone_fax'),
                array ('address_street','address_city'),
                array ('description'),
            ),
        ),
    ),
);
