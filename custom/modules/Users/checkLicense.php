<?php

if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicCheckLicense
{
    /// for before_save
    /**
     * @throws DotbApiExceptionMaintenance
     */
    public function updateTotalUser(&$bean, $event, $arguments)
    {
        /// create new record
        if ($GLOBALS['service']->action === 'save' && $arguments['isUpdate'] !== true) { /// add record
            $isTargetedUser = $this->isTargetedUser($bean);
            if ($isTargetedUser) {
                $licenseBean = new LicenseBean();
                $licenseBean->fromClient();
                $totalUsers = (int)$licenseBean->lic_activated_users + 1;
                if ($totalUsers > (int)$licenseBean->lic_users) {
                    /// over subscription
                    $this->redirectExceedinglimitation();
                } else {
                    $licenseBean->lic_activated_users = $totalUsers;
                    $licenseBean->save();
                }
                unset($licenseBean);
            }

            return;
        }

        /// edit record
        if ($GLOBALS['service']->action === 'save' && $arguments['isUpdate'] === true) { /// edit record
            $currentDbRecord = BeanFactory::getBean('Users', $bean->id);

            /// ignore case: change user_name (admin, app_admin) <-> regular
            /// so that we only check case status change
            if ($currentDbRecord->status !== $bean->status) {
                $licenseBean = new LicenseBean();
                $licenseBean->fromClient();

                /// Active
                if ($bean->status === 'Active') {
                    $totalUsers = (int)$licenseBean->lic_activated_users + 1;
                    if ($totalUsers > (int)$licenseBean->lic_users) {
                        /// over limitation
                        $this->redirectExceedingLimitation();
                    } else {
                        $licenseBean->lic_activated_users = $totalUsers;
                        $licenseBean->save();
                        unset($licenseBean);
                    }

                    /// break
                    return;
                }

                /// Inactive
                if ($currentDbRecord->status === 'Active') {
                    $licenseBean->lic_activated_users -= 1;
                    $licenseBean->save();
                    unset($licenseBean);

                    /// break
                    return;
                }

                unset($licenseBean);
            }
        }
    }

    /// before_delete

    /**
     */
    public function decreaseCurrentTotalUser(&$bean, $event, $arguments)
    {
        if ($event === 'before_delete') {

            $oldRecord = BeanFactory::getBean('Users', $bean->id);
            $isTargetedUser = $this->isTargetedUser($oldRecord);
            $hadActivated = $oldRecord->status === 'Active';

            if ($isTargetedUser && $hadActivated) {
                $licenseBean = new LicenseBean();
                $licenseBean->fromClient();
                $licenseBean->lic_activated_users -= 1;
                $licenseBean->save();
                unset($licenseBean);
            }
        }
    }
    /**
     * vu
     * @throws DotbApiExceptionMaintenance
     */
    function redirectExceedingLimitation() {
        $e = new DotbApiExceptionMaintenance(
            'LLI_LICENSE_EXPIRED',
            null,
            null,
            0,
            $_SESSION['license']['lic_status'] ?? 'NO-LICENSE'
        );
        $e->setExtraData("url", '#bwc/index.php?module=EMS_Settings&action=subscription&bwcRedirect=1');

        throw $e;
    }

    /**
     * check admin
     */
    function isTargetedUser($bean) :bool {
        return !empty($bean->user_name) /// user_name not empty
            && empty($bean->teacher_id) /// not a teacher
            && !in_array($bean->user_name, array('admin', 'app_admin')); /// not admin user
    }
}
