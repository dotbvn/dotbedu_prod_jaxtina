<?php
$hook_version = 1;
$hook_array['before_save'][] = array(1, 'Update Total User For License', 'custom/modules/Users/checkLicense.php', 'logicCheckLicense', 'updateTotalUser');
$hook_array['before_save'][] = array(2, 'update profile', 'custom/modules/Users/logicUser.php', 'logicUser', 'users_update');

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(1, 'Decrease current total users', 'custom/modules/Users/checkLicense.php', 'logicCheckLicense', 'decreaseCurrentTotalUser');