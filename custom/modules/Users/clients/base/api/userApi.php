<?php


class userApi extends DotbApi
{
        function registerApiRest()
        {
                return array(
                        'student-get-info' => array(
                                'reqType' => 'GET',
                                'path' => array('v1', 'user', 'list'),
                                'pathVars' => array(''),
                                'method' => 'getUserList',
                                'shortHelp' => '',
                                'longHelp' => ''
                        ),
                );
        }

        function getUserList(ServiceBase $api, $args)
        {
                global $db;

                $sqlSelect = "SELECT phone_mobile FROM users";

                $row = $db->fetchArray($sqlSelect);


                return array(
                        'success' => true,
                        'data'   => $row
                );   // Logic xử lý
        }
}
