<?php
///////////////////////--------------------------------------------/////////////////
function getSubSituation(){
    $args = func_get_args();
    $class_id = $args[0]['class_id'];
    global $timedate;

    //Set entries page subpanel
    $sql = "SELECT DISTINCT (CASE WHEN jst.student_type = 'Leads' THEN l3.id ELSE l2.id END) student_id,
    IFNULL((CASE WHEN jst.student_type = 'Leads' THEN l3.first_name ELSE l2.first_name END),'') first_name
    FROM j_classstudents jst LEFT JOIN contacts l2 ON jst.student_id = l2.id AND l2.deleted = 0 AND jst.student_type = 'Contacts'
    LEFT JOIN leads l3 ON jst.student_id = l3.id AND l3.deleted = 0 AND jst.student_type = 'Leads'
    WHERE (jst.class_id = '$class_id') AND jst.deleted = 0
    AND (IFNULL((CASE WHEN jst.student_type = 'Leads' THEN l3.id ELSE l2.id END),'') <> '')
    GROUP BY student_id ORDER BY first_name ASC , student_id ASC";
    $limit = $GLOBALS['dotb_config']['list_max_entries_per_subpanel'];

    if(empty($limit)) $limit = 20;
    //get Offset - Kinh nghiệm xương máu
    $varOffset = 'J_Class_j_class_studentsituations_CELL_offset';
    if(strval($_SESSION[$varOffset]) == 'end'){
        $rows_found = $GLOBALS['db']->getOne("SELECT COUNT(*) FROM ($sql) u1");
        $subOffset = (floor(($rows_found - 1) / $limit)) * $limit;
    }
    else $subOffset = (isset($_SESSION[$varOffset]) && is_numeric($_SESSION[$varOffset])) ? ($_SESSION[$varOffset]) : 0;
    $sql .= " LIMIT $subOffset, $limit";
    $studentIDS = array_column($GLOBALS['db']->fetchArray($sql),'student_id');


    $date = $timedate->nowDbDate();
    $q0 = "SELECT DISTINCT (CASE WHEN jst.student_type = 'Leads' THEN l3.id ELSE l2.id END) student_id,
    IFNULL(jst.id, '') situation_id, IFNULL(jst.type, '') type, IFNULL(jst.total_amount,0) total_amount,
    (jst.total_minute/60) total_hour FROM j_studentsituations jst
    INNER JOIN j_class l1 ON jst.ju_class_id = l1.id AND l1.deleted = 0
    LEFT JOIN contacts l2 ON jst.student_id = l2.id AND l2.deleted = 0 AND jst.student_type = 'Contacts'
    LEFT JOIN leads l3 ON jst.student_id = l3.id AND l3.deleted = 0 AND jst.student_type = 'Leads'
    WHERE (l1.id = '$class_id') AND (jst.type IN ('Enrolled','OutStanding','Demo','Delayed'))
    AND (IFNULL((CASE WHEN jst.student_type = 'Leads' THEN l3.id ELSE l2.id END),'') IN ('".implode("','", $studentIDS)."'))
    AND jst.deleted = 0";
    $rows = $GLOBALS['db']->fetchArray($q0);
    $sts  = array();
    foreach($rows as $key => $r){
        $sts[$r['student_id']]['situations'][$r['situation_id']]['type']         = $r['type'];
        $sts[$r['student_id']]['situations'][$r['situation_id']]['total_amount'] = $r['total_amount'];
        $sts[$r['student_id']]['situations'][$r['situation_id']]['total_hour']   = $r['total_hour'];
        if($r['type'] == 'Enrolled') $sts[$r['student_id']]['total_amount']     += $r['total_amount'];
        if($r['type'] != 'Delayed') $sts[$r['student_id']]['total_hour']        += $r['total_hour'];
    }

    //biuld student progress
    $q1 = "(SELECT DISTINCT IFNULL(l2.id, '') student_id,
    IFNULL(jst.id, '') situation_id, IFNULL(mt.id, '') meeting_id FROM j_studentsituations jst
    INNER JOIN j_class l1 ON jst.ju_class_id = l1.id AND l1.deleted = 0
    INNER JOIN contacts l2 ON jst.student_id = l2.id AND l2.deleted = 0 AND jst.student_type = 'Contacts'
    INNER JOIN meetings_contacts mtc ON jst.id = mtc.situation_id AND l2.id = mtc.contact_id AND mtc.deleted=0
    INNER JOIN meetings mt ON mt.id = mtc.meeting_id AND mt.ju_class_id = l1.id
    AND mt.deleted=0 AND IFNULL(mt.session_status,'') <> 'Cancelled'
    WHERE (l1.id = '$class_id') AND (jst.type IN ('Enrolled','OutStanding','Demo'))
    AND (l2.id IN ('".implode("','", $studentIDS)."')) AND jst.deleted = 0) UNION ALL (SELECT DISTINCT IFNULL(l2.id, '') student_id,
    IFNULL(jst.id, '') situation_id, IFNULL(mt.id, '') meeting_id FROM j_studentsituations jst
    INNER JOIN j_class l1 ON jst.ju_class_id = l1.id AND l1.deleted = 0
    INNER JOIN leads l2 ON jst.student_id = l2.id AND l2.deleted = 0 AND jst.student_type = 'Leads'
    INNER JOIN meetings_leads mtc ON jst.id = mtc.situation_id AND l2.id = mtc.lead_id AND mtc.deleted=0
    INNER JOIN meetings mt ON mt.id = mtc.meeting_id AND mt.ju_class_id = l1.id
    AND mt.deleted=0 AND IFNULL(mt.session_status,'') <> 'Cancelled'
    WHERE (l1.id = '$class_id') AND (jst.type IN ('Enrolled','OutStanding','Demo'))
    AND (l2.id IN ('".implode("','", $studentIDS)."')) AND jst.deleted = 0)";
    $rows = $GLOBALS['db']->fetchArray($q1);
    foreach($rows as $key => $r) $sts[$r['student_id']]['situations'][$r['situation_id']]['ssIDs'][] = $r['meeting_id'];
    $sss = get_list_lesson_by_class($class_id);
    foreach($sts as $student_id => $st){
        $last_key    = 0;
        $lastSitType = 'Undefined';
        //Quét tất cả các buổi để vẽ timeline
        foreach($sss as $ss){
            $found = 0;
            foreach($st['situations'] as $situation_id => $sit){
                //Tìm buổi trong trong situation
                if(in_array($ss['primaryid'], $sit['ssIDs'])){
                    if($sit['type'] != $lastSitType){
                        $last_key++;
                        $lastSitType = $sit['type'];
                    }
                    $found = 1;
                    break;
                }
            }
            //Không tìm thấy buổi trong trong situation => Undefined
            if(!$found){
                if($lastSitType != 'Undefined') {
                    $last_key++;
                    $lastSitType  = 'Undefined';
                }
            }
            if($lastSitType == 'Undefined'){
                $situation_id = '';
                $sts[$student_id]['process'][$last_key]['total_hour'] += $ss['delivery_hour'];
            }
            $StID = $sts[$student_id]['process'][$last_key]['situation_ids'];
            $sts[$student_id]['process'][$last_key]['type'] = $lastSitType;
            if(!empty($situation_id) && !in_array($situation_id, $StID)){
                $sts[$student_id]['process'][$last_key]['situation_ids'][] = $situation_id;
                $sts[$student_id]['process'][$last_key]['total_amount']   += $sit['total_amount'];
                $sts[$student_id]['process'][$last_key]['total_hour']     += $sit['total_hour'];
            }
            if(empty($sts[$student_id]['process'][$last_key]['total_ss']))
                $sts[$student_id]['process'][$last_key]['start']      = $ss['date_start_tz'];

            $sts[$student_id]['process'][$last_key]['end']        = $ss['date_end_tz'];
            $sts[$student_id]['process'][$last_key]['duration']  += $ss['delivery_hour'];
            $sts[$student_id]['process'][$last_key]['sessions'][] = $ss['primaryid'];
            $sts[$student_id]['process'][$last_key]['total_ss']  += 1;
            if($date >= $ss['date'] && $lastSitType != 'Undefined')
                $sts[$student_id]['hours_now'] += $ss['delivery_hour'];
        }
        $sts[$student_id]['remain_hour'] = $sts[$student_id]['total_hour'] - $sts[$student_id]['hours_now'];
        unset($sts[$student_id]['situations']);
    }
    //BIULD SQL QUERY
    //case CASE HACK :) - progress
    if(!empty($sts)){
        $case_hack_ext1 = '(CASE';
        foreach($sts as $st_id => $st_s) $case_hack_ext1 .= " WHEN jst.student_id = '$st_id' THEN '".json_encode($st_s)."'";
        $case_hack_ext1 .= " ELSE '' END) progress,";
    }

    //bổ sung phân quyền field
    $fsList = ['nick_name', 'phone_mobile', 'birthdate'];
    foreach ($fsList as $field){
        $fsList[$field] = "'".translate('LBL_NO_DATA')."'";
        if(DotbACL::checkField('Contacts', $field, 'edit', array("owner_override" => true)))
            $fsList[$field] = "(CASE WHEN jst.student_type = 'Leads' THEN l3.$field ELSE l2.$field END)";
    }

    $sql = "SELECT DISTINCT IFNULL(jst.id, '') id, IFNULL(jst.id, '') class_student_id, '#' num,
    IFNULL(jst.student_type, '') student_type,
    (CASE WHEN jst.student_type = 'Leads' THEN l3.id ELSE l2.id END) student_id,
    (CASE WHEN jst.student_type = 'Leads' THEN l3.full_lead_name ELSE l2.full_student_name END) student_name,
    (CASE WHEN jst.student_type = 'Leads' THEN l3.first_name ELSE l2.first_name END) first_name,
    jst.avg_attendance avg_attendance,
    jst.total_attended total_attended,
    jst.total_absent total_absent,
    {$fsList['nick_name']} nick_name, {$fsList['phone_mobile']} phone_mobile, {$fsList['birthdate']} birthdate,
    jst.start_study start_study, jst.end_study end_study, jst.total_hrs total_hour,
    IFNULL(jst.description,'') description,
    (CASE WHEN (jst.status = 'Delayed') THEN 'Delayed'
    WHEN ('$date' < jst.start_study) THEN 'Not Started'
    WHEN ('$date' >= jst.start_study AND '$date' <= jst.end_study) THEN 'In Progress'
    WHEN ('$date' > jst.end_study) THEN 'Finished' ELSE '' END) status,
    $case_hack_ext1
    IFNULL(l1.team_id, '') team_id, jst.date_entered date_entered,
    IFNULL(l1.id, '') ju_class_id, 'j_class_studentsituations' panel_name
    FROM j_classstudents jst
    INNER JOIN j_class l1 ON jst.class_id = l1.id AND l1.deleted = 0
    LEFT JOIN contacts l2 ON jst.student_id = l2.id AND l2.deleted = 0 AND jst.student_type = 'Contacts'
    LEFT JOIN leads l3 ON jst.student_id = l3.id AND l3.deleted = 0 AND jst.student_type = 'Leads'
    WHERE (l1.id = '$class_id') AND jst.deleted = 0
    AND (IFNULL((CASE WHEN jst.student_type = 'Leads' THEN l3.id ELSE l2.id END),'') <> '')
    GROUP BY student_id ORDER BY first_name ASC, student_id ASC";
    return $sql;
}

function getSubGradebook(){
    $args = func_get_args();
    $class_id = $args[0]['class_id'];
    $sql = "SELECT
    j_gradebook.id, j_gradebook.name,
    j_gradebook.type, j_gradebook.minitest,
    j_gradebook.status, j_gradebook.date_input,
    j_gradebook.date_confirm, l2.full_teacher_name c_teachers_j_gradebook_1_name,
    IFNULL(l2.id, '') c_teachers_j_gradebook_1c_teachers_ida, j_gradebook.date_modified,
    j_gradebook.assigned_user_id, 'j_class_j_gradebook_1' panel_name
    FROM j_gradebook
    INNER JOIN j_class_j_gradebook_1_c ON j_gradebook.id = j_class_j_gradebook_1_c.j_class_j_gradebook_1j_gradebook_idb AND j_class_j_gradebook_1_c.j_class_j_gradebook_1j_class_ida = '$class_id' AND j_class_j_gradebook_1_c.deleted = 0
    LEFT JOIN c_teachers_j_gradebook_1_c l2_1 ON j_gradebook.id = l2_1.c_teachers_j_gradebook_1j_gradebook_idb AND l2_1.deleted = 0
    LEFT JOIN c_teachers l2 ON l2.id = l2_1.c_teachers_j_gradebook_1c_teachers_ida AND l2.deleted = 0
    WHERE j_gradebook.deleted = 0
    ORDER BY CASE WHEN (j_gradebook.type = '' OR j_gradebook.type IS NULL) THEN 0";

    $ccd = $GLOBALS['app_list_strings']['gradebook_minitest_options'];
    $types = $GLOBALS['app_list_strings']['gradebook_type_options'];
    $count_k = 1;
    foreach($types as $_type => $val_){
        $sql .= " WHEN j_gradebook.type = '$_type' AND j_gradebook.minitest = '' THEN ".$count_k++;
        if($_type == 'Progress')
            foreach($ccd as $cc_ => $value) $sql .= " WHEN j_gradebook.type = '$_type' AND j_gradebook.minitest = '$cc_' THEN ".$count_k++;
    }
    $sql .= " ELSE $count_k END ASC";

    return $sql;
}

function sortStartDate( $a, $b ) {
    return strtotime($a["start"]) - strtotime($b["start"]);
}

?>
