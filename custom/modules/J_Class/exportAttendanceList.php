<?php

require_once("custom/include/PHPExcel/Classes/PHPExcel.php");

global $timedate, $current_user;

$fdir = 'ReportAttendance';
if (!file_exists("upload/$fdir"))
    mkdir("upload/$fdir", 0777, true);


$fi = new FilesystemIterator("upload/$fdir", FilesystemIterator::SKIP_DOTS);
if(iterator_count($fi) > 5)
    array_map('unlink', glob("upload/$fdir/*"));


$objPHPExcel = new PHPExcel();

//get Template
$templateUrl = "custom/include/TemplateExcel/ReportAttendance2.xlsx";

//Import Template
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($templateUrl);

// Set properties
$objPHPExcel->getProperties()->setCreator("DotB Center");
$objPHPExcel->getProperties()->setLastModifiedBy("DotB Center");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX");


//get Class data
$class  = BeanFactory::getBean('J_Class', $_REQUEST['record']);
$from   = $_REQUEST['from'];
$to     = $_REQUEST['to'];
$arr_week = array();
for($i = $from; $i <= $to; $i++)
    $arr_week[] = 'W'.$i;


//get teacher data
$sqlTeacher = "SELECT teacher_id
FROM meetings WHERE ju_class_id = '{$class->id}'
AND deleted = 0 AND session_status <> 'Cancelled'
AND teacher_id <> '' AND teacher_id IS NOT NULL
AND week_no IN ('".implode("','",$arr_week)."')";
$teacherId  = $GLOBALS['db']->getOne($sqlTeacher);
if(!empty($teacherId)){
    $teacher    = BeanFactory::getBean("C_Teachers", $teacherId);
    $teacherName= $teacher->name;
}

$uTz = $timedate->getUserTimeZone()['gmt'];
//get table data
$data = $lessonArr = $scheduleArr = array();
$sqlLesson = "SELECT lesson_number, IFNULL(id,'') id,
CONVERT_TZ(date_start,'+0:00','$uTz') day_schedule_time,
LCASE(REPLACE(DATE_FORMAT(CONVERT_TZ(date_start,'+0:00','$uTz'),'%d/%m %l:%i%p'),':00','')) start_time_format,
week_no
FROM meetings WHERE deleted = 0 AND session_status <> 'Cancelled' AND ju_class_id = '{$class->id}' AND week_no IN ('".implode("','",$arr_week)."')
ORDER BY CAST(lesson_number AS UNSIGNED)";
$resLesson = $GLOBALS['db']->query($sqlLesson);
$ss_arr = array();
while($lesson = $GLOBALS['db']->fetchByAssoc($resLesson)){
    $lessonArr[$lesson['week_no']][] = array(
        'id'                => $lesson['id'],
        'week_no'           => $lesson['week_no'],
        'lesson'            => $lesson['lesson_number'],
        'day_schedule_time' => $lesson['day_schedule_time'],
        'start_time_format' => $lesson['start_time_format'],
    );
    $ss_arr[] = $lesson['id'];
}

$sqlStudents = "SELECT p1.* FROM (SELECT DISTINCT IFNULL(st.id,'') student_id,
IFNULL(st.contact_id,'') contact_id, IFNULL(st.full_student_name,'') name,
st.first_name first_name, st.last_name last_name,
IFNULL(st.nick_name,'') nick_name, IFNULL(st.birthdate,'') birthdate,
IFNULL(st.phone_mobile,'') phone_mobile,
IFNULL(ea.email_address, '') email_address,
IFNULL(st.gender,'') gender, IFNULL(MIN(l2.start_study), '') situation_start_study,
IFNULL(MAX(l2.end_study), '') situation_end_study,
l4.avg_attendance avg_attendance
FROM contacts st
INNER JOIN meetings_contacts l1_1 ON st.id=l1_1.contact_id AND l1_1.deleted=0
INNER JOIN meetings l1 ON l1.id=l1_1.meeting_id AND l1.deleted=0 AND l1.id IN ('".implode("','",$ss_arr)."')
INNER JOIN j_studentsituations l2 ON l2.ju_class_id = '{$class->id}' AND l2.student_id = st.id AND l2.deleted=0
LEFT JOIN j_classstudents l4 ON l4.class_id = '{$class->id}' AND l4.student_id = l2.student_id AND l4.deleted = 0
LEFT JOIN email_addr_bean_rel ear ON st.id = ear.bean_id AND ear.deleted = 0
LEFT JOIN email_addresses ea ON ear.email_address_id = ea.id AND ea.deleted = 0
WHERE st.deleted = 0
GROUP BY student_id
UNION
SELECT DISTINCT IFNULL(st.id,'') student_id,
IFNULL(st.contact_id,'') contact_id, IFNULL(st.full_lead_name,'') name,
st.first_name first_name, st.last_name last_name,
IFNULL(st.nick_name,'') nick_name, IFNULL(st.birthdate,'') birthdate,
IFNULL(st.phone_mobile, '') phone_mobile,
IFNULL(ea.email_address, '') email_address,
IFNULL(st.gender,'') gender, IFNULL(MIN(l2.start_study), '') situation_start_study,
IFNULL(MAX(l2.end_study), '') situation_end_study,
l4.avg_attendance avg_attendance
FROM leads st
INNER JOIN meetings_leads l1_1 ON st.id=l1_1.lead_id AND l1_1.deleted=0
INNER JOIN meetings l1 ON l1.id=l1_1.meeting_id AND l1.deleted=0 AND l1.id IN ('".implode("','",$ss_arr)."')
INNER JOIN j_studentsituations l2 ON l2.ju_class_id = '{$class->id}' AND l2.student_id = st.id AND l2.deleted=0
LEFT JOIN j_classstudents l4 ON l4.class_id = '{$class->id}' AND l4.student_id = l2.student_id AND l4.deleted = 0
LEFT JOIN email_addr_bean_rel ear ON st.id = ear.bean_id AND ear.deleted = 0
LEFT JOIN email_addresses ea ON ear.email_address_id = ea.id AND ea.deleted = 0
WHERE st.deleted = 0
GROUP BY student_id) p1
ORDER BY p1.first_name, p1.student_id";
$resStudents = $GLOBALS['db']->query($sqlStudents);
while($student = $GLOBALS['db']->fetchByAssoc($resStudents)){
    if(!is_array($data[$student["student_id"]]))
        $data[$student["student_id"]] = array(
            'contact_id'        => $student['contact_id'],
            'student_name'      => $student['name'],
            'gender'            => (!empty($student['gender']) ? $GLOBALS['app_list_strings']['gender_list'][$student['gender']] : ''),
            'nick_name'         => $student['nick_name'],
            'phone_mobile'      => $student['phone_mobile'],
            'email_address'     => $student['email_address'],
            'dob'               => $timedate->to_display_date($student['birthdate'],false),
            'join_date'         => $timedate->to_display_date($student['situation_start_study'],false),
            'birthdate'         => $timedate->to_display_date($student['birthdate'],false),
            'end_date'          => $timedate->to_display_date($student['situation_end_study'],false),
            'avg_attendance'    => $student['avg_attendance'],
        );

}
$scheduleStr = "Schedule: ";
foreach($scheduleArr as $key => $value){
    if($scheduleStr != "Schedule: ") $scheduleStr .= "/";
    $scheduleStr .= $value;
}

$sheet = $objPHPExcel->getActiveSheet();
//Set Value
$sheet->SetCellValue('C3',$class->class_code);
$sheet->SetCellValue('C4',$class->name);
$sheet->SetCellValue('C5',$teacherName);
$sheet->SetCellValue('C6',$class->start_date);
$sheet->SetCellValue('D6',$class->end_date);
$sheet->SetCellValue('C7',$scheduleStr);
$sheet->SetCellValue('C8','');

//Print Column
$countLesson = 0;
$week_ = '';
$colBegin   = '';
$colFinish  = '';
foreach($lessonArr as $key => $week){
    foreach($week as $value){
        $colIndexStart = 9 + $countLesson;
        if(empty($colBegin)) $colBegin = $colIndexStart;
        $colStart   = PHPExcel_Cell::stringFromColumnIndex($colIndexStart);
        $colEnd     = PHPExcel_Cell::stringFromColumnIndex($colIndexStart + (count($week) - 1) );

        $sheet->SetCellValue($colStart.'9', $value['lesson']);
        if($week_ != $value['week_no']){
            $sheet->mergeCells($colStart.'10:'.$colEnd.'10');
            $sheet->SetCellValue($colStart.'10', $value['week_no']);
            //Set Style
            $sheet->duplicateStyle($sheet->getStyle('J10'), $colStart.'10:'.$colEnd.'10');
            $week_ = $value['week_no'];
        }
        $sheet->SetCellValue($colStart.'11', $value['start_time_format']);
        //Set Style
        $sheet->duplicateStyle($sheet->getStyle('J11'), $colStart.'11');
        $sheet->duplicateStyle($sheet->getStyle('J9'), $colStart.'9');
        $countLesson++;

        $index = 1;
        foreach($data as $key => $student_info){
            $row = 11 + $index;
            $data[$key]['attendance'][$value['day_schedule_time']]['id']                = $value['id'];
            $data[$key]['attendance'][$value['day_schedule_time']]['lesson']            = $value['lesson'];
            $data[$key]['attendance'][$value['day_schedule_time']]['col']               = $colStart.$row;
            $data[$key]['attendance'][$value['day_schedule_time']]['day_schedule_time'] = $value['day_schedule_time'];
            $index++;
        }
    }
}

//Set Style
$style = array('alignment' => array(
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
    ),
    'borders' => array(
        'right' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
        'left' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
        'top' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);

$colIndexStart  = 9 + $countLesson;
$colStart       = PHPExcel_Cell::stringFromColumnIndex($colIndexStart);
$sheet->mergeCells($colStart.'10:'.$colStart.'11');
$sheet->SetCellValue($colStart.'10', "Overall Atd %");
$sheet->getColumnDimension($colStart)->setWidth(12);
$sheet->getStyle($colStart.'10:'.$colStart.'11')->applyFromArray($style);

if(empty($colFinish)) $colFinish = $colStart;

$q1 = "SELECT DISTINCT IFNULL(c.id, '') student_id,
IFNULL(mt.id, '') meeting_id,
CONVERT_TZ(mt.date_start,'+0:00','+07:00') day_schedule_time,
IFNULL(a.attendance_type, '') attendance_type
FROM contacts c
INNER JOIN meetings_contacts mc ON mc.contact_id = c.id AND mc.deleted = 0 AND mc.meeting_id IN ('".implode("','",$ss_arr)."')
INNER JOIN meetings mt ON mc.meeting_id = mt.id AND mt.deleted = 0 AND mt.id IN ('".implode("','",$ss_arr)."')
LEFT JOIN c_attendance a ON a.student_id = c.id AND a.meeting_id = mt.id AND a.deleted = 0
WHERE (mt.session_status <> 'Cancelled') AND (mt.ju_class_id = '{$class->id}')
GROUP BY c.id, day_schedule_time
UNION
SELECT DISTINCT IFNULL(c.id, '') student_id,
IFNULL(mt.id, '') meeting_id,
CONVERT_TZ(mt.date_start,'+0:00','+07:00') day_schedule_time,
IFNULL(a.attendance_type, '') attendance_type
FROM leads c
INNER JOIN meetings_leads mc ON mc.lead_id = c.id AND mc.deleted = 0 AND mc.meeting_id IN ('".implode("','",$ss_arr)."')
INNER JOIN meetings mt ON mc.meeting_id = mt.id AND mt.deleted = 0 AND mt.id IN ('".implode("','",$ss_arr)."')
LEFT JOIN c_attendance a ON a.student_id = c.id AND a.meeting_id = mt.id AND a.deleted = 0
WHERE (mt.session_status <> 'Cancelled') AND (mt.ju_class_id = '{$class->id}')
GROUP BY c.id, day_schedule_time";
$rs = $GLOBALS['db']->query($q1);
while($row1 = $GLOBALS['db']->fetchByAssoc($rs)){
    $data[$row1['student_id']]['attendance'][$row1['day_schedule_time']]['attendance_type'] = $row1['attendance_type'];
    $data[$row1['student_id']]['reg_list'][] = $row1['meeting_id'];
}

//get color Attendance
$att_config = $GLOBALS['app_list_strings']['att_config_options'];

//Print Student
$index = 1;
foreach($data as $key => $value){
    if(!empty($value['student_name'])){
        $row = 11 + $index;
        $sheet->SetCellValue('A'.$row, $index);
        $sheet->SetCellValue('B'.$row, $value['contact_id']);
        $sheet->SetCellValue('C'.$row, $value['student_name']);
        $sheet->SetCellValue('D'.$row, $value['nick_name']);
        $sheet->SetCellValue('E'.$row, $value['gender']);
        $sheet->SetCellValue('F'.$row, $value['birthdate']);
        $sheet->SetCellValue('G'.$row, $value['phone_mobile']);
        $sheet->SetCellValue('H'.$row, $value['email_address']);
        $sheet->SetCellValue('I'.$row, $value['join_date']);
        $joinD = $timedate->convertToDBDate($value['join_date'],false);
        $endD  = $timedate->convertToDBDate($value['end_date'],false);
        $index++;
        $sheet->duplicateStyle($sheet->getStyle('A12:I12'), 'A'.$row.':I'.$row);
        foreach($value['attendance'] as $key => $attendance){
            $sheet->duplicateStyle($sheet->getStyle('A12'), $attendance['col']);
            if(in_array($attendance['id'], $value['reg_list'])){
                $sum = $attendance['attendance_type'];
                if(empty($sum)) $sum = '-';
                $sheet->SetCellValue($attendance['col'], $sum);

                $sheet->getStyle($attendance['col'])
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB(labelAttOverallForExcel($att_config[$sum])['color']);
            } else{
                $sheet->getStyle($attendance['col'])
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB('CCCCCC');
            }
        }
        $sheet->getColumnDimension('H')->setAutoSize(true);
        //Set AVG Attendance
        $avg = labelAttOverallForExcel($value['avg_attendance']);
        $lrc = $colFinish.$row;
        $sheet->duplicateStyle($sheet->getStyle('A12'), $lrc);
        $sheet->SetCellValue($lrc, $avg['text']);
        $sheet->getStyle($lrc)
        ->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()->setRGB($avg['color']);

    }
}

//Print sign row
$row = $index + 16;
$sheet->getStyle('B'.$row)->getFont()->setBold(true);
$sheet->getStyle('E'.$row)->getFont()->setBold(true);
$sheet->getStyle('H'.$row)->getFont()->setBold(true);
$sheet->SetCellValue('B'.$row, "Prepared");
$sheet->SetCellValue('E'.$row, "Checked");
$sheet->SetCellValue('H'.$row, "Approval");



// Save Excel 2007 file
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$section = create_guid_section(6);
$file = "upload/$fdir/ReportAttendance-$section.xlsx";
ob_end_clean();
$objWriter->save($file);
//download to browser
if (file_exists($file)) {
    ob_end_clean();
    header('Content-Description: File Transfer');
    //header('Content-type: application/vnd.ms-excel');
    //    header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
    unlink($file);
    exit;
}

function cellColor($cells,$color){
    global $objPHPExcel;


}
