<?php
    // Do not store anything in this file that is not part of the array or the hook version.  This file will
    // be automatically rebuilt in the future.
    $hook_version = 1;
    $hook_array = Array();


    $hook_array['before_save'] = Array();
    $hook_array['before_save'][] = Array(1, 'Class Code', 'custom/modules/J_Class/logicClass.php', 'logicClass', 'addClassCode');
    $hook_array['before_save'][] = Array(2, 'Logic Hooks Class', 'custom/modules/J_Class/logicClass.php', 'logicClass', 'handleSave');
    $hook_array['before_save'][] = Array(3, 'Create LMS Course', 'custom/modules/J_Class/logicClass.php', 'logicClass', 'handleLMS');

    $hook_array['before_delete'] = Array();
    $hook_array['before_delete'][] = Array(1, 'Check Bofore Class', 'custom/modules/J_Class/logicClass.php','logicClass', 'checkBeforeDelete');
    $hook_array['before_delete'][] = Array(2, 'Delete Notification', 'custom/modules/C_News/logicNews.php', 'logicNews', 'deleteMobileNotification');

    $hook_array['after_save'] = Array();
    $hook_array['after_save'][] = Array(1, 'Handle Save Test', 'custom/modules/J_Class/logicClass.php','logicClass', 'handleAfterSave');
    $hook_array['after_save'][] = Array(2, 'Handle Create ClassIn Course', 'custom/modules/J_Class/logicClass.php','logicClass', 'handleClassInCourse');
    $hook_array['after_save'][] = Array(3, 'Handle Massupdate Session', 'custom/modules/J_Class/logicClass.php','logicClass', 'handleMassUpdateCancelSesssion');


    $hook_array['process_record'] = Array();
    $hook_array['process_record'][] = Array(1, 'Color', 'custom/modules/J_Class/logicClass.php','logicClass', 'listViewColorClass');

    $hook_array['before_filter'] = Array();
    $hook_array['before_filter'][] = array(1,'Add Filter For Role Teacher','custom/modules/J_Class/logicClass.php','logicClass','addFilter');
    $hook_array['before_filter'][] = array(2,'Add Filter For Holiday','custom/modules/J_Class/logicClass.php','logicClass','filterHoliday');

    $hook_array['after_delete'] = Array();
    $hook_array['after_delete'][] = array(1,'After delete','custom/modules/J_Class/logicClass.php','logicClass','afterDelete');

    $hook_array['after_retrieve'] = Array();
    $hook_array['after_retrieve'][] = array(1,'HandleBsend','custom/modules/J_Class/logicClass.php','logicClass','HandleBsend');
    $hook_array['after_retrieve'][] = array(2,'addHTMLSchedule','custom/modules/J_Class/logicClass.php','logicClass','buildScheduleHTML');