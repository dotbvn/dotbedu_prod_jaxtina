{dotb_getscript file="custom/include/javascript/BootstrapMultiselect/js/bootstrap-multiselect-min.js"}
{dotb_getscript file="custom/modules/J_Class/js/segbar_new.js"}
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/J_Class/css/segbar.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Bootstrap/bootstrap.min.css}"/>
{literal}
<style type="text/css">
.table-border td, .table-border th{
    border: 1px solid #ddd !important;
    border-collapse:collapse;
    line-height: 18px;
    padding: 15px !important;
    vertical-align: middle !important;
    text-align: center;
}
 .table-border {
     border-collapse:collapse;
 }
.no-bottom-border {
     border-bottom: 0px solid cadetblue !important;
}
.no-top-border {
     border-top: 0px solid cadetblue !important;
}
</style>
{/literal}
<div id="diaglog_prv_student" title="{$MOD.LBL_STUDENT_FROM_PREVIOUS}" style="display:none;">
    <input type="hidden" id="last_modified" value="{$fields.date_modified.value}">
    <input type="hidden" id="prv_student_list" value="">
    <input type="hidden" name="ern_sessions" id="ern_sessions" value="{$json_ss}">
    {if !$ernCf.allow_outstanding}<input style="display:none;" type="checkbox" name="prv_allow_ost" id="prv_allow_ost" value="1">{/if}
    {if !$ce_settle_date}<input type="hidden" name="prv_settle_date" id="prv_settle_date" value="{$today}">{/if}
    <table width="100%" class="edit view ">
    <thead>
            <tr style="height: 50px;"><td colspan="4">
            <table width="100%">
            <tbody>
            <tr>
                    <td width="15%" scope="col">{$MOD.LBL_CLASS_CODE}: </td>
                    <td width="35%">{$fields.class_code.value}</td>
                    <td width="15%" scope="col">{$MOD.LBL_START_DATE}: </td>
                    <td width="35%">{$fields.start_date.value}</td>
            </tr>
            <tr>
                    <td scope="col">{$MOD.LBL_NAME}: </td>
                    <td style="font-weight: bold">{$fields.name.value}</td>
                    <td scope="col">{$MOD.LBL_END_DATE}: </td>
                    <td>{$fields.end_date.value}</td>
            </tr>
            <tr>
                    <td scope="col">{$MOD.LBL_MAIN_SCHEDULE}: </td>
                    <td  id="prv_schedule">{$SCHEDULE}</td>
                    <td scope="col">{$MOD.LBL_KIND_OF_COURSE}: </td>
                    <td >{$KOC}</td>
            </tr>
            <tr>
                    <td colspan="4"><b style="float: left;color: #002bff;">{$MOD.LBL_STEP1}:</b></td>
            </tr>
            <tr>
                    <td scope="col">{$MOD_S.LBL_START_STUDY}:<span style="color:red;"> *</span></td></td>
                    <td ><span class="dateTime"><input disabled name="prv_start_study" size="10" id="prv_start_study" type="text" value="{$fields.start_date.value}">  <img border="0" src="custom/themes/default/images/jscalendar.png" alt="Enter Date" id="prv_start_study_trigger" align="absmiddle"></span></td>
                    <td scope="col">{$MOD.LBL_TOTAL_SESSIONS}: </td>
                    <td  id="prv_total_sessions_text" style="font-weight:bold;"></td><input type = "hidden" id="prv_total_sessions" value=""/>
            </tr>
            <tr>
                    <td scope="col">{$MOD_S.LBL_END_STUDY}:<span style="color:red;"> *</span></td>
                    <td ><span class="dateTime"><input disabled name="prv_end_study" size="10" id="prv_end_study" type="text" value="{$fields.end_date.value}">  <img border="0" src="custom/themes/default/images/jscalendar.png" alt="Enter Date" id="prv_end_study_trigger" align="absmiddle"></span></td>
                    <td scope="col">{$MOD.LBL_TOTAL_HOURS}: </td>
                    <td  id="prv_total_hours_text" style="font-weight:bold;"></td><input type = "hidden" id="prv_total_hours" value=""/>
            </tr>
            <tr>
                    <td scope="col">{$MOD.LBL_ALLOW_OST}:</td>
                    <td >
                    {if $ernCf.allow_outstanding}
                    <input title="{$MOD.LBL_ALLOW_OST}" style="vertical-align: middle; margin-right: 5px;width: 15px;height: 15px;" checked type="checkbox" name="prv_allow_ost" id="prv_allow_ost" value="1">
                    {else}
                    <b style="color:red">-{$MOD.LBL_NONE}-</b>
                    {/if}
                    </td>
                    {if $ernCf.allow_settle}
                    <td scope="col">{$MOD.LBL_ENROLLMENT_DATE}:<span style="color:red;"> *</span></td>
                    <td >
                    {if $ce_settle_date}
                    <span class="dateTime"><input disabled name="prv_settle_date" size="10" id="prv_settle_date" type="text" value="{$today}"><img border="0" src="custom/themes/default/images/jscalendar.png" alt="Enter Date" id="prv_settle_date_trigger" align="absmiddle"></span>
                    {else}
                    {$today}
                    {/if}
                    </td>
                    {else}<td></td><td></td>{/if}
            </tr>
                    </tbody>
                    </table></td>
            </tr>
            <tr> <td colspan="2"><b style="padding-left: 10px; color: #002bff;">{$MOD.LBL_STEP2}:</b></td></tr>
            <tr><td>
                <span style="padding-left: 30px;">{$MOD.LBL_STEP2_1}:<span style="color:red;"> *</span></span>
                <input type="text" name="prv_class_name" id="prv_class_name" value="" disabled class="input_readonly">
                <input type="hidden" name="prv_class_id" id="prv_class_id" value="">
                <span class="id-ff multiple">
                <button type="button" name="btn_prv_class_name" id="btn_prv_class_name" tabindex="0" title="{$APPS.LBL_ID_FF_SELECT} {$MOD.LBL_NAME}" class="button firstChild" value="{$APPS.LBL_ID_FF_SELECT} {$MOD.LBL_NAME}""><img src="themes/default/images/id-ff-select.png"></button>
                <button type="button" name="btn_clr_prv_class_name" id="btn_clr_prv_class_name" tabindex="0" title="{$APPS.LBL_ID_FF_CLEAR} {$MOD.LBL_NAME}" class="button lastChild" value="{$APPS.LBL_ID_FF_CLEAR} {$MOD.LBL_NAME}"><img src="themes/default/images/id-ff-clear.png"></button>
                </span>
                <input style="margin-left: 5px;" type="button" id="prv_load_student" value="{$MOD.LBL_BTT_LOAD_STUDENT}"></input>
            </td>
            <td>
            <span style="padding-left: 10px;">{$MOD.LBL_STEP2_2}:<span style="color:red;">* </span></span> <input style="margin-left: 30px;" type="button" class="button" id="prv_openpopup_student" value="{$MOD.LBL_BTT_GET_STUDENT}"></input>
            <input type="hidden" name="prv_selected_student" id="prv_selected_student" value="">
            </td>
            <td>
            <span style="padding-left: 10px;">{$MOD.LBL_STEP2_3}:<span style="color:red;">* </span></span> <input style="margin-left: 30px;" type="button" class="button" id="prv_btn_ost_load" value="{$MOD.LBL_BTT_GET_OUTSTANDING}"></input>
            <input type="hidden" id="prv_ost_load" value="0">
            </td>
            </tr>
            <tr> <td colspan="4">
            <table id="table_prv_student" class="table_prv_student table-border" width="100%" cellpadding="10" cellspacing="0" style="min-height:50px">
            <thead>
            <tr>
                <th width="3%" style="text-align: center;"><input type="checkbox" class="checkall_custom_checkbox" module_name="prv_student_list" onclick="handleCheckBox($(this));handleCountItem();">
                <div class="selectedTopSupanel"></div>
                </th>
                <th width="10%" style="text-align: center;">{$MOD.LBL_STUDENT_ID} </th>
                <th width="10%" style="text-align: center;">{$MOD.LBL_STUDENT_NAME} </th>
                <th width="10%" style="text-align: center;">{$MOD.LBL_MOBILE} </th>
                <th width="10%" style="text-align: center;">{$MOD.LBL_BIRTHDATE} </th>
                <th width="10%" style="text-align: center;">{$MOD.LBL_LAST_GRADEBOOK} </th>
                <th width="25%" style="text-align: center;">
                <span>{$MOD.LBL_PAYMENT_LIST}</span>
                <div><button style="font-weight: normal;" id="prv_reload_all">{$MOD.LBL_BTN_RELOAD}</button>
                <button style="font-weight: normal;" id="prv_unselect_all">{$MOD.LBL_BTN_UNSELECTALL}</button></div>
                </th>
                <th width="35%" style="text-align: center;min-width: 85px;">{$MOD.LBL_ADD_TYPE} </th>
                <th width="10%" style="text-align: center;">{$MOD.LBL_ADMISSION_STATUS} </th>
            </tr>
            </thead>
            <tbody id="tbody_prv_student">
            </tbody>
    </table>
    </td></tr>
    <tr> <td colspan="1">
    <table width="100%">
    <tbody>
            <tr>
                    <td scope="col">{$MOD.LBL_TOTAL_STUDENT}: </td>
                    <td width="10%" style="color: green; font-weight:bold;" id="total_prv_student">0</td>
            </tr>
            <tr><td style="text-align: center" colspan="4"></td></tr>
    </tbody>
    </table>
    </td></tr>
    </thead></table></div>
