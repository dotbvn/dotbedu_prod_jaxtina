
<div id="delay_class_dialog" title="{$MOD.LBL_DELAY_CLASS}" style="display: none;">
    <table width="100%"  class="edit view">
        <input id="dl_student_id" type="hidden" value=""/>
        <input id="dl_class_student_id" type="hidden" value=""/>
        <input id="dl_date" type="hidden" value=""/>
        <input id="dl_enrollment_start" type="hidden" value=""/>
        <input id="dl_enrollment_end" type="hidden" value=""/>
        <input id="dl_reversed_hours" type="hidden" value="0"/>
        <input id="dl_remove_from_class" type="hidden" value="0"/>
        <tbody>
            <tr>
                <td colspan="4">{$MOD.LBL_DELAY_INTRO}</td>
            </tr>
            <tr>
                <td width="20%">{$MOD.LBL_NAME}: </td>
                <td width="40%" class="dl_class_name">{$fields.name.value}</td>
                <td width="20%"></td>
                <td width="20%"></td>
            </tr>
            <tr>
                <td>{$MOD.LBL_MAIN_SCHEDULE}: </td>
                <td class="dl_schedule">{$SCHEDULE}</td>
                <td>{$MOD.LBL_START_ENROLLMENT}: </td>
                <td class="dl_enrollment_start"></td>
            </tr>

            <tr>
                <td>{$MOD.LBL_STUDENT_NAME}: </td>
                <td class="dl_student_name"></td>
                <td>{$MOD.LBL_END_ENROLLMENT}: </td>
                <td class="dl_enrollment_end"></td>
            </tr>

            <tr><td colspan="4"><b style="float: left;color: #002bff;">{$MOD.LBL_DELAY_STEP_1}</b></td></tr>
            <tr>
                <td nowrap>{$MOD.LBL_DELAY_DATE}:</td>
                <td><span class="dl_date"></span></td>
                <td nowrap>{$MOD.LBL_TOTAL_HOURS}</td>
                <td><span class="dl_total_hours"></span></td>
            </tr>
            <tr>
                <td nowrap>{$MOD.LBL_DELAY_FROM}:<span style="color:red;"> *</span>
                <img border='0' onclick='return DOTB.util.showHelpTips(this,"{$MOD.LBL_REMOVE_FROM_CLASS_DES}");' src='themes/RacerX/images/helpInline.png' alt='studiedHelpTip' class='studiedHelpTip'></td>
                </td>
                <td><span class="dateTime" style="white-space: nowrap;"><input disabled size="10" id="dl_from_date" type="text" value="{$today}">  <img border="0" src="custom/themes/default/images/jscalendar.png" alt="Enter Date" id="dl_from_date_trigger" align="absmiddle"><input style="margin-left: 5px;" type="button" class="button" id='btn_dl_remove_from_class' value="{$MOD.LBL_BTN_REMOVE_FROM_CLASS}"></span>
                </td>
                <td nowrap>{$MOD.LBL_DELAY_TO}:</td>
                <td nowrap>
                <span class="dl_to_date"></span>
                <span class="dateTime" style="display:none">
                <input disabled size="10" id="dl_to_date" type="text" value="{$next_session_date}"></span>
                </td>
            </tr>
            <tr>
            <td colspan="2"><b style="float: left;color: #002bff;">{$MOD.LBL_DELAY_STEP_2}</b></td>
            <td nowrap><span>{$MOD.LBL_LAST_STUDY_DATE}: </span></td>
            <td><span id="dl_last_study" style="font-weight:bold;"></span> </td>
            </tr>
            <tr>
                <td nowrap> <span>{$MOD.LBL_BF_ENROLLED_HOURS}: </span></td>
                <td> <span id="dl_bf_enrolled_hours" style="font-weight:bold; color:#468931;">0</span></td>
                <td nowrap> <span>{$MOD.LBL_BF_OST_HOURS}:</span></td>
                <td> <span id="dl_bf_ost_hours" style="font-weight:bold; color:#FF8C00;">0</span></td>
            </tr>
            <tr>
                <td nowrap><span>{$MOD.LBL_AF_ENROLLED_HOURS}: </span></td>
                <td><span id="dl_af_enrolled_hours" style="font-weight:bold; color:#468931;">0</span> </td>
                <td nowrap><span>{$MOD.LBL_AF_OST_HOURS}: </span>
                <img border='0' onclick='return DOTB.util.showHelpTips(this,"{$MOD.LBL_AF_ENROLLED_HOURS_DES}");' src='themes/RacerX/images/helpInline.png' alt='studiedHelpTip' class='studiedHelpTip'>
                </td>
                <td><span id="dl_af_ost_hours" style="font-weight:bold; color:#FF8C00;">0</span></td>
            </tr>
            <tr>
                <td nowrap><span>{$MOD.LBL_DELAY_HOUR}: </span></td>
                <td><span id="dl_delay_hours" style="font-weight:bold;">0</span> </td>
                <td rowspan="2" colspan="2" style="font-weight: 600;">
                <label for="dl_revert_delay"><input type="radio" name="dl_case" checked id="dl_revert_delay" value="revert_delay"> {$MOD.LBL_DELAY_CASE_1}</label><br>
                {if $can_drop_revenue}<label for="dl_drop_revenue"><input type="radio" name="dl_case" id="dl_drop_revenue" value="drop_revenue"> {$MOD.LBL_DELAY_CASE_2}</label>{/if}
                </td>
            </tr>
            <tr>
                <td nowrap><span>{$MOD.LBL_DELAY_AMOUNT}: </span>
                 <img border='0' onclick='return DOTB.util.showHelpTips(this,"{$MOD.LBL_DELAY_DES}<br>{$MOD.LBL_DELAY_CASE_1_DES}<br>{$MOD.LBL_DELAY_CASE_2_DES}");' src='themes/RacerX/images/helpInline.png' alt='studiedHelpTip' class='studiedHelpTip'></td>
                <td><span id="dl_delay_amount" style="font-weight:bold;">0</span></td>
            </tr>
            <tr>
            <td colspan="2"><b style="float: left;color: #002bff;">{$MOD.LBL_DELAY_STEP_3} <span style="color:red;"> *</span></b></td>
            <td colspan="2">
                {html_options name="dl_reason_for" id="dl_reason_for" options=$dl_reason_for_options selected=''}
                <textarea cols="50" rows="2" style="margin-top: 5px;" id="dl_reason"></textarea>
            </td>
            </tr>
        </tbody>
    </table>
</div>
