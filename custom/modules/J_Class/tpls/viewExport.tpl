{dotb_getscript file='custom/modules/J_Class/js/showExport_new.js'}
<div id="export_other_dialog" title="{$MOD.BTN_EXPORT_CERTIFICATE}" style="display: none;" >
    <div id ="change_template">
        <label for="template" class="span5">{$MOD.LBL_CHOOSE_TEMPLATE}:</label>
        <select name='template' id='template'>
            <option value="-none-">-{$MOD.LBL_NONE}-</option>
            <option value="In Course Report (New)">In Course Report (New)</option>
            <option value="Certificate (New)">Certificate (New)</option>
        </select>
    </div>
    <table class="studentList table-border" id="tbl_export_tp"><tbody></tbody>
    </table>
</div>
