<script type="text/javascript"  src="custom/modules/J_Class/js/jax_attendance.js"></script>
{dotb_getscript file="custom/include/javascript/Mask-Input/jquery.mask.min.js"}
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/Bootstrap/bootstrap.min.css'}">
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/modules/J_Class/tpls/css/attendance.css'}">
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file=custom/include/javascript/simptip.min.css}"/>
{literal}
<style type="text/css">
.table-border td, .table-border th{
    border: 1px solid #ddd;
    border-collapse:collapse;
    line-height: 18px;
    padding: 6px;
    vertical-align: middle;
    text-align: center;
}
 .table-border {
     border-collapse:collapse;
 }
.no-bottom-border {
     border-bottom: 0px solid #ddd !important;
}
.no-top-border {
     border-top: 0px solid #ddd !important;
}
.dialog_header {
    width: 100%;
}
.bold {
    font-weight: bold;
}
.text_comment {
    width: 100%;
    height: 100px;
}
</style>
{/literal}
<form action="" method="POST" name="AttendanceForm" id="AttendanceForm">
    <div id="content">
        <div class="container">
            <h4>{$MOD.LBL_CLASS_ATTENDANCE_TITLE}</h4>
            <input type="hidden" name="current_user_id" id="current_user_id" value="{$CURRENT_USER_ID}">
            <input type="hidden" name="session_id" id="session_id" value="{$SESSION_ID}">
            <input type="hidden" name='json_session' id='json_session' value='{$JSON_SESSION}'>
        <table class="table-condensed">
            <tbody>
                <tr valign="top">
                    <td width="15%" nowrap><b>{$MOD.LBL_CLASS}:<span class="required">*</span></b></td>
                    <td width="35%"  nowrap >
                         <input type="text" name="class_name" id="class_name" value="{$CLASS_NAME}">
                         <input type="hidden" name="class_id" id="class_id" value="{$CLASS_ID}">
                         <button type="button" name="btn_select_class" id="btn_select_class" title="{$MOD.LBL_SELECT}" class="button firstChild" value="Select"><img src="themes/default/images/id-ff-select.png"></button>
                         <button type="button" name="btn_clr_class" id="btn_clr_class" title="{$MOD.LBL_CLEAR_SELECTION}" class="button lastChild"  value="Clear Selection"><img src="themes/default/images/id-ff-clear.png"></button>
                    </td>
                    <td rowspan="6" style="width:50%;vertical-align: top;padding-left: 5%;" id="td_class_info">

                    </td>
                </tr>
                <tr  valign="top">
                    <td nowrap><b>{$MOD_M.LBL_TOPIC_CUSTOM}: </b></td>
                    {if $lockEditTopicCustom}
                    <td id="topic_custom_text">{$topic_custom}</td>
                    {else}
                    <td nowrap colspan="1"><input type="text" name="topic_custom" id="topic_custom" value="{$topic_custom}" title="{$MOD.LBL_TOPIC_CUSTOM}" onchange="saveSyllabus($(this));" style="width: 300px;"></td>
                    {/if}

                </tr>
                <tr valign="top">
                    <td nowrap><b>{$MOD_M.LBL_SYLLABUS_CUSTOM}: </b></td>
                    {if $lockEditSyllabusCustom}
                    <td id="syllabus_custom_text">{$syllabus_custom}</td>
                    {else}
                    <td nowrap colspan="1">
                        <textarea  id="syllabus_custom" name="syllabus_custom" rows="4" cols="50" style="width: 300px;resize: vertical;"  onchange="saveSyllabus($(this));">{$syllabus_custom}</textarea>
                    </td>
                    {/if}
                </tr>
                <tr valign="top">
                    <td nowrap><b>{$MOD_M.LBL_NOTE_FOR_TEACHER_CUSTOM}: </b></td>
                    {if $lockEditNoteTeacherCustom}
                    <td id="objective_custom_text">{$objective_custom}</td>
                    {else}
                    <td nowrap colspan="1">
                        <textarea  id="objective_custom" name="objective_custom" rows="4" cols="50" style="width: 300px;resize: vertical;"  onchange="saveSyllabus($(this));">{$objective_custom}</textarea>
                    </td>
                    {/if}
                </tr>
                <tr valign="top">
                <td colspan="2"></td>
                </tr>
                <tr>
                    <td  nowrap>
                        <b>{$MOD.LBL_DATE_IN_CONTENT}: <span class="required">*</span></b>
                    </td>
                    <td nowrap>
                        <span class="dateTime">
                            <input class="date_input" size="15" autocomplete="off" type="text" name="date_in_content" id="date_in_content" value="{$DATE}" title="{$MOD.LBL_DATE_IN_CONTENT}" tabindex="0" maxlength="10" style="vertical-align: top;">
                            <img src="themes/RacerX/images/jscalendar.png" alt="Enter Date" style="position:relative; top:0px" border="0" id="date_in_content_trigger" style="margin-top: 10px;">
                            <span id='time_html' style="vertical-align: top;"></span>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" nowrap align='center'>
                    {if $lockView}
                    {else}
                        <input type="button" class="button primary" id="refresh_sms" value="{$MOD.LBL_BTN_RELOAD}" onclick="load_ss_json();">
                        <input type="button" class="button" id="clear_content" value="{$MOD.LBL_BTN_CANCEL_C}" style="margin-left: 20px;">
                    {/if}
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
        <div><hr></div>
        <span style="color:red; font-size:20px"><i class="far fa-exclamation-circle"></i>  Giáo viên vui lòng nhập đủ các cột thông tin điểm danh hằng ngày để đảm bảo quyền lợi tính công.</span>
        <table class="tbl student_list table-border" id="tbl_student" width="100%" >
            <thead>
                <tr>
                    <th width="5%" style="text-align: center;" rowspan="2">#</th>
                    <th width="15%" style="text-align: center;padding-left: 15px;" rowspan="2">{$MOD.LBL_STUDENT_NAME}</th>
                    <th width="7%" style="text-align: center;" rowspan="2">{$MOD.LBL_BIRTHDATE}</th>
                    <th width="7%" style="text-align: center;" rowspan="2">{$MOD.LBL_NICK_NAME}</th>
                    <th width="7%" style="text-align: center;" rowspan="2">{$MOD.LBL_TYPE}</th>
                    <th width="7%" style="text-align: center;" rowspan="2">{$MOD.LBL_AVG_ATTENDANCE}</th>
                    {if $lockEdit}
                    <th class="send_att" width="7%" style="text-align: center;" nowrap="" rowspan="2">{$MOD.LBL_ATTENDANCE}</th>
                    {else}
                    <th class="send_att" width="7%" style="text-align: center;" nowrap="" rowspan="2">{$attendance}</th>
                    {/if}
                    <th class="send_att" width="5%" style="text-align: center;" colspan="5">{$MOD.LBL_FLIPPED} <br>
                        <img border="0" onclick="showHelpTips(this,'<strong>Lưu ý:</strong>  0 <=  Số điểm Flipped <= 100', '', '', '', 300, 70);" src="themes/RacerX/images/help-dashlet.png" style="vertical-align: bottom;">
                    </th>
                    <th class="send_att" width="5%" style="text-align: center;" rowspan="2">{$MOD.LBL_HOMEWORK_NUM}
                        <img border="0" onclick="showHelpTips(this,'<strong>Lưu ý:</strong>  0 <= Số điểm Homework <= 100','', '', '', 300, 70);" src="themes/RacerX/images/help-dashlet.png" style="vertical-align: bottom;">
                    </th>
                    <th colspan="2" class="send_att" width="5%" style="text-align: center;">{$MOD.LBL_TEACHER_COMMENT_NUM}
                        <img border="0" onclick="showHelpTips(this, '<p> <strong>Lưu ý:</strong> Points bằng tổng điểm 3 phần: Giao tiếp và lắng nghe (max 5 điểm), Tham gia hoạt động nhóm (max 5 điểm), Tập trung lớp học (max 5 điểm)</p> <br>' +
                         '<table style=&quot;width: 100%; border-collapse: collapse &quot;;>' +
                         '<thead>' +
                         '<tr>' +
                         '<th style=&quot;border: solid 1pt black; padding: 3pt&quot;;> </th>' +
                         '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Excellent <br>5</th>' +
                         '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Very Good <br>4</th>' +
                         '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Good <br>3</th>' +
                         '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Fair <br>2</th>' +
                         '<th style=&quot;border: solid 1pt black; padding: 3pt; text-align:center&quot;;>Need Improvement <br>1</th>' +
                         '</tr> ' +
                         '</thead> ' +
                         '<tbody> ' +
                         '<tr>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Giao tiếp và lắng nghe</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Luôn luôn trả lời câu hỏi và chủ động đặt câu hỏi</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thường xuyên trả lời câu hỏi và chủ động đặt câu hỏi</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Trả lời câu hỏi nhưng hiếm khi chủ động đặt câu hỏi</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thỉnh thoảng trả lời câu hỏi và hiếm khi chủ động đặt câu hỏi</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Chỉ trả lời câu hỏi khi thực sự cần thiết/ bắt buộc</td>' +
                         '</tr>' +
                         '<tr>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Tham gia hoạt động nhóm</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Luôn luôn chủ động tham gia đầy đủ các hoạt động  nhóm</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thường xuyên chủ động tham gia đầy đủ các hoạt động  nhóm</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Tham gia hoạt động  nhóm nhưng chưa chủ động</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thỉnh thoảng tham gia hoạt động nhóm</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Chỉ tham gia khi thực sự cần thiết/ bắt buộc</td>' +
                         '</tr>' +
                         '<tr>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Tập trung trong lớp học</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Luôn luôn tập trung vào giáo viên và chủ động thực hiện nhiệm vụ / bài tập.</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thường xuyên tập trung vào giáo viên và chủ động thực hiện nhiệm vụ / bài tập. </td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Đôi khi mất tập trung nhưng vẫn thực hiện đủ nhiệm vụ / bài tập.</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Thỉnh thoảng tập trung và cần được nhắc nhở thực hiện nhiệm vụ/ bài tập</td>' +
                         '<td style=&quot;border: solid 1pt black; padding: 3pt&quot;;>Không tập trung và chỉ thực hiện nhiệm vụ / bài tập khi được nhắc nhở thường xuyên</td>' +
                         '</tr>' +
                         '</tbody>' +
                         '</table>', '', '', '', 700, 450);" src="themes/RacerX/images/help-dashlet.png" style="vertical-align: bottom;">
                    </th>
                    {if $lockEdit}
                    <th class="send_att" width="10%" style="text-align: center;" rowspan="2">{$MOD.LBL_CARE_COMMENT}</th>
                    {else}
                    <th class="send_att" width="10%" style="text-align: center;" rowspan="2">{$care_comment}</th>
                    {/if}

                    <th class="send_att" width="5%" style="text-align: center;" rowspan="2">Note</th>
                    <th class="send_att" width="5%" style="text-align: center;" rowspan="2">{$MOD.LBL_HOMEWORK_SCORE}
                        <img border="0" onclick="return showHelpTips(this,'<strong>Lưu ý:</strong> Công thức tính Active Leaning: <br> <br> (% Câu đúng x 40)+(Homework x 40)+(Student\'s interaction % x 20) _____________________________________________________________________ <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                         '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                          '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100', '', '', '', 450, 150);" src="themes/RacerX/images/help-dashlet.png" style="vertical-align: bottom;">

                    </th>
                    <th class="send_att" width="5%" style="text-align: center;" rowspan="2">Total Stickers</th>
                    <th class="send_att" width="5%" style="text-align: center;" rowspan="2">{$MOD.LBL_LOYALTY_POINT}</th>
                </tr>
                <tr>
                    <th class="send_att" width="5%" style="text-align: center;">Thời gian học</th>
                    <th class="send_att" width="5%" style="text-align: center;">Số câu đúng</th>
                    <th class="send_att" width="5%" style="text-align: center;">Số câu sai</th>
                    <th class="send_att" width="5%" style="text-align: center;">Tổng điểm</th>
                    <th class="send_att" width="5%" style="text-align: center;">% Câu đúng</th>
                    <th class="send_att" width="5%" style="text-align: center;">Point</th>
                    <th class="send_att" width="5%" style="text-align: center;">%</th>
                </tr>
            </thead>
            <tbody>
                {$STUDENT_LIST}
            </tbody>
        </table>
    </div>
</form>

<div id="dialog_messenge" title="{$MOD.LBL_MESSAGES}" style="display: none;">
    <table width="100%"  class="edit view">
        <tbody>
        <tr>
            <td  nowrap>
                <span><b>{$MOD.LBL_TEMPLATE}:</b></span>
            </td>
            <td  nowrap>
                <select id="template" name="template" onchange="load_template();">{$TEMPLATE_OPTIONS}</select>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <b>{$MOD.LBL_MESSAGES}: </b>
            </td>
            <td nowrap colspan="2">
                <textarea id="template_content" name="template_content" rows="4" cols="50" style="resize: vertical;height: 100px;"></textarea>
            </td>
        </tr>
        <table>
            <tbody>
            <tr>
                <button type="submit" class="btn btn-primary" id="ok_messenge">{$MOD.LBL_GENERATE}</button>
            </tr>
            </tbody>
        </table>
        </tbody>
    </table>
</div>

<div id='dialog_comment' style="display:none;">
    <input type="hidden" name = 'comment_options'>
    <div class="dialog_header">
        <table >
            <tbody>
            <tr>
                <td class="right"><b>{$MOD.LBL_STUDENT_NAME_C}</b></td>
                <td class="mid"></td>
                <td><span id = 'dialog_student_name'  class="bold"></span></td>
            </tr>
            </tbody>
        </table>
        <hr>
    </div>
    <div class="dialog_content">
        <table width="100%">
            <tbody>
            <tr><td id="title"></td></tr>
            <tr><td><textarea name='dialog_text_comment' id = 'total_comment' class="text_comment"></textarea></td></tr>
            </tbody>
        </table>
    </div>
</div>


