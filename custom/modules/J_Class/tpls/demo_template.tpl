<div id="diaglog_demo" title="{$MOD.LBL_ADD_DEMO}" style="display:none;">
    <table width="100%"  class="edit view">
        <tbody>
        <input type="hidden" id="dm_type_student" value="{$dm_student_type}">
        <input type="hidden" id="dm_student_id" value="{$dm_student_id}">
        <input type="hidden" id="dm_add_type" value="create">
            <tr>
                <td> {$MOD.LBL_STUDENT_NAME}: </td>
                <td style="font-weight: bold" colspan="3" id="dm_student_name">{$dm_student_name}</td>
            </tr>
            <tr>
            <td>{$MOD.LBL_CLASS_CODE}: </td>
            <td id="dm_class_code" style="font-weight: bold">{$fields.class_code.value}</td>
            <td>{$MOD.LBL_START_DATE}: </td>
            <td id="dm_start_date">{$fields.start_date.value}</td>
            </tr>
            <tr>
            <td>{$MOD.LBL_NAME}: </td>
            <td id="dm_class_name_demo" style="font-weight: bold">{$fields.name.value}</td>
            <td>{$MOD.LBL_END_DATE}: </td>
            <td id="dm_end_date">{$fields.end_date.value}</td>
            </tr>
            <tr>
            <td>{$MOD.LBL_NEXT14_SCHEDULE}: </td>
            <td id="dm_schedule">{$SCHEDULE}</td>
            </tr>
            <tr>
            <td style="vertical-align: middle;">{$MOD.LBL_FROM_DATE}:</td>
            <td><span class="dateTime"><input disabled id="dm_lesson_date" name="dm_lesson_date" size="10" type="text" value="{$next_session_date}">  <img border="0" src="custom/themes/default/images/jscalendar.png" alt="Enter Date" id="dm_lesson_date_trigger" align="absmiddle"></span></td>
            <td style="vertical-align: middle;">{$MOD.LBL_TOTAL_SESSIONS}: </td>
            <td style="vertical-align: middle; font-weight: bold;" id="dm_total_sessions_text"></td><input type = "hidden" id="dm_total_sessions" value=""/>

            </tr>
            <tr>
            <td style="vertical-align: middle;">{$MOD.LBL_TO_DATE}:</td>
            <td><span class="dateTime"><input disabled id="dm_lesson_to_date" name="dm_lesson_to_date" size="10" type="text" value="{$fields.end_date.value}">  <img border="0" src="custom/themes/default/images/jscalendar.png" alt="Enter Date" id="dm_lesson_to_date_trigger" align="absmiddle"></span></td>
            <td style="vertical-align: middle;">{$MOD.LBL_TOTAL_HOURS}: </td>
            <td style="vertical-align: middle;font-weight: bold;" id="dm_total_hours_text"></td><input type = "hidden" id="dm_total_hours" value=""/>

            </tr>
        </tbody>
    </table>
</div>