<div id="timeframe_panel">
   <form action="" method="POST" name="ClassScheduleForm" id="ClassScheduleForm">
      <table class="list view" width="40%">
         <thead>
            <tr>
               <th width="25%" align="center" style="text-align: center;">{$MOD.LBL_WEEKDAY}</th>
               <th width="55%" align="center" style="text-align: center;">{$MOD.LBL_TIME_SLOT}<span class="required">*</span></th>
               <th width="20%" align="center" class="hour_item1" style="text-align: center;">{$MOD.LBL_DURATION_HOUR}<span class="required">*</span></th>
               <th width="20%" align="center" class="hour_item2" style="text-align: center;">{$MOD.LBL_SCHEDULE_BY}<span class="required">*</span> <img border="0" onclick="return DOTB.util.showHelpTips(this,'{$MOD.LBL_SCHEDULE_BY_DES}');" src="themes/RacerX/images/helpInline.png"></th>
            </tr>
         </thead>
         <tbody>
            {foreach from=$_weekdays item=wd}
            <tr id="TS_{$wd.short_name}" class="timeslot" style="display: none;">
               <td nowrap align="left" style="text-align: center;">{$wd.label}</td>
               <td nowrap align="center">
                  <table>
                     <tbody>
                        <tr style="height: 34px;">
                           <td>
                              <p class="timeOnly">
                                 <input class="time start" type="text" style="width: 70px; text-align: center;">
                                 {$MOD.LBL_TO}
                                 <input class="time end" type="text" style="width: 70px; text-align: center;">
                              </p>
                           </td>
                           <td><span class="id-ff multiple ownline"><button type="button" style="margin-bottom: 0px;" class="button addTimeSlot primary" value="Add" title="{$MOD.LBL_ADD_TIME_SLOT}"><img src="themes/default/images/id-ff-add.png" alt="Add Time Slot"></button></span></td>
                        </tr>
                     </tbody>
                  </table>
               </td>
               <td nowrap align="center" class="hour_item1">
                    <table>
                        <tbody>
                            <tr style="height: 34px;"><td align="center"><span class="duration" style="color:#a52a2a;font-weight: 600;">---</span><input type="hidden" class="duration"></td></tr>
                        </tbody>
                    </table>
               </td>
               <td nowrap align="center" class="hour_item2">
                    <table>
                        <tbody>
                            <tr style="height: 34px;"><td align="center">{html_options name="schedule_by" class="schedule_by" options=$schedule_by_options selected=""}</td></tr>
                        </tbody>
                    </table>
               </td>
            </tr>
            {/foreach}

         </tbody>
      </table>
   </form>
</div>