<table width="100%">
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_LESSON_NUMBER} {$ss.lesson}:</b></td> <td><a target="_parent" href="{$uri}#bwc/index.php?module=J_LessonPlan&action=DetailView&record={$ss.lessonplan_id}&hl={$ss.syllabus_id}">{$ss.topic}</a></td></tr>
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_SYL_SYLLABUS}:</b></td> <td>{$ss.activities}</td></tr>
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_SYL_NOTE_FOR_TEACHER}:</b></td><td>{$ss.objective}</td></tr>
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_SYL_HOMEWORK}:</b></td> <td>{$ss.homework}</td></tr>
        <tr><td width="25%" valign="top"></td></tr>
        <tr><td colspan="2" width="100%" valign="top" align="right">
        <a class="button" target="_parent" href="{$uri}#bwc/index.php?module=J_Class&action=attendance&session_id={$ss.primaryid}"><i class="far fa-tasks"></i> {$MOD_M.LBL_CHECK_ATTENDANCE}</a>
        </td></tr>
</table>

