{dotb_getscript file="custom/modules/J_Class/js/changeAvatar.js"}
{if isset($smarty.request.isDuplicate) && $smarty.request.isDuplicate eq "true"}
    <input type="hidden" id="picture_duplicate" name="picture_duplicate" value="{$picture_value}"/>
{/if}
<div class="input-group mb-3">
    {if !empty($picture_value)}
        <div class="icon-image-choose"
             style="display: block;width: 40px;height: 40px;border: 2px solid #e6eaed;    border-radius: 6px;    margin: 10px;    position: relative;">
            <img src='index.php?entryPoint=download&id={$picture_value}&type=DotbFieldImage&isTempFile=1&isProfile=1'
                 data-return-image="{$name_avatar}"
                 id="img_picture"
                 name="img_picture"
                 data-src2="index.php?entryPoint=download&id={$picture_value}&type=DotbFieldImage&isTempFile=1&isProfile=1"
                 alt="" style="float: left;  width: 40px; height:40px;">
            <img id="bt_remove_picture"
                 name="bt_remvoe_picture"
                 alt=$APP.LBL_REMOVE
                 title="{$APP.LBL_REMOVE}"
                 src="{dotb_getimagepath file='delete_inline.gif'}"
                 onclick="remove_upload_imagefile('picture');"
                 style="position: absolute;top: -9px;left: 33px;"
            />
            <input id="remove_imagefile_picture" name="remove_imagefile_picture" type="hidden" value=""/>
        </div>
        <div class="input-group-prepend" style="display: none;">
            <input class="button" id="choose_avatar" type="button" value="{$MOD.LBL_UPLOAD_AVATAR}">
        </div>
        <input id="imagefile_picture" name="imagefile_picture" type="hidden" value=""/>
    {else}
        <div class="icon-image-choose"
             style="display: none;width: 40px;height: 40px;border: 2px solid #e6eaed;    border-radius: 6px;    margin: 10px;    position: relative;">
            <img src="" data-return-image="" data-src2="" alt="" style="float: left; width: 40px; height: 40px;"
                 id="img_picture" name="img_picture">
            <img
                    id="bt_remove_picture"
                    name="bt_remvoe_picture"
                    alt=$APP.LBL_REMOVE
                    title="{$APP.LBL_REMOVE}"
                    src="{dotb_getimagepath file='delete_inline.gif'}"
                    onclick="remove_upload_imagefile('picture');"
                    style="display: none;position: absolute;top: -9px;left: 33px;"
            />
            <input id="remove_imagefile_picture" name="remove_imagefile_picture" type="hidden" value=""/>
        </div>
        <div class="input-group-prepend">
            <input class="button" id="choose_avatar" type="button" value="{$MOD.LBL_UPLOAD_AVATAR}">
        </div>
        <input id="imagefile_picture" name="imagefile_picture" type="hidden" value=""/>
    {/if}

</div>
<div class="modal" id="imagemodal" style="display: none;" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex;align-items: flex-start;justify-content: space-between;">
                <h5 class="modal-title" style="margin-top: 3px;font-size: 21px;">{$MOD.LBL_CLASS_ICON}</h5>
                <button type="button" class="close" id="closemodal" data-dismiss="modal" aria-label="Close"
                        style="float: right;font-size: 30px;font-weight: 700;color: #000;text-shadow: 0 1px 0 #fff;opacity: .5;background-color: transparent;border: 0;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 3px;">
                <div class="list-class-icon">
                    {*Show image upload*}
                    <div class="icon-image-upload"
                         style="display: none !important;width: 42px;height: 42px;border: 2px solid #e6eaed;    border-radius: 6px;    margin: 10px;    position: relative;    display: inline-block;    display: inline-flex;    justify-content: center;align-items: center;">
                        <img src="" id="output" data-imageupload="" alt="" width="40" height="40">
                    </div>
                    {*List default image*}
                    {foreach from=$arraysrcimages key="key" item="item"}
                        <div class="icon-image"
                             style="width: 42px;height: 42px;border: 2px solid #e6eaed;    border-radius: 6px;    margin: 10px;    position: relative;    display: inline-block;    display: inline-flex;    justify-content: center;align-items: center;">
                            <img src="{$item}"
                                 data-nameimage="{$arraynameimages[$key]}"
                                 alt=""
                                 onclick="get_upload_imagefile('{$arraynameimages[$key]}');">
                        </div>
                    {/foreach}
                </div>
                <p>
                    <input type="file" id="picture" name="picture" title="" size="30" maxlength="255" value=""
                           tabindex="0" onchange="loadFile('picture')" style="display: none;">
                </p>
                <p style="border: 1px solid #e6eaed; border-radius: 6px; width: 86px; padding: 8px; position:relative; left:50%; transform:translateX(-50%);">
                    <label for="picture" style="cursor: pointer;">{$MOD.LBL_UPLOAD_AVATAR2}</label>
                </p>
            </div>
        </div>
    </div>
</div>
{*preview image*}
<div class="modal" id="imagemodal2" style="display: none;" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display: flex;align-items: flex-start;justify-content: space-between;">
                <h5 class="modal-title" style="margin-top: 3px;font-size: 21px;">Class Icon</h5>
                <button type="button" class="close" id="closemodal2" data-dismiss="modal" aria-label="Close"
                        style="float: right;font-size: 30px;font-weight: 700;color: #000;text-shadow: 0 1px 0 #fff;opacity: .5;background-color: transparent;border: 0;">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding: 3px;">
                {*preview image*}
                <div class="preview-upload" style="display: block;">
                    <img src="" alt="" id="img-preview"
                         style="max-width: 400px; max-height: 388px; display: block; margin:auto;">
                    <p id="save-photo"
                       style="margin-top: 12px;text-align: center;cursor: pointer;border: 1px solid #e6eaed; border-radius: 6px; width: 86px; padding: 8px; position:relative; left:50%; transform:translateX(-50%);">
                        Save Photo
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript'>
    function get_upload_imagefile(field_name) {ldelim}
        var field = document.getElementById('imagefile_picture');
        field.value = field_name;
        $('div.icon-image-choose img').attr("data-return-image", field_name);
        var field = document.getElementById('remove_imagefile_picture');
        field.value = 1;
        $('#picture').val('');
        {rdelim}

    function loadFile(field_name) {ldelim}
        var image = document.getElementById('output');
        $('.icon-image-upload').css("display", "inline-block");
        var urlImage = URL.createObjectURL(event.target.files[0]);
        image.src = urlImage;
        $('#imagemodal').css('display', 'none');
        //set lai
        $('div.icon-image-upload img').attr('data-imageupload', urlImage);

        var modalImg = document.getElementById("img-preview");
        modalImg.src = urlImage;
        $('#imagemodal2').css('display', 'block');

        var field = document.getElementById(field_name);
        var filename = field.value;
        var fileExtension = filename.substring(filename.lastIndexOf(".") + 1);
        fileExtension = fileExtension.toLowerCase();
        if (fileExtension == "jpg" || fileExtension == "jpeg"
            || fileExtension == "gif" || fileExtension == "png" || fileExtension == "bmp") {ldelim}
            //image file
            {rdelim} else {ldelim}
            field.value = null;
            alert("{$APP.LBL_UPLOAD_IMAGE_FILE_INVALID|strip}");
            $('#imagemodal2').css('display', 'none');
            $('#picture').val('');
            $('div.icon-image-upload img').prop('src', '');
            $('div.icon-image-upload img').attr("data-imageupload", "");

            $('#imagemodal2').css('display', 'none');
            $('#imagemodal').css('display', 'block');
            $('.icon-image-upload').css('display', 'none');

            var field = document.getElementById('remove_imagefile_picture');
            var urlSRCOrigin = $(".icon-image-choose img").attr("data-src2");
            $('div.icon-image-choose img').prop('src', urlSRCOrigin);
            $('div.icon-image-choose').css("display", "none");
            field.value = 1;
            var field = document.getElementById('imagefile_picture');
            field.value = '';
            $('div.icon-image-choose img').attr("data-return-image", "")
            {rdelim}
        {rdelim}

    function remove_upload_imagefile(field_name) {ldelim}
        var field = document.getElementById('remove_imagefile_' + field_name);
        field.value = 1;
        $('.icon-image-choose').css('display', 'none');
        $('.input-group-prepend').css('display', 'block');
        var field = document.getElementById('bt_remove_' + field_name);
        field.style.display = "none";

        {rdelim}
</script>