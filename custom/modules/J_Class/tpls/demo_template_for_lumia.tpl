{literal}
    <style type="text/css">
        div#diaglog_demo {
            border: #CCCCCC solid 2px;
            padding: 10px;
            background-color: #FFFFFF;
            text-align: left;
            transition: opacity .15s linear;
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 6px;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            white-space: normal;
        }

        td {
            padding-left: 10px;
        }
    </style>
{/literal}
<div id="diaglog_demo" title="{$MOD.LBL_ADD_DEMO}">
    <h3 class="" style="
    background: #fff;
    color: #555;
    border-bottom: 1px solid #d7d7d7;
    border-radius: 2px 2px 0 0;
    font-size: 13px;
    margin: 0;
    padding: 8px 10px;
    font-weight: normal;
    line-height: 18px;
    ">{$MOD.LBL_CLASS_DEMO}</h3>
    <table width="100%" class="edit view">
        <tbody>
        <input type="hidden" id="dm_type_student" value="{$dm_student_type}" autofocus="true">
        <input type="hidden" id="dm_student_id" value="{$dm_student_id}">
        <input type="hidden" name="json_sessions" id="json_sessions" value={$json_ss}>
        <tr>
            <td> {$MOD.LBL_STUDENT_NAME}:</td>
            <td style="color: blue;font-weight: bold" colspan="3" id="dm_student_name">{$dm_student_name}</td>
        </tr>
        <tr>
            <td>{$MOD.LBL_CLASS_CODE}:</td>
            <td id="dm_class_code" style="color: blue;font-weight: bold">{$model.class_code}</td>
            <td>{$MOD.LBL_START_DATE}:</td>
            <td id="dm_start_date">{$model.start_date}</td>
        </tr>
        <tr>
            <td>{$MOD.LBL_NAME}:</td>
            <td id="dm_class_name_demo" style="color: blue;font-weight: bold">{$model.name}</td>
            <td>{$MOD.LBL_END_DATE}:</td>
            <td id="dm_end_date">{$model.end_date}</td>
        </tr>
        <tr>
            <td>{$MOD.LBL_NEXT14_SCHEDULE}:</td>
            <td id="dm_schedule">{$SCHEDULE}</td>
        </tr>
        <tr>
            <td>{$MOD.LBL_FROM_DATE}:</td>

            <td>
                <div class="input-append date">
                    <div class="date-wrapper">
                        <input id="dm_lesson_date" type="text" tabindex="-1" data-type="date" name="dm_lesson_date" class="datepicker"
                               value="{$next_session_date}">
                        <span class="add-on" data-icon="calendar"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>{$MOD.LBL_TO_DATE}:</td>

            <td>
                <div class="input-append date">
                    <div class="date-wrapper">
                        <input id="dm_lesson_to_date" type="text" tabindex="-1" data-type="date" name="dm_lesson_to_date" class="datepicker"
                               value="{$class_end}">
                        <span class="add-on" data-icon="calendar"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <input class="btn btn-primary pull-right " type="button" id="btn_add_demo" value="Add"/>
            </td>
        </tr>
        </tbody>
    </table>
</div>