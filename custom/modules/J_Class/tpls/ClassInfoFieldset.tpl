<fieldset>
    <legend style="margin-bottom: 5px;"><table width="100%">
        <tr><td width="25%" style="font-size: 20px">{$MOD.LBL_CLASS_INFO}:</td> <td><a style="font-size: 20px!important;" target="_parent" href="{$uri}#J_Class/{$class_id}">{$class_name}</a></td></tr>
    </table></legend>
    <div>
    <table width="100%">
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_LESSON_NUMBER} {$current_lesson.lesson}:</b></td> <td><a target="_parent" href="{$uri}#bwc/index.php?module=J_LessonPlan&action=DetailView&record={$current_lesson.lessonplan_id}&hl={$current_lesson.syllabus_id}">{$current_lesson.topic}</a></td></tr>
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_SYL_SYLLABUS}:</b></td> <td><div style="max-height: 100px;overflow: auto;">{$current_lesson.activities}</div></td></tr>
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_SYL_NOTE_FOR_TEACHER}:</b></td> <td><div style="max-height: 100px;overflow: auto;">{$current_lesson.objective}</div></td></tr>
        <tr><td width="25%" valign="top"><b>{$MOD_M.LBL_SYL_HOMEWORK}:</b></td> <td><div style="max-height: 100px;overflow: auto;">{$current_lesson.homework}</div></td></tr>
    </table>
    </div>
    <div><label>---------------------------------------------------------------</label></div>
    <div>
    <table width="100%">
    <tr><td width="25%"><b>{$MOD.LBL_PREV_LESSON}: </b></td>
    <td><a style="cursor: pointer;" {if $prev_lesson.primaryid != ''}onclick="showHelp(this,$(this).next().val());"{/if}>{$prev_lesson.title}</a>
    <textarea style="display:none;">{$tips_prev}</textarea></td>
    </tr>
    <tr><td width="25%"><b>{$MOD.LBL_NEXT_LESSON}: </b></td>
    <td><a style="cursor: pointer;" {if $next_lesson.primaryid != ''}onclick="showHelp(this,$(this).next().val());"{/if}>{$next_lesson.title}</a>
    <textarea style="display:none;">{$tips_next}</textarea></td>
    </tr>
    </table>
    </div>
</fieldset>