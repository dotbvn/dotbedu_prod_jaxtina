<link rel="stylesheet" type="text/css" href="custom/modules/J_Class/tpls/css/teacher_schedule_screen.css">

<div id="dialog_teacher" title="{$MOD.LBL_SCHEDULE_TEACHER}" style="display:none; max-height: 500px; overflow: auto;">
    <table style="border: 1px solid #F0F0F0;width:100%;">
        <tr>
            <td width="15%" style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_SCHEDULE_FOR}: <span class="required">*</span></td>
            <td width="35%"><select id="sc_type" name="sc_type">{$SCHEDULE_FOR}</select></td>
            <td width="15%" style="text-align: right; font-weight: bold;padding-right: 10px;"></td>
            <td width="35%" style="color: blue; font-weight: bold"></td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_CLASS_CODE}: </td>
            <td id="class_code_schedule" name = "class_code" style="color: blue;font-weight: bold;width: 170px;height: 50px;">{$CLASS_CODE}</td>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_START_DATE}: </td>
            <td style="color: blue; font-weight: bold">{$fields.start_date.value}</td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_NAME}: </td>
            <td id="class_name_schedule" style="color: blue;font-weight: bold">{$NAME}</td>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_END_DATE}: </td>
            <td style="color: blue;font-weight: bold">{$fields.end_date.value}</td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_FROM_DATE}: <span class="required">*</span></td>
            <td>
                <span class="dateTime">
                    <input class="date_input" size="11" autocomplete="off" type="text"  maxlength="10" style="font-size: 1em" name="start_schedule" id="start_schedule"/>
                &nbsp<img src="themes/RacerX/images/jscalendar.png" alt="Enter Date" id="start_schedule_trigger" style="vertical-align: middle"> </span>
                {literal}
                <script type="text/javascript">
                    Calendar.setup ({
                    inputField : "start_schedule",
                    ifFormat : cal_date_format,
                    daFormat : cal_date_format,
                    button : "start_schedule_trigger",
                    singleClick : true,
                    dateStr : "",
                    startWeekday: 0,
                    step : 1,
                    weekNumbers:false
                    });
                </script>
                {/literal}
            </td>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_TO_DATE}: <span class="required">*</span></td>
            <td style="height: 50px;">
                <span class="dateTime">
                    <input class="date_input" size="11" autocomplete="off" type="text"  maxlength="10" style="font-size: 1em" name="end_schedule" id="end_schedule"/>
                &nbsp<img src="themes/RacerX/images/jscalendar.png" alt="Enter Date" id="end_schedule_trigger" style="vertical-align: middle"> </span>
                {literal}
                <script type="text/javascript">
                    Calendar.setup ({
                    inputField : "end_schedule",
                    ifFormat : cal_date_format,
                    daFormat : cal_date_format,
                    button : "end_schedule_trigger",
                    singleClick : true,
                    dateStr : "",
                    startWeekday: 0,
                    step : 1,
                    weekNumbers:false
                    });
                </script>
                {/literal}
                </span>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;height:50px;">{$MOD.LBL_DAY}: <span class="required">*</span></td>
            <td nowrap>
                <div style="margin-bottom: 5px;">
                <input style="vertical-align: middle; margin-right: 5px; display: none;" type=checkbox class="day_of_week" id='Monday' onclick='clearTeacherList();'/><label for="Monday" style="margin-right: 5px;width: 70px; display: none" id="lbl_mon">{$MOD.LBL_MONDAY}</label>
                </div>
                <div style="margin-bottom: 5px;">
                <input style="vertical-align: middle; margin-right: 5px; display: none;" type=checkbox class="day_of_week" id='Tuesday' onclick='clearTeacherList();'/><label for="Tuesday" style="margin-right: 5px;width: 70px; display: none" id="lbl_tue">{$MOD.LBL_TUESDAY}</label>
                </div>
                <div style="margin-bottom: 5px;">
                <input style="vertical-align: middle; margin-right: 5px; display: none;" type=checkbox class="day_of_week" id='Wednesday' onclick='clearTeacherList();'/><label for="Wednesday" style="margin-right: 5px;width: 70px; display: none" id="lbl_wed">{$MOD.LBL_WEDNESDAY}</label>
                </div>
                <div style="margin-bottom: 5px;">
                <input style="vertical-align: middle; margin-right: 5px; display: none;" type=checkbox class="day_of_week" id='Thursday' onclick='clearTeacherList();'/><label for="Thursday" style="margin-right: 5px;width: 70px; display: none" id="lbl_thu">{$MOD.LBL_THURSDAY}</label>
                </div>
                <div style="margin-bottom: 5px;">
                <input style="vertical-align: middle; margin-right: 5px; display: none;" type=checkbox class="day_of_week" id='Friday' onclick='clearTeacherList();'/><label for="Friday" style="margin-right: 5px;width: 70px; display: none" id="lbl_fri">{$MOD.LBL_FRIDAY}</label>
                </div>
                <div style="margin-bottom: 5px;">
                <input style="vertical-align: middle; margin-right: 5px; display: none;" type=checkbox class="day_of_week" id='Saturday' onclick='clearTeacherList();'/><label for="Saturday" style="margin-right: 5px;width: 70px; display: none" id="lbl_sat">{$MOD.LBL_SATURDAY}</label>
                </div>
                <div style="margin-bottom: 5px;">
                <input style="vertical-align: middle; margin-right: 5px; display: none;" type=checkbox class="day_of_week" id='Sunday' onclick='clearTeacherList();'/><label for="Sunday" style="margin-right: 5px;width: 70px; display: none" id="lbl_sun">{$MOD.LBL_SUNDAY}</label>
                </div>
            </td>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_MAIN_SCHEDULE}: </td>
            <td id="dlg_class_schedule" name = "dlg_class_schedule" style="width: 170px;height: 50px;"></td>
        </tr>
        <tr>
            <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_TEACHING_TYPE}: </td>
            <td style="width: 170px;height: 50px;">
                <select name = 'select_teaching_type' id = 'select_teaching_type' class="select_teaching_type">
                    {$teaching_type_options}
                </select>
            </td>
             <td style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_CHANGE_TEACHER_REASON}: <span style="display: none;" class="required change_reason_required">*</span> </td>
            <td style="width: 170px;height: 50px;">
                <textarea id="change_teacher_reason" name="change_teacher_reason" rows="2" cols="40"></textarea>
            </td>
        </tr>
        <tr style="display: none" class="more_teacher_action">
            <td></td>
            <td colspan =3 style="width: 170px;height: 50px;">
                <li >
                    <a id="busy_schedule" href="#">Show busy teachers list </a>
                </li>
                <li >
                    <a id="another_schedule" href="#">Show teachers list from another centers </a>
                </li>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center;">
                <input type="button" class="button" value="{$MOD.LBL_CHECK}" id="btn_check_schedule"></input>
                <input type="button" class="button" value="{$MOD.LBL_RESET}" id="btn_reset_input"></input>
            </td>
        </tr>
    </table>
    <div>
        <table id="list_teacher" class="list view" style="width: 100%;margin-top: 10px;">
            <thead>
                <tr class="list_teacher">
                    <th></th>
                    <th>{$MOD.LBL_TEACHER_NAME}</th>
                    <th>{$MOD.LBL_PHONE}</th>
                    <th>{$MOD.LBL_TEACHER_CONTRACT_TYPE}</th>
                    <th>{$MOD.LBL_TEACHER_REQUIRED_HOURS}</th>
                    <th>{$MOD.LBL_TEACHER_TAUGHT_HOURS}</th>
                    <th>{$MOD.LBL_TEACHER_EXPIRE_DAY}</th>
                    <th>{$MOD.LBL_KIND_OF_COURSE_TEACHING}</th>
                    <th>{$MOD.LBL_DAY_OFF}</th>
                    <th>{$MOD.LBL_NOTE}</th>
                    <th>{$MOD.LBL_HOLIDAYS}</th>
                </tr>
                <tr class="list_room" style="display: none">
                    <th></th>
                    <th>{$MOD.LBL_TEACHER_NAME}</th>
                    <th>{$MOD.LBL_CAPACITY}</th>
                    <th>{$MOD.LBL_NOTE}</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div id="list_teacher_busy" title="Busy Teachers List" style="display:none; max-height: 500px; overflow: auto;" >
        <table  class="list view" style="width: 100%;margin-top: 10px;">
            <thead>
            <tr>
                <th>Teacher Name</th>
                <th>Class Teaching</th>
                <th>{$MOD.LBL_SCHEDULE}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div id="list_teacher_available_another" title="Another Available Teachers List" style="display:none; max-height: 500px; overflow: auto;" >
        <table  class="list view" style="width: 100%;margin-top: 10px;">
            <thead>
            <tr>
                <th>{$MOD.LBL_TEAM}</th>
                <th width='30%'>{$MOD.LBL_TEACHER_NAME}</th>
                <th width='30%'>{$MOD.LBL_PHONE}</th>
                <th width='30%'>{$MOD.LBL_TEACHER_CONTRACT_TYPE}</th>
                <th width='30%'>{$MOD.LBL_TEACHER_REQUIRED_HOURS}</th>
                <th width='30%'>{$MOD.LBL_TEACHER_TAUGHT_HOURS}</th>
                <th width='30%'>{$MOD.LBL_TEACHER_EXPIRE_DAY}</th>
                <th width='30%'>{$MOD.LBL_KIND_OF_COURSE_TEACHING}</th>
                <th>{$MOD.LBL_DAY_OFF}</th>
                <th width='30%'>{$MOD.LBL_NOTE}</th>
                <th width='10%'>{$MOD.LBL_HOLIDAYS}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div style="text-align: center;">
        <input type="hidden" id="teacher_schedule_start_date"></input>
        <input type="hidden" id="teacher_schedule_end_date"></input>
        <input type="hidden" id="teacher_schedule_day_of_week"></input>
        <img id="teacher_schedule_loading_icon" src="themes/default/images/loading.gif" align="absmiddle" style="width:16;display:none;">
    </div>
</div>

