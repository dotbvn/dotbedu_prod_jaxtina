{dotb_getscript file="custom/modules/J_Class/js/generate_online.js"}
<div>
    <div id="generate_online" title="{$MOD_M.LBL_GENERATE_ONLINE_LEARNING}" style="display:none;">
        <div>
            <form id="GenerateOnline">
            <input type="hidden" value="" id="go_ssid">
                <table width="100%">
                    <tr>
                        <td width="15%" style="text-align: right; font-weight: bold; padding-right: 10px;">{$MOD.LBL_CLASS_CODE}: </td>
                        <td width="35%" style="color: blue;font-weight: bold"> {$fields.class_code.value}</td>
                        <td width="15%" style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_START_DATE}: </td>
                        <td width="35%"> {$fields.start_date.value}</td>
                    </tr>
                    <tr>
                        <td width="15%" style="text-align: right; font-weight: bold; height: 35px; padding-right: 10px;">{$MOD.LBL_NAME}: </td>
                        <td width="35%" style="color: blue;font-weight: bold"> {$fields.name.value}</td>
                        <td width="15%" style="text-align: right; font-weight: bold; padding-right: 10px;">{$MOD.LBL_END_DATE}: </td>
                        <td width="35%" > {$fields.end_date.value}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height: 0; padding:0"><hr style="padding:0; margin:0;"></td>
                    </tr>
                    <tr>
                        <td style="width:15%; text-align: right; font-weight: bold; height: 40px; padding-right: 10px;">{$MOD_M.LBL_START_DATETIME}: </td>
                        <td style="width:35%;"><label id="go_date_start" style="font-weight: bold; color: red;"></label></td>
                        <td style="width:15%; text-align: right; font-weight: bold; height: 40px; padding-right: 10px;">{$MOD_M.LBL_END_DATETIME}: </td>
                        <td style="width:35%;"><label id="go_date_end" style="font-weight: bold; color: red;"></label></td>
                    </tr>

                    <tr>
                        <td width="15%" style="text-align: right; font-weight: bold; height: 35px; padding-right: 10px;">{$MOD_M.LBL_TYPE}: </td>
                        <td width="35%"> {$go_learning_type}</td>
                        <td width="15%" style="text-align: right; font-weight: bold; padding-right: 10px;"></td>
                        <td width="35%"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
