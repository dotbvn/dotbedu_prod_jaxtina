<tr attend_id = "{$ATTENDANCE_ID}" pid="{$STUDENT_ID}" pname="{$STUDENT_NAME}" ptype="{$STUDENT_TYPE}" class="{$IN_CLASS_TYPE}">
    <td nowrap="" style="text-align: center;" >
        {$NO}
    </td>
    <td nowrap="" style="text-align: left;" ><b><a target="_parent" href="{$uri}#{$STUDENT_TYPE}/{$STUDENT_ID}" class="gs_link student_name">{$STUDENT_NAME}</a></b></td>
    <td nowrap="" style="text-align: left;" >
        <span>{$BIRTHDATE}</span>
    </td>
    <td nowrap="" style="text-align: left;" >
        <span>{$NICK_NAME}</span>
    </td>
    <td nowrap="">
        <span>{$SITUATION_TYPE}</span>
    </td>
    <td nowrap="">
        <span class='avg_attendance'>{$AVG_ATTENDANCE}</span>
    </td>
    <td class="send_att" nowrap="" style="text-align: center;" >
        {if !empty($ATTENDANCE_ID)}
        {if $lockEdit || $lock_attendance_type}
        {$attendance_value}
        {else}
        <select name="attendance_type" onchange="saveAttendance($(this));" class="attendance_type">{$attendance_options}</select>
        {/if}
        {/if}
    </td>

    <td class="send_att" nowrap="" style="text-align: center;" >
        {if !empty($ATTENDANCE_ID)}
            <p>{$time_online}</p>
        {/if}
    </td>
    <td class="send_att" nowrap="" style="text-align: center;" >
        {if !empty($ATTENDANCE_ID)}
            <p>{$number_of_right}</p>
        {/if}
    </td>
    <td class="send_att" nowrap="" style="text-align: center;" >
        {if !empty($ATTENDANCE_ID)}
            <p>{$number_of_wrong}</p>
        {/if}
    </td>
    <td class="send_att" nowrap="" style="text-align: center;" >
        {if !empty($ATTENDANCE_ID)}
            <p>{$total_score}</p>
        {/if}
    </td>
    <td class="send_att" nowrap="" style="text-align: center;" >
        {if !empty($ATTENDANCE_ID)}
            <p>{$right_percent}</p>
        {/if}
    </td>

    <td class="send_att">
        {if !empty($ATTENDANCE_ID)}
        {if $lockEdit}
        <span>{$homework_num}</span>
        {else}
        <input type="number" class="homework_num" style="width: 80%;text-align:right" onchange="saveAttendance($(this));" value="{$homework_num}">
        {/if}
        {/if}
    </td>
    <td class="send_att">
        {if !empty($ATTENDANCE_ID)}
        {if $lockEdit}
        <span>{$teacher_comment_num}</span>
        {else}
            <input type="number" class="teacher_comment_num" style="width: 22pt;text-align:right" onchange="saveAttendance($(this));" value="{$teacher_comment_num}">
        {/if}
        {/if}
    </td>
    <td class="send_att">
        {if !empty($ATTENDANCE_ID)}
        {if $lockEdit}
        <span>{$teacher_comment_num_per}</span>
        {else}
            <input class="teacher_comment_num_per" style="width: 22pt;text-align:center" value="{$teacher_comment_num_per}" disabled>
        {/if}
        {/if}
    </td>

    <td class="send_att">
        {if !empty($ATTENDANCE_ID)}
        {if $lockEdit || $lock_care_comment}
        <p title="{$care_comment}" style="overflow: hidden; width:150px; height:50px;">{$care_comment}</p>
        {else}
        <textarea class="care_comment" rows="2" cols="15" style="resize: vertical;vertical-align: middle;" onchange="saveAttendance($(this));">{$care_comment}</textarea>
        {/if}
        {/if}
    </td>

    <td class="send_att" nowrap="">
        {if !empty($ATTENDANCE_ID)}
            {if $lockEdit || $lock_note}
                <p title="{$description}" style="overflow: hidden; width:150px; height:50px;">{$description}</p>
            {else}
                <textarea class="description" rows="2" cols="15" style="resize: vertical;vertical-align: middle;" onchange="saveAttendance($(this));">{$description}</textarea>
            {/if}
        {/if}
    </td>
    <td class="send_att" nowrap="">
        {if !empty($ATTENDANCE_ID)}
        {if $lockEdit}
        <span>{$homework_score}</span>
        {else}
                <input name="homework_score" class="homework_score" type="text" value="{$homework_score}" size="4" maxlength="10" disabled>
        {/if}
        {/if}
    </td>
    <td class="send_att" nowrap="" >
        {if !empty($ATTENDANCE_ID)}
            <span class="total_loyalty_point">{$total_loyalty_point}</span>
        {/if}
    </td>
    <td class="send_att" nowrap="" >
        {if !empty($ATTENDANCE_ID)}
            {if $lockEdit || $lockEditLoyalty}
                <b>{$LOYALTY_POINT}</b>
            {else}
                <input name="loyalty_point" class="loyalty_point" type="text" value="{$LOYALTY_POINT}" onchange="saveAttendance($(this));" size="4" >
            {/if}
        {/if}
    </td>
</tr>
