<link rel="stylesheet" href="{dotb_getjspath file="custom/include/javascript/simptip.min.css"}"/>
<link rel="stylesheet" href="{dotb_getjspath file="custom/include/javascript/Select2/select2.css"}"/>
{dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}

{dotb_getscript file="custom/include/javascript/Timepicker/js/jquery.timepicker.min.js"}
{dotb_getscript file="custom/include/javascript/Timepicker/js/datepair.min.js"}
<link rel="stylesheet" href="{dotb_getjspath file="custom/include/javascript/Timepicker/css/jquery.timepicker.css"}"/>

{dotb_getscript file="custom/include/javascript/CustomDatePicker.js"}

<div>
    <div id="cancel_dialog" style="display:none;">
        <div>
            <form name = "CancelView" id = "CancelView" method="post" action="cancelSession">
            <input type="hidden" value="" id= "before_ss">
            <input type="hidden" value="" id= "after_ss">
            <input type="hidden" value="" id= "cancel_session_id">
                <table width="100%">
                    <tr>
                        <td width="15%" style="text-align: right; font-weight: bold; padding-right: 10px;">{$MOD.LBL_CLASS_CODE}: </td>
                        <td width="35%" style="color: blue;font-weight: bold"> {$fields.class_code.value}</td>
                        <td width="15%" style="text-align: right; font-weight: bold;padding-right: 10px;">{$MOD.LBL_START_DATE}: </td>
                        <td width="35%"> {$fields.start_date.value}</td>
                    </tr>
                    <tr>
                        <td width="15%" style="text-align: right; font-weight: bold; height: 35px; padding-right: 10px;">{$MOD.LBL_NAME}: </td>
                        <td width="35%" style="color: blue;font-weight: bold"> {$fields.name.value}</td>
                        <td width="15%" style="text-align: right; font-weight: bold; padding-right: 10px;">{$MOD.LBL_END_DATE}: </td>
                        <td width="35%" > {$fields.end_date.value}</td>
                    </tr>
                    <tr>
                        <td width="15%" style="text-align: right; font-weight: bold; height: 20px; padding-right: 10px;">{$MOD.LBL_MAIN_SCHEDULE}: </td>
                        <td width="35%"> {$SCHEDULE}</td>
                        <td width="15%" style="text-align: right; font-weight: bold; padding-right: 10px;">{$MOD.LBL_KIND_OF_COURSE}: </td>
                        <td width="35%" >{$KOC}</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height: 0; padding:0"><hr style="padding:0; margin:0;"></td>
                    </tr>
                    <tr>
                        <td style="width:20%; text-align: right; font-weight: bold; height: 40px;">{$MOD.LBL_CANCEL_SESSION_DATE}:</td>
                        <td colspan="3" style="padding-left: 10px;"><label id="cs_cancel_date" style="font-weight: bold; color: red;"></label>: <label id="cs_cancel_time" style="font-weight: bold; color: red;"></label></td>
                    </tr>
                    <tr>
                        <td style="width:15%; text-align: right; font-weight: bold; height: 40px;">{$MOD_M.LBL_CANCEL_BY}:<span class="required"> *</span></td></td>
                        <td style="width:35%; padding-left: 10px;">
                            <select style="width: 100%" id='cs_cancel_by' name='cs_cancel_by'>
                                {$cancel_reason_list}
                            </select>

                        </td>
                        <td style="width:15%; text-align: right; font-weight: bold; display:none;">{$MOD_M.LBL_CANCEL_REASON}:</td>
                        <td style="width:35%; padding-left: 10px; display:none;"><textarea rows="3" style="resize: none;" id="cs_cancel_reason" name="cs_cancel_reason"></textarea></td>
                    </tr>
                    </tr>
                    <tr>
                        <td style="width:20%; text-align: right; font-weight: bold;vertical-align: top;height: 40px;"></td>
                        <td style="padding-left: 10px;" colspan="3">
                            <label style="vertical-align: inherit;"><input type="radio" name="cs_makeup_type" id="cs_this_schedule" value="this_schedule" checked > {$MOD.LBL_MAKE_UP_THIS_SCHEDULE}</label>
                            <br>
                            <label style="vertical-align: inherit;"><input type="radio" name="cs_makeup_type" id="cs_other_schedule" value="other_schedule"> {$MOD.LBL_MAKE_UP_OTHER_SCHEDULE}</label>
                            <br>
                        </td>
                    </tr>
                </table>
                <div>
                    <fieldset>
                        <table style="width:100%">
                            <tr>
                                <td style="width:20%; text-align: right; font-weight: bold;height: 40px;">{$MOD.LBL_MAKE_UP_DATE}:<span class="required">*</span></td>
                                <td style="width:30%;padding-left: 10px;">
                                    <span class="dateTime">
                                        <input class="date_input datePicker" value="" oldval = "" size="11" readonly type="text" name="cs_date_makeup" id="cs_date_makeup" title="{$MOD.LBL_MAKE_UP_DATE}" maxlength="10" style="vertical-align: top;">
                                    </span>

                                </td>
                                <td style="width:50%; height: 40px;" colspan="2">
                                    <span class="timeOnly">
                                        <input class="time start" value="" oldval="" type="text" style="width: 70px; text-align: center;" readonly name = 'cs_start'>
                                        {$MOD.LBL_TO}
                                        <input class="time end input_readonly" value="" oldval="" type="text" style="width: 70px; text-align: center;" readonly name = 'cs_end'>
                                    </span>
                                    <input type="hidden" value="" oldval="" name='cs_duration' id='cs_duration' class="duration_hour input_readonly" readonly style="width: 70px; text-align: center;">
                                    <input type="button" value="{$MOD.LBL_BTN_CHECK_C}" class="btn_check_cancel_session" style="margin-left: 50px;"></input>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"></td>
                            </tr>
                            <tr>
                                <td style="width:20%; text-align: right; font-weight: bold;height: 40px;">{$MOD.LBL_TEACHER_COVER}: </td>
                                <td style="width:30%;padding-left: 10px;">
                                    <select style="width: 100%" name = 'cs_teacher' id='cs_teacher'>
                                        <option value = ''> {$MOD.LBL_NONE}</option>
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="width:20%; text-align: right; font-weight: bold;height: 40px;">{$MOD.LBL_TEACHER_TYPE}:</td>
                                <td style="width:30%;padding-left: 10px;">
                                    <select style="width: 100%" name='cs_teaching_type' id='cs_teaching_type'>
                                        {$teaching_type_options}
                                    </select>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>
