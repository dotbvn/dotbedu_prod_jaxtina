<?php

$module_name = 'J_Class';
$viewdefs[$module_name]['portal']['menu']['quickcreate'] = array(
    'layout' => 'create',
    'label' => 'LNK_NEW_RECORD',
    'visible' => false,
    'icon' => 'fa-plus',
    'order' => 1,
);
