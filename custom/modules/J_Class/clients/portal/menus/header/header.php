<?php

$moduleName = 'J_Class';
$viewdefs[$moduleName]['portal']['menu']['header'] = array(
    array(
        'route'=>'#layout/calendar',
        'label' =>'LNK_VIEW_CALENDAR',
        'acl_action'=>'list',
        'acl_module'=>'Calendar',
        'icon' => 'fa-calendar-alt',
    ),
);
