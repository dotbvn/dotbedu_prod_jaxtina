

(function(app) {
    app.events.on('router:init', function(router) {
        var module = 'J_Class';
        var routes = [
            {
                route: 'J_Class/attendance_screen',
                name: 'attendance_screen',
                callback: function(){
                     window.location.replace('index.php?entryPoint=Monitor&dotb_body_only=true');
                }
            },
        ];

        app.router.addRoutes(routes);
    });
})(DOTB.App);
