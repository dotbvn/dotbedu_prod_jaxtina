<?php
$module_name = 'J_Class';
$viewdefs[$module_name] =
array (
  'portal' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                0 =>
                    array (
                        'name' => 'class_code',
                        'label' => 'LBL_CLASS_CODE',
                        'enabled' => true,
                        'default' => true,
                    ),
                1 =>
                    array (
                        'name' => 'name',
                        'label' => 'LBL_NAME',
                        'enabled' => true,
                        'link' => true,
                        'default' => true,
                        'width' => 'xlarge',
                    ),
                2 =>
                    array (
                        'name' => 'main_teachers',
                        'label' => 'LBL_TEACHERS',
                        'enabled' => true,
                        'default' => true,
                        'type' => 'html',
                        'width' => 'large',
                    ),
                3 =>
                    array (
                        'name' => 'short_schedule',
                        'label' => 'LBL_CURRENT_SCHEDULE',
                        'enabled' => true,
                        'default' => true,
                        'type' => 'html',
                        'width' => 'large',
                    ),
                4 =>
                    array (
                        'name' => 'koc_name',
                        'label' => 'LBL_KOC_NAME',
                        'enabled' => true,
                        'id' => 'KOC_ID',
                        'link' => true,
                        'sortable' => false,
                        'default' => true,
                    ),
                5 =>
                    array (
                        'name' => 'status',
                        'label' => 'LBL_STATUS',
                        'enabled' => true,
                        'default' => true,
                        'type' => 'html',
                    ),
                6 =>
                    array (
                        'name' => 'start_date',
                        'label' => 'LBL_START_DATE',
                        'enabled' => true,
                        'default' => true,
                    ),
                7 =>
                    array (
                        'name' => 'end_date',
                        'label' => 'LBL_END_DATE',
                        'enabled' => true,
                        'default' => true,
                    ),

            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'start_date',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
