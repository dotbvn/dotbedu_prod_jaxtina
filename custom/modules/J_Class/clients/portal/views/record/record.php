<?php

$viewdefs['J_Class']['portal']['view']['record'] = array(
    'panels' =>
        array(
            0 =>
                array (
                    'name' => 'panel_header',
                    'label' => 'LBL_RECORD_HEADER',
                    'header' => true,
                    'fields' => array (
                        0 =>
                            array (
                                'name' => 'picture',
                                'type' => 'avatar',
                                'width' => 42,
                                'height' => 42,
                                'dismiss_label' => true,
                                'readonly' => true,
                            ),
                        1 => 'name',
                        2 =>
                            array (
                                'name' => 'favorite',
                                'label' => 'LBL_FAVORITE',
                                'type' => 'favorite',
                                'readonly' => true,
                                'dismiss_label' => true,
                            ),
                        3 =>
                            array (
                                'name' => 'follow',
                                'label' => 'LBL_FOLLOW',
                                'type' => 'follow',
                                'readonly' => true,
                                'dismiss_label' => true,
                            ),
                        4 => array(
                            'name' => 'status',
                            'comment' => 'Status of the class',
                            'label' => 'LBL_STATUS',
                            'type' => 'event-status',
                            'enum_width' => 'auto',
                            'dropdown_width' => 'auto',
                            'dropdown_class' => 'select2-menu-only',
                            'container_class' => 'select2-menu-only',
                        ),
                    ),
                ),
            1 =>
                array(
                    'name' => 'panel_body',
                    'label' => 'LBL_RECORD_BODY',
                    'columns' => 3,
                    'labelsOnTop' => true,
                    'placeholders' => true,
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                    'fields' =>
                        array(
                                //Row 0
                                0 => 'class_code',
                                1 => array (
                                    'name' => 'main_teachers',
                                    'label' => 'LBL_MAIN_TEACHER_NAME',
                                    'enabled' => true,
                                    'default' => true,
                                    'type' => 'html',
                                    'width' => 'large',
                                ),
                                2 => 'status',

                                // Row 1
                                3 => 'name',
                                4 => array (
                                    'name' => 'short_schedule',
                                    'label' => 'LBL_MAIN_SCHEDULE',
                                    'enabled' => true,
                                    'default' => true,
                                    'type' => 'html',
                                    'width' => 'large',
                                ),
                                5 => array (
                                    'name' => 'syllabus_by',
                                    'label' => 'LBL_SCHEDULE_BY',
                                ),
                                // Row 2

                                6 => 'koc_name',
                                7 => 'start_date',
                                8 => 'start_lesson',

                                // Row 3
                                9 => '',
                                10 => 'end_date',
                                11 => '',
                        ),
                ),
        ),
);
