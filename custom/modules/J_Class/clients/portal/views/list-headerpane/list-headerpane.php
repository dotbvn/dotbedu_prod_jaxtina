<?php


$viewdefs['J_Class']['portal']['view']['list-headerpane'] = array(

    //    'title' => 'LBL_J_Class',
    'buttons' => array(

        array(
            'label' => 'LNK_VIEW_CALENDAR',
            'tooltip' => 'LNK_VIEW_CALENDAR',
            'acl_action'=>'list',
            'type' => 'button',
            'acl_module'=>'Calendar',
             'route'=>'#layout/calendar',
            'icon' => 'fa-calendar-alt',
        ),
        array(
            'label' => 'LNK_J_CLASS_REPORTS',
            'tooltip' => 'LNK_J_CLASS_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_Class',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);