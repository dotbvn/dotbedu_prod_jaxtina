<?php


$viewdefs['J_Class']['base']['view']['list-headerpane'] = array(

    //    'title' => 'LBL_J_Class',
    'buttons' => array(
        array(
            'label' => 'LNK_NEW_CLASS',
            'tooltip' => 'LNK_NEW_CLASS',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_Class',
            'route'=>'#J_Class/create',
            'icon' => 'fa-plus',
        ),
        array(
            'label' => 'LNK_VIEW_CALENDAR',
            'tooltip' => 'LNK_VIEW_CALENDAR',
            'acl_action'=>'list',
            'type' => 'button',
            'acl_module'=>'Calendar',
             'route'=>'#bwc/index.php?module=Calendar&action=index&view=week',
            'icon' => 'fa-calendar-alt',
        ),
        array(
            'label' => 'LNK_J_CLASS_REPORTS',
            'tooltip' => 'LNK_J_CLASS_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_Class',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);