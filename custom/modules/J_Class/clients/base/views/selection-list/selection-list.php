<?php

$viewdefs['J_Class']['base']['view']['selection-list'] = array(
    'panels' => array(
        array(
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                0 =>
                array (
                    'name' => 'class_code',
                    'label' => 'LBL_CLASS_CODE',
                    'enabled' => true,
                    'default' => true,
                    'type' => 'html',
                    'width' => 'large',
                ),
                1 =>
                array (
                    'name' => 'name',
                    'label' => 'LBL_NAME',
                    'enabled' => true,
                    'link' => true,
                    'default' => true,
                ),
                3 =>
                array (
                    'name' => 'period',
                    'label' => 'LBL_PERIOD',
                    'enabled' => true,
                    'default' => true,
                    'type' => 'html'
                ),
                4 =>
                array (
                    'name' => 'status',
                    'label' => 'LBL_STATUS',
                    'enabled' => true,
                    'default' => true,
                    'type' => 'html'
                ),
                5 =>
                array (
                    'name' => 'main_teachers',
                    'label' => 'LBL_TEACHERS',
                    'enabled' => true,
                    'default' => true,
                    'type' => 'html',
                    'width' => 'large',
                ),
                6 =>
                array (
                    'name' => 'number_of_student',
                    'label' => 'LBL_NUMBER_OF_STUDENT',
                    'enabled' => true,
                    'default' => true,
                ),
                7 =>
                array (
                    'name' => 'start_date',
                    'label' => 'LBL_START_DATE',
                    'enabled' => true,
                    'default' => true,
                ),
                8 =>
                array (
                    'name' => 'end_date',
                    'label' => 'LBL_END_DATE',
                    'enabled' => true,
                    'default' => true,
                ),
                9 =>
                array (
                    'name' => 'koc_name',
                    'label' => 'LBL_KOC_NAME',
                    'enabled' => true,
                    'id' => 'KOC_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                10 =>
                array (
                    'name' => 'team_name',
                    'label' => 'LBL_TEAM',
                    'sortable' => false,
                    'default' => true,
                    'enabled' => true,
                ),
            ),
        ),
    ),
);
