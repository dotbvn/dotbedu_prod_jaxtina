<?php
// created: 2019-07-11 21:27:25
$viewdefs['J_Class']['base']['view']['subpanel-for-c_news-c_news_j_class_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'class_code',
          'label' => 'LBL_CLASS_CODE',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        2 => 
        array (
          'name' => 'teacher_name',
          'label' => 'LBL_MAIN_TEACHER_NAME',
          'enabled' => true,
          'id' => 'TEACHER_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'period',
          'label' => 'LBL_PERIOD',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'kind_of_course',
          'label' => 'LBL_KIND_OF_COURSE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'level',
          'label' => 'LBL_LEVEL',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAMS',
          'enabled' => true,
          'id' => 'TEAM_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);