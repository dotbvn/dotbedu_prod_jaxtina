<?php
$module_name = 'J_Class';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_DEFAULT',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'class_code',
                                'label' => 'LBL_CLASS_CODE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'name',
                                'label' => 'LBL_NAME',
                                'enabled' => true,
                                'link' => true,
                                'default' => true,
                                'width' => 'xlarge',
                            ),
                            2 =>
                            array (
                                'name' => 'main_teachers',
                                'label' => 'LBL_TEACHERS',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                                'width' => 'large',
                            ),
                            3 =>
                            array (
                                'name' => 'j_class_j_class_1_name',
                                'label' => 'LBL_UPGRADE',
                                'enabled' => true,
                                'id' => 'J_CLASS_J_CLASS_1J_CLASS_IDA',
                                'link' => true,
                                'sortable' => false,
                                'default' => true,
                            ),
                            4 =>
                            array (
                                'name' => 'short_schedule',
                                'label' => 'LBL_CURRENT_SCHEDULE',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                                'width' => 'large',
                            ),
                            5 =>
                            array (
                                'name' => 'koc_name',
                                'label' => 'LBL_KOC_NAME',
                                'enabled' => true,
                                'id' => 'KOC_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => true,
                            ),
                            6 =>
                            array (
                                'name' => 'level',
                                'label' => 'LBL_LEVEL',
                                'enabled' => true,
                                'default' => true,
                                'width' => 'small',
                            ),
                            7 =>
                            array (
                                'name' => 'status',
                                'label' => 'LBL_STATUS',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            8 =>
                            array (
                                'name' => 'hours',
                                'label' => 'LBL_HOURS',
                                'enabled' => true,
                                'default' => true,
                                'width' => 'small',
                            ),
                            9 =>
                            array (
                                'name' => 'period',
                                'label' => 'LBL_PERIOD',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                                'width' => 'small',
                            ),
                            10 =>
                            array (
                                'name' => 'number_student',
                                'label' => 'LBL_NUMBER_OF_STUDENT',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                                'width' => 'small',
                            ),
                            11 =>
                            array (
                                'name' => 'start_date',
                                'label' => 'LBL_START_DATE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            12 =>
                            array (
                                'name' => 'end_date',
                                'label' => 'LBL_END_DATE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            13 =>
                            array (
                                'name' => 'assigned_user_name',
                                'enabled' => true,
                                'default' => true,
                            ),
                            14 =>
                            array (
                                'name' => 'team_name',
                                'label' => 'LBL_TEAM',
                                'sortable' => false,
                                'default' => true,
                                'enabled' => true,
                            ),
                            15 =>
                            array (
                                'name' => 'date_entered',
                                'enabled' => true,
                                'default' => true,
                            ),
                            16 =>
                            array (
                                'name' => 'teacher_id',
                                'label' => 'LBL_MAIN_TEACHER_NAME',
                                'enabled' => true,
                                'default' => false,
                            ),
                            18 =>
                            array (
                                'name' => 'date_modified',
                                'enabled' => true,
                                'default' => false,
                            ),
                            19 =>
                            array (
                                'name' => 'cso_name',
                                'enabled' => true,
                                'default' => false,
                            ),
                            20 =>
                            array (
                                'name' => 'main_schedule',
                                'enabled' => true,
                                'default' => false,
                            ),
                            21 =>
                            array (
                                'name' => 'number_of_student',
                                'enabled' => true,
                                'default' => false,
                            ),
                            22 =>
                            array (
                                'name' => 'max_size',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
                'orderBy' =>
                array (
                    'field' => 'date_entered',
                    'direction' => 'desc',
                ),
            ),
        ),
    ),
);
