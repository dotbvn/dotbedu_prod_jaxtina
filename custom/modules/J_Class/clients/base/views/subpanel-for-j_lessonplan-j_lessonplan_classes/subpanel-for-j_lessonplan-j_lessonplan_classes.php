<?php
// created: 2023-08-31 17:09:32
$viewdefs['J_Class']['base']['view']['subpanel-for-j_lessonplan-j_lessonplan_classes'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'name' => 'class_code',
          'label' => 'LBL_CLASS_CODE',
          'enabled' => true,
          'default' => true,
        ),
        1 =>
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        2 =>
        array (
          'name' => 'main_teachers',
          'label' => 'LBL_TEACHERS',
          'enabled' => true,
          'default' => true,
        ),
        3 =>
        array (
          'name' => 'j_class_j_class_1_name',
          'label' => 'LBL_J_CLASS_J_CLASS_1_FROM_J_CLASS_L_TITLE',
          'enabled' => true,
          'id' => 'J_CLASS_J_CLASS_1J_CLASS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 =>
        array (
          'name' => 'short_schedule',
          'label' => 'LBL_SHORT_SCHEDULE',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 =>
        array (
          'name' => 'koc_name',
          'label' => 'LBL_KOC_NAME',
          'enabled' => true,
          'id' => 'KOC_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 =>
        array (
          'name' => 'level',
          'label' => 'LBL_LEVEL',
          'enabled' => true,
          'default' => true,
        ),
        7 =>
        array (
          'name' => 'start_date',
          'label' => 'LBL_START_DATE',
          'enabled' => true,
          'default' => true,
        ),
        8 =>
        array (
          'name' => 'end_date',
          'label' => 'LBL_END_DATE',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' =>
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);