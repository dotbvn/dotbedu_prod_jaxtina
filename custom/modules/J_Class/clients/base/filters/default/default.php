<?php
// created: 2019-05-17 01:59:00
$viewdefs['J_Class']['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' =>
    array(
        'class_code' =>
        array(),
        'name' =>
        array(),
        'period' =>
        array(),
        'koc_name' =>
        array(),
        'level' =>
        array(),
        'hours' =>
        array(),
        'status' =>
        array(),
        'number_of_student' =>
        array(),
        'max_size' =>
        array(),
        'modules' =>
        array(),
        'start_date' =>
        array(),
        'end_date' =>
        array(),
        'room_name' =>
        array(),
        'kind_of_course' => array(),
        'teacher_name' => array(),
        'isupgrade' => array(),
        'is_skip_holiday' => array(),
        'holiday_name' => array(),
        'change_by' => array(),
        'change_reason' => array(),
        'lms_course_id' => array(),
        'lms_template_id' => array(),
        'team_name' => array(),
        '$owner' =>
        array(
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' =>
        array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'modified_by_name' =>
        array(),
        'cso_name' =>
        array(),
        'created_by_name' =>
        array(),
        'date_modified' =>
        array(),
        'date_entered' =>
        array(),
        'assigned_user_name' => array(),
        'onl_status' => array(),


        'met_lesson_number' => array(),
        'met_syllabus_topic' => array(),
        'met_syllabus_type' => array(),
        'met_date_start' => array(),
        'met_attendance_status' => array(),
    ),
);
