<?php


$viewdefs['J_Class']['base']['filter']['basic'] = array(
    'create'               => true,
    'quicksearch_field'    => array('class_code','name'),
    'quicksearch_priority' => 1,
    'quicksearch_split_terms' => false,
    'filters'              => array(
        array(
            'id'                => 'all_records',
            'name'              => 'LBL_LISTVIEW_FILTER_ALL',
            'filter_definition' => array(),
            'editable'          => false
        ),
        array(
            'id'                => 'assigned_to_me',
            'name'              => 'LBL_ASSIGNED_TO_ME',
            'filter_definition' => array(
                '$owner' => '',
            ),
            'editable'          => false
        ),
        array(
            'id'                => 'favorites',
            'name'              => 'LBL_FAVORITES',
            'filter_definition' => array(
                '$favorite' => '',
            ),
            'editable'          => false
        ),
        array(
            'id'                => 'recently_viewed',
            'name'              => 'LBL_RECENTLY_VIEWED',
            'filter_definition' => array(
                '$tracker' => '-7 DAY',
            ),
            'editable'          => false
        ),
        array(
            'id'                => 'recently_created',
            'name'              => 'LBL_NEW_RECORDS',
            'filter_definition' => array(
                'date_entered' => array(
                    '$dateRange' => 'last_7_days',
                ),
            ),
            'editable'          => false
        ),
        array(
            'id' => 'planning_inprogress',
            'name' => 'List Planning/In Progress',
            'filter_definition' => array(array('status' => array('$in' => array('Planning', 'In Progress')))),
            'is_template' => true,
            'orderBy' => array(
                'field' => 'start_date',
                'direction' => 'asc'
            ),
        ),
        array(
            'id' => 'finished_closed',
            'name' => 'List Finished/Closed',
            'filter_definition' => array(array('status' => array('$in' => array('Finished', 'Closed')))),
            'is_template' => true,
            'orderBy' => array(
                'field' => 'start_date',
                'direction' => 'asc'
            ),
        ),
    ),
);
