({
    extendsFrom: 'BaseRelateField',

    initialize: function (options) {

        this._super('initialize', [options]);
    },
    setValue: async function (models) {
        if (!models) {
            return;
        }
        if (this.name === 'holiday_name' && models.length > 1) {
            var arr = [];
            for (let i = 0; i < models.length; i++) {
                arr.push(models[i].id);
            }
            models = await new Promise((resolve, reject) => {
                // this.getHoliday(arr, models).then((data) => {
                //     resolve(data);
                // })
                let holiday = app.data.createBean('Holidays');
                holiday.fetch({
                    fields: ['holiday_date'],
                    params: {
                        filter: [
                            {'id': {'$in': arr}}
                        ],
                    },
                    success: function (model) {
                        for (let i = 0; i < model.attributes.records.length; i++) {
                            models[i].holiday_date = model.attributes.records[i].holiday_date;
                            models[i].value = model.attributes.records[i].holiday_date;
                        }
                        resolve(models);
                    }
                })
            })
        }
        var isErased = false;
        var updateRelatedFields = true,
            values = {};
        if (_.isArray(models)) {
            // Does not make sense to update related fields if we selected
            // multiple models
            updateRelatedFields = false;
        } else {
            models = [models];
        }

        values[this.def.id_name] = [];
        values[this.def.name] = [];
        if (this.fieldDefs.link) {
            values[this.fieldDefs.link] = [];
        }

        _.each(models, _.bind(function (model) {
            values[this.def.id_name].push(model.id);
            //FIXME SC-4196 will fix the fallback to `formatNameLocale` for person type models.
            values[this.def.name].push(model[this.getRelatedModuleField()] ||
                app.utils.formatNameLocale(model) || model.value);
            if (this.fieldDefs.link) {
                values[this.fieldDefs.link].push(model);
            } else {
                isErased = app.utils.isNameErased(app.data.createBean(model._module, model));
            }
        }, this));

        // If it's not a multiselect relate, we get rid of the array.
        if (!this.def.isMultiSelect) {
            values[this.def.id_name] = values[this.def.id_name][0];
            values[this.def.name] = values[this.def.name][0];
            if (this.fieldDefs.link) {
                values[this.fieldDefs.link] = values[this.fieldDefs.link][0];
            } else {
                this._nameIsErased = isErased;
            }
        }

        //In case of selecting an erased value twice, we need to force a re-render to show the erased placeolder.
        var forceUpdate = _.isEmpty(this.model.get(this.def.name)) && _.isEmpty(values[this.def.name]);

        this.model.set(values);

        if (updateRelatedFields) {
            //Force an update to the link field as well so that DotbLogic and other listeners can react
            if (this.fieldDefs.link && _.isEmpty(values[this.fieldDefs.link]) && forceUpdate) {
                this.model.trigger('change:' + this.fieldDefs.link);
            }
            this.updateRelatedFields(models[0]);
        }

        if (forceUpdate) {
            this._updateField();
        }
    }
})
