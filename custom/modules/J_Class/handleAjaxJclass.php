<?php
require_once('custom/include/_helper/junior_class_utils.php');
require_once('custom/include/_helper/junior_schedule.php');
require_once("custom/include/_helper/junior_revenue_utils.php");
require_once("custom/include/lms/alovip-ggc/utils.php");
require_once('include/externalAPI/ClassIn/utils.php');

switch ($_POST['type']) {
    case 'ajaxMakeJsonSession':
        $result = ajaxMakeJsonSession($_POST['class_id'], $_POST['change_date_from'], $_POST['change_date_to'], $_POST['class_case'], $_POST['start_date'], $_POST['schedule'], $_POST['total_hours'], $_POST['is_skip_holiday'], $_POST['koc_id'], $_POST['level'], $_POST['start_lesson'], $_POST['syllabus_by']);
        if ($result['count_sessions']>0)
            echo json_encode(
                array(
                    "success" => 1,
                    "content" => json_encode($result),
                )
            );
        else
            echo json_encode(
                array(
                    "success" => 0,
                )
            );
        break;
    case 'addDemo':
        echo addDemo($_POST['dm_student_id'], $_POST['dm_type_student'], $_POST['dm_lesson_date'], $_POST['dm_lesson_to_date'], $_POST['dm_class_id']);
        break;
    case 'caculateFreeBalance':
        $result = caculateFreeBalance($_POST['class_student_id'], $_POST['dl_from_date'], $_POST['dl_to_date']);
        echo $result;
        break;
    case 'handleCreateDelay':
        $result = handleCreateDelay($_POST['class_student_id'], $_POST['dl_from_date'], $_POST['dl_to_date'], $_POST['dl_date'] , $_POST['dl_reason'], $_POST['dl_remove_from_class'], $_POST['dl_case'], $_POST['dl_reason_for']);
        echo $result;
        break;
    case 'fixIssueSituation':
        $result = fixIssueSituation($_POST['class_id']);
        echo $result;
        break;
    case 'ajaxGetWeekDay':
        $result = ajaxGetWeekDay($_POST['from_date'], $_POST['to_date'], $_POST['class_id']);
        echo $result;
        break;
    case 'ajaxGetTeacherBySchedule':
        $result = ajaxGetTeacherBySchedule($_POST['class_id'], $_POST['from_date'], $_POST['to_date'], $_POST['day_of_week'], $_POST['sc_type'], $_POST['available'], $_POST['another']);
        echo $result;
        break;
    case 'ajaxSaveTeacherSchedule':
        $result = ajaxSaveTeacherSchedule($_POST['scheduleForId'], $_POST['class_id'], $_POST['from_date'], $_POST['to_date'], $_POST['day_of_week'], $_POST['sc_type']);
        echo $result;
        break;
    case 'ajaxGetStudentList'://Attendance
        $result = ajaxGetStudentList($_POST['class_id'], $_POST['date_in_content'], $_POST['ss_id']);
        echo $result;
        break;
        break;
    case 'ajaxsendMgsApp':
        $result = ajaxsendMgsApp($_POST);
        echo $result;
        break;
    case 'ajaxSaveAttendance':
        $result = saveAttendance($_POST);
        echo $result;
        break;
    case 'ajaxSaveAllAttendance':
        $result = saveAllAttendance($_POST);
        echo $result;
        break;
    case 'ajaxLoadClassInfo':
        $result = ajaxLoadClassInfo($_POST['class_id'], $_POST['session_id']);
        echo $result;
        break;
    case 'ajaxSaveSessionDescription':
        $result = ajaxSaveSessionDescription($_POST['session_id'], $_POST['description']);
        echo $result;
        break;
    case 'getDataForCancelSession':
        echo getDataForCancelSession($_POST['session_id']);
        break;
    case 'cancelSession' : // add by Lap Nguyen
        $data = $_REQUEST;
        echo cancelSession($data);
        break;
    case 'getTeacherandRoom' :
        echo getTeacherAndRoom($_POST['date'], $_POST['start'], $_POST['end'], $_POST['class_id']);
        break;
    case 'deleteSession':
        echo deleteSession($_POST);
        break;
    case 'saveStsDescription':
        echo saveStsDescription($_POST);
        break;
    case 'saveSyllabus':
        $result = saveSyllabus();
        echo $result;
        break;
    case 'ajaxSaveSyllabus':
        $result = customSaveSyllabus();
        echo $result;
        break;
    case 'ajaxLoadStudents':
        $result = ajaxLoadStudents($_POST['selected_student'], $_POST['prv_class_id'], $_POST['prv_ost_load'], $_POST['cur_class_id'], $_POST['start'], $_POST['end'], $_POST['pm_selected']);
        echo $result;
        break;
    case 'ajaxAddStudentToClass':
        $result = ajaxAddStudentToClass($_POST['cur_class_id'], $_POST['settle_date'], $_POST['students_selected'], $_POST['is_allow_ost']);
        echo $result;
        break;
    case 'generate_online':
        $result = generate_online($_POST['go_ssid'], $_POST['class_id'], $_POST['go_learning_type']);
        echo $result;
        break;
    case 'cancel_online':
        $result = generate_online($_POST['go_ssid'], $_POST['class_id']);
        echo $result;
        break;
    case 'send_bmes':
        $result = send_bmes($_POST['parent_id'], $_POST['parent_type'], $_POST['action_type'], $_POST['send_to'], $_POST['title'], $_POST['description'], $_POST['template_id']);
        echo $result;
        break;
    case 'load_bmes_template':
        $result = load_bmes_template($_POST['parent_id'], $_POST['parent_type']);
        echo $result;
        break;
    case 'ajax_lms_sync':
        $result = ajax_lms_sync($_POST['class_id']);
        echo $result;
        break;
    case 'restoreAttendance':
        $result = restoreAttendance($_POST['class_id']);
        echo $result;
        break;
    case 'ajaxChangeLessonPlan':
        $result = changeLessonPlan($_POST['class_id'], $_POST['lessonplan_id']);
        echo $result;
        break;
}
function ajaxMakeJsonSession($class_id = '', $change_from = '', $change_to = '', $class_case = '', $start_date = '', $schedule = '', $total_hours = '', $is_skip_holiday = 0, $koc_id = '', $level = '', $start_lesson = 1, $syllabus_by = '1'){
    global $timedate;

    //Make schedule - Check loi
    $schedule_formated = array();
    foreach ($schedule as $weekdate => $schl) {
        foreach ($schl as $key => $time_slot) {
            $_duration = unformat_number($time_slot['duration']);
            if ($_duration <= 0) return false; //Check loi

            $schedule_formated[$weekdate][$key]['start_time']    = $timedate->to_db_time($time_slot['start_time'], false);
            $schedule_formated[$weekdate][$key]['end_time']      = $timedate->to_db_time($time_slot['end_time'], false);
            $schedule_formated[$weekdate][$key]['duration']      = $_duration;
        }
    }
    if($total_hours <= 0) return false;
    //END- Check loi

    $total_minutes = round($total_hours*60); //Convert Hours to Minutes

    //Session found
    $sessions = $holiday_list= array();
    $lesson = $count_holidays = 0;
    $max_sessions = 10000;
    if(empty($start_lesson)) $start_lesson  = 1;
    $holidays = $holidays_date = $startLesson = array();

    //Public Holiday found
    if($is_skip_holiday){
        if (!empty($start_date)) $holiday_list = getPublicHolidays($start_date, '');
        else $holiday_list = getPublicHolidays($change_from, '');
    }

    $wStart  = strtotime($timedate->convertToDBDate($start_date, false));

    // Load Syllabus - by phgiahannn
    $result = $GLOBALS['db']->getOne("SELECT IFNULL(content, '') content FROM j_kindofcourse WHERE id = '$koc_id'");
    $koc_content = json_decode(html_entity_decode($result),true);
    $level_idx = array_search($level, array_column($koc_content, 'levels'));
    $level_lp = $koc_content[$level_idx]['lessonplan_id'];
    $cd = 0;
    $syll = array();
    $rows = getSyllabus($level_lp);
    foreach ($rows as $row) {
        $syll[$row['lesson']] = $row;
    }
    // schedule
    if ($class_case == 'create' || $class_case == 'change_startdate') {
        $start = strtotime($timedate->convertToDBDate($start_date, false));
        //foreach days check weekdate
        while ($total_minutes > 0) {
            $chck_day = date('D', $start);
            $run_date = date('Y-m-d', $start);
            $cd++;
            if (array_key_exists($chck_day, $schedule)) {
                if (array_key_exists($run_date, $holiday_list)) {
                    $hol_des = (!empty($holiday_list[$run_date])) ? "({$holiday_list[$run_date]})" : '';
                    $holidays[date('D', strtotime($run_date))][] = $timedate->to_display_date($run_date, false) . $hol_des;
                    $count_holidays++;
                    if ($run_date == $timedate->convertToDBDate($start_date, false))
                        $changeStartDate = true;
                } else {
                    foreach ($schedule[$chck_day] as $key => $time_slot) {
                        $wRun = strtotime($run_date);
                        if(!checkScheduleBy($time_slot['schedule_by'],[$wStart,$wRun])) continue;
                        $duration       = unformat_number($time_slot['duration']);
                        $total_minutes -= $duration;
                        if($total_minutes >= 0 && $lesson <= $max_sessions){
                            if($syllabus_by == '2'){ //By Day - qua 1 ngày hệ thống mới tính 1 lesson
                                if($_date != $run_date ){
                                    $lesson++;
                                    $_date = $run_date;
                                }
                            }else $lesson++;
                            $startLesson[$lesson] = $lesson.'. '.$syll[$lesson]['theme'];

                            $start_time = $timedate->to_db($timedate->to_display_date($run_date, false) . ' ' . $time_slot['start_time']);
                            $end_time = date('Y-m-d H:i:s', strtotime("+$duration minutes $start_time"));
                            $ss_date = array();
                            $ss_date['date']            = $run_date;
                            $ss_date['start_time']      = $start_time;
                            $ss_date['end_time']        = $end_time;
                            $ss_date['duration']        = $duration;
                            $sessions[$run_date . "-$key"] = $ss_date;
                            //Set new startdate if have holidays
                            if ($changeStartDate && empty($newStartDate))
                                $newStartDate = $timedate->to_display_date($run_date, false);
                        }else break;
                    }
                }
            }
            $start = strtotime('+1 day', $start);
        }

        foreach ($sessions as $key_ => $ses){
            //check overlap
            $total_ss_hours += $ses['duration'];
        }
        return array(
            'holidays'          => $holidays,
            'count_holidays'    => $count_holidays,
            'new_start_Date'    => $newStartDate,
            'sessions'          => $sessions,
            'count_sessions'    => $lesson,
            'defaut_hours'      => round($total_hours,2),
            'total_ss_hours'    => round($total_ss_hours/60,2),
            'start_date'        => reset($sessions)['date'],
            'end_date'          => end($sessions)['date'],
            'startLesson'       => get_select_options_with_id($startLesson, $start_lesson),
            'warning'           => ($lesson >= $max_sessions) ? translate('LBL_ERR_EXCEEDED', 'J_Class')." $max_sessions ".translate('LBL_SUPPANEL_SESSION', 'J_Class') : '',
            'schedule'          => $schedule_formated,
        );

    } elseif ($class_case == 'change_schedule') {
        $class = BeanFactory::getBean('J_Class', $class_id);
        $change_from_db = date('Y-m-d H:i:s', strtotime("-7 hours " . $timedate->convertToDBDate($change_from, false) . " 00:00:00"));
        $change_to_db = date('Y-m-d H:i:s', strtotime("-7 hours " . $timedate->convertToDBDate($change_to, false) . " 23:59:59"));
        $total_remain = $total_minutes;

        //get list sessions remove
        $remove_session = array();
        $q1 = "SELECT DISTINCT IFNULL(l1.id, '') l1_id
        FROM j_class
        INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
        WHERE (((j_class.id = '$class_id')  AND ((l1.date_start >= '$change_from_db'
        AND l1.date_start <= '$change_to_db')))) AND j_class.deleted = 0
        ORDER BY date_start ASC";
        $rs1 = $GLOBALS['db']->query($q1);
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1))
            $remove_session[] = $row['l1_id'];

        //Make new list sessions
        $class_start_db = date('Y-m-d H:i:s', strtotime("-7 hours " . $timedate->convertToDBDate($class->start_date, false) . " 00:00:00"));
        $class_end_db = date('Y-m-d H:i:s', strtotime("-7 hours " . $timedate->convertToDBDate($class->end_date, false) . " 23:59:59"));
        $wStart = strtotime($timedate->convertToDBDate($class->start_date, false));
        $sessions = array();
        $run_date = $timedate->convertToDBDate($start_date, false);
        //List 1 - Danh sách buổi trước ngày chỉnh sửa $change_from_db
        $q1 = "SELECT DISTINCT IFNULL(l1.id, '') l1_id,
        l1.date_start date_start, l1.date_end date_end, (l1.duration_minutes + (l1.duration_hours * 60)) duration
        FROM j_class
        INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
        WHERE (((j_class.id = '$class_id') AND (l1.date_end < '$change_from_db'))) AND j_class.deleted = 0 AND l1.session_status <> 'Cancelled'
        ORDER BY date_start ASC";
        $rs1 = $GLOBALS['db']->query($q1);
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
            $total_remain -= $row['duration'];
            if($total_remain >= 0 && $lesson <= $max_sessions) {
                $run_date = date('Y-m-d', strtotime("+7 hours " . $row['date_start']));
                if($syllabus_by == '2'){ //By Day - qua 1 ngày hệ thống mới tính 1 lesson
                    if($_date != $run_date ){
                        $lesson++;
                        $_date = $run_date;
                    }
                }else $lesson++;

                $sessions[$row['l1_id']]['date']        = $run_date;
                $sessions[$row['l1_id']]['start_time']  = $row['date_start'];
                $sessions[$row['l1_id']]['end_time']    = $row['date_end'];
                $sessions[$row['l1_id']]['duration']    = $row['duration'];
                $end_time = $sessions[$row['l1_id']]['end_time'];
            }else $remove_session[] = $row['l1_id'];
        }

        //List 2 - Danh sách buổi tạo mới trong thời gian chỉnh sửa
        $date_in_range = get_array_weekdates_db($change_from, $change_to, $schedule);
        foreach ($date_in_range as $ind => $date) {
            $cd++;
            if (array_key_exists($date, $holiday_list)) {
                if (!in_array($date, $holidays_date)) {
                    $holidays_date[] = $date;
                    $hol_des = (!empty($holiday_list[$date])) ? "({$holiday_list[$date]})" : '';
                    $holidays[date('D', strtotime($date))][] = $timedate->to_display_date($date, false) . $hol_des;
                    $count_holidays++;
                }
            } elseif ($total_remain > 0) {
                $arr_schedule = $schedule[date('D', strtotime($date))];
                foreach ($arr_schedule as $key => $time_slot) {
                    $wRun = strtotime($date);
                    if(!checkScheduleBy($time_slot['schedule_by'],[$wStart,$wRun])) continue;
                    $duration       = unformat_number($time_slot['duration']);
                    $total_remain -= $duration;
                    if($total_remain >= 0 && $lesson <= $max_sessions){
                        if($syllabus_by == '2'){ //By Day - qua 1 ngày hệ thống mới tính 1 lesson
                            if($_date != $run_date ){
                                $lesson++;
                                $_date = $run_date;
                            }
                        }else $lesson++;

                        $start_time = $timedate->to_db($timedate->to_display_date($date, false) . ' ' . $time_slot['start_time']);
                        $end_time = date('Y-m-d H:i:s', strtotime("+$duration minutes $start_time"));
                        $sessions[$date . "-$key"]['date']       = $date;
                        $sessions[$date . "-$key"]['start_time'] = $start_time;
                        $sessions[$date . "-$key"]['end_time']   = $end_time;
                        $sessions[$date . "-$key"]['duration']   = $duration;
                        $run_date = $date;
                    }else break;
                }
            }
        }

        //List 3 - Danh sách buổi sau ngày chỉnh sửa
        $q1 = "SELECT DISTINCT IFNULL(l1.id, '') l1_id, l1.date_start date_start,
        l1.date_end date_end, (l1.duration_minutes + (l1.duration_hours * 60)) duration
        FROM j_class
        INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
        WHERE (j_class.id = '$class_id') AND (l1.date_start >= '$change_to_db')
        AND j_class.deleted = 0 AND l1.session_status <> 'Cancelled'
        ORDER BY date_start ASC";
        $rs1 = $GLOBALS['db']->query($q1);
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
            $total_remain -= $row['duration'];
            if($total_remain >= 0 && $lesson <= $max_sessions) {
                $run_date = date('Y-m-d', strtotime("+7 hours " . $row['date_start']));
                if($syllabus_by == '2'){ //By Day - qua 1 ngày hệ thống mới tính 1 lesson
                    if($_date != $run_date ){
                        $lesson++;
                        $_date = $run_date;
                    }
                }else $lesson++;

                $sessions[$row['l1_id']]['date']        = $run_date;
                $sessions[$row['l1_id']]['start_time']  = $row['date_start'];
                $sessions[$row['l1_id']]['end_time']    = $row['date_end'];
                $sessions[$row['l1_id']]['duration']    = $row['duration'];
                $end_time = $sessions[$row['l1_id']]['end_time'];
            }else $remove_session[] = $row['l1_id'];
        }

        //List 4 - Tạo tiếp session nếu chưa đủ
        while ($total_remain > 0) {

            if ($change_to_db == $class_end_db) {
                //TH1: lịch học thay đổi đến cuối lớp thì generate tiếp lịch mới đổi - Replace template Schedule

            }else{
                //TH2: lịch học thay đổi đến giữa lớn thì generate tiếp lịch mới với template cũ - load old schedule template
                $json = json_decode(html_entity_decode($class->content), true);
                $schedule = $json['schedule'];
            }
            $run_init   = strtotime('+1 day', strtotime($run_date));
            $run_date   = date('Y-m-d', $run_init);
            if (array_key_exists(date('D', $run_init), $schedule)) {
                if (array_key_exists($run_date, $holiday_list)) {
                    $hol_des = (!empty($holiday_list[$run_date])) ? "({$holiday_list[$run_date]})" : '';
                    $holidays[date('D', $run_init)][] = $timedate->to_display_date($run_date, false) . $hol_des;
                    $count_holidays++;
                } else {
                    foreach ($schedule[date('D', $run_init)] as $key => $time_slot) {
                        $duration       = unformat_number($time_slot['duration']);
                        $total_remain -= $duration;
                        if($total_remain >= 0 && $lesson <= $max_sessions){
                            if($syllabus_by == '2'){ //By Day - qua 1 ngày hệ thống mới tính 1 lesson
                                if($_date != $run_date ){
                                    $lesson++;
                                    $_date = $run_date;
                                }
                            }else $lesson++;

                            $start_time = $timedate->to_db_time($time_slot['start_time'], false); //Xử lý từ data to_db_time
                            if(empty($start_time) && !empty($time_slot['start_time'])) $start_time = $time_slot['start_time']; //Xử lý từ data to_db_time
                            $start_time = $run_date . ' ' . $start_time;
                            if(!isValidDate($start_time,'Y-m-d H:i:s')) break;
                            $start_time = date('Y-m-d H:i:s', strtotime("-7hours $start_time"));
                            $end_time   = date('Y-m-d H:i:s', strtotime("+$duration minutes $start_time"));

                            $ss_date               = array();
                            $ss_date['date']       = $run_date;
                            $ss_date['start_time'] = $start_time;
                            $ss_date['end_time']   = $end_time;
                            $ss_date['duration']   = $duration;
                            $sessions[$run_date."-$key"] = $ss_date;

                        }else break;
                    }
                }
            }
        }

        foreach ($sessions as $key_ => $ses) $total_ss_hours += $ses['duration'];

        //Count Student
        $countStudent = $GLOBALS['db']->getOne("SELECT DISTINCT
            IFNULL(COUNT(DISTINCT l2.id), 0) count_student
            FROM j_studentsituations
            INNER JOIN j_class l1 ON j_studentsituations.ju_class_id = l1.id AND l1.deleted = 0
            INNER JOIN contacts l2 ON j_studentsituations.student_id = l2.id AND l2.deleted = 0
            WHERE (j_studentsituations.type IN ('Enrolled' , 'OutStanding', 'Demo')) AND (l1.id = '$class_id') AND j_studentsituations.deleted = 0
            GROUP BY l1.id");

        return array(
            'holidays'          => $holidays,
            'count_holidays'    => $count_holidays,
            'sessions'          => $sessions,
            'sessions_remove'   => $remove_session,
            'count_sessions'    => $lesson - 1,
            'defaut_hours'      => round($total_hours,2),
            'total_ss_hours'    => round($total_ss_hours/60,2),
            'start_date'        => reset($sessions)['date'],
            'end_date'          => end($sessions)['date'],
            'end_date'          => $run_date,
            'count_student'     => $countStudent,
            'warning'           => ($max_sessions <= $lesson) ? translate('LBL_ERR_EXCEEDED', 'J_Class')." $max_sessions ".translate('LBL_SUPPANEL_SESSION', 'J_Class') : '',
            'schedule'          => $schedule_formated,
        );
    }
}

function addDemo($dm_student_id = '', $dm_type_student = '', $dm_lesson_date = '', $dm_lesson_to_date = '', $dm_class_id = ''){
    global $timedate;
    //HANDLE CREATE
    $class = BeanFactory::getBean('J_Class', $dm_class_id);
    $stu = BeanFactory::getBean($dm_type_student, $dm_student_id);
    if ($stu->module_name == 'Leads' && $stu->converted){
        $stu = BeanFactory::getBean('Contacts', $stu->contact_id);
        if(empty($stu->id))
            return json_encode(
                array(
                    "success" => "0",
                    "error" => translate('LBL_CONVERTED_LEAD_ERR', 'J_Class'),
                )
            );
    }

    //Process Enroll Demo
    $student['class_id']     = $class->id;
    $student['settle_date']  = $GLOBALS['timedate']->nowDbDate();
    $student['team_id']      = $class->team_id;
    $student['id']           = $stu->id;
    $student['name']         = $stu->name;
    $student['type']         = $stu->module_name;
    $student['ost_type']     = 'Demo';
    $student['start']        = $timedate->convertToDBDate($dm_lesson_date);
    $student['end']          = $timedate->convertToDBDate($dm_lesson_to_date);
    $student['enroll_list']  = $_REQUEST['enroll_list'];
    $student['is_allow_ost'] = true;

    if(empty($student['enroll_list'])
    && !empty($student['start'])
    && !empty($student['end'])
    && !empty($student['class_id'])){
        $student['enroll_list'] = array_column($GLOBALS['db']->fetchArray("SELECT IFNULL(mt.id, '') mt_id FROM meetings mt
            WHERE mt.deleted = 0 AND (mt.ju_class_id = '{$student['class_id']}')
            AND (mt.date >= '{$student['start']}' AND mt.date <= '{$student['end']}')"),'mt_id');
    }

    if(empty($student['enroll_list'])){
        return json_encode(array("success" => 0,
            "student_id" => $student['id'],
            "student_type" => $student['type'],
            "error" => $student['name'].translate('LBL_ADD_FAIL', 'J_Class').'<br>'.translate('LBL_ALERT_STWR', 'J_Class')));
    }

    $res = addEMSEnrollment($student);
    if($res['success']){
        return json_encode(array("success" => 1,
            "student_id" => $student['id'],"student_type" => $student['type'],
            "notify" => $student['name']. translate('LBL_ADD_SUCCESSFULLY', 'J_Class')));
    }
    else return json_encode(array("success" => 0,
        "student_id" => $student['id'],"student_type" => $student['type'],
        "error" => $student['name'].translate('LBL_ADD_FAIL', 'J_Class').'<br>'.$res['errorLabel']));
}

//====================== Some funtion In Subpanel Situation======================//
function caculateFreeBalance($class_student_id='', $from_date=''){
    $situa = BeanFactory::getBean('J_ClassStudents', $class_student_id, array('disable_row_level_security'=>true));
    if (empty($situa->id)) return json_encode(array('success'=>0,'error'=>translate('LBL_ERR_CAN_NOT_SITUATION','J_Class')));
    return json_encode(calculateDelay($situa->student_id, $situa->student_type, $situa->class_id, $from_date));
}

function handleCreateDelay($class_student_id='', $from_date='', $to_date='', $dl_date='', $dl_reason='', $force_rm_from_class=0, $rvn_case='revert_delay', $dl_reason_for){
    $situa = BeanFactory::getBean('J_ClassStudents', $class_student_id, array('disable_row_level_security'=>true));
    if (empty($situa->id)) return json_encode(array('success'=>0,'error'=>translate('LBL_ERR_CAN_NOT_SITUATION','J_Class')));
    $delayType = (($force_rm_from_class) ? 'Remove' : 'Delay');
    return json_encode(handleDelay($situa->student_id, $situa->student_type, $situa->class_id, $from_date, $to_date, $dl_date, $dl_reason, $delayType, $rvn_case, $dl_reason_for));
}

function fixIssueSituation($class_id){
    $class = BeanFactory::getBean('J_Class', $class_id);
    $situationArr = GetStudentsProcessInClass($class_id);
    addStudentToNewSessions($situationArr, $class_id);
    return json_encode(array("success" => "1"));
}
//====================== End Some funtion Schedule======================//

//Function get student list for Send SMS - Modified By Lap Nguyen
function ajaxGetStudentList($classId = '', $lessonDate = '', $ss_id = '')
{
    require_once("custom/include/_helper/junior_class_utils.php");
    global $timedate, $current_user;
    $student_list = "";
    $res = json_decode(getListAttendanceStudent($classId, $lessonDate , $ss_id));
    if ($res->success == '0') {
        return json_encode(
            array(
                "success" => "0",
                "content" => "<tr><td colspan='100%' style='text-align:center;color:red;font-weight:bold;'>" . translate('LBL_NO_SESION_ON', 'J_Class') . " $lessonDate</td></tr>",
            )
        );
    } else {
        $count = 1;
        //Check rule Lock-date
        $lockEdit = false;
        if(!ACLController::checkAccess('C_Attendance', 'edit', false))  $lockEdit = false;

        //Custom Jaxtina lock_status
        if($res->lock_status == 'Locked' || $res->lock_status == 'Requesting')  $lockEdit = true;


        $lockView = false;
        if(!ACLController::checkAccess('C_Attendance', 'view', false))  $lockView = false;

        $sqlGetSyllabus = "
        SELECT
        IFNULL(topic_custom, '') topic_custom,
        IFNULL(homework, '') homework_custom,
        IFNULL(syllabus_custom, '') syllabus_custom,
        IFNULL(objective_custom, '') objective_custom
        FROM meetings WHERE id = '{$res->session_id}'
        ";
        $rsGetSyllabus = $GLOBALS['db']->query($sqlGetSyllabus);
        $row_custom = $GLOBALS['db']->fetchByAssoc($rsGetSyllabus);
        $sss = new Dotb_Smarty();
        $sss->assign('homework_custom', $row_custom['homework_custom']);
        $sss->assign('topic_custom', $row_custom['topic_custom']);
        $sss->assign('syllabus_custom', $row_custom['syllabus_custom']);
        $sss->assign('objective_custom', $row_custom['objective_custom']);
        $sss->assign('MOD_M', return_module_language($GLOBALS['current_language'], 'Meetings'));
        $sss->assign("lockEdit", $lockEdit);
        $sss->assign("lockView", $lockView);
        $sss->assign("lockEditLoyalty", !ACLController::checkAccess('J_Loyalty', 'edit', false));
        $sss->fetch('custom/modules/J_Class/tpls/attendance.tpl');
        $runStudentId = '###';
        $count_dup = 0;
        $count_error = 0;

        foreach ($res->array_student as $student_row) {
            $ss = new Dotb_Smarty();
            $ss->assign("lockEdit", $lockEdit);
            $ss->assign("lockView", $lockView);
            $ss->assign("lockEditLoyalty", !ACLController::checkAccess('J_Loyalty', 'edit', false));
            $ss->assign("NO", $count++);
            $ss->assign("IN_CLASS_TYPE", (empty($student_row->session_id)) ? "tr_not_in_class" : "");
            $ss->assign("STUDENT_ID", $student_row->student_id);
            $ss->assign("PORTAL_ID", $student_row->portal_id);
            $ss->assign("STUDENT_NAME", $student_row->student_name);
            $ss->assign("STUDENT_TYPE", $student_row->student_type);
            $ss->assign("STUDENT_TYPE_TEXT", $GLOBALS['app_list_strings']['student_type_list'][$student_row->student_type]);
            $ss->assign("SITUATION_TYPE", $GLOBALS['app_list_strings']['situation_type_list'][$student_row->situation_type]);
            $ss->assign("STUDENT_NAME_EN", viToEn($student_row->student_name));
            $ss->assign("STUDENT_PHONE", $student_row->student_phone);
            $ss->assign("BIRTHDATE", $timedate->to_display_date($student_row->birthdate, false));
            $ss->assign("NICK_NAME", $student_row->nick_name);

            //CUSTOMIZE JAXTINA
            $ss->assign("time_online", $student_row->total_time);
            $ss->assign("number_of_right", $student_row->number_of_right);
            $ss->assign("number_of_wrong", "<b style='cursor:pointer;' title='Wrong Ans List: ".htmlspecialchars_decode(html_entity_decode($student_row->wrong_ans_list))."'><span style='color:blue;'>$student_row->number_of_wrong </b>");
            $ss->assign("total_score", $student_row->total_score);
            if(empty($student_row->number_of_question)) $ss->assign("right_percent","");
            else $ss->assign("right_percent", round((($student_row->number_of_right/$student_row->number_of_question)*100)));
            $ss->assign("flipped", !empty($student_row->flipped) ? format_number($student_row->flipped, 0, 0) : '');
            $ss->assign("homework_num", !empty($student_row->homework_num) ? format_number($student_row->homework_num, 0, 0) : '');
            $ss->assign("teacher_comment_num", !empty($student_row->teacher_comment_num) ? format_number($student_row->teacher_comment_num, 0, 0) : '');
            $ss->assign("teacher_comment_num_per", round($student_row->teacher_comment_num_per ));
            if(!empty($student_row->number_of_question))
                $ss->assign("apps_result1", "<b style='cursor:pointer;' title='Wrong Ans List: ".htmlspecialchars_decode(html_entity_decode($student_row->wrong_ans_list))."'><span style='color:blue;'>{$student_row->number_of_right}</span> / {$student_row->number_of_question} </b>");
            if(!empty($student_row->total_score))
                $ss->assign("apps_result2", "<b style='color:blue;'> ".format_number($student_row->total_score,2,2)." </b>");

            $tool_tip = translate('LBL_TOTAL_ATTENDED', 'J_ClassStudents') . ": " . $student_row->total_attended . ' / ' . ($student_row->total_absent + $student_row->total_attended);
            $tool_tip .= "\n" . translate('LBL_TOTAL_ABSENT', 'J_ClassStudents') . ": " . $student_row->total_absent . ' / ' . ($student_row->total_absent + $student_row->total_attended);
            $avg_attendance = labelAttOverall($student_row->avg_attendance,$tool_tip);
            $ss->assign("AVG_ATTENDANCE", $avg_attendance);
            $ss->assign("total_loyalty_point", $student_row->total_loyalty_point);
            //END: CUSTOMIZE JAXTINA


            $ss->assign("ATTENDANCE_ID", $student_row->attend_id);
            $ss->assign("attendance_options", get_select_options_with_id($GLOBALS['app_list_strings']['attendance_type_list'], $student_row->attendance_type));
            $ss->assign("attendance_value", $GLOBALS['app_list_strings']['attendance_type_list'][$student_row->attendance_type]);
            $ss->assign("homework_options", get_select_options_with_id($GLOBALS['app_list_strings']['do_homework_list'], $student_row->homework));
            $ss->assign("homework_value", $GLOBALS['app_list_strings']['homework_value_list'][$student_row->homework]);
            $ss->assign("homework_score", !empty($student_row->homework_score) ? format_number($student_row->homework_score, 0, 0) : '');
            $ss->assign("care_comment", htmlspecialchars_decode(html_entity_decode($student_row->care_comment)));
            $ss->assign("homework_comment", htmlspecialchars_decode(html_entity_decode($student_row->homework_comment)));
            $ss->assign("sms_content", htmlspecialchars_decode(html_entity_decode($student_row->sms_content)));
            $ss->assign("description", htmlspecialchars_decode(html_entity_decode($student_row->description)));
            $ss->assign("PARENTNAME", $student_row->parent_name);
            $ss->assign("ATTENDANCE_CHECKED", $student_row->attended == 1 ? "checked" : "");
            $ss->assign("HOMEWORK_CHECKED", $student_row->homework == 1 ? "checked" : "");
            $ss->assign('MOD', return_module_language($GLOBALS['current_language'], 'J_Class'));
            $ss->assign("LOYALTY_POINT", $student_row->loyalty_point);

            $ss->assign("uri",str_replace('index.php','',$_SERVER['SCRIPT_NAME']));

            $student_list .= $ss->fetch('custom/modules/J_Class/tpls/attendance_item.tpl');
        }
        if(empty($res->array_student)) $student_list = "<tr><td colspan='100%'>".translate('LBL_NO_DATA', 'J_Class')."</td></tr>";

        return json_encode(
            array(
                "success" => "1",
                "session_id" => $res->session_id,
                "session_description" => $res->session_description,
                "content" => $student_list,
                "homework_custom" => $row_custom['homework_custom'],
                //Custom Display special characters
                "syllabus_custom" =>html_entity_decode_utf8(html_entity_decode($row_custom['syllabus_custom'], ENT_QUOTES | ENT_HTML5, 'UTF-8')),
                "objective_custom" =>html_entity_decode_utf8(html_entity_decode($row_custom['objective_custom'], ENT_QUOTES | ENT_HTML5, 'UTF-8')),
                "topic_custom" =>html_entity_decode_utf8(html_entity_decode($row_custom['topic_custom'], ENT_QUOTES | ENT_HTML5, 'UTF-8')),
                //END
            )
        );
    }
}
//Save Attendance - ALL
function saveAllAttendance($data = ''){
    require_once("custom/include/_helper/junior_class_utils.php");
    $listVals = json_decode(html_entity_decode($data['listVal']),true);
    foreach($listVals as $attend_id => $att){
        $data['savePos']    = array_keys($att)[0];
        $data['saveVal']    = $att[$data['savePos']];
        $data['no_cache']   = false;  //Custom Jaxtina
        $data['attend_id']  = $attend_id;
        $res[] = saveAttendanceUtils($data);
    }
    ////Custom Jaxtina - DO đã cache từng dòng ở trên nên ko cần logic này
    //Update Cache
    //    if(in_array($data['savePos'], ['attendance_type', 'homework'])){
    //        updateMeetingAttendance($data['session_id']);
    //        updateClassStudent_ATT($data['class_id']);
    //    }
    ////Custom Jaxtina
    return json_encode(array( "success" => 1, 'data' => $res));
}

function saveAttendance($data = ''){
    require_once("custom/include/_helper/junior_class_utils.php");
    return saveAttendanceUtils($data);
}
//END
function ajaxGetWeekDay($fromDate = '', $toDate = '', $classId = ''){
    global $timedate;
    $fromDateDb = $timedate->convertToDBDate($fromDate, false);
    $toDateDb = $timedate->convertToDBDate($toDate, false);

    $sqlGetDate = "SELECT
    DAYNAME(DATE(CONVERT_TZ(meetings.date_end,'+00:00','+7:00'))) dayname,
    CONCAT(DATE_FORMAT(CONVERT_TZ(meetings.date_start,'+00:00','+7:00'),'%H:%i'), ' - ', DATE_FORMAT(CONVERT_TZ(meetings.date_end,'+00:00','+7:00'),'%H:%i')) timeslot
    FROM meetings
    WHERE meetings.ju_class_id = '$classId'
    AND DATE(CONVERT_TZ(meetings.date_end,'+00:00','+7:00')) between '$fromDateDb' and '$toDateDb'
    AND meetings.deleted = 0 AND session_status <> 'Cancelled'
    GROUP BY dayname , timeslot";
    $rs = $GLOBALS['db']->query($sqlGetDate);
    $arrayWeekDate = array();
    while ($row = $GLOBALS['db']->fetchByAssoc($rs)) {
        $arrayWeekDate[$row['dayname']]['timeslot'][] = $row['timeslot'];
    }
    return json_encode(
        array(
            "success" => "1",
            "array_date" => json_encode($arrayWeekDate)
        )
    );
}

function ajaxsendMgsApp($data){
    require_once("custom/include/_helper/junior_class_utils.php");
    require_once('modules/C_SMS/SMS/sms.php');
    $listVals = json_decode(html_entity_decode($data['listVal']),true);
    foreach($listVals as $attend_id => $att){
        if(in_array($att['send_type'],['send_att','send_mes'])){
            //Send attendance daily report & App messages
            $param['attend_id'] = $att['attend_id'];
            $param['send_type'] = $att['send_type'];
            $param['no_cache']  = true;
            $param['msg']       = $att['msg'];
            $res = json_decode(attendenceSendApp($param));
        }
        if($att['send_type'] == 'send_sms'){
            $student = BeanFactory::getBean($att['student_type'], $att['student_id']);
            if(!empty($student->id)){
                $sms     = new sms();
                $result  = (int)$sms->send_message($student->phone_mobile, $att['msg'], $att['student_type'], $att['student_id'], $GLOBALS['current_user']->id, $student->team_id);
                $res->status = ($result <= 0) ? 'FAILED' : 'RECEIVED';
            }
        }

        if ($res->status == 'RECEIVED') $sent_success[] = array('student_id' => $att['student_id'],'student_name' => $att['student_name']);
        else $sent_fail[] = array('student_id' => $att['student_id'],'student_name' => $att['student_name']);
    }
    //Update Cache
    if($data['send_type'] == 'send_att') updateMeetingAttendance($data['session_id']);

    return json_encode(array(
        "success" => 1,
        "count_success" => count($sent_success),
        "sent_success" => $sent_success,
        "count_fail"  => count($sent_fail),
        "sent_fail"  => $sent_fail));
}

//Get Teacher List - Create By Lap Nguyen
function ajaxGetTeacherBySchedule($classId = '', $from_date = '', $to_date = '', $day_of_week = '', $sc_type = '', $available = 1, $another = 0)
{
    global $timedate;
    $start_date = $timedate->convertToDBDate($from_date, false);
    $end_date = $timedate->convertToDBDate($to_date, false);
    $style1 = ""; //dùng để chỉnh column nếu $sc_type == 'Room'
    $style2 = ""; //dùng để ẩn column nếu $sc_type == 'Room'
    if (in_array($sc_type, array_keys($GLOBALS['app_list_strings']['teacher_schedule_list'])) && $sc_type !== 'Room') {
        $teacherList = checkTeacherInClass($classId, $from_date, $to_date, $day_of_week, $available, $another, $sc_type);
        $style1 .= 'text-align: center;';
    } elseif ($sc_type == 'Room') {
        $teacherList = checkRoomInClass($classId, $from_date, $to_date, $day_of_week);
        $style2 .= 'display: none;';
    }
    $teacherListHtml = "";
    if ($available) {
        if (!$another) {
            array_unshift(
                $teacherList, array(
                    "teacher_id" => '',
                    "teacher_name" => '<b style="color: red;">'.str_replace('{schedule_for}',$GLOBALS['app_list_strings']['teacher_schedule_list'][$sc_type],translate('LBL_CLEAR_TEACHER', 'J_Class')).'</b>' ,
                    "contract_id" => '',
                    "contract_type" => '',
                    "require_hours" => '',
                    "total_hour" => '',
                    "contract_until" => '',
                    "contract_until_span" => '',
                    "day_off" => '',
                    "kind_of_course" => '',
                    "note" => '',
                    "holiday" => '',
                    "priority" => '2',
                )
            );
        }
        foreach ($teacherList as $key => $value) {
            $ss = new Dotb_Smarty();
            $ss->assign("ANOTHER", $another);
            $ss->assign("CENTER", $value["center"]);
            $ss->assign("PHONE", $value["phone"]);
            $ss->assign("TEACHER_ID", $value["teacher_id"]);
            $ss->assign("NAME", $value["teacher_name"]);
            $ss->assign("CONTRACT_ID", $value["contract_id"]);
            $ss->assign("CONTRACT_TYPE", $value["contract_type"]);
            $ss->assign("REQUIRED_HOURS", $value["require_hours"]);
            $ss->assign("TAUGHT_HOURS", $value["total_hour"]);
            $ss->assign("EXPIRE_DAY", $value["contract_until"]);
            $ss->assign("EXPIRE_DAY_SPAN", $value["contract_until_span"]);
            $ss->assign("KOC", $value["kind_of_course"]);
            $ss->assign("DAY_OFF", $value["day_off"]);
            $ss->assign("NOTE", $value["note"]);
            $ss->assign("HOLIDAYS", $value["holiday"]);
            $ss->assign("STYLE_1", $style1);
            $ss->assign("STYLE_2", $style2);

            $teacherListHtml .= $ss->fetch('custom/modules/J_Class/tpls/teacher_schedule_screen_item.tpl');
        }
        if (!count($teacherList) && $another) {
            $teacherListHtml .= '<tr id="no_another_teacher" >
            <td style="text-align: center;" colspan="8"> No another available teacher on this time</td>
            </tr>';
        }
    } else {
        if (count($teacherList)) {
            foreach ($teacherList as $key => $value) {
                $teacherListHtml .= "<tr><td colspan='3'><hr/></td></tr><tr>";
                $teacherListHtml .= "<td rowspan='" . count($value['class_teaching']) . "'>
                <a href=index.php?module=C_Teachers&action=DetailView&record=" . $value['teacher_id'] . " target=_blank><label style='cursor:pointer' for='" . $value['teacher_id'] . "'>" . $value['teacher_name'] . "</label></a></td>";
                foreach ($value['class_teaching'] as $k_class => $v_class) {
                    $teacherListHtml .= "<td>
                    <a href=index.php?module=J_Class&action=DetailView&record=" . $v_class['class_id'] . " target=_blank><label style='cursor:pointer' for='" . $value['teacher_id'] . "'>" . $v_class['class_name'] . "</label></a>
                    </td><td><div class='more_schedule'>";
                    foreach ($v_class['schedule'] as $k_schedule => $v_schedule)
                        $teacherListHtml .= "<div class='busy_schedule'>$v_schedule</div>";
                    $teacherListHtml .= "<a class='show_more_schedule' style='display: none' href='#'>Show more >></a>";
                    $teacherListHtml .= "</div></td></tr>";
                }
            }
        } else
            $teacherListHtml .= '<tr id="no_busy_teacher" ><td style="text-align: center;" colspan="3"> No busy teacher on this time</td></tr>';
    }
    return $teacherListHtml;
}

function ajaxSaveTeacherSchedule($scheduleForId = '', $classId = '', $from_date = '', $to_date = '', $day_of_week = '', $sc_type = ''){
    global $timedate;
    $class = BeanFactory::getBean('J_Class',$classId);
    if(empty($class->id)) return false;

    if(in_array($sc_type, ['Teacher', 'TA 1', 'TA 2'])){
        $teacher = BeanFactory::getBean('C_Teachers', $scheduleForId);
        if($sc_type == 'Teacher') $field_update = 'teacher_id';
        if($sc_type == 'TA 1') $field_update = 'teacher_cover_id';
        if($sc_type == 'TA 2') $field_update = 'sub_teacher_id';
    }elseif($sc_type == 'Room') $field_update = 'room_id';

    $ext_teacher = "";
    if($sc_type == 'Teacher'){
        if(!empty($_POST['teaching_type']))
            $ext_teacher .= "teaching_type = '{$_POST['teaching_type']}', ";
        if(empty($scheduleForId)) $ext_teacher = "teaching_type = '', ";
        if(!empty($_POST['change_reason']))
            $ext_teacher .= "description = '{$_POST['change_reason']}', ";
    }

    //Update Session
    $day_of_week = explode(",", $day_of_week);
    $sessionList = getClassSession($classId, $from_date, $to_date, $day_of_week);
    foreach ($sessionList as $key => $value) $sss[] = $value['meeting_id'];
    $sqlUpdateSession = "UPDATE meetings SET $field_update = '$scheduleForId', $ext_teacher date_modified='{$timedate->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE id IN ('".implode("','", $sss)."')";
    $GLOBALS['db']->query($sqlUpdateSession);


    //Update Relationship Class
    if(count($sss) > 0){
        require_once("custom/include/utils/lms_helpers.php");
        if($sc_type == 'Teacher' && $_POST['teaching_type'] == 'main_teacher') {
            // Check tổng số buổi dạy của giáo viên được scheduled để set lại main teacher
            $change_main_teacher = checkMainTeacher($class->id, $scheduleForId);
            if($change_main_teacher)
                $GLOBALS['db']->query("UPDATE j_class SET teacher_id = '$scheduleForId' WHERE id = '$classId' AND deleted = 0");
            // END
        }
        if($sc_type == 'Room')
            $GLOBALS['db']->query("UPDATE j_class SET room_id = '$scheduleForId' WHERE id = '$classId' AND deleted = 0");
        //Add Relationship
        if(in_array($sc_type, ['Teacher', 'TA 1', 'TA 2'])){
            $class->load_relationship('j_class_c_teachers_1');
            $res = $class->j_class_c_teachers_1->add($scheduleForId);
            if(!empty($teacher->lms_user_id) && $class->lms_course_id){
                if($teacher->type == 'Teacher') $result2 = enrollUser($teacher->lms_user_id, $class->lms_course_id, 'TeacherEnrollment');
                if($teacher->type == 'TA') $result2 = enrollUser($teacher->lms_user_id, $class->lms_course_id, 'TaEnrollment');
                if($result2['status'] == 'success')
                    $GLOBALS['db']->query("UPDATE j_class_c_teachers_1_c SET lms_enrollment_id = '{$result2['enrollment_id']}' WHERE j_class_c_teachers_1j_class_ida = '{$class->id}' AND j_class_c_teachers_1c_teachers_idb = '{$teacher->id}' AND deleted = 0");
            }

            //Kiểm tra xóa quan hệ Giáo viên ko còn dạy
            $teachers_have_schedule = array_column($GLOBALS['db']->fetchArray("SELECT DISTINCT
                IFNULL(l2.id, '') teacher_id,
                COUNT(meetings.id) count_ss
                FROM meetings
                INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
                INNER JOIN c_teachers l2 ON (meetings.teacher_id = l2.id OR meetings.teacher_cover_id = l2.id OR meetings.sub_teacher_id = l2.id) AND l2.deleted = 0
                WHERE (((l1.id = '$classId'))) AND meetings.deleted = 0
                GROUP BY l1.id , l1.name , l2.id"),'teacher_id');
            $all_teachers_rel = array_column($GLOBALS['db']->fetchArray("SELECT DISTINCT
                IFNULL(l1.id, '') teacher_id
                FROM j_class
                INNER JOIN j_class_c_teachers_1_c l1_1 ON j_class.id = l1_1.j_class_c_teachers_1j_class_ida AND l1_1.deleted = 0
                INNER JOIN c_teachers l1 ON l1.id = l1_1.j_class_c_teachers_1c_teachers_idb AND l1.deleted = 0
                WHERE (j_class.id = '$classId') AND j_class.deleted = 0"),'teacher_id');
            foreach( $all_teachers_rel as $key => $rel_id){
                if(!in_array($rel_id, $teachers_have_schedule)){
                    $lms_enrollment_id = $GLOBALS['db']->getOne("SELECT IFNULL(lms_enrollment_id, '') lms_enrollment_id FROM j_class_c_teachers_1_c WHERE j_class_c_teachers_1j_class_ida = '{$class->id}' AND j_class_c_teachers_1c_teachers_idb = '{$teacher->id}' AND deleted = 0 AND lms_enrollment_id <> '' AND lms_enrollment_id IS NOT NULL");
                    if(!empty($lms_enrollment_id)) $res = deleteEnroll($class->lms_course_id,$lms_enrollment_id);
                    $class->load_relationship('j_class_c_teachers_1');
                    $class->j_class_c_teachers_1->delete($rel_id);

                }
            }
        }
    }

    //HANDLE ONLINE CLASS
    $today_time = $timedate->nowDb();
    foreach ($sessionList as $key => $value) {
        $time_start = $value['time_start'];
        $time_end = $value['time_end'];
        if (!empty($value['external_id']) //Đã tạo lịch Online
        && ($today_time < $time_start)  //Buổi trong tương lai
        ) {
            $meeting = BeanFactory::getBean('Meetings', $value['meeting_id']);

            //TH gỡ bỏ Giáo viên hoặc thay đổi giáo viên => Xóa lịch để cron tự động tại lại lịch mới
            if (!empty($value['teacher_id']) && $sc_type == 'Teacher') {
                //============GOOGLE MEET================
                if ($meeting->type == 'Google Meet')
                    deleteGM($meeting);
                //============CLASSIN================
                if ($meeting->type == 'ClassIn') {
                    // TH sử dụng Room để làm Teacher đại diện cho 1 course trong ClassIn thì Main teacher thay đổi không làm ảnh hưởng teacher của lesson
                    // Room thay đổi mới thay đổi hàng loạt teacher trong lesson của ClassIn
                }
                //============ZOOM================
                if ($meeting->type == 'Zoom') {

                }
            }

            //TH gỡ bỏ TA hoặc thay đổi TA  ==> Gọi API update
            if (($sc_type == 'TA 1' && ($value['ta1_id'] != $scheduleForId))
            || ($sc_type == 'TA 2' && ($value['ta2_id'] != $scheduleForId))) {
                //============GOOGLE MEET================
                if ($meeting->type == 'Google Meet') {
                    updateGM($meeting);
                }
                //============CLASSIN================
                if ($meeting->type == 'ClassIn') {
                    //Trường hợp sử dụng Tài khoản ClassIn có giới hạn thì không thể tùy tiện thêm TA vào Course được
                    // => Không cần update
                }
                //============ZOOM================
                if ($meeting->type == 'Zoom') {

                }
            }
        }

        //Đã tạo lịch Online & buổi chưa kết thúc & trong tương lai
        if (!empty($value['external_id']) && $today_time < $time_end){
            //TH thay đổi room online
            if ($sc_type == 'Room' && $value['room_id'] != $scheduleForId){
                $meeting = BeanFactory::getBean('Meetings', $value['meeting_id']);
                //============CLASSIN================
                //Ở ClassIn: thay đổi room thì cần update lại room (teacher) trong lesson đó
                if ($meeting->type == 'ClassIn') {
                    updateClassInLesson($meeting);
                }
            }
        }
    }
    updateClassStatusUtil($class->id);

    return true;
}

function checkMainTeacher($class_id, $teacher_id) {
    $query = "SELECT
            COUNT(id) AS count_session,
            teacher_id
        FROM
            meetings
        WHERE
            ju_class_id = '$class_id' AND deleted = 0 AND status <> 'Cancelled'
            AND meeting_type = 'Session' AND teacher_id <> ''
        GROUP BY teacher_id ORDER BY count_session DESC LIMIT 1";
    $res = $GLOBALS['db']->fetchOne($query);
    if($res['teacher_id'] == $teacher_id) return true;
    else return false;

}

function ajaxLoadClassInfo($class_id = '', $session_id = ''){
    global $timedate;

    //Get list class  session
    $class = BeanFactory::getBean('J_Class',$class_id);
    if(empty($class->id) || empty($session_id)) return json_encode(array("success" => 0));

    $sylla = array();
    $sss = get_list_lesson_by_class($class->id);
    $ssIds = array_column($sss, 'primaryid');
    $cridx = array_search($session_id, $ssIds);
    $current_lesson = $sss[$cridx];
    $prev_lesson = $sss[$cridx-1];
    $next_lesson = $sss[$cridx+1];

    //Load syllabus
    $querySyllabus = "SELECT DISTINCT IFNULL(mt.id, '') primaryid, IFNULL(jb.id, '') syll_id, IFNULL(mt.type, '') type,
    IFNULL(jb.lesson_type, '') type, IFNULL(jb.lesson, '') lesson, IFNULL(jb.name, '') theme, IFNULL(jb.lessonplan_id, '') lessonplan_id,
    IFNULL(jb.homework, '') homework, IFNULL(jb.note_for_teacher, '') objective, IFNULL(jb.description, '') content
    FROM meetings mt INNER JOIN j_syllabus jb ON mt.syllabus_id = jb.id AND jb.deleted = 0
    WHERE (mt.id IN ('$session_id', '{$prev_lesson['primaryid']}', '{$next_lesson['primaryid']}')) AND mt.deleted = 0";
    $rowSS = $GLOBALS['db']->fetchArray($querySyllabus);
    foreach ($rowSS as $key => $rowS){
        $ssID = $rowS['primaryid'];
        $sylla[$ssID]['syllabus_id']= $rowS['syll_id'];
        $sylla[$ssID]['lessonplan_id']= $rowS['lessonplan_id'];
        $sylla[$ssID]['type']       = $GLOBALS['app_list_strings']['type_list'][$rowS['type']];
        $sylla[$ssID]['lesson']     = $rowS['lesson'];
        $sylla[$ssID]['topic']      = $rowS['theme'];
        $sylla[$ssID]['activities'] = $rowS['content'];
        $sylla[$ssID]['objective']  = $rowS['objective'];
        $sylla[$ssID]['homework']   = $rowS['homework'];
        //Xử lý thêm thẻ a với url
        foreach(['topic','activities','objective','homework'] as $field){
            preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $sylla[$ssID][$field], $matches);
            foreach(array_unique($matches[0]) as $match){$sylla[$ssID][$field] = str_replace($match,"<a href=\"$match\" target=\"_blank\">$match</a>",$sylla[$ssID][$field]);}
        }//END
    }
    $current_lesson = array_merge($current_lesson, (array)$sylla[$current_lesson['primaryid']]);

    $help = new Dotb_Smarty();
    $help->assign('MOD_M', return_module_language($GLOBALS['current_language'], 'Meetings'));
    $help->assign("uri",str_replace('index.php','',$_SERVER['SCRIPT_NAME']));
    $prev_lesson = array_merge($prev_lesson, (array)$sylla[$prev_lesson['primaryid']]);
    $prev_lesson['title'] = $timedate->to_display_date($prev_lesson['date'],false).(!empty($prev_lesson['topic']) ? ' - '.$prev_lesson['topic']: '');;
    if(!empty($prev_lesson)){
        $help->assign("ss", $prev_lesson);
        $tips_prev = $help->fetch('custom/modules/J_Class/tpls/helpClassInfo.tpl');
    }
    $next_lesson = array_merge($next_lesson, (array)$sylla[$next_lesson['primaryid']]);
    $next_lesson['title'] = $timedate->to_display_date($next_lesson['date'],false).(!empty($next_lesson['topic']) ? ' - '.$next_lesson['topic']: '');
    if(!empty($next_lesson)){
        $help->assign("ss", $next_lesson);
        $tips_next = $help->fetch('custom/modules/J_Class/tpls/helpClassInfo.tpl');
    }

    $ssClass = new Dotb_Smarty();
    $ssClass->assign('MOD', return_module_language($GLOBALS['current_language'], 'J_Class'));
    $ssClass->assign('MOD_M', return_module_language($GLOBALS['current_language'], 'Meetings'));
    $ssClass->assign("class_name",$class->name);
    $ssClass->assign("class_id",$class->id);
    $ssClass->assign("current_lesson", array_merge($current_lesson, (array)$sylla[$session_id]));
    $ssClass->assign("prev_lesson", $prev_lesson);
    $ssClass->assign("next_lesson", $next_lesson);
    $ssClass->assign("tips_prev", $tips_prev);
    $ssClass->assign("tips_next", $tips_next);
    $ssClass->assign("uri",str_replace('index.php','',$_SERVER['SCRIPT_NAME']));
    $html = $ssClass->fetch('custom/modules/J_Class/tpls/ClassInfoFieldset.tpl');

    return json_encode(array(
        "success" => 1,
        "html" => $html
        )
    );
}

function ajaxSaveSessionDescription($sessionId = '', $description = '')
{
    $sql = "UPDATE meetings
    SET description = '{$description}'
    WHERE id = '{$sessionId}'
    AND deleted <> 1
    ";
    $result = $GLOBALS['db']->query($sql);
    return json_encode(
        array(
            "success" => "1",
        )
    );
}

function getTeacherAndRoom($date = '', $start = '', $end = '', $class_id = '')
{
    global $timedate;
    $sql = "SELECT team_id FROM j_class WHERE id = '$class_id' ";
    $center_id = $GLOBALS['db']->getOne($sql);
    $start_time = $timedate->to_db($date . " " . $start);
    $end_time = $timedate->to_db($date . " " . $end);
    $data = array();
    $teacher_list = getTeacherOfCenter($center_id, 'Teacher');
    foreach ($teacher_list as $key => $val) {
        if (!checkTeacherInDateime($key, $start_time, $end_time)) {
            unset($teacher_list[$key]);
            continue;
        }
        if (checkTeacherHolidays($key, array(array('date_start' => date('Y-m-d H:i:s', strtotime('-7 hours ' . $start_time)))))) {
            unset($teacher_list[$key]);
            continue;
        }
    }
    $teacher_list = array_merge(array('' => '--None--'), $teacher_list);

    $data['teacher_options'] = get_select_options_with_id($teacher_list, '');


    $room_list = getRoomOfCenter($center_id);
    foreach ($room_list as $key => $val) {
        if (!checkRoomInDateime($key, $start_time, $end_time)) {
            unset($room_list[$key]);
        }
    }

    $room_list = array_merge(array('' => '--None--'), $room_list);
    $data['room_options'] = get_select_options_with_id($room_list, '');
    return json_encode($data);
}

function getDataForCancelSession($session_id){
    global $timedate, $current_user;
    $ss      = BeanFactory::getBean('Meetings', $session_id, array('disable_row_level_security'=>true));
    $class   = BeanFactory::getBean('J_Class', $ss->ju_class_id, array('disable_row_level_security'=>true));

    if(empty($ss->id) || empty($class->id) || $ss->session_status == 'Cancelled')
        return json_encode(array(
            'success' => 0,
            'message' => translate('LBL_SESSION_NOT_FOUND','J_Class')));

    //Lấy buổi học sau cùng
    $next_date = getEndNextTimeSession($class);
    if(empty($next_date))
        return json_encode(array(
            'success' => 0,
            'message' => translate('LBL_SESSION_NOT_FOUND','J_Class')));


    if($class->syllabus_by == '2'){
        //Lấy tất cả buổi học ngày hôm đó
        $ThatDate   = $timedate->to_display_date($ss->date_start);
        $ssThatDate = get_list_lesson_by_class($ss->ju_class_id, $ThatDate, $ThatDate);
        $countThatDate = count($ssThatDate);
        if($countThatDate > 1)  $notify = str_replace('{cancel_date}',$ThatDate,translate('LBL_CANCEL_SYLLABUS_BY_DAY', 'J_Class'));
    }

    $duration= ($ss->duration_hours*60) + $ss->duration_minutes;
    $data_array = array(
        'success'      => 1,
        'id'           => $ss->id,
        'duration'     => $duration,
        'ss_date'      => $timedate->to_display_date($ss->date_start),
        'ss_time'      => substr($timedate->to_display_date_time($ss->date_start), 11, 5).' - '.substr($timedate->to_display_date_time($ss->date_end), 11),
        'session_date' => date($timedate->get_date_format($current_user), strtotime($next_date)),
        'start_time'   => date($timedate->get_time_format($current_user), strtotime($next_date)),
        'end_time'     => date($timedate->get_time_format($current_user), strtotime("+$duration minutes $next_date")),
        'notify'       => $notify
    );
    return json_encode($data_array);
}

function cancelSession($data = ''){
    global $timedate;
    $thisSS      = BeanFactory::getBean('Meetings', $data['session_id'], array('disable_row_level_security'=>true));
    $class   = BeanFactory::getBean('J_Class', $thisSS->ju_class_id, array('disable_row_level_security'=>true));
    if(empty($thisSS->id) || empty($class->id) || $thisSS->session_status == 'Cancelled')
        return json_encode(array(
            'success' => 0,
            'message' => translate('LBL_SESSION_NOT_FOUND','J_Class')));

    $res = handleCancelledSS_p1($data);

    //ĐOẠN HAO TỐN PERFORMANCE
    //update lesson number: Hao tốn performance
    $resClass = updateClassSession($class);
    $q11 = "UPDATE j_class SET start_date='{$resClass['start_date']}', end_date='{$resClass['end_date']}', short_schedule='{$resClass['short_schedule']}' WHERE id='{$class->id}'";
    $GLOBALS['db']->query($q11);

    // TODO - get Quá trình học của học viên từ ngày change lịch đến cuối để add lại- ĐANG SAI
    $_dateChange = $timedate->convertToDBDate($data['date']);
    if($thisSS->date < $_dateChange) $_dateChange = $thisSS->date;
    $situationArr = GetStudentsProcessInClass($class->id, $_dateChange);
    addStudentToNewSessions($situationArr, $class->id, $_dateChange);

    //Set Count Class Number
    updateClassAttendance($class->id);
    updateClassStudent($class->id);
    updateClassStatusUtil($class->id);
    return json_encode(
        array(
            'success' => 1,
            'message' => translate('LBL_CANCELLED_SUCCESS','J_Class'),
            'start_date' => $timedate->to_display_date($resClass['start_date'], false),
            'end_date' => $timedate->to_display_date($resClass['end_date'], false)
        )
    );
}

function deleteSession($data = '')
{
    $mt = BeanFactory::getBean('Meetings',$data['session_id']);
    if (!empty($mt->id) && $mt->session_status == 'Cancelled') {
        $GLOBALS['db']->query("DELETE FROM meetings_leads WHERE meeting_id = '{$data['session_id']}'");
        $GLOBALS['db']->query("DELETE FROM meetings_contacts WHERE meeting_id = '{$data['session_id']}'");
        $GLOBALS['db']->query("UPDATE meetings SET deleted = 1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE id='{$data['session_id']}'");
    }
    return true;
}

function saveStsDescription($data = ''){
    $clst = BeanFactory::getBean('J_ClassStudents',$data['sts_id']);
    if(!empty($clst->id)){
        $GLOBALS['db']->query("UPDATE j_classstudents SET description='{$data['description']}' WHERE id = '{$clst->id}' AND deleted=0");
        return true;
    }else return false;
}


function saveSyllabus()
{
    $meeting = BeanFactory::getBean('Meetings', $_POST['meeting_id']);
    if (!empty($meeting->id)) {
        $sql = "UPDATE meetings SET {$_POST['field']} = '{$_POST['content']}' WHERE id = '{$_POST['meeting_id']}'";
        $GLOBALS['db']->query($sql);
        return json_encode(
            array(
                "success" => "1",
            )
        );
    } else {
        return json_encode(
            array(
                "success" => "0",
            )
        );
    }
}

function customSaveSyllabus()
{
    $meeting = BeanFactory::getBean('Meetings', $_POST['session_id']);
    if (!empty($meeting->id)) {
        $meeting->homework          = $_POST['homework_custom'];
        $meeting->syllabus_custom   = $_POST['syllabus_custom'];
        $meeting->objective_custom  = $_POST['objective_custom'];
        $meeting->topic_custom      = $_POST['topic_custom'];
        $meeting->save();
        return json_encode(array("success" => 1));
    } else
        return json_encode(array("success" => 0));

}

//Function get student list for previous class
function ajaxLoadStudents($selected_student = '', $prv_class_id = '', $prv_ost_load = 0, $cur_class_id = '', $start = '', $end = '', $pm_selected = ''){
    global $timedate;
    if( (empty($prv_class_id) && empty($selected_student) && empty($prv_ost_load) )
        || empty($cur_class_id) || empty($start) || empty($end))
        return json_encode(array("success" => 0));

    $today = $timedate->nowDbDate();
    $crClass = BeanFactory::getBean('J_Class', $cur_class_id);
    $start   = $timedate->convertToDBDate($start);
    $end     = $timedate->convertToDBDate($end);
    //Reload session list
    $sss     = get_list_lesson_by_class($cur_class_id);
    //Create new list lesson
    $json_ss = $ern_ss = array();
    foreach($sss as $ss){
        $sesion = array(
            'id'    => $ss['primaryid'],
            'dhour' => $ss['delivery_hour'],
            'start' => $ss['date_start_tz'],
            'end'   => $ss['date_end_tz'],
        );
        //Reload session list
        $json_ss[] = $sesion;

        //load danh sách buổi Enroll
        if($ss['date'] >= $start && $ss['date'] <= $end){
            $total_hours += $ss['delivery_hour'];
            $ern_ss[] = $sesion;
        }
    }

    $students = array();
    //List 1: Get All student have relationship with previous class
    if (!empty($prv_class_id)) {
        $sql1 = "SELECT DISTINCT IFNULL(l2.id, '') student_id,
        IFNULL(l2.contact_id, '') student_code, IFNULL(l2.full_student_name, '') full_student_name,
        IFNULL(l2.phone_mobile, '') phone_mobile, l2.birthdate birthdate
        FROM j_studentsituations INNER JOIN j_class l1 ON j_studentsituations.ju_class_id = l1.id AND l1.deleted = 0
        INNER JOIN contacts l2 ON j_studentsituations.student_id = l2.id AND l2.deleted = 0 AND j_studentsituations.student_type = 'Contacts'
        WHERE (l1.id = '$prv_class_id') AND (j_studentsituations.type IN ('Enrolled', 'OutStanding')) AND j_studentsituations.deleted = 0
        GROUP BY l2.id";
        $rsSt1 = $GLOBALS['db']->fetchArray($sql1);
    }
    //List 2: Get list selected students
    if (!empty($selected_student)){
        if (is_array($selected_student)) $sts = $selected_student;
        else {
            $json_decode = json_decode(html_entity_decode($selected_student), true);
            if (!empty($json_decode['student_id'])) $sts[] = $json_decode['student_id'];
            else  $sts = $json_decode;
        }
        $sql2 = "SELECT DISTINCT IFNULL(l2.id, '') student_id,
        IFNULL(l2.contact_id, '') student_code,  IFNULL(l2.full_student_name, '') full_student_name,
        IFNULL(l2.phone_mobile, '') phone_mobile, l2.birthdate birthdate
        FROM contacts l2 WHERE deleted = 0 AND l2.id IN('" . implode("','", $sts) . "')";
        $rsSt2 = $GLOBALS['db']->fetchArray($sql2);
    }
    //List 3: Get list load Outstanding
    if (!empty($prv_ost_load)) {
        $sql3 = "SELECT IFNULL(l1.id, '') student_id,
        IFNULL(l1.contact_id, '') student_code,  IFNULL(l1.full_student_name, '') full_student_name,
        IFNULL(l1.phone_mobile, '') phone_mobile, l1.birthdate birthdate
        FROM j_studentsituations jst
        INNER JOIN contacts l1 ON jst.student_id = l1.id AND l1.deleted = 0 AND jst.student_type = 'Contacts'
        INNER JOIN j_class l2 ON jst.ju_class_id = l2.id AND l2.deleted = 0
        WHERE (jst.type='OutStanding') AND (l2.id = '{$crClass->id}') AND jst.deleted = 0
        GROUP BY student_id";
        $rsSt3 = $GLOBALS['db']->fetchArray($sql3);
    }
    //merge list students
    $enroll_list = array_column($ern_ss,'id');
    foreach (array_merge((array)$rsSt1,(array)$rsSt2,(array)$rsSt3) as $key => $r){
        $students[$r['student_id']]['type']              = 'Contacts';
        $students[$r['student_id']]['ost_type']          = 'OutStanding';
        $students[$r['student_id']]['student_id']        = $r['student_id'];
        $students[$r['student_id']]['student_code']      = $r['student_code'];
        $students[$r['student_id']]['full_student_name'] = $r['full_student_name'];
        $students[$r['student_id']]['phone_mobile']      = $r['phone_mobile'];
        $students[$r['student_id']]['birthdate']         = $r['birthdate'];
        $students[$r['student_id']]['total_hours']       = $total_hours;
        $students[$r['student_id']]['used_hours']        = $total_hours; //số giờ suggestion
        $students[$r['student_id']]['enroll_list']       = $enroll_list; //danh sách buổi ghi danh
        $students[$r['student_id']]['start']             = $start;
        $students[$r['student_id']]['end']               = $end;
    }

    $uTz = $timedate->getUserTimeZone()['gmt'];
    // Check student in present class
    $q2 = "SELECT DISTINCT IFNULL(l3.student_id, '') student_id,
    IFNULL(mt.id, '') meeting_id, ROUND(mt.duration_hours+(mt.duration_minutes/60),9) delivery_hour,
    CONVERT_TZ(mt.date_start,'+00:00','$uTz') date_end
    FROM meetings mt
    INNER JOIN j_class l1 ON mt.ju_class_id = l1.id AND l1.deleted = 0 AND l1.id = '$cur_class_id'
    INNER JOIN meetings_contacts l2_1 ON mt.id = l2_1.meeting_id AND (l2_1.contact_id IN ('".implode("','", array_keys($students))."')) AND l2_1.deleted = 0
    INNER JOIN j_studentsituations l3 ON l3.id = l2_1.situation_id AND (l3.type IN ('Enrolled','Demo')) AND l3.deleted = 0
    WHERE mt.deleted = 0 AND (mt.date BETWEEN '$start' AND '$end') AND (mt.session_status <> 'Cancelled')
    ORDER BY student_id, mt.date_start ASC";
    $prS = $GLOBALS['db']->fetchArray($q2);
    foreach($prS as $index => $pr){
        $students[$pr['student_id']]['busy_list'][] = $pr['meeting_id'];
        if (($key = array_search($pr['meeting_id'], $students[$pr['student_id']]['enroll_list'])) !== false) {
            unset($students[$pr['student_id']]['enroll_list'][$key]); //giảm buổi ghi danh
            $students[$pr['student_id']]['used_hours'] -= $pr['delivery_hour'];//Giảm số giờ suggestion
        }
    }

    //Check Max size
    $res = getClassSize($crClass->id);
    $countStds = $res['current_size'];
    $studentList = $res['student_list'];

    //Kiểm tra sỉ số Student đã ở trong lớp
    foreach($students as $stId => $student){
        //Kiểm tra lớp đã đóng
        if($crClass->status == 'Closed'){
            $students[$stId]['errorLabel'] = 'LBL_ENR_CLOSED_CLASS';
            continue;
        }

        if(!in_array($stId, $studentList)){
            if(!empty($crClass->max_size)){
                if($countStds <= $crClass->max_size) $countStds++;
                if($countStds > $crClass->max_size)  $students[$stId]['errorLabel'] = 'LBL_OVER_CLASS_SIZE';
            }
        }

        if(round($students[$stId]['total_hours'],2) <= 0){ $students[$stId]['errorLabel']  = 'LBL_ALLOWED_ENROLL'; //xét TH số giờ ko đúng
        }elseif(round($students[$stId]['used_hours'],2) <= 0 ) $students[$stId]['errorLabel']  = 'LBL_EXISTING_IN_CLASS'; //Xét TH học viên đã có trong lớp
    }

    //get Payment Remain for Enrollment
    $stRemains = getRemainForEnroll(array_keys($students), $crClass);
    $pm_selected = json_decode(html_entity_decode($pm_selected), true);
    //Get List Center Class
    if ($crClass->team_set_id != $crClass->team_id) {
        $teamSetBean = new TeamSet();
        $teams = $teamSetBean->getTeams($crClass->team_set_id);
        $crClass_teams = array_keys($teams);
    } else $crClass_teams = array($crClass->team_id);

    foreach ($stRemains as $key => $pay) {
        $stpm_selected = $pm_selected[$pay['student_id']];
        if(in_array($pay['id'], $stpm_selected)) $pay['selected'] = 1;
        else{
            $kind_of_course = unencodeMultienum($pay['kind_of_course']);
            //Tự động đề xuất nếu chưa chọn
            if ((empty($stpm_selected) && !isset($stpm_selected))
            && ($students[$pay['student_id']]['used_hours'] > 0)
            && ($today <= $pay['expired'])
            && (in_array($crClass->kind_of_course, $kind_of_course) || empty($pay['kind_of_course']))
            && (in_array($pay['team_id'], $crClass_teams))
            && empty($students[$pay['student_id']]['errorLabel'])) {
                $pay['selected'] = 1;
                $students[$pay['student_id']]['used_hours'] -= $pay['remain_hours'];
                $students[$pay['student_id']]['count_selected_pm']++;
            }
        }
        $students[$pay['student_id']]['paid_list'][$pay['id']] = $pay;
    }

    //get html content
    if(!empty($crClass->j_class_j_class_1j_class_ida) || !empty($prv_class_id) ){
        //get last Gradebook By Student
        $q2 = "SELECT DISTINCT
        IFNULL(l1.id, '') student_id, gd.final_result final_result,
        IFNULL(gd.certificate_type, '') certificate_type, IFNULL(gd.certificate_level, '') certificate_level
        FROM j_gradebookdetail gd
        INNER JOIN contacts l1 ON gd.student_id = l1.id AND l1.deleted = 0
        INNER JOIN j_gradebook l2 ON gd.gradebook_id = l2.id AND l2.deleted = 0
        INNER JOIN j_class_j_gradebook_1_c l3_1 ON l2.id = l3_1.j_class_j_gradebook_1j_gradebook_idb AND l3_1.deleted = 0
        INNER JOIN j_class l3 ON l3.id = l3_1.j_class_j_gradebook_1j_class_ida AND l3.deleted = 0
        WHERE (l1.id IN ('". implode("','", array_keys($students))."')) AND (l3.id = '".(empty($prv_class_id) ? $crClass->j_class_j_class_1j_class_ida : $prv_class_id )."') AND (IFNULL(l2.type,'') = 'Overall' ) AND gd.deleted = 0";
        $result = $GLOBALS['db']->fetchArray($q2);
        foreach ($result as $key => $rs)
            $students[$rs['student_id']]['final_result'] = $rs['final_result'] . ((!empty($rs['certificate_type'])) ? "- {$rs['certificate_type']}" : '');
    }
    //Sắp xếp kết quả Fail xuống dưới, True lên trên
    $stF =  $stT = array();
    foreach ($students as $key => $row) {
        if (!empty($row['errorLabel'])) $stF[$key] = $row;
        else $stT[$key] = $row;
    }
    $html = '';
    foreach (array_merge($stT,$stF) as $key => $row) {
        $row[$row['student_id']][] = $pr['meeting_id'];
        $checkBox = $image = $title = '';
        //handle alert error
        if (!empty($row['errorLabel'])) {
            $title = translate($row['errorLabel'], 'J_Class');
            $icon = '<i class="fa fa-exclamation-circle" style="color: #EF384B;" title="'.$title.'"></i>';
            $errLabelAll .= "<li>{$row['full_student_name']} | $title</li>";
        }else{
            $checkBox = '<input type="checkbox" checked class="custom_checkbox checkbox_item" module_name="J_Class" onclick="handleCheckBox($(this));handleCountItem($(this));" value="'.$row['student_id'].'">';
            $title = translate('LBL_ALLOWED_ENROLL', 'J_Class');
            $icon = '<i class="fa fa-check-circle" style="color: #43B977;" title="'.$title.'"></i>';
        }
        $html .= '<tr>
        <td>' .$checkBox. '</td>
        <td>' .$row['student_code']. '</td>
        <td>' .$row['full_student_name']. '</td>
        <td>' .$row['phone_mobile']. '</td>
        <td>' .$timedate->to_display_date($row['birthdate']). '</td>
        <td>' .$row['final_result']. '</td>
        <td>' .buildHTMLPaidList('prv_', $row). '</td>
        <td class="prv_join_type"><span class="error">'.$title.'</span></td>';
        unset($row['paid_list']); //Rút gọn Json
        $html .= '<td><div class="prv_icon">'.$icon.'</div>'
        .'<input type = "hidden" class="prv_total_hours" value="'.$row['total_hours'].'"/>'
        .'<input type = "hidden" class="prv_student_id" value="'.$row['student_id'].'"/>'
        ."<input type = 'hidden' class='prv_json' value='".json_encode($row)."'/>"
        .'</td></tr>';
    }

    return json_encode(array(
        "success"     => "1",
        "errLabelAll" => $errLabelAll,
        "html"        => $html,
        "json_ss"     => json_encode($json_ss),
        "ern_ss"      => json_encode($ern_ss),
        )
    );
}

function ajaxAddStudentToClass($class_id, $settle_date, $students_selected, $is_allow_ost = 1){
    global $timedate;
    $students = json_decode(html_entity_decode($students_selected), true);
    $class = BeanFactory::getBean('J_Class', $class_id,array('disable_row_level_security'=>true));
    if(empty($class->id) || count($students) == 0) return json_encode(array("success" => 0));

    foreach ($students as $key => $student) {
        //Enroll Student - prepare param
        $student['settle_date']     = $settle_date;
        $student['class_id']        = $class->id;
        $student['team_id']         = $class->team_id;
        $student['kind_of_course']  = $class->kind_of_course;
        $student['level']           = $class->level;
        $student['is_allow_ost']    = $is_allow_ost;

        $res = addEMSEnrollment($student);
        if($res['success']){
            $enr_success[] = array('student_name' => $student['name']);
        }else
            $enr_fail[] = array(
                'student_name' => $student['name'],
                'error_label' => $res['errorLabel']);
    }

    //HANDLE ONLINE CLASS
    $sessionList = getOnlineSession($class_id);
    $today_time = $timedate->nowDb();
    foreach ($sessionList as $key => $value) {
        $time_start = $value['time_start'];
        if (!empty($value['external_id']) //Đã tạo lịch Online
        && ($today_time < $time_start)){ //Buổi trong tương lai
            $meeting = BeanFactory::getBean('Meetings', $value['meeting_id']);
            //============GOOGLE MEET================
            if ($meeting->type == 'Google Meet') {
                updateGM($meeting);
            }
            //============CLASSIN================
            if ($meeting->type == 'ClassIn') {

            }
            //============ZOOM================
            if ($meeting->type == 'Zoom') {

            }
        }
    }
    return json_encode(
        array(
            "success" => "1",
            "students" => $students,
            "enr_success" => $enr_success,
            "enr_fail"  => $enr_fail,
        )
    );
}

function generate_online($go_ssid, $class_id, $type = ''){
    global $timedate;
    $meeting = BeanFactory::getBean('Meetings', $go_ssid);
    if (empty($meeting->id) || $meeting->session_status == 'Cancelled')
        return json_encode(array(
            "success" => 0,
            "notify" => translate('LBL_ERROR_UNLINK', 'Meetings')
        ));

    if ($type != $meeting->type) {
        //offline => online
        if (empty($meeting->type) || $meeting->type == 'Dotb') {
            $meeting->type = $type;
            $meeting->external_id = '';
            $meeting->join_url = '';
            $meeting->creator = '';

            //Set Meeting Type
            $GLOBALS['db']->query("UPDATE meetings SET external_id='{$meeting->external_id}', type='{$meeting->type}', join_url='{$meeting->join_url}', creator='{$meeting->creator}' WHERE id = '{$meeting->id}'");
        } else {
            //online => offline
            //Nếu chưa tạo lịch trên API thì chỉ cần update trên DB
            if (empty($meeting->external_id)) {
                $meeting->type = $type;
                $meeting->external_id = '';
                $meeting->join_url = '';
                $meeting->creator = '';

                //Set Meeting Type
                $GLOBALS['db']->query("UPDATE meetings SET external_id='{$meeting->external_id}', type='{$meeting->type}', join_url='{$meeting->join_url}', creator='{$meeting->creator}' WHERE id = '{$meeting->id}'");
            } else {
                //Nếu đã tạo lịch trên API thì gọi API để hủy lịch online
                //============GOOGLE MEET================
                if ($meeting->type == 'Google Meet')
                    deleteGM($meeting);
                //============CLASSIN================
                if ($meeting->type == 'ClassIn') {
                    deleteClassInLesson($meeting);
                }
                //============ZOOM================
                if ($meeting->type == 'Zoom') {

                }
            }
        }

    }
    return json_encode(array(
        "success" => 1,
        "notify" => translate('LBL_SUCCESS_UNLINK', 'Meetings')
    ));
}


function send_bmes($parent_id, $parent_type, $action_type, $send_to, $title, $description, $template_id){

    if(empty($parent_id)
    || empty($parent_type)
    || empty($action_type)
    || empty($send_to)
    || (empty($title) && empty($description))){
        return json_encode(array(
            "success" => 0,
            "notify" => translate('LBL_ERROR1', 'BMessage')
        ));
    }
    $bmes                = new BMessage();
    $bmes->parent_id     = $parent_id;
    $bmes->parent_type   = $parent_type;
    $bmes->action_type   = $action_type;
    $bmes->send_to       = $send_to;
    $bmes->title         = $title;
    $bmes->description   = $description;
    $bmes->template_id   = $template_id;
    $bmes->save();

    return json_encode(array(
        "success" => 1,
        "notify" => translate('LBL_SEND_SUCCESS', 'BMessage')
    ));
}

function load_bmes_template($parent_id, $parent_type){
    if(empty($parent_id)
    || empty($parent_type)){
        return json_encode(array(
            "success" => 0,
            "notify" => translate('LBL_ERROR1', 'BMessage')
        ));
    }
    //Load template


    return json_encode(array(
        "success" => 1,
        "notify" => translate('LBL_SEND_SUCCESS', 'BMessage')
    ));
}
//LMS auto-sync
function ajax_lms_sync($class_id){
    $class = BeanFactory::getBean('J_Class', $class_id);
    $class->lms_sync = 1;
    $class->save();
    return json_encode(array(
        "success" => 1,
    ));
}

//Khôi phục kết quả điểm danh
function restoreAttendance($class_id){
    global $timedate;
    $sss = get_list_lesson_by_class($class_id);
    $count = 0;
    //Check Attendance
    foreach ($sss as $key => $ss) {
        //Check attended
        $qAttended = $GLOBALS['db']->getOne("SELECT COUNT(DISTINCT l2.id) total_result
            FROM meetings INNER JOIN c_attendance l2 ON l2.meeting_id = meetings.id AND l2.deleted = 0
            WHERE meetings.id = '{$ss['primaryid']}' AND l2.attendance_type IN ('P','L','A','E') AND meetings.deleted = 0
            GROUP BY meetings.id");
        if(empty($qAttended)){
            $dsdate = date('Y-m-d H:i:s', strtotime("-14 days". $timedate->nowDb()));
            //Check Old Attendance result
            $q1 = "SELECT cc.id attId FROM c_attendance cc
            INNER JOIN meetings mt ON mt.id = cc.meeting_id AND mt.deleted = 1
            AND cc.date_modified >= '$dsdate'
            AND cc.date_modified = mt.date_modified
            AND mt.date_start = '{$ss['date_start']}'
            AND mt.date_end = '{$ss['date_end']}'
            AND mt.ju_class_id = '$class_id'
            WHERE cc.class_id = '$class_id' AND cc.attendance_type IN ('P','L','A','E') AND cc.deleted = 1";
            $rows = array_column($GLOBALS['db']->fetchArray($q1),'attId');
            if(count($rows)>0){
                $count++;
                //delete empty row
                $GLOBALS['db']->query("DELETE FROM c_attendance WHERE meeting_id='{$ss['primaryid']}' AND deleted=0 AND (attendance_type IS NULL OR attendance_type='')");
                //query update retore
                $GLOBALS['db']->query("UPDATE c_attendance cc SET
                    cc.meeting_id = '{$ss['primaryid']}',
                    cc.deleted = 0,
                    cc.name = 'restore|by_admin_action',
                    cc.date_modified='{$timedate->nowDb()}', cc.modified_user_id='{$GLOBALS['current_user']->id}'
                    WHERE cc.id IN ('".implode("','",$rows)."')");
            }

        }
    }
    if($count > 0) return json_encode(array("success" => 1, 'count_restored' => $count));
    else return json_encode(array("success" => 0));
}

function isValidDate(string $date, string $format = 'Y-m-d'): bool
{
    $dateObj = DateTime::createFromFormat($format, $date);
    return $dateObj && $dateObj->format($format) == $date;
}

// Update syllabus for sessions when changing the lesson plan
function changeLessonPlan($class_id, $lessonplan_id = '') {
    $class = BeanFactory::getBean('J_Class', $class_id);
    if(!empty($class->id)
    && $class->lessonplan_id != $lessonplan_id){
        $_POST['class_case'] = 'edit';
        $class->lessonplan_id = $lessonplan_id;
        $class->save();
    }

    return json_encode(
        array(
            'success'         => 1, //get start_lesson
            'start_lesson'    => $GLOBALS['db']->getOne("SELECT CONCAT(meetings.lesson_number, '. ',IFNULL(l1.name,'')) FROM meetings LEFT JOIN j_syllabus l1 ON meetings.syllabus_id = l1.id AND l1.deleted = 0 WHERE meetings.deleted = 0 AND meetings.ju_class_id = '{$class->id}' AND meetings.session_status <> 'Cancelled' AND meetings.lesson_number = '{$class->start_lesson}'"),
            'lessonplan_name' => $GLOBALS['db']->getOne("SELECT IFNULL(name, '') name FROM j_lessonplan WHERE id='$lessonplan_id'"),
            'lessonplan_id'   => $lessonplan_id,
        )
    );
}

//Procedural style
function getUTCOffset($timezone)
{
    $current   = timezone_open($timezone);
    $utcTime  = new \DateTime('now', new \DateTimeZone('UTC'));
    $offsetInSecs =  timezone_offset_get( $current, $utcTime);
    $hoursAndSec = gmdate('H:i', abs($offsetInSecs));
    return stripos($offsetInSecs, '-') === false ? "+{$hoursAndSec}" : "-{$hoursAndSec}";
}


