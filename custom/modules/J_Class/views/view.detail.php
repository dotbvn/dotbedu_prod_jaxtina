<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class J_ClassViewDetail extends ViewDetail
{
    /**
    * @see DotbView::display()
    *
    * We are overridding the display method to manipulate the portal information.
    * If portal is not enabled then don't show the portal fields.
    */
    public function display()
    {
        global $timedate, $current_user;

        require_once("custom/include/_helper/junior_revenue_utils.php");
        $this->ss->assign('MOD', return_module_language($GLOBALS['current_language'], 'J_Class'));
        $this->ss->assign('MOD_P', return_module_language($GLOBALS['current_language'], 'J_Payment'));
        $this->ss->assign('MOD_M', return_module_language($GLOBALS['current_language'], 'Meetings'));
        $this->ss->assign('MOD_S', return_module_language($GLOBALS['current_language'], 'J_StudentSituations'));
        //DETAILVIEW LAYOUT
        //kind Of course
        $koc = '';
        $koc .='<span id="class_koc">'.$this->bean->koc_name.'</span>&nbsp &nbsp &nbsp '.$GLOBALS['mod_strings']['LBL_LEVEL'].': <span>'.$this->bean->level.'</span>';
        if(!empty($this->bean->modules)){
            $koc .='&nbsp; &nbsp; &nbsp;'.$GLOBALS['mod_strings']['LBL_MODULE'].': <label>'.$this->bean->modules.'</label>';
        }

        $this->ss->assign('KOC',$koc);

        //upgrade to class
        $this->bean->load_relationship('j_class_j_class_1');
        $upgrade_to_class = reset($this->bean->j_class_j_class_1->getBeans());
        $atag = '<a href="#bwc/index.php?module=J_Class&action=DetailView&record='.$upgrade_to_class->id.'">'.$upgrade_to_class->name.'</a>';
        $this->ss->assign("UTC", $atag);

        //Upgrade Button

        if($this->bean->isupgrade == 0 && ACLController::checkAccess('J_Class', 'edit', true)){
            $btn = '<input title="'.$GLOBALS['mod_strings']['LBL_ISUPGRADE'].'" class="button" onclick="var _form = document.getElementById(\'formDetailView\'); _form.return_module.value=\'J_Class\'; _form.return_action.value=\'DetailView\'; _form.isDuplicate.value=true; _form.action.value=\'EditView\'; _form.return_id.value=\''.$this->bean->id.'\';DOTB.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="'.$GLOBALS['mod_strings']['LBL_UPGRADE_TITLE'].'" id="duplicate_button">';
            $this->ss->assign("UPGRADE_BUTTON", $btn);
        }

        //end
        $this->ss->assign('checkAccess', ACLController::checkAccess('J_Class', 'edit', true));

        //Get list Session & Parse to Json
        $sss = get_list_lesson_by_class($this->bean->id);
        $arr        = array();
        $defautDate = '';
        $week_nos   = array();
        $today = $timedate->nowDbDate();
        //tạo ra lịch học gần đây
        if($today < $this->bean->start_date){
            $start_sch = date('Y-m-d', strtotime($this->bean->start_date));
            $end_sch = date('Y-m-d', strtotime("+14 days " . $this->bean->start_date));
        }elseif($today >= $this->bean->start_date && $today <= $this->bean->end_date){
            $start_sch = date('Y-m-d', strtotime("-7 days " . $today));
            $end_sch = date('Y-m-d', strtotime("+7 days " . $today));
        }elseif($today > $this->bean->end_date){
            $start_sch = date('Y-m-d', strtotime("-14 days " . $this->bean->end_date));
            $end_sch = date('Y-m-d', strtotime($this->bean->end_date));
        }

        foreach ($sss as $ss){
            $arr[] = array(
                'id'    => $ss['primaryid'],
                'dhour' => $ss['delivery_hour'],
                'start' => $ss['date_start_tz'],
                'end'   => $ss['date_end_tz'],
            );
            if($ss['date'] >= $today && empty($defautDate)) $defautDate = $ss['date'];
            if(!in_array($ss['week_no'],$week_nos)) $week_nos[] = $ss['week_no'];

            //Custom short view schedule
            if(!empty($start_sch) && !empty($end_sch)){
                $dstring = date('D', strtotime($ss['date'])).' '.$timedate->to_display_time($ss['date_start']).' - '.$timedate->to_display_time($ss['date_end']);
                if($ss['date'] >= $start_sch && $ss['date'] <= $end_sch && !in_array($dstring, $arraySche)) $arraySche[$dstring] = $dstring;
            }
        }
        $json_ss = htmlentities(json_encode($arr));
        $this->ss->assign("json_ss", $json_ss);


        // Display schedule
        if(!empty($arraySche)) $short_schedule = $arraySche;
        else $short_schedule = json_decode(html_entity_decode($this->bean->short_schedule),true);
        if (!empty($short_schedule)){
            $html = '<ul class="weekdays-list">';
            $appList = $GLOBALS['app_list_strings']['dayoff_teacher_contract_list'];
            $s_s = array();
            $appList2 = array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
            foreach ($short_schedule as $key => $value){
                $wd = substr($key, 0, 3);
                $td = substr($key, 3);
                if(!in_array($td,$s_s[$wd])) $s_s[$wd] .= ((!empty($s_s[$wd]))? ("\n".$td) : $td);
            }
            foreach($appList2 as $v) {if(array_key_exists($v,$s_s)) $s_s2[$v]=$s_s[$v];}
            foreach($s_s2 as $key => $value)
                $html .= '<li rel="tooltip" class="weekdays-day item-wrapper simptip-position-top simptip-movable simptip-multiline" data-tooltip="'.$value.'">'.$appList[$key].'</li>';
            $html .= '</ul>';
        }
        $this->ss->assign("HELP_SCHEDULE", $timedate->to_display_date($start_sch).'->'.$timedate->to_display_date($end_sch));
        $this->ss->assign("SCHEDULE", $html);

        //today Variable
        $this->ss->assign("today",$timedate->nowDate());

        //Handle Schedule Teacher / TA
        $this->ss->assign("SCHEDULE_FOR", get_select_options_with_id($GLOBALS['app_list_strings']['teacher_schedule_list'],'Teacher'));
        $this->ss->assign('cancel_reason_list',get_select_options_with_id($GLOBALS['app_list_strings']['cancel_reason_list'],''));
        $this->ss->assign('teaching_type_options',get_select_options_with_id($GLOBALS['app_list_strings']['teaching_type_options'],''));
        $this->ss->assign('next_session_date',$timedate->to_display_date($defautDate,false));
        $this->ss->assign('can_drop_revenue', $current_user->isAdminForModule('J_Class'));
        $this->ss->assign('ernCf', loadEnrollmentConfig()); //Enrollment configs
        // create option for function export attendance list
        $lessonListHtml = "";
        foreach($week_nos as $lesson => $week_no) {
            $lessonListHtml .= "<option value='{$week_no}'>".$week_no."</option>";
        }

        $this->ss->assign('LESSON_LIST',$lessonListHtml);

        //Check Role Button Export
        $btnExport = "";
        if(ACLController::checkAccess('J_Class', 'export', true))
            $btnExport .= '<input type="button" class="button" id="export_other_file" name="export_other_file" value="'.$GLOBALS['mod_strings']['BTN_EXPORT_CERTIFICATE'].'" />';

        //   if($current_user->id == '1')
        //            $btnExport .= '<input type="button" class="button primary" id="btn_fixIssueSituation" value="Generate Situation Again" />';
        //
        $this->ss->assign("BTN_EXPORT", $btnExport);

        $this->bean->closed_date =$timedate->nowDate();
        //Create Gradebook
        $countGb = $GLOBALS['db']->getOne("SELECT COUNT(id) count FROM j_class_j_gradebook_1_c WHERE deleted = 0 AND j_class_j_gradebook_1j_class_ida = '{$this->bean->id}'");
        if(empty($countGb)){

            $q1 = "SELECT content FROM j_kindofcourse WHERE id = '{$this->bean->koc_id}'";
            $r1 = $GLOBALS['db']->fetchOne($q1);
            if(!empty($r1['content'])){
                $KOCContents      = json_decode(html_entity_decode($r1['content']),true);

                foreach($KOCContents as $key => $value){
                    $gbsettinggroupID = $value['gbsettinggroup_id'];
                    if($value['levels'] == $this->bean->level && !empty($gbsettinggroupID)){
                        $q2 = "SELECT DISTINCT
                        IFNULL(l1.id, '') gbconfig_id,
                        IFNULL(l1.type, '') type,
                        IFNULL(l1.content,'') content
                        FROM j_gradebooksettinggroup
                        INNER JOIN j_gradebookconfig l1 ON j_gradebooksettinggroup.id = l1.gbsettinggroup_id AND l1.deleted = 0
                        WHERE (j_gradebooksettinggroup.id = '$gbsettinggroupID')
                        AND j_gradebooksettinggroup.deleted = 0";
                        $rs2 = $GLOBALS['db']->query($q2);
                        while($row = $GLOBALS['db']->fetchByAssoc($rs2)){
                            $grade          = new J_Gradebook();
                            $grade->type    = $row['type'];
                            $grade->j_class_j_gradebook_1j_class_ida = $this->bean->id;
                            $grade->date_input = date('Y-m-d');
                            $grade->gradebook_config_id = $row['gbconfig_id'];
                            $grade->grade_config = $row['content'];

                            $grade->assigned_user_id    = $this->bean->assigned_user_id;
                            $grade->team_id             = $this->bean->team_id;
                            $grade->team_set_id         = $this->bean->team_id;
                            $grade->save();
                        }
                    }
                }

            }

        }
        //LMS Course link
        if(!empty($this->bean->lms_course_id)){
            require_once("custom/include/utils/lms_helpers.php");
            $res = getLMSConfig();
            $lms        = json_decode(html_entity_decode($this->bean->lms_content), true);
            $url1 = 'https://'.$res->apiHost. "/courses/".$this->bean->lms_course_id;
            $course_link = '<a href="'.$url1.'" target="_blank" title="'.$url1.'"> '.$lms['name'].' </a>';

            $url2 = 'https://'.$res->apiHost. "/courses/".$this->bean->lms_template_id;
            $template_link = '<a href="'.$url2.'" target="_blank" title="'.$url2.'"> '.'>>Blueprint Course<<'.' </a>';

            $this->ss->assign('course_link', $course_link);
            $this->ss->assign('template_link', $template_link);
            $this->ss->assign('lms_enable', (int)$res->enable);
        }

        //Generate Online
        $online_options = array(
            'Google Meet' => 'Google Meet',
            'ClassIn' => 'ClassIn'
        );
        $this->ss->assign('go_learning_type', '<select id="go_learning_type" name="go_learning_type">'.get_select_options($online_options,'').'</select>');

        //Show avatar class: Nam Nguyen
        $picture_class = $GLOBALS['db']->getOne("SELECT picture FROM j_class WHERE deleted = 0 AND id = '{$this->bean->id}'");
        $srcImage = 'index.php?entryPoint=download&id='.$picture_class.'&type=DotbFieldImage&isTempFile=1&isProfile=1';


        if($picture_class != ""){
            $this->ss->assign('picture_class', '<div class="icon-image-choose" style="display: block;width: 40px;height: 40px;border: 2px solid #e6eaed;    border-radius: 6px;    margin: 10px;    position: relative;"><img src="'.$srcImage.'" alt="" width="40" height="40" style="float: left;"></div>');
        } else{
            $this->ss->assign('picture_class',"");
        }

        //ClassIn Settings
        require_once ('include/externalAPI/ClassIn/ExtAPIClassIn.php');
        if (ExtAPIClassIn::hasAPIConfig()){
            $shownSetting = array();
            $onlineLearningSetting = json_decode(html_entity_decode($this->bean->onl_setting), true);
            //Find teachers name
            if(!empty($onlineLearningSetting['classin_head_teacher']))
                $shownSetting['head_teacher'] = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid = '{$onlineLearningSetting['classin_head_teacher']}' AND deleted = 0");
            if(!empty($onlineLearningSetting['classin_assistant']))
                $shownSetting['assistant'] = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid = '{$onlineLearningSetting['classin_assistant']}' AND deleted = 0");

            $auditorsName = array();
            foreach ($onlineLearningSetting['classin_auditors'] as $auditor){
                if(!empty($auditor))
                    $auditorsName[] = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid = '{$auditor}' AND deleted = 0");
            }
            $shownSetting['auditors'] = implode(',', $auditorsName);
            $shownSetting['recording'] = $onlineLearningSetting['classin_recording'] == 1 ? 'YES' : 'NO';
            $shownSetting['students_on_stage'] = '1v' . $onlineLearningSetting['classin_on_stage'];
            $shownSetting['folder_name'] = $onlineLearningSetting['classin_folder_option'] !== '0' ? $onlineLearningSetting['classin_folder_name'] : '-None-';

            $this->ss->assign('classin_setting', $shownSetting);
            $this->ss->assign('onl_enable', $this->bean->onl_status);

        }else{
            // Không có config ClassIn thì ẩn panel liên quan ClassIn settings
            unset($this->dv->defs['panels']['LBL_EDITVIEW_PANEL_ONL']);
        }
        $this->ss->assign('CONFIG_ACTIVE', $this->bean->onl_status === '1');
        $app_list_strings = return_app_list_strings_language($GLOBALS['current_language']);
        $this->ss->assign('ONLINE_LEARNING_ACTIVE', $app_list_strings['online_learning_status_options'][$this->bean->onl_status]);

        $this->ss->assign('dl_reason_for_options', $app_list_strings['dl_reason_for_options']);
        //Change settle date
        if (DotbACL::checkField('J_StudentSituations', 'settle_date', 'edit', array("owner_override" => false)))
            $this->ss->assign('ce_settle_date', 1);
        else $this->ss->assign('ce_settle_date', 0);

        //get start_lesson
        $start_lesson = $GLOBALS['db']->getOne("SELECT CONCAT(meetings.lesson_number, '. ',IFNULL(l1.name,''))
            FROM meetings LEFT JOIN j_syllabus l1 ON meetings.syllabus_id = l1.id AND l1.deleted = 0
            WHERE meetings.deleted = 0 AND meetings.ju_class_id = '{$this->bean->id}' AND meetings.session_status <> 'Cancelled' AND meetings.lesson_number = '{$this->bean->start_lesson}'");
        $this->ss->assign('start_lesson', htmlspecialchars_decode($start_lesson));

        if($current_user->isAdminForModule('J_Class')){
            if((int)$res->enable || $this->bean->onl_status)
                $bt_admin_li .= '<li><a id="btn_lms_sync" href="#" >'.$GLOBALS['mod_strings']['LBL_LMS_SYNC'].'</a></li>';
            $bt_admin_li .= '<li><a id="btn_restore_att" href="#" >'.$GLOBALS['mod_strings']['LBL_BTN_RESTORE_ATT'].'</a></li>';
            $bt_admin_ul='<ul class="clickMenu fancymenu" style="margin-left: 5px !important;"><li class="dotb_action_button"><a>'.$GLOBALS['mod_strings']['LBL_ADMIN_ACTION'].'</a><ul class="subnav" style="display: none;">'.$bt_admin_li.'</ul><span class="ab subhover"></span></li></ul> ';
            $this->ss->assign('bt_admin_ul', $bt_admin_ul);
        }
        parent::display();
    }

    function _displaySubPanels(){
        require_once ('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($this->bean, $this->module);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_class_c_teachers_1']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_class_j_teachercontract_1']);
        echo $subpanel->display();
    }
}
