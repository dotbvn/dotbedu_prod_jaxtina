<?php

class J_ClassViewEdit extends ViewEdit
{
    public function display()
    {
        global $current_user, $timedate;
        $this->ss->assign('MOD', return_module_language($GLOBALS['current_language'], 'J_Class'));

        if ($_REQUEST['isDuplicate'] == "true") {
            $this->bean->j_class_j_class_1_name = $this->bean->name;
            $this->bean->j_class_j_class_1j_class_ida = $this->bean->id;
            $this->bean->c_teachers_j_class_1c_teachers_ida = $this->bean->c_teachers_j_class_1c_teachers_ida;
            $this->bean->c_teachers_j_class_1_name = $this->bean->c_teachers_j_class_1_name;
            //get list for function export student list
            $this->bean->class_code = '';
            //        $this->bean->name = 'Auto-Generate';
            $this->bean->name = '';
            $this->bean->hours = '';
            $class_case = 'create';
            $this->bean->onl_status = '0';
            $this->ss->assign('class_case', $class_case);
            $this->ss->assign('isupgrade', true);
            $this->ss->assign('upgrade_from_end_date', $timedate->to_display_date($this->bean->end_date,false) );
            $this->ss->assign('lessonplan_id', $this->bean->lessonplan_id);

            //get start_lesson
            $this->ss->assign('lessons', array(1 => '1. -default-'));
        } else {
            //In Case create new
            if (empty($this->bean->id)) {
                $this->bean->class_code = '';
                //        $this->bean->name = 'Auto-Generate';
                $class_case = 'create';
                //get start_lesson
                $this->ss->assign('lessons', array(1 => '1. -default-'));
            } else { //In Case edit
                $class_case = 'edit';
                //Alert Upgrade Class
                $this->bean->load_relationship('j_class_j_class_1');
                $upgrade_to_class = reset($this->bean->j_class_j_class_1->getBeans());
                $this->ss->assign('upgrade_to_name', $upgrade_to_class->name);
                $this->ss->assign('upgrade_to_id', $upgrade_to_class->id);
                $this->ss->assign('lessonplan_id', $this->bean->lessonplan_id);

                //get start_lesson
                $start_lesson = $GLOBALS['db']->getOne("SELECT CONCAT(lesson_number, '. ',IFNULL(l1.name,''))
                    FROM meetings LEFT JOIN j_syllabus l1 ON meetings.syllabus_id = l1.id AND l1.deleted = 0
                    WHERE meetings.deleted = 0 AND meetings.ju_class_id = '{$this->bean->id}' AND meetings.session_status <> 'Cancelled' AND meetings.lesson_number = '{$this->bean->start_lesson}'");
                $this->ss->assign('lessons', array($this->bean->start_lesson => htmlspecialchars_decode($start_lesson)));

                //Checksum
                $this->ss->assign('checksum_code', getSumClassInfo($this->bean->id));
            }
            $this->ss->assign('class_case', $class_case);
        }

        if (ACLController::checkField('J_Class', 'class_code', 'edit')) $this->ss->assign('edit_class_code', true);
        else $this->ss->assign('edit_class_code', false);


        $this->ss->assign('schedule_by_options', $GLOBALS['app_list_strings']['schedule_by_list']);

        if (!empty($this->bean->id) && empty($this->bean->j_class_j_class_1j_class_ida)) //Loại bỏ TH Upgrade
            $or_selected = "OR j_kindofcourse.id = '{$this->bean->koc_id}'";

        $q1 = "SELECT DISTINCT
        IFNULL(j_kindofcourse.id, '') primaryid,
        IFNULL(j_kindofcourse.name, '') name,
        IFNULL(j_kindofcourse.kind_of_course, '') kind_of_course,
        IFNULL(j_kindofcourse.short_course_name, '') short_course_name,
        IFNULL(j_kindofcourse.content, '') content,
        IFNULL(l1.id, '') team_id,
        IFNULL(l1.name, '') team_name,
        IFNULL(j_kindofcourse.max_size,'') max_size,
        j_kindofcourse.date_entered j_kindofcourse_date_entered
        FROM j_kindofcourse
        INNER JOIN teams l1 ON j_kindofcourse.team_id = l1.id AND l1.deleted = 0
        WHERE ((j_kindofcourse.team_set_id IN (SELECT
        tst.team_set_id FROM team_sets_teams tst
        INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id AND team_memberships.user_id = '{$current_user->id}' AND team_memberships.deleted = 0)))
        AND j_kindofcourse.deleted = 0
        AND j_kindofcourse.status = 'Active'
        $or_selected
        ORDER BY CASE WHEN (j_kindofcourse.kind_of_course = '' OR j_kindofcourse.kind_of_course IS NULL) THEN 0";
        $kocc = $GLOBALS['app_list_strings']['kind_of_course_list'];
        $count_koc = 1;
        foreach ($kocc as $koc => $value) $q1 .= " WHEN j_kindofcourse.kind_of_course = '$koc' THEN " . $count_koc++;
        $q1 .= " ELSE $count_koc END ASC, j_kindofcourse.name ASC";
        $rs1 = $GLOBALS['db']->query($q1);
        //Generate html option
        $htm_koc = '<select style="width:200px" name="kind_of_course" id="kind_of_course">';
        $htm_koc .= '<option label="-' . $GLOBALS['mod_strings']['LBL_NONE'] . '-" value = "" >-' . $GLOBALS['mod_strings']['LBL_NONE'] . '-</option>';
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
            if ($this->bean->koc_id == $row['primaryid'])
                $str_selected = 'selected';
            else $str_selected = '';

            $htm_koc .= '<option koc_id="' . $row['primaryid'] . '" ' . $str_selected . ' json="' . $row['content'] . '" label="' . $row['name'] . '" max_size ="' . $row['max_size'] . '" value="' . $row['kind_of_course'] . '">' . $row['name'] . '</option>';
        }
        $htm_koc .= '</select>';
        $this->ss->assign('htm_koc', $htm_koc);

        // Display schedule
        $html = '';
        $short_schedule = json_decode(html_entity_decode($this->bean->short_schedule),true);
        if (!empty($short_schedule)){
            $html = '<ul class="weekdays-list">';
            $s_s = array();
            $appList = $GLOBALS['app_list_strings']['dayoff_teacher_contract_list'];
            $appList2 = array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
            foreach ($short_schedule as $key => $value){
                $wd = substr($key, 0, 3);
                $td = substr($key, 3);
                if(!in_array($td,$s_s[$wd])) $s_s[$wd] .= ((!empty($s_s[$wd]))? ("\n".$td) : $td);
            }
            //sort weekday
            foreach($appList2 as $v) {if(array_key_exists($v,$s_s)) $s_s2[$v]=$s_s[$v];}

            foreach($s_s2 as $key => $value)
                $html .= '<li class="weekdays-day simptip-position-top simptip-movable simptip-multiline" data-tooltip="'.$value.'">'.$appList[$key].'</li>';
            $html .= '</ul>';
        }

        $this->ss->assign("SCHEDULE", $html);

        //Add Multiple enum
        $levels = get_list_level();
        $this->ss->assign('dropdown_level', '<select style="width:150px" name="level" id="level">' . get_select_options($levels, $this->bean->level) . '</select>');

        //Mo quyen Edit Hour cho moi truong hop theo koc
        if (DotbACL::checkField('J_Class', 'kind_of_course', 'edit', array("owner_override" => true))
            || DotbACL::checkField('J_Class', 'koc_name', 'edit', array("owner_override" => true)) )
            $this->ss->assign('can_edit_koc', 1);
        else $this->ss->assign('can_edit_koc', 0);

        if (DotbACL::checkField('J_Class', 'is_change_schedule', 'edit', array("owner_override" => true)))
            $this->ss->assign('is_change_schedule', 1);
        else $this->ss->assign('is_change_schedule', 0);

        if (DotbACL::checkField('J_Class', 'is_change_startdate', 'edit', array("owner_override" => true)))
            $this->ss->assign('is_change_startdate', 1);
        else $this->ss->assign('is_change_startdate', 0);

        if (DotbACL::checkField('J_Class', 'hours', 'edit', array("owner_override" => true)))
            $this->ss->assign('can_edit_hour', 1);
        else $this->ss->assign('can_edit_hour', 0);

        if (DotbACL::checkField('J_Class', 'lms_template_id', 'edit', array("owner_override" => true)))
            $this->ss->assign('can_edit_lms', 1);
        else $this->ss->assign('can_edit_lms', 0);

        if (DotbACL::checkField('J_Class', 'teaching_rate', 'edit', array("owner_override" => true)))
            $this->ss->assign('can_edit_teaching', 1);
        else $this->ss->assign('can_edit_teaching', 0);

        $this->ss->assign('can_edit_revenue', 0);   //Tạm thời khóa chỉnh sửa revenue

        //Set layout timeslot
        $ful_wd = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $_weekdays = array();
        foreach ($ful_wd as $wd) {
            $_weekdays[] = array(
                'name' => $wd,
                'short_name' => substr($wd, 0, 3),
                'label' => translate('LBL_' . strtoupper($wd), 'J_Class'),
            );
        }
        $this->ss->assign('_weekdays', $_weekdays);

        if (!checkDataLockDate($this->bean->start_date))
            $this->ss->assign('change_start_date', 0);
        else
            $this->ss->assign('change_start_date', 1);

        if (DotbACL::checkField('J_Class', 'lms_template_id', 'edit', array("owner_override" => true))) {
            //Load Canvas Blueprint Course
            require_once("custom/include/utils/lms_helpers.php");
            //comment tạm
            $lms = getBlueprintCourse();
            $this->ss->assign('lms', $lms);
        }
        //Show avatar class: Nam Nguyen
        //Get all default avatar in folder
        $arrSrcNameImage = array();
        $arrNameImage = array();
        foreach (glob("custom/images/class_icon/*") as $filename) {
            $nameFileImage = substr($filename, strrpos($filename, '/')+1,-4);
            $srcFileImage = substr($filename, strrpos($filename, ''));
            array_push($arrSrcNameImage, $srcFileImage);
            array_push($arrNameImage, $nameFileImage);
        }
        $this->ss->assign('arraysrcimages',$arrSrcNameImage);
        $this->ss->assign('arraynameimages',$arrNameImage);

        $qr1 = "SELECT DISTINCT
        IFNULL(j_class.picture, '') picture
        FROM j_class
        WHERE deleted = 0
        AND id = '{$this->bean->id}'";
        $r1 = $GLOBALS['db']->query($qr1);
        while ($row = $GLOBALS['db']->fetchByAssoc($r1))
            $picture = $row['picture'];
        $returnNameAvatar = $picture;
        $returnSrcAvatar = $picture;
        $this->ss->assign('name_avatar',$returnNameAvatar);
        $this->ss->assign('picture_value',$returnSrcAvatar);

        //Online learning Settings
        require_once('include/externalAPI/ClassIn/ExtAPIClassIn.php');
        if (ExtAPIClassIn::hasAPIConfig()) {
            //Check acl for active field
            require_once('include/utils.php');
            $app_list_strings = return_app_list_strings_language($GLOBALS['current_language']);
            if (!DotbACL::checkField('J_Class', 'onl_status', 'edit', array("owner_override" => true))){
                // field onl_status
                $this->ss->assign('ACTIVE_READONLY', true);
                $this->ss->assign('HTML_ONL_LEARNING_STATUS', $app_list_strings['online_learning_status_options'][$this->bean->onl_status]);

                // field onl_setting
                $this->ss->assign('CLASSIN_READONLY', true);
                $shownSetting = array();
                $onlineLearningSetting = json_decode(html_entity_decode($this->bean->onl_setting), true);
                //Load head teacher
                if(!empty($onlineLearningSetting['classin_head_teacher']))
                    $shownSetting['head_teacher'] = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid = '{$onlineLearningSetting['classin_head_teacher']}' AND deleted = 0");

                //Find teachers name
                if(!empty($onlineLearningSetting['classin_assistant']))
                    $shownSetting['assistant'] = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid = '{$onlineLearningSetting['classin_assistant']}' AND deleted = 0");

                $auditorsName = array();
                foreach ($onlineLearningSetting['classin_auditors'] as $auditor){
                    if(!empty($auditor))
                        $auditorsName[] = $GLOBALS['db']->getOne("SELECT name FROM c_rooms WHERE onl_uid = '{$auditor}' AND deleted = 0");
                }
                $shownSetting['auditors'] = implode(',', $auditorsName);
                $shownSetting['recording'] = $onlineLearningSetting['classin_recording'] == 1 ? 'YES' : 'NO';
                $shownSetting['students_on_stage'] = '1v' . $onlineLearningSetting['classin_on_stage'];
                $shownSetting['folder_name'] = $onlineLearningSetting['classin_folder_option'] !== '0' ? $onlineLearningSetting['classin_folder_name'] : '-None-';
                $this->ss->assign('classin_setting', $shownSetting);
            }else{
                $onlineLearningSetting = json_decode(html_entity_decode($this->bean->onl_setting), true);

                // field onl_status
                $this->ss->assign('ACTIVE_READONLY', false);
                $this->ss->assign('HTML_ONL_LEARNING_STATUS', get_select_options_with_id($app_list_strings['online_learning_status_options'], $this->bean->onl_status == '1' ? '1' : ''));

                // field onl_setting
                $this->ss->assign('CLASSIN_READONLY', false);
                $htmlHeadTeacher = '<select style="width:200px" name="classin_head_teacher" id="classin_head_teacher">';
                $htmlAuditors = '<select style="width: 200px; display:none;" id="classin_auditors" multiple  >';
                $htmlAssistant = '<select style="width:200px" name="classin_assistant" id="classin_assistant">';
                //Load Head Teacher
                $htmlHeadTeacher .= '<option value="">-none-</option>';

                if(!empty($onlineLearningSetting['classin_head_teacher'])) $extSQL1 = " OR onl_uid = '{$onlineLearningSetting['classin_head_teacher']}'";
                $queryResult = $GLOBALS['db']->query("SELECT id, name, onl_uid classin_teacher_uid FROM c_rooms WHERE deleted = 0 AND (classin_user_type = 'Head Teacher' $extSQL1) AND type = 'online' AND status = 'Active' ");
                while ($row = $GLOBALS['db']->fetchByAssoc($queryResult)) $htmlHeadTeacher .= '<option value="' . $row['classin_teacher_uid'] . '" >' . $row['name'] . '</option>';

                $htmlHeadTeacher .= '</select>';
                //Load Assistant
                $htmlAssistant .= '<option value="">-none-</option>';
                $queryResult = $GLOBALS['db']->query("SELECT id, name, onl_uid as classin_teacher_uid FROM c_rooms WHERE deleted = 0 AND type = 'online' AND status = 'Active' AND ( classin_user_type = 'Teacher User' OR classin_user_type = 'Head Teacher' )");
                while ($row = $GLOBALS['db']->fetchByAssoc($queryResult)) {
                    $htmlAssistant .= '<option value="' . $row['classin_teacher_uid'] . '" >' . $row['name'] . '</option>';
                }
                $htmlAssistant .= '</select>';
                //Load Auditors
                $queryResult = $GLOBALS['db']->query("SELECT id, name, onl_uid as classin_teacher_uid FROM c_rooms WHERE deleted = 0 AND type ='online' AND classin_user_type = 'Regular User' AND status = 'Active' ");
                while ($row = $GLOBALS['db']->fetchByAssoc($queryResult)) {
                    $htmlAuditors .= '<option value="' . $row['classin_teacher_uid'] . '" >' . $row['name'] . '</option>';
                }
                $htmlAuditors .= '</select>';

                $this->ss->assign('HTML_CLASSIN_HEAD_TEACHER', $htmlHeadTeacher);
                $this->ss->assign('HTML_CLASSIN_ASSISTANT', $htmlAssistant);
                $this->ss->assign('HTML_CLASSIN_AUDITORS', $htmlAuditors);
            }
        }
        else{
            // Không có config ClassIn thì ẩn row liên quan ClassIn settings
            unset($this->ev->defs['panels']['lbl_editview_panel_onl']);
        }

        parent::display();
    }
}

?>
