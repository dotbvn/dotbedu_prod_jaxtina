<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
include_once("custom/include/_helper/junior_class_utils.php");
include_once("custom/include/_helper/junior_revenue_utils.php");
require_once('include/externalAPI/ClassIn/utils.php');


class logicClass{
    function addClassCode(&$bean, $event, $arguments){
        global $timedate;
        $code_field = 'class_code';
        if ($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save') {
            if (empty($bean->fetched_row) || $bean->fetched_row['level'] != $bean->level || $bean->fetched_row['kind_of_course'] != $bean->kind_of_course || $bean->fetched_row['start_date'] != $bean->start_date) {
                $q100 = "SELECT DISTINCT
                IFNULL(jk.short_course_name, '') short_course_name,
                IFNULL(jk.content, '') content
                FROM j_kindofcourse jk
                WHERE jk.deleted = 0 AND jk.id = '{$bean->koc_id}'";
                $codes = $GLOBALS['db']->fetchOne($q100);
                $options = json_decode(html_entity_decode($codes['content']), true);
                foreach ($options as $key => $option) {
                    if ($option['levels'] == $bean->level)
                        $level_code = $option['level_code'];
                }
                if (empty($level_code)) $level_code = substr($bean->level, 0, 3);

                $short_course_name = $codes['short_course_name'];
                $date = new DateTime($bean->start_date);
                $str_mid = mb_strtoupper(str_replace(' ', '', ($short_course_name . $level_code)), "UTF-8") . $date->format('y');
                $prefix = $GLOBALS['db']->getOne("SELECT code_prefix FROM teams WHERE id = '{$bean->team_id}'");
                $sep = '-';
                $first_pad = '0000';
                $padding = 4;
                $str_code = $prefix . $sep . $str_mid . $sep;
                if (!empty($bean->$code_field)) {
                    $num = substr($bean->$code_field, -$padding, $padding);
                    $bean->$code_field = $str_code . $num;
                    if (strpos($bean->name, $num) !== false)
                        $_POST['name_s'] = ''; //triger change name

                } else {
                    //Tạo mã mới
                    $table = $bean->table_name;
                    $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id <> '{$bean->id}' AND (LEFT($code_field, " . strlen($prefix) . ") = '" . $prefix . "') AND deleted = 0 ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";
                    $result = $GLOBALS['db']->query($query);
                    if ($row = $GLOBALS['db']->fetchByAssoc($result))
                        $last_code = $row[$code_field];
                    else
                        $last_code = $str_code . $first_pad;      //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $num = substr($last_code, -$padding, $padding);
                    $num++;
                    $pads = $padding - strlen($num);
                    $new_code = $str_code;

                    //preform the lead padding 0
                    for ($i = 0; $i < $pads; $i++)
                        $new_code .= "0";
                    $new_code .= $num;

                    //write to database - Logic: Before Save
                    $bean->$code_field = $new_code;
                }
            }

            if (!empty($_POST['name_s']))
                $bean->name = $_POST['name_s'];
            else
                $bean->name = substr($bean->$code_field, strpos($bean->$code_field, '-') + 1);
        }
        //Custom Class Code
        if (!empty($_POST['class_code_s']))
            $bean->class_code = $_POST['class_code_s'];
    }

    function handleSave(&$bean, $event, $arguments){
        global $timedate;
        if ($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save') {
            //Check lỗi nghiêm trọng
            if (empty($bean->id)) {
                $GLOBALS['log']->security("Serious error: DELETE SESSIONS - User ID: {$GLOBALS['current_user']->id} - Date: {$GLOBALS['timedate']->nowDate()}");
                die(translate('LBL_ALERT_STWR', 'J_Class'));
            }

            if (empty($bean->content) || $bean->content == 'null')
                die(translate('LBL_ALERT_STWR', 'J_Class'));
            //Fix bug remove upgrade class
            if (!empty($_POST['fetched_row_j_class_j_class_1j_class_ida']) && ($bean->j_class_j_class_1j_class_ida != $_POST['fetched_row_j_class_j_class_1j_class_ida'])) {
                $GLOBALS['db']->query("UPDATE j_class SET isupgrade = 0 WHERE id='{$_POST['fetched_row_j_class_j_class_1j_class_ida']}'");
            }

            // Checksum
            if (!empty($_POST['checksum_code'])
            && ($_POST['class_case'] == 'change_startdate' || $_POST['class_case'] == 'change_schedule')) {
                $checksum_code = getSumClassInfo($bean->id);
                if ($_POST['checksum_code'] != $checksum_code) {
                    $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Class','".$bean->id."'));";
                    echo '<script type="text/javascript">
                    window.parent.DOTB.App.alert.show(\'message-id\', {
                    level: \'error\',
                    messages: \''.str_replace(array("\r", "\n"), '', nl2br(translate('LBL_ALERT_CHECKSUM_ERR', 'J_Class'))).'\',
                    autoClose: false
                    });
                    '.$route.'
                    </script>';
                    die();
                }
            }

            //Set revenue rate
            if (empty($bean->revenue_rate) || $bean->revenue_rate <= 0) $bean->revenue_rate = 1;
            if (empty($bean->teaching_rate) || $bean->teaching_rate <= 0) $bean->teaching_rate = 1;

            //Making Session
            if ($_POST['class_case'] == 'create' || $_POST['class_case'] == 'change_startdate') {
                // TODO - get Quá trình học của học viên
                if ($_POST['class_case'] == 'change_startdate') {
                    $situationArr = GetStudentsProcessInClass($bean->id);
                    $ss_changelist = get_list_lesson_by_class($bean->id);//Kèm theo xoá Sesion Cancelled
                    foreach ($ss_changelist as $key => $ss) {
                        if($ss['session_status'] != 'Cancelled') $ss_changeDate[$ss['date_start']] = $ss;
                        $ss_remove[$ss['primaryid']] = $ss['primaryid'];
                    }
                }
                $json_content = json_decode(html_entity_decode($bean->content));
                //Create new record
                foreach ($json_content->sessions as $key => $ss_value) {
                    $db_start = $ss_value->start_time;
                    $db_end = $ss_value->end_time;
                    //Calculate duration
                    $d1 = strtotime($db_start);
                    $d2 = strtotime($db_end);
                    $interval = $d2 - $d1;
                    $minutes = round($interval / 60);

                    $ss = new Meeting();
                    $ss->id = create_guid();
                    $ss->new_with_id = true;
                    $ss->name = $bean->name;
                    $ss->date_start = $db_start;
                    $ss->duration_hours = floor($minutes / 60);
                    $ss->duration_minutes = floor($minutes % 60);
                    $ss->meeting_type = 'Session';
                    $ss->ju_class_id = $bean->id;
                    $ss->week_date = date('l', strtotime('+7 hour ' . $db_start));
                    $ss->update_vcal = false;
                    $ss->team_id = $bean->team_id;
                    $ss->team_set_id = $bean->team_set_id;
                    $ss->save();
                    //Restore Teacher/Room/Attendance
                    $sycSS = $ss_changeDate[$ss->date_start];
                    if (!empty($sycSS)
                    && ($sycSS['date_end'] == $ss->date_end)
                    && ($_POST['class_case'] == 'change_startdate')) {
                        $GLOBALS['db']->query("UPDATE meetings SET
                            teacher_id='{$sycSS['teacher_id']}',
                            teaching_type='{$sycSS['teaching_type']}',
                            teacher_cover_id='{$sycSS['teacher_cover_id']}',
                            sub_teacher_id='{$sycSS['sub_teacher_id']}',
                            room_id='{$sycSS['room_id']}'
                            WHERE id='{$ss->id}' AND deleted=0");
                        //Update Online Learning/LMS: Nếu buổi học thay đổi mà đã tạo Link Online rồi thì vẫn giữ lại
                        if(!empty($sycSS['external_id'])){
                            $GLOBALS['db']->query("UPDATE meetings SET
                                external_id='{$sycSS['external_id']}', type='{$sycSS['type']}',
                                creator='{$sycSS['creator']}', join_url='{$sycSS['join_url']}'
                                WHERE id='{$ss->id}' AND deleted=0");
                        }
                        $GLOBALS['db']->query("UPDATE c_attendance SET meeting_id='{$ss->id}' WHERE meeting_id='{$sycSS['primaryid']}' AND deleted=0");
                        $GLOBALS['db']->query("UPDATE j_loyalty SET meeting_id='{$ss->id}' WHERE meeting_id='{$sycSS['primaryid']}' AND deleted=0");
                    }
                }
                //Delete record
                if (!empty($ss_remove)) {
                    $ss_rmv = "'" . implode("','", array_keys($ss_remove)) . "'";
                    //Giảm tải cho table meetings_contacts
                    $GLOBALS['db']->query("DELETE FROM meetings_contacts WHERE meeting_id IN ($ss_rmv)");
                    $GLOBALS['db']->query("DELETE FROM meetings_leads WHERE meeting_id IN ($ss_rmv)");
                    $GLOBALS['db']->query("UPDATE meetings SET deleted = 1, date_modified='{$timedate->nowDb()}' WHERE id IN ($ss_rmv)"); //Không đc xóa vì để chạy Cron job generate lịch Online
                    $GLOBALS['db']->query("UPDATE c_attendance SET deleted=1, name='delete|by_change_schedule',  date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE meeting_id IN ($ss_rmv) AND deleted=0");
                    $GLOBALS['db']->query("UPDATE j_loyalty SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE meeting_id IN ($ss_rmv) AND deleted=0");

                }

                //Update Session no & Class Start-End
                $resClass = updateClassSession($bean);
                $bean->start_date = $resClass['start_date'];
                $bean->end_date = $resClass['end_date'];
                $bean->short_schedule = $resClass['short_schedule'];

                //Add hoc vien vao sesson moi
                if ($_POST['class_case'] == 'change_startdate')
                    addStudentToNewSessions($situationArr, $bean->id);

            } elseif ($_POST['class_case'] == 'change_schedule') { //Incase edit
                $json_content = json_decode(html_entity_decode($bean->content));
                // TODO - get Quá trình học của học viên từ ngày change lịch đến cuối để add lại
                $situationArr = GetStudentsProcessInClass($bean->id, $bean->change_date_from);

                //Remove học viên ra khỏi lớp từ ngày đổi lịch
                $ss_changelist = get_list_lesson_by_class($bean->id, $bean->change_date_from);
                if (!empty($ss_changelist)) {
                    $ss_rmv = "'" . implode("','", array_column($ss_changelist, 'primaryid')) . "'";
                    //Giảm tải cho table meetings_contacts , meeting
                    $GLOBALS['db']->query("DELETE FROM meetings_contacts WHERE meeting_id IN ($ss_rmv)");
                    $GLOBALS['db']->query("DELETE FROM meetings_leads WHERE meeting_id IN ($ss_rmv)");
                }

                foreach ($ss_changelist as $key => $ss) $ss_changeDate[$ss['date_start']] = $ss;
                foreach ($json_content->sessions_remove as $key => $session_id) $ss_remove[$session_id] = $session_id;

                foreach ($json_content->sessions as $key => $ss_value) {
                    $db_start = $ss_value->start_time;
                    $db_end = $ss_value->end_time;
                    $new_with_id = false;
                    //Calculate duration
                    $d1 = strtotime($db_start);
                    $d2 = strtotime($db_end);
                    $interval = $d2 - $d1;
                    $minutes = round($interval / 60);
                    if (strlen($key) == 36) //Edit Record
                        $ss = BeanFactory::getBean('Meetings', $key);
                    else {
                        $ss = new Meeting(); //Create Record
                        $ss->id = create_guid();
                        $new_with_id = true;
                        $ss->new_with_id = $new_with_id;
                    }

                    $ss->name = $bean->name;
                    $ss->date_start = $db_start;
                    $ss->duration_hours = floor($minutes / 60);
                    $ss->duration_minutes = floor($minutes % 60);
                    $ss->meeting_type = 'Session';
                    $ss->ju_class_id = $bean->id;
                    $ss->update_vcal = false;
                    $ss->week_date = date('l', strtotime('+7 hour ' . $db_start));
                    $ss->team_id = $bean->team_id;
                    $ss->team_set_id = $bean->team_set_id;
                    $ss->save();
                    //Restore Teacher/Room/Attendance
                    $sycSS = $ss_changeDate[$ss->date_start];
                    if (!empty($sycSS)
                    && $sycSS['date_end'] == $ss->date_end
                    && $new_with_id) {
                        $GLOBALS['db']->query("UPDATE meetings SET
                            teacher_id='{$sycSS['teacher_id']}',
                            teaching_type='{$sycSS['teaching_type']}',
                            teacher_cover_id='{$sycSS['teacher_cover_id']}',
                            sub_teacher_id='{$sycSS['sub_teacher_id']}',
                            room_id='{$sycSS['room_id']}'
                            WHERE id='{$ss->id}' AND deleted=0");
                        //Update Online Learning/LMS: Nếu buổi học thay đổi mà đã tạo Link Online rồi thì vẫn giữ lại
                        if(!empty($sycSS['external_id'])){
                            $GLOBALS['db']->query("UPDATE meetings SET
                                external_id='{$sycSS['external_id']}', type='{$sycSS['type']}',
                                creator='{$sycSS['creator']}', join_url='{$sycSS['join_url']}'
                                WHERE id='{$ss->id}' AND deleted=0");
                        }

                        $GLOBALS['db']->query("UPDATE c_attendance SET meeting_id='{$ss->id}' WHERE meeting_id='{$sycSS['primaryid']}' AND deleted=0");
                        $GLOBALS['db']->query("UPDATE j_loyalty SET meeting_id='{$ss->id}' WHERE meeting_id='{$sycSS['primaryid']}' AND deleted=0");
                    }
                }

                if (!empty($ss_remove)) {
                    $ss_rmv = "'" . implode("','", $ss_remove) . "'";
                    $GLOBALS['db']->query("UPDATE meetings SET deleted = 1, date_modified='{$timedate->nowDb()}' WHERE id IN ($ss_rmv)"); //Không đc xóa vì để chạy Cron job generate lịch Online
                    $GLOBALS['db']->query("UPDATE c_attendance SET deleted=1, name='delete|by_change_schedule', date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE meeting_id IN ($ss_rmv) AND deleted=0");
                    $GLOBALS['db']->query("UPDATE j_loyalty SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE meeting_id IN ($ss_rmv) AND deleted=0");
                }


                //Update Session no & Class Start-End
                $resClass = updateClassSession($bean);
                $bean->start_date = $resClass['start_date'];
                $bean->end_date = $resClass['end_date'];
                $bean->short_schedule = $resClass['short_schedule'];

                //GET - Danh sách các buổi học sau khi đã change. TỪ ngày change đến cuối lớp
                addStudentToNewSessions($situationArr, $bean->id, $bean->change_date_from);
            }

            //check status
            $today = $timedate->nowDbDate();

            //Update TH edit lớp class_name
            if (!empty($bean->fetched_row) && $bean->fetched_row['name'] != $bean->name) {
                $q10 = "UPDATE meetings SET name='{$bean->name}' WHERE name != '{$bean->name}' AND ju_class_id='{$bean->id}' AND deleted = 0 AND meeting_type = 'Session'";
                $GLOBALS['db']->query($q10);
            }

            //Update team id
            $_POST['team_set_id'] = $bean->fetched_row['team_set_id'];

            //Remove Temp data
            if (!empty($json_content) && $_POST['class_case'] != 'edit')
                $bean->content = json_encode(array('schedule' => $json_content->schedule), JSON_UNESCAPED_UNICODE);


            //Save default picture for avatar class: Nam Nguyen
            if (!empty($_POST['imagefile_picture'])) {
                $class = BeanFactory::getBean('J_Class', $bean->id);
                $name = create_guid();
                if (!is_dir('upload/')) {
                    mkdir('upload/');
                }
                $file = 'upload/' . $name;
                $file1 = '';
                foreach (glob("custom/images/class_icon/*") as $filename) {
                    $nameFileImage = substr($filename, strrpos($filename, '/') + 1, -4);
                    if ($_POST['imagefile_picture'] == $nameFileImage) {
                        $file1 = substr($filename, strrpos($filename, ''));
                        break;
                    }
                }
                file_put_contents($file, file_get_contents($file1));
                $class->picture = $name;
            }
        }
        //update lesson number for class
        if($_POST['class_case'] == 'edit'){
            if($bean->fetched_row['syllabus_by'] != $bean->syllabus_by
            || $bean->fetched_row['lessonplan_id'] != $bean->lessonplan_id
            ){
                $resClass = updateClassSession($bean);
                $bean->start_date = $resClass['start_date'];
                $bean->end_date = $resClass['end_date'];
                $bean->short_schedule = $resClass['short_schedule'];
            }
        }


        //IMPORT TASK - CLASS
        if ($_POST['module'] == 'Import') {
            $bean->level = str_replace('^', '', $bean->level);
            //Get ID Student
            $koc_id = $GLOBALS['db']->getOne("SELECT id FROM j_kindofcourse WHERE name LIKE '%{$bean->kind_of_course} - " . intval($bean->hours) . "%'");
            if (!empty($koc_id))
                $bean->koc_id = $koc_id;
            else {
                $koc_id = $GLOBALS['db']->getOne("SELECT id FROM j_kindofcourse WHERE name LIKE '%{$bean->kind_of_course}%' ORDER BY name ASC");
                if (!empty($koc_id))
                    $bean->koc_id = $koc_id;
            }

            $user_id = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = '{$bean->created_by}'");
            if (!empty($user_id))
                $bean->created_by = $user_id;

            $bean->modified_user_id = $bean->created_by;
            $bean->date_modified = $bean->date_entered;
            $bean->team_set_id = $bean->team_id;
        }


    }

    function handleLMS(&$bean, $event, $arguments){
        require_once("custom/include/utils/lms_helpers.php");
        //Create New Course
        if (empty($bean->lms_course_id) && !empty($bean->lms_template_id)) {
            //Create Empty Course
            $result = createNewCourse($bean);
            if ($result['status'] == 'success') {
                $bean->lms_course_id = $result['course_id'];
                $bean->lms_content = json_encode($result['course']);
                //Update Blueprinf Course
                $result = updateAssociatedBlueprint($bean->lms_course_id, $bean->lms_template_id);
                $bean->lms_linked_template = ($result['status'] == 'success') ? 1 : 0;

                //Update Course Event
                $result2 = updateCourseEvent($bean, 'offer');
            }
        }
        //sync data class
        if (!empty($bean->lms_course_id))
            if (($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save') || $bean->lms_sync)
                syncEnrollment($bean->id);

            //Clear Blueprint
            if (!empty($bean->fetched_row['lms_template_id'] && empty($bean->lms_template_id))) {
            $result = updateCourseEvent($bean, 'delete'); //Delete
            if ($result['status'] == 'success') {
                $bean->lms_course_id = '';
            }
        }
        //Chỉnh sửa Class
        if (!empty($bean->lms_course_id) && !empty($bean->fetched_row)) {
            //Thay đổi template
            if (!empty($bean->lms_template_id)
                && ($bean->fetched_row['lms_template_id'] != $bean->lms_template_id
                    || ($bean->lms_sync && !$bean->lms_linked_template))
                && !empty($bean->fetched_row['lms_template_id'])) {
                    //change Blueprinf Course
                    $result = changeAssociatedBlueprint($bean->lms_course_id, $bean->fetched_row['lms_template_id'], $bean->lms_template_id);
                    $bean->lms_linked_template = ($result['status'] == 'success') ? 1 : 0;
            }


            //Thay đổi tên, ngày start/End hoac Thay đổi status
            if ($bean->fetched_row['name'] != $bean->name
            || $bean->fetched_row['start_date'] != $bean->start_date
            || $bean->fetched_row['end_date'] != $bean->end_date
            || $bean->fetched_row['status'] != $bean->status) {
                $result = updateCourse($bean);
                if ($result['status'] == 'success')
                    $bean->lms_course_id = $result['course_id'];
                $bean->lms_content = json_encode($result['course']);
            }
        }
        $bean->lms_auto_enroll = 0;
    }

    function handleAfterSave(&$bean, $event, $arguments){
        if ($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save') {
            //save is upgrade
            if (!empty($bean->j_class_j_class_1j_class_ida)) $GLOBALS['db']->query("UPDATE j_class SET isupgrade = 1 WHERE id='{$bean->j_class_j_class_1j_class_ida}'");
        }
        //Update TH edit lớp Team_id
        if ($_POST['team_set_id'] != $bean->team_set_id) {
            $q10 = "UPDATE meetings SET team_id='{$bean->team_id}', team_set_id='{$bean->team_set_id}' WHERE ju_class_id='{$bean->id}' AND deleted=0 AND meeting_type='Session'";
            $GLOBALS['db']->query($q10);
        }

        //Set Count Attendances
        updateClassAttendance($bean->id);
        updateClassStudent($bean->id);
        updateClassStatusUtil($bean->id);
    }

    function handleMassUpdateCancelSesssion(&$bean, $event, $arguments){
        //        if (!empty($_REQUEST['__dotb_url'])){
        //            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
        //            $url_module = $arrayUrl[1];
        //            $url_action = $arrayUrl[2];
        //        }
        //        if ($url_module == $bean->module_name && $url_action == 'MassUpdate' && isset($arguments['dataChanges']['holiday_id'])) {
        //            $selected_holiday = $arguments['dataChanges']['holiday_id']['after'];
        //            $bean_holiday = BeanFactory::getBean('Holidays', $selected_holiday);
        //            $session_query = "SELECT
        //             IFNULL(mt.id , '') primaryid,
        //             IFNULL(mt.date, '') mt_date
        //             FROM meetings mt WHERE mt.deleted = 0 AND mt.ju_class_id = '{$bean->id}'
        //                    AND mt.date = '{$bean_holiday->holiday_date}'
        //                    AND mt.session_status <> 'Cancelled' AND mt.meeting_type = 'Session'
        //                    ";
        //            $session_list = $GLOBALS['db']->fetchArray($session_query);
        //            if (!empty($session_list)) {
        //                $session_id = array_column($session_list, 'primaryid');
        //                $session_id = "'" . implode("','", $session_id) . "'";
        //            }
        //        }
    }
    function checkBeforeDelete(&$bean, $event, $arguments){
        global $timedate;
        if (!empty($_REQUEST['__dotb_url'])) {
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $action = $arrayUrl[sizeof($arrayUrl) - 1];
            $module = $arrayUrl[sizeof($arrayUrl) - 2];
        }
        $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Class','" . $bean->id . "'));";
        if (($_REQUEST['module'] == $bean->module_name && $_REQUEST['action'] == 'Delete') || ($module == $bean->module_name && $action == 'MassUpdate')) {
            $sqlCountSitu = "SELECT DISTINCT
            COUNT(DISTINCT j_studentsituations.id) j_studentsituations__count
            FROM j_studentsituations
            INNER JOIN j_class l1 ON j_studentsituations.ju_class_id = l1.id AND l1.deleted = 0
            WHERE ((l1.id = '{$bean->id}')) AND j_studentsituations.type IN ('OutStanding', 'Demo', 'Enrolled') AND j_studentsituations.deleted = 0";
            $countSitu = $GLOBALS['db']->getOne($sqlCountSitu);
            if ($countSitu > 0) {
                echo '<script type="text/javascript">
                window.parent.DOTB.App.alert.show(\'message-id\', {
                level: \'error\',
                messages: '.translate('LBL_ALERT_DELETE_CLASS', 'J_Class').',
                autoClose: true});
                ' . $route . '
                </script>';
                die();
            } else {
                if (empty($bean->id)) {
                    $GLOBALS['log']->security("Error: DELETE SESSIONS - User ID: {$GLOBALS['current_user']->id} - Date: {$GLOBALS['timedate']->nowDate()}");
                    echo '<script type="text/javascript">
                    window.parent.DOTB.App.alert.show(\'message-id\', {
                    level: \'error\',
                    messages: '.translate('LBL_ALERT_STWR', 'J_Class').',
                    autoClose: true});
                    ' . $route . '
                    </script>';
                    die();
                }
                $ss_remove = get_list_lesson_by_class($bean->id);
                foreach ($ss_remove as $session_id => $ss) {
                    if (!empty($ss['primaryid'])) {
                        $GLOBALS['db']->query("UPDATE meetings SET deleted = 1, date_modified='{$timedate->nowDb()}' WHERE id ='{$ss['primaryid']}'"); //Không đc xóa vì để chạy Cron job generate lịch Online
                        $GLOBALS['db']->query("DELETE FROM meetings_contacts WHERE meeting_id ='{$ss['primaryid']}'");
                        $GLOBALS['db']->query("DELETE FROM meetings_leads WHERE meeting_id = '{$ss['primaryid']}'");
                    }
                }
                //remove attendance
                $GLOBALS['db']->query("UPDATE c_attendance SET deleted=1, name='delete|by_delete_class', date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE class_id='{$bean->id}' AND deleted=0");

                if (!empty($bean->j_class_j_class_1j_class_ida)) {
                    $GLOBALS['db']->query("UPDATE j_class SET isupgrade = 0 WHERE id='{$bean->j_class_j_class_1j_class_ida}'");
                }
                //Delete Gradebook
                $q11 = "SELECT DISTINCT
                IFNULL(j_gradebook.id, '') primaryid
                FROM j_gradebook
                INNER JOIN j_class_j_gradebook_1_c l1_1 ON j_gradebook.id = l1_1.j_class_j_gradebook_1j_gradebook_idb AND l1_1.deleted = 0
                INNER JOIN j_class l1 ON l1.id = l1_1.j_class_j_gradebook_1j_class_ida AND l1.deleted = 0
                WHERE (((l1.id = '{$bean->id}')))
                AND j_gradebook.deleted = 0";
                $rs11 = $GLOBALS['db']->query($q11);
                while ($row11 = $GLOBALS['db']->fetchByAssoc($rs11)) {
                    $GLOBALS['db']->query("UPDATE j_gradebook SET deleted = 1 WHERE id='{$row11['primaryid']}'");
                    $GLOBALS['db']->query("UPDATE j_gradebookdetail SET deleted = 1 WHERE gradebook_id='{$row11['primaryid']}'");
                }
                $GLOBALS['db']->query("UPDATE j_class_j_gradebook_1_c SET deleted = 1 WHERE j_class_j_gradebook_1j_class_ida = '{$bean->id}'");
            }

            //Delete LMS Course
            if (!empty($bean->lms_course_id)) {
                require_once("custom/include/utils/lms_helpers.php");
                $result = deleteCourse($bean);
            }

        } else {
            $GLOBALS['log']->security("Serious error: DELETE SESSIONS - User ID: {$GLOBALS['current_user']->id} - Date: {$GLOBALS['timedate']->nowDate()}");
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: '.translate('LBL_ALERT_STWR', 'J_Class').',
            autoClose: true});
            ' . $route . '
            </script>';
            die();
        }
    }

    ///to mau id va status Lap Nguyen
    function listViewColorClass(&$bean, $event, $arguments){
        if (!empty($_REQUEST['__dotb_url'])) {
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[sizeof($arrayUrl) - 1];
        }

        //optimize performance - only load when in list view
        if ((in_array($_REQUEST['view'], ['list', 'selection-list']) && $url_module == $bean->module_name) || $_REQUEST['action'] == 'Popup') {
            global $timedate;
            $max_size = $max_size_text = $bean->max_size;
            if(empty($bean->max_size)){
                $max_size = '∞';
                $max_size_text = translate('LBL_UNLIMIT', 'J_Class');
            }
            $title_ = translate('LBL_NUM_OF_STUDENT', 'J_Class').': '.$bean->number_of_student;
            $title_ .= "\n".translate('LBL_MAX_SIZE', 'J_Class').': '.$max_size_text;
            $bean->number_student = '<span align="center" style="color: '.(($bean->number_of_student < 1) ? '#DC143C' : '#468931').';font-weight: bold; white-space:nowrap;" title="'.$title_.'">' . $bean->number_of_student . ' / '.$max_size.'</span>';

            $bean->period = '';
            $q10 = "SELECT DISTINCT
            IFNULL(meetings.id, '') primaryid,
            DATE(CONVERT_TZ(meetings.date_end,'+00:00','+7:00')) date,
            meetings.date_end date_end, meetings.date_start date_start,
            IFNULL(l3.id, '') teacher_id, IFNULL(l3.full_teacher_name, '') teacher_name,
            IFNULL(l4.id, '') ta1_id, IFNULL(l4.full_teacher_name, '') ta1_name,
            IFNULL(l5.id, '') ta1_id, IFNULL(l5.full_teacher_name, '') ta2_name
            FROM meetings
            INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
            LEFT JOIN c_teachers l3 ON meetings.teacher_id = l3.id AND l3.deleted = 0
            LEFT JOIN c_teachers l4 ON meetings.teacher_cover_id = l4.id AND l4.deleted = 0
            LEFT JOIN c_teachers l5 ON meetings.sub_teacher_id = l5.id AND l5.deleted = 0
            WHERE (meetings.deleted = 0) AND (l2.id = '{$bean->id}') AND (meetings.session_status <> 'Cancelled')
            ORDER BY date_start ASC";
            $sss = $GLOBALS['db']->fetchArray($q10);
            $now = $timedate->nowDbDate();

            $start_date = str_replace('/', '-', $bean->start_date);
            $end_date = str_replace('/', '-', $bean->end_date);

            //TH lớp chưa bắt đầu
            if($now < $start_date){
                $start_sch = date('Y-m-d', strtotime($start_date));
                $end_sch = date('Y-m-d', strtotime("+14 days " . $start_date));
            }elseif($now >= $start_date && $now <= $end_date){
                $start_sch = date('Y-m-d', strtotime("-7 days " . $now));
                $end_sch = date('Y-m-d', strtotime("+7 days " . $now));
            }elseif($now > $end_date){
                $start_sch = date('Y-m-d', strtotime("-14 days " . $end_date));
                $end_sch = date('Y-m-d', strtotime($end_date));
            }
            $studied = 0;
            $arrayTea = $arraySche = array();
            $Tea1 = array();
            $Tea2 = array();
            $Tea3 = array();
            foreach ($sss as $ss) {
                if ($ss['date_end'] < $now) $studied++;

                if (!in_array($ss['teacher_id'], $arrayTea) && !empty($ss['teacher_id'])) {
                    $arrayTea[] = $ss['teacher_id'];
                    $Tea1[$ss['teacher_id']]    = '' . $ss['teacher_name'];
                }
                if (!in_array($ss['ta1_id'], $arrayTea) && !empty($ss['ta1_id'])) {
                    $arrayTea[] = $ss['ta1_id'];
                    $Tea3[$ss['ta1_id']]        = "[TA 1] " . $ss['ta1_name'];
                }
                if (!in_array($ss['ta2_id'], $arrayTea) && !empty($ss['ta2_id'])) {
                    $arrayTea[] = $ss['ta2_id'];
                    $Tea2[$ss['ta2_id']]        = "[TA 2] " . $ss['ta2_name'];
                }

                //Custom short view schedule
                if(!empty($start_sch) && !empty($end_sch)){
                    $dstring = date('D', strtotime($ss['date'])).' '.$timedate->to_display_time($ss['date_start']).' - '.$timedate->to_display_time($ss['date_end']);
                    if($ss['date'] >= $start_sch && $ss['date'] <= $end_sch && !in_array($dstring, $arraySche)) $arraySche[$dstring] = $dstring;
                }

            }

            global $current_user;

            $html_tea = '';

            if ($current_user->user_name == 'DotbCustomerSupportPortalUser') {
                $html_tea = implode(', ', array_merge($Tea1, $Tea2, $Tea3));
            } else {
                foreach (array_merge($Tea1, $Tea2, $Tea3) as $tea_id => $tea_name)
                    $html_tea .= '<a href="#C_Teachers/' . $tea_id . '">' . $tea_name . '</a><br>';
            }
            $bean->main_teachers = $html_tea;

            if(!empty($arraySche)) $short_schedule = $arraySche;
            else $short_schedule = json_decode(html_entity_decode($bean->short_schedule),true);

            // short schedule
            if (!empty($short_schedule)){
                $bean->short_schedule = '<ul class="weekdays-list">';
                $s_s = array();
                $html = '';
                $appList = $GLOBALS['app_list_strings']['dayoff_teacher_contract_list'];
                $appList2 = array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
                foreach ($short_schedule as $key => $value){
                    $wd = substr($key, 0, 3);
                    $td = substr($key, 3);
                    $html .= "\n" . $appList[$wd] . $td;
                    if(!in_array($td,$s_s[$wd])) $s_s[$wd] .= ((!empty($s_s[$wd]))? ("\n".$td) : $td);
                }
                //sort weekday
                foreach($appList2 as $v) {if(array_key_exists($v,$s_s)) $s_s2[$v]=$s_s[$v];}

                foreach($s_s2 as $key => $value)
                $bean->short_schedule .= '<li rel="tooltip" data-placement="top" class="weekdays-day" title="'.$value.'" data-original-title="'.$value.'">'.$appList[$key].'</li>';
                $bean->short_schedule .= '</ul>';
            }
            $period = format_number(($studied / count($sss)) * 100, 0, 0);
            $bean->period = '<div class="progress" rel="tooltip" data-placement="top" data-original-title="' . $studied . ' / ' . count($sss) . ' ' . translate('LBL_SS_COMPLETED', 'J_Class') . $html . '"><div class="bar" style="width: ' . $period . '%;"></div>';
        }
        //Color status
        switch ($bean->status) {
            case "In Progress":
                $colorClass = 'textbg_blue';
                break;
            case "Planning":
                $colorClass = 'textbg_green';
                break;
            case "Closed":
                $colorClass = 'textbg_black';
                break;
            case "Finished":
                $colorClass = 'textbg_crimson';
                break;
        }
        $bean->status = "<span class='full-width visible'><span class='label ellipsis_inline $colorClass' title='{$bean->status}'>" . $GLOBALS['app_list_strings']['status_class_list'][$bean->status] . "</span></span>";

    }

    //Custom filter class

    /**
    * @throws DotbQueryException
    */
    function addFilter(&$bean, $event, &$args){
        global $current_user;

        // Add by phgiahannn to handle custom filter for syllabus
        $url_module = '';
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[1];
        }
        // Only use this custom filter if feature "LessonPlan" is enabled
        if($url_module == $bean->module_name && isset($bean->field_defs['lessonplan_id'])
        && isset($_REQUEST['view']) && ($_REQUEST['view'] == 'list' || $_REQUEST['view'] == 'selection-list')){
            if(isset($_REQUEST['filter'])
                && (!empty(getkeypath($_REQUEST['filter'], 'ju_meetings.syllabus_type'))
                    || !empty(getkeypath($_REQUEST['filter'], 'ju_meetings.syllabus_topic')))){
                    $cnt_conditions = count($args[0]->where()->conditions);
                    $index = 0;
                    while($index < $cnt_conditions){
                        $field = $args[0]->where()->conditions[$index]->conditions[0]->field->field;
                        if (in_array($field, array("syllabus_topic", "syllabus_type"))) {
                            $mapping_fields = ($field == "syllabus_topic") ? "name" : "lesson_type";
                            $this->buildFilter($args[0], $index, $mapping_fields);
                            $this->buildFilter($args[1]['id_query'], $index, $mapping_fields);
                        }
                        $index++;
                    }
            }
        }
        // End
    }

    /**
    * Build filter for syllabus
    * @throws DotbQueryException
    */
    function buildFilter(&$filter_args, $index, $mapping_fields) {
        $filter = $filter_args->where()->conditions[$index];
        $operator = $filter->conditions[0]->operator;
        $values = $filter->conditions[0]->values;
        $table = $filter->conditions[0]->field->table;

        if (!array_key_exists('j_syllabus', $filter_args->join)) {
            $filter_args->joinTable("j_syllabus", array("alias" => "j_syllabus", "joinType" => "LEFT", "linkingTable" => true))->on()
            ->equalsField("$table.syllabus_id", "j_syllabus.id")
            ->equals("j_syllabus.deleted", '0');
        }
        $orWhere = new DotbQuery_Builder_Orwhere($filter_args);
        foreach ($filter->conditions as $condition) {
            if (!in_array($condition->field->field, array("syllabus_type", "syllabus_topic"))) {
                $orWhere->add($condition);
            }
        }
        unset($filter_args->where()->conditions[$index]);
        $condition = new DotbQuery_Builder_Condition($filter_args);
        $condition->setOperator($operator)->setField("j_syllabus.$mapping_fields")->setValues($values)->ignoreAcl();
        $orWhere->add($condition);
        array_unshift($filter_args->where()->conditions, $orWhere);
    }

    function afterDelete(&$bean, $event, &$args){
        //End the ClassIn Course if have
        if ($bean->onl_status === '1' && ExtAPIClassIn::hasAPIConfig() == true)
            endTheCourse($bean->onl_course_id);
    }

    function handleClassInCourse(&$bean, $event, $arg){
        // ClassIn
        if ($bean->onl_status === '1' && ExtAPIClassIn::hasAPIConfig() == true) {
            if (empty($bean->onl_course_id)) {
                $courseRes = createClassInCourse($bean);
                // Add students to course
                if ($courseRes) addStudentsToClassInCourse($bean->id);
                return;
            } elseif ($arg['isUpdate'] == true) {
                $name = $arg['dataChanges']['name'];
                $online_learning_active = $arg['dataChanges']['onl_status'];

                //Handle Change Setting
                $beforeSetting = json_decode(html_entity_decode($arg['dataChanges']['onl_setting']['before']), true);
                $afterSetting = json_decode(html_entity_decode($arg['dataChanges']['onl_setting']['after']), true);
                $changedSettings = array();
                foreach ($afterSetting as $key => $value) {
                    if ($value != $beforeSetting[$key]) $changedSettings[] = $key;
                }
                $current_onl_setting = json_decode(html_entity_decode($bean->onl_setting), true);
                //If this course has course ware folder => have to change folder name
                if (empty($beforeSetting['classin_folder_id'])) $beforeSetting['classin_folder_id'] = $current_onl_setting['classin_folder_id'];
                if ($name['before'] !== $name['after'] && !empty($beforeSetting['classin_folder_id']))
                    renameFolder($beforeSetting['classin_folder_id'], $name['after']);

                //update Auditor
                if (in_array('classin_auditors', $changedSettings)) {
                    $classin_auditors_before = isset($beforeSetting['classin_auditors'])?$beforeSetting['classin_auditors']:[];
                    $classin_auditors_after = isset($afterSetting['classin_auditors'])?$afterSetting['classin_auditors']:[];
                    updateClassInCourseAuditors($bean->id, $classin_auditors_before, $classin_auditors_after);
                }

                //update assistant
                if (in_array('classin_assistant', $changedSettings) && isset($afterSetting['classin_assistant']))
                    updateClassInCourseAssistants($bean->id, $afterSetting['classin_assistant']);

                //edit name or new head teacher or new resource folder
                if (!empty($arg['dataChanges']['name'])
                    || in_array('classin_head_teacher', $changedSettings)
                    || in_array('classin_folder_id', $changedSettings)
                    || $bean->lms_sync)
                    updateClassInCourse($bean);

                // if this course have to update head teacher, there will be update new head teacher in updateClassInCourse function
                // AND we MUST remove old head teacher from this course
                if (in_array('classin_head_teacher', $changedSettings))
                    removeCourseTeacher($bean->onl_course_id, $beforeSetting['classin_head_teacher']);

                // Tạo chức năng LMS/Online Sync
                if ($bean->lms_sync
                    || (!empty($online_learning_active) && $online_learning_active['before'] !== $online_learning_active['after']))
                    updateStudentsInEMSClassToClassInCourseHandle($bean->id);
            }
        }
    }
    //Add by HoangHvy to handle schedule_text when Bsend run
    function HandleBsend(&$bean, $event, $args){
        $main_schedule = json_decode(str_replace('&quot;','"', $bean->main_schedule), true);
        $is_first = true;
        foreach($main_schedule as $key => $weekday){
            if($is_first){
                $time .= $key;
                $is_first = false;
            } else {
                $time .= ', '.$key;
            }
        }
        $bean->schedule_text = $time;
    }
    //end

    function buildScheduleHTML(&$bean, $event, $args) {
        global $current_user;
        global $timedate;
        $now = $timedate->now();

        if ($current_user->user_name == 'DotbCustomerSupportPortalUser' && ($_REQUEST['view'] == 'record' || $bean->module_dir == 'J_Class')) {
            $q10 = "SELECT DISTINCT
            IFNULL(meetings.id, '') primaryid,
            DATE(CONVERT_TZ(meetings.date_end,'+00:00','+7:00')) date,
            meetings.date_end date_end, meetings.date_start date_start,
            IFNULL(l3.id, '') teacher_id, IFNULL(l3.full_teacher_name, '') teacher_name,
            IFNULL(l4.id, '') ta1_id, IFNULL(l4.full_teacher_name, '') ta1_name,
            IFNULL(l5.id, '') ta1_id, IFNULL(l5.full_teacher_name, '') ta2_name
            FROM meetings
            INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
            LEFT JOIN c_teachers l3 ON meetings.teacher_id = l3.id AND l3.deleted = 0
            LEFT JOIN c_teachers l4 ON meetings.teacher_cover_id = l4.id AND l4.deleted = 0
            LEFT JOIN c_teachers l5 ON meetings.sub_teacher_id = l5.id AND l5.deleted = 0
            WHERE (meetings.deleted = 0) AND (l2.id = '{$bean->id}') AND (meetings.session_status <> 'Cancelled')
            ORDER BY date_start ASC";
            $sss = $GLOBALS['db']->fetchArray($q10);
            $arraySche = array();
            $studied = 0;
            $arrayTea = array();
            $Tea1 = array();
            $Tea2 = array();
            $Tea3 = array();

            $start_date = str_replace('/', '-', $bean->start_date);
            $end_date = str_replace('/', '-', $bean->end_date);

            //TH lớp chưa bắt đầu
            if($now < $start_date){
                $start_sch = date('Y-m-d', strtotime($start_date));
                $end_sch = date('Y-m-d', strtotime("+14 days " . $start_date));
            }elseif($now >= $start_date && $now <= $end_date){
                $start_sch = date('Y-m-d', strtotime("-7 days " . $now));
                $end_sch = date('Y-m-d', strtotime("+7 days " . $now));
            }elseif($now > $end_date){
                $start_sch = date('Y-m-d', strtotime("-14 days " . $end_date));
                $end_sch = date('Y-m-d', strtotime($end_date));
            }

            foreach ($sss as $ss) {
                if ($ss['date_end'] < $now) $studied++;

                if (!in_array($ss['teacher_id'], $arrayTea) && !empty($ss['teacher_id'])) {
                    $arrayTea[] = $ss['teacher_id'];
                    $Tea1[$ss['teacher_id']]    = '' . $ss['teacher_name'];
                }
                if (!in_array($ss['ta1_id'], $arrayTea) && !empty($ss['ta1_id'])) {
                    $arrayTea[] = $ss['ta1_id'];
                    $Tea3[$ss['ta1_id']]        = "[TA 1] " . $ss['ta1_name'];
                }
                if (!in_array($ss['ta2_id'], $arrayTea) && !empty($ss['ta2_id'])) {
                    $arrayTea[] = $ss['ta2_id'];
                    $Tea2[$ss['ta2_id']]        = "[TA 2] " . $ss['ta2_name'];
                }

                //Custom short view schedule
                if(!empty($start_sch) && !empty($end_sch)){
                    $dstring = date('D', strtotime($ss['date'])).' '.$timedate->to_display_time($ss['date_start']).' - '.$timedate->to_display_time($ss['date_end']);
                    if($ss['date'] >= $start_sch && $ss['date'] <= $end_sch && !in_array($dstring, $arraySche)) $arraySche[$dstring] = $dstring;
                }
            }

            if(!empty($arraySche)) $short_schedule = $arraySche;
            else $short_schedule = json_decode(html_entity_decode($bean->short_schedule),true);

            $html_tea = implode(', ', array_merge($Tea1, $Tea2, $Tea3));
            $bean->main_teachers = $html_tea;

            // short schedule
            if (!empty($short_schedule)){
                $bean->short_schedule = '<ul class="weekdays-list">';
                $s_s = array();
                $html = '';
                $appList = $GLOBALS['app_list_strings']['dayoff_teacher_contract_list'];
                $appList2 = array('Mon','Tue','Wed','Thu','Fri','Sat','Sun');
                foreach ($short_schedule as $key => $value){
                    $wd = substr($key, 0, 3);
                    $td = substr($key, 3);
                    $html .= "\n" . $appList[$wd] . $td;
                    if(!in_array($td,$s_s[$wd])) $s_s[$wd] .= ((!empty($s_s[$wd]))? ("\n".$td) : $td);
                }
                //sort weekday
                foreach($appList2 as $v) {if(array_key_exists($v,$s_s)) $s_s2[$v]=$s_s[$v];}

                foreach($s_s2 as $key => $value)
                    $bean->short_schedule .= '<li rel="tooltip" data-placement="top" class="weekdays-day" title="'.$value.'" data-original-title="'.$value.'">'.$appList[$key].'</li>';
                $bean->short_schedule .= '</ul>';
            }

            $period = format_number(($studied / count($sss)) * 100, 0, 0);
            $bean->period = '<div class="progress" rel="tooltip" data-placement="top" data-original-title="' . $studied . ' / ' . count($sss) . ' ' . translate('LBL_SS_COMPLETED', 'J_Class') . $html . '"><div class="bar" style="width: ' . $period . '%;"></div>';
        }
    }


    function filterHoliday(&$bean, $event, &$args){
        //Check fillter holiday existing

        if(!empty(array_column($_REQUEST['filter'], 'holiday_id'))){
            $filterHoliday = array_shift(array_shift(array_column($_REQUEST['filter'], 'holiday_id')));
            $holidayList = array_column($GLOBALS['db']->fetchArray("SELECT holiday_date FROM holidays
                    WHERE id IN ('".implode($filterHoliday,"','")."')
                     AND deleted = 0 AND type = 'Public Holiday'"), 'holiday_date');

            $cnt_conditions = count($args[0]->where()->conditions);
            $index = 0;

            while($index < $cnt_conditions){
                if (isset($_REQUEST['filter'][$index]['holiday_id'])) {
                    $this->buildFilterHoliday($args[0], $index, $holidayList);
                    $this->buildFilterHoliday($args[1]['id_query'], $index, $holidayList);
                }
                $index++;
            }
        }
    }

    function buildFilterHoliday(&$filter_args, $index, $holiday_list){
        $filter = $filter_args->where()->conditions[$index];

        if (!array_key_exists('meetings', $filter->args->join)) {
            $filter_args->joinTable("meetings", array("alias" => "mt", "joinType" => "INNER", "linkingTable" => true))->on()->equalsField("j_class.id", "mt.ju_class_id")->equals("mt.deleted", '0');
        }

        $orWhere = new DotbQuery_Builder_Orwhere($filter_args);
        unset($filter_args->where()->conditions[$index]);
        $condition = new DotbQuery_Builder_Condition($filter_args);
        $condition->setOperator('IN')->setField("mt.date")->setValues($holiday_list)->ignoreAcl();
        $orWhere->add($condition);

        array_unshift($filter_args->where()->conditions, $orWhere);
        $filter_args->where()->notEquals("mt.session_status", 'Cancelled');
        $filter_args->where()->gt("mt.date", $GLOBALS['timedate']->nowDbDate());
    }
}
?>
