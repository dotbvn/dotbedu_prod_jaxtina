<?php
echo showAttendance();

function showAttendance()
{
    require_once("custom/include/_helper/junior_class_utils.php");

    global $current_user, $timedate;
    $MOD   = return_module_language($GLOBALS['current_language'],'J_Class');
    $MOD_A = return_module_language($GLOBALS['current_language'],'C_Attendance');
    $MOD_M = return_module_language($GLOBALS['current_language'],'Meetings');

    $ss = new Dotb_Smarty();
    // SMS templates
    $q1 = "SELECT id,name FROM email_templates WHERE type='sms' AND base_module='J_Class' AND deleted = 0";
    $rs1 = $GLOBALS['db']->query($q1);
    $email_templates_arr = array('' => '');
    while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) $email_templates_arr[$row['id']] = $row['name'];

    //Load Class
    $session_id = $_GET['session_id'];
    $class_id   = $_GET['class_id'];
    $class_name = (!empty($class_id) ? $GLOBALS['db']->getOne("SELECT DISTINCT IFNULL(name, '') class_name FROM j_class WHERE id = '$class_id' AND deleted = 0") : '');
    $date       = (!empty($_GET['session_date'])) ? $_GET['session_date'] : $timedate->nowDate();

    //Load json session time & prepare to assign template
    $res = getJsonSession($session_id, $class_id ,$date);
    $ss->assign("CLASS_ID", (!empty($res['class_id']) ? $res['class_id'] : $class_id));
    $ss->assign("CLASS_NAME", (!empty($res['class_name']) ? $res['class_name'] : $class_name));
    $ss->assign("DATE", (!empty($res['date']) ? $timedate->to_display_date($res['date'],false) : $date));
    $ss->assign("SESSION_ID", $session_id);
    $ss->assign("JSON_SESSION", $res['json_session']);

    //add dropdown action
    $html = '<div class="dropdown">';
    $html .= '<a class="dropdown-toggle" type="button" id="attendance_type" data-toggle="dropdown">' . $MOD['LBL_ATTENDANCE'] . '<span class="caret"></span></a>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="attendance_type">';
    foreach ($GLOBALS['app_list_strings']['attendance_type_list'] as $kop => $vop)
        $html .= '<li><a onclick="handleSaveAll(\'attendance_type\',$(this).attr(\'value\'))" value="' . $kop . '">' . $MOD['LBL_MARK_ALL'] . " <b>$vop</b> " . '</a></li>';
    $html .= '</ul></div>';
    $ss->assign("attendance", $html);

    $html = '<div class="dropdown">';
    $html .= '<a class="dropdown-toggle" type="button" id="do_homework" data-toggle="dropdown">' . $MOD_A['LBL_DO_HOMEWORK'] . '<span class="caret"></span></a>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="do_homework">';
    foreach ($GLOBALS['app_list_strings']['do_homework_list'] as $kop => $vop)
        $html .= '<li><a onclick="handleSaveAll(\'homework\',$(this).attr(\'value\'))" value="' . $kop . '">' . $MOD['LBL_MARK_ALL'] . " <b>$vop</b> " . '</a></li>';
    $html .= '</ul></div>';
    $ss->assign("homework", $html);

    $html = '<div class="dropdown">';
    $html .= '<a class="dropdown-toggle" type="button" id="homework_score" data-toggle="dropdown">' . $MOD_A['LBL_HOMEWORK_SCORE'] . '<span class="caret"></span></a>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="homework_score">';
    $html .= '<li><a onclick="handleSaveAll(\'homework_score\',$(this).attr(\'value\'))" value="5">' . $MOD['LBL_MARK_ALL'] . ' 5</a></li>';
    $html .= '<li><a onclick="handleSaveAll(\'homework_score\',$(this).attr(\'value\'))" value="4">' . $MOD['LBL_MARK_ALL'] . ' 4</a></li>';
    $html .= '<li><a onclick="handleSaveAll(\'homework_score\',$(this).attr(\'value\'))" value="3">' . $MOD['LBL_MARK_ALL'] . ' 3</a></li>';
    $html .= '<li><a onclick="handleSaveAll(\'homework_score\',$(this).attr(\'value\'))" value="2">' . $MOD['LBL_MARK_ALL'] . ' 2</a></li>';
    $html .= '<li><a onclick="handleSaveAll(\'homework_score\',$(this).attr(\'value\'))" value="1">' . $MOD['LBL_MARK_ALL'] . ' 1</a></li>';
    $html .= '<li><a onclick="handleSaveAll(\'homework_score\',$(this).attr(\'value\'))" value="">' . $MOD['LBL_MARK_ALL'] . ' ' . translate('LBL_MARKALL_EMPTY_C', 'J_Class') . '</a></li>';
    $html .= '</ul></div>';
    $ss->assign("homework_score", $html);

    $html = '<div class="dropdown" style="display: inline-flex;">';
    $html .= '<button type="button" '.((DotBACL::checkAccess('C_Attendance', 'export')||DotBACL::checkAccess('C_Attendance', 'import'))?'':'disabled').' class="btn btn-danger" id="send_button" style="border-top-right-radius: 0;border-bottom-right-radius: 0;" value="send_att">' . $MOD['LBL_SEND_APP'] . '</button>
    <button type="button" '.(DotBACL::checkAccess('C_Attendance', 'import')?'':'disabled').' class="btn btn-danger dropdown-toggle" data-toggle="dropdown" style="border-top-left-radius: 0;border-bottom-left-radius: 0;"><span class="caret"></span></button>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="send_button">';
    $html .= '<li><a onclick="showSend($(this))" value="send_att">' . $MOD['LBL_SEND_APP'] . '</a></li>';
    $html .= '<li><a onclick="showSend($(this))" value="send_mes">' . $MOD['LBL_SEND_MESSAGES'] . '</a></li>';
    $html .= '<li><a onclick="showSend($(this))" value="send_sms">' . $MOD['LBL_SEND_SMS'] . '</a></li>';
    $html .= '</ul>';
    $html .= '</div>';
    $ss->assign("send_button", $html);

    $html = '<div class="dropdown">';
    $html .= '<a class="dropdown-toggle" type="button" id="homework_comment" data-toggle="dropdown">' . $MOD_A['LBL_HOMEWORK_COMMENT'] . '<span class="caret"></span></a>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="homework_comment">';
    $html .= '<li><a onclick="genenrateMes(\'homework_comment\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
    $html .= '</ul></div>';
    $ss->assign("homework_comment", $html);

    $html = '<div class="dropdown">';
    $html .= '<a class="dropdown-toggle" type="button" id="care_comment" data-toggle="dropdown">' . $MOD_A['LBL_CARE_COMMENT'] . '<span class="caret"></span></a>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="care_comment">';
    $html .= '<li><a onclick="genenrateMes(\'care_comment\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
    $html .= '</ul></div>';
    $ss->assign("care_comment", $html);

    $html = '<div class="dropdown">';
    $html .= '<a class="dropdown-toggle" type="button" id="teacher_comment" data-toggle="dropdown">' . $MOD_A['LBL_TEACHER_COMMENT'] . '<span class="caret"></span></a>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="teacher_comment">';
    $html .= '<li><a onclick="genenrateMes(\'description\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
    $html .= '</ul></div>';
    $ss->assign("teacher_comment", $html);

    $html = '<div class="dropdown">';
    $html .= '<a class="dropdown-toggle" type="button" id="sms_content" data-toggle="dropdown">' . $MOD['LBL_MESSAGES'] . '<span class="caret"></span></a>';
    $html .= '<ul class="dropdown-menu" aria-labelledby="sms_content">';
    $html .= '<li><a onclick="genenrateMes(\'sms_content\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
    $html .= '</ul></div>';

    //custom permission
    $lockEdit = false;
    if (!ACLController::checkAccess('C_Attendance', 'edit', false)) $lockEdit = false;
    $lockView = false;
    if (!ACLController::checkAccess('C_Attendance', 'view', false)) $lockView = false;

    $ss->assign("lockEdit", $lockEdit);
    $ss->assign("lockView", $lockView);
    $ss->assign("lockEditLoyalty", !ACLController::checkAccess('J_Loyalty', 'edit', false));
    $ss->assign('MOD', $MOD);
    $ss->assign('MOD_A', $MOD_A);
    $ss->assign('MOD_M', $MOD_M);
    $ss->assign("CURRENT_USER_ID", $current_user->id);
    $ss->assign("TEMPLATE_OPTIONS", get_select_options_with_id($email_templates_arr, ""));
    // check acl field topic_custom,syllabus_custom
    $ss->assign("lockEditTopicCustom", !ACLController::checkField('Meetings', 'topic_custom', 'edit'));
    $ss->assign("lockEditSyllabusCustom", !ACLController::checkField('Meetings', 'syllabus_custom', 'edit'));
    $ss->assign("lockEditNoteTeacherCustom", !ACLController::checkField('Meetings', 'objective_custom', 'edit'));
    $ss->assign("HAVE_ROLE_SEND_ATT",DotBACL::checkAccess('C_Attendance', 'import'));
    return $ss->fetch('custom/modules/J_Class/tpls/attendance.tpl');
}
?>
