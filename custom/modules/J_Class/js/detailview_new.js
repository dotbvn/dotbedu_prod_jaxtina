var today = new Date();
var input_list = {};

$(document).ready(function(){
    //xu ly dau x de dong bang chon ngay
    $('.container-close').live('click',function(){
        $('.ui-widget-overlay').trigger('click');
    });

    //////////////////////////////////---------------Already DotB By Lap Nguyen --------------------------///////////////
    sqs_objects = [];
    //seacch input popup
    sqs_objects["move_class_name"] = {
        "form":"moveClass",
        "method":"query",
        "modules":['J_Class'],
        "group":"or",
        "field_list":["name", "id"],
        "populate_list":["move_class_name", "move_class_id"],
        "required_list":"move_class_id",
        "conditions":[{"name":"name","op":"like_custom","end":"%","value":""}],
        "order":"move_class_name",
        "limit":"30",
        "no_match_text":"No Match"
    };
    enableQS(true);

    //////////////////////////////////---------------End Already DotB By Lap Nguyen --------------------------///////////////
    $("#attendance_homework").on('click',function(){
        parent.DOTB.App.router.redirect("#bwc/index.php?module=J_Class&action=attendance&class_id="+$('input[name="record"]').val(),'_blank');
    });
    //add by Lap Nguyen
    $('.sts_description').live('change',function(){
        var _this = $(this);
        $.ajax({
            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
            type: "POST",
            async: true,
            data: {
                type   : 'saveStsDescription',
                sts_id : _this.attr('sts_id'),
                description: _this.val(),
            },
            success: function(res){

            },
        });
    });
    $('.syllabus_custom, .objective_custom, .homework, .observe_note ').live('click',function(){
        textAreaAdjust(this);
    });
    $('.observe_score').mask("000");

    $('#btn_fixIssueSituation').live('click', function(){
        fixIssueSituation();
    });
    $('#btn_change_lp').live('click',function(){
        let currentLpId = $('#lessonplan_id').data('id-value');
        let classId = $('input[name=record]').val();
        open_popup("J_LessonPlan", 1000, 700, "&current_lp_id=" + currentLpId + "&target_module=J_Class&target_id=" + classId, true, true, {
            "call_back_function": "setLessonPlanReturn",
            "form_name": "EditView",
            "field_to_name_array": {
                "id": "lessonplan_id",
                "name": "lessonplan_name"
            },
        }, "single", true);
    });
});

function setLessonPlanReturn(popup_reply_data){
    var name_to_value_array = popup_reply_data.name_to_value_array;
    DOTB.ajaxUI.showLoadingPanel();
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data: {
            type: 'ajaxChangeLessonPlan',
            class_id: $("input[name='record']").val(),
            lessonplan_id: name_to_value_array.lessonplan_id,
        },
        dataType: "json",
        success: function (res) {
            DOTB.ajaxUI.hideLoadingPanel();
            if (res.success == 1) {
                showSubPanel('j_class_meetings_syllabus', null, true);
                $('#lessonplan_id').closest('td').html('<a href="index.php?module=J_LessonPlan&action=DetailView&record='+res.lessonplan_id+'"><span id="lessonplan_id" data-id-value="'+res.lessonplan_id+'">'+res.lessonplan_name+'</span></a>');
                $('#start_lesson').text(res.start_lesson);
                parent.DOTB.App.alert.show('message-id', {
                    level: 'success',
                    messages: DOTB.language.get('J_Class','LBL_SUCCESS_CHANGE_LP'),
                    autoClose: true
                });
            }
            else toastr.error(DOTB.language.get('J_Class','LBL_ERROR_CHANGE_LP'));
        },
    });
}

//////////////////////////////////---------------Function DotB By Lap Nguyen --------------------------///////////////

//==========================Filter Date Situation Student==========================//
//lay sql tra ve
function got_data_cutom(args, inline) {
    var list_subpanel = document.getElementById('list_subpanel_j_class_studentsituations');
    if (list_subpanel != null) {
        var subpanel = document.getElementById('subpanel_j_class_studentsituations');
        var child_field = 'j_class_studentsituations';
        if (inline) {
            child_field_loaded[child_field] = 2;
            list_subpanel.innerHTML = '';
            list_subpanel.innerHTML = args;
        }
        DOTB.util.evalScript(args);
        subpanel.style.display = '';
        set_div_cookie(subpanel.cookie_name, '');
        if (current_child_field != '' && child_field != current_child_field) {}
        current_child_field = child_field;
        $("ul.clickMenu").each(function(index, node) {
            $(node).dotbActionMenu();
        });
    }
}
//==========================End Filter Date Situation Student==========================//

//Show dialog Export Attendance List
function showDialogExportAttendance(){
    //re-select lesson
    $("#export_from_lesson option:first").prop("selected",true)
    $("#export_to_lesson option:last").prop("selected",true)
    //responsive
    $('#diaglog_export_attendance').dialog({
        resizable    : false,
        width:       '700px',
        height:      'auto',
        modal        : true,
        visible      : true,
        dialogClass: 'fixed-dialog',
        buttons: {
            "Export":{
                click:function() {
                    ajaxExportAttendance();
                },
                class    : 'button primary',
                // text    : 'Export',
                text    : [DOTB.language.get('J_Class','LBL_BUTTON_EXPORT_C')],
            },
            "Cancel":{
                click:function() {
                    $(this).dialog('close');
                },
                class    : 'button',
                // text    : 'Cancel',
                text    : [DOTB.language.get('J_Class','LBL_BTN_CANCEL_C')],
            },
        },
    });
}

function ajaxExportAttendance(){
    //Check export to >= export from
    var export_from_lesson  = Numeric.parse($('#export_from_lesson').val().replace( /^\D+/g, ''));
    var export_to_lesson    = Numeric.parse($('#export_to_lesson').val().replace( /^\D+/g, ''));
    if (export_to_lesson < export_from_lesson){
        toastr.error(DOTB.language.get('J_Class','LBL_LESSON_RANGE_NOT_VALID'));
    }
    else{
        var class_id = $("input[name='record']").val();
        window.open("index.php?module=J_Class&action=exportAttendanceList&record="+class_id+"&from="+export_from_lesson+"&to="+export_to_lesson,'_blank');
        $('#diaglog_export_attendance').dialog("close");
    }
}
Calendar.setup ({
    inputField : "cc_closed_date",
    daFormat : cal_date_format,
    button : "cc_closed_date_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});

function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (14+o.scrollHeight)+"px";
}

function fixIssueSituation(){
    ajaxStatus.showStatus('Checking & Saving <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
    $('.btn_add_outstanding, .btn_cancel_dialog_outstanding').prop('disabled',true);
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        data:  {
            class_id            : $('input[name=record]').val(),
            type                : "fixIssueSituation",
        },
        dataType: "json",
        success:function(data){
            ajaxStatus.hideStatus();
            if (data.success == "1") {
                showSubPanel('j_class_studentsituations', null, true);
                toastr.success('Update Success!!');
            }else
                toastr.error('Update Error. Please Try Again!!');
        },
    });
}
function clickToCall(bean_type, bean_id, phone){
    window.top.App.view.createView({type: 'callCenter', data: {phoneNumber: phone, beanName: bean_type, beanId: bean_id}});

    if (window.has_callcenter) {
        if (window.callcenter_supplier === 'Oncall') window.callcenter_supplier_oncall.clickToCall(phone);
        else if (window.callcenter_supplier === 'Voip24h') window.callcenter_supplier_voip24h.clickToCall(phone);
            else if (window.callcenter_supplier === 'VoiceCloud') window.callcenter_supplier_voicecloud.clickToCall(phone);
    }
}
