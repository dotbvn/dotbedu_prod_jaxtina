
$(document).ready(function(){
    $('#btn_restore_att').live('click', function () {
        lbl_content = DOTB.language.get('J_Class','LBL_ALERT_RESTORE_ATT');
        $.confirm({
            title: DOTB.language.get('J_Class','LBL_CONFIRM_ALERT_TITLE'),
            content: lbl_content,
            buttons: {
                "OK": {
                    btnClass: 'btn-blue',
                    action: function(){
                        restoreAttendance();
                    }
                },
                "Cancel": {
                    text  : DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                    action: function(){

                    }
                },
            }
        });
    });

    $('#btn_lms_sync').live('click', function(){
        ajax_lms_sync();
    });
});

function restoreAttendance(){
    ajaxStatus.showStatus('Restoring <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        data:  {
            type : "restoreAttendance",
            class_id :$('input[name=record]').val(),
        },
        dataType: "json",
        success:function(data){
            if (data.success == 1){
                toastr.success(DOTB.language.get('J_Class','LBL_ALERT_RESTORE_ATT_SUCCESS')+' <b>'+data.count_restored+' '+DOTB.language.get('J_Class','LBL_SESSIONS')+'</b>');
                showSubPanel('class_attendances', null, true);
            }
            else toastr.error(DOTB.language.get('J_Class','LBL_ALERT_RESTORE_ATT_FAIL'));
            ajaxStatus.hideStatus();
        },
    });
}

function ajax_lms_sync(){
    ajaxStatus.showStatus('Checking <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        data:  {
            type : "ajax_lms_sync",
            class_id :$('input[name=record]').val(),
        },
        dataType: "json",
        success:function(data){
            ajaxStatus.hideStatus();
            if (data.success == "1") {
                toastr.success('Successful Sync LMS!');
            }
        },
    });
}