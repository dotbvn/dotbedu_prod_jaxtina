$(document).ready(function(){
    $('#dl_from_date, #dl_to_date').on('change',function(){
        $('#dl_af_enrolled_hours, #dl_af_ost_hours, #dl_bf_ost_hours, #dl_bf_enrolled_hours, #dl_delay_hours, #dl_delay_amount').text(0);
        var enrollment_start = DOTB.util.DateUtils.parse($('#dl_enrollment_start').val(),cal_date_format);
        var dl_from_date = DOTB.util.DateUtils.parse($('#dl_from_date').val(),cal_date_format);
        // Validate Data Lock
        if(!checkDataLockDate($(this).attr('id'),true)) return ;
        //TH xóa học viên khỏi lớp
        if(dl_from_date <= enrollment_start){
            $.alert(DOTB.language.get('J_Class','LBL_REMOVE_FROM_CLASS_DES'));
            $("#dl_revert_delay").prop("checked", true);
            $("#dl_drop_revenue").attr('disabled', true);
            $('#dl_remove_from_class').val(1);
        }else{
            $("#dl_drop_revenue").attr('disabled', false);
            $('#dl_remove_from_class').val(0);
        }
        caculateFreeBalance();

    });

    $('#btn_dl_remove_from_class').on('click',function(){
        //var bef_start = DOTB.util.DateUtils.formatDate(YAHOO.widget.DateMath.subtract(DOTB.util.DateUtils.parse($('#dl_enrollment_start').val(),cal_date_format),'D',1));
        $('#dl_from_date').val($('#dl_enrollment_start').val()).trigger('change');
    });
    $("input[name=dl_case]").on('change',function(){
        if($('#dl_revert_delay').is(":checked")){
            $.alert(DOTB.language.get('J_Class','LBL_DELAY_CASE_1_DES'));
            caculateFreeBalance();
        }
        if($('#dl_drop_revenue').is(":checked")) $.alert(DOTB.language.get('J_Class','LBL_DELAY_CASE_2_DES'));
    });

    $('.btn_reverse').live('click', function(){
        //Prepare dialog
        var student_id       = this.getAttribute('student_id');
        var student_name     = this.getAttribute('student_name');
        var class_student_id = this.getAttribute('class_student_id');
        var total_hours      = Numeric.toFloat(this.getAttribute('total_hour'),2,2);
        var start_study      = DOTB.util.DateUtils.formatDate(new Date(this.getAttribute('start_study')));
        var end_study        = DOTB.util.DateUtils.formatDate(new Date(this.getAttribute('end_study')));
        var today            = DOTB.util.DateUtils.formatDate(new Date());

        $("#dl_revert_delay").prop("checked", true);
        $('#dl_class_student_id').val(class_student_id);
        $('#dl_student_id').val(student_id);
        $('#dl_to_date').val(end_study);$('.dl_to_date').text(end_study);
        $('#dl_enrollment_start').val(start_study);$('.dl_enrollment_start').text(start_study);
        $('#dl_enrollment_end').val(end_study);$('.dl_enrollment_end').text(end_study);
        $('#dl_date').val(today);$('.dl_date').text(today);
        $('.dl_student_name').text(student_name);
        $('.dl_total_hours').text(total_hours);
        showDialogDelay();
        $('#dl_from_date').val($("#next_session_date").val()).trigger('change');
    });
});

//Show dialog Delay
function showDialogDelay(){
    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    var dialogWidth = '60%';
    var dialogHeight = 'auto';
    if ( screenHeight < 550 || screenWidth < 550 || screenWidth < screenHeight ) {
        dialogWidth = screenWidth * .95;
        dialogClass = '';
    } else {
        dialogWidth = screenWidth * .61;
        dialogClass = 'fixed-dialog';
    }
    $('#delay_class_dialog').dialog({
        resizable: false,
        width: dialogWidth,
        height: dialogHeight,
        dialogClass: dialogClass,
        modal: true,
        buttons: {
            "Create Delay":{
                click:function(event) {
                    if(!event.detail || event.detail == 1){
                        handleCreateDelay();
                    }
                },
                class    : 'button primary btn_create_delay',
                text    : [DOTB.language.get('J_Class','LBL_BTN_SAVE_C')],
            },
            "Cancel":{
                click:function() {
                    $(this).dialog('close');
                },
                class    : 'button btn_cancel_delay',
                text    : [DOTB.language.get('J_Class','LBL_BTN_CANCEL_C')],
            },
        }
    });
    $('#dl_reason').focus();
}

function caculateFreeBalance(){
    var dl_from_date    = $('#dl_from_date').val();
    var dl_date         = $('#dl_date').val();
    var dl_to_date      = $('#dl_to_date').val();
    var dl_class_student_id = $('#dl_class_student_id').val();
    var dl_case         =  $("input[name=dl_case]:checked").val();
    if(dl_from_date == '' || dl_to_date == '' ||dl_class_student_id == ''){
        $('#dl_af_enrolled_hours, #dl_af_ost_hours, #dl_bf_ost_hours, #dl_bf_enrolled_hours, #dl_delay_hours, #dl_delay_amount').text(0);
        return ;
    }
    $('.btn_create_delay, .btn_cancel_delay').prop('disabled',true);
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:{
            class_student_id : dl_class_student_id,
            dl_from_date : dl_from_date,
            dl_to_date   : dl_to_date,
            dl_date      : dl_date,
            type         : "caculateFreeBalance",
        },
        dataType: "json",
        success: function(res){
            if(res.success == "1"){
                $('#dl_bf_enrolled_hours').text(Numeric.toFloat(res.bf_enrolled_hours,2,2)).effect("highlight", {color: '#3594FF'}, 500);
                $('#dl_bf_ost_hours').text(Numeric.toFloat(res.bf_ost_hours,2,2)).effect("highlight", {color: '#3594FF'}, 500);
                $('#dl_af_enrolled_hours').text(Numeric.toFloat(res.af_enrolled_hours,2,2)).effect("highlight", {color: '#3594FF'}, 500);
                $('#dl_af_ost_hours').text(Numeric.toFloat(res.af_ost_hours,2,2)).effect("highlight", {color: '#3594FF'}, 500);
                $('#dl_reversed_hours').val(Numeric.toFloat(res.reversed_hours,2,2));

                $('#dl_delay_hours').text(Numeric.toFloat(res.delay_hours,2,2)).effect("highlight", {color: '#3594FF'}, 500);
                $('#dl_delay_amount').text(Numeric.toFloat(res.delay_amount)).effect("highlight", {color: '#3594FF'}, 500);
                $('#dl_last_study').text(res.last_study).effect("highlight", {color: '#3594FF'}, 500);
                $('.btn_create_delay, .btn_cancel_delay').prop('disabled',false);
            }else
                toastr.error(res.error);
        },
        error: function(){
            toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
        },
    });
}

function handleCreateDelay(){
    var dl_from_date    = $('#dl_from_date').val();
    var dl_date         = $('#dl_date').val();
    var dl_reason_for   = $('#dl_reason_for').val();
    var dl_to_date      = $('#dl_to_date').val();
    var dl_class_student_id = $('#dl_class_student_id').val();
    var dl_reason       =  $('#dl_reason').val();
    var dl_remove_from_class =  $('#dl_remove_from_class').val();
    var dl_case         =  $("input[name=dl_case]:checked").val();
    var dl_reversed_hours =  Numeric.parse($('#dl_reversed_hours').val());
    if(dl_from_date == '' || dl_to_date == '' || dl_class_student_id == ''){
        toastr.error(DOTB.language.get('J_Class','LBL_ERR_REQUIRED'));
        return ;
    }

    if(dl_reason == ''){
        //TH xóa học viên khỏi lớp ko cần lý do
        var dl_en_db        = DOTB.util.DateUtils.parse($('#dl_enrollment_start').val(),cal_date_format);
        var dl_from_db      = DOTB.util.DateUtils.parse($('#dl_from_date').val(),cal_date_format);
        if(dl_from_db > dl_en_db){
            toastr.error(DOTB.language.get('J_Class','LBL_ERR_REQUIRED'));
            $('#dl_reason').effect("highlight", {color: 'red'}, 500);
            return ;
        }
    }
    $('#delay_class_dialog').dialog("close");
    ajaxStatus.showStatus(DOTB.language.languages['app_strings']['LBL_SAVING']+'<img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:{
            class_student_id : dl_class_student_id,
            dl_from_date : dl_from_date,
            dl_to_date   : dl_to_date,
            dl_date      : dl_date,
            dl_reason    : dl_reason,
            dl_case      : dl_case,
            dl_remove_from_class : dl_remove_from_class,
            dl_reason_for: dl_reason_for,
            type         : "handleCreateDelay",
        },
        dataType: "json",
        success: function(res){
            ajaxStatus.hideStatus();
            if(res.success == '1'){
                showSubPanel('j_class_studentsituations', null, true);
                toastr.success(DOTB.language.get('J_Class','LBL_SAVED_SUCCESS'));
                progressStudents();
            }else{
                if(res.errorLabel)
                    toastr.error(res.errorLabel);
                else
                    toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
            }

        },
        error: function(){
            ajaxStatus.hideStatus();
            toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
        },
    });
}
Calendar.setup ({
    inputField : "dl_from_date",
    daFormat : cal_date_format,
    button : "dl_from_date_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});

Calendar.setup ({
    inputField : "dl_to_date",
    daFormat : cal_date_format,
    button : "dl_to_date_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});
