$(document).ready(function () {
     $('.syllabus_custom, .objective_custom, .homework, .observe_note, .observe_score, .sls_late_time, .sls_late_time_ta1, .sls_late_time_ta2').live('change',function(){
        var _this = $(this);
        if(_this.attr('name') == 'observe_score' && _this.val() > 100)
            _this.val('100');
        ajaxStatus.showStatus(DOTB.language.get('J_Class','LBL_SAVING'));
        $.ajax({
            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
            type: "POST",
            async: true,
            data: {
                type            : 'saveSyllabus',
                meeting_id      : _this.attr('meeting_id'),
                field           : _this.attr('name'),
                content         : _this.val(),
            },
            dataType: "json",
            success: function(res){
                if(res.success == '1'){
                    app.alert.show('message-id', {
                        level: 'success',
                        messages: DOTB.language.get('J_Class','LBL_SUCCESSFULLY_SAVED_C'),
                        autoClose: true
                    });
                }else
                    app.alert.show('message-id', {
                        level: 'error',
                        messages: DOTB.language.get('J_Class','LBL_ERROR_OCCURRED_C'),
                        autoClose: true
                    });
                ajaxStatus.hideStatus();
            },
            error: function(){
                ajaxStatus.hideStatus();
                app.alert.show('message-id', {
                        level: 'error',
                        messages: DOTB.language.get('J_Class','LBL_ERROR_OCCURRED_C'),
                        autoClose: true
                    });
            },
        });
    });
});
