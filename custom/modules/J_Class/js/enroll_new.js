$(document).ready(function(){
    $('#prv_start_study, #prv_end_study').on('change',function(){
        //Validate Is Between and check valid input
        var rs1 = validateDateIsBetween($(this).val(), $('#start_date').text(), $('#end_date').text());
        if(!rs1) {
            $(this).val('').effect("highlight", {color: 'red'}, 1000);
            return ;
        }

        var res = calSession(to_db_date($('#prv_start_study').val()), to_db_date($('#prv_end_study').val()));
        setValueToFieldPRV(res.total_sessions, res.total_hours);

        //load lại thông tin học viên - Tạo - 10/10/2020
        ajaxLoadStudents();
    });

    $('#prv_allow_ost').live('change',function(){
        //re-check status student
        $('.prv_paid_list').each(function(ind, selected){
            var student_id = $(selected).attr('student_id');
            enrollStudent(student_id);
        });
    });

    $('#prv_settle_date').on('change',function(){
        if(!checkDataLockDate($(this).attr('id'),true))
            return ;
    });

    $('button[name=listViewStartButton],button[name=listViewPrevButton],button[name=listViewNextButton],button[name=listViewEndButton]').live('click',function(){
        if(~$(this).attr('onclick').indexOf("j_class_studentsituations"))
            progressStudents();
    });

    $('#prv_unselect_all').live('click',function(){
        $('.prv_paid_list').each(function(ind, selection){
            var student_id = $(selection).attr('student_id');
            $(selection).multiselect('clearSelection');
            enrollStudent(student_id);
        });
    });

    $('#prv_reload_all').live('click',function(){
        ajaxLoadStudents();
    });

    //Pop-up add Student
    $('#prv_openpopup_student').live('click', function () {
        open_popup("Contacts", 1000, 700, "", true, true, {
            "call_back_function": "addStudentToClass",
            "form_name": "DetailView",
            "field_to_name_array": {
                "id": "student_id",
                "name": "student_name"
            },
            "passthru_data": {}
            }, "MultiSelect", true);
    });
    //
    progressStudents();
});
$('#btn_prv_class_name').live('click', function () {
    open_popup('J_Class', 1000, 700, "", true, false, {"call_back_function":"set_class_return","form_name":"EditView","field_to_name_array":{"id":"prv_class_id","name":"prv_class_name"}}, "single", true);
});
$('#btn_clr_prv_class_name').live('click', function () {
    $('#prv_class_name').val('');
    $('#prv_class_id').val('');
});
$('#prv_load_student').live('click', function () {
    ajaxLoadStudents();
});

$('#prv_btn_ost_load').live('click', function () {
    $('#prv_ost_load').val('1');
    ajaxLoadStudents();
});

function set_class_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'prv_class_id':
                    $("#prv_class_id").val(val);
                    break;
                case 'prv_class_name':
                    $("#prv_class_name").val(val);
                    break;
            }
        }
    }
    ajaxLoadStudents();
}

function addStudentToClass(popup_reply_data){
    var student_list = {};
    if (typeof (popup_reply_data.name_to_value_array) === "undefined" || popup_reply_data.name_to_value_array == '' || popup_reply_data.name_to_value_array == null ){
        student_list = popup_reply_data.selection_list;
    }else{
        var student_id   = popup_reply_data.name_to_value_array.student_id;
        student_list['ID_'+student_id] = student_id;
    }

    //get current list student
    var selected_students =  $.parseJSON($('#prv_selected_student').val());

    $.each( selected_students, function( key, value ) {
        student_list['ID_'+value] = value;
    });

    $('#prv_selected_student').val(JSON.stringify(student_list));
    ajaxLoadStudents();
}

//Mở Pop Up chọn học viên
function open_popup_add_students(student_id){
    if( (current_status.value == 'Closed') && is_admin != '1' ){
        $.alert("<span class=\"ui-icon ui-icon-alert\" style=\"float:left;\"></span> "+DOTB.language.get('J_Class','LBL_ERR_CLASS_CLOSED')+current_status.value+" !!");
        return ;
    }else{
        // Clear table enroll
        $('#prv_class_id, #prv_class_name, #prv_selected_student, #prv_ost_load').val('');
        if (student_id){
            var student_list = {0 : student_id};
            $('#prv_selected_student').val(JSON.stringify(student_list));
        }
        //responsive
        var screenWidth = window.screen.width;
        var screenHeight = window.screen.height;
        var dialogWidth = "90%";
        var dialogHeight = screenHeight * .70;
        if ( screenHeight < 550 || screenWidth < 550 ) {
            dialogClass = '';
        } else {
            dialogClass = 'fixed-dialog';
        }

        //Preset
        $('#prv_start_study').val($('#start_date').text());
        $('#prv_end_study').val($('#end_date').text());
        $('#diaglog_prv_student').dialog({
            resizable    : false,
            modal        : true,
            width        : dialogWidth,
            height       : dialogHeight,
            dialogClass  : dialogClass,
            buttons: {
                "Add Students":{
                    click:function(event) {
                        ajaxAddStudents();
                    },
                    class   : 'button primary btn_prv_add',
                    text    : DOTB.language.get('J_Class','LBL_ADD_STUDENTS'),
                },
                "Cancel":{
                    click:function() {
                        $(this).dialog('close');
                        //.....
                    },
                    class   : 'button btn_prv_add',
                    text    : DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                },
            },
        });
        var res = calSession(to_db_date($('#prv_start_study').val()), to_db_date($('#prv_end_study').val()));
        setValueToFieldPRV(res.total_sessions, res.total_hours);
        ajaxLoadStudents();
    }
}

function ajaxLoadStudents(){
    handleCountItem();
    var prv_class_id        = $('#prv_class_id').val();
    var selected_student    = $('#prv_selected_student').val();
    var prv_ost_load        = $('#prv_ost_load').val();
    if(prv_class_id == '' && selected_student == '' && prv_ost_load == 0){
        $('#tbody_prv_student').html('');
        handleCountItem();
        return ;
    }
    var cur_class_id        = $('input[name="record"]').val();
    var start               = $('#prv_start_study').val();
    var end                 = $('#prv_end_study').val();
    //get selected payment id
    var pm_selected = {};
    $('.prv_paid_list').each(function(ind, selected){
        var student_id = $(selected).attr('student_id');
        pm_selected[student_id] = new Array();
        $(this).find("option:selected").each(function(ind, opt){
            pm_selected[student_id].push($(opt).val());
        });
    });
    if(checkform_prv()){
        $('.btn_prv_add').prop('disabled',true);
        $.ajax({
            type: "POST",
            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
            data:  {
                type             : "ajaxLoadStudents",
                selected_student : selected_student,
                prv_class_id     : prv_class_id,
                prv_ost_load     : prv_ost_load,
                cur_class_id     : cur_class_id,
                start            : start,
                end              : end,
                pm_selected      : JSON.stringify(pm_selected),
            },
            dataType: "json",
            success:function(data){
                $('.btn_prv_add').prop('disabled',false);
                if (data.success == "1") {
                    $('#json_sessions').val(data.json_ss); //rebiuld list sessions

                    $('#tbody_prv_student').html(data.html);
                    $('#ern_sessions').val(data.ern_ss);
                    generatePaidlistMultiSelect();
                    handleCountItem();
                }else{
                    $('#ern_sessions').val('');
                }
                if(data.errLabelAll != '' && data.errLabelAll != null) toastr.error(data.errLabelAll);
                $('.checkbox_item:checked').each(function () {
                    enrollStudent($(this).val());
                });
                var res = calSession(to_db_date($('#prv_start_study').val()), to_db_date($('#prv_end_study').val()));
                setValueToFieldPRV(res.total_sessions, res.total_hours);
            },
        });
    }
}

function setValueToFieldPRV(total_sessions, total_hours){
    $('#prv_total_sessions').val(total_sessions);
    $('#prv_total_hours').val(total_hours);
    $('#prv_total_sessions_text').text(total_sessions);
    $('#prv_total_hours_text').text(formatNumber(total_hours,num_grp_sep,dec_sep,2,2));
}

function checkform_prv(){
    var prv_class_id        = $('#prv_class_id').val();
    var prv_class_name      = $('#prv_class_name').val();
    var prv_total_sessions  = $('#prv_total_sessions').val();
    var total_hours         = $('#prv_total_hours').val();
    var selected_student    = $('#prv_selected_student').val();
    var cr_class_id         = $('input[name=record]').val

    if(prv_class_id == cr_class_id){
        $("#prv_class_id").val('');
        $("#prv_class_name").val('').effect("highlight", {color: 'red'}, 2000);
        toastr.error(DOTB.language.get('J_Class','LBL_CLASS_DUPLICATED'));
        return false;
    }
    return true;
}

function handleCountItem(){
    var count_all_student=0;
    var count_enrolled_student=0;
    var count_outstanding_student=0;
    var list_student = [];
    $('.checkbox_item').each(function () {
        if($(this).is(':checked')){
            list_student.push($(this).val());
            count_all_student++;
        }
    })
    $('#prv_student_list').val(list_student);
    $('#total_prv_student').text(count_all_student);
}

function ajaxAddStudents(){
    var cur_class_id    = $('input[name="record"]').val();
    var prv_settle_date = $('#prv_settle_date').val();
    var is_allow_ost    = ($('#prv_allow_ost').is(":checked")) ? 1 : 0;
    var stsd = {};
    //get selected students
    $('.checkbox_item:checked').each(function () {
        var student = JSON.parse($(this).closest('tr').find('.prv_json').val());
        var student_id                  = $(this).val();
        stsd[student_id]                = {};
        stsd[student_id]['id']          = student_id;
        stsd[student_id]['name']        = student['full_student_name'];
        stsd[student_id]['start']       = student['start'];
        stsd[student_id]['end']         = student['end'];
        stsd[student_id]['type']        = student['type'];
        stsd[student_id]['enroll_list'] = student['enroll_list'];
        stsd[student_id]['ost_type']    = student['ost_type'];
        stsd[student_id]['pmId_sel']    = new Array();
        $(this).closest('tr').find(".prv_paid_list").find("option:selected").each(function(ind, opt){
            stsd[student_id]['pmId_sel'].push($(opt).val())
        });
    });
    //validate
    if(_.size(stsd)==0){
        toastr.error(DOTB.language.get('J_Class','LBL_STR_STUDENT_REQUIRED'));
        return false;
    }
    if( prv_settle_date == '' || $('#prv_start_study').val() == '' || $('#prv_end_study').val() == ''){
        toastr.error(DOTB.language.get('J_Class','LBL_MISSING_FIELD'));
        return false;
    }
    if(checkform_prv()){
        $.confirm({
            title: DOTB.language.get('J_Class','LBL_CONFIRM_ALERT_TITLE'),
            content: DOTB.language.get('J_Class','LBL_CONFIRM_ALERT_UPGRADE'),
            buttons: {
                "OK": {
                    btnClass: 'btn-blue',
                    action: function(){
                        DOTB.ajaxUI.showLoadingPanel();
                        $('#diaglog_prv_student').dialog("close");
                        $.ajax({
                            type: "POST",
                            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
                            data:  {
                                type               : "ajaxAddStudentToClass",
                                cur_class_id       : cur_class_id,
                                settle_date        : prv_settle_date,
                                is_allow_ost       : is_allow_ost,
                                students_selected  : JSON.stringify(stsd),
                            },
                            dataType:"json",
                            async   :true,
                            success :function(data){
                                DOTB.ajaxUI.hideLoadingPanel();
                                if (data.success == "1"){
                                    $.each(data.enr_success, function( index, value ){ toastr.success(value.student_name + DOTB.language.get('J_Class','LBL_ADD_SUCCESSFULLY'));});
                                    $.each(data.enr_fail, function( index, value ){
                                        toastr.error(value.student_name + DOTB.language.get('J_Class','LBL_ADD_FAIL') + '<br>' + value.error_label);
                                    });
                                    showSubPanel('j_class_studentsituations', null, true);
                                    progressStudents();
                                }else{
                                    toastr.error(DOTB.language.get('J_Class','LBL_ERR_CLASS_CHANGE'));
                                    location.reload();
                                }
                            },
                            error: function(){
                                DOTB.ajaxUI.hideLoadingPanel();
                                toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                            },
                        });
                    }
                },
                "Cancel": {
                    text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                    action: function(){
                        //do nothing
                    }
                },
            }
        });
    }
}

////--------------------FUNCTION XÀI CHUNG-----------------------------------
function calSession(start_study, end_study){
    var count_ss      = total_hours = 0
    var _start_study  = _end_study  = '';
    var json_sessions = $("#json_sessions").val();
    var rs = new Object();
    if(start_study && end_study && json_sessions){
        _start_study = start_study = (DOTB.util.DateUtils.guessFormat(start_study) == 'Y-m-d') ? new Date(start_study+' 00:00:00') : new Date(start_study);
        _end_study = end_study = (DOTB.util.DateUtils.guessFormat(end_study) == 'Y-m-d') ? new Date(end_study+' 23:59:59') : new Date(end_study);
        obj = JSON.parse(json_sessions);
        rs['sessions'] = new Array();
        $.each(obj, function( key, value ) {
            var key_date = new Date(value.start);
            if((key_date <= end_study) && (key_date >= start_study)){
                rs['sessions'].push(value.id);
                _end_study = key_date;
                if(count_ss == 0) _start_study = key_date;
                total_hours += parseFloat(value.dhour);
                count_ss++;
            }
        });
        if(count_ss == 0) _end_study = _start_study;
        rs['start_f'] = DOTB.util.DateUtils.formatDate(_start_study);
        rs['end_f']   = DOTB.util.DateUtils.formatDate(_end_study);
    }
    rs['start']   = DOTB.util.DateUtils.formatDate(_start_study,false,'Y-m-d');
    rs['end']     = DOTB.util.DateUtils.formatDate(_end_study,false,'Y-m-d');;
    rs['total_sessions']= count_ss;
    rs['total_hours']   = parseFloat(total_hours.toFixed(2));
    return rs;
}

function validateDateIsBetween(check_date, from, to){
    if(check_date == '') return true;
    check_date     = DOTB.util.DateUtils.parse(check_date, cal_date_format);
    if(from != '' && to != ''){
        from_check           = DOTB.util.DateUtils.parse(from, cal_date_format).getTime();
        to_check             = DOTB.util.DateUtils.parse(to, cal_date_format).getTime();
        if(check_date == false){
            toastr.error('Invalid date range. Date must be between '+from+' and '+to+'.');
            return false;
        }else{
            check_date = check_date.getTime();
            if(check_date < from_check || check_date > to_check){
                toastr.error('Invalid date range. Date must be between '+from+' and '+to+'.');
                return false;
            }
        }
    }else if(from != ''){
        from_check           = DOTB.util.DateUtils.parse(from, cal_date_format).getTime();

        if(check_date < from_check){
            toastr.error('Invalid date range. Date must after '+from+'.');
            return false;
        }
    } else{
        to_check             = DOTB.util.DateUtils.parse(to, cal_date_format).getTime();
        if(check_date > to_check){
            toastr.error('Invalid date range. Date must before '+to+'.');
            return false;
        }
    }
    return true;
}

function generatePaidlistMultiSelect(drop_up){
    if (typeof drop_up == 'undefined' ) drop_up = true;
    $('.prv_paid_list').multiselect({
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonWidth: '95%',
        enableHTML : true,
        maxHeight: 300,
        dropUp: drop_up,
        optionLabel: function(element)
        {
            var sub_text  = $(element).attr("sub_text");
            return $(element).html() + sub_text;
        },
        onChange: function(option, checked, select) {
            var student_id        = option.closest('select').attr('student_id');
            var kind_of_course    = option.attr('kind_of_course');
            var payment_name      = option.attr('payment_name');
            var cr_class_koc      = $('#kind_of_course').val();
            var expired           = option.attr('expired');
            var team_id           = option.attr('team_id');
            var payment_id        = option.val();
            var count_dif_team    = 0;
            $('#prv_'+student_id).find("option:selected").each(function(ind, opt){
                if($(opt).val() != payment_id && $(opt).attr('team_id') != team_id)
                    count_dif_team++;
            });

            if(count_dif_team > 0){
                parent.DOTB.App.alert.show('message-id', {
                    level: 'error',
                    messages: DOTB.language.get('J_Class','LBL_ERR_ENROLL_PROCESS1'),
                    autoClose: true
                });
                $('#prv_'+student_id).multiselect('deselect', $(option).val());
            }

            if(option.is(':selected')){
                if (kind_of_course != '' && typeof kind_of_course != 'undefined' && kind_of_course != '[""]' ){
                    var kocObject = $.parseJSON(kind_of_course);
                    if($.inArray(cr_class_koc , kocObject ) == -1){
                        var confirm_p = DOTB.language.get('J_Class','LBL_ERR_ENROLL_PROCESS2');
                        if(confirm_p != '' && typeof confirm_p != 'undefined'){
                            confirm_p = confirm_p.replace("{payment}", payment_name);
                            confirm_p = confirm_p.replace("{kind_of_course}", '['+kocObject.join(', ')+']');
                            confirm_p = confirm_p.replace("{class_name}", $('#name').text()+' - ['+cr_class_koc+']');
                        }

                        $.confirm({
                            title: DOTB.language.get('J_Class','LBL_CONFIRM_C'),
                            content: confirm_p,
                            buttons: {
                                "OK": {
                                    btnClass: 'btn-blue',
                                    action: function(){
                                        //do nothing
                                    }
                                },
                                'Cancel': {
                                    text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                                    action: function(){
                                        $('#prv_'+student_id).multiselect('deselect', $(option).val());
                                        enrollStudent(student_id);
                                    }
                                },
                            }
                        });
                    }

                }
            }
            enrollStudent(student_id);
        },
        filterPlaceholder: ''
    });
}

function enrollStudent (student_id){
    var prv_tr = $('#prv_'+student_id).closest('tr');
    var enroll_hours = 0;
    var data_chart = [];
    var student = JSON.parse(prv_tr.find('.prv_json').val());
    var ern_sessions = $("#ern_sessions").val();
    var obj = JSON.parse(ern_sessions);
    //Xử lý Enroll theo từng Payment
    var pay_list = {};
    $('#prv_'+student_id).find("option:selected").each(function(ind, opt){
        var pm_id = $(opt).val();
        pay_list[pm_id] = new Array();
        pay_list[pm_id] = parseFloat($(opt).attr('remain_hours'));
    });

    var erl = {};
    var lastSit  = '###';
    var looprest = false;
    var is_allow_ost = $('#prv_allow_ost').is(":checked");
    //Defined
    $.each(obj, function( key, value ) {
        if($.inArray(value.id, student.busy_list) !== -1 || looprest){
            if( (lastSit == 'Enrolled' || lastSit == 'OutStanding' || lastSit == 'Undefined') || looprest){
                lastSit   = 'Defined2';
                looprest  = true;
            }else lastSit = 'Defined1';
        }else{
            lastSit = 'OutStanding';
            if(!is_allow_ost) lastSit = 'Undefined';
            $.each(pay_list, function( pm_id, remain_hours ) {
                if(pay_list[pm_id] > 0){
                    pay_list[pm_id] -= parseFloat(value.dhour);
                    if(parseFloat(pay_list[pm_id].toFixed(2)) >= 0){
                        lastSit = 'Enrolled';
                        return false;
                    }
                }
            });
        }
        if(lastSit == 'Enrolled' || lastSit == 'OutStanding') enroll_hours += parseFloat(value.dhour);
        if($.isEmptyObject(erl[lastSit])) erl[lastSit] = { hours: 0, count: 0, start: '', end: '', type: ''};
        erl[lastSit].hours += parseFloat(value.dhour);
        erl[lastSit].count += 1;
        if(erl[lastSit].count == 1) erl[lastSit].start = value.start;
        erl[lastSit].end   = value.end;
        erl[lastSit].type  = lastSit.replace(/[0-9]/g, '');
    });
    //Draw timeline
    var colorList = {"Defined": '#d7d7d7',"Undefined": '#8d8d8d',"OutStanding": '#F3960B',"Enrolled": '#32CD32'};
    var progressLabel = DOTB.language.languages['app_list_strings']['process_type_list'];
    $.each(erl, function( key, value ) {
        var start_f = DOTB.util.DateUtils.formatDate( new Date(erl[key].start) );
        var end_f = DOTB.util.DateUtils.formatDate( new Date(erl[key].end) );
        var hours = Numeric.toFloat(erl[key].hours,2,2);
        data_chart.push({start: start_f, end: end_f, title: hours, value: erl[key].hours, color: colorList[erl[key].type], enr_type: erl[key].type, description: progressLabel[erl[key].type]+'|'+erl[key].count+' '+DOTB.language.get('J_Class','LBL_SESSIONS')});
    });

    prv_tr.find('.prv_join_type').html("<div id='prv__chart_"+student_id+"'></div>");
    $('#prv__chart_'+student_id).segbar([{data: data_chart}]);
    //add warning icon
    if(enroll_hours > 0)
        prv_tr.find('.prv_icon').html("<i class='fa fa-check-circle' style='color: #43B977;' title='"+DOTB.language.get('J_Class','LBL_ALLOWED_ENROLL')+"'></i>");
    else
        prv_tr.find('.prv_icon').html("<i class='fa fa-exclamation-triangle' style='color: #ff8800;' title='"+DOTB.language.get('J_Class','LBL_ERR_ENROLL_HOURS')+"'></i>");
}

function progressStudents(){
    var progressLabel = parent.DOTB.App.lang.getAppListStrings("process_type_list");
    var set_color     = {Undefined:"#8d8d8d", OutStanding:"#F3960B", Enrolled:"#32CD32", Demo:"#04ADEC"};
    $('.sts_json').each(function(ind, sts_json){
        if(sts_json.value == '') return ;
        var student    = JSON.parse(sts_json.value);
        var data_chart = [];
        var student_id = $(this).attr('student_id');
        $.each(student.process, function( key, res ){
            var text_total = (res.total_amount && res.total_amount > 0) ? (' | '+Numeric.toFloat(res.total_amount)) : '';
            data_chart.push({start:DOTB.util.DateUtils.formatDate(new Date(res.start)), end:DOTB.util.DateUtils.formatDate(new Date(res.end)), title: Numeric.toFloat(res.duration,2,2) + text_total, value: res.duration, amount: res.total_amount, color: set_color[res.type], show_per: 0, enr_type:res.type, description: progressLabel[res.type]+'|'+res.total_ss+' '+DOTB.language.get('J_Class','LBL_SESSIONS') });
        });
        if(data_chart.length){
            $(this).closest('tr').find('.sts_process').html("<div id='sts_chart_"+student_id+"'></div>");
            $('#sts_chart_'+student_id).segbar([{data: data_chart}]);
        }
    });
}

Calendar.setup ({
    inputField : "prv_start_study",
    daFormat : cal_date_format,
    button : "prv_start_study_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});
Calendar.setup ({
    inputField : "prv_end_study",
    daFormat : cal_date_format,
    button : "prv_end_study_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});
Calendar.setup ({
    inputField : "prv_settle_date",
    daFormat : cal_date_format,
    button : "prv_settle_date_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});

function to_db_date(prv_date){return DOTB.util.DateUtils.formatDate(DOTB.util.DateUtils.parse(prv_date,cal_date_format),false,'Y-m-d');}
