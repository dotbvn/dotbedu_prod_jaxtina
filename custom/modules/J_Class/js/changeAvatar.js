$(document).ready(function(){
    //Choose avatar class: Nam Nguyen
    $('#choose_avatar').live('click', function () {
        $('#imagemodal').css('display', 'block');
    });
    $('.icon-image img').live('click', function () {
        var imageName = $(this).attr("data-nameimage");
        $('#imagemodal').css('display', 'none');
        var urlImage = "custom/images/class_icon/" + imageName + '.png';
        $('div.icon-image-choose img').prop('src', urlImage);
        $('.icon-image-choose').css('display', 'block');
    });

    $('.icon-image-upload img').live('click', function () {
        var urlImage = $(this).attr("src");
        $('#imagemodal').css('display', 'none');
        $('div.icon-image-choose img').prop('src', urlImage);
        $('.icon-image-choose').css('display', 'block');

        $('#imagefile_picture').prop('value', '');
    });

    $('#closemodal').live('click', function () {
        $('#imagemodal').css('display', 'none');
        var field = document.getElementById('imagefile_picture');
        if($('div.icon-image-upload img').attr("data-imageupload") != ""){
            field.value = '';
        }
    });
    $('#closemodal2').live('click', function () {
        $('#picture').val('');
        $('div.icon-image-upload img').prop('src', '');
        $('div.icon-image-upload img').attr("data-imageupload","");

        $('#imagemodal2').css('display', 'none');
        $('#imagemodal').css('display', 'block');
        $('.icon-image-upload').css('display', 'none');

        var field=document.getElementById('remove_imagefile_picture');
        var urlSRCOrigin = $(".icon-image-choose img").attr("data-src2");
        $('div.icon-image-choose img').prop('src', urlSRCOrigin);
        $('div.icon-image-choose').css("display","none");
        field.value=1;

        $('#imagefile_picture').prop('value', '');

    });
    $('#save-photo').live('click', function () {
        var field = document.getElementById('imagefile_picture');
        field.value = '';
        $('#imagemodal').css('display', 'none');
        $('#imagemodal2').css('display', 'none');
        //set to show when choosing
        var urlImage = $('.icon-image-upload img').attr("data-imageupload");
        $('div.icon-image-choose img').prop('src', urlImage);
        $('.icon-image-choose').css('display', 'block');
    });
});