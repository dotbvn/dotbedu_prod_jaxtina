(function($) {
    const DEFAULT_WIDTH = "100%";
    const DEFAULT_HEIGHT = "30px";
    const palette = [
        '#26C281',
        '#E9D460',
        '#81CFE0',
        '#F22613',
        '#3A539B',
        '#E67E22',
        '#22A7F0',
        '#C0392B',
        '#663399',
        '#913D88',
        '#F9BF3B',
        '#6C7A89',
        '#DB0A5B',
        '#03A678',
        '#FDE3A7',
        '#6BB9F0',
        '#8E44AD',
        '#D24D57',
        '#CF000F',
        '#86E2D5'
    ];

    $.fn.segbar = function(options = []) {
        $(this).each(function makeSegBar(index, item) {
            //Get data from options array
            let currentOptions = options[index];
            if( !currentOptions.data || (currentOptions.data.length <= 0) ) {
                throw new Error(`No data provided for chart at position ${index}`);
            }
            constructBar(this, currentOptions);
        });
        return this;
    };

    function constructBar(element, options) {
        let percentages = getPercentages(options.data);

        for(let i=0; i<options.data.length; i++)  {
            options.data[i].percent = +percentages[i];
        }
        //console.log(options.data);

        element.style.width = options.width ? options.width : DEFAULT_WIDTH;
        element.style.height = options.height ? options.height : DEFAULT_HEIGHT;
        element.style.paddingTop = '15px';
        element.style.textAlign = "left";
        element.classList.add('segbar');
        let colorIt = getNextColor();
        for(let item of options.data) {
            var percent = (item.percent*100).toFixed(0);
            if(percent > 0){
                let div = document.createElement('div');

                //Prepare wrapper
                div.style.display = "inline-block";
                div.style.height = "100%";
                div.style.width = `${parseFloat(item.percent*100)}%`;
                div.style.backgroundColor = (item.color) ? item.color : colorIt.next().value;
                div.style.position = "relative";
                div.classList.add('item-wrapper');
                //start span
                if(item.start && item.start.length > 0 && percent >= 30) {
                    let span = document.createElement('span');
                    span.style.position = "absolute";
                    span.style.top = '-12px';
                    span.style.color = "#333";
                    span.textContent = item.start;
                    span.style.fontSize = "10px";
                    //span.style.fontWeight = "600";
                    span.style.borderLeft = "2px solid "+item.color;
                    span.classList.add('item-header');
                    div.appendChild(span);
                }
                if(item.end && item.end.length > 0 && percent >= 30) {
                    let span = document.createElement('span');
                    span.style.position = "absolute";
                    span.style.top = '-12px';
                    span.style.right = '0px';
                    span.style.color = "#333";
                    span.textContent = item.end;
                    span.style.fontSize = "10px";
                    //span.style.fontWeight = "600";
                    span.style.borderRight = "2px solid "+item.color;
                    span.classList.add('item-header');
                    div.appendChild(span);
                }
                //Title span
                if(item.title && item.title.length > 0) {
                    let span = document.createElement('span');
                    span.style.position = "absolute";
                    span.textContent = item.title;
                    span.classList.add('item-title-'+item.enr_type);
                    div.appendChild(span);
                }
                if( (item.description && item.description.length > 0 )|| item.show_per) {
                    let span = document.createElement('span');
                    //var percent = prettifyPercentage(item.percent*100);

                    //Description span
                    span.textContent = (item.description && item.description.length > 0 ) ? (item.description+' ') : '';
                    //Percentage span
                    span.textContent += (item.show_per) ? `${percent}%` : '';
                    span.style.color = "white";
                    span.style.position = "absolute";
                    span.style.bottom = '0px';
                    span.style.right = '4px';
                    span.style.fontSize = "9px";
                    span.classList.add('item-percentage');
                    if(span.textContent.trim().length > 0){
                        if(percent >= 10) div.appendChild(span);
                        div_title = item.description;
                        var text_total = (item.amount && item.amount > 0) ? (' | '+Numeric.toFloat(item.amount)) : '';
                        div_title += '\n'+(Number.isInteger(item.value) ? item.value.toString() : Numeric.toFloat(item.value,2,2))+' '+DOTB.language.get('J_Class','LBL_HRS')+text_total;
                        div_title += (item.start && item.end) ? ('\n'+item.start+' - '+item.end) : '';
                        div_title += (item.alt_text) ? ('\n'+item.alt_text) : '';
                        $(div).attr('data-tooltip', div_title).addClass("simptip-position-top simptip-movable simptip-multiline");
                    }
                }
                element.appendChild(div);
            }
        }
    }

    function prettifyPercentage(percentage) {
        let pretty = parseFloat(percentage).toFixed(2);
        let v = pretty.split('.');
        let final = 0;
        if(v[1]){
            let digits = v[1].split('');
            if(digits[0] == 0 && digits[1] == 0) {
                final = parseFloat(`${v[0]}`);
            }else{
                final = pretty;
            }
        }
        else {
            final = parseFloat(v[0]);
        }
        return final;
    }

    //Accepts an array of chart data, returns an array of percentages
    function getPercentages(data) {
        let sum = getSum(data);

        return data.map(function (item) {
            return parseFloat(item.value / sum);
        });
    }

    //Accepts an array of chart data, returns the sum of all values
    function getSum(data) {
        return data.reduce(function (sum, item) {
            return sum + item.value;
            }, 0);
    }

    function* getNextColor() {
        let i = 0;
        while(true) {
            yield palette[i];
            i = (i + 1) % palette.length;
        }
    }

    }(jQuery));