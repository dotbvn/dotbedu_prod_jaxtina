$(document).ready(function(){
    $('.btn_request_ss').live('click',function(){
        submitRequest($(this),'Requesting');
    });
    $('.btn_lock_ss').live('click',function(){
        submitRequest($(this),'Locked');
    });
    $('.btn_unlock_ss').live('click',function(){
        submitRequest($(this),'Unlocked');
    });
});

function submitRequest(btn, status){
    var language = parent.DOTB.App.lang;
    var met_id = btn.attr('ssid');
    var timedate = btn.attr('timedate')
    if(status == 'Requesting') content = language.get('LBL_SUBMIT_STATUS_SHOW_MSG','Meetings');
    if(status == 'Locked') content = language.get('LBL_LOCKED_MSG','Meetings');
    if(status == 'Unlocked') content = language.get('LBL_UNLOCKED_MSG','Meetings');
    $.confirm({
        title: timedate,
        content: content,
        buttons: {
            "OK": {
                btnClass: 'btn-blue',
                action: function(){
                    sendLockUnlock(met_id, status);
                }
            },
            'Cancel': {
                text: language.get('LBL_BTN_CANCEL_C','J_Class'),
                action: function(){

                }
            },
        }
    });
}

function sendLockUnlock(met_id, status){
    ajaxStatus.showStatus('Saving <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
    $.ajax({
        type: "POST",
        url: "index.php?module=Meetings&action=ajaxMeeting&dotb_body_only=true",
        data:  {
            type    : "sendRequestUnlock",
            status  : status,
            id      : met_id,
        },
        dataType: "json",
        success:function(data){
            if (data.success == 1){
                app.alert.show('message-id', {
                    level: 'success',
                    messages: data.mgs,
                    autoClose: true
                });
            }else {
                app.alert.show('message-id', {
                    level: 'error',
                    messages: data.mgs,
                    autoClose: true
                });
            }
            showSubPanel('class_attendances', null, true);
            ajaxStatus.hideStatus();
        },
    });
}