$(document).ready(function(){
    $('#start_schedule,#end_schedule').on('change',function(){
        if(!checkDataLockDate($(this).attr('id'),true))
            return ;

        $('.more_teacher_action').css('display','none');
        getWeekDay($('#start_schedule').val(),$('#end_schedule').val());
        clearTeacherList();
    });

    $('#btn_check_schedule').on('click',function(){
        var available = 1;
        ajaxCheckScheduleTecher(available);

    });

    $('#busy_schedule').on('click',function(){
        var available = 0;
        ajaxCheckScheduleTecher(available);
    });
    $('#another_schedule').on('click',function(){
        var available = 1;
        ajaxCheckScheduleTecher(available,1);
    });
    $('#btn_reset_input').live('click',function(){
        resetScreen();
    });
    $('#select_teaching_type').live('change',function(){
        if($(this).val() != 'main_teacher'){
            $('.change_reason_required').show();
        }else{
            $('.change_reason_required').hide();
        }
    });
    //Schedule For
    $('#sc_type').live('change',function(){
        $('.more_teacher_action').css('display','none');
        if($(this).val() == 'Room'){ // hiện header table của room
            $('.list_teacher').css('display','none');
            $('.list_room').css('display','');
        }else{
            $('.list_room').css('display','none');
            $('.list_teacher').css('display','');
        }
        if($(this).val() != 'Teacher'){
            $('#select_teaching_type').closest('tr').hide();
            $('#change_teacher_reason').closest('tr').hide();
        }else{
            $('#select_teaching_type').closest('tr').show();
            $('#change_teacher_reason').closest('tr').show();
        }
        clearTeacherList();
    });

    //    ajaxCheckScheduleTecher();


    $('.day_of_week').live('change',function(){
        $('.more_teacher_action').css('display','none');
        if($(this).is(':checked')){
            var time_slot =  $.parseJSON($(this).attr('time_slot'));
            var html = '<select class="time_slot" size="1" title="'+DOTB.language.get('J_Class','LBL_SHIFT')+'">';
            if(time_slot['timeslot'].length > 1){
                html += '<option value="select_all">'+DOTB.language.get('J_Class','LBL_SELECT_ALL')+'</option>';
            }
            $.each( time_slot['timeslot'], function( key, value ) {
                html += '<option value="'+value+'">'+value+'</option>';
            });
            html += '</select>';

            //Add Option Odd/Even weeks
            html += '<span style="padding: 0 5px 0 15px;font-weight: bold;" class="hs_schedule_by">'+DOTB.language.get('J_Class','LBL_SCHEDULE_BY')+':</span>';
            html += '<select class="hs_schedule_by" title="'+DOTB.language.get('J_Class','LBL_SCHEDULE_TEACHER_BY_DES')+'">';
            $.each(parent.DOTB.App.lang.getAppListStrings('schedule_teacher_by_list'), function (key_, val_) {
                html += '<option value="'+key_+'">'+val_+'</option>';
            });
            html += '</select>';

            $(this).closest('div').append(html);
        }else {
            $(".time_slot,.hs_schedule_by").prop("disabled",false);
            $(this).closest('div').find('.time_slot,.hs_schedule_by').remove();
        }
    });

    $('.show_more_schedule').live('click', function() {
        //toggle elements with class .busy_schedule that their index is bigger than 2
        $(this).parent().find('.busy_schedule:gt(2)').toggle();
        //change text of show more element just for demonstration purposes to this demo
        $(this).text() === 'Show more >>' ? $(this).text('Show less <<') : $(this).text('Show more >>');
    });

});
function schedule_teacher(schedule){
    //Show dialog
    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    if ( screenHeight < 550 || screenWidth < 550 || screenWidth < screenHeight ) dialogClass = '';
    else dialogClass = 'fixed-dialog';

    $('.more_teacher_action').css('display','none');
    $('#dialog_teacher').dialog({
        resizable: false,
        width:"80%",
        height:600,
        modal: true,
        visible: false,
        dialogClass: dialogClass,
        buttons: {
            "Save":{
                click:function() {
                    saveSchedule();
                },
                class   : 'button primary btn_teacher_schedule_save',
                text    : DOTB.language.get('J_Class','LBL_SAVE'),
            },
            "Cancel":{
                click:function() {
                    $(this).dialog('close');
                    //.....
                },
                class   : 'button btn_teacher_schedule_close',
                text    : DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
            },
        },
    });
    $('#dlg_class_schedule').html($("#main_schedule").html());
    $('#class_code_schedule').text($('#class_code').text());
    $('#class_name_schedule').text($('#name').text());
    $('#upgrade_schedule').text($('#j_class_j_class_1j_class_ida').text());
    $('#start_schedule').val($("#next_session_date").val());
    $('#end_schedule').val($('#end_date').text());
    $('#select_teaching_type').val("main_teacher");
    $('#change_teacher_reason').val("");
    //Clear input
    $('input:checkbox').removeAttr('checked');
    $(".day_of_week").closest('div').find('.time_slot,.hs_schedule_by').remove();
    $(".day_of_week").attr('time_slot','');
    $(".day_of_week").next("label").hide();
    getWeekDay($('#start_schedule').val(),$('#end_schedule').val());
    clearTeacherList();
}

//Get Day of Week have session in class
function getWeekDay(str_from_date,str_to_date){
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:
        {
            type     : "ajaxGetWeekDay",
            from_date: str_from_date,
            to_date  : str_to_date,
            class_id : $("input[name='record']").val(),
        },
        dataType: "json",
        success: function(res){
            if(res.success == "1"){
                //reset day_of_week
                $('.day_of_week').prop('checked',false).trigger('change').hide();
                $('.day_of_week').next("label").hide();
                var array_date = $.parseJSON(res.array_date);
                $.each(array_date, function(key, value){
                    $("#"+key+".day_of_week").show();
                    $("#"+key+".day_of_week").attr('time_slot',JSON.stringify(value));
                    $("#"+key+".day_of_week").next("label").show();
                });
            }
        },
    });
}

function ajaxCheckScheduleTecher(available, another){
    if(typeof another == 'undefined' || another == '') another = 0;
    var day_of_week = [];
    $(".day_of_week").each(function() {
        if ($(this).prop('checked') == true)
            day_of_week.push($(this).attr("id")+ ' | ' +$(this).closest('div').find('.time_slot').val()+' | '+$(this).closest('div').find('select.hs_schedule_by').val());
    });
    var day_of_week_string = day_of_week.join(', ');
    if(day_of_week.length == 0 || $('#start_schedule').val()== '' || $('#end_schedule').val() == '' || $('#sc_type').val() == ''){
        toastr.error(DOTB.language.get('J_Class','LBL_ERR_REQUIRED'));
        $('#list_teacher tbody').html("");
        $('.btn_teacher_schedule_save').prop('disabled',true);
        return ;
    }

    $("#start_schedule,#end_schedule,.day_of_week,.time_slot, select.hs_schedule_by").prop("disabled",true);
    $("#start_schedule_trigger,#end_schedule_trigger").hide();

    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:
        {
            type        : "ajaxGetTeacherBySchedule",
            class_id    : $("[name='record']").val(),
            from_date   : $('#start_schedule').val(),
            to_date     : $('#end_schedule').val(),
            sc_type     : $('#sc_type').val(),
            day_of_week : day_of_week,
            available   : available,
            another     : another,
        },
        success: function(data){
            $("#teacher_schedule_loading_icon").hide();
            if(available){
                if(another){
                    $('#list_teacher_available_another tbody').html(data);
                    another_teacher();
                }
                else{
                    $('#list_teacher tbody').html(data);
                }
            }
            else {
                $('#list_teacher_busy tbody').html(data);
                busy_teacher();
                $('.more_schedule').each(function(){
                    if ($(this).find('.busy_schedule').length > 3) {
                        $(this).find('.busy_schedule:gt(2)').hide();
                        $(this).find('.show_more_schedule').show();
                    }
                });
            }
            if($('#sc_type').val() == 'Room'){
                $('.more_teacher_action').css('display','none');
            }
            else{
                $('.more_teacher_action').css('display','');
            }
            $('.btn_teacher_schedule_save, .btn_teacher_schedule_close').prop('disabled',false);
            $('[name="teacher_schedule_radio"]:first').prop("checked",true);
            // Remember this filter to alert when save
            $('#teacher_schedule_start_date').val($('#start_schedule').val());
            $('#teacher_schedule_end_date').val($('#end_schedule').val());
            $('#teacher_schedule_day_of_week').val(day_of_week_string);

            $("#start_schedule,#end_schedule,.day_of_week").prop("disabled",false);
            $("#start_schedule_trigger,#end_schedule_trigger").show();
            var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
            bwcComponent.rewriteLinks();
        },
    });
}

function saveSchedule(){
    var scheduleForId  = $('[name="teacher_schedule_radio"]:checked').val();
    var teacher_name   = $('[name="teacher_schedule_radio"]:checked').closest("tr").find(".teacher_name").text();
    var contract_until = $('[name="teacher_schedule_radio"]:checked').closest("tr").find(".schedule_contract_until").val();
    var start_date     = $('#teacher_schedule_start_date').val();
    var end_date       = $('#teacher_schedule_end_date').val();
    var sc_type        = $('#sc_type').val();
    var teaching_type  = $('#select_teaching_type').val();
    var change_reason  = $('#change_teacher_reason').val();
    var class_id       = $('input[name=record]').val();
    var day_of_week    = $('#teacher_schedule_day_of_week').val();

    if(typeof scheduleForId == 'undefined') toastr.error(DOTB.language.get('J_Class','LBL_SCHEDULE_NO_TEACHER'));
    else{
        var alertString = DOTB.language.get('J_Class','LBL_NAME')+": "+$('#name').text();
        alertString += "<br>"+DOTB.language.get('J_Class','LBL_FROM_DATE')+": "+start_date;
        alertString += "<br>"+DOTB.language.get('J_Class','LBL_TO_DATE')+": "+ end_date;
        alertString += "<br>"+DOTB.language.get('J_Class','LBL_WEEKDAY')+": "+day_of_week;
        if( typeof contract_until != 'undefined'
            && contract_until != ''
            && Date.compare(DOTB.util.DateUtils.parse(contract_until, cal_date_format), DOTB.util.DateUtils.parse(end_date,cal_date_format)) < 0){
            alertString += "<br><b>"+DOTB.language.get('J_Class','LBL_NOTICE')+"</b> " + teacher_name + " "+DOTB.language.get('J_Class','LBL_EXP_CONTRACT')+" " + contract_until}
        alertString += "<br><br> "+DOTB.language.get('J_Class','LBL_ALERT_SAVE');
        var alertTit = (scheduleForId != '') ? (DOTB.language.get('J_Class','LBL_SCHEDULE_FOR')+': '+teacher_name) : teacher_name;

        $.confirm({
            title: alertTit,
            content: alertString,
            buttons: {
                "OK": {
                    btnClass: 'btn-blue',
                    action: function(){
                        $('.btn_teacher_schedule_save, .btn_teacher_schedule_close').prop('disabled',true);
                        $.ajax({
                            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
                            type: "POST",
                            async: true,
                            data:{
                                type            : "ajaxSaveTeacherSchedule",
                                scheduleForId   : scheduleForId,
                                class_id        : class_id,
                                from_date       : start_date,
                                to_date         : end_date,
                                day_of_week     : day_of_week,
                                sc_type         : sc_type,
                                teaching_type   : teaching_type,
                                change_reason   : change_reason,
                            },
                            success: function(data){
                                $('.btn_teacher_schedule_save, .btn_teacher_schedule_close').prop('disabled',false);
                                $(".time_slot,.hs_schedule_by").hide();
                                $('#dialog_teacher').dialog('close');
                                showSubPanel('j_class_meetings', null, true);
                                showSubPanel('j_class_meetings_syllabus', null, true);
                            },
                        });
                    }
                },
                "Cancel": {
                    text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                    action: function(){
                        //do nothing
                    }
                },
            }
        });
    }
}

function clearTeacherList(){
    $("#list_teacher tbody").html("");
}

function resetScreen(){
    $(".day_of_week").prop("checked",false).trigger('change');
    clearTeacherList();
}
function busy_teacher(){
    //Show dialog
    $('#list_teacher_busy').dialog({
        resizable: false,
        width:700,
        height:500,
        modal: true,
        visible: false,
        position: { my: 'top', at: 'top+50' },
        beforeClose: function (event, ui) {
            $('#list_teacher_busy').dialog("destroy");
        },
        buttons: [
            {
                text: "Close",
                class    : 'button btn_cancel_cancel',
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}
function another_teacher(){
    //Show dialog
    $('#list_teacher_available_another').dialog({
        resizable: false,
        width:1000,
        height:500,
        modal: true,
        visible: false,
        position: { my: 'top', at: 'top+50' },
        beforeClose: function (event, ui) {
            $('#list_teacher_available_another').dialog("destroy");
        },
        buttons: [
            {
                text: "Close",
                class    : 'button btn_cancel_cancel',
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}
