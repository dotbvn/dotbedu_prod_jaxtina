$(document).ready(function(){
    $('#export_other_file').click(function(){
        //responsive
        var screenWidth = window.screen.width;
        var screenHeight = window.screen.height;
        var dialogWidth = "60%";
        var dialogHeight = screenHeight * .60;
        if ( screenHeight < 550 || screenWidth < 550 ) dialogClass = '';
        else dialogClass = 'fixed-dialog';

        $('#export_other_dialog').dialog({
            resizable    : false,
            width        : dialogWidth,
            height       : dialogHeight,
            dialogClass  : dialogClass,
            modal        : true,
            visible      : true,
            buttons: {
                "export":{
                    click:function() {
                        var studentID = new Array();
                        var template    = $('#template').val();
                        var classID     = $('input[name="record"]').val();

                        $('#export_other_dialog').parent().find('[name="student_id[]"]:checked').each(function(){
                            studentID.push($(this).val());
                        });

                        studentID = JSON.stringify(studentID);
                        var url = 'index.php?module=J_Class&action=exportfile&template=' + template + '&classID=' + classID + '&studentID=' + studentID + '&certificate_no='+$('#certificate_no').val();
                        window.open(url,'_blank');
                        $(this).dialog('close');
                    },
                    class   : 'button primary',
                    text    : DOTB.language.get('J_Class','LBL_EXPORT'),
                },
                "cancel":{
                    click:function(){$(this).dialog('close');},
                    class   : 'button btn_cancel',
                    text    : DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                },
            },
        });
    });

    $("#template").change(function(){
        var template = $(this).val();
        $('.certificate_no').show();

        $.ajax({
            type: "POST",
            url: "index.php?module=J_Class&action=showdialog&dotb_body_only=true",
            data:{'template':$("#template").val(), 'classID':$('input[name="record"]').val()} ,
            success: function(data){
                $('#tbl_export_tp tbody').html(data);
            }
        });
    });
});