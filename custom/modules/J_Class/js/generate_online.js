$(document).ready(function(){
    $('.btn_create_online').live('click', function(){

        $('#go_ssid').val($(this).attr("id"));
        //$('#go_learning_type').val($(this).attr("learning_type"));
        $('#go_date_start').text($(this).attr("date_start"));
        $('#go_date_end').text($(this).attr("date_end"));
        $('#generate_online').dialog({
            resizable    : false,
            width        : 'auto',
            height       : 'auto',
            modal        : true,
            visible      : true,
            dialogClass: 'fixed-dialog',
            buttons: {
                "Save":{
                    click:function() {
                        $(this).dialog('close');
                        DOTB.ajaxUI.showLoadingPanel();

                        jQuery.ajax({
                            url: "index.php?module=J_Class&dotb_body_only=true&action=handleAjaxJclass",
                            type: "POST",
                            data:{
                                type               : "generate_online",
                                go_ssid            : $('#go_ssid').val(),
                                go_learning_type   : $('#go_learning_type').val(),
                                class_id           : $('input[name=record]').val(),
                            },
                            dataType: "json",
                            success: function(res){

                                DOTB.ajaxUI.hideLoadingPanel();
                                if(res.success == 1){
                                    showSubPanel('j_class_meetings_syllabus', null, true);
                                    toastr.success(res.notify);
                                }else
                                    toastr.error(res.notify);
                            },
                            error: function(){
                                DOTB.ajaxUI.hideLoadingPanel();
                                toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                            },
                        });
                    },
                    class   : 'button primary',
                    text    : DOTB.language.get('J_Class','LBL_SAVE'),
                },
                "Cancel":{
                    click:function() {
                        $(this).dialog('close');
                    },
                    class   : 'button',
                    text    : DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                },
            },
        });
    });



    $('.btn_cancel_online').live('click', function(){
        var learning_type = $(this).attr("learning_type");
        var go_ssid       = $(this).attr("id");
        $.confirm({
            title: parent.DOTB.App.lang.get('LBL_CONFIRM_LMS_UNLINK', 'Meetings') +" "+ learning_type,
            content: parent.DOTB.App.lang.get('LBL_CONFIRM_LMS_UNLINK_DES', 'Meetings'),
            buttons: {
                "OK": {
                    btnClass: 'btn-blue',
                    text    : parent.DOTB.App.lang.get('LBL_REMOVE_LMS_LINK', 'Meetings'),
                    action  : function(){
                        DOTB.ajaxUI.showLoadingPanel();
                        jQuery.ajax({
                            url: "index.php?module=J_Class&dotb_body_only=true&action=handleAjaxJclass",
                            type: "POST",
                            data:{
                                type     : "cancel_online",
                                go_ssid  : go_ssid,
                                class_id : $('input[name=record]').val(),
                            },
                            dataType: "json",
                            success: function(res){
                                DOTB.ajaxUI.hideLoadingPanel();
                                if(res.success == 1){
                                    showSubPanel('j_class_meetings_syllabus', null, true);
                                    toastr.success(res.notify);
                                }else
                                    toastr.error(res.notify);

                            },
                            error: function(){
                                DOTB.ajaxUI.hideLoadingPanel();
                                toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                            },
                        });
                    }
                },
                "Cancel": {
                    text    : parent.DOTB.App.lang.get('LBL_CANCEL_LMS_LINK', 'Meetings'),
                    class   : 'button btn_cancel',
                    action  : function(){
                    }
                },
            }
        });





    });
});