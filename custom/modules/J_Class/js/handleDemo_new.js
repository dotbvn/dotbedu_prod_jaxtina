$(document).ready(function(){
    showDialogDemo();
    $('#dm_lesson_date, #dm_lesson_to_date').on('change',function(){
        var res = calSession(to_db_date($('#dm_lesson_date').val()), to_db_date($('#dm_lesson_to_date').val()));
        setValueToFieldDemo(res.total_sessions, res.total_hours);
    });

    $('#btn_open_popup_demo').live('click', function(){
        $('#dm_add_type').val('create');
        $('#dm_lesson_date').val($('#start_date').text());
        $('#dm_lesson_to_date').val($('#end_date').text());

    });
});

function setValueToFieldDemo(total_sessions, total_hours){
    $('#dm_total_sessions').val(total_sessions);
    $('#dm_total_hours').val(total_hours);
    $('#dm_total_sessions_text').text(total_sessions).effect("highlight", {color: 'red'}, 1000);
    $('#dm_total_hours_text').text(formatNumber(total_hours,num_grp_sep,dec_sep,2,2)).effect("highlight", {color: 'red'}, 1000);
}

//Mở popup chọn học viên
function open_popup_demo(thisButton){
    if( (current_status.value == 'Closed' || current_status.value == 'Finished') && is_admin != '1' ){
        $.alert("<span class=\"ui-icon ui-icon-alert\" style=\"float:left;\"></span> "+DOTB.language.get('J_Class','LBL_ERR_CLASS_CLOSED')+current_status.value+" !!");
        return ;
    }
    var module = thisButton.closest('tr').find('select#parent_demo').val();
    open_popup(module, 1000, 700, "", true, false, {"call_back_function":"set_return_lead_student","form_name":"EditView","field_to_name_array":{"id":"student_id","name":"student_name","converted":"converted"}}, "single", true);
};

//Trả về pop-up và Show dialog
function set_return_lead_student(popup_reply_data) {
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    if(typeof(name_to_value_array.converted) != 'undefined'
        && name_to_value_array.converted === '1'){
        app.alert.show('message-id', {
            level: 'warning',
            messages: DOTB.language.get('J_Class','LBL_CONVERTED_LEAD'),
            autoClose: false,
        });

        setTimeout(function(){ app.alert.dismiss('message-id'); }, 10000);
    }
    $('#dm_student_id').val(name_to_value_array.student_id);
    $('#dm_student_name').text(name_to_value_array.student_name);
    $('#dm_type_student').val($('#parent_demo').val());
    showDialogDemo();
}

//Show dialog Demo
function showDialogDemo(){
    var dm_student_id 	= $('#dm_student_id').val();
    var dm_type_student = $('#dm_type_student').val();
    if(dm_student_id == '' || dm_type_student == '') return false;
    $("body").css({ overflow: 'hidden' });
    $('#diaglog_demo').dialog({
        resizable: false,
        width:'auto',
        height:'auto',
        modal: true,
        visible: true,
        beforeClose: function(event, ui) {
            $("body").css({ overflow: 'inherit' });
        },
        buttons: {
            "Add Students":{
                click:function(event) {
                    addDemo();
                },
                class   : 'button primary btn_add_demo',
                text    : DOTB.language.get('J_Class','LBL_BTN_ADD_C'),
            },
            "Cancel":{
                click:function() {
                    $(this).dialog('close');
                    //.....
                },
                class   : 'button btn_add_demo',
                text    : DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
            },
        },
    });
    var res = calSession(to_db_date($('#dm_lesson_date').val()), to_db_date($('#dm_lesson_to_date').val()));
    setValueToFieldDemo(res.total_sessions, res.total_hours);
}

function addDemo(){
    var dm_student_id 	= $('#dm_student_id').val();
    var dm_lesson_date  = $('#dm_lesson_date').val();
    var dm_lesson_to_date = $('#dm_lesson_to_date').val();
    var dm_type_student = $('#dm_type_student').val();
    var dm_class_id 	= $('input[name=record]').val();

    var res = calSession(to_db_date(dm_lesson_date), to_db_date(dm_lesson_to_date));
    if(res['total_hours'] == 0 || dm_student_id == '' || dm_lesson_date == '' || dm_lesson_to_date == '' || dm_type_student == '') return false;
    $('.btn_add_demo').prop('disabled',true);
    ajaxStatus.showStatus('Saving <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        data:
        {
            dm_student_id     : dm_student_id,
            dm_lesson_date    : dm_lesson_date,
            dm_lesson_to_date : dm_lesson_to_date,
            dm_type_student   : dm_type_student,
            dm_class_id       : dm_class_id,
            enroll_list       : res['sessions'],
            type              : "addDemo",
        },
        dataType: "json",
        success:function(data){
            ajaxStatus.hideStatus();
            $('.btn_add_demo').prop('disabled',false);
            if(data.success == "1") {
                showSubPanel('j_class_studentsituations', null, true);
                toastr.success(data.notify);
                $('#diaglog_demo').dialog("close");
                progressStudents();
            }else toastr.error(data.error);
        },
    });
}

//Set up calendar
Calendar.setup ({
    inputField : "dm_lesson_date",
    daFormat : cal_date_format,
    button : "dm_lesson_date_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
    }
);
Calendar.setup ({
    inputField : "dm_lesson_to_date",
    daFormat : cal_date_format,
    button : "dm_lesson_to_date_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
    }
);
