$( document ).ajaxStop(function() {
    $("#btn_send_all").show();
});

$(document).ready(function() {
    $('#btn_select_class').click(function(){open_popup('J_Class', 1000, 700, '', true, false, {"call_back_function":"set_class_return","form_name":"EditView","field_to_name_array":{"id":"class_id","name":"class_name"}}, "single", true);});

    $('#btn_clr_class').click(function(){
        $("#class_id").val('');
        $("#class_name").val('');
        $("#json_session").val('');
        $(".student_list").find("tbody").html('')
        loadClassInfo();
    });

    $('#clear_content').click(function(){
        //Lock input
        $('#class_name, #btn_select_class, #btn_clr_class, #date_in_content').prop('disabled',false).removeClass('input_readonly');
        $('#date_in_content_trigger').show();
        $('#ss_id').prop('disabled',false);
        $("#tbl_student tbody").html('');
        $("#session_id").val('');
        $("#session_description").val('');
    });

    if ($("#class_id").val() != '' && $("#date_in_content").val() != '') load_ss_json();

    $("#date_in_content").on("change",function(){
        $("#json_session").val('');
        load_ss_json();
        load_template();
        loadClassInfo();
    });

    $("#ss_id").live("change",function(){load_student();});
    $('.homework_score').mask("000");
    $('.loyalty_point').mask("0000");

    $('.homework_comment, .description, .care_comment, .sms_content').live('click',function(){
        open_popup_comment($(this));
    });
    $('#messenges').live('click',function(){
        $('#dialog_messenge').dialog({
            resizable: false,
            width:'700px',
            height:'auto',
            modal: true,
            visible: true,
            dialogClass: 'fixed-dialog',
        });
    })
    $('#ok_messenge').live('click',function(){
        handleSaveAll('sms_content', $('#template_content').val());
        $("#dialog_messenge" ).dialog("close");
    });
})

function load_ss_json(){
    var json_ss     = $("#json_session").val();
    var class_id    = $("#class_id").val();
    var session_id  = $("#session_id").val();
    var date        = $("#date_in_content").val();
    if(json_ss == '' || json_ss == '{}' || json_ss == '[]'){
        //AJAX load JSON Session
        parent.DOTB.App.api.call('read',  parent.DOTB.App.api.buildURL('get_ss_att')+"&class_id=" + class_id + "&session_id=" + session_id + "&date=" + date, null, {
            dataType: "json",
            success: function (data) {
                json_ss = data;
            },
            error: function (e) {
                toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
            }
            }, {async: false});
    }
    //Build Selection
    if(json_ss != '' && json_ss != '{}' && json_ss != '[]'){
        var objs = $.parseJSON(json_ss);
        var select = '<select id="ss_id" name="ss_id" style="padding: 4px;">';
        $.each(objs, function(key, value) {
            select += '<option value="'+value.id+'" '+value.selected+'>'+value.time+'</option>';
        });
        select += '</select>';
        $("#time_html").html(select);
        load_student();
    }else{
        $("#time_html").html('');
        toastr.error(DOTB.language.get('J_Class','LBL_NO_SESION_ON')+' '+date);
    }
    loadClassInfo();

}

function set_class_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'class_id':
                    $("#class_id").val(val);
                    break;
                case 'class_name':
                    $("#class_name").val(val);
                    break;
            }
        }
    }
    load_template();
    if ($("#date_in_content").val() != '') load_ss_json();
}

// Load session id & all student of this session
function load_student(){
    //Trigger show send
    showSend();
    $('#showless_student').trigger('click');
    ajaxStatus.showStatus(DOTB.language.get('J_Class','LBL_LOADING_STUDENT_LIST_C'));
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type            : 'ajaxGetStudentList',
            class_id        : $("#class_id").val(),
            ss_id           : $("#ss_id").val(),
            date_in_content : $("#date_in_content").val(),
        },
        dataType: "json",
        success: function(res){
            if (res.success == "1") {
                $("#tbl_student tbody").html(res.content);
                $("#session_id").val(res.session_id);
                $("#session_description").val(res.session_description);
                $(".checkall_custom_checkbox").prop("checked",true);
                handleCheckBox($(".checkall_custom_checkbox"));
                $('#class_name, #btn_select_class, #btn_clr_class, #date_in_content').prop('disabled',true).addClass('input_readonly');
                $('#date_in_content_trigger').hide();
                $('#ss_id').prop('disabled',true);
                $('#topic_custom').val(res.topic_custom);
                $('#syllabus_custom').val(res.syllabus_custom);
                $('#objective_custom').val(res.objective_custom);
                $('#topic_custom_text').text(res.topic_custom);
                $('#syllabus_custom_text').text(res.syllabus_custom);
                $('#objective_custom_text').text(res.objective_custom);
            }else{
                $("#tbl_student tbody").html(res.content);
                $("#session_id").val('');
                $("#session_description").val('');
                $('#class_name, #btn_select_class, #btn_clr_class, #date_in_content').prop('disabled',false).removeClass('input_readonly');
                $('#date_in_content_trigger').show();
                $('#ss_id').prop('disabled',false);
                $('#topic_custom, #syllabus_custom, #objective_custom').val('');
                $('#topic_custom_text, #syllabus_custom_text, #objective_custom_text').text('');
            }
            //Trigger show send
            showSend();
            ajaxStatus.hideStatus();
        },
        error: function(xhr, textStatus, errorThrown){
            ajaxStatus.hideStatus();
            toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
        }
    });
}

function saveSyllabus(_this) {
    ajaxStatus.showStatus('Checking...');
    if ($("#session_id").val() == ''){
        app.alert.show('message-id', {
            level: 'error',
            messages: DOTB.language.get('J_Class','LBL_LESSON_NOT_FOUND'),
            autoClose: true
        });
        ajaxStatus.hideStatus();
        return false;
    }
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type            : 'ajaxSaveSyllabus',
            session_id      : $("#session_id").val(),
            homework_custom : $("#homework_custom").val(),
            objective_custom : $("#objective_custom").val(),
            syllabus_custom : $("#syllabus_custom").val(),
            topic_custom    : $("#topic_custom").val(),
        },
        dataType: "json",
        success: function(res){
            if(res.success == '1'){
                app.alert.show('message-id', {
                    level: 'success',
                    messages: (DOTB.language.get('J_Class','LBL_LESSON_UPDATED')),
                    autoClose: true
                });
            }else{
                app.alert.show('message-id', {
                    level: 'error',
                    messages: (DOTB.language.get('J_Class','LBL_LESSON_NOT_FOUND')),
                    autoClose: true
                });
            }
            ajaxStatus.hideStatus();
        },
    });
}

//Handle Save All Attendance
function handleSaveAll(field, value){
    if (typeof value != 'undefined' && typeof field != 'undefined'){
        var language = parent.DOTB.App.lang;
        var countDiff = countET = 0;
        var value_text = value;
        var field_text = field;
        if(field == 'attendance_type') value_text = language.getAppListStrings('attendance_type_list')[value];
        if(field == 'homework'){
            field_text = 'do_homework';
            value_text = language.getAppListStrings('do_homework_list')[value];
        }

        $('.'+field).each(function( index ){
            if($(this).val() != value && $(this).val() != '') countDiff++;
            if($(this).val() == '' && value != '') countET++
        });
        if(value_text == '') value_text = '<span style="color:red;">'+language.get('LBL_MARKALL_EMPTY_C','J_Class')+'</span>';
        if(countDiff != 0){
            $.confirm({
                title: language.get('LBL_ATT_OVERRIDE_TITLE','J_Class'),
                content: language.get('LBL_'+field_text.toUpperCase(),'C_Attendance')+': <b>'+value_text+'</b><br>'+language.get('LBL_ATT_OVERRIDE_CONTENT','J_Class'),
                buttons: {
                    "OK": {
                        btnClass: 'btn-blue',
                        action: function(){
                            saveAllAttendance(field, value);
                        }
                    },
                    'Cancel': {
                        text: language.get('LBL_BTN_CANCEL_C','J_Class'),
                        action: function(){

                        }
                    },
                }
            });
        }else if(countET != 0) saveAllAttendance(field, value);
    }

}

function saveAllAttendance(field, value){
    if (typeof value != 'undefined' && typeof field != 'undefined'){
        var listVal  = {};
        var countAtt = 0;
        $('.'+field).each(function( index ){
            //prepare data
            var attend_id             = $(this).closest('tr').attr('attend_id');
            listVal[attend_id]        = {};
            listVal[attend_id][field] = value;
            countAtt++;
        });
        if(countAtt > 0){
            //Save Data
            DOTB.ajaxUI.showLoadingPanel();
            $.ajax({
                url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
                type: "POST",
                async: true,
                data:  {
                    type       : 'ajaxSaveAllAttendance',
                    session_id : $("#session_id").val(),
                    class_id   : $("#class_id").val(),
                    listVal    : JSON.stringify(listVal),
                },
                dataType: "json",
                success: function(res){
                    if(res.success == '1'){
                        $.each(res.data, function(index, item) {
                            var attres = $.parseJSON(item);
                            if(attres.avg_attendance != '') $("[attend_id="+attres.attend_id+"]").find('.avg_attendance').html(attres.avg_attendance);
                        });
                        app.alert.show('message-id', {
                            level: 'success',
                            messages: DOTB.language.get('J_Class','LBL_SUCCESSFULLY_SAVED_C'),
                            autoClose: true
                        });
                    }else
                        app.alert.show('message-id', {
                            level: 'error',
                            messages: DOTB.language.get('J_Class','LBL_ERROR_OCCURRED_C'),
                            autoClose: true
                        });
                    if(field == 'sms_content')
                        $(".sms_content").each(function( index ) {
                            res = countSms($(this));
                        });
                    DOTB.ajaxUI.hideLoadingPanel();
                },
                error: function(){
                    DOTB.ajaxUI.hideLoadingPanel();
                    toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                },
            });
            //fill data
            $('.'+field).each(function(index){
                $(this).val(value);
            });
        }
    }
}

//Handle save one row attendance
function saveAttendance(_this){
    var this_tr  = _this.closest('tr');
    var this_td  = _this.closest('td');
    var field  = _this.attr('class');
    var value  = this_tr.find('.'+field).val();

    //if(field == 'homework_score' && value > 5) value = '5';
    if(field == 'homework_score' && value < 0) value = '0';

    //custom Jaxtina
    var jaxtina_fields = ['flipped',  'homework_num' , 'teacher_comment_num'];
    if($.inArray(field, jaxtina_fields) !== -1 && (value == '' || value == 0 || !$.isNumeric(value))){
        app.alert.show('message-id_w', {
            level: 'warning',
            messages: DOTB.language.get('J_Class','LBL_WARNING_MISSING'),
            autoClose: true
        });
    }
    ///END: custom Jaxtina
    this_tr.find('.'+field).val(value);

    //Check student in session
    var inClass = true;
    if(this_tr.hasClass("tr_not_in_class")) inClass = false;
    ajaxStatus.showStatus(DOTB.language.get('J_Class','LBL_SAVING'));
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type          : 'ajaxSaveAttendance',
            session_id    : $("#session_id").val(),
            class_id      : $("#class_id").val(),
            student_id    : this_tr.attr("pid"),
            attend_id     : this_tr.attr("attend_id"),
            savePos       : field,
            saveVal       : value,
            in_class      : inClass,
        },
        dataType: "json",
        success: function(res){
            if(res.success == '1'){
                app.alert.show('message-id', {
                    level: 'success',
                    messages: DOTB.language.get('J_Class','LBL_SUCCESSFULLY_SAVED_C'),
                    autoClose: true
                });
                if(res.avg_attendance != '' && res.avg_attendance != null) $("[attend_id="+res.attend_id+"]").find('.avg_attendance').html(res.avg_attendance);
                if(res.total_loyalty != '' && field == 'loyalty_point') $("[attend_id="+res.attend_id+"]").find('.total_loyalty_point').html(res.total_loyalty);
            }else if(res.success == "2") {
                app.alert.show('message-id', {
                    level: 'error',
                    messages: DOTB.language.get('J_Class','LBL_LOYALTY_POINT_C') + res.max_point + '.',
                    autoClose: true
                });
                $(_this).val(res.point);

            }

            //Jaxtina Customize
            else if(res.success == 9) {
                app.alert.show('message-id_e', {
                    level: 'error',
                    messages: res.message,
                    autoClose: true
                });
                $(_this).val(res.old_value);
            }else if(res.success == 10){
                $.each(res, function( key, value ) {
                    this_tr.find('.'+key).val(value);
                });
                app.alert.show('message-id_s', {
                    level: 'success',
                    messages: DOTB.language.get('J_Class','LBL_SUCCESSFULLY_SAVED_C'),
                    autoClose: true
                });
            } else {
                app.alert.show('message-id', {
                    level: 'error',
                    messages: DOTB.language.get('J_Class','LBL_ERROR_OCCURRED_C'),
                    autoClose: true
                });
            }
            ajaxStatus.hideStatus();
        },
        error: function(){
            ajaxStatus.hideStatus();
            toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
        },
    });
}

function saveSessionDescription(){
    if ($("#session_id").val() == '') return false;
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type            : 'ajaxSaveSessionDescription',
            session_id      : $("#session_id").val(),
            description     : $("#session_description").val(),
        },
        dataType: "json",
        success: function(res){
        },
    });
}

//Show/hide button send type
function showSend(_this){
    if(_this == '' || typeof _this == 'undefined') var _this = $('#send_button');
    var send_type = _this.attr('value');
    $('#send_button').text(_this.text());
    $("#send_button").attr("onclick","sendMgsApp('all','"+send_type+"')");
    $("#send_button").attr("value",send_type);

    switch (send_type){
        case 'send_att':
            $(".send_att").show();
            $(".send_mes").hide();
            break;
        case 'send_mes':
            $(".send_att").hide();
            $(".send_mes").show();
            $(".message_counter").hide();
            break;
        case 'send_sms':
            $(".send_att").hide();
            $(".send_mes").show();
            $(".message_counter").show();
            $(".sms_content").each(function( index ) {
                res = countSms($(this));
            });
            break;
    }
}

function sendMgsApp(attend_id, send_type){
    if(attend_id == '' || typeof attend_id == 'undefined') return ;
    var rowAtt = attend_id == 'all' ? $("input.custom_checkbox:checked").closest('tr') : $("tr[attend_id="+attend_id+"]");
    if(send_type == '' || typeof send_type == 'undefined') send_type = $('#send_button').attr('value');
    if(send_type == '' || typeof send_type == 'undefined') return ;
    var listVal = {};
    var count   = countEmpty = 0;
    var mesStd  = '';
    rowAtt.each(function(ind, attend_tr){
        var att_id     = $(attend_tr).attr('attend_id');
        var student_id = $(attend_tr).attr('pid');
        var msg_content= $(attend_tr).find(".sms_content").val();
        var validated  = true;
        //validate
        switch (send_type){
            case 'send_att':
                if(att_id == '' || typeof att_id == 'undefined') validated = false;
                break;
            case 'send_mes':
            case 'send_sms':
                if(msg_content == '' || typeof msg_content == 'undefined'){
                    validated = false;
                    countEmpty++;
                    mesStd = $(attend_tr).attr('pname');
                }
                break;
        }
        if(validated){
            //prepare data
            listVal[student_id]                 = {};
            listVal[student_id]['attend_id']    = att_id;
            listVal[student_id]['send_type']    = send_type;
            listVal[student_id]['student_id']   = student_id;
            listVal[student_id]['student_name'] = $(attend_tr).attr('pname');
            listVal[student_id]['student_type'] = $(attend_tr).attr('ptype');
            listVal[student_id]['msg']          = msg_content;
            count++;
        }
    });
    if(countEmpty == 1) toastr.error(DOTB.language.get('J_Class','LBL_EMPTY_CONTENT'), mesStd);
    if(countEmpty > 1) toastr.error(DOTB.language.get('J_Class','LBL_EMPTY_CONTENT'), countEmpty+' '+DOTB.language.languages['app_strings']['LBL_RECORDS'].toLowerCase());

    if(count == 1) ajaxSendMgsApp(listVal, send_type);
    if(count > 1){
        $.confirm({
            title: DOTB.language.get('J_Class','LBL_CONFIRM_C'),
            content: DOTB.language.get('J_Class','LBL_SEND_ALL_STUDENT_C'),
            buttons: {
                "OK": {
                    btnClass: 'btn-blue',
                    action: function(){
                        DOTB.ajaxUI.showLoadingPanel();
                        var numMes  = Object.keys(listVal).length;
                        var counter = 0;
                        var cTxt    = $('#loadingPage').find('b').text();
                        var looper  = setInterval(function(){
                            counter++;
                            $('#loadingPage').find('b').text(DOTB.language.get('J_Class','LBL_SENDING')+counter+'/'+numMes);
                            if (counter >= numMes){
                                clearInterval(looper);
                                $('#loadingPage').find('b').text(cTxt);
                            }
                            }, Math.floor((Math.random() * (580 +1)) + 200));
                        ajaxSendMgsApp(listVal, send_type);
                    }
                },
                'Cancel': {
                    text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                    action: function(){}
                },
            }
        });
    }
}
function ajaxSendMgsApp(listVal, send_type){
    //show load buttons
    $.each(listVal, function(student_id, value ){
        var attend_tr = $("tr[pid="+student_id+"]");
        attend_tr.find(".loading_icon").show();
        attend_tr.find(".btn_send").hide();
    });
    //Send ALL
    $.ajax({
        url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type       : 'ajaxsendMgsApp',
            session_id : $("#session_id").val(),
            class_id   : $("#class_id").val(),
            listVal    : JSON.stringify(listVal),
            send_type  : send_type,
        },
        dataType: "json",
        success: function(data){
            if(data.success == '1'){
                if(data.count_success > 1){
                    app.alert.show('message-success', {
                        level: 'success',
                        messages: DOTB.language.get('J_Class','LBL_SENT_SUCCESS_LIST').replace("{count}", data.count_success),
                        autoClose: true
                    });
                }
                $.each(data.sent_success, function( index, value ){
                    var this_btn = $("[pid="+value.student_id+"]").find('.btn_send');
                    this_btn.closest('td').find(".loading_icon").hide();
                    this_btn.val(DOTB.language.get('J_Class','LBL_SEND'));
                    this_btn.css("background-color","");
                    $("<span style='color: red;'>"+DOTB.language.get('J_Class','LBL_SENT')+"</span>").appendTo( this_btn.closest('td') ).fadeOut(3000);
                    this_btn.hide().delay(3000).fadeIn('fast');
                    if(data.count_success<5)
                        toastr.success(DOTB.language.get('J_Class','LBL_SMS_SENT') + value.student_name);
                });



                if(data.count_fail > 1){
                    app.alert.show('message-fail', {
                        level: 'error',
                        messages: DOTB.language.get('J_Class','LBL_SENT_FAIL_LIST').replace("{count}", data.count_fail),
                        autoClose: true
                    });
                }
                $.each(data.sent_fail, function( index, value ){
                    var this_btn = $("[pid="+value.student_id+"]").find('.btn_send');
                    this_btn.closest('td').find(".loading_icon").hide();
                    this_btn.val(DOTB.language.get('J_Class','LBL_RESEND'));
                    this_btn.css("background-color","orange");
                    this_btn.show();
                    if(data.count_fail<5)
                        toastr.error(DOTB.language.get('J_Class','LBL_SMS_NOT_SENT') + value.student_name);
                });
            }else
                app.alert.show('message-id', {
                    level: 'error',
                    messages: DOTB.language.get('J_Class','LBL_ERROR_OCCURRED_C'),
                    autoClose: true
                });
            DOTB.ajaxUI.hideLoadingPanel();
        },
        error: function(){
            DOTB.ajaxUI.hideLoadingPanel();
            toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
        },
    });
}

//Keyup count character for SMS/Apps Message
function genenrateMes(js_class){
    var language = parent.DOTB.App.lang;
    // prompt dialog
    $.confirm({
        title: DOTB.language.get('J_Class','LBL_ENTER_MESSAGES_C'),
        content:'<form action="" class="formName">' +
        '<div class="form-group">' +
        '<div>'+language.get('LBL_'+js_class.toUpperCase(),'C_Attendance')+': </div>' +
        '<div><textarea placeholder="'+language.get('LBL_PLACEHOLDER_MESSAGES_C','J_Class')+'" class="name form-control" required style="width: 85%;" rows="5"></textarea></div>' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: DOTB.language.get('J_Class','LBL_BTN_SUBMIT_C'),
                btnClass: 'btn-blue',
                action: function () {
                    var content = this.$content.find('.name').val();
                    handleSaveAll(js_class, content);
                }
            },
            'Cancel': {
                text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

//Load SMS Template
function load_template() {
    var class_id = $("#class_id").val();
    var id = $("#template").val();
    var appModel = parent.App.controller.context.get("model");
    parent.DOTB.App.api.call('read',  parent.DOTB.App.api.buildURL('sms-template')+"&id=" + id + "&mod=" + appModel.module + "&rec=" + class_id, null, {
        dataType: "json",
        success: function (data) {
            if (data.success == "1") {
                $("#template_content").val(data.content.replace("$date_in_content",date_in_content)).trigger('click');
            }
        }
    });
}

function loadClassInfo(){
    var class_id    = $("#class_id").val();
    var session_id  = $("#ss_id").val();
    if (class_id == '') $("#td_class_info").html('');
    else{
        $.ajax({
            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
            type: "POST",
            async: true,
            data:  {
                type       : 'ajaxLoadClassInfo',
                session_id : session_id,
                class_id   : class_id,
            },
            dataType: "json",
            success: function(res){
                $("#td_class_info").html(res.html);
            },
            error: function(xhr, textStatus, errorThrown){
            }
        });
    }
}

//Function Phụ trợ
function open_popup_comment(_this) {
    var language = parent.DOTB.App.lang;
    var cmt_name = _this.attr('class');
    var title = language.get('LBL_'+cmt_name.toUpperCase(),'C_Attendance');
    $('td#title').html('<b>'+title+': </b>')
    var student_name = _this.closest('tr').find('a.student_name').text();
    $('.dialog_header span#dialog_student_name').text(student_name);
    $("[name=dialog_text_comment]").val(_this.val());
    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    var dialogWidth = "60%";
    var dialogHeight = screenHeight *.35;
    if ( screenHeight < 550 || screenWidth < 550 ) {
        dialogClass = '';
        dialogWidth = '70%';
    } else {
        dialogClass = 'fixed-dialog';
    }

    $('#dialog_comment').dialog({
        messages: title,
        width  : dialogWidth,
        height : dialogHeight,
        resizable: false,
        modal: true,
        dialogClass: dialogClass,
        buttons:
        [
            {
                text: language.get('LBL_BTN_SUBMIT_C','J_Class'),
                class: 'button primary',
                click: function () {
                    _this.val($("[name=dialog_text_comment]").val());
                    saveAttendance(_this);
                    $(this).dialog("close");
                }
            }, {
                text: language.get('LBL_BTN_CANCEL_C','J_Class'),
                class: 'button',
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

// Setup date field
Calendar.setup ({
    inputField : "date_in_content",
    daFormat : cal_date_format,
    button : "date_in_content_trigger",
    singleClick : true,
    dateStr : '',
    step : 1,
    weekNumbers:false
});

function showHelp(el, helpText, id) {
    myPos = "left top";
    atPos = "left top";
    var pos = $(el).offset()
    , ofWidth = $(el).width()
    , elmId = id || 'helpTip' + pos.left + '_' + ofWidth
    , $dialog = elmId ? ($("#" + elmId).length > 0 ? $("#" + elmId) : $('<div></div>').attr("id", elmId)) : $('<div></div>');
    $dialog.html(helpText).dialog({
        autoOpen: false,
        title: DOTB.language.get('app_strings', 'LBL_HELP'),
        position: {
            my: myPos,
            at: atPos,
            of: $(el)
        }
    });
    var width = $dialog.dialog("option", "width");
    if ((pos.left + ofWidth) - 40 < width) {
        $dialog.dialog("option", "position", {
            my: 'left top',
            at: 'right top',
            of: $(el)
        });
    }
    $dialog.dialog("option", "width", window.screen.width *.35);

    $dialog.dialog('open');
    $(".ui-dialog").appendTo("#content");
}


function showHelpTips(el,helpText,myPos,atPos,id, dialogWidth, dialogHeight) {
    if(myPos == undefined || myPos == "") {
        myPos = "left top";
    }
    if(atPos == undefined || atPos == "") {
        atPos = "right top";
    }

    var pos = $(el).offset(),
    ofWidth = $(el).width(),
    elmId = id || 'helpTip' + pos.left + '_' + ofWidth,
    $dialog = elmId ? ( $("#"+elmId).length > 0 ? $("#"+elmId) : $('<div></div>').attr("id", elmId) ) : $('<div></div>');
    $dialog.html(helpText)
    .dialog({
        autoOpen: false,
        title: DOTB.language.get('app_strings', 'LBL_HELP'),
        position: {
            my: myPos,
            at: atPos,
            of: $(el)
        }
    });


    var width = $dialog.dialog( "option", "width" );

    if((pos.left + ofWidth) - 40 < width) {
        $dialog.dialog("option","position",{my: 'left top',at: 'right top',of: $(el)})    ;
    }
    $dialog.dialog("option", "width", dialogWidth);
    $dialog.dialog("option", "height", dialogHeight);
    $dialog.dialog('open');
    $(".ui-dialog").appendTo("#content");


}