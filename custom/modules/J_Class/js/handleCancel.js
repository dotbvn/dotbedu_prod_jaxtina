var is_generate = false;
$(document).ready(function(){
    addToValidate('CancelView', 'cs_date_makeup', 'date', true, parent.DOTB.App.lang.get('LBL_MAKE_UP_DATE', 'Meetings') );
    addToValidate('CancelView', 'cs_start', 'text', true, parent.DOTB.App.lang.get('LBL_DATE_TIME', 'Meetings') );
    addToValidate('CancelView', 'cs_cancel_by', 'enum', true, parent.DOTB.App.lang.get('LBL_CANCEL_BY', 'Meetings'));
    $('.btn_check_cancel_session').live('click',function(){
        var _this = $(this);
        var date =  $('#cs_date_makeup').val();
        var start = $('input[name=cs_start]').val();
        var end =   $('input[name=cs_end]').val();
        $(this).parent().append('<span id = "cs_teacher_loading" >'+DOTB.language.get('J_Class','LBL_LOADING_C')+' <img src="custom/include/images/loader.gif" align="absmiddle" width="16"></span>');
        $(this).hide();
        ajaxStatus.showStatus(DOTB.language.get('J_Class','LBL_PROCESSING_C'));
        jQuery.ajax({
            url: "index.php?module=J_Class&dotb_body_only=true&action=handleAjaxJclass",
            type: "POST",
            data:{
                type    : "getTeacherandRoom",
                date    : date,
                start   : start,
                end     : end,
                class_id: $('input[name=record]').val(),
            },
            dataType: "json",
            success: function(data){
                ajaxStatus.hideStatus();
                $('#cs_teacher').html(data.teacher_options);
                $('#cs_teacher').select2({
                    dropdownParent: $(".fixed-dialog"),
                });
                $('#cs_room').html(data.room_options);
                $('#cs_room').select2({
                    dropdownParent: $(".fixed-dialog"),
                });
                $("#cs_teacher_loading").remove();
                $('.btn_check_cancel_session').show();
            },
            error: function(){
                ajaxStatus.hideStatus();
                toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
            },
        });
    });
    //end
    /* CHỈ CÓ TRUNG-BUG MỚI HIỂU */
    $('#cs_date_makeup').live('change',function(){
        var rs1 = checkDataLockDate($(this).attr('id'),true);
        if(!rs1) return ;

    });
    $('#cs_cancel_by').live('change',function(){
        if($(this).val() == 'Others'){
            $('#cs_cancel_reason').closest('td').show().prev().show();
            addToValidate('CancelView', 'cs_cancel_reason', 'text', true, parent.DOTB.App.lang.get('LBL_CANCEL_REASON', 'Meetings'));
        }else{
            $('#cs_cancel_reason').closest('td').hide().prev().hide();
            removeFromValidate('CancelView', 'cs_cancel_reason')
        }
    });

});

function cancelSession(this_session){
    //Show dialog
    var session_id =   this_session.attr("id");
    $('#cancel_session_id').val(session_id);
    var div_template_id = "#cancel_dialog";
    var div_template = $(div_template_id);

    //get data for cancel template
    ajaxStatus.showStatus("Processing...");
    jQuery.ajax({
        url: "index.php?module=J_Class&dotb_body_only=true&action=handleAjaxJclass",
        type: "POST",
        async: false,
        data:{
            type        : "getDataForCancelSession",
            session_id  : session_id,
        },
        dataType: "json",
        success: function(data){
            if(data.success == 1){
                $('#cs_date_makeup').val(data.session_date).attr('oldval',data.session_date);
                $('#cs_cancel_reason').val('');
                $('#cs_change_teacher_reason').val('');

                $('#cs_cancel_by').val('').trigger('change');
                $('#cs_this_schedule').attr('checked',true);
                $('input[name=cs_start]').val(data.start_time).attr('oldval',data.start_time);
                $('input[name=cs_end]').val(data.end_time).attr('oldval',data.end_time);
                $('#cs_duration').val(data.duration);

                $('#cs_teacher').html("<option value=''>"+DOTB.language.get('J_Class','LBL_NONE_C')+"</option>");
                $('#cs_room').html("<option value=''>"+DOTB.language.get('J_Class','LBL_NONE_C')+"</option>");
                $('#cs_cancel_date').html(data.ss_date);
                $('#cs_cancel_time').html(data.ss_time);

                //LOAD DIALOG
                var duration = parseInt($('#cs_duration').val());
                if(!is_generate) {
                    $(div_template_id + ' .timeOnly').each(function () {
                        $(this).find(".time").eq(0).timepicker({
                            'minTime': '6:00am',
                            'maxTime': '9:30pm',
                            'showDuration': false,
                            'timeFormat': DOTB.expressions.userPrefs.timef,
                            'step': 15
                        });
                        $(this).find(".time").eq(1).timepicker({
                            'showDuration': true,
                            'timeFormat': DOTB.expressions.userPrefs.timef,
                            'step': 15
                        });
                        var timeOnlyDatepair = new Datepair(this, {
                            'defaultTimeDelta': duration * 60000 // milliseconds
                        });

                        $(this).on('rangeSelected', function(){
                            var js_start    = DOTB.util.DateUtils.parse($(this).find(".start").val(),DOTB.expressions.userPrefs.timef);
                            var js_end      = DOTB.util.DateUtils.parse($(this).find(".end").val(),DOTB.expressions.userPrefs.timef);
                            var duration    = formatNumber(((js_end - js_start)/60000),num_grp_sep,dec_sep);
                            $('#cs_duration').val(duration);
                        });
                    });
                    is_generate = true;
                }   else {
                    $('input[name=cs_start]').timepicker('setTime', $('input[name=cs_start]').attr('oldval'));
                    $('input[name=cs_end]').timepicker('setTime', $('input[name=cs_end]').attr('oldval')).trigger('change');
                }
                setReadonlyField(true,div_template);

                $('input[type=radio][name="cs_makeup_type"]').live('change',function() {
                    if (this.value == 'this_schedule') setReadonlyField(true,div_template);
                    else setReadonlyField(false,div_template);
                });
                //responsive
                var screenWidth = window.screen.width;
                var screenHeight = window.screen.height;
                if ( screenHeight < 550 || screenWidth < 550 || screenWidth < screenHeight ) {
                    dialogWidth = screenWidth * .80;
                    dialogClass = '';
                } else {
                    dialogWidth = screenWidth * .52;
                    dialogClass = 'fixed-dialog';
                }

                $(div_template_id).dialog({
                    // title: "Cancel Session",
                    title: DOTB.language.get('J_Class','LBL_CANCEL_SESSION_C'),
                    width: dialogWidth,
                    resizable: false,
                    modal: true,
                    dialogClass: dialogClass,
                    open: function(){
                        $("#cs_teacher").select2({
                            dropdownParent: $(".fixed-dialog"),
                        });
                        $('#cs_cancel_by').select2({
                            dropdownParent: $(".fixed-dialog"),
                        });
                        $('#cs_teaching_type').select2({
                            dropdownParent: $(".fixed-dialog"),
                        });
                    },
                    buttons: [
                        {
                            text: DOTB.language.get('J_Class','LBL_BTN_SAVE_C'),
                            class    : 'button primary btn_save_cancel',
                            click: function() {
                                saveCancelSession();
                            }
                        },
                        {
                            text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                            class    : 'button btn_cancel_cancel',
                            click: function() {
                                clear_all_errors();
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
                $(div_template_id).effect("highlight", {color: '#228ef1'}, 1000);

            }else
                toastr.error(data.message);
            ajaxStatus.hideStatus();
        },
        error: function(){
            toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
            ajaxStatus.hideStatus();
        },
    });
}

function saveCancelSession(){
    debugger;
    if(check_form("CancelView")){
        $('#cancel_dialog').dialog("close");
        DOTB.ajaxUI.showLoadingPanel();
        jQuery.ajax({
            url: "index.php?module=J_Class&dotb_body_only=true&action=handleAjaxJclass",
            type: "POST",
            dataType: "json",
            data:{
                type                : "cancelSession",
                session_id          : $('#cancel_session_id').val(),
                cancel_by           : $('#cs_cancel_by').val(),
                cancel_reason       : $('#cs_cancel_reason').val(),
                makeup_type         : $('input[name="cs_makeup_type"]:checked').val(),
                date                : $('#cs_date_makeup').val(),
                start               : $('input[name=cs_start]').val(),
                end                 : $('input[name=cs_end]').val(),
                teacher             : $('#cs_teacher').val(),
                room                : $('#cs_room').val(),
                duration            : $('#cs_duration').val(),
                cancel_teaching_type: $('#cs_teaching_type').val(),
            },
            success: function(data){
                DOTB.ajaxUI.hideLoadingPanel();
                if(data.success == 1)
                    toastr.success(data.message);
                else
                    toastr.error(data.message);

                ajaxStatus.showStatus('Reloading <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
                location.reload();
            },
        });
    }
}

function setReadonlyField(is_readonly, div_template) {
    if (is_readonly) {
        $('input[name=cs_start]').attr('readonly','readonly').addClass('input_readonly');

        $('#cs_date_makeup').val($('#cs_date_makeup').attr('oldval')).addClass('input_readonly').prop('readonly',true);
        $('#cs_date_makeup_trigger').closest('span').hide();

        div_template.find('.start').timepicker('setTime', div_template.find('.start').attr('oldval'));
        div_template.find('.end').timepicker('setTime', div_template.find('.end').attr('oldval')).trigger('change');
    } else {
        $('input[name=cs_start]').removeAttr('readonly').removeClass('input_readonly');

        $('#cs_date_makeup').val($('#cs_cancel_date').text()).removeClass('input_readonly').prop('readonly',false);
        $('#cs_date_makeup_trigger').closest('span').show();

        div_template.find('.end').trigger('change');
    }
}

function deleteSession(_this) {
    $.confirm({
        title: DOTB.language.get('J_Class','LBL_CONFIRM_ALERT_TITLE'),
        content: DOTB.language.get('J_Class','LBL_DELETE_SESSON'),
        buttons: {
            formSubmit: {
                btnClass: 'btn-blue',
                text: 'OK',
                action: function(){
                    ajaxStatus.showStatus('Deleting...');
                    jQuery.ajax({
                        url: "index.php?module=J_Class&dotb_body_only=true&action=handleAjaxJclass",
                        type: "POST",
                        dataType: "json",
                        data:{
                            type: "deleteSession",
                            record: $('input[name=record]').val(),
                            session_id: _this.attr('id'),
                        },
                        success: function(data){
                            ajaxStatus.hideStatus();
                            toastr.success(DOTB.language.get('J_Class','LBL_DELETED'));
                            window.onbeforeunload = null;
                            showSubPanel('j_class_meetings', null, true);
                        },
                        error: function(){
                            ajaxStatus.hideStatus();
                            toastr.success(DOTB.language.get('J_Class','LBL_ERROR_OCCURRED_C'));
                            window.onbeforeunload = null;
                            showSubPanel('j_class_meetings', null, true);
                        },
                    });
                }
            },
            "Cancel": {
                text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                action: function(){

                }
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            var form = this.$content.find('form');
            form.on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

