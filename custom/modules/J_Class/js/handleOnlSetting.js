var onl_status = $('#onl_status').val();

$(document).ready(function () {
    //ClassIn Settings
    // $('select[name="onl_status"]').select2({minimumResultsForSearch: -1});
    if (onl_status === '1') {
        $('#classin_settings').show();
    } else {
        $('#classin_settings').hide();
    }
    $('select[name="onl_status"]').live('change', function (e) {
        if (e.target.value === '1') {
            $('#classin_settings').show();
        } else {
            $('#classin_settings').hide();
        }
        // $('#onl_status').val(e.target.value);
    });
    $('#classin_head_teacher').select2();
    $('#classin_assistant').select2();
    $('#classin_recording').select2();
    $('#classin_students_on_stage').select2();
    $('#classin_auditors').select2({
        maximumSelectionLength: 20
    });
    $('#classin_select_folder_option').select2();
    $('#classin_select_folder_option').val('1').trigger('change');
    showClassInFolderInputHandle('1');
    // if ($('#name').val()!== '') $('#classin_folder_name').val($('#name').val());
    if ($('#onl_setting').val() !== '') {
        currentSetting = JSON.parse($('#onl_setting').val());
        $('#classin_head_teacher').val(currentSetting.classin_head_teacher).trigger('change');
        $('#classin_assistant').val(currentSetting.classin_assistant).trigger('change');
        $('#classin_recording').val(currentSetting.classin_recording).trigger('change');
        $('#classin_students_on_stage').val(currentSetting.classin_on_stage).trigger('change');
        $('#classin_auditors').val(currentSetting.classin_auditors).trigger('change');
        $('#classin_select_folder_option').val(currentSetting.classin_folder_option).trigger('change');
        showClassInFolderInputHandle(currentSetting.classin_folder_option);
        $('#classin_folder_name').val(currentSetting.classin_folder_name);
        $('#classin_folder_id').val(currentSetting.classin_folder_id);
    } else { //init
        handle_change_onl_settings('classin_recording', $('#classin_recording').val());
        handle_change_onl_settings('classin_on_stage', $('#classin_students_on_stage').val());
        handle_change_onl_settings('classin_head_teacher', $('#classin_head_teacher').val());
        handle_change_onl_settings('classin_folder_option', $('#classin_select_folder_option').val());
    }
    $('#classin_head_teacher').live('change', function (e) {
        handle_change_onl_settings('classin_head_teacher', e.target.value);
    });
    $('#classin_assistant').live('change', function (e) {
        handle_change_onl_settings('classin_assistant', e.target.value);
    });
    $('#classin_auditors').live('change', function (e) {
        const values = $('#classin_auditors').val();
        handle_change_onl_settings('classin_auditors', values);
    });
    $('#classin_recording').live('change', function (e) {
        handle_change_onl_settings('classin_recording', e.target.value);
    });
    $('#classin_students_on_stage').live('change', function (e) {
        handle_change_onl_settings('classin_on_stage', e.target.value);
    });
    $('#name').live('change', function (e) {
        $('#classin_folder_name').val(e.target.value);
    });

    $('#classin_select_folder_option').live('change', function (e) {
        $('#classin_folder_name').val('');
        $('#classin_folder_id').val('');
        showClassInFolderInputHandle(e.target.value);
        handle_change_onl_settings('classin_folder_option', e.target.value);
        handle_change_onl_settings('classin_folder_name', $('#classin_folder_name').val());
        handle_change_onl_settings('classin_folder_id', $('#classin_folder_id').val());
    });

    //ClassIn
    $('#bt_select_classin_folder').click(function () {
        window.open(window.location.protocol + '//' + window.location.hostname + '/index.php?module=Teams&action=classindrive&dotb_body_only=1', 'popup', 'width=600,height=600');
    });

    $('#bt_clear_classin_folder').click(function () {
        $('#classin_folder_id').val('');
        $('#classin_folder_name').val('');
    });
});

function handle_change_onl_settings(new_key, new_value) {
    let data = $('#onl_setting').val();
    let arr = {};
    if (data !== '') {
        arr = JSON.parse(data);
    }
    arr[new_key] = new_value;
    $('#onl_setting').val(JSON.stringify(arr));
}

function setFolderFromClassInDrive(folderId, folderName) {
    $('#classin_folder_name').val(folderName);
    $('#classin_folder_id').val(folderId);
    handle_change_onl_settings('classin_folder_id', folderId);
    handle_change_onl_settings('classin_folder_name', folderName);
}

function showClassInFolderInputHandle(option) {
    if (option === '0') {
        $('#bt_select_classin_folder').hide();
        $('#bt_clear_classin_folder').hide();
        $('#classin_folder_name').hide();
    }
    if (option === '1') {
        $('#bt_select_classin_folder').hide();
        $('#bt_clear_classin_folder').hide();
        $('#classin_folder_name').show();
        $('#classin_folder_name').val($('#name').val());
    }
    if (option === '2') {
        $('#bt_select_classin_folder').show();
        $('#bt_clear_classin_folder').show();
        $('#classin_folder_name').show();
        $('#classin_folder_name').val('-Please choose a folder-');
    }
}
