var record_id = $('input[name=record]').val();
var duplicate_id = $('input[name=duplicateId]').val();
if (typeof duplicate_id == 'undefined')
    duplicate_id = '';
var ug_koc = ug_level = ug_module = '';
var ajax_running = 0;

var can_edit_koc    = $('input[name=can_edit_koc]').val();
var can_edit_hour   = $('input[name=can_edit_hour]').val();
var can_edit_teaching  = $('input[name=can_edit_teaching]').val();
var can_edit_revenue   = $('input[name=can_edit_revenue]').val();
var can_edit_lms       = $('input[name=can_edit_lms]').val();

$(document).ready(function () {
    //Fix bug Upgrade Class - Do not input this field
    $('#j_class_j_class_1_name').prop('readonly', true)
    // Set TimePicker
    setTimePickerAll();
    // case Duplicate
    if (duplicate_id != "") {
        ug_koc = $('#kind_of_course').val();
        ug_level = $('#level').val();
        generateSchedule();
    }

    $("#ct_date").show();
    $("#ct_date").multiPicker({
        selector: "li",
        cssOptions  : {
            size      : "large",
            element   : {
                "font-size"   : "13px",
                "font-weight" : "bold"
            },
            selected: {
                "font-size"    : "15px"
            },
        },
        onInit: function () {
            toggleTimeslots();
        },
        onSelect: function () {
            toggleTimeslots();
            ajaxMakeJsonSession();
        },
        onUnselect : function (el, val) {
            toggleTimeslots();
            ajaxMakeJsonSession();
        }
    });
    $('#start_date').live('change', function () {
        if (!checkDataLockDate($(this).attr('id'), false))
            return;
        else
            ajaxMakeJsonSession();
    });

    $('#start_lesson, .schedule_by').live('change', function () {
        ajaxMakeJsonSession();
    });

    //Handle Button Clear Schedule
    $('#btn_clr_time').click(function () {
        clrSelection();
    });
    //Handle Create / Edit
    $('#kind_of_course').select2();
    $('#change_by').select2();
    $('#level').select2();
    $('#lms_template_id').select2();
    $('#start_lesson').select2();
    $('#status').select2({minimumResultsForSearch: -1});
    $('#syllabus_by').select2({minimumResultsForSearch: -1, width: '120px'});

    generateOption();
    if (record_id == '') {
        addToValidate('EditView', 'validate_weekday', 'varchar', true, 'Weekdays');
        getClassHours();
        $("#change_date_from").closest("tr").hide();
        $("#btn_edit_schedule").closest("tr").hide();
    } else {
        //Validate Data Lock
        $('#change_date_from').live('change', function () {
            if (!checkDataLockDate('change_date_from', false))
                return;
        });
        $('#validate_weekday, #timeframe_panel').closest('tr').hide();
        $('#btn_edit_schedule, #btn_edit_startdate').show();
        $('#start_date_trigger').hide();
        $('#hours, #start_date').prop('readonly', true).addClass('input_readonly');


        if (can_edit_koc != '1')
            $('#kind_of_course, #level').prop('disabled', true).addClass('input_readonly');

        getClassHours();
    }
    $("#change_reason_label").closest("tr").hide();


    $('#kind_of_course, #level').live('change',function(){
        $('#koc_id').val($('#kind_of_course option:selected').attr('koc_id'));
        generateOption();
        $('#lessonplan_id').val($('#level option:selected').data('lessonplan_id'))
        if(record_id == '')
            ajaxMakeJsonSession();
    });
    toggleChangeBy();
    $('#change_by').live('change',function(){
        toggleChangeBy();
    });

    $('#is_skip_holiday').live('change',function(){
        var _alert = '';
        if($(this).prop("checked"))
            _alert += DOTB.language.get('J_Class','LBL_SKIP_HOLIDAYS_CHEKED_ALERT')+":<br><br>";
        else _alert += DOTB.language.get('J_Class','LBL_SKIP_HOLIDAYS_UNCHEKED_ALERT')+":<br><br>";
        _alert += DOTB.language.get('J_Class','LBL_SKIP_HOLIDAYS_DES')+":<br>";
        $.alert(_alert);
        ajaxMakeJsonSession();
    });

    $('#hours').on('blur', function () {
        ajaxMakeJsonSession();
    });

    $('#syllabus_by').live('change', function () {
        var class_case = $('#class_case').val();
        if(class_case == 'create' || class_case == 'change_startdate')
            ajaxMakeJsonSession();
    });

    $('#change_date_from').live('change', validateChangeFrom);
    $('#change_date_to').live('change', validateChangeTo);


    $('#btn_clr_j_class_j_class_1_name').on('click', function () {
        $('#upgrade_class_info').remove();
        $('#show_dialog').css('display','none');
        $('#ls_student').val('');
    });

    //Remove SQS
    sqs_objects['EditView_j_class_j_class_1_name'] = {};

    //    //Validate Data Lock
    //    if (!checkDataLockDate('start_date', false, false))
    //        $('#btn_edit_startdate').hide();


    $('.addTimeSlot').live('click', function () {
        addTimeSlot($(this));
        ajaxMakeJsonSession();
    });
    $('.removeTimeSlot').live('click', function () {
        var sc_index = $(this).closest('tr').index();
        $(this).closest('table').closest('tr').find('td.hour_item1').find('tbody tr').eq(sc_index).remove();
        $(this).closest('table').closest('tr').find('td.hour_item2').find('tbody tr').eq(sc_index).remove();
        $(this).closest('tr').remove();
        ajaxMakeJsonSession();
    });

    $('#btn_edit_schedule, #btn_edit_startdate').live('click', function () {
        var btn_name = $(this).attr('name');
        var lbl_content = DOTB.language.get('J_Class','LBL_ALERT_CHANGE_SCHEDULE')+'<b> '+DOTB.language.get('J_Class','LBL_CHANGE_START_DATE_C')+'</b> )'+DOTB.language.get('J_Class','LBL_ALERT_CHANGESS');
        if(btn_name == 'btn_edit_startdate')
            lbl_content = DOTB.language.get('J_Class','LBL_ALERT_CHANGE_STARTDATE')+DOTB.language.get('J_Class','LBL_ALERT_CHANGESS');
        $.confirm({
            title: DOTB.language.get('J_Class','LBL_CONFIRM_C'),
            content: lbl_content,
            buttons: {
                "OK": {
                    btnClass: 'btn-blue',
                    action: function(){
                        handle_change_schedule(btn_name);
                    }
                },
                "Cancel": {
                    text: DOTB.language.get('J_Class','LBL_BTN_CANCEL_C'),
                    action: function(){

                    }
                },
            }
        });
    });
});

function addTimeSlot(_this) {
    var sc_html = $('<tr style="height: 34px;"><td><p class="timeOnly"><input class="time start" type="text" style="width: 70px; text-align: center;"> '+DOTB.language.get('J_Class','LBL_TO')+' <input class="time end" type="text" style="width: 70px; text-align: center;"></p></td><td><span class="id-ff multiple ownline"><button type="button" style="margin-bottom: 0px;" class="button removeTimeSlot" value="Remove" title="Remove Time Slot"><img src="themes/default/images/id-ff-remove-nobg.png" alt="Remove Time Slot"></button></span></td></tr>');
    var dr_html = $('<tr style="height: 34px;"><td align="center"><span class="duration" style="color:#a52a2a;font-weight: 600;"></span><input type="hidden" class="duration"></td></tr>');
    var sb_html = $('<tr style="height: 34px;">'+_this.closest('table').closest('tr').find('td.hour_item2').find('tbody>tr:first').html()+'</tr>');

    _this.closest('tbody').append(sc_html);
    _this.closest('table').closest('tr').find('td.hour_item1').find('tbody').append(dr_html);
    _this.closest('table').closest('tr').find('td.hour_item2').find('tbody').append(sb_html);

    //Set DatePair
    var timeOnly        = sc_html.find('.timeOnly');
    var koc_duration    = '';
    var objs = $.parseJSON($('#kind_of_course :selected').attr('json'));
    if (objs != '' && objs != null && typeof objs != 'undefined')
        $.each(objs, function (key, koc) {
            if (koc.levels == $('#level').val()){
                koc_duration = koc.time_lot;
                return false;
            }
        });
    var weekDate = _this.closest('table').closest('tr').attr('id');
    setTimePicker(timeOnly, koc_duration, weekDate);
}

// Show/Hide Timeslot, Mapping weekday and Timeslot and Clear Input
function toggleTimeslots() {
    var count_selected = 0;
    $('#ct_date > li').each(function () {
        var week_row = "#TS_" + $(this).attr('valu');
        if ($(this).hasClass('active')) {
            $(week_row).show();
            count_selected++
        } else {
            $(week_row).hide();
            $(week_row).find(':input').val('');
            $(week_row).find('span.duration').text('');
            $(week_row).find('select.schedule_by').val('');
        }
    });
    //Catch validate Weekday
    if (count_selected == 0)
        $('#validate_weekday').val('');
    else
        $('#validate_weekday').val('1');
}

function toggleChangeBy(){
    var change_by = $('#change_by').val();
    if(change_by == 'Others'){
        $('#change_reason').show();
        addToValidate('EditView', 'change_reason', 'text', true, DOTB.language.get('J_Class','LBL_CHANGE_REASON'));
    }else{
        $('#change_reason').hide();
        removeFromValidate('EditView','change_reason');
    }
}

function getClassHours() {
    var class_case   = $('#class_case').val();
    var kind_of_course = $('#kind_of_course').val();
    var level_selected = $('#level').val();
    var objs = $.parseJSON($('#kind_of_course :selected').attr('json'));

    //Set LMS Course Template
    $.each(objs, function (key, koc) {
        if (koc.levels == level_selected) {
            if($('#lms_template_id').val() == '' && class_case != 'edit')
                $('#lms_template_id').val(koc.lms_template_id).trigger('change');

            if(can_edit_lms == '1') $('#lms_template_id').prop('disabled', false);
            else $('#lms_template_id').prop('disabled', true);
            return false; // breaks
        }
    });

    if ((class_case == 'edit')) {
        return false;
    } else {
        if ((class_case == 'create') && (kind_of_course == "") && (level_selected == "") ) {
            $('#hours').prop('readonly', true).addClass('input_readonly').val("");
            return false;
        }

        $.each(objs, function (key, koc) {
            if (koc.levels == level_selected) {
                //TH edit theo logic moi
                if(class_case == 'create'){
                    $('#hours').val(Numeric.toFloat(koc.hours,4,4));
                    if ( koc.is_set_hour == '1' || typeof koc.is_set_hour == 'undefined' )
                        $('#hours').prop('readonly', true).addClass('input_readonly');
                    else
                        $('#hours').prop('readonly', false).removeClass('input_readonly');
                }else{
                    if(can_edit_hour == '1'){
                        if ( koc.is_set_hour == '1' || typeof koc.is_set_hour == 'undefined' )
                            $('#hours').prop('readonly', true).addClass('input_readonly');
                        else
                            $('#hours').prop('readonly', false).removeClass('input_readonly');
                    }
                }
                return false; // breaks
            }
        });
    }
}

//showing Kind Of Course
function generateOption() {
    var kind_of_course  = $('#kind_of_course').val();
    var level_selected  = $('#level').val();
    var objs = $.parseJSON($('#kind_of_course :selected').attr('json'));
    var max_size = $('#kind_of_course :selected').attr('max_size');
    //Generate options level
    if (kind_of_course != '' && kind_of_course != null) {
        $('#level').prop('disabled', false).removeClass('input_readonly');
        if (record_id == "" || $('#class_case').val() == 'create'){
            if(max_size == ''){
                $('#max_size').val('');
                $('#max_size').removeClass('input_readonly');
            }
            else{
                $('#max_size').val(max_size);
                $('#max_size').addClass('input_readonly');
            }
        }
        // Clear all select list items except first one
        $('#level').find('option:gt(0)').remove();
        $.each(objs, function (key, obj) {
            if (obj.levels != '')
                $('#level').append('<option label="' + obj.levels + '" value="' + obj.levels + '" data-lessonplan_id="'+ obj.lessonplan_id+'">' + obj.levels + '</option>');
            else $('#level :selected').attr('data-lessonplan_id', obj.lessonplan_id);
        });
        if ($('#level option').size() == 1) {
            $('#level').prop('disabled', true).addClass('input_readonly');
        } else {
            $('#level').prop('disabled', false).removeClass('input_readonly');
        }
        $('#level').val(level_selected);
    }
    getClassHours();
    reloadHourRate();
}

//Ajax Cal Ending Date
function ajaxMakeJsonSession() {
    var schedule   = {};
    var start_date = $('#start_date').val();
    var flag_ajax  = 0;
    var class_case = $('#class_case').val();
    var koc_id     = $('#koc_id').val();
    var level      = $('#level').val();
    var start_lesson= $('#start_lesson').val();
    var change_date_from = $('#change_date_from').val();
    var change_date_to   = $('#change_date_to').val();
    var syllabus_by      = $('#syllabus_by').val();
    var hours            = Numeric.parse($('#hours').val());
    $('#ct_date > li.active').each(function () {
        var week_date = $(this).attr('valu');
        schedule[week_date] = new Array();
        var durations    = $("#TS_" + week_date).find("input.duration");
        var schedules_by = $("#TS_" + week_date).find("select.schedule_by");
        var start_times  = $("#TS_" + week_date).find(".start");
        var end_times    = $("#TS_" + week_date).find(".end");

        if (durations.length > 0) {
            for (ind = 0; ind < durations.length; ind++) {
                var duration    = durations[ind].value;//minutes
                var start_time  = start_times[ind].value;
                var end_time    = end_times[ind].value;
                var schedule_by = schedules_by[ind].value;
                if (start_time == '' || end_time == '' || duration <= 0 || duration == '') flag_ajax += 1;
                else {
                    schedule[week_date][ind] = {};
                    schedule[week_date][ind]['start_time']  = start_time;
                    schedule[week_date][ind]['end_time']    = end_time;
                    schedule[week_date][ind]['duration']    = duration;
                    schedule[week_date][ind]['schedule_by'] = schedule_by;
                }
            }
        } else flag_ajax += 1;
    });

    if (record_id == "" || $('#class_case').val() == 'change_startdate') {
        $('#main_schedule').val(JSON.stringify(schedule));
    }
    if ($('#class_case').val() == 'change_schedule' && $('#change_date_from').val() == '') {
        flag_ajax += 1;
    }

    //check start date
    $('#notification_weekdate').remove();
    if ($('#ct_date > li.active').length != 0)
        checkStartDate();

    //checking run validate ajax
    if (flag_ajax > 0 || $('#ct_date > li.active').length == 0 || isNaN(hours) || hours <= 0)
        return false;
    else
        $('#end_date').val(''); // Run Ajax

    //get hour test of Kind of Course
    var level_selected = $('#level').val();
    var is_skip_holiday = (($('#is_skip_holiday').prop("checked")) ? 1 : 0);
    var objs = $.parseJSON($('#kind_of_course :selected').attr('json'));
    if (start_date != '' && schedule != '') {
        ajaxStatus.showStatus('Processing...');
        $.ajax({
            url: "index.php?module=J_Class&action=handleAjaxJclass&dotb_body_only=true",
            type: "POST",
            async: true,
            data:
            {
                type            : 'ajaxMakeJsonSession',
                class_id        : record_id,
                class_case      : class_case,
                change_date_from: change_date_from,
                change_date_to  : change_date_to,
                start_date      : start_date,
                schedule        : schedule,
                total_hours     : hours,
                is_skip_holiday : is_skip_holiday,
                koc_id          : koc_id,
                level           : level,
                syllabus_by     : syllabus_by,
                start_lesson    : start_lesson,
            },
            dataType: "json",
            success: function (res) {
                ajaxStatus.hideStatus();
                if (res.success == "1") {
                    var _alert = '';
                    $('input#content').val(res.content);
                    var json = jQuery.parseJSON(res.content);

                    var start_date_format = DOTB.util.DateUtils.formatDate(new Date(json.start_date));
                    var end_date_format = DOTB.util.DateUtils.formatDate(new Date(json.end_date));

                    _alert += '<table class="edit view tabForm" style="width: 100%">'
                    +'<tbody>'

                    +'<tr><td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_KIND_OF_COURSE') +'</td><td width="30%">'+$('#kind_of_course').val()+'</td>'
                    +'<td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_LEVEL') +'</td><td width="30%">'+$('#level').val()+'</td></tr>'

                    +'<tr><td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_START_DATE') +'</td><td width="30%">'+start_date_format+'</td>'
                    +'<td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_END_DATE') +'</td><td width="30%">'+end_date_format+'</td></tr>'

                    +'<tr><td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_COUNT_SS') +'</td><td width="30%">'+Numeric.toFloat(json.count_sessions)+'</td>'
                    +'<td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_NUM_OF_STUDENT') +'</td><td width="30%">'+Numeric.toFloat(json.count_student)+'</td></tr>'

                    +'<tr><td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_HOURS') +'</td><td width="30%">'+Numeric.toFloat(json.defaut_hours,2,2)+'</td>'
                    +'<td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_TOTAL_SS_HOURS') +'</td><td width="30%">'+Numeric.toFloat(json.total_ss_hours,2,2)+'</td></tr>';

                    $('#end_date').val(end_date_format);
                    $('#end_date').effect("highlight", {color: '#ff9933'}, 2000);
                    if ((typeof json.holidays != 'undefined' && json.holidays != '')) {
                        var holiday_alert = "";
                        $.each(json.holidays, function (index, value) {
                            holiday_alert += "<b>- " + index + ": </b> " + value + "<br>";
                        });
                        holiday_alert += DOTB.language.get('J_Class','LBL_SKIP_HOLIDAYS_CHEKED_ALERT');

                        if (json.new_start_Date != "" && typeof json.new_start_Date != 'undefined' && json.new_start_Date != null) {
                            holiday_alert += "<br>"+DOTB.language.get('J_Class','LBL_SKIP_HOLIDAYS_START_CHANGE')+": " + json.new_start_Date;
                            $("#start_date").val(json.new_start_Date).effect("highlight", {color: '#ff9933'}, 2000);
                        }

                        _alert += '<tr><td scope="row" valign="top" width="20%">'+DOTB.language.get('J_Class','LBL_SKIP_HOLIDAYS_LIST') +'</td><td colspan="3">'+holiday_alert+'</td></tr>';
                    }
                    _alert += '</tbody></table>';
                    //Build dropdown options - Lesson Start
                    if(class_case == 'create' || class_case == 'change_startdate') $('#start_lesson').html(json.startLesson);
                    else $('#start_lesson').prop("disabled", true);
                    $.alert({
                        title: '<i class="fa fa-rocket"></i> '+DOTB.language.get('J_Class','LBL_CLASS_INFO'),
                        content: _alert,
                        animation: 'scale',
                        closeAnimation: 'scale',
                        buttons: {
                            okay: {
                                text: 'OK',
                                btnClass: 'btn-blue'
                            }
                        }
                    });


                } else {
                    $('#end_date').val('');
                    $.alert({
                        title: DOTB.language.get('J_Class','LBL_ERR')+'!!',
                        content: DOTB.language.get('J_Class','LBL_ERROR_NO_LESSON'),
                    });
                }

                if(typeof json != 'undefined' && typeof json.warning != 'undefined' && json.warning != '')
                    app.alert.show('message-id', {
                        level: 'warning',
                        title: json.warning,
                        autoClose: false
                    });
            },
            beforeSend: function() {
                ajax_running = 1;
            },
            complete: function() {
                ajax_running = 0;
            }
        });
    } else {
        $('#end_date').val('');
        return;
    }
}

//Validate Change To
function validateChangeTo() {
    $from = DOTB.util.DateUtils.parse($('#change_date_from').val(), cal_date_format).getTime();
    $to = DOTB.util.DateUtils.parse($('#root_end_date').val(), cal_date_format).getTime();
    //get date change
    $date_change = DOTB.util.DateUtils.parse($('#change_date_to').val(), cal_date_format);
    if ($date_change == false) {
        toastr.error(DOTB.language.get('J_Class','LBL_ERR_INVALID_RANGE'), DOTB.language.get('J_Class','LBL_ERR'));
        $('#change_date_to').val('');
    } else {
        $change = $date_change.getTime();
        if ($change < $from || $change > $to) {
            toastr.error(DOTB.language.get('J_Class','LBL_ERR_INVALID_RANGE'), DOTB.language.get('J_Class','LBL_ERR'));
            $('#change_date_to').val('');
        } else
            ajaxMakeJsonSession();
    }
    // check lock data
    if(!checkDataLockDate('change_date_to',true,true)) {
        return;
    }

}

//Validate Change From
function validateChangeFrom() {
    $from = DOTB.util.DateUtils.parse($('#root_start_date').val(), cal_date_format).getTime();
    $to = DOTB.util.DateUtils.parse($('#root_end_date').val(), cal_date_format).getTime();
    //get date change
    $date_change = DOTB.util.DateUtils.parse($('#change_date_from').val(), cal_date_format);
    if ($date_change == false) {
        toastr.error(DOTB.language.get('J_Class','LBL_ERR_INVALID_RANGE'), DOTB.language.get('J_Class','LBL_ERR'));
        $('#change_date_from').val('');
    } else {
        $change = $date_change.getTime();
        if ($change < $from || $change > $to) {
            toastr.error(DOTB.language.get('J_Class','LBL_ERR_INVALID_RANGE'), DOTB.language.get('J_Class','LBL_ERR'));
            $('#change_date_from').val('');
        } else
            ajaxMakeJsonSession();
    }
    // check lock data
    if(!checkDataLockDate('change_date_from',true,true)) {
        return;
    }
}

//Overwrite function set_return: Handle upgrade class
function set_class_return(popup_reply_data) {
    $('#upgrade_class_info').remove();
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    if (name_to_value_array.isupgrade != 0) {
        $.alert('<b>' + name_to_value_array.j_class_j_class_1_name + "</b> is upgraded. You can not choose this class upgrade again !!");
        return false;
    }
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key) {
                case 'j_class_j_class_1j_class_ida':
                    if(val == record_id && record_id != ''){
                        $("#j_class_j_class_1j_class_ida").val('');
                        $("#j_class_j_class_1_name").val('').effect("highlight", {color: 'red'}, 2000);
                        toastr.error(DOTB.language.get('J_Class','LBL_CLASS_DUPLICATED'), DOTB.language.get('J_Class','LBL_ERR'));
                        return false;
                    }else{
                        $('#j_class_j_class_1j_class_ida').val(val);
                        var upgrade_id = val;
                    }
                    break;
                case 'j_class_j_class_1_name':
                    $('#j_class_j_class_1_name').val(val);
                    var upgrade_name = val;
                    break;
                case 'c_teachers_j_class_1c_teachers_ida':
                    if (record_id == '')
                        $('#c_teachers_j_class_1c_teachers_ida').val(val);
                    break;
                case 'c_teachers_j_class_1_name':
                    if (record_id == '') {
                        $('#c_teachers_j_class_1_name').val(val);
                        $('#c_teachers_j_class_1_name').effect("highlight", {color: '#ff9933'}, 3000);
                    }
                    break;
                case 'level':
                    if (record_id == '') {
                        $('#level').val(val);
                        $('#level').effect("highlight", {color: '#ff9933'}, 3000);
                        ug_level = val;
                    }
                    break;
                case 'content':
                    if (record_id == ''){
                        $('input#content').val(val);
                    }
                    break;
                case 'end_date':
                    if (record_id == ''){
                        $('#upgrade_from_end_date').val(val);
                    }
                    break;
            }
        }
    }
    //Append btn More Info
    generateSchedule();

}

// check the Start date of course
function checkStartDate() {
    var class_case = $('#class_case').val();
    var start_date = $('#start_date').val();
    $('#notification_weekdate').remove();
    if ($('#ct_date > li.active').length == 0) return true;
    if (class_case == 'change_startdate' || class_case == 'create') {
        var daysOfWeek = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        var weekdate_checking = '';
        if(start_date != '' && start_date !== null && typeof start_date != 'undefined')
            weekdate_checking = daysOfWeek[DOTB.util.DateUtils.parse(start_date, cal_date_format).getDay()];

        var count = 0;
        $('#ct_date > li.active').each(function () {
            var week_date = $(this).attr('valu');
            if (week_date == weekdate_checking)
                count++;
        });
        if (count == 0) {
            $('#ct_date').closest('td').append("<div id='notification_weekdate' class='required validation-message'>" + DOTB.language.get('J_Class', 'LBL_CHECK_DATE_ERROR') + "</div>");
            return false;
        } else return true;
    } else
        return true;
}

//Overwrite check_form to validate
function check_form(formname) {
    //Validate timepicker
    var validate_days = true;
    $('#ct_date > li.active').each(function () {
        var week_date = $(this).attr('valu');
        var week_row = "#TS_" + week_date;
        $(week_row + ' :input:not(:checkbox):not(:button)').each(function () {
            if (week_date == '') {
                validate_days = false;
                $(this).effect("highlight", {color: '#FF0000'}, 2000);
            }
        });
    });
    if (validate_form(formname, '') && validate_days && checkStartDate())
        return true;
    else return false;
}

//Generate suggest selected schedule ===EDIT FEATURE==
function generateScheduleEdit(){
    var content     = $('input#content').val();
    if (record_id != '') {
        clrSelection();
        var json = jQuery.parseJSON(content);
        if (json != null && typeof json !== "undefined") {
            $.each(json.schedule, function (key, obj) {
                $('#ct_date > li[valu='+key+']').addClass('active');
            });
            toggleTimeslots();
            $.each(json.schedule, function (key, obj) {
                for (ind = 0; ind < obj.length; ind++) {
                    if (obj[ind].duration_hour != '' && typeof obj[ind].duration_hour != 'undefined')
                        var minutes = Numeric.toFloat(obj[ind].duration_hour) * 60; //duration_hour -> biến cũ
                    if (obj[ind].duration != '' && typeof obj[ind].duration != 'undefined') //minutes -> Biến mới
                        var minutes = Numeric.toFloat(obj[ind].duration);
                    if (ind > 0) addTimeSlot($('#TS_' + key).find('.addTimeSlot'));
                    var sss = new Date("2015-03-25 " + obj[ind].start_time);
                    var eee = new Date("2015-03-25 " + obj[ind].end_time);
                    $('#TS_' + key).find('.start').eq(ind).timepicker('setTime', sss);
                    $('#TS_' + key).find('.end').eq(ind).timepicker('setTime', eee);
                    $('#TS_' + key).find('input.duration').eq(ind).val(minutes);
                    $('#TS_' + key).find('span.duration').eq(ind).text(duration_to_text(minutes));
                    $('#TS_' + key).find('select.schedule_by').eq(ind).val('');
                    $('#TS_' + key).find('p.timeOnly').eq(ind).datepair(); // initialize datepair
                }
            });
        }
        ajaxMakeJsonSession();
    }
}

//Generate suggest selected schedule ===UPGRADE FEATURE==
function generateSchedule() {
    var upgrade_id  = $('#j_class_j_class_1j_class_ida').val();
    var content     = $('input#content').val();
    var upgrade_from_end_date = $('#upgrade_from_end_date').val();
    if (record_id == '') {
        clrSelection();
        // Generate schedule from class upgrade
        var day_schedule = [];

        var json = jQuery.parseJSON(content);
        if (json != null && typeof json !== "undefined") {
            $.each(json.schedule, function (key, obj) {
                $('#ct_date > li[valu='+key+']').addClass('active');
            });
            toggleTimeslots();
            $.each(json.schedule, function (key, obj) {
                for (ind = 0; ind < obj.length; ind++) {
                    if (obj[ind].duration_hour != '' && typeof obj[ind].duration_hour != 'undefined')
                        var minutes = Numeric.toFloat(obj[ind].duration_hour) * 60; //duration_hour -> biến cũ
                    if (obj[ind].duration != '' && typeof obj[ind].duration != 'undefined') //minutes -> Biến mới
                        var minutes = Numeric.toFloat(obj[ind].duration);
                    if (ind > 0) addTimeSlot($('#TS_' + key).find('.addTimeSlot'));
                    var sss = new Date("2015-03-25 " + obj[ind].start_time);
                    var eee = new Date("2015-03-25 " + obj[ind].end_time);
                    $('#TS_' + key).find('.start').eq(ind).timepicker('setTime', sss);
                    $('#TS_' + key).find('.end').eq(ind).timepicker('setTime', eee);
                    $('#TS_' + key).find('input.duration').eq(ind).val(minutes);
                    $('#TS_' + key).find('span.duration').eq(ind).text(duration_to_text(minutes));
                    $('#TS_' + key).find('select.schedule_by').eq(ind).val('');
                    $('#TS_' + key).find('p.timeOnly').eq(ind).datepair(); // initialize datepair
                }
                day_schedule.push(key);
            });
            // set TimePicker
            if(upgrade_from_end_date != '' && upgrade_from_end_date != null && typeof upgrade_from_end_date != 'undefined'){
                var end_date = DOTB.util.DateUtils.parse(upgrade_from_end_date,DOTB.expressions.userPrefs.datef);
                var count_r = 0;
                var end_date_next = YAHOO.widget.DateMath.add(end_date,'D',1);
                var daysOfWeek = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
                var end_date_checking = daysOfWeek[end_date_next.getDay()];
                while (day_schedule.indexOf(end_date_checking) == -1 && count_r <= 60) {
                    end_date_next = YAHOO.widget.DateMath.add(end_date_next,'D',1);
                    end_date_checking = daysOfWeek[end_date_next.getDay()];
                    count_r++;//Tránh loop vô tận
                }
                $('#start_date').val(DOTB.util.DateUtils.formatDate(end_date_next));
            }
            checkStartDate();
        }

        // suggest kind of course, level, module
        generateOption();
        if ($('#level option:selected').index() != $('#level option').length - 1) {
            if($('#kind_of_course').val() != '') {
                $('#level').val($('#level option:selected').next().val());
            } else $('#level').val('');
        } else if ($('#kind_of_course option:selected').index() != $('#kind_of_course option').length - 1) {
            $('#kind_of_course').val($('#kind_of_course option:selected').next().val());
            var objs = $.parseJSON($('#kind_of_course :selected').attr('json'));
            $.each(objs, function (key, koc) {
                $('#level').val('');
                $('#level').val($('#level option:selected').next().val());
            });
        }else $('#level, #kind_of_course').val('');
        $('#lessonplan_id').val($('#level option:selected').data('lessonplan_id'));
        // link more info
        $('#btn_j_class_j_class_1_name').closest('.multiple').after("<a  id='upgrade_class_info' style='text-decoration: none; font-style: italic;' href='#bwc/index.php?module=J_Class&offset=1&stamp=1439367238038569100&return_module=J_Class&action=DetailView&record=" + upgrade_id + "'>&nbsp;&nbsp;"+DOTB.language.get('J_Class','LBL_MORE_INFO_C')+" >></a>");

        $('#koc_id').val($('#kind_of_course option:selected').attr('koc_id'));
        generateOption();
        ajaxMakeJsonSession();
    }
}

// Set Timepicker
function setTimePickerAll(koc_duration) {
    //Clear input
    $('#timeframe_panel').find('input').unbind().val('');

    $('.timeslot').each(function () {
        var weekDate = $(this).attr('id');
        $(this).find('.timeOnly').each(function () {
            setTimePicker($(this), koc_duration, weekDate);
        });
    });
}

function setTimePicker(timeOnly, koc_duration, weekDate) {
    var TimeDelta = defaultTimeDelta * 60000;  // milliseconds
    if (koc_duration != '' && typeof koc_duration != 'undefined')
        TimeDelta = parseInt(koc_duration) * 60000;   // milliseconds

    //set max/min by Timeslot
    if ((typeof weekDate != 'undefined' && typeof timeslot != 'undefined' && timeslot[weekDate] != ''))
        var slot = timeslot[weekDate].split("|");

    var minTime = '6:00am';
    var maxTime = '9:30pm';
    if (slot != "" && typeof slot != 'undefined'){
        minTime = slot[0];
        maxTime = slot[1];
    }

    //Clear event
    timeOnly.unbind();

    timeOnly.find(".time").eq(0).timepicker({
        'minTime': minTime,
        'maxTime': maxTime,
        'showDuration': false,
        'timeFormat': DOTB.expressions.userPrefs.timef,
        'step': step,
        'disableTextInput': disableTextInput,
        'disableTouchKeyboard': disableTextInput,
    });
    timeOnly.find(".time").eq(1).timepicker({
        'showDuration': true,
        'timeFormat': DOTB.expressions.userPrefs.timef,
        'step': step,
        'disableTextInput': disableTextInput,
        'disableTouchKeyboard': disableTextInput,
        'showOn': showOn,
    });
    var settingDatePair = new Datepair(timeOnly[0], {
        'defaultTimeDelta': TimeDelta,
    });

    timeOnly.on('rangeSelected', function () {
        var sc_index    = $(this).closest('tr').index();
        var js_start    = DOTB.util.DateUtils.parse($(this).find(".start").val(), DOTB.expressions.userPrefs.timef);
        var js_end      = DOTB.util.DateUtils.parse($(this).find(".end").val(), DOTB.expressions.userPrefs.timef);

        var minutes = parseFloat((js_end - js_start) / 60000); //minutes

        if(minutes <= 0 || isNaN(minutes)){
            toastr.error(DOTB.language.get('J_Class','LBL_ERR_INVALID_RANGE'), DOTB.language.get('J_Class','LBL_ERR'));
            return false;
        }

        var td_duration = $(this).closest('table').closest('tr').find('td.hour_item1').find('tbody > tr > td').eq(sc_index);
        td_duration.find("input.duration").val(Numeric.toFloat(minutes));
        td_duration.find("span.duration").text(duration_to_text(minutes));
        if(ajax_running == 0) ajaxMakeJsonSession(); //Prevent bulk ajax
    }).on('rangeError', function(){
        toastr.error(DOTB.language.get('J_Class','LBL_ERR_INVALID_RANGE'), DOTB.language.get('J_Class','LBL_ERR'));
        return ;
    });
}

//Clear all selection && Input
function clrSelection() {
    $('.start, .end, input.duration, #end_date').val('');
    $('span.duration').text('---');
    $('select.schedule_by').val('');
    $('#ct_date > li').removeClass();
    toggleTimeslots();
    ajaxMakeJsonSession();
}

function reloadHourRate() {
    var kind_of_course = $('#kind_of_course').val();
    var level_selected = $('#level').val();
    var objs = $.parseJSON($('#kind_of_course :selected').attr('json'));
    //Teacher Rate & Revenue Rate
    var teaching_rate = 1;
    var revenue_rate = 1;
    if (objs != '' && objs != null && typeof objs != 'undefined')
        $.each(objs, function (key, koc) {
            if (koc.levels == level_selected) {
                if (koc.teaching_rate == '' || typeof koc.teaching_rate == 'undefined') teaching_rate = 1;
                else teaching_rate = koc.teaching_rate;

                if (koc.revenue_rate == '' || typeof koc.revenue_rate == 'undefined') revenue_rate = 1;
                else revenue_rate = koc.revenue_rate;

                //reset TimeDelta
                setTimePickerAll(koc.time_lot);
            }
        });
    $('#teaching_rate').val(teaching_rate);
    $('#revenue_rate').val(revenue_rate);
    //End Teaching Rate & Revenue Rate
}

function handle_change_schedule(btn){
    $('#btn_edit_startdate').hide();
    $('#btn_edit_schedule').hide();
    $('#btn_reload').show();
    $('#change_reason_label').closest('tr').show();
    $('#validate_weekday, #timeframe_panel').closest('tr').show();
    addToValidate('EditView', 'validate_weekday', 'varchar', true, DOTB.language.get('J_Class','LBL_WEEKDAY'));
    addToValidate('EditView', 'change_by', 'enum', true, DOTB.language.get('J_Class','LBL_CHANGE_BY'));

    if(btn == 'btn_edit_schedule') var action = 'change_schedule';
    else var action = 'change_startdate';
    $('#class_case').val(action);

    if(action == 'change_startdate'){ // handle change start date
        $('#start_date').prop('readonly', false).removeClass('input_readonly').effect("highlight", {color: '#ff9933'}, 2000).closest('tr');
        $('#start_date_trigger').show();
    }

    if(action == 'change_schedule'){   //Handle edit schedule
        $('#change_date_from_label, #change_date_to_label, #change_date_from_span, #change_date_to_span').show();
        $('#change_date_to').val($('#end_date').val()).effect("highlight", {color: '#ff9933'}, 2000);
        $('#change_date_from').effect("highlight", {color: '#ff9933'}, 2000);

        $('#start_lesson').prop("disabled", true);

        addToValidate('EditView', 'change_date_from', 'date', true, DOTB.language.get('J_Class','LBL_CHANGE_DATE_FROM'));
        addToValidate('EditView', 'change_date_to', 'date', true, DOTB.language.get('J_Class','LBL_CHANGE_DATE_TO'));
    }

    getClassHours();
    generateScheduleEdit();
}

function duration_to_text(duration){
    var duration_text = '';
    var dh = Math.floor(duration/60);
    var dm = Math.floor(duration%60);
    if(dh == 1) duration_text += dh+' '+DOTB.language.get('J_Class','LBL_HR')+' ';
    if(dh > 1) duration_text += dh+' '+DOTB.language.get('J_Class','LBL_HRS')+' ';
    if(dm == 1) duration_text += dm+' '+DOTB.language.get('J_Class','LBL_MIN');
    if(dm > 1) duration_text += dm+' '+DOTB.language.get('J_Class','LBL_MINS');
    return duration_text;
}
