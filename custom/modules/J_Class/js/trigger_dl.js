
//TRIGGER DO WHEN
DOTB.util.doWhen(
    function() {
        return $('a[data-subpanel=whole_subpanel_j_class_studentsituations]').length == 1;
    },
    function() {
        $('a[data-subpanel=whole_subpanel_j_class_studentsituations]').trigger('click');
});

//Handle Gradebook Config Reload
$(document).ready(function () {
    // reload
    $('.gradebook_checkbox,.checkall_custom_checkbox').live('click', function (e) {
        var is_check = 0;
        $('.gradebook_checkbox').each(function() {
            if($(this).is(':checked')) is_check = 1;
        });
        if(is_check){
            $('#reloadconfig_gradebook_btn').css('cursor','pointer');
            $('#reloadconfig_gradebook_btn').removeAttr('disabled');
            $('#reloadconfig_gradebook_btn').removeClass('btn-secondary');
            $('#reloadconfig_gradebook_btn').addClass('primary');
        }else {
            $('#reloadconfig_gradebook_btn').css('cursor', 'context-menu');
            $('#reloadconfig_gradebook_btn').attr('disabled', 'true');
            $('#reloadconfig_gradebook_btn').removeClass('primary');
            $('#reloadconfig_gradebook_btn').addClass('btn-secondary');
        }
    });
    $('#reloadconfig_gradebook_btn').live('click', function (e) {
        if (confirm(DOTB.language.get('J_Class','LBL_ALERT_GB1'))){
            $('.gradebook_checkbox').each(function() {
                if($(this).is(':checked')){
                    var gradebook_id = $(this).val();
                    reloadGBconfig(gradebook_id,1);
                }
            });
        }
    });
});

function reloadGBconfig(gb_id, reloadconfig) {
    if (typeof reloadconfig == 'undefined' || reloadconfig == '') reloadconfig = 0;
    var _this = $(this);
    DOTB.ajaxUI.showLoadingPanel();
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data: {
            process_type: "getGradebookDetail",
            gradebook_id: gb_id,
            reloadconfig: reloadconfig,
        },
        dataType: "json",
        success: function (data) {
            DOTB.ajaxUI.hideLoadingPanel();
        },
        error: function () {
            DOTB.ajaxUI.hideLoadingPanel();
            toastr.error("There are errors. Please try again!");
        },
    });
}