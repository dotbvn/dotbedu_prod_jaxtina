<?php
// created: 2019-04-04 09:51:20
$subpanel_layout['list_fields'] = array (
  'class_code' =>
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CLASS_CODE',
    'width' => 10,
    'default' => true,
  ),
  'name' =>
  array (
    'type' => 'name',
    'vname' => 'LBL_NAME',
    'width' => 10,
    'default' => true,
  ),
  'status' =>
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'start_date' =>
  array (
    'type' => 'date',
    'vname' => 'LBL_START_DATE',
    'width' => 10,
    'default' => true,
  ),
  'end_date' =>
  array (
    'type' => 'date',
    'vname' => 'LBL_END_DATE',
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' =>
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'team_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'studio' =>
    array (
      'portallistview' => false,
      'portalrecordview' => false,
    ),
    'vname' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Teams',
    'target_record_key' => 'team_id',
  ),
);