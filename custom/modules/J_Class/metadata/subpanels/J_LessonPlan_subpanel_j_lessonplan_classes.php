<?php
// created: 2023-08-31 17:09:32
$subpanel_layout['list_fields'] = array (
  'class_code' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CLASS_CODE',
    'width' => 10,
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'j_class_j_class_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_J_CLASS_J_CLASS_1_FROM_J_CLASS_L_TITLE',
    'id' => 'J_CLASS_J_CLASS_1J_CLASS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'J_Class',
    'target_record_key' => 'j_class_j_class_1j_class_ida',
  ),
  'koc_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_KOC_NAME',
    'id' => 'KOC_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'J_Kindofcourse',
    'target_record_key' => 'koc_id',
  ),
  'level' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_LEVEL',
    'width' => 10,
  ),
  'start_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_START_DATE',
    'width' => 10,
    'default' => true,
  ),
  'end_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_END_DATE',
    'width' => 10,
    'default' => true,
  ),
);