<?php
// created: 2019-07-11 21:27:23
$subpanel_layout['list_fields'] = array (
  'class_code' =>
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CLASS_CODE',
    'width' => 10,
    'default' => true,
  ),
  'name' =>
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'teacher_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_MAIN_TEACHER_NAME',
    'id' => 'TEACHER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'C_Teachers',
    'target_record_key' => 'teacher_id',
  ),
  'period' =>
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PERIOD',
    'width' => 10,
  ),
  'kind_of_course' =>
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_KIND_OF_COURSE',
    'width' => 10,
  ),
  'level' =>
  array (
    'type' => 'multienum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_LEVEL',
    'width' => 10,
  ),
  'team_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'studio' =>
    array (
      'portallistview' => false,
      'portaldetailview' => false,
      'portaleditview' => false,
    ),
    'vname' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Teams',
    'target_record_key' => 'team_id',
  ),
);