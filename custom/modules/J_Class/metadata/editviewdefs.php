<?php
$module_name = 'J_Class';
$viewdefs[$module_name] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'hidden' =>
                array (
                    1 => '<input type="hidden" name="content" id="content" value="{$fields.content.value}">',
                    2 => '<input type="hidden" name="class_case" id="class_case" value="{$class_case}">',
                    3 => '<input type="hidden" name="root_start_date" id="root_start_date" value="{$fields.start_date.value}">',
                    4 => '<input type="hidden" name="root_end_date" id="root_end_date" value="{$fields.end_date.value}">',
                    6 => '<input type="hidden" name="main_schedule" id="main_schedule" value="{$fields.main_schedule.value}">',
                    8 => '<input type="hidden" name="fetched_row_j_class_j_class_1j_class_ida" id="fetched_row_j_class_j_class_1j_class_ida" value="{$fields.j_class_j_class_1j_class_ida.value}">',
                    9 => '<input type="hidden" name="upgrade_to_id" id="upgrade_to_id" value="{$upgrade_to_id}">',
                    10 => '<input type="hidden" name="upgrade_to_name" id="upgrade_to_name" value="{$upgrade_to_name}">',
                    11 => '<input type="hidden" name="upgrade_from_end_date" id="upgrade_from_end_date" value="{$upgrade_from_end_date}">',
                    12 => '<input type="hidden" id="koc_id" name="koc_id" value="{$fields.koc_id.value}">',
                    13 => '<input type="hidden" id="teaching_rate" name="teaching_rate" value="{$fields.teaching_rate.value}">',
                    14 => '<input type="hidden" id="revenue_rate" name="revenue_rate" value="{$fields.revenue_rate.value}">',
                    15 => '<input type="hidden" id="can_edit_koc" name="can_edit_koc" value="{$can_edit_koc}">',
                    16 => '<input type="hidden" id="can_edit_hour" name="can_edit_hour" value="{$can_edit_hour}">',
                    17 => '<input type="hidden" id="can_edit_teaching" name="can_edit_teaching" value="{$can_edit_teaching}">',
                    18 => '<input type="hidden" id="can_edit_revenue" name="can_edit_revenue" value="{$can_edit_revenue}">',
                    19 => '<input type="hidden" id="can_edit_lms" name="can_edit_lms" value="{$can_edit_lms}">',
                    20 => '<input type="hidden" id="lms_template_content" name="lms_template_content" value=\'{$lms.content}\'>',
                    21 => '<input type="hidden" id="onl_setting" name="onl_setting" value=\'{$fields.onl_setting.value}\'>',
                    22 => '<input type="hidden" id="onl_status" name="onl_status"  value=\'{$fields.onl_status.value}\'>',
                    23 => '<input type="hidden" id="checksum_code" name="checksum_code"  value=\'{$checksum_code}\'>',
					24 => '<input type="hidden" id="lessonplan_id" name="lessonplan_id"  value=\'{$lessonplan_id}\'>',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'javascript' => '
            <script>
            var defaultTimeDelta = 90;
            var step             = 15;
            var disableTextInput = false;
            var showOn           = ["click","focus"];
            </script>
            {dotb_getscript file="custom/modules/J_Class/js/editview_new.js"}
            {dotb_getscript file="custom/include/javascript/Timepicker/js/jquery.timepicker.min.js"}
            {dotb_getscript file="custom/include/javascript/Timepicker/js/datepair.min.js"}
            {dotb_getscript file="custom/include/javascript/multiPicker/multipicker.min.js"}
            <link rel=\'stylesheet\' href=\'{dotb_getjspath file="custom/include/javascript/Timepicker/css/jquery.timepicker.css"}\'/>
            <link rel=\'stylesheet\' href=\'{dotb_getjspath file="custom/modules/J_Class/css/style_nd.css"}\'/>
            <link rel=\'stylesheet\' href=\'{dotb_getjspath file="custom/include/javascript/multiPicker/multipicker.min.css"}\'/>
            {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
            <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>
            <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/simptip.min.css}"/>',

            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'lbl_editview_panel_onl' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'lbl_editview_panel2' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array (
            'LBL_EDITVIEW_PANEL1' =>
            array (
                0 =>
                array(
                    0 =>
                    array(
                        'name' => 'picture',
                        'customCode' => '{include file="custom/modules/J_Class/tpls/change_avatar.tpl"}',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'name',
                        'customLabel' => '{$MOD.LBL_CLASS_CODE}:',
                        'customCode' => '{if $edit_class_code}
                        <input type="text" name="class_code_s" id="class_code" maxlength="255" value="{$fields.class_code.value}" title="{$MOD.LBL_CLASS_CODE}" placeholder="{$MOD.LBL_AUTO_GENERATE}" size="30">
                        {else}
                        {if $fields.class_code.value == ""}<span style="color: #797979;">{$MOD.LBL_AUTO_GENERATE_IF_BLANK}</span>
                        {else}<span name="class_code_s" style="width: 100px">{$fields.class_code.value}</span>{/if}
                        {/if}',
                    ),
                    1 =>
                    array (
                        'name' => 'status',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'name',
                        'studio' => 'visible',
                        'label' => 'LBL_NAME',//Auto-Generate if blank
                        'customCode' => '<input type="text" placeholder="{$MOD.LBL_AUTO_GENERATE_IF_BLANK}" name="name_s" id="name" maxlength="255" value="{$fields.name.value}" title="{$MOD.LBL_NAME}" size="30">',
                        'displayParams' =>
                        array (
                            'required' => false,
                        ),
                    ),
                    1 => 'start_date',

                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'j_class_j_class_1_name',
                        'label' => 'LBL_UPGRADE',
                        'displayParams' =>
                        array (
                            'field_to_name_array' =>
                            array (
                                'id' => 'j_class_j_class_1j_class_ida',
                                'name' => 'j_class_j_class_1_name',
                                'content' => 'content',
                                'kind_of_course' => 'kind_of_course',
                                'level' => 'level',
                                'modules' =>  'modules',
                                'isupgrade' =>  'isupgrade',
                                'start_date' =>  'start_date',
                                'end_date' =>  'end_date',
                            ),
                            'initial_filter' => '&isupgrade_advanced=0',
                            'call_back_function' => 'set_class_return',
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'end_date',
                        'customCode' => '<span class="dateTime"><input class="date_input input_readonly" autocomplete="off" type="text" name="end_date" id="end_date" value="{$fields.end_date.value}" tabindex="0" size="11" maxlength="10" readonly></span> {if !empty($fields.end_date.value) && !$isupgrade}<span style="font-style: italic;">cr:  {$fields.end_date.value}</span>{/if}',
                    ),

                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'kind_of_course',
                        'studio' => 'visible',
                        'label' => 'LBL_KOC_NAME',
                        'customCode' => '
                        <table width="70%" style="padding:0px!important;">
                        <tbody><tr colspan = "3">
                        <td nowrap width = "50%" >{$htm_koc}</td>
                        <td nowrap width = "1%" scope="col"> <label>{$MOD.LBL_LEVEL}: </label><span class="required">*</span></td>
                        <td nowrap width = "50%" >{$dropdown_level}</td>
                        </tr></tbody>
                        </table>',
                    ),
                    1 =>
                    array(
                        'name' => 'lms_template_id',
                        'customLabel' => '{if $lms.enable}<b style="color: #002bff;">{$MOD.LBL_LMS_TEMPLATE_ID}:</b>{/if}',
                        'customCode' => '{if $lms.enable}{html_options name="lms_template_id" id="lms_template_id" options=$lms.options selected=$fields.lms_template_id.value}{/if}',
                    ),

                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'hours',
                        'label' => 'LBL_HOURS',
                        'required' => true,
                        'customCode' => '
                        <input type="text" class="input_readonly" name="hours" id="hours"  maxlength="26" value="{dotb_number_format var=$fields.hours.value precision=4}" title="{$MOD.LBL_HOURS}" tabindex="0" size="15">
                        ',
                    ),
                    1 =>  array (
                        'name' => 'max_size',
                        'customLabel' => '{$MOD.LBL_MAX_SIZE}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_MAX_SIZE_DES}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input autocomplete="off" type="text" name="max_size" id="max_size" value="{$fields.max_size.value}" title="{$MOD.LBL_MAX_SIZE}" tabindex="0" size="11">',
                    ),
                ),
                6 =>
                array (
                    0 =>   array(
                        'customCode' =>'{if $class_case == "edit"}
                        <input type="button" style="display:none;" name="btn_reload" id="btn_reload" onclick="window.parent.document.getElementById(\'bwc-frame\').contentDocument.location.reload(true);" class="button" value="{$MOD.LBL_BTN_RELOAD}" />
                        {/if}
                        '
                    ),
                    1 =>  'syllabus_by'
                ),
                7 =>
                array (
                    0 => array(
                        'name' => 'change_date_from',
                        'customLabel' =>'<label style="display:none;" id="change_date_from_label">{$MOD.LBL_CHANGE_DATE_FROM} <span class="required">*</span></label>',
                        'customCode' =>'
                        {if $is_change_schedule}
                        <input type="button" style="display:none;" name="btn_edit_schedule" class="button primary" id="btn_edit_schedule" value="{$MOD.LBL_CHANGE_SCHEDULE_C}" />
                        {/if}
                        {if $change_start_date}
                        {if $is_change_startdate}
                        <input type="button" style="display:none;" name="btn_edit_startdate" class="button" id="btn_edit_startdate" value="{$MOD.LBL_CHANGE_START_DATE_C}" />
                        {/if}{/if}
                        <span class="dateTime" id="change_date_from_span" style="display:none;">
                        <input class="date_input" autocomplete="off" type="text" name="change_date_from" id="change_date_from" value="" title="" tabindex="0" size="11" maxlength="10">
                        <img src="themes/RacerX/images/jscalendar.png" alt="Enter Date" style="position:relative; top:6px" border="0" id="change_date_from_trigger">
                        </span>
                        {literal}<script type="text/javascript">
                        Calendar.setup ({
                        inputField : "change_date_from",
                        ifFormat : cal_date_format,
                        daFormat : cal_date_format,
                        button : "change_date_from_trigger",
                        singleClick : true,
                        dateStr : "",
                        startWeekday: 0,
                        step : 1,
                        weekNumbers:false
                        });</script>
                        {/literal}

                        <label id="change_date_to_label" width="12.5%" style="background-color:#eee; color: #444; padding:.6em;display:none;">{$MOD.LBL_CHANGE_DATE_TO}:<span class="required">*</span></label>&nbsp;
                        <span class="dateTime" id="change_date_to_span" style="display:none;">
                        <input class="date_input" autocomplete="off" type="text" name="change_date_to" id="change_date_to" value="" title="" tabindex="0" size="11" maxlength="10">
                        <img src="themes/RacerX/images/jscalendar.png" alt="Enter Date" style="position:relative; top:6px" border="0" id="change_date_to_trigger">
                        </span>
                        {literal}
                        <script type="text/javascript">
                        Calendar.setup ({
                        inputField : "change_date_to",
                        ifFormat : cal_date_format,
                        daFormat : cal_date_format,
                        button : "change_date_to_trigger",
                        singleClick : true,
                        dateStr : "",
                        startWeekday: 0,
                        step : 1,
                        weekNumbers:false
                        });</script>
                        {/literal}
                        ',
                    ),

                    1 => array(
                        'customLabel' =>'{if !empty($fields.id.value)}{$MOD.LBL_CURRENT_SCHEDULE}{/if}',
                        'customCode' => '{if !empty($fields.id.value)}{$SCHEDULE}{/if}',
                    ),
                ),
                8 =>
                array (
                    0 => array (
                        'name' => 'change_reason',
                        'customLabel' =>'<label id="change_reason_label">{$MOD.LBL_CHANGE_BY} <span class="required">*</span></label>',
                        'customCode' => '
                        <table width="70%" style="padding:0px!important;">
                        <tbody><tr colspan = "2">
                        <td nowrap width = "50%" >{html_options name="change_by" id="change_by" options=$fields.change_by.options selected=""}</td>
                        <td nowrap width = "50%" ><textarea style="display:none;" id="change_reason" name="change_reason" rows="3" cols="25"></textarea></td>
                        </tr></tbody>
                        </table>',
                    ),
                    1 => ''

                ),
                9 =>
                array (
                    0 =>
                    array(
                        'customLabel' =>'{$MOD.LBL_DAY} <span class="required">*</span>',
                        'customCode' =>'
                        <ul id="ct_date" style="margin: 0; display:none;">
                        <li valu="Mon" title="{$MOD.LBL_MONDAY}">{$MOD.LBL_MON}</li>
                        <li valu="Tue" title="{$MOD.LBL_TUESDAY}">{$MOD.LBL_TUE}</li>
                        <li valu="Wed" title="{$MOD.LBL_WEDNESDAY}">{$MOD.LBL_WED}</li>
                        <li valu="Thu" title="{$MOD.LBL_THURSDAY}">{$MOD.LBL_THU}</li>
                        <li valu="Fri" title="{$MOD.LBL_FRIDAY}">{$MOD.LBL_FRI}</li>
                        <li valu="Sat" title="{$MOD.LBL_SATURDAY}">{$MOD.LBL_SAT}</li>
                        <li valu="Sun" title="{$MOD.LBL_SUNDAY}">{$MOD.LBL_SUN}</li></ul>
                        <button class="button" type="button" id="btn_clr_time">{$MOD.LBL_BTN_CLEAR}</button>
                        <span style="white-space: nowrap;">
                        <input type="hidden" name="is_skip_holiday" value="0">
                        <input type="checkbox" style="margin-left: 10px;" {if $fields.is_skip_holiday.value == 1}checked{/if} id="is_skip_holiday" name="is_skip_holiday" value="1">
                        <label for="is_skip_holiday">{$MOD.LBL_SKIP_HOLIDAYS}</label>
                        </span>
                        <br><br>
                        <input name="validate_weekday" id="validate_weekday" type="text" style="display:none;"/>',
                    ),
                    1 =>
                    array(
                        'name' => 'start_lesson',
                        'customLabel' => '{$MOD.LBL_START_LESSON}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_START_LESSON_DES}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '{html_options name="start_lesson" id="start_lesson" options=$lessons selected=$fields.start_lesson.value style="width:200px"}',
                    ),
                ),
                10 =>
                array (
                    0 => array(
                        'customLabel' =>'{$MOD.LBL_TIMESLOT}',
                        'customCode' => '{include file="custom/modules/J_Class/tpls/class_schedule.tpl"}',
                    ),
                ),
            ),
            'lbl_editview_panel_onl' =>
            array (
                0 => array(
                    0=> array(
                        'customLabel' => '{$MOD.LBL_ONL_STATUS}:',
                        'customCode' => '{dotb_getscript file="custom/modules/J_Class/js/handleOnlSetting.js"}
                        {if not $ACTIVE_READONLY}
                        <select name="onl_status">{$HTML_ONL_LEARNING_STATUS}</select>
                        {else}
                        <span>{$HTML_ONL_LEARNING_STATUS}</span>
                        {/if}'
                    ),
                    1=>array(
                        'customLabel' => '{$MOD.LBL_CLASSIN_SETTINGS}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_ONLINE_LEARNING_SETTINGS_HELP}\');" src="themes/RacerX/images/helpInline.png" alt="paidAmountHelpTip" class="paidAmountHelpTip">',
                        'customCode' => '{if not $CLASSIN_READONLY}<table width="100%" style="padding:0px!important;">
                        <tbody id="classin_settings">
                        <tr>
                        <td> {$MOD.LBL_CLASSIN_HEAD_TEACHER}</td>
                        <td>{$HTML_CLASSIN_HEAD_TEACHER}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_ASSISTANT}</td>
                        <td>{$HTML_CLASSIN_ASSISTANT}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_AUDITORS}</td>
                        <td>{$HTML_CLASSIN_AUDITORS}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_RECORDING}</td>
                        <td>
                        <select  id="classin_recording" style="width: 200px">
                        <option value="1" selected>{$MOD.LBL_YES}</option>
                        <option value="0">{$MOD.LBL_NO}</option>
                        </select>
                        </td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_STUDENTS_ON_STAGE}</td>
                        <td>
                        <select  id="classin_students_on_stage" style="width: 200px">
                        <option value="1">1v1</option>
                        <option value="2">1v2</option>
                        <option value="3">1v3</option>
                        <option value="4">1v4</option>
                        <option value="5">1v5</option>
                        <option value="6">1v6</option>
                        <option value="7">1v7</option>
                        <option value="8">1v8</option>
                        <option value="9">1v9</option>
                        <option value="10">1v10</option>
                        <option value="11">1v11</option>
                        <option value="12" selected>1v12</option>
                        </select>
                        </td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_FOLDER_NAME}</td>
                        <td>
                        <select  id="classin_select_folder_option" style="width: 200px; margin-bottom: 0.5rem">
                        <!--option value="0" selected>{$MOD.LBL_CLASSIN_SELECT_FOLDER_OP1}</option-->
                        <option value="1" selected>{$MOD.LBL_CLASSIN_SELECT_FOLDER_OP2}</option>
                        <option value="2">{$MOD.LBL_CLASSIN_SELECT_FOLDER_OP3}</option>
                        </select>
                        <input style="width: 200px; margin-top: 0.4rem; display: none" id="classin_folder_name" name="classin_folder_name" placeholder="{$MOD.LBL_AUTO_GENERATE_IF_BLANK}"  disabled />
                        <input id="classin_folder_id" type="hidden" name="classin_folder_id" />
                        <span class="id-ff multiple">
                        <button style="display: none" type="button" id="bt_select_classin_folder" tabindex="0" title="Select" class="button firstChild" value="Select"><img src="themes/default/images/id-ff-select.png"></button>
                        <button style="display: none" type="button" id="bt_clear_classin_folder" tabindex="0" title="Clear" class="button firstChild" value="Clear"><img src="themes/default/images/id-ff-clear.png"></button>
                        </span>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        {else}
                        <table>
                        <tbody id ="classin_settings">
                        <tr>
                        <td> {$MOD.LBL_CLASSIN_HEAD_TEACHER}</td>
                        <td>{$classin_setting.head_teacher}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_ASSISTANT}</td>
                        <td>{$classin_setting.assistant}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_AUDITORS}</td>
                        <td>{$classin_setting.auditors}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_RECORDING}</td>
                        <td>{$classin_setting.recording}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_STUDENTS_ON_STAGE}</td>
                        <td>
                        {$classin_setting.students_on_stage}
                        </td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_FOLDER_NAME}</td>
                        <td>
                        {$classin_setting.folder_name}
                        </td>
                        </tr>

                        </tbody>
                        </table>
                        {/if}',
                    )
                )

            ),
            'lbl_editview_panel2' =>
            array (
                0 =>
                array (
                    '',
                    1 => array(
                        'name' => 'teacher_name',
                    ),
                ),
                1 =>
                array (
                    'assigned_user_name',
                    'cso_name'

                ),
                2 =>
                array (
                    0 =>array (
                        'name' => 'description',
                        'displayParams' =>
                        array (
                            'rows' => 4,
                            'cols' => 60,
                        ),
                    ),
                    1 => 'team_name'
                ),
            ),
        ),
    ),
);
