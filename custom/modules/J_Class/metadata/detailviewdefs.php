<?php
$module_name = 'J_Class';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'hidden' =>
                array (
                    1 => '<input type="hidden" name="json_sessions" id="json_sessions" value=\'{$json_ss}\'>',
                    2 => '<input type="hidden" name="next_session_date" id="next_session_date" value="{$today}">',
                    3 => '<input type="hidden" name="current_status" id="current_status" value="{$fields.status.value}">',
                    4 => '<input type="hidden" name="kind_of_course" id="kind_of_course" value="{$fields.kind_of_course.value}">',
                    5 => '{include file="custom/modules/J_Class/tpls/reverseClass.tpl"}
                    {include file="custom/modules/J_Class/tpls/export_attendance_dialog.tpl"}
                    {include file="custom/modules/J_Class/tpls/teacher_schedule_screen.tpl"}
                    {include file="custom/modules/J_Class/tpls/enrollment.tpl"}
                    {include file="custom/modules/J_Class/tpls/generate_online.tpl"}
                    {include file="custom/modules/J_Class/tpls/session_cancel.tpl"}
                    {include file="custom/modules/J_Class/tpls/demo_template.tpl"}',
                ),
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DELETE',
                    2 =>
                    array (
                        'customCode' => '{$UPGRADE_BUTTON}',

                    ),
                    3 =>
                    array (
                        'customCode' => '<input type="button" class="button" id="attendance_homework" name="attendance_homework" value="{$MOD.LBL_TAKE_ATTENDANCE}"/>',
                    ),
//                    4 =>
//                    array (
//                         'customCode' => '{$bt_admin_ul}',
//                    ),
                    5 =>
                    array (
                        'customCode' => '{$BTN_EXPORT}',
                    ),

                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'javascript' => '
            {dotb_getscript file="custom/modules/J_Class/js/trigger_dl.js"}
            {dotb_getscript file="custom/modules/J_Class/js/enroll_new.js"}
            {dotb_getscript file="custom/modules/J_Class/js/detailview_new.js"}
            {dotb_getscript file="custom/modules/J_Class/js/handleDemo_new.js"}
            {dotb_getscript file="custom/modules/J_Class/js/handleSchedule.js"}
            {dotb_getscript file="custom/modules/J_Class/js/handleCancel.js"}
            {dotb_getscript file="custom/modules/J_Class/js/handleRevert.js"}
            {dotb_getscript file="custom/include/javascript/Mask-Input/jquery.mask.min.js"}
            {dotb_getscript file="custom/modules/J_Class/js/admin_action.js"}
            {dotb_getscript file="custom/modules/J_Class/js/ac_evaluation.js"}
            {dotb_getscript file="custom/modules/J_Class/js/approve_unlock.js"}
            ',

            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_DETAILVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_DETAILVIEW_PANEL2' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array (
            'lbl_detailview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array(
                        'name' => 'picture',
                        'customCode' => '{$picture_class}'
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'class_code',
                    ),
                    1 =>
                    array (
                        //Edit by nnamnguyen
                        'name' => 'status',
                        'customCode' => '{if $fields.status.value == "In Progress"}
                        <span style="color: blue;font-weight: bold;">'+$GLOBALS['app_list_strings']['status_class_list']['{$fields.status.value}']+'</span>
                        {elseif $fields.status.value == "Closed"}
                        <span style="color: #272727;font-weight: bold;">'+$GLOBALS['app_list_strings']['status_class_list']['{$fields.status.value}']+'</span>
                        {elseif $fields.status.value == "Finished"}
                        <span style="color: #A8141B;font-weight: bold;">'+$GLOBALS['app_list_strings']['status_class_list']['{$fields.status.value}']+'</span>
                        {elseif $fields.status.value == "Planning"}
                        <span style="color: #468931;font-weight: bold;">'+$GLOBALS['app_list_strings']['status_class_list']['{$fields.status.value}']+'</span>
                        {/if}
                        '
                    ),
                ),
                2 =>
                array (
                    0 => 'name',
                    1 =>
                    array (
                        'name' => 'start_date',
                        'label' => 'LBL_START_DATE',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'j_class_j_class_1_name',
                        'customLabel' => '{$MOD.LBL_UPGRADE}',
                        'customCode' => '
                        <a href="#bwc/index.php?module=J_Class&action=DetailView&record={$fields.j_class_j_class_1j_class_ida.value}">
                        <span id="j_class_j_class_1j_class_ida" class="dotb_field" data-id-value="{$fields.j_class_j_class_1j_class_ida.value}">{$fields.j_class_j_class_1_name.value}</span></a>
                        &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {$MOD.LBL_UPGRADE_TO_CLASS}: {$UTC}
                        '
                    ),
                    1 =>
                    array (
                        'name' => 'end_date',
                        'label' => 'LBL_END_DATE',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'kind_of_course',
                        'studio' => 'visible',
                        'label' => 'LBL_KOC_NAME',
                        'customCode' => '{$KOC}',
                    ),
                    1 =>
                    array(
                        'name' => 'lms_course_id',
                        'customLabel' => '{if !empty($fields.lms_course_id.value)}<b style="color: #002bff;">{$MOD.LBL_LMS_COURSE_ID}</b>{/if}',
                        'customCode'  => '{if !empty($fields.lms_course_id.value)}{$course_link}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        {$MOD.LBL_LMS_TEMPLATE_ID}: {$template_link}
                        {/if}',
                    ),

                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'hours',
                        'label' => 'LBL_HOURS',
                    ),
                    1 =>
                    array (
                        'name' => 'max_size',
                        'studio' => 'visible',
                        'customLabel' => '{$MOD.LBL_MAX_SIZE}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_MAX_SIZE_DES}\');" src="themes/RacerX/images/helpInline.png">',
                    ),
                ),
                6 =>
                array (
                    0 => 'teacher_name',
                    1 => 'syllabus_by'
                ),
                7 =>
                array (
                    0 =>
                    array (
                        'name' => 'short_schedule',
                        'customLabel' => '{$MOD.LBL_CURRENT_SCHEDULE} <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$HELP_SCHEDULE}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '{$SCHEDULE}',
                    ),
                    1 =>
                    array (
                        'name' => 'start_lesson',
                        'customLabel' => '{$MOD.LBL_START_LESSON}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_START_LESSON_DES}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '{$start_lesson}',
                    ),
                ),
                8 =>
                array (
                    0 => 'description',
                    1 => 'lessonplan_name'
                ),
            ),
            'LBL_EDITVIEW_PANEL_ONL' =>
            array (
                0 => array(
                    array(
                        'customLabel' => '{$MOD.LBL_ONL_STATUS}:',
                        'customCode' => '{$ONLINE_LEARNING_ACTIVE}'
                    ),
                    array(
                        'customLabel' => '{if $CONFIG_ACTIVE}{$MOD.LBL_CLASSIN_SETTINGS}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_ONLINE_LEARNING_SETTINGS_HELP}\');" src="themes/RacerX/images/helpInline.png">{/if}',
                        'customCode' => '{if $CONFIG_ACTIVE}<table width="100%" style="padding:0px!important;">
                        <tbody id="classin_settings">
                        <tr>
                        <td width="30%">{$MOD.LBL_ONL_COURSE}:</td>
                        <td width="70%">{$fields.onl_course_id.value}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_HEAD_TEACHER}:</td>
                        <td>{$classin_setting.head_teacher}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_ASSISTANT}:</td>
                        <td>{$classin_setting.assistant}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_AUDITORS}:</td>
                        <td>{$classin_setting.auditors}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_RECORDING}:</td>
                        <td>{$classin_setting.recording}</td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_STUDENTS_ON_STAGE}:</td>
                        <td>
                        {$classin_setting.students_on_stage}
                        </td>
                        </tr>
                        <tr>
                        <td>{$MOD.LBL_CLASSIN_FOLDER_NAME}:</td>
                        <td>
                        {$classin_setting.folder_name}
                        </td>
                        </tr>

                        </tbody>
                        </table>{/if}',
                    )
                )
            ),
            'lbl_detailview_panel2' =>
            array (
                0 =>
                array (
                    0 => 'assigned_user_name',
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
                1 =>
                array (
                    0 => 'cso_name',
                    1 =>
                    array (
                        'name' => 'date_entered',
                        'customCode' => '
                        {$fields.date_entered.value} {$APP.LBL_BY}
                        {$fields.created_by_name.value}
                        ',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),
                2 =>
                array (
                    0 => 'team_name',
                    1 =>
                    array (
                        'customCode' => '{include file="custom/modules/J_Class/tpls/viewExport.tpl"}',
                    ),
                ),
            ),
        ),
    ),
);

