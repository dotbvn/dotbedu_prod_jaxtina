<?php
// created: 2019-07-19 13:52:03
$viewdefs['KBContents']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'status' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'assigned_user_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'active_date' => 
    array (
    ),
    'exp_date' => 
    array (
    ),
    'is_external' => 
    array (
    ),
    'kbsapprover_name' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'tag' => 
    array (
    ),
  ),
);