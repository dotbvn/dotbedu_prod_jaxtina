<?php
$viewdefs['DRI_Workflow_Task_Templates']['base']['view']['record'] = array(
  'panels' =>
  array(
      0 =>
      array(
        'name' => 'panel_header',
        'label' => 'LBL_PANEL_HEADER',
        'header' => true,
        'fields' =>
        array(
          0 =>
          array(
            'name' => 'picture',
            'type' => 'avatar',
            'width' => 42,
            'height' => 42,
            'dismiss_label' => true,
            'readonly' => true,
          ),
          1 =>
          array(
            'name' => 'name',
            'link' => false,
          ),
          2 =>
          array(
            'name' => 'favorite',
            'label' => 'LBL_FAVORITE',
            'type' => 'favorite',
            'dismiss_label' => true,
          ),
          3 =>
          array(
            'name' => 'follow',
            'label' => 'LBL_FOLLOW',
            'type' => 'follow',
            'readonly' => true,
            'dismiss_label' => true,
          ),
        ),
      ),
      1 =>
      array(
        'name' => 'panel_body',
        'label' => 'LBL_PANEL_BODY',
        'columns' => 2,
        'labelsOnTop' => true,
        'placeholders' => true,
        'newTab' => false,
        'panelDefault' => 'expanded',
        'fields' =>
        array(
          0 =>
          array(
            'name' => 'dri_workflow_template_name',
            'readonly' => true,
            'label' => 'LBL_DRI_WORKFLOW_TEMPLATE',
          ),
          1 => 'type',
          2 => 'sort_order',
          3 => 'priority',
          4 => 'points',
          5 =>
          array(
            'name' => 'blocked_by',
            'label' => 'LBL_BLOCKED_BY',
            'type' => 'blocked_by',
            'related_fields' =>
            array(
                0 => 'dri_workflow_template_id',
            ),
          ),
          6 => 'remind_email',
          7 => 'remind_popup',
          8 =>
          array(
            'name' => 'description',
            'span' => 12,
          ),
          9 => 'assignee_rule',
          10 => array(),
          11 => 'target_assignee',
          12 => 'target_assignee_user_name',
          13 => 'target_assignee_team_name',
          14 => array(),
          15 => 'completed_by_module',
          16 => array(),
          17 => 'completed_by_key',
        ),
      ),
      2 =>
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
        'name' => 'LBL_RECORDVIEW_PANEL2',
        'label' => 'LBL_RECORDVIEW_PANEL2',
        'columns' => 2,
        'labelsOnTop' => 1,
        'placeholders' => 1,
        'fields' =>
        array (
          0 =>
          array (
            'name' => 'task_due_date_type',
            'label' => 'LBL_TASK_DUE_DATE_TYPE',
          ),
          1 =>
          array (
            'name' => 'task_due_days',
            'label' => 'LBL_TASK_DUE_DAYS',
          ),
          2 => 'due_date_module',
          3 =>
          array (
            'name' => 'due_date_field',
            'type' => 'due_date_field',
          ),
        ),
      ),
      3 =>
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
        'name' => 'LBL_RECORDVIEW_PANEL3',
        'label' => 'LBL_RECORDVIEW_PANEL3',
        'columns' => 2,
        'labelsOnTop' => 1,
        'placeholders' => 1,
        'fields' =>
        array (
          0 =>
          array (
            'name' => 'momentum_start_type',
            'label' => 'LBL_MOMENTUM_START_TYPE',
          ),
          1 =>
          array (
            'name' => 'momentum_points',
            'label' => 'LBL_MOMENTUM_POINTS',
          ),
          2 =>
          array (
            'name' => 'momentum_due_days',
            'label' => 'LBL_MOMENTUM_DUE_DAYS',
          ),
          3 =>
          array (
            'name' => 'momentum_due_hours',
            'label' => 'LBL_MOMENTUM_DUE_HOURS',
          ),
          4 =>
          array (
            'name' => 'momentum_start_module',
            'label' => 'LBL_MOMENTUM_START_MODULE',
          ),
          5 =>
          array (
            'name' => 'momentum_start_field',
            'type' => 'momentum_start_field',
            'label' => 'LBL_MOMENTUM_START_FIELD',
          ),
        ),
      ),
      4 =>
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
        'name' => 'LBL_RECORDVIEW_PANEL1',
        'label' => 'LBL_RECORDVIEW_PANEL1',
        'columns' => 2,
        'labelsOnTop' => 1,
        'placeholders' => 1,
        'fields' =>
        array (
          0 =>
          array (
            'name' => 'date_entered_by',
            'readonly' => true,
            'inline' => true,
            'type' => 'fieldset',
            'label' => 'LBL_DATE_ENTERED',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'date_entered',
              ),
              1 =>
              array (
                'type' => 'label',
                'default_value' => 'LBL_BY',
              ),
              2 =>
              array (
                'name' => 'created_by_name',
              ),
            ),
          ),
          1 => 'team_name',
          2 =>
          array (
            'name' => 'date_modified_by',
            'readonly' => true,
            'inline' => true,
            'type' => 'fieldset',
            'label' => 'LBL_DATE_MODIFIED',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'date_modified',
              ),
              1 =>
              array (
                'type' => 'label',
                'default_value' => 'LBL_BY',
              ),
              2 =>
              array (
                'name' => 'modified_by_name',
              ),
            ),
          ),
        ),
      ),
  ),
  'templateMeta' =>
  array (
    'useTabs' => false,
  ),
);
