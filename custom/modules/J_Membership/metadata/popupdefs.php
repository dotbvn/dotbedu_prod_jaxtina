<?php
$popupMeta = array (
    'moduleMain' => 'J_Membership',
    'varName' => 'J_Membership',
    'orderBy' => 'j_membership.name',
    'whereClauses' => array (
  'name' => 'j_membership.name',
),
    'searchInputs' => array (
  0 => 'j_membership_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'PHONE_MOBILE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => 10,
    'default' => true,
    'name' => 'phone_mobile',
  ),
  'BIRTHDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BIRTHDATE',
    'width' => 10,
    'default' => true,
    'name' => 'birthdate',
  ),
  'EMAIL' => 
  array (
    'type' => 'email',
    'studio' => 
    array (
      'visible' => true,
      'searchview' => true,
      'editview' => true,
      'editField' => true,
    ),
    'link' => 'email_addresses_primary',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'width' => 10,
    'default' => true,
    'name' => 'email',
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
    'name' => 'date_modified',
  ),
),
);
