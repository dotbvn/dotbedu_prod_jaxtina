<?php
    $hook_version = 1;
    $hook_array = Array();
    // position, file, function

    $hook_array['process_record'] = Array();
    $hook_array['process_record'][] = Array(1, 'Load Invoice Content', 'custom/modules/J_Invoice/logicInvoice.php', 'logicInvoice', 'loadInvoiceContent');

    $hook_array['before_save'] = Array();
    $hook_array['before_save'][] = Array(1, 'Handle before save', 'custom/modules/J_Invoice/logicInvoice.php','logicInvoice', 'handleBeforeSave');


    $hook_array['before_delete'] = Array();
    $hook_array['before_delete'][] = Array(2, 'Delete Invoice', 'custom/modules/J_Invoice/logicInvoice.php', 'logicInvoice', 'deletedInvoice');

    ?>
