<?php
$module_name = 'J_Invoice';
$viewdefs[$module_name] =
array (
    'QuickCreate' =>
    array (
        'templateMeta' =>
        array (
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array (
            'default' =>
            array (
                0 =>
                array (
                    0 => 'name',
                    1 =>
                    array (
                        'name' => 'status',
                        'label' => 'LBL_STATUS',
                    ),
                ),
                1 =>
                array (
                    0 => 'supplier',
                    1 =>
                    array (
                        'name' => 'invoice_date',
                        'label' => 'LBL_INVOICE_DATE',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'serial_no',
                        'label' => 'LBL_SERIAL_NO',
                    ),
                    1 =>  ''
                ),
                3 =>
                array (
                    array (
                        'name' => 'transaction_id',
                        'label' => 'LBL_TRANSACTION_ID',
                    ),
                    'account_name',
                ),
            ),
        ),
    ),
);
