<?php
$popupMeta = array (
    'moduleMain' => 'J_Invoice',
    'varName' => 'J_Invoice',
    'orderBy' => 'j_invoice.name',
    'whereClauses' => array (
  'name' => 'j_invoice.name',
  'invoice_date' => 'j_invoice.invoice_date',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'invoice_date',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'invoice_date' => 
  array (
    'type' => 'date',
    'label' => 'LBL_INVOICE_DATE',
    'width' => '10',
    'name' => 'invoice_date',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => 10,
    'name' => 'status',
  ),
  'INVOICE_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_INVOICE_DATE',
    'width' => 10,
    'default' => true,
    'name' => 'invoice_date',
  ),
  'INVOICE_AMOUNT' => 
  array (
    'type' => 'currency',
    'default' => true,
    'label' => 'LBL_INVOICE_AMOUNT',
    'currency_format' => true,
    'width' => 10,
    'name' => 'invoice_amount',
  ),
  'TRANSACTION_ID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TRANSACTION_ID',
    'width' => 10,
    'default' => true,
  ),
  'TEAM_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
),
);
