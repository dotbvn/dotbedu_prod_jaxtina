<?php
$module_name = 'J_Invoice';
$viewdefs[$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DELETE',
          2 => 
          array (
            'customCode' => '<input type="button" class="button" id="download_pdf" name="download_pdf" value="{$MOD.LBL_DOWNLOAD_PDF}"/>',
          ),
        ),
        'hidden' => 
        array (
          0 => '{dotb_getscript file="custom/modules/J_Invoice/js/detail.js"}',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'status',
            'label' => 'LBL_STATUS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'serial_no',
            'label' => 'LBL_SERIAL_NO',
          ),
          1 => 
          array (
            'name' => 'pattern',
            'label' => 'LBL_PATTERN',
          ),
        ),
        2 => 
        array (
          0 => 'supplier',
          1 => 
          array (
            'name' => 'before_discount',
            'label' => 'LBL_BEFORE_DISCOUNT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'invoice_date',
            'label' => 'LBL_INVOICE_DATE',
          ),
          1 => 
          array (
            'name' => 'total_discount_amount',
            'label' => 'LBL_DISCOUNT_AMOUNT',
          ),
        ),
        4 => 
        array (
          0 => 'vat',
          1 => 
          array (
            'name' => 'vat_amount',
            'label' => 'LBL_VAT_AMOUNT',
          ),
        ),
        5 => 
        array (
          0 => 'reason_cancel',
          1 => 
          array (
            'name' => 'invoice_amount',
            'label' => 'LBL_INVOICE_AMOUNT',
          ),
        ),
        6 => 
        array (
          0 => 'is_manual',
          1 => 
          array (
            'name' => 'transaction_id',
            'label' => 'LBL_TRANSACTION_ID',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'invoiceGUID',
            'label' => 'LBL_INVOICEGUID',
          ),
          1 => 
          array (
            'name' => 'license_remove_id',
            'label' => 'LBL_LICENSE_REMOVE_ID',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'account_name',
            'label' => 'LBL_ACCOUNT_NAME',
          ),
          1 => 
          array (
            'name' => 'PartnerInvoiceStringID',
            'label' => 'PartnerInvoiceStringID',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'content_vat_invoice',
            'label' => 'LBL_CONTENT_VAT_INVOICE',
          ),
        ),
        10 => 
        array (
          0 => 'description',
        ),
        11 => 
        array (
          0 => 'team_name',
          1 => 'assigned_user_name',
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'date_modified',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
      ),
    ),
  ),
);
