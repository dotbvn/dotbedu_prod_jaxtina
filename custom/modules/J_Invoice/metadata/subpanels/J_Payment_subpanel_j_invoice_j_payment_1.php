<?php
// created: 2022-06-12 00:35:28
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '10%',
        'default' => true,
    ),
    'supplier' =>
    array (
        'type' => 'enum',
        'vname' => 'LBL_SUPPLIER',
        'width' => '10%',
        'default' => true,
    ),
    'status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_STATUS',
        'width' => '10%',
    ),
    'invoice_date' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_INVOICE_DATE',
        'width' => '10%',
        'default' => true,
    ),
    'transaction_id' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_TRANSACTION_ID',
        'width' => '20%',
        'default' => true,
    ),
    'date_entered' =>
    array (
        'type' => 'datetime',
        'studio' =>
        array (
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
    ),
    'created_by_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'readonly' => true,
        'vname' => 'LBL_CREATED',
        'id' => 'CREATED_BY',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Users',
        'target_record_key' => 'created_by',
    ),
    'team_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'studio' =>
        array (
            'portallistview' => false,
            'portalrecordview' => false,
        ),
        'vname' => 'LBL_TEAMS',
        'id' => 'TEAM_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Teams',
        'target_record_key' => 'team_id',
    ),
    'custom_button' =>
    array (
        'type' => 'varchar',
        'width' => '10%',
        'default' => true,
    ),
);