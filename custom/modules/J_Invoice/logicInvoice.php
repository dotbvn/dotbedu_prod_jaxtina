<?php
class logicInvoice{
    function deletedInvoice($bean, $event, $arguments){
        global $current_user;
        if(empty($bean->is_manual)
        && !empty($bean->id)
        && !$current_user->isAdminForModule('J_Invoice')){
            $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Invoice','".$bean->id."'));";
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: \'You may not be authorized to delete this Invoice!\',
            autoClose: true});
            '.$route.'
            </script>';
            die();
        }

        if(!empty($bean->id)) $GLOBALS['db']->query("UPDATE j_paymentdetail SET invoice_id='' WHERE invoice_id = '{$bean->id}' AND deleted = 0");
    }

    function loadInvoiceContent(&$bean, $event, $arguments) {

        if($bean->supplier == 'Bkav' && !empty($bean->transaction_id)){
            $bean->transaction_id = "<b>Download 🔗: <a target='_blank' href='https://van.ehoadon.vn/TCHD?MTC={$bean->transaction_id}'>{$bean->transaction_id}</a></b>";
        } elseif($bean->supplier == 'Misa' && !empty($bean->transaction_id)){
            $bean->transaction_id = "<b>Download 🔗: <a target='_blank' href='https://www.meinvoice.vn/tra-cuu/downloadhandler.ashx?type=pdf&code={$bean->transaction_id}'>{$bean->transaction_id}</a></b>";
        }
        if($bean->status != 'Cancelled'){
            if(checkDataLockDate($bean->invoice_date) && ACLController::checkAccess('J_Invoice', 'delete', true))
            $bean->custom_button = '<button style="margin-left: 5px;width: 115px;color:#E61718" class="button cancel_invoice" type="button" onclick = \'cancel_invoice( "'.$bean->id.'" ,"")\'> <i class="far fa-minus-circle"></i> '.translate('LBL_VOID_INVOICE','J_Payment').'</button>';
        }

        //Payment type
        switch ($bean->status) {
            case "Draft":
                $colorClass = "textbg_yellow";
                break;
            case "Published":
                $colorClass = "textbg_redlight";
                break;
            case "Paid":
                $colorClass = "textbg_green";
                break;
            case "Cancelled":
                $colorClass = "textbg_blood";
                break;
        }
        $bean->status = "<span class='full-width visible'><span class='label ellipsis_inline $colorClass'>". $GLOBALS['app_list_strings']['status_invoice_list'][$bean->status] ."</span>";


    }

    function handleBeforeSave(&$bean, $event, $arguments){

        //Hóa đơn manual
        if($bean->status == 'Cancelled'){
            if ($bean->load_relationship('j_invoice_j_payment_1')){
                foreach($bean->j_invoice_j_payment_1->getBeans() as $payId => $pay)
                    $student_id = $pay->parent_id;
                if(!empty($student_id))
                    $GLOBALS['db']->query("UPDATE j_paymentdetail SET invoice_id='' WHERE invoice_id = '{$bean->id}' AND parent_id = '$student_id' AND deleted = 0");
            }
        }

        //Kiểm tra TH hoá đơn tạo Manual
        if($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save'){
            $bean->is_manual = 1;
            if($bean->status != 'Cancelled'){
                //update payment_details
                if($bean->load_relationship('j_invoice_j_payment_1')){
                    foreach($bean->j_invoice_j_payment_1->getBeans() as $payId => $pay)
                        $GLOBALS['db']->query("UPDATE j_paymentdetail SET invoice_id='{$bean->id}' WHERE status = 'Paid' AND IFNULL(invoice_id,'') = '' AND payment_id = '$payId' AND deleted = 0");
                }
            }
        }
    }
}
?>
