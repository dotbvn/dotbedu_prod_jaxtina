<?php
// created: 2023-10-16 14:31:26
$viewdefs['J_Invoice']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'account_name' => 
    array (
    ),
    'license_remove_id' => 
    array (
    ),
    'transaction_id' => 
    array (
    ),
    'supplier' => 
    array (
    ),
    'name' => 
    array (
    ),
    'serial_no' => 
    array (
    ),
    'description' => 
    array (
    ),
    'before_discount' => 
    array (
    ),
    'total_discount_amount' => 
    array (
    ),
    'invoice_amount' => 
    array (
    ),
    'vat' => 
    array (
    ),
    'vat_amount' => 
    array (
    ),
    'invoice_date' => 
    array (
    ),
    'status' => 
    array (
    ),
    'is_manual' => 
    array (
    ),
    'partnerinvoicestringid' => 
    array (
    ),
    'invoiceguid' => 
    array (
    ),
    'pattern' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'team_name' => 
    array (
    ),
  ),
);