<?php
$module_name = 'J_Invoice';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_1',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'name',
                                'label' => 'LBL_NAME',
                                'default' => true,
                                'enabled' => true,
                                'link' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'supplier',
                                'label' => 'LBL_SUPPLIER',
                                'enabled' => true,
                                'default' => true,
                            ),
                            2 =>
                            array (
                                'name' => 'invoice_amount',
                                'label' => 'LBL_INVOICE_AMOUNT',
                                'enabled' => true,
                                'default' => true,
                            ),
                            3 =>
                            array (
                                'name' => 'invoice_date',
                                'label' => 'LBL_INVOICE_DATE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            4 =>
                            array (
                                'name' => 'status',
                                'label' => 'LBL_STATUS',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            5 =>
                            array (
                                'name' => 'transaction_id',
                                'label' => 'LBL_TRANSACTION_ID',
                                'enabled' => true,
                                'default' => true,
                                  'type' => 'html',
                                  'width' => 'xlarge',
                            ),
                            6 =>
                            array (
                                'name' => 'date_entered',
                                'enabled' => true,
                                'default' => true,
                            ),
                            7 =>
                            array (
                                'name' => 'created_by_name',
                                'label' => 'LBL_CREATED',
                                'enabled' => true,
                                'readonly' => true,
                                'id' => 'CREATED_BY',
                                'link' => true,
                                'default' => true,
                            ),
                            8 =>
                            array (
                                'name' => 'team_name',
                                'label' => 'LBL_TEAM',
                                'default' => true,
                                'enabled' => true,
                            ),
                            9 =>
                            array (
                                'name' => 'serial_no',
                                'label' => 'LBL_SERIAL_NO',
                                'enabled' => true,
                                'default' => false,
                            ),
                            10 =>
                            array (
                                'name' => 'before_discount',
                                'label' => 'LBL_BEFORE_DISCOUNT',
                                'enabled' => true,
                                'default' => false,
                            ),
                            11 =>
                            array (
                                'name' => 'total_discount_amount',
                                'label' => 'LBL_DISCOUNT_AMOUNT',
                                'enabled' => true,
                                'default' => false,
                            ),
                            12 =>
                            array (
                                'name' => 'content_vat_invoice',
                                'label' => 'LBL_CONTENT_VAT_INVOICE',
                                'enabled' => true,
                                'sortable' => false,
                                'default' => false,
                            ),
                            13 =>
                            array (
                                'name' => 'date_modified',
                                'enabled' => true,
                                'default' => false,
                            ),
                            14 =>
                            array (
                                'name' => 'pattern',
                                'label' => 'LBL_PATTERN',
                                'enabled' => true,
                                'default' => false,
                            ),
                            15 =>
                            array (
                                'name' => 'modified_by_name',
                                'label' => 'LBL_MODIFIED',
                                'enabled' => true,
                                'readonly' => true,
                                'id' => 'MODIFIED_USER_ID',
                                'link' => true,
                                'default' => false,
                            ),
                            16 =>
                            array (
                                'name' => 'description',
                                'label' => 'LBL_DESCRIPTION',
                                'enabled' => true,
                                'sortable' => false,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
                'orderBy' =>
                array (
                    'field' => 'date_modified',
                    'direction' => 'desc',
                ),
            ),
        ),
    ),
);
