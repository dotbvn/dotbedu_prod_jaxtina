$(document).ready(function(){
    $('#download_pdf').live('click',function(){
        ajaxStatus.showStatus('Waiting <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
        $.ajax({
            type: "POST",
            url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
            data: {
                type: "ajaxExportInvoice",
                payment_id: '',
                invoice_id: window.top.App.controller.context.attributes.model.attributes.id,
            },
            success: function (data) {
                data = JSON.parse(data);
                if(data.success == "1") {
                    window.open(data.url);
                } else {
                    toastr.error(data.label);
                }
                ajaxStatus.hideStatus();
                $(".textbg_greenDotB").attr("disabled", false);
            },
        });
    })
})
