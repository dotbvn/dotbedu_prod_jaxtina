<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class J_InvoiceViewEdit extends ViewEdit
{
    var $useForSubpanel = true;
    var $useModuleQuickCreateTemplate = true;

    public function display(){
        global $current_user;
        if(empty($this->bean->is_manual)
        && !empty($this->bean->id)
        && !$current_user->isAdminForModule('J_Invoice')){
            $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Invoice','".$this->bean->id."'));";
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: \'You may not be authorized to edit this Invoice!\',
            autoClose: true});
            '.$route.'
            </script>';
            die();
        }
        //Set default suplier
        if(empty($this->bean->id) && $_REQUEST['return_module'] == 'J_Payment' && !empty($_REQUEST['return_id'])){
            $payment = BeanFactory::getBean('J_Payment', $_REQUEST['return_id']);
            $evat_id = $GLOBALS['db']->getOne("SELECT id FROM j_configinvoiceno WHERE team_id = '{$payment->team_id}' AND deleted = 0 AND active = 1");
            $evat = BeanFactory::getBean('J_ConfigInvoiceNo', $evat_id, array('disable_row_level_security' => true));
            $_REQUEST['supplier'] = $evat->supplier;
            $_REQUEST['serial_no'] = $evat->serial_no;
            $_REQUEST['account_name'] = $payment->account_name;
            $_REQUEST['account_id'] = $payment->account_id;
        }
        parent::display();
    }

}