<?php
$module_name = 'J_ConfigInvoiceNo';
$viewdefs[$module_name] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'javascript' => '{dotb_getscript file="custom/modules/J_ConfigInvoiceNo/js/edit.js"}',
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'default' =>
            array (
                array (
                    0 =>
                    array (
                        'name' => 'supplier',
                        'label' => 'LBL_SUPPLIER',
                    ),
                    1 =>
                    array (
                        'name' => 'active',
                        'label' => 'LBL_ACTIVE',
                    ),
                ),

                array (
                    0 =>
                    array (
                        'name' => 'team_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldTeam.tpl"}',
                    ),
                    ''
                ),

                array (
                    0 =>
                    array (
                        'name' => 'host',
                        'label' => 'LBL_HOST',
                    ),
                    1 =>
                    array (
                        'name' => 'serial_no',
                        'label' => 'LBL_SERIAL_NO',
                    ),
                ),

                array (
                    0 =>
                    array (
                        'name' => 'user_name',
                        'label' => 'LBL_USER_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'invoice_no_current',
                        'label' => 'LBL_INVOICE_NO_CURRENT',
                    ),
                ),

                array (
                    0 =>
                    array (
                        'name' => 'password',
                        'label' => 'LBL_PASSWORD',
                    ),
                    1 =>
                    array (
                        'name' => 'pattern',
                        'label' => 'LBL_PATTERN',
                    ),
                ),

                array (
                    0 =>
                    array (
                        'name' => 'accpass',
                        'label' => 'LBL_ACCPASS',
                    ),

                ),

                array (
                    0 =>
                    array (
                        'name' => 'seller_legal_name',
                        'label' => 'LBL_SELLER_LEGAL_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'seller_address_line',
                        'label' => 'LBL_SELLER_ADDRESS_LINE',
                    ),
                ),

                array (
                    0 =>
                    array (
                        'name' => 'seller_legal_name_1',
                        'label' => 'LBL_SELLER_LEGAL_NAME_1',
                    ),
                    1 =>
                    array (
                        'name' => 'seller_address_line_1',
                        'label' => 'LBL_SELLER_ADDRESS_LINE_1',
                    ),
                ),
                array (
                    'seller_phone_number',
                    'seller_bank_name',
                ),
                array (
                    'publish_type',
                    'invoicing',
                ),


                array (
                    0 =>
                    array (
                        'name' => 'buyer_null',
                        'label' => 'LBL_BUYER_NULL',
                    ),
                    1 =>
                    array (
                        'name' => 'account',
                        'label' => 'LBL_ACCOUNT',
                    ),
                ),

                array (
                    0 => 'description',
                    1 =>  'seller_tax_code'
                ),

                array (
                    0 => 'bookgift_content',
                    1 => 'no_email_send_to'
                ),

                array (
                    0 => 'deposit_content',
                    1 => 'placement_test_content'
                ),
                array (
                    0 => '',
                    1 => 'partnerGUID'   //Field này của BKAV ko đc bỏ
                ),
            ),
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'e_sign_user',
                        'label' => 'LBL_E_SIGN_USER',
                    ),
                    1 =>
                    array (
                        'name' => 'pass_code',
                        'label' => 'LBL_PASS_CODE',
                    ),
                ),
            ),
        ),
    ),
);
