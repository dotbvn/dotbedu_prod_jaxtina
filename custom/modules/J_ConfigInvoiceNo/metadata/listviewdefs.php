<?php
$module_name = 'J_ConfigInvoiceNo';
$listViewDefs[$module_name] =
array (
  'name' =>
  array (
    'width' => '15%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
    'supplier' =>
  array (
    'width' => '15%',
    'label' => 'LBL_SUPPLIER',
    'default' => true,
    'link' => true,
  ),
    'partnerGUID' =>
        array (
            'width' => '15%',
            'label' => 'LBL_PARTNERGUID',
            'default' => true,
            'link' => true,
            'type' => 'varchar',
        ),
  'serial_no' =>
  array (
    'type' => 'varchar',
    'label' => 'LBL_SERIAL_NO',
    'width' => '10%',
    'default' => true,
  ),
  'invoice_no_current' =>
  array (
    'type' => 'varchar',
    'label' => 'LBL_INVOICE_NO_CURRENT',
    'width' => '10%',
    'default' => true,
  ),
);
