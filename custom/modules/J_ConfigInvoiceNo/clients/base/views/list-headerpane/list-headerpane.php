<?php


$viewdefs['J_ConfigInvoiceNo']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_CONFIG_INVOICE_NO',
			'tooltip' => 'LNK_NEW_J_CONFIG_INVOICE_NO',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_ConfigInvoiceNo',
            'route'=>'#J_ConfigInvoiceNo/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_CONFIG_INVOICE_NO_REPORTS',
			'tooltip' => 'LNK_J_CONFIG_INVOICE_NO_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_ConfigInvoiceNo',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);