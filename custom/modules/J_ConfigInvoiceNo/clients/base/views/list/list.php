<?php
$module_name = 'J_ConfigInvoiceNo';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 =>
              array (
                'name' => 'supplier',
                'label' => 'LBL_SUPPLIER',
                'enabled' => true,
                'default' => true,
              ),
              2 =>
              array (
                'name' => 'active',
                'label' => 'LBL_ACTIVE',
                'enabled' => true,
                'default' => true,
              ),
              3 =>
              array (
                'name' => 'serial_no',
                'label' => 'LBL_SERIAL_NO',
                'enabled' => true,
                'default' => true,
              ),
              4 =>
              array (
                'name' => 'invoice_no_current',
                'label' => 'LBL_INVOICE_NO_CURRENT',
                'enabled' => true,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'invoicing',
                'label' => 'LBL_INVOICING',
                'enabled' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              7 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              8 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
