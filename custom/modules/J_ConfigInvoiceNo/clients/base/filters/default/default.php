<?php
// created: 2019-04-11 08:31:21
//Modified by Tuan Anh
$viewdefs['J_ConfigInvoiceNo']['base']['filter']['default'] = array(
    'default_filter' => 'all_records',
    'fields' =>
    array(

        'invoice_no_current' => array(),
        'host' => array(),
        'user_name' => array(),
        'account' => array(),
        'pattern' => array(),
        'active' => array(),
        'serial_no' => array(),
        'account' => array(),

        '$favorite' =>
        array(
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),

        //Nhom 3
        'utm_agent' => array(),
        'date_entered' => array(),
        'date_modified' => array(),
        'modified_by_name' => array(),
        'created_by_name' => array(),
        //End Nhom 3
    ),
    'quicksearch_field' => array(
        'name',
        'team_name',
    ),
    'quicksearch_priority' => 2,
);