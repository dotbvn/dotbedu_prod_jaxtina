$(document).ready(function() {
    $('#btn_team_id').click(function(){
        open_popup('Teams', 1000, 700, "", true, false, {"call_back_function":"set_team_return","form_name":"EditView","field_to_name_array":{"id":"team_id","name":"team_name"}}, "single", true);
    });
    $('#btn_clr_team_id').click(function(){
        $('#team_name,#team_id').val('');
    });
});

function set_team_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'team_name':
                    var team_name = val;
                    break;
                case 'team_id':
                    var team_id = val;
                    break;
            }
        }
    }
    $('#team_name').val(team_name);
    $('#team_id').val(team_id);
}
