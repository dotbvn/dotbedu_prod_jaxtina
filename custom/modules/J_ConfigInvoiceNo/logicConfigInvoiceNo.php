<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicConfigInvoiceNo {
    function beforeSave(&$bean, $event, $arguments){
        //Hard code : Team set
        //Todo: Action này chỉ chạy khi save tại màn hình Save KHÔNG chạy ở bất cứ đâu khác
        if(($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save')){
            $rs1 = $GLOBALS['db']->query("SELECT name, code_prefix FROM teams WHERE id = '{$_POST['team_id']}'");
            $row = $GLOBALS['db']->fetchByAssoc($rs1);
            $bean->name = $row['name'];
            if(!empty($row['code_prefix'])) $bean->name .= '-'.$row['code_prefix'];
            //Upper Case - Fix code
            $bean->serial_no   = strtoupper($bean->serial_no);

            $bean->team_id     = $_POST['team_id'];
            $bean->team_set_id = $_POST['team_id'];

            //check Duplicate
            $qD = "SELECT id FROM j_configinvoiceno WHERE team_id='{$bean->team_id}' AND deleted=0 AND id <> '{$bean->id}'";
            $id = $GLOBALS['db']->getOne($qD);
            if(!empty($id)){
                $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_ConfigInvoiceNo','".$id."'));";
                echo '<script type="text/javascript">
                window.parent.DOTB.App.alert.show(\'message-id\', {
                level: \'error\',
                messages: \'--Configuration already exists !!--<br>--REDIRECTED TO RECORD--\',
                autoClose: false});
                '.$route.'
                </script>';
                die();
            }
        }
    }
}