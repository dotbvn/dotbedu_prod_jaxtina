<?php

switch ($_REQUEST['type']) {
    case 'loadConfig':
        $result = loadConfig();
        echo $result;
        break;
    case 'saveConfig':
        $result = saveConfig();
        echo $result;
        break;
}

// ----------------------------------------------------------------------------------------------------------\\

function loadConfig(){

    if(( empty($_POST['tg_center']) &&  empty($_POST['tg_region']) &&  empty($_POST['tg_user'])) || empty($_POST['tg_by']) || empty($_POST['tg_type']) || empty($_POST['tg_year']) || empty($_POST['tg_frequency']) || empty($_POST['tg_unit_from']) || empty($_POST['tg_unit_to']))
        return json_encode(array("success" => "0"));

    $is_center = $is_user = $is_user_by_center = false;
    $result = $tg_center = $tg_user = $tg_user_by_center = $invalid_input = array();
    $by_c = $_POST['tg_by'][0];
    // Get list Center
    if(!empty($_POST['tg_center']) && $by_c == 'center'){
        $tg_center = $GLOBALS['db']->fetchArray("SELECT DISTINCT
            IFNULL(teams.id, '') team_id,
            IFNULL(teams.code_prefix, '') code_prefix,
            IFNULL(teams.name, '') team_name
            FROM teams
            WHERE (((teams.id IN ('".implode("','",$_POST['tg_center'])."')))) AND teams.deleted = 0
        ORDER BY team_name ASC");

        foreach($tg_center as $center)
            $result[$center['team_id']] = $center;
    }

    // Get list User
    if(!empty($_POST['tg_user']) && $by_c == 'user'){
        if(!empty($tg_center)) $ext_center = "AND (l1.id IN ('".implode("','",array_column($tg_center, 'team_id'))."'))";
        $tg_user = $GLOBALS['db']->fetchArray("SELECT DISTINCT
            IFNULL(users.id, '') primaryid,
            IFNULL(users.full_user_name, '') full_user_name,
            IFNULL(users.title, '') title,
            IFNULL(users.is_admin, 0) is_admin,
            IFNULL(l1.id, '') team_id,
            IFNULL(l1.code_prefix, '') code_prefix,
            IFNULL(l1.name, '') team_name
            FROM users
            INNER JOIN teams l1 ON users.default_team = l1.id
            AND l1.deleted = 0 $ext_center
            WHERE users.deleted = 0 AND users.id IN('".implode("','",$_POST['tg_user'])."')
        ORDER BY team_name ASC");

        foreach($tg_user as $user)
            $result[$user['team_id']][$user['primaryid']] = $user;
    }

    // Get list User by Center
    if (!empty($_POST['tg_center'])
    && !empty($_POST['tg_user'])
    && $by_c == 'user_by_center') {
        $tg_user_by_center = $GLOBALS['db']->fetchArray("SELECT DISTINCT
            IFNULL(users.id, '') primaryid,
            IFNULL(users.full_user_name, '') full_user_name,
            IFNULL(l1.id, '') team_id,
            IFNULL(l1.name, '') team_name
            FROM users
            INNER JOIN teams l1 ON l1.deleted = 0 AND (l1.id IN ('".implode("','",$_POST['tg_center'])."'))
            WHERE users.deleted = 0 AND users.id IN('".implode("','",$_POST['tg_user'])."')
        ORDER BY team_name, full_user_name ASC");

        foreach($tg_user_by_center as $user)
            $result[$user['team_id']][$user['primaryid']] = $user;
    }

    //Get Table
    $html = "<table width='100%' class='table table-striped table-bordered dataTable' id='celebs'><thead><tr>";
    $html .= '<th width="1%" style="text-align: center;vertical-align: text-top;">No.</th>';

    if(!empty($tg_center) && empty($tg_user_by_center)){
        $is_center = true;
        $html .= '<th width="3%" style="text-align: center;vertical-align: text-top;min-width: 100px;">Center</th>';
    }

    if(!empty($tg_user) && empty($tg_user_by_center)){
        $is_user = true;
        $html .= '<th width="3%" style="text-align: center;vertical-align: text-top;min-width: 100px;">User</th>';
    }

    if(!empty($tg_user_by_center)) {
        $is_user_by_center = true;
        $html .= '<th width="3%" style="text-align: center;vertical-align: text-top;min-width: 100px;">Center</th>';
        $html .= '<th width="3%" style="text-align: center;vertical-align: text-top;min-width: 100px;">User</th>';
    }

    switch ($_POST['tg_frequency']) {
        case "Daily":
            $dates = getDateOfMonth($_POST['tg_year'], $_POST['tg_unit_from']);
            foreach($dates as $date){
                $dateF  = date_create($date);
                $html .= '<td width="3%"><b>'.date_format($dateF,"d/m").'</b></td>';
            }
            break;
        case "Weekly":
            for($i = $_POST['tg_unit_from']; $i <= $_POST['tg_unit_to']; $i++){
                $parts = getStartEndFromWeek($i,$_POST['tg_year']);
                $html .= '<td width="3%"><b>W '.$i.'</b><br>'.$parts['week_start'].' - '.$parts['week_end'].'</td>';
            }
            break;
        case "Monthly":
            for($i = $_POST['tg_unit_from']; $i <= $_POST['tg_unit_to']; $i++){
                $dt = DateTime::createFromFormat('!m', $i);
                $html .= '<th width="3%" style="text-align: center;vertical-align: text-top;">'.$dt->format('F').' - '.$_POST['tg_year'].'</th>';
            }
            break;
        case "Quarterly":
            for($i = $_POST['tg_unit_from']; $i <= $_POST['tg_unit_to']; $i++){
                $parts = getStartEndFromQuarter($i,$_POST['tg_year']);
                $html .= '<td width="3%" style="text-align: center;vertical-align: text-top;"><b>Q '.$i.'</b><br>'.$parts['quarter_start'].' - '.$parts['quarter_end'].'</td>';
            }
            break;
        case "Yearly":
            $html .= '<th width="3%" style="text-align: center;vertical-align: text-top;">Year '.$_POST['tg_year'].'</th>';
            break;
    }
    $html .= '</tr></thead>';
    $html .= '<tbody>';


    $count = 1;
    if($is_center){
        $data = getDataConfig(array_unique(array_column($tg_center,'team_id')), '', $_POST['tg_type'],$by_c, $_POST['tg_year'], $_POST['tg_frequency'], $_POST['tg_unit_from'], $_POST['tg_unit_to']);
        foreach($result as $ind_C => $center){
            $html .= generateRow($count, $ind_C, '', $center, $data);
            $count++;
        }
    }

    if($is_user){
        $data = getDataConfig('', array_unique(array_column($tg_user,'primaryid')), $_POST['tg_type'],$by_c, $_POST['tg_year'], $_POST['tg_frequency'], $_POST['tg_unit_from'], $_POST['tg_unit_to']);
        foreach($result as $ind_C => $center)
            foreach($center as $ind_U => $user){
                $html .= generateRow($count, '', $ind_U, $user, $data);
                $count++;
        }
    }

    if($is_user_by_center){
        $data = getDataConfig(array_unique(array_column($tg_user_by_center,'team_id')), array_unique(array_column($tg_user_by_center,'primaryid')), $_POST['tg_type'],$by_c, $_POST['tg_year'], $_POST['tg_frequency'], $_POST['tg_unit_from'], $_POST['tg_unit_to']);
        foreach($result as $ind_C => $center) {
            $invalid_users = array();
            $team = BeanFactory::getBean('Teams', $ind_C);
            foreach ($center as $ind_U => $user) {
                $teams_for_user = $team->get_teams_for_user($user['primaryid']);
                if (in_array($ind_C, array_keys($teams_for_user)))
                    $html .= generateRow($count, $ind_C, $ind_U, $user, $data);
                else
                    $invalid_users[] = $user['full_user_name'];
                $count++;
            }
            if (!empty($invalid_users))
                $invalid_input[$team->name] = implode(', ', $invalid_users);
        }
    }

    $html .= '</tbody></table>
    <input class="button primary" type="button" name="tg_saveconfig" value="Save Config" id="tg_saveconfig" style="padding: 6px 10px 6px 10px; margin:15px;">
    <input class="button" type="button" name="tg_clearconfig" value="Clear" id="tg_clearconfig" style="padding: 6px 10px 6px 10px; margin:15px;">
    ';
    $js   = "
    <script>
    $(document).ready(function() {
    var table = $('#celebs');
    var oTable = table.dataTable({ 'fnFooterCallback': function( nFoot, aData, iStart, iEnd, aiDisplay ) { }, bStateSave: true, aLengthMenu: [
    [-1],
    ['All']
    ], iDisplayLength: -1, 'bPaginate': false, 'bSort' : false, ordering: false
    });
    });
    </script>";
    return json_encode(array(
        "success" => "1",
        "html" => $html.$js,
        "invalidInput" => $invalid_input ? : '',
    ));
}

function saveConfig(){
    global $timedate;
    $data   = getDataConfig(array_unique($_POST['team_id']),array_unique($_POST['user_id']), $_POST['tg_type'],$_POST['tg_by'], $_POST['tg_year'], $_POST['tg_frequency'], $_POST['tg_unit_from'], $_POST['tg_unit_to'], 'roaming');
    if(!empty($data)){
        $ss_rmv = implode("','", array_column($data,'primaryid'));
        $GLOBALS['db']->query("DELETE FROM j_targetconfig WHERE id IN ('$ss_rmv')");
    }

    foreach($_POST['row'] as $rowNum => $rowVal ){
        foreach($_POST['value'.$rowVal] as $key => $value){
            if(!empty($value)){
                $tg             = new J_Targetconfig();
                $tg->type       = $_POST['tg_type'];
                $tg->year       = $_POST['tg_year'];
                $tg->frequency  = $_POST['tg_frequency'];
                $tg->time_unit  = $_POST['time_unit'.$rowVal][$key];
                $tg->value      = $value;
                $tg->by_c       = $_POST['tg_by'];
                if (!empty($_POST['team_id'][$rowNum])) $tg->tg_team_id = $_POST['team_id'][$rowNum];
                if (!empty($_POST['user_id'][$rowNum])) $tg->tg_user_id = $_POST['user_id'][$rowNum];
                $tg->name       = 'manual_save';
                $tg->save();
            }
        }
    }

    return json_encode(array("success" => "1"));
}

function getStartEndFromWeek($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['week_start'] = $dto->format('j/n');
    $dto->modify('+6 days');
    $ret['week_end'] = $dto->format('j/n');
    return $ret;
}

function getStartEndFromQuarter($quaty, $year) {
    $ret['quarter_start'] =  date('j/n', strtotime(date($year) . '-' . (($quaty * 3) - 2) . '-1'));
    $ret['quarter_end'] = date('t/n', strtotime(date($year) . '-' . (($quaty * 3)) . '-1'));
    return $ret;
}

function getDateOfMonth($year, $month) {
    $arr = array();
    $month = sprintf('%02d', $month);
    $now     = strtotime(date('Y-m-01',strtotime("$year-$month-01")));
    $last    = strtotime(date('Y-m-t',strtotime("$year-$month-01")));

    while($now <= $last ) {
        $arr[] = date('Y-m-d', $now);
        $now = strtotime('+1 day', $now);
    }

    return $arr;
}

function getDataConfig($team = '', $user_id = '' , $type = '', $by_c = '', $year = '', $frequency = '', $unit_from = '', $unit_to = '', $return_type = 'checking'){
    if(!empty($user_id))
        $ext_User = "AND (tg_user_id IN ('".implode("','",$user_id)."'))";
    if(!empty($team))
        $ext_Team = "AND (tg_team_id IN ('".implode("','",$team)."'))";

    $tg_unit = getTgUnit($year, $unit_from, $unit_to);
    $ext_time = "AND ((j_targetconfig.time_unit >= '".$tg_unit['from']."')
    AND (j_targetconfig.time_unit <= '".$tg_unit['to']."'))";

    $q2 = "SELECT DISTINCT
    IFNULL(j_targetconfig.id, '') primaryid, IFNULL(j_targetconfig.tg_team_id, '') team_id,
    IFNULL(j_targetconfig.name, '') name, IFNULL(j_targetconfig.frequency, '') frequency,
    IFNULL(j_targetconfig.time_unit, '') time_unit, IFNULL(j_targetconfig.tg_user_id, '') user_id,
    IFNULL(j_targetconfig.by_c, '') by_c, IFNULL(j_targetconfig.type, '') type,
    j_targetconfig.value value, IFNULL(j_targetconfig.year, '') year
    FROM j_targetconfig
    WHERE j_targetconfig.deleted = 0
    AND (j_targetconfig.year = '$year')
    AND (j_targetconfig.type = '$type')
    AND (j_targetconfig.by_c = '$by_c')
    AND (j_targetconfig.frequency = '$frequency')
    $ext_time $ext_User $ext_Team
    ORDER BY team_id";

    if($return_type == 'checking'){
        $rs2 = $GLOBALS['db']->query($q2);
        $data = array();
        while($row = $GLOBALS['db']->fetchbyAssoc($rs2))
            $data[$row['team_id']][$row['user_id']][$type][$year][$frequency][$row['time_unit']] = $row['value'];
    }else
        $data = $GLOBALS['db']->fetchArray($q2);

    return $data;
}

function getTgUnit($year, $unit_from, $unit_to) {
    $tg_unit = array();
    switch($_POST['tg_frequency']) {
        case "Daily":
            $month = sprintf('%02d', $unit_from);
            $tg_unit['from'] = date('Y-m-01',strtotime("$year-$month-01"));
            $tg_unit['to']   = date('Y-m-t',strtotime("$year-$month-01"));
            break;
        case "Weekly":
            $dto_from = $dto_to = new DateTime();
            $dto_from->setISODate($year, $unit_from);
            $tg_unit['from'] = $dto_from->format('Y-m-d');
            $dto_to->setISODate($year, $unit_to);
            $dto_to->modify('+6 days');
            $tg_unit['to'] = $dto_to->format('Y-m-d');
            break;
        case "Monthly":
            $monthS = sprintf('%02d', $unit_from);
            $monthE = sprintf('%02d', $unit_to);
            $tg_unit['from'] = date('Y-m-01',strtotime("$year-$monthS-01"));
            $tg_unit['to'] = date('Y-m-t',strtotime("$year-$monthE-01"));
            break;
        case "Quarterly":
        case "Yearly":
            $tg_unit['from'] = date('Y-01-01', strtotime("$year-01-01"));
            $tg_unit['to']   = date('Y-12-31', strtotime("$year-12-31"));
            break;
    }
    return $tg_unit;
}

function generateRow($count, $team_id = '', $user_id = '', $row = '', $data = ''){
    global $app_list_strings;
    $style = 'style="font-size: 13px; list-style-type: none; cursor: pointer;"';
    $icon_copy = '<div class="dropdown" style="display: inline-block; vertical-align: middle;">
    <img class="dropdown-toggle" data-toggle="dropdown" src="index.php?entryPoint=getImage&themeName=default&imageName=id-ff-copy.png" title="Copy to all" style="width: 16px;cursor: pointer;padding-left: 3px;">
    <ul class="dropdown-menu" aria-labelledby="copy_options">
    <li '.$style.'><a value="copy_to_rows" class="btn_copy">Copy to rows</a></li>
    <li '.$style.'><a value="copy_to_cols" class="btn_copy">Copy to cols</a></li>
    <li '.$style.'><a value="copy_to_all" class="btn_copy">Copy to all</a></li></ul></div>';
    $html = "<tr><td>$count
    <input type='hidden' name='row[]' class='row' value='{$count}'>
    </td>";

    if(!empty($team_id)) $html .= "<td>{$row['team_name']} <input type='hidden' name='team_id[]' class='team_id' value='{$team_id}'></td>";
    if(!empty($user_id)) $html .= "<td>{$row['full_user_name']} <input type='hidden' name='user_id[]' class='user_id' value='{$user_id}'></td>";

    switch ($_POST['tg_frequency']) {
        case "Daily":
            $dates = getDateOfMonth($_POST['tg_year'], $_POST['tg_unit_from']);
            foreach($dates as $date){
                $value = $data[$team_id][$user_id][$_POST['tg_type']][$_POST['tg_year']][$_POST['tg_frequency']][$date];
                if(empty($value)) $value = '';
                $html .= generateCell($count, $date, $value, $icon_copy);
            }
            break;
        case "Weekly":
            for($i = $_POST['tg_unit_from']; $i <= $_POST['tg_unit_to']; $i++){
                $dto = new DateTime();
                $dto->setISODate($_POST['tg_year'], $i);
                $week_start = $dto->format('Y-m-d');
                $value = $data[$team_id][$user_id][$_POST['tg_type']][$_POST['tg_year']][$_POST['tg_frequency']][$week_start];
                if(empty($value)) $value = '';
                $html .= generateCell($count, $week_start, $value, $icon_copy);
            }
            break;
        case "Monthly":
            for($i = $_POST['tg_unit_from']; $i <= $_POST['tg_unit_to']; $i++){
                $month = sprintf('%02d', $i);
                $year = $_POST['tg_year'];
                $month_start     = date('Y-m-01',strtotime("$year-$month-01"));

                $value = $data[$team_id][$user_id][$_POST['tg_type']][$_POST['tg_year']][$_POST['tg_frequency']][$month_start];
                if(empty($value)) $value = '';
                $html .= generateCell($count, $month_start, $value, $icon_copy);
            }
            break;
        case "Quarterly":
            for($i = $_POST['tg_unit_from']; $i <= $_POST['tg_unit_to']; $i++){
                $quarter_start =  date('Y-m-d', strtotime(date($_POST['tg_year']) . '-' . (($i * 3) - 2) . '-1'));
                $value = $data[$team_id][$user_id][$_POST['tg_type']][$_POST['tg_year']][$_POST['tg_frequency']][$quarter_start];
                if(empty($value)) $value = '';
                $html .= generateCell($count, $quarter_start, $value, $icon_copy);
            }
            break;
        case "Yearly":
            if(empty($value)) $value = '';
            $year = $_POST['tg_year'];
            $year_start     = date('Y-01-01',strtotime("$year-01-01"));
            $value = $data[$team_id][$user_id][$_POST['tg_type']][$_POST['tg_year']][$_POST['tg_frequency']][$year_start];
            $html .= generateCell($count, $year_start, $value, $icon_copy);
            break;
    }
    $html .= '</tr>';
    return $html;
}

function generateCell($count, $time_unit, $value, $icon_copy) {
    global $app_list_strings;
    $data_type = explode("|", $_POST['tg_type']);
    if ($data_type[1] == 'enum') {
        $html_sel = get_select_options_with_id($app_list_strings[$data_type[2]], $value);
        $html = '<td nowrap>
        <select class="configVal"
        name="value'.$count.'[]"
        time_unit="'.$time_unit.'"
        class="'.$_POST['tg_frequency'].'">'.$html_sel.'</select>'.$icon_copy.'
        <input type="hidden" name="time_unit'.$count.'[]" class="time_unit" value="'.$time_unit.'"></td>';
    } elseif( $data_type[1] == 'timepicker'){
        $_part = explode("-",str_replace(' ', '', $value));
        $html = '<td nowrap>
        <p class="timeslot">
        <input class="time start ui-timepicker-input" type="text" style="width: 40px; text-align: center;" autocomplete="off" value="'.$_part[0].'">
        -
        <input class="time end ui-timepicker-input" type="text" style="width: 40px; text-align: center;" autocomplete="off" value="'.$_part[1].'">
        <input type="hidden" name="value'.$count.'[]" class="configVal" time_unit="'.$time_unit.'" class="'.$_POST['tg_frequency'].'" value="'.$value.'">
        <input type="hidden" name="time_unit'.$count.'[]" class="time_unit" value="'.$time_unit.'">
        '.$icon_copy.'</p></td>';
    }
    else {
        $html = '<td nowrap>
        <input name="value'.$count.'[]" time_unit="'.$time_unit.'" value="'.$value.'" class="'.$_POST['tg_frequency'].' configVal currency" style="text-align:center;" size="13" type="text">'.$icon_copy.'
        <input type="hidden" name="time_unit'.$count.'[]" class="time_unit" value="'.$time_unit.'"></td>';
    }
    return $html;
}

?>
