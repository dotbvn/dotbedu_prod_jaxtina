$(document).ready(function(){
    $('select').select2();
    $('#tg_refresh').live('click',function(){
        $('#tg_type, .tg_by, #tg_year, #tg_frequency, #tg_unit_from, #tg_unit_to').prop('disabled',false);
        var td_this = $('.tg_by').closest('td').next();
        td_this.find('select.tg_user').prop("disabled", false);
        td_this.find('select.tg_center').prop("disabled", false);
        var tr = $('.tg_by').closest('tr').next();
        tr.find('select.tg_user').prop("disabled", false);

        $('div#result_table').html('');
    });
    $('#tg_clearconfig').live('click',function(){
        $('.configVal').each(function(){
            $(this).val('');
            var pcls  = $(this).closest('p.timeslot');
            pcls.find('.start').val('');
            pcls.find('.end').val('');
        });
    });
    //Handle change timepicker
    let timeSlot = $('.timeslot');
    timeSlot.find('.configVal').live('change',function(){
        var val__ = $(this).val();
        var pcls  = $(this).closest('p.timeslot');
        if(val__ != ''){
            var arrVal = val__.replace(/\s/g, '').split('-');
            var sss = new Date("2015-03-25 " + arrVal[0]);
            var eee = new Date("2015-03-25 " + arrVal[1]);
            pcls.find('.start').timepicker('setTime', sss);
            pcls.find('.end').timepicker('setTime', eee);
            pcls.datepair(); // initialize datepair
        }else{
            pcls.find('.start').val('');
            pcls.find('.end').val('');
        }

    });
    timeSlot.find('.time').live('change',function(){
        var pcls  = $(this).closest('p');
        var start = pcls.find(".start").val();
        var end   = pcls.find(".end").val();
        if (start === '' && end === '')
            pcls.find('.configVal').val('');
        else if(start !== '' && end !== '') {
            var js_start = DOTB.util.DateUtils.parse(start, 'H:i');
            var js_end = DOTB.util.DateUtils.parse(end, 'H:i');
            var minutes = parseFloat((js_end - js_start) / 60000); //minutes
            if (minutes <= 0 || isNaN(minutes)) {
                toastr.error('Invalid range');
                return;
            }
            pcls.find('.configVal').val(start + '-' + end);
        }
    });

    $('td.by_opt').find('div').hide();

    $('.tg_by').live('change',function(){
        var ind = $(this).index();
        var td_this = $(this).closest('td').next();
        td_this.find('select.tg_center').val('').trigger("change");
        td_this.find('select.tg_user').val('').trigger("change");
        td_this.find('div').hide();

        var tr = $(this).closest('tr').next();
        tr.find('select.tg_user').val('').trigger("change");
        tr.find('div').hide();

        switch ($(this).val()){
            case 'center':
                td_this.find('select.tg_center').closest('div').show();
                td_this.find('select.tg_center').select2({placeholder: "Select Center", allowClear: true});
                break;
            case 'user':
                td_this.find('select.tg_user').closest('div').show();
                td_this.find('select.tg_user').select2({placeholder: "Select User", allowClear: true});
                break;
            case 'user_by_center':
                td_this.find('select.tg_center').closest('div').show();
                td_this.find('select.tg_center').select2({placeholder: "Select Center", allowClear: true});
                tr.find('select.tg_user').closest('div').show();
                tr.find('select.tg_user').select2({placeholder: "Select User", allowClear: true});
                break;
            default:
        }
    });

    $('#tg_saveconfig').live('click',function(){
        var configVal = {};

        var form = $("#TargetConfigForm");
        var nonFormValue = '&tg_type=' + $('#tg_type').val();
        nonFormValue += '&tg_by=' + $('#tg_by').val();
        nonFormValue += '&tg_year=' + $('#tg_year').val();
        nonFormValue += '&tg_frequency=' + $('#tg_frequency').val();
        nonFormValue += '&tg_unit_from=' + $('#tg_unit_from').val();
        nonFormValue += '&tg_unit_to=' + $('#tg_unit_to').val();

        DOTB.ajaxUI.showLoadingPanel();
        $.ajax({
            url: "index.php?module=J_Targetconfig&action=ajaxTargetConfig&dotb_body_only=true&type=saveConfig",
            type: "POST",
            async: true,
            data: form.serialize() + nonFormValue,
            dataType: "json",
            success: function(res){
                DOTB.ajaxUI.hideLoadingPanel();
                if(res.success == "1")
                    toastr.success('Target config saved successfully!');
                else
                    toastr.error('Something Wrong. Please, Try again!');
            },
        });
    });

    $('.btn_copy').live('click',function(){
        var copyOptions = $(this).attr('value');
        var type = $('#tg_type').val();
        var dataType = type.split('|')[1];
        var configVal = (dataType === 'enum') ? $('select.configVal') : $('input.configVal');
        var tdQ = $(this).closest('td');
        var indexQ = tdQ.index();
        var valueQ = tdQ.find(configVal).val();

        var selectedCells;
        switch(copyOptions) {
            case 'copy_to_rows':
                selectedCells = $(this).closest('td').nextAll('td');
                break;
            case 'copy_to_cols':
                selectedCells = $(this).closest('tbody tr').nextAll('tr').find('td:nth-child(' + (indexQ + 1) + ')');
                break;
            case 'copy_to_all':
                selectedCells = $(this).closest('tbody').find('td');
                break;
            default:
                return;
        }
        selectedCells.find('.start').val('');
        selectedCells.find('.end').val('');
        selectedCells.find('.configVal').val(valueQ).trigger('change');
    });

    $('#tg_loadconfig').live('click',function(){
        if(validateTargetConfig()){

            $('div#result_table').html('');
            //Disable Selects
            $('#tg_type, .tg_by, #tg_year, #tg_frequency, #tg_unit_from, #tg_unit_to').prop('disabled',true);
            var td_this = $('.tg_by').closest('td').next();
            td_this.find('select.tg_user').prop("disabled", true);
            td_this.find('select.tg_center').prop("disabled", true);
            var tr = $('.tg_by').closest('tr').next();
            tr.find('select.tg_user').prop("disabled", true);
            //Run Ajax
            ajaxStatus.showStatus('Loading');
            $.ajax({
                url: "index.php?module=J_Targetconfig&action=ajaxTargetConfig&dotb_body_only=true",
                type: "POST",
                async: true,
                data:  {
                    type            : 'loadConfig',
                    tg_type         : $('#tg_type').val(),
                    tg_by           : $("select.tg_by").map(function(){return $(this).val();}).get(),
                    tg_center       : $("select.tg_center").map(function(){return $(this).val();}).get(),
                    tg_user         : $("select.tg_user").map(function(){return $(this).val();}).get(),
                    tg_year         : $('#tg_year').val(),
                    tg_frequency    : $('#tg_frequency').val(),
                    tg_unit_from    : $('#tg_unit_from').val(),
                    tg_unit_to      : $('#tg_unit_to').val(),
                },
                dataType: "json",
                success: function(res){
                    if(res.success == "1"){

                        $('div#result_table').html(res.html);
                        setTimePicker();
                        if(res.invalidInput !== '') {
                            var title = "Unable to configure KPIs",
                            message = '',
                            invalidArr = Object.entries(res.invalidInput).map(([key, value]) => ({key,value}));
                            invalidArr.forEach(function(data, index) {
                                message += "<b>" + (index + 1) + ". " + data.key + ": </b>" + data.value + "</br></br>";
                            });
                            toastr.error(message, title, {
                                "closeButton": false,
                                "allowHtml": true,
                            });
                        }
                    }else{
                        toastr.error('Something Wrong. Please, Try again!');
                        $('#tg_type,.tg_by, #tg_year, #tg_frequency, #tg_unit_from, #tg_unit_to').prop('disabled',false);
                        td_this.find('select.tg_user').prop("disabled", false);
                        td_this.find('select.tg_center').prop("disabled", false);
                        tr.find('select.tg_user').prop("disabled", false);
                    }
                    ajaxStatus.hideStatus();
                },
            });
        }else{
            toastr.error('Missing required fields!');
        }
    });
    $('#tg_frequency, #tg_year').live('change',function(){
        var frequency   = $('#tg_frequency').val();
        var year        = $('#tg_year').val();
        var list        = '<option value="">-none-</option>';
        var months      = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        var tg_unit_from = $('#tg_unit_from');
        tg_unit_from.empty();

        var tg_unit_to = $('#tg_unit_to');
        tg_unit_to.empty();

        switch(frequency) {
            case 'Weekly':
                for (var j = 1; j <= weeksInYear(year); j++)
                    list += "<option value='" +j+ "'>W " +j+ "</option>";
                break;
            case 'Monthly':
                for (var j = 0; j < months.length; j++)
                    list += "<option value='" +(j+1)+ "'>" +months[j]+ "</option>";
                break;
            case 'Quarterly':
                for (var j = 1; j < 5; j++)
                    list += "<option value='" +j+ "'>Q " +j+ "</option>";
                break;
            case 'Yearly':
                list += "<option value='" +year+ "'>" +year+ "</option>";
                break;
            case 'Daily':
                for (var j = 0; j < months.length; j++)
                    list += "<option value='" +(j+1)+ "'>" +months[j]+ "</option>";
                break;
            default:
                list += "<option value='1'>1</option>";
        }
        tg_unit_from.html(list).effect("highlight", {color: '#ff9933'}, 1000);
        tg_unit_to.html(list);

        $('#tg_unit_to, #tg_label_unit_to').show();
        $('#tg_unit_to').next(".select2-container").show();
        switch(frequency) {
            case 'Weekly':
                var currentWeek = getWeekNumber(new Date())[1];
                $('#tg_unit_from').val(currentWeek);
                if(currentWeek + 6 > $('#tg_unit_to option:last').val())
                    $('#tg_unit_to option:last').prop('selected', true);
                else
                    $('#tg_unit_to').val(currentWeek + 6);
                break;
            case 'Monthly':
                var d = new Date();
                var currentMonth = d.getMonth() + 1;
                $('#tg_unit_from').val(currentMonth);
                if(currentMonth + 6 > $('#tg_unit_to option:last').val())
                    $('#tg_unit_to option:last').prop('selected', true);
                else
                    $('#tg_unit_to').val(currentMonth + 6);
                break;
            case 'Daily':
                var d = new Date();
                var currentMonth = d.getMonth() + 1;
                $('#tg_unit_from').val(currentMonth);
                $('#tg_unit_to').val(currentMonth).hide();
                $('#tg_unit_to').next(".select2-container").hide();
                $('#tg_label_unit_to').hide();
                break;
            case 'Quarterly':
            case 'Yearly':
            default:
                $('#tg_unit_from option:eq(1)').prop('selected', true);
                $('#tg_unit_to option:last').prop('selected', true);
                break;
        }
    });
    $('#tg_frequency').trigger('change');
});
function validateTargetConfig(){
    var validate_arr=  ['tg_type','tg_by','tg_year','tg_frequency','tg_unit_from','tg_unit_to'];
    this.count      = 0;
    var self        = this;
    $('.select2-selection').css('border-color', '');
    $.each(validate_arr, function(index, item) {
        if($('#'+item).val() == '' || $('#'+item).val() == undefined){
            self.count++;
            $('#'+item).next().find('.select2-selection').css('border-color', '#FF0000');
        }else{
            if($('#tg_unit_to').val() === parseInt($('#tg_unit_to').val(), 10))
                if(item == 'tg_unit_from' && $('#tg_unit_to').val() != '' && $('#'+item).val() > $('#tg_unit_to').val()){
                    self.count++;
                    $('#'+item+',  #tg_unit_to').next().find('.select2-selection').css('border-color', '#FF0000');
                    toastr.error('Invalid Time Unit!');
                }
        }
    });

    var selector;
    switch($('#tg_by').val()) {
        case 'center':
            selector = 'select.tg_center';
            break;
        case 'user':
            selector = 'select.tg_user';
            break;
        case 'user_by_center':
            selector = 'select.tg_user_by_center, select.tg_center';
            break;
        default:
    }
    if($(selector).val() === null) {
        this.count++;
        $(selector).next().find('.select2-selection').css('border-color', '#FF0000');
    }

    if(this.count > 0) return false;
    else return true;
}
function weeksInYear(year) {
    var month = 11, day = 31, week;

    // Find week that 31 Dec is in. If is first week, reduce date until
    // get previous week.
    do {
        d = new Date(year, month, day--);
        week = getWeekNumber(d)[1];
    } while (week == 1);

    return week;
}
function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0,0,0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay()||7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(),0,1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7)
    // Return array of year and week number
    return [d.getFullYear(), weekNo];
}

var getDaysInMonth = function(month,year) {
    // Here January is 1 based
    //Day 0 is the last day in the previous month
    return new Date(year, month, 0).getDate();
    // Here January is 0 based
    // return new Date(year, month+1, 0).getDate();
};
//Set time picker
function setTimePicker() {
    $('.timeslot').each(function () {
        var TimeDelta = 2*60*60000;  // 4hrs -> milliseconds
        var timeOnly = $(this);
        //Clear event
        timeOnly.unbind();
        timeOnly.find(".time").eq(0).timepicker({
            'minTime': '6:00am',
            'maxTime': '10:00pm',
            'showDuration': false,
            'timeFormat': 'H:i',
            'step': 60,
            'disableTextInput': false,
            'disableTouchKeyboard': false,
        });
        timeOnly.find(".time").eq(1).timepicker({
            'showDuration': true,
            'timeFormat': 'H:i',
            'step': 60,
            'disableTextInput': false,
            'disableTouchKeyboard': false,
            'showOn': ["click","focus"],
        });
        var settingDatePair = new Datepair(timeOnly[0], {
            'defaultTimeDelta': TimeDelta,
        });

        timeOnly.on('rangeSelected', function () {
            var pcls  = $(this).closest('p');
            var start = $(this).find(".start").val();
            var end   = $(this).find(".end").val();
            if(start != '' && end != ''){
                var js_start = DOTB.util.DateUtils.parse(start, 'H:i');
                var js_end   = DOTB.util.DateUtils.parse(end, 'H:i');
                var minutes = parseFloat((js_end - js_start) / 60000); //minutes
                if(minutes <= 0 || isNaN(minutes)){
                    toastr.error('Invalid range');
                    return ;
                }
                pcls.find('.configVal').val(start+'-'+end);
            }
        }).on('rangeError', function(){
            toastr.error('Invalid range');
            return ;
        });
    });
}