<?php
    function loadTargetConfig(){
        global $current_user, $app_list_strings, $timedate;
        include_once("custom/include/_helper/junior_gradebook_utils.php");
        $ss = new Dotb_Smarty();

        $qr = "";
        if(!$current_user->isAdmin()) {
            $sql_get_my_team = "SELECT DISTINCT
            rel.team_id FROM team_memberships rel
            RIGHT JOIN teams ON (rel.team_id = teams.id)
            WHERE rel.user_id = '".$current_user->id."' AND teams.private = 0
            AND rel.deleted = 0 AND teams.deleted = 0";
            $result = $GLOBALS['db']->query($sql_get_my_team);
            $teamIds = array();
            while($row = $GLOBALS['db']->fetchByAssoc($result))
                $teamIds[] = $row['team_id'] ;
            $qr = " AND t1.id IN ('".implode("','", $teamIds)."') ";
        }


        $sql_get_team = "SELECT DISTINCT IFNULL(t1.id, '') id, IFNULL(t1.name, '') name, IFNULL(t1.code_prefix, '') center_code
        FROM teams t1
        INNER JOIN teams t2 ON t1.parent_id = t2.id AND t2.deleted = 0
        $qr AND t1.deleted = 0 AND t1.id NOT IN (SELECT DISTINCT tt.parent_id FROM teams tt WHERE tt.private = 0 AND tt.deleted = 0 AND (tt.parent_id <> '' AND tt.parent_id IS NOT NULL))
        ORDER BY t1.name";
        $result = $GLOBALS['db']->query($sql_get_team);

        $html       = "";
        $teams      = array();
        while($row = $GLOBALS['db']->fetchByAssoc($result)) {
            $html .= '<option value="'.$row["id"].'">'.$row["name"].'</option>';
            $teams[] = $row['id'];
        }

        // user
        $users = $GLOBALS['db']->fetchArray("SELECT DISTINCT
            IFNULL(users.id, '') primaryid,
            IFNULL(users.full_user_name, '') full_user_name,
            IFNULL(users.user_name, '') user_name,
            IFNULL(users.title, '') title,
            IFNULL(l1.id, '') team_id,
            IFNULL(l1.name, '') team_name
            FROM users
            INNER JOIN teams l1 ON users.default_team = l1.id AND l1.deleted = 0
            WHERE users.deleted = 0
        ORDER BY team_name ASC");
        $temTeam = "###";
        $htm_u = $htm_uc = "";
        foreach($users as $u){
            if($temTeam != $u['team_name']){
                if($temTeam != '###') $htm_uc  .= '</optgroup>';
                $temTeam = $u['team_name'];
            }
            $htm_u .= '<option value="'.$u['primaryid'].'">'.$u['full_user_name'].' '.(!empty($u['title']) ? "({$u['title']})" : '').'</option>';
        }


        $ss->assign("MOD", $GLOBALS['mod_strings']);
        $ss->assign("option_center", "".$html);
        $ss->assign("option_user", $htm_u);


         //Type
        $htm_ = '';
        foreach($app_list_strings['target_config_list'] as $list_key => $label){
            $htm_ .= '<optgroup label="'.$label.'">';
            $htm_ .= get_select_options_with_id($app_list_strings[$list_key],'');
            $htm_ .= '</optgroup>';
        }
        $ss->assign("option_type", $htm_);
        $ss->assign("option_by", get_select_options_with_id($app_list_strings['targetby_list'],''));
        $ss->assign("option_year", get_select_options_with_id($app_list_strings['year_list'],date('Y')));
        $ss->assign("option_frequency", get_select_options_with_id($app_list_strings['frequency_targetconfig_list'],'Monthly'));
        $ss->assign("option_unit_from", get_select_options_with_id(['','1'],''));
        $ss->assign("option_unit_to", get_select_options_with_id(['','1'],''));
        return $ss->fetch('custom/modules/J_Targetconfig/tpls/targetConfig.tpl');
    }
echo  loadTargetConfig();