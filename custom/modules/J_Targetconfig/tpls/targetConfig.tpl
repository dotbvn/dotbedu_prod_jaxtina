{dotb_getscript file="custom/modules/J_Targetconfig/js/tgconfigs.js"}
{dotb_getscript file="custom/include/javascript/Bootstrap/bootstrap.min.js"}
{dotb_getscript file="custom/include/javascript/DataTables/js/dataTables.bootstrap.js"}
{dotb_getscript file="custom/include/javascript/DataTables/js/dataTables.fixedColumns.min.js"}
{dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
{dotb_getscript file="custom/include/javascript/Timepicker/js/jquery.timepicker.min.js"}
{dotb_getscript file="custom/include/javascript/Timepicker/js/datepair.min.js"}

<link rel='stylesheet' type="text/css" href='{dotb_getjspath file="custom/include/javascript/Timepicker/css/jquery.timepicker.css"}'/>
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/DataTables/css/jquery.dataTables.min.css'}">
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/Bootstrap/bootstrap.min.css'}">
<link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>

{literal}
<style type="text/css">
.timeslot{display: inline;}
</style>
{/literal}

<form action="" method="POST" name="TargetConfigForm" id="TargetConfigForm">
    <div class="container">
        <div class="page-header">
            <h1>{$MOD.LBL_MODULE_NAME}</h1>
        </div>
        <table class="table_config table-condensed" width="100%">
            <tbody>
                <tr>
                    <td width="15%" nowrap>
                        <b>{$MOD.LBL_TYPE}: <span class="required">*</span> </b>
                    </td>
                    <td width="35%" nowrap>
                        <select id="tg_type" name="tg_type" style="width: 80%">{$option_type}</select>
                    </td>
                    <td width="15%" nowrap>
                        <b>{$MOD.LBL_BY}: <span class="required">*</span> </b>
                    </td>
                    <td width="15%" nowrap>
                        <select id="tg_by" class="tg_by" name="tg_by[]" style="width: 80%">{$option_by}</select>
                    </td>
                    <td class="by_opt" width="20%" nowrap>
                        <div><select class="tg_center" multiple="" name="tg_center[]" style="width: 100%">{$option_center}</select></div>
                        <div><select class="tg_user" multiple="" name="tg_user[]" style="width: 100%">{$option_user}</select></div>
                    </td>
                </tr>

                <tr>
                    <td width="15%" nowrap>
                        <b>{$MOD.LBL_YEAR}: <span class="required">*</span> </b></span>
                    </td>
                    <td width="35%" colspan='1' nowrap>
                    <select id="tg_year" name="tg_year" style="width: 100px;">{$option_year}</select>
                    </td>
                    <td></td>
                    <td class="by_opt" colspan="2" nowrap>
                        <div><select class="tg_user tg_user_by_center" multiple="" name="tg_user[]" style="width: 100%">{$option_user}</select></div>
                    </td>
                </tr>
                <tr>
                    <td width="15%" nowrap>
                        <b>{$MOD.LBL_FREQUENCY}: <span class="required">*</span> </b>
                    </td>
                    <td width='35%' nowrap>
                        <select id="tg_frequency" name="tg_frequency" style="width: 100px;">{$option_frequency}</select>
                    </td>
                    <td width = "15%" nowrap>
                        <b>{$MOD.LBL_TIME_UNIT}: <span class="required">*</span> </b>
                    </td>
                    <td width='35%' nowrap colspan="2">
                        <select id="tg_unit_from" name="tg_unit_from" style="width: 25%">{$option_unit_from}</select>
                        <span id ="tg_label_unit_to"> - To - </span>
                        <select id="tg_unit_to" name="tg_unit_to" style="width: 25%">{$option_unit_to}</select>
                    </td>
                </tr>
                <tr>
                    <td nowrap colspan='2' align='right'>
                    <input class="button primary" type="button" name="tg_loadconfig" value="Load Config" id="tg_loadconfig" style="padding: 6px 10px 6px 10px;"></td>
                    <td nowrap colspan='2' align='left'>
                    <input class="button" type="button" name="tg_refresh" value="Refresh" id="tg_refresh" style="padding: 6px 10px 6px 10px;">
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <div id="result_table" style="text-align: center;"></div>
    </div>
</form>
</div>

