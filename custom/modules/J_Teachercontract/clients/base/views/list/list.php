<?php
$module_name = 'J_Teachercontract';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'enabled' => true,
                'default' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'c_teachers_j_teachercontract_1_name',
                'label' => 'LBL_C_TEACHERS_J_TEACHERCONTRACT_1_FROM_C_TEACHERS_TITLE',
                'enabled' => true,
                'id' => 'C_TEACHERS_J_TEACHERCONTRACT_1C_TEACHERS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'contract_type',
                'label' => 'LBL_CONTRACT_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'contract_date',
                'label' => 'LBL_CONTRACT_DATE',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'contract_until',
                'label' => 'LBL_CONTRACT_UNTIL',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'day_off',
                'label' => 'LBL_DAY_OFF',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              8 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              9 => 
              array (
                'name' => 'less_non_working_hours',
                'label' => 'LBL_LESS_NON_WORKING_HOURS',
                'enabled' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'teach_hours',
                'label' => 'LBL_TEACH_HOURS',
                'enabled' => true,
                'default' => false,
              ),
              11 => 
              array (
                'name' => 'working_hours_monthly',
                'label' => 'LBL_WORKING_HOURS_MONTHLY',
                'enabled' => true,
                'default' => false,
              ),
              12 => 
              array (
                'name' => 'admin_hours',
                'label' => 'LBL_ADMIN_HOURS',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
