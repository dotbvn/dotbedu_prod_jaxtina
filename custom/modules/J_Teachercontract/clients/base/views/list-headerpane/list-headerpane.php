<?php


$viewdefs['J_Teachercontract']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_TEACHER_CONTRACT',
			'tooltip' => 'LNK_NEW_J_TEACHER_CONTRACT',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_Teachercontract',
            'route'=>'#J_Teachercontract/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_TEACHER_CONTRACT_REPORTS',
			'tooltip' => 'LNK_J_TEACHER_CONTRACT_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_Teachercontract',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);