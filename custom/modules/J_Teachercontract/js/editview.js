function set_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'c_teachers_j_teachercontract_1_name':
                    window.document.forms[form_name].elements['c_teachers_j_teachercontract_1_name'].value = val;
                    break;
                case 'c_teachers_j_teachercontract_1c_teachers_ida':
                    window.document.forms[form_name].elements['c_teachers_j_teachercontract_1c_teachers_ida'].value = val;
                    break;
            }
        }
    }
}