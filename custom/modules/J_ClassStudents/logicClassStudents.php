<?php

require_once('custom/include/_helper/junior_schedule.php');
require_once("custom/include/utils/lms_helpers.php");
require_once('include/externalAPI/ClassIn/utils.php');
//require_once('include/externalAPI/ClassIn/ExtAPIClassIn.php');

if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
//Sử dụng Bean cho Module này: Mục đích để Kích hoạt API Class In/Canvas...

class logicClassStudents
{

    function handle_save(&$bean, $event, $arguments){
        //HANDLE AUTO-DELAY
        if(in_array($bean->process_action, ['Remove', 'Delay'])){
            $res = handleDelay($bean->student_id, $bean->student_type, $bean->class_id, $bean->delay_from_date, '', $bean->run_time, $bean->description, $bean->process_action, 'revert_delay', $bean->dl_reason_for);
            //DONE - Refresh
            if($res['success']) $bean->name = 'HANDLE AUTO-DELAY: SUCCESS';
            else $bean->name = 'HANDLE AUTO-DELAY: FAIL';
            $GLOBALS['db']->query("UPDATE j_classstudents SET process_action='', name='{$bean->name}' WHERE id='{$bean->id}'");
        }else{
            //LMS-Canvas
            $class   = BeanFactory::getBean('J_Class', $bean->class_id);
            $student = BeanFactory::getBean($bean->student_type, $bean->student_id);
            if($bean->student_type == 'Contacts' && !empty($student->id)){
                if(!empty( $student->lms_user_id) && !empty( $class->lms_course_id)){
                    $result = enrollUser($student->lms_user_id, $class->lms_course_id);
                    if($result['status'] == 'success') $err_id = $result['enrollment_id'];
                }elseif(empty($student->lms_user_id) && !empty($class->lms_course_id)){
                    //Create User
                    $result = createUser($student);
                    if($result['status'] == 'success'){
                        //Update to DB
                        $lms_content = json_encode($result['user']);
                        $lms_user_id = $result['user_id'];
                        $GLOBALS['db']->query("UPDATE contacts SET
                            lms_user_id = '$lms_user_id',
                            lms_content = '$lms_content',
                            lms_user_name = '{$result['user_name']}',
                            lms_user_password = '{$result['user_password']}' WHERE id='{$student->id}'");
                        //Enroll to LMS
                        $result2 = enrollUser($lms_user_id, $class->lms_course_id);
                        if($result2['status'] == 'success') $err_id = $result2['enrollment_id'];
                    }
                }
                if(!empty($err_id)) $GLOBALS['db']->query("UPDATE j_classstudents SET lms_enrollment_id = '$err_id' WHERE student_id = '{$student->id}' AND class_id = '{$class->id}' AND deleted = 0 ");
            }
            //END: LMS: Canvas

            //Online Learning: Class In - Tạo mới record mới add học viên
            if (empty($bean->fetched_row) && ExtAPIClassIn::hasAPIConfig() && getClassActiveClassInStatus($bean->class_id)) {
                // Thêm học viên vào course
                $student->classin_nickname = formatCINickName($student);
                addStudentToClassInCourse($bean->class_id, $student);
            }
            //END: Online Learning: Class In

            //Add Student to Gradebook
            if($bean->student_type == 'Contacts' && !empty($student->id)){
                $gbList = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(l2.id, '') class_id,
                    IFNULL(l3.id, '') gradebook_id, IFNULL(l4.id, '') std_in_gb
                    FROM j_class l2 INNER JOIN j_class_j_gradebook_1_c l3_1 ON l2.id = l3_1.j_class_j_gradebook_1j_class_ida AND l3_1.deleted = 0
                    INNER JOIN j_gradebook l3 ON l3.id = l3_1.j_class_j_gradebook_1j_gradebook_idb AND l3.deleted = 0
                    LEFT JOIN j_gradebook_contacts_1_c l4_1 ON l3.id = l4_1.j_gradebook_contacts_1j_gradebook_ida
                    AND l4_1.j_gradebook_contacts_1contacts_idb = '{$student->id}' AND l4_1.deleted = 0
                    LEFT JOIN contacts l4 ON l4.id = l4_1.j_gradebook_contacts_1contacts_idb AND l4.deleted = 0
                    WHERE (l2.id = '{$bean->class_id}') AND l2.deleted = 0");
                $student->load_relationship('j_gradebook_contacts_1');
                foreach($gbList as $key => $rgb){if(empty($rgb['std_in_gb'])) $student->j_gradebook_contacts_1->add($rgb['gradebook_id']);}
            }//END: Add Student to Gradebook
        }
    }

    function before_delete(&$bean, $event, $arguments){
        $_POST['CS_class_id']      = $bean->class_id;
        $_POST['CS_student_type']  = $bean->student_type;
        $_POST['CS_student_id']    = $bean->student_id;
    }

    function handle_delete(&$bean, $event, $arguments)
    {
        //LMS: Canvas
        if(!empty($bean->lms_enrollment_id)){
            $class   = BeanFactory::getBean('J_Class', $_POST['CS_class_id']);
            $result = deleteEnroll( $class->lms_course_id, $bean->lms_enrollment_id);
        }
        //END: LMS: Canvas

        //Online Learning: Class In
        if (ExtAPIClassIn::hasAPIConfig() && getClassActiveClassInStatus($_POST['CS_class_id'])) {
            removeStudentFromClassInCourse($_POST['CS_class_id'], $_POST['CS_student_type'], $_POST['CS_student_id']);
        }
        //END: Online Learning: Class In

//        //Remove from Gradebook - Lỗi: random remove học viên khỏi lớp
//        if($_POST['CS_student_type'] == 'Contacts' && !empty($_POST['CS_student_id'])){
//            $student = BeanFactory::getBean($_POST['CS_student_type'], $_POST['CS_student_id']);
//            $gbList = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(l2.id, '') class_id,
//                IFNULL(l3.id, '') gradebook_id, IFNULL(l4.id, '') std_in_gb
//                FROM j_class l2 INNER JOIN j_class_j_gradebook_1_c l3_1 ON l2.id = l3_1.j_class_j_gradebook_1j_class_ida AND l3_1.deleted = 0
//                INNER JOIN j_gradebook l3 ON l3.id = l3_1.j_class_j_gradebook_1j_gradebook_idb AND l3.deleted = 0
//                LEFT JOIN j_gradebook_contacts_1_c l4_1 ON l3.id = l4_1.j_gradebook_contacts_1j_gradebook_ida
//                AND l4_1.j_gradebook_contacts_1contacts_idb = '{$_POST['CS_student_id']}' AND l4_1.deleted = 0
//                LEFT JOIN contacts l4 ON l4.id = l4_1.j_gradebook_contacts_1contacts_idb AND l4.deleted = 0
//                WHERE (l2.id = '{$_POST['CS_class_id']}') AND l2.deleted = 0");
//            $student->load_relationship('j_gradebook_contacts_1');
//            foreach($gbList as $key => $rgb){if(!empty($rgb['std_in_gb'])) $student->j_gradebook_contacts_1->delete($rgb['gradebook_id']);}
//        }//END: Remove from Gradebook
    }
}

?>
