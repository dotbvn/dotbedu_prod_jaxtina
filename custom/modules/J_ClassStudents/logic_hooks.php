<?php
    // Do not store anything in this file that is not part of the array or the hook version.  This file will
    // be automatically rebuilt in the future.
    $hook_version = 1;
    $hook_array = Array();

    $hook_array['before_delete']   = Array();
    $hook_array['before_delete'][] = Array(1, 'Handle Class Student Relationship', 'custom/modules/J_ClassStudents/logicClassStudents.php','logicClassStudents', 'before_delete');

    $hook_array['after_delete'] = Array();
    $hook_array['after_delete'][] = Array(1, 'Handle Class Student Relationship', 'custom/modules/J_ClassStudents/logicClassStudents.php','logicClassStudents', 'handle_delete');

    $hook_array['after_save'] = Array();
    $hook_array['after_save'][] = Array(1, 'Handle Class Student Relationship', 'custom/modules/J_ClassStudents/logicClassStudents.php','logicClassStudents', 'handle_save');
