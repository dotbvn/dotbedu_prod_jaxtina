<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');



class J_DiscountViewDetail extends ViewDetail {


    function display() {
        //Get Hour Discount
        if($this->bean->type == 'Hour'){
            $html = '<table id="discount_by_hour" style="width: 40%;float:left;" border="1" class="list view">
            <thead>
            <tr>
            <th width="50%" style="text-align: center;">Tuition Hours</th>
            <th width="50%" style="text-align: center;">Promotion Hours</th>
            </tr>
            </thead>
            <tbody id="tbodydiscount_by_hour">
            ';
            $content = json_decode(htmlspecialchars_decode($this->bean->content), true);
            foreach($content['discount_by_hour'] as $key => $value)
                $html .= "<tr><td style='text-align: center;'>{$value['hours']}</td><td style='text-align: center;'>{$value['promotion_hours']}</td></tr>";
            //add Discount by Block
            $html .= '</tbody></table>';
        }
        $this->ss->assign('CONTENT_2', $html);
        parent::display();
    }
}
?>