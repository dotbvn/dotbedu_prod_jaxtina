<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class J_DiscountViewEdit extends ViewEdit{
    var $useModuleQuickCreateTemplate = true;
    var $useForSubpanel = true;

    public function display(){
        $arrayNotApplyWith = array();
        $dih = getHtmlAddRowHour('','',true);
        //add Discount by Hour
        if(!empty($this->bean->id)){
            $hourObj = json_decode(html_entity_decode($this->bean->content), true);
            if(!empty($hourObj['discount_by_hour'])){
                foreach ($hourObj['discount_by_hour'] as $key => $hour){
                    $dih .= getHtmlAddRowHour($hour['hours'],$hour['promotion_hours'],false);
                    $count++;
                }
            }
        }
        if(!$count) $dih .= getHtmlAddRowHour('','',false);
        $this->ss->assign('DIH', $dih);
        parent::display();
    }
}

// Generate Add row template
function getHtmlAddRowHour( $hour, $promtion_hour, $showing){
    if($showing) $display = 'style="display:none;"';
    $tpl_addrow  = "<tr class='row' $display >";
    $tpl_addrow .= '<td nowrap align="center"><div><input tabindex="0" autocomplete="off" type="text" name="hours[]" class="hours" value="'.$hour.'" size="4" maxlength="10" style="text-align: center;"></div></td>';
    $tpl_addrow .= '<td nowrap align="center"><div><input tabindex="0" autocomplete="off" type="text" name="promotion_hours[]" class="promotion_hours" value="'.$promtion_hour.'" size="4" maxlength="10" style="text-align: center;color: rgb(165, 42, 42);"></div></td>';
    $tpl_addrow .= "<td align='center'><button type='button' class='btnRemoveHour' style='display: inline-block;'><img src='themes/default/images/id-ff-remove-nobg.png' alt='Remove'></button></td>";
    $tpl_addrow .= '</tr>';
    return $tpl_addrow;
}