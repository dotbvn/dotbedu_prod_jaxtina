<?php
// created: 2022-05-31 15:19:32
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '25%',
        'default' => true,
    ),
    'status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_STATUS',
        'width' => 10,
    ),
    'type' =>
    array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_TYPE',
        'width' => 10,
    ),
    'policy' =>
    array (
        'type' => 'text',
        'studio' => 'visible',
        'vname' => 'LBL_POLICY',
        'sortable' => false,
        'width' => '15%',
        'default' => true,
    ),
    'description' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_DESCRIPTION',
        'sortable' => false,
        'width' => '15%',
        'default' => true,
    ),
    'date_modified' =>
    array (
        'type' => 'datetime',
        'studio' =>
        array (
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname' => 'LBL_DATE_MODIFIED',
        'width' => 10,
        'default' => true,
    ),
    'team_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'studio' =>
        array (
            'portallistview' => false,
            'portalrecordview' => false,
        ),
        'vname' => 'LBL_TEAMS',
        'id' => 'TEAM_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Teams',
        'target_record_key' => 'team_id',
    ),
    'remove_button'=>array(
        'vname' => 'LBL_REMOVE',
        'widget_class' => 'SubPanelRemoveButton',
        'module' => 'J_Discount',
        'width' => '5%',
    ),
);