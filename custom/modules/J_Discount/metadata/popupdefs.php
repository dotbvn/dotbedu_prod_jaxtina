<?php
$popupMeta = array (
    'moduleMain' => 'J_Discount',
    'varName' => 'J_Discount',
    'orderBy' => 'j_discount.name',
    'whereClauses' => array (
        'name' => 'j_discount.name',
        'assigned_user_id' => 'j_discount.assigned_user_id',
        'status' => 'j_discount.status',
        'team_name' => 'j_discount.team_name',
        'favorites_only' => 'j_discount.favorites_only',
    ),
    'searchInputs' => array (
        1 => 'name',
        3 => 'status',
        4 => 'assigned_user_id',
        5 => 'team_name',
        6 => 'favorites_only',
    ),
    'searchdefs' => array (
        'name' =>
        array (
            'name' => 'name',
            'width' => 10,
        ),
        'assigned_user_id' =>
        array (
            'name' => 'assigned_user_id',
            'label' => 'LBL_ASSIGNED_TO',
            'type' => 'enum',
            'function' =>
            array (
                'name' => 'get_user_array',
                'params' =>
                array (
                    0 => false,
                ),
            ),
            'width' => 10,
        ),
        'status' =>
        array (
            'type' => 'enum',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
            'width' => 10,
            'name' => 'status',
        ),
        'team_name' =>
        array (
            'type' => 'relate',
            'link' => true,
            'studio' =>
            array (
                'portallistview' => false,
                'portalrecordview' => false,
            ),
            'label' => 'LBL_TEAMS',
            'id' => 'TEAM_ID',
            'width' => 10,
            'name' => 'team_name',
        ),
        'favorites_only' =>
        array (
            'name' => 'favorites_only',
            'label' => 'LBL_FAVORITES_FILTER',
            'type' => 'bool',
            'width' => 10,
        ),
    ),
    'listviewdefs' => array (
        'NAME' =>
        array (
            'width' => '30%',
            'label' => 'LBL_NAME',
            'default' => true,
            'link' => true,
            'name' => 'name',
        ),
        'STATUS' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
            'width' => 10,
            'name' => 'status',
        ),
        'TYPE' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
            'width' => 10,
            'name' => 'type',
        ),
        'CATEGORY' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'label' => 'LBL_CATEGORY',
            'width' => 10,
            'name' => 'category',
        ),
        'TEAM_NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_TEAM',
            'default' => true,
            'name' => 'team_name',
        ),
        'DATE_MODIFIED' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_MODIFIED',
            'width' => 10,
            'default' => true,
            'name' => 'date_modified',
        ),
    ),
);
