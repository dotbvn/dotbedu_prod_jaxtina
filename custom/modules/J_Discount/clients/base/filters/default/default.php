<?php
// created: 2022-06-15 11:47:49
$viewdefs['J_Discount']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' =>
  array (
    'name' =>
    array (
    ),
    'status' =>
    array (
    ),
    'type' =>
    array (
    ),
    'category' =>
    array (
    ),
    'discount_percent' =>
    array (
    ),
    'discount_amount' =>
    array (
    ),
    'date_modified' =>
    array (
    ),
    'date_entered' =>
    array (
    ),
    'assigned_user_name' =>
    array (
    ),
    'team_name' =>
    array (
    ),
    '$owner' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'created_by_name' =>
    array (
    ),
    'modified_by_name' =>
    array (
    ),
    'maximum_discount_percent' =>
    array (
    ),
    'policy' =>
    array (
    ),
    'order_no' =>
    array (
    ),
    'apply_with_payment_type' =>
    array (
    ),
    'is_auto_set' =>
    array (
    ),
    'is_ratio' =>
    array (
    ),
    'is_trade_discount' =>
    array (
    ),
    'is_chain_discount' =>
    array (
    ),
  ),
);