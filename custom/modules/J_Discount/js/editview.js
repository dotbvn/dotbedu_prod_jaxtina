var record_id       = $('input[name=record]').val();
var duplicateId       = $('input[name=duplicateId]').val();
$(document).ready(function(){
    $('.table-striped').selectableList();
    //add gift
    $('#tblLevelConfig').multifield({
        section :   '.row_tpl', // First element is template
        addTo   :   '#tbodylLevelConfig', // Append new section to position
        btnAdd  :   '#btnAddrow', // Button Add id
        btnRemove:  '.btnRemove', // Buton remove id,
    });
    //add Discount by Hour
    $('#tblHourConfig').multifield({
        section :   '.row', // First element is template
        addTo   :   '#tbodyHourConfig', // Append new section to position
        btnAdd  :   '#btnAddrowHour', // Button Add id
        btnRemove:  '.btnRemoveHour', // Buton remove id,
    });
    //add Discount by Block
    $('#tblBlockConfig').multifield({
        section :   '.row', // First element is template
        addTo   :   '#tbodyBlockConfig', // Append new section to position
        btnAdd  :   '#btnAddrowRange', // Button Add id
        btnRemove:  '.btnRemoveRange', // Buton remove id,
    });

    $('#type').live('change', function(){
        if($('#type').val()=='Gift'){
            $('#div_hour').css("display","none");
            $('#discount_percent, #discount_amount').prop('disabled',true);
            if(record_id == '' && duplicateId == '') $('#discount_percent, #discount_amount').val('');
        }
        if($('#type').val()=='Other'){
            $('#div_hour').css("display","none");
            $('#div_rewards').css("display","none");
            $('#discount_percent, #discount_amount').prop('disabled',false);
            if(record_id == '' && duplicateId == '') $('#discount_percent, #discount_amount').val('');
        }
        if($('#type').val()=='Hour'){
            $('#div_hour').css("display","");
            $('#discount_percent, #discount_amount').prop('disabled',true);
            if(record_id == '' && duplicateId == '') $('#discount_percent, #discount_amount').val('');
        }

    });
    $('#type').trigger('change');

});
// check require khi chọn type mà không chọn giá trị đi kèm
check_form = function(formname) {
    if (typeof (siw) != 'undefined' && siw && typeof (siw.selectingSomething) != 'undefined' && siw.selectingSomething)
        return false;

    type= $('#type').val();
    book_name=$('#book_name').val();
    qty_book=$('#qty_book').val();
    if(type=='Gift' && book_name==""){
        $('.required').remove(); //xoa dong thong bao loi
        add_error_style(formname,'book_name','Missing required field',true);
        return false;
    }
    if(type=='Gift' && qty_book==""){
        $('.required').remove(); //xoa dong thong bao loi
        add_error_style(formname,'qty_book','Missing required field',true);
        return false;
    }
    if( validate_form(formname, '')) {
        document.forms[formname].submit();
    }
}