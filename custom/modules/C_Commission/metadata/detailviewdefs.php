<?php
$module_name = 'C_Commission';
$viewdefs[$module_name] = 
array (
    'DetailView' => 
    array (
        'templateMeta' => 
        array (
            'form' => 
            array (
                'buttons' => 
                array (
                    0 => 'EDIT',
//                    1 => 'DUPLICATE',
                    2 => 'DELETE',
//                    3 => 'FIND_DUPLICATES',
                ),
            ),
            'maxColumns' => '2',
            'widths' => 
            array (
                0 => 
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 => 
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' => 
            array (
                'DEFAULT' => 
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' => 
        array (
            'default' => 
            array (
                0 => 
                array (
                    0 => 'name',
                    1 => 
                    array (
                        'name' => 'input_date',
                        'label' => 'LBL_INPUT_DATE',
                    ),
                ),
                1 => 
                array (
                    0 => 
                    array (
                        'name' => 'pmd_name',
                        'label' => 'LBL_PMD_NAME',
                    ),
                    1 => 
                    array (
                        'name' => 'type',
                        'label' => 'LBL_TYPE',
                    ),
                ),
                2 => 
                array (
                    0 => 
                    array (
                        'name' => 'payment_name',
                        'label' => 'LBL_PAYMENT_NAME',
                    ),
                    1 => 'assigned_user_name',
                ),
                3 => 
                array (
                    0 => 'team_name',
                    1 => 
                    array (
                        'name' => 'amount',
                        'label' => 'LBL_AMOUNT',
                    ),
                ),
                4 => 
                array (
                    0 => 'description',
                ),
                5 => array (
                    array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                    array (
                        'name' => 'date_modified',
                        'label' => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),

                ),
            ),
        ),
    ),
);
