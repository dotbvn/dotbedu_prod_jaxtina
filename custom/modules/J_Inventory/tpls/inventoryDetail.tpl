<table id="tblbook" width="100%" border="1">
<thead>
            <th width="20%" style="text-align: center;"><b>{$MOD.LBL_CODE}</b></th>
            <th width="40%" style="text-align: center;"><b>{$MOD.LBL_NAME}</b></th>
            <th width="10%" style="text-align: center;"><b>{$MOD.LBL_QUANTITY}</b></th>
            <th width="10%" style="text-align: center;"><b>{$MOD.LBL_UNIT}</b></th>
            <th width="5%" style="text-align: left;"><button class="button primary" type="button" id="btnAddrow"><b> + </b></button></th>
</thead>
<tbody id = 'tbodybook'>
            {$html_tpl}
</tbody>
<tfoot>
        <tr>
        <td align="center" style="vertical-align: middle;" colspan="2"><b>{$MOD.LBL_TOTAL_QUANTITY}</b></td>
        <td style="text-align: center;" colspan="1"><input size="5" type="text" readonly="true" class='currency input_readonly' name="total_quantity" id="total_quantity" value="{$fields.total_quantity.value}" style="text-align: center;width: 100%;"/></td>
        </tr>
</tfoot> 
</table>

</div>