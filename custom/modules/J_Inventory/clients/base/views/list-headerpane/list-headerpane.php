<?php


$viewdefs['J_Inventory']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_INVENTORY',
			'tooltip' => 'LNK_NEW_J_INVENTORY',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_Inventory',
            'route'=>'#J_Inventory/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_INVENTORY_REPORTS',
			'tooltip' => 'LNK_J_INVENTORY_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_Inventory',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);