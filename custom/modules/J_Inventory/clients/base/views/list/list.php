<?php
$module_name = 'J_Inventory';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'j_payment_j_inventory_1_name',
                'label' => 'LBL_J_PAYMENT_J_INVENTORY_1_FROM_J_PAYMENT_TITLE',
                'enabled' => true,
                'id' => 'J_PAYMENT_J_INVENTORY_1J_PAYMENT_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'student_name_text',
                'label' => 'LBL_STUDENT',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
              ),
              3 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'date_create',
                'label' => 'LBL_DATE_CREATE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'type',
                'label' => 'LBL_TYPE',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
                'width' => 'xsmall',
              ),
              6 => 
              array (
                'name' => 'total_quantity',
                'label' => 'LBL_TOTAL_QUANTITY',
                'enabled' => true,
                'default' => true,
                'width' => 'medium',
              ),
              7 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              8 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              9 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              10 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              11 => 
              array (
                'name' => 'student_id',
                'enabled' => true,
                'default' => false,
              ),
              12 => 
              array (
                'name' => 'lead_id',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'j_payment_j_inventory_1_id',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
