<?php


$viewdefs['J_Inventory']['base']['filter']['basic'] = array(
    'create'               => true,
    'quicksearch_field'    => array('name','j_payment_j_inventory_1_name', 'lead_name', 'student_name'),
    'quicksearch_priority' => 1,
    'quicksearch_split_terms' => false,
    'filters'              => array(
        array(
            'id'                => 'all_records',
            'name'              => 'LBL_LISTVIEW_FILTER_ALL',
            'filter_definition' => array(),
            'editable'          => false
        ),
        array(
            'id' => 'to_do_list1',
            'name' => 'Sách chưa giao: Unconfirmed ',
            'filter_definition' => array(
                array(
                    'status' => array(
                        '$in' => array('Unconfirmed'),
                    ),
                ),

            ),
            'editable' => true
        ),
        array(
            'id' => 'to_do_list2',
            'name' => 'Sách đã giao: Confirmed ',
            'filter_definition' => array(
                array(
                    'status' => array(
                        '$in' => array('Confirmed'),
                    ),
                ),

            ),
            'editable' => true
        ),
        array(
            'id'                => 'assigned_to_me',
            'name'              => 'LBL_ASSIGNED_TO_ME',
            'filter_definition' => array(
                '$owner' => '',
            ),
            'editable'          => false
        ),
        array(
            'id'                => 'favorites',
            'name'              => 'LBL_FAVORITES',
            'filter_definition' => array(
                '$favorite' => '',
            ),
            'editable'          => false
        ),
        array(
            'id'                => 'recently_viewed',
            'name'              => 'LBL_RECENTLY_VIEWED',
            'filter_definition' => array(
                '$tracker' => '-7 DAY',
            ),
            'editable'          => false
        ),
        array(
            'id'                => 'recently_created',
            'name'              => 'LBL_NEW_RECORDS',
            'filter_definition' => array(
                'date_entered' => array(
                    '$dateRange' => 'last_7_days',
                ),
            ),
            'editable'          => false
        ),
    ),
);
