<?php
// created: 2021-03-15 21:31:46
$viewdefs['J_Inventory']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'student_name' => 
    array (
    ),
    'lead_name' => 
    array (
    ),
    'j_payment_j_inventory_1_name' => 
    array (
    ),
    'amount_bef_discount' => 
    array (
    ),
    'j_class_j_inventory_1_name' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'total_amount' => 
    array (
    ),
    'total_quantity' => 
    array (
    ),
    'request_no' => 
    array (
    ),
    'description' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_create' => 
    array (
    ),
    'type' => 
    array (
    ),
    'status' => 
    array (
    ),
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);