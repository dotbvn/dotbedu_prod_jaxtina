<?php
$module_name = 'J_Inventory';
$viewdefs[$module_name] =
array (
    'QuickCreate' =>
    array (
        'templateMeta' =>
        array (
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array (
            'default' =>
            array (
                0 =>
                array (
                    0 => array (
                        'name' => 'name',
                        'customCode' => '<input type="text" class="input_readonly" name="request_noo" id="request_noo" maxlength="255" value="{$fields.name.value}" title="{$MOD.LBL_AUTO_GENENERATE}" readonly size="30">',
                    ),
                    1 =>''
                ),
                1 =>
                array (
                    0 =>'status',
                    1 =>''
                ),
                2 =>
                array (
                    0 =>'date_create',
                    1 =>''
                ),
                3 =>
                array (
                    0 =>'description',
                ),
            ),
        ),
    ),
);
