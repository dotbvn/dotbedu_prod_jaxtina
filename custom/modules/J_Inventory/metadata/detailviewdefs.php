<?php
$module_name = 'J_Inventory';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DELETE',
                    2 =>
                    array (
                        'customCode' => '{$EXPORT_DETAIL}',
                    ),
                ),
            ),
            'maxColumns' => '2',
            'javascript' => '{dotb_getscript file="custom/modules/J_Inventory/js/detail.js"}',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL2' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'name',
                    ),
                    1 =>
                    array (
                        'name' => 'type',
                        'studio' => 'visible',
                        'label' => 'LBL_TYPE',
                    ),
                ),
                1 =>
                array (
                    0 =>  'j_payment_j_inventory_1_name',
                    1 =>
                    array (
                        'name' => 'date_create',
                        'label' => 'LBL_DATE_CREATE',
                    ),
                ),
                2 =>
                array (
                    0 =>  'total_quantity',
                    1 => 'status' //'total_amount'
                ),
            ),
            'lbl_editview_panel2' =>
            array (
                0 =>
                array (
                    0 => 'description',
                    1 => 'import_type'
                ),
                1 =>
                array (
                    0 => 'assigned_user_name',
                    1 => 'team_name',
                ),
                1 =>
                array (
                    0 => 'assigned_user_name',
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
                2 =>
                array (
                    0 => 'team_name',
                    1 =>
                    array (
                        'name' => 'date_entered',
                        'customCode' => '
                        {$fields.date_entered.value} {$APP.LBL_BY}
                        {$fields.created_by_name.value}
                        ',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),
            ),
        ),
    ),
);
