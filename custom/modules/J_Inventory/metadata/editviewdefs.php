<?php
$module_name = 'J_Inventory';
$viewdefs[$module_name] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array ('form' =>
                array (
                    'hidden' =>
                    array (
                        1 => '<input type="hidden" name="is_admin" id="is_admin" value="{$is_admin}">',
                        2 => '<input type="hidden" name="payment_id" id="payment_id" value="{$payment_id}">',
                    ),
                ),
            'maxColumns' => '2',
            'javascript' => '
            {dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.min.js"}
            {dotb_getscript file="custom/modules/J_Inventory/js/edit.js"}',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL2' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'name',
                        'customCode' => '<input type="text" class="input_readonly" name="request_noo" id="request_noo" maxlength="255" value="{$fields.name.value}" title="{$MOD.LBL_AUTO_GENENERATE}" readonly size="30">',
                    ),
                    1 =>
                    array (
                        'name' => 'type',
                        'studio' => 'visible',
                        'label' => 'LBL_TYPE',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'status',
                        'label' => 'LBL_INVENTORYDETAIL',
                        'customCode' => '{include file="custom/modules/J_Inventory/tpls/inventoryDetail.tpl"}',
                    ),
                    1 =>
                    array (
                        'name' => 'date_create',
                        'label' => 'LBL_DATE_CREATE',
                    ),
                ),
                2 =>
                array (
                    0 =>  'import_type',
                    1 =>
                    array (
                        'name' => 'status',
                    ),
                ),
            ),
            'lbl_editview_panel2' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'description',
                        'displayParams' =>
                        array (
                            'rows' => 4,
                            'cols' => 60,
                        ),
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'assigned_user_name',
                        'displayParams' =>
                        array (
                            'required' => true,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'team_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldTeam.tpl"}',
                    ),
                ),
            ),
        ),
    ),
);
