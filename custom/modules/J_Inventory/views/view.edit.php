<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class J_InventoryViewEdit extends ViewEdit {
    public function display() {
        global $current_user;
        $this->ss->assign('is_admin',$current_user->isAdmin());
        $this->ss->assign('payment_id',$this->bean->j_payment_j_inventory_1j_payment_ida);
        if(empty($this->bean->id)){
            $this->bean->name = translate('LBL_AUTO_GENENERATE');
            //Book Manage
            $book_list  = getBookList();
            $html_tpl   = getHtmlAddRow($book_list,'','','','1','','',true);
            $html_tpl   .= getHtmlAddRow($book_list,'','','','1','','',false);
            //ADD Book Template
            $this->ss->assign('html_tpl',$html_tpl);
        }else{
            global $current_user;
            $book_list  = getBookList();
            $html_tpl   = getHtmlAddRow($book_list,'','','','1','','',true);
            $q1 = "SELECT DISTINCT
            IFNULL(j_inventorydetail.id, '') primaryid,
            ABS(j_inventorydetail.quantity) quantity,
            j_inventorydetail.price price,
            j_inventorydetail.amount amount,
            IFNULL(l2.id, '') book_id,
            IFNULL(l2.unit,'') unit,
            IFNULL(l2.code,'') code
            FROM j_inventorydetail
            INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id
            AND l1.deleted = 0
            INNER JOIN product_templates l2 ON j_inventorydetail.book_id = l2.id
            AND l2.deleted = 0
            WHERE (((l1.id = '{$this->bean->id}')))
            AND j_inventorydetail.deleted = 0";
            $details = $GLOBALS['db']->fetchArray($q1);
            foreach($details as $detail)
                $html_tpl .= getHtmlAddRow($book_list , $detail['code'], $detail['book_id'], $detail['unit'], $detail['quantity'],format_number($detail['price']),format_number($detail['amount']), false);
            $this->ss->assign('html_tpl',$html_tpl);

            $this->bean->assigned_user_name = $current_user->full_user_name;
            $this->bean->assigned_user_id = $current_user->id;

            $this->bean->total_quantity = abs($this->bean->total_quantity);
        }
        parent::display();
    }
}
function getHtmlAddRow( $book_list, $book_code, $book_id, $book_unit, $book_quantity,$book_price, $book_amount, $showing){
    if($showing)
        $display = 'style="display:none;"';
    $tpl_addrow = "<tr class='row_tpl' $display>";
    $htm_sel    = '<select name="book_id[]" class="book_id" style="width:199px;"><option value="" price="0" unit="">-none-</option>';
    $first_opt  = true;
    $previous   = '';
    foreach($book_list as $key => $value){
        if ($value['category'] != $previous){
            if(!$first_opt)
                $htm_sel .= '</optgroup>';
            else
                $first_opt = false;

            $htm_sel       .= '<optgroup label="' .$value['category'] . '">';
            $htm_sel       .= '<option style="color: green;" value="full-set">-- select full-set --</option>';
            $previous   = $value['category'];
        }
        $sel = '';
        if(!empty($book_id) && $book_id == $value["primaryid"])
            $sel = 'selected';
        $htm_sel .= '<option '.$sel.' value="'.$value["primaryid"].'" price="'.format_number($value['list_price']).'" unit="'.$value['unit'].'" code="'.$value['code'].'">'.$value["name"].'</option>';
    }
    $htm_sel .= '</optgroup></select>';
    $tpl_addrow .= '<td scope="col" align="center"><span class="book_code">'.$book_code.'</span></td>';
    $tpl_addrow .= '<td scope="col" align="center">'.$htm_sel.'</td>';
    $tpl_addrow .= '<td align="center"><input type="number" size="5" class="book_quantity" name="book_quantity[]" value="'.$book_quantity.'" min="1" max="10000" style="text-align: right;width: 100%;"></td>';
    $tpl_addrow .= '<td align="center"><span class="book_unit">'.$book_unit.'</span>
    <input class="book_price" type="hidden" name="book_price[]" value="'.$book_price.'">
    <input class="book_amount" type="hidden" name="book_amount[]" value="'.$book_amount.'">
    </td>';

    //    $tpl_addrow .= '<td nowrap align="center"><input class="currency input_readonly book_price" type="text" name="book_price[]" size="13" value="'.$book_price.'" style="font-weight: bold;" readonly></td>';
    //    $tpl_addrow .= '<td nowrap align="center"><input class="currency input_readonly book_amount" type="text" name="book_amount[]" size="13" value="'.$book_amount.'" style="font-weight: bold;" readonly></td>';
    $tpl_addrow .= "<td align='center'><button type='button' class='btn btn-danger btnRemove'>-</button></td>";
    $tpl_addrow .= '</tr>';
    return $tpl_addrow;
}

function getBookList(){
    return $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(product_templates.id, '') primaryid,
        IFNULL(product_templates.name, '') name,
        IFNULL(product_templates.code, '') code,
        product_templates.list_price list_price,
        product_templates.unit unit,
        IFNULL(l2.name, '') parent,
        IFNULL(l2.id, '') parent_id,
        IFNULL(l2.list_order, '') parent_order,
        IFNULL(l1.name, '') category,
        IFNULL(l1.id, '') category_id,
        IFNULL(l1.list_order, '') list_order
        FROM product_templates
        LEFT JOIN product_categories l1 ON product_templates.category_id = l1.id AND l1.deleted = 0
        LEFT JOIN product_categories l2 ON l1.parent_id = l2.id AND l2.deleted = 0
        WHERE (product_templates.status2 = 'Active')
        AND product_templates.deleted = 0
    ORDER BY l1.name, l1.id, name ASC");
}
