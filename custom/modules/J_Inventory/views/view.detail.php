<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');
class J_InventoryViewDetail extends ViewDetail {
    function display() {
        $EXPORT_DETAIL='<input class="button" type="submit" value="'.$GLOBALS['mod_strings']['LBL_EXPORT_DETAIL'].'" id="export"></input>';
        $this->ss->assign('EXPORT_DETAIL',$EXPORT_DETAIL);
        parent::display();
    }
}
?>