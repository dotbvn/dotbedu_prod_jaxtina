<?php
    // Do not store anything in this file that is not part of the array or the hook version.  This file will	
    // be automatically rebuilt in the future. 
    $hook_version = 1; 
    $hook_array = Array(); 
    // position, file, function 
    $hook_array['before_save'] = Array(); 
    $hook_array['before_save'][] = Array(1, 'Add Auto-Increment Code', 'custom/modules/J_Inventory/InventoryLogic.php','InventoryLogic', 'autoCode'); 
    $hook_array['before_save'][] = Array(2, 'Save Detail Inventory', 'custom/modules/J_Inventory/InventoryLogic.php','InventoryLogic', 'handleBeforeSave');
     
    $hook_array['before_delete'] = Array(); 
    $hook_array['before_delete'][] = Array(1, 'Delete Inventory', 'custom/modules/J_Inventory/InventoryLogic.php','InventoryLogic', 'deletedInventory'); 

    $hook_array['process_record'] = Array(); 
    $hook_array['process_record'][] = Array(1, 'Color', 'custom/modules/J_Inventory/InventoryLogic.php','InventoryLogic', 'listViewColorInven');

?>