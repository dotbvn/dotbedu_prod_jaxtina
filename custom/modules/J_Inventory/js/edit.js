$(document).ready(function(){
    var is_admin   = $('#is_admin').val();
    var payment_id = $('#payment_id').val();
    var record     = $('input[name=record]').val();
    if(payment_id == ''){
        $('#tblbook').multifield({
            section :   '.row_tpl', // First element is template
            addTo   :   '#tbodybook', // Append new section to position
            btnAdd  :   '#btnAddrow', // Button Add id
            btnRemove:  '.btnRemove', // Buton remove id
        });

        $('.book_quantity').live('change',function(){
            calBookPayment();
        });

        //Change Team Action - Fix bug Mutiple User
        $('#btn_team_id').click(function(){
            open_popup('Teams', 1000, 700, "", true, false, {"call_back_function":"set_team_return","form_name":"EditView","field_to_name_array":{"id":"team_id","name":"team_name"}}, "single", true);
        });

        $('#btn_clr_team_id').click(function(){
            $('#team_name,#team_id').val('');
        });

        //Ẩn Option thu tiền theo theo tháng

        $('.book_id').live('change',function(){
            if($(this).val() == 'full-set'){
                var arrSet = [];
                $(this).find(":selected").closest('optgroup').find('option').each(function () {
                    if($(this).val() != 'full-set')
                        arrSet.push($(this).val());
                });
                //Xu ly add row
                var countRow = $('select.book_id').length - 1;
                var rowEq    = $(this).closest('tr').index();
                var remainRow= (countRow - rowEq) + 1;
                var countArrSet = arrSet.length;
                if(countArrSet > remainRow)
                    for (i = 0; i < (countArrSet - remainRow); i++)
                        $('#btnAddrow').trigger('click');
                $(this).val('');//Clear option
                var startAdd = rowEq;
                $.each(arrSet, function( index, value ){
                    $('select.book_id:eq('+startAdd+')').val(value);
                    $('input.book_quantity:eq('+startAdd+')').val('1');
                    startAdd++;
                });
            }
            $('input.book_quantity').each(function() {
                if($(this).val() == '')
                    $(this).val('1');
            });
            calBookPayment();
        });
    }else{
        $('.book_id, .book_quantity, #type, #assigned_user_name, #assigned_user_name').prop('disabled', true);
        $('#btn_team_id, #btn_clr_team_id, #btnAddrow, .btnRemove, #btn_clr_assigned_user_name, #btn_assigned_user_name').hide();
        var type = $('#type').val();
    }
});
function calBookPayment(){
    var total_pay = 0;
    var total_quantity = 0;
    $('#tblbook tbody tr:not(:first)').each(function(index, brand){
        var book_price      = Numeric.parse($(this).find('select.book_id option:selected').attr('price'));
        var book_unit       = $(this).find('select.book_id option:selected').attr('unit');
        var code            = $(this).find('select.book_id option:selected').attr('code');
        var book_quantity   = parseInt($(this).find('.book_quantity').val());
        var book_cost       = (book_price * book_quantity);
        $(this).find('.book_code').text(code);
        $(this).find('.book_price').val(Numeric.toInt(book_price));
        $(this).find('.book_amount').val(Numeric.toInt(book_cost));
        $(this).find('.book_unit').text(book_unit);
        total_pay           = total_pay + book_cost;
        total_quantity = total_quantity + book_quantity;
    });

    $('#total_quantity').val(total_quantity);
}
function handleRemoveRow(){
    calBookPayment();
}



function set_team_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'team_name':
                    var team_name = val;
                    break;
                case 'team_id':
                    var team_id = val;
                    break;
            }
        }
    }
    $('#team_name').val(team_name);
    $('#team_id').val(team_id);

}