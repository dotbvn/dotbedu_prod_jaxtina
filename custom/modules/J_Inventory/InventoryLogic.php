<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class InventoryLogic{
    function deletedInventory(&$bean, $event, $arguments){
        //        if(!empty($bean->j_payment_j_inventory_1j_payment_ida)){
        //            echo '
        //            <script type="text/javascript">
        //            alert("This transaction was linked to a payment. You can not delete !");
        //            location.href=\'index.php?module=J_Inventory&action=DetailView&record='.$bean->id.'\';
        //            </script>';
        //            die();
        //        }else
        $GLOBALS['db']->query("UPDATE j_inventorydetail SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE inventory_id = '{$bean->id}'");
    }
    function handleBeforeSave(&$bean, $event, $arguments){
        if ($_POST["module"] == "J_Inventory" && $_POST['action'] == 'Save'){
            if(empty($bean->j_payment_j_inventory_1j_payment_ida)){
                if(!empty($bean->fetched_row)) InventoryLogic::deletedInventory($bean, $event, $arguments);

                for ($i = 1; $i < count($_POST["book_id"]); $i++) {
                    $bookId         = $_POST["book_id"][$i];
                    $bookQuantity   = $_POST["book_quantity"][$i];
                    $bookPrice      = unformat_number($_POST["book_price"][$i]);
                    if ($bookId != ""){
                        // Create Inventory Detail
                        $inventoryDetail = BeanFactory::newBean("J_Inventorydetail");
                        $inventoryDetail->book_id       = $bookId;
                        $inventoryDetail->inventory_id  = $bean->id;

                        if($bean->type == 'Income') $inventoryDetail->quantity      =  abs($bookQuantity);
                        elseif($bean->type == 'Expense') $inventoryDetail->quantity      = -1 * abs($bookQuantity);

                        $inventoryDetail->price         = $bookPrice;
                        $inventoryDetail->amount        = $bookPrice * $bookQuantity;
                        $inventoryDetail->team_id       = $bean->team_id;
                        $inventoryDetail->team_set_id   = $bean->team_set_id;
                        $inventoryDetail->assigned_user_id = $bean->assigned_user_id;
                        $inventory_total_quantity += $inventoryDetail->quantity;
                        $bean->total_amount += $inventoryDetail->amount;
                        $inventoryDetail->save();
                    }
                }
            }
            if($bean->type == 'Expense')
                $bean->total_quantity  = -1 * abs($bean->total_quantity);
            $bean->team_set_id = $bean->team_id;
        }
    }

    function handleBeforeDelete(&$bean, $event, $arguments){
        // delete payment book/gift
        $payment = BeanFactory::getBean("J_Payment", $bean->j_payment_j_inventory_1j_payment_ida);
        if ($payment->deleted == 0){
            $payment->deleted=1;
            $payment->save();
        }
        //delete detail inventory relationship
        $bean->deleteDetail();
    }

    ///to mau id va status Lap Nguyen
    function listViewColorInven(&$bean, $event, $arguments){


        if($bean->type == 'Expense'){
            $q1 = "SELECT DISTINCT
            IFNULL(l1.id, '') payment_id,
            (CASE WHEN l1.parent_type = 'Leads' THEN IFNULL(l3.id, '') ELSE IFNULL(l2.id, '') END) student_id,
            IFNULL(l1.parent_type, '') parent_type,
            (CASE WHEN l1.parent_type = 'Leads' THEN IFNULL(l3.full_lead_name, '') ELSE IFNULL(l2.full_student_name, '') END) student_name
            FROM j_inventory
            INNER JOIN j_payment_j_inventory_1_c l1_1 ON j_inventory.id = l1_1.j_payment_j_inventory_1j_inventory_idb AND l1_1.deleted = 0
            INNER JOIN j_payment l1 ON l1.id = l1_1.j_payment_j_inventory_1j_payment_ida  AND l1.deleted = 0
            LEFT JOIN contacts l2 ON l2.id = l1.parent_id AND l1.parent_type = 'Contacts' AND l2.deleted = 0
            LEFT JOIN leads l3 ON l3.id = l1.parent_id AND l1.parent_type = 'Leads' AND l3.deleted = 0
            WHERE  j_inventory.deleted = 0 AND j_inventory.id = '{$bean->id}'";
            $app = $GLOBALS['db']->fetchOne($q1);
            $bean->student_name_text .= "<a href='#{$app['parent_type']}/{$app['student_id']}'>{$app['student_name']}</a>";
        }
        switch ($bean->type) {
            case "Income":
                $bean->type = '<span class="label ellipsis_inline textbg_blue">'.$GLOBALS['app_list_strings']['type_inventory_list'][$bean->type].'</span>';
                break;
            case "Expense":
                $bean->type = '<span class="label ellipsis_inline textbg_green">'.$GLOBALS['app_list_strings']['type_inventory_list'][$bean->type].'</span>';
                break;
        }

    }

    function autoCode(&$bean, $event, $arg) {
        $code_field = 'name';
        if (($bean->$code_field === "Auto generate") || empty($bean->$code_field)) {
            switch ($bean->type) {
                case "Income":
                    $typecode = 'IN-';
                    break;
                case "Expense":
                    $typecode = 'OUT-';
                    break;
            }
            //Get Prefix
            $res = $GLOBALS['db']->query("SELECT teams.code_prefix code FROM teams WHERE id = '{$bean->team_id}'");
            $row = $GLOBALS['db']->fetchByAssoc($res);
            $prefix = $row['code'];
            $year = date('ym', strtotime('+ 7hours' . (!empty($bean->date_entered) ? $bean->date_entered : $bean->fetched_row['date_entered'])));
            $table = $bean->table_name;
            $sep = '-';
            $first_pad = '000';
            $padding = 3;
            $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, " . strlen($typecode.$prefix . $year) . ") = '" . $typecode.$prefix . $year . "') ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";

            $result = $GLOBALS['db']->query($query);
            if ($row = $GLOBALS['db']->fetchByAssoc($result))
                $last_code = $row[$code_field];
            else
                //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                $last_code = $typecode.$prefix . $year . $first_pad;


            $num = substr($last_code, -$padding, $padding);
            $num++;
            $pads = $padding - strlen($num);
            $new_code = $typecode.$prefix . $year . $sep;

            //preform the lead padding 0
            for ($i = 0; $i < $pads; $i++)
                $new_code .= "0";
            $new_code .= $num;

            //write to database - Logic: Before Save
            $bean->$code_field = $new_code;
        }
    }
}
?>