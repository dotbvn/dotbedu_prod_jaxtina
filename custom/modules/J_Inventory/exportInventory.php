<?php
require_once 'custom/include/PHPExcel/Classes/PHPExcel.php';
require_once("custom/include/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php");
require_once("custom/include/PHPExcel/Classes/PHPExcel/IOFactory.php");

global $current_user;

$fdir = 'InvoiceExcel';
if (!file_exists("upload/$fdir"))
    mkdir("upload/$fdir", 0777, true);

$fi = new FilesystemIterator("upload/$fdir", FilesystemIterator::SKIP_DOTS);
if(iterator_count($fi) > 10)
    array_map('unlink', glob("upload/$fdir/*"));

$objPHPExcel = new PHPExcel();

//Import Template
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load("custom/include/TemplateExcel/phieu_giao_sach.xlsx");

// Set properties
$objPHPExcel->getProperties()->setCreator("DotB Center");
$objPHPExcel->getProperties()->setLastModifiedBy("DotB Center");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX");

//Add data
$details = BeanFactory::getBean('J_Inventory',$_REQUEST['record']);
global $timedate;

//get to list
if($details->type == "Expense"){
    $payment_id = $this->bean->j_payment_j_inventory_1j_payment_ida;
    $sql = "SELECT DISTINCT
    (CASE WHEN l2.parent_type = 'Contacts' THEN IFNULL(l3.id, '') ELSE IFNULL(l6.id, '') END) student_id,
    (CASE WHEN l2.parent_type = 'Contacts' THEN IFNULL(l3.full_student_name, '') ELSE IFNULL(l6.full_lead_name, '') END) student_name,
    IFNULL(l4.id, '') team_id,
    IFNULL(l4.name, '') team_name,
    IFNULL(l1.id, '') inventory_id,
    l1.date_create date_issue,
    (CASE WHEN l2.parent_type = 'Contacts' THEN l3.birthdate ELSE l6.birthdate END) birthdate,
    (CASE WHEN l2.parent_type = 'Contacts' THEN IFNULL(l3.contact_id, '') ELSE '' END) student_code,
    (CASE WHEN l2.parent_type = 'Contacts' THEN IFNULL(l3.phone_mobile, '') ELSE IFNULL(l6.phone_mobile, '') END) phone_mobile,
    IFNULL(l5.id, '') product_id,
    IFNULL(l5.name, '') product_name,
    IFNULL(j_inventorydetail.id, '') primaryid,
    ABS(j_inventorydetail.quantity) quantity,
    IFNULL(l2.id, '') payment_id,
    IFNULL(l2.name, '') payment_code,
    IFNULL(l1.name, '') request_no,
    IFNULL(l7.full_user_name, '') full_user_name
    FROM j_inventorydetail
    INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id AND l1.deleted = 0
    INNER JOIN j_payment_j_inventory_1_c l2_1 ON l1.id = l2_1.j_payment_j_inventory_1j_inventory_idb AND l2_1.deleted = 0
    INNER JOIN j_payment l2 ON l2.id = l2_1.j_payment_j_inventory_1j_payment_ida AND l2.deleted = 0
    LEFT JOIN contacts l3 ON l3.id = l2.parent_id AND l2.parent_type = 'Contacts' AND l3.deleted = 0
    LEFT JOIN leads l6 ON l6.id = l2.parent_id AND l2.parent_type = 'Leads' AND l6.deleted = 0
    INNER JOIN teams l4 ON l1.team_id = l4.id AND l4.deleted = 0
    INNER JOIN product_templates l5 ON j_inventorydetail.book_id = l5.id AND l5.deleted = 0
    INNER JOIN users l7 ON l1.assigned_user_id = l7.id AND l7.deleted = 0
    WHERE (((l2.id = '$payment_id'))) AND j_inventorydetail.deleted = 0";
    $res1 = $GLOBALS['db']->query($sql);
    $i=0;
    while($r = $GLOBALS['db']->fetchByAssoc($res1)){
        $row = 15 + $i;
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $i+1);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $r['product_name']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $r['quantity']);
        $i++;
    }
    $res1 = $GLOBALS['db']->query($sql);
    $r = $GLOBALS['db']->fetchByAssoc($res1);
    $objPHPExcel->getActiveSheet()->SetCellValue('E7', $timedate->to_display_date($r['date_issue'],true));
    $objPHPExcel->getActiveSheet()->SetCellValue('E8', $r['team_name']);
    $objPHPExcel->getActiveSheet()->SetCellValue('E9', $r['student_name']);
    $objPHPExcel->getActiveSheet()->SetCellValue('E10',$timedate->to_display_date($r['birthdate'],true));
    $objPHPExcel->getActiveSheet()->SetCellValue('E11',$r['student_code']);
    $objPHPExcel->getActiveSheet()->SetCellValue('E12',$r['phone_mobile']);
    $objPHPExcel->getActiveSheet()->SetCellValue('H3', $r['payment_code']);
    $objPHPExcel->getActiveSheet()->SetCellValue('H4', $r['request_no']);
    $objPHPExcel->getActiveSheet()->SetCellValue('B38', $current_user->full_user_name);
    $objPHPExcel->getActiveSheet()->SetCellValue('H5', '01');

}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Liên 1');

//clone
$objSheetBase   = $objPHPExcel->getActiveSheet();
$objSheetBase   = clone $objSheetBase;
$objSheetBase->setTitle('Liên 2');
$objPHPExcel->addSheet($objSheetBase);
$objPHPExcel->setActiveSheetIndexByName('Liên 2')->SetCellValue('H5', '02');

// Save Excel 2007 file
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$section = create_guid_section(6);
$file = 'upload/'.$fdir.'/Inventory'.$details->name.'-'.$section.'.xlsx';

$objWriter->save($file);
header('Location: '.$file);