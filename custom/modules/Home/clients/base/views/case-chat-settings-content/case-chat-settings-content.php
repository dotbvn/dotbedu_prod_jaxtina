<?php

$viewdefs['Home']['base']['view']['case-chat-settings-content'] = array(
    'fields' => array(
        'external_default_subject' => array(
            'name' => 'external_default_subject',
            'type' => 'varchar',
            'label' => 'LBL_EXTERNAL_DEFAULT_SUBJECT',
            'default' => '',
            'span' => 6
        ),
        'internal_default_subject' => array(
            'name' => 'internal_default_subject',
            'type' => 'varchar',
            'label' => 'LBL_INTERNAL_DEFAULT_SUBJECT',
            'default' => '',
            'span' => 6
        ),
        'license_type' => array(
            'name' => 'cc_license_type',
            'type' => 'enum',
            'options' => 'license_type_list',
            'label' => 'LBL_LICENSE_TYPE',
            'default' => 'outfitters',
            'span' => 6
        )
    ),
    'exsitec_fields' => array(
        array(
            'name' => 'cc_license_key',
            'label' => 'LBL_LICENSE_KEY',
        ),
        array(
            'name' => 'cc_validation_key',
            'label' => 'LBL_VALIDATION_KEY',
            'type' => 'textarea',
            'rows' => '5',
        ),
    ),
    'outfitters_fields' => array(
        array(
            'name' => 'cc_license_key',
            'label' => 'LBL_LICENSE_KEY',
        ),
    ),
);
