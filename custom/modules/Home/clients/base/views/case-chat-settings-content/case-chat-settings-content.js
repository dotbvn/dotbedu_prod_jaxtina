({
    loading: false,
    initialize: function (options) {
        this._super("initialize", [options]);
        this.context.on('settings:save', this.saveConfig, this);
        this.configModel = new app.data.beanModel();

        //Whenever we change to license type (exsitec/outfitters)
        //We need to rerender the view so that the correct metadata
        //fields are being displayed. See HBS.
        this.configModel.on('change:cc_license_type', function (model) {
            if (!this.loading) {
                this.render();
            }
        }, this);
    },

    _render: function () {
        this._super('_render');
    },

    /**
     * Loads configuration parameters and sets on configModel
     * @returns {undefined}
     */
    loadData: function () {
        this.loading = true;
        app.api.call('read', app.api.buildURL('Cases', 'loadConfig'), {}, {
            success: _.bind(function (data) {
                if (!_.isEmpty(data)) {
                    _.each(data, function (value, key) {
                        this.configModel.set(key, value);
                    }, this);
                }
                this.loading = false;
                this.render();
            }, this),
            error: _.bind(function (error) {
                console.log(error);
            }, this)});
    },

    /**
     * Save changes to config parameters
     */
    saveConfig: function () {
        let data = {};

        data['external_default_subject'] = this.configModel.get('external_default_subject');
        data['internal_default_subject'] = this.configModel.get('internal_default_subject');

        var fields = {};
        switch (this.configModel.get('cc_license_type')) {
            case 'exsitec':
                fields = this.meta.exsitec_fields;
                break;
            case 'outfitters':
                fields = this.meta.outfitters_fields;
                break;
        }

        _.each(fields, function (def) {
            data[def.name] = this.configModel.get(def.name);
        }, this);

        data['cc_license_type'] = this.configModel.get('cc_license_type');

        app.alert.show('settings:save', {
            level: 'process',
            title: app.lang.getAppString('LBL_LOADING')
        });

        var url = app.api.buildURL('Cases', 'saveConfig');

        app.api.call('update', url, data, {
            success: _.bind(function (result) {
                app.alert.dismiss('settings:save');

                app.alert.show('settings:success', {
                    level: 'success',
                    title: app.lang.getAppString('LBL_SUCCESS'),
                    messages: 'All settings saved.',
                    autoClose: true
                });

                app.router.goBack();
            }, this),
            error: _.bind(function (error) {
                console.log(error);
            }, this),
            complete: _.bind(function (data) {
                app.alert.dismiss('settings:save');
            }, this)
        });
    }
})
