<?php

$viewdefs['Home']['base']['layout']['case-chat-settings'] = array (
    'components' => array (
        array (
            'view' => 'case-chat-settings-headerpane',
        ),
        array (
            'view' => 'case-chat-settings-content',
        ),
    ),
    'type' => 'simple',
    'name' => 'base',
    'span' => 12,
);
