/**
 * @author Exsitec AB
 */
({
    extendsFrom: 'RecordView',
    initialize: function (options) {
        this._super('initialize', [options]);

        this.cancelCallback = _.bind(function(){
            if(this.context.get('drawer')){
                app.drawer.close();
            }
        }, this);

        this.saveCallback = _.bind(function(){
            if(this.context.get('drawer')){
                app.drawer.close();
            }
        }, this);
    },
})