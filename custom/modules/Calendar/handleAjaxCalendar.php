<?php

switch ($_POST['type']) {
    case 'ajaxUpdateFilter':
        $result = ajaxUpdateFilter($_POST['centers'], $_POST['selected_date']);
        echo $result;
        break;
    case 'ajaxGetData':
        $result = getData($_POST['centers'], $_POST['session_class'], $_POST['session_teacher'], $_POST['session_ta'], $_POST['session_room'], $_POST['dataType'], $_POST['dataColor'], $_POST['select_date'], $_POST['displayType']);
        echo $result;
        break;
    case 'ajaxSaveSettingCalendar':
        $result = saveSettingCalendar($_POST['dataType'], $_POST['dataColor'], $_POST['filter']);
        echo $result;
        break;

}
//++++++++++++++=====================+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//filter Teams
function ajaxUpdateFilter($centers=array(), $selected_date = ''){
    global $timedate;
    require_once("custom/modules/Calendar/_helper.php");
    $arrayClass = $arrayTeacher = $arrayTa = $arrayRoom = array();
    if(empty($selected_date)) $selected_date = $timedate->nowDbDate();
    else $selected_date = $timedate->convertToDBDate($selected_date);

    foreach(getClasses($centers , $selected_date) as $id => $value){array_push($arrayClass, array('text' => $value, 'value' => $id));};
    foreach(getTeachers($selected_date) as $id => $value){array_push($arrayTeacher, array('text' => $value, 'value' => $id));}
    foreach(getTas($selected_date) as $id => $value){array_push($arrayTa, array('text' => $value, 'value' => $id));}
    foreach(getRooms($selected_date) as $id => $value){array_push($arrayRoom, array('text' => $value, 'value' => $id));}

    return json_encode(array(
        'success' => 1,
        'arrayClass' => $arrayClass,
        'arrayTeacher' => $arrayTeacher,
        'arrayTa' => $arrayTa,
        'arrayRoom' => $arrayRoom,
    ));
}

function getData($centers = [], $session_class = [], $session_teacher = [], $session_ta = [], $session_room = [], $dataType = [], $dataColor = [],  $select_date = '', $display_type = ''){
    global $timedate, $current_user, $current_language;
    $mod_list_strings = return_mod_list_strings_language($current_language,'Calendar');
    //Check view
    $date        = $select_date['year'].'-'.$select_date['month'].'-'.$select_date['day'];
    $filter_date = date($timedate->get_db_date_format(),strtotime($date));
    $week        = $select_date['week'];
    $year        = $select_date['year'];
    $uri         = str_replace('index.php','', $_SERVER['SCRIPT_NAME']);
    switch ($select_date['view']){
        case 'day':
        case 'list':
            $start = $end = $filter_date;
            break;
        case 'week':
            $start  = date('Y-m-d',strtotime('monday this week', strtotime($date)));
            $end    = date('Y-m-d',strtotime('sunday this week', strtotime($date)));
            break;
        case 'month':
            $start = date('Y-m-01',strtotime($filter_date)); //first_day_this_month
            $end   = date('Y-m-t',strtotime($filter_date)); //last_day_this_month
            break;
    }
    //Filter CLASS/CENTER *Bắt buộc

    if(empty($centers) || empty($session_class) )
        return json_encode(array("success" => 1, "timetables" => array(), "count" => 0));
    else{
        $ext_center = "AND l8.id IN ('".implode("','", $centers)."')";
        $ext_class = "AND l1.id IN ('".implode("','", $session_class)."')";
    }

    //PHÂN QUYỀN GIÁO VIÊN/TA
    if(!empty($current_user->teacher_id)){
        //load ds buổi học của giáo viên
        $mtlist = "AND mt.id IN (SELECT DISTINCT IFNULL(mta.id, '') meeting_id FROM meetings mta
        WHERE mta.deleted = 0 AND (mta.session_status <> 'Cancelled')
        AND (mta.teacher_id = '{$current_user->teacher_id}'
        OR mta.teacher_cover_id = '{$current_user->teacher_id}'
        OR mta.teacher_cover_id = '{$current_user->teacher_id}')
        AND (mta.date>='$start' AND mta.date<='$end'))";

    }

    //Filter TEACHER/TA/ROOM
    if(!empty($session_teacher))
        $mt_teacher = "AND (IFNULL(mt.teacher_id,'') IN ('".implode("','", $session_teacher)."'))";

    if(!empty($session_ta)){
        $_imploded = implode("','", $session_ta);
        if(in_array('no_teacher_cover_id',$session_ta) && in_array('no_sub_teacher_id',$session_ta) && count($session_ta) == 2) //TH: Chọn cả 2 option -No TA-
            $mt_ta = "AND (IFNULL(mt.teacher_cover_id,'') = '' AND IFNULL(mt.sub_teacher_id,'') = '')";
        else
            $mt_ta = "AND ((IFNULL(mt.teacher_cover_id,'') IN ('".str_replace('no_teacher_cover_id','',$_imploded)."'))
            OR (IFNULL(mt.sub_teacher_id,'') IN ('".str_replace('no_sub_teacher_id','',$_imploded)."')))";
    }
    if(!empty($session_room))
        $mt_room = "AND (IFNULL(mt.room_id,'') IN ('".implode("','", $session_room)."'))";

    $uTz = $timedate->getUserTimeZone()['gmt'];
    //get session
    $sqlSelect = "SELECT DISTINCT IFNULL(mt.id, '') meeting_id,
    IFNULL(mt.external_id, '') external_id, IFNULL(mt.join_url, '') join_url,
    IFNULL(mt.type, '') type, IFNULL(mt.lesson_number,0) lesson_number,
    IFNULL(l1.id, '') class_id, 'dotb-blue' color,
    IFNULL(l1.name, '') class_name, IFNULL(l1.status, '') class_status,
    IFNULL(l1.class_code, '') class_code, IFNULL(l8.id, '') center_id,
    IFNULL(l8.name, '') center_name, IFNULL(l9.id, '') assigned_user_id,
    IFNULL(l9.full_user_name, '') assigned_user_name, l1.start_date class_start, l1.end_date class_end,
    IFNULL(l7.name, '') koc_name, IFNULL(l1.max_size, 0) max_size,
    IFNULL(l1.number_of_student, 0) number_of_student, IFNULL(l1.level, '') level,
    IFNULL(l2.id, '') teacher_id, IFNULL(l2.full_teacher_name, '') teacher_name,
    IFNULL(l3.id, '') ta1_id, IFNULL(l3.full_teacher_name, '') ta1_name,
    IFNULL(l4.id, '') ta2_id, IFNULL(l4.full_teacher_name, '') ta2_name,
    CONCAT_WS(' ',l3.full_teacher_name, l4.full_teacher_name) ta_name,
    IFNULL(l5.id, '') room_id, IFNULL(l5.name, '') room_name,
    IFNULL(l5.status, '') room_status, mt.date_modified date_modified,
    mt.date_start date_start, mt.date_end date_end, mt.date date,
    DATE_FORMAT(CONVERT_TZ(mt.date_start,'+0:00','$uTz'),'%Y-%m-%d %H:%i:%s') date_time,
    LCASE(REPLACE(DATE_FORMAT(CONVERT_TZ(mt.date_start,'+0:00','$uTz'),'%l:%i%p'),':00','')) start_time,
    DATE_FORMAT(CONVERT_TZ(mt.date_end,'+0:00','$uTz'),'%H:%i') end_time,
    DATE_FORMAT(CONVERT_TZ(mt.date_start,'+0:00','$uTz'),'%H:%i') start_time_2,
    DATE_FORMAT(CONVERT_TZ(mt.date_start,'+0:00','$uTz'),'%H:00') start_hour,
    DATE_FORMAT(CONVERT_TZ(mt.date_start,'+0:00','$uTz'),'%H:%i') time_24hr,
    DATE_FORMAT(CONVERT_TZ(mt.date_start,'+0:00','$uTz'),'%W') week_date,
    IFNULL(l6.id, '') syllabus_id, IFNULL(l6.name, '') syllabus_topic, IFNULL(l6.lesson_type, '') syllabus_lesson_type,
    IFNULL(l6.description, '') syllabus_content, IFNULL(l6.homework, '') syllabus_homework,
    IFNULL(mt.total_absent, 0) total_absent, IFNULL(mt.total_attended, 0) total_attended,
    IFNULL(mt.total_student, 0) total_student, IFNULL(mt.attendance_status, 0) attendance_status
    FROM meetings mt INNER JOIN j_class l1 ON l1.id = mt.ju_class_id AND l1.deleted = 0 $ext_class
    INNER JOIN teams l8 ON l8.id = l1.team_id AND l8.deleted = 0 $ext_center
    LEFT JOIN j_kindofcourse l7 ON l7.id = l1.koc_id AND l7.deleted = 0
    LEFT JOIN users l9 ON l9.id = l1.assigned_user_id AND l9.deleted = 0
    LEFT JOIN c_teachers l2 ON l2.deleted = 0 AND (l2.id = mt.teacher_id)
    LEFT JOIN c_teachers l3 ON l3.deleted = 0 AND (l3.id = mt.teacher_cover_id)
    LEFT JOIN c_teachers l4 ON l4.deleted = 0 AND (l4.id = mt.sub_teacher_id)
    LEFT JOIN c_rooms l5 ON l5.deleted = 0 AND l5.id = mt.room_id
    LEFT JOIN j_syllabus l6 ON mt.syllabus_id = l6.id AND l6.deleted = 0
    WHERE mt.deleted = 0 $mtlist AND (mt.session_status <> 'Cancelled') AND (mt.date >= '$start' AND mt.date <= '$end')
    $mt_teacher $mt_ta $mt_room
    ORDER BY start_time_2, end_time, date, date_modified";
    $rows = $GLOBALS['db']->fetchArray($sqlSelect);
    $timetables = $array_day_list = array();
    $count = 0;
    $empty = array();
    //Duyệt qua từng dòng kết quả
    $MOD = $GLOBALS['mod_strings'];
    $MOD_S = return_module_language($GLOBALS['current_language'], 'J_Syllabus');
    $MOD_M = return_module_language($GLOBALS['current_language'], 'Meetings');
    if($display_type == 'room' || $display_type == 'teacher'){
        foreach ($rows as $row){
            //format kết quả
            $row['name']       = $row['class_name'];
            $row['date_db']    = $row['date'];
            $wd_n              = date('w',strtotime($row['date']));
            $row['wd_n']       = ($wd_n == 0) ? 7 : $wd_n;
            $row['week_date_s']= $mod_list_strings['dom_cal_weekdays'][$wd_n];
            $row['week_date_l']= $mod_list_strings['dom_cal_weekdays_long'][$wd_n];
            $row['date_start'] = $timedate->to_display_date_time($row['date_start']);
            $row['date_end']   = $timedate->to_display_date_time($row['date_end']);
            $row['date']       = $timedate->to_display_date($row['date'],false);
            $row['time_start'] = substr($row['date_start'], 11,5);
            $row['time_end']   = substr($row['date_end'], 11);
            $row['time_range']        = $row['start_time_2'] .' - '. $row['end_time'];
            $row['class_status']      = $GLOBALS['app_list_strings']['status_class_list'][$row['class_status']];
            $row['room_status']       = $GLOBALS['app_list_strings']['teacher_status_list'][$row['room_status']];
            $row['number_students']   = '<span align="center" style="color: '.((($row['number_of_student']) < 1) ? '#DC143C' : '#468931').';font-weight: bold; white-space:nowrap;">'.$row['number_of_student'].' / '.$row['max_size'].'</span>';
            $row['type']              = $GLOBALS['app_list_strings']['learning_type_list'][$row['type']];
            $row['display_type']      = $display_type;
            $row['name_class_length'] = strlen($row['name']);

            if(!empty($row['join_url'])) $row['join_url'] = '<a class="btn btn-info" title="'.translate('LBL_JOIN_EXT_MEETING','Meetings').'" role="button" href="'.$row['join_url'].'" target="_blank"><img style="height: 15px;" src="themes/default/images/'.str_replace(' ','_',strtolower($row['type'])).'.png">'.' '.$row['type'].'</a>';


            // assign popup
            $tpl = new Dotb_Smarty();
            $tpl->assign('data', $row);
            $tpl->assign('MOD', $MOD);
            $tpl->assign('MOD_S', $MOD_S);
            $tpl->assign('MOD_M', $MOD_M);
            $tpl->assign('uri', $uri);
            $row['popup_modal'] = $tpl->fetch("custom/modules/Calendar/tpls/popup.tpl");

            //Biuld day list
            $day_list = new Dotb_Smarty();
            $day_list->assign('data', $row);
            $day_list->assign('MOD', $MOD);
            $day_list->assign('popup_modal', $row['popup_modal']);
            $row['row_day'] = $day_list->fetch("custom/modules/Calendar/tpls/view2.tpl");

            if($display_type == 'room'){
                if(!empty($row['room_name'])){
                    if(!array_key_exists($row['room_name'], $timetables)){
                        $timetables[$row['room_name']]['id'] = $row['room_id'];
                    }
                    $timetables[$row['room_name']]['session'][$row['time_range']][] = $row;
                } else {
                    $empty['session'][$row['time_range']][] = $row;
                }
            } else if ($display_type == 'teacher'){
                if(!empty($row['teacher_name'])){
                    if(!array_key_exists($row['teacher_name'], $timetables)){
                        $timetables[$row['teacher_name']]['id'] = $row['teacher_id'];
                    }
                    $timetables[$row['teacher_name']]['session'][$row['time_range']][] = $row;
                } else {
                    $empty['session'][$row['time_range']][] = $row;
                }
            }
            //Show more
            if ($select_date['view'] == 'month') $array_day_list[$row['date_db']] .= $row['row_day'];
            elseif ($select_date['view'] == 'week') $array_day_list[$row['date_db'].'-'.$row['start_hour']] .= $row['row_day'];
            $count++;
        }
        if(!empty($empty)){
            $empty_label = translate('LBL_EMPTY', 'Calendar');
            $timetables['empty'] = $empty;
        }
        foreach($timetables as $key => $value){
            if(count($timetables[$key]['session']) >= 2){
                uksort($timetables[$key]['session'], 'sortTime');
            }
        }
    } else {
        foreach ($rows as $row){
            //format kết quả
            $row['name']       = $row['class_name'];
            $row['date_db']    = $row['date'];
            $row['wd_n']       = date('w',strtotime($row['date']));
            $row['week_date_s']= $mod_list_strings['dom_cal_weekdays'][$row['wd_n']];
            $row['week_date_l']= $mod_list_strings['dom_cal_weekdays_long'][$row['wd_n']];
            $row['date_start'] = $timedate->to_display_date_time($row['date_start']);
            $row['date_end']   = $timedate->to_display_date_time($row['date_end']);
            $row['date']       = $timedate->to_display_date($row['date'],false);
            $row['time_start'] = substr($row['date_start'], 11,5);
            $row['time_end']   = substr($row['date_end'], 11);
            $row['time_range']   = $row['start_time_2'] .'-'. $row['end_time'];
            $row['class_start']   = $timedate->to_display_date($row['class_start'],false);
            $row['class_end']     = $timedate->to_display_date($row['class_end'],false);
            $row['class_status'] = $GLOBALS['app_list_strings']['status_class_list'][$row['class_status']];
            $row['room_status'] = $GLOBALS['app_list_strings']['teacher_status_list'][$row['room_status']];
            $row['number_students'] = '<span align="center" style="color: '.((($row['number_of_student']) < 1) ? '#DC143C' : '#468931').';font-weight: bold; white-space:nowrap;">'.$row['number_of_student'].' / '.$row['max_size'].'</span>';
            $row['type'] = $GLOBALS['app_list_strings']['learning_type_list'][$row['type']];
            if(!empty($row['join_url'])) $row['join_url'] = '<a class="btn btn-info" title="'.translate('LBL_JOIN_EXT_MEETING','Meetings').'" role="button" href="'.$row['join_url'].'" target="_blank"><img style="height: 15px;" src="themes/default/images/'.str_replace(' ','_',strtolower($row['type'])).'.png">'.' '.$row['type'].'</a>';

            //Convert Array to Ojbect results
            $timetable = new stdClass();
            foreach ($row as $key => $value) $timetable->$key = $value;

            // assign popup
            $tpl = new Dotb_Smarty();
            $tpl->assign('data', $row);
            $tpl->assign('MOD', $MOD);
            $tpl->assign('MOD_S', $MOD_S);
            $tpl->assign('MOD_M', $MOD_M);
            $tpl->assign('uri', $uri);
            $timetable->popup_modal = $tpl->fetch("custom/modules/Calendar/tpls/popup.tpl");

            //Biuld day list
            $day_list = new Dotb_Smarty();
            $day_list->assign('data', $row);
            $day_list->assign('MOD', $MOD);
            $day_list->assign('popup_modal', $timetable->popup_modal);
            $timetable->row_day = $day_list->fetch("custom/modules/Calendar/tpls/modal_day_list.tpl");

            //Show more
            if ($select_date['view'] == 'month') $array_day_list[$row['date_db']] .= $timetable->row_day;
            elseif ($select_date['view'] == 'week') $array_day_list[$row['date_db'].'-'.$row['start_hour']] .= $timetable->row_day;

            array_push($timetables, $timetable);
            $count++;
        }
    }

    return json_encode(array("success" => 1, "timetables" => $timetables, "showmore" => $array_day_list, "count" => $count));
}
function sortTime($a, $b){
    $timeA = substr($a, 0, strpos($a, '-'));
    $timeB = substr($b, 0, strpos($b, '-'));

    $timeA = trim($timeA);
    $timeB = trim($timeB);

    if(strtotime($timeA) > strtotime($timeB)){
        return 1;
    } else if (strtotime($timeA) < strtotime($timeB)){
        return -1;
    } else {
        return 0;
    }
}
function saveSettingCalendar($dataType, $dataColor, $filter){
    global $current_user, $timedate;
    $current_user->setPreference('show_session', "true", 0, 'global', $current_user);
    $current_user->setPreference('color_session', $dataColor['session'], 0, 'global', $current_user);//set color

    if(empty($dataType['view'])) $dataType['view'] = '';
    if(empty($dataType['view_as'])) $dataType['view_as'] = '';
    if(!isset($dataType['nav_status'])) $dataType['nav_status'] = '1';
    $current_user->setPreference('calendar_view', $dataType['view'], 0, 'global', $current_user);//set view
    $current_user->setPreference('calendar_view_as', $dataType['view_as'], 0, 'global', $current_user);//set view
    $current_user->setPreference('calendar_nav_status', $dataType['nav_status'], 0, 'global', $current_user);//set view

    foreach ($filter as $key => $value){
        //if($key != 'centers') continue; //Tạm thời chỉ lưu center
        $data = (count($value) > 10) ? '' : json_encode($value);
        $current_user->setPreference($key, $data, 0, 'calender_filter', $current_user);
    }
    $current_user->setPreference('filter_modified', $timedate->nowDb(), 0, 'calender_filter', $current_user);//set timeout 8hrs
    return json_encode(array("success" => 1));
}


