<?php

function getClasses($teamIds = array(), $selected_date){
    global $current_user;
    if(!empty($current_user->teacher_id)){
        $ext_teacher = "INNER JOIN meetings l1 ON j_class.id = l1.ju_class_id AND l1.deleted = 0
        AND ('{$current_user->teacher_id}' IN (l1.teacher_id, l1.teacher_cover_id, l1.sub_teacher_id, j_class.teacher_id))
        AND l1.session_status <> 'Cancelled'";
    }
    $selected_date = date('Y-m-d', strtotime("first day of this month $selected_date"));
    $sClass = "SELECT DISTINCT IFNULL(j_class.id, '') class_id,
    IFNULL(j_class.name, '') class_name
    FROM j_class
    INNER JOIN team_sets_teams tst ON tst.team_set_id = j_class.team_set_id AND tst.deleted = 0
    INNER JOIN teams ts ON ts.id = tst.team_id AND ts.deleted = 0 AND (ts.id IN ('".implode("','",$teamIds)."'))
    $ext_teacher
    WHERE j_class.deleted = 0 AND ('$selected_date' <= j_class.end_date) AND (IFNULL(j_class.status,'') <> 'Closed')";
    $classes = $GLOBALS['db']->fetchArray($sClass);
    foreach($classes as $row) $arrayClass[$row['class_id']] = $row['class_name'];
    return $arrayClass;
}

function getTeachers($date=''){
    global $current_user;
    if(!empty($date)){
        $start= date('Y-m-d',strtotime('-31 days '.$date));
        $end  = date('Y-m-d',strtotime('+31 days '.$date));
        $ext  = "INNER JOIN meetings mt ON (mt.deleted = 0) AND (mt.date >= '$start' AND mt.date <= '$end') AND (mt.session_status <> 'Cancelled')";
        if(!empty($current_user->teacher_id)) $ext .= " AND ((mt.teacher_cover_id = '{$current_user->teacher_id}') OR (mt.sub_teacher_id = '{$current_user->teacher_id}') OR (mt.teacher_id = '{$current_user->teacher_id}'))";
        else $ext .= " AND (ct.id IN (mt.teacher_cover_id , mt.sub_teacher_id, mt.teacher_id))";
    }
    $sTeacher = "SELECT DISTINCT IFNULL(ct.id, '') teacher_id,
    IFNULL(ct.full_teacher_name, '') teacher_name
    FROM c_teachers ct $ext
    WHERE ct.deleted = 0 AND ct.type LIKE '%Teacher%'";
    $teachers = $GLOBALS['db']->fetchArray($sTeacher);

    $arrayTeacher = array('' => translate('LBL_NO_TEACHER', 'Calendar'));
    foreach($teachers as $row) $arrayTeacher[$row['teacher_id']] = $row['teacher_name'];
    return $arrayTeacher;
}

function getTas($date=''){
    global $current_user;
    if(!empty($date)){
        $start= date('Y-m-d',strtotime('-31 days '.$date));
        $end  = date('Y-m-d',strtotime('+31 days '.$date));
        $ext  = "INNER JOIN meetings mt ON (mt.deleted = 0) AND (mt.date >= '$start' AND mt.date <= '$end') AND (mt.session_status <> 'Cancelled')";
        if(!empty($current_user->teacher_id)) $ext .= " AND ((mt.teacher_cover_id = '{$current_user->teacher_id}') OR (mt.sub_teacher_id = '{$current_user->teacher_id}') OR (mt.teacher_id = '{$current_user->teacher_id}'))";
        else $ext .= " AND (ct.id IN (mt.teacher_cover_id , mt.sub_teacher_id, mt.teacher_id))";
    }
    $sTeacher = "SELECT DISTINCT IFNULL(ct.id, '') teacher_id,
    IFNULL(ct.full_teacher_name, '') teacher_name
    FROM c_teachers ct $ext
    WHERE ct.deleted = 0 AND ct.type LIKE '%TA%'";
    $teachers = $GLOBALS['db']->fetchArray($sTeacher);
    $arrayTa = array(
        'no_teacher_cover_id' => translate('LBL_NO_TA1', 'Calendar'),
        'no_sub_teacher_id' => translate('LBL_NO_TA2', 'Calendar'),
    );

    foreach($teachers as $row) $arrayTa[$row['teacher_id']] = $row['teacher_name'];
    return $arrayTa;
}

function getRooms($date=''){
    if(!empty($date)){
        $start= date('Y-m-d',strtotime('-31 days '.$date));
        $end  = date('Y-m-d',strtotime('+31 days '.$date));
        $ext  = "INNER JOIN meetings mt ON (mt.deleted = 0) AND (mt.date >= '$start' AND mt.date <= '$end') AND (mt.session_status <> 'Cancelled')";
        $ext .= " AND (cr.id = mt.room_id)";
    }

    $sRoom = "SELECT DISTINCT IFNULL(cr.id, '') room_id,
    IFNULL(cr.name, '') room_name
    FROM c_rooms cr $ext
    WHERE cr.deleted = 0";
    $rooms = $GLOBALS['db']->fetchArray($sRoom);
    //Array Room
    $arrayRoom = array('' => translate('LBL_NO_ROOM', 'Calendar'));
    foreach($rooms as $row) $arrayRoom[$row['room_id']] = $row['room_name'];
    return $arrayRoom;
}



?>
