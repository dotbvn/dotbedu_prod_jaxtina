var round = 0
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,'');
}
//config time slot (timepicker)
var minTime = '6:00am';
var maxTime = '9:30pm';
var step             = 15;
var disableTextInput = true;
var showOn           = ["click","focus"];
var defaultTimeDelta = 90;
var TimeDelta = defaultTimeDelta * 60000;
var targetAction ='';
var targetColor = ''
$(document).ready(function(){
    var lang = window.top.App.lang.getLanguage().substr(0, 2) === 'vn' ? 'vi' : window.top.App.lang.getLanguage().substr(0, 2);
    $.tdtpicker.setLocale(lang);
    $('#picked_date').tdtpicker({
        format: window.top.App.user.getPreference('datepref'),
        dayOfWeekStart: parseInt(window.top.App.user.getPreference('first_day_of_week'), 10),
        step: 15,
        inline: true,
        timepicker: false,
        onSelectDate:function(current_time,$input){
            getDataChangeDate(current_time);
        },
        onChangeMonth:function(current_time,$input){
            getDataChangeDate(current_time);
        },
        onChangeYear:function(current_time,$input){
            getDataChangeDate(current_time);
        }
    });

    $('.open_select_color').click(function () {
        targetAction = this;
        $('#selectColor-dialog').dialog({
            title: "Select color",
            resizable: false,
            width: 'auto',
            height: 'auto',
            modal: true,
            visible: true,
            beforeClose: function (event, ui) {
                if(targetColor != '') {
                    $(targetAction).attr('data-value', targetColor);
                    targetColor = '';
                }
            },
        });
    });
    $('.select_color').click(function () {
        targetColor = $(this).data('value');
        $('#selectColor-dialog').dialog("close");
    })
    $('.btn-view').click(function (){
        $('.btn-group').each(function() {
            var buttonView = $(this).find('.btn-view');
            if(buttonView.hasClass('btn-active')) buttonView.removeClass('btn-active');
        })
        $(this).addClass('btn-active')
    })


});

function getDataChangeDate(ct) {
    tiva_current_year = ct.getFullYear();
    tiva_current_month = ct.getMonth()+1;
    tiva_current_date = ct;
    window.location.href="index.php?module=Calendar&action=index&bwcRedirect=1&view=" + $('.tiva-timetable').attr('data-view') + "&year=" + tiva_current_year + "&month=" + tiva_current_month + "&day=" + tiva_current_date.getDate();
}