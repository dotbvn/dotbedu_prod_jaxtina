
$(document).ready(function () {

    $('#centers, #session_class, #session_teacher, #session_ta, #session_room').multipleSelect({
        width: 146,
        height: 30,
        filter: true,
    });
    setTimeout(showCalendar, 200);
    $('#data-view button').click(function () {
        window.location.href = "index.php?module=Calendar&action=index&bwcRedirect=1&view=" + $(this).attr('data-view') + "&year=" + tiva_current_year + "&month=" + tiva_current_month + "&day=" + tiva_current_date.getDate();
    });
    $('input[name="radioGroup"]').click(function() {
        setDataType();
        if ($('#centers').val() != null && $('#session_class').val() != null) {
            $.ajax({
                url: "index.php?module=Calendar&action=handleAjaxCalendar&dotb_body_only=true",
                type: "POST",
                async: true,
                data: {
                    'dataType'  : dataType,
                    'dataColor' : dataColor,
                    'filter'    : setFilter(),
                    'type'      : "ajaxSaveSettingCalendar"
                },
                dataType: "json",
                success: function (data) {
                    if(!_.isNull(data)){
                        if (data.success) console.log(DOTB.language.get('Calendar','LBL_SAVE_SETINGS_SUCCESS'));
                        else toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                    }
                }
            });
        }
        renderCalendar();
    })
    $('#cal_settings').click(function () {
        $('#setting-dialog').dialog({
            title: "Setting",
            resizable: false,
            width: '450px',
            height: 'auto',
            modal: true,
            visible: true,
            beforeClose: function (event, ui) {
                $("section").css({overflow: 'inherit'});
            },
        });
    });

    //Change Teams
    $('#centers').change(function () {
        var list_center = $("#centers").val();
        $.ajax({
            url: "index.php?module=Calendar&action=handleAjaxCalendar&dotb_body_only=true",
            type: "POST",
            async: true,
            data:{
                'type'          : "ajaxUpdateFilter",
                'selected_date' : $('#picked_date').val(),
                'centers'       : list_center
            },
            dataType: "json",
            success: function (data) {
                if(!_.isNull(data)){
                    refreshDataOption($('#session_class'), data.arrayClass);
                    refreshDataOption($('#session_teacher'), data.arrayTeacher);
                    refreshDataOption($('#session_ta'), data.arrayTa);
                    refreshDataOption($('#session_room'), data.arrayRoom);
                }
            },
        });
    });



    $('#btn-save-settings').click(function () {
        setDataType();
        if ($('#centers').val() != null && $('#session_class').val() != null) {
            $.ajax({
                url: "index.php?module=Calendar&action=handleAjaxCalendar&dotb_body_only=true",
                type: "POST",
                async: true,
                data: {
                    'dataType'  : dataType,
                    'dataColor' : dataColor,
                    'filter'    : setFilter(),
                    'type'      : "ajaxSaveSettingCalendar"
                },
                dataType: "json",
                success: function (data) {
                    if(!_.isNull(data)){
                        if (data.success) console.log(DOTB.language.get('Calendar','LBL_SAVE_SETINGS_SUCCESS'));
                        else toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                    }
                }
            });
        }
        renderCalendar();
    });

    $('#btn-cancel-settings').click(function () {
        $('#show_session').val(true);
        $('#setting-dialog').dialog("close");
    })

    $('#minimizeButton').click(function() {
        let sidebar = $('#sidebar')
        if (sidebar.hasClass('col-2')) {
            $(this).attr('title',DOTB.language.get('Calendar','LBL_SHOW_PANEL'));
            $(this).attr('nav_status','0');
            $('.col-10').stop().animate({'margin-left':'0'},300).attr("class","col-12");$('.col-2').hide("slide",{direction: "left"}, 300);
            sidebar.removeClass('col-2');
        } else {
            sidebar.addClass('col-2');
            $(this).attr('title',DOTB.language.get('Calendar','LBL_HIDE_PANEL'));
            $(this).attr('nav_status','1');
            $('.col-12').stop().animate({'margin-left':'0'},300).attr("class","col-10");$('.col-2').show("slide",{direction: "left"}, 300);
        }
        setDataType();
        $.ajax({
            url: "index.php?module=Calendar&action=handleAjaxCalendar&dotb_body_only=true",
            type: "POST",
            async: true,
            data: {
                'dataType'  : dataType,
                'dataColor' : dataColor,
                'filter'    : setFilter(),
                'type'      : "ajaxSaveSettingCalendar"
            },
            dataType: "json",
        });

    });
    $('#day-tab').click(function() {
        $('.radio-container').hide();
    });
    $('#month-tab').click(function() {
        $('.radio-container').hide();
    });
    $('#week-tab').click(function() {
        $('.radio-container').show();
    });
});
function showCalendar(){
    $('.calendar_settings').show();
}
function renderCalendar() {
    setDataType();
    centers         = $('#centers').val();
    session_class   = $('#session_class').val();
    session_teacher = $('#session_teacher').val();
    session_ta = $('#session_ta').val();
    session_room    = $('#session_room').val();
    display_type    = $('input[name="radioGroup"]:checked').val();
    getDateSeleted();
    $('.tiva-timetable').empty();
    loadTimetable(true);
};

function getDateSeleted() {
    tiva_current_date   = DOTB.util.DateUtils.parse($('#picked_date').val(), cal_date_format);
    tiva_date_seleted   = tiva_current_date;
    tiva_current_month  = tiva_current_date.getMonth() + 1;
    tiva_current_year   = tiva_current_date.getFullYear();
}


function setDataType() {
    dataType.session  = true;
    dataType.view     = $('#data-view').find('.btn-active').attr('data-view');
    dataType.view_as  = $('input[name="radioGroup"]:checked').val();
    dataType.nav_status  = $('#minimizeButton').attr('nav_status');
    dataColor.session = 'dotb-blue';
}

function refreshDataOption(focus, data) {
    var parent = focus.closest('tr');
    parent.find('.ms-parent').remove();
    focus.empty();
    focus.multipleSelect('destroy');
    data.forEach(function (item) {
        var $opt = $('<option />', item)
        focus.append($opt)
    })
    focus.multipleSelect({
        width: 146,
        height: 30,
        filter: true,
    });
    focus.multipleSelect('checkAll');

}

function setFilter() {
    return {
        'centers': $('#centers').val(),
        'session_class': $('#session_class').val(),
        'session_teacher': $('#session_teacher').val(),
        'session_ta': $('#session_ta').val(),
        'session_room': $('#session_room').val(),
        'display_type': $('input[name="radioGroup"]:checked').val(),
    }
}
