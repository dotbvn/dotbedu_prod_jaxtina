// ========== Language ===========
// Day, Month
var wordMonth = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var wordDay_sun = "Sunday";
var wordDay_mon = "Monday";
var wordDay_tue = "Tuesday";
var wordDay_wed = "Wednesday";
var wordDay_thu = "Thursday";
var wordDay_fri = "Friday";
var wordDay_sat = "Saturday";
// ===============================

// Global Variable
dataType = {};
dataColor = {};
var session_class = '';
var session_teacher = '';
var session_ta = '';
var session_room = '';
var centers = '';
var display_type = '';

var tiva_timetables = [];
var tiva_current_date   = new Date();
var tiva_date_seleted   = tiva_current_date;
var tiva_current_month  = tiva_current_date.getMonth() + 1;
var tiva_current_year   = tiva_current_date.getFullYear();
var array_day_list = [];

function sortByEndTime(a, b) {
    if (a.end_time < b.end_time) {
        return -1;
    } else if (a.end_time > b.end_time) {
        return 1;
    } else {
        return 0;
    }
}

function getMinTime(timetables) {
    function sortOnlyTime(a, b) {
        if (parseInt(a.start_hour, 10) < parseInt(b.start_hour, 10)) {
            return -1;
        } else if (parseInt(a.start_hour, 10) > parseInt(b.start_hour, 10)) {
            return 1;
        } else {
            return 0;
        }
    }

    timetables.sort(sortOnlyTime);
    if (timetables.length > 0) return parseInt(timetables[0].start_time, 10);
    return 0;
}

function getMaxTime(timetables) {
    timetables.sort(sortByEndTime);
    for (var i = timetables.length - 1; i >= 0; i--) {
        if (timetables[i].end_time) {
            var time = timetables[i].end_time.split(':');
            if (time[1] == '00') {
                return time[0];
            } else {
                return parseInt(time[0], 10) + 1;
            }
        }
    }
    return 23;
}

function calHour(time) {
    var t = time.split(':');
    return parseInt(t[0], 10) + (t[1] / 60);
}

function getPosition(axis, start_time) {
    return parseInt((calHour(start_time) - axis) * 100, 10); // 1 cell top = 55px;
}

function getHeight(start_time, end_time) {
    return parseInt((calHour(end_time) - calHour(start_time)) * 100, 10); // 1 cell = 55px;
}

function getDayAfter(day, num) {
    var d = tiva_date_seleted;
    return new Date(d.setTime(day.getTime() + (num * 24 * 60 * 60 * 1000)));
}

function sortByTime(a, b) {
    if (new Date(a.date_time) < new Date(b.date_time)) {
        return -1;
    } else if (new Date(a.date_time) > new Date(b.date_time)) {
        return 1;
    } else {
        return 0;
    }
}

function naviClick(id, btn, weekNum, monthNum, yearNum) {
    createTimetable($('#' + id), btn, weekNum, monthNum, yearNum);
}


// Get timetables of day
function getTimetables(tiva_timetables, day, month, year) {
    var timetables = [];
    for (var i = 0; i < tiva_timetables.length; i++) {
        var t = new Date(tiva_timetables[i].date_time);
        if ((t.getDate() == day) && (t.getMonth() + 1 == month) && (t.getFullYear() == year)) {
            timetables.push(tiva_timetables[i]);
        }
    }
    return timetables.sort(sortByTime);
}

// Get timetables of day
function getTimetablesDay(tiva_timetables, day) {
    var timetables = [];
    for (var i = 0; i < tiva_timetables.length; i++) {
        if (tiva_timetables[i].date_db === day) {
            timetables.push(tiva_timetables[i]);
        }
    }
    return timetables.sort(sortByTime);
}

// Change month or year on calendar
function createTimetable(el, btn, start_date, monthNum, yearNum) {
    var view = $('#data-view').find('.btn-active').attr('data-view');
    var view_as = $('input[name="radioGroup"]:checked').val();
    var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
    switch (btn) {
        case 'nextmo':
            var next_month = new Date(yearNum + '-' + monthNum + '-01').addMonths(1);
            getDataChangeDate(next_month);
            break;
        case 'prevmo':
            var last_month = new Date(yearNum + '-' + monthNum + '-01').addMonths(-1);
            getDataChangeDate(last_month);
            break;
        case 'prevwe':
            var last_week = new Date(start_date).addDays(-7);
            getDataChangeDate(last_week);
            break;
        case 'nextwe':
            var next_week = new Date(start_date).addDays(7);
            getDataChangeDate(next_week);
            break;
        case 'prevda':
            var yesterday = new Date(start_date).addDays(-1);
            getDataChangeDate(yesterday);
            break;
        case 'nextda':
            var tomorrow = new Date(start_date).addDays(1);
            getDataChangeDate(tomorrow);
            break;
    }
    if (btn == 'current') {
        $('.lds-ellipsis').show()
        switch (view) {
            case 'day':
                timetableList(el, tiva_timetables, getFirstDateOfWeek(start_date));
                break;
            case 'week':
                if (date_start === 'sunday'){
                    if(new Date(start_date).is().sunday()) firstDate = start_date;
                    else firstDate = new Date(start_date).last().sunday();
                }else{
                    if(new Date(start_date).is().monday()) firstDate = start_date;
                    else firstDate = new Date(start_date).last().monday();
                }
                if(view_as === 'time'){
                    timetableWeek(el, tiva_timetables, firstDate);
                } else {
                    timetableWeekType2(el, tiva_timetables, firstDate);
                }
                break;
            case 'month':
                var firstDate = new Date(start_date).moveToFirstDayOfMonth();
                var lastDate = new Date(start_date).moveToLastDayOfMonth();
                var numbDays = lastDate.getDate();
                if (date_start === 'sunday') {
                    firstDate = firstDate.getDay() + 1;
                } else {
                    firstDate = firstDate.getDay() === 0 ? 7 : firstDate.getDay();
                }
                timetableMonth(el, tiva_timetables, firstDate, numbDays, monthNum, yearNum);
                break;
        }
        $('.lds-ellipsis').hide()
    }
}


// Create timetable week
function timetableWeek(el, tiva_timetables, firstDayWeek) {
    var firstWeek = new Date(firstDayWeek);
    var firstWeekMonth = firstWeek.getMonth() + 1;
    var firstWeekYear = firstWeek.getFullYear();

    var lastWeek = getDayAfter(firstWeek, 6);
    var lastWeekMonth = lastWeek.getMonth() + 1;
    var lastWeekYear = lastWeek.getFullYear();
    var view = $('#data-view').find('.btn-active').attr('data-view');

    var d;
    var date;
    var month;
    var year;

    var first;
    var last;
    var header_time;

    // Set wordDay
    var wordDay;
    var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
    var timetableString = "";

    if (date_start == 'sunday') {
        wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
    } else { // Start with Monday
        wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
    }

    //lay tuan custom
    var week_nav;
    if ((firstWeekMonth != lastWeekMonth) && (firstWeekYear != lastWeekYear)) {
        week_nav = app.lang.getAppListStrings('list_month')[wordMonth[firstWeekMonth - 1]] + ' ' + firstWeekYear + ' - ' + app.lang.getAppListStrings('list_month')[wordMonth[lastWeekMonth - 1]] + ' ' + lastWeekYear;
    } else if (firstWeekMonth != lastWeekMonth) {
        week_nav = app.lang.getAppListStrings('list_month')[wordMonth[firstWeekMonth - 1]] + ' ' + ' - ' + app.lang.getAppListStrings('dom_cal_month_short')[lastWeekMonth] + ' ' + firstWeekYear;
    } else {
        week_nav = app.lang.getAppListStrings('list_month')[wordMonth[firstWeekMonth - 1]] + ' ' + ' - ' + ' ' + firstWeekYear;
    }
    if (!(el.attr('data-nav') === 'hide') && view != 'day') {
        $('#default_header').html('<div class="time-navigation">'
            + '<span class="navi-icon navi-prev" onClick="naviClick(\'' + el.attr('id') + '\', \'prevwe\', \'' + firstWeek + '\', ' + firstWeekMonth + ', ' + firstWeekYear + ')">&#10094;</span>'
            + '<span class="navi-time">' + week_nav + '</span>'
            + '<span class="navi-icon navi-next" onClick="naviClick(\'' + el.attr('id') + '\', \'nextwe\', \'' + firstWeek + '\', ' + firstWeekMonth + ', ' + firstWeekYear + ')">&#10095;</span>'
            + '</div>');
    }
    // Get min, max time
    var min_time = getMinTime(tiva_timetables);
    var max_time = getMaxTime(tiva_timetables);
    var show_time = (el.attr('data-header-time') === 'hide') ? '' : 'show-time';

    timetableString += '<div class="timetable-week ' + show_time + '">';
    timetableString += '<div class="timetable-axis">';
    for (let n = min_time; n <= max_time; n++) {
        if (n === 0) timetableString += '<div class="axis-item">GMT+07</div>';
        else {
            var hour = n;
            if (n > 12) hour = hour - 12;
            timetableString += '<div class="axis-item">' + hour + ':00 ' + (n >= 12 ? 'pm' : 'am') + ' </div>';
        }
    }
    timetableString += '</div>';

    timetableString += '<div class="timetable-columns">';
    for (var m = 0; m < wordDay.length; m++) {
        // Caculate date of week
        d = getDayAfter(firstWeek, m);
        date = d.getDate();
        month = d.getMonth() + 1;
        year = d.getFullYear();
        header_time = (el.attr('data-header-time') == 'hide') ? '' : '<p></p><span>' + ' ' + date + '</span>';
        first = (m == 0) ? 'first-column' : '';
        last = (m == wordDay.length - 1) ? 'last-column' : '';

        var current_date = new Date();
        var todaysDate = current_date.getDate();
        var current_month = current_date.getMonth() + 1;
        var current_year = current_date.getFullYear();
        var class_today = ((todaysDate == date) && (current_month == month) && (current_year == year)) ? 'week-today' : '';
        timetableString += '<div class="timetable-column">';
        // Header

        if (screen.width > 768) {
            timetableString += '<div class="timetable-header ' + class_today + ' ' + last + '">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]] + header_time + '</div>';
        } else {
            timetableString += '<div class="timetable-header ' + class_today + ' ' + last + '">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]] + header_time + '</div>';
        }

        // Content
        timetableString += '<div class="timetable-column-content">';

        // Get timetables of day
        var timetables;
        if (el.attr('data-mode') == 'day') {
            timetables = getTimetablesDay(tiva_timetables, dayArr[m]);
        } else {
            timetables = getTimetables(tiva_timetables, date, month, year);
        }
        for (var t = 0; t < timetables.length; t++) {
            if (timetables[t].start_time && timetables[t].end_time) {
                var array_by_time_start = timetables.filter(data => data['start_hour'] === timetables[t]['start_hour']);
                var item_index = array_by_time_start.indexOf(timetables[t]);

                if (item_index < 3) {
                    var position = getPosition(min_time, timetables[t].start_hour) + (23 * item_index);
                    var _row_day = timetables[t].row_day.replace('__top','top:'+position+'px;position: absolute;width: 96%;margin-top: 5px');

                    timetableString += _row_day;
                } else {
                    if (item_index === 3) {
                        let position   = getPosition(min_time, timetables[t].start_hour) + (23 * 3);
                        let date_focus = timetables[t].date;
                        let week_date  = timetables[t].week_date_l;
                        const date_hour = timetables[t].start_hour.split(':')[0];
                        timetableString += '<div class="timetable-item" style="display: flex;top:' + position + 'px;position: absolute;width: 96%;margin-top: 5px"><a class="open-popup-link showmore-action" href="#modal_showmore_' + timetables[t].date_db + '-' + date_hour + '">'+DOTB.language.get('Calendar','LBL_MORE')+'(' + array_by_time_start.length + ')</a>';
                        timetableString += '<div id="modal_showmore_' + timetables[t].date_db + '-' + date_hour + '" class="timetable-popup zoom-anim-dialog mfp-hide">'
                        timetableString += '<div style="border-top-left-radius:10px;border-top-right-radius: 10px" class="popup-header" data-value="dotb-mint">' +week_date+', '+ date_focus + ' ' + date_hour + ':00 </div>';
                        timetableString += '<div class="popup-body tiva-timetable">';
                        timetableString += '<div class="popup-body timetable-month">'
                        timetableString += array_day_list[timetables[t].date_db + '-' + timetables[t].start_hour]
                        timetableString += '</div>';
                        timetableString += '</div>';
                        timetableString += '<button title="Close (Esc)" type="button" class="mfp-close">×</button>';
                        timetableString += '</div>';
                        timetableString += '</div>';
                    }
                }
                timetableString += timetables[t].popup_modal;
            }
        }
        timetableString += '</div>';

        // Grid
        timetableString += '<div class="timetable-column-grid">';
        for (var n = min_time; n < max_time; n++) {
            timetableString += '<div class="grid-item ' + first + '"></div>';
        }
        timetableString += '</div>';
        timetableString += '</div>';
    }
    timetableString += '</div>';

    timetableString += '</div>';

    el.html(timetableString);

    // Popup
    el.find('.timetable-title').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        mainClass: 'my-mfp-zoom-in'
    });
    //showmore
    el.find('.showmore-action').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        mainClass: 'my-mfp-zoom-in',
        key: 'showmore',
        modal: true
    });
}

function timetableWeekType2(el, tiva_timetables, firstDayWeek) {
    var firstWeek = new Date(firstDayWeek);
    var firstWeekMonth = firstWeek.getMonth() + 1;
    var firstWeekYear = firstWeek.getFullYear();

    var lastWeek = getDayAfter(firstWeek, 6);
    var lastWeekMonth = lastWeek.getMonth() + 1;
    var lastWeekYear = lastWeek.getFullYear();
    var view = $('#data-view').find('.btn-active').attr('data-view');

    var d;
    var date;
    var month;
    var year;

    var first;
    var last;
    var header_time;

    // Set wordDay
    var wordDay;
    var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
    var timetableString = "";

    if (date_start == 'sunday') {
        wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
    } else { // Start with Monday
        wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
    }

    //lay tuan custom
    var week_nav;
    if ((firstWeekMonth != lastWeekMonth) && (firstWeekYear != lastWeekYear)) {
        week_nav = app.lang.getAppListStrings('list_month')[wordMonth[firstWeekMonth - 1]] + ' ' + firstWeekYear + ' - ' + app.lang.getAppListStrings('list_month')[wordMonth[lastWeekMonth - 1]] + ' ' + lastWeekYear;
    } else if (firstWeekMonth != lastWeekMonth) {
        week_nav = app.lang.getAppListStrings('list_month')[wordMonth[firstWeekMonth - 1]] + ' ' + ' - ' + app.lang.getAppListStrings('dom_cal_month_short')[lastWeekMonth] + ' ' + firstWeekYear;
    } else {
        week_nav = app.lang.getAppListStrings('list_month')[wordMonth[firstWeekMonth - 1]] + ' ' + ' - ' + ' ' + firstWeekYear;
    }
    if (!el.attr('data-nav') === 'hide' && view != 'day') {
        $('#default_header').html('<div class="time-navigation">'
            + '<span class="navi-icon navi-prev" onClick="naviClick(\'' + el.attr('id') + '\', \'prevwe\', \'' + firstWeek + '\', ' + firstWeekMonth + ', ' + firstWeekYear + ')">&#10094;</span>'
            + '<span class="navi-time">' + week_nav + '</span>'
            + '<span class="navi-icon navi-next" onClick="naviClick(\'' + el.attr('id') + '\', \'nextwe\', \'' + firstWeek + '\', ' + firstWeekMonth + ', ' + firstWeekYear + ')">&#10095;</span>'
            + '</div>');
    }
    var show_time = (el.attr('data-header-time') === 'hide') ? '' : 'show-time';
    timetableString += '<div class="timetable-week ' + show_time + '">';
    timetableString += '<div class="timetable-columns-2">';
    timetableString += '<div class="timetable-column-3" style="width: 22.1%;position: relative"></div>';
    for (var m = 0; m < wordDay.length; m++) {
        // Caculate date of week
        d = getDayAfter(firstWeek, m);
        date = d.getDate();
        month = d.getMonth() + 1;
        year = d.getFullYear();
        header_time = (el.attr('data-header-time') == 'hide') ? '' : '<p></p><span>' + ' ' + date + '</span>';
        var current_date = new Date();
        var todaysDate = current_date.getDate();
        var current_month = current_date.getMonth() + 1;
        var current_year = current_date.getFullYear();
        var class_today = ((todaysDate == date) && (current_month == month) && (current_year == year)) ? 'week-today' : '';
        timetableString += '<div class="timetable-column">';
        // Header
        let clsHeader = 'timetable-header-2 ' + class_today;
        if(m === 0){
            clsHeader = 'timetable-header-first-2 ' + class_today;
        }
        let content = app.lang.getAppListStrings('list_date_long')[wordDay[m]] + header_time;
        if (screen.width > 768) {
            timetableString += '<div class="' + clsHeader + '">' + content + '</div>';
        } else {
            timetableString += '<div class="' + clsHeader + '">' + content + '</div>';
        }
        timetableString += '</div>';
    }
    timetableString += '</div>';
    timetableString += '<div class="timetable-axis-2">';
    let count = 0;
    let countTime = 0;
    Object.keys(tiva_timetables).forEach (function(name){
        timetableString += '<div class="item-axis-container">';
        var cls = 'item-axis-2'
        if(count === 0){
            cls = 'item-axis-2-first';
        }
        var url = 'index.php?module=C_Teachers&action=DetailView&record=' + tiva_timetables[name].id;
        if(display_type == 'room') url = '#';
        timetableString += '<div class="' + cls + '" style="height: ' + Object.keys(tiva_timetables[name].session).length*77 + 'px">';
        timetableString += '<a href="' + url + '" style="text-decoration: none">' + name + '</a>'
        timetableString += '</div>';

        timetableString += '<div class="time-container">'
        Object.keys(tiva_timetables[name].session).forEach(function(time) {
            var clsTime = 'time-axis'
            if(countTime === 0 && count === 0){
                clsTime = 'time-axis-first';
            }
            timetableString += '<div class="timetable-content-time">';
            timetableString += '<div class="' + clsTime + '">' + time + '</div>';
            for(let i = 1; i <= 7; i++){
                let cellId = tiva_timetables[name].id + '|' + time + '|' + i;
                let clasGrid = i === 1 ? 'grid-item-2-first' : 'grid-item-2'
                let content = '';
                (tiva_timetables[name].session[time]).forEach(function(element){
                    if(parseInt(element.wd_n) === i){
                        content = element.row_day
                        timetableString += element.popup_modal
                    }
                })
                timetableString += '<div class=' + clasGrid + ' id="' + cellId + '" >' + content + '</div>'
            }
            timetableString += '</div>';
            countTime++;
        })
        timetableString += '</div>'
        count++;
        timetableString += '</div>';
    })
    timetableString += '</div>';

    timetableString += '</div>';

    el.html(timetableString);

    // Popup
    el.find('.timetable-title-2').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        mainClass: 'my-mfp-zoom-in'
    });
    el.find('.timetable-title-2-center').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        mainClass: 'my-mfp-zoom-in'
    });
}

// Create timetable list
function timetableList(el, tiva_timetables, firstDayWeek) {
    var dataListType = $('.tiva-timetable').attr('data-view');
    var firstWeek = new Date(firstDayWeek);
    var firstWeekDate = firstWeek.getDate();
    var firstWeekMonth = firstWeek.getMonth() + 1;
    var firstWeekYear = firstWeek.getFullYear();

    var lastWeek = getDayAfter(firstWeek, 6);
    var lastWeekDate = lastWeek.getDate();
    var lastWeekMonth = lastWeek.getMonth() + 1;
    var lastWeekYear = lastWeek.getFullYear();
    var view = $('#data-view').find('.btn-active').attr('data-view');

    var dayArr;
    var d;
    var date;
    var month;
    var year;

    var header_time;
    //lay ngay da chon
    var date_selected = DOTB.util.DateUtils.parse($('#picked_date').val(), cal_date_format); //Format by User DateFormat
    var year_selected = date_selected.getFullYear();
    var month_selected = date_selected.getMonth();
    var day_selected = date_selected.getDate();

    // Set wordDay
    var wordDay;
    var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
    var timetableString = "";

    if (date_start == 'sunday') {
        wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
        dayArr = new Array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
    } else { // Start with Monday
        wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
        dayArr = new Array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
    }

    var week_nav;
    var cr_language = app.lang.getLanguage();
    if (dataListType == 'day') {
        if(cr_language == 'vn_vn')
            week_nav = day_selected + ' ' + app.lang.getAppListStrings('list_month')[wordMonth[month_selected]] + ', ' + year_selected;
        else
            week_nav = app.lang.getAppListStrings('list_month')[wordMonth[month_selected]] + ' ' + day_selected + ', ' + year_selected;
    } else if ((firstWeekMonth != lastWeekMonth) && (firstWeekYear != lastWeekYear)) {
        week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ', ' + firstWeekYear
        + ' - ' + wordMonth[lastWeekMonth - 1].substring(0, 3) + ' ' + lastWeekDate + ', ' + lastWeekYear;
    } else if (firstWeekMonth != lastWeekMonth) {
        week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ' - ' + wordMonth[lastWeekMonth - 1].substring(0, 3) + ' ' + lastWeekDate + ', ' + firstWeekYear;
    } else {
        week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ' - ' + lastWeekDate + ', ' + firstWeekYear;
    }

    if (!(el.attr('data-nav') == 'hide') && view== 'day') {
        $('#default_header').html('<div class="time-navigation">'
            + '<span class="navi-icon navi-prev" onClick="naviClick(\'' + el.attr('id') + '\', \'prevda\', \'' + date_selected + '\', ' + firstWeekMonth + ', ' + firstWeekYear + ')">&#10094;</span>'
            + '<span class="navi-time">' + week_nav + '</span>'
            + '<span class="navi-icon navi-next" onClick="naviClick(\'' + el.attr('id') + '\', \'nextda\', \'' + date_selected + '\', ' + firstWeekMonth + ', ' + firstWeekYear + ')">&#10095;</span>'
            + '</div>');
    }
    var timetables;
    timetableString += '<div class="timetable-list">';
    for (var m = 0; m < wordDay.length; m++) {
        // Caculate date of week
        d = getDayAfter(firstWeek, m);
        date = d.getDate();
        month = d.getMonth() + 1;
        year = d.getFullYear();
        //if list date

        if (dataListType == 'day')
            if (day_selected != date || (month_selected+1) != month || year_selected != year)
                continue;
        // Get timetables of day
        if (el.attr('data-mode') == 'day') {
            timetables = getTimetablesDay(tiva_timetables, dayArr[m]);
        } else {
            timetables = getTimetables(tiva_timetables, date, month, year);
        }


        if (timetables.length > 0) {
            timetableString += '<div class="timetable-day">';

            //css ngay hien tai
            var current_date = new Date();
            var todaysDate = current_date.getDate();
            var current_month = current_date.getMonth() + 1;
            var current_year = current_date.getFullYear();
            var class_today = ((todaysDate == date) && (current_month == month) && (current_year == year)) ? 'week-today' : '';
            // Header
            if(cr_language == 'vn_vn')
                header_time = (el.attr('data-header-time') == 'hide') ? '' : '<span>' + date + ' ' + app.lang.getAppListStrings('list_month')[wordMonth[month - 1]] + ', ' + year + '</span>';
            else
                header_time = (el.attr('data-header-time') == 'hide') ? '' : '<span>' + app.lang.getAppListStrings('list_month')[wordMonth[month - 1]] + ' ' + date + ', ' + year + '</span>';
            timetableString += '<div class="timetable-header' + ' ' + class_today + '">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]] + header_time + '</div>';
            // Get timetables of day
            timetableString += '<div class="timetable-content">';
            for (var t = 0; t < timetables.length; t++) {
                if (timetables[t].start_time && timetables[t].end_time) {
                    timetableString += timetables[t].row_day;
                    timetableString += timetables[t].popup_modal;
                }
            }
            timetableString += '</div>';
            timetableString += '</div>';
        }
    }
    timetableString += '</div>';

    el.html(timetableString);

    // Popup
    el.find('.timetable-title').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        mainClass: 'my-mfp-zoom-in'
    });
}

// Create timetable month
function timetableMonth(el, tiva_timetables, firstDay, numbDays, monthNum, yearNum) {
    var current_date = new Date();
    var todaysDate = current_date.getDate();
    var current_month = current_date.getMonth() + 1;
    var current_year = current_date.getFullYear();
    var wordDay;
    var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
    var timetableString = "";
    var daycounter = 0;

    if (date_start == 'sunday') {
        wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
    } else { // Start with Monday
        wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
    }

    if (!(el.attr('data-nav') == 'hide')) {
        $('#default_header').html('<div class="time-navigation">'
            + '<span class="navi-icon navi-prev" onClick="naviClick(\'' + el.attr('id') + '\', \'prevmo\', \'\', ' + monthNum + ', ' + yearNum + ')">&#10094;</span>'
            + '<span class="navi-time">' + app.lang.getAppListStrings('list_month')[wordMonth[monthNum - 1]] + '&nbsp;&nbsp;' + yearNum + '</span>'
            + '<span class="navi-icon navi-next" onClick="naviClick(\'' + el.attr('id') + '\', \'nextmo\', \'\', ' + monthNum + ', ' + yearNum + ')">&#10095;</span>'
            + '</div>');
    }

    timetableString += '<table class="timetable-month">';
    timetableString += '<thead>';
    for (var m = 0; m < wordDay.length; m++) {
        if (screen.width > 768) {
            timetableString += '<th class="timetable-header">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]] + '</th>';
        } else {
            timetableString += '<th class="timetable-header">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]].substring(0, 3) + '</th>';
        }
    }
    timetableString += '</thead>';
    timetableString += '<tbody>';
    timetableString += '</tr>';
    var thisDate = 1;
    var class_today;

    for (var i = 1; i <= 6; i++) {
        var k = (i - 1) * 7 + 1;
        if (k < (firstDay + numbDays)) {
            timetableString += '<tr>';
            for (var x = 1; x <= 7; x++) {
                daycounter = (thisDate - firstDay) + 1;
                thisDate++;
                class_today = ((todaysDate == daycounter) && (current_month == monthNum) && (current_year == yearNum)) ? 'today' : '';
                timetableString += '<td class="calendar-day ' + class_today + '">';
                if ((daycounter <= numbDays) && (daycounter >= 1)) {
                    timetableString += '<div class="calendar-daycounter">' + daycounter + '</div>';
                }

                // Get timetables of day
                var timetables = getTimetablesDay(tiva_timetables, yearNum + '-' + (monthNum.toString().padStart(2, '0')) + '-' + (daycounter.toString().padStart(2, 0)));
                for (var t = 0; t < timetables.length; t++) {
                    if (t < 5) {
                        timetableString += timetables[t].row_day;
                    } else {
                        if (t === 5) {
                            timetableString += '<div class="timetable-item"><a class="open-popup-link showmore-action" href="#modal_showmore_' + timetables[t].date_db + '">'+DOTB.language.get('Calendar','LBL_MORE')+'(' + timetables.length + ')</a>';
                            timetableString += '<div id="modal_showmore_' + timetables[t].date_db + '" class="timetable-popup zoom-anim-dialog mfp-hide">'
                            timetableString += '<div style="border-top-left-radius:10px;border-top-right-radius: 10px" class="popup-header" data-value="dotb-mint">' + timetables[t].week_date_l+', '+timetables[t].date + '</div>';
                            timetableString += '<div class="popup-body tiva-timetable" style="border-top-left-radius: 0px;border-top-right-radius: 0;">';
                            timetableString += '<div class="popup-body timetable-month">'
                            timetableString += array_day_list[timetables[t].date_db]
                            timetableString += '</div>';
                            timetableString += '</div>';
                            timetableString += '<button title="Close (Esc)" type="button" class="mfp-close">×</button>';
                            timetableString += '</div>';
                            timetableString += '</div>';
                        }
                    }
                    timetableString += timetables[t].popup_modal;
                }
                timetableString += '</td>';
            }
            timetableString += '</tr>';
        }
    }
    timetableString += '</tbody>';
    timetableString += '</table>';

    el.html(timetableString);

    // Popup
    el.find('.timetable-title').magnificPopup({
        index: 1,
        type: 'inline',
        removalDelay: 500,
        mainClass: 'my-mfp-zoom-in',
        key: 'detail',
    });
    //showmore
    el.find('.showmore-action').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        mainClass: 'my-mfp-zoom-in',
        key: 'showmore',
        modal: true
    });
}

var globalContain;
$(document).ready(function () {
    let app = window.parent.DOTB.App;
    app.alert.show('loading', {
        level: 'process',
        title: 'loading'
    })
    renderCalendar();
    app.alert.dismiss('loading');
});

function loadTimetable(loading = false) {
    $('.tiva-timetable').each(function (index) {

        // Set id for timetable
        $(this).attr('id', 'timetable-' + (index + 1));

        // Get timetables from json file or ajax php
        var source = (typeof $(this).attr('data-source') != 'undefined') ? $(this).attr('data-source') : 'json';
        var timetable_contain = $(this);
        globalContain = $(this);

        if (source == 'json') { // Get timetables from json file
            if (mode == 'day') {
                var timetable_json = 'timetable/timetables_day.json';
            } else {
                var timetable_json = 'index.php?module=Calendar&action=getjson&dotb_body_only=true';
            }

            $.getJSON(timetable_json, function (data) {
                // Init timetables variable
                tiva_timetables = [];

                for (var i = 0; i < data.items.length; i++) {
                    tiva_timetables.push(data.items[i]);
                }

                // Sort timetables by date
                tiva_timetables.sort(sortByTime);

                for (var j = 0; j < tiva_timetables.length; j++) {
                    tiva_timetables[j].id = j;
                }

                // Create timetable
                var todayDate = tiva_date_seleted;
                var date_start = (typeof timetable_contain.attr('data-start') != "undefined") ? timetable_contain.attr('data-start') : 'sunday';
                if (date_start == 'sunday') {
                    var tiva_current_week = new Date(todayDate.setDate(tiva_current_date.getDate() - todayDate.getDay()));
                } else {
                    var today_date = (todayDate.getDay() == 0) ? 7 : todayDate.getDay();
                    var tiva_current_week = new Date(todayDate.setDate(tiva_current_date.getDate() - today_date + 1));
                }
                createTimetable(timetable_contain, 'current', tiva_current_week, tiva_current_month, tiva_current_year);
            });
        } else {
            var view = $('#data-view').find('.btn-active').attr('data-view');
            if (loading) $('.lds-ellipsis').show();
            $.ajax({
                url: "index.php?module=Calendar&action=handleAjaxCalendar&dotb_body_only=true",
                type: "POST",
                async: true,
                data:
                {
                    'type': "ajaxGetData",
                    'centers': centers,
                    'session_class': session_class,
                    'session_teacher': session_teacher,
                    'session_ta': session_ta,
                    'session_room': session_room,
                    'dataType': dataType,
                    'dataColor': dataColor,
                    'displayType': display_type,
                    'select_date': {
                        'day': tiva_date_seleted.getDate(),
                        'month': tiva_current_month,
                        'year': tiva_current_year,
                        'view': view,
                        'week': tiva_current_date.getWeek()
                    },

                },
                dataType: "json",
                success: function (data) {
                    $('.lds-ellipsis').hide();
                    if (data.success == 1) {
                        tiva_timetables = data.timetables;
                        array_day_list = data.showmore;

                        for (var j = 0; j < tiva_timetables.length; j++) {
                            tiva_timetables[j].id = j;
                        }
                        // Create timetable
                        createTimetable(timetable_contain, 'current', tiva_date_seleted, tiva_current_month, tiva_current_year);
                        if(data.count == 0) toastr.error(DOTB.language.get('Calendar','LBL_NO_RESULTS_DES'),DOTB.language.get('Calendar','LBL_NO_RESULTS'));
                        else toastr.info('',data.count+DOTB.language.get('Calendar','LBL_RESULTS_FOUND'));
                    } else
                        toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                },
            });


        }
    });
};
//Add by HoangHvy
function getFirstDateOfWeek(date) {
    let start = new Date(date);
    let day = start.getDay();
    let adjust = day === 0 ? 6 : day - 1;
    start.setDate(date.getDate() - adjust);
    return start;
}
//end
function activeButton() {
    let position = '';
    var view = (typeof $('#data-view-source').attr('data-view') != 'undefined') ? $('#data-view-source').attr('data-view') : 'month';
    if (view != 'list')
        position = '#' + view + '-tab';
    else position = '#time-line-tab'

    $(position).addClass('btn-active');
}

