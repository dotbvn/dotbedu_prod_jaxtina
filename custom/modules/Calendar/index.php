<?php
require_once("custom/modules/Calendar/_helper.php");
global $current_user, $timedate, $dotb_config;


$dataYear        = isset($_GET['year']) ? $_GET['year']  : date('Y');
$dataMonth       = isset($_GET['month'])? $_GET['month'] : date('n');
$dataDay         = isset($_GET['day'])  ? $_GET['day']   : date('d');
$dataGet['date'] = isset($_GET['day'])  ? ($timedate->to_display_date("$dataYear-$dataMonth-$dataDay",false)) : $timedate->nowDate();
$date = $timedate->convertToDBDate($dataGet['date']);

//get list of team by permission
$is_admin = $current_user->isAdmin();
if(!$is_admin) $teamIds = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(l2.id, '') team_id, IFNULL(l2.name, '') team_name
    FROM users INNER JOIN team_memberships l2_1 ON users.id = l2_1.user_id AND l2_1.deleted = 0
    INNER JOIN teams l2 ON l2.id = l2_1.team_id AND l2.deleted = 0
    WHERE ((users.id = '{$current_user->id}') AND l2.private = 0) AND users.deleted = 0");
else $teamIds = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(id, '') team_id, IFNULL(name, '') team_name FROM teams WHERE deleted = 0 AND private = 0");
foreach($teamIds as $row) $list_center[$row['team_id']] = $row['team_name'];
$filter_center = json_decode($current_user->getPreference('centers', 'calender_filter'),true);
if(empty($filter_center)) $filter_center = array_keys($list_center);

$filter_modified = date_create($current_user->getPreference('filter_modified', 'calender_filter'));
$now = date_create($timedate->nowDb());
$diff = date_diff($now, $filter_modified);

$use_filter = true;
if($filter_modified != false && $diff->h >3 ) //Hours
    $use_filter = false;



//Classes in center
$arrayClass = getClasses(array_column($teamIds,'team_id'), $date);
if($use_filter) $filter_class = json_decode($current_user->getPreference('session_class', 'calender_filter'),true);
if(!empty($_GET['class_id'])) $filter_class = array($_GET['class_id']);
if(empty($filter_class)) $filter_class = array_keys($arrayClass);

//Teachers in center
$arrayTeacher = getTeachers($date);
if($use_filter) $filter_teacher = json_decode($current_user->getPreference('session_teacher', 'calender_filter'),true);
if(empty($filter_teacher)) $filter_teacher = array_keys($arrayTeacher);

//Tas in center
$arrayTa = getTas($date);
if($use_filter) $filter_ta = json_decode($current_user->getPreference('session_ta', 'calender_filter'),true);
if(empty($filter_ta)) $filter_ta = array_keys($arrayTa);

//Rooms in Center
$arrayRoom = getRooms($date);
if($use_filter) $filter_room = json_decode($current_user->getPreference('session_room', 'calender_filter'),true);
if(empty($filter_room)) $filter_room = array_keys($arrayRoom);


//action color
$dataColor['session'] = (!empty($current_user->getPreference('color_session'))) ? $current_user->getPreference('color_session') : 'dotb-blue';

//Unknown
$dataType['session'] = (!empty($current_user->getPreference('show_session'))) ? $current_user->getPreference('show_session') : $dotb_config['calendar']['show_session_by_default'];

//get/set view
$user_view = (!empty($current_user->getPreference('calendar_view'))) ? $current_user->getPreference('calendar_view') : $dotb_config['calendar']['default_view'];
$dataType['view'] = $_GET['view'];
if(empty($dataType['view'])) $dataType['view'] = $user_view;
elseif($user_view != $dataType['view'])// Trường hợp đổi view set lại
    $current_user->setPreference('calendar_view', $dataType['view'], 0, 'global', $current_user);
$user_view_as = (!empty($current_user->getPreference('calendar_view_as'))) ? $current_user->getPreference('calendar_view_as') : 'time';
$dataType['view_as'] = $_GET['view_as'];
if(empty($dataType['view_as'])) $dataType['view_as'] = $user_view_as;
elseif($user_view_as != $dataType['view_as'])
    $current_user->setPreference('calendar_view_as', $dataType['view_as'], 0, 'global', $current_user);

$user_nav_status = $current_user->getPreference('calendar_nav_status');
if(!isset($user_nav_status)) $dataType['nav_status'] = '1';
else $dataType['nav_status'] = $user_nav_status;


$view = array (
    0 =>
    array (
        'name' => 'day',
        'label' => 'LBL_DAY',
    ),
    1 =>
    array (
        'name' => 'week',
        'label' => 'LBL_WEEK',
    ),
    2 =>
    array (
        'name' => 'month',
        'label' => 'LBL_MONTH',
));
//Label list
$label = array();
$label['time'] = translate('LBL_TIME', 'Calendar');
$label['room'] = translate('LBL_ROOM', 'Calendar');
$label['teacher'] = translate('LBL_TEACHER', 'Calendar');
$label['titRadio'] = translate('LBL_TITLE_RADIO', 'Calendar');
$label['minimizeButton'] = translate('LBL_CLOSE_DIALOG', 'Calendar');

$a = new Dotb_Smarty();
$a->assign('first_day_of_week', $current_user->get_first_day_of_week());
$a->assign('MOD', $GLOBALS['mod_strings']);
$a->assign('type_list', $GLOBALS['app_list_strings']['calendar_type_list']);
$a->assign('array_teams', $list_center);
$a->assign('array_class', $arrayClass);
$a->assign('array_teacher', $arrayTeacher);
$a->assign('array_ta', $arrayTa);
$a->assign('array_room', $arrayRoom);
$a->assign('filter_center', $filter_center);
$a->assign('filter_class', $filter_class);
$a->assign('filter_teacher', $filter_teacher);
$a->assign('filter_ta', $filter_ta);
$a->assign('filter_room', $filter_room);
$a->assign('view_as', $filter_room);
$a->assign('dataType', $dataType);
$a->assign('data_get', $dataGet);
$a->assign('dataColor', $dataColor);
$a->assign('config_calender_view', $view);
$a->assign('label', $label);
$html = $a->fetch("custom/modules/Calendar/tpls/calendar.tpl");
echo $html;
