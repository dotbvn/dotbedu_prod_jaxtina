<div class="timetable-item-2" title="{$data.class_name} {if $data.teacher_name != ''} | {$data.teacher_name} {/if}{if $data.ta_name != ''} | {$data.ta_name} {/if}{if $data.room_name != ''} | {$data.room_name} {/if}">
        <a class="timetable-title-2 open-popup-link" href="#modal-{$data.meeting_id}">
            <span class="class-name">{$data.class_name}</span>
            {if $data.display_type == 'room'}
                <span class="timetable-room-name" style="font-size: 13px" title="{$MOD.LBL_TEACHER_NAME}" >{$data.teacher_name}</span>
            {else}
                <span class="timetable-room-name" style="font-size: 13px" title="{$MOD.LBL_TEACHER_NAME}" >{$data.room_name}</span>
            {/if}
        </a>
</div>
