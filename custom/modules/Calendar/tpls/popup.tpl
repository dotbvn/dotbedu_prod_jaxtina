<div id="modal-{$data.meeting_id}" class="timetable-popup zoom-anim-dialog mfp-hide">
<div style="border-top-left-radius:20px;border-top-right-radius: 20px" class="popup-header" data-value="{$data.color}">{$data.class_name} <a style="font-size: inherit;" target="_parent" href="{$uri}#J_Class/{$data.class_id}"><i class="far fa-external-link btn-link-calendar"></i></a></div>
<div class="popup-body">
    <div class="timetable-row">
        <div class="timetable-value col-6"><span class="title"><i class="far fa-calendar-alt"></i>{$data.week_date_s}, {$data.date}</span></div>
        <div class="timetable-value col-6"><span class="title"><i class="far fa-clock-o"></i>{$data.time_start} - {$data.time_end}</span></div>
    </div>
    <hr style="margin: 0">

    <div class="timetable-row">
        <div class="timetable-value col-6"><span class="normal"><i class="far fa-school"></i>{$MOD.LBL_CENTER_NAME}: <span class="cover_link">{$data.center_name}</span></span></div>
        <div class="timetable-value col-6"><span class="normal"><i class="far fa-chalkboard-teacher"></i>{$MOD.LBL_TEACHER_NAME}: <span class="cover_link">{$data.teacher_name}</span></span></div>
    </div>
    <div class="timetable-row">
        <div class="timetable-value col-6"><span class="normal"><i class="far fa-chalkboard-teacher"></i>{$MOD.LBL_TEACHER_ASSISTANT}: <span class="cover_link">{$data.ta1_name}{if $data.ta2_name != ''}, {$data.ta2_name}{/if}</span></span></div>
        <div class="timetable-value col-6"><span class="normal"><i class="far fa-door-open"></i>{$MOD.LBL_ROOM_NAME}: <span class="cover_link">{$data.room_name}</span></span></div>
    </div>
    {if $data.external_id != '' || $data.syllabus_id != ''}
    <div class="timetable-row">
        <div class="timetable-value col-6"><span class="normal"><i class="far fa-book-spells"></i>{$MOD_S.LBL_NAME}: {$data.syllabus_topic}</span></div>
        <div class="timetable-value col-6"><span class="normal"><i class="far fa-video-camera"></i>{$MOD.LBL_ONLINE_LMS}: {$data.join_url}</span></div>
    </div>
    {/if}
    <div class="timetable-row">
        <div class="timetable-value col-6"><span class="normal"><i class="far fa-clipboard-user"></i>{$MOD_M.LBL_TOTAL_ATTENDED}: <span title="">{$data.total_attended} {$MOD_M.LBL_OF} {$data.total_student} {$MOD_M.LBL_PRESENT}</span></span></div>
        <div class="timetable-value col-6"><span class="normal"><a class="button btn-primary" target="_parent" href="{$uri}#bwc/index.php?module=J_Class&action=attendance&session_id={$data.meeting_id}" title="{$MOD_M.LBL_CHECK_ATTENDANCE}"><i class="far fa-tasks"></i> {$MOD_M.LBL_CHECK_ATTENDANCE}</a></span></div>
    </div>
</div>
<button title="{$MOD.LBL_CLOSE_ESC}" type="button" class="mfp-close">×</button>
</div>