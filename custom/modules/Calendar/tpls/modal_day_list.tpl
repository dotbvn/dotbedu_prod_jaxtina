<div class="timetable-item" title="{$data.class_name} {if $data.teacher_name != ''} | {$data.teacher_name} {/if}{if $data.ta_name != ''} | {$data.ta_name} {/if}{if $data.room_name != ''} | {$data.room_name} {/if}" style="display: flex;__top">
<span class="timetable-color" data-value="{$data.color}"></span>
    <a class="timetable-title open-popup-link" href="#modal-{$data.meeting_id}">
        <span class="timetable-time">{$data.start_time}</span>
        <span class="timetable-name">{$data.class_name}</span>
        <span class="timetable-name" title="{$MOD.LBL_TEACHER_NAME}" style="color:blue">{$data.teacher_name}</span>
        <span class="timetable-name" title="{$MOD.LBL_TEACHER_ASSISTANT}" style="color:#FF8C00">{$data.ta_name}</span>
        <span class="timetable-name" title="{$MOD.LBL_ROOM_NAME}" style="color:#468931">{$data.room_name}</span>
    </a>
</div>
