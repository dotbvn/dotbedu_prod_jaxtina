
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/Calendar/css/pretty-checkbox.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/Calendar/css/magnific-popup.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/Calendar/css/calendar.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/Calendar/css/multiple-select.min.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/Calendar/css/select-time-table.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Bootstrap/css/bootstrap.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/Calendar/css/custom-calendar.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/Calendar/css/second-theme.css}"/>
<link rel="stylesheet" href="{dotb_getjspath file=include/javascript/datetimepicker/jquery.datetimepicker.min.css}"/>

{dotb_getscript file="custom/modules/Calendar/js/date.min.js"}
{dotb_getscript file="include/javascript/datetimepicker/jquery.datetimepicker.full.min.js"}
{dotb_getscript file="custom/modules/Calendar/js/datetimepikerr.js"}
{dotb_getscript file="custom/modules/Calendar/js/jquery.magnific-popup.js"}
{dotb_getscript file="custom/modules/Calendar/js/multiple-select.min.js"}
{dotb_getscript file="custom/modules/Calendar/js/calendar.js"}
{dotb_getscript file="custom/modules/Calendar/js/timetable.js"}

<!-- Section / Default -->


<section class="header">
    <div class="row">
        <div id="sidebar" {if $dataType.nav_status == '0'} class="" style="display: none" {else} class="col-2" {/if}>
            <div id="data-view" class="btn-group" role="group">
                {foreach from=$config_calender_view item=view}
                    <button id="{$view.name}-tab" type="button" class="btn btn-view {if $view.name == $dataType.view}btn-active{/if}" data-view="{$view.name}">{$MOD[$view.label]}</button>
                {/foreach}
            </div>
            {if $dataType.view == 'week'}
                <div class="radio-container">
                    <span style="font-weight: 300;">{$label.titRadio}</span>
                    <label class="container">
                        <input type="radio" id="timeOption" name="radioGroup" value="time" {if $dataType.view_as == 'time'}checked{/if}>
                        <span class="label-radio">{$label.time}</span>
                    </label>
                    <label class="container">
                        <input type="radio" id="roomOption" name="radioGroup" value="room" {if $dataType.view_as == 'room'}checked{/if}>
                        <span class="label-radio">{$label.room}</span>
                    </label>
                    <label class="container">
                        <input type="radio" id="teacherOption" name="radioGroup" value="teacher" {if $dataType.view_as == 'teacher'}checked{/if}>
                        <span class="label-radio">{$label.teacher}</span>
                    </label>
                </div>
            {/if}
            <div id="calender" class="calendar_settings" style="display:none;">
                <input type="text" id="picked_date" value="{$data_get.date}"/>
            </div>
            <div id="setting-dialog" class="bd calendar_settings" style="display:none;">
                <form id="form-setting">
                    <table class="view tabForm custom-table">
                        <tbody>
                        <tr><td valign="top" width="55%">{$MOD.LBL_CENTER}:</td></tr>
                        <tr>
                            <td width="45%">
                                <select multiple="multiple" id="centers" name="centers" style="width: 100%;height: 30px">
                                    {foreach from=$array_teams key=k item=v}
                                        <option value='{$k}' {if in_array($k,$filter_center)}selected{/if}>{$v}</option>
                                    {/foreach}
                                </select>
                            </td>
                        </tr>
                        {foreach from=$type_list key=k item=v}
                            {if $k == 'session'}
                                <tr class="session_action">
                                    <td>
                                        <div class="state">
                                            <span style="margin: auto">{$MOD.LBL_CLASS}</span>
                                            <select multiple="multiple" id="session_class" name="session_class[]">
                                                {foreach from=$array_class key=k item=v}
                                                    <option value='{$k}' {if in_array($k,$filter_class)}selected{/if}>{$v}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="session_action">
                                    <td>
                                        <div class="state">
                                            <span style="margin: auto">{$MOD.LBL_TEACHER}</span>
                                            <select multiple="multiple" id="session_teacher" name="session_teacher[]">
                                                {foreach from=$array_teacher key=k item=v}
                                                    <option value='{$k}' {if in_array($k,$filter_teacher)}selected{/if}>{$v}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="session_action">
                                    <td>
                                        <div class="state">
                                            <span style="margin: auto">{$MOD.LBL_TA}</span>
                                            <select multiple="multiple" id="session_ta" name="session_ta[]">
                                                {foreach from=$array_ta key=k item=v}
                                                    <option value='{$k}' {if in_array($k,$filter_ta)}selected{/if}>{$v}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="session_action">
                                    <td>
                                        <div class="state">
                                            <span style="margin: auto">{$MOD.LBL_ROOM}</span>
                                            <select multiple="multiple" id="session_room" name="session_room[]">
                                                {foreach from=$array_room key=k item=v}
                                                    <option value='{$k}' {if in_array($k,$filter_room)}selected{/if}>{$v}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>
                </form>
                <div style="text-align: right;padding-top: 15px;">
                    <button id="btn-save-settings" class="button primary" type="button">{$MOD.LBL_APPLY_BUTTON}</button>&nbsp;
                </div>
            </div>
        </div>
        <div {if $dataType.nav_status == '0'} class="col-12" {else} class="col-10" {/if} id="contentTable">
            <div id="minimizeButton" nav_status="{$dataType.nav_status}" class="button-4" role="button" title="{if $dataType.nav_status == '0'}{$MOD.LBL_SHOW_PANEL}{else}{$MOD.LBL_HIDE_PANEL}{/if}"><i class="far fa-bars" aria-hidden="true"></i></div>
            <div id="default_header"></div>
            <div class="max-width">
                <div class="lds-ellipsis">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
                <div id="data-view-source" class="tiva-timetable hidden-xs" data-source="php" data-view="{$dataType.view}" data-mode="date" data-start="{if $first_day_of_week == 0}sunday{else}monday{/if}"></div>
            </div>
        </div>
    </div>
</section>
<!-- Content / End -->
