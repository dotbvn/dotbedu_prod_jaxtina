<?php
// created: 2022-06-14 12:30:52
$viewdefs['ProductTemplates']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' =>
  array (
    'code' =>
    array (
    ),
    'name' =>
    array (
    ),
    'type_name' =>
    array (
    ),
    'category_name' =>
    array (
    ),
    'unit' =>
    array (
    ),
    'list_price' =>
    array (
    ),
    'cost_price' =>
    array (
    ),
    'date_available' =>
    array (
    ),
    'description' =>
    array (
    ),
    'taxrate_name' =>
    array (
    ),
    'assigned_user_name' =>
    array (
    ),
    'tag' =>
    array (
    ),
    'date_modified' =>
    array (
    ),
    'date_entered' =>
    array (
    ),
    'modified_by_name' =>
    array (
    ),
    'created_by_name' =>
    array (
    ),
  ),
);