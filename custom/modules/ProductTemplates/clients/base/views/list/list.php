<?php
$viewdefs['ProductTemplates'] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'favorites' => false,
                'panels' =>
                array (
                    0 =>
                    array (
                        'name' => 'panel_header',
                        'fields' =>
                        array (

                            array (
                                'name' => 'code',
                                'label' => 'LBL_CODE',
                                'enabled' => true,
                                'default' => true,
                            ),

                            array (
                                'name' => 'name',
                                'link' => true,
                                'enabled' => true,
                                'default' => true,
                            ),

                            array (
                                'name' => 'list_price',
                                'type' => 'currency',
                                'related_fields' =>
                                array (
                                    0 => 'list_usdollar',
                                    1 => 'currency_id',
                                    2 => 'base_rate',
                                ),
                                'currency_field' => 'currency_id',
                                'base_rate_field' => 'base_rate',
                                'enabled' => true,
                                'default' => true,
                            ),
                            array (
                                'name' => 'cost_price',
                                'type' => 'currency',
                                'related_fields' =>
                                array (
                                    0 => 'cost_usdollar',
                                    1 => 'currency_id',
                                    2 => 'base_rate',
                                ),
                                'currency_field' => 'currency_id',
                                'base_rate_field' => 'base_rate',
                                'enabled' => true,
                                'default' => true,
                            ),
                            array (
                                'name' => 'taxrate_name',
                                'label' => 'LBL_TAXRATE_NAME',
                                'enabled' => true,
                                'id' => 'TAXRATE_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => true,
                            ),

                            array (
                                'name' => 'unit',
                                'label' => 'LBL_UNIT',
                                'enabled' => true,
                                'default' => true,
                            ),

                            array (
                                'name' => 'type_name',
                                'enabled' => true,
                                'default' => true,
                            ),

                            array (
                                'name' => 'category_name',
                                'enabled' => true,
                                'default' => true,
                            ),

                            array (
                                'name' => 'status2',
                                'label' => 'LBL_STATUS_2',
                                'type' => 'event-status',
                                'enabled' => true,
                                'default' => true,
                            ),

                            array (
                                'name' => 'date_available',
                                'label' => 'LBL_DATE_AVAILABLE',
                                'enabled' => true,
                                'default' => true,
                            ),

                            array (
                                'name' => 'description',
                                'label' => 'LBL_DESCRIPTION',
                                'enabled' => true,
                                'sortable' => false,
                                'default' => true,
                            ),



                            array (
                                'name' => 'tax_class',
                                'label' => 'LBL_TAX_CLASS',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
