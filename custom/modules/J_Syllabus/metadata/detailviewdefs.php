<?php
$module_name = 'J_Syllabus';
$viewdefs[$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'lesson',
            'studio' => 'visible',
            'label' => 'LBL_LESSON',
          ),
          1 => 
          array (
            'name' => 'lessonplan_name',
            'label' => 'LBL_LP_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'learning_type',
            'studio' => 'visible',
            'label' => 'LBL_LEARNING_TYPE',
          ),
          1 => 
          array (
            'name' => 'lesson_type',
            'studio' => 'visible',
            'label' => 'LBL_LESSON_TYPE',
          ),
        ),
        2 => 
        array (
          0 => 'name',
        ),
        3 => 
        array (
          0 => 'description',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'note_for_teacher',
            'comment' => '',
            'label' => 'LBL_NOTE_FOR_TEACHER',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'homework',
            'comment' => '',
            'label' => 'LBL_HOMEWORK',
          ),
        ),
        6 => 
        array (
          0 => 'assigned_user_name',
          1 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
        7 => 
        array (
          0 => 'team_name',
          1 => 
          array (
            'name' => 'date_modified',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
            'label' => 'LBL_DATE_MODIFIED',
          ),
        ),
      ),
    ),
  ),
);
