<?php
$module_name = 'J_Syllabus';
$viewdefs[$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'lesson',
            'studio' => 'visible',
            'label' => 'LBL_LESSON',
          ),
          1 => 
          array (
            'name' => 'lessonplan_name',
            'label' => 'LBL_LP_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'learning_type',
            'studio' => 'visible',
            'label' => 'LBL_LEARNING_TYPE',
          ),
          1 => 
          array (
            'name' => 'lesson_type',
            'studio' => 'visible',
            'label' => 'LBL_LESSON_TYPE',
          ),
        ),
        2 => 
        array (
          0 => 'name',
        ),
        3 => 
        array (
          0 => 'description',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'note_for_teacher',
            'comment' => '',
            'label' => 'LBL_NOTE_FOR_TEACHER',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'homework',
            'comment' => '',
            'label' => 'LBL_HOMEWORK',
          ),
        ),
      ),
    ),
  ),
);
