<?php
$module_name = 'J_Syllabus';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'lessonplan_name',
                'label' => 'LBL_LP_NAME',
                'enabled' => true,
                'id' => 'LESSONPLAN_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
                'width' => 'large',
              ),
              1 => 
              array (
                'name' => 'lesson',
                'label' => 'LBL_LESSON',
                'enabled' => true,
                'default' => true,
                'width' => 'small',
              ),
              2 => 
              array (
                'name' => 'lesson_type',
                'label' => 'LBL_LESSON_TYPE',
                'enabled' => true,
                'default' => true,
                'width' => 'small',
              ),
              3 => 
              array (
                'name' => 'learning_type',
                'label' => 'LBL_LEARNING_TYPE',
                'enabled' => true,
                'default' => true,
                'width' => 'small',
              ),
              4 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'large',
              ),
              5 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
                'width' => 'large',
              ),
              6 => 
              array (
                'name' => 'note_for_teacher',
                'label' => 'LBL_NOTE_FOR_TEACHER',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
                'width' => 'large',
              ),
              7 => 
              array (
                'name' => 'homework',
                'label' => 'LBL_HOMEWORK',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
                'width' => 'large',
              ),
              8 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              9 => 
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              11 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              12 => 
              array (
                'name' => 'tag',
                'label' => 'LBL_TAGS',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
