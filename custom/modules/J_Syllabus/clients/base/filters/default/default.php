<?php
// created: 2023-06-25 12:18:44
$viewdefs['J_Syllabus']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'lesson' => 
    array (
    ),
    'lessonplan_name' => 
    array (
    ),
    'lesson_type' => 
    array (
    ),
    'learning_type' => 
    array (
    ),
    'description' => 
    array (
    ),
    'note_for_teacher' => 
    array (
    ),
    'name' => 
    array (
    ),
    'homework' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);