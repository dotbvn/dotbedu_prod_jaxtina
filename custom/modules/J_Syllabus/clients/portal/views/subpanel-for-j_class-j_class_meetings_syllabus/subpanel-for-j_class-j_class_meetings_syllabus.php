<?php

$viewdefs['J_Syllabus']['portal']['view']['subpanel-for-j_class-j_class_meetings_syllabus'] = array (
    'panels' =>
        array (
            0 =>
                array (
                    'name' => 'panel_header',
                    'label' => 'LBL_PANEL_1',
                    'fields' =>
                        array (
                            0 =>
                                array (
                                    'name' => 'lessonplan_name',
                                    'label' => 'LBL_LP_NAME',
                                    'enabled' => true,
                                    'id' => 'LESSONPLAN_ID',
                                    'link' => true,
                                    'sortable' => false,
                                    'default' => true,
                                    'width' => 'large',
                                ),
                            1 =>
                                array (
                                    'name' => 'description',
                                    'label' => 'LBL_DESCRIPTION',
                                    'enabled' => true,
                                    'sortable' => false,
                                    'default' => true,
                                    'width' => 'large',
                                ),
                            2 =>
                                array (
                                    'name' => 'note_for_teacher',
                                    'label' => 'LBL_NOTE_FOR_TEACHER',
                                    'enabled' => true,
                                    'sortable' => false,
                                    'default' => true,
                                    'width' => 'large',
                                ),
                            3 =>
                                array (
                                    'label' => 'LBL_LIST_DATE_CREATED',
                                    'enabled' => true,
                                    'default' => true,
                                    'name' => 'date_entered',
                                ),
                        ),
                ),
        ),
    'type' => 'subpanel-list',
);