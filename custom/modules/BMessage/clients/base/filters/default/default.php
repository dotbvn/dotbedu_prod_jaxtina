<?php
// created: 2023-04-30 18:51:44
$viewdefs['BMessage']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'action_type' => 
    array (
    ),
    'send_to' => 
    array (
    ),
    'title' => 
    array (
    ),
    'description' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'priority' => 
    array (
    ),
    'role_name' => 
    array (
    ),
    'parent_name' => 
    array (
    ),
    'status' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);