<?php
$module_name = 'BMessage';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NUMBER',
                'default' => true,
                'enabled' => true,
                'width' => 'small',
                'link' => true,
              ),
              1 =>
              array (
                'name' => 'action_type',
                'label' => 'LBL_ACTION_TYPE',
                'enabled' => true,
                'default' => true,

              ),
              2 =>
              array (
                'name' => 'send_to',
                'label' => 'LBL_SEND_TO',
                'enabled' => true,
                'default' => true,
              ),
              3 =>
              array (
                'name' => 'title',
                'label' => 'LBL_TITLE',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
                'link' => true,
              ),
              4 =>
              array (
                'name' => 'parent_type',
                'label' => 'LBL_PARENT_TYPE',
                'enabled' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'parent_name',
                'label' => 'LBL_PARENT_NAME',
                'enabled' => true,
                'id' => 'PARENT_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              7 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              8 =>
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => false,
              ),
              9 =>
              array (
                'name' => 'priority',
                'label' => 'LBL_PRIORITY',
                'enabled' => true,
                'default' => false,
              ),
              10 =>
              array (
                'name' => 'role_name',
                'label' => 'LBL_ROLE_NAME',
                'enabled' => true,
                'default' => false,
              ),
              11 =>
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              12 =>
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => false,
              ),
              13 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              15 =>
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => false,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
