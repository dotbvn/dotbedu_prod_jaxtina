<?php
$module_name = 'BMessage';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'record' =>
            array (
                'buttons' =>
                array (),
                'panels' =>
                array (
                    0 =>
                    array (
                        'name' => 'panel_header',
                        'label' => 'LBL_RECORD_HEADER',
                        'header' => true,
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'picture',
                                'type' => 'avatar',
                                'width' => 42,
                                'height' => 42,
                                'dismiss_label' => true,

                            ),
                            array(
                                'name' => 'name',
                                'dismiss_label' => true,
                                'readonly' => true,
                            ),
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'panel_body',
                        'label' => 'LBL_RECORD_BODY',
                        'columns' => 2,
                        'labelsOnTop' => true,
                        'placeholders' => true,
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                        'fields' =>
                        array (
                            array (
                                'name' => 'parent_name',
                                'studio' => true,
                                'label' => 'LBL_PARENT_NAME',
                                'span' => 12,
                            ),

                            array (
                                'name' => 'action_type',
                                'label' => 'LBL_ACTION_TYPE',
                            ),
                            'status',
                            array (
                                'name' => 'send_to',
                                'label' => 'LBL_SEND_TO',
                                'span' => 12,
                            ),

                            array (
                                'name' => 'title',
                                'span' => 12,
                            ),

                            array (
                                'name' => 'description',
                                'span' => 12,
                            ),
                            'assigned_user_name',
                            'team_name',
                            'priority',
                            'role_name',
                            array (
                                'name' => 'date_modified_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_MODIFIED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_modified',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'modified_by_name',
                                    ),
                                ),
                            ),
                            array (
                                'name' => 'date_entered_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_ENTERED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_entered',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'created_by_name',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
                'templateMeta' =>
                array (
                    'useTabs' => false,
                ),
            ),
        ),
    ),
);
