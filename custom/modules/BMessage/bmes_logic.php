<?php

//CHƯA SỬA XONG QUA HỆ j_class_leads_1, j_class_contact_1
class bmes_logic
{
    function send_messages(&$bean, $event, $arg){
        if (!isset($arg['isUpdate']) || $arg['isUpdate'] == true) { return; }

        global $timedate;

        //RECIPIENTS
        $recipients = array();
        $recipients['Users'] = array();
        $recipients['Teachers'] = array();
        $recipients['Contacts'] = array();
        $recipients['Leads'] = array();
        $recipients['Prospects'] = array();


        //RELATED TO
        $parentBean = BeanFactory::getBean($bean->parent_type, $bean->parent_id);
        $bean->assigned_user_id = $parentBean->assigned_user_id;
        //set list 1 receipient and settle
        switch ($bean->send_to) {
            case 'users_assigned':
                $recipients['Users'][] = BeanFactory::getBean('Users', $parentBean->assigned_user_id);
                break;
            case 'cso_assigned':
                $recipients['Users'][] = BeanFactory::getBean('Users', $parentBean->cso_id);
                break;
            case 'users_modified':
                $recipients['Users'][] = BeanFactory::getBean('Users', $parentBean->modified_user_id);
                break;
            case 'users_created':
                $recipients['Users'][] = BeanFactory::getBean('Users', $parentBean->created_by);
                break;
            case 'users_role':
                //Get Users By Role INNER JOIN Teams Set List
                if($bean->team_id != $bean->team_set_id) $teams = array_column($GLOBALS['db']->fetchArray("SELECT teams.id team_id FROM teams INNER JOIN team_sets_teams tst ON tst.team_id = teams.id AND tst.deleted=0 WHERE tst.team_set_id = '{$bean->team_set_id}' AND teams.deleted = 0"),'team_id');
                else $teams = array(0 => $bean->team_id);

                $u1 = "SELECT p1.* FROM (SELECT users.id user_id, users.user_name user_name
                FROM acl_roles INNER JOIN acl_roles_users ON acl_roles_users.role_id = acl_roles.id AND acl_roles_users.deleted = 0
                INNER JOIN users ON users.id = acl_roles_users.user_id AND users.deleted = 0 AND users.status = 'Active'
                WHERE acl_roles.deleted = 0 AND acl_roles.name = '{$bean->role_name}') p1
                INNER JOIN (SELECT DISTINCT
                IFNULL(l1.id, '') user_id, l1.user_name user_name
                FROM teams t INNER JOIN team_memberships l1_1 ON t.id = l1_1.team_id AND l1_1.deleted = 0
                INNER JOIN users l1 ON l1.id = l1_1.user_id AND l1.deleted = 0 AND l1.status = 'Active'
                WHERE (t.id IN ('".implode("','",$teams)."')) AND t.deleted = 0) p2 ON p2.user_id = p1.user_id";
                $users_role = array();
                foreach ($GLOBALS['db']->fetchArray($u1) as $row)
                    $recipients['Users'][] = BeanFactory::getBean('Users', $row['user_id']);
                break;
            case 'center_manager':
                if (!empty($parentBean->team_id)) {
                    $team = BeanFactory::getBean('Teams', $parentBean->team_id);
                    if (!empty($team->manager_user_id))
                        $recipients['Users'][] = BeanFactory::getBean('Users', $team->manager_user_id);
                }
                break;
            default:
                break;
        }

        switch ($bean->parent_type) {
            case 'Contacts':
                $recipients['Contacts'][] = $parentBean;
                break;
            case 'Leads':
                $recipients['Leads'][] = $parentBean;
                break;
            case 'Prospects':
                $recipients['Prospects'][] = $parentBean;
                break;
            case 'C_Gallery':
            case 'J_Class':
                if ($bean->parent_type == 'C_Gallery') {
                    $class = BeanFactory::getBean('J_Class', $parentBean->j_class_c_gallery_1j_class_ida);
                    $classId = $parentBean->j_class_c_gallery_1j_class_ida;
                } else {
                    $class = $parentBean;
                    $classId = $parentBean->id;
                }
                switch ($bean->send_to) {
                    case 'students_leads':
                        $q =   "SELECT c.id, 'Contacts' as student_type
                        FROM j_classstudents cs INNER JOIN contacts c ON cs.student_id = c.id AND c.deleted = 0
                        WHERE cs.class_id = '{$classId}' AND cs.deleted = 0
                        UNION
                        SELECT l.id, 'Leads' as student_type
                        FROM j_classstudents cs INNER JOIN leads l ON cs.student_id = l.id AND l.deleted =  0
                        WHERE cs.class_id = '{$classId}' AND cs.deleted = 0";
                        foreach ($GLOBALS['db']->fetchArray($q) as $row) {
                            if ($row['student_type'] == 'Leads')
                                $recipients['Leads'][] = BeanFactory::getBean('Leads', $row['id']);
                            if ($row['student_type'] == 'Contacts')
                                $recipients['Contacts'][] = BeanFactory::getBean('Contacts', $row['id']);
                        }
                        break;
                    case 'teachers':
                        if ($class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($parentBean->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'Teacher'
                            ))));
                        break;
                    case 'assistants':
                        if ($class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($parentBean->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'TA'
                            ))));
                        break;
                }
                break;
            case 'Meetings':
                $class = BeanFactory::getBean('J_Class', $parentBean->ju_class_id);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->assigned_user_id);
                        break;
                    case 'cso_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->cso_id);
                        break;
                    case 'students_leads':
                        if ($parentBean->load_relationship('leads'))
                            $recipients['Leads'] = array_merge($parentBean->leads->getBeans());
                        if ($parentBean->load_relationship('contacts'))
                            $recipients['Contacts'] = array_merge($parentBean->contacts->getBeans());
                        break;
                    case 'teachers':
                        if ($parentBean->load_relationship('c_teachers'))
                            $recipients['Teachers'] = array_merge($parentBean->c_teachers->getBeans());
                        break;
                    case 'assistants':
                        if ($parentBean->load_relationship('sub_c_teachers'))
                            $recipients['Teachers'] = array_merge($parentBean->sub_c_teachers->getBeans());
                        if ($parentBean->load_relationship('c_teachers_cover'))
                            $recipients['Teachers'] = array_merge($recipients['Teachers'], $parentBean->c_teachers_cover->getBeans());
                        break;
                }
                break;
            case 'Calls':
            case 'Tasks':
            case 'J_ReferenceLog':
                $student = BeanFactory::getBean($parentBean->parent_type, $parentBean->parent_id);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $student->assigned_user_id);
                        break;
                    case 'students_leads':
                        if($parentBean->parent_type == 'Contacts')
                            $recipients['Contacts'][] = $student;
                        if($parentBean->parent_type == 'Leads')
                            $recipients['Leads'][] = $student;
                        break;
                }
                break;
            case 'J_PTResult':
                $student = BeanFactory::getBean($parentBean->parent, $parentBean->student_id);
                switch ($bean->send_to){
                    case 'students_leads':
                        if ($parentBean->parent == 'Contacts')
                            $recipients['Contacts'][] = $student;
                        if ($parentBean->parent == 'Leads')
                            $recipients['Leads'][] = $student;
                        break;
                    case 'teachers':
                        $meeting = BeanFactory::getBean('Meetings', $parentBean->meeting_id);
                        if (!empty($meeting->id)) {
                            if ($meeting->load_relationship('c_teachers'))
                                $recipients['Teachers'] = array_merge($meeting->c_teachers->getBeans());
                        }
                        break;
                    case 'assistants':
                        $meeting = BeanFactory::getBean('Meetings', $parentBean->meeting_id);
                        if (!empty($meeting->id)) {
                            if ($meeting->load_relationship('sub_c_teachers'))
                                $recipients['Teachers'] = array_merge($meeting->sub_c_teachers->getBeans());
                            if ($meeting->load_relationship('c_teachers_cover'))
                                $recipients['Teachers'] = array_merge($recipients['Teachers'], $meeting->c_teachers_cover->getBeans());
                        }
                        break;
                }
                break;
            case 'J_StudentSituations':
                $student = BeanFactory::getBean($parentBean->student_type, $parentBean->student_id);
                $class = BeanFactory::getBean('J_Class', $parentBean->ju_class_id);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->assigned_user_id);
                        break;
                    case 'cso_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->cso_id);
                        break;
                    case 'students_leads':
                        if ($parentBean->student_type == 'Contacts')
                            $recipients['Contacts'][] = $student;
                        if ($parentBean->student_type == 'Leads')
                            $recipients['Leads'][] = $student;
                        break;
                    case 'teachers':
                        if ($class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'Teacher'
                            ))));
                        break;
                    case 'assistants':
                        $class = BeanFactory::getBean('J_Class', $parentBean->ju_class_id);
                        if ($class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'TA'
                            ))));
                        break;
                }
                break;
            case 'J_ClassStudents':
                $student = BeanFactory::getBean($parentBean->student_type, $parentBean->student_id);
                $class = BeanFactory::getBean('J_Class', $parentBean->class_id);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $student->assigned_user_id);
                        break;
                    case 'cso_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->cso_id);
                        break;
                    case 'students_leads':
                        if ($parentBean->student_type == 'Contacts')
                            $recipients['Contacts'][] = $student;
                        if ($parentBean->student_type == 'Leads')
                            $recipients['Leads'][] = $student;
                        break;
                    case 'teachers':
                        if ($class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'Teacher'
                            ))));
                        break;
                    case 'assistants':
                        $class = BeanFactory::getBean('J_Class', $parentBean->ju_class_id);
                        if ($class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'TA'
                            ))));
                        break;
                }
                break;
            case 'J_Gradebook':
                $class = BeanFactory::getBean('J_Class', $parentBean->j_class_j_gradebook_1j_class_ida);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->assigned_user_id);
                        break;
                    case 'cso_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->cso_id);
                        break;
                    case 'students_leads':
                        if ($parentBean->load_relationship('j_gradebook_contacts_1'))
                            $recipients['Contacts'] = array_merge($parentBean->j_gradebook_contacts_1->getBeans());
                        break;
                    case 'teachers':
                        if ($parentBean->load_relationship('c_teachers_j_gradebook_1'))
                            $recipients['Teachers'] = array_merge($parentBean->c_teachers_j_gradebook_1->getBeans());
                        break;
                }
                break;
            case 'J_GradebookDetail':
                $gradebook = BeanFactory::getBean('J_Gradebook', $parentBean->gradebook_id);
                $class = BeanFactory::getBean('J_Class', $gradebook->j_class_j_gradebook_1j_class_ida);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->assigned_user_id);
                        break;
                    case 'cso_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->cso_id);
                        break;
                    case 'students_leads':
                        if ($parentBean->load_relationship('student_j_gradebookdetail'))
                            $recipients['Contacts'] = array_merge($parentBean->student_j_gradebookdetail->getBeans());
                        break;
                    case 'teachers':
                        if ($gradebook->load_relationship('c_teachers_j_gradebook_1'))
                            $recipients['Teachers'] = array_merge($gradebook->c_teachers_j_gradebook_1->getBeans());
                        break;
                }
                break;
            case 'J_Payment':
                switch ($bean->send_to) {
                    case 'students_leads':
                        if ($parentBean->load_relationship('new_payment_contacts'))
                            $recipients['Contacts'] = array_merge($parentBean->new_payment_contacts->getBeans());
                        if ($parentBean->load_relationship('new_payment_leads'))
                            $recipients['Leads'] = array_merge($parentBean->new_payment_leads->getBeans());
                        break;
                    default:
                        break;
                }
                break;
            case 'J_PaymentDetail':
                switch ($bean->send_to) {
                    case 'students_leads':
                        if ($parentBean->load_relationship('pmd_student_link'))
                            $recipients['Contacts'] = array_merge($parentBean->pmd_student_link->getBeans());
                        if ($parentBean->load_relationship('pmd_lead_link'))
                            $recipients['Leads'] = array_merge($parentBean->pmd_lead_link->getBeans());
                        break;
                    default:
                        break;
                }
                break;
            case 'C_Attendance':
                $meeting = BeanFactory::getBean('Meetings', $parentBean->meeting_id);
                $class = BeanFactory::getBean('J_Class', $meeting->ju_class_id);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->assigned_user_id);
                        break;
                    case 'cso_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $class->cso_id);
                        break;
                    case 'students_leads':
                        if ($parentBean->load_relationship('leads'))
                            $recipients['Leads'] = array_merge($parentBean->leads->getBeans());
                        if ($parentBean->load_relationship('contacts'))
                            $recipients['Contacts'] = array_merge($parentBean->contacts->getBeans());
                        break;
                    case 'teachers':
                        if ($meeting->load_relationship('c_teachers'))
                            $recipients['Teachers'] = array_merge($meeting->c_teachers->getBeans());
                        break;
                    case 'assistants':
                        if ($meeting->load_relationship('sub_c_teachers'))
                            $recipients['Teachers'] = array_merge($meeting->sub_c_teachers->getBeans());
                        if ($meeting->load_relationship('c_teachers_cover'))
                            $recipients['Teachers'] = array_merge($recipients['Teachers'], $meeting->c_teachers_cover->getBeans());
                        break;
                }
                break;
            case 'Cases':
                $class = BeanFactory::getBean('J_Class', $parentBean->j_class_cases_1j_class_ida);
                switch ($bean->send_to) {
                    case 'students_leads':
                        if ($parentBean->load_relationship('contacts_cases_1'))
                            $recipients['Contacts'] = array_merge($parentBean->contacts_cases_1->getBeans());
                        break;
                    case 'teachers':
                        if (!empty($class->id) && $class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'Teacher'
                            ))));
                        break;
                    case 'assistants':
                        if (!empty($class->id) && $class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'TA'
                            ))));
                        break;
                }
                break;
            case 'C_Comments':
                $case = BeanFactory::getBean('Cases', $parentBean->parent_id);
                $class = BeanFactory::getBean('J_Class', $case->j_class_cases_1j_class_ida);
                switch ($bean->send_to) {
                    case 'users_assigned':
                        $recipients['Users'][] = BeanFactory::getBean('Users', $case->assigned_user_id);
                        break;
                    case 'cso_assigned':
                        if(!empty($case->cso_id))
                            $recipients['Users'][] = BeanFactory::getBean('Users', $case->cso_id);
                        else $recipients['Users'][] = BeanFactory::getBean('Users', $class->cso_id);
                        break;
                    case 'students_leads':
                        if ($case->load_relationship('contacts_cases_1'))
                            $recipients['Contacts'] = array_merge($case->contacts_cases_1->getBeans());
                        break;
                    case 'teachers':
                        if (!empty($class->id) && $class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'Teacher'
                            ))));
                        break;
                    case 'assistants':
                        if (!empty($class->id) && $class->load_relationship('j_class_c_teachers_1'))
                            $recipients['Teachers'] = array_merge($class->j_class_c_teachers_1->getBeans(array('where' => array(
                                'lhs_field' => 'type',
                                'operator' => '=',
                                'rhs_value' => 'TA'
                            ))));
                        break;
                }
                //Điều hướng về Feedback
                $bean->parent_type  = $parentBean->parent_type;
                $bean->parent_id    = $parentBean->parent_id;
                break;
            case 'C_Teachers':
                $recipients['Teachers'][] = $parentBean;
                break;
            case 'J_Teachercontract':
                $teacher = BeanFactory::getBean('C_Teachers', $parentBean->c_teachers_j_teachercontract_1c_teachers_ida);
                switch ($bean->send_to) {
                    case 'teachers':
                        $recipients['Teachers'][] = $teacher;
                        break;
                    default:
                        break;
                }
                break;
            default:

                break;
        }

        //remove empty recipients
        foreach ($recipients as $module => $records) foreach ($records as $_key => $rec){
            if(empty($rec->id)) unset($recipients[$module][$_key]);
        }
        //END: remove empty recipients

        //ACTIONS
        switch ($bean->action_type) {
            case 'send_sms':
                require_once('modules/C_SMS/SMS/sms.php');
                $message = (!empty($bean->title)) ? ($bean->title . "\n" . $bean->description) : $bean->description;
                foreach ($recipients as $module => $records) foreach ($records as $rec_id => $rec) {
                    if(!empty($rec->phone_mobile)){
                        $sms               = new C_SMS();
                        $sms->description  = $message;  //text message
                        $sms->parent_type  = $rec->module_name;
                        $sms->parent_id    = $rec->id;
                        $sms->phone_number = $rec->phone_mobile; //send to phone_mobile
                        $sms->date_in_content = $timedate->nowDate();
                        $sms->assigned_user_id    = $bean->created_by;
                        $sms->team_id             = $bean->team_id;
                        $sms->team_set_id         = $bean->team_set_id;
                        $sms->save();
                        $bean->status = $sms->delivery_status;
                    }
                }
                break;
            case 'send_notif_app':
                require_once("custom/clients/mobile/helper/NotificationHelper.php");
                foreach ($recipients as $module => $records) foreach ($records as $rec_id => $rec){
                    if (in_array($module, ['Teachers', 'Contacts'])) {
                        $notify = new NotificationMobile();
                        $notify->pushNotification($bean->title, $bean->description, $bean->parent_type, $bean->parent_id, $module == 'Teachers' ? $rec->user_id : $rec->id, $module == 'Teachers' ? 'Teacher' : 'Student');
                    }
                }
                break;
            case 'send_notif_metrikcal':
                require_once("custom/clients/mobile/helper/NotificationHelper.php");
                foreach ($recipients as $module => $records) foreach ($records as $rec_id => $rec){
                    if (in_array($module, ['Users'])) {
                        $notify = new NotificationMobile();
                        $notify->pushNotificationMetrikal($bean->title, $bean->description, $rec->id, $parentBean->report_id);
                    }
                }
                break;
            case 'send_notif_ems':
                foreach ($recipients as $module => $records) foreach ($records as $rec_id => $rec) {
                    if (in_array($module, ['Teachers', 'Users'])) {
                        $nt = BeanFactory::getBean("Notifications");
                        $nt->name = $bean->title;
                        $nt->assigned_user_id = $module == 'Teachers' ? $rec->user_id : $rec->id;
                        $nt->parent_id = $bean->parent_id;
                        $nt->parent_type = $bean->parent_type;
                        $nt->created_by = $bean->created_by;
                        $GLOBALS['db']->query("UPDATE notifications SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE parent_id='{$bean->parent_id}' AND assigned_user_id = '{$nt->assigned_user_id}' AND deleted = 0");
                        $nt->description = $bean->description;
                        $nt->is_read = 0;
                        $nt->severity = $bean->priority;
                        $nt->save();
                    }
                }
                break;
            case 'send_notif_email':
                try {
                    $mailer = MailerFactory::getSystemDefaultMailer();
                    $mailer->getMailTransmissionProtocol();
                    $mailer->setSubject($bean->title);
                    $textOnly = EmailFormatter::isTextOnly($bean->description);
                    if ($textOnly) {
                        $mailer->setTextBody($bean->description);
                        $mailer->setHtmlBody('<span style="font-size: 14px;">' . $bean->description . '</span>');
                    } else {
                        $textBody = strip_tags(br2nl($bean->description));
                        $mailer->setTextBody($textBody);
                        $mailer->setHtmlBody($bean->description);
                    }
                    foreach ($recipients as $module => $records) foreach ($records as $rec_id => $rec) {
                        if (!empty($rec->email1)) {
                            $mailer->clearRecipients();
                            $mailer->addRecipientsTo(new EmailIdentity($rec->email1, $rec->name));
                            $mailer->send();
                        }
                    }
                    $bean->status = 'sent';
                } catch (MailerException $me) {
                    $message = $me->getMessage();
                    switch ($me->getCode()) {
                        case MailerException::FailedToConnectToRemoteServer:
                            $GLOBALS["log"]->fatal("Email Reminder: error sending email, system smtp server is not set");
                            break;
                        default:
                            $GLOBALS["log"]->fatal("Email Reminder: error sending e-mail, (error: {$message})");
                            break;
                    }
                    $bean->status = 'failed';

                }
                break;
            case 'send_zalo':
                foreach ($recipients as $module => $records) {
                    foreach ($records as $rec_id => $rec) {
                        if (in_array($module, ['Contacts', 'Leads'])) {
                            $result = sendZalo($rec, $bean->title, $bean->description);
                            if ($result['success'] == '1') $bean->status = 'sent';
                            else $bean->status = 'failed';
                        }
                    }
                }
                break;
            case 'create_task':
                foreach ($recipients as $module => $records){
                    foreach ($records as $rec_id => $rec) {
                        $task               = new Task();
                        $task->name         = $bean->title;
                        $task->description  = $bean->description;
                        //Set related
                        $task->parent_type  = $parentBean->module_name;
                        $task->parent_id    = $parentBean->id;
                        switch ($task->parent_type) {
                            case 'C_Attendance':
                                $task->parent_type = $parentBean->student_type;
                                $task->parent_id = $parentBean->student_id;
                                break;
                            case 'J_GradebookDetail':
                                $task->parent_type = 'Contacts';
                                $task->parent_id   = $parentBean->student_id;
                                break;
                            case 'J_Payment':
                            case 'J_PaymentDetail':
                                $task->parent_type = $parentBean->parent_type;
                                $task->parent_id = $parentBean->parent_id;
                                break;
                            case 'Cases':
                                $task->parent_type = 'Contacts';
                                $task->parent_id = $parentBean->contacts_cases_1contacts_ida;
                                break;
                            case 'Meetings':
                            case 'J_Class':
                                if (in_array($module, ['Contacts', 'Leads'])) {
                                    $task->parent_type  = $module;
                                    $task->parent_id    = $rec->id;
                                }
                                break;
                            case 'J_PTResult':
                                $task->parent_type = $parentBean->parent;
                                $task->parent_id   = $parentBean->student_id;
                                break;
                            default:
                                break;
                        }
                        //Set Asssigned To
                        $task->assigned_user_id = ($module == 'Teachers' ? $rec->user_id : $rec->id);
                        //Set Team links
                        if(!empty($class->id)){
                            $task->j_class_tasks_1j_class_ida = $class->id;
                            $task->team_id  = $class->team_id;
                            $task->team_set_id = $class->team_set_id;
                        }else{
                            $task->team_id     = $rec->team_id;
                            $task->team_set_id = $task->team_id;
                        }
                        if($parentBean->module_name == 'Contacts') $task->contact_id = $parentBean->id;
                        if($parentBean->module_name == 'C_Attendance') $task->attendance_id  = $parentBean->id;
                        $task->priority   = 'High';
                        $task->status     = 'Not Started';
                        $task->date_start = (!empty($bean->date_start)) ? date('Y-m-d', strtotime($bean->date_start)) : $timedate->nowDbDate();
                        $task->date_due   = date('Y-m-d', strtotime('+1 days ' . $task->date_start));
                        $task->save();
                        // Add task to CJ
                        if ($bean->is_customer_journey_activity) {
                            $ext1 = ($task->parent_type === 'Leads')
                                ? 'FROM leads INNER JOIN dri_workflows l1 ON l1.lead_id = leads.id AND l1.deleted = 0'
                                : 'FROM contacts INNER JOIN dri_workflows l1 ON l1.contact_id = contacts.id AND l1.deleted = 0';

                            $ext2 = ($task->parent_type === 'Leads')
                                ? "WHERE leads.id = '$task->parent_id' AND leads.deleted = 0"
                                : "WHERE contacts.id = '$task->parent_id' AND contacts.deleted = 0";

                            $q = "SELECT l2.id stage_id, dri_workflow_sort_order $ext1
                            INNER JOIN dri_subworkflows l2 ON l1.current_stage_id = l2.id AND l2.deleted = 0
                            INNER JOIN tasks l3 ON l3.dri_subworkflow_id = l2.id AND l3.deleted = 0
                            $ext2 ORDER BY l3.dri_workflow_sort_order DESC";
                            $res = $GLOBALS['db']->fetchOne($q);
                            $stage_id = $res['stage_id'];
                            $sort_order = (int)$res['dri_workflow_sort_order'] + 1;

                            $GLOBALS['db']->query("UPDATE tasks SET dri_subworkflow_id = '$stage_id', is_customer_journey_activity = 1,
                            customer_journey_type = '" . $bean->customer_journey_type . "', dri_workflow_sort_order = " .$sort_order."
                            WHERE tasks.id = '$task->id' AND deleted = 0");
                        }
                    }
                }
                break;
            default:
                break;
        }
        //UPDATE BSend-STATUS
        $GLOBALS['db']->query("UPDATE bmessage SET status='{$bean->status}' WHERE id='{$bean->id}'");

    }
}

//Send Zalo Message
function sendZalo($parentBean, $templateName, string $stringParams)
{
    require_once('custom/include/utils/parseTemplate.php');

    //Config
    $admin = new Administration();
    $admin->retrieveSettings();
    $config = [];
    if (!empty($admin->settings['zalo_link'])) $config['link'] = $admin->settings['zalo_link'];
    if (!empty($admin->settings['zalo_key'])) $config['key'] = $admin->settings['zalo_key'];
    if (!empty($admin->settings['zalo_supplier'])) $config['supplier'] = $admin->settings['zalo_supplier'];
    if (!empty($admin->settings['zalo_username'])) $config['username'] = $admin->settings['zalo_username'];
    if (!empty($admin->settings['zalo_enable'])) $config['enable'] = $admin->settings['zalo_enable'];

    $result = [
        'success' => '0'
    ];

    if ($config['enable'] != 1) {
        return $result;
    }

    if ($config['supplier'] === 'INCOM') {
        $postFields = array(
            'username' => $config['username'],
            'password' => $config['key'],
            'phonenumber' => parsePhoneNumber($parentBean->phone_mobile),
            'routerule' => ["1"], //kênh Zalo
            'templatecode' => $templateName,
            'list_param' => parseZaloParams($stringParams)
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://omni.incom.vn/api/OmniMessage/SendMessage',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postFields)
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        switch ($response['status']) {
            case '1':
                $result['success'] = '1';
                break;
            default:
                $result['success'] = '0';
                $result['errorMessage'] = $response['code'];
        }
        curl_close($curl);
    } elseif ($config['supplier'] === 'VIHAT') {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://rest.esms.vn/MainService.svc/json/SendZaloMessage_V4_post_json',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode(array(
                "ApiKey" => $config['username'],
                "SecretKey" => $config['key'],
                "Phone" => (string)$parentBean->phone_mobile,
                "Params" => explode("\n", $stringParams),
                "TempID" => (string)$templateName,
                "OAID" => (string)$config['link'],
                "SendDate" => "",
                "Sandbox" => "0",
                "RequestId" => $parentBean->id . time(),
                "campaignid" => "",
                "CallbackUrl" => "https://v2.socket.dotb.cloud/webhooks/zns-vihat-".$GLOBALS['dotb_config']['unique_key']
            )),
            CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        switch ($response['CodeResult']) {
            case '100':
                $result['success'] = '1';
                break;
            default:
                $result['success'] = '0';
                $result['errorMessage'] = $response['ErrorMessage'];
        }
    }

    return $result;
}

?>
