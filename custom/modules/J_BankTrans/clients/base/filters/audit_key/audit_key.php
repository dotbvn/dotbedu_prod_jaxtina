<?php


$viewdefs['J_BankTrans']['base']['filter']['audit_key']['filters'][] = array(
    'id' => 'audit_key',
    'name' => 'LBL_FILTER_AUDITKEY',
    'filter_definition' => array(
        array(
            'audit_key' => array('$starts' => ''),
        ),

    ),
    'editable' => true,
    'is_template' => true,
);