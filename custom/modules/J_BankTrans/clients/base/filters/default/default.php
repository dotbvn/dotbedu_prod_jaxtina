<?php
// created: 2023-04-20 00:01:32
$viewdefs['J_BankTrans']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' =>
  array (
    'name' =>
    array (
    ),
    'tid' =>
    array (
    ),
    'audit_key' =>
    array (
    ),
    'when_entered' =>
    array (
    ),
    'bank_sub_acc_id' =>
    array (
    ),
    'subaccid' =>
    array (
    ),
    'bankname' =>
    array (
    ),
    'bankabbreviation' =>
    array (
    ),
    'parent_name' =>
    array (
    ),
    'parent_type' =>
    array (
    ),
    'audit_status' =>
    array (
    ),
    'assigned_user_name' =>
    array (
    ),
    'description' =>
    array (
    ),
    'virtualaccount' =>
    array (
    ),
    'corresponsiveaccount' =>
    array (
    ),
    'corresponsivebankname' =>
    array (
    ),
    'corresponsivebankid' =>
    array (
    ),
    'corresponsivename' =>
    array (
    ),
    'virtualaccountname' =>
    array (
    ),
    'amount' =>
    array (
    ),
    'created_by_name' =>
    array (
    ),
    'date_entered' =>
    array (
    ),
    'modified_by_name' =>
    array (
    ),
    'date_modified' =>
    array (
    ),
    '$owner' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);