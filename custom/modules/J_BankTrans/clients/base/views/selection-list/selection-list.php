<?php
$module_name = 'J_BankTrans';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'selection-list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 =>
              array (
                'name' => 'tid',
                'label' => 'LBL_TID',
                'enabled' => true,
                'default' => true,
              ),
              2 =>
              array (
                'name' => 'bankname',
                'label' => 'LBL_BANK_NAME',
                'enabled' => true,
                'default' => true,
              ),
              3 =>
              array (
                'name' => 'amount',
                'label' => 'LBL_AMOUNT',
                'enabled' => true,
                'currency_format' => true,
                'default' => true,
              ),
              4 =>
              array (
                'name' => 'when_entered',
                'label' => 'LBL_WHEN',
                'enabled' => true,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'bankabbreviation',
                'label' => 'LBL_BANK_ABBREVIATION',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
