<?php
$module_name = 'J_BankTrans';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'record' =>
            array (
                'buttons' =>
                array (
                    0 =>
                    array (
                        'type' => 'button',
                        'name' => 'cancel_button',
                        'label' => 'LBL_CANCEL_BUTTON_LABEL',
                        'tooltip' => 'LBL_CANCEL_BUTTON_LABEL',
                        'css_class' => 'btn ',
                        'icon' => 'fa-window-close',
                        'showOn' => 'edit',
                        'events' =>
                        array (
                            'click' => 'button:cancel_button:click',
                        ),
                    ),
                    1 =>
                    array (
                        'type' => 'rowaction',
                        'event' => 'button:save_button:click',
                        'name' => 'save_button',
                        'label' => 'LBL_SAVE_BUTTON_LABEL',
                        'tooltip' => 'LBL_SAVE_BUTTON_LABEL',
                        'icon' => 'fa-save',
                        'css_class' => 'btn btn-primary',
                        'showOn' => 'edit',
                        'acl_action' => 'admin',
                    ),
                    2 =>
                    array (
                        'type' => 'actiondropdown',
                        'name' => 'main_dropdown',
                        'primary' => true,
                        'showOn' => 'view',
                        'buttons' =>
                        array (
//                            array (
//                                'type' => 'rowaction',
//                                'event' => 'button:edit_button:click',
//                                'name' => 'edit_button',
//                                'tooltip' => 'LBL_EDIT_BUTTON_LABEL',
//                                'label' => 'LBL_EDIT_BUTTON_LABEL',
//                                'icon' => 'fa-pencil',
//                                'acl_action' => 'admin',
//                            ),
//
//                            array (
//                                'type' => 'rowaction',
//                                'event' => 'button:duplicate_button:click',
//                                'name' => 'duplicate_button',
//                                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
//                                'acl_module' => 'J_VietQRConfig',
//                                'acl_action' => 'admin',
//                            ),
//
//                            array (
//                                'type' => 'rowaction',
//                                'event' => 'button:audit_button:click',
//                                'name' => 'audit_button',
//                                'label' => 'LNK_VIEW_CHANGE_LOG',
//                                'acl_action' => 'admin',
//                            ),
//
//                            array (
//                                'type' => 'divider',
//                            ),
//
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:delete_button:click',
                                'name' => 'delete_button',
                                'tooltip' => 'LBL_DELETE_BUTTON_LABEL',
                                'label' => 'LBL_DELETE_BUTTON_LABEL',
                                'acl_action' => 'admin',
                            ),
                        ),
                    ),
                    3 =>
                    array (
                        'name' => 'sidebar_toggle',
                        'type' => 'sidebartoggle',
                    ),
                ),
                'panels' =>
                array (
                    0 =>
                    array (
                        'name' => 'panel_header',
                        'label' => 'LBL_RECORD_HEADER',
                        'header' => true,
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'picture',
                                'type' => 'avatar',
                                'width' => 42,
                                'height' => 42,
                                'dismiss_label' => true,
                                'readonly' => true,
                            ),
                            1 => array (
                                'name' => 'name',
                                'label' => 'LBL_NAME',
                            ),
                            2 =>
                            array (
                                'name' => 'favorite',
                                'label' => 'LBL_FAVORITE',
                                'type' => 'favorite',
                                'readonly' => true,
                                'dismiss_label' => true,
                            ),
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'panel_body',
                        'label' => 'LBL_RECORD_BODY',
                        'columns' => 2,
                        'labelsOnTop' => true,
                        'placeholders' => true,
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'parent_name',
                                'studio' => 'visible',
                                'label' => 'LBL_PARENT_NAME',
                            ),
                            1 =>
                            array (
                                'name' => 'audit_status',
                                'studio' => 'visible',
                                'label' => 'LBL_AUDIT_STATUS',
                                'readonly' => true,
                            ),
                            2 =>
                            array (
                                'name' => 'bankname',
                                'label' => 'LBL_BANK_NAME',
                            ),
                            3 =>
                            array (
                                'name' => 'bank_id',
                                'label' => 'LBL_BANK_ID',
                            ),
                            4 =>
                            array (
                                'name' => 'tid',
                                'label' => 'LBL_TID',
                            ),
                            5 =>
                            array (
                                'name' => 'bank_sub_acc_id',
                                'label' => 'LBL_BANK_SUB_ACC_ID',
                            ),
                            6 =>
                            array (
                                'name' => 'when_entered',
                                'label' => 'LBL_WHEN',
                            ),
                            7 =>
                            array (
                                'name' => 'description',
                            ),
                            8 =>
                            array (
                                'name' => 'amount',
                                'label' => 'LBL_AMOUNT',
                            ),
                            9 =>
                            array (
                                'name' => 'audit_key',
                                'label' => 'LBL_AUDIT_KEY',
                            ),
                            10 => 'debt_amount',
                            11 => 'awkey_note'
                        ),
                    ),
                    2 =>
                    array (
                        'name' => 'panel_hidden',
                        'label' => 'LBL_SHOW_MORE',
                        'hide' => true,
                        'columns' => 2,
                        'labelsOnTop' => true,
                        'placeholders' => true,
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'subaccid',
                                'label' => 'LBL_SUBACCID',
                                'readonly' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'corresponsivebankname',
                                'label' => 'LBL_CORRESPONSIVE_BANK_NAME',
                            ),
                            2 =>
                            array (
                                'name' => 'virtualaccount',
                                'label' => 'LBL_VIRTUAL_ACCOUNT',
                            ),
                            3 =>
                            array (
                                'name' => 'corresponsiveaccount',
                                'label' => 'LBL_CORRESPONSIVE_ACCOUNT',
                            ),
                            4 =>
                            array (
                                'name' => 'virtualaccountname',
                                'label' => 'LBL_VIRTUAL_ACCOUNT_NAME',
                            ),
                            5 =>
                            array (
                                'name' => 'corresponsivebankid',
                                'label' => 'LBL_CORRESPONSIVE_BANK_ID',
                            ),
                            6 =>
                            array (
                                'name' => 'date_entered_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_ENTERED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_entered',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'created_by_name',
                                    ),
                                ),
                            ),
                            7 =>
                            array (
                                'name' => 'corresponsivename',
                                'label' => 'LBL_CORRESPONSIVE_NAME',
                            ),
                        ),
                    ),
                ),
                'templateMeta' =>
                array (
                    'useTabs' => false,
                ),
            ),
        ),
    ),
);
