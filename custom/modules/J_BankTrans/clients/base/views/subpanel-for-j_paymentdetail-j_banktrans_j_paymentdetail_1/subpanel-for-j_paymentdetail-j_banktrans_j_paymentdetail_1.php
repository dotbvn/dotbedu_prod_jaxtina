<?php
// created: 2023-04-23 00:32:40
$viewdefs['J_BankTrans']['base']['view']['subpanel-for-j_paymentdetail-j_banktrans_j_paymentdetail_1'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                'LBL_NAME' =>
                array (
                    'label' => 'LBL_NAME',
                    'enabled' => true,
                    'default' => true,
                    'name' => 'name',
                    'link' => true,
                ),
                'tid' =>
                array (
                    'name' => 'tid',
                    'label' => 'LBL_TID',
                    'enabled' => true,
                    'default' => true,
                    'link' => true,
                ),
                'amount' =>
                array (
                    'name' => 'amount',
                    'label' => 'LBL_AMOUNT',
                    'enabled' => true,
                    'currency_format' => true,
                    'default' => true,
                ),
                'when_entered' =>
                array (
                    'name' => 'when_entered',
                    'label' => 'LBL_WHEN',
                    'enabled' => true,
                    'default' => true,
                ),
                'bankname' =>
                array (
                    'name' => 'bankname',
                    'label' => 'LBL_BANK_NAME',
                    'enabled' => true,
                    'default' => true,
                ),
                'bank_sub_acc_id' =>
                array (
                    'name' => 'bank_sub_acc_id',
                    'label' => 'LBL_BANK_SUB_ACC_ID',
                    'enabled' => true,
                    'default' => true,
                ),
                'audit_status' =>
                array (
                    'name' => 'audit_status',
                    'label' => 'LBL_AUDIT_STATUS',
                    'enabled' => true,
                    'default' => true,
                ),
                'date_entered' =>
                array (
                    'name' => 'date_entered',
                    'label' => 'LBL_DATE_ENTERED',
                    'enabled' => true,
                    'readonly' => true,
                    'default' => true,
                ),
                'team_id' =>
                array (
                    'usage'=>'query_only',
                ),
            ),
        ),
    ),
    'orderBy' =>
    array (
        'field' => 'date_modified',
        'direction' => 'desc',
    ),
    'type' => 'subpanel-list',
);