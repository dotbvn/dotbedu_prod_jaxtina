<?php
$module_name = 'J_BankTrans';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 =>
              array (
                'name' => 'parent_name',
                'label' => 'LBL_PARENT_NAME',
                'enabled' => true,
                'id' => 'PARENT_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 =>
              array (
                'name' => 'tid',
                'label' => 'LBL_TID',
                'enabled' => true,
                'default' => true,
              ),
              3 =>
              array (
                'name' => 'audit_status',
                'label' => 'LBL_AUDIT_STATUS',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
              ),
              4 =>
              array (
                'name' => 'audit_key',
                'label' => 'LBL_AUDIT_KEY',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
              ),
              5 =>
              array (
                'name' => 'amount',
                'label' => 'LBL_AMOUNT',
                'enabled' => true,
                'currency_format' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'debt_amount',
                'label' => 'LBL_DEBT_AMOUNT',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
              ),
              7 =>
              array (
                'name' => 'when_entered',
                'label' => 'LBL_WHEN',
                'enabled' => true,
                'default' => true,
              ),
              8 =>
              array (
                'name' => 'bankname',
                'label' => 'LBL_BANK_NAME',
                'enabled' => true,
                'default' => true,
              ),
              9 =>
              array (
                'name' => 'subaccid',
                'label' => 'LBL_SUBACCID',
                'enabled' => true,
                'default' => true,
              ),
              10 =>
              array (
                'name' => 'parent_type',
                'label' => 'LBL_PARENT_NAME',
                'enabled' => true,
                'default' => false,
              ),
              11 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              12 =>
              array (
                'name' => 'bank_sub_acc_id',
                'label' => 'LBL_BANK_SUB_ACC_ID',
                'enabled' => true,
                'default' => false,
              ),
              13 =>
              array (
                'name' => 'bankabbreviation',
                'label' => 'LBL_BANK_ABBREVIATION',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
