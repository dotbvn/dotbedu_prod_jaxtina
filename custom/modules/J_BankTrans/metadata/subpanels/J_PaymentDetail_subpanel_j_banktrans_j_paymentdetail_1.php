<?php
// created: 2023-04-23 00:32:40
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'tid' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_TID',
    'width' => 10,
    'default' => true,
  ),
  'amount' => 
  array (
    'type' => 'currency',
    'vname' => 'LBL_AMOUNT',
    'currency_format' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'width' => 10,
    'default' => true,
  ),
  'when_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_WHEN',
    'width' => 10,
    'default' => true,
  ),
  'bankname' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_BANK_NAME',
    'width' => 10,
    'default' => true,
  ),
  'bank_sub_acc_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_BANK_SUB_ACC_ID',
    'width' => 10,
    'default' => true,
  ),
  'audit_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_AUDIT_STATUS',
    'width' => 10,
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
);