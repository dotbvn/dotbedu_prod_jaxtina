<?php
$popupMeta = array (
    'moduleMain' => 'J_BankTrans',
    'varName' => 'J_BankTrans',
    'orderBy' => 'j_banktrans.name',
    'whereClauses' => array (
  'name' => 'j_banktrans.name',
  'tid' => 'j_banktrans.tid',
  'amount' => 'j_banktrans.amount',
  'when_entered' => 'j_banktrans.when_entered',
  'bankname' => 'j_banktrans.bankname',
  'bank_sub_acc_id' => 'j_banktrans.bank_sub_acc_id',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'tid',
  5 => 'amount',
  6 => 'when_entered',
  7 => 'bankname',
  8 => 'bank_sub_acc_id',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'tid' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TID',
    'width' => 10,
    'name' => 'tid',
  ),
  'amount' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_AMOUNT',
    'currency_format' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'width' => 10,
    'name' => 'amount',
  ),
  'when_entered' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_WHEN',
    'width' => 10,
    'name' => 'when_entered',
  ),
  'bankname' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANK_NAME',
    'width' => 10,
    'name' => 'bankname',
  ),
  'bank_sub_acc_id' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANK_SUB_ACC_ID',
    'width' => 10,
    'name' => 'bank_sub_acc_id',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'TID' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TID',
    'width' => 10,
    'default' => true,
    'name' => 'tid',
  ),
  'BANKNAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_BANK_NAME',
    'width' => 10,
    'default' => true,
    'name' => 'bankname',
  ),
  'AMOUNT' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_AMOUNT',
    'currency_format' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'width' => 10,
    'default' => true,
    'name' => 'amount',
  ),
  'WHEN_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_WHEN',
    'width' => 10,
    'default' => true,
    'name' => 'when_entered',
  ),
),
);
