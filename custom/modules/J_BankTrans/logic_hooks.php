<?php
$hook_version = 2;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(0, 'Check Api Logs', 'custom/modules/J_BankTrans/logicBankTrans.php', 'logicBankTrans', 'checkapi');

$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(0, 'Color Status', 'custom/modules/J_BankTrans/logicBankTrans.php', 'logicBankTrans', 'listViewColor');

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(1,'Handle Delete','custom/modules/J_BankTrans/logicBankTrans.php', 'logicBankTrans', 'handleDelete');