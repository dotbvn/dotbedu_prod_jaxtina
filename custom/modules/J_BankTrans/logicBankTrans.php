<?php


class logicBankTrans
{

    public function handleDelete($bean, $event, $arguments){

        $rels = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(l1.id, '') receipt_id,
            IFNULL(l1.status, 0) pmd_status, IFNULL(j_banktrans.amount, 0) amount
            FROM j_banktrans INNER JOIN j_banktrans_j_paymentdetail_1_c l1_1 ON j_banktrans.id = l1_1.banktrans_id AND l1_1.deleted = 0
            INNER JOIN j_paymentdetail l1 ON l1.id = l1_1.receipt_id AND l1.deleted = 0
            WHERE (j_banktrans.id = '{$bean->id}') AND j_banktrans.deleted = 0");

        //Không được phép xoá khi có Receipt đã PAID
        foreach($rels as $key => $r){
            if($r['pmd_status'] == 'Paid'){
                $route = "App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_VietQRConfig','".$id."'));";
                echo '<script type="text/javascript">
                App.alert.show(\'message-id\', {
                level: \'error\',
                messages: \'--Has paid receipt - This log can not delete !!--<br>--REDIRECTED TO RECORD--\',
                autoClose: false});
                '.$route.'
                </script>';
                die();
            }

        }

        //Xử lý giảm số đối soát
        foreach($rels as $key => $r){
            $rc = BeanFactory::getBean('J_PaymentDetail',$r['receipt_id'], array('disable_row_level_security'=>true));
            $rc->audited_amount  -= $r['amount'];
            $rc->reconcile_status = reconcileState($rc->audited_amount, $rc->payment_amount);
            $rc->save();
        }
    }

    public function checkapi(&$bean, $event, $arguments){
        if($bean->amount <= 0) return false; //chỉ check tt tiền vào
        //Tạm
        if(!empty($bean->fetched_row)) return false;

        global $dotb_config, $timedate, $current_user, $app_list_strings;
        //TÌM: mã đối soát
        $bean->when_entered =  date('Y-m-d H:i:s', strtotime("-7 hours " . $bean->when_entered));//GMT 0 - when_entered is datetime field
        $bean->audit_status = '';
        if(empty($bean->audit_key)){
            //TEST CASE: Lấy thanh toán tạo sau cùng bởi Admin
            if($bean->tid == 'MA_GIAO_DICH_THU_NGHIEM'){
                $bean->audit_key = $GLOBALS['db']->getOne("SELECT DISTINCT IFNULL(pmd.name, '') reciept_id FROM j_paymentdetail pmd WHERE (pmd.created_by = '1') AND pmd.deleted = 0 AND pmd.payment_date = '".date('Y-m-d', strtotime($bean->when_entered))."' AND pmd.status IN ('Unpaid','Paid') ORDER BY CASE WHEN pmd.status = 'Unpaid' THEN 1 WHEN pmd.status = 'Paid' THEN 2 ELSE 3 END ASC, pmd.date_entered DESC LIMIT 1");
                $bean->name = 'TEST_ID';
            }else{
                //Check BIN
                $bin_list = $GLOBALS['app_list_strings']['vietqr_bank_list'];
                foreach($bin_list as $bin => $bank_name){
                    if(strpos(mb_strtolower($bank_name,'UTF-8'), '('.mb_strtolower($bean->bankname,'UTF-8').')') !== false){
                        $bean->bank_id = $bin;
                        break;
                    }
                }
                //Load Pattern
                if(!empty($bean->bank_id)) $row_p = getVietQRConfig(['bank_id'=>$bean->bank_id,'account_number'=>$bean->bank_sub_acc_id]);
                else $row_p = getVietQRConfig(['account_number'=>$bean->bank_sub_acc_id]);
                if(!empty($row_p)){
                    $pattern = implode('|',unencodeMultienum($row_p['pattern']));
                    $len_rid = $row_p['len_rid'];
                    $brandname  = formatQRPrefix($dotb_config['brand_id']);
                    //Tìm mã audit_key
                    switch (true) {
                        case (strpos($pattern, "BRANDNAME|RECEIPT_ID") !== false):
                            //Dạng 1: mã BRANDNAME|RECEIPT_ID
                            $prefix   = $brandname;
                            $position = stripos($bean->description, $prefix)+strlen($prefix);
                            break;
                        case (strpos($pattern, "CENTER_CODE|RECEIPT_ID") !== false):
                            //Dạng 2: mã CENTER_CODE|RECEIPT_ID
                            //lấy danh sách team theo team_set
                            if($row_p['team_id'] != $row_p['team_set_id']) $team_codes = array_column($GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(teams.code_prefix,'') team_code FROM teams INNER JOIN team_sets_teams tst ON tst.team_id = teams.id AND tst.deleted=0 WHERE tst.team_set_id = '{$row_p['team_set_id']}' AND teams.deleted = 0"),'team_code');
                            else $team_codes = [$row_p['team_code']];
                            foreach($team_codes as $_ind => $team_code){
                                //Dạng 3: mã BRANDNAME|CENTER_CODE|RECEIPT_ID
                                if((strpos($pattern, "BRANDNAME|CENTER_CODE|RECEIPT_ID") !== false))
                                    $prefix  = formatQRPrefix($brandname.$team_code);
                                else $prefix = formatQRPrefix($team_code);
                                //find position
                                if(strpos($bean->description,$prefix) !== false){
                                    $position = stripos($bean->description, $prefix)+strlen($prefix);
                                    break;
                                }
                            }
                            break;
                    }
                    if(isset($position))
                        $bean->audit_key = preg_replace('/[^a-zA-Z0-9]/','',substr($bean->description, $position, $len_rid));
                }
            }
        }
        //ĐỐI SOÁT THANH TOÁN
        if(!empty($bean->audit_key)){
            //TH1: tìm thấy Receipt Unpaid
            $row = $GLOBALS['db']->fetchOne("SELECT DISTINCT IFNULL(pmd.id, '') primaryid,
                IFNULL(pmd.name, '') receipt_id, IFNULL(l1.id, '') payment_id, IFNULL(pmd.status, '') pmd_status,
                pmd.expired_date expired_date, pmd.date_modified date_modified, pmd.payment_amount receipt_amount,
                IFNULL(l1.parent_type,'') parent_type, IFNULL(l1.parent_id, '') parent_id
                FROM j_paymentdetail pmd INNER JOIN j_payment l1 ON pmd.payment_id = l1.id AND l1.deleted = 0
                WHERE (pmd.name='{$bean->audit_key}') AND pmd.deleted = 0 AND pmd.status='Unpaid'
                ORDER BY pmd.payment_no ASC, pmd.date_modified DESC");

            //TH2: Tìm thấy Receipt tương tự nằm trong Payment trùng thông tin với receipt gốc để tiến hành đối soát - Đối soát thông minh 1
            if(empty($row['primaryid'])){
                $q2 = "SELECT DISTINCT IFNULL(l2.id, '') primaryid,
                IFNULL(l2.name, '') receipt_id, IFNULL(l1.id, '') payment_id, IFNULL(l2.status, '') pmd_status,
                (CASE WHEN (pmd.deleted=1) THEN 'deleted' ELSE IFNULL(pmd.status, '') END) old_status,
                l2.expired_date expired_date, l2.date_modified date_modified, l2.payment_amount receipt_amount,
                IFNULL(l1.parent_type,'') parent_type, IFNULL(l1.parent_id, '') parent_id
                FROM j_paymentdetail pmd INNER JOIN j_payment l1 ON pmd.payment_id=l1.id AND l1.deleted=0
                INNER JOIN j_paymentdetail l2 ON l2.payment_id=l1.id AND l2.deleted=0 AND l2.status IN ('Unpaid', 'Paid')
                WHERE (pmd.name='{$bean->audit_key}') AND ((pmd.deleted=1) OR (pmd.status='Cancelled') OR (pmd.status='Paid'))
                ORDER BY CASE WHEN (pmd_status = 'Unpaid' AND l2.payment_amount = pmd.payment_amount) THEN 1
                WHEN (pmd_status = 'Paid' AND pmd.name = l2.name) THEN 2
                WHEN (pmd_status = 'Unpaid') THEN 3
                WHEN (pmd_status = 'Paid') THEN 4 ELSE 5 END ASC,
                l2.payment_no ASC , l2.date_modified DESC";
                $row = $GLOBALS['db']->fetchOne($q2);
                if(!empty($row['primaryid'])){
                    $bean->awkey_note = "{$bean->audit_key} ({$row['old_status']}) -> {$row['receipt_id']} (SR1)";
                    $bean->audit_key  = $row['receipt_id'];
                }
            }
             //TH3: Tìm thấy Receipt của học viên có cùng số tiền và Unpaid - Đối soát thông minh 2
            if(empty($row['primaryid'])){
                $q3 = "SELECT DISTINCT IFNULL(l2.id, '') primaryid,
                IFNULL(l2.name, '') receipt_id, IFNULL(l1.id, '') payment_id, IFNULL(l2.status, '') pmd_status,
                (CASE WHEN (pmd.deleted=1) THEN 'deleted' ELSE IFNULL(pmd.status, '') END) old_status,
                l2.expired_date expired_date, l2.date_modified date_modified, l2.payment_amount receipt_amount,
                IFNULL(l1.parent_type,'') parent_type, IFNULL(l1.parent_id, '') parent_id
                FROM j_paymentdetail pmd
                INNER JOIN contacts l0 ON l0.id=pmd.parent_id AND l0.deleted=0
                INNER JOIN j_payment l1 ON l1.parent_id=l0.id AND l1.deleted=0 AND l1.team_id=pmd.team_id
                INNER JOIN j_paymentdetail l2 ON l2.payment_id=l1.id AND l2.deleted=0 AND l2.status='Unpaid'
                WHERE (pmd.name='{$bean->audit_key}') AND ((pmd.deleted=1) OR (pmd.status='Cancelled') OR (pmd.status='Paid'))
                ORDER BY CASE WHEN (l2.payment_amount = pmd.payment_amount) THEN 1 ELSE 2 END ASC,
                l2.payment_no ASC , l2.date_modified DESC";
                $row = $GLOBALS['db']->fetchOne($q3);
                if(!empty($row['primaryid'])){
                    $bean->awkey_note = "{$bean->audit_key} ({$row['old_status']}) -> {$row['receipt_id']} (SR2)";
                    $bean->audit_key  = $row['receipt_id'];
                }
            }
            //Đối soát thành công
            if(!empty($row['primaryid'])){
                $rc = BeanFactory::getBean('J_PaymentDetail',$row['primaryid'], array('disable_row_level_security'=>true));
                //Mapping fields
                $bean->parent_id        = $row['parent_id'];
                $bean->parent_type      = $row['parent_type'];
                $bean->assigned_user_id = $rc->created_by; //Noti send to Created User receipt
                $rc->assigned_user_id   = $rc->created_by;
                $bean->audit_status     = 'success';
                $bean->load_relationship('j_banktrans_j_paymentdetail_1');
                $bean->j_banktrans_j_paymentdetail_1->add($rc->id);
                $rc->audited_amount    += $bean->amount;
                $rc->reconcile_status   = reconcileState($rc->audited_amount, $rc->payment_amount);


                //Set payment method
                $mtList = $GLOBALS['app_list_strings']['payment_method_list'];
                if(array_key_exists('BANK TRANSFER',$mtList)) $rc->payment_method = 'BANK TRANSFER';
                if(array_key_exists('Bank Transfer',$mtList)) $rc->payment_method = 'Bank Transfer';
                $rc->bank_name     = $bean->bankname;
                $rc->bank_account  = $bean->bank_sub_acc_id;

                //TH1: amount >= receipt_amount -> Get Paid Receipt
                if($rc->audited_amount >= $rc->payment_amount && $rc->status == 'Unpaid'){
                    $rc->status       = 'Paid';
                    $rc->payment_date = date('Y-m-d', strtotime("+7 hours " . $bean->when_entered));//GMT +7
                    $rc->payment_datetime = $bean->when_entered;//GMT +7
                }

                //TH2: Chuyển khoản với amount < receipt_amount
                if($rc->audited_amount < $rc->payment_amount){
                }
                $rc->save();
            }
        }
        //TH3: Nhận tiền nhưng không tìm thấy thông tin đối soát
        if(empty($bean->audit_status)){
            $bean->audit_status = 'not_found';
            $bean->assigned_user_id = $current_user->id;
        }
    }

    public function listViewColor(&$bean, $event, $arguments){
        $audit_status = $bean->audit_status;
        switch ($bean->audit_status) {
            case "success":
                $colorClass = "label-success";
                break;
            case "not_found":
                $colorClass = "label-important";
                break;
            case "in_progress":
                $colorClass = "label-warning";
                break;
            default:
                $colorClass = "";
        }
        $bean->audit_status = "<div class='label ellipsis_inline $colorClass'>".$GLOBALS['app_list_strings']['audit_status_list'][$bean->audit_status]."</div>";
        if($audit_status == 'success'){
            $bean->debt_amount = $GLOBALS['db']->getOne("SELECT DISTINCT (SUM(l1.payment_amount)-SUM(l1.audited_amount)) sum_debt
                FROM j_banktrans INNER JOIN j_banktrans_j_paymentdetail_1_c l1_1 ON j_banktrans.id = l1_1.banktrans_id AND l1_1.deleted = 0
                INNER JOIN j_paymentdetail l1 ON l1.id = l1_1.receipt_id AND l1.deleted = 0
                WHERE (j_banktrans.id = '{$bean->id}') AND j_banktrans.deleted = 0 GROUP BY j_banktrans.id");

            if($bean->debt_amount <= 0 || empty($bean->debt_amount)) $bean->debt_amount = '<span style="font-weight: bold; color: #468931;">'.$GLOBALS['app_list_strings']['status_paymentdetail_list']['Paid'].'</span>';
            else $bean->debt_amount = "<span class='error'>".format_number($bean->debt_amount)."</span>";
        }

    }
}