<?php
$module_name = 'C_Rooms';
$viewdefs[$module_name] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'javascript' => '
            {dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.min.js"}
            {dotb_getscript file="custom/modules/C_Rooms/js/editview.js"}
            {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
            <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>
            ',
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'default' =>
            array (
                0 =>
                array (
                    0 => 'name',
                    1 => 'status',
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'capacity',
                        'type' => 'int',
                    ),
                    1 => 'type',
                ),
                2 =>
                array (
                    0 => 'description',
                    1 => '',
                ),
                3 =>
                array (
                    0 => 'assigned_user_name',
                    1 =>
                    array (
                        'name' => 'team_name',
                        'displayParams' =>
                        array (
                            'display' => true,
                        ),
                    ),
                ),
            ),
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'classin_user_type',
                        'label' => 'LBL_CLASSIN_USER_TYPE',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'phone_prefix',
                        'label' => 'LBL_CLASSIN_HOST_PREFIX',
                    ),
                    1 =>
                    array (
                        'name' => 'phone_mobile',
                        'label' => 'LBL_CLASSIN_HOST_ID',
                    ),
                ),
            ),
        ),
    ),
);
