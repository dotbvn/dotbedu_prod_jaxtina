<?php
$popupMeta = array (
    'moduleMain' => 'C_Rooms',
    'varName' => 'C_Rooms',
    'orderBy' => 'c_rooms.name',
    'whereClauses' => array (
  'name' => 'c_rooms.name',
  'status' => 'c_rooms.status',
  'type' => 'c_rooms.type',
  'online_meeting_type' => 'c_rooms.online_meeting_type',
  'capacity' => 'c_rooms.capacity',
  'team_name' => 'c_rooms.team_name',
),
    'searchInputs' => array (
  1 => 'name',
  3 => 'status',
  4 => 'type',
  5 => 'online_meeting_type',
  6 => 'capacity',
  7 => 'team_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => 10,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => 10,
    'name' => 'status',
  ),
  'type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => 10,
    'name' => 'type',
  ),
  'online_meeting_type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_ONLINE_MEETING_TYPE',
    'width' => 10,
    'name' => 'online_meeting_type',
  ),
  'capacity' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_CAPACITY',
    'width' => 10,
    'name' => 'capacity',
  ),
  'team_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'portallistview' => false,
      'portalrecordview' => false,
    ),
    'label' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => 10,
    'name' => 'team_name',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => 10,
    'name' => 'status',
  ),
  'TYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_TYPE',
    'width' => 10,
    'name' => 'type',
  ),
  'ONLINE_MEETING_TYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_ONLINE_MEETING_TYPE',
    'width' => 10,
    'name' => 'online_meeting_type',
  ),
  'CAPACITY' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_CAPACITY',
    'width' => 10,
    'name' => 'capacity',
  ),
  'TEAM_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
),
);
