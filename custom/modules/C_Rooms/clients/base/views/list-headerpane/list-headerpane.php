<?php


$viewdefs['C_Rooms']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_C_ROOMS',
			'tooltip' => 'LNK_NEW_C_ROOMS',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'C_Rooms',
            'route'=>'#C_Rooms/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_C_ROOMS_REPORTS',
			'tooltip' => 'LNK_C_ROOMS_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=C_Rooms',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);