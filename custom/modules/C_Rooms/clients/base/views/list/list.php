<?php
$module_name = 'C_Rooms';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'type',
                'label' => 'LBL_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'capacity',
                'label' => 'LBL_CAPACITY',
                'enabled' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'classin_user_type',
                'label' => 'LBL_CLASSIN_USER_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'phone_mobile',
                'label' => 'LBL_CLASSIN_HOST_ID',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'phone_prefix',
                'label' => 'LBL_CLASSIN_HOST_PREFIX',
                'enabled' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              8 => 
              array (
                'name' => 'location',
                'label' => 'LBL_LOCATION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              9 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
                'width' => 'xlarge',
              ),
              11 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              12 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              13 => 
              array (
                'name' => 'onl_uid',
                'label' => 'LBL_ONL_UID',
                'enabled' => true,
                'default' => false,
              ),
              14 => 
              array (
                'name' => 'online_meeting_type',
                'label' => 'LBL_ONLINE_MEETING_TYPE',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
