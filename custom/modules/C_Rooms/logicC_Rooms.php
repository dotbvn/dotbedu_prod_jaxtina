<?php

require_once ('include/externalAPI/ClassIn/utils.php');

class logicC_Rooms
{
    function handleAfterSave(&$bean, $event, $arg)
    {
        // ONLINE ROOM : CLASSIN
        if ($bean->type == 'online' && ExtAPIClassIn::hasAPIConfig() == true){
            $is_teacher = (in_array($bean->classin_user_type,['Head Teacher','Teacher User'])) ? true : false;
            if (empty($bean->onl_uid) //Register new teacher user
                || ($arg['isUpdate'] == true && !empty($arg['dataChanges']['phone_mobile'])) //change host phone number create new ClassIn account
                || ($arg['isUpdate'] == true && !empty($arg['dataChanges']['classin_user_type']))) //change user type
                registerClassInTeacher($bean, $is_teacher);
            if($arg['isUpdate'] == true && !empty($arg['dataChanges']['name'])){
                $bean->onl_uid = $GLOBALS['db']->getOne("SELECT onl_uid FROM c_rooms WHERE id = '{$bean->id}' ");
                editTeacherName($bean->onl_uid, $bean->name);
            }
        }
    }

    function handleBeforeDelete(&$bean, $event, $arg)
    {
        // ONLINE ROOM  : CLASSIN
        if ($bean->type == 'online' && ExtAPIClassIn::hasAPIConfig() == true)
            if (!empty($bean->onl_uid)) stopClassInTeacher($bean);
    }
}