<?php

$hook_version = 2;
$hook_array = Array();

$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1, 'Register Teacher User', 'custom/modules/C_Rooms/logicC_Rooms.php', 'logicC_Rooms', 'handleAfterSave');
$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(1, 'Handle delete stop Teacher User', 'custom/modules/C_Rooms/logicC_Rooms.php', 'logicC_Rooms', 'handleBeforeDelete');
