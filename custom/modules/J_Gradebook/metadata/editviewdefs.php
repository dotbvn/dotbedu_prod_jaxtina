<?php
$module_name = 'J_Gradebook';
$viewdefs[$module_name] =
array (
  'EditView' =>
  array (
    'templateMeta' =>
    array (
      'maxColumns' => '2',
      'widths' =>
      array (
        0 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '{dotb_getscript file="custom/modules/J_Gradebook/js/editview.js"}',
      'useTabs' => false,
      'tabDefs' =>
      array (
        'DEFAULT' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'form' =>
      array (
        'hidden' =>
        array (
          0 => '<input type="hidden" name="kind_of_course" id = "kind_of_course" class ="kind_of_course" value="{$kind_of_course}">',
        ),
      ),
    ),
    'panels' =>
    array (
      'default' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'name',
            'label' => 'LBL_NAME',
          ),
          1 =>
          array (
            'name' => 'j_class_j_gradebook_1_name',
            'label' => 'LBL_J_CLASS_J_GRADEBOOK_1_FROM_J_CLASS_TITLE',
            'customCode' => '<input type="text" name="j_class_j_gradebook_1_name" class="input_readonly" id="j_class_j_gradebook_1_name" value="{$class_name}" readonly>
                        <input type="hidden" name="j_class_j_gradebook_1j_class_ida" id="j_class_j_gradebook_1j_class_ida" value="{$class_id}">',
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'gradebook_config_name',
            'customCode' => '{html_options name="gradebook_config_id" id="gradebook_config_id" options=$gradebook_settings selected=$fields.gradebook_config_id.value}',
          ),
          1 =>
          array (
            'name' => 'status',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'c_teachers_j_gradebook_1_name',
          ),
          1 =>
          array (
            'name' => 'date_exam',
            'label' => 'LBL_DATE_EXAM',
          ),
        ),
        3 =>
        array (
          0 =>
          array (
            'name' => 'date_input',
            'label' => 'LBL_DATE_INPUT',
          ),
          1 => 'date_confirm',
        ),
        4 =>
        array (
          0 =>
          array (
            'name' => 'description',
            'displayParams' =>
            array (
              'rows' => 4,
              'cols' => 60,
            ),
          ),
        ),
      ),
    ),
  ),
);
