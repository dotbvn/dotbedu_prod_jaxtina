<?php
// created: 2021-02-01 10:31:45
$subpanel_layout['list_fields'] = array (
    'check_box' =>
    array (
        'vname' => '<div class="selectedTopSupanel"></div><input type="checkbox" class="checkall_custom_checkbox" module_name="J_Gradebook" onclick="handleCheckBox($(this));">',
        'type' => 'varchar',
        'studio' => 'visible',
        'width' => 5,
        'default' => true,
        'sortable' => false,
    ),
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => 10,
        'default' => true,
    ),
    'type' =>
    array (
        'type' => 'enum',
        'vname' => 'LBL_TYPE',
        'width' => 10,
        'default' => true,
    ),
    'status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_STATUS',
        'width' => 10,
    ),
    'date_exam' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_DATE_EXAM',
        'width' => 10,
        'default' => true,
    ),
    'date_input' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_DATE_INPUT',
        'width' => 10,
        'default' => true,
    ),
    'date_confirm' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_DATE_CONFIRM',
        'width' => 10,
        'default' => true,
    ),
    'c_teachers_j_gradebook_1_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_C_TEACHERS_TITLE',
        'id' => 'C_TEACHERS_J_GRADEBOOK_1C_TEACHERS_IDA',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'c_teachers_j_gradebook_1c_teachers_ida',
    ),
    'modified_by_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'readonly' => true,
        'vname' => 'LBL_MODIFIED',
        'id' => 'MODIFIED_USER_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Users',
        'target_record_key' => 'modified_user_id',
    ),
    'date_modified' =>
    array (
        'vname' => 'LBL_DATE_MODIFIED',
        'width' => 10,
        'default' => true,
    ),
    'custom_button' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'width' => 10,
        'default' => true,
        'sortable' => false,
        'align' => 'right',
    ),
);
