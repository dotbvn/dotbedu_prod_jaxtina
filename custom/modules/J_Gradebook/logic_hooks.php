<?php
    $hook_version = 1;
    $hook_array = Array();
    $hook_array['before_save'] = Array();
    $hook_array['before_save'][] = Array(1, 'set name', 'custom/modules/J_Gradebook/GradebookLogicHooks.php', 'GradebookLogicHooks', 'generate_name');

    $hook_array['after_save'] = Array();
    $hook_array['after_save'][] = Array(1, 'create notification', 'custom/modules/J_Gradebook/GradebookLogicHooks.php', 'GradebookLogicHooks', 'createNoti');
    $hook_array['after_save'][] = Array(20, 'handle other gradebook', 'custom/modules/J_Gradebook/GradebookLogicHooks.php', 'GradebookLogicHooks', 'afterSaveHandleOtherGradebook');

    $hook_array['before_delete'] = Array();
    $hook_array['before_delete'][] = Array(1, 'Delete Gradebook', 'custom/modules/J_Gradebook/GradebookLogicHooks.php','GradebookLogicHooks', 'checkBeforeDelete');

    $hook_array['after_delete'] = Array();
    $hook_array['after_delete'][] = Array(1, 'handle other gradebook', 'custom/modules/J_Gradebook/GradebookLogicHooks.php','GradebookLogicHooks', 'afterDeleteHandleOtherGradebook');

    $hook_array['process_record'] = Array();
    $hook_array['process_record'][] = Array(1, 'Add button on subpanel', 'custom/modules/J_Gradebook/GradebookLogicHooks.php','GradebookLogicHooks', 'processRecording');
?>
