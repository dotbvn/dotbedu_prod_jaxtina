var allow_submit = false;
var timeoutc = null;
function translate( lbl, module){return DOTB.language.get(module,lbl); }
$(document).ready(function () {
    allow_submit = true;
    // mass action (reload & delete) subpanel gradebook trong module class
    // reload
    $('.gradebook_checkbox,.checkall_custom_checkbox').live('click', function (e) {
        var is_check = 0;
        $('.gradebook_checkbox').each(function() {
            if($(this).is(':checked')) is_check = 1;
        });
        if(is_check){
            $('#reloadconfig_gradebook_btn').css('cursor','pointer');
            $('#reloadconfig_gradebook_btn').removeAttr('disabled');
            $('#reloadconfig_gradebook_btn').removeClass('btn-secondary');
            $('#reloadconfig_gradebook_btn').addClass('primary');
        }else {
            $('#reloadconfig_gradebook_btn').css('cursor', 'context-menu');
            $('#reloadconfig_gradebook_btn').attr('disabled', 'true');
            $('#reloadconfig_gradebook_btn').removeClass('primary');
            $('#reloadconfig_gradebook_btn').addClass('btn-secondary');
        }
    });
    $('#reloadconfig_gradebook_btn').live('click', function (e) {
        if (confirm(translate('LBL_ALERT_RELOAD_CONFIG','J_Gradebook'))){
            $('.gradebook_checkbox').each(function() {
                if($(this).is(':checked')){
                    var gradebook_id = $(this).val();
                    getGradebookDetail(gradebook_id,1);
                }
            });
        }
    });
    //delete
    $('.delete_gradebook_btn').live('click', function (e) {
        if (confirm(translate('LBL_ALERT_DELETE','J_Gradebook'))){
            var gradebook_id = $(this).attr('gradebook_id');
            deleteGradebook(gradebook_id);
            setTimeout(function() {
                showSubPanel('j_class_j_gradebook_1', null, true);
                },1500);
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////

    $("#j_class_j_gradebook_1j_class_ida").live("change", handleChangeClass);

    $("#gradebook_id").live("change", handleChangeGradebook);

    if ($("#j_class_j_gradebook_1j_class_ida").val() != '' && $("#gradebook_id") != '')
        displayFilter(false);

    $('.bnt_find').live('click', function (e) {
        var gradebook_id = $('#gradebook_id').val();
        getGradebookDetail(gradebook_id,0);
    });

    $('.bnt_reloadconfig').live('click', function (e) {
        var gradebook_id = $('#gradebook_id').val();
        getGradebookDetail(gradebook_id,1);
    });

    $('.bnt_edit_config').live('click', function (e) {
        parent.DOTB.App.router.redirect('#bwc/index.php?module=J_GradebookConfig&action=DetailView&record=' + $('#gradebook_config_id').val(), '_blank');
    });

    $('.input_mark').live('blur', handleBlurInput);
    $('.input_mark').live('change', handleBlurInput);

    $(".bnt_save").live("click", function (e) {
        saveGradebook();
    });

    $('.bnt_clear').live('click', function () {
        displayFilter(true);
    });

    $('.comment').live('click', inputComment);
    $('.select_comment').live('change', function () {
        getComment('');
    });

    $('#btn_add_vie').live('click', function () {
        $('#total_comment').val($('#total_comment').val() + '\n' + '[Vie] ')
    });

    showTooltip();
    dataTable();
    $("#exportResultExcel").mousedown(function() {
        ajaxStatus.showStatus(translate('LBL_EXPORTING','J_Gradebook'));
    });
    $("#exportResultExcel").on('click', function () {
        ajaxStatus.showStatus(translate('LBL_EXPORTING','J_Gradebook'));
        var tip = [];
        var drop_down = [];
        var input_mark = [];
        var i = 0;
        // Lưu lại tooltip và dropdown, sau đó ẩn đi
        $('.td-mark').not('input.input_mark').each(function () {
            tip[i] = $(this).find('.more').html();
            if($(this).has('.dropdown')){
                drop_down[i] = $(this).find('.dropdown').html();
            }
            i++;
        });
        i=0;
        $('.more').empty();
        $('.dropdown').empty();
        // Lưu lại thẻ input mark -> ẩn nó đi -> thêm thẻ span điểm để xuất excel
        $('.td-mark').has('input.input_mark').each(function(){
            var span = $(this).find('input.input_mark').val();
            span= "<span class='span-mark'>"+ span +"</span>";
            input_mark[i] = $(this).html();
            $(this).empty();
            $(this).append(span);
            i++;
        });
        //
        var uri = $('#gradebook_mark').find("table#config_content").btechco_excelexport({
            containerid     : "gradebook_mark",
            datatype        : $datatype.Table,
            worksheetName   : $('input#j_class_j_gradebook_1_name').val() +' - '+ $('select#gradebook_id :selected').text().trim(),
            returnUri       : false  //Fix bug Unlimit row - By Lap Nguyen - HEHEHE
        });
        i=0;
        // Hiện lại thẻ input mark, remove thẻ span điểm
        $('.td-mark').has('span.span-mark').each(function(){
            $(this).html(input_mark[i]);
            i++;
        });
        $('.span-mark').remove();
        i=0;
        // Hiển thị lại tooltip và dropdown
        $('.td-mark').not('input.input_mark').each(function () {
            $(this).find('.more').html(tip[i]);
            if($(this).has('.dropdown')){
                $(this).find('.dropdown').html(drop_down[i]);
            }
            i++;
        });
        ajaxStatus.hideStatus();
    });
});
function showTooltip() {
    $('.more_tip').each(function() {
        $(this).tipTip({maxWidth: "auto",
            edgeOffset: 10,
            content: $(this).attr('tip'),
            defaultPosition: "top"});
    });


}
function dataTable() {
    var table = $('#config_content').DataTable({
        scrollY:        true,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        info: false,
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ['td-mark'],
        }],
    });
    $('.DTFC_LeftBodyWrapper').find('input[name="student_id[]"]').attr('name','studentid_fixedcolumn');
}

function getComment(over_write_text) {
    over_write_text = typeof over_write_text !== 'undefined' ? over_write_text : '';
    var keys = {};
    var values = [];
    var student_name = $("#dialog_student_name").text() + ",";
    values.push(student_name);
    $('.select_comment').each(function () {
        var _this = $(this);
        var val = _this.val();
        if (val != '') {
            keys[_this.attr('id')] = val;
            if (_this.attr('id') == 'gradebook_other_comment') {
                values.push(_this.val());
            } else
                values.push($(this).find('option[value="' + val + '"]').text());
        }
    });
    if (over_write_text == '')
        $("[name=dialog_text_comment]").val(values.join(" ")).effect("highlight", {color: '#ff9933'}, 1000);
    else $("[name=dialog_text_comment]").val(over_write_text)
    $("[name=dialog_key_comment]").val(JSON.stringify(keys));
}

function inputComment() {
    var _this = $(this);
    var comment_name = _this.attr('config-name');
    var student_name = _this.closest('tr').find('span.student_name').text();
    var class_name = $('input[name=j_class_j_gradebook_1_name]').val();
    var comment_key = _this.find('input[name="key_teacher_' + comment_name + '[]"]').val();
    var current_cmt = _this.find('input[name="value_teacher_' + comment_name + '[]"]').val();
    try {
        var comment_keys = JSON.parse(comment_key);
    } catch (e) {
        var comment_keys = {};
    }
    $('.dialog_content select').val('');
    $('.gradebook_other_comment').val('');
    $('.gradebook_other_comment').text('');
    for ($id in comment_keys) {
        if ($id == 'gradebook_other_comment') {
            $('#gradebook_other_comment').val(comment_keys[$id]);
            $('#gradebook_other_comment').text(comment_keys[$id]);
        } else {
            $('select#' + $id).val(comment_keys[$id]);
            $('select#' + $id).select2();
        }
        //$('select#'+$id).select2('val',comment_keys[$id]);
    }
    ;
    $('.dialog_content select').select2()
    $('.dialog_header span#dialog_student_name').text(student_name);
    $('.dialog_header span#dialog_student_class').text(class_name);
    getComment(current_cmt);
    $('#comment_dialog').hide();
    $('#comment_dialog').dialog({
        title: translate('LBL_TEACHER_COMMENT','J_Gradebook'),
        width: "900px",
        resizable: false,
        modal: true,
        buttons:
        [
            {
                text: "Post",
                class: 'button primary',
                click: function () {
                    _this.find('input[name="key_teacher_' + comment_name + '[]"]').val($("[name=dialog_key_comment]").val());
                    _this.find('input[name="value_teacher_' + comment_name + '[]"]').val($("[name=dialog_text_comment]").val());
                    if ($("[name=dialog_text_comment]").val().toString().length > 35) {
                        _this.find('.value_teacher_' + comment_name).text($("[name=dialog_text_comment]").val().toString().substring(0, 35) + '...');
                    } else if ($("[name=dialog_text_comment]").val().toString().length > 0) {
                        _this.find('.value_teacher_' + comment_name).text($("[name=dialog_text_comment]").val().toString());
                    } else {
                        _this.find('.value_teacher_' + comment_name).text('--None--');
                    }
                    _this.find('.value_teacher_' + comment_name).attr('title', $("[name=dialog_text_comment]").val());
                    $(this).dialog("close");
                }
            }, {
                text: "Cancel",
                class: 'button',
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function saveGradebook() {
    if (!checkError())
        return;
    if (!allow_submit) return;
    var form = jQuery("#GradebookInputMark");
    DOTB.ajaxUI.showLoadingPanel();
    jQuery.ajax({  //Make the Ajax Request
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data: form.serialize(),
        dataType: "json",
        success: function (data) {
            if (data.success == "1") {
                toastr.success(translate('LBL_SAVED_SUCCESS','J_Gradebook'));
                window.onbeforeunload = null;
            } else
                toastr.error(DOTB.language.get('J_Gradebook', data.errorLabel));
            DOTB.ajaxUI.hideLoadingPanel();
        },
        error: function () {
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
            DOTB.ajaxUI.hideLoadingPanel();
        }
    });
}

function handleBlurInput() {
    var _this = $(this);
    var mark = Numeric.parse(_this.val());
    _this.val(Numeric.toFloat(mark, 2, 2));
    var gradebook_progress = $('#gradebook_progress').val();
    //Get Band Score List
    var band_score_list = '';

    if ($('#band_score').val() != null && $('#band_score').val() != '')
        band_score_list = DOTB.language.languages['app_list_strings'][$('#band_score').val()];

    if (!Number(_this.attr('config-readonly')) && mark > Number(_this.attr('config-max'))) {
        allow_submit = false;
        toastr.error( translate('LBL_ALERT_VALID_INPUT','J_Gradebook') +' [0-' + _this.attr('config-max') + ']');
        add_error_style('GradebookInputMark', _this.attr('id'), '', true);
        _this.focus();
    } else {
        allow_submit = true;
        _this.closest('tr').find('input.input_mark[config-readonly="1"]').each(function () {
            var max_mark = Number($(this).attr('config-max'));
            var input_type = $(this).attr('config-type');
            var formula = $(this).attr("config-formula");
            formula = formula.replace("=", '');
            var eval_formula = formula;
            // Re-construct data into right format  - HuyHoang -
            var json_progress = JSON.parse($(this).attr("config-mark-progress"));
            var object_progress_key = Object.keys(json_progress);
            for (var index = 0; index < object_progress_key.length; index++) {
                // Split (eg: P01 => P, 01)
                let key = object_progress_key[index].split("P");
                let int_progress = parseInt(key[1])
                object_progress_key[index] = "P" + int_progress
            }
            Object.keys(json_progress).forEach((key, index) => {
                json_progress[object_progress_key[index]] = json_progress[key];
                delete json_progress[key];
            });
            var map_obj_progress = new Map(Object.entries(json_progress));
            var val = 0.0;
            var is_null = 1;
            for (var i = 0; i < formula.length; i++) {
                var next_c = formula[i + 1];
                var col_in_progress = '';
                var formula_slice = formula.slice(i);
                var position_count = formula_slice.indexOf('COUNT');
                var position_sum = formula_slice.indexOf('SUM');
                var position_average = formula_slice.indexOf('AVERAGE');
                if(position_count === 0 || position_sum === 0 || position_average === 0){
                    is_null = 0;
                    var end_count = formula_slice.indexOf(')');
                    var colon = formula_slice.indexOf(':');
                    var bracket = formula_slice.indexOf('[');
                    var count_col = 0;
                    var sum_col = 0.00;
                    var replace = '';
                    if(formula_slice[end_count-1] >= '0' && formula_slice[end_count-1] <= '9'){
                        var begin = '';
                        if(formula_slice[colon-2]==='P') begin += formula_slice[colon-1];
                        else begin += formula_slice[colon-2] + formula_slice[colon-1];
                        begin = parseFloat(begin);
                        var end = '';
                        if(formula_slice[end_count-2]==='P') end += formula_slice[end_count-1];
                        else end += formula_slice[end_count-2] + formula_slice[end_count-1];
                        end = parseFloat(end);
                        replace = 'P'+begin+':P'+end;
                        var j = 0;
                        for( j = begin; j <= end; j++){
                            var progress = map_obj_progress.get('P'+j); //get value by key
                            if(typeof progress != 'undefined'){
                                count_col++;
                                var map_obj_mark = new Map(Object.entries(progress));
                                var result_progress = map_obj_mark.get('final');
                                sum_col += parseFloat(result_progress);
                            }
                        }
                    }
                    else {
                        var begin_word = formula_slice[colon-1].charCodeAt() ;
                        var end_word = formula_slice[colon+1].charCodeAt() ;
                        replace += formula_slice[colon-1] + ':' + formula_slice[colon+1];
                        for( var j = begin_word; j <= end_word; j++){
                            var word = String.fromCharCode(j);
                            var col_word = _this.closest('tr').find('input.input_mark[config-alias="' + word + '"]');
                            var t = col_word.attr('config-type');
                            if(col_word.attr('config-type') === 'score' || col_word.attr('config-type') === 'formula' || col_word.attr('config-type') === 'total'){
                                count_col++;
                                sum_col += parseFloat(col_word.val());
                            }
                        }
                    }
                    var name_function = '';
                    var result_function = 0.00;
                    if(position_count === 0) {
                        name_function = 'COUNT(';
                        result_function = count_col;
                    } else if(position_sum === 0) {
                        name_function = 'SUM(';
                        result_function = sum_col;
                    } else {
                        name_function = 'AVERAGE(';
                        if(count_col !== 0)
                            result_function = Numeric.toFloat(sum_col/count_col, 2, 2);
                    }
                    eval_formula = eval_formula.replace(name_function + replace + ')', result_function);
                    i += end_count;
                }
                if(formula[i]=='P' && next_c >= '1' && next_c <= '9' ){
                    if(next_c == '1' && formula[i+2] >= '0' && formula[i+2] < '3'){
                        next_c += formula[i+2];
                        if(formula[i+3]==='[')
                            col_in_progress = formula[i+4];
                    }
                    var key_progress = 'P'+ next_c;
                    var key_val = '';
                    if(formula[i+2]==='[')
                        col_in_progress = formula[i+3];
                    if(col_in_progress === ''){
                        key_val = 'final';
                    }
                    else {
                        key_val = col_in_progress;
                        next_c += '['+col_in_progress+']';
                    }
                    var progress = map_obj_progress.get(key_progress); //get value by key
                    if(typeof progress != 'undefined'){
                        var map_obj_mark = new Map(Object.entries(progress));
                        var check_val = map_obj_mark.get(key_val);
                        val = 0.00;
                        if(typeof check_val != 'undefined'){
                            val = parseFloat(check_val); //get value by key
                            is_null = 0;
                        }
                    }
                    else {
                        val = 0.00;
                    }
                    eval_formula = eval_formula.replace(formula[i] + next_c, val);
                }else
                    if (formula[i].charCodeAt() >= 65 && formula[i].charCodeAt() <= 90 && formula[i-1] !=='[') { //form A->Z
                        val = Numeric.parse(_this.closest('tr').find('input.input_mark[config-alias="' + formula[i] + '"]').val());
                        //Parse Fomulate
                        if(val === ''){
                            val = 0.00;
                        }
                        else is_null = 0;
                        eval_formula = eval_formula.replace(formula[i], val);
                    }
            }
            var result_eval = Numeric.toFloat(math.eval(eval_formula), 2, 2);
            //Handle band formula
            if (input_type == 'band') {
                //load app_list_string
                var options = $(this).attr("config-options");
                var ojbs = DOTB.language.languages['app_list_strings'][options];
                var result_band = '';
                if (typeof ojbs != 'undefined') {
                    $.each(ojbs, function (key, value) {
                        var replace = '';
                        //Dạng so sánh x dùng toán tử _ (vd: 5_10 -> 5 < x < 10)
                        if(key.toLowerCase().indexOf('_') !== -1){
                            key = key.toLowerCase().replace('_', ' < x < ');
                        }
                        else if(key.toLowerCase().indexOf(',') !== -1){ //Dạng so sánh x dùng dấu ( ) [ ] (vd: (5,10] -> 5 < x <= 10)
                            var before = '<';
                            var after  = '<';
                            if(key.toLowerCase().indexOf('[') !== -1){
                                before += '=';
                                key = key.replace('[', '');
                            }
                            if(key.toLowerCase().indexOf(']') !== -1){
                                after  += '=';
                                key = key.replace(']', '');
                            }
                            key = key.replace('(', '');
                            key = key.replace(')', '');
                            key = key.toLowerCase().replace(',', before+' x '+after);
                        }

                        //Dạng so sánh x dùng toán tử >, <, >=, <=
                        key = key.toLowerCase().replace('x', result_eval);
                        result_band = eval("if(math.eval('" + key + "')) '" + value + "'; else '';");
                        if (result_band != '') return false;
                    });
                }
                $(this).val(result_band);
            } else {
                if (max_mark)
                    $(this).val(result_eval);
                else
                    $(this).val(0);
                if(is_null) $(this).val('');
            }
        });
    }
}

function getGradebookDetail(gb_id, reloadconfig) {
    if (typeof reloadconfig == 'undefined' || reloadconfig == '') reloadconfig = 0;
    var _this = $(this);
    DOTB.ajaxUI.showLoadingPanel();
    displayFilter(true);
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data: {
            process_type: "getGradebookDetail",
            gradebook_id: gb_id,
            reloadconfig: reloadconfig,
        },
        dataType: "json",
        success: function (data) {
            $(".gradebook_detail").html(data.html);
            $("input[name=grade_config]").val(JSON.stringify(data.grade_config));
            //Trigger input mark
            if (typeof data.grade_config['AA'] === 'undefined') //Check Overall Gradebook
                displayFilter(false);
            DOTB.ajaxUI.hideLoadingPanel();
            showTooltip();
            dataTable();

            if(reloadconfig){ //trigger blur all input
                $('input.input_mark').each(function(){
                    $(this).trigger('blur');
                });
            }
        },
        error: function () {
            DOTB.ajaxUI.hideLoadingPanel();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
        },
    });
}

function deleteGradebook(gb_id) {
    DOTB.ajaxUI.showLoadingPanel();
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data: {
            process_type: "deleteGradebook",
            gradebook_id: gb_id,
        },
        dataType: "json",
        success: function (data) {
            DOTB.ajaxUI.hideLoadingPanel();
            toastr.success(translate('LBL_ALERT_DELETED','J_Gradebook'));
        },
        error: function () {
            DOTB.ajaxUI.hideLoadingPanel();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
        },
    });
}

function handleChangeGradebook() {
    var option          = $(this).find('option:selected');
    var teacher_id      = option.attr('teacher_id');
    var teacher_name    = option.attr('teacher_name');
    var description     = option.attr('gb_description');
    var gradebook_name  = option.text();
    var gradebook_id    = $(this).val();
    $('#teacher_link_url').text(teacher_name);
    $('#grade_description').text(description);
    $('#gradebook_link_url').text(gradebook_name);
    $('#gradebook_link_url').attr("href","#bwc/index.php?module=J_Gradebook&action=DetailView&record="+gradebook_id);
    // Convert Link BWC Frame
    var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
    bwcComponent.rewriteLinks();
}

function handleChangeClass() {
    //Add by Lap Nguyen -- Clean some input
    ajaxStatus.showStatus('Proccessing...');
    $("#gradebook_id").parent().append('<span id = "config_loading" ><img src="custom/include/images/loader.gif" align="absmiddle" width="16"></span>');
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data: {
            process_type: "getGradebook",
            class_id: $('#j_class_j_gradebook_1j_class_ida').val(),
        },
        success: function (data) {
            data = JSON.parse(data);
            ajaxStatus.hideStatus();
            $("#gradebook_id").html(data.html);
            $("#gradebook_id").val($("#gradebook_id option:first").val());
            $("#gradebook_id").trigger("change");
            $("#config_loading").remove();
        },
        error: function () {
            ajaxStatus.hideStatus();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
            $("#gradebook_id").html("");
            $("#gradebook_id").val($("#gradebook_id option:first").val());
            $("#gradebook_id").trigger("change");

            $("#config_loading").remove();
        },
    });
}

function displayFilter(visible) {
    if (visible) {
        $("#btn_j_class_j_gradebook_1_name, #j_class_j_gradebook_1_name, #gradebook_id").show();
        $("#class_link_url,#gradebook_link_url").hide();

        $('.bnt_save').hide();
        $('.bnt_find').show();

        $(".gradebook_detail, .config_button").hide();
    } else {
        $("#btn_j_class_j_gradebook_1_name, #j_class_j_gradebook_1_name, #gradebook_id").hide();
        $("#class_link_url, #gradebook_link_url").show();
        $('.bnt_save').show();
        $('.bnt_find').hide();

        $(".gradebook_detail, .config_button").show();
    }
}

function showUploadMark(_element) {
    var type = $(_element).attr('data-mark');
    var import_template_link = $(_element).attr('template_link');
    $('#upload_dialog').dialog({
        title: translate('LBL_IMPORT_GB','J_Gradebook'),
        width: "500px",
        resizable: false,
        modal: true,
    });
    $('.import_template_link').attr("onclick",import_template_link);
    $('.submit_file_mark').val(type);
}

function uploadFileMark(_element) {
    app.alert.show('uploading', {
        level: 'process',
        title: translate('LBL_UPLOADING','J_Gradebook')
    });
    var file = $('.fileToUpload').prop('files')[0];
    var data = new FormData();
    var type = $(_element).val();
    var id = $('#gradebook_id').val();
    data.append('file', file);
    $.ajax({
        url: 'index.php?module=J_Gradebook&action=handelAjaxsInputMark&dotb_body_only=true&type=ajaxGetMarkFromExcel&id=' + id + '&mark_type=' + type,
        method: 'POST',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            showMark(data, type);
        }

    })
}

function showMark(data, type) {
    var json = jQuery.parseJSON(data);
    app.alert.dismiss('uploading');
    if (json['success'] == "1") {
        json['data'].forEach(function (value) {
            $(value['id']).val(value['mark']);
        })
    } else {
        app.alert.show('error-mess', {
            level: 'error',
            messages: app.lang.get(json.errorLabel, 'Contacts'),
            autoClose: true
        });
    }
    $('#upload_dialog').dialog("close");
    $("#config_content tbody tr").find('.input_mark').each(function (index, value) {
        $(this).trigger('blur');
    })
}
function checkError() {
    var check = 0;
    $('.input_mark').each(function (index, element) {
        var mark = $(element).val();
        if (!Number($(element).attr('config-readonly')) && mark > Number($(element).attr('config-max'))) {
            allow_submit = false;
            $(element).click();
            check = 1;
        }
    })
    if (check == 0) return true;
    else return false;
}
