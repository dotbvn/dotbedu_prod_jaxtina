function translate( lbl, module){return DOTB.language.get(module,lbl); }
function checkDuplicateTest(from_id){
    ajaxStatus.showStatus(translate('LBL_PROCESSING','J_Gradebook'));
    var gradebook_id        = $('#'+from_id).find('input[name=record]').val();
    var classID             = $('#'+from_id).find('input[name=j_class_j_gradebook_1j_class_ida]').val();
    var gradebook_config_id = $('#'+from_id).find('select[name=gradebook_config_id]').val();
    var check = false;
    if(gradebook_config_id == ''){
        ajaxStatus.hideStatus();
        toastr.error(translate('LBL_ERR_GBSETTING1','J_Gradebook'));
        return false;
    }
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: false,
        data:{
            process_type        : "checkDuplicateTest",
            gradebook_id        : gradebook_id,
            class_id            : classID,
            gradebook_config_id : gradebook_config_id,
        },
        success: function(data){
            ajaxStatus.hideStatus();
            if(data) {
                toastr.error(translate('LBL_ERR_GBSETTING2','J_Gradebook'));
                check = false;
            }else
                check = true;

        },
        error: function(){
            ajaxStatus.hideStatus();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
            check = false;
        },
    });
    return check;
}
function check_form(formname) {
    if (typeof (siw) != 'undefined' && siw && typeof (siw.selectingSomething) != 'undefined' && siw.selectingSomething)
    {
        return false;
    }

    if(document.getElementById(formname).module.value == 'J_Gradebook') {
        if(validate_form(formname, '') && checkDuplicateTest(formname)) {
            setTimeout(function() {
                showSubPanel('j_class_j_gradebook_1', null, true);
                },1500);
            return true;
        } else return false;

    } else {
        if(validate_form(formname, '')) return true; else return false;
    }
    return false;
}
$(document).ready(function(){
    $('#gradebook_config_id').live('change',function() {
        var record_id = $('input[name=record]').val();
        if(record_id == ''){
            checkDuplicateTest($('#gradebook_config_id').closest('form').attr('id'));
        }
    });
    $('#status').live('change',function() {
        if($(this).val() == 'Approved') {
            var today = new Date();
            var date = ('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear();
            $('#date_confirm').val(date);
        }
    });


});
//end
