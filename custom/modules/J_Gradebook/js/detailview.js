function translate( lbl, module){return DOTB.language.get(module,lbl); }
$(document).ready(function(){
    $("body").append("<div id='displayConfig' style = 'display:none'></div>");
    $("body").append("<div id='displayAddStudent' style = 'display:none'></div>");

    if(Number($('#lock_update').val()))  {
        $("#edit_button").remove();
    }
    // $("#sendNotification").live('click', sendNoti();
    $('.more').hover(function () {
        $(this).find('.moreinfo_tip').css("display", "").fadeIn("fast");
        }, function () {
            $(this).find('.moreinfo_tip').css("display", "none").fadeOut("fast");
    });
    // remove student in gradebook
    $('.remove_student_btn').live('click', function (e) {
        if (confirm(translate('LBL_ALERT_REMOVE','J_Gradebook'))){
            var student_id = $(this).attr('student_id');
            RemoveStudent(student_id);
        }
    });
    // Add student in gradebook
    $('#add_student_btn').live('click', function (e) {
        ShowDialogAddStudent();
    });
});
function viewConfig(gradebook_id) {
    var _this = $(this);
    ajaxStatus.showStatus(translate('LBL_PROCESSING','J_Gradebook'));
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data:{
            process_type: "showConfig",
            gradebook_id: gradebook_id,
        },
        success: function(data){
            data = JSON.parse(data);
            ajaxStatus.hideStatus();
            $("#displayConfig").html(data.html);
            $("#displayConfig").dialog({
                title: translate('LBL_GRADEBOOKCONFIG_NAME','J_Gradebook'),
                width: "800px",
                resizable: false,
                modal: true,
                buttons: [
                    {
                        text  : translate('LBL_CLOSE','J_Gradebook'),
                        class : 'button primary',
                        click : function() {
                            $(this).dialog("close");
                        }
                    },
                ]
                }
            );
        },
        error: function(){
            ajaxStatus.hideStatus();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
        },
    });
}

function sendNoti() {
    var _this = $(this);
    ajaxStatus.showStatus(translate('LBL_PROCESSING','J_Gradebook'));
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data:{
            process_type: "sendNoti",
            gradebook_id: $( "input[name='record']" ).val(),
        },
        success: function(data){
            if(data == '1'){
                toastr.success(translate('LBL_STUDENT_ALERT4','J_Gradebook'));
            }else
                toastr.error(translate('LBL_STUDENT_ALERT5','J_Gradebook'));
            ajaxStatus.hideStatus();
        },
        error: function(){
            ajaxStatus.hideStatus();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
        },
    });
}
function ShowDialogAddStudent(){
    ajaxStatus.showStatus(translate('LBL_PROCESSING','J_Gradebook'));
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data:{
            process_type: "showAddStudentList",
            gradebook_id: $( "input[name='record']" ).val(),
        },
        success: function(data){
            data = JSON.parse(data);
            ajaxStatus.hideStatus();
            $("#displayAddStudent").html(data.html);
            $("#displayAddStudent").dialog({
                title: DOTB.language.get('J_Gradebook','LBL_ADD_STUDENT'),
                resizable: false,
                width:900,
                height:500,
                modal: true,
                visible: false,
                dialogClass: 'fixed-dialog',
                buttons:{
                    "Add":{
                        click:function() {
                            if (confirm(DOTB.language.get('J_Gradebook','LBL_STUDENT_ALERT1'))){
                                var is_check = 0;
                                var str_student_id = '';
                                $('.student_checkbox').each(function() {
                                    if($(this).is(':checked')){
                                        is_check = 1;
                                        var student_id = $(this).val();
                                        str_student_id += student_id + ',';
                                    }
                                });
                                if(!is_check)
                                    toastr.error(DOTB.language.get('J_Gradebook','LBL_STUDENT_ALERT2'));
                                else {
                                    AddStudent(str_student_id);
                                    $('#displayAddStudent').dialog('close');
                                }
                            }
                        },
                        class   : 'button primary btn_add_std',
                        text    : DOTB.language.get('J_Gradebook','LBL_ADD'),
                    },
                    "Close":{
                        click:function() {
                            $(this).dialog('close');
                        },
                        class   : 'button ',
                        text    : DOTB.language.get('J_Gradebook','LBL_CLOSE'),
                    },
                },
                }
            );
        },
        error: function(){
            ajaxStatus.hideStatus();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
        },
    });
}

function AddStudent(student_id_list){
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data: {
            process_type: "addStudent",
            gradebook_id: $( "input[name='record']" ).val(),
            student_id_list: student_id_list,
        },
        dataType: "json",
        success: function (data) {
            toastr.success(translate('LBL_STUDENT_ALERT3_1','J_Gradebook'));
            ajaxStatus.hideStatus();
            $("#report_results").html(data.html);
            // Convert Link BWC Frame
            var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
            bwcComponent.rewriteLinks();
        },
        error: function () {
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
        },
    });
}
function RemoveStudent(student_id){
    DOTB.ajaxUI.showLoadingPanel();
    jQuery.ajax({
        url: "index.php?module=J_Gradebook&dotb_body_only=true&action=ajaxGradebook",
        type: "POST",
        async: true,
        data: {
            process_type: "removeStudent",
            gradebook_id: $( "input[name='record']" ).val(),
            student_id: student_id,
        },
        dataType: "json",
        success: function (data) {
            DOTB.ajaxUI.hideLoadingPanel();
            toastr.success(DOTB.language.get('J_Gradebook','LBL_STUDENT_ALERT3'));
            ajaxStatus.hideStatus();
            $("#report_results").html(data.html);
            // Convert Link BWC Frame
            var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
            bwcComponent.rewriteLinks();
        },
        error: function () {
            DOTB.ajaxUI.hideLoadingPanel();
            toastr.error(translate('LBL_ERROR_OCCURRED','J_Gradebook'));
        },
    });
}
$("#exportResultExcel").mousedown(function() {
    ajaxStatus.showStatus(translate('LBL_EXPORTING','J_Gradebook'));
});
$("#exportResultExcel").on('click', function () {
    ajaxStatus.showStatus(translate('LBL_EXPORTING','J_Gradebook'));
    var tip = [];
    var i = 0;
    $('.more').each(function () {
        tip[i] = $(this).html();
        i++;
    });
    $('.more').empty();
    var uri = $("#markdetail_content").btechco_excelexport({
        containerid     : "report_results",
        datatype        : $datatype.Table,
        worksheetName   : $('span#j_class_j_gradebook_1j_class_ida').text() +' - '+ $('span#name').text(),
        returnUri       : false  //Fix bug Unlimit row - By Lap Nguyen - HEHEHE
    });
    i=0;
    $('.more').each(function () {
        $(this).html(tip[i]);
        i++;
    });
    ajaxStatus.hideStatus();
});
