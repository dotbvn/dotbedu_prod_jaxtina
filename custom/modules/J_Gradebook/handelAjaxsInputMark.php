<?php
require_once("custom/include/_helper/junior_gradebook_utils.php");
require_once("custom/modules/J_Gradebook/GradebookFunctions.php");
switch ($_REQUEST['type']) {
    case 'ajaxDownloadTemplate':
        $result = ajaxDownloadTemplate();
        break;
    case 'ajaxGetMarkFromExcel':
        $result = ajaxGetMarkFromExcel();
        break;
}
echo $result;
die;

function ajaxDownloadTemplate() {
    require_once("custom/include/PHPExcel/Classes/PHPExcel.php");
    $bean = BeanFactory::getBean('J_Gradebook',$_GET['id']);
    $class = $bean->j_class_j_gradebook_1_name;
    $class_name = $bean->name;
    $bean->_constructDefault(false);
    $fileName="custom/include/TemplateExcel/Import/Template_Import_StudentMark.xlsx";
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('B2', "Class: ".$class."")
    ->setCellValue('B3', "Name: ".$class_name."")
    ->setCellValue('D4', $bean->config[$_GET['mark']]['label'].'('.$bean->config[$_GET['mark']]['max_mark'].')');

    $i =5;
    foreach ($bean->students as $student_id => $student){
        $student_mark = $bean->gradebookDetail[$student_id];
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, $i-4)
        ->setCellValue('B'.$i, $student->contact_id)
        ->setCellValue('C'.$i, $student->name)
        ->setCellValue('D'.$i, $student_mark[$_GET['mark']]);


        //
        $i++;
    }
    PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007')->save('Template_Import_StudentMark.xlsx');
    if (!headers_sent()) {
        foreach (headers_list() as $header)
            header_remove($header);
    }
    ob_clean();
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="Template_Import_StudentMark.xlsx"');
    header("Content-Transfer-Encoding: binary");
    readfile('Template_Import_StudentMark.xlsx');
    unlink('Template_Import_StudentMark.xlsx');
    die();
}

function ajaxGetMarkFromExcel(){
    require_once("custom/include/PHPExcel/Classes/PHPExcel.php");
    $info = pathinfo($_FILES['file']['name']);
    $ext = $info['extension']; // get the extension of the file

    if ($ext != "xlsx" && $ext != "xls") {
        return json_encode(
            array(
                "success" => "0",
                "errorLabel" => "LBL_PLEASE_UPLOAD_EXCEL_FILE",
            )
        );
    }
    $bean = BeanFactory::getBean('J_Gradebook',$_GET['id']);
    $bean->_constructDefault(false);

    $fdir = 'gradebook';
    if (!file_exists("upload/$fdir")) mkdir("upload/$fdir", 0777, true);

    $newname = "stdent_mark_list." . $ext;
    $filename = "upload/$fdir/". $newname;
    unlink($filename);
    move_uploaded_file($_FILES['file']['tmp_name'], $filename);

    $inputFileType = PHPExcel_IOFactory::identify($filename);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load("$filename");
    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();

    $markArray = array();
    for ($row = 5; $row <= $highestRow; ++$row) {
        $studen_code = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
        $mark = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
        foreach ($bean->students as $student_id => $student){
            if($studen_code == $student->contact_id){
                $markArray[$row-5]['id'] = '#'.$student_id.'-'.$_GET['mark_type'];
                break;}
        }
        $markArray[$row-5]['mark'] = $mark;
    }
    unlink($filename);
    return json_encode(
        array(
            "success" => "1",
            "data" => $markArray
        )
    );
}
?>