<?php
    require_once("custom/include/_helper/junior_gradebook_utils.php");
    require_once("custom/modules/J_Gradebook/GradebookFunctions.php");

    switch ($_POST['process_type']) {
        case 'getGradebook':
            $result = json_encode(getGradebook($_POST['class_id']));
            echo $result;
            break;
        case 'getGradebookDetail':
            $result = json_encode(getGradebookDetail($_POST['gradebook_id'], $_POST['reloadconfig']));
            echo $result;
            break;
        case 'inputMark':
            $result = saveInputMark($_POST);
            echo $result;
            break;
        case 'showConfig':
            $result = showConfig($_POST['gradebook_id']);
            echo $result;
            break;
        case 'showAddStudentList':
            $result = showAddStudentList($_POST['gradebook_id']);
            echo $result;
            break;
        case 'checkDuplicateTest':
            $result = checkDuplicateTest($_POST['gradebook_id'], $_POST['class_id'], $_POST['gradebook_config_id']);
            echo $result;
            break;
        case 'sendNoti':
            $result = sendNoti($_POST['gradebook_id']);
            echo $result;
            break;
        case 'deleteGradebook':
            $result = deleteGradebook($_POST['gradebook_id']);
            echo $result;
            break;
        case 'removeStudent':
            $result = removeStudent($_POST['gradebook_id'], $_POST['student_id']);
            echo $result;
            break;
        case 'addStudent':
            $result = addStudent($_POST['gradebook_id'], $_POST['student_id_list']);
            echo $result;
            break;
    }
    die;
?>
