<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class J_GradebookViewDetail extends ViewDetail {
    function preDisplay(){
        global $timedate, $dotb_config, $current_user;

        // Tự động add std trong lớp vào gradebook nếu gradebook chưa có std nào
        $sql_get_std_in_gb = "SELECT j_gradebook_contacts_1contacts_idb
        FROM j_gradebook_contacts_1_c
        WHERE deleted = 0 AND j_gradebook_contacts_1j_gradebook_ida = '{$this->bean->id}'";
        $student_id = $GLOBALS['db']->getOne($sql_get_std_in_gb);
        if(empty($student_id)) {
            $list_std_id = '';
            $sql_get_std_in_class = "SELECT student_id
            FROM j_classstudents
            WHERE deleted = 0 AND class_id = '{$this->bean->j_class_j_gradebook_1j_class_ida}' AND student_type = 'Contacts'";
            $result1 = $GLOBALS['db']->query($sql_get_std_in_class);
            while($row1 = $GLOBALS['db']->fetchByAssoc($result1)){
                $list_std_id .= $row1['student_id'].',';
            }
            if(!empty($list_std_id)){
                require_once("custom/modules/J_Gradebook/GradebookFunctions.php");
                addStudent($this->bean->id,$list_std_id);
            }
        }
        ////////
        parent::preDisplay();

        $this->ss->assign('MARKDETAIL', $this->bean->getGradebookDetailView());

        $this->ss->assign('LOCKUPDATE', 0);

        if( ACLController::checkAccess('J_Gradebook', 'export', true) || ($current_user->isAdmin()))
        $this->ss->assign('can_export', 1);
        else $this->ss->assign('can_export', 0);
    }
}

