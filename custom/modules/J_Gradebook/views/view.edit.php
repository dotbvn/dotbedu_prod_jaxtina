<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class J_GradebookViewEdit extends ViewEdit
{
    var $useModuleQuickCreateTemplate = true;
    var $useForSubpanel = true;

    public function display()
    {
        $thisClassId = $_REQUEST['j_class_j_gradebook_1j_class_ida'];
        if(empty($thisClassId)) $thisClassId =  $_REQUEST['j_class_id'];
        if(empty($thisClassId)) $thisClassId =  $this->bean->j_class_j_gradebook_1j_class_ida;
        if(empty($thisClassId)) die('Pls, Create Gradebook From A Class!');

        $thisClass = BeanFactory::getBean("J_Class", $thisClassId);

        $this->ss->assign('kind_of_course', $thisClass->kind_of_course);

        // Validate - Lap Nguyen
        $this->bean->team_id        = $thisClass->team_id;
        $this->bean->team_set_id    = $this->bean->team_id;

        $gradebook_settings = array('' => '-none-');
        //Get config Process by Team
        $q1 = "SELECT content FROM j_kindofcourse WHERE id = '{$thisClass->koc_id}'";
        $r1 = $GLOBALS['db']->fetchOne($q1);
        if(!empty($r1['content'])) {
            $KOCContents = json_decode(html_entity_decode($r1['content']), true);
            foreach ($KOCContents as $key => $value) {
                $gbsettinggroupID = $value['gbsettinggroup_id'];
                if ($value['levels'] == $thisClass->level && !empty($gbsettinggroupID)) {
                    $q2 = "SELECT id, name
                            FROM j_gradebookconfig
                            WHERE deleted = 0 
                                AND gbsettinggroup_id ='$gbsettinggroupID'";
                }
            }
        }
        $rs1 = $GLOBALS['db']->query($q2);
        while($row = $GLOBALS['db']->fetchByAssoc($rs1))
           $gradebook_settings[$row['id']]    = $row['name'];

        $this->ss->assign('gradebook_settings', $gradebook_settings);

        $this->ss->assign('class_name', $thisClass->name);
        $this->ss->assign('class_id', $thisClass->id);
        parent::display();
    }

}
