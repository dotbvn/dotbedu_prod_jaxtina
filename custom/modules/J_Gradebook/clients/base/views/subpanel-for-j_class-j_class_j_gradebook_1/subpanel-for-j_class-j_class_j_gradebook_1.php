<?php
// created: 2021-02-01 10:31:46
$viewdefs['J_Gradebook']['base']['view']['subpanel-for-j_class-j_class_j_gradebook_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'type',
          'label' => 'LBL_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'date_exam',
          'label' => 'LBL_DATE_EXAM',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_input',
          'label' => 'LBL_DATE_INPUT',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'date_confirm',
          'label' => 'LBL_DATE_CONFIRM',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'c_teachers_j_gradebook_1_name',
          'label' => 'LBL_C_TEACHERS_J_GRADEBOOK_1_FROM_C_TEACHERS_TITLE',
          'enabled' => true,
          'id' => 'C_TEACHERS_J_GRADEBOOK_1C_TEACHERS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'modified_by_name',
          'label' => 'LBL_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'id' => 'MODIFIED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        8 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);