<?php
// created: 2020-12-28 14:48:02
$viewdefs['J_Gradebook']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'status' => 
    array (
    ),
    'type' => 
    array (
    ),
    'date_input' => 
    array (
    ),
    'date_exam' =>
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'gradebook_config_name' => 
    array (
    ),
    'c_teachers_j_gradebook_1_name' => 
    array (
    ),
    'j_class_j_gradebook_1_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);
