<?php
class GradebookLogicHooks{
    function generate_name(&$bean, $event, $arg) {
        $thisClass          = BeanFactory::getBean("J_Class", $bean->j_class_j_gradebook_1j_class_ida);
        $bean->team_id      = $thisClass->team_id;
        $bean->team_set_id  = $bean->team_id;
        $gradebookConfig = BeanFactory::getBean('J_GradebookConfig', $bean->gradebook_config_id);

        if(!empty($gradebookConfig->content) && !empty($gradebookConfig->id)){
            $bean->grade_config         = $gradebookConfig->content;
            $bean->weight               = $gradebookConfig->weight;
            $bean->gradebook_config_id  = $gradebookConfig->id;
            $bean->band_score           = $gradebookConfig->band_score;
            $bean->type                 = $gradebookConfig->type;
        }

        if(empty($bean->name) || $bean->name == 'Auto-Generate' ){
            if(!empty($gradebookConfig->name))
                $bean->name = $gradebookConfig->name;
            else{
                $teamCode   = $GLOBALS['db']->getOne("SELECT short_name FROM teams WHERE id = '{$bean->team_id}' ");
                $bean->name = $teamCode .'-' . $thisClass->name .'-'.$bean->type;
            }
        }
        $bean->description = $gradebookConfig->description;

        //Approve bang diem - Gui thong bao
        if ($bean->status == 'Approved' && $bean->fetched_row['status'] != 'Approved'){
            $bean->notificated = 1;
            //Re-save Bean Gradebook Details - Kích hoạt các Process
            if($bean->load_relationship('j_gradebook_j_gradebookdetail')){
                $gbds = $bean->j_gradebook_j_gradebookdetail->getBeans();
                foreach ($gbds as $gbd_id => $gbd) $gbd->save();
            }
        }


        if($bean->notificated)
            $bean->datetime_notify_sent = $GLOBALS['timedate']->nowDb();

    }

    //Notification Apps
    function createNoti (&$bean, $event, $arg){
        if ($bean->notificated) {
            require_once("custom/clients/mobile/helper/NotificationHelper.php");
            global $timedate;
            $nowDate = $timedate->nowDbDate();
            $q1 = "SELECT DISTINCT
            IFNULL(l2.id, '') student_id
            FROM j_gradebookdetail
            INNER JOIN j_gradebook l1 ON j_gradebookdetail.gradebook_id = l1.id AND l1.deleted = 0
            INNER JOIN contacts l2 ON j_gradebookdetail.student_id = l2.id AND l2.deleted = 0
            INNER JOIN j_studentsituations l3 ON
            l2.id = l3.student_id AND l3.deleted = 0 AND l3.student_type = 'Contacts'
            WHERE (l1.id = '{$bean->id}')
            AND j_gradebookdetail.deleted = 0
            AND l3.ju_class_id = '{$bean->j_class_j_gradebook_1j_class_ida}'
            AND l3.type <> 'Delayed'
            AND l3.start_study <= '{$nowDate}'";
            $rows = $GLOBALS['db']->fetchArray($q1);
            foreach ($rows as $row) {
                $notify = new NotificationMobile();
                $notify->pushNotification('Bảng điểm lớp ' . $bean->j_class_j_gradebook_1_name, 'Bảng điểm ' . $bean->name . ' của lớp ' . $bean->j_class_j_gradebook_1_name . ' vừa được cập nhật.', 'J_Class', $bean->j_class_j_gradebook_1j_class_ida, $row['student_id']);
            }
        }
    }

    function checkBeforeDelete(&$bean, $event, $arguments){
        $GLOBALS['db']->query("UPDATE j_gradebookdetail SET deleted = 1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE gradebook_id='{$bean->id}' AND deleted=0");
    }

    function processRecording(&$bean, $event, $arguments){
        if(substr( $bean->type,0,1) === 'P') $bean->type = "<span style='white-space: nowrap;' class='textbg_bluelight'>".$GLOBALS['app_list_strings']['gradebook_type_options'][$bean->type]."</span>";
        else $bean->type = "<span style='white-space: nowrap;' class='textbg_orange'>{$bean->type}</span>";

        if ($_REQUEST['module'] == 'J_Class'){
            $bean->custom_button = '<div style="display: inline-flex; float:right;"><input style="margin-left: 5px;" title="Input Mark" onclick="parent.DOTB.App.router.redirect(\'#bwc/index.php?module=J_Gradebook&action=InputMark&record='.$bean->id.'&return_module=J_Class&return_action=DetailView&return_id='.$_REQUEST['record'].'\',\'_blank\')" class="button primary" type="submit" name="btn_InputMark" id="btn_InputMark" value="'.translate('LNK_INPUT_MARK','J_Gradebook').'">';
            $bean->custom_button .= '<input style="margin-left:5px;" title="Edit" onclick="parent.DOTB.App.router.redirect(\'#bwc/index.php?module=J_Gradebook&action=EditView&record='.$bean->id.'&return_module=J_Class&return_action=DetailView&return_id='.$_REQUEST['record'].'\',\'_blank\')" class="button" type="submit" name="btn_EditGradebook" id="btn_EditGradebook" value="'.translate('LBL_EDIT_BUTTON','J_Gradebook').'">';
            $bean->custom_button .= '<input style="margin-left:5px;" type="button" name="delete_gradebook_btn" gradebook_id="'.$bean->id.'" class="button btn-danger delete_gradebook_btn" title="Delete" value="'.translate('LBL_BTN_DELETE','J_Gradebook').'"></div>';
            $bean->check_box = '<input type="checkbox" class="gradebook_checkbox custom_checkbox" module_name="J_Gradebook" onclick="handleCheckBox($(this));" value="'.$bean->id.'">';
        }
        $bean->loadGradebookDetail($_SESSION["contact_id"]);
        $bean->mark_html = $this->getDetailForStudentPortal($bean,$_SESSION["contact_id"]);
    }
    function getDetailForStudentPortal($bean, $student_id){

        $sqlGBConfig = "SELECT l2.content
                        FROM j_gradebook l1
                        INNER JOIN j_gradebookconfig l2 ON l1.gradebook_config_id = l2.id AND l2.deleted = 0
                        WHERE l1.id = '$bean->id'";
        $resultGBConfig = json_decode($GLOBALS['db']->getOne($sqlGBConfig),1);

        $mark_html = "<div class='mark-container'>";
        foreach($bean->fullDetail as $gbDetail){
            foreach($gbDetail['column'] as $key => $value){
                if(!empty($value)){
                    if($key == 'Z'){
                        $overall = 'overall';
                    }
                    $mark_html .= "<div class='mark-row $overall'><span class='mark-label {$overall}'>{$resultGBConfig[$key]['label']}</span>: $value</div>";
                }
            }
        }
        $mark_html .= "</div>";
        return $mark_html;
    }
    function afterSaveHandleOtherGradebook(&$bean, $event, $arguments){
        require_once("custom/modules/J_Gradebook/GradebookFunctions.php");
        $sql_get_std_in_gb = "SELECT j_gradebook_contacts_1contacts_idb
        FROM j_gradebook_contacts_1_c
        WHERE deleted = 0 AND j_gradebook_contacts_1j_gradebook_ida = '{$bean->id}'";
        $student_id = $GLOBALS['db']->getOne($sql_get_std_in_gb);

        if((isset($arguments['isUpdate']) && $arguments['isUpdate'] == false) || empty($student_id)) {
            $list_std_id = '';
            $sql_get_std_in_class = "SELECT student_id student_id
            FROM j_classstudents
            WHERE deleted = 0 AND class_id = '{$bean->j_class_j_gradebook_1j_class_ida}' AND student_type = 'Contacts'";
            $result1 = $GLOBALS['db']->query($sql_get_std_in_class);
            while($row1 = $GLOBALS['db']->fetchByAssoc($result1)){
                $list_std_id .= $row1['student_id'].',';
            }
            if(!empty($list_std_id))
                addStudent($bean->id,$list_std_id);
        }
    }
    function afterDeleteHandleOtherGradebook(&$bean, $event, $arguments){
        require_once("modules/J_Gradebook/J_Gradebook.php");
        $class = $bean->j_class_j_gradebook_1j_class_ida;
        $bean->saveOtherGradebook($class);
    }
}
?>
