<html>
    <head>
        <title></title>
        {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
        {dotb_getscript file="custom/include/javascript/Numeric.js"}
        {dotb_getscript file="custom/modules/J_Gradebook/js/inputMark.js"}
        {dotb_getscript file="custom/include/javascript/math.min.js"}
        {dotb_getscript file="modules/Reports/javascript/Blob_new.js"}
        {dotb_getscript file="custom/include/javascript/DataTables/jquery.dataTables.min.js"}
        {dotb_getscript file="custom/include/javascript/DataTables/dataTables.fixedColumns.min.js"}

<!--        {dotb_getscript file="custom/include/javascript/DataTables/dataTables.fixedHeader.min.js"}  -->
        <link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/Select2/select2.css'}"/>
        <link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/modules/J_Gradebook/css/inputMark.css'}"/>

        <link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/DataTables/css/fixedColumns.bootstrap.min.css'}"/>
        <link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/DataTables/css/jquery.dataTables.min.css'}"/>
        <link href="custom/include/javascript/Bootstrap/bootstrap.min.css" rel="stylesheet">

        {literal}
<style type="text/css">
.table-border td, .table-border th{
    padding: 5px;
    vertical-align: middle;
    text-align: center;
    border: 1px solid #ddd !important;
}
.table-border {
    border-collapse:collapse;
}
.table-border thead td {
     color: #495057;
     font-size:14px;
     border: 1px solid #ddd !important;
}
.table-border tbody td{

    vertical-align: middle !important;
}
.moreinfo_tip {
    display: none;
}
.span-mark{
    display: none;
}
.dataTables_scrollBody{
    border-bottom: none !important;
}
.note {
border-radius: 3px;
border:red solid 2px;
background-color:#FFC;
padding:10px;
font-size:11px;
position:absolute;
z-index:10;
text-align: left;
display: inline;
}
.no-bottom-border {
 border-bottom: 0px solid #ddd !important;
}
.no-top-border {
 border-top: 0px solid #ddd !important;
}
</style>
{/literal}
    </head>
    <body>
        <br/>
        <br/>
        <form action="index.php" name="GradebookInputMark" id ="GradebookInputMark">
            <input name ='process_type' value="inputMark" type="hidden">
            <input name ='grade_config' value="{$FIELDS.grade_config}" type="hidden">
            <input name ='weight' value="{$FIELDS.weight}" type="hidden">
            <div id = "main_form">
                <div class="overide">
                    <div class="title"><p><b>{$MOD.LBL_GRADEBOOK_DETAIL}</b></p></div>
                    <div class = "content" style="height: 80px">
                        <table style="vertical-align: text-bottom" width="100%">
                            <tbody>
                                <tr>
                                    <td width="15%"><b>{$MOD.LBL_CLASS_NAME}</b> <span class="required">*</span></td>
                                    <td width="15%"><b>{$MOD.LBL_GRADEBOOK_NAME}</b> <span class="required">*</span> </td>
                                    <td width="15%"><b>{$MOD.LBL_TEACHER_NAME}</b></td>
                                    <td width="20%"><b>{$MOD.LBL_DESCRIPTION}</b><input name='grade_description' value="{$FIELDS.description}" type="hidden"></td>
                                    <td width="10%"></td>
                                    <td width="10%"></td>
                                </tr>
                                <tr>
                                    <td class="field" >
                                        <input type="text" style="display:none" name="j_class_j_gradebook_1_name" class="sqsEnabled yui-ac-input" tabindex="0" id="j_class_j_gradebook_1_name" size="" value="{$FIELDS.j_class_j_gradebook_1_name}" db-data="{$FIELDS.j_class_j_gradebook_1_name}">
                                        <input type="hidden" name="j_class_j_gradebook_1j_class_ida" id="j_class_j_gradebook_1j_class_ida" value="{$FIELDS.j_class_j_gradebook_1j_class_ida}" db-data="{$FIELDS.j_class_j_gradebook_1j_class_ida}">
                                        <span class="id-ff multiple">
                                            <button type="button" style="display:none" name="btn_j_class_j_gradebook_1_name" id="btn_j_class_j_gradebook_1_name" title="Select" class="button" value="Select"
                                                    onclick="open_popup('J_Class', 600, 400, '', true, false,{ldelim}'call_back_function':'set_return','form_name':'GradebookInputMark','field_to_name_array':{ldelim}'id':'j_class_j_gradebook_1j_class_ida','name':'j_class_j_gradebook_1_name'{rdelim}{rdelim},'single',true);" db-data="Select"><img src="themes/default/images/id-ff-select.png">
                                            </button>
                                        </span>
                                        <span><a id="class_link_url" style="cursor: pointer;" href="#bwc/index.php?module=J_Class&action=DetailView&record={$FIELDS.j_class_j_gradebook_1j_class_ida}">{$FIELDS.j_class_j_gradebook_1_name}</a></span>
                                    </td>
                                    <td class="field">
                                        <select style='height: 28px;display:none' class="select_config gradebook_id" id='gradebook_id' name='gradebook_id'>
                                            {$GRADEBOOK_OPTIONS}
                                        </select>
                                        <span><a id="gradebook_link_url" style="cursor: pointer;" href="#bwc/index.php?module=J_Gradebook&action=DetailView&record={$GRADEBOOK_ID}">{$GRADEBOOK_NAME}</a></span>
                                    </td>
                                    <td class="field" >
                                        <span id='teacher_link_url'>{$FIELDS.c_teachers_j_gradebook_1_name}</span>
                                    </td>
                                    <td width="20%" class="field" style="text-align: left;vertical-align: text-top;" >
                                        <span id='grade_description'>{$FIELDS.description}</span>
                                    </td>
                                    <td><input type="button" class="button primary bnt_find" value="{$MOD.LBL_FIND}">
                                    <input style="display:none;" type="button" class="button primary bnt_save" value="{$MOD.LBL_SAVE_BUTTON_TITLE}">
                                    <td>
                                        <ul class="clickMenu fancymenu" style="margin-left: 5px !important;">
                                            <li class="dotb_action_button"><a>{$MOD.LBL_ACTION}</a>
                                                <ul class="subnav" style="display: none;">
                                                    <li><a id="bnt_clear" class="bnt_clear" href="#" >{$MOD.LBL_CLEAR_BUTTON_LABEL}</a></li>
                                                    <li><a id="bnt_reloadconfig" class="bnt_reloadconfig" href="#" >{$MOD.LBL_LOADNEWCONFIG}</a></li>
                                                    <li><a id="bnt_edit_config" class="bnt_edit_config" href="#" >{$MOD.LBL_EDIT_CONFIG}</a></li>
                                                    <li><a id="exportResultExcel" name="exportResultExcel" href="#" >{$MOD.LNK_EXPORT_EXCEL}</a></li>
                                                </ul>
                                                <span class="ab subhover"></span>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div style="height: 25px;"></div>
                <div class = 'gradebook_detail' id="gradebook_mark">
                    {$GRADEBOOK_CONTENT}
                </div>
                <div class="config_button">
                <input type="button" class="button primary bnt_find" value="{$MOD.LBL_FIND}">
                <input style="display:none;" type="button" class="button primary bnt_save" value="{$MOD.LBL_SAVE_BUTTON_TITLE}">
                <input type="button" class="button bnt_clear" value="{$MOD.LBL_CLEAR_BUTTON_LABEL}">
                </div>
            </div>
        </form>
        <div id = 'comment_dialog' style="display:none;">
            <div class="dialog_header">
                <table >
                    <tbody>
                        <tr>
                            <td class="right"><b>{$MOD_S.LBL_NAME}:</b></td>
                            <td class="mid"></td>
                            <td><span id = 'dialog_student_name'  class="bold"></span></td>
                        </tr>
                        <tr>
                            <td  class="right"><b>{$MOD.LBL_CLASS_NAME}:</b></td>
                            <td class="mid"></td>
                            <td><span id = 'dialog_student_class' class="bold"></span></td>
                        </tr>
                    </tbody>
                </table>
                <hr>
            </div>
            <div class="dialog_content">
                <table >
                    <tbody>
                        {foreach from=$COMMENTLIST key=k item=COMMENT}
                        <tr style="display:none;">
                            <td class="right label_comment"><b>{$COMMENT.LABEL}:</b></td>
                            <td class="mid"></td>
                            <td>
                                <select id='{$COMMENT.ID}' class = '{$COMMENT.ID} select_comment'>
                                    {$COMMENT.OPTIONS}
                                </select>
                            </td>
                        </tr>
                        {/foreach}
                        <tr style="display:none;">
                            <td class="right label_comment"><b>{$MOD.LBL_COMMENT_OTHER}:</b></td>
                            <td class="mid"></td>
                            <td>
                                <textarea id='gradebook_other_comment' class = 'gradebook_other_comment select_comment'></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td  class="right"><b>{$MOD.LBL_TEACHER_COMMENT}:</b></td>
                            <td class="mid"></td>
                            <td>
                                <textarea cols="" rows="" name='dialog_text_comment' id = 'total_comment' class="text_comment"></textarea>
                                <input type="hidden" name = 'dialog_key_comment'>
                            </td>
                        </tr>
                        <tr>
                            <td  class="right"><b></b></td>
                            <td class="mid"></td>
                            <td>
                                <button type="button" class="button btn_add_vie" id='btn_add_vie'>Add [Vie]</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div id ='upload_dialog' style="display: none">
            <form action="index.php?module=J_Gradebook&action=handelAjaxsInputMark&dotb_body_only=true&type=ajaxGetMarkFromExcel"
                  method="post" class="submitFileMark" enctype="multipart/form-data">
                 <a href="javascript: void(0);" class="import_template_link">{$MOD.LBL_DOWNLOAD_FILE}</a>
                 <br><br>
                 Upload file: <span class="required">*</span><input type="file" name="fileToUpload" class="fileToUpload">
                 <br><br>
                <button onclick="uploadFileMark(this)" value="" class="button btn btn-primary submit_file_mark" type="button"><b>{$MOD.LBL_IMPORTF}</b></button>
            </form>
        </div>
    </body>
</html>
