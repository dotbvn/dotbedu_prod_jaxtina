<?php
function getGradebook($class_id  = '', $gradebook_id = '') {
    return array('html' => getGradebookSelectOptions($class_id, $gradebook_id));
}

function getGradebookDetail($gradebook_id  = '', $reloadconfig  = '') {
    $gradebook = BeanFactory::getBean('J_Gradebook',$gradebook_id);
    return array(
        'html'          => $gradebook->loadGradeContent($reloadconfig),
        'grade_config'  => $gradebook->config,
        'grade_description'  => nl2br($gradebook->description),
    );
}

function sendNoti($gradebook_id = '') {
    $gradebook = BeanFactory::getBean('J_Gradebook',$gradebook_id);
    $gradebook->notificated = 1;
    $gradebook->save();
    return $gradebook->notificated;
}

function getGradebookSelectOptions($class_id  = '', $gradebook_id = '') {
    $q1 = "SELECT DISTINCT
    IFNULL(j_gradebook.id, '') primaryid,
    IFNULL(j_gradebook.name, '') j_gradebook_name,
    IFNULL(j_gradebook.description, '') description,
    IFNULL(l2.id, '') l2_id,
    l2.full_teacher_name l2_full_teacher_name,
    IFNULL(j_gradebook.type, '') j_gradebook_type
    FROM j_gradebook
    INNER JOIN j_class_j_gradebook_1_c l1_1 ON j_gradebook.id = l1_1.j_class_j_gradebook_1j_gradebook_idb AND l1_1.deleted = 0
    INNER JOIN j_class l1 ON l1.id = l1_1.j_class_j_gradebook_1j_class_ida AND l1.deleted = 0
    LEFT JOIN c_teachers_j_gradebook_1_c l2_1 ON j_gradebook.id = l2_1.c_teachers_j_gradebook_1j_gradebook_idb AND l2_1.deleted = 0
    LEFT JOIN c_teachers l2 ON l2.id = l2_1.c_teachers_j_gradebook_1c_teachers_ida AND l2.deleted = 0
    WHERE (((l1.id = '$class_id'))) AND j_gradebook.deleted = 0 ORDER BY CASE WHEN (j_gradebook.type = '' OR j_gradebook.type IS NULL) THEN 0";

    $types = $GLOBALS['app_list_strings']['gradebook_type_options'];
    $count_k = 1;
    foreach($types as $_type => $val_){
        $q1 .= " WHEN j_gradebook.type = '$_type' THEN ".$count_k++;
    }
    $q1 .= " ELSE $count_k END ASC";

    $gradebooks = $GLOBALS['db']->fetchArray($q1);
    foreach($gradebooks as $index => $grade){
        $sel = ($grade['primaryid'] == $gradebook_id) ? "selected" : '';
        $select_html .= "<option value='{$grade['primaryid']}' label = '{$grade['j_gradebook_name']}' $sel
        teacher_id = '{$grade['l2_id']}'
        teacher_name = '{$grade['l2_full_teacher_name']}'
        gb_description = '{$grade['description']}'>{$grade['j_gradebook_name']}</option>";
    }
    return $select_html;//get_select_options_with_id($list, $gradebook_id);
}

function saveInputMark($_data  = '') {
    global $timedate, $current_user;
    $gradebook = new J_Gradebook();
    $gradebook->retrieve($_data['gradebook_id']);
    if(empty($gradebook->id)){
        return json_encode(array(
            "success" => "0",
            "errorLabel" => "LBL_GRADEBOOK_NOT_FOUND",
        ));
    }
    $gradebook->grade_config = $_data['grade_config'];
    if($_data['grade-reload']){//Nếu Reload Config thì mới update
        $gradebook->save();
    }
    $keys = json_decode(html_entity_decode($_data['key']),true);
    // ADD BY LAM ANH KHOA - Set teacher for gradebook
    if(empty($gradebook->c_teachers_j_gradebook_1c_teachers_ida)){ //Check if gradebook have a teacher
        $teacher_id = $GLOBALS['db']->getOne("SELECT id FROM c_teachers WHERE user_id='$current_user->id' AND deleted=0");
        if(!empty($teacher_id)){
            $gradebook->c_teachers_j_gradebook_1c_teachers_ida=$teacher_id;
            $gradebook->save();
        }
    }
    //danh dau xoa cac detail cu
    $dateModifield = $timedate->nowDb();
    $GLOBALS['db']->query("UPDATE j_gradebookdetail SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE gradebook_id = '{$gradebook->id}' AND deleted = 0");
    $GLOBALS['db']->query("UPDATE j_gradebook
        SET date_modified = '$dateModifield',
        modified_user_id = '{$current_user->id}'
        WHERE id = '{$gradebook->id}'");

    $countSaveErrors = 0; //Add by Lap Nguyen - Check log save gradebook detail

    $gradebook->_constructDefault();
    $config_array = $gradebook->config;
    for($i = 0; $i < count($_data['student_id']); $i++){
        $detail                 = new J_GradebookDetail();
        $detail->name           = $GLOBALS['db']->getOne("SELECT IFNULL(full_student_name, '') name FROM contacts WHERE id = '{$_data['student_id'][$i]}'");
        $detail->student_id     = $_data['student_id'][$i];
        $detail->gradebook_id   = $gradebook->id;
        $detail->team_id        = $gradebook->team_id;
        $detail->team_set_id    = $gradebook->team_set_id;

        //Set value mark
        $content = array();
        $is_total = 0; // kiểm tra bảng điểm có cột total không
        $f_mark = 0; // final mark
        foreach($keys as $key){
            $col = 'col_'.$key;
            if($_data[$key][$i] != '')
            $detail->$col = $_data[$key][$i];
            switch ($config_array[$key]['type']) {
                case 'band':
                    $content[$key]          = $_data[$key][$i];
                    //Save Result
                    if($config_array[$key]['options'] == 'level_band')
                        $detail->certificate_level = $_data[$key][$i];
                    else $detail->certificate_type = $_data[$key][$i];
                    break;
                case 'comment':
                    $comment_name = $config_array[$key]['name'];
                    $content[$comment_name.'_key'] = json_decode(html_entity_decode($_data['key_teacher_'.$comment_name][$i]),true);
                    $content[$comment_name.'_comment_label'] = $_data['value_teacher_'.$comment_name][$i];
                    $detail->$col = htmlspecialchars_decode($_data['value_teacher_'.$comment_name][$i]);
                    break;
                case 'total':
                    $_mark                  = unformat_number($_data[$key][$i]);
                    if($_mark !== ''){
                        $content[$key]          = $_mark;
                        $detail->final_result   = format_number($_mark,2,2);
                    }
                    $is_total = 1;
                    break;
                default:
                    $_mark                  = unformat_number($_data[$key][$i]);
                    if($_mark !== ''){
                        $content[$key]          = $_mark;
                        $f_mark                 = format_number($_mark,2,2);
                    }
                    break;
            }
        }

        if ($is_total == 0 && $_mark !== ''){
            $detail->final_result = $f_mark;
        }
        $detail->content = json_encode($content, JSON_UNESCAPED_UNICODE);
        $detail->j_class_id = $gradebook->j_class_j_gradebook_1j_class_ida;
        $detail->teacher_id = $gradebook->c_teachers_j_gradebook_1c_teachers_ida;
        $detail->date_input = $timedate->nowDate();

        try {
            $detail->save();
        }catch(Exception $e){
            $countSaveErrors++;
        }
    }

    //Add by Lap Nguyen  - Check log save gradebook detail
    if($countSaveErrors == 0){
        $gradebook->saveOtherGradebook();
        return json_encode(array(
            "success" => "1",
        ));
    }else{
        return json_encode(array(
            "success" => "0",
            "errorLabel" => "LBL_SAVE_INPUT_GRADEBOOK_UNSUCCESSFULL",
        ));
    }
}

function showConfig($gradebook_id  = '') {
    $gradebook = BeanFactory::getBean('J_Gradebook',$gradebook_id);
    $gradebook->_constructDefault(false);
    return json_encode(array(
        'html' =>  $gradebook->getConfigHTML($gradebook->config),
    ));
}

function showAddStudentList($gradebook_id  = '') {
    $gradebook = BeanFactory::getBean('J_Gradebook',$gradebook_id);
    $res = $gradebook->getStudentListInClass();
    return json_encode(array(
        'html' =>  $res['html'],
    ));
}

function checkDuplicateTest($gradebook_id = '', $class_id  = '', $gradebook_config_id  = '') {
    $sql = "SELECT count(j_gradebook.id)
    FROM j_gradebook
    INNER JOIN j_class_j_gradebook_1_c cg ON j_gradebook.id = cg.j_class_j_gradebook_1j_gradebook_idb AND cg.deleted = 0
    WHERE j_gradebook.deleted = 0
    AND cg.j_class_j_gradebook_1j_class_ida = '$class_id'
    AND j_gradebook.gradebook_config_id = '$gradebook_config_id'
    AND j_gradebook.id != '$gradebook_id'";
    $count = $GLOBALS['db']->getOne($sql);
    if($count > 0)
        return true;

    return false;
}

function deleteGradebook($gradebook_id = ''){
    $gradebook = BeanFactory::getBean('J_Gradebook',$gradebook_id);
//    if(!empty($gradebook->id)) $gradebook->mark_deleted($gradebook_id);
//    return true;

    $class = $gradebook->j_class_j_gradebook_1j_class_ida;
    $GLOBALS['db']->query("UPDATE j_gradebookdetail SET deleted = 1 WHERE gradebook_id = '{$gradebook_id}'");
    $GLOBALS['db']->query("UPDATE c_teachers_j_gradebook_1_c SET deleted = 1 WHERE c_teachers_j_gradebook_1j_gradebook_idb = '{$gradebook_id}'");
    $GLOBALS['db']->query("UPDATE j_class_j_gradebook_1_c SET deleted = 1 WHERE j_class_j_gradebook_1j_gradebook_idb = '{$gradebook_id}'");
    $GLOBALS['db']->query("UPDATE j_gradebook SET deleted = 1 WHERE id = '{$gradebook_id}'");
    $gradebook->saveOtherGradebook($class);
    return true;
}

function removeStudent($gradebook_id = '',$student_id = ''){
    $GLOBALS['db']->query("UPDATE j_gradebookdetail SET deleted = 1 WHERE gradebook_id = '{$gradebook_id}' AND student_id = '{$student_id}'");
    $GLOBALS['db']->query("UPDATE j_gradebook_contacts_1_c SET deleted = 1 WHERE j_gradebook_contacts_1j_gradebook_ida = '{$gradebook_id}' AND 	j_gradebook_contacts_1contacts_idb = '{$student_id}'");
    $gradebook = BeanFactory::getBean('J_Gradebook',$gradebook_id);

    $html = $gradebook->getGradebookDetailView(1);
    return json_encode(array(
        'html' =>  $html,
    ));
}

function addStudent($gradebook_id = '',$student_id_list = ''){
    global $timedate;
    $arr_student_id = explode(",", chop($student_id_list,","));
    $gradebook = BeanFactory::getBean('J_Gradebook',$gradebook_id);
    $gradebook->_constructDefault();
    $config_array = $gradebook->config;
    foreach ($arr_student_id as $student_id){
        $student = BeanFactory::getBean('Contacts',$student_id);
        $student->load_relationship('j_gradebook_contacts_1');
        $student->j_gradebook_contacts_1->add($gradebook_id);
        //    $student->save();

        $detail                 = new J_GradebookDetail();
        $detail->student_id     = $student_id;
        $detail->gradebook_id   = $gradebook_id;
        $detail->team_id        = $gradebook->team_id;
        $detail->team_set_id    = $gradebook->team_set_id;
        $detail->final_result   = 0.00;

        //Set value mark
        $content = array();
        foreach($config_array as $key => $value){
            $col = 'col_'.$key;
            $detail->$col = 0.00;
            switch ($value['type']) {
                case 'band':
                    $content[$key]          = '';
                    break;
                case 'comment':
                    $comment_name = $value['name'];
                    $content[$comment_name.'_key'] = '';
                    $content[$comment_name.'_comment_label'] = '';
                    $detail->$col = '';
                    break;
                default:
                    $content[$key]          = 0.00;
                    break;
            }
        }

        $detail->content = json_encode($content, JSON_UNESCAPED_UNICODE);
        $detail->j_class_id = $gradebook->j_class_j_gradebook_1j_class_ida;
        $detail->teacher_id = $gradebook->c_teachers_j_gradebook_1c_teachers_ida;
        $detail->date_input = $timedate->nowDate();
        $detail->save();
    }
    $gradebook->saveOtherGradebook();
    $html = $gradebook->getGradebookDetailView(1);
    return json_encode(array(
        'html' =>  $html,
    ));
}
?>
