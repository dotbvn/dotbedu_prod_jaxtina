<?php
$result = '';
switch ($_REQUEST['type']) {
    case 'ajaxGetSyllabusFromExcel':
        $result = ajaxGetSyllabusFromExcel();
        break;
    case 'ajaxLoadSyllabusByLP':
        $result = ajaxLoadSyllabusByLP($_POST['lessonplan_id']);
        break;
    case 'ajaxSyncSyllabus':
        $result = ajaxSyncSyllabus();
        break;
}
echo $result;
die;

function ajaxGetSyllabusFromExcel() {
    require_once("custom/include/PHPExcel/Classes/PHPExcel.php");
    $info = pathinfo($_FILES['file']['name']);
    $ext = $info['extension']; // get the extension of the file

    if ($ext != "xlsx" && $ext != "xls") {
        return json_encode(
            array(
                "success" => "0",
                "errorLabel" => translate('LBL_PLEASE_UPLOAD_EXCEL_FILE', 'Contacts'),
            )
        );
    }
    $fdir = 'syllabus';
    if (!file_exists("upload/$fdir")) mkdir("upload/$fdir", 0777, true);
    $newname = "syllabus_list".create_guid_section(4)."." . $ext;
    $filename = "upload/$fdir/" . $newname;
    unlink($filename);
    move_uploaded_file($_FILES['file']['tmp_name'], $filename);

    $inputFileType = PHPExcel_IOFactory::identify($filename);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load("$filename");
    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();

    //Xác định vị trí cột
    $coll = [];
    $row_num = 0;
    for ($i = 1; $i <= 5; ++$i) {
        for ($j = 0; $j <= 15; ++$j) {
            $value = preg_replace('/[^a-z0-9\-]/', '', mb_strtolower($objWorksheet->getCellByColumnAndRow($j, $i)->getValue()));
            if($value == 'lessonno') $row_num =  $i+1;    //Set Row Number
            $coll['lesson']     = (($value == 'lessonno') ? $j : $coll['lesson']);
            $coll['type']       = (($value == 'type') ? $j : $coll['type']);
            $coll['learning_type'] = (($value == 'learningtype') ? $j : $coll['learning_type']);
            $coll['topic']      = (($value == 'themetopic') ? $j : $coll['topic']);
            $coll['syllabus']   = (($value == 'syllabus') ? $j : $coll['syllabus']);
            $coll['note']       = (($value == 'noteforteacher') ? $j : $coll['note']);
            $coll['homework']   = (($value == 'homework') ? $j : $coll['homework']);
        }
    }

    $countInvalid = 0;
    //Check valid collum
    if(!isset($coll['lesson'])) $countInvalid++;
    if($countInvalid > 0)
        return json_encode(array(
            "success" => 0,
            "errorLabel" => translate('LBL_PLEASE_UPLOAD_RIGHT_TEMPLATE', 'Meetings'),
            )
        );


    $syllabusArray = array();
    for ($row = $row_num; $row <= $highestRow; ++$row) {
        $arrSyl             = array();
        $arrSyl['lesson']   = $objWorksheet->getCellByColumnAndRow($coll['lesson'], $row)->getValue();
        $arrSyl['type']     = $objWorksheet->getCellByColumnAndRow($coll['type'], $row)->getValue();
        $arrSyl['learning_type'] = $objWorksheet->getCellByColumnAndRow($coll['learning_type'], $row)->getValue();
        $arrSyl['topic']    = $objWorksheet->getCellByColumnAndRow($coll['topic'], $row)->getValue();
        $arrSyl['syllabus'] = $objWorksheet->getCellByColumnAndRow($coll['syllabus'], $row)->getValue();
        $arrSyl['note']     = $objWorksheet->getCellByColumnAndRow($coll['note'], $row)->getValue();
        $arrSyl['homework'] = $objWorksheet->getCellByColumnAndRow($coll['homework'], $row)->getValue();
        $syllabusArray[] = $arrSyl;
    }
    unlink($filename);
    return json_encode(
        array(
            "success" => "1",
            "data" => $syllabusArray
        )
    );
}
function ajaxLoadSyllabusByLP($lessonplan_id = ''){
    $syllabusArray = array();
    if (!empty($lessonplan_id)) {
        $rows = getSyllabus($lessonplan_id);
        foreach ($rows as $row) {
            $arrSyl = array();
            $arrSyl['lesson'] = $row['lesson'];
            $arrSyl['type'] = $row['type'];
            $arrSyl['learning_type'] = $row['learning_type'];
            $arrSyl['topic'] = $row['theme'];
            $arrSyl['syllabus'] = $row['content'];
            $arrSyl['note'] = $row['objective'];
            $arrSyl['homework'] = $row['homework'];
            $syllabusArray[] = $arrSyl;
        }
    }
    return json_encode(array(
        'success'  => true,
        'data' => $syllabusArray
    ));
}

function ajaxSyncSyllabus(){
    require_once("custom/include/_helper/junior_revenue_utils.php");
    $lessonPlan = BeanFactory::getBean("J_LessonPlan", $_POST['syllabus_id']);
    if(empty($lessonPlan->id)) return json_encode(array("success" => 0));
    else{
        //Load class list
        $qClass = "SELECT IFNULL(j_class.id, '') class_id FROM j_class WHERE lessonplan_id = '{$lessonPlan->id}' AND deleted = 0";
        $resClass = $GLOBALS['db']->fetchArray($qClass);
        foreach ($resClass as $rClass){
            $res = updateClassSyllabus($rClass['class_id'], $lessonPlan->id);
            if($res) $updatedClass++;
        }
        return json_encode(array(
            "success" => 1,
            "message"   => ' <b>'.$updatedClass.' '. translate('LBL_CLASS_UPDATED', 'J_LessonPlan').'</b>',
        ));

    }
}