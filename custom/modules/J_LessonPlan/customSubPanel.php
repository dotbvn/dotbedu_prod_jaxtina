<?php

function getKindOfCourse($params) {
    $args = func_get_args();
    $lessonPlanId = $args[0]['lessonplan_id'];

    $q1 = "SELECT DISTINCT IFNULL(j_kindofcourse.id, '') primary_id, j_kindofcourse.content
    FROM j_kindofcourse WHERE j_kindofcourse.deleted = 0";

    $rows1 = $GLOBALS['db']->fetchArray($q1);
    $koc_list = array();
    foreach ($rows1 as $r1) {
        $koc_content = json_decode(html_entity_decode($r1['content']));
        $koc_lps = array_column($koc_content, 'lessonplan_id');
        if (in_array($lessonPlanId, $koc_lps)) {
            $koc_list[] = $r1['primary_id'];
        }
    }
    $ids = implode("', '", $koc_list);
    return "SELECT IFNULL(j_kindofcourse.id, '') id, name, short_course_name, status, year, kind_of_course
    FROM j_kindofcourse WHERE j_kindofcourse.id IN ('$ids') AND j_kindofcourse.deleted = 0";
}