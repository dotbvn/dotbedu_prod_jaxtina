<?php

class J_LessonPlanViewEdit extends ViewEdit
{
    public function preDisplay()
    {
        parent::preDisplay();
    }

    public function display()
    {
        $lessonplan_id = $this->bean->id;
        if (isset($_POST['isDuplicate']) && $_POST['isDuplicate'] == 'true') {
            $lessonplan = BeanFactory::getBean('J_LessonPlan', $lessonplan_id);
            $this->bean->level = $lessonplan->level;
            $this->bean->name = $lessonplan->name;
            $this->bean->description = $lessonplan->description;
            $this->bean->assigned_user_id = $lessonplan->assigned_user_id;
            $this->bean->assigned_user_name = $lessonplan->assigned_user_name;
            $this->bean->team_id = $lessonplan->team_id;
            $this->bean->team_name = $lessonplan->team_name;
            $this->bean->team_set_id = $lessonplan->team_set_id;
        }

        $syllItems = getSyllabusRow(false);
        $rows = getSyllabus($lessonplan_id);

        if (count($rows) == 0)
            $syllItems .= getSyllabusRow(true, '1');
        else {
            $level = $lessonplan->level;
            foreach ($rows as $row) {
                if (isset($_POST['isDuplicate']) && $_POST['isDuplicate'] == 'true')
                    $row['primary_id'] = '';
                $syll[$level] = $row;
                $syllItems .= getSyllabusRow(true, $row['lesson'], $row['type'], $row['learning_type'], $row['theme'], $row['homework'], $row['objective'], $row['content'], $row['primary_id'], htmlspecialchars(json_encode($syll), ENT_QUOTES, 'UTF-8'));
            }
        }
        $MOD = $GLOBALS['mod_strings'];
        $MOD_S = return_module_language($GLOBALS['current_language'], 'J_Syllabus');
        $ssLevelItem = new Dotb_Smarty();
        $ssLevelItem->assign("TBODYSYL", $syllItems);
        $ssLevelItem->assign("MOD", $MOD);
        $ssLevelItem->assign("MOD_S", $MOD_S);
        $ssLevelItem->assign('ACTION', 'edit');

        //add Dropdown

        $html = '<div class="dropdown">';
        $html .= '<a class="dropdown-toggle" type="button" id="mu_sys_type" data-toggle="dropdown">' . $MOD_S['LBL_LESSON_TYPE'] . '<span class="caret"></span></a>';
        $html .= '<ul class="dropdown-menu" aria-labelledby="sys_type">';
        foreach ($GLOBALS['app_list_strings']['syllabus_type_list'] as $kop => $vop)
            $html .= '<li><a onclick="handleSaveAll(\'sys_type\',$(this).attr(\'value\'))" value="' . $kop . '">' . $MOD['LBL_MARK_ALL'] . " <b>$vop</b> " . '</a></li>';
        $html .= '</ul></div>';
        $ssLevelItem->assign("sys_type", $html);

        $html = '<div class="dropdown">';
        $html .= '<a class="dropdown-toggle" type="button" id="mu_learning_type" data-toggle="dropdown">' . $MOD_S['LBL_LEARNING_TYPE'] . '<span class="caret"></span></a>';
        $html .= '<ul class="dropdown-menu" aria-labelledby="learning_type">';
        foreach ($GLOBALS['app_list_strings']['learning_type_list'] as $kop => $vop)
            $html .= '<li><a onclick="handleSaveAll(\'learning_type\',$(this).attr(\'value\'))" value="' . $kop . '">' . $MOD['LBL_MARK_ALL'] . " <b>$vop</b> " . '</a></li>';
        $html .= '</ul></div>';
        $ssLevelItem->assign("learning_type", $html);

        $html = '<div class="dropdown">';
        $html .= '<a class="dropdown-toggle" type="button" id="mu_sys_theme" data-toggle="dropdown">' . $MOD_S['LBL_NAME'] . '<span class="caret"></span></a>';
        $html .= '<ul class="dropdown-menu" aria-labelledby="sys_theme">';
        $html .= '<li><a onclick="genenrateMes(\'sys_theme\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
        $html .= '</ul></div>';
        $ssLevelItem->assign("sys_theme", $html);

        $html = '<div class="dropdown">';
        $html .= '<a class="dropdown-toggle" type="button" id="mu_sys_content" data-toggle="dropdown">' . $MOD_S['LBL_DESCRIPTION'] . '<span class="caret"></span></a>';
        $html .= '<ul class="dropdown-menu" aria-labelledby="sys_content">';
        $html .= '<li><a onclick="genenrateMes(\'sys_content\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
        $html .= '</ul></div>';
        $ssLevelItem->assign("sys_content", $html);

        $html = '<div class="dropdown">';
        $html .= '<a class="dropdown-toggle" type="button" id="mu_sys_objective" data-toggle="dropdown">' . $MOD_S['LBL_NOTE_FOR_TEACHER'] . '<span class="caret"></span></a>';
        $html .= '<ul class="dropdown-menu" aria-labelledby="sys_objective">';
        $html .= '<li><a onclick="genenrateMes(\'sys_objective\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
        $html .= '</ul></div>';
        $ssLevelItem->assign("sys_objective", $html);

        $html = '<div class="dropdown">';
        $html .= '<a class="dropdown-toggle" type="button" id="mu_sys_homework" data-toggle="dropdown">' . $MOD_S['LBL_HOMEWORK'] . '<span class="caret"></span></a>';
        $html .= '<ul class="dropdown-menu" aria-labelledby="sys_homework">';
        $html .= '<li><a onclick="genenrateMes(\'sys_homework\')">' . $MOD['LBL_GENERATE_MESSAGE'] . '</a></li>';
        $html .= '</ul></div>';
        $ssLevelItem->assign("sys_homework", $html);

        $levelItems = $ssLevelItem->fetch('custom/modules/J_LessonPlan/tpls/syllabus_config.tpl');

        $this->ss->assign('SYLLABUS_CONFIG', $levelItems);

        //End
        parent::display();
    }
}

function getSyllabusRow($show, $lesson = '', $type = '', $learning_type = '', $theme = '', $homeWorks = '', $objective = '', $content = '', $primary_id = '', $json = '')
{
    $display = (!$show) ? 'style="display:none;"' :'';
    $tpl_addrow = "<tr class='rowSyllabus' $display>";
    $tpl_addrow .= '<td scope="col" align="center">';
    $tpl_addrow .= "<input name='json_syl[]' value='$json' class='json_syl' type='hidden'/>";
    $tpl_addrow .= '<input name="sys_lesson[]" value="' . $lesson . '" class="sys_lesson" type="text" style="width: 70px;text-align: center;" db-data="100"></td>';
    $tpl_addrow .= '<td align="center">
    <select class="sys_type" name="sys_type" rows="1" cols="25">
    '.get_select_options_with_id($GLOBALS['app_list_strings']['syllabus_type_list'],$type).'
    </select></td>';

    $tpl_addrow .= '<td align="center">
    <select class="learning_type" name="learning_type" rows="1" cols="25">
    '.get_select_options_with_id($GLOBALS['app_list_strings']['learning_type_list'],$learning_type).'
    </select></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_theme" name="sys_theme" rows="1" cols="25">' . $theme . '</textarea></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_content" name="sys_content" rows="1" cols="25">' . $content . '</textarea></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_objective" name="sys_objective" rows="1" cols="25">' . $objective . '</textarea></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_homework" name="sys_homework" rows="1" cols="25">' . $homeWorks . '</textarea></td>';
    $tpl_addrow .= '<td style="text-align: center;"><input name="sys_primary_id" class="sys_primary_id" type="hidden" value="' . $primary_id. '">
    <button type="button" onclick="removeSyllabus(this)"  class="btnRemoveSyl">
    <img src="themes/default/images/id-ff-remove-nobg.png" alt="Remove"></button></td>';
    $tpl_addrow .= '</tr>';
    return $tpl_addrow;
}