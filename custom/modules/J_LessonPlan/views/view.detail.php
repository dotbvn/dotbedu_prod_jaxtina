<?php

class J_LessonPlanViewDetail extends ViewDetail
{
    public function preDisplay()
    {
        parent::preDisplay();
    }

    public function display()
    {
        $syllItems = '';
        $rows = getSyllabus($this->bean->id);

        if(count($rows) == 0)
            $syllItems .= getSyllabusRow();
        else {
            foreach ($rows as $row) {
                $syllItems .= getSyllabusRow($row);
            }
        }
        $MOD = $GLOBALS['mod_strings'];
        $ssLevelItem = new Dotb_Smarty();
        $ssLevelItem->assign("TBODYSYL", $syllItems);
        $ssLevelItem->assign("MOD", $MOD);
        $ssLevelItem->assign("MOD_S", return_module_language($GLOBALS['current_language'], 'J_Syllabus'));
        $ssLevelItem->assign("ACTION", 'detail');
        $levelItems = $ssLevelItem->fetch('custom/modules/J_LessonPlan/tpls/syllabus_config.tpl');
        $this->ss->assign('SYLLABUS_CONFIG', $levelItems);

        //highlight script
        if(!empty($_REQUEST['hl'])){
            $hl_script = '<script>
            $(document).ready(function () {
            var hl_row = $("tr[syllabus_id='.$_REQUEST['hl'].']");
            hl_row.find("td").animate({backgroundColor: "yellow"}, "slow");
            $("html,body").animate({scrollTop: hl_row.offset().top-(window.innerHeight - hl_row.height())/2}, "slow");
            });
            </script>';
        }
        $this->ss->assign('hl_script', $hl_script);


        parent::display();
    }
}

function getSyllabusRow($row = array())
{
    if (empty($row['lesson'])) {
        $tpl_addrow = "<tr class='rowSyllabus'>";
        $tpl_addrow .= '<td colspan="7" scope="col" style="text-align: center;"><i>'.translate('LBL_NO_SYLLABUS_CONFIG','J_LessonPlan').'</i></td>';
        $tpl_addrow .= '</tr>';
    } else {
        $tpl_addrow = "<tr class='rowSyllabus' syllabus_id='".$row['primary_id']."'>";
        $tpl_addrow .= '<td scope="col" style="text-align: center;">'.$row['lesson'].'</td>';
        $tpl_addrow .= '<td style="text-align: center;">'.$GLOBALS['app_list_strings']['syllabus_type_list'][$row['type']].'</td>';
        $tpl_addrow .= '<td style="text-align: center;">'.$GLOBALS['app_list_strings']['learning_type_list'][$row['learning_type']].'</td>';
        $tpl_addrow .= '<td>'.$row['theme'].'</td>';
        $tpl_addrow .= '<td>'.$row['content'].'</td>';
        $tpl_addrow .= '<td>'.$row['objective'].'</td>';
        $tpl_addrow .= '<td>'.$row['homework'].'</td>';
        $tpl_addrow .= '</tr>';
    }
    return $tpl_addrow;
}