<?php
$popupMeta = array (
  'moduleMain' => 'J_LessonPlan',
  'varName' => 'J_LessonPlan',
  'orderBy' => 'j_lessonplan.name',
  'whereClauses' =>
  array (
    'name' => 'j_lessonplan.name',
    'date_modified' => 'j_lessonplan.date_modified',
    'date_entered' => 'j_lessonplan.date_entered',
  ),
  'searchInputs' =>
  array (
    1 => 'name',
    4 => 'date_modified',
    5 => 'date_entered',
  ),
  'create' =>
  array (
    'formBase' => 'LessonPlanFormBase.php',
    'formBaseClass' => 'LessonPlanFormBase',
    'getFormBodyParams' =>
    array (
      0 => '',
      1 => '',
      2 => 'J_LessonPlanSave',
    ),
  'createButton' => 'LNK_NEW_RECORD',
  ),
  'searchdefs' =>
  array (
    'name' =>
    array (
      'name' => 'name',
      'width' => '10',
    ),
    'date_modified' =>
    array (
      'type' => 'datetime',
      'studio' =>
      array (
        'portaleditview' => false,
      ),
      'readonly' => true,
      'label' => 'LBL_DATE_MODIFIED',
      'width' => '10',
      'name' => 'date_modified',
    ),
    'date_entered' =>
    array (
      'type' => 'datetime',
      'studio' =>
      array (
        'portaleditview' => false,
      ),
      'readonly' => true,
      'label' => 'LBL_DATE_ENTERED',
      'width' => '10',
      'name' => 'date_entered',
    ),
  ),
  'listviewdefs' =>
  array (
    'NAME' =>
    array (
      'width' => 10,
      'label' => 'LBL_NAME',
      'default' => true,
      'link' => true,
      'name' => 'name',
    ),
    'SHORT_SYLLABUS' =>
    array (
      'name' => 'short_syllabus',
      'label' => 'LBL_SHORT_SYLLABUS',
      'type' => 'html',
      'width' => 20,
      'default' => true,
    ),
    'DESCRIPTION' =>
    array (
      'type' => 'text',
      'label' => 'LBL_DESCRIPTION',
      'sortable' => false,
      'width' => 10,
      'default' => true,
      'name' => 'description',
    ),
    'DATE_MODIFIED' =>
    array (
      'type' => 'datetime',
      'studio' =>
      array (
        'portaleditview' => false,
      ),
      'readonly' => true,
      'label' => 'LBL_DATE_MODIFIED',
      'width' => 10,
      'default' => true,
      'name' => 'date_modified',
    ),
  ),
);
// Custom filter By Lap nguyen - Giữ lại filter
if(!empty($_REQUEST['current_lp_id']))
    $popupMeta['customInput']['current_lp_id'] = $_REQUEST['current_lp_id'];
if(!empty($_REQUEST['target_module']))
    $popupMeta['customInput']['target_module'] = $_REQUEST['target_module'];
if(!empty($_REQUEST['target_module']))
    $popupMeta['customInput']['target_id'] = $_REQUEST['target_id'];