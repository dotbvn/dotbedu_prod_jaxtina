<?php
$module_name = 'J_LessonPlan';
$viewdefs[$module_name]['QuickCreate'] = array(
  'templateMeta' =>
  array(
    'maxColumns' => '2',
    'javascript' => '{dotb_getscript file="custom/modules/J_LessonPlan/js/edit.js"}
    {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
    <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>',
    'widths' =>
    array(
      array('label' => '10', 'field' => '30'),
      array('label' => '10', 'field' => '30')
    ),
  ),
 'panels' => array (
    'default' =>
    array (
      0 =>
      array (
        0 => 'name',
        1 =>
        array (
          'name' => 'is_copy_from_current_lp',
          'hideLabel' => true,
          'customCode' => '<input type="hidden" id="is_copy_from_current_lp" name="is_copy_from_current_lp" value="0">
          <input type="checkbox" id="copy_from_lp_select" name="copy_from_lp_select">
          <label for="copy_from_lp_select">{$MOD.LBL_COPY_FROM_CURRENT_LP}</label>'
        ),
      ),
      1 =>
      array (
        0 => 'description',
        1 => '',
      ),
    ),
  ),
);