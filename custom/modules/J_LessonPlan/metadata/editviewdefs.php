<?php
$module_name = 'J_LessonPlan';
$viewdefs[$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'javascript' => '{dotb_getscript file="custom/modules/J_LessonPlan/js/edit.js"}
      {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
      <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>',
      'widths' =>
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 'assigned_user_name',
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'description',
            'displayParams' =>
            array (
              'rows' => 4,
              'cols' => 60,
            ),
          ),
          1 =>
          array (
            'name' => 'team_name',
            'displayParams' =>
            array (
              'display' => true,
            ),
          ),
        ),
      ),
      'lbl_editview_panel1' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'syllabus_config',
            'hideLabel' => true,
            'customCode' => '{$SYLLABUS_CONFIG}',
          ),
        ),
      ),
    ),
  ),
);