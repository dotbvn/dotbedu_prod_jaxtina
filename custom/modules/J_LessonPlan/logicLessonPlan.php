<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicLessonPlan
{
    function handleBeforeSave($bean, $event, $arguments) {
        // Handle duplicate name
        if ($bean->name !== $bean->fetched_row['name']) {
            $bean->level = (is_null($bean->level)) ? '' : $bean->level;
            $base_name = $bean->name;
            $existing_names = $GLOBALS['db']->fetchArray("SELECT IFNULL(name, '') lessonplan_name FROM j_lessonplan WHERE (name = '{$bean->name}' OR name LIKE '{$bean->name} (%)') AND id != '{$bean->id}' AND deleted = 0 ORDER BY name");
            $bean->name = $this->generateUniqueName($base_name, array_column($existing_names, 'lessonplan_name'));
        }

        if (!empty($_POST['target_module']) && !empty($_POST['target_id'])) {
            $target_bean = BeanFactory::getBean($_POST['target_module'], $_POST['target_id']);
            $bean->team_id = $target_bean->team_id;
            $bean->team_set_id = $target_bean->team_set_id;
            $bean->assigned_user_id = $target_bean->assigned_user_id;
        }
    }

    function handleAfterSave($bean, $event, $arguments) {
        // Quick create Lesson Plan
        if (!empty($_POST['is_copy_from_current_lp'])) {
            $curr_lp_id = $_POST['current_lp_id'];
            $rows = getSyllabus($curr_lp_id);
            foreach ($rows as $row) {
                $syllabus_bean = BeanFactory::newBean('J_Syllabus');
                $syllabus_bean->lessonplan_id = $bean->id;
                $syllabus_bean->lesson = $row['lesson'];
                $syllabus_bean->lesson_type = $row['type'];
                $syllabus_bean->learning_type = $row['learning_type'];
                $syllabus_bean->description = $row['content'];
                $syllabus_bean->name = $row['theme'];
                $syllabus_bean->homework = $row['homework'];
                $syllabus_bean->note_for_teacher = $row['objective'];
                $syllabus_bean->save();
            }
        }

        if (!empty($_POST['json_syl'])) {
            require_once('custom/modules/J_LessonPlan/LessonPlanHelper.php');
            $update_flag = saveSyllabusConfig($_POST['json_syl'], $bean->id);  //- Cần tối ưu performance hơn

            // Update syllabus for all class using this lesson plan  - Cần tối ưu performance hơn
            if ($update_flag) {
                require_once("custom/include/_helper/junior_revenue_utils.php");
                $qClass = "SELECT IFNULL(j_class.id, '') class_id FROM j_class WHERE lessonplan_id = '{$bean->id}' AND deleted = 0";
                $resClass = $GLOBALS['db']->fetchArray($qClass);
                foreach ($resClass as $rClass) {
                    updateClassSyllabus($rClass['class_id'], $bean->id);
                }
            }
        }
    }

    function processRecord($bean, $event, $arguments) {
        $max_display_rows = 1;
        $html = '<div style="padding-top: 6px;">';
        $rows = getSyllabus($bean->id);
        foreach ($rows as $key => $row) {
            if ($key > $max_display_rows)
                break;
            $html .= '<p style="margin-bottom: 6px;" class="ellipsis_inline">' . $row['lesson'] . ". " . $row['theme'];
            $html .= empty($row['type']) ? "" : " <i>(" . $row['type'] .")</i>";
            $html .= '</p>';
        }
        $html .= (count($rows) > 2) ? '<p style="margin-bottom: 0;">...</p></div>' : '</div>';
        $bean->short_syllabus = $html;
    }

    function handleBeforeDelete($bean, $event, $arguments) {
        $GLOBALS['db']->query("UPDATE j_syllabus SET deleted = 1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE lessonplan_id = '{$bean->id}' AND deleted = 0");
    }

    /*
     * Handle generate unique name by format name, name (1), name (2),....
     * */
    private function generateUniqueName($base_name, $existing_names) {
        if (!in_array($base_name, $existing_names)) {
            return $base_name;
        }
        $counter = 1;
        while (in_array($base_name . " ($counter)", $existing_names)) {
            $counter++;
        }
        return $base_name . " ($counter)";
    }
}

?>
