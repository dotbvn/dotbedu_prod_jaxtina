{dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.js"}
<link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/include/javascript/Bootstrap/bootstrap.min.css'}">
{literal}
    <style>
        .ui-sortable tr {
            cursor:move;
        }
        #tblSyllabusContent thead th {
            background-color: #eee;
            border: solid 1px #555;
            padding: 5px 5px 5px 7px !important;
        }
        #tblSyllabusContent td {
            border: solid 1px #555;
            padding: 10px;
            background-color: #ffffff;
        }
        #tblSyllabusContent.edit thead th {
            background-color: transparent;
            border: none;
            padding: 5px 5px 5px 7px !important;
        }
        #tblSyllabusContent.edit td {
            border: solid 2px transparent;
            padding: 10px;
            background-color: aliceblue;
        }
        #tblSyllabusContent.edit tbody tr {
            background-color: aliceblue;
        }
        #tblSyllabusContent.edit tr:first-child td:first-child {
            border-left-style: solid;
            border-top-left-radius: 10px;
        }
        #tblSyllabusContent.edit tr:last-child td:first-child {
            border-left-style: solid;
            border-bottom-left-radius: 10px;
        }
        #tblSyllabusContent.edit tr:first-child td:last-child {
            border-right-style: solid;
            border-top-right-radius: 10px;
        }
        #tblSyllabusContent.edit tr:last-child td:last-child {
            border-right-style: solid;
            border-bottom-right-radius: 10px;
        }
        ul.dropdown-menu li{list-style-type: none;}
        a.dropdown-toggle{
            text-decoration: none !important;
            transform: scale(1);
            font-size: 13px!important;
            cursor: pointer;
            color: #0048ff;
        }
    </style>
{/literal}
<table style="border-collapse: collapse;background-color: white;margin-bottom:10px" class="tblSyllabusContent {if $ACTION eq "edit"}edit{/if}" width="100%" id="tblSyllabusContent">
    <thead>
    {if $ACTION eq "edit"}
    <tr>
        <td colspan="5" style="background: white">
            <div style="padding: 1rem 4rem; float: right; display: flex; flex-direction: column; gap: .8rem;">
                <form method="post" class="submitFileFormSyllabus" enctype="multipart/form-data">
                    <span>Upload file:</span>
                    <input type="file" name="fileToUpload" class="fileToUpload">
                    <div>
                        <button class="button btn-primary submit_file_syllabus" type="submit"><b>Import</b></button>
                        <a href="javascript: void(0);" style="text-align: center; margin-left: 10px"
                           onclick="window.location.href='custom/include/TemplateExcel/Import/Template_Import_Syllabus.xlsx'">
                            >> Download Template Syllabus << </a>
                    </div>
                </form>
            </div>
        </td>
        <td colspan="3" style="background: white">
            <div style="padding:10px 0">
                <span style="padding-left: 10px;">| {$MOD.LBL_COPY_FROM_ANOTHER_LP}:</span>
                <input style="margin-left: 30px;" type="button" class="button" id="prv_openpopup_lesson_plan" value="{$MOD.LBL_SELECT_LP}" data-focus="{$ID_TARGET}">
                <input type="hidden" name="prv_selected_lesson_plan" id="prv_selected_lesson_plan" value="">
            </div>
        </td>
    </tr>
    {/if}
    <tr style="background-color: white">
        {if $ACTION eq "edit"}
        <th width="4%" style="text-align: center;" nowrap="">{$MOD_S.LBL_LESSON} <span class="required"> *</span></th>
        <th width="8%" style="text-align: center;">{$sys_type}</th>
        <th width="8%" style="text-align: center;">{$learning_type}</th>
        <th width="10%" style="text-align: center;">{$sys_theme}</th>
        <th width="20%" style="text-align: center;">{$sys_content}</th>
        <th width="20%" style="text-align: center;">{$sys_objective}</th>
        <th width="20%" style="text-align: center;">{$sys_homework}</th>
        <th style="text-align: center;">
            <button class="button" onclick="addSyllabus(this)" type="button" id="btnAddSyl"><b>+</b></button>
        </th>
        {else}
        <th width="4%" style="text-align: center;" nowrap="">{$MOD_S.LBL_LESSON}</th>
        <th width="8%" style="text-align: center;">{$MOD_S.LBL_LESSON_TYPE}</th>
        <th width="8%" style="text-align: center;">{$MOD_S.LBL_LEARNING_TYPE}</th>
        <th width="10%" style="text-align: center;">{$MOD_S.LBL_NAME}</th>
        <th width="20%" style="text-align: center;">{$MOD_S.LBL_DESCRIPTION}</th>
        <th width="20%" style="text-align: center;">{$MOD_S.LBL_NOTE_FOR_TEACHER}</th>
        <th width="20%" style="text-align: center;">{$MOD_S.LBL_HOMEWORK}</th>
        {/if}
    </tr>
    </thead>
    <tbody class="tbodySyllabus" id="tbodySyllabus">
    {$TBODYSYL}
    </tbody>
</table>