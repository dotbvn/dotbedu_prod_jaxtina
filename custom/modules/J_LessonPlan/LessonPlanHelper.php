<?php
function saveSyllabusConfig($data_json_syl, $lessonplan_id){
    $json_syl = array();
    foreach ($data_json_syl as $json) {
        if (!empty($json)) {
            $syllabus_item = json_decode(html_entity_decode($json), true);
            foreach ($syllabus_item as $value) {
                if (empty($value['lesson']))
                    continue;
                $json_syl[$value['lesson']] = $value;
            }
        }
    }

    $cnt_removed    = removeSyllabus($json_syl, $lessonplan_id);
    $new_syllabus   = addSyllabus($json_syl, $lessonplan_id);
    $cnt_added      = $new_syllabus['cnt_added'];
    $cnt_updated    = $new_syllabus['cnt_updated'];
    return ($cnt_removed > 0 || $cnt_added > 0 || $cnt_updated > 0);
}
function removeSyllabus($json_syl, $lessonplan_id){
    // Remove Syllabus
    $ids = array();
    $q = "SELECT IFNULL(j_syllabus.id, '') primary_id
    FROM j_syllabus WHERE j_syllabus.lessonplan_id = '{$lessonplan_id}' AND j_syllabus.deleted = 0";
    $rows = $GLOBALS['db']->fetchArray($q);
    $before_sylls = array_column($rows, 'primary_id');
    $after_sylls = array_column($json_syl, 'primary_id');
    $remove_sylls = array_diff($before_sylls, $after_sylls);
    foreach ($before_sylls as $key => $syll){
        if (in_array($syll, $remove_sylls)) {
            $ids[] = $syll;
        }
    }

    if (count($ids) > 0) {
        $extIds = implode("','", $ids);
        $GLOBALS['db']->query("UPDATE j_syllabus SET deleted = 1, date_modified='{$GLOBALS['timedate']->nowDb()}',modified_user_id='{$GLOBALS['current_user']->id}'
            WHERE deleted = 0 AND id IN ('$extIds')");
    }
    return count($ids);
}
function addSyllabus($json_syl, $lessonplan_id){
    $cnt_added = $cnt_updated = 0;
    foreach($json_syl as $syll) {
        if(!empty($syll['primary_id'])) $syllabus_bean = BeanFactory::getBean('J_Syllabus', $syll['primary_id']);
        else $syllabus_bean = BeanFactory::newBean('J_Syllabus');

        if(empty($syllabus_bean->id)) $cnt_added++;
        else{
            //2 field này có update trong meetings
            if($syll['lesson'] != $syllabus_bean->lesson
            || $syll['learning_type'] != $syllabus_bean->learning_type) $cnt_updated++;
        }

        $syllabus_bean->lessonplan_id = $lessonplan_id;
        $syllabus_bean->lesson = $syll['lesson'];
        $syllabus_bean->lesson_type = $syll['type'];
        $syllabus_bean->learning_type = $syll['learning_type'];
        $syllabus_bean->description = $syll['content'];
        $syllabus_bean->name = $syll['theme'];
        $syllabus_bean->homework = $syll['homework'];
        $syllabus_bean->note_for_teacher = $syll['objective'];
        $syllabus_bean->save();
    }
    return array(
        'cnt_added' => $cnt_added,
        'cnt_updated' => $cnt_updated,
    );
}