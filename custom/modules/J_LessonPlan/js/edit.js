$(document).ready(function () {
    var section = $('#tblSyllabusContent');
    displayRemoveBtn(section);
    dragTable();
    s2_tbl();
    $('.edit > h4').css('padding', '6px 0');

    // Hide quick create button when open popup in module Lesson Plan
    if ($('input[name=target_id]').val() === undefined && $('input[name=target_module]').val() === undefined)
        $('#addformlink').hide();

    $('#copy_from_lp_select').live('click',function(){
        let value = $(this).is(':checked') ? '1' : '0'
        $('#is_copy_from_current_lp').val(value);
    });

    $('.sys_lesson, .sys_type, .learning_type, .sys_content, .sys_theme, .sys_homework, .sys_objective').live('change', function () {
        var row = $(this).closest('.rowSyllabus');
        saveJsonSyl(row);
    });

    // Pop-up select Lesson Plan
    $('#prv_openpopup_lesson_plan').live('click', function () {
        open_popup("J_LessonPlan", 1000, 700, "", true, true, {
            "call_back_function": "setLessonPlanReturn",
            "form_name": "EditView",
            "field_to_name_array": {
                "id": "copy_from_lp_id",
                "name": "copy_from_lp_name"
            },
        }, "single", true);
    });

    //Import syllabus
    $('.submit_file_syllabus').live('click',function (e) {
        e.preventDefault();
        app.alert.show('uploading', {
            level: 'process',
            title: 'Uploading'
        });
        var table = $(this).closest('table');
        var file = table.find('.fileToUpload').prop('files')[0];
        var data = new FormData();
        data.append('file',file);
        $.ajax({
            url: 'index.php?module=J_LessonPlan&action=handleAjaxLessonPlan&dotb_body_only=true&type=ajaxGetSyllabusFromExcel',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {
                showSyllabus(data,table);
            }
        })
    });
});
function dragTable() {
    //Helper function to keep table row from collapsing when being sorted
    var fixHelperModified = function (e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function (index) {
            $(this).width($originals.eq(index).width());
        });
        return $helper;
    };

    //Make diagnosis table sortable
    $("#tblSyllabusContent tbody").sortable({
        helper: fixHelperModified
    });
}
function s2_tbl(){
    $('#tblSyllabusContent > tr').not(':first').find('.lms_template_id').each(function(index, tr) {
        $(this).select2({width: 'resolve'});
    });
}
function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (14+o.scrollHeight)+"px";
}
function displayRemoveBtn(section) {
    if (section.find('tbody tr').length > 1) {
        section.find('tbody tr').each(function () {
            $(this).find('td:last>button').show();
        });
    } else {
        section.find('tbody tr').each(function () {
            $(this).find('td:last>button').hide();
        });
    }

}

function setLessonPlanReturn(popup_reply_data){
    DOTB.ajaxUI.showLoadingPanel();
    var name_to_value_array = popup_reply_data.name_to_value_array;
    $.ajax({
        url: "index.php?module=J_LessonPlan&action=handleAjaxLessonPlan&dotb_body_only=true",
        type: "POST",
        async: true,
        data: {
            type: 'ajaxLoadSyllabusByLP',
            lessonplan_id: name_to_value_array.copy_from_lp_id,
        },
        dataType: "json",
        success: function (res) {
            DOTB.ajaxUI.hideLoadingPanel();
            if (res.success){
                var position = $('#tblSyllabusContent');
                $(position).find('tr').each(function (index) {
                    if(index > 2) $(this).detach();
                });
                if (res.data.length === 0)
                    addSyllabusWithValue(position , []);
                else {
                    $.each(res.data, function(level, json_syl){
                        addSyllabusWithValue(position , json_syl);
                        displayRemoveBtn(position);
                    });
                }
            }
        },
    });

}

function addSyllabus(element) {
    var section = $(element).closest('table');
    var itemSyllabus = section.find('tbody tr').first().clone();

    var body = section.find('tbody');
    var value = parseInt(body.find('.sys_lesson').last().val());
    if (!value) {
        value = 0;
    }
    itemSyllabus.removeAttr('style');
    itemSyllabus.find('.sys_lesson').val(value + 1);
    body.append(itemSyllabus);
    displayRemoveBtn(section);
    saveJsonSyl(itemSyllabus);
}
function removeSyllabus(element) {
    var section = $(element).closest('table');
    var removeItem = $(element).closest("tr");
    $(removeItem).fadeOut(300, function () {
        $(removeItem).detach();
        displayRemoveBtn(section);
    });
}
function showSyllabus(responseText,form) {
    var json = jQuery.parseJSON(responseText);
    app.alert.dismiss('uploading');
    if (json['success'] === "1") {
        var position = $('#tblSyllabusContent');

        //Delete all tr
        $(position).find('tr').each(function (index) {
            if(index > 2) $(this).detach();
        });

        $.each(json['data'], function(index, json_syl){
            addSyllabusWithValue(position , json_syl);
            displayRemoveBtn(position);
        });
    } else {
        app.alert.show('error-mess', {
            level: 'error',
            messages: json['errorLabel'],
            autoClose: true
        });
    }
}
function addSyllabusWithValue(position , data) {
    var body = position.find('tbody');
    var itemSyllabus = position.find('tbody tr.rowSyllabus').first().clone();
    let lesson = (data['lesson'] === undefined) ? '1' : data['lesson'];
    itemSyllabus.find('.sys_lesson').val(lesson);
    itemSyllabus.find('.sys_type').val(data['type']);
    itemSyllabus.find('.learning_type').val(data['learning_type']);
    itemSyllabus.find('.sys_theme').val(data['topic']);
    itemSyllabus.find('.sys_content').val(data['syllabus']);
    itemSyllabus.find('.sys_objective').val(data['note']);
    itemSyllabus.find('.sys_homework').val(data['homework']);
    itemSyllabus.removeAttr('style');
    body.append(itemSyllabus);
    saveJsonSyl(itemSyllabus)
}

//Overwrite check_form to validate
function check_form(formname) {
    var validateConfig = true;
    $('#tblSyllabusContent').each(function(){
        var arrayCheck = [];
        $(this).find('.rowSyllabus').not(':first').each(function(){
            var lessonNum = $(this).find('input[name="sys_lesson[]"]').val();
            if (!isNumeric(lessonNum)) {
                $(this).find('.sys_lesson').effect("highlight", {color: '#FF0000'}, 4000);
                toastr.error('Error: Invalid lesson number (This field can only contain numeric values)');
                validateConfig = false;
            } else {
                if(arrayCheck.indexOf(lessonNum) === -1){
                    arrayCheck.push(lessonNum)
                } else {
                    $(this).find('.sys_lesson').effect("highlight", {color: '#FF0000'}, 4000);
                    toastr.error('Error: Duplicate lesson number');
                    validateConfig = false;
                }
            }
        });
    });
    return validate_form(formname, '') && validateConfig;
}
// Lưu syllabus
function saveJsonSyl(row) {
    let json = {};
    let child = {}

    child.primary_id = row.find('.sys_primary_id').val();
    child.lesson = row.find('.sys_lesson').val();
    child.type = row.find('.sys_type').val();
    child.learning_type = row.find('.learning_type').val();
    child.content = row.find('.sys_content').val();
    child.theme = row.find('.sys_theme').val();
    child.homework = row.find('.sys_homework').val();
    child.objective = row.find('.sys_objective').val();
    json[child.lesson] = child;
    if (child.lesson !== '') {
        //Assign json
        var json_str = JSON.stringify(json);
        row.find("input.json_syl").val(json_str);
    }
}

//Handle Save All Lesson Type, Learning Type
function handleSaveAll(field, value){
    if (typeof value != 'undefined' && typeof field != 'undefined'){
        var language = parent.DOTB.App.lang;
        var countDiff = countET = 0;
        var value_text = value;
        var field_text = field;
        if(field == 'sys_type'){
            field_text = 'LBL_LESSON_TYPE';
            value_text = language.getAppListStrings('syllabus_type_list')[value];
        }
        if(field == 'learning_type'){
            field_text = 'LBL_LEARNING_TYPE';
            value_text = language.getAppListStrings('learning_type_list')[value];
        }


        $('.'+field).each(function( index ){
            if($(this).val() != value && $(this).val() != '') countDiff++;
            if($(this).val() == '' && value != '') countET++
        });
        if(countDiff != 0){
            $.confirm({
                title: language.get('LBL_OVERRIDE_TITLE','J_LessonPlan'),
                content: language.get(field_text,'J_Syllabus')+': <b>'+value_text+'</b><br>'+language.get('LBL_ATT_OVERRIDE_CONTENT','J_Class'),
                buttons: {
                    "OK": {
                        btnClass: 'btn-blue',
                        action: function(){
                            $('.'+field).val(value).trigger('change');
                        }
                    },
                    'Cancel': {
                        text: language.get('LBL_BTN_CANCEL_C','J_Class'),
                        action: function(){

                        }
                    },
                }
            });
        }else if(countET != 0) $('.'+field).val(value).trigger('change');
    }

}

//Keyup count character for SMS/Apps Message
function genenrateMes(field){
    debugger;
    var language = parent.DOTB.App.lang;
    // prompt dialog
    $.confirm({
        title: language.get('LBL_ENTER_MESSAGES_C','J_Class'),
        content:'<form action="" class="formName">' +
        '<div class="form-group">' +
        '<div><textarea placeholder="'+language.get('LBL_PLACEHOLDER_MESSAGES_C','J_Class')+'" class="name form-control" required style="width: 85%;" rows="5"></textarea></div>' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: language.get('LBL_BTN_SUBMIT_C','J_Class'),
                btnClass: 'btn-blue',
                action: function () {
                    var content = this.$content.find('.name').val();
                    $('.'+field).val(content).trigger('change');
                }
            },
            'Cancel': {
                text: language.get('LBL_BTN_CANCEL_C','J_Class'),
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}