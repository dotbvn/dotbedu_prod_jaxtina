
//TRIGGER DO WHEN
DOTB.util.doWhen(
    function() {
        return $('a[data-subpanel=whole_subpanel_j_lessonplan_classes]').length == 1;
    },
    function() {
        $('a[data-subpanel=whole_subpanel_j_lessonplan_classes]').trigger('click');
});

$(document).ready(function () {
    $('#btn_syllabus_sync').live('click',function(){
        app.alert.show('message-id', {
            level: 'confirmation',
            messages: DOTB.language.get('J_LessonPlan', 'LBL_SYLLABUS_SYNC_ALERT'),
            autoClose: false,
            onConfirm: function () {
                DOTB.ajaxUI.showLoadingPanel();
                $.ajax({
                    type: "POST",
                    url : "index.php?module=J_LessonPlan&action=handleAjaxLessonPlan&dotb_body_only=true",
                    data:  {
                        type   : "ajaxSyncSyllabus",
                        syllabus_id : $('input[name="record"]').val(),
                    },
                    dataType: "json",
                    success:function(data){
                        DOTB.ajaxUI.hideLoadingPanel();
                        if (data.success == "1")
                            toastr.success(DOTB.language.get('J_LessonPlan', 'LBL_SYLLABUS_SYNC_SUCCESS')+' '+ data.message);
                        else toastr.success(DOTB.language.get('J_LessonPlan', 'LBL_SYLLABUS_SYNC_SUCCESS'));
                    },
                });
            },
            onCancel: function () {
                return false;
            }
        });
    });
});

