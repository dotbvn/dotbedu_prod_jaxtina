<?php
    // Do not store anything in this file that is not part of the array or the hook version.  This file will	
    // be automatically rebuilt in the future. 
    $hook_version = 1; 
    $hook_array = Array(); 
    $hook_array['before_save'] = Array();
    $hook_array['before_save'][] = Array(1, 'Handle Before Save', 'custom/modules/J_LessonPlan/logicLessonPlan.php', 'logicLessonPlan', 'handleBeforeSave');

    $hook_array['after_save'] = Array();
    $hook_array['after_save'][] = Array(1, 'Handle After Save', 'custom/modules/J_LessonPlan/logicLessonPlan.php', 'logicLessonPlan', 'handleAfterSave');

    $hook_array['process_record'] = Array();
    $hook_array['process_record'][] = Array(1, 'Process Record', 'custom/modules/J_LessonPlan/logicLessonPlan.php', 'logicLessonPlan', 'processRecord');

    $hook_array['before_delete'] = Array();
    $hook_array['before_delete'][] = Array(1, 'Handle Before Delete', 'custom/modules/J_LessonPlan/logicLessonPlan.php', 'logicLessonPlan', 'handleBeforeDelete');