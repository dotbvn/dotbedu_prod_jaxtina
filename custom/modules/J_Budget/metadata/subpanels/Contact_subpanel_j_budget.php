<?php
// created: 2023-08-20 23:50:17
$subpanel_layout['list_fields'] = array (
  'expense_code' => 
  array (
    'type' => 'varchar',
    'readonly' => true,
    'vname' => 'LBL_EXPENSE_CODE',
    'width' => 10,
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'expense_for' => 
  array (
    'type' => 'enum',
    'default' => true,
    'vname' => 'LBL_EXPENSE_FOR',
    'width' => 10,
  ),
  'expense_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_EXPENSE_DATE',
    'width' => 10,
    'default' => true,
  ),
  'amount' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_AMOUNT',
    'width' => 10,
    'default' => true,
  ),
  'payment_method' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PAYMENT_METHOD',
    'width' => 10,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);