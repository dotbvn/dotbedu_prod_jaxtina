<?php

class logicExpense
{
    /*AFTER_SAVE*/
    public function addCode(&$bean, $event, $arguments){
        $code_field = 'expense_code';
        if (empty($bean->$code_field)) {
            //AFTER_SAVE: Repeat 10 times to avoid duplicate codes
            for ($x = 0; $x < 10; $x++) {
                //Get Prefix
                $res = $GLOBALS['db']->query("SELECT teams.code_prefix code FROM teams WHERE id = '{$bean->team_id}'");
                $row = $GLOBALS['db']->fetchByAssoc($res);
                $prefix = 'EXP'.$row['code'];
                $year = date('y', strtotime('+ 7hours' . (!empty($bean->date_entered) ? $bean->date_entered : $bean->fetched_row['date_entered'])));
                $table = $bean->table_name;
                $sep = '';
                $first_pad = '0000';
                $padding = 4;
                $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, " . strlen($prefix . $year) . ") = '" . $prefix . $year . "') ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";

                $result = $GLOBALS['db']->query($query);
                if ($row = $GLOBALS['db']->fetchByAssoc($result))
                    $last_code = $row[$code_field];
                else
                    //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $last_code = $prefix . $year . $sep . $first_pad;


                $num = substr($last_code, -$padding, $padding);
                $num++;
                $pads = $padding - strlen($num);
                $new_code = $prefix . $year . $sep;

                //preform the lead padding 0
                for ($i = 0; $i < $pads; $i++) $new_code .= "0";
                $new_code .= $num;

                //check duplicate code
                $countDup = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM $table WHERE $code_field = '$new_code' AND deleted = 0");
                if(empty($countDup)){
                    //write to database - Logic: Before Save
                    $GLOBALS['db']->query("UPDATE $table SET $code_field = '$new_code' WHERE id='{$bean->id}' AND deleted = 0");
                    break;
                }
            }
        }
    }

}
