({
    extendsFrom: 'RecordView',

    initialize: function (options) {
        this._super('initialize', [options]);

        this.events = _.extend({}, this.events, {
            'click [name=export_excel]': 'exportExcel',
        });
    },
    
    exportExcel: function (event) {
        var id = app.controller.context.attributes.modelId;
        var module = app.controller.context.attributes.module;
        window.open('#bwc/index.php?module=J_Payment&action=centerExpenses&record=' + id+'&module_type=' + module);
    },
});
