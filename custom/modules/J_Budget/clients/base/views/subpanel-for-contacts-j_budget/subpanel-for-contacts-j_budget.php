<?php
// created: 2023-08-20 23:50:17
$viewdefs['J_Budget']['base']['view']['subpanel-for-contacts-j_budget'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'expense_code',
          'label' => 'LBL_EXPENSE_CODE',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
        1 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
          'width' => 'large',
        ),
        2 => 
        array (
          'name' => 'expense_for',
          'label' => 'LBL_EXPENSE_FOR',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'expense_date',
          'label' => 'LBL_EXPENSE_DATE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'amount',
          'label' => 'LBL_AMOUNT',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'payment_method',
          'label' => 'LBL_PAYMENT_METHOD',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'date_modified',
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_entered',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);