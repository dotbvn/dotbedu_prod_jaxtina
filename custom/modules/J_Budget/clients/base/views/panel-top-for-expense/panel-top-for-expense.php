<?php

$viewdefs['J_Budget']['base']['view']['panel-top-for-expense'] = array(
    'type' => 'panel-top',
    'template' => 'panel-top',
    'buttons' => array(
        array(
            'type' => 'button',
            'name' => 'create_button',
            'icon' => 'fa-plus',
            'css_class' => 'btn',
            'label' => 'LBL_CREATE_EXPENSE',
            'acl_module' => 'J_Budget',
            'acl_action' => 'create',
        ),
    ),
);
