<?php


$viewdefs['J_Budget']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_BUDGET',
			'tooltip' => 'LNK_NEW_J_BUDGET',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_Budget',
            'route'=>'#J_Budget/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_BUDGET_REPORTS',
			'tooltip' => 'LNK_J_BUDGET_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_Budget',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);