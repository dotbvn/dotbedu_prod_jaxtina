<?php
// created: 2021-09-22 07:02:02
$viewdefs['J_Budget']['base']['filter']['default'] = array(
  'default_filter' => 'all_records',
  'fields' =>
  array(
    'name' =>
    array(
    ),
    'payment_method' =>
    array(
    ),
    'expense_date' =>
    array(
    ),
    'expected_expense_date' =>
    array(
    ),
    'money_receiver' =>
    array(
    ),
    'expense_code' =>
    array(
    ),
    'status' =>
    array(
    ),
    'expense_for' =>
    array(
    ),
    'team_name' =>
    array(
    ),
    'amount' =>
    array(
    ),
    'assigned_user_name' =>
    array(
    ),
    'date_modified' =>
    array(
    ),
    'date_entered' =>
    array(
    ),
    'modified_by_name' =>
    array(
    ),
    'description' =>
    array(
    ),
    'tag' =>
    array(
    ),
    'created_by_name' =>
    array(
    ),
    '$owner' =>
    array(
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' =>
    array(
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);