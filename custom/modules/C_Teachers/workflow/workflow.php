<?php

use Dotbcrm\Dotbcrm\Util\Arrays\ArrayFunctions\ArrayFunctions;
include_once("include/workflow/alert_utils.php");
include_once("include/workflow/action_utils.php");
include_once("include/workflow/time_utils.php");
include_once("include/workflow/trigger_utils.php");
//BEGIN WFLOW PLUGINS
include_once("include/workflow/custom_utils.php");
//END WFLOW PLUGINS
	class C_Teachers_workflow {
	function process_wflow_triggers(& $focus){
		include("custom/modules/C_Teachers/workflow/triggers_array.php");
		include("custom/modules/C_Teachers/workflow/alerts_array.php");
		include("custom/modules/C_Teachers/workflow/actions_array.php");
		include("custom/modules/C_Teachers/workflow/plugins_array.php");
		if(empty($focus->fetched_row['id']) || (!empty($_SESSION["workflow_cron"]) && $_SESSION["workflow_cron"]=="Yes" && !empty($_SESSION["workflow_id_cron"]) && ArrayFunctions::in_array_access("5a0c041a-9f14-11e9-a713-005056b6e651", $_SESSION["workflow_id_cron"]))){ 
 
 if(true){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['e773f994_fb6c_11ed_9e27_067482011a2a'])){
		$triggeredWorkflows['e773f994_fb6c_11ed_9e27_067482011a2a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "e1fbcba8-9f14-11e9-9b37-005056b6e651"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['C_Teachers0_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 


	//end function process_wflow_triggers
	}

	//end class
	}

?>