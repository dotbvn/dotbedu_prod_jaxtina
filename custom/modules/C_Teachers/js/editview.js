$(document).ready(function(){
    toggleGeneratePWEMS();
    $('#reset_password').on('change',function(){
        toggleGeneratePWEMS();
        togglePWInputEMS();
    });
    toggleGeneratePWLMS();
    $('#lms_reset_password').on('change',function(){
        toggleGeneratePWLMS();
        togglePWInputLMS();
    });
    togglePWInputEMS();
    $('input[name="generate_password"]').live('change',function(){
        togglePWInputEMS();
    });

    togglePWInputLMS();
    $('input[name="lms_generate_password"]').live('change',function(){
        togglePWInputLMS();
    });

    $('select#type').select2({width: '150px' });
    $('select#kind_of_course').select2({width: '200px' });
});

function toggleGeneratePWEMS(){
    if($('#reset_password').is(':checked')){
        $('input[name="generate_password"]').closest('label').show();
        $('#generate_password_label').find('label').show();
    }else {
        $('input[name="generate_password"]').closest('label').hide();
        $('#generate_password_label').find('label').hide();
    }
}

function toggleGeneratePWLMS(){
    if($('#lms_reset_password').is(':checked')){
        $('input[name="lms_generate_password"]').closest('label').show();
        $('#lms_generate_password_label').find('label').show();
    }else{
        $('input[name="lms_generate_password"]').closest('label').hide();
        $('#lms_generate_password_label').find('label').hide();
    }
}

function togglePWInputEMS(){
    if($('input[name="generate_password"]:checked').val() == 'manual' && $('#reset_password').is(':checked')){
        $('#user_password').show();
        addToValidateMinLength('EditView', 'user_password', 'varchar', true, 8 , DOTB.language.get('C_Teachers', 'LBL_USER_PASSWORD'));
    }else{
        $('#user_password').hide();
        removeFromValidate('EditView','user_password');
    }
}

function togglePWInputLMS(){
    if($('input[name="lms_generate_password"]:checked').val() == 'manual' && $('#lms_reset_password').is(':checked')){
        $('#lms_user_password').show();
        addToValidateMinLength('EditView', 'lms_user_password', 'varchar', true, 8 , DOTB.language.get('C_Teachers', 'LBL_USER_PASSWORD'));
    }else{
        $('#lms_user_password').hide();
        removeFromValidate('EditView','lms_user_password');
    }
}