<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will
// be automatically rebuilt in the future.
 $hook_version = 1;
$hook_array = Array();
// position, file, function
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1,'Make Full Name','custom/modules/C_Teachers/logicTeacher.php','logicTeacher','make_full_name');
$hook_array['before_save'][] = Array(2, 'Add User LMS', 'custom/modules/C_Teachers/logicTeacher.php', 'logicTeacher', 'handleLMSUser');

$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1,'Add Team','custom/modules/C_Teachers/logicTeacher.php','logicTeacher','addTeam');
$hook_array['after_save'][] = Array(2,'Add Team','custom/modules/C_Teachers/logicTeacher.php','logicTeacher','addUser');
$hook_array['after_save'][] = Array(100,'Add Code','custom/modules/C_Teachers/logicTeacher.php','logicTeacher','addCode');

$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(1,'Color','custom/modules/C_Teachers/logicTeacher.php','logicTeacher','ListviewTeacher');
$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(2,'Delete Teacher','custom/modules/C_Teachers/logicTeacher.php','logicTeacher','deletedTeacher');
?>