<?php
// created: 2024-03-01 11:46:21
$subpanel_layout['list_fields'] = array (
  'teacher_id' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_TEACHER_ID',
    'width' => 10,
    'default' => true,
  ),
  'full_teacher_name' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_NAME',
    'width' => 10,
    'default' => true,
  ),
  'first_name' => 
  array (
    'name' => 'first_name',
    'usage' => 'query_only',
  ),
  'last_name' => 
  array (
    'name' => 'last_name',
    'usage' => 'query_only',
  ),
  'salutation' => 
  array (
    'name' => 'salutation',
    'usage' => 'query_only',
  ),
);