<?php
$module_name = 'C_Teachers';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                ),
            ),
            'includes' =>
            array (
                1 =>
                array (
                    'file' => 'custom/include/javascript/DateRange/moment.min.js',
                ),
                2 =>
                array (
                    'file' => 'custom/include/javascript/DateRange/jquery.daterangepicker.js',
                ),
                3 =>
                array (
                    'file' => 'custom/modules/C_Teachers/js/detailview.js',
                ),
            ),
            'useTabs' => true,
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'tabDefs' =>
            array (
                'LBL_CONTACT_INFORMATION' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_MORE_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_OTHER_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_contact_information' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'teacher_id',
                        'label' => 'LBL_TEACHER_ID',
                        'customCode' => '{$TEACHER_CODE}',
                    ),
                    1 =>
                    array (
                        'name' => 'status',
                        'studio' => 'visible',
                        'label' => 'LBL_STATUS',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'full_teacher_name',
                        'label' => 'LBL_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'picture',
                        'comment' => 'Picture file',
                        'label' => 'LBL_PICTURE_FILE',
                    ),
                ),
                2 =>
                array (
                    0 => 'title',
                    1 =>
                    array (
                        'name' => 'type',
                        'studio' => 'visible',
                        'label' => 'LBL_TYPE',
                    ),
                ),
                3 =>
                array (
                    0 => 'phone_mobile',
                    1 => 'phone_other',
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'identify_id',
                        'label' => 'LBL_IDENTIFY_ID',
                    ),
                    1 =>
                    array (
                        'name' => 'kind_of_course',
                        'studio' => 'visible',
                        'label' => 'LBL_KIND_OF_COURSE',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'dob',
                        'label' => 'LBL_DOB',
                    ),
                    1 => 'hr_code',
                ),
                6 =>
                array (
                    0 => 'email1',
                    1 =>
                    array (
                        'name' => 'primary_address_street',
                        'label' => 'LBL_PRIMARY_ADDRESS',
                        'type' => 'address',
                        'displayParams' =>
                        array (
                            'key' => 'primary',
                        ),
                    ),
                ),
                7 =>
                array (
                    0 => 'description',
                ),
            ),
            'LBL_MORE_INFORMATION' =>
            array (
                0 =>
                array (
                    0 => 'teaching_cerificate',
                    1 =>
                    array (
                        'name' => 'experience',
                        'studio' => 'visible',
                        'label' => 'LBL_EXPERIENCE',
                    ),
                ),
                1 =>
                array (
                    0 => 'married',
                    1 => 'passport_number',
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'visa_number',
                        'label' => 'LBL_VISA_NUMBER',
                    ),
                    1 =>
                    array (
                        'name' => 'visa_expiration_date',
                        'label' => 'LBL_VISA_EXPIRATION_DATE',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'passport_issued_date',
                        'label' => 'LBL_PASSPORT_ISSUED_DATE',
                    ),
                    1 =>
                    array (
                        'name' => 'passport_expired_date',
                        'label' => 'LBL_PASSPORT_EXPIRED_DATE',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'nationality',
                        'label' => 'LBL_NATIONALITY',
                    ),
                    1 => 'gender',
                ),
                5 =>
                array (
                    0 => '',
                    1 => 'salary',
                ),
            ),
            'LBL_OTHER_INFORMATION' =>
            array (
                0 =>
                array (
                    0 => 'assigned_user_name',
                    1 => 'team_name',
                ),
                1 =>
                array (
                    0 => array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 => '',
                    1 =>
                    array (
                        'name' => 'lms_active',
                        'studio' => 'true',
                        'customLabel' => '{$MOD.LBL_LMS_ACTIVE}',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'user_name',
                        'customLabel' => '{$MOD.LBL_USER_NAME}',
                    ),
                    1 =>
                    array (
                        'name' => 'lms_user_name',
                        'customLabel' => '{$MOD.LBL_LMS_USER_NAME}',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'user_password',
                        'customLabel' => '{if access_edit}{$MOD.LBL_USER_PASSWORD}{/if}',
                        'customCode' => '{if access_edit}<mark style="font-weight: bold;">{$fields.user_password.value}</mark>{/if}'
                    ),
                    1 =>
                    array (
                        'name' => 'lms_user_password',
                        'customLabel' => '{if access_edit}{$MOD.LBL_LMS_USER_PASSWORD}{/if}',
                        'customCode' => '{if access_edit}<mark style="font-weight: bold;">{$fields.lms_user_password.value}</mark>{/if}'
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'user_id',
                        'customLabel' => '{$MOD.LBL_USER_ID}',
                    ),
                    1 =>
                    array (
                        'name' => 'lms_user_id',
                        'customLabel' => '{$MOD.LBL_LMS_USER_ID}',
                    ),
                ),
            ),
        ),
    ),
);
