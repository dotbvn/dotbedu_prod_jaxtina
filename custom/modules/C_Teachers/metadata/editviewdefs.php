<?php
$module_name = 'C_Teachers';
$viewdefs[$module_name] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'hidden' =>
                array (
                    1 => '<input type="hidden" name="teacher_type" id="teacher_type" value="{$fields.teacher_type.value}">',
                ),
            ),
            'maxColumns' => '2',
            'javascript' => '
            {dotb_getscript file="custom/modules/C_Teachers/js/editview.js"}
            {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
            <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>
            ',
            'useTabs' => true,
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'tabDefs' =>
            array (
                'LBL_CONTACT_INFORMATION' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_MORE_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_OTHER_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_contact_information' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'teacher_id',
                        'label' => 'LBL_TEACHER_ID',
                        'customCode' => '<input type="text" class="input_readonly" name="teacher_idd" id="teacher_id" maxlength="255" value="{$fields.teacher_id.value}" title="{$MOD.LBL_TEACHER_ID}" size="30" readonly>',
                    ),
                    0 =>
                    array (
                        'name' => 'teacher_id',
                        'customCode' => '<input type="text" name="teacher_id_s" id="teacher_id_s" maxlength="255" value="{$fields.teacher_id.value}" title="{$MOD.LBL_TEACHER_ID}" placeholder="{$MOD.LBL_AUTO_GENERATE}" size="30"> ',
                    ),
                    1 =>
                    array (
                        'name' => 'status',
                        'studio' => 'visible',
                        'label' => 'LBL_STATUS',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'full_teacher_name',
                        'label' => 'LBL_NAME',
                        'displayParams' =>
                        array (
                            'required' => false,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'picture',
                        'comment' => 'Picture file',
                        'label' => 'LBL_PICTURE_FILE',
                    ),
                ),
                2 =>
                array (
                    0 => 'title',
                    1 =>
                    array (
                        'name' => 'type',
                        'studio' => 'visible',
                        'label' => 'LBL_TYPE',
                    ),
                ),
                3 =>
                array (
                    0 => 'phone_mobile',
                    1 => 'phone_other',
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'identify_id',
                        'label' => 'LBL_IDENTIFY_ID',
                    ),
                    1 =>
                    array (
                        'name' => 'kind_of_course',
                        'studio' => 'visible',
                        'label' => 'LBL_KIND_OF_COURSE',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'dob',
                        'label' => 'LBL_DOB',
                    ),
                    1 => 'hr_code',
                ),
                6 =>
                array (
                    0 => 'email1',
                    1 =>
                    array (
                        'name' => 'primary_address_street',
                        'hideLabel' => true,
                        'type' => 'address',
                        'displayParams' =>
                        array (
                            'key' => 'primary',
                            'rows' => 2,
                            'cols' => 30,
                            'maxlength' => 150,
                        ),
                    ),
                ),
                7 =>
                array (
                    0 =>
                    array(
                      'name' => 'teacher_contract_info',
                      'label' => 'LBL_CONTRACT_INFO',
                    ),
                    1 =>
                    array (
                        'name' => 'description',
                        'displayParams' =>
                        array (
                            'rows' => 4,
                            'cols' => 60,
                        ),
                    ),
                ),
            ),
            'LBL_MORE_INFORMATION' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'teaching_cerificate',
                        'studio' => 'visible',
                        'label' => 'LBL_TEACHING_CERIFICATE',
                    ),
                    1 =>
                    array (
                        'name' => 'experience',
                        'studio' => 'visible',
                        'label' => 'LBL_EXPERIENCE',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array(
                        'name' => 'teacher_entrance_score',
                        'label' => 'LBL_ENTRANCE_SCORE',
                    ),
                    1 => 'passport_number',
                ),
                2 =>
                array (
                    0 => 'married',
                    1 =>
                    array (
                        'name' => 'visa_expiration_date',
                        'label' => 'LBL_VISA_EXPIRATION_DATE',
                    ),
                ),
                3 =>
                array (
                    0 =>
                        array (
                            'name' => 'visa_number',
                            'label' => 'LBL_VISA_NUMBER',
                        ),
                    1 =>
                        array (
                            'name' => 'passport_expired_date',
                            'label' => 'LBL_PASSPORT_EXPIRED_DATE',
                        ),
                ),
                4 =>
                array (
                    0 =>
                        array (
                            'name' => 'passport_issued_date',
                            'label' => 'LBL_PASSPORT_ISSUED_DATE',
                        ),
                    1 => 'gender',
                ),
                5 =>
                array (

                    0 =>
                    array (
                        'name' => 'nationality',
                        'label' => 'LBL_NATIONALITY',
                    ),
                    1 => 'salary',
                ),
            ),
            'LBL_OTHER_INFORMATION' =>
            array (
                0 =>
                array (
                    0 => 'assigned_user_name',
                    1 => 'team_name',
                ),
            ),
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 => '',
                    1 =>
                    array (
                        'name' => 'lms_active',
                        'studio' => 'true',
                        'label' => 'LBL_LMS_ACTIVE',
                    ),
                ),
                1 =>
                array (
                    0 => array (
                        'name' => 'user_name',
                        'readonly' => true,
                        'label' => 'LBL_USER_NAME',
                        'customCode' => '{$fields.user_name.value}',
                    ),
                    1 =>
                    array (
                        'name' => 'lms_user_name',
                        'readonly' => true,
                        'label' => 'LBL_LMS_USER_NAME',
                        'customCode' => '{$fields.lms_user_name.value}',
                    ),
                ),
                2 =>
                array (
                    0 => array (
                        'name' => 'reset_password',
                        'customLabel' => '{$MOD.LBL_RESET_PASSWORD}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_RESET_PASSWORD_HELP}\');" src="themes/RacerX/images/helpInline.png">',
                    ),
                    1 =>
                    array (
                        'name' => 'lms_reset_password',
                        'customLabel' => '{$MOD.LBL_RESET_PASSWORD}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_RESET_PASSWORD_HELP}\');" src="themes/RacerX/images/helpInline.png">',
                    ),
                ),
                3 =>
                array (
                    0 => array (
                        'name' => 'generate_password',
                        'customLabel' => '<img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_GENERATE_PASSWORD_HELP}\');" src="themes/RacerX/images/helpInline.png" style="float: right;">',
                    ),
                    1 => array (
                        'name' => 'lms_generate_password',
                        'customLabel' => '<img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_GENERATE_PASSWORD_HELP}\');" src="themes/RacerX/images/helpInline.png" style="float: right;">',
                    ),
                ),
                4 =>
                array (
                    0 => array (
                        'name' => 'user_password',
                        'label' => 'LBL_USER_PASSWORD',
                        'customCode' => '<input type="password" name="user_passwordd" id="user_password" size="30" maxlength="255" value="">',
                    ),
                    1 =>
                    array (
                        'name' => 'lms_user_password',
                        'label' => 'LBL_LMS_USER_PASSWORD',
                        'customCode' => '<input type="password" name="lms_user_passwordd" id="lms_user_password" size="30" maxlength="255" value="">',
                    ),
                ),
                5 =>
                array (
                    0 => array (
                        'name' => 'user_id',
                        'readonly' => true,
                        'label' => 'LBL_USER_ID',
                        'customCode' => '{$fields.user_id.value}',
                    ),
                    1 =>
                    array (
                        'name' => 'lms_user_id',
                        'readonly' => true,
                        'label' => 'LBL_LMS_USER_ID',
                        'customCode' => '{$fields.lms_user_id.value}',
                    ),
                ),
            ),
        ),
    ),
);
