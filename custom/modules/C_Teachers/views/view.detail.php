<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class C_TeachersViewDetail extends ViewDetail {
    function display() {
        echo '<link rel="stylesheet" type="text/css" href="custom/include/javascript/DateRange/daterangepicker.css">';
        if($this->bean->type=='TA')
            $html 	= '<span class="textbg_orange">'.$this->bean->teacher_id.'</span>';
        else
            $html 	= '<span class="textbg_blue">'.$this->bean->teacher_id.'</span>';

        $html .='<input type="hidden" id="teacher_type" name="teacher_type" value="'.$this->bean->teacher_type.'"/>';
        $this->ss->assign('TEACHER_CODE', $html);

//        //User Teacher Link
//        $user_name = $GLOBALS['db']->getOne("SELECT user_name FROM users WHERE teacher_id = '{$this->bean->id}' AND deleted = 0");
//        $link_user = '';
//        if(!empty($user_name)){
//            $link_user = '<a style="text-decoration: none;font-weight: normal;" href="#bwc/index.php?module=Users&action=DetailView&record='.$this->bean->user_id.'">'.$user_name.'</a> | Default Password: @teacher123';
//            if($this->bean->type == 'TA')
//                $link_user = '<a style="text-decoration: none;font-weight: normal;" href="#bwc/index.php?module=Users&action=DetailView&record='.$this->bean->user_id.'">'.$user_name.'</a> | Default Password: @ta123';
//        }
//        $this->ss->assign('user_name',$link_user);

         $this->ss->assign('access_edit', (int)ACLController::checkAccess('C_Teachers', 'edit', true));

        parent::display();
    }
    function _displaySubPanels(){
        require_once ('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($this->bean, $this->module);
        if($this->bean->type=='TA'){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['c_teachers_j_teachercontract_1']);//hiding session
        }
        echo $subpanel->display();
    }

}
?>