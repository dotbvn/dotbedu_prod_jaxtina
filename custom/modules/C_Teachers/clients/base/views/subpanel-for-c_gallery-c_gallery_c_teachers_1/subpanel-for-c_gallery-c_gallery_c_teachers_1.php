<?php
// created: 2024-03-01 11:46:21
$viewdefs['C_Teachers']['base']['view']['subpanel-for-c_gallery-c_gallery_c_teachers_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'teacher_id',
          'label' => 'LBL_TEACHER_ID',
          'width' => 'xsmall',
          'type' => 'html',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'full_teacher_name',
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);