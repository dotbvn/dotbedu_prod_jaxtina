<?php


$viewdefs['C_Teachers']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_C_TEACHERS',
			'tooltip' => 'LNK_NEW_C_TEACHERS',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'C_Teachers',
            'route'=>'#C_Teachers/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_C_TEACHERS_REPORTS',
			'tooltip' => 'LNK_C_TEACHERS_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=C_Teachers',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);