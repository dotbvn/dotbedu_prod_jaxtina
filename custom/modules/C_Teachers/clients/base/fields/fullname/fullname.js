({
    extendsFrom: 'FieldsetField',

    /**
     * Mapping name field name to format initial
     *
     * @property {Object}
     */
    formatMap: {
        'l': 'last_name',
        'f': 'first_name',
    }
})