<?php
// created: 2019-05-24 01:39:25
$viewdefs['C_Teachers']['base']['filter']['default'] = array (
    'default_filter' => 'all_records',
    'fields' =>
    array (
        'full_teacher_name' =>
        array (
        ),
        'teacher_id' =>
        array (
        ),
        'experience' =>
        array (
        ),
        'type' =>
        array (
        ),
        'title' =>
        array (
        ),
        'primary_address_street' =>
        array (
        ),
        'email' =>
        array (
        ),
        'phone_mobile' =>
        array (
        ),
        'phone_other' =>
        array (
        ),
        'date_entered' =>
        array (
        ),
        'gender' =>
        array (
        ),
        'team_name' =>
        array (
        ),
        'date_modified' =>
        array (
        ),
        '$owner' =>
        array (
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' =>
        array (
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'created_by_name' =>
        array (
        ),
        'modified_by_name' =>
        array (
        ),
    ),
    'quicksearch_field' =>
    array (
        0 => 'teacher_id',
        1 => 'full_teacher_name',
        2 => 'phone_mobile',
        3 => 'email1',
    ),
    'quicksearch_priority' => 2,
);