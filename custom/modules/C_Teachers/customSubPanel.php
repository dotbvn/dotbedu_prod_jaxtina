<?php
///////////////////////--------------------------------------------/////////////////
function getSessions(){
    $args = func_get_args();
    $id_teacher = $args[0]['teacher_id'];

    $sql = "SELECT IFNULL(meetings.id, '') id,
    IFNULL(cl.id, '') ju_class_id,
    IFNULL(cl.name, '') ju_class_name,
    IFNULL(meetings.lesson_number, '') lesson_number,
    meetings.till_hour,
    IFNULL(meetings.name, '') name,
    IFNULL(meetings.session_status, '') session_status,
    meetings.duration_cal,
    meetings.teaching_hour,
    tea.assigned_user_id teacher_name_owner,
    'C_Teachers' teacher_name_mod,
    tea.full_teacher_name teacher_name,
    tea.full_teacher_name sub_teacher_name,
    tea.full_teacher_name teacher_cover_name,
    meetings.teacher_id,
    meetings.sub_teacher_id,
    meetings.teacher_cover_id,
    meetings.teaching_type,
    meetings.description,
    meetings.recurring_source,
    meetings.meeting_type,
    meetings.date_start,
    meetings.date_end,
    meetings.assigned_user_id,
    meetings.created_by,
    meetings.modified_user_id,
    'teachers_meetings' panel_name
    FROM meetings
    INNER JOIN c_teachers tea ON ( tea.id IN (meetings.teacher_id, meetings.sub_teacher_id, meetings.teacher_cover_id)) AND tea.deleted = 0
    INNER JOIN j_class cl ON meetings.ju_class_id = cl.id AND cl.deleted = 0
    WHERE (tea.id = '$id_teacher') AND (meetings.session_status <> 'Cancelled') AND meetings.deleted = 0 ";

    return $sql;
}

?>
