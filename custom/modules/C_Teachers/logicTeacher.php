<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicTeacher {
    /*AFTER_SAVE*/
    function addCode(&$bean, $event, $arguments){
        global $timedate;
        $code_field = 'teacher_id';
        $table      = $bean->table_name;
        if(empty($bean->$code_field)){
            //AFTER_SAVE: Repeat 10 times to avoid duplicate codes
            for ($x = 0; $x < 10; $x++) {
                if (strpos($bean->type, 'Teacher') !== false) $prefix = 'T';
                elseif(strpos($bean->type, 'TA') !== false) $prefix = 'A';
                //Get Prefix
                $prefix     .= date('y', strtotime($bean->date_entered));
                $first_pad  = '0000';
                $padding    = 4;
                $query      = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, ".strlen($prefix).") = '".$prefix."') ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";
                $result = $GLOBALS['db']->query($query);

                if($row = $GLOBALS['db']->fetchByAssoc($result))
                    $last_code = $row[$code_field];
                else
                    //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $last_code = $prefix   . $first_pad;

                $num = substr($last_code, -$padding, $padding);
                $num++;
                $pads = $padding - strlen($num);
                $new_code = $prefix . $sep ;

                //preform the lead padding 0
                for($i=0; $i < $pads; $i++) $new_code .= "0";
                $new_code .= $num;
                //check duplicate code
                $countDup = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM $table WHERE $code_field = '$new_code' AND deleted = 0");
                if(empty($countDup)){
                    //write to database - Logic: Before Save
                    $GLOBALS['db']->query("UPDATE $table SET $code_field = '$new_code' WHERE id='{$bean->id}' AND deleted = 0");
                    break;
                }
            }
        }
    }

    function make_full_name(&$bean, $event, $arguments){

        if ($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save' ) {
            if (!empty($bean->email1)) {
                //Check Duplicate Teacher Email
                $qC = "SELECT DISTINCT
                c_teachers.id teacher_id,
                COUNT(DISTINCT c_teachers.id) count_tea
                FROM c_teachers
                INNER JOIN email_addr_bean_rel l1_1 ON c_teachers.id = l1_1.bean_id AND l1_1.deleted = 0 AND l1_1.bean_module = 'C_Teachers' AND l1_1.primary_address = 1
                INNER JOIN email_addresses l1 ON l1.id = l1_1.email_address_id AND l1.deleted = 0
                WHERE (l1.email_address = '{$bean->email1}') AND c_teachers.deleted = 0 AND  c_teachers.id <> '{$bean->id}'
                GROUP BY l1.email_address";
                $rowC = $GLOBALS['db']->fetchOne($qC);
                if ($rowC['count_tea'] > 0) {
                    $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('C_Teachers','".$rowC['teacher_id']."'));";
                    echo '<script type="text/javascript">
                    window.parent.DOTB.App.alert.show(\'message-id\', {
                    level: \'error\',
                    messages: \''.translate('LBL_TEACHER_DUPLICATE','C_Teachers').'\',
                    autoClose: false});
                    '.$route.'
                    </script>';
                    die();
                };
            }
        }

        if(empty($bean->full_teacher_name)) $bean->full_teacher_name = $bean->name;
        if(empty($bean->full_teacher_name)) $bean->full_teacher_name = $bean->full_name;
        $parts = explode(' ',$bean->full_teacher_name);
        $bean->first_name   = $parts[count($parts) - 1];
        $bean->last_name    = trim(str_replace($bean->first_name,'',$bean->full_teacher_name));
        $bean->name         = $bean->full_teacher_name;

        if (!empty($_POST['teacher_id_s'])) $bean->teacher_id = $_POST['teacher_id_s'];
        $_POST['team_set_id']   = $bean->fetched_row['team_set_id'];
    }

    function ListviewTeacher(&$bean, $event, $arguments) {
        $class = "textbg_blue";
        if($bean->type=='^TA^') $class = "textbg_orange";
        $bean->teacher_id = "<span class='full-width visible'><span class='label ellipsis_inline $class' title='{$bean->teacher_id}'>".$bean->teacher_id.'</span></span>';
    }
    //after save
    function addTeam($bean, $event, $arguments){
        //Check MassUpdate by Lumia
        if(!empty($_REQUEST['__dotb_url'])){
            $post = explode("/",$_REQUEST['__dotb_url']);
            $_POST['module'] = $post[1];
            $_POST['action'] = $post[2];
        }
        if(($_POST['module'] == $bean->module_name  && ($_POST['action'] == 'Save' || $_POST['action'] == 'MassUpdate')) || $_POST['module'] == 'Import'){
            if($_POST['team_set_id'] != $bean->team_set_id){
                // Get all team set
                $teamSetBean = new TeamSet();
                $teams = $teamSetBean->getTeams($bean->team_set_id);
                // Add all team set to  $team_list
                $team_list = array();
                foreach ($teams as $key => $value) {
                    // Get children of team
                    $q1 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$key}'";
                    $rs1 = $GLOBALS['db']->query($q1);

                    while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
                        if (!isset($teams[$row['id']])) $team_list[] = $row['id'];
                        $q2 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$row['id']}'";
                        $rs2 = $GLOBALS['db']->query($q2);
                        while($row2 = $GLOBALS['db']->fetchByAssoc($rs2))
                            if (!isset($teams[$row['id']])) $team_list[] = $row2['id'];
                    }
                }
                if(count($team_list) > 0){
                    foreach ($teams as $key => $value) $team_list[] = $key;

                    if(!empty($team_list)){
                        $bean->load_relationship('teams');
                        //Add the teams
                        $bean->teams->replace($team_list);
                    }
                }
            }
        }
    }

    function addUser($bean, $event, $arguments){
        //Create user Teacher
        global $dotb_config;
        require_once("modules/Users/User.php");
        require_once('include/formbase.php');
        $newUser = false;
        if(empty($bean->user_id)){
            //check duplicate user - auto sync - Nếu trùng email KHÔNG khởi tạo tài khoản giáo viên
            $q1 = "SELECT IFNULL(id, '') user_id FROM users WHERE deleted = 0 AND user_name = '{$bean->email1}'";
            $user_id = $GLOBALS['db']->getOne($q1);
            if(!empty($user_id)){
                return true;
            }else{
                $user = BeanFactory::newBean('Users');
                $newUser = true;
                $user->user_name    = $bean->email1;
            }
        }else{
            $user = BeanFactory::getBean('Users', $bean->user_id);
            //Kiểm tra nếu đổi trùng email thì từ chối update user_name
            $q1 = "SELECT IFNULL(id, '') user_id FROM users WHERE deleted = 0 AND user_name='{$bean->email1}' AND id <> '{$bean->user_id}'";
            $user_id = $GLOBALS['db']->getOne($q1);
            if(empty($user_id)) $user->user_name = $bean->email1;
        }
        $user->email1       = $bean->email1;
        $user->first_name   = $bean->first_name;
        $user->last_name    = $bean->last_name;
        $user->title        = decodeMultienum($bean->type);
        $user->phone_mobile = $bean->phone_mobile;
        $user->phone_other  = $bean->phone_other;
        $user->teacher_id   = $bean->id;
        $user->description  = $bean->description;

        $user->address_street   = $bean->primary_address_street;
        $user->address_city     = $bean->primary_address_city;
        $user->address_state    = $bean->primary_address_state;
        $user->address_country  = $bean->primary_address_country;
        $user->status           = $bean->status;
        $user->save();
        //Set User_id
        $GLOBALS['db']->query("UPDATE c_teachers SET user_id = '{$user->id}' WHERE deleted = 0 AND id = '{$bean->id}'");
        // Set user preferences
        if($newUser){
            $user->setPreference('date_format', 'd/m/Y');
            $user->setPreference('time_format', 'h:ia');
            $user->setPreference('ut', 1);
            $user->setPreference('timezone', 'Asia/Ho_Chi_Minh');
            $user->setPreference('default_locale_name_format', 's l f');
            $user->savePreferencesToDB();
        }
        //Set Team
        if(( !empty($user->id) &&  $_POST['team_set_id'] != $bean->team_set_id ) || ($newUser) ){
            //Add User to Team
            if($user->team_id != $bean->team_id || $bean->team_set_id != $user->team_set_id){
                $teamSetBean = new TeamSet();
                $team_set_teacher =  array_keys($teamSetBean->getTeams($bean->team_set_id));
                $team_set_user = array_column($GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(l2.id, '') team_id FROM users
                    INNER JOIN team_memberships l2_1 ON users.id = l2_1.user_id AND l2_1.deleted = 0
                    INNER JOIN teams l2 ON l2.id = l2_1.team_id AND l2.deleted = 0 AND l2.id <> '1'
                    WHERE (users.deleted = 0) AND (users.id = '{$user->id}') AND (l2.private = 0)"), 'team_id');
                $team_diff1 =array_diff($team_set_teacher,$team_set_user);
                $team_diff2 =array_diff($team_set_user,$team_set_teacher);
                //Remove & add User to team
                foreach ($team_diff1 as $key => $team_id) {
                    $team_bean = BeanFactory::getBean('Teams',$team_id );
                    $team_bean->add_user_to_team($user->id);
                }
                foreach ($team_diff2 as $key => $team_id) {
                    $team_bean = BeanFactory::getBean('Teams',$team_id );
                    $team_bean->remove_user_from_team($user->id);
                }

                //Set Primary Team
                $user->team_id      = $bean->team_id;
                $user->team_set_id  = $bean->team_set_id;
                $user->save();
            }
        }
        //TH tạo mới thì set pass
        if($bean->reset_password || $newUser){
            if($bean->generate_password == 'defaut' || empty($bean->generate_password)){
                //Set Default Password
                $user->password = $GLOBALS['db']->getOne("SELECT IFNULL(value, '') value FROM config WHERE name = 'defaultteacherpwd' AND category = 'pwd'");
                if(empty($user->password)) $user->password = strtolower(str_replace(' ', '', $dotb_config['brand_name'])).'123';
            }

            if($bean->generate_password == 'auto')
                $user->password = User::generatePassword();

            if($bean->generate_password == 'manual')
                $user->password = $_POST['user_passwordd'];

            $GLOBALS['db']->query("UPDATE c_teachers SET reset_password=0, user_password='{$user->password}', generate_password='defaut' WHERE id='{$bean->id}'");
            $user->setNewPassword($user->password);
        }
        //Set Role Teacher

        if(!empty($user->id)){
            $types = unencodeMultienum($bean->type);
            //Kiểm tra tài khoản Giáo viên đó có đang set sai role ko ?
            if(in_array('Teacher',$types)) $special_role = 'teacher';
            elseif(in_array('TA',$types)) $special_role = 'ta';
            $current_roles = $GLOBALS['db']->fetchArray("SELECT
                acl_roles.id id, acl_roles.name name, acl_roles.special_role special_role FROM acl_roles
                INNER JOIN acl_roles_users ON acl_roles_users.user_id = '{$user->id}' AND acl_roles_users.role_id = acl_roles.id AND acl_roles_users.deleted = 0
                WHERE acl_roles.deleted = 0");
            foreach($current_roles as $role){
                if($role['special_role'] != $special_role) $wrong_roles++;
            }
            if($wrong_roles>0){ //nếu set sai role ==> Xoá Roles của nó
                $sql = "UPDATE acl_roles_users SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE user_id='{$user->id}' AND deleted = 0";
                $GLOBALS['db']->query($sql);
            }
            $role_id = $GLOBALS['db']->getOne("SELECT id FROM acl_roles WHERE special_role = '$special_role' AND deleted = 0 LIMIT 1");
            if(!empty($role_id)){
                $current_role = $GLOBALS['db']->fetchArray("SELECT id, role_id FROM acl_roles_users WHERE user_id='{$user->id}' AND deleted = 0");
                if(!in_array($role_id, array_column($current_role, 'role_id'))){
                    $focus = new ACLRole();
                    $focus->retrieve($role_id);
                    $focus->set_relationship('acl_roles_users', array('role_id'=>$focus->id ,'user_id'=>$user->id), false);
                }
            }else{ //Nếu không tìm thấy Role Teacher thì Inactive account teacher
                $sql = "UPDATE users SET status='Inactive' WHERE id='{$user->id}' AND deleted = 0";
                $GLOBALS['db']->query($sql);
            }
        }

        //END: Create Teacher User Name
    }

    function deletedTeacher($bean, $event, $arguments){
        $bean->load_relationship('c_teachers_j_teachercontract_1');
        $ct_ids = $bean->c_teachers_j_teachercontract_1->getBeans();
        foreach($ct_ids as $key =>$value){
            $GLOBALS['db']->query("UPDATE j_teachercontract SET deleted = 1  WHERE id = '$key' AND deleted = 0");
        }

        //Remove notification Mobile
        if(!empty($bean->user_id)){
            require_once("custom/clients/mobile/helper/NotificationHelper.php");
            $notify = new NotificationMobile();
            $notify->deleteNotification($bean->user_id, 'Teacher');
        }
        //Xóa teacher
        if(!empty($bean->user_id)){
            $user = BeanFactory::getBean('Users', $bean->user_id);
            $user->mark_deleted($user->id);
        }

        //Delete LMS Account
        if(!empty($bean->lms_user_id)){
            require_once('custom/include/utils/lms_helpers.php');
            $result = deleteUser($bean->lms_user_id);
        }
    }

    public function handleLMSUser(&$bean, $event, $arguments){
        //Student LMS user
        require_once('custom/include/utils/lms_helpers.php');
        $lms_config = getLMSConfig();
        if(isset($lms_config->token)){
            //Enroll tạo tài khoản luôn
            if(empty($bean->lms_user_id) && !empty($bean->email1)) {
                $result = createUser($bean);
                if($result['status'] == 'success'){
                    $bean->lms_user_id       = $result['user_id'];
                    $bean->lms_user_name     = $result['user_name'];
                    $bean->lms_user_password = $result['user_password'];
                    $bean->lms_content       = json_encode($result['user']);
                }else $bean->lms_user_id = $bean->lms_user_password = $bean->lms_user_id = $bean->lms_content ='';
            }

            //Chỉnh sửa hồ sơ học viên
            if(!empty($bean->fetched_row) && !empty($bean->lms_user_id)){
                //Thay đổi Email
                if(trim(strtolower($bean->lms_user_name)) != trim(strtolower($bean->email1)) && !empty(trim(strtolower($bean->email1)))){
                    $result = updateUserLogin($bean);
                }

                if($bean->lms_reset_password){
                    //Thay đổi mật khẩu
                    if($bean->lms_generate_password == 'defaut' || empty($bean->lms_generate_password)){
                        $password =  $GLOBALS['db']->getOne("SELECT IFNULL(value, '') value FROM config WHERE name = 'defaultteacherpwd' AND category = 'pwd'");
                        if(empty($password)) $password = strtolower(str_replace(' ', '', $dotb_config['brand_name'])).'123';
                    }

                    if($bean->lms_generate_password == 'auto')
                        $password = User::generatePassword();

                    if($bean->lms_generate_password == 'manual')
                        $password = $_POST['user_passwordd'];

                    $bean->lms_user_password = $password;
                    $result = updateUserLogin($bean,'password');

                    $bean->lms_generate_password = 'defaut';
                    $bean->lms_reset_password = 0;
                }
                if($bean->fetched_row['full_teacher_name'] != $bean->full_teacher_name){
                    //Update Name
                    $res = updateUser($bean,'name');
                }

                if($result['status'] == 'success'){
                    $bean->lms_user_id       = $result['user_id'];
                    $bean->lms_user_name     = $result['user_name'];
                    $bean->lms_user_password = $result['user_password'];
                    $bean->lms_content       = json_encode($result['user']);
                }
            }
        }
    }
}
?>
