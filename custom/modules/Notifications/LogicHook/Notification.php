<?php


/**
* This class contains logic hooks related to the Addoptify Customer Insight plugin for the leads module
*
* @author Emil Kilhage <emil.kilhage@addoptify.com>
*/
class LogicHook_Notification
{

    public function deleteNotification($bean, $event, $arguments){
        if (!empty($bean->id)) $GLOBALS['db']->query("DELETE FROM notifications WHERE parent_id = '{$bean->id}' AND parent_type = '{$bean->module_name}'");
    }

    public function updateNotification($bean, $event, $arguments)
    {
        global $current_user;
        if ((!empty($bean->id)) && ($bean->assigned_user_id == $current_user->id))
            $GLOBALS['db']->query("UPDATE notifications SET is_read = 1 WHERE parent_type='{$bean->module_name}' and parent_id='{$bean->id}' AND deleted = 0 AND is_read = 0");

    }
}
