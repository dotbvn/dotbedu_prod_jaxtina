<?php
  $viewdefs['C_Siblings']['base']['view']['panel-top-for-sibling'] = array(
    'type' => 'panel-top',
    'template' => 'panel-top',

    'buttons' => array(
        array(
            'type' => 'button',
            'name' => 'create_button',
            'icon' => 'fa-plus',
            'css_class' => 'btn',
            'label'=> 'LBL_CREATE_SIBLINGS',
            'acl_module' => 'C_Siblings',
            'acl_action' => 'create',
        ),

    ),

);
?>
