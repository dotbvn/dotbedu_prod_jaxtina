<?php
// created: 2022-10-19 16:54:38
$viewdefs['C_Siblings']['base']['view']['subpanel-for-prospects-link_primary_siblings'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                0 =>
                array (
                    'label' => 'LBL_NAME',
                    'enabled' => true,
                    'default' => true,
                    'name' => 'name',
                    'link' => true,
                ),
                1 =>
                array (
                    'name' => 'type',
                    'label' => 'LBL_TYPE',
                    'enabled' => true,
                    'default' => true,
                ),
                2 =>
                array (
                    'name' => 'parent_name',
                    'label' => 'LBL_B_PARENT_NAME',
                    'enabled' => true,
                    'id' => 'PARENT_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                3 =>
                array (
                    'name' => 'is_referrer',
                    'label' => 'LBL_IS_REFERRER',
                    'enabled' => true,
                    'default' => true,
                ),
                4 =>
                array (
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                5 =>
                array (
                    'label' => 'LBL_DATE_MODIFIED',
                    'enabled' => true,
                    'default' => true,
                    'name' => 'date_modified',
                ),
            ),
        ),
    ),
    'orderBy' =>
    array (
        'field' => 'date_modified',
        'direction' => 'desc',
    ),
    'rowactions' =>
    array (
        'actions' =>
        array (
            0 =>
            array (
                'type' => 'fieldset',
                'name' => 'main_dropdown',
                'primary' => false,
                'showOn' => 'view',
                'fields' =>
                array (
                    0 =>
                    array (
                        'type' => 'unlink-action',
                        'icon' => 'fa-trash-alt',
                        'tooltip' => 'LBL_DELETE_BUTTON',
                        'css_class' => 'btn btn-danger',
                        'name' => 'select_button',
                        'label' => 'LBL_DELETE_BUTTON',
                    ),
                ),
            ),
        ),
    ),
    'type' => 'subpanel-list',
);