
({
    extendsFrom: 'PanelTopView',
    /**
    * @inheritdoc
    */
    initialize: function(options) {
        this._super('initialize', [options]);
    },
    /**
    * Event handler for the create button.
    *
    * @param {Event} event The click event.
    */
    createRelatedClicked: function(event, params) {
        var self = this;
        var parentModel = this.context.parent.get('model');
        var link = this.context.get('link');
        var model = this.createLinkModel(parentModel, link);

        //Set defaut
        model.set('parent_id', '');
        model.set('parent_name', '');

        app.drawer.open({
            layout: 'create',
            context: {
                create: true,
                module: model.module,
                model: model
            }
            }, function(context, model) {
                if (!model) {
                    return;
                }
                self.trigger('linked-model:create', model);
        })
    },
})





