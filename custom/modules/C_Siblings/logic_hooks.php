<?php
    // Do not store anything in this file that is not part of the array or the hook version.  This file will
    // be automatically rebuilt in the future.
    $hook_version = 1;
    $hook_array = Array();
    $hook_array['before_save']      = Array();
    $hook_array['before_save'][]    = Array(3, 'Create Code', 'custom/modules/C_Siblings/logicSiblings.php','logicSiblings', 'addCode');

    $hook_array['before_relationship_delete'] = Array();
    $hook_array['before_relationship_delete'][] = Array(1, 'Remove Relationship', 'custom/modules/C_Siblings/logicSiblings.php','logicSiblings', 'removeRelationship');