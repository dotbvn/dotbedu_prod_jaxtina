<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicSiblings{
    function addCode(&$bean, $event, $arguments){
        $code_field = 'name';
        if (empty($bean->$code_field)) {
            //Get Prefix
            $res = $GLOBALS['db']->query("SELECT IFNULL(first_name,'no_name') first_name FROM contacts WHERE id = '{$bean->student_id}'");
            $row = $GLOBALS['db']->fetchByAssoc($res);
            $new_code = str_replace(' ', '', $row['first_name']).date('y').self::generateRandomString(3);
            $new_code .= $num;
            //write to database - Logic: Before Save
            $bean->$code_field = $new_code;
            $bean->$code_field = strtoupper(viToEn($bean->$code_field));
        }
        $bean->team_set_id = 1;
        $bean->team_id = 1;
        if($bean->student_id == $bean->parent_id){
            $bean->skip_link = true;
            $bean->deleted = 1;
        }

        if(empty($bean->fetched_row) && !isset($bean->skip_link)){
            //DELETE old relationship
            $GLOBALS['db']->query("DELETE FROM c_siblings WHERE student_id='{$bean->student_id}' AND parent_id = '{$bean->parent_id}'");
            $GLOBALS['db']->query("DELETE FROM c_siblings WHERE parent_id='{$bean->student_id}' AND student_id = '{$bean->parent_id}'");
            $sib = new C_Siblings();
            $sib->skip_link  = true;
            $sib->parent_id  = $bean->student_id;
            if (strpos($_REQUEST['__dotb_url'], 'Leads') !== false)
                $sib->parent_type = 'Leads';
            elseif (strpos($_REQUEST['__dotb_url'], 'Prospects') !== false)
                $sib->parent_type = 'Prospects';
            elseif (strpos($_REQUEST['__dotb_url'], 'Contacts') !== false)
                $sib->parent_type = 'Contacts';
            $sib->student_id = $bean->parent_id;
            $sib->type       = $bean->type;
            if($bean->is_referrer) $sib->is_referrer = 0;
            $sib->description= $bean->description;
            $sib->save();


        }

    }

    function removeRelationship(&$bean, $event, $arg){
        //DELETE old relationship
        $GLOBALS['db']->query("DELETE FROM c_siblings WHERE student_id='{$arg['related_id']}' AND parent_id = '{$bean->parent_id}'");
        $GLOBALS['db']->query("DELETE FROM c_siblings WHERE parent_id='{$arg['related_id']}' AND student_id = '{$bean->parent_id}'");
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

?>