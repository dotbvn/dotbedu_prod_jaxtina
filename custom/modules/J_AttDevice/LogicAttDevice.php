<?php
class logicAttDevice {
    public function HandleBeforeSave($bean, $event, $arguments){
        global $dotb_config;
        $bean->name = $bean->device_id;
        if($bean->fetched_row['team_id'] != $bean->team_id){
            $bean->synced_number_students = 0;
        }
        if($bean->fetched_row['device_id'] != $bean->device_id){
            $site = rtrim($dotb_config['site_url'], '/');
            $curl1 = curl_init();
            curl_setopt_array($curl1, array(
                CURLOPT_URL => 'https://socket.dotb.cloud/set-phenikaa-device?token=dotb',
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode(array(
                    'customer' => $site.'/rest/v11_3/phenikaa/webhook',
                    'name' => $bean->device_id
                )),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                ),
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
            ));
            curl_exec($curl1);
            curl_close($curl1);
        }
        if($bean->fetched_row['device_volume'] != $bean->device_volume){
            $device_volume = 100;
            if(!empty($bean->device_volume)){
                $device_volume = $bean->device_volume;
            }
            $curl3 = curl_init();
            curl_setopt_array($curl3, array(
                CURLOPT_URL => 'http://cam-api.bsmart.city/v2/cam/action/'.$bean->device_id.'/soundcfg',
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode(array(
                    'VerifySuccAudio' => 1,
                    'VerifyFailAudio' => 1,
                    'Volume' => $device_volume,
                )),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'SecretKey: '.$bean->secret_key
                )
            ));
            curl_exec($curl3);
            curl_close($curl3);
        }
    }

    public function HandleDelete($bean, $event, $arguments){
        global $dotb_config;
        $check_count = strlen($dotb_config['site_url']);
        $site = str_replace('https://', '',$dotb_config['site_url']);
        if($check_count == strlen($site)){
            $site = str_replace('http://', '',$dotb_config['site_url']);
        }
        $curl1 = curl_init();
        curl_setopt_array($curl1, array(
            CURLOPT_URL => 'https://socket.dotb.cloud/delete-phenikaa-device?token=dotb',
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode(array(
                'customer' => $site.'/rest/v11_3/phenikaa/webhook',
                'name' => $bean->device_id
            )),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));
        curl_exec($curl1);
        curl_close($curl1);

        $q = "SELECT l1.id, l1.frc_device_id_list FROM contacts l1 WHERE l1.frc_device_id_list LIKE '%{$bean->id}%' AND deleted = 0";
        $result = $GLOBALS['db']->fetchArray($q);
        $id_list = array();
        $index = 0;
        $lenght_result = count($result);
        foreach($result as $student){
            $id_list[$index] = $student['id'];
            $student_list[$student['id']] = $student['frc_device_id_list'];
            if(($index % 200 == 0 && $index != 0) || $index == $lenght_result - 1){
                $curl3 = curl_init();
                curl_setopt_array($curl3, array(
                    CURLOPT_URL => 'http://cam-api.bsmart.city/v2/cam/persons/'.$bean->device_id,
                    CURLOPT_CUSTOMREQUEST => 'DELETE',
                    CURLOPT_POSTFIELDS => json_encode($id_list),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'SecretKey: '.$bean->secret_key
                    )
                ));
                $respone = curl_exec($curl3);
                if ($respone == true) {
                    $http_info = curl_getinfo($curl3, CURLINFO_HTTP_CODE);
                    if ($http_info == 200) {
                        $result = json_decode($respone, true);
                        foreach ($result['info']['DelSucInfo'] as $item){
                            if(in_array($item['customId'], $id_list)){
                                $device_id_list = unencodeMultienum($student_list[$item['customId']]);
                                $key = array_search($bean->id, $device_id_list);
                                if($key !== false){
                                    unset($device_id_list[$key]);
                                }
                                $device_id_list = decodeMultienum($device_id_list);
                                $q1 = "UPDATE contacts SET frc_device_id_list = '{$device_id_list}' 
                                        WHERE id = '{$item['customId']}'";
                                $GLOBALS['db']->query($q1);
                            }
                        }
                    }
                }
                curl_close($curl3);
                $id_list = array();
            }
            $index++;
        }

    }
    public function HandleAfterRetrieve($bean){
        $q = "SELECT COUNT(l1.id) FROM contacts l1 INNER JOIN team_sets_teams l2 ON  l1.team_set_id = l2.team_set_id WHERE l1.team_id = '{$bean->team_id}' AND l1.deleted = 0";
        $total = $GLOBALS['db']->getOne($q);
        $bean->synced_number_students_display = '<span>'.$bean->synced_number_students .' / '.$total.'&nbsp&nbsp&nbsp'.'<a style = "font-size: 12px" onclick="window.parent.DOTB.App.router.navigate(\'#Contacts?filterDevices='.$bean->id.'&filterTeams='.$bean->team_id.'\', {trigger: true})">'. $GLOBALS['app_strings']['LBL_LIST_UNREGISTER'].'</a>'.'</span>';

    }

}