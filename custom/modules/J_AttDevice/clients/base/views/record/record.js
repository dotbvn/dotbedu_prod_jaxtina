({
    extendsFrom: 'RecordView',

    initialize: function (options) {
        this._super('initialize', [options])
        this.context.on('button:sync_face_id:click', this.syncFaceID,this);
    },

    syncFaceID: function(){
        app.alert.show('message-id', {
            level: 'process',
            title: 'In Process...'
        });
        console.log(App.api.buildURL('attdevice/sync_faceid'));
        app.api.call("update", App.api.buildURL('attdevice/sync_faceid'), {id:this.model.id}, {
            success: function (data) {
                app.alert.show('message-id', {
                    level: 'success',
                    messages: 'Successfull',
                    autoClose: true
                });
                location.reload(true);
            },
            error:function (data) {
                app.alert.dismiss('alert-id');
            }
        });
    },

})
