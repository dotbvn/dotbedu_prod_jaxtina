<?php


$module_name = 'J_AttDevice';
$viewdefs[$module_name]['base']['view']['list'] = array(
    'panels' => array(
        array(
            'label' => 'LBL_PANEL_1',
            'fields' => array(
                array(
                    'name' => 'name',
                    'label' => 'LBL_DEVICE_ID',
                    'default' => true,
                    'enabled' => true,
                    'link' => true,
                ),
                array (
                    'name' => 'device_type',
                    'label' => 'LBL_DEVICE_TYPE'
                ),
                array(
                    'name' => 'team_id',
                    'label' => 'LBL_TEAM',
                ),
                array(
                    'name' => 'device_volume',
                    'label' => 'LBL_DEVICE_VOLUME',
                ),
                array(
                    'name' => 'date_modified',
                    'enabled' => true,
                    'default' => true,
                ),
            ),
        ),
    ),
    'orderBy' => array(
        'field' => 'date_modified',
        'direction' => 'desc',
    ),
);
