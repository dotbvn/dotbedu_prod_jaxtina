<?php


class AttDeviceAPI extends DotbApi
{
    function registerApiRest()
    {
        return array(
            'sync_faceid' => array(
                'reqType' => 'PUT',
                'path' => array('attdevice', 'sync_faceid'),
                'pathVars' => array(),
                'method' => 'syncFaceID'
            )
        );
    }

    /**
     * @param ServiceBase $api
     * @param array $args : dotb_ext, dotb_phone, dotb_call_status,dotb_direction
     * @return |null
     */

    function syncFaceID(ServiceBase $api, array $args)
    {
        global $dotb_config;
        $check_count = strlen($dotb_config['site_url']);
        $site = str_replace('https://', '',$dotb_config['site_url']);
        if($check_count == strlen($site)){
            $site =str_replace('http://', '',$dotb_config['site_url']);
        }
        $bean = BeanFactory::getBean('J_AttDevice', $args['id']);
        $q = "SELECT
                IFNULL(l.id,'') id,
                IFNULL(l.picture, '') avt,
                IFNULL(l.full_student_name,'') full_student_name,
                IFNULL(l.frc_device_id_list,'') frc_device_id_list
            FROM contacts l
            INNER JOIN team_sets_teams  l1 ON l.team_set_id = l1.team_set_id AND l1.team_id = '{$bean->team_id}'
            WHERE l.picture <> ''
              AND l.deleted = 0
              AND l.frc_device_id_list NOT LIKE '%{$bean->id}%'";
        $result = $GLOBALS['db']->fetchArray($q);
        $url = 'http://cam-api.bsmart.city/v2/cam/person/' . $bean->device_id;
        $uploadFile = new UploadFile();
        foreach($result as $student){
            $img_file = 'http://cam-api.bsmart.city/v2/cam/proxy/' . $site . '/upload/origin/' . $student['avt'];
            $respone = RegisterFaceID($student, $url, $img_file, 'POST', $bean->secret_key, $bean->id);
            if($respone['success'] == 1){
                $pic_id = create_guid();
                $uploadFile->duplicate_file('origin/'.$student['avt'],'FaceID/RegisterImage/' .$pic_id);
                $frc_device_id_list = (empty($student['frc_device_id_list']) || $student['frc_device_id_list'] == '0' ) ? '^'.$bean->id.'^' : $student['frc_device_id_list'].','.'^'.$bean->id.'^';
                $q1 = "UPDATE contacts SET frc_device_id_list = '{$frc_device_id_list}', frc_face_id_image = '{$pic_id}' 
                        WHERE id = '{$student['id']}'";
                $GLOBALS['db']->query($q1);
            }
        }
        $bean->retrieve($args['id']);
        $bean->save();
    }


}