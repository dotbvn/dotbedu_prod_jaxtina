<?php

$hook_version = 1;
$hook_array = array();
$hook_array['before_save'] = array();
$hook_array['before_save'][] = array(0, 'Logic Attendance Device', 'custom/modules/J_AttDevice/LogicAttDevice.php', 'logicAttDevice', 'HandleBeforeSave');

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(0, 'Check Api Logs', 'custom/modules/J_AttDevice/LogicAttDevice.php', 'logicAttDevice', 'HandleDelete');

$hook_array['after_retrieve'] = Array();
$hook_array['after_retrieve'][] = Array(0, 'Handle After Retrieve', 'custom/modules/J_AttDevice/LogicAttDevice.php', 'logicAttDevice', 'HandleAfterRetrieve');
