<?php

$viewdefs['RevenueLineItems']['base']['view']['record'] = array(
  'buttons' =>
  array(
    array(
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'tooltip' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn ',
      'icon' => 'fa-window-close',
      'showOn' => 'edit',
      'events' => array(
        'click' => 'button:cancel_button:click',
      ),
    ),
    array(
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'tooltip' => 'LBL_SAVE_BUTTON_LABEL',
      'icon' => 'fa-save',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    array(
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' =>
      array(
        array(
          'type' => 'rowaction',
          'event' => 'button:edit_button:click',
          'name' => 'edit_button',
          'tooltip' => 'LBL_EDIT_BUTTON_LABEL',
          'label' => 'LBL_EDIT_BUTTON_LABEL',
          'icon' => 'fa-pencil',
          'primary' => true,
          'acl_action' => 'edit',
        ),
        array(
          'type' => 'shareaction',
          'name' => 'share',
          'label' => 'LBL_RECORD_SHARE_BUTTON',
          'acl_action' => 'view',
        ),
        array(
          'type' => 'pdfaction',
          'name' => 'download-pdf',
          'label' => 'LBL_PDF_VIEW',
          'action' => 'download',
          'acl_action' => 'view',
        ),
        array(
          'type' => 'pdfaction',
          'name' => 'email-pdf',
          'label' => 'LBL_PDF_EMAIL',
          'action' => 'email',
          'acl_action' => 'view',
        ),
        array(
          'type' => 'divider',
        ),
        array(
          'type' => 'convert-to-quote',
          'event' => 'button:convert_to_quote:click',
          'name' => 'convert_to_quote_button',
          'label' => 'LBL_CONVERT_TO_QUOTE',
          'acl_module' => 'Quotes',
          'acl_action' => 'create',
        ),
        array(
          'type' => 'divider',
        ),
        array(
          'type' => 'rowaction',
          'event' => 'button:find_duplicates_button:click',
          'name' => 'find_duplicates_button',
          'label' => 'LBL_DUP_MERGE',
          'acl_action' => 'edit',
        ),
        array(
          'type' => 'rowaction',
          'event' => 'button:duplicate_button:click',
          'name' => 'duplicate_button',
          'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
          'acl_module' => 'RevenueLineItems',
          'acl_action' => 'create',
        ),
        array(
          'type' => 'rowaction',
          'event' => 'button:historical_summary_button:click',
          'name' => 'historical_summary_button',
          'label' => 'LBL_HISTORICAL_SUMMARY',
          'acl_action' => 'view',
        ),
        array(
          'type' => 'rowaction',
          'event' => 'button:audit_button:click',
          'name' => 'audit_button',
          'label' => 'LNK_VIEW_CHANGE_LOG',
          'acl_action' => 'view',
        ),
        array(
          'type' => 'divider',
        ),
        array(
          'type' => 'rowaction',
          'event' => 'button:delete_button:click',
          'name' => 'delete_button',
          'label' => 'LBL_DELETE_BUTTON_LABEL',
          'acl_action' => 'delete',
        ),
      ),
    ),
    array(
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'panels' =>
  array(
    0 =>
    array(
      'name' => 'panel_header',
      'header' => true,
      'fields' => array(
        array(
          'name'          => 'picture',
          'type'          => 'avatar',
          'size'          => 'large',
          'dismiss_label' => true,
          'readonly'      => true,
        ),
        array(
          'name' => 'name',
          'label' => 'LBL_MODULE_NAME_SINGULAR'
        ),
        array(
          'name' => 'favorite',
          'label' => 'LBL_FAVORITE',
          'type' => 'favorite',
          'dismiss_label' => true,
        ),
        array(
          'name' => 'follow',
          'label' => 'LBL_FOLLOW',
          'type' => 'follow',
          'readonly' => true,
          'dismiss_label' => true,
        ),
        array(
          'type' => 'badge',
          'name' => 'quote_id',
          'event' => 'button:convert_to_quote:click',
          'readonly' => true,
          'tooltip' => 'LBL_CONVERT_RLI_TO_QUOTE',
          'acl_module' => 'RevenueLineItems',
        ),
      ),
    ),
    1 =>
    array(
      'name' => 'panel_body',
      'label' => 'LBL_RECORD_BODY',
      'columns' => 2,
      'labels' => true,
      'labelsOnTop' => true,
      'placeholders' => true,
      'fields' =>
      array(
        0 =>
        array(
          'name' => 'opportunity_name',
          'filter_relate' =>
          array(
              'parent_id' => 'parent_id',
          ),
        ),
        1 => 'product_template_name',
        2 =>
        array(
            'name' => 'parent_name',
            'readonly' => true,
        ),
        3 =>
        array(
          'name' => 'category_name',
          'type' => 'relate',
          'label' => 'LBL_CATEGORY',
        ),
        4 => 'sales_stage',
        5 => 'quantity',
        6 => 'commit_stage',
        7 =>
        array(
          'name' => 'list_price',
          'readonly' => true,
          'type' => 'currency',
          'related_fields' =>
          array(
              'list_price',
              'currency_id',
              'base_rate',
          ),
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'currency_field' => 'currency_id',
          'base_rate_field' => 'base_rate',
        ),
        8 => 'probability',
        9 =>
        array(
          'name' => 'discount_amount',
          'type' => 'currency',
          'related_fields' => array(
              'discount_amount',
              'currency_id',
              'base_rate',
          ),
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'currency_field' => 'currency_id',
          'base_rate_field' => 'base_rate',
        ),
        10 =>
        array(
          'name' => 'date_closed',
          'related_fields' => array(
              'date_closed_timestamp'
          )
        ),
        11 =>
        array(
          'name' => 'discount_price',
          'type' => 'currency',
          'related_fields' =>
          array(
            'discount_price',
            'currency_id',
            'base_rate',
          ),
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'currency_field' => 'currency_id',
          'base_rate_field' => 'base_rate',
        ),
        12 =>
        array(
          'name' => 'total_amount',
          'type' => 'currency',
          'label' => 'LBL_CALCULATED_LINE_ITEM_AMOUNT',
          'readonly' => true,
          'related_fields' => array(
              'total_amount',
              'currency_id',
              'base_rate',
          ),
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'currency_field' => 'currency_id',
          'base_rate_field' => 'base_rate',
        ),
        13 =>
        array(
          'name' => 'likely_case',
          'type' => 'currency',
          'related_fields' => array(
              'likely_case',
              'currency_id',
              'base_rate',
          ),
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'currency_field' => 'currency_id',
          'base_rate_field' => 'base_rate',
        ),
        14 =>
        array(
          'name' => 'quote_name',
          'label' => 'LBL_ASSOCIATED_QUOTE',
          'related_fields' => array('quote_id'),
          // this is a hack to get the quote_id field loaded
          'readonly' => true,
          'related_fields' => array(
              'mft_part_num',
          ),
        ),
        15 =>
        array(
          'name' => 'best_case',
          'type' => 'currency',
          'related_fields' =>
          array(
              'best_case',
              'currency_id',
              'base_rate',
          ),
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'currency_field' => 'currency_id',
          'base_rate_field' => 'base_rate',
        ),
        16 => 'description',
        17 =>
        array(
          'name' => 'worst_case',
          'type' => 'currency',
          'related_fields' => array(
              'worst_case',
              'currency_id',
              'base_rate',
          ),
          'convertToBase' => true,
          'showTransactionalAmount' => true,
          'currency_field' => 'currency_id',
          'base_rate_field' => 'base_rate',
        ),
      ),
    ),
    array(
        'name' => 'panel_hidden',
        'label' => 'LBL_RECORD_SHOWMORE',
        'hide' => true,
        'columns' => 2,
        'labelsOnTop' => true,
        'placeholders' => true,
        'fields' => array(
            0 => 'lead_source',
            1 => 'campaign_name',
            2 => 'next_step',
            3 => 'tag',
            4 => 'product_type',
            5 => 'tax_class',
            6 => 'team_name',
            7 =>'assigned_user_name',
            8 =>
            array(
                'name' => 'date_entered_by',
                'readonly' => true,
                'type' => 'fieldset',
                'inline' => true,
                'label' => 'LBL_DATE_ENTERED',
                'fields' => array(
                    array(
                        'name' => 'date_entered',
                    ),
                    array(
                        'type' => 'label',
                        'default_value' => 'LBL_BY',
                    ),
                    array(
                        'name' => 'created_by_name',
                    ),
                ),
            ),
            9 =>
            array(
                'name' => 'date_modified_by',
                'readonly' => true,
                'type' => 'fieldset',
                'inline' => true,
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => array(
                    array(
                        'name' => 'date_modified',
                    ),
                    array(
                        'type' => 'label',
                        'default_value' => 'LBL_BY',
                    ),
                    array(
                        'name' => 'modified_by_name',
                    ),
                ),
            ),
        ),
    ),
  ),
);
