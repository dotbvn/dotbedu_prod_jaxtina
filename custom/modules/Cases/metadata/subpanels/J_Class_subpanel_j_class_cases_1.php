<?php
// created: 2019-04-18 19:01:53
$subpanel_layout['list_fields'] = array (
  'case_number' =>
  array (
    'vname' => 'LBL_LIST_NUMBER',
    'width' => 10,
    'default' => true,
  ),
  'name' =>
  array (
    'vname' => 'LBL_LIST_SUBJECT',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'status' =>
  array (
    'vname' => 'LBL_LIST_STATUS',
    'width' => 10,
    'default' => true,
  ),
  'j_class_cases_1_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_J_CLASS_CASES_1_FROM_J_CLASS_TITLE',
    'id' => 'J_CLASS_CASES_1J_CLASS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'J_Class',
    'target_record_key' => 'j_class_cases_1j_class_ida',
  ),
  'contacts_cases_1_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_CASES_1CONTACTS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'contacts_cases_1contacts_ida',
  ),
  'description' =>
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'date_entered' =>
  array (
    'vname' => 'LBL_LIST_DATE_CREATED',
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' =>
  array (
    'name' => 'assigned_user_name',
    'vname' => 'LBL_LIST_ASSIGNED_TO_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'target_record_key' => 'assigned_user_id',
    'target_module' => 'Employees',
    'width' => 10,
    'default' => true,
  ),
);