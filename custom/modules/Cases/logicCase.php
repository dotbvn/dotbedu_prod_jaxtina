<?php
class logicCase{

    function handleStatusColor(&$bean, $event, $arguments){
        //handle Rate
        $rate = $bean->rate;
        if($rate>0 && $rate<6){
            $bean->rate ='<span>';
            for($i=0; $i<$rate; $i++) $bean->rate .= '<i class="fas fa-star" style="color: orange;margin: 0 0px"></i>';
            $bean->rate ='</span>';
        }
        //END: handle Rate
        switch ($bean->status) {
            case 'New':
                $colorClass = "textbg_green";
                break;
            case 'Assigned':
                $colorClass = "textbg_bluelight";
                break;
            case 'Overdue':
                $colorClass = "textbg_red";
                break;
            case 'Closed':
                $colorClass = "No_color";
                break;
            default :
                $colorClass = "No_color";
        }
        $bean->status = "<span class='label ellipsis_inline ".$colorClass." '>".translate('case_status_dom', '', $bean->status)."</span>";
        //Handle Listview Case - kiểm tra list view Case
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[sizeof($arrayUrl) - 1];
        }
        global $current_user;
        if(($_REQUEST['view'] == 'list'
        && $url_module == $bean->module_name) || $GLOBALS['current_user']->user_name = 'DotbCustomerSupportPortalUser'){
            $sql_count = "SELECT DISTINCT count(cc.id) count_
            FROM c_comments cc INNER JOIN cases l1 ON l1.id = cc.parent_id AND cc.parent_type = 'Cases' AND l1.deleted = 0
            WHERE (((l1.id = '$case_id'))) AND cc.deleted = 0";
            $count_case = $GLOBALS['db']->getOne($sql_count);

            $bean->name = '<div><a href="#Cases/'.$bean->id.'">'.$bean->name.'</a>';
            $case_id = $bean->id;
            $sql = "SELECT DISTINCT IFNULL(cc.id, '') primaryId,
            IFNULL(cc.date_entered, '') date_entered,
            IFNULL(cc.description, '') description,
            IFNULL(cc.direction, '') direction,
            IFNULL(l5.user_name, '') created_by,
            IFNULL(l3.full_student_name, '') student_name,
            IFNULL(cc.is_read_inems, 0) is_read_inems
            FROM c_comments cc
            INNER JOIN cases l1 ON l1.id = cc.parent_id AND cc.parent_type = 'Cases'
            LEFT JOIN contacts_cases_1_c l3_1 ON l1.id = l3_1.contacts_cases_1cases_idb AND l3_1.deleted = 0
            LEFT JOIN contacts l3 ON l3.id = l3_1.contacts_cases_1contacts_ida AND l3.deleted = 0
            LEFT JOIN users l5 ON cc.created_by = l5.id AND l5.deleted = 0
            WHERE (((l1.id = '$case_id'))) AND cc.is_unsent = 0 AND cc.deleted = 0
            ORDER BY cc.date_entered DESC limit 1";
            $row = $GLOBALS['db']->fetchOne($sql);


            if(!empty($row)) {
                if($row['direction'] == 'outbound'){
                    $class = "text-info";
                    $chatter = $row['created_by'];
                }elseif($row['direction'] == 'inbound'){
                    $class = "purple-text";
                    $chatter = $row['student_name'];
                }

                $content = $this->get_excerpt($row['description'],0,55);
                $bean->name .='<br><span style="background: #b280ff; margin-right: 10px" class="badge"><i class="far fa-comments" style="color: white;margin-right: 5px"></i><font style="vertical-align: inherit;">'.$count_case.'</font></span>
                <strong style="font-size: 11px;" class="'.$class.'">@'.$chatter.':</strong>
                <span rel="tooltip" data-original-title="'.$row['description'].'">'.$content.'</span>';
                $bean->name .=' <i rel="tooltip" style="float:right;color: #606060;" data-original-title="'.$GLOBALS['timedate']->to_display_date_time($row['date_entered']).'">'.$GLOBALS['timedate']->time_Ago($row['date_entered']);
                if(!$row['is_read_inems']) $bean->name .= '<i class="unread-notification fa fa-circle" style="font-size: 5px;vertical-align: middle;padding-left: 5px;"></i>';
                $bean->name .= '</i>';

            }
            $bean->name .='</div>';
        }
    }

    function handleBfSave(&$bean, $event, $arguments){
        //update cso Case -  Class
        //Custom logic for portal
        if (isset($_SESSION['type']) && $_SESSION['type'] == 'support_portal') {
            $bean->portal_viewable = 1;
            $bean->source = 'Support Portal';
            $bean->j_class_cases_1->add($bean->related_class);
            $bean->j_class_cases_1j_class_ida = $bean->related_class;
        }
        $class_id = $bean->j_class_cases_1j_class_ida;
        if(!empty($class_id)){
            $class = BeanFactory::getBean('J_Class',$class_id);
            if(!empty($class->cso_id) && empty($bean->cso_id))
                $bean->cso_id = $class->cso_id;
        }
    }
    function handleAfterRetrieve(&$bean, $event, $arguments){
        $bean->load_relationship('j_class_cases_1');
        $related_class = $bean->j_class_cases_1->getBeans();
        foreach($related_class as $key => $value){
            $bean->related_class = $key;
        }
    }
    function get_excerpt( $string, $start_postion = 0, $max_length = 100 ) {
        if ( strlen( $string ) <= $max_length ) {
            return $string;
        }
        $excerpt    = substr( $string, $start_postion, $max_length - 3 );
        $last_space = strrpos( $excerpt, ' ' );
        $excerpt    = substr( $excerpt, 0, $last_space );
        $excerpt .= '...';

        return $excerpt;
    }
}
