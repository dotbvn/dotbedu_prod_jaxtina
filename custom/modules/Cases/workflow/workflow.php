<?php

use Dotbcrm\Dotbcrm\Util\Arrays\ArrayFunctions\ArrayFunctions;
include_once("include/workflow/alert_utils.php");
include_once("include/workflow/action_utils.php");
include_once("include/workflow/time_utils.php");
include_once("include/workflow/trigger_utils.php");
//BEGIN WFLOW PLUGINS
include_once("include/workflow/custom_utils.php");
//END WFLOW PLUGINS
	class Cases_workflow {
	function process_wflow_triggers(& $focus){
		include("custom/modules/Cases/workflow/triggers_array.php");
		include("custom/modules/Cases/workflow/alerts_array.php");
		include("custom/modules/Cases/workflow/actions_array.php");
		include("custom/modules/Cases/workflow/plugins_array.php");
		
 if(true){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 

	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['e77adc6e_fb6c_11ed_b323_067482011a2a'])){
		$triggeredWorkflows['e77adc6e_fb6c_11ed_b323_067482011a2a'] = true;
		 $alertshell_array = array(); 

	 $alertshell_array['alert_msg'] = "56006e96-96ff-11e9-94c3-005056b6e651"; 

	 $alertshell_array['source_type'] = "Custom Template"; 

	 $alertshell_array['alert_type'] = "Email"; 

	 process_workflow_alerts($focus, $alert_meta_array['Cases0_alert0'], $alertshell_array, false); 
 	 unset($alertshell_array); 
		}
 

	 //End Frame Secondary 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 


	//end function process_wflow_triggers
	}

	//end class
	}

?>