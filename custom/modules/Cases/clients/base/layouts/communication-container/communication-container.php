<?php
$viewdefs['Cases']['base']['layout']['communication-container'] = array(
    'components' => array(
        array(
            'view' => 'contact-communication',
        ),
    ),
    'type' => 'communication-container',
    'span' => 12,
    'css_class' => 'communication-container row-fluid ninja'
);
