<?php

$viewdefs['Cases']['base']['layout']['communication'] = array(
    'components' => array(
        array(
            'view' => 'communication-header',
        ),
        array(
            'layout' => 'communication-container',
        ),
    ),
    'type' => 'communication',
    'span' => 12,
    'css_class' => 'communication row-fluid'
);
