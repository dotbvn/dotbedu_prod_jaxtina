/**
 * @author Exsitec AB
 */
({
    eventsInitialized: false,
    initialize: function (options) {
        this._super('initialize', [options]);
        this.eventsInitialized = false;
        Mousetrap.bind(['shift+alt+up'], function (e) {
            $('.communication-header').click();
        });

        this.layout.caseModel = app.data.createBean('Cases', {id: this.model.get('id')});
        this.layout.caseModel.fetch({
            success: _.bind(function () {
                this.layout.render();
            }, this)
        });
    },

    _render: function () {
        this._super('_render');

        if (!this.eventsInitialized && $('.communication-header').length > 0) {
            $('.communication-header').on('click', _.bind(function(){
                this.toggleCommunication();
            }, this));
            this.eventsInitialized = true;
            let commState = app.user.lastState.get('case-communication');
            if(!_.isUndefined(commState) && commState == 'open'){
                this.toggleCommunication(true);
            }
        }
    },

    toggleCommunication: function (forceOpen) {
        if (!$('.communication-container').is(':visible') || forceOpen) {
            $('.communication-container').slideDown('fast');
            app.user.lastState.set('case-communication', 'open');

            if (
                $('.user-communication').find('.messageArea').is(':visible')
                && $('.contact-communication').find('.messageArea').is(':visible')
            ) {
                //If both message areas are visible, make sure sides are 50% each
                $('.contact-communication').css('width', '50%');
                $('.user-communication').css('width', '50%');
            }

            $('.communication-header').find('.chevron').removeClass('sicon-chevron-down');
            $('.communication-header').find('.chevron').addClass('sicon-chevron-up');

            $('.user-communication .messageArea').scrollTop($('.user-communication .messageArea').prop("scrollHeight"));
            $('.contact-communication .messageArea').scrollTop($('.contact-communication .messageArea').prop("scrollHeight"));
        } else {
            $('.communication-container').slideUp('fast');
            $('.communication-header').find('.chevron').removeClass('sicon-chevron-up');
            $('.communication-header').find('.chevron').addClass('sicon-chevron-down');
            app.user.lastState.set('case-communication', 'closed');
        }
    }
})