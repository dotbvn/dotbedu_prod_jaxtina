/**
 * @author Exsitec AB
 */
({
    noteDummy: undefined,
    initialize: function (options) {
        this._super('initialize', [options]);

        this.initNoteDummy();

        Mousetrap.bind(['shift+alt+right'], function (e) {
            if ($('.communication-container').is(':visible')) {
                $('.user-communication').find('.sideToggle').click();
            }
        });

        Mousetrap.bind(['ctrl+alt+right'], _.bind(function (e) {
            this.saveNote();
        }, this));

        this.context.on('save-user-note', _.bind(function(){
            this.saveNote();
        }, this));

        this.context.on('saved-contact-note', _.bind(function () {
            if (!_.isEmpty(this.noteDummy.get('description')) || !_.isEmpty(this.noteDummy.get('filename'))) {
                this.layout.activeNote = this.noteDummy;
            } else {
                this.layout.activeNote = undefined;
            }
        }, this));
    },

    initDragDrop: function () {
        this.$(".fileInput").on('dragenter', _.bind(function (ev) {
            // Entering drop area. Highlight area
            this.$(".fileInput").addClass("highlightDropArea");
        },this));

        this.$(".fileInput").on('dragleave', _.bind(function (ev) {
            // Going out of drop area. Remove Highlight
            this.$(".fileInput").removeClass("highlightDropArea");
        }, this));

        this.$(".fileInput").on('drop', _.bind(function (ev) {
            // Dropping files
            ev.preventDefault();
            ev.stopPropagation();
            if (ev.originalEvent.dataTransfer) {
                if (ev.originalEvent.dataTransfer.files.length) {
                    let droppedFile = _.first(ev.originalEvent.dataTransfer.files);
                    let list = new DataTransfer();
                    list.items.add(droppedFile);
                    this.$('input[type="file"]')[0].files = list.files;
                    this.noteDummy.set('filename', droppedFile.name);
                }
            }

            this.$(".fileInput").removeClass("highlightDropArea");
            return false;
        }, this));

        this.$(".fileInput").on('dragover', _.bind(function (ev) {
            ev.preventDefault();
        }, this));
    },

    initNoteDummy: function () {
        this.noteDummy = app.data.createBean('C_Comments', {
            parent_id: this.layout.layout.caseModel.get('id'),
            parent_type: this.layout.layout.caseModel.module,
            // portal_flag: 0,
            // entry_source: 'internal',
            assigned_user_id: app.user.get('id'),
            created_by: app.user.get('id'),
            attachment_list: app.data.createBeanCollection('Notes'),
            team_id: 1,
            team_set_id: 1,
        });
        if (
            !_.isUndefined(app.config.Cases)
            && !_.isUndefined(app.config.Cases.internal_default_subject)
            && !_.isEmpty(app.config.Cases.internal_default_subject)
        ) {
            this.noteDummy.set('name', app.config.Cases.internal_default_subject);
        }
        this.noteDummy.fields.description.required = true;
        this.noteDummy.fields.description.type = 'textarea';

        this.noteDummy.fields.attachment_list.bIcon = '';
        this.noteDummy.fields.attachment_list.bLabel = 'Browse';

        this.noteDummy.fields.attachment_list.type = 'multi-attachments';
        this.noteDummy.fields.attachment_list.fields = ['name', 'filename', 'file_mime_type'];
        this.noteDummy.fields.attachment_list.related_fields = ['filename', 'file_mime_type'];
        this.noteDummy.fields.attachment_list.link = 'attachments';
        this.noteDummy.fields.attachment_list.module = 'Notes';
        this.noteDummy.fields.attachment_list.modulefield = 'filename';

        this.noteDummy.on('change:description', _.bind(function () {
            this.layout.activeNote = this.noteDummy;
        }, this));

        this.noteDummy.get('attachment_list').on('add remove', _.bind(function () {
            this.layout.activeNote = this.noteDummy;
        }, this));

        $(window).on('resize', _.bind(function () {
            if (!$('.contact-communication').find('.messageArea').is(':visible')) {
                let width = $("#content").prop("clientWidth") - $('.contact-communication').find('.sideToggle').outerWidth() - 17;
                $('.user-communication').css('width', width + 'px');
            }
        }, this));
    },

    _render: function () {
        this._super('_render');
        this.$el.find('.sideToggle').off('click', this.toggleMessageArea).on('click', this.toggleMessageArea);

        this.$el.find('.noteAttachment').off('click', this.downloadNote).on('click', this.downloadNote);

        this.$el.find('.sendButton').on('click', _.bind(this.saveNote, this));
        $('.user-communication .messageArea').scrollTop($('.user-communication .messageArea').prop("scrollHeight"));

        this.$el.find('.emailTemplate select').change(_.bind(this.insertTemplate, this));

        this.bindContentKeyCombos();
        this.initDragDrop();
        this.$el.css('width', '50%');

        this.$('.deleteNote').click(_.bind(this.deleteNote, this));
        this.$('.editNote').click(_.bind(this.editNote, this));
    },

    bindContentKeyCombos: function(){
        this.$el.find('.noteContent textarea').keydown(_.bind(function (e) {
            if (e.ctrlKey && e.altKey && e.which == '39') {
                //Ctrl + Alt + Arrow right to save right
                $(e.currentTarget).trigger('change');
                this.saveNote();
            } else if (e.ctrlKey && e.altKey && e.which == '37') {
                //Ctrl + Alt + Arrow left to save left
                this.layout.getComponent('contact-communication').saveNote();
            }
        }, this));
    },

    toggleMessageArea: function () {
        let messageArea = $('.user-communication').find('.messageArea');
        if ($(messageArea).is(':visible')) {
            $('.user-communication').animate(
                {width: '30px'},
                100,
                'linear',
                function () {
                    $('.user-communication').find('.messageArea').hide();
                }
            );

            if(!$('.contact-communication').find('.messageArea').is(':visible')){
                $('.contact-communication').find('.messageArea').show();
            }
            let width = $('.user-communication').parent().width() -  $('.user-communication').find('.sideToggle').outerWidth();
            $('.contact-communication').animate(
                {width: width + 'px'},
                100,
                'linear'
            );
            $('.contact-communication').find('.chevron').removeClass('sicon-chevron-right');
            $('.contact-communication').find('.chevron').addClass('sicon-chevron-left');

            $('.user-communication').find('.chevron').removeClass('sicon-chevron-right');
            $('.user-communication').find('.chevron').addClass('sicon-chevron-left');
        } else{
            $('.user-communication').find('.messageArea').show();
            $('.contact-communication').animate(
                {width: '50%'},
                100,
                'linear'
            );
            $('.user-communication').animate(
                {width: '50%'},
                100,
                'linear'
            );

            $('.user-communication').find('.chevron').removeClass('sicon-chevron-left');
            $('.user-communication').find('.chevron').addClass('sicon-chevron-right');
        }
    },

    insertTemplate: function (e) {
        let content = $(e.currentTarget).val();
        let subject = $(e.currentTarget).find(':selected').attr('subject')
        if (_.isEmpty(this.noteDummy.get('description'))) {
            this.noteDummy.set('description', content);
            this.noteDummy.set('name', subject);
        } else {
            app.alert.show('clear-and-insert', {
                level: 'confirmation',
                messages: 'This will overwrite the current content. Are you sure?',
                onConfirm: _.bind(function () {
                    this.context.noteDummy.set('description', this.content);
                    this.context.noteDummy.set('name', this.subject);
                }, {context: this, content: content, subject: subject})
            });
        }
    },

    downloadNote: function (e) {
        let noteId = $(e.currentTarget).data('id');
        let fileUrl = app.api.buildFileURL({module: 'Notes', id: noteId, field: 'filename'});
        $('#fileDownloader').attr('src', fileUrl);
    },

    saveNote: function () {
        this.noteDummy.doValidate(['name','description'], _.bind(function (isValid) {
            if (isValid) {
                app.alert.show('saving-note', {
                    level: 'process',
                    title: 'Sending'
                });
                this.noteDummy.save(null, {
                    success: _.bind(function () {
                        this.layout.layout.caseModel.fetch({
                            success: _.bind(function () {
                                app.alert.dismiss('saving-note');
                                this.initNoteDummy();
                                this.render();
                                this.model.getRelatedCollection('notes').fetch({relate: true});
                                this.context.trigger('saved-user-note');
                                this.refreshCase();
                            }, this)
                        });
                    }, this)
                });
            } else{
                //Must rebind field listeners after append error div
                _.delay(_.bind(function(){
                    this.bindContentKeyCombos();
                }, this), 100);
            }
        }, this));
    },

    deleteNote: function (e) {
        app.alert.show('delete-note-confirm', {
            level: 'confirmation',
            messages: 'Are you sure you want to delete this note?',
            onConfirm: _.bind(function () {
                app.alert.show('deleting-note', {
                    level: 'process',
                    title: 'Deleting'
                });
                app.data.createBean('Notes', {id: $(e.currentTarget).data('id')}).destroy({
                    success: _.bind(function () {
                        app.alert.dismiss('deleting-note');
                        this.refreshNotes();
                    }, this)
                });
            }, this)
        });
    },

    editNote: function (e) {
        app.data.createBean('Notes', {id: $(e.currentTarget).data('id')}).fetch({
            success: _.bind(function (model) {
                app.drawer.open({
                    layout: 'record',
                    context: {
                        action: 'edit',
                        module: 'Notes',
                        model: model,
                        layout: 'record',
                        modelId: model.get('id')
                    }
                }, _.bind(function(){
                    this.refreshNotes();
                }, this));
            }, this)
        });
    },

    refreshNotes: function(){
        this.layout.layout.caseModel.fetch({
            success: _.bind(function () {
                this.render();
                this.model.getRelatedCollection('notes').fetch({relate: true});
            }, this)
        });
    },

    refreshCase: function (){
        //We need to fetch the case model to avoid edit conflicts from hook(s) that sets value on Case when
        //Note is saved. We fetch and then restore any changed values on the Case (if user is editing)
        let changedAttrs = this.context.get('model').changedAttributes(this.context.get('model').getSynced());
        _.each(changedAttrs, function (value, field) {
            //The changed attributes are showing the synced values for some reason, so overwrite them with the changed values
            changedAttrs[field] = this.context.get('model').get(field);
        }, this);
        this.context.get('model').fetch({
            complete: _.bind(function (model) {
                //Defer restore of changed attributes to not have it overridden from fetch of model
                _.defer(_.bind(function () {
                    _.each(changedAttrs, function (value, field) {
                        this.context.get('model').set(field, value);
                    }, this)
                }, this));
            }, this)
        });
    },

})