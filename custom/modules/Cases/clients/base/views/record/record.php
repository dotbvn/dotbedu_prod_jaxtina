<?php
$viewdefs['Cases'] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'record' =>
            array (
                'buttons' =>
                array (
                    0 =>
                    array (
                        'type' => 'button',
                        'name' => 'cancel_button',
                        'label' => 'LBL_CANCEL_BUTTON_LABEL',
                        'tooltip' => 'LBL_CANCEL_BUTTON_LABEL',
                        'css_class' => 'btn ',
                        'icon' => 'fa-window-close',
                        'showOn' => 'edit',
                        'events' =>
                        array (
                            'click' => 'button:cancel_button:click',
                        ),
                    ),
                    1 =>
                    array (
                        'type' => 'rowaction',
                        'event' => 'button:save_button:click',
                        'name' => 'save_button',
                        'label' => 'LBL_SAVE_BUTTON_LABEL',
                        'tooltip' => 'LBL_SAVE_BUTTON_LABEL',
                        'icon' => 'fa-save',
                        'css_class' => 'btn btn-primary',
                        'showOn' => 'edit',
                        'acl_action' => 'edit',
                    ),
                    2 =>
                    array (
                        'type' => 'actiondropdown',
                        'name' => 'main_dropdown',
                        'primary' => true,
                        'showOn' => 'view',
                        'buttons' =>
                        array (
                            0 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:edit_button:click',
                                'name' => 'edit_button',
                                'tooltip' => 'LBL_EDIT_BUTTON_LABEL',
                                'label' => 'LBL_EDIT_BUTTON_LABEL',
                                'icon' => 'fa-pencil',
                                'primary' => true,
                                'acl_action' => 'edit',
                            ),
                            1 =>
                            array (
                                'type' => 'shareaction',
                                'name' => 'share',
                                'label' => 'LBL_RECORD_SHARE_BUTTON',
                                'acl_action' => 'view',
                            ),
                            2 =>
                            array (
                                'type' => 'pdfaction',
                                'name' => 'download-pdf',
                                'label' => 'LBL_PDF_VIEW',
                                'action' => 'download',
                                'acl_action' => 'view',
                            ),
                            3 =>
                            array (
                                'type' => 'pdfaction',
                                'name' => 'email-pdf',
                                'label' => 'LBL_PDF_EMAIL',
                                'action' => 'email',
                                'acl_action' => 'view',
                            ),
                            4 =>
                            array (
                                'type' => 'divider',
                            ),
                            8 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:duplicate_button:click',
                                'name' => 'duplicate_button',
                                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                                'acl_module' => 'Cases',
                                'acl_action' => 'create',
                            ),
                            10 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:audit_button:click',
                                'name' => 'audit_button',
                                'label' => 'LNK_VIEW_CHANGE_LOG',
                                'acl_action' => 'view',
                            ),
                            11 =>
                            array (
                                'type' => 'divider',
                            ),
                            12 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:delete_button:click',
                                'name' => 'delete_button',
                                'label' => 'LBL_DELETE_BUTTON_LABEL',
                                'acl_action' => 'delete',
                            ),
                        ),
                    ),
                    3 =>
                    array (
                        'name' => 'sidebar_toggle',
                        'type' => 'sidebartoggle',
                    ),
                ),
                'panels' =>
                array (
                    0 =>
                    array (
                        'name' => 'panel_header',
                        'header' => true,
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'picture',
                                'type' => 'avatar',
                                'size' => 'large',
                                'dismiss_label' => true,
                                'readonly' => true,
                            ),
                            1 => 'name',
                            2 =>
                            array (
                                'name' => 'favorite',
                                'label' => 'LBL_FAVORITE',
                                'type' => 'favorite',
                                'dismiss_label' => true,
                            ),
                            3 =>
                            array (
                                'name' => 'status',
                                'type' => 'event-status',
                                'enum_width' => 'auto',
                                'dropdown_width' => 'auto',
                                'dropdown_class' => 'select2-menu-only',
                                'container_class' => 'select2-menu-only',
                            ),
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'panel_body',
                        'label' => 'LBL_RECORD_BODY',
                        'columns' => 2,
                        'labelsOnTop' => true,
                        'placeholders' => true,
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'case_number',
                                'readonly' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'parent_case_name',
                                'label' => 'LBL_PARENT_CASE',
                                'type' => 'html'
                            ),
                            2 =>
                            array (
                                'name' => 'contacts_cases_1_name',
                                'label' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
                            ),
                            3 =>
                            array (
                                'name' => 'type',
                            ),
                            4 =>
                            array (
                                'name' => 'j_class_cases_1_name',
                                'label' => 'LBL_J_CLASS_CASES_1_FROM_J_CLASS_TITLE',
                            ),
                            5 =>
                            array (
                                'name' => 'source',
                            ),
                            6 =>
                            array (
                                'name' => 'rate',
                                'readonly' => true,
                                'label' => 'LBL_RATE',
                            ),
                            7 =>
                            array (
                                'name' => 'portal_viewable',
                                'label' => 'LBL_SHOW_IN_PORTAL',
                            ),
                            8 =>
                            array (
                                'name' => 'description',
                                'span' => 12,
                            ),
                            9 =>
                            array (
                                'name' => 'assigned_user_name',
                            ),
                            10 =>
                            array (
                                'name' => 'cso_name',
                                'label' => 'LBL_CSO_NAME',
                            ),
                            11 =>
                            array (
                                'name' => 'date_entered_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_ENTERED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_entered',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'created_by_name',
                                    ),
                                ),
                            ),
                            12 => 'team_name',
                            13 =>
                            array (
                                'name' => 'date_modified_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_MODIFIED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_modified',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'modified_by_name',
                                    ),
                                ),
                            ),
                            14 =>
                            array (
                                'name' => 'last_comment_date',
                                'label' => 'LBL_LAST_COMMENT_DATE',
                            ),
                        ),
                    ),
                ),
                'templateMeta' =>
                array (
                    'useTabs' => false,
                ),
            ),
        ),
    ),
);
