({
    extendsFrom: 'RecordView',

    initialize: function (options) {
        this._super('initialize', [options]);
        this.context.on('button:mark_complete:click', this.markComplete, this);
    },

    markComplete: function(model) {
        debugger;
        model.attributes.status='Closed';
        model.save(null, {
            success: function() {
                app.alert.show('save-success', {
                    level: 'success',
                    messages: 'Save Successfully'
                });

                setTimeout(function() {
                    location.reload(); // Load lại trang sau khi lưu thành công
                }, 2000); // Đợi 2 giây trước khi tự động reload
            }
        });
    }
})
