<?php


    $viewdefs['Cases']['base']['view']['list-headerpane'] = array(

        'buttons' => array(

            array(


                'label' => 'LNK_NEW_CASE',
                'tooltip' => 'LNK_NEW_CASE',
                'acl_action' => 'create',
                'type' => 'button',
                'acl_module' => 'Cases',
                'route'=>'#Cases/create',
                'icon' => 'fa-plus',
            ),
            array(


                'label' => 'LNK_CASE_REPORTS',
                'tooltip' => 'LNK_CASE_REPORTS',
                'acl_action' => 'list',
                'type' => 'button',
                'acl_module' => 'Reports',
                'route'=>'#Reports?filterModule=Cases',
                'icon' => 'fa-user-chart',
            ),
            array(
                'name' => 'sidebar_toggle',
                'type' => 'sidebartoggle',
            ),
        ),
);