/**
 * @author Exsitec AB
 */
({
    iconList: {
        'docx': 'fa-file-word-o',
        'txt': 'fa-file-text',
        'excel': 'fa-file-excel-o',
        'video': 'fa-file-video-o',
        'pptx': 'fa-file-powerpoint-o',
        'audio': 'fa-file-audio-o',
        'pdf': 'fa-file-pdf-o',
        'zip': 'fa-file-archive-o',
        'others': 'fa-file'
    },
    events: {
        'click .add-file-button': 'handleFileBrowsed',
        'click .send-message-button': 'handleSendMessage',
        'keypress .chat__conversation-panel__input': 'checkForEnter',
        'input .chat__conversation-panel__input': 'toggleSendButton',
        'change #file-input': 'handleFileChanged',
        'dragover .boxChatContainer': 'dragOverEvt',
        'dragleave .boxChatContainer': 'dragLeaveEvt',
        'drop .boxChatContainer': 'dropEvt',
        'click .delete-file': 'handleDeleteFile',
        'click .img-preview': 'popupImage',
        'click .messageFile .file-element': 'handleDownloadFile',
    },
    isLoading: false,
    canLoad: true,
    currentPage: 1,
    page: 1,
    countUploading: 0,
    commentDummy: undefined,
    fileList: [],
    countFile: 0,
    unSentList: [],
    initialize: function (options) {
        this._super('initialize', [options]);

        this.platform = app.config.platform;

        this.initCommentDummy();
        this.initSocketListener();
        this.fileList = [];
    },
    /*
    * Socket listener init to listen to the chat channel
    */
    initSocketListener: function () {
        var socketURL = "https://socket.dotb.cloud/";
        var socket = io.connect(socketURL, {"path": "", "transports": ["websocket"], "reconnection": true});
        socket.on('connect', _.bind(function () {
            console.log('Socket server is live!');
            socket.emit('join', App.config.uniqueKey + '/loadBaseMessage/' + this.layout.layout.caseModel.get('id'));
        }, this));
        socket.on('error', function () {
            console.log('Cannot connect to socket server!')
        })
        socket.on('event-phenikaa', _.bind(function (msg) {
            let lastComment = $('.messageArea.portal .commNote').last();
            var firstPWithContent = lastComment.find('p').filter(function () {
                return $.trim($(this).text()).length > 0;
            }).first();
            var signatureDate = lastComment.find('.signatureDate').text();

            if (lastComment.length > 0 && firstPWithContent.text() !== msg.message || (firstPWithContent.text() == msg.message && signatureDate != msg.date_entered)) {
                _.each(msg.attachments, _.bind(function (fileModel) {
                    this.appendFile(fileModel, 'inbound', msg.avt);
                }, this));
                if (msg.message != '') this.appendMessage(msg);
            }
        }, this));
    },
    /*
    * Init drag drop event
    * */
    initDragDrop: function () {
        this.$(".fileInput").on('dragenter', _.bind(function (ev) {
            this.$(".fileInput").addClass("highlightDropArea");
        }, this));

        this.$(".fileInput").on('dragleave', _.bind(function (ev) {
            this.$(".fileInput").removeClass("highlightDropArea");
        }, this));

        this.$(".fileInput").on('drop', _.bind(function (ev) {
            // Dropping files
            ev.preventDefault();
            ev.stopPropagation();
            if (ev.originalEvent.dataTransfer) {
                if (ev.originalEvent.dataTransfer.files.length) {
                    let droppedFile = _.first(ev.originalEvent.dataTransfer.files);
                    let list = new DataTransfer();
                    list.items.add(droppedFile);
                    this.$('input[type="file"]')[0].files = list.files;
                    this.commentDummy.set('filename', droppedFile.name);
                }
            }

            this.$(".fileInput").removeClass("highlightDropArea");
            return false;
        }, this));

        this.$(".fileInput").on('dragover', _.bind(function (ev) {
            ev.preventDefault();
        }, this));
    },
    /*
    * Show/Hide the outline when drag over the box chat container
    */
    dragOverEvt: function (evt) {
        evt.preventDefault();
        $('.boxChatContainer').addClass("onDragover");
        $('#file-input').toggleClass("dragover", evt.type === "dragover");
    },
    dragLeaveEvt: function (evt) {
        evt.preventDefault();
        $('.boxChatContainer').removeClass("onDragover");
        $('#file-input').removeClass("dragover");
    },

    /*
    * Drop file to box chat container
    * Init the file list and show the file list at chat box
     */
    dropEvt: function (evt) {
        evt.preventDefault();
        $('.boxChatContainer').removeClass("onDragover");
        $('#file-input').removeClass("dragover");
        this.fileList = [];
        this.showFileList();
        this.handleFileSelected(evt.originalEvent.dataTransfer.files);
    },
    // Popup file selector when click on attachent button
    handleFileBrowsed: function () {
        $('#file-input').click();
    },
    /*
    * FileChanged event mean that user has selected file from file input
    */

    handleFileChanged: function (e) {
        let files = e.target.files;
        this.fileList = [];
        this.showFileList();
        this.handleFileSelected(files);
    },

    /*
    * The file selected will be handled here
    * If the file size is greater than 100000KB, show the alert
    * If the file size is less than 100000KB, add the file to the file list
    **/
    handleFileSelected: function (files) {
        if (files.length > 0) {
            $.each(files, _.bind(function (index, file) {
                var fileSize = (file.size / 1024).toFixed(2);
                var fileType = file.type;
                if (fileSize > 100000) {
                    app.alert.show('error', {
                        level: 'error',
                        messages: app.lang.get('LBL_ALERT_FILE_SIZE_LIMIT'),
                        autoClose: true
                    });
                    return;
                }
                this.addFileRow(file, this.countFile);
            }, this));
        }
    },

    /*
    * File row will be added to the file list
    * We use countFile to tracking how many file has been added
    * We use countUploading to tracking how many file is being uploaded
    * */
    addFileRow: function (file, countFile) {
        let fileName = file.name;
        let fileSize = (file.size / 1024).toFixed(2);
        let fileType = file.type;
        let icon = this.getFileIcon(fileName, fileType);
        let fileSrc = URL.createObjectURL(file);
        let preview = fileType.startsWith("image")
            ? `<img class="img-preview show-image" src="${fileSrc}" alt="${fileName}">`
            : `<i class="fa ${icon}"></i>`;

        this.countFile++;
        this.countUploading++;
        let fileModel = {
            isImage: fileType.startsWith("image"),
            fileName: fileName,
            fileSize: fileSize,
            fileType: fileType,
            preview: preview,
            countFile: countFile,
            fileUrl: fileSrc,
            showDownload: false
        };
        this.fileList.push(fileModel);
        this.appendFileList(fileModel);
        this.uploadFile(file, countFile);
    },
    getFileExtension: function (filename) {
        if (filename.includes('zip')) {
            return 'zip';
        }
        return filename.slice(((filename.lastIndexOf(".") - 1) >>> 0) + 2);
    },
    getFileIcon: function (fileName, fileType) {
        let fileIcon = 'others';
        if (fileType.startsWith("video")) {
            fileIcon = 'video';
        }
        let extension = this.getFileExtension(fileName);
        switch (extension) {
            case 'csv':
            case 'xls':
            case 'xlsx':
                fileIcon = 'excel';
                break;
            case 'docx':
            case 'doc':
                fileIcon = 'docx';
                break;
            case 'txt':
                fileIcon = 'txt';
                break;
            case 'pptx':
                fileIcon = 'pptx';
                break;
            case 'pdf':
                fileIcon = 'pdf';
                break;
            case 'zip':
            case 'rar':
                fileIcon = 'zip';
                break;
        }
        return this.iconList[fileIcon];
    },

    handleDeleteFile: function (evt) {
        let noteId = evt.target.parentElement.parentElement.parentElement.dataset.noteid;
        let saved = evt.target.parentElement.parentElement.parentElement.dataset.saved;

        evt.target.closest(".file-element").remove();
        this.fileList = this.fileList.filter(_.bind(function (file) {
            return file.countFile != this.countFile - 1;
        }, this));
        this.countFile--;

        if (this.countFile === 0) {
            this.clearFileInput();
        }
    },

    /*
    * Add file to the file list to show on the chat box
    */
    appendFileList: function (fileModel) {
        var fileHtml = '<div class="file-element" id="file-upload-' + fileModel.countFile + '">' +
            '<div class="file-info">' +
            '<div class="file-preview">' + fileModel.preview + '</div>' +
            '<div class="file-content">' +
            '<span class="file-name ellipsis_inline" data-original-title="' + fileModel.fileName + '"> ' + fileModel.fileName + '</span>' +
            '<div class="file-size-contain">' +
            '<span class="file-size">' + fileModel.fileSize + ' KB</span>' +
            (fileModel.showDownload ? '<a class="file-download" id="file-download-' + fileModel.countFile + '" download="' + fileModel.fileName + '" href="' + fileModel.fileSrc + '">LBL_DOWNLOAD<i class="fa fa-download" aria-hidden="true"></i></a>' : '') +
            '</div>' +
            '</div>' +
            '<div class="exit-and-percent">' +
            '<i class="fa fa-times delete-file" aria-hidden="true"></i>' +
            '<div id="loader-' + fileModel.countFile + '" ><span class="send-message-loader" style="border: 2px solid #717171;"></span></div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $('.file-list').append(fileHtml);
    },

    /*
    * Event handler
    * */
    checkForEnter: function (e) {
        // Check if the pressed key is Enter
        if (e.which === 13) { // 13 is the keycode for Enter
            this.handleSendMessage();
        }
    },
    toggleSendButton: function () {
        if ($(".chat__conversation-panel__input").val() == '') {
            $('.send-message-button').addClass('disabled');
        } else {
            $('.send-message-button').removeClass('disabled');
        }
    },
    /*
    * */
    initCommentDummy: function () {
        this.commentDummy = app.data.createBean('C_Comments', {
            id: this.generateUUID(),
            new_with_id: true,
            parent_id: this.layout.layout.caseModel.get('id'),
            parent_type: this.layout.layout.caseModel.module,
            direction: 'outbound',
            assigned_user_id: app.user.get('id'),
            created_by: app.user.get('id'),
            attachment_list: [],
            team_id: 1,
            team_set_id: 1,
        });
    },

    _render: function () {
        this._super('_render');
        this.$el.find('.noteAttachment').off('click', this.downloadNote).on('click', this.downloadNote);
        $('.contact-communication .messageArea').scrollTop($('.contact-communication .messageArea').prop("scrollHeight"));

        this.initDragDrop();
        this.$el.css('display', 'flex');
        this.$el.css('justify-content', 'center');
        this.$el.css('align-items', 'center');
        $('.communication-container.row-fluid.ninja').css('height', '470px');
        $('.messageArea').on('scroll', _.bind(this.onScrollHandling, this));
    },
    /*
    * Send message related events
    * */
    clearMessage: function () {
        $(".chat__conversation-panel__input").val('');
    },
    getMessage: function () {
        return $(".chat__conversation-panel__input").val();
    },
    scrollBottom: function () {
        $('.contact-communication .messageArea').scrollTop($('.contact-communication .messageArea').prop("scrollHeight"));
    },
    disableSendButton: function () {
        $('.send-message-button').addClass('disabled'); // Disable button send
    },
    enableSendButton: function () {
        $('.send-message-button').addClass('e'); // Disable button send
    },
    toggleSendState: function (id, type) {
        if(type == 'success') {
            $('#' + id).html('Sent <i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;color: white;font-weight: 400;"></i>');
            $('.file-send-loading').each(function() {
                $(this).html('<i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;font-weight: 400;"></i>');
            });
        } else {
            $('#' + id).html('Sent Failed <i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;color: white;font-weight: 400;"></i>');
            $('.file-send-loading').each(function() {
                $(this).html('Sent Failed');
            });
        }
    },
    handleSendMessage: function () {
        if (this.validateContent()) { // Make sure message not empty
            this.disableSendButton(); // Disable button send
            this.clearFileInput();
            this.commentDummy.set('description', this.getMessage());
            this.commentDummy.set('direction', 'outbound');
            app.api.call(
                'create',
                app.api.buildURL('chat-message/saveMessage'),
                this.commentDummy,
                {
                    success: _.bind(function (respone) {
                        this.toggleSendState(respone.id, 'success')
                    }, this),
                    error: _.bind(function (respone) {
                        this.toggleSendState(respone.id, 'failed')
                    }, this),
                });
            if (this.commentDummy.get('attachment_list').length > 0) {
                this.sendFile('outbound');
                this.countFile = 0;
            }
            if (this.getMessage() != '') {
                //add message
                let message = {
                    'message': $(".chat__conversation-panel__input").val(),
                    'direction': 'outbound',
                    'person_name': app.user.get('full_name'),
                    'date_entered': this.formatCurrentDateTime()
                }
                this.appendMessage(message);
                this.clearMessage();
            }
            this.scrollBottom();
            //init new message object
            this.initCommentDummy();
        }
    },
    appendMessage: function (messageObject) {
        let messageHtml = '';
        let direction = messageObject.direction == 'inbound' ? 'from' : 'to';
        let pullDirection = messageObject.direction == 'inbound' ? 'left' : 'right';
        let chater = messageObject.direction == 'inbound' ? 'Contacts' : 'Users';
        let signature = messageObject.direction == 'inbound' ? 'C' : 'U';

        messageHtml += "<div class ='commNote " + direction + " clearfix'>";
        if(direction == 'from') {
            messageHtml += "<div class ='commIcon userIcon pull-" + pullDirection + "'>";
            if(!_.isEmpty(messageObject.avt)){
                messageHtml += "<div class='image_rounded' style='width: 42px;'><img src='"+ messageObject.avt +"'></div>";
            } else {
                messageHtml += "<span class ='label label-module label-module-lg label-" + chater + "'>" + signature + "</span>";
            }
            messageHtml += "</div>";
        }
        messageHtml += "<div class='messageContent pull-" + pullDirection + "'>";
        messageHtml += "<div class='messageText'><p>" + messageObject.message + "</p>\n";
        messageHtml += "<span class='commSignature'>" + messageObject.person_name + ": ";
        messageHtml += "<span class='signatureDate'>" + this.formatCurrentDateTime() + "</span></span>";
        messageHtml += "</div>";
        if (direction == 'to') messageHtml += "<div class='sending-title' id='" + this.commentDummy.id + "'>Sending <span class='send-message-loader'></span></div>";
        messageHtml += "</div>";
        messageHtml += "</div>";
        $('.messageArea').append(messageHtml);
        this.scrollBottom();
    },
    prependMessage: function (messageObject) {
        let messageHtml = '';
        let direction = messageObject.direction == 'from' ? 'from' : 'to';
        let pullDirection = messageObject.direction == 'from' ? 'left' : 'right';
        let chater = messageObject.direction == 'from' ? 'Contacts' : 'Users';
        let signature = messageObject.direction == 'from' ? 'C' : 'U';

        _.each(messageObject.attachments, _.bind(function (fileModel) {
            this.prependFile(fileModel, direction, messageObject.avt);
        }, this));

        if(messageObject.message != '') {
            messageHtml += "<div class ='commNote " + direction + " clearfix'>";
            messageHtml += "<div class ='commIcon userIcon pull-" + pullDirection + "'>";
            if(messageObject.direction == 'from') {
                if(!_.isEmpty(messageObject.avt)){
                    messageHtml += "<div class='image_rounded' style='width: 42px;'><img src='"+ messageObject.avt +"'></div>";
                } else {
                    messageHtml += "<span class ='label label-module label-module-lg label-" + chater + "'>" + signature + "</span>";
                }
            }
            messageHtml += "</div>";
            messageHtml += "<div class='messageContent pull-" + pullDirection + "'>";
            messageHtml += "<div class='messageText'><p>" + messageObject.message + "</p>\n";
            messageHtml += "<span class='commSignature'>" + messageObject.person_name + ": ";
            messageHtml += "<span class='signatureDate'>" + messageObject.date_entered + "</span></span>";
            messageHtml += "</div>";
            messageHtml += "</div>";
            messageHtml += "</div>";
            $('.messageArea').prepend(messageHtml);
        }
    },
    formatCurrentDateTime: function () {
        const now = new Date();
        const day = now.getDate().toString().padStart(2, '0');
        const month = (now.getMonth() + 1).toString().padStart(2, '0'); // +1 because months are 0-indexed.
        const year = now.getFullYear();
        let hours = now.getHours();
        const minutes = now.getMinutes().toString().padStart(2, '0');
        const ampm = hours >= 12 ? 'PM' : 'AM';

        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        hours = hours.toString().padStart(2, '0');

        return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes + ' ' + ampm;
    },
    validateContent: function () {
        if ($(".chat__conversation-panel__input").val() == '' && this.countFile == 0) {
            return false;
        }
        if (this.countUploading > 0) {
            app.alert.show('error_validation_process', {
                level: 'error',
                messages: "Please wait for the file to finish uploading.",
                autoClose: false
            });
            return false;
        }
        return true;
    },
    generateUUID: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    /*
    * Lazy loading
    * */
    onScrollHandling: function (e) {
        if (this.canLoad && !this.isLoading) {
            let scrollHeight = $('.contact-communication .messageArea').prop("scrollHeight");
            if ($('.contact-communication .messageArea').scrollTop() == 0) {
                this.isLoading = true;
                this.page++;
                this.showLoading();
                if (parseInt(this.page) == parseInt(this.currentPage) + 1) {
                    app.api.call(
                        'read',
                        app.api.buildURL('chat-message/loadMessage?case_id=' + this.layout.layout.caseModel.get('id') + '&page=' + this.page),
                        null,
                        {
                            success: _.bind(function (respone) {
                                this.isLoading = false;
                                this.canLoad = respone.canLoad;
                                this.currentPage = respone.currentPage;
                                $('.contact-communication .messageArea').scrollTop($('.contact-communication .messageArea').prop("scrollHeight") - scrollHeight)
                                this.hideLoading();
                                _.each(respone.messageList, _.bind(function (messageObject) {
                                    if (messageObject.message != '' || messageObject.attachments.length != 0) this.prependMessage(messageObject);
                                }, this));
                            }, this),
                        });
                } else {
                    this.page = parseInt(this.currentPage) + 1;
                }
            }
        }
    },
    showLoading: function () {
        $('.messageArea').prepend('<span class="message-loader"></span>') //Add loader
    },
    hideLoading: function () {
        $('.message-loader').remove();
    },

    /*
    * File handler
    * */
    sendFile: function (direction) {
        _.each(this.fileList, _.bind(function (fileModel) {
            this.appendFile(fileModel, direction, '');
        }, this));
        this.fileList = [];
    },
    appendFile: function (fileModel, direction, avt) {
        let direction_mark = direction == 'inbound' ? 'from' : 'to';
        let pullDirection = direction == 'inbound' ? 'left' : 'right';
        let chater = direction == 'inbound' ? 'Contacts' : 'Users';
        let signature = direction == 'inbound' ? 'C' : 'U';
        let fileHtml = '';
        let messageHtml = '';

        if(fileModel.isImage){
            fileHtml = fileModel.preview
        } else {
            fileHtml = '<div class="file-element" id="file-upload-' + fileModel.countFile + '" data-noteId="'+ fileModel.id +'">' +
                '<div class="file-info">' +
                '<div class="file-preview">' + fileModel.preview + '</div>' +
                '<div class="file-content">' +
                '<span class="file-name ellipsis_inline" data-original-title="' + fileModel.fileName + '"> ' + fileModel.fileName + '</span>' +
                '<div class="file-size-contain">' +
                '<span class="file-size">' + fileModel.fileSize + ' KB</span>' +
                '<a class="file-download" id="file-download-' + fileModel.countFile + '" download="' + fileModel.fileName + '" href="' + fileModel.fileUrl + '"> ' + app.lang.get('LBL_DOWNLOAD') + '<i class="fa fa-download" aria-hidden="true"></i></a>';
            if(direction == 'outbound') {
                fileHtml += '<div class="file-send-loading"><span class="send-message-loader" style="border: 2px solid #717171;"></span></div>'
            }
            fileHtml += '</div>'
            fileHtml +='</div>'
            fileHtml += '</div>'
            fileHtml += '</div>';
        }
        messageHtml += "<div class ='noteContainer commNote " + direction_mark + " clearfix'>";
        if(direction == 'inbound') {
            messageHtml += "<div class ='commIcon userIcon pull-" + pullDirection + "'>";
            if(!_.isEmpty(avt)){
                messageHtml += "<div class='image_rounded' style='width: 42px;'><img src='"+ avt +"'></div>";
            } else {
                messageHtml += "<span class ='label label-module label-module-lg label-" + chater + "'>" + signature + "</span>";
            }
            messageHtml += "</div>";
        }
        messageHtml += "<div class='messageFile pull-" + pullDirection + "'>";
        messageHtml += fileHtml;
        messageHtml += "</div>";
        messageHtml += "</div>";
        this.scrollBottom();
        $('.messageArea').append(messageHtml);
    },
    prependFile: function (fileModel, direction, avt) {
        let direction_mark = direction == 'from' ? 'from' : 'to';
        let pullDirection = direction == 'from' ? 'left' : 'right';
        let chater = direction == 'from' ? 'Contacts' : 'Users';
        let signature = direction == 'from' ? 'C' : 'U';
        let fileHtml = '';
        if(fileModel.isImage){
            fileHtml = fileModel.preview;
        } else {
            fileHtml = '<div class="file-element" id="file-upload-' + fileModel.countFile + '" data-s3uploaded="'+ fileModel.s3Uploaded +'" data-noteId="'+ fileModel.attachment_id +'">' +
                '<div class="file-info">' +
                '<div class="file-preview">' + fileModel.preview + '</div>' +
                '<div class="file-content">' +
                '<span class="file-name ellipsis_inline" data-original-title="' + fileModel.fileName + '"> ' + fileModel.fileName + '</span>' +
                '<div class="file-size-contain">' +
                '<span class="file-size">' + fileModel.fileSize + ' KB</span>' +
                '<a class="file-download" id="file-download-' + fileModel.countFile + '" download="' + fileModel.fileName + '" href="' + fileModel.fileUrl + '"> ' + app.lang.get('LBL_DOWNLOAD') + '<i class="fa fa-download" aria-hidden="true"></i></a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
        }

        let messageHtml = '';
        messageHtml += "<div class ='noteContainer commNote " + direction_mark + " clearfix'>";
        messageHtml += "<div class ='commIcon userIcon pull-" + pullDirection + "'>";
        if(direction == 'from') {
            if(!_.isEmpty(avt)){
                messageHtml += "<div class='image_rounded' style='width: 42px;'><img src='"+ avt +"'></div>";
            } else {
                messageHtml += "<span class ='label label-module label-module-lg label-" + chater + "'>" + signature + "</span>";
            }
        }
        messageHtml += "</div>";
        messageHtml += "<div class='messageFile pull-" + pullDirection + "'>";
        messageHtml += fileHtml;
        messageHtml += "</div>";
        messageHtml += "</div>";
        $('.messageArea').prepend(messageHtml);
    },
    showFileList: function () {
        $('.file-list').css('display', 'flex');
        $('.messageArea').css('height', '70%');
        $('.actionBar').css('height', '18%');
        $('.send-message-button').removeClass('disabled');
    },
    uploadFile: function (file, countFile) {
        var formData = new FormData();
        let self = this;

        formData.append('file', file);
        formData.append('action', 'createNote');
        formData.append('parentModule', 'C_Comments');
        formData.append('userId', app.user.id);

        $.ajax({
            url: 'index.php?entryPoint=uploadFile',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: _.bind(function (response) {
                // Handle successful upload
                var response = JSON.parse(response);
                this.completeHandler(response, countFile);
            }, this),
            error: _.bind(function (error) {
                // Handle upload error
                console.error('Upload failed:', error);
                this.uploadFailed(countFile);
            }, this)
        });
    },
    clearFileInput: function () {
        $('.file-list').empty();
        $('.file-list').css('display', 'none');
        $('.messageArea').css('height', '');
        $('.actionBar').css('height', '');
        $('.send-message-button').addClass('disabled');
    },
    completeHandler: function (response, countFile) {
        let fileSelector = "#file-upload-" + countFile;
        let loader = "#loader-" + countFile;
        // Successful upload
        var id = response.id;
        if (response.success === true) {
            this.countUploading--;
            $(fileSelector).attr("data-noteId", id);
            $(loader).html('<i class="fa fa-check" aria-hidden="true" style="margin-left: 5px;font-weight: 400;"></i>');
            let noteList = this.commentDummy.get('attachment_list');
            noteList.push({
                id: id,
                action: 'add'
            })
            this.commentDummy.set('attachment_list', noteList);
        }
        if (response.error_num === 102) {
            this.uploadFailed(countFile);
        }
    },
    uploadFailed: function (countFile) {
        $(`#file-upload-${countFile} .file-size-contain`).text('Failed to upload')
    },
    popupImage: function (evt) {
        var sourceImage = evt.target.src;
        var width = evt.target.naturalWidth;
        var height = evt.target.naturalHeight;
        if (width > 1000 || height > 1000) {
            width = width / 2;
            height = height / 2;
        }
        var data = `<div style="background-color: #fff;
                height: ${height}px; width: ${width}px;
                border-radius: 3px;
                box-shadow: 5px 5px 20px rgba(0,0,0,0.05);
                position: relative;"><img style="height: ${height}px; width: ${width}px;" src="${sourceImage}"></div>`;
        window.top.$.openPopupLayer({
            name: "imagePopup",
            html: data
        });
    },
    handleDownloadFile: function (evt) {
        this.currentTarget
        if(evt.currentTarget.dataset.s3uploaded){
            $.ajax({
                url: 'index.php?entryPoint=uploadFile',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'downloadNotes',
                    noteId: evt.currentTarget.dataset.noteid
                },
                success: function (response) {
                    var a = document.createElement('a');
                    a.download = evt.currentTarget.children[0].children[1].children[1].children[1].download;
                    a.href = response.data.file_source;
                    document.body.appendChild(a);
                    a.click();
                    document.body.removeChild(a);
                    console.log('Download successfully');
                },
                error: function (error) {
                    console.log('Download failed')
                }
            });
        } else {
            let file = evt.currentTarget.children[0].children[1].children[1].children[1];
            var a = document.createElement('a');
            a.download = file.download;
            a.href = file.href;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }
    },

    refreshCase: function () {
        //We need to fetch the case model to avoid edit conflicts from hook(s) that sets value on Case when
        //Note is saved. We fetch and then restore any changed values on the Case (if user is editing)
        let changedAttrs = this.context.get('model').changedAttributes(this.context.get('model').getSynced());
        _.each(changedAttrs, function (value, field) {
            //The changed attributes are showing the synced values for some reason, so overwrite them with the changed values
            changedAttrs[field] = this.context.get('model').get(field);
        }, this);
        this.context.get('model').fetch({
            complete: _.bind(function (model) {
                //Defer restore of changed attributes to not have it overridden from fetch of model
                _.defer(_.bind(function () {
                    _.each(changedAttrs, function (value, field) {
                        this.context.get('model').set(field, value);
                    }, this)
                }, this));
            }, this)
        });
    },
})