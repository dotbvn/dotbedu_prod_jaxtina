<?php
// created: 2019-04-18 19:01:55
$viewdefs['Cases']['base']['view']['subpanel-for-j_class-j_class_cases_1'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'label' => 'LBL_LIST_NUMBER',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
          'name' => 'case_number',
        ),
        1 =>
        array (
          'label' => 'LBL_LIST_SUBJECT',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
          'type' => 'html',
        ),
        2 =>
        array (
          'label' => 'LBL_LIST_STATUS',
          'enabled' => true,
          'default' => true,
          'name' => 'status',
          'type' => 'html',

        ),
        3 =>
        array (
          'name' => 'j_class_cases_1_name',
          'label' => 'LBL_J_CLASS_CASES_1_FROM_J_CLASS_TITLE',
          'enabled' => true,
          'id' => 'J_CLASS_CASES_1J_CLASS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 =>
        array (
          'name' => 'contacts_cases_1_name',
          'label' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
          'enabled' => true,
          'id' => 'CONTACTS_CASES_1CONTACTS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 =>
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 =>
        array (
          'label' => 'LBL_LIST_DATE_CREATED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_entered',
        ),
        7 =>
        array (
          'name' => 'assigned_user_name',
          'target_record_key' => 'assigned_user_id',
          'target_module' => 'Employees',
          'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);