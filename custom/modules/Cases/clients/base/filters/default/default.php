<?php
// created: 2022-06-14 15:39:18
$viewdefs['Cases']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'quicksearch_field' => 
  array (
    0 => 'name',
    1 => 'case_number',
  ),
  'quicksearch_priority' => 2,
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'status' => 
    array (
    ),
    'type' =>
    array (
    ),
    'team_name' =>
    array(
    ),
    'priority' => 
    array (
    ),
    'case_number' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'j_class_cases_1_name' => 
    array (
    ),
    'contacts_cases_1_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
  'filters' => 
  array (
    0 => 
    array (
      'id' => 'favorites',
      'name' => 'LBL_FAVORITES',
      'filter_definition' => 
      array (
        '$favorite' => '',
      ),
      'editable' => false,
      'order' => 2,
    ),
    1 => 
    array (
      'id' => 'all_records',
      'name' => 'LBL_LISTVIEW_FILTER_ALL',
      'filter_definition' => 
      array (
      ),
      'editable' => false,
      'order' => 3,
    ),
  ),
);