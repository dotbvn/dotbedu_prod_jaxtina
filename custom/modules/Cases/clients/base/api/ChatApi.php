<?php
require_once 'clients/base/api/ModuleApi.php';

use CaseChat\ConnectorHelper;
use CaseChat\Exception\InvalidLicenseException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class ChatApi extends ModuleApi
{

    public function registerApiRest()
    {
        return array(
            'retrieveCaseRecord' => array(
                'reqType' => 'GET',
                'path' => array('Cases', '?'),
                'pathVars' => array('module', 'record'),
                'method' => 'retrieveRecord',
                'shortHelp' => 'Returns a single record',
                'longHelp' => 'include/api/help/module_record_get_help.html',
            ),
            'saveMessage' => array(
                'reqType' => 'POST',
                'path' => array('chat-message', 'saveMessage'),
                'method' => 'saveMessage',
                'shortHelp' => 'save the message',
                'longHelp' => '',
            ),
            'loadMessage' => array(
                'reqType' => 'GET',
                'path' => array('chat-message', 'loadMessage'),
                'method' => 'loadMessage',
                'shortHelp' => 'load the message',
                'longHelp' => '',
            ),
            'pushNotification' => array(
                'reqType' => 'POST',
                'path' => array('chat-message', 'pushNotification'),
                'method' => 'pushNotification',
                'shortHelp' => 'push notification',
                'longHelp' => '',
            ),
        );
    }

    /**
     * Fix for other api's beeing overwritten.
     *
     * @param ServiceBase $api
     * @param array $args
     *
     * @return string
     * @throws Exception
     * @throws DotbApiExceptionEditConflict
     * @throws DotbApiExceptionInvalidParameter
     */
    public function retrieveRecord($api, $args)
    {
        if (isset($args['record']) && $args['record'] == "filter") {
            require_once 'clients/base/api/FilterApi.php';
            $filterApi = new FilterApi();
            return $filterApi->filterList($api, $args);
        } elseif (isset($args['record']) && $args['record'] == "config") {
            require_once 'clients/base/api/ConfigModuleApi.php';
            $configApi = new ConfigModuleApi();
            return $configApi->config($api, $args);
        } elseif (isset($args['record']) && $args['record'] == "count") {
            require_once 'clients/base/api/FilterApi.php';
            $configApi = new FilterApi();
            return $configApi->getFilterListCount($api, $args);
        } else {
            return $this->retrieveCaseRecord($api, $args);
        }
    }

    public function retrieveCaseRecord($api, $args)
    {
        $this->requireArgs($args, array('module', 'record'));

        $data = parent::retrieveRecord($api, $args);
        $this->fetchCommunication($api, $data);
        return $data;
    }

    private function fetchCommunication($api, array &$data)
    {
        require_once('custom/include/CaseChat/ChatHelper.php');
        $chatHelper = new ChatHelper();
        $data['valid_license'] = true;
        $messageList = $chatHelper->fetchComment(1, $data['id']);
        $data['customerMessages'] = $messageList['messageList'];
    }

    function loadMessage($api, $args)
    {
        require_once('custom/include/CaseChat/ChatHelper.php');
        $chatHelper = new ChatHelper();
        return $chatHelper->fetchComment($args['page'], $args['case_id']);
    }

    function saveMessage($api, $args)
    {
        unset($args['__dotb_url']);
        $comment = BeanFactory::newBean('C_Comments');
        foreach ($args as $field => $value) {
            $comment->$field = $value;
        }
        $this->moveTempFile($args['attachment_list'], $comment);
        $comment->save();
        if ($comment->direction == 'inbound') {
            $sqlContactsName = "SELECT IFNULL(full_student_name,'') full_student_name 
                                    FROM contacts 
                                    INNER JOIN contacts_cases_1_c ON contacts.id = contacts_cases_1_c.contacts_cases_1contacts_ida
                                    WHERE contacts_cases_1_c.contacts_cases_1cases_idb = '{$comment->parent_id}' AND contacts.deleted = 0";
            $personName = $GLOBALS['db']->getOne($sqlContactsName);;
        } else {
            $sqlUserName = "SELECT IFNULL(full_user_name,'') full_user_name FROM users WHERE id = '{$comment->created_by}'";
            $personName = $GLOBALS['db']->getOne($sqlUserName);
        }
        $dateEntered = TimeDate::getInstance()->fromDb($comment->date_entered);

        return [
            'success' => true,
            'id' => $comment->id,
            'message' => $comment->description,
            'direction' => $comment->direction,
            'person_name' => $personName,
            'date_entered' => TimeDate::getInstance()->asUser($dateEntered),
        ];
    }

    private function moveTempFile($attachment_list, $bean)
    {
        if (!empty($attachment_list)) {
            $_dir = 'upload/s3_storage';
            if (!file_exists($_dir)) mkdir($_dir, 0777, true);

            foreach ($attachment_list as $note_object) {
                if ($note_object['action'] == 'add' && !empty($note_object['id']) && !$note_object['is_saved']) {
                    $noteAdd = BeanFactory::getBean('Notes', $note_object['id']);
                    $noteAdd->parent_id = $bean->id;
                    $noteAdd->save();

                    if (file_exists(UploadStream::STREAM_NAME . "://tmp/" . $noteAdd->name)) {
                        $from = UploadStream::STREAM_NAME . "://tmp/" . $noteAdd->name;
                        $to = UploadStream::STREAM_NAME . "://s3_storage/" . $noteAdd->name;
                        UploadStream::move_temp_file($from, $to);
                    }
                } else if ($note_object['action'] == 'delete') {
                    $noteDel = BeanFactory::getBean('Notes', $note_object['id']);
                    $noteDel->mark_deleted($note_object['id']);
                    $noteDel->save();

                    if (file_exists(UploadStream::STREAM_NAME . "://s3_storage/" . $noteDel->name)) {
                        $source_file = 'upload/s3_storage/' . $noteDel->name;
                        unlink($source_file);
                    }
                }
            }
        }
    }

    public function pushNotification(ServiceBase $api, $args)
    {
        if (!empty($args['tittle'])) {
            foreach($this->getNotificationReceiver($args['module_id'], $args['module_name']) as $userId) {
                //Create new Notification
                $notify = BeanFactory::newBean("Notifications");
                $notify->id = create_guid();
                $notify->new_with_id = true;
                $notify->name = $args['tittle'];
                $notify->assigned_user_id = $userId;
                $notify->parent_id = $args['module_id'];
                $notify->parent_type = $args['module_name'];
                $notify->description = $args['description'];
                $notify->is_read = 0;
                $notify->severity = $args['severity'];
                $notify->save();
            }
            $this->triggerNotification($this->getNotificationReceiver($args['module_id'], $args['module_name']));
            return ['success' => true];
        }
    }
    private function getNotificationReceiver($parentId, $parentName){
        if($parentName == 'Bugs') {
            $bug = BeanFactory::getBean('Bugs', $parentId);
            return unencodeMultienum($bug->receive_noti_users);
        } else {
            //Send to admin
            return [0 => '1'];
        }
    }
    private function triggerNotification($userList){
        $url = 'https://socket.dotb.cloud/send-event-phenikaa';

        $headers = [
            'Content-Type: application/json'
        ];
        $body = json_encode([
            "to" => $GLOBALS['dotb_config']['unique_key'].'/pushNotification',
            "data" => [
                'userList' => $userList
            ]
        ]);
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return array (
            'status_code' => $http_code,
            'data' => json_decode($response, 1)
        );
    }
}