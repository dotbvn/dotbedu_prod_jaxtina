<?php
// created: 2019-04-18 19:01:55
$viewdefs['Cases']['portal']['view']['subpanel-for-j_class-j_class_cases_1'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'label' => 'LBL_LIST_NUMBER',
          'enabled' => true,
          'default' => true,
          'readonly' => true,
          'name' => 'case_number',
          'width' => 'xsmall',
        ),
        1 =>
        array (
          'label' => 'LBL_LIST_SUBJECT',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
          'type' => 'html',
          'width' => 'large',
        ),
        2 =>
        array (
          'label' => 'LBL_LIST_STATUS',
          'enabled' => true,
          'default' => true,
          'name' => 'status',
          'type' => 'html',
          'width' => 20,
        ),
        3 =>
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        4 =>
        array (
          'label' => 'LBL_LIST_DATE_CREATED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_entered',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);