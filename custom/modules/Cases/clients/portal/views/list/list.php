<?php
$viewdefs['Cases'] =
array (
  'portal' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'case_number',
                'label' => 'LBL_LIST_NUMBER',
                'default' => true,
                'link' => true,
                'enabled' => true,
                'readonly' => true,
                'width' => 'xsmall',
              ),
              1 =>
              array (
                'name' => 'name',
                'label' => 'LBL_LIST_SUBJECT',
                'link' => false,
                'default' => true,
                'enabled' => true,
                'width' => 'xlarge',
                'type' => 'html',
              ),
              2 =>
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'default' => true,
                'enabled' => true,
                'type' => 'html',
                'width' => 'small',
              ),
              3 =>
              array (
                'name' => 'source',
                'label' => 'LBL_SOURCE',
                'enabled' => true,
                'default' => true,
              ),
              4 =>
              array (
                'name' => 'j_class_cases_1_name',
                'label' => 'LBL_J_CLASS_CASES_1_FROM_J_CLASS_TITLE',
                'enabled' => true,
                'id' => 'J_CLASS_CASES_1J_CLASS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'contacts_cases_1_name',
                'label' => 'LBL_CONTACTS_CASES_1_FROM_CONTACTS_TITLE',
                'enabled' => true,
                'id' => 'CONTACTS_CASES_1CONTACTS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'default' => true,
                'enabled' => true,
                'readonly' => true,
              ),
              8 =>
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
