<?php

$viewdefs['Cases']['portal']['view']['panel-top-for-j_class_cases_1'] = array(
    'type' => 'panel-top',
    'template' => 'panel-top',
    'buttons' => array(
        array(
            'type' => 'button',
            'css_class' => 'btn',
            'icon' => 'fa fa-comments',
            'name' => 'create_button',
            'label' => 'LNK_NEW_CASE',
            'acl_action' => 'create',
            'tooltip' => 'LNK_NEW_CASE',
        ),

    ),

);



