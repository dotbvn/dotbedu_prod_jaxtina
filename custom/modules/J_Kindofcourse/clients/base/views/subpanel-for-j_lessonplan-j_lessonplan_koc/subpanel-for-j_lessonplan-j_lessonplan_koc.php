<?php
// created: 2023-09-04 16:19:46
$viewdefs['J_Kindofcourse']['base']['view']['subpanel-for-j_lessonplan-j_lessonplan_koc'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'default' => true,
          'enabled' => true,
          'name' => 'name',
          'link' => true,
          'type' => 'name',
        ),
        1 => 
        array (
          'name' => 'short_course_name',
          'label' => 'LBL_SHORT_COURSE_NAME',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'kind_of_course',
          'label' => 'LBL_KIND_OF_COURSE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'year',
          'label' => 'LBL_YEAR',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAMS',
          'enabled' => true,
          'id' => 'TEAM_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'default' => true,
          'enabled' => true,
          'name' => 'date_modified',
          'type' => 'datetime',
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);