<?php


$viewdefs['J_Kindofcourse']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_KINDOFCOURSE',
			'tooltip' => 'LNK_NEW_J_KINDOFCOURSE',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_Kindofcourse',
            'route'=>'#J_Kindofcourse/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_KINDOFCOURSE_REPORTS',
			'tooltip' => 'LNK_J_KINDOFCOURSE_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_Kindofcourse',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);