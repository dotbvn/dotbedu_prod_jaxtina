<?php
$module_name = 'J_Kindofcourse';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'xlarge',
              ),
              1 =>
              array (
                'name' => 'short_course_name',
                'label' => 'LBL_SHORT_COURSE_NAME',
                'enabled' => true,
                'default' => true,
              ),
              2 =>
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
              ),
              3 =>
              array (
                'name' => 'kind_of_course',
                'label' => 'LBL_KIND_OF_COURSE',
                'enabled' => true,
                'default' => true,
              ),
              4 =>
              array (
                'name' => 'year',
                'label' => 'LBL_YEAR',
                'enabled' => true,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              6 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
              8 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'kind_of_course,name',
          'direction' => 'asc',
        ),
      ),
    ),
  ),
);
