<?php
function removeSyllabus($json_syl, &$koc_bean,&$jsons_level){
    // Remove Level
    $koc_content = json_decode(html_entity_decode($koc_bean->content), true);
    $before_levels = array_column($koc_content, 'levels');
    $after_levels = array_column($jsons_level, 'levels');
    $remove_levels = array_diff($before_levels, $after_levels);
    foreach($remove_levels as $key => $item) {
        $lessonplan = BeanFactory::getBean('J_LessonPlan', $koc_content[$key]['lessonplan_id']);
        $lessonplan->mark_deleted($lessonplan->id);
    }

    // Remove Syllabus
    $ids = array();
    foreach ($after_levels as $key => $level) {
        if (!empty($json_syl[$level])) {
            $lessonplan_id = $koc_content[$key]['lessonplan_id'];
            $q = "SELECT IFNULL(j_syllabus.id, '') primary_id
            FROM j_syllabus WHERE j_syllabus.lessonplan_id = '{$lessonplan_id}' AND j_syllabus.deleted = 0";
            $rows = $GLOBALS['db']->fetchArray($q);
            $before_sylls = array_column($rows, 'primary_id');
            $after_sylls = array_column($json_syl[$level], 'primary_id');
            $remove_sylls = array_diff($before_sylls, $after_sylls);
            foreach ($before_sylls as $syll){
                if (in_array($syll, $remove_sylls) || empty($syll['theme'])) {
                    $ids[] = $syll;
                }
            }
            if (count($after_sylls) == 1 && empty($json_syl[$level][1]['theme'])) {
                $jsons_level[$level]->lessonplan_id = '';
                $lessonplan = BeanFactory::getBean('J_LessonPlan', $lessonplan_id);
                $lessonplan->mark_deleted($lessonplan->id);
            }
        }
    }
    if (count($ids) > 0) {
        $extIds = implode("','", $ids);
        $GLOBALS['db']->query("UPDATE j_syllabus SET deleted = 1, date_modified='{$GLOBALS['timedate']->nowDb()}',modified_user_id='{$GLOBALS['current_user']->id}'
        WHERE deleted = 0 AND id IN ('{$extIds}')");
        $koc_bean->content = json_encode(array_values($jsons_level), JSON_UNESCAPED_UNICODE);
    }
}

function addSyllabus($syllabus,$levels, &$koc_bean, &$jsons_level){
    foreach ($levels as $level){
        foreach($syllabus[$level] as $syll) {
            if (!empty($syll['theme'])) {
                $lesson_plan_id = $jsons_level[$level]->lessonplan_id;
                if (empty($lesson_plan_id)) {
                    // Create new J_LessonPlan
                    $lesson_plan_bean = BeanFactory::getBean('J_LessonPlan');
                    $lesson_plan_bean->name = $koc_bean->name . ' - ' . $level;
                    $lesson_plan_bean->assigned_user_id = $koc_bean->assigned_user_id;
                    $lesson_plan_bean->team_id = $koc_bean->team_id;
                    $lesson_plan_bean->team_set_id = $koc_bean->team_set_id;
                    $lesson_plan_bean->save();
                    $lesson_plan_id = $lesson_plan_bean->id;
                    // Update koc content
                    $jsons_level[$level]->lessonplan_id = $lesson_plan_id;
                }

                $syllabus_bean = BeanFactory::getBean('J_Syllabus', $syll['primary_id']);
                $syllabus_bean->lessonplan_id = $lesson_plan_id;
                $syllabus_bean->lesson = $syll['lesson'];
                $syllabus_bean->lesson_type = $syll['type'];
                $syllabus_bean->learning_type = $syll['learning_type'];
                $syllabus_bean->description = $syll['content'];
                $syllabus_bean->name = $syll['theme'];
                $syllabus_bean->homework = $syll['homework'];
                $syllabus_bean->note_for_teacher = $syll['objective'];
                $syllabus_bean->save();
            }
        }
    }
    $koc_bean->content = json_encode(array_values($jsons_level), JSON_UNESCAPED_UNICODE);
}

function updateSyllabus(&$koc_bean,$syllabus,$array_level){
    $jsons_level = array();
    foreach ($array_level as $key => $json) {
        if ($key > 0) {
            $level = json_decode(html_entity_decode($json));
            $jsons_level[$level->levels] = $level;
        }
    }
    $json_syl = array();
    $index_level = '#####';
    $levels = array();
    foreach ($syllabus as $json) {
        if (!empty($json)) {
            $sysll = json_decode(html_entity_decode($json), true);
            foreach ($sysll as $level => $value) {
                if($level != $index_level && array_search($level,$levels) == false) {
                    $index_level = $level;
                    array_push($levels, $level);
                }
                if (empty($value['lesson']))
                    continue;
                $json_syl[$level][$value['lesson']] = $value;
            }
        }
    }
    removeSyllabus($json_syl, $koc_bean, $jsons_level);
    addSyllabus($json_syl, $levels, $koc_bean, $jsons_level);
}
