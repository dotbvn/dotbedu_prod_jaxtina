<?php

class J_KindofcourseViewDetail extends ViewDetail
{
    function display()
    {
        //get list levels to array
        $tmpContent = json_decode(html_entity_decode($this->bean->content), true);
        $lms        = json_decode(html_entity_decode($this->bean->lms_content), true);
        $levelConfigHtml = '<style>
        .tbl_level{
        width: 100%;
        border: solid 0.5px #555;
        border-spacing: 0px;
        border-collapse: collapse;
        }
        .tbl_level thead th {
        background-color: #eee;
        border: solid 0.5px #555;
        padding: 7px !important;
        }
        .tbl_level tbody td {
        border: solid 0.5px #555;
        padding: 10px !important;
        background-color: #ffffff;
        text-align:center;
        }
        </style>';

        $levelConfigHtml .= '<table class="tbl_level"><thead><tr>';
        $levelConfigHtml .= '<th width="15%">' . translate('LBL_LEVEL', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="15%">' . translate('LBL_SHORT_COURSE_NAME', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="5%">' . translate('LBL_HOURS', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="5%">' . translate('LBL_IS_SET_HOUR', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="5%">' . translate('LBL_IS_UPGRADE', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="10%">' . translate('LBL_TEACHING_RATE', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="15%">' . translate('LBL_TIME_LOT', 'J_Kindofcourse') . '</th>';
        //        $levelConfigHtml .= '<th width="15%">' . translate('LBL_URL_DOCUMENT', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="15%">' . translate('LBL_GB_SETTING_GROUP', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '<th width="15%">' . translate('LBL_LESSONPLAN', 'J_Kindofcourse') . '</th>';
        if($lms['enable']) $levelConfigHtml .= '<th width="20%" nowrap>' . translate('LBL_LMS_CONTENT', 'J_Kindofcourse') . '</th>';
        $levelConfigHtml .= '</tr><tbody>';
        foreach ($tmpContent as $key => $value) {
            // Get Level Config
            $levelConfigHtml .= '<tr>';
            $levelConfigHtml .= '<td>' . (empty($value['levels']) ? '-none-' : $value['levels']) . '</td>';
            $levelConfigHtml .= '<td>' . $value['level_code'] . '</td>';
            $levelConfigHtml .= '<td>' . format_number($value['hours'],4,4). '</td>';
            $levelConfigHtml .= '<td>' . (($value['is_set_hour'] == '1') ? '<input type="checkbox" name="is_set_hour" checked disabled>' : '<input type="checkbox" name="is_set_hour" disabled>') . ' </td>';
            $levelConfigHtml .= '<td>' . (($value['is_upgrade'] == '1') ? '<input type="checkbox" name="is_upgrade" checked disabled>' : '<input type="checkbox" name="is_upgrade" disabled>') . ' </td>';
            $levelConfigHtml .= '<td>' . (empty($value['teaching_rate']) ? '1.00' : $value['teaching_rate']) . '</td>';
            $levelConfigHtml .= '<td nowrap>' . format_number($value['time_lot']) .  ' '. translate('LBL_MIN_PER_SESSION', 'J_Kindofcourse').'</td>';
            //    $levelConfigHtml .= '<td style="text-align:center">' . (empty($value['doc_url']) ? '' : '<a target=\'_blank\' href="'.$value['doc_url'].'">>>link<<</a>' ). '</td>';

            $value['gbsettinggroup_name'] = $GLOBALS['db']->getOne("SELECT name FROM j_gradebooksettinggroup WHERE id = '{$value['gbsettinggroup_id']}' AND deleted = 0");
            if(empty($value['gbsettinggroup_name'])) $value['gbsettinggroup_id'] = '';
            $levelConfigHtml .= '<td>' . (empty($value['gbsettinggroup_id']) ? '' : '<a href="/#bwc/index.php?module=J_GradebookSettingGroup&action=DetailView&record='.($value['gbsettinggroup_id']).'"> '.$value['gbsettinggroup_name'].' </a>' ). '</td>';

            $value['lessonplan_name'] = $GLOBALS['db']->getOne("SELECT name FROM j_lessonplan WHERE id = '{$value['lessonplan_id']}' AND deleted = 0");
            if(empty($value['lessonplan_name'])) $value['lessonplan_id'] = '';
            $levelConfigHtml .= '<td>' . (empty($value['lessonplan_id']) ? '' : '<a href="#bwc/index.php?module=J_LessonPlan&action=DetailView&record='.($value['lessonplan_id']).'"> '.$value['lessonplan_name'].' </a>' ). '</td>';

            if($lms['enable']){
                require_once("custom/include/utils/lms_helpers.php");
                $res = getLMSConfig();
                $url = 'https://'.$res->apiHost."/courses/".$value['lms_template_id'];
                $levelConfigHtml .= '<td>'.'<a href="'.$url.'" target="_blank" title="'.$url.'"> '.$lms['courses'][$value['lms_template_id']]['name'].' </a>'. '</td>';
            }
            $levelConfigHtml .= '</tr>';
        }
        $syllHtml .= '</tbody></table>';
        $levelConfigHtml .= '</tbody></table>';
        $this->ss->assign('LEVEL_CONFIG', $levelConfigHtml);

        parent::display();
    }
}

?>
