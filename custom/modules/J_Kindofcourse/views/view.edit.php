<?php

use Dotbcrm\Dotbcrm\Security\InputValidation\InputValidation;
require_once ('include/externalAPI/ClassIn/ExtAPIClassIn.php');


class J_KindofcourseViewEdit extends ViewEdit
{
    const MAX_BOOK_NAME = 3;

    public function preDisplay()
    {
        parent::preDisplay();
    }

    public function display()
    {
        //Load Canvas Blueprint Course
        require_once("custom/include/utils/lms_helpers.php");
        $lms = getBlueprintCourse();
        $this->ss->assign('lms', $lms);
        $this->ss->assign('lms_enable', $lms['enable'] ? 1 : 0);

        //Format by Tuan Anh to show syllabus table in each level config row
        //Chèn một dòng display none trên cùng
        $levelItems = generateEmptyRow(false, 0, $lms);

        //In case Create
        if (empty($this->bean->id))
            $this->bean->year = date('Y');


        if (empty($this->bean->content) || $this->bean->content == '[]') {
            // Nếu content trống thì generate một dòng trống
            $levelItems .= generateEmptyRow(true, 1, $lms);

        } else {
            // Nếu có content thì generate cho content
            $content = json_decode(html_entity_decode($this->bean->content), true);
            $level_lp = array_column($content, 'lessonplan_id');

            foreach ($content as $key => $value) {
                $ssLevelItem = new Dotb_Smarty();
                $ssLevelItem->assign("DISPLAY", "");
                $ssLevelItem->assign("DISPLAY_SYL",$_POST['isDuplicate'] == 'true'?'1':'0' );
                $ssLevelItem->assign("LEVEL", $value['levels']);
                $ssLevelItem->assign("level_code", $value['level_code']);
                if (empty($value['teaching_rate']) || !isset($value['teaching_rate'])) $value['teaching_rate'] = '1';
//                if (empty($value['revenue_rate']) || !isset($value['revenue_rate'])) $value['revenue_rate'] = '1';
                $ssLevelItem->assign("TEACHING_RATE_OPTIONS", get_select_options_with_id($GLOBALS['app_list_strings']['teaching_rate_list'], $value['teaching_rate']));
                $ssLevelItem->assign("TIME_SLOT", $value['time_lot']);
                $ssLevelItem->assign("DRIVE_OPTIONS", get_select_options_with_id($GLOBALS['app_list_strings']['kind_of_course_drive_type'], $value['doc_type']));
//                $ssLevelItem->assign("REVENUE_RATE_OPTIONS", get_select_options_with_id($GLOBALS['app_list_strings']['revenue_rate_list'], $value['revenue_rate']));
                $ssLevelItem->assign("HOURS", $value['hours']);
        //        $ssLevelItem->assign("URL_DOCUMENT", $value['doc_url']);

                $value['gbsettinggroup_name'] = $GLOBALS['db']->getOne("SELECT name FROM j_gradebooksettinggroup WHERE id = '{$value['gbsettinggroup_id']}' AND deleted = 0");
                if(empty($value['gbsettinggroup_name'])) $value['gbsettinggroup_id'] = '';

                $value['lessonplan_name'] = $GLOBALS['db']->getOne("SELECT name FROM j_lessonplan WHERE id = '{$value['lessonplan_id']}' AND deleted = 0");
                if(empty($value['lessonplan_name'])) $value['lessonplan_id'] = '';

                $ssLevelItem->assign("gbsettinggroup_id", $value['gbsettinggroup_id']);
                $ssLevelItem->assign("gbsettinggroup_name", $value['gbsettinggroup_name']);
                $ssLevelItem->assign("lessonplan_id", $value['lessonplan_id']);
                $ssLevelItem->assign("lessonplan_name", $value['lessonplan_name']);


                $ssLevelItem->assign("IS_UPGRADE", $value['is_upgrade']);
                $ssLevelItem->assign("IS_SET_HOUR", $value['is_set_hour']);
                $ssLevelItem->assign("JSON", json_encode($value));
                $indexL = $key + 1;
                $ssLevelItem->assign("ID_TARGET", $indexL);
                $sylItems = getSyllabusRow(false);
                if($_POST['isDuplicate'] == 'true') {
                    //Display syllabus nếu có
                    if (empty($level_lp[$key])) {
                        $sylItems .= getSyllabusRow(true, '1');
                    } else {
                        $rows = getSyllabus($level_lp[$key]);
                        foreach ($rows as $row) {
                            $syll[$value['levels']] = $row;
                            $sylItems .= getSyllabusRow(true , $row['lesson'], $row['type'], $row['learning_type'], $row['theme'], $row['homework'], $row['objective'], $row['content'], '', json_encode($syll));
                        }
                    }
                    $value['lessonplan_id'] = '';
                    $ssLevelItem->assign("lessonplan_id", $value['lessonplan_id']);
                    $ssLevelItem->assign("JSON", json_encode($value));
                }
                $ssLevelItem->assign("TBODYSYL", $sylItems);
                $ssLevelItem->assign("MOD", $GLOBALS['mod_strings']);

                $lms['selected'][$indexL] = $value['lms_template_id'];
                $ssLevelItem->assign("lms", $lms);

                //Assign ClassIn config status
                $classInConfig = ExtAPIClassIn::hasAPIConfig();
                $ssLevelItem->assign("classin", $classInConfig);

                $levelItems .= $ssLevelItem->fetch('custom/modules/J_Kindofcourse/tpls/levelConfigItem.tpl');
            }
        }

        // Assign tpl level config
        $ssLevelConfig = new Dotb_Smarty();
        $ssLevelConfig->assign("TBODY", $levelItems);
        $ssLevelConfig->assign("MOD", $GLOBALS['mod_strings']);
        $ssLevelConfig->assign("lms", $lms);
        $levelConfigHtml = $ssLevelConfig->fetch('custom/modules/J_Kindofcourse/tpls/levelConfig.tpl');

        $this->ss->assign('LEVEL_CONFIG', $levelConfigHtml);
        //End
        parent::display();
    }
}

// Generate Add row template
function generateEmptyRow($show, $idTarget, $lms)
{
    $ssLevelItem = new Dotb_Smarty();
    $sylItems = getSyllabusRow(false);
    $sylItems .= getSyllabusRow(true, '1');

    if ($show) $ssLevelItem->assign("DISPLAY", "");
    else $ssLevelItem->assign("DISPLAY", "style='display:none;'");
    $ssLevelItem->assign("TBODYSYL", $sylItems);
    $ssLevelItem->assign("MOD", $GLOBALS['mod_strings']);
    $ssLevelItem->assign("LEVEL", '');
    $ssLevelItem->assign("TEACHING_RATE_OPTIONS", get_select_options_with_id($GLOBALS['app_list_strings']['teaching_rate_list'], '1'));
    $ssLevelItem->assign("DRIVE_OPTIONS", get_select_options_with_id($GLOBALS['app_list_strings']['kind_of_course_drive_type'], ''));
//    $ssLevelItem->assign("REVENUE_RATE_OPTIONS", get_select_options_with_id($GLOBALS['app_list_strings']['revenue_rate_list'], '1'));
    $ssLevelItem->assign("IS_UPGRADE", '1');
    $ssLevelItem->assign("IS_SET_HOUR", '1');
    $ssLevelItem->assign("lessonplan_id", '');
    $ssLevelItem->assign("JSON", "");
    $ssLevelItem->assign('lms', $lms);
    $ssLevelItem->assign("ID_TARGET", $idTarget);
    //Assign ClassIn config status
    $classInConfig = ExtAPIClassIn::hasAPIConfig();
    $ssLevelItem->assign("classin", $classInConfig);

    return $ssLevelItem->fetch('custom/modules/J_Kindofcourse/tpls/levelConfigItem.tpl');
}

function getSyllabusRow($show, $lesson = '', $type = '', $learning_type = '', $theme = '', $homeWorks = '', $objective = '', $content = '', $primary_id = '', $json = '')
{
    if (!$show)
        $display = 'style="display:none;"';
    $tpl_addrow = "<tr class='rowSyllabus' $display>";
    $tpl_addrow .= '<td scope="col" align="center">';
    $tpl_addrow .= "<input name='json_syl[]' value='$json' class='json_syl' type='hidden'/>";
    $tpl_addrow .= '<input name="sys_lesson[]" value="' . $lesson . '" class="sys_lesson" type="text" style="width: 70px;text-align: center;" db-data="100"></td>';
    $tpl_addrow .= '<td align="center">
    <select class="sys_type" name="sys_type" rows="1" cols="25">
    '.get_select_options_with_id($GLOBALS['app_list_strings']['syllabus_type_list'],$type).'
    </select></td>';
    $tpl_addrow .= '<td align="center">
    <select class="learning_type" name="learning_type" rows="1" cols="25">
    '.get_select_options_with_id($GLOBALS['app_list_strings']['learning_type_list'],$learning_type).'
    </select></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_theme" name="sys_theme" rows="1" cols="25">' . $theme . '</textarea></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_content" name="sys_content" rows="1" cols="25">' . $content . '</textarea></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_objective" name="sys_objective" rows="1" cols="25">' . $objective . '</textarea></td>';
    $tpl_addrow .= '<td align="center"><textarea onkeyup="textAreaAdjust(this)" onclick="textAreaAdjust(this)" class="sys_homework" name="sys_homework" rows="1" cols="25">' . $homeWorks . '</textarea></td>';
    $tpl_addrow .= '<td><input name="sys_primary_id" class="sys_primary_id" type="hidden" value="' . $primary_id. '">
                    <button type="button" onclick="removeSyllabus(this)"  class="btnRemoveSyl">
                    <img src="themes/default/images/id-ff-remove-nobg.png" alt="Remove"></button></td>';
    $tpl_addrow .= '</tr>';
    return $tpl_addrow;
}
