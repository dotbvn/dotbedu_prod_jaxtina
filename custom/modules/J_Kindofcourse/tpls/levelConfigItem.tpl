<tr class='row_tpl' class="accordion-toggle" {$DISPLAY}>

    <td align="center">
        <input name="levels[]" value="{$LEVEL}" class="levels" type="text" style="width: 150px;">
    </td>
    <td align="center">
        <input name="level_code[]" value="{$level_code}" class="level_code code" type="text" style="width: 70px;text-transform: uppercase;">
    </td>
    <td nowrap align="center">
        <input name="hours[]" value="{dotb_number_format var=$HOURS precision=4}" class="hours" type="text" style="width: 120px; text-align: center;">
    </td>
    <td align="center">
        {if $IS_SET_HOUR == '1'}
            <input type="checkbox" name="is_set_hour" class="is_set_hour" checked="" value="{$IS_SET_HOUR}">
        {else}
            <input type="checkbox" name="is_set_hour" class="is_set_hour" value="{$IS_SET_HOUR}">
        {/if}
    </td>
    <td align="center">
        {if $IS_UPGRADE == '1'}
            <input type="checkbox" name="is_upgrade" class="is_upgrade" checked="" value="{$IS_UPGRADE}">
        {else}
            <input type="checkbox" name="is_upgrade" class="is_upgrade" value="{$IS_UPGRADE}">
        {/if}
    </td>
    <td align="center">
        <select name="teaching_rate[]" class="teaching_rate">
            {$TEACHING_RATE_OPTIONS}
        </select>
    </td>
    <td align="center">
        <input name="time_lot[]" value="{dotb_number_format var=$TIME_SLOT}" class="time_lot" type="text" style="width: 70px; text-align: center;"> {$MOD.LBL_MIN_PER_SESSION}
    </td>
<!--    <td nowrap>
        <select name="doc_type[]" class="doc_type">
            {$DRIVE_OPTIONS}
        </select>
        <input name="doc_url[]" value="{$URL_DOCUMENT}" class="doc_url" type="text">
    </td> -->
    <td valign="top" nowrap>
        <input type="text" name="gbsettinggroup_name[]" class="gbsettinggroup_name" class="sqsEnabled" value="{$gbsettinggroup_name}" title="" autocomplete="off"/>
        <input type="hidden" name="gbsettinggroup_id[]" class="gbsettinggroup_id" value="{$gbsettinggroup_id}"/>
        <span class="id-ff multiple">
        <button type="button" name="btn_gbsettinggroup_name" class="btn_gbsettinggroup_name" tabindex="0" title="{$APPS.LBL_LINK_SELECT} {$MOD.LBL_GB_SETTING_GROUP}" class="button firstChild" value="Select User" onclick=""><img src="themes/default/images/id-ff-select.png"></button>
        <button type="button" name="btn_clr_gbsettinggroup_name" style="margin-left: -5px;" class="btn_clr_gbsettinggroup_name" tabindex="0" title="{$APPS.LBL_CLEAR_BUTTON_LABEL} {$MOD.LBL_GB_SETTING_GROUP}" class="button lastChild" onclick=""><img src="themes/default/images/id-ff-clear.png"></button>
        </span>
    </td>
    <td valign="top" nowrap>
        <input type="text" name="lessonplan_name[]" class="lessonplan_name" class="sqsEnabled" value="{$lessonplan_name}" title="" autocomplete="off"/>
        <input type="hidden" name="lessonplan_id[]" class="lessonplan_id" value="{$lessonplan_id}"/>
        <span class="id-ff multiple">
        <button type="button" name="btn_lessonplan_name" class="btn_lessonplan_name" tabindex="0" title="{$APPS.LBL_LINK_SELECT} {$MOD.LBL_LESSONPLAN}" class="button firstChild" value="Select Lesson Plan" onclick=""><img src="themes/default/images/id-ff-select.png"></button>
        <button type="button" name="btn_clr_lessonplan_name" style="margin-left: -5px;" class="btn_clr_lessonplan_name" tabindex="0" title="{$APPS.LBL_CLEAR_BUTTON_LABEL} {$MOD.LBL_LESSONPLAN}" class="button lastChild" onclick=""><img src="themes/default/images/id-ff-clear.png"></button>
        </span>
    </td>
    {if $lms.enable}<td>{html_options style="width:125px" name="lms_template_id" class="lms_template_id" options=$lms.options selected=$lms.selected.$ID_TARGET}</td>{/if}
    <td align='center'>
        <input name='jsons[]' value='{$JSON}' class='jsons' type='hidden'/>
        <button type='button' class='btn btn-danger btnRemove btnRemoveSyl' style="padding: 2px;"><img
                    src='themes/default/images/id-ff-remove-nobg.png' alt='Remove'></button>
    </td>
</tr>