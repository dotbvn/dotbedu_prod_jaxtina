<link rel="stylesheet" href="{dotb_getjspath file='styleguide/assets/css/fontawesome.min.css'}" type="text/css">
{dotb_getscript file="custom/include/javascript/jQuery/jquery-1.7.1.js"}
{dotb_getscript file="custom/modules/J_Kindofcourse/js/classindriveview.js"}
{literal}
    <style>

        ul[role="tree"] {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        [role="treeitem"][aria-expanded="false"] > ul {
            display: none;
        }

        [role="treeitem"][aria-expanded="true"] > ul {
            display: block;
        }

        /*[role="treeitem"][aria-expanded="false"] > span::before {*/
        /*    content: url(../images/closed.png);*/
        /*}*/

        /*[role="treeitem"][aria-expanded="true"] > span::before {*/
        /*    content: url(../images/open.png);*/
        /*}*/

        [role="treeitem"],
        [role="treeitem"] span {
            /*width: 9em;*/
            margin: 0;
            padding: 0.125em;
            border: 2px transparent solid;
            display: block;
            cursor: pointer;
        }

        /* disable default keyboard focus styling for treeitems
           Keyboard focus is styled with the following CSS */
        [role="treeitem"]:focus {
            outline: 0;
        }

        /*[role="treeitem"].focus,*/
        [role="treeitem"] span.focus {
            border-color: black;
            border-width: thin;
            background-color: #eee;
        }

        [role="treeitem"].hover,
        [role="treeitem"] span:hover {
            background-color: #ddd;
        }

        .root-tree:hover {
            background-color: #ddd;
        }

        .root-tree.focus {
            border-color: black;
            border-width: thin;
            background-color: #eee;
        }

        .content {
            padding-top: 0;
            padding-bottom: 10px;
            margin-left: 20px;
            margin-right: 20px;
            font-family: "SFUIText", Verdana, Helvetica, sans-serif;
            font-size: 13px;
            color: #666;
            font-weight: normal;
        }

        .moduleTitle {
            line-height: 2em;
            padding-bottom: 3px;
            padding-top: 0;
            margin-bottom: 10px;
            margin-top: 0;
        }

        .button {
            display: inline-block;
            padding: 4px 6px;
            margin-bottom: 0;
            font-size: 11px;
            line-height: 18px;
            color: #555;
            font-weight: bold;
            text-align: center;
            vertical-align: middle;
            background-color: #f6f6f6;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
            *margin-left: .3em;
            appearance: auto;
            user-select: none;
            white-space: pre;
            align-items: flex-start;
        }

        .primary {
            background-color: #176de5;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            color: #ffffff !important;
        }

        .button:focus {
            background-color: rgba(0, 0, 0, 0.1);
            outline: rgba(0, 0, 0, 0.1);
        }

        #last_action {
            width: 10rem;
        }

        .body {
            border: 1px solid #AFAFAE;
        }

        .add-button {
            z-index: 2000;
            cursor: pointer;
        }

        .add-button:hover {
            color: #176de5;
            resize: both;
        }
    </style>
{/literal}
<div class="content">
    <h3 id="tree_label" class="moduleTitle">
        {$MOD.LBL_LINK_SELECT} {$MOD.LBL_CLASSIN_DRIVE}
    </h3>
    <div class="action-buttons">
        <input id="save-button" class="button primary" title="Save" value="Save" readonly/>
        <input id="cancel-button" class="button" title="Cancel" value="Cancel" readonly/>
    </div>
    <p>
        <label>
            {$MOD.LBL_SELECTED_FOLDER}
            <input id="last_action"
                   type="text"
                   size="15"
                   readonly="" data-value="">
        </label>
    </p>
    <div class="body">
        <div class="row-fluid">
            <li role="treeitem" aria-expanded="false" data-value="{$ROOT.id}" data-name="{$ROOT.name}" class="root-tree root-folder">
                <i class="fa fa-plus add-button" aria-hidden="true" style="display: none"></i>
                <i class="far fa-folder-open" aria-hidden="true"></i>
                Root
            </li>
            <li style="list-style-type: none; display: none" class="create_folder">
                <input type="text" style="width: 100%" class="create_folder_input"/>
                <span class="id-ff multiple">
                    <button type="button" name="create_folder_btn" class="create_folder_btn button" tabindex="0">Create</button>
                    <button type="button" name="cancel_folder_btn" class="cancel_folder_btn button" tabindex="0">Cancel</button>
                </span>
            </li>

        </div>

        <ul role="tree" aria-labelledby="tree_label" style="margin-left: 1rem">
            {foreach from=$ROOT_FOLDERS key=key item=value}
                <li role="treeitem" aria-expanded="false" data-value="{$value.id}" data-name="{$value.name}">
                        <span>
                          <i class="far fa-folder" aria-hidden="true"></i>
                            {$value.name}
                        </span>
                </li>
            {/foreach}
        </ul>
    </div>
</div>
