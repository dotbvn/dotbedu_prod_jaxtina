{dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.js"}
{literal}
<style>
    .ui-sortable tr {
        cursor:move;
    }
    #tblLevelConfig tbody tr {
        background-color: aliceblue;
    }
    #tblLevelConfig thead th {
        background-color: transparent;
    }
    #tblLevelConfig td {
        border: solid 2px transparent;
        border-style: solid none;
        padding: 10px;

    }

    #tblLevelConfig td:first-child {
        border-left-style: solid;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }
    #tblLevelConfig td:last-child {
        border-right-style: solid;
        border-bottom-right-radius: 10px;
        border-top-right-radius: 10px;
    }
</style>
{/literal}
{*<table id="tblLevelConfig" width="80%" border="1" class="list view">*}
<table  id="tblLevelConfig" width="100%" class="table table-condensed" style="border-collapse:collapse;">
    <thead>
    <tr>
        <th width="10%" style="text-align: center;">{$MOD.LBL_LEVEL}</th>
        <th width="20%" style="text-align: center;">{$MOD.LBL_LEVEL_CODE}</th>
        <th width="10%" style="text-align: center;">{$MOD.LBL_HOURS}<span class="required">*</span></th>
        <th width="10%" style="text-align: center;">{$MOD.LBL_IS_SET_HOUR} </th>
        <th width="10%" style="text-align: center;">{$MOD.LBL_IS_UPGRADE} </th>
        <th width="20%" style="text-align: center;">{$MOD.LBL_TEACHING_RATE} </th>
        <th width="20%" style="text-align: center;">{$MOD.LBL_TIME_LOT}<span class="required">*</span></th>
        <th width="20%" style="text-align: center;">{$MOD.LBL_GB_SETTING_GROUP} </th>
        <th width="5%" style="text-align: center;">{$MOD.LBL_LESSONPLAN}</th>
        {if $lms.enable}
        <th width="5%" style="text-align: center;">{$MOD.LBL_LMS_CONTENT}</th>
        {/if}
        <th width="5%" style="text-align: center;">
            <button class="button" type="button" id="btnAddrow">+</button>
        </th>
    </tr>
    </thead>
    <tbody id="tbodylLevelConfig">
    {$TBODY}
    </tbody>
</table>
