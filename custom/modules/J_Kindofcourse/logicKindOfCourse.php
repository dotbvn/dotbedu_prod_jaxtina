<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicKindOfCourse
{
    //before save
    function handleSave($bean, $event, $arguments)
    {
        if ($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save') {
            $jsons_level = array();
            foreach ($_POST["jsons"] as $key => $json) {
                if ($key > 0) {
                    $level = json_decode(html_entity_decode($json));
                    if(!empty($level->time_lot) && !empty($level->hours))
                        $jsons_level[$level->levels] = $level;
                }
            }
            // Update class session
            $o_content = json_decode(html_entity_decode($bean->content));
            $n_content = array_values($jsons_level);
            $before_lp = array_column($o_content, 'lessonplan_id');
            $after_lp = array_column($jsons_level, 'lessonplan_id');
            $diff_lp = array_diff($after_lp, $before_lp) + array_diff($before_lp, $after_lp);

            require_once("custom/include/_helper/junior_revenue_utils.php");
            foreach($diff_lp as $key => $item) {
                $o_lessonplan_id = $o_content[$key]->lessonplan_id;
                $n_lessonplan_id = $n_content[$key]->lessonplan_id;
                $level = $o_content[$key]->levels;

                $qClass = "SELECT IFNULL(j_class.id, '') class_id FROM j_class
                LEFT JOIN j_lessonplan ON j_class.lessonplan_id = j_lessonplan.id AND j_lessonplan.deleted = 0
                WHERE j_class.koc_id = '{$bean->id}' AND j_class.level = '$level'
                AND j_class.lessonplan_id = '$o_lessonplan_id' AND j_class.deleted = 0";
                $resClass = $GLOBALS['db']->fetchArray($qClass);
                $classes = array_column($resClass, 'class_id');
                $classIds = implode("', '", $classes);
                $GLOBALS['db']->query("UPDATE j_class SET lessonplan_id = '$n_lessonplan_id' WHERE id IN ('$classIds') AND deleted = 0");

                foreach ($resClass as $rClass) {
                    updateClassSyllabus($rClass['class_id'], $n_lessonplan_id);
                }
            }

            // Update koc content
            $bean->content = json_encode(array_values($jsons_level), JSON_UNESCAPED_UNICODE);
            $bean->short_course_name = mb_strtoupper($bean->short_course_name, "UTF-8");
        }
        $_POST['team_set_id'] = $bean->fetched_row['team_set_id'];
    }

    //after save
    function addTeam($bean, $event, $arguments)
    {
        //Check MassUpdate by Lumia
        if (!empty($_REQUEST['__dotb_url'])) {
            $post = explode("/", $_REQUEST['__dotb_url']);
            $_POST['module'] = $post[1];
            $_POST['action'] = $post[2];
        }
        if ($_POST['module'] == $bean->module_name && ($_POST['action'] == 'Save' || $_POST['action'] == 'MassUpdate')) {
            $bean->name = str_replace(' | ', ' - ', $bean->name);
            if ($_POST['team_set_id'] != $bean->team_set_id) {
                $team_list = array();
                // Get all team set
                $teamSetBean = new TeamSet();
                $teams = $teamSetBean->getTeams($bean->team_set_id);
                // Add all team set to  $team_list
                foreach ($teams as $key => $value) {
                    $team_list[] = $key;
                }
                // Add children of team set to $team_list
                foreach ($teams as $key => $value) {
                    // Get children of team
                    $q1 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$key}'";
                    $rs1 = $GLOBALS['db']->query($q1);

                    while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
                        if (!isset($teams[$row['id']])) $team_list[] = $row['id'];
                        $q2 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$row['id']}'";
                        $rs2 = $GLOBALS['db']->query($q2);
                        while ($row2 = $GLOBALS['db']->fetchByAssoc($rs2))
                            if (!isset($teams[$row['id']])) $team_list[] = $row2['id'];
                    }
                }

                if (!empty($team_list)) {
                    $bean->load_relationship('teams');
                    //Add the teams
                    $bean->teams->replace($team_list);
                }
            }
        }
    }

    function listViewColor(&$bean, $event, $arguments)
    {
        switch ($bean->status) {
            case "Active":
                $bean->status = '<span class="textbg_dream">' . $bean->status . '</span>';
                break;
            case "Inactive":
                $bean->status = '<span class="textbg_crimson">' . $bean->status . '</span>';
                break;
        }

    }
}

?>
