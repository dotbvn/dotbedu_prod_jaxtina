$(document).ready(function () {

    let folderList;

    //Load ClassIn drive folders
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Kindofcourse&action=handelAjaxsKindOfCourse&dotb_body_only=true",
        async: true,
        data:
            {
                type: 'ajaxGetClassInDrive',
            },
        dataType: "json",
        success: function (data) {
            folderList = data;
        }
    });

    $('.root-folder').on('click', function(e){
        const folderId = $(this).attr('data-value');
        const folderName = $(this).attr('data-name');
        setInputValue(folderName, folderId);
        $('span').removeClass('focus'); //remove tất cả các span đã focus
        $(this).addClass('focus');

        if(e.target.className==="fa fa-plus add-button"){
            let liCreateFolder = $(this).closest('div').find('.create_folder');
            liCreateFolder.css('display', '');
            let input = liCreateFolder.find('input');
            input.val('New folder');
        }
    });

    $('.cancel_folder_btn').live('click', function (e){
        let liElement = $(this).closest('li');
        liElement.css('display', 'none');
    });

    $('.create_folder_btn').live('click', function (e){
        let folderName = $(this).closest('li').find('input').val();
        let parentFolderId = $(this).closest('div').find('li[role="treeitem"]').attr('data-value');
        //Ajax create ClassIn folder
        $.ajax({
            type: "POST",
            url: "index.php?module=J_Kindofcourse&action=handelAjaxsKindOfCourse&dotb_body_only=true",
            async: true,
            data:
                {
                    type: 'ajaxCreateClassInFolder',
                    parentFolderId: parentFolderId,
                    folderName: folderName
                },
            dataType: "json",
            success: function (data) {
                if (data['success']){
                    const id = data['id'];
                    console.log(id);
                    // location.reload();
                }
                else{
                    alert('Can not create new folder.');
                }
            }
        });
    });

    $('li span').live('click', function (event) {
        event.stopPropagation();
        event.preventDefault();

        $('span').removeClass('focus'); //remove tất cả các span đã focus
        removeFocusRoot(); //remove focus ở root
        $(this).addClass('focus');

        let liEle = $(this).closest('li');
        const folderId = liEle.attr('data-value');
        const folderName = liEle.attr('data-name');

        const expanded = liEle.attr('aria-expanded');
        if (expanded === 'false'){
            insertTreeItems(folderId, liEle);
        }else{
            removeTreeItems(liEle);
        }

        setInputValue(folderName, folderId);
    });

    function insertTreeItems(folderId, targetDOM) {
        const children = getFolderChildren(folderId);
        let htmlString = "";
        if (children.length > 0) {
            setOpenFolderIcon(targetDOM);

            htmlString += `<ul role="group">`;

            for(const [key,value] of Object.entries(children)){
                htmlString += `<li role="treeitem" aria-expanded="false" data-value="${value.id}" data-name="${value.name}">
                                    <span><i class="far fa-folder" aria-hidden="true"></i>${value.name}</span>
                                </li>\n`;
            }
            htmlString += `</ul>`;
            targetDOM.append(htmlString);
            targetDOM.attr('aria-expanded', true);
        }
    }

    function removeTreeItems(target){
        let children = target.find('ul');
        children.remove();
        target.attr('aria-expanded', false);
        setCloseFolderIcon($(this));
    }

    function setCloseFolderIcon(target){
        let iconEle = target.find('i');
        iconEle.removeClass('fa-folder-open');
        iconEle.addClass('fa-folder');
    }

    function setOpenFolderIcon(target){
        let iconEle = target.find('i');
        iconEle.removeClass('fa-folder');
        iconEle.addClass('fa-folder-open');
    }

    function setInputValue(folderName, folderId){
        let inputElement = $("#last_action");
        inputElement.val (folderName);
        inputElement.attr("data-value", folderId);
    }

    function removeFocusRoot(){
        $('.root-folder').removeClass('focus');
    }

    // Click Save Button Handle
    document.getElementById('save-button').addEventListener('click', function (e) {
        const value = document.getElementById('last_action').getAttribute('data-value');
        try {
            window.opener.setDocumentFromClassInDrive(value);
        } catch (err) {
        }
        window.close();
        return false;
    });

    //Click Cancel Button Handle
    document.getElementById('cancel-button').addEventListener('click', function (e) {
        window.close();
        return false;
    });


    function getFolderChildren(parentId) {
        let ret = [];

        for(const [key,value] of Object.entries(folderList)){
            if (value.parentId === parentId) {
                ret.push(value);
            }
        }

        return ret;
    }
});
