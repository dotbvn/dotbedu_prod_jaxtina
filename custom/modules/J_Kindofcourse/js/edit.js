$(document).ready(function () {
    var record_id = $('input[name=record]').val();

    $('#tblLevelConfig').multifield({
        section: '.row_tpl', // First element is template
        addTo: '#tbodylLevelConfig', // Append new section to position
        btnAdd: '#btnAddrow', // Button Add id
        btnRemove: '.btnRemove', // Buton remove id
        prompt: false,  //confirm ?
    });
    dragTable();

    $('#kind_of_course').on('change', function () {
        $('#name').val($(this).val());
    });
    $('.hours').live('blur', function () {
        var val = Numeric.parse($(this).val());
        if(Number.isInteger(val) && val > 0)
            $(this).val(Numeric.toFloat(val));
        else if(val == 0){
            toastr.error(app.lang.get('LBL_CORRECT_FORMAT', 'J_Kindofcourse'));
            $(this).val('').effect("highlight", {color: '#FF0000'}, 2000);
        }else $(this).val(Numeric.toFloat(val,4,4));
    });

    $('.time_lot').live('blur', function () {
        var val = Numeric.parse($(this).val());
        if(Number.isInteger(val) && val > 0)
            $(this).val(Numeric.toFloat(val));
        else{
            toastr.error(app.lang.get('LBL_CORRECT_FORMAT', 'J_Kindofcourse'));
            $(this).val('').effect("highlight", {color: '#FF0000'}, 2000);
        }
    });

    $('.levels, .level_code, .hours, .is_upgrade, .is_set_hour, .teaching_rate, .revenue_rate, .time_lot, .doc_url, .doc_type, .gbsettinggroup_id, .lessonplan_id, .lms_template_id').live('change', function () {
        if (this.name == 'levels[]') {
            var level = this.value;
            if (level != '') {
                // $(this).closest('tr').attr('level', level);
                // $(this).closest('tr').next().attr('level', level);
                $(this).closest('tr').next().find('table').find('textarea').trigger('change');

            }
        }
        var row = $(this).closest('.row_tpl');
        saveJson(row);
    });

    $('.btn_gbsettinggroup_name').live('click', function(){
        clickGbstg($(this));
    });

    $('.btn_clr_gbsettinggroup_name').live('click',function(){
        $(this).closest('td').find('input').val('');
        $(this).closest('td').find('.gbsettinggroup_id').trigger('change');
    });

    $('.btn_lessonplan_name').live('click', function(){
        clickLpstg($(this));
    });

    $('.btn_clr_lessonplan_name').live('click',function(){
        $(this).closest('td').find('input').val('');
        $(this).closest('td').find('.lessonplan_id').trigger('change');
    });

    //Select ClassIn Drive
    $('.btn_selectClassInDrive').live('click',function(){
        clickClassInDrive($(this));
    });

    s2_tbl();
    $('#btn_reload_lms_course_template').live('click',function(){
        app.alert.show('message-id', {
            level: 'confirmation',
            messages: DOTB.language.get('J_Kindofcourse', 'LBL_RELOAD_LMS_COURSE_TEMPLATE_ALERT'),
            autoClose: false,
            onConfirm: function () {
                DOTB.ajaxUI.showLoadingPanel();
                $.ajax({
                    type: "POST",
                    url : "index.php?module=J_Kindofcourse&action=handelAjaxsKindOfCourse&dotb_body_only=true",
                    data:  {
                        type   : "ajaxReloadCourseTemplate",
                    },
                    dataType: "json",
                    success:function(data){
                        DOTB.ajaxUI.hideLoadingPanel();
                        if (data.success == "1") {
                            toastr.success(DOTB.language.get('J_Kindofcourse', 'LBL_RELOAD_LMS_COURSE_TEMPLATE_SUCCESS')+' '+ data.message);
                            const separateObject = obj => {
                                const res = [];
                                Object.keys(obj).map(key => {
                                    if (key.length !== 0) {
                                        res.push({
                                            id: key,
                                            text: obj[key],
                                        });
                                    }
                                })
                                return res;
                            };
                            var lmsOptions = data.lms['options']
                            var select2Options = separateObject(lmsOptions)
                            select2Options.unshift({id: "", text: "-none-"})
                            $('select.lms_template_id').each(function() {
                                var selectedVal = $(this).val();
                                $(this).html('').select2({data: select2Options});
                                $(this).val(selectedVal);
                                $(this).trigger('change');
                            })
                        }
                        else toastr.success(DOTB.language.get('J_Kindofcourse', 'LBL_RELOAD_LMS_COURSE_TEMPLATE_ERROR'));
                    },
                });
            },
            onCancel: function () {
                return false;
            }
        });
    });
});

var selectedButton;

//CLASSIN
function clickClassInDrive(thisButton){
    selectedButton = thisButton;
    window.open(window.location.protocol + '//' + window.location.hostname + '/index.php?module=J_Kindofcourse&action=classindrive&dotb_body_only=1','popup','width=600,height=600');
}

function setDocumentFromClassInDrive(folderId){
    selectedButton.closest('td').find('.doc_url').val(folderId);
}

function clickGbstg(thisButton){
    thisBtn = thisButton;
    open_popup('J_GradebookSettingGroup', 1000, 700, "", true, false, {"call_back_function":"set_gbstg_return","form_name":"EditView","field_to_name_array":{"id":"gbsettinggroup_id","name":"gbsettinggroup_name"}}, "single", true);
};

function set_gbstg_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'gbsettinggroup_name':
                    //trigger change
                    thisBtn.closest('td').find('.gbsettinggroup_name').val(val);
                    break;
                case 'gbsettinggroup_id':
                    thisBtn.closest('td').find('.gbsettinggroup_id').val(val).trigger('change');
                    break;
            }
        }
    }
}

function clickLpstg(thisButton){
    thisBtn = thisButton;
    let currentLpId = thisBtn.closest('td').find('.lessonplan_id').val();
    let kocId = $('input[name=record]').val();
    open_popup("J_LessonPlan", 1000, 700, "&current_lp_id=" + currentLpId + "&target_module=J_Kindofcourse&target_id=" + kocId, true, true, {
        "call_back_function": "setLessonPlanReturn",
        "form_name": "EditView",
        "field_to_name_array": {
            "id": "lessonplan_id",
            "name": "lessonplan_name"
        },
    }, "single", true);
}

function setLessonPlanReturn(popup_reply_data){
    var name_to_value_array = popup_reply_data.name_to_value_array;

    for (var the_key in name_to_value_array) {
        if (the_key === 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key) {
                case 'lessonplan_name':
                    // trigger change
                    thisBtn.closest('td').find('.lessonplan_name').val(val);
                    break;
                case 'lessonplan_id':
                    thisBtn.closest('td').find('.lessonplan_id').val(val).trigger('change');
                    break;
                default:
            }
        }
    }
}

function dragTable() {
    //Helper function to keep table row from collapsing when being sorted
    var fixHelperModified = function (e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function (index) {
            $(this).width($originals.eq(index).width());
        });
        return $helper;
    };

    //Make diagnosis table sortable
    $("#tblLevelConfig tbody").sortable({
        helper: fixHelperModified
    });
}

// Lưu json để đưa xuống database
function saveJson(row) {
    json = {};
    json.levels = row.find('.levels').val();
    json.level_code = row.find('.level_code').val().toUpperCase();
    json.hours = row.find('.hours').val();
    json.doc_url = row.find('.doc_url').val();
    json.doc_type = row.find('.doc_type').val();
    json.lms_template_id = row.find('.lms_template_id').val();
    json.gbsettinggroup_id = row.find('.gbsettinggroup_id').val();
    json.teaching_rate = row.find('.teaching_rate').val();
    json.revenue_rate = row.find('.revenue_rate').val();
    json.time_lot = row.find('.time_lot').val();
    json.lessonplan_id = row.find('.lessonplan_id').val();
    if (row.find('.is_upgrade').is(":checked"))
        json.is_upgrade = '1';
    else json.is_upgrade = '0';

    if (row.find('.is_set_hour').is(":checked"))
        json.is_set_hour = '1';
    else json.is_set_hour = '0';
    console.log(json);
    var json_str = JSON.stringify(json);
    //Assign json
    row.find("input.jsons").val(json_str);
}

//Overwrite check_form to validate
function check_form(formname) {
    //Validate level config
    var validateConfig = true;
    $('.hours,.time_lot').each(function () {
        if (($(this).val() == '' || $(this).val() <= 0)
        && $(this).closest("tr").is(":visible")
        && ($(this).closest("tr").find('.is_set_hour').is(":checked"))) {
            $(this).effect("highlight", {color: '#FF0000'}, 2000);
            validateConfig = false;
        }
    });
    // check special character  Code
    var validateSpecial = true;
    $('.code').each(function () {
        if($(this).val() != ''){
            var reg_exp = /^[A-Za-z0-9]+$/;
            var is_valid = reg_exp.test($(this).val());
            if (!is_valid) {
                $(this).effect("highlight", {color: '#FF0000'}, 2000);
                toastr.error('Invalid Level code (No special characters)!!');
                validateSpecial = false;
            }
        }

    });
    return validate_form(formname, '') && validateConfig && validateSpecial;
}

function checkDataTarget() {

    var index = $('#tbodylLevelConfig>tr:last').prev().find('div').attr('id');
    return parseInt(index + 1);
}
function s2_tbl(){
    $('#tbodylLevelConfig > tr').not(':first').find('.lms_template_id').each(function(index, tr) {
        $(this).select2({
            width: 'resolve' // need to override the changed default
        });
    });
}
function handleAddRow(_tbl) {
    s2_tbl();
    if (_tbl.attr('id') === 'tblLevelConfig') {
        var tbSyslabus = _tbl.find('tbody tr:nth-child(1)').first().clone();
        tbSyslabus.find('.accordian-body').attr('id', checkDataTarget());

        _tbl.find('tr:last').find('.accordion-toggle').attr('data-target', '#' + checkDataTarget());
        $('#tbodylLevelConfig').append(tbSyslabus);
    }
}
function handleRemoveRow(_tbl) {
    $(_tbl).next('tr').detach();
}

function displayRemoveBtn(section) {
    if (section.find('tbody tr').length > 2) {
        section.find('tbody tr').each(function () {
            $(this).find('td:last>button').show();
        });
    } else {
        section.find('tbody tr').each(function () {
            $(this).find('td:last>button').hide();
        });
    }

}