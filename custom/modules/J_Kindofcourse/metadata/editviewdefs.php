<?php
$module_name = 'J_Kindofcourse';
$viewdefs[$module_name] =
array(
    'EditView' =>
    array(
        'templateMeta' =>
        array(
            'maxColumns' => '2',
            'javascript' => '{dotb_getscript file="custom/modules/J_Kindofcourse/js/edit.js"}
            {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
            <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>

            ',
            'form' =>
            array (
                'enctype' => 'multipart/form-data',
                'hidden' =>
                array (
                    1 => '<input type="hidden" name="lms_content" value=\'{$lms.content}\'>',
                ),
                'buttons' =>
                array (
                    0 => 'SAVE',
                    1 => 'CANCEL',
                    2 =>
                    array (
                        'customCode' => '{if $lms.enable}<input type="button" class="button" id="btn_reload_lms_course_template" value="{$MOD.LBL_RELOAD_LMS_COURSE_TEMPLATE}"/>{/if}',
                    ),
                ),
            ),
            'widths' =>
            array(
                0 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array(
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array(
                'DEFAULT' =>
                array(
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array(
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array(
            'default' =>
            array(
                0 =>
                array(
                    0 =>
                    array(
                        'name' => 'kind_of_course',
                        'studio' => 'visible',
                        'label' => 'LBL_KIND_OF_COURSE',
                        'customCode' => '{html_options name="kind_of_course" id="kind_of_course" options=$fields.kind_of_course.options selected=$fields.kind_of_course.value}'
                    ),
                    1 =>
                    array(
                        'name' => 'status',
                        'studio' => 'visible',
                        'label' => 'LBL_STATUS',
                    ),
                ),
                1 =>
                array(
                    0 => 'name',
                    1 =>
                    array(
                        'name' => 'short_course_name',
                        'label' => 'LBL_SHORT_COURSE_NAME',
                        'customCode' => '<input type="text" name="short_course_name" class="code" id="short_course_name" value="{$fields.short_course_name.value}" size="10" style="text-transform: uppercase;" maxlength="10">',
                    ),
                ),
                2 =>
                array (
                    0 => 'max_size',
                    1 => ''/*array(
                    'name' => 'lms_content',
                    'customLabel' => '{if $lms_enable}{$MOD.LBL_LMS_CONTENT}{/if}',
                    'customCode' => '{if $lms_enable}{$MOD.LBL_LMS_CONTENT}{/if}',
                    ),*/
                ),
                3 =>
                array(
                    0 =>
                    array(
                        'name' => 'content',
                        'label' => 'LBL_CONTENT',
                        'customCode' => '{$LEVEL_CONFIG}',
                    ),
                ),

                4 =>
                array(
                    0 => array(
                        'name' => 'description',
                        'displayParams' =>
                        array(
                            'rows' => 4,
                            'cols' => 60,
                        ),
                    ),
                    1 =>
                    array(
                        'name' => 'year',
                        'studio' => 'visible',
                        'label' => 'LBL_YEAR',
                    ),
                ),
            ),
            'lbl_editview_panel1' =>
            array(
                0 =>
                array(
                    0 => 'assigned_user_name',
                    1 =>
                    array(
                        'name' => 'team_name',
                        'displayParams' =>
                        array(
                            'display' => true,
                        ),
                    ),
                ),
            ),
        ),
    ),
);
