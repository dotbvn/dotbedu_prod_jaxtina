<?php
$module_name = 'J_Kindofcourse';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                ),
            ),
            'javascript' => '{dotb_getscript file="custom/modules/J_Kindofcourse/js/detail.js"}',
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'default' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'kind_of_course',
                        'studio' => 'visible',
                        'label' => 'LBL_KIND_OF_COURSE',

                    ),
                    1 =>
                    array (
                        'name' => 'status',
                        'studio' => 'visible',
                        'label' => 'LBL_STATUS',
                    ),
                ),
                1 =>
                array (
                    0 => 'name',
                    1 =>
                    array (
                        'name' => 'short_course_name',
                        'label' => 'LBL_SHORT_COURSE_NAME',
                    ),

                ),
                2 =>
                array (
                    0 => 'max_size',
                    '',
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'content',
                        'label' => 'LBL_CONTENT',
                        'customCode' => '{$LEVEL_CONFIG}',
                    ),
                ),
                4 =>
                array (
                    0 => 'description',
                    1 =>
                    array (
                        'name' => 'year',
                        'studio' => 'visible',
                        'label' => 'LBL_YEAR',
                    ),
                ),
            ),
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 => 'assigned_user_name',
                    1 => 'team_name',
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
        ),
    ),
);
