<?php
switch ($_REQUEST['type']) {
    case 'ajaxGetClassInDrive':
        $result = ajaxGetClassInDrive();
        break;
    case 'ajaxCreateClassInFolder':
        $result = ajaxCreateClassInFolder();
        break;
    case 'ajaxReloadCourseTemplate':
        $result = ajaxReloadCourseTemplate();
        break;
}
echo $result;
die;

function ajaxGetClassInDrive()
{
    //    $result = J_KindofcourseViewClassindrive::getOtherFolders();
    require_once ('include/externalAPI/ClassIn/utils.php');

    $result = array();
    $otherFolderListRes = getDriveFolderList();
    if ($otherFolderListRes['success'] == true){
        foreach ($otherFolderListRes['data'] as $parent => $children){
            if ($parent !== 0) { // '0' is root id, but root folder is stored in another variable
                foreach ($children as $child) {
                    $folderTemp = array();
                    $folderTemp['id'] = $child['id'];
                    $folderTemp['parentId'] = $child['pid'];
                    $folderTemp['name'] = $child['name'];

                    $result[] = $folderTemp;
                }
            }
        }
    }
    return json_encode($result);
}

function ajaxCreateClassInFolder(){
    require_once ('include/externalAPI/ClassIn/utils.php');
    $folderName = $_POST['folderName'];
    $parentId = $_POST['parentFolderId'];
    $response = createNewFolder($folderName, $parentId);
    $result = array();
    if ($response['success']){
        $response['success'] = true;
        $response['id'] = $response['data'];
    }else{
        $response['success'] = false;
    }

    return json_encode($result);
}



function ajaxReloadCourseTemplate(){
    require_once("custom/include/utils/lms_helpers.php");
    $lms = getBlueprintCourse(false);
    return json_encode(array(
        "success" => 1,
        "message" => '<br> Total: '. (count($lms['options']) - 1) .' LMS Course Templates.',
        "lms" => $lms,
    ));
}
?>
