<?php


$viewdefs['J_School']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_SCHOOL',
			'tooltip' => 'LNK_NEW_J_SCHOOL',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_School',
            'route'=>'#J_School/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_SCHOOL_REPORTS',
			'tooltip' => 'LNK_J_SCHOOL_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_School',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);