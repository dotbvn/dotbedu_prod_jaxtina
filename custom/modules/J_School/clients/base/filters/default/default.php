<?php
// created: 2023-06-19 17:20:08
$viewdefs['J_School']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'level' => 
    array (
    ),
    'billing_address_street' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'tag' => 
    array (
    ),
  ),
);