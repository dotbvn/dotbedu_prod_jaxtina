<?php
$hook_version = 1;
$hook_array = Array();
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1, '', 'custom/modules/Calls/logicCJCall.php','logicCJCall', 'handleCJAfterSave');
$hook_array['after_save'][] = Array(2, '', 'custom/modules/Calls/logicHandle.php','logicHandle', 'handleAfterSave');

$hook_array['before_save'] = Array();

$hook_array['before_save'][] = Array(1, '', 'custom/modules/Calls/logicHandle.php','logicHandle', 'handleBeforeSave');

$hook_array['before_save'][] = Array(200, '', 'custom/modules/Calls/logicImport.php','logicImportCall', 'importCall');
$hook_array['after_retrieve'] = Array();
$hook_array['after_retrieve'][] = Array(1, '', 'custom/modules/Calls/logicHandle.php','logicHandle', 'handleAfterRetrieve');
$hook_array['process_record'] = array();
$hook_array['after_retrieve'][] = Array(2, 'Handle duration retrieve', 'custom/modules/Calls/logicHandle.php', 'logicHandle', 'HandleRetrieveDuration');
$hook_array['process_record'][] = Array(2, 'Handle duration', 'custom/modules/Calls/logicHandle.php', 'logicHandle', 'HandleRetrieveDuration');

