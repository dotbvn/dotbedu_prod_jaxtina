<?php
class logicImportCall{
    //Import Payment
    function importCall(&$bean, $event, $arguments){
        global $timedate;
        if($_POST['module'] == 'Import'){
            //Get Student ID
            $phone_number = substr(trim($bean->phone_mobile), -9);
            $q1 = "SELECT id FROM contacts WHERE deleted = 0 AND (full_student_name LIKE '%{$bean->student_name}%' AND phone_mobile LIKE '%$phone_number%')";
            $rs = $GLOBALS['db']->query($q1);
            $row = $GLOBALS['db']->fetchByAssoc($rs);
            if(!empty($row['id'])){
                $bean->parent_type = 'Contacts';
                $student = BeanFactory::getBean('Contacts', $row['id']);
            }

            else{
                //check Lead convert to Student
                $q2 = "SELECT id FROM leads WHERE deleted = 0 AND (full_lead_name LIKE '%{$bean->student_name}%' AND phone_mobile LIKE '%$phone_number%')";
                $rs2 = $GLOBALS['db']->query($q2);
                $row = $GLOBALS['db']->fetchByAssoc($rs2);

                if (!empty($row['id'])){
                    $student = BeanFactory::getBean('Leads', $row['id']);
                    $bean->parent_type = 'Leads';
                }

            }

            if(empty($student->id)){
                $bean->deleted = 1;
            }

            $bean->parent_id    = $student->id;

            if($bean->parent_type == 'Leads'){
                $bean->load_relationship('leads');
                $bean->leads->add($student->id);
            }else{
                $bean->load_relationship('contacts');
                $bean->contacts->add($student->id);
            }
            $bean->parent_id        = $student->id;
            $bean->duration_hours   = 0;
            $bean->duration_minutes = 15;
            $bean->direction        = 'Outbound';
            $bean->status           = 'Held';
            $bean->email_reminder_time = -1;
            $bean->reminder_time    = -1;
            $bean->update_vcal      = false;
            $bean->send_invites     = false;
            if(empty($bean->assigned_user_id))
                $bean->assigned_user_id = $student->assigned_user_id;
            $bean->created_by       = $bean->assigned_user_id;
            $bean->modified_user_id = $bean->assigned_user_id;
            $bean->date_created     = $bean->date_start;
            $bean->date_modified    = $bean->date_start;

        }
    }
}
?>
