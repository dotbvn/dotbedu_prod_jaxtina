<?php

/*********************************************************************************
 * Portions created by DotBCRM are Copyright (C) DotBCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

$module_name = 'J_Workpermit';
$subpanel_layout = array(
    'top_buttons' => array(
        array('widget_class' => 'SubPanelTopCreateButton'),
        array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name,),
    ),
    'where' => '',

    'list_fields' => array(
        'object_image' => array(
            'widget_class' => 'SubPanelIcon',
            'width' => '2%',
            'image2' => 'attachment',
            'image2_url_field' => array('id_field' => 'selected_revision_id', 'filename_field' => 'selected_revision_filename'),
            'attachment_image_only' => true,

        ),
        'document_name' => array(
            'name' => 'document_name',
            'vname' => 'LBL_DOCUMENT_NAME',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => 'auto',
        ),
        'status' => array(
            'name' => 'status',
            'vname' => 'LBL_STATUS',
            'width' => 'auto',
        ),
        'start_date' => array(
            'name' => 'start_date',
            'vname' => 'LBL_START_DATE',
            'width' => 'auto',
        ),
        'end_date' => array(
            'name' => 'end_date',
            'vname' => 'LBL_END_DATE',
            'width' => 'auto',
        ),
    ),
);
