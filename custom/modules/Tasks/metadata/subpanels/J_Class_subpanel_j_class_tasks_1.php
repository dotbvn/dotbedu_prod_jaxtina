<?php
    // created: 2019-10-17 05:00:08
    $subpanel_layout['list_fields'] = array (
        'name' =>
        array (
            'vname' => 'LBL_LIST_SUBJECT',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => 10,
            'default' => true,
        ),
        'parent_name' =>
        array (
            'vname' => 'LBL_LIST_RELATED_TO',
            'width' => 10,
            'target_record_key' => 'parent_id',
            'target_module_key' => 'parent_type',
            'widget_class' => 'SubPanelDetailViewLink',
            'sortable' => false,
            'default' => true,
        ),
        'status' =>
        array (
            'widget_class' => 'SubPanelActivitiesStatusField',
            'vname' => 'LBL_LIST_STATUS',
            'width' => 10,
            'default' => true,
        ),
        'date_start' =>
        array (
            'type' => 'datetimecombo',
            'studio' =>
            array (
                'required' => true,
                'no_duplicate' => true,
            ),
            'vname' => 'LBL_START_DATE',
            'width' => 10,
            'default' => true,
        ),
        'date_due' =>
        array (
            'type' => 'datetimecombo',
            'studio' =>
            array (
                'required' => true,
                'no_duplicate' => true,
            ),
            'vname' => 'LBL_DUE_DATE',
            'width' => 10,
            'default' => true,
        ),
        'description' =>
        array (
            'type' => 'text',
            'vname' => 'LBL_DESCRIPTION',
            'sortable' => false,
            'width' => 10,
            'default' => true,
        ),
        'assigned_user_name' =>
        array (
            'link' => true,
            'type' => 'relate',
            'vname' => 'LBL_ASSIGNED_TO',
            'id' => 'ASSIGNED_USER_ID',
            'width' => 10,
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => 'Users',
            'target_record_key' => 'assigned_user_id',
        ),
        'team_name' =>
        array (
            'type' => 'relate',
            'link' => true,
            'studio' =>
            array (
                'portallistview' => false,
                'portalrecordview' => false,
            ),
            'vname' => 'LBL_TEAMS',
            'id' => 'TEAM_ID',
            'width' => 10,
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => 'Teams',
            'target_record_key' => 'team_id',
        ),
        'parent_id' =>
        array (
            'usage' => 'query_only',
        ),
        'parent_type' =>
        array (
            'usage' => 'query_only',
        ),
        'filename' =>
        array (
            'usage' => 'query_only',
            'force_exists' => true,
        ),
        'edit_button'=>array(
            'vname' => 'LBL_EDIT_BUTTON',
            'widget_class' => 'SubPanelEditButton',
            'width' => '2%',
        ),
        'remove_button'=>array(
            'vname' => 'LBL_REMOVE',
            'widget_class' => 'SubPanelRemoveButton',
            'width' => '2%',
        ),
);