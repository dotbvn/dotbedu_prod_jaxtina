<?php
// created: 2023-05-26 16:56:55
$viewdefs['Tasks']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'parent_name' => 
    array (
    ),
    'status' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_start' => 
    array (
    ),
    'date_due' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'task_duration' => 
    array (
    ),
    'j_class_tasks_1_name' => 
    array (
    ),
    'priority' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'description' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);