({
    extendsFrom: 'EventStatusField',
    initialize: function (options) {
        this._super('initialize', [options]);
        this.statusClasses = {
            //status
            'Not Started': 'label-pending',
            'Pending Input': 'textbg_bluelight',
            'In Progress': 'textbg_blue',
            'Deferred': 'textbg_crimson',
            'Completed': 'textbg_green',
            'Not Applicable': 'textbg_gray',
            //Priority
            'High': 'label-important',
            'Medium': 'label-warning',
            'Low': 'label-info'
        };

        this.type = 'badge-select';
    },
})