<?php
// created: 2023-06-05 01:47:46
$viewdefs['Tasks']['base']['view']['subpanel-for-contacts-custom_tasks_link'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_SUBJECT',
          'link' => true,
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
          'width' => 'xxlarge',
        ),
        2 => 
        array (
          'name' => 'status',
          'label' => 'LBL_LIST_STATUS',
          'type' => 'event-status',
          'enabled' => true,
          'default' => true,
          'width' => 'small',
        ),
        3 => 
        array (
          'name' => 'date_start',
          'label' => 'LBL_LIST_START_DATE',
          'css_class' => 'overflow-visible',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'date_due',
          'label' => 'LBL_LIST_DUE_DATE',
          'type' => 'datetimecombo-colorcoded',
          'completed_status_value' => 'Completed',
          'link' => false,
          'css_class' => 'overflow-visible',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
          'id' => 'ASSIGNED_USER_ID',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_start',
    'direction' => 'desc',
  ),
  'rowactions' => 
  array (
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-search-plus',
        'acl_action' => 'view',
      ),
      1 => 
      array (
        'type' => 'rowaction',
        'name' => 'edit_button',
        'icon' => 'fa-pencil',
        'label' => 'LBL_EDIT_BUTTON',
        'event' => 'list:editrow:fire',
        'css_class' => 'btn',
        'acl_action' => 'edit',
      ),
      2 => 
      array (
        'type' => 'unlink-action',
        'icon' => 'fa-unlink',
        'tooltip' => 'LBL_UNLINK_BUTTON',
        'css_class' => 'btn',
      ),
      3 => 
      array (
        'type' => 'closebutton',
        'name' => 'record-close',
        'dismiss_label' => true,
        'tooltip' => 'LBL_CLOSE_BUTTON_TITLE',
        'closed_status' => 'Completed',
        'acl_action' => 'edit',
        'css_class' => 'btn',
        'icon' => 'fa-times',
      ),
    ),
  ),
  'type' => 'subpanel-list',
);