<?php
$viewdefs['Tasks'] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'record' =>
            array (
                'buttons' =>
                array (
                    0 =>
                    array (
                        'type' => 'button',
                        'name' => 'cancel_button',
                        'label' => 'LBL_CANCEL_BUTTON_LABEL',
                        'tooltip' => 'LBL_CANCEL_BUTTON_LABEL',
                        'css_class' => 'btn customRecordViewButton',
                        'icon' => 'fa-window-close',
                        'showOn' => 'edit',
                        'events' =>
                        array (
                            'click' => 'button:cancel_button:click',
                        ),
                    ),
                    1 =>
                    array (
                        'type' => 'rowaction',
                        'event' => 'button:save_button:click',
                        'name' => 'save_button',
                        'label' => 'LBL_SAVE_BUTTON_LABEL',
                        'tooltip' => 'LBL_SAVE_BUTTON_LABEL',
                        'icon' => 'fa-save',
                        'customButton' => true,
                        'css_class' => 'btn btn-primary',
                        'showOn' => 'edit',
                        'acl_action' => 'edit',
                    ),
                    2 =>
                    array (
                        'type' => 'actiondropdown',
                        'name' => 'main_dropdown',
                        'primary' => true,
                        'showOn' => 'view',
                        'buttons' =>
                        array (
                            0 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:edit_button:click',
                                'name' => 'edit_button',
                                'tooltip' => 'LBL_EDIT_BUTTON_LABEL',
                                'label' => 'LBL_EDIT_BUTTON_LABEL',
                                'icon' => 'fa-pencil',
                                'customButton' => true,
                                'primary' => true,
                                'acl_action' => 'edit',
                            ),
                            6 =>
                            array (
                                'type' => 'closebutton',
                                'name' => 'record-close',
                                'label' => 'LBL_COMPLETE_BUTTON_TITLE',
                                'closed_status' => 'Completed',
                                'acl_action' => 'edit',
                            ),
                            7 =>
                            array (
                                'type' => 'rowaction',
                                'name' => 'duplicate_button',
                                'event' => 'button:duplicate_button:click',
                                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                                'acl_module' => 'Tasks',
                                'acl_action' => 'create',
                            ),
                            8 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:audit_button:click',
                                'name' => 'audit_button',
                                'label' => 'LNK_VIEW_CHANGE_LOG',
                                'acl_action' => 'view',
                            ),
                            9 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:delete_button:click',
                                'name' => 'delete_button',
                                'label' => 'LBL_DELETE_BUTTON_LABEL',
                                'acl_action' => 'delete',
                            ),
                        ),
                    ),
                ),
                'panels' =>
                array (
                    0 =>
                    array (
                        'name' => 'panel_header',
                        'header' => true,
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'picture',
                                'type' => 'avatar',
                                'size' => 'large',
                                'dismiss_label' => true,
                                'readonly' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'name',
                            ),
                            2 =>
                            array (
                                'name' => 'favorite',
                                'label' => 'LBL_FAVORITE',
                                'type' => 'favorite',
                                'dismiss_label' => true,
                            ),
                            3 =>
                            array (
                                'name' => 'status',
                                'type' => 'event-status',
                                'enum_width' => 'auto',
                                'dropdown_width' => 'auto',
                                'dropdown_class' => 'select2-menu-only',
                                'container_class' => 'select2-menu-only',
                            ),
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'panel_body',
                        'label' => 'LBL_RECORD_BODY',
                        'columns' => 2,
                        'labelsOnTop' => true,
                        'placeholders' => true,
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'parent_name',
                                'requried' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'priority',
                            ),
                            2 =>
                            array (
                                'name' => 'date_start',
                            ),
                            3 =>
                            array (
                                'name' => 'j_class_tasks_1_name',
                                'label' => 'LBL_J_CLASS_TASKS_1_FROM_J_CLASS_TITLE',
                            ),
                            4 =>
                            array (
                                'name' => 'date_due',
                            ),
                            5 =>
                            array (
                                'name' => 'assigned_user_name',
                            ),
//                            6 =>
//                            array (
//                                'name' => 'remind_email',
//                                'label' => 'LBL_REMINDER_EMAIL',
//                            ),
//                            7 =>
//                            array (
//                                'name' => 'remind_popup',
//                                'label' => 'LBL_REMINDER_POPUP',
//                            ),
                            8 =>
                            array (
                                'name' => 'description',
                                'span' => 12,
                            ),
                            9 =>
                            array (
                                'name' => 'date_entered_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_ENTERED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_entered',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'created_by_name',
                                    ),
                                ),
                            ),
                            10 => 'team_name',
                            11 =>
                            array (
                                'name' => 'date_modified_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_MODIFIED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_modified',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'modified_by_name',
                                    ),
                                ),
                            ),
                            12 =>
                            array (
                                'name' => 'customer_journey_type',
                            ),
                        ),
                    ),
                ),
                'templateMeta' =>
                array (
                    'useTabs' => false,
                ),
            ),
        ),
    ),
);
