<?php
$module_name = 'J_PaymentDetail';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    1 =>
                    array (
                        'customCode' => '{$BTN_CUSTOM}',
                    ),
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'default' =>
            array (

                array (
                    0 =>
                    array (
                        'name' => 'parent_name',
                        'studio' => 'visible',
                        'label' => 'LBL_STUDENT_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'parent_type',
                        'studio' => 'visible',
                        'label' => 'LBL_STUDENT_TYPE',
                    ),
                ),

                array (
                    0 => 'name',
                    1 => 'status',
                ),

                array (
                    0 =>
                    array (
                        'name' => 'payment_name',
                        'label' => 'LBL_PAYMENT_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'payment_date',
                        'label' => 'LBL_PAYMENT_DATE',
                    ),
                ),

                array (
                    0 =>
                    array (
                        'name' => 'before_discount',
                        'label' => 'LBL_BEFORE_DISCOUNT',
                    ),
                    1 =>
                    array (
                        'name' => 'payment_datetime',
                        'comment' => 'Receipt Datetime',
                        'label' => 'LBL_PAYMENT_DATETIME',
                    ),
                ),

                array (
                    0 =>
                    array (
                        'name' => 'discount_amount',
                        'label' => 'LBL_TOTAL_DISCOUNT_SPONSOR',
                    ),
                    1 => 'expired_date',
                ),

                array (
                    0 =>
                    array (
                        'name' => 'payment_amount',
                    ),
                    1 =>
                    array (
                        'name' => 'payment_method',
                        'studio' => 'visible',
                        'label' => 'LBL_PAYMENT_METHOD',
                    ),
                ),

                array (
                    0 => '',
                    1 => 'card_type',
                ),

                array (
                    0 =>
                    array (
                        'name' => 'inv_code',
                    ),
                    1 => 'pos_code',
                ),

                array (
                    0 =>
                    array (
                        'name' => 'sale_type',
                        'label' => 'LBL_SALE_TYPE',
                    ),
                    1 => 'bank_name'
                ),

                array (
                    0 =>
                    array (
                        'name' => 'sale_type_date',
                        'label' => 'LBL_SALE_TYPE_DATE',
                    ),
                    1 => 'bank_account'
                ),

                array (
                    0 => 'description',
                    1 =>
                    array (
                        'name' => 'audited_amount',
                        'customLabel' => '{$MOD.LBL_AUDITED_AMOUNT}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_AUDITED_AMOUNT_DES}\');" src="themes/RacerX/images/helpInline.png" alt="auditedAmountHelpTip" class="auditedAmountHelpTip">',

                    ),
                ),

                array (
                    0 => 'note',
                    1 =>
                    array (
                        'name' => 'reconcile_status',
                        'label' => 'LBL_RECONCILE_STATUS',
                    ),
                ),

                array (
                    0 => 'assigned_user_name',
                    1 =>
                    array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),

                array (
                    0 => 'team_name',
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
        ),
    ),
);
