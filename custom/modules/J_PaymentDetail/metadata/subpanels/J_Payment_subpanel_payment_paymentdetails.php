<?php
    // created: 2015-11-09 16:00:25
    $subpanel_layout['where'] = "(j_paymentdetail.status <> 'Cancelled')";
    $subpanel_layout['list_fields'] = array (
        'payment_no' =>
        array (
            'vname' => 'LBL_PAYMENT_NO',
            'widget_class' => 'SubPanelDetailViewLink',
            'width' => '3%',
            'default' => true,
            'link' => true,
        ),
        'name' =>
        array (
            'vname' => 'LBL_NAME',
            'type' => 'varchar',
            'width' => '9%',
            'default' => true,
        ),
        'payment_method' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'vname' => 'LBL_PAYMENT_METHOD',
            'width' => '9%',
            'link' => true,
        ),
        'payment_amount' =>
        array (
            'type' => 'currency',
            'default' => true,
            'vname' => 'LBL_PAYMENT_AMOUNT',
            'currency_format' => true,
            'width' => '10%',
        ),
        'payment_date' =>
        array (
            'type' => 'date',
            'default' => true,
            'studio' => 'visible',
            'vname' => 'LBL_PAYMENT_DATE',
            'width' => '9%',
        ),
        'expired_date' =>
        array (
            'type' => 'date',
            'default' => true,
            'studio' => 'visible',
            'vname' => 'LBL_EXPIRED_DATE',
            'width' => '9%',
        ),
        'description' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'vname' => 'LBL_DESCRIPTION',
            'width' => '20%',
            'link' => true,
        ),
        //    'sale_type' =>
        //    array (
        //        'type' => 'enum',
        //        'default' => true,
        //        'studio' => 'visible',
        //        'vname' => 'LBL_SALE_TYPE',
        //        'width' => '9%',
        //    ),
        //    'sale_type_date' =>
        //    array (
        //        'type' => 'date',
        //        'default' => true,
        //        'studio' => 'visible',
        //        'vname' => 'LBL_SALE_TYPE_DATE',
        //        'width' => '9%',
        //    ),
        'status' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'vname' => 'LBL_STATUS',
            'width' => '7%',
        ),
         'audited_amount_text' =>
        array (
            'type' => 'varchar',
            'default' => true,
            'studio' => 'visible',
            'vname' => 'LBL_AUDITED_AMOUNT',
            'width' => '10%',
            'sortable' => false,
        ),
//        'created_by_name' =>
//        array (
//            'type' => 'relate',
//            'default' => true,
//            'studio' => 'visible',
//            'vname' => 'LBL_CREATED_BY',
//            'width' => '9%',
//        ),
        'assigned_user_name' =>
        array (
            'type' => 'relate',
            'default' => true,
            'studio' => 'visible',
            'vname' => 'LBL_ASSIGNED_TO_NAME',
            'width' => '9%',
        ),

        'invoice_name' =>
        array (
            'type' => 'relate',
            'link' => true,
            'vname' => 'LBL_INVOICE_NUMBER',
            'id' => 'INVOICE_ID',
            'width' => '10%',
            'default' => true,
            'widget_class' => 'SubPanelDetailViewLink',
            'target_module' => 'J_Invoice',
            'target_record_key' => 'invoice_id',
            'sortable' => false,
        ),
        'custom_button' =>
        array (
            'type' => 'varchar',
            'width' => '20%',
            'default' => true,
        ),
        'team_id' =>
        array (
            'usage'=>'query_only',
        ),
        'card_type' =>
        array (
            'usage'=>'query_only',
        ),
        'payment_id' =>
        array (
            'usage'=>'query_only',
        ),
        'is_release' =>
        array (
            'usage'=>'query_only',
        ),
        'student_id' =>
        array (
            'usage'=>'query_only',
        ),
        'bank_account' =>
        array (
            'usage'=>'query_only',
        ),
        'reference_document' =>
        array (
            'usage'=>'query_only',
        ),
        'reference_number' =>
        array (
            'usage'=>'query_only',
        ),
        'loyalty_amount' =>
        array (
            'usage'=>'query_only',
        ),
        'sponsor_amount' =>
        array (
            'usage'=>'query_only',
        ),
        'before_discount' =>
        array (
            'usage'=>'query_only',
        ),
        'discount_amount' =>
        array (
            'usage'=>'query_only',
        ),
        'inv_code' =>
        array (
            'usage'=>'query_only',
        ),
        'invoice_id' =>
        array (
            'usage'=>'query_only',
        ),
        'bank_name' =>
        array (
            'usage'=>'query_only',
        ),
        'note' =>
        array (
            'usage'=>'query_only',
        ),
        'pos_code' =>
        array (
            'usage'=>'query_only',
        ),
        'is_old' =>
        array (
            'usage'=>'query_only',
        ),
        'audited_amount' =>
        array (
            'usage'=>'query_only',
        ),
        'reconcile_status' =>
        array (
            'usage'=>'query_only',
        ),
);