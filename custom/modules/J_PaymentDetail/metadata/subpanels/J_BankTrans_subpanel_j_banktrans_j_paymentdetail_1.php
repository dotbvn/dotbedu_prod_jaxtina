<?php
// created: 2023-05-07 20:58:30
$subpanel_layout['list_fields'] = array (
  'payment_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_PAYMENT_NAME',
    'id' => 'PAYMENT_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'J_Payment',
    'target_record_key' => 'payment_id',
  ),
  'payment_no' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_PAYMENT_NO',
    'width' => 10,
  ),
  'payment_amount' => 
  array (
    'type' => 'currency',
    'default' => true,
    'vname' => 'LBL_PAYMENT_AMOUNT',
    'currency_format' => true,
    'related_fields' => 
    array (
      0 => 'currency_id',
      1 => 'base_rate',
    ),
    'width' => 10,
  ),
  'payment_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_PAYMENT_DATE',
    'width' => 10,
    'default' => true,
  ),
  'expired_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_EXPIRED_DATE',
    'width' => 10,
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'reconcile_status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_RECONCILE_STATUS',
    'width' => 10,
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);