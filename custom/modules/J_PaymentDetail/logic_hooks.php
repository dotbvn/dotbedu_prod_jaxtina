<?php
    $hook_version = 1;
    $hook_array = Array();
    // position, file, function

    $hook_array['process_record'] = Array();
    $hook_array['process_record'][] = Array(1, 'Add button export', 'custom/modules/J_PaymentDetail/logicPaymentDetail.php','logicPaymentDetail', 'displayButton');

    $hook_array['before_save'] = Array();
    $hook_array['before_save'][] = Array(2, 'Handle before save', 'custom/modules/J_PaymentDetail/logicPaymentDetail.php','logicPaymentDetail', 'handleBeforeSave');
    //$hook_array['before_save'][] = Array(3, 'TAPTAP add transaction', 'custom/modules/J_PaymentDetail/logicPaymentDetail.php','logicPaymentDetail', 'requestTransactionTapTap');

    $hook_array['after_save'] = Array();
    $hook_array['after_save'][] = Array(1, 'Handle CJ After Save', 'custom/modules/J_PaymentDetail/logicCJJ_PaymentDetail.php','logicCJJ_PaymentDetail', 'handleCJAfterSave');
    $hook_array['after_save'][] = Array(2, 'Handle After save', 'custom/modules/J_PaymentDetail/logicPaymentDetail.php','logicPaymentDetail', 'handleAfterSave');
    $hook_array['after_save'][] = Array(100, 'Add Auto-Increment Code', 'custom/modules/J_PaymentDetail/logicPaymentDetail.php','logicPaymentDetail', 'addCode');

    $hook_array['before_delete'] = Array();
    $hook_array['before_delete'][] = Array(2, 'Delete Payment', 'custom/modules/J_PaymentDetail/logicPaymentDetail.php','logicPaymentDetail', 'deletedPaymentDetail');