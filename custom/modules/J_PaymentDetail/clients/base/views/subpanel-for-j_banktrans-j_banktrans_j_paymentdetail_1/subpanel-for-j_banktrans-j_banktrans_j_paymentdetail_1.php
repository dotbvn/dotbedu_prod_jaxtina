<?php
// created: 2023-05-07 20:58:30
$viewdefs['J_PaymentDetail']['base']['view']['subpanel-for-j_banktrans-j_banktrans_j_paymentdetail_1'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'payment_name',
          'label' => 'LBL_PAYMENT_NAME',
          'enabled' => true,
          'id' => 'PAYMENT_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'payment_no',
          'label' => 'LBL_PAYMENT_NO',
          'enabled' => true,
          'default' => true,
          'width' => 'small',
        ),
        2 => 
        array (
          'name' => 'payment_amount',
          'label' => 'LBL_PAYMENT_AMOUNT',
          'enabled' => true,
          'currency_format' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'payment_date',
          'label' => 'LBL_PAYMENT_DATE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'expired_date',
          'label' => 'LBL_EXPIRED_DATE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'reconcile_status',
          'label' => 'LBL_RECONCILE_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        9 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);