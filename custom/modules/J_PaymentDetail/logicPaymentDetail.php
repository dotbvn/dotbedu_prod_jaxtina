<?php
class logicPaymentDetail{

    function deletedPaymentDetail($bean, $event, $arguments){
        global $current_user;
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_action = $arrayUrl[sizeof($arrayUrl) - 1];//Check Mass Delete
        }
        if(($_POST['module'] == $bean->module_name && $_POST['action'] == 'Delete') || $url_action == 'MassUpdate'){
            $pmdS      = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(id, '') id, IFNULL(status, '') status, IFNULL(payment_amount, 0) payment_amount FROM j_paymentdetail WHERE payment_id = '{$bean->payment_id}' AND status <> 'Cancelled' AND deleted = 0");
            $countUsed = $GLOBALS['db']->getOne("SELECT count(id) count FROM j_payment_j_payment_1_c WHERE payment_idb = '{$bean->payment_id}' AND deleted = 0");
            $countPAID = 0;
            foreach($pmdS as $id => $pmd) if($pmd['status'] == 'Paid' && $pmd['payment_amount'] > 0) $countPAID++;
                //Check Paid
                if(($countPAID > 0) || ($countUsed > 0)){
                $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Payment','".$bean->payment_id."'));";
                echo '<script type="text/javascript">
                window.parent.DOTB.App.alert.show(\'message-id\', {
                level: \'error\',
                messages: \''.str_replace(array("\r", "\n"), '', nl2br(translate('LBL_ALERT_DELETE_RECEIPT', 'J_Payment'))).'\',
                autoClose: false
                });
                '.$route.'
                </script>';
                die();
            }
        }


        $GLOBALS['db']->query("UPDATE j_loyalty SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE paymentdetail_id = '{$bean->id}'");
        $GLOBALS['db']->query("UPDATE c_commission SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE pmd_id = '{$bean->id}'");

        //TAPTAP
        $administration = new Administration();
        $administration->retrieveSettings();
        //check config TAPTAP
        if(!empty($administration->settings['taptap_config_keycloak_host'])){
            require_once "custom/include/_helper/connectTapTapApi.php";
            $tapTap = new TapTapHelper();

            //check is_external_accumulate, payment_type
            $resCheck = $tapTap->checkIsExternalAccumulate($bean->id);
            if(($resCheck['is_not_external_accumulate'] < 1 && $resCheck['payment_type'] == 'Cashholder')
            || $resCheck['payment_type'] == 'Deposit'){

                //check token
                $responeToken = $tapTap->requestToken();
                if ($responeToken['success']) {

                    //return transaction
                    $responeTransaction = $tapTap->returnTransaction($bean, $resCheck['payment_type']);
                    $external_connection = json_decode(html_entity_decode($bean->external_connection));
                    $external_connection->external_accumulation->TAPTAP->return_transaction = (object)$responeTransaction;
                    $bean->external_connection = json_encode($external_connection, JSON_UNESCAPED_UNICODE);
                    $GLOBALS['db']->query("UPDATE j_paymentdetail SET external_connection = '{$bean->external_connection}' WHERE id = '{$bean->id}'");
                }
            }
        }
    }
    function displayButton($bean, $event, $arguments) {
        require_once("custom/modules/J_Payment/HelperPayment.php");
        $helper_payment = new HelperPayment();

        global $timedate,$dotb_config;
        if ($_REQUEST['module']=='J_Payment'){

            //Show paid amount, unpaid amount
            $sqlPayDtl = "SELECT DISTINCT IFNULL(pmd.id, '') primaryId,
            IFNULL(pmd.payment_no, '') payment_no, IFNULL(pmd.status, '') status,
            IFNULL(l2.id, '') invoice_id, IFNULL(l2.transaction_id, '') inv_transaction_id, IFNULL(l2.supplier, '') inv_supplier,
            IFNULL(l1.id, '') payment_id,
            IFNULL(l1.payment_type, '') payment_type, IFNULL(l1.kind_of_course, '') kind_of_course,
            IFNULL(pmd.expired_date, '') expired_date, IFNULL(pmd.payment_date, '') receipt_date,
            IFNULL(pmd.payment_amount, '0') payment_amount
            FROM j_paymentdetail pmd
            INNER JOIN j_payment l1 ON pmd.payment_id = l1.id AND l1.deleted = 0
            LEFT JOIN j_invoice l2 ON l2.id = pmd.invoice_id AND l2.deleted = 0
            WHERE l1.id = '{$bean->payment_id}' AND pmd.deleted = 0 AND pmd.status <> 'Cancelled'
            ORDER BY pmd.payment_no";
            $resultPayDtl = $GLOBALS['db']->query($sqlPayDtl);
            $unpaidAmount = 0;
            while($row = $GLOBALS['db']->fetchByAssoc($resultPayDtl)){
                if($row['status'] == "Unpaid") $unpaidAmount += $row['payment_amount'];
                if($bean->id == $row['primaryId']) $pmd = $row;
            }

            //Get description
            if(empty($bean->description)) $description = $helper_payment->generateContent($pmd);
            else $description = $bean->description;

            $bean->custom_button = '<div style="text-align: center;">';
            switch ($bean->status) {
                case "Paid":
                    $colorClass = "textbg_green";
                    //check config E-Invoice receipt
                    $checkConfigEvat = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM j_configinvoiceno WHERE active = 1 AND team_id = '{$bean->team_id}' AND deleted = 0");
                    if (((int)$checkConfigEvat) > 0
                    && $bean->payment_amount > 0
                    && (ACLController::checkAccess('J_Invoice', 'edit', true))
                    && empty($pmd['invoice_id'])){
                        $admin = new Administration();
                        $admin->retrieveSettings();
                        if($admin->settings['einvoice_setting_export_einvoice_type'] == 'Receipt')
                            if(noInvoiceCheck($bean->payment_id))
                                $bean->invoice_name = '<div style="display: inline-flex;"><button class="button primary btn_get_evat" title="'.translate('LBL_GET_INVOICE_NO', 'J_Payment').'" type="button" payment_id="'.$bean->payment_id.'" payment_detail_id="' . $bean->id . '" value="'.translate('LBL_GET_INVOICE_NO', 'J_Payment').'" onclick="get_invoice_no(this);"><i class="far fa-money-check-edit-alt"></i></button></div>';
                    }

                    if(!empty($pmd['inv_supplier']) && !empty($pmd['inv_transaction_id']) && !empty($pmd['invoice_id'])){
                        if($pmd['inv_supplier'] == 'Bkav')
                            $bean->invoice_name = "<b>🔗<a target='_blank' href='https://van.ehoadon.vn/TCHD?MTC={$pmd['inv_transaction_id']}'>{$bean->invoice_name}</a></b>";
                        elseif($pmd['inv_supplier'] == 'Misa')
                            $bean->invoice_name = "<b>🔗<a target='_blank' href='https://www.meinvoice.vn/tra-cuu/downloadhandler.ashx?type=pdf&code={$pmd['inv_transaction_id']}'>{$bean->invoice_name}</a></b>";
                    }

                    $pdf = $GLOBALS['db']->fetchOne("SELECT id FROM pdfmanager WHERE deleted =0 AND base_module = 'J_PaymentDetail' AND published ='yes' AND (IFNULL(keywords,'') != 'dotb_fee_slip') ORDER BY date_modified DESC LIMIT 1");
                    if(!empty($pdf['id'])) $check =1; else $check =0;

                    $bean->custom_button .='<ul class="clickMenu fancymenu button"><li class="dotb_action_button primary" style="min-width: 155px;"><a style="width: 70%;" pdf_id = "'.$pdf['id'].'"pdf="'.$check.'"payment_detail_id="'.$bean->id.'" onclick="ex_invoice(this);" ><i class="far fa-file-export"></i> '.translate('LBL_EXPORT','J_PaymentDetail').'</a><ul class="subnav" style="display: none;">';
                    //$bean->custom_button .= '<li><a id="id1" pdf_id = "'.$pdf['id'].'"pdf="'.$check.'"payment_detail_id="'.$bean->id.'" onclick="ex_invoice(this);" > <i class="far fa-file-export"></i> '.translate('LBL_EXPORT','J_PaymentDetail').'</a></li>';

                    if(checkDataLockDate($bean->payment_date) && ACLController::checkAccess('J_PaymentDetail', 'edit', true) && $bean->payment_amount > 0){
                        if(DotbACL::checkField('J_PaymentDetail', 'payment_method', 'edit', array("owner_override" => true)))
                            $bean->custom_button .= '<li><a id="id2" payment_method="'.$bean->payment_method.'" payment_detail_amount="'.format_number($bean->payment_amount).'" payment_date="'.$timedate->to_display_date($bean->payment_date,true).'" card_type="'.$bean->card_type.'" bank_type="'.$bean->bank_name.'" bank_account="'.$bean->bank_account.'" pmd_reference_document="'.$bean->reference_document.'" pmd_reference_number="'.$bean->reference_number.'" pmd_unpaid_amount="'.format_number($unpaidAmount).'" pmd_description="'.$description.'" pmd_note="'.$bean->note.'" onclick = \'edit_invoice(this)\' payment_detail_id="'.$bean->id.'" pos_code="'.$bean->pos_code.'" card_type="'.$bean->card_type.'" inv_code="'.$bean->inv_code.'" assigned_user_name="'.$bean->assigned_user_name.'" status="'.$bean->status.'"> <i class="far fa-money-check-edit-alt"></i> '.translate('LBL_EDIT','J_PaymentDetail').'</a></li>';
                        if(DotbACL::checkField('J_PaymentDetail', 'status', 'edit', array("owner_override" => true)))
                            $bean->custom_button .= '<li><a id="id3" style="color:#E61718" payment_detail_id="' . $bean->id . '" class="cancel_invoice" onclick = \'cancel_receipt("' . $bean->id . '")\'> <i class="far fa-minus-circle"></i> '.translate('LBL_VOID','J_PaymentDetail').'</a></li>';

                    }
                    break;
                case "Unpaid":
                    $colorClass = "textbg_orange";
                    $bean->custom_button .='<ul class="clickMenu fancymenu button"><li class="dotb_action_button primary" style="min-width: 155px;"><a style="width: 70%;" title="'.translate('LBL_PAY_DES','J_PaymentDetail').'" payment_detail_id="'.$bean->id.'"  assigned_user_name="'.$bean->assigned_user_name.'" status="'.$bean->status.'" payment_detail_amount="'.format_number($bean->payment_amount).'" pmd_bank_account="'.$bean->bank_account.'" pmd_reference_document="'.$bean->reference_document.'" pmd_reference_number="'.$bean->reference_number.'" inv_code="'.$bean->inv_code.'" pmd_unpaid_amount="'.format_number($unpaidAmount).'" pmd_description="'.$description.'" pmd_note="'.$bean->note.'" class="pay" onclick="pay(this);"><i class="far fa-hand-holding-usd"></i> '.translate('LBL_PAY','J_PaymentDetail').'</a><ul class="subnav" style="display: none;">';
                    $bean->custom_button .= '<li><a id="id2" payment_method="'.$bean->payment_method.'" payment_detail_amount="'.format_number($bean->payment_amount).'" payment_date="'.$timedate->to_display_date($bean->payment_date,true).'" card_type="'.$bean->card_type.'" bank_type="'.$bean->bank_name.'" bank_account="'.$bean->bank_account.'" pmd_reference_document="'.$bean->reference_document.'" pmd_reference_number="'.$bean->reference_number.'" pmd_unpaid_amount="'.format_number($unpaidAmount).'" pmd_description="'.$description.'" pmd_note="'.$bean->note.'" onclick = \'edit_invoice(this)\' payment_detail_id="'.$bean->id.'" pos_code="'.$bean->pos_code.'" card_type="'.$bean->card_type.'" inv_code="'.$bean->inv_code.'" assigned_user_name="'.$bean->assigned_user_name.'" status="'.$bean->status.'"> <i class="far fa-money-check-edit-alt"></i> '.translate('LBL_EDIT','J_PaymentDetail').'</a></li>';
                    //$bean->custom_button .= '<li><a id="id1" payment_detail_id="'.$bean->id.'" payment_detail_amount="'.format_number($bean->payment_amount).'" pmd_bank_account="'.$bean->bank_account.'" pmd_reference_document="'.$bean->reference_document.'" pmd_reference_number="'.$bean->reference_number.'" inv_code="'.$bean->inv_code.'" pmd_unpaid_amount="'.format_number($unpaidAmount).'" pmd_description="'.$description.'" pmd_note="'.$bean->note.'" class="pay" onclick="pay(this);"/> <i class="far fa-hand-holding-usd"></i> '.translate('LBL_PAY','J_PaymentDetail').'</a></li>';
                    $pdf = $GLOBALS['db']->fetchOne("SELECT id FROM pdfmanager WHERE deleted =0 AND base_module = 'J_PaymentDetail' AND published ='yes' AND keywords = 'dotb_fee_slip' ORDER BY date_entered LIMIT 1");
                    if(!empty($pdf['id'])) $bean->custom_button .= '<li><a id="id2" pdf_id="'.$pdf['id'].'" payment_detail_id="'.$bean->id.'" onclick="ex_invoice(this);"> <i class="far fa-file-pdf"></i> '.translate('LBL_EXPORT_FEE_SLIP','J_PaymentDetail').'</a></li>';
                    $bean->custom_button .= '<li><a id="id3" payment_detail_id="'.$bean->id.'" onclick="send_pay_slip(this);"> <i class="far fa-paper-plane"></i> '.translate('LBL_SEND_REQUEST','J_PaymentDetail').'</a></li>';
                    $bean->payment_date = '';
                    break;
                case "Cancelled":
                    $colorClass = "textbg_black";
                    break;
            }

            //Xử lý thông tin đối soát Bank Transaction
            if(ACLController::checkAccess('J_BankTrans', 'list', true))
                $bean->custom_button .= '<li><a id="id4" onclick="window.parent.DOTB.App.router.navigate(\'#J_BankTrans?filterReceipt='.$bean->name.'\', {trigger: true})" style="color:#537600"> <i class="far fa-exchange"></i> '.translate('LBL_CHECK_AUDIT','J_PaymentDetail').'</a></li>';
            $bean->custom_button .='</ul><span class="ab subhover"></span></li></ul>';
            $bean->custom_button .= '</div> <input type="hidden" class="receipt_status" id="'.$bean->id.'" value="'.$bean->status.'" audited_amount="'.$bean->audited_amount.'">';//Set hidden field - Change Status automatic by Bank Transaction Auditor

            //Set other fields
            $bean->discount_amount += $bean->sponsor_amount + $bean->loyalty_amount;
            $bean->status = "<span class='full-width visible'><span class='label ellipsis_inline $colorClass'>". $GLOBALS['app_list_strings']['status_paymentdetail_list'][$bean->status] ."</span>";
            //Load thông tin số tiền đối soát
            if($bean->audited_amount > 0)
                $bean->audited_amount_text = '<span style="cursor: pointer;" title="'.$GLOBALS['app_list_strings']['reconcile_status_list'][$bean->reconcile_status].'" class="full-width visible"><span class="label ellipsis_inline label-success" onclick="window.parent.DOTB.App.router.navigate(\'#J_BankTrans?filterReceipt='.$bean->name.'\', {trigger: true})">+'.format_number($bean->audited_amount).'</span>';

            $bean->name = '<a href="#bwc/index.php?module=J_PaymentDetail&action=DetailView&record='.$bean->id.'">'.$bean->name.'</a>';
            if($bean->is_old) $bean->name = '<span class="simptip-position-top simptip-movable simptip-multiline" data-tooltip="'.translate('LBL_IS_IMPORT_DATA_DES','J_PaymentDetail').'">'.$bean->name.' <img src="custom/images/import.png" style="width: 12px;"></span>';
        }
    }
    /*AFTER_SAVE*/
    function addCode(&$bean, $event, $arguments){
        global $timedate;
        $code_field = 'name';
        //Create Receipt code
        if( ($bean->$code_field == '-none-') || empty($bean->$code_field)){
            //AFTER_SAVE: Repeat 10 times to avoid duplicate codes
            for ($x = 0; $x < 10; $x++) {
                //Get Prefix
                $year       = date('ym',strtotime(((!empty($bean->expired_date)) ? ($bean->expired_date) : date('Y-m-d', strtotime("+7 hours " . $bean->date_entered)))));
                $table      = $bean->table_name;
                $first_pad  = '0000';
                $padding    = 4;
                $str_code   = $year;
                //Query lấy ra Receipt sau cùng - Đoạn này (AND date_entered >= ...)  loại bỏ TH receipt tạo và xoá trong ngày
                $query = "SELECT $code_field FROM $table WHERE ($code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, ".strlen($str_code).") = '".$str_code."') AND IFNULL(is_old,0)=0 AND ((deleted=0) OR ((deleted=1) AND date_entered >= '".date('Y-m-d 00:00:00', strtotime($timedate->nowDbDate()))."'))  ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";
                $result = $GLOBALS['db']->query($query);

                if($row = $GLOBALS['db']->fetchByAssoc($result))
                    $last_code = $row[$code_field];
                else
                    //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $last_code = $str_code. $first_pad;

                $num = substr($last_code, -$padding, $padding);
                $num++;
                $pads = $padding - strlen($num);
                $new_code = $str_code;

                //preform the lead padding 0
                for($i=0; $i < $pads; $i++) $new_code .= "0";
                $new_code .= $num;

                //check duplicate code
                $countDup = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM $table WHERE $code_field = '$new_code' AND deleted = 0");
                if(empty($countDup)){
                    //write to database - Logic: Before Save
                    $GLOBALS['db']->query("UPDATE $table SET $code_field = '$new_code' WHERE id='{$bean->id}' AND deleted = 0");
                    break;
                }
            }
        }
    }


    function handleBeforeSave($bean, $event, $arguments){
        global $timedate;
        if(empty($bean->parent_id) || empty($bean->parent_type)){
            $payment = BeanFactory::getBean('J_Payment', $bean->payment_id);
            $bean->parent_id = $payment->parent_id;
            $bean->parent_type = $payment->parent_type;
        }

        if(($bean->fetched_row['status'] == 'Unpaid') && $bean->status =='Paid'
        && $bean->payment_amount > 0){
            if($bean->parent_type == 'Contacts'){
                require_once("custom/clients/mobile/helper/NotificationHelper.php");
                //Push notification
                $notify = new NotificationMobile();
                $notify->pushNotification('Thông tin học phí','Hóa đơn '.$bean->name.' đã thanh toán thành công!', 'J_PaymentDetail', $bean->id, $bean->parent_id, 'Student' );
            }
            //Tính ngày giờ thanh toán
            if(empty($bean->payment_date)) $bean->payment_date = $timedate->nowDbDate();
            if(empty($bean->payment_datetime)) $bean->payment_datetime = $bean->payment_date.' '.substr($timedate->nowDb(),11);
        }
        $bean->pos_code     = strtoupper($bean->pos_code);

        //Check đồng bộ ngay lập tức Thanh toán
        if($bean->status == 'Unpaid'
            && $bean->fetched_row['reconcile_status'] != 'processing'
            && $bean->reconcile_status == 'processing'
            && $bean->payment_amount > 0)
            $res = forceReconcile($bean);
        //END

        //Fetched Row Receipt
        $_POST['_fetched_row'] = $bean->fetched_row;
    }

    function handleAfterSave($bean, $event, $arguments){
        $fetchedRow = $_POST['_fetched_row'];
        $payment = BeanFactory::getBean('J_Payment', $bean->payment_id);
        //Loyalty Point
        if ($bean->status == 'Paid') {
            //delete First
            $GLOBALS['db']->query("UPDATE j_loyalty SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE payment_id='{$bean->payment_id}' AND paymentdetail_id='{$bean->id}' AND deleted=0");
            $q1 = "SELECT DISTINCT
            IFNULL(l1.id, '') payment_id,
            IFNULL(l2.loyalty_points, 0) loyalty_points,
            IFNULL(l4.id, '') student_id,
            IFNULL(l2.description, '') description,
            COUNT(l3.id) count_loyalty,
            COUNT(*) count
            FROM j_paymentdetail
            INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
            INNER JOIN j_sponsor l2 ON l1.id = l2.payment_id AND l2.deleted = 0
            INNER JOIN contacts l4 ON l4.id = l2.student_id AND l4.deleted = 0
            LEFT JOIN j_loyalty l3 ON l1.id = l3.payment_id AND l3.deleted = 0
            WHERE (j_paymentdetail.id = '{$bean->id}')
            AND (IFNULL(l2.type, '') = 'Loyalty')
            AND ((foc_type <> ''))
            AND j_paymentdetail.deleted = 0";
            $rs_loyal = $GLOBALS['db']->query($q1);
            while($r_loyal = $GLOBALS['db']->fetchByAssoc($rs_loyal)){
                if ($r_loyal['loyalty_points'] > 0 && $r_loyal['count_loyalty'] == 0 ) {
                    //Count Loyalty point
                    $loyalty = new J_Loyalty();
                    $loyalty->point            = abs($r_loyal['loyalty_points']);
                    $loyalty->type             = 'Referral';
                    $loyalty->student_id       = $r_loyal['student_id'];
                    $loyalty->payment_id       = $bean->payment_id;
                    $loyalty->paymentdetail_id = $bean->id;
                    $loyalty->team_id          = $bean->team_id;
                    $loyalty->team_set_id      = $bean->team_id;
                    $loyalty->input_date       = $bean->payment_date;
                    $loyalty->description      = $r_loyal['description'];
                    $loyalty->save();
                }
            }

            //Tích điểm Reward mua theo khóa học
            if($payment->accrual_rate_value > 0){
                $loyalty = new J_Loyalty();
                $loyalty->point            = abs( floor(($payment->accrual_rate_value * $bean->payment_amount) / 1000) );
                $loyalty->type             = 'Reward';
                $loyalty->student_id       = $payment->parent_id;
                $loyalty->payment_id       = $payment->id;
                $loyalty->paymentdetail_id = $bean->id;
                $loyalty->team_id          = $bean->team_id;
                $loyalty->team_set_id      = $bean->team_id;
                $loyalty->input_date       = $bean->payment_date;
                $loyalty->description      = 'Tích lũy '.($payment->accrual_rate_value*100).'% theo khóa học.';
                $loyalty->save();
            }

        } elseif ($bean->status == 'Cancelled') {
            //delete First
            $GLOBALS['db']->query("UPDATE j_loyalty SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE payment_id='{$bean->payment_id}' AND paymentdetail_id='{$bean->id}' AND deleted=0");
        }
        //END: Loyalty Point

        //Add Commision
        if ($bean->status == 'Paid' && $bean->payment_amount > 0) {
            addCommisson($bean);
        }elseif ($bean->status == 'Cancelled'){
            $GLOBALS['db']->query("UPDATE c_commission SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE pmd_id='{$bean->id}' AND deleted=0");
        }


        require_once("custom/include/_helper/junior_revenue_utils.php");
        //Update remain
        getPaymentRemain($bean->payment_id);
        //Update Sale Type
        if($bean->payment_amount >= 0){
            if ($bean->status == 'Paid'){
                checkSaleType($bean->id);
            }elseif ($bean->status == 'Cancelled'){
                //Tính lại sale của các payment related
                revokeSaleType($bean->payment_id);
            }
        }
        //Xử lý Auto-Enroll - TH thu tiền đối soát tự động
        if($fetchedRow['status'] == 'Unpaid' && $bean->status == 'Paid'
        && in_array($bean->reconcile_status,['overpaid','fullpaid'])){
            //Process Auto-Enroll
            if ($payment->is_auto_enroll) {
                //cache Payment: Remain;  Class: start_study/end_study
                $payment = BeanFactory::getBean('J_Payment', $bean->payment_id, array('use_cache' => false));
                $class = BeanFactory::getBean('J_Class', $payment->ju_class_id, array('use_cache' => false));
                $payment->pmd_id = $bean->id;
                $res = processAutoEnroll($payment, $class);
            }
        }
        // update student status
        if($bean->parent_type == 'Contacts'
            && !empty($bean->parent_id)
            && $fetchedRow['status'] != $bean->status
            && !empty($fetchedRow))
            updateStudentStatus($bean->parent_id);
    }

    function requestTransactionTapTap(&$bean, $event, $arguments){
        $administration = new Administration();
        $administration->retrieveSettings();
        // check config TAPTAP
        if(!empty($administration->settings['taptap_config_keycloak_host'])){
            require_once "custom/include/_helper/connectTapTapApi.php";
            $tapTap = new TapTapHelper();
            //check is_external_accumulate, payment_type
            $resCheck = $tapTap->checkIsExternalAccumulate($bean->id);

            if((($bean->fetched_row['status'] == "Unpaid" && $bean->status == "Paid")
            || ($bean->fetched_row['status'] == "Paid" && $bean->status == "Cancelled"))
            && (($resCheck['is_not_external_accumulate'] < 1 && $resCheck['payment_type'] == 'Cashholder') || $resCheck['payment_type'] == 'Deposit')) {

                //check token
                $responeToken = $tapTap->requestToken();
                if ($responeToken['success']) {
                    if ($bean->status == "Paid") {
                        //add transaction
                        $responeTransaction = $tapTap->addTransaction($bean, $resCheck['payment_type']);
                        $external_connection = json_decode(html_entity_decode($bean->external_connection));
                        $external_connection->external_accumulation->TAPTAP->add_transaction = (object)$responeTransaction;
                        $bean->external_connection = json_encode($external_connection, JSON_UNESCAPED_UNICODE);

                    } elseif ($bean->status == "Cancelled") {
                        //return transaction
                        $responeTransaction = $tapTap->returnTransaction($bean, $resCheck['payment_type']);
                        $external_connection = json_decode(html_entity_decode($bean->external_connection));
                        $external_connection->external_accumulation->TAPTAP->return_transaction = (object)$responeTransaction;
                        $bean->external_connection = json_encode($external_connection, JSON_UNESCAPED_UNICODE);
                    }
                }
            }
        }
    }
}
?>
