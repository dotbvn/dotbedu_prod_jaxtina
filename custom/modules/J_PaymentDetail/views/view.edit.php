<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class J_PaymentDetailViewEdit extends ViewEdit
{
    public function display(){

        if(!$GLOBALS['current_user']->isAdmin()){
            $pmdS      = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(id, '') id, IFNULL(status, '') status, IFNULL(payment_amount, 0) payment_amount FROM j_paymentdetail WHERE payment_id = '{$this->bean->payment_id}' AND status <> 'Cancelled' AND deleted = 0");
            $countUsed = $GLOBALS['db']->getOne("SELECT count(id) count FROM j_payment_j_payment_1_c WHERE payment_idb = '{$this->bean->payment_id}' AND deleted = 0");
            $countPAID = 0;
            foreach($pmdS as $id => $pmd) if($pmd['status'] == 'Paid' && $pmd['payment_amount'] > 0) $countPAID++;
                //Check Paid
                if(($countPAID > 0) || ($countUsed > 0)){
                echo '<script type="text/javascript">
                alert(`'.translate('LBL_ALERT_DELETE_RECEIPT', 'J_Payment').'`);
                location.href=\'index.php?module=J_Payment&action=DetailView&record='.$this->bean->payment_id.'\';
                </script>';
                die();
            }
        }
        $this->ss->assign('card_name_list',$GLOBALS['app_list_strings']['card_name_list']);
        $this->ss->assign('bank_name_list',$GLOBALS['app_list_strings']['bank_name_list']);
        $this->ss->assign('status_paymentdetail_list',$GLOBALS['app_list_strings']['status_paymentdetail_list']);
        parent::display();
    }
}