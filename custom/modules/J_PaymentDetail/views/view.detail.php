<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class J_PaymentDetailViewDetail extends ViewDetail{

    public function display(){
        global $current_user, $app_strings;
        $btn_custom = '';
        $this->bean->discount_amount =  $this->bean->discount_amount + $this->bean->sponsor_amount + $this->bean->loyalty_amount;

        if($current_user->isAdminForModule('J_PaymentDetail')){
            $btn_custom = '<input title="'.$app_strings['LBL_EDIT_BUTTON'].'" accesskey="i" class="button primary" onclick="var _form = document.getElementById(\'formDetailView\'); _form.return_module.value=\'J_PaymentDetail\'; _form.return_action.value=\'DetailView\'; _form.return_id.value=\''.$this->bean->id.'\'; _form.action.value=\'EditView\';DOTB.ajaxUI.submitForm(_form);" type="button" name="Edit" id="edit_button" value="'.$app_strings['LBL_EDIT_BUTTON'].'">';
            $btn_custom .= '<input title="'.$app_strings['LBL_DELETE_BUTTON'].'" accesskey="d" class="button" onclick="var _form = document.getElementById(\'formDetailView\'); _form.return_module.value=\'J_PaymentDetail\'; _form.return_action.value=\'ListView\'; _form.action.value=\'Delete\'; if(confirm(\''.$app_strings['NTC_DELETE_CONFIRMATION'].'\')) DOTB.ajaxUI.submitForm(_form);" type="submit" name="Delete" value="'.$app_strings['LBL_DELETE_BUTTON'].'" id="delete_button">';
            $btn_custom .= '<input title="'.$app_strings['LBL_DUPLICATE_BUTTON'].'" accesskey="u" class="button" onclick="var _form = document.getElementById(\'formDetailView\'); _form.return_module.value=\'J_PaymentDetail\'; _form.return_action.value=\'DetailView\'; _form.isDuplicate.value=true; _form.action.value=\'DuplicateView\'; _form.return_id.value=\''.$this->bean->id.'\';DOTB.ajaxUI.submitForm(_form);" type="button" name="Duplicate" value="'.$app_strings['LBL_DUPLICATE_BUTTON'].'" id="duplicate_button">';
        }
        $this->ss->assign('BTN_CUSTOM', $btn_custom);

        parent::display();
    }

}