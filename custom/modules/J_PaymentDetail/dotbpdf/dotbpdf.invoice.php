<?php


if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class J_PaymentDetailDotbpdfinvoice extends DotbpdfSmarty
{

    public function preDisplay()
    {
        parent::preDisplay();
        // check request param
        if (empty($_REQUEST['record'])) {
            $this->Error('Have to choose a receipt');
            return;
        }

        // Set template
        $templateId = $_REQUEST['pdf_id'];
        //        $templateId = $GLOBALS['db']->getOne("SELECT id FROM pdfmanager WHERE base_module = 'J_PaymentDetail' AND deleted = 0 ORDER BY date_entered LIMIT  1");
        $template = BeanFactory::getBean('PdfManager', array('disable_row_level_security' => true));
        $template->retrieve($templateId);
        if (!empty($template)) {
            $this->ss->security_settings['PHP_TAGS'] = false;
            $this->ss->security = true;
            if (defined('DOTB_SHADOW_PATH')) {
                $this->ss->secure_dir[] = DOTB_SHADOW_PATH;
            }

            // settting page
            $this->setPageUnit('mm');
            $this->setPageFormat('A5', 'L');
            $this->SetAutoPageBreak(TRUE, 5);
            $this->SetMargins(15, 5, 10);
            $this->setPrintHeader(false);
            $this->setPrintFooter(false);
            $this->SetCreator($template->author);
            $this->SetAuthor($template->author);


            // build template
            $this->templateLocation = $this->buildTemplateFile($template);

            // data
            $fields = $this->prepareData($_REQUEST['record']);

            $this->SetTitle('Receipt-' . $fields['receipt']['name']);
            $this->SetSubject('Receipt-' . $fields['receipt']['name']);
            $this->SetKeywords($template->keywords);

            // Assign variables
            $this->ss->assign('payment', $fields['payment']);
            $this->ss->assign('receipt', $fields['receipt']);
            $this->ss->assign('student', $fields['student']);
            $this->ss->assign('center' , $fields['center']);
            $this->ss->assign('vietqr' , $fields['vietqr']);

        }
    }

    private function buildTemplateFile($template)
    {
        if (!empty($template)) {

            if (!file_exists(dotb_cached('modules/PdfManager/tpls'))) {
                mkdir_recursive(dotb_cached('modules/PdfManager/tpls'));
            }
            $tpl_filename = dotb_cached('modules/PdfManager/tpls/' . $template->id . '.tpl');

            $template->body_html = str_replace(" ", "", $template->body_html);
            $template->body_html = html_entity_decode($template->body_html);

            //Tìm thẻ PageBreak
            $template->body_html = str_replace('<!--|', '', $template->body_html);
            $template->body_html = str_replace('|-->', '', $template->body_html);


            dotb_file_put_contents($tpl_filename, $template->body_html);

            return $tpl_filename;
        }
        return '';
    }

    private function prepareData($receiptId)
    {
        require_once 'modules/PdfManager/PdfManagerHelper.php';
        require_once("custom/include/ConvertMoneyString/convert_number_to_string.php");

        global $db;
        $paymentFields = [];
        $receiptFields = [];
        $studentFields = [];
        $centerFields = [];

        $receipt = BeanFactory::getBean('J_PaymentDetail', $receiptId, array('disable_row_level_security' => true));
        $payment = BeanFactory::getBean('J_Payment', $receipt->payment_id, array('disable_row_level_security' => true));
        $student = BeanFactory::getBean($receipt->parent_type, $receipt->parent_id, array('disable_row_level_security' => true));
        $center = BeanFactory::getBean('Teams', $payment->team_id, array('disable_row_level_security' => true));

        if (empty($student->id))
            $student = BeanFactory::getBean($payment->parent_type, $payment->parent_id, array('disable_row_level_security' => true));
        else { //parse Student
            $studentFields = PdfManagerHelper::parseBeanFields($student, true);
            $studentFields['address'] = $studentFields['primary_address_street'] . ', ' . $studentFields['primary_address_city'];
        }

        //parse Payment
        if (!empty($payment)) {
            $paymentFields = PdfManagerHelper::parseBeanFields($payment, true);
            if ($payment->is_auto_enroll && !empty($payment->ju_class_id)) {
                $paymentFields['count_enrolled'] = $GLOBALS['db']->getOne("SELECT DISTINCT
                    COUNT(l1.id) l1__allcount
                    FROM contacts
                    INNER JOIN j_studentsituations l1 ON contacts.id = l1.student_id AND l1.deleted = 0 AND l1.student_type = 'Contacts' AND l1.type = 'Enrolled'
                    INNER JOIN j_class l2 ON l1.ju_class_id = l2.id AND l2.deleted = 0
                    WHERE (l2.id = '{$payment->ju_class_id}') AND (contacts.id = '{$student->id}') AND contacts.deleted = 0
                    GROUP BY contacts.id");
            }
        }

        if (!empty($receipt)) {
            $receiptFields = PdfManagerHelper::parseBeanFields($receipt, true);
            //format Text
            $int = new Integer();
            $receiptFields['payment_amount_inwords'] = $int->toText($receipt->payment_amount);
            $receiptFields['payment_amount_inwords_en'] = $int->toTextEN($receipt->payment_amount);
        }
        if (!empty($center))
            $centerFields = PdfManagerHelper::parseBeanFields($center, true);

        return array(
            'payment' => $paymentFields,
            'receipt' => $receiptFields,
            'student' => $studentFields,
            'center'  => $centerFields,
            'vietqr'  => getVietQR($receipt),
        );
    }

    public function Output($name = "doc.pdf", $dest = 'I')
    {
        if (!empty($this->pdfFilename)) {
            $name = $this->pdfFilename;
        }
        if (isset($_REQUEST['view_type']))
            $dest = $_REQUEST['view_type'];

        parent::Output($name, $dest); //I: Open luôn - D: Download
    }

    public function Image($file, $x = '', $y = '', $w = 0, $h = 0, $type = '', $link = '', $align = '', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false, $altimgs = array())
    {
        // Handle create tmp images
        $file_path = str_replace($GLOBALS['dotb_config']['site_url'] . '/', '', $file);
        $array_url = explode('/', $file_path);
        if (is_file($file_path) && $array_url[0] == 'upload') {
            $note = BeanFactory::getBean('Notes', $array_url[1], array('disable_row_level_security' => true));
            if ($note->id) {
                $file_name = $note->id . '.' . $note->file_ext;
                $content = dotb_file_get_contents($file_path);
                $file = dotb_cached('modules/PdfManager/tpls/' . $file_name);
                dotb_file_put_contents($file, $content);
            }
        }

        return parent::Image($file, $x, $y, $w, $h, $type, $link, $align, $resize, $dpi, $palign, $ismask, $imgmask, $border, $fitbox);
    }
}
