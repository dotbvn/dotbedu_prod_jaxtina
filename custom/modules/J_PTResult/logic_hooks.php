<?php
    // Do not store anything in this file that is not part of the array or the hook version.  This file will
    // be automatically rebuilt in the future.
    $hook_version = 1;
    $hook_array = Array();
    //position, file, function
    $hook_array['before_save'] = Array();
    $hook_array['before_save'][] = Array(2, 'update Info', 'custom/modules/J_PTResult/PTResultLogicHook.php','PTResultLogicHook', 'updateStudentInfo');
    $hook_array['after_save'] = Array();
    $hook_array['after_save'][] = Array(1, 'Handle CJ After Save', 'custom/modules/J_PTResult/logicCJJ_PTResult.php','logicCJJ_PTResult', 'handleCJAfterSave');
?>