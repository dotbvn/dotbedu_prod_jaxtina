<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class PTResultLogicHook{
    function updateStudentInfo(&$bean, $event, $arg) {
        if($bean->parent == "Leads") {
            $student = BeanFactory::getBean('Leads',$bean->student_id, array('disable_row_level_security' => true));
            $change = 0;
            $status = $student->status;
            $last_pt_status = $student->last_pt_status;

            $meeting_id = $_REQUEST['meeting_id'];
            if(empty($meeting_id)) $meeting_id = $bean->meeting_id;
            if(empty($meeting_id)){
                $meeting = $bean->meeting_link->focus;
                $meeting->date_start = $meeting->time_start;
            }
            else
                $meeting = BeanFactory::getBean('Meetings', $meeting_id);
            //update Lead Status
            $notInStatus = array(
                'Appointment',
                'Ready to PT',
                'Ready to Demo',
                'PT/Demo',
                'Converted',
                'Deposit',
                'New Sale');

            $last_pt_status    = 'Registered';   //Handle PT filter

            if(!in_array($status,$notInStatus)){
                $status = 'Ready to PT';
                if($bean->type_result == 'Demo') $status = 'Ready to Demo';
                if(array_key_exists('Appointment', $GLOBALS['app_list_strings']['lead_status_dom']))
                    $status = 'Appointment';
            }
            $notInStatus = array('PT/Demo', 'Converted', 'Deposit','New Sale');
            if($bean->attended === 'Yes'){
                $last_pt_status    = 'Attended';
                if(!in_array($status,$notInStatus))
                    $status = 'PT/Demo';
            }
            if($student->last_pt_status != $last_pt_status && !empty($last_pt_status)){
                $change++;
                $student->last_pt_status = $last_pt_status;
            }

            if($student->status != $status && !empty($status) ){
                $change++;
                $student->status = $status;
            }

            if($student->last_pt_date != $meeting->date_start) $change++;
            $student->last_pt_date = $meeting->date_start;
            if($change > 0)
                $student->save();
        }
        $bean->assigned_user_id = $student->assigned_user_id;
        //        if($bean->date_entered == $bean->date_modified)
        //            $bean->attended = 1;
    }
}
?>
