<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicCJJ_PTResult
{
    function handleCJAfterSave(&$bean, $event, $arguments){
        // Auto-complete Customer Journey Task - Add by phgiahannn
        global $dotb_config;
        $enabled_mods = $dotb_config['additional_js_config']['customer_journey']['enabled_modules'];
        if (!empty($enabled_mods) && hasInProgressCJ($bean->parent, $bean->student_id)) {
            autoCompleteCJTask($bean);
        }
    }
}


?>