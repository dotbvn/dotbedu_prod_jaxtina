
//TRIGGER DO WHEN
DOTB.util.doWhen(
    function() {
        return $('a[data-subpanel=whole_subpanel_sub_demo_result]').length == 1;
    },
    function() {
        $('a[data-subpanel=whole_subpanel_sub_demo_result]').trigger('click');
});

DOTB.util.doWhen(
    function() {
        return $('a[data-subpanel=whole_subpanel_sub_pt_result]').length == 1;
    },
    function() {
        $('a[data-subpanel=whole_subpanel_sub_pt_result]').trigger('click');
});