var meeting_type = $('#meeting_type').val();
$(document).ready(function () {
    $('#btnFreeText').live('click', function () {
        if ($("input.custom_checkbox:checked").length > 0)
            openPopupSendSMSForMultiPTDemo("J_PTResult", "");
        else
            alertUser('choice', DOTB.language.get('Meetings','LBL_ALERT_RECIPIENT'));
    });

    $('#btnSendEmail').live('click', function () {
        if ($("input.custom_checkbox:checked").length > 0){
            $.ajax({
                type: "POST",
                url: "index.php?module=Meetings&action=ajaxMeeting&dotb_body_only=true",
                data:  {
                    list_id : $('#J_PTResult_checked_str').val(),
                    type    : 'send_email',
                },
                dataType: "json",
                success:function(data){
                    if (data.success == "1") {
                        var obj = JSON.parse(data.onEmail);
                        DOTB.quickCompose.init(obj);
                    }else
                        toastr.error(data.error);
                },
                error: function(){
                    toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
                },
            });
        }else
            alertUser('choice', DOTB.language.get('Meetings','LBL_ALERT_RECIPIENT'));
    });

    //hide subpanel default
    $('#list_subpanel_sub_pt_result table:eq(1)').hide();
    $('#list_subpanel_sub_demo_result table:eq(1)').hide();
    //table sorter
    //dragTable();


    $('.check_attended').live('change', function () {
        var val = $(this).val();
        var tr_res = $(this).closest('tr');

        if(val)
            $(this).closest('td').find('.attended_label').removeClass('no_attended').addClass('yes_attended').text($(this).text());
        else
            $(this).closest('td').find('.attended_label').removeClass('yes_attended').addClass('no_attended').text($(this).text());

        //HED handler
        if(val == 'No' || val == '' || val == 'No result')
            tr_res.find('.listening,.speaking,.reading,.writing,.score,.result_koc').val('');

        var tr_template = tr_res.find('input, textarea, select');
        ajaxUpdatePTResult(tr_template);
    });

    //set multifield
    $('#diagnosis_list').multifield({
        section: '.pt_template',
        addTo: '#tbodyPT', // Append new section to position
        btnAdd: '#btnAddRow',
        btnRemove: '.btn-delete',
        min: 0,
    });

    ///---------Save PT Result-----------////////////////
    $(".writing, .speaking, .listening, .reading, .score, .result_koc, .teacher_comment").live("change", function () {
        handleScore($(this), true);
    });

    $(".ec_note, .teacher_comment").live('change', function () {
        var tr_template = $(this).closest('tr').find('input, textarea, select');
        ajaxUpdatePTResult(tr_template);
    });
});

function handleAddRow() {
    //renum order table//
    $('#tbodyPT tr').each(function () {
        count = $(this).parent().children().index($(this)) + 1;
        $(this).find('.priority').html(count - 1);
        $(this).find('.pt_order').val(count - 1);
    });
}

function handleRemoveRow() {
    renumber_table('#diagnosis_list');
    saveResultAll();
}

//Renumber table rows
function renumber_table(tableID) {
    $(tableID).find("tbody>tr:not(:first-child)").each(function () {
        count = $(this).index();

        //////////-- this is the line that I need to change
        $(this).find('.priority').html(count);
        $(this).find('.pt_order').val(count);
        //////// --- I would like to put sequencial count in the "priority filed, in the sequence INPUT.

    });
}
// Modified by Hieu Pham
function saveResultAll() {
    app.alert.show('saving', {
        level: 'process',
        title: 'Saving'
    });

    app.api.call('read', app.api.buildURL('Meetings', 'save-result') + '?' + $('#form_result').serialize(), null, {
        success: function (data) {
            app.alert.dismiss('saving');
        },
        error: function (data) {
            app.alert.dismiss('saving');
            alertUser('error_infor', 'LBL_ERROR_TRY_AGAIN');
        }
    });
}

function dragTable() {
    //Helper function to keep table row from collapsing when being sorted
    var fixHelperModified = function (e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function (index) {
            $(this).width($originals.eq(index).width());
        });
        return $helper;
    };

    //Make diagnosis table sortable
    $("#diagnosis_list tbody").sortable({
        helper: fixHelperModified,
        stop: function (event, ui) {
            renumber_table('#diagnosis_list');
            saveResultAll();
        }
    });
}

//Modified by Hieu Pham to set_return of open popup
function set_return_parent(popup_reply_data, filter) {

    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    var check = check_lead_in_schedule(name_to_value_array['id'], $("input[name='record']").val());
    var student_type = $('#student_type').val();
    if (!check) {
        $("#btnAddRow").trigger( "click" );
        var _lastrow = $('#tbodyPT tr:last');
        for (var the_key in name_to_value_array) {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key) {
                case 'name':
                    _lastrow.find('.pt_name').text(val);
                    break;
                case 'id':
                    _lastrow.find('.pt_id').val(val);
                    _lastrow.find('.student_type').val(student_type);
                    _lastrow.find('.pt_name').attr('href', '#bwc/index.php?module=' + student_type + '&action=DetailView&record=' + val);
                    var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
                    bwcComponent.convertToLumiaLink(_lastrow.find('.pt_name'));
                    break;
                case 'birthdate':
                    _lastrow.find('.birthdate').text(val);
                    break;
                case 'gender':
                    _lastrow.find('.gender').text(val);
                    break;
                case 'email':
                    _lastrow.find('.email').text(val);
                    break;
                case 'status':
                    _lastrow.find('.status').html(val);
                    break;
                case 'phone_mobile':
                    _lastrow.find('.phone_mobile').text(val);
                    break;
                case 'assigned_user_name':
                    _lastrow.find('.assigned_user_name').text(val);
                    break;
                case 'lead_source':
                    _lastrow.find('.lead_source').text(val);
                    break;
                case 'c_contacts_contacts_1_name':
                case 'guardian_name':
                    if (val != "undefined")
                        _lastrow.find('.parent_name').text(val);
                    break;
            }
        }
        saveResult(_lastrow.find('input, textarea, select'));
    } else {
        alertUser('exist_student', 'LBL_EXIST_STUDENT');
    }
}

function clickChooseLead() {
    var module = $('#student_type').val();
    var duration = parseInt($('input#limit_row_pt').val());
    var count_row_table = $('#tbodyPT tr').length - 1;
    open_popup(module, 1000, 700, "", true, false, {
        "call_back_function": "set_return_parent",
        "form_name": "DetailView",
        "field_to_name_array": {
            "name": "name",
            "id": "id",
            "birthdate": "birthdate",
            "phone_mobile": "phone_mobile",
            "assigned_user_name": "assigned_user_name",
            "guardian_name": "guardian_name",
            "lead_source": "lead_source",
            "gender": "gender",
            "email": "email",
            "status": "status",
        }
        }, "single", true);
}

// Modified by Hieu Pham
function saveResult(tr_template) {
    app.alert.show('saving', {
        level: 'process',
        title: 'Saving'
    });
    app.api.call('read', app.api.buildURL('Meetings', 'save-select') + '?' + tr_template.serialize() + '&meeting_id=' + $("input[name='record']").val(), null, {
        success: function (data) {
            tr_template.closest('tr').find('.id_of_result, .custom_checkbox').val(data);
            tr_template.closest('tr').attr('id','pt_'+data);
            app.alert.dismiss('saving');
        },
        error: function (data) {
            app.alert.dismiss('saving');
            alertUser('error_infor', 'LBL_ERROR_INFORMATION');
        }
    });
}

// Modified by Hieu Pham to check student/lead have exists in PT
function check_lead_in_schedule(student_id, meeting_id) {
    var check = false;
    var preferences = {};
    preferences['student_id'] = student_id;
    preferences['meeting_id'] = meeting_id;
    app.api.call('update', app.api.buildURL('Meetings', 'check-lead-in-schedule'), preferences, {
        success: function (data) {
            check = data;
        }
        }, {async: false});
    return check;
}

function ajaxUpdatePTResult(tr_template) {
    app.alert.show('saving', {
        level: 'process',
        title: 'Saving'
    });
    app.api.call('read', app.api.buildURL('Meetings', 'update-result') + '?' + tr_template.serialize(), null, {
        success: function (data) {
            app.alert.dismiss('saving');
        },
        error: function (data) {
            app.alert.dismiss('saving');
            alertUser('error_infor', 'LBL_ERROR_INFORMATION');
        }
    });
}

function beforeRemoveSection(section) {
    app.alert.show('deleting', {
        level: 'process',
        title: 'Deleting'
    });
    var result = false;
    var preferences = {};
    preferences['result_id'] = $(section).find('.id_of_result').val();
    app.api.call('update', app.api.buildURL('Meetings', 'delete-result'), preferences, {
        success: function (data) {
            result = true;
            app.alert.dismiss('deleting');
        },
        error: function (data) {
            app.alert.dismiss('deleting');
            alertUser('error_again', 'LBL_ERROR_TRY_AGAIN');
        }
        }, {async: false});

    return result;
}

function handleScore(inputS, is_update) {
    var tr_res = inputS.closest('tr');
    var writing = tr_res.find('input.writing').val();
    var speaking = tr_res.find('input.speaking').val();
    var listening = tr_res.find('input.listening').val();
    var reading = tr_res.find('input.reading').val();
    var score = tr_res.find('input.score').val();
    var result_koc = tr_res.find('select.result_koc').val();
    var teacher_comment = tr_res.find('.teacher_comment').val();
    var count_res = 0;

    if (writing != '') count_res++;
    if (speaking != '') count_res++;
    if (listening != '') count_res++;
    if (reading != '') count_res++;
    if (score != '') count_res++;
    if (result_koc != '') count_res++;
    if (teacher_comment != '') count_res++;

    if (writing != '' && speaking != '' && listening != '' && reading != '') inputS.closest('tr').find('.check_attended').val('Yes').trigger('change');

    if (is_update) {
        var tr_template = inputS.closest('tr').find('input, textarea, select');
        ajaxUpdatePTResult(tr_template);
    }

}

function showUploadPTResult(_element) {
    var import_template_link = $(_element).attr('template_link');
    $('#import_pt_result').dialog({
        title: app.lang.get('LBL_IMPORT_PT_RESULT','Meetings'),
        width: "500px",
        resizable: false,
        modal: true,
    });
    $('.import_template_link').attr("onclick",import_template_link);
}

function uploadFilePT(_element) {
    $('#import_pt_result').dialog("close");
    var file = $('.fileToUpload').prop('files')[0];
    var data = new FormData();
    var id = $(_element).attr('meeting_id');
    data.append('file', file);
    $.ajax({
        url: 'index.php?module=Meetings&action=ajaxHandleImportPT&dotb_body_only=true&type=ajaxImportPT&id=' + id ,
        method: 'POST',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            var json = jQuery.parseJSON(data);
            if(json.success)
                fillData(json.data);
            else
                alertUser('error_infor', json.errorLabel);
        },
        error: function(){
            $('input[name=fileToUpload]').val('');
            toastr.error(DOTB.language.languages['app_strings']['LBL_CONNECTION_ERROR']);
        },

    })
}

function fillData(data) {
    $.each(data, function( index, value ) {
        var id = '#pt_' + value['id'];
        if(value['attended'] == null || value['attended'] == "undefined") value['attended'] = '';

        var attended = value['attended'].toString().toLowerCase();
        if (attended.indexOf("none") >= 0 || attended.indexOf("-empty-") >= 0)
            value['attended'] = '';
        else if (attended.indexOf("no") >= 0 || attended.indexOf("absent") >= 0 || Number(attended) == 0)
            value['attended'] = 'No';
            else if (attended.indexOf("yes") >= 0 || attended.indexOf("present") >= 0 || Number(attended) > 0)
                value['attended'] = 'Yes';
                else value['attended'] = '';

        $(id).find('[name ="check_attended[]"]').val(value['attended']);
        $(id).find('[name ="listening[]"]').val(value['listening']);
        $(id).find('[name ="speaking[]"]').val(value['speaking']);
        $(id).find('[name ="reading[]"]').val(value['reading']);
        $(id).find('[name ="writing[]"]').val(value['writing']);
        $(id).find('[name ="score[]"]').val(value['score']);
        var result = value['result'];

        $(id).find('[name ="result_koc[]"]').val(result);
        var result_sel = $(id).find('[name ="result_koc[]"]').val();
        if(result_sel == '' || result_sel == null)
            $(id).find('[name ="result_koc[]"] option:contains('+result+')').prop("selected",true)

        $(id).find('[name ="ec_note[]"]').val(value['ec_note']);
        $(id).find('[name ="teacher_comment[]"]').val(value['teacher_comment']);

        handleScore($(id), true);
    });
    toastr.success('Import Complete!!');
}

function alertUser(key, title, msg) {
    app.alert.show(key, {
        level: 'error',
        messages: app.lang.get(msg, 'Meetings'),
        title: app.lang.get(title, 'Meetings'),
        autoClose: true,
    });
}