<?php

use Dotbcrm\Dotbcrm\Util\Arrays\ArrayFunctions\ArrayFunctions;
include_once("include/workflow/alert_utils.php");
include_once("include/workflow/action_utils.php");
include_once("include/workflow/time_utils.php");
include_once("include/workflow/trigger_utils.php");
//BEGIN WFLOW PLUGINS
include_once("include/workflow/custom_utils.php");
//END WFLOW PLUGINS
	class Meetings_workflow {
	function process_wflow_triggers(& $focus){
		include("custom/modules/Meetings/workflow/triggers_array.php");
		include("custom/modules/Meetings/workflow/alerts_array.php");
		include("custom/modules/Meetings/workflow/actions_array.php");
		include("custom/modules/Meetings/workflow/plugins_array.php");
		if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if((isset($focus->lesson_number) && $focus->lesson_number ==  stripslashes('2'))){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 
	 //Secondary Trigger number #1
	 if((isset($focus->session_status) && $focus->session_status ==  stripslashes('Finished'))	 ){ 
	 

	 //Secondary Trigger number #2
	 if((
 	 ( 
 		isset($focus->process_trigger) && $focus->process_trigger === true ||
 		isset($focus->process_trigger) && $focus->process_trigger === 'true' ||
 		isset($focus->process_trigger) && $focus->process_trigger === 'on' ||
 		isset($focus->process_trigger) && $focus->process_trigger === 1 ||
 		isset($focus->process_trigger) && $focus->process_trigger === '1'
 	 )  
)	 ){ 
	 


	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['e778c820_fb6c_11ed_bac4_067482011a2a'])){
		$triggeredWorkflows['e778c820_fb6c_11ed_bac4_067482011a2a'] = true;
		$action_meta_array['Meetings0_action0']['trigger_id'] = 'e778c820_fb6c_11ed_bac4_067482011a2a';
	$action_meta_array['Meetings0_action0']['action_id'] = '0d2d5534-0980-11ed-96a8-067482011a2a';
	 $action_meta_array['Meetings0_action0']['workflow_id'] = 'd9497fb4-097e-11ed-8439-067482011a2a';
	 process_workflow_actions($focus, $action_meta_array['Meetings0_action0']); 
 	$_SESSION['WORKFLOW_ALERTS'] = isset($_SESSION['WORKFLOW_ALERTS']) && ArrayFunctions::is_array_access($_SESSION['WORKFLOW_ALERTS']) ? $_SESSION['WORKFLOW_ALERTS'] : array();
		$_SESSION['WORKFLOW_ALERTS']['Meetings'] = isset($_SESSION['WORKFLOW_ALERTS']['Meetings']) && ArrayFunctions::is_array_access($_SESSION['WORKFLOW_ALERTS']['Meetings']) ? $_SESSION['WORKFLOW_ALERTS']['Meetings'] : array();
		$_SESSION['WORKFLOW_ALERTS']['Meetings'] = ArrayFunctions::array_access_merge($_SESSION['WORKFLOW_ALERTS']['Meetings'],array ());	}
 

	 //End Frame Secondary 

	 // End Secondary Trigger number #1
 	 } 

	 // End Secondary Trigger number #2
 	 } 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 


	//end function process_wflow_triggers
	}

	//end class
	}

?>