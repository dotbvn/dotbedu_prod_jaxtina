<?php
// created: 2019-04-20 22:24:29
$viewdefs['Meetings']['portal']['view']['subpanel-for-j_class-j_class_meetings'] = array(
    'panels' =>
        array(
            0 =>
                array(
                    'name' => 'panel_header',
                    'label' => 'LBL_PANEL_1',
                    'fields' =>
                        array(
                            0 =>
                                array(
                                    'name' => 'lesson_number',
                                    'label' => 'LBL_LESSON_NUMBER',
                                    'enabled' => true,
                                    'type' => 'int',
                                    'default' => true,
                                    'sortable' => false,
                                    'width' => 3
                                ),
                            1 =>
                                array(
                                    'name' => 'week_date',
                                    'label' => 'LBL_WEEK_DATE',
                                    'enabled' => true,
                                    'default' => true,
                                    'sortable' => false,
                                    'width' => 5
                                ),
                            2 =>
                                array(
                                    'name' => 'time_start_end',
                                    'vname' => 'LBL_DATE_TIME',
                                    'type' => 'html',
                                    'default' => true,
                                    'sortable' => false,
                                    'width' => 15
                                ),
                            3 =>
                                array(
                                    'name' => 'duration_text',
                                    'label' => 'LBL_DURATION',
                                    'enabled' => true,
                                    'default' => true,
                                    'sortable' => false,
                                    'type' => 'html',
                                    'width' => 10
                                ),
                            4 =>
                                array(
                                    'name' => 'syllabus_topic',
                                    'label' => 'LBL_SYL_TOPIC',
                                    'enabled' => true,
                                    'sortable' => false,
                                    'default' => true,
                                    'width' => 15
                                ),
                            5 =>
                                array(
                                    'name' => 'syllabus_homework',
                                    'label' => 'LBL_SYL_HOMEWORK',
                                    'enabled' => true,
                                    'sortable' => false,
                                    'default' => true,
                                    'width' => 15
                                ),
                            6 =>
                                array(
                                    'name' => 'student_feedback',
                                    'label' => 'LBL_STUDENT_FEEDBACK',
                                    'enabled' => true,
                                    'default' => true,
                                    'sortable' => false,
                                    'type' => 'html',
                                    'width' => 10
                                ),
                            7 =>
                                array(
                                    'name' => 'teacher_comment',
                                    'label' => 'LBL_TEACHER_COMMENT',
                                    'enabled' => true,
                                    'default' => true,
                                    'sortable' => false,
                                    'type' => 'html',
                                    'width' => 10
                                ),
                            8 =>
                                array(
                                    'name' => 'room_name',
                                    'label' => 'LBL_ROOM_NAME',
                                    'enabled' => true,
                                    'id' => 'ROOM_ID',
                                    'link' => true,
                                    'sortable' => false,
                                    'default' => true,
                                    'width' => 5
                                ),
                            9 =>
                                array(
                                    'name' => 'attendance',
                                    'label' => 'LBL_ATTENDANCE',
                                    'enabled' => true,
                                    'default' => true,
                                    'sortable' => false,
                                    'type' => 'html',
                                    'width' => 7
                                ),
                            10 =>
                                array(
                                    'name' => 'student_rate',
                                    'label' => 'LBL_RATE',
                                    'enabled' => true,
                                    'default' => true,
                                    'sortable' => false,
                                    'type' => 'html',
                                    'width' => 5
                                ),
                            11 =>
                                array(
                                    'name' => 'date_start',
                                    'default' => false,
                                ),
                            12 =>
                                array(
                                    'name' => 'date_end',
                                    'default' => false,
                                ),
                            13 =>
                                array(
                                    'name' => 'duration_minutes',
                                    'default' => false,
                                ),
                            14 =>
                                array(
                                    'name' => 'duration_hours',
                                    'default' => false,
                                ),

                        ),
                ),
        ),
    'orderBy' =>
        array (
            'field' => 'date_start',
            'direction' => 'asc',
        ),
    'type' => 'subpanel-list',
);