<?php

$viewdefs['Meetings']['base']['filter']['basic'] = array(
    'create' => true,
    'quicksearch_field' => array('name'),
    'quicksearch_priority' => 1,
    'quicksearch_split_terms' => false,
    'filters' => array(
//        array(
//            'id' => 'dashboard_schedule',
//            'name' => 'ONLINE CLASSES - NEXT 7 DAYS',
//            'filter_definition' => array(
//                array('meeting_type' => array('$in' => array('Session'))),
//                array('session_status' => array('$not_in' => array('Cancelled'))),
//                array('date_start' => array('$dateRange' => 'next_7_days')),
//            ),
//            'is_template' => true,
//            'editable' => true,
//            'order' => 1,
//            'orderBy' => array(
//                'field' => 'date_start',
//                'direction' => 'asc'
//            ),
//        ),

        array(
            'id' => 'all_records',
            'name' => 'LBL_LISTVIEW_FILTER_ALL',
            'filter_definition' => array(
                array(
                    'meeting_type' => array(
                        '$in' => array('Meeting', 'Demo', 'Placement Test'),
                    ),
                ),
            ),
            'editable' => false,
            'order' => 2,
            'orderBy' => array(
                'field' => 'date_start',
                'direction' => 'desc'
            ),
        ),

        array(
            'id' => 'by_type_pt',
            'name' => 'LBL_MY_PT',
            'filter_definition' => array(
                array(
                    'meeting_type' => array(
                        '$in' => array('Placement Test'),
                    ),
                ),
            ),
            'editable' => false,
            'order' => 3,
            'orderBy' => array(
                'field' => 'date_start',
                'direction' => 'desc'
            ),
        ),

        array(
            'id' => 'by_type_demo',
            'name' => 'LBL_MY_DEMO',
            'filter_definition' => array(
                array(
                    'meeting_type' => array(
                        '$in' => array('Demo'),
                    ),
                ),
            ),
            'editable' => false,
            'order' => 4,
            'orderBy' => array(
                'field' => 'date_start',
                'direction' => 'desc'
            ),
        ),
        array(
            'id' => 'recently_viewed',
            'name' => 'LBL_RECENTLY_VIEWED',
            'filter_definition' => array(
                '$tracker' => '-7 DAY',
            ),
            'editable' => false,
            'order' => 5,
        ),
        array(
            'id' => 'recently_created',
            'name' => 'LBL_NEW_RECORDS',
            'filter_definition' => array(
                'date_entered' => array(
                    '$dateRange' => 'last_7_days',
                ),
            ),
            'editable' => false,
            'order' => 6,
        ),
    ),
);
