<?php
// created: 2021-09-20 03:59:04
$viewdefs['Meetings']['base']['filter']['default'] = array (
    'default_filter' => 'all_records',
    'fields' =>
    array (
        'name' =>
        array (
        ),
        'meeting_type' =>
        array (
        ),
        'date_start' =>
        array (
        ),
        'date_end' =>
        array (
        ),
        'status' =>
        array (
        ),
        'parent_name' =>
        array (
        ),
        'location' =>
        array (
        ),
        'created_by_name' =>
        array (
        ),
        'team_name' =>
        array (
        ),
        'reminder_time' =>
        array (
        ),
        'email_reminder_time' =>
        array (
        ),
        'assigned_user_name' =>
        array (
        ),
        'send_invites' =>
        array (
        ),
        'date_entered' =>
        array (
        ),
        '$owner' =>
        array (
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' =>
        array (
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
        'session_status' =>
        array (
        ),
        'teacher_name' =>
        array (
        ),
        'sub_teacher_name' =>
        array (
        ),
        'teacher_cover_name' =>
        array (
        ),
        'room_name' =>
        array (
        ),
        'syllabus_type' =>
        array (
        ),
        'modified_by_name' =>
        array (
        ),
    ),
);