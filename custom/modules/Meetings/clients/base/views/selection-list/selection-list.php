<?php
$viewdefs['Meetings'] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'selection-list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_LIST_SUBJECT',
                'link' => true,
                'default' => true,
                'enabled' => true,
              ),
              1 =>
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'default' => true,
                'enabled' => true,
                'sortable' => false,
              ),
              2 =>
              array (
                'name' => 'status',
                'label' => 'LBL_STATUS',
                'default' => true,
                'enabled' => true,
                'sortable' => false,
              ),
              3 =>
              array (
                'name' => 'date_start',
                'label' => 'LBL_LIST_DATE',
                'link' => false,
                'default' => true,
                'enabled' => true,
              ),
              4 =>
              array (
                'name' => 'number_of_student',
                'label' => 'LBL_REGISTER',
                'default' => true,
                'enabled' => true,
                'type' => 'html',
              ),
              5 =>
              array (
                'name' => 'attended_label',
                'label' => 'LBL_TAKER',
                'default' => true,
                'enabled' => true,
                'type' => 'html',
              ),
              6 =>
              array (
                'link' => true,
                'type' => 'relate',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'id' => 'ASSIGNED_USER_ID',
                'default' => true,
                'name' => 'assigned_user_name',
                'enabled' => true,
              ),
              7 =>
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAMS',
                'enabled' => true,
                'id' => 'TEAM_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
            ),
          ),
        ),
      ),
    ),
  ),
);
