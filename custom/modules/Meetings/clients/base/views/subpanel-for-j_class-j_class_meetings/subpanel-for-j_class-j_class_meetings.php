<?php
// created: 2019-05-15 23:27:15
$viewdefs['Meetings']['base']['view']['subpanel-for-j_class-j_class_meetings'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'lesson_number',
          'label' => 'LBL_LESSON_NUMBER',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'till_hour',
          'label' => 'LBL_TILL_HOUR',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_SUBJECT',
          'enabled' => true,
          'default' => true,
          'link' => true,
        ),
        3 => 
        array (
          'name' => 'session_status',
          'label' => 'LBL_SESSION_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'teacher_name',
          'label' => 'LBL_TEACHER_NAME',
          'enabled' => true,
          'id' => 'TEACHER_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'teacher_cover_name',
          'label' => 'LBL_TEACHER_COVER_NAME',
          'enabled' => true,
          'id' => 'TEACHER_COVER_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'room_name',
          'label' => 'LBL_ROOM_NAME',
          'enabled' => true,
          'id' => 'ROOM_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'time_start_end',
          'label' => 'LBL_TIME',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'duration_cal',
          'label' => 'LBL_DURATION',
          'enabled' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'teaching_hour',
          'label' => 'LBL_TEACHING_HOUR',
          'enabled' => true,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'teaching_type',
          'label' => 'LBL_TEACHING_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'rowactions' => 
  array (
    'actions' => 
    array (
      0 => 
      array (
        'type' => 'rowaction',
        'css_class' => 'btn',
        'tooltip' => 'LBL_PREVIEW',
        'event' => 'list:preview:fire',
        'icon' => 'fa-search-plus',
        'acl_action' => 'view',
      ),
      1 => 
      array (
        'type' => 'rowaction',
        'name' => 'edit_button',
        'icon' => 'fa-pencil',
        'label' => 'LBL_EDIT_BUTTON',
        'event' => 'list:editrow:fire',
        'acl_action' => 'edit',
      ),
      2 => 
      array (
        'type' => 'unlink-action',
        'icon' => 'fa-chain-broken',
        'label' => 'LBL_UNLINK_BUTTON',
      ),
      3 => 
      array (
        'type' => 'closebutton',
        'icon' => 'fa-times-circle',
        'name' => 'record-close',
        'label' => 'LBL_CLOSE_BUTTON_TITLE',
        'closed_status' => 'Held',
        'acl_action' => 'edit',
      ),
    ),
  ),
  'type' => 'subpanel-list',
);