<?php
$viewdefs['Meetings'] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_1',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'name',
                                'label' => 'LBL_LIST_SUBJECT',
                                'link' => true,
                                'default' => true,
                                'enabled' => true,
                                'related_fields' =>
                                array (
                                    0 => 'repeat_type',
                                ),
                                'width' => 'large',
                            ),
                            1 =>
                            array (
                                'name' => 'meeting_type',
                                'label' => 'LBL_MEETING_TYPE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            2 =>
                            array (
                                'name' => 'number_of_student',
                                'label' => 'LBL_REGISTER',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            3 =>
                            array (
                                'name' => 'attended_label',
                                'label' => 'LBL_TAKER',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            4 =>
                            array (
                                'name' => 'description',
                                'label' => 'LBL_DESCRIPTION',
                                'enabled' => true,
                                'sortable' => false,
                                'default' => true,
                                'width' => 'medium',
                            ),
                            5 =>
                            array (
                                'name' => 'date_start',
                                'label' => 'LBL_LIST_DATE',
                                'type' => 'datetimecombo-colorcoded',
                                'css_class' => 'overflow-visible',
                                'completed_status_value' => 'Held',
                                'link' => false,
                                'default' => true,
                                'enabled' => true,
                                'readonly' => true,
                                'related_fields' =>
                                array (
                                    0 => 'status',
                                ),
                            ),
                            6 =>
                            array (
                                'name' => 'date_end',
                                'link' => false,
                                'default' => true,
                                'enabled' => true,
                            ),
                            7 =>
                            array (
                                'name' => 'status',
                                'type' => 'event-status',
                                'label' => 'LBL_LIST_STATUS',
                                'link' => false,
                                'default' => true,
                                'enabled' => true,
                                'css_class' => 'full-width',
                            ),
                            8 =>
                            array (
                                'name' => 'assigned_user_name',
                                'label' => 'LBL_LIST_ASSIGNED_USER',
                                'id' => 'ASSIGNED_USER_ID',
                                'default' => true,
                                'enabled' => true,
                            ),
                            9 =>
                            array (
                                'name' => 'team_name',
                                'label' => 'LBL_LIST_TEAM',
                                'default' => true,
                                'enabled' => true,
                            ),
                            10 =>
                            array (
                                'name' => 'ju_class_name',
                                'label' => 'LBL_JU_CLASS_NAME',
                                'enabled' => true,
                                'id' => 'JU_CLASS_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => false,
                                'width' => 'large',
                            ),
                            11 =>
                            array (
                                'name' => 'lesson_number',
                                'label' => 'LBL_LESSON_NUMBER',
                                'enabled' => true,
                                'default' => false,
                                'width' => 'small',
                            ),
                            12 =>
                            array (
                                'name' => 'syllabus_type',
                                'label' => 'LBL_SYL_TYPE',
                                'enabled' => true,
                                'default' => false,
                            ),
                            13 =>
                            array (
                                'name' => 'syllabus_topic',
                                'label' => 'LBL_SYL_TOPIC',
                                'enabled' => true,
                                'default' => false,
                                'width' => 'large',
                            ),
                            14 =>
                            array (
                                'name' => 'syllabus_activities',
                                'label' => 'LBL_SYL_SYLLABUS',
                                'enabled' => true,
                                'default' => false,
                                'width' => 'large',
                            ),
                            15 =>
                            array (
                                'name' => 'syllabus_homework',
                                'label' => 'LBL_SYL_HOMEWORK',
                                'enabled' => true,
                                'default' => false,
                                'width' => 'large',
                            ),
                            16 =>
                            array (
                                'name' => 'syllabus_objective',
                                'label' => 'LBL_SYL_NOTE_FOR_TEACHER',
                                'enabled' => true,
                                'default' => false,
                                'width' => 'medium',
                            ),
                            17 =>
                            array (
                                'name' => 'join_url',
                                'label' => 'LBL_URL',
                                'enabled' => true,
                                'default' => false,
                                'type' => 'html',
                                'width' => 'xsmall',
                            ),
                            18 =>
                            array (
                                'name' => 'teacher_name',
                                'label' => 'LBL_TEACHER_NAME',
                                'enabled' => true,
                                'id' => 'TEACHER_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => false,
                                'width' => 'medium',
                            ),
                            19 =>
                            array (
                                'name' => 'teacher_cover_name',
                                'label' => 'LBL_TEACHER_COVER_NAME',
                                'enabled' => true,
                                'id' => 'TEACHER_COVER_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => false,
                            ),
                            20 =>
                            array (
                                'name' => 'sub_teacher_name',
                                'label' => 'LBL_SUB_TEACHER_NAME',
                                'enabled' => true,
                                'id' => 'SUB_TEACHER_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => false,
                            ),
                            21 =>
                            array (
                                'name' => 'date_modified',
                                'label' => 'LBL_DATE_MODIFIED',
                                'enabled' => true,
                                'readonly' => true,
                                'default' => false,
                            ),
                            22 =>
                            array (
                                'name' => 'modified_by_name',
                                'label' => 'LBL_MODIFIED',
                                'enabled' => true,
                                'readonly' => true,
                                'id' => 'MODIFIED_USER_ID',
                                'link' => true,
                                'default' => false,
                            ),
                            23 =>
                            array (
                                'name' => 'type',
                                'label' => 'LBL_TYPE',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
