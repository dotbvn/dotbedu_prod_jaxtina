<?php
// created: 2020-12-04 16:54:40
$viewdefs['Meetings']['base']['view']['subpanel-for-c_teachers-teacher_meetings'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'lesson_number',
          'label' => 'LBL_LESSON_NUMBER',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'till_hour',
          'label' => 'LBL_TILL_HOUR',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'name',
          'label' => 'LBL_LIST_SUBJECT',
          'default' => true,
          'enabled' => true,
          'link' => true,
          'type' => 'name',
        ),
        3 => 
        array (
          'name' => 'session_status',
          'label' => 'LBL_SESSION_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'week_date',
          'label' => 'LBL_WEEK_DATE',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'time_start_end',
          'label' => 'LBL_TIME',
          'enabled' => true,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'duration',
          'label' => 'LBL_DURATION',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'teaching_hour',
          'label' => 'LBL_TEACHING_HOUR',
          'enabled' => true,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'teacher_name',
          'label' => 'LBL_TEACHER_NAME',
          'enabled' => true,
          'id' => 'TEACHER_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'sub_teacher_name',
          'label' => 'LBL_SUB_TEACHER_NAME',
          'enabled' => true,
          'id' => 'SUB_TEACHER_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        10 => 
        array (
          'name' => 'teacher_cover_name',
          'label' => 'LBL_TEACHER_COVER_NAME',
          'enabled' => true,
          'id' => 'TEACHER_COVER_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        11 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);