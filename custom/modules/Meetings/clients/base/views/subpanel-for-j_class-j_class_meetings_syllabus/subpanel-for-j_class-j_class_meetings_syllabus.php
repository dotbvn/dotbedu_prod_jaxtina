<?php
// created: 2019-04-20 22:24:29
$viewdefs['Meetings']['base']['view']['subpanel-for-j_class-j_class_meetings_syllabus'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'name' => 'lesson_number',
          'label' => 'LBL_LESSON_NUMBER',
          'enabled' => true,
          'default' => true,
          'sortable' => false,
        ),
        1 =>
        array (
          'name' => 'week_date',
          'label' => 'LBL_WEEK_DATE',
          'enabled' => true,
          'default' => true,
          'sortable' => false,
        ),
        2 =>
        array (
          'name' => 'date_start',
          'label' => 'LBL_LIST_DATE',
          'default' => true,
          'enabled' => true,
          'type' => 'datetimecombo',
        ),
        3 =>
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
          'sortable' => false,
        ),
        4 =>
        array (
          'name' => 'time_start_end',
          'label' => 'LBL_TIME',
          'enabled' => true,
          'default' => true,
        ),
        6 =>
        array (
          'name' => 'syllabus_topic',
          'label' => 'LBL_SYL_TOPIC',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        7 =>
        array (
          'name' => 'syllabus_activities',
          'label' => 'LBL_SYL_SYLLABUS',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        8 =>
        array (
          'name' => 'syllabus_homework',
          'label' => 'LBL_SYL_HOMEWORK',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        10 =>
        array (
          'name' => 'homework',
          'label' => 'LBL_HOMEWORK',
          'enabled' => true,
          'default' => true,
          'sortable' => false,
        ),
      ),
    ),
  ),
  'type' => 'subpanel-list',
);