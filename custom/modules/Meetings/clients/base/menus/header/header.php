<?php

$module_name = 'Meetings';
$viewdefs[$module_name]['base']['menu']['header'] = array(
    array(
        'route' => "#bwc/index.php?module=Meetings&action=EditView&return_module=Meetings&return_action=DetailView&type=PT",
        'label' => 'LNK_NEW_TESTING',
        'acl_action' => 'create',
        'acl_module' => $module_name,
        'icon' => 'fa-plus',
    ),
    // Hide menu create Demo and Meeting by HP
    array(
        'route' => "#bwc/index.php?module=Meetings&action=EditView&return_module=Meetings&return_action=DetailView&type=Demo",
        'label' => 'LNK_NEW_DEMO',
        'acl_action' => 'create',
        'acl_module' => $module_name,
        'icon' => 'fa-plus',
    ),
    array(
        'route' => "#{$module_name}/create",
        'label' => 'LNK_NEW_MEETING',
        'acl_action' => 'create',
        'acl_module' => $module_name,
        'icon' => 'fa-plus',
    ),

    
//    array(
//        'route' => "#bwc/index.php?module=Import&action=Step1&import_module={$module_name}&return_module={$module_name}&return_action=index",
//        'label' => 'LNK_IMPORT_MEETINGS',
//        'acl_action' => 'import',
//        'acl_module' => $module_name,
//        'icon' => 'fa-cloud-upload',
//    ),
);
