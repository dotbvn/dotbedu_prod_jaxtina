{dotb_getscript file="modules/Administration/smsPhone/javascript/jquery.jmpopups-0.5.1.js"}
{dotb_getscript file="modules/Administration/smsPhone/javascript/smsPhone.js"}
{dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.js"}
<link rel="stylesheet" type="text/css" href="modules/Administration/smsPhone/style/smsPhone.css"/>
{literal}
<style>
    .ui-sortable tr {
    cursor:move;
    }
    .pt_template:hover td{
    background:#ecf7ff;
    }
    .yes_attended{
    color: #008000;
    font-weight: bold;
    }
    .no_attended{
    color: #FF0000;
    font-weight: bold;
    }
</style>
{/literal}
<form method="POST" name="form_result" id="form_result" enctype="multipart/form-data">
    <table id="diagnosis_list" width="100%" border="0" class="list view">
        <thead>
            <tr class="row" style="margin-left: 5px;margin-top: 10px;">
                <td colspan="10">
                    <div style="float:left;" class="selectedTopSupanel"></div>

                    <select style="float:left;" name="student_type" id="student_type" style="margin-left: 20px;">
                        <option value="Leads">Lead</option>
                        <option value="Contacts">Student</option>
                    </select>
                    <button  style="float:left; margin-left: 20px;" type="button" id="btnSelectDemo" class="button" onclick="clickChooseLead()" >{$MOD.LBL_SELECT}</button>

                    <button  style="float:left; margin-left: 20px;display:none" type="button" id="btnAddRow">{$MOD.LBL_SELECT}</button>
                    <button  style="float:left; margin-left: 20px;" type="button" id="btnFreeText" class="button" >{$MOD.LBL_FREE_TEXT}</button>
                    <button  style="float:left; margin-left: 20px;" type="button" id="btnSendEmail" class="button" >{$MOD.LBL_FREE_EMAIL}</button>                    <a class="button" style="float:left; margin-left: 20px;" target="_parent" href="{$uri}#Reports/8b13014e-5efe-11ea-887d-1ae6c757f9ff">{$MOD.LBL_BTN_EXPORT_RESULT}</a>
                    </td>
            </tr>
            <tr>
                <th style="text-align: left;padding-left: 5px !important;">
                    <input  type="checkbox" class="checkall_custom_checkbox" module_name="J_PTResult" onclick="handleCheckBox($(this));"/>
                </th>
                <th style="text-align: center;width: 1%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_NO}</span></th>
                <th style="text-align: center;width: 15%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_NAME}</span></th>
                <th style="text-align: center;width: 7%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_GENDER}</span></th>
                <th style="text-align: center;width: 7%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_BIRTHDAY}</span></th>
                <th style="text-align: center;width: 15%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_PARENT}</span></th>
                <th style="text-align: center;width: 11%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_MOBILE}</span></th>
                <th style="text-align: center;width: 11%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_EMAIL}</span></th>
                <th style="text-align: center;width: 7%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_SESSION_STATUS}</span></th>
                <th style="text-align: center;width: 9%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_SOURCE}</span></th>
                <th style="text-align: center;width: 15%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_ASSIGNED_TO_NAME}</span></th>
                <th style="text-align: center;width: 8%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_ATTENDED}</span></th>
                <th style="text-align: center;width: 15%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_EC_NOTE}</span></th>
                <th style="text-align: center;width: 15%;"><span dotb="slot12" style="white-space:normal;">{$MOD.LBL_TEACHER_COMMENT}</span></th>

                <th style="text-align: center;"><span dotb="slot12" style="white-space:normal;">&nbsp;</span></th>
            </tr>
        </thead>
        <tbody id="tbodyPT">
            {$html_tpl}
            {$html}
        </tbody>
    </table>
</form>
{dotb_getscript file="custom/modules/Meetings/js/ptdemo.js"}
<?php die(); ?>