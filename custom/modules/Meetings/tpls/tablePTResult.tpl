{dotb_getscript file="modules/Administration/smsPhone/javascript/jquery.jmpopups-0.5.1.js"}
{dotb_getscript file="modules/Administration/smsPhone/javascript/smsPhone.js"}
{dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.js"}
<link rel="stylesheet" type="text/css" href="modules/Administration/smsPhone/style/smsPhone.css" />
{literal}
<style>
    .ui-sortable tr {
    cursor:move;
    }
    .pt_template:hover td{
    background:#ecf7ff;
    }
    .yes_attended{
    color: #008000;
    font-weight: bold;
    }
    .no_attended{
    color: #FF0000;
    font-weight: bold;
    }
</style>
{/literal}
<form method="POST" name="form_result" id="form_result" enctype="multipart/form-data">
    <div>
     <table class="list view" id="diagnosis_list">
        <thead>
            <tr class="row" style="margin-left: 5px;margin-top: 10px;">
                <td colspan="12">

                    <span style="float:left;" class="selectedTopSupanel"></span>
                    <select style="float:left;" name="student_type" id="student_type" style="margin-left: 20px;">
                        <option value="Leads">Lead</option>
                        <option value="Contacts">Student</option>
                    </select>

                    <button  style="float:left; margin-left: 20px;" type="button" id="btnSelect" class="button" onclick="clickChooseLead()" >{$MOD.LBL_SELECT}</button>
                    <button  style="float:left; margin-left: 20px;display:none"  type="button" id="btnAddRow" class="button" >{$MOD.LBL_SELECT}</button>
                    <button  style="float:left; margin-left: 20px;" type="button" id="btnFreeText" class="button" >{$MOD.LBL_FREE_TEXT}</button>
                    <button  style="float:left; margin-left: 20px;" type="button" id="btnSendEmail" class="button" >{$MOD.LBL_FREE_EMAIL}</button>
                    {if $isExport}
                    <a class="button" style="float:left; margin-left: 20px;" target="_parent" href="{$uri}#Reports/4ec88572-57b7-11ea-ade8-02170a961336">{$MOD.LBL_BTN_EXPORT_RESULT}</a>
                    <a class="button" style="float:left; margin-left: 20px;" href="javascript: void(0);" class="import_template_link" onclick="window.open('index.php?module=Meetings&action=ajaxHandleImportPT&dotb_body_only=true&type=ajaxDownloadTemplate&id={$MEETING_ID}')" >{$MOD.LBL_EXPORT_LIST}</a>
                    {/if}
                    {if $isEdit}<button  style="float:left; margin-left: 20px;" type="button" onclick="showUploadPTResult(this)" id="btnImportPTResult" class="button" template_link="window.open('index.php?module=Meetings&action=ajaxHandleImportPT&dotb_body_only=true&type=ajaxDownloadTemplate&id={$MEETING_ID}')">{$MOD.LBL_IMPORT_PT_RESULT}</button>{/if}
                    </td>
            </tr>
            <tr>
                <th style="text-align: left; padding-left: 5px !important"><input  type="checkbox" class="checkall_custom_checkbox" module_name="J_PTResult" onclick="handleCheckBox($(this));"/></th>
                <th style="text-align: center;">{$MOD.LBL_NO}</th>
                <th style="text-align: center;">{$MOD.LBL_NAME}</th>
                <th style="text-align: center;">{$MOD.LBL_EMAIL}</th>
                <th style="text-align: center;">{$MOD.LBL_MOBILE}</th>
                <th style="text-align: center;">{$MOD.LBL_PARENT}</th>
                <th style="text-align: center;">{$MOD.LBL_ASSIGNED_TO_NAME}</th>
                <th style="text-align: center;">{$MOD.LBL_SOURCE}</th>
                <th style="text-align: center;">{$MOD.LBL_ATTENDED}</th>
                <th style="text-align: center;">{$MOD.LBL_LISTENING}</th>
                <th style="text-align: center;">{$MOD.LBL_SPEAKING}</th>
                <th style="text-align: center;">{$MOD.LBL_READING}</th>
                <th style="text-align: center;">{$MOD.LBL_WRITING}</th>
                <th style="text-align: center;">{$MOD.LBL_SCORE}</th>
                <th style="text-align: center;">{$MOD.LBL_RESULT}</th>
                <th style="text-align: center;">{$MOD.LBL_EC_NOTE}</th>
                <th style="text-align: center;">{$MOD.LBL_TEACHER_COMMENT}</th>
                <th style="text-align: center;">&nbsp;</th>
            </tr>
        </thead>
        <tbody id="tbodyPT">
            {$html_tpl}
            {$html}
        </tbody>
        <tfoot id="noti">
            <tr></tr>
        </tfoot>
    </table>
    </div>

</form>
<div id ='import_pt_result' style="display: none">
    <form action="index.php?module=Meetings&action=ajaxHandleImportPT&dotb_body_only=true&type=ajaxImportPT"
          method="post" class="submitFileMark" enctype="multipart/form-data">
        <a href="javascript: void(0);" class="import_template_link">{$MOD.LBL_DOWLOAD_IMPORT_FILE_TEMPLATE}</a>
        <br><br>
        {$MOD.LBL_UPLOAD_FILE}: <span class="required">*</span><input type="file" name="fileToUpload" class="fileToUpload">
        <br><br>
        <button onclick="uploadFilePT(this)" value="" meeting_id ="{$MEETING_ID}"class="button btn btn-primary submit_file_mark" type="button"><b>{$MOD.LBL_IMPORT}</b></button>
    </form>
</div>
{dotb_getscript file="custom/modules/Meetings/js/ptdemo.js"}
<?php die(); ?>