<?php
switch ($_POST['type']) {
    case 'send_email':
        echo send_email($_POST['list_id']);
        break;
    case 'sendRequestUnlock':
        echo sendRequestUnlock($_POST['id'], $_POST['status']);
        break;
    default:
        break;
}die;
///////////////////////////////////////////////

function send_email($list_id){
    $PTids = explode(",",$list_id);
    $q1="SELECT DISTINCT
    IFNULL(rs.id, '') id,
    IFNULL(rs.parent, '') parent_type,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.id,'') ELSE IFNULL(l1.id,'') END) student_id
    FROM j_ptresult rs
    LEFT JOIN leads l1 ON rs.student_id = l1.id AND l1.deleted = 0 AND rs.parent = 'Leads'
    LEFT JOIN contacts l2 ON rs.student_id = l2.id AND l2.deleted = 0 AND rs.parent = 'Contacts'
    INNER JOIN meetings l3 ON rs.meeting_id = l3.id AND l3.deleted = 0
    WHERE rs.id IN ('".implode("','", $PTids)."')  AND rs.deleted = 0
    ORDER  BY pt_order ASC";
    $rows = $GLOBALS['db']->fetchArray($q1);
    if(count($rows) > 0){
        $eUi = new EmailUI();
        $composeData = array();
        foreach($rows as $k => $r)
            $composeData[] = array("parent_id" => $r['student_id'], "parent_type"=>$r['parent_type']);
        $j_quickComposeOptions = $eUi->generateCompose($composeData, http_build_query($composeData), 'bcc');
        return json_encode(array(
            "success" => "1",
            'onEmail' => $j_quickComposeOptions,
        ));
    }else{
        return json_encode(array(
            "success" => "0",
            "error" => "An Error Occurred, Please, Try again!",
        ));
    }
}

//Request to Unlock Session
function sendRequestUnlock($id, $status){
    $success = 0;
    if(!empty($id)){
        $met = BeanFactory::getBean('Meetings', $id);
        switch($status) {
            case 'Requesting':
                if($met->lock_status == 'Locked'){
                    $met->lock_status = $status;
                    $met->save();
                    $msg     = translate('LBL_SUBMIT_STATUS_SUCCESS','Meetings');
                    $success = 1;
                }
                break;
            case 'Locked':
            case 'Unlocked':
                if($met->lock_status == 'Requesting'
                && ACLController::checkField('Meetings', 'lock_status', 'edit')){
                    $met->lock_status = $status;
                    $met->save();
                    $msg     = translate('LBL_'.strtoupper($status).'_DES','Meetings');
                    $success = 1;
                }
                break;
        }
    }
    if($success == 0) $msg = translate('LBL_ERROR','Meetings');
    return json_encode(array(
        'success' => $success,
        'mgs' => $msg,
    ));
}
?>
