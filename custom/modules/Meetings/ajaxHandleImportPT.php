<?php
require_once("custom/include/_helper/junior_gradebook_utils.php");
require_once("custom/modules/J_Gradebook/GradebookFunctions.php");
require_once("custom/modules/Meetings/subPanelPTResult.php");
switch ($_REQUEST['type']) {
    case 'ajaxDownloadTemplate':
        $result = ajaxDownloadTemplate();
        break;
    case 'ajaxImportPT':
        $result = ajaxImportPT();
        break;
}
echo $result;
die;

function ajaxDownloadTemplate() {
    global $timedate;
    require_once("custom/include/PHPExcel/Classes/PHPExcel.php");
    $bean = BeanFactory::getBean('Meetings',$_GET['id']);

    $pt_list = $GLOBALS['db']->fetchArray(queryListResult($bean->id));

    $fileName="custom/include/TemplateExcel/Import/Template_Import_PT_Result.xlsx";
    $objPHPExcel = PHPExcel_IOFactory::load($fileName);
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', $bean->name)
    ->setCellValue('A3', $timedate->to_display_date_time($bean->date_start)." - ". $timedate->to_display_date_time($bean->date_end));

    $i =5;
    foreach ($pt_list as $key => $pt){
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, $i-4)
        ->setCellValue('B'.$i, $pt['last_name'])
        ->setCellValue('C'.$i, $pt['first_name'])
        ->setCellValue('D'.$i, $timedate->to_display_date($pt['birthdate'],true))
        ->setCellValue('E'.$i, $pt['phone_mobile'])
        ->setCellValue('F'.$i, $pt['email'])
        ->setCellValue('G'.$i,$GLOBALS['app_list_strings']['check_attended_list'][$pt['attended']] )
        ->setCellValue('H'.$i, $pt['listening'])
        ->setCellValue('I'.$i, $pt['speaking'])
        ->setCellValue('J'.$i, $pt['reading'])
        ->setCellValue('K'.$i, $pt['writing'])
        ->setCellValue('L'.$i, $pt['score'])
        ->setCellValue('M'.$i, $pt['result'])
        ->setCellValue('N'.$i, $pt['ec_note'])
        ->setCellValue('O'.$i, $pt['teacher_comment']);
        //
        $i++;
    }
    PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007')->save('Template_Import_PT_Result.xlsx');
    if (!headers_sent()) {
        foreach (headers_list() as $header)
            header_remove($header);
    }
    ob_clean();
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header('Content-Disposition: attachment;filename="Template_Import_PT_Result.xlsx"');
    header("Content-Transfer-Encoding: binary");
    readfile('Template_Import_PT_Result.xlsx');
    unlink('Template_Import_PT_Result.xlsx');
    die();
}

function ajaxImportPT(){
    require_once("custom/include/PHPExcel/Classes/PHPExcel.php");
    $info = pathinfo($_FILES['file']['name']);
    $ext = $info['extension']; // get the extension of the file

    if ($ext != "xlsx" && $ext != "xls") {
        return json_encode(
            array(
                "success" => "0",
                "errorLabel" => "LBL_PLEASE_UPLOAD_EXCEL_FILE",
            )
        );
    }
    $bean = BeanFactory::getBean('Meetings',$_GET['id']);
    $pt_list = $GLOBALS['db']->fetchArray(queryListResult($bean->id));

    $fdir = 'pt_result';
    if (!file_exists("upload/$fdir")) mkdir("upload/$fdir", 0777, true);

    $newname = "pt_result." . $ext;
    $filename = "upload/$fdir/" . $newname;
    unlink($filename);
    move_uploaded_file($_FILES['file']['tmp_name'], $filename);

    $inputFileType = PHPExcel_IOFactory::identify($filename);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objReader->setReadDataOnly(true);
    $objPHPExcel = $objReader->load("$filename");
    $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
    $highestRow = $objWorksheet->getHighestRow();
    $result = array();

    $row_num = 0;
    $coll = [];
    for ($i = 1; $i <= 10; ++$i) {
        for ($j = 0; $j <= 15; ++$j) {
            $value = mb_strtolower($objWorksheet->getCellByColumnAndRow($j, $i)->getValue());
            if($value == 'mobile') $row_num =  $i+1;    //Set Row Number

            $coll['last_name'] = (($value == 'last name') ? $j : $coll['last_name']);
            $coll['first_name'] = (($value == 'first name') ? $j : $coll['first_name']);
            $coll['full_name'] = (($value == 'name' || $value == 'full name') ? $j : $coll['full_name']);
            $coll['phone_mobile'] = (($value == 'mobile') ? $j : $coll['phone_mobile']);
            $coll['attended'] = (($value == 'attended') ? $j : $coll['attended']);
            $coll['listening'] = (($value == 'listening') ? $j : $coll['listening']);
            $coll['speaking'] = (($value == 'speaking') ? $j : $coll['speaking']);
            $coll['reading'] = (($value == 'reading') ? $j : $coll['reading']);
            $coll['writing'] = (($value == 'writing') ? $j : $coll['writing']);
            $coll['score'] = (($value == 'score') ? $j : $coll['score']);
            $coll['result'] = (($value == 'result') ? $j : $coll['result']);
            $coll['ec_note'] = (($value == "sale's note") ? $j : $coll['ec_note']);
            $coll['teacher_comment'] = (($value == "teacher's comment") ? $j : $coll['teacher_comment']);

        }
    }
    unlink($filename);

    $countInvalid = 0;
    //Check valid collum
    if(empty($coll['last_name']) && empty($coll['first_name']) && empty($coll['full_name'])) $countInvalid++;
    if(empty($coll['phone_mobile'])) $countInvalid++;
    if($countInvalid > 0)
        return json_encode(array(
            "success" => 0,
            "errorLabel" => 'LBL_PLEASE_UPLOAD_RIGHT_TEMPLATE'
            )
        );

    for ($row = $row_num; $row <= $highestRow; ++$row) {
        foreach ($pt_list as $pt) {
            if ( ($pt['first_name'] == $objWorksheet->getCellByColumnAndRow($coll['first_name'], $row)->getValue()
            && $pt['last_name'] == $objWorksheet->getCellByColumnAndRow($coll['last_name'], $row)->getValue())
            || ($pt['full_name'] == $objWorksheet->getCellByColumnAndRow($coll['full_name'], $row)->getValue())
            || ($pt['phone_mobile']  == $objWorksheet->getCellByColumnAndRow($coll['phone_mobile'], $row)->getValue())) {
                $result[$pt['id']]['id']        = $pt['id'];
                $result[$pt['id']]['attended']  = $objWorksheet->getCellByColumnAndRow($coll['attended'], $row)->getValue();
                $result[$pt['id']]['listening'] = $objWorksheet->getCellByColumnAndRow($coll['listening'], $row)->getValue();
                $result[$pt['id']]['speaking']  = $objWorksheet->getCellByColumnAndRow($coll['speaking'], $row)->getValue();
                $result[$pt['id']]['reading']   = $objWorksheet->getCellByColumnAndRow($coll['reading'], $row)->getValue();
                $result[$pt['id']]['writing']   = $objWorksheet->getCellByColumnAndRow($coll['writing'], $row)->getValue();
                $result[$pt['id']]['score']     = $objWorksheet->getCellByColumnAndRow($coll['score'], $row)->getValue();
                $result[$pt['id']]['result']    = $objWorksheet->getCellByColumnAndRow($coll['result'], $row)->getValue();
                $result[$pt['id']]['ec_note']   = $objWorksheet->getCellByColumnAndRow($coll['ec_note'], $row)->getValue();
                $result[$pt['id']]['teacher_comment']   = $objWorksheet->getCellByColumnAndRow($coll['teacher_comment'], $row)->getValue();
                break;
            }
        }
    }

    return json_encode(array(
        "success" => 1,
        "data" => $result
        )
    );
}
?>