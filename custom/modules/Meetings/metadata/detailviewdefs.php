<?php

$viewdefs ['Meetings'] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    'EDIT',
                    'DELETE',
                ),
                'hidden' => array(
                    '<input type="hidden" name="isSaveAndNew">',
                    '<input type="hidden" name="status">',
                    '<input type="hidden" name="isSaveFromDetailView">',
                    '<input type="hidden" name="isSave">',
                ),
                'headerTpl' => 'modules/Meetings/tpls/detailHeader.tpl',
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
             'javascript' => '{dotb_getscript file="custom/modules/Meetings/js/trigger_dl.js"}',

            'useTabs' => false,
        ),
        'panels' =>
        array (
            'lbl_meeting_information' =>
            array (
                array (
                    array (
                        'name' => 'name',
                        'label' => 'LBL_SUBJECT',
                    ),
                    'meeting_type',
                ),
                array (
                    array (
                        'name' => 'date_start',
                        'label' => 'LBL_DATE_TIME',
                    ),
                    'status'

                ),
                array (
                    array (
                        'name' => 'date_end',
                    ),
                    'teacher_name'
                ),
                array (
                    array (
                        'name' => 'duration_hours',
                        'customCode' => '{$fields.duration_hours.value}{$MOD.LBL_HOURS_ABBREV} {$fields.duration_minutes.value}{$MOD.LBL_MINSS_ABBREV} ',
                        'label' => 'LBL_DURATION',
                    ),
                    'teacher_cover_name'
                ),


                array (

                    '',
                    'sub_teacher_name'
                ),

                array (

                    'description',
                    'room_name'
                ),
            ),
            'LBL_PANEL_ASSIGNMENT' =>
            array (
                array (
                    array (
                        'name' => 'assigned_user_name',
                        'label' => 'LBL_ASSIGNED_TO',
                    ),
                    array (
                        'name' => 'date_modified',
                        'label' => 'LBL_DATE_MODIFIED',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                    ),

                ),
                array (
                    'team_name',
                    array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                    ),
                ),
            ),
        ),
    ),
);
?>
