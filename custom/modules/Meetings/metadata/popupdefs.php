<?php
$popupMeta = array (
    'moduleMain' => 'Meetings',
    'varName' => 'Meetings',
    'orderBy' => 'meetings.name',
    'whereClauses' => array (
  'name' => 'meetings.name',
  'parent_name' => 'meetings.parent_name',
  'current_user_only' => 'meetings.current_user_only',
  'status' => 'meetings.status',
  'assigned_user_id' => 'meetings.assigned_user_id',
  'team_name' => 'meetings.team_name',
),
    'searchInputs' => array (
  1 => 'name',
  3 => 'status',
  4 => 'parent_name',
  5 => 'current_user_only',
  6 => 'assigned_user_id',
  7 => 'team_name',
),
    'searchdefs' => array (
  'name' =>
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'parent_name' =>
  array (
    'type' => 'parent',
    'label' => 'LBL_LIST_RELATED_TO',
    'width' => '10',
    'name' => 'parent_name',
  ),
  'current_user_only' =>
  array (
    'name' => 'current_user_only',
    'label' => 'LBL_CURRENT_USER_FILTER',
    'type' => 'bool',
    'width' => '10',
  ),
  'status' =>
  array (
    'name' => 'status',
    'width' => '10',
  ),
  'assigned_user_id' =>
  array (
    'name' => 'assigned_user_id',
    'type' => 'enum',
    'label' => 'LBL_ASSIGNED_TO',
    'function' =>
    array (
      'name' => 'get_user_array',
      'params' =>
      array (
        0 => false,
      ),
    ),
    'width' => '10',
  ),
  'team_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'studio' =>
    array (
      'portallistview' => false,
      'portalrecordview' => false,
    ),
    'label' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => 10,
    'name' => 'team_name',
  ),
),
    'listviewdefs' => array (
  'NAME' =>
  array (
    'width' => 10,
    'label' => 'LBL_LIST_SUBJECT',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'DESCRIPTION' =>
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'STATUS' =>
  array (
    'width' => 10,
    'label' => 'LBL_LIST_STATUS',
    'link' => false,
    'default' => true,
    'name' => 'status',
  ),
  'DATE_START' =>
  array (
    'width' => 10,
    'label' => 'LBL_LIST_DATE',
    'link' => false,
    'default' => true,
    'related_fields' =>
    array (
      0 => 'time_start',
    ),
    'name' => 'date_start',
  ),
  'NUMBER_OF_STUDENT' =>
  array (
    'type' => 'varchar',
    'label' => 'LBL_NUMBER_OF_STUDENT',
    'width' => 10,
    'default' => true,
  ),
  'ATTENDED_LABEL' =>
  array (
    'type' => 'bool',
    'label' => 'LBL_ATTENDED',
    'width' => 10,
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' =>
  array (
    'width' => 10,
    'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'TEAM_NAME' =>
  array (
    'width' => 10,
    'label' => 'LBL_LIST_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
),
);
