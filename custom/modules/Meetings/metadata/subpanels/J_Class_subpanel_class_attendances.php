<?php
// created: 2019-03-30 17:44:19
$subpanel_layout['where'] = " (meetings.session_status <> 'Cancelled') ";
$subpanel_layout['custom_order_by'] = 'CASE
WHEN Date_format(convert_tz(meetings.date_end,"+0:00","+7:00"),"%Y-%m-%d")< CURDATE() THEN 0
WHEN Date_format(convert_tz(meetings.date_end,"+0:00","+7:00"),"%Y-%m-%d") >= CURDATE() THEN 1
ELSE 2
END DESC, meetings.date_end ASC';

$subpanel_layout['list_fields'] = array (
    'lesson_number' =>
    array (
        'type' => 'int',
        'default' => true,
        'vname' => 'LBL_LESSON_NUMBER',
        'width' => '2%',
        'sortable' => false,
    ),
    'week_date' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_WEEK_DATE',
        'width' => '6%',
        'default' => true,
        'sortable' => false,
    ),
    'time_start_end' =>
    array (
        'name' => 'time_start_end',
        'vname' => 'LBL_DATE_TIME',
        'width' => '9%',
        'default' => true,
        'sort_by' => 'date_start',
    ),
    'duration_text' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_DURATION',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'session_status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_STATUS',
        'width' => '9%',
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'teacher_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_TEACHER_NAME',
        'id' => 'TEACHER_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'teacher_id',
        'sortable' => false,
    ),
    'total_student' =>
    array (
        'type' => 'int',
        'vname' => 'LBL_TOTAL_STUDENT',
        'width' => '10%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'total_attended' =>
    array (
        'type' => 'int',
        'vname' => 'LBL_TOTAL_ATTENDED',
        'width' => '10%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'total_absent' =>
    array (
        'type' => 'int',
        'vname' => 'LBL_TOTAL_ABSENT',
        'width' => '10%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'attended_label' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'vname' => 'LBL_ATTENDANCE_STATUS',
        'width' => '10%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
//    'sent_app_label' =>
//    array (
//        'type' => 'varchar',
//        'studio' => 'visible',
//        'vname' => 'LBL_SENDING_APPS_STATUS',
//        'width' => '10%',
//        'default' => true,
//        'align' => 'center',
//        'sortable' => false,
//        'css_class' => 'align-center',
//    ),
    'lock_status' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'vname' => '',
        'width' => '7%',
        'default' => true,
        'align' => 'center',
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'subpanel_btn_edit' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'width' => '5%',
        'default' => true,
        'sortable' => false,
        'align' => 'right',
    ),
    'ju_class_id' =>
    array (
        'usage' => 'query_only',
    ),
    'recurring_source' =>
    array (
        'usage' => 'query_only',
    ),
    'meeting_type' =>
    array (
        'usage' => 'query_only',
    ),
    'date_start' =>
    array (
        'usage' => 'query_only',
    ),
    'date_end' =>
    array (
        'usage' => 'query_only',
    ),
    'teacher_cover_id' =>
    array (
        'usage' => 'query_only',
    ),
    'sub_teacher_id' =>
    array (
        'usage' => 'query_only',
    ),
    'teaching_type' =>
    array (
        'usage' => 'query_only',
    ),
    'room_id' =>
    array (
        'usage' => 'query_only',
    ),
    'makeup_session_id' =>
    array (
        'usage' => 'query_only',
    ),
    'name' =>
    array (
        'usage' => 'query_only',
    ),
    'duration_minutes' =>
    array (
        'usage' => 'query_only',
    ),
    'duration_hours' =>
    array (
        'usage' => 'query_only',
    ),
    'attendance_status' =>
    array (
        'usage' => 'query_only',
    ),
    'total_sent' =>
    array (
        'usage' => 'query_only',
    ),
    'date' =>
    array (
        'usage' => 'query_only',
    ),
);
