<?php
// created: 2019-03-30 17:44:19
$subpanel_layout['custom_order_by'] = 'CASE
WHEN Date_format(convert_tz(meetings.date_end,"+0:00","+7:00"),"%Y-%m-%d")< CURDATE() THEN 0
WHEN Date_format(convert_tz(meetings.date_end,"+0:00","+7:00"),"%Y-%m-%d") >= CURDATE() THEN 1
ELSE 2
END DESC, meetings.date_end ASC';

$subpanel_layout['list_fields'] = array (
     'lesson_number' =>
    array (
        'type' => 'int',
        'default' => true,
        'vname' => 'LBL_LESSON_NUMBER',
        'width' => '2%',
        'sortable' => false,
    ),
    'week_date' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_WEEK_DATE',
        'width' => '6%',
        'default' => true,
        'sortable' => false,
    ),
    'time_start_end' =>
    array (
        'name' => 'time_start_end',
        'vname' => 'LBL_DATE_TIME',
        'width' => '9%',
        'default' => true,
        'sort_by' => 'date_start',
    ),
    'duration_text' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_DURATION',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
    ),
    'session_status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_STATUS',
        'width' => '9%',
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'teacher_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_TEACHER_NAME',
        'id' => 'TEACHER_ID',
        'width' => '20%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'teacher_id',
        'sortable' => false,
    ),
    'late_time' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_LATE_TIME',
        'width' => '20%',
        'sortable' => false,
    ),
    'planned_absence' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_PLANNED_ABSENCE',
        'width' => '20%',
        'sortable' => false,
    ),

    'ju_class_id' =>
    array (
        'usage' => 'query_only',
    ),
    'recurring_source' =>
    array (
        'usage' => 'query_only',
    ),
    'meeting_type' =>
    array (
        'usage' => 'query_only',
    ),
    'date_start' =>
    array (
        'usage' => 'query_only',
    ),
    'date_end' =>
    array (
        'usage' => 'query_only',
    ),
    'teacher_cover_id' =>
    array (
        'usage' => 'query_only',
    ),
    'sub_teacher_id' =>
    array (
        'usage' => 'query_only',
    ),
    'teaching_type' =>
    array (
        'usage' => 'query_only',
    ),
    'room_id' =>
    array (
        'usage' => 'query_only',
    ),
    'makeup_session_id' =>
    array (
        'usage' => 'query_only',
    ),
    'name' =>
    array (
        'usage' => 'query_only',
    ),
    'duration_minutes' =>
    array (
        'usage' => 'query_only',
    ),
    'duration_hours' =>
    array (
        'usage' => 'query_only',
    ),
);
