<?php
// created: 2020-12-04 16:54:40
$subpanel_layout['list_fields'] = array (
    'lesson_number' =>
    array (
        'type' => 'int',
        'default' => true,
        'vname' => 'LBL_LESSON_NUMBER',
        'width' => 10,
    ),
    'till_hour' =>
    array (
        'type' => 'decimal',
        'vname' => 'LBL_TILL_HOUR',
        'width' => 10,
        'default' => true,
    ),
//    'name' =>
//    array (
//        'name' => 'name',
//        'vname' => 'LBL_LIST_SUBJECT',
//        'widget_class' => 'SubPanelDetailViewLink',
//        'width' => 10,
//        'default' => true,
//    ),
    'ju_class_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_JU_CLASS_NAME',
        'id' => 'JU_CLASS_ID',
        'width' => 12,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'J_Class',
        'target_record_key' => 'ju_class_id',
    ),
    'session_status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_STATUS',
        'width' => 10,
    ),
    'week_date' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_WEEK_DATE',
        'width' => 10,
        'default' => true,
    ),
    'time_start_end' =>
    array (
        'width' => 10,
        'default' => true,
        'vname' => 'LBL_TIME_DATE',
    ),
    'teaching_hour' =>
    array (
        'type' => 'decimal',
        'vname' => 'LBL_TEACHING_HOUR',
        'width' => 10,
        'default' => true,
    ),
    'teacher_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_TEACHER_NAME',
        'id' => 'TEACHER_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'teacher_id',
    ),
    'sub_teacher_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_SUB_TEACHER_NAME',
        'id' => 'SUB_TEACHER_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'sub_teacher_id',
    ),
    'teacher_cover_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_TEACHER_COVER_NAME',
        'id' => 'TEACHER_COVER_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'teacher_cover_id',
    ),
    'description' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_DESCRIPTION',
        'sortable' => false,
        'width' => 10,
        'default' => true,
    ),
    'recurring_source' =>
    array (
        'usage' => 'query_only',
    ),
    'ju_class_id' =>
    array (
        'usage' => 'query_only',
    ),
);