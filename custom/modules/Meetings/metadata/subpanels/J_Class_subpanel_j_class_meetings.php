<?php
// created: 2019-03-30 17:44:19
$subpanel_layout['custom_order_by'] = 'CASE
WHEN Date_format(convert_tz(meetings.date_end,"+0:00","+7:00"),"%Y-%m-%d")< CURDATE() THEN 0
WHEN Date_format(convert_tz(meetings.date_end,"+0:00","+7:00"),"%Y-%m-%d") >= CURDATE() THEN 1
ELSE 2
END DESC, meetings.date_end ASC';
$subpanel_layout['list_fields'] = array (
    'lesson_number' =>
    array (
        'type' => 'int',
        'default' => true,
        'vname' => 'LBL_LESSON_NUMBER',
        'width' => '2%',
        'sortable' => false,
    ),
    'till_hour' =>
    array (
        'type' => 'decimal',
        'vname' => 'LBL_TILL_HOUR',
        'width' => '2%',
        'default' => true,
        'sortable' => false,
    ),
    'week_date' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_WEEK_DATE',
        'width' => '6%',
        'default' => true,
        'sortable' => false,
    ),
    'time_start_end' =>
    array (
        'name' => 'time_start_end',
        'vname' => 'LBL_DATE_TIME',
        'width' => '9%',
        'default' => true,
        'sort_by' => 'date_start',
    ),
    'duration_text' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_DURATION',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'session_status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_STATUS',
        'width' => '9%',
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'teacher_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_TEACHER_NAME',
        'id' => 'TEACHER_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'teacher_id',
        'sortable' => false,
    ),
    'teacher_cover_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_TEACHER_COVER_NAME',
        'id' => 'TEACHER_COVER_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'teacher_cover_id',
        'sortable' => false,
    ),
    'sub_teacher_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_SUB_TEACHER_NAME',
        'id' => 'SUB_TEACHER_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'sub_teacher_id',
        'sortable' => false,
    ),
    'room_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_ROOM_NAME',
        'id' => 'ROOM_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Rooms',
        'target_record_key' => 'room_id',
        'sortable' => false,
    ),
    'description' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_DESCRIPTION',
        'sortable' => false,
        'width' => '20%',
        'default' => true,
    ),
    'subpanel_button' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'vname' => 'LBL_SUBPANEL_BUTTON',
        'width' => '5%',
        'default' => true,
        'align' => 'right',
        'sortable' => false,
    ),
    'recurring_source' =>
    array (
        'usage' => 'query_only',
    ),
    'meeting_type' =>
    array (
        'usage' => 'query_only',
    ),
    'date_start' =>
    array (
        'usage' => 'query_only',
    ),
    'date_end' =>
    array (
        'usage' => 'query_only',
    ),
    'teacher_cover_id' =>
    array (
        'usage' => 'query_only',
    ),
    'sub_teacher_id' =>
    array (
        'usage' => 'query_only',
    ),
    'room_id' =>
    array (
        'usage' => 'query_only',
    ),
    'makeup_session_id' =>
    array (
        'usage' => 'query_only',
    ),
    'name' =>
    array (
        'usage' => 'query_only',
    ),
    'teaching_type' =>
    array (
        'usage' => 'query_only',
    ),
    'cancel_by' =>
    array (
        'usage' => 'query_only',
    ),
    'teaching_hour' =>
    array (
        'usage' => 'query_only',
    ),
    'delivery_hour' =>
    array (
        'usage' => 'query_only',
    ),
    'duration_minutes' =>
    array (
        'usage' => 'query_only',
    ),
    'duration_hours' =>
    array (
        'usage' => 'query_only',
    ),
);
