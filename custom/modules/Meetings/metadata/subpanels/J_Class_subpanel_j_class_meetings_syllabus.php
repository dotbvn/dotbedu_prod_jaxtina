<?php
// created: 2019-04-20 22:24:27
$subpanel_layout['where'] = " (meetings.session_status <> 'Cancelled') ";
$subpanel_layout['list_fields'] = array (
    'lesson_number' =>
    array (
        'type' => 'int',
        'default' => true,
        'vname' => 'LBL_LESSON_NUMBER',
        'width' => '2%',
        'sortable' => false,
    ),
    'week_date' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_WEEK_DATE',
        'width' => '6%',
        'default' => true,
        'sortable' => false,
    ),
    'time_start_end' =>
    array (
        'name' => 'time_start_end',
        'vname' => 'LBL_DATE_TIME',
        'width' => '9%',
        'default' => true,
        'sort_by' => 'date_start',
    ),
    'duration_text' =>
    array (
        'type' => 'varchar',
        'vname' => 'LBL_DURATION',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'syllabus_type' =>
    array (
        'type' => 'enum',
        'vname' => 'LBL_SYL_TYPE',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
    ),
    'syllabus_topic' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_SYL_TOPIC',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
    ),
    'syllabus_activities' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_SYL_SYLLABUS',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
    ),
    'syllabus_objective' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_SYL_NOTE_FOR_TEACHER',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
    ),
    'syllabus_homework' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_SYL_HOMEWORK',
        'width' => '9%',
        'default' => true,
        'sortable' => false,
    ),
    'teacher_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_TEACHER_NAME',
        'id' => 'TEACHER_ID',
        'width' => '9%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Teachers',
        'target_record_key' => 'teacher_id',
        'sortable' => false,
    ),
    'room_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_ROOM_NAME',
        'id' => 'ROOM_ID',
        'width' => '9%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'C_Rooms',
        'target_record_key' => 'room_id',
        'sortable' => false,
    ),
    'type' =>
    array (
        'type' => 'enum',
        'vname' => 'LBL_TYPE',
        'width' => '5%',
        'default' => true,
        'sortable' => false,
    ),
    'join_url' =>
    array (
        'type' => 'url',
        'vname' => 'LBL_URL',
        'sortable' => false,
        'width' => '9%',
        'default' => true,
    ),
    'subpanel_btn3' =>
    array (
        'type' => 'varchar',
        'width' => '5%',
        'default' => true,
        'align' => 'right',
        'sortable' => false,
    ),
    'syllabus_id' =>
    array (
        'usage' => 'query_only',
    ),
    'recurring_source' =>
    array (
        'usage' => 'query_only',
    ),
    'meeting_type' =>
    array (
        'usage' => 'query_only',
    ),
    'date_start' =>
    array (
        'usage' => 'query_only',
    ),
    'topic_custom' =>
    array (
        'usage' => 'query_only',
    ),
    'syllabus_custom' =>
    array (
        'usage' => 'query_only',
    ),
    'objective_custom' =>
    array (
        'usage' => 'query_only',
    ),
    'date_end' =>
    array (
        'usage' => 'query_only',
    ),
    'makeup_session_id' =>
    array (
        'usage' => 'query_only',
    ),
    'displayed_url' =>
    array (
        'usage' => 'query_only',
    ),

    'creator' =>
    array (
        'usage' => 'query_only',
    ),

    'learning_type' =>
    array (
        'usage' => 'query_only',
    ),
    'external_id' =>
    array (
        'usage' => 'query_only',
    ),
        'duration_minutes' =>
    array (
        'usage' => 'query_only',
    ),
    'duration_hours' =>
    array (
        'usage' => 'query_only',
    ),
);
