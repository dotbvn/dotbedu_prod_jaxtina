<?php

// TODO REMOVE THIS FILE
$viewdefs ['Meetings'] =
array (
  'EditView' =>
  array (
    'templateMeta' =>
    array (
      'maxColumns' => '2',
      'form' =>
      array (
        'hidden' =>
        array (
          0 => '<input type="hidden" name="isSaveAndNew" value="false">',
          0 => '<input type="hidden" name="meeting_type" value="{$MEETING_TYPE}">',
        ),
        'buttons' =>
        array (
          0 => 'SAVE',
          1 => 'CANCEL',
        ),
        'headerTpl' => 'modules/Meetings/tpls/header.tpl',

          'buttons_footer' =>
        array (
            0 => 'SAVE',
          1 => 'CANCEL',
        ),
      ),
      'widths' =>
      array (
        0 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '<script type="text/javascript">{$JSON_CONFIG_JAVASCRIPT}</script>
<script>toggle_portal_flag();function toggle_portal_flag()  {ldelim} {$TOGGLE_JS} {rdelim}
function disableSaveBtn() {ldelim}document.getElementById(\'SAVE_HEADER\').disabled=true; document.getElementById(\'SAVE_FOOTER\').disabled=true; document.getElementById(\'save_and_send_invites_header\').disabled=true; document.getElementById(\'save_and_send_invites_footer\').disabled=true;{rdelim}
function formSubmitCheck(){ldelim}if(check_form(\'EditView\') && CAL.checkRecurrenceForm()){ldelim}disableSaveBtn();document.EditView.submit();{rdelim}{rdelim}</script>
{dotb_getscript file="custom/modules/Meetings/js/editview.js"}',
      'useTabs' => false,
    ),
    'panels' =>
    array (
      'lbl_meeting_information' =>
      array (
        array (
            array(
                'name' => 'name',
                'customCode' => '<input type="text" name="name" id="name" placeholder="Auto-Generate if blank" size="30" maxlength="50" value="{$fields.name.value}" />',
                'displayParams' =>
                    array (
                        'required' => false,
                    ),
            ),
            'status'
      ),

      array (
          array (
            'name' => 'date_start',
            'type' => 'datetimecombo',
            'displayParams' =>
            array (
              'required' => true,
              'updateCallback' => 'DotbWidgetScheduler.update_time();',
            ),
          ),
           'teacher_name'
        ),
        array(
            array(
                'name' => 'duration',
                'customCode' => '
                        @@FIELD@@
                        <input id="duration_hours" name="duration_hours" type="hidden" value="{$fields.duration_hours.value}">
                        <input id="duration_minutes" name="duration_minutes" type="hidden" value="{$fields.duration_minutes.value}">
                        {dotb_getscript file="modules/Meetings/duration_dependency.js"}
                        <script type="text/javascript">
                        var date_time_format = "{$CALENDAR_FORMAT}";
                        {literal}
                             if ($("#duration").val() == "3600"){
                                $("#duration_hours").val("1");
                                $("#duration_minutes").val("0");
                             }

                             $("#duration").change(function(){
                                var s= $(this).val();
                                //Get whole hours
                                var h = Math.floor(s/3600);
                                s -= h*3600;
                                //Get remaining minutes
                                var m = Math.floor(s/60);
                                // Update value
                                $("#duration_hours").val(h);
                                $("#duration_minutes").val(m);

                              });
                        {/literal}
                        </script>
                        ',
                'customCodeReadOnly' => '{$fields.duration_hours.value}{$MOD.LBL_HOURS_ABBREV} {$fields.duration_minutes.value}{$MOD.LBL_MINSS_ABBREV} ',
            ),
             'teacher_cover_name'
        ),
		array (
		   '',
           'sub_teacher_name'
        ),
        array (
          array (
            'name' => 'description',
            'comment' => 'Full text of the note',
            'label' => 'LBL_DESCRIPTION',
          ),
          'room_name'
        ),
        array (
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO_NAME',
              'required'=>true
          ),
	      'team_name',
        ),
      ),
    ),
  ),
);
?>
