<?php

require_once('custom/include/_helper/junior_class_utils.php');
//Subpanel Placement Test In Meeting
function getSubResult($params) {
    global $current_user;
    $args = func_get_args();
    $meetingId = $args[0]['meeting_id'];
    $uri = str_replace('index.php','',$_SERVER['SCRIPT_NAME']);
    $smarty = new Dotb_Smarty();
    global $mod_strings, $timedate;
    $meeting = BeanFactory::getBean('Meetings', $meetingId);

    //////////////----------Get Subpanel PT Result-----------/////////////////
    $result_meeting = $GLOBALS['db']->query(queryListResult($meetingId));

    if($meeting->meeting_type == 'Placement Test'){
        $r['arrKOC'] = get_option_koc();
        $html_tpl    = getHtmlAddRow(true,$r);
    }

    elseif($meeting->meeting_type == 'Demo')
        $html_tpl   = getDemoAddRow(true);

    //getbean check case edit
    $count = 0;
    while($r = $GLOBALS['db']->fetchByAssoc($result_meeting)){
        $count++;
        $r['pt_order'] = $count;
        $r['uri'] = $uri;
        if($meeting->meeting_type == 'Placement Test'){
            $r['arrKOC'] = get_option_koc();
            $html   .= getHtmlAddRow(false,$r);
        }elseif($meeting->meeting_type == 'Demo')
            $html   .= getDemoAddRow(false,$r);


    }
    $smarty->assign('html_tpl',$html_tpl);
    $smarty->assign('html',$html);
    $smarty->assign('MOD',$mod_strings);
    $smarty->assign('MEETING_ID',$meetingId);
    $smarty->assign('isImport',ACLController::checkAccess('J_PTResult', 'import', true));
    $smarty->assign('isExport',ACLController::checkAccess('J_PTResult', 'export', true));
    $smarty->assign('isEdit',ACLController::checkAccess('J_PTResult', 'edit', true));
    $smarty->assign('isEditLead',ACLController::checkAccess('Leads', 'edit', true));
    $smarty->assign("uri",$uri);
    if($meeting->meeting_type == 'Placement Test')
        echo $smarty->fetch('custom/modules/Meetings/tpls/tablePTResult.tpl');
    elseif($meeting->meeting_type == 'Demo')
        echo $smarty->fetch('custom/modules/Meetings/tpls/tableDemoResult.tpl');
    //////////////----------End Get Subpanel PT Result-----------/////////////////
}
//Get PT/Row
function getHtmlAddRow($disNone, $r = array()){
    $tpl_addrow  = "<tr id='pt_".$r['id']."' class='pt_template ".($r['attended'] ? 'pt_attended' : '')."' ".($disNone ? 'style="display:none;"' : '').">";
    $tpl_addrow .= '<td align="left">
    <input type="checkbox" class="custom_checkbox" module_name="J_PTResult" onclick="handleCheckBox($(this));" value="'.$r['id'].'"/>
    <input type="hidden" name="id_of_result[]" class="id_of_result" value="'.$r['id'].'"/>';
    $tpl_addrow .= '<td align="left"><span class="priority">'.$r['pt_order'].'</span>
    <input type="hidden" name="pt_order[]" class="pt_order" value="'.$r['pt_order'].'"/></td>';

    $href = $r['uri'].'#'.$r['parent_type'].'/'.$r['student_id'];

    $tpl_addrow.= '<td nowrap="nowrap">
    <a target="_parent" href="'.$href.'" name="pt_name[]" class="pt_name" >'.$r['full_name'].'</a>
    <input type="hidden" name="pt_id[]" class="pt_id" value="'.$r['id'].'" />
    <input type="hidden" name="student_type[]" class="student_type" value="'.$GLOBALS['app_list_strings']['student_type_list'][$r['parent_type']].'" /></td>';

    $eUi = new EmailUI();
    $composeData[] = array("parent_id" => $r['student_id'], "parent_type"=>$r['parent_type']);
    $j_quickComposeOptions = $eUi->generateCompose($composeData, http_build_query($composeData), 'to');
    $onEmail = "onclick='DOTB.quickCompose.init($j_quickComposeOptions);'";
    $tpl_addrow .= '<td nowrap="nowrap" class="email"><a href="#" name="email[]" '.$onEmail.' >'.$r['email'].'</a></td>';

    $tpl_addrow .= '<td nowrap="nowrap" class="phone_mobile">'.$r['phone_mobile'].'</td>';
    $tpl_addrow .= '<td nowrap="nowrap" class="parent_name">'.$r['parent_name'].'</td>';
    $tpl_addrow .= '<td nowrap="nowrap" class="assigned_user_name">'.$r['assigned_to_name'].'</td>';
    $tpl_addrow .= '<td nowrap="nowrap" class="lead_source">'.$r['lead_source'].'</td>';

    ////Result
    if(empty($r['attended'])) $r['attended'] = '';
    $isEdit     = ACLController::checkAccess('J_PTResult', 'edit', true);
    $isDelete     = ACLController::checkAccess('J_PTResult', 'delete', true);
    if($isEdit){
        $select = '<select name="check_attended[]" class="check_attended">'.get_select_options($GLOBALS['app_list_strings']['check_attended_list'],$r['attended']).'</select>';
        $tpl_addrow .= '<td align="center"><span style="display:none;" class="attended_label '.($r['attended'] ? 'yes_attended' : 'no_attended').'">'.$GLOBALS['app_list_strings']['check_attended_list'][$r['attended']].'</span>'.$select.'</td>';
        $tpl_addrow .= '<td align="center" nowrap><input style="text-align:center;" name="listening[]" class="listening" type="text" value="'.$r['listening'].'" size="4" maxlength="10"></td>';
        $tpl_addrow .= '<td align="center" nowrap><input style="text-align:center;" name="speaking[]" class="speaking" type="text" value="'.$r['speaking'].'" size="4" maxlength="10"></td>';
        $tpl_addrow .= '<td align="center" nowrap><input style="text-align:center;" name="reading[]" class="reading" type="text" value="'.$r['reading'].'" size="4" maxlength="10"></td>';
        $tpl_addrow .= '<td align="center" nowrap><input style="text-align:center;" name="writing[]" class="writing" type="text" value="'.$r['writing'].'" size="4" maxlength="10"></td>';
        $tpl_addrow .= '<td align="center" nowrap><input style="text-align:center;" name="score[]" class="score" type="text" value="'.$r['score'].'" size="4" maxlength="10"></td>';

        $select2 = '<select name="result_koc[]" class="result_koc">'.get_select_options($r['arrKOC'],$r['result']).'</select>';
        $tpl_addrow .= '<td align="center">'.$select2.'</td>';

        $tpl_addrow .= '<td align="center"><textarea class=\'ec_note\' name="ec_note[]" rows="3" cols="15">'.htmlspecialchars_decode(html_entity_decode($r['ec_note'])).'</textarea></td>';
        $tpl_addrow .= '<td align="center"><textarea class=\'teacher_comment\' name="teacher_comment[]" rows="3" cols="20">'.htmlspecialchars_decode(html_entity_decode($r['teacher_comment'])).'</textarea></td>';
    }else{
        $tpl_addrow .= '<td align="center"><span class="attended_label '.($r['attended'] ? 'yes_attended' : 'no_attended').'">'.$GLOBALS['app_list_strings']['check_attended_list'][$r['attended']].'</span></td>';
        $tpl_addrow .= '<td align="center" nowrap>'.$r['listening'].'</td>';
        $tpl_addrow .= '<td align="center" nowrap>'.$r['speaking'].'</td>';
        $tpl_addrow .= '<td align="center" nowrap>'.$r['reading'].'</td>';
        $tpl_addrow .= '<td align="center" nowrap>'.$r['writing'].'</td>';
        $tpl_addrow .= '<td align="center" nowrap>'.$r['score'].'</td>';
        $tpl_addrow .= '<td align="center" nowrap>'.$GLOBALS['app_list_strings']['test_result_list'][$r['result_koc']].'</td>';
        $tpl_addrow .= '<td align="center">'.htmlspecialchars_decode(html_entity_decode($r['ec_note'])).'</td>';
        $tpl_addrow .= '<td align="center">'.htmlspecialchars_decode(html_entity_decode($r['teacher_comment'])).'</td>';
    }
    if($isDelete)
        $tpl_addrow .= '<td align="center" nowrap="nowrap"><button style="margin-left: 5px;" type="button" class="btn btn-delete" ><img src="themes/default/images/id-ff-clear.png"></button></td>';
    else
        $tpl_addrow .= '<td></td>';

    $tpl_addrow .= '</tr>';

    return $tpl_addrow;
}
//Get Demo Row
function getDemoAddRow($disNone, $r = array()){
    $tpl_addrow  = "<tr id='pt_".$r['id']."' class='pt_template ".($r['attended'] ? 'pt_attended' : '')."' ".($disNone ? 'style="display:none;"' : '').">";
    $tpl_addrow .= '<td align="center">
    <input type="checkbox" class="custom_checkbox" module_name="J_PTResult" onclick="handleCheckBox($(this));" value="'.$r['id'].'"/>
    <input type="hidden" name="id_of_result[]" class="id_of_result" value="'.$r['id'].'"/></td>';

    $href = $r['uri'].'#'.$r['parent_type'].'/'.$r['student_id'];

    $tpl_addrow .= '<td align="left"><span class="priority">'.$r['pt_order'].'</span>
    <input type="hidden" name="pt_order[]" class="pt_order" value="'.$r['pt_order'].'"/></td>';

    $tpl_addrow.= '<td nowrap="nowrap" style="text-align: center;">
    <a target="_parent" href="'.$href.'" name="pt_name[]" class="pt_name" >'.$r['full_name'].'</a>
    <input type="hidden" name="pt_id[]" class="pt_id" value="'.$r['id'].'" />
    <input type="hidden" name="student_type[]" value="'.$r['parent_type'].'" class="student_type"/></td>';

    //Check phân quyền
    $isEdit     = ACLController::checkAccess('J_PTResult', 'edit', true);
    $isDelete     = ACLController::checkAccess('J_PTResult', 'delete', true);
    $tpl_addrow .= '<td align="center"><label class="gender">'.$r['gender'].'</label></td>';
    $tpl_addrow .= '<td align="center"><label class="birthdate">'.$GLOBALS['timedate']->to_display_date($r['birthdate'],false).'</label></td>';
    $tpl_addrow .= '<td align="center"><label class="parent_name">'.$r['parent_name'].'</label></td>';
    $tpl_addrow .= '<td align="center"><label class="phone_mobile">'.$r['phone_mobile'].'</label></td>';
    $tpl_addrow .= '<td align="center"><label class="email">'.$r['email'].'</label></td>';
    $tpl_addrow .= '<td align="center"><label class="status">'.($r['parent_type'] ? $GLOBALS['app_list_strings']['lead_status_dom'][$r['status']] : $GLOBALS['app_list_strings']['contact_status_list'][$r['status']]).'</label></td>';
    $tpl_addrow .= '<td align="center" nowrap="nowrap" class="lead_source">'.$r['lead_source'].'</td>';
    $tpl_addrow .= '<td align="center"><label class="assigned_user_name">'.$r['assigned_to_name'].'</label></td>';
    if(empty($r['attended'])) $r['attended'] = '';
    if($isEdit){
        $select = '<select name="check_attended[]" class="check_attended">'.get_select_options($GLOBALS['app_list_strings']['check_attended_list'],$r['attended']).'</select>';
        $tpl_addrow .= '<td align="center">'.$select.'</td>';

        $tpl_addrow .= '<td align="center"><textarea class=\'ec_note\' name="ec_note[]" rows="3" cols="15">'.htmlspecialchars_decode(html_entity_decode($r['ec_note'])).'</textarea></td>';
        $tpl_addrow .= '<td align="center"><textarea class=\'teacher_comment\' name="teacher_comment[]" rows="3" cols="20">'.htmlspecialchars_decode(html_entity_decode($r['teacher_comment'])).'</textarea></td>';
    }else{
        $tpl_addrow .= '<td align="center">'.$GLOBALS['app_list_strings']['check_attended_list'][$r['attended']].'</td>';

        $tpl_addrow .= '<td align="center">'.htmlspecialchars_decode(html_entity_decode($r['ec_note'])).'</td>';
        $tpl_addrow .= '<td align="center">'.htmlspecialchars_decode(html_entity_decode($r['teacher_comment'])).'</td>';
    }
    if($isDelete)
        $tpl_addrow .= '<td align="center" nowrap="nowrap"><button style="margin-left: 5px;" type="button" class="btn btn-delete"><img src="themes/default/images/id-ff-clear.png"></button></td>';
    else
        $tpl_addrow .= '<td></td>';

    $tpl_addrow .= '</tr>';

    return $tpl_addrow;

}


//Subpanel Demo In Lead
function getSubDemoLead($params){
    $args = func_get_args();
    $lead_id = $args[0]['lead_id'];
    $sql_demo_lead = "
    SELECT DISTINCT
    IFNULL(j_ptresult.id, '') id,
    IFNULL(j_ptresult.name, '') name,
    IFNULL(meetings.name, '') meeting_name,
    meetings.date_start date_start,
    meetings.date_end ,
    meetings.duration_cal duration_cal,
    IFNULL(j_ptresult.attended, '') attended,
    l3.full_teacher_name full_teacher_name,
    IFNULL(l4.name, '') room_name
    FROM j_ptresult
    INNER JOIN leads ON j_ptresult.student_id = leads.id AND leads.deleted = 0 AND j_ptresult.parent = 'Leads'
    INNER JOIN meetings ON j_ptresult.meeting_id = meetings.id AND meetings.deleted = 0
    LEFT JOIN c_teachers l3 ON meetings.teacher_id = l3.id AND l3.deleted = 0
    LEFT JOIN c_rooms l4 ON meetings.room_id = l4.id AND l4.deleted = 0
    WHERE (leads.id = '$lead_id') AND (j_ptresult.type_result = 'Demo')
    AND j_ptresult.deleted = 0";
    return $sql_demo_lead;
}

//Subpanel Placement Test In Lead
function getSubPTLead($params){

    global $current_user;
    $args = func_get_args();
    $lead_id = $args[0]['lead_id'];
    $sql_pt_lead = "
    SELECT DISTINCT
    IFNULL(j_ptresult.id, '') id,
    IFNULL(j_ptresult.name, '') name,
    IFNULL(meetings.name, '') meeting_name,
    IFNULL(j_ptresult.speaking, '') speaking,
    IFNULL(j_ptresult.listening, '') listening,
    IFNULL(j_ptresult.reading, '') reading,
    IFNULL(j_ptresult.writing, '') writing,
    IFNULL(j_ptresult.result, '') result,
    IFNULL(j_ptresult.score, '') score,
    IFNULL(j_ptresult.ec_note, '') ec_note,
    IFNULL(j_ptresult.teacher_comment, '') teacher_comment,
    IFNULL(j_ptresult.attended, '') attended
    FROM j_ptresult
    INNER JOIN leads ON j_ptresult.student_id = leads.id AND leads.deleted = 0 AND j_ptresult.parent = 'Leads'
    INNER JOIN meetings ON j_ptresult.meeting_id = meetings.id AND meetings.deleted = 0
    WHERE (leads.id = '$lead_id') AND (j_ptresult.type_result = 'Placement Test')
    AND j_ptresult.deleted = 0";

    return $sql_pt_lead;
}

//Subpanel Demo In Contact
function getSubDemoContact($params){
    $args = func_get_args();
    $contact_id = $args[0]['contact_id'];
    $sql_demo_contact = "
    SELECT DISTINCT
    IFNULL(j_ptresult.id, '') id,
    IFNULL(j_ptresult.name, '') name,
    IFNULL(meetings.date_start, '') date_start,
    IFNULL(meetings.date_end, '') date_end,
    IFNULL(meetings.duration_cal, '') duration_cal,
    IFNULL(j_ptresult.attended, '') attended,
    IFNULL(c_teachers.full_teacher_name, '') full_teacher_name,
    IFNULL(c_rooms.name, '') room_name
    FROM j_ptresult
    INNER JOIN leads ON j_ptresult.student_id = leads.id AND leads.deleted = 0 AND j_ptresult.parent = 'Leads'
    INNER JOIN meetings ON j_ptresult.meeting_id = meetings.id AND meetings.deleted = 0
    LEFT JOIN c_teachers ON meetings.teacher_id = c_teachers.id AND c_teachers.deleted = 0
    LEFT JOIN c_rooms ON meetings.room_id = c_rooms.id AND c_rooms.deleted = 0
    WHERE (j_ptresult.type_result = 'Demo') AND (contacts.id = '$contact_id') AND j_ptresult.deleted = 0";
    return $sql_demo_contact;
}

//Subpanel Placement Test In Contact
function getSubPTContact($params){
    global $current_user;
    $args = func_get_args();
    $contact_id = $args[0]['contact_id'];
    $sql_pt_contact = "SELECT DISTINCT
    IFNULL(j_ptresult.id, '') id,
    IFNULL(j_ptresult.name, '') name,
    IFNULL(j_ptresult.speaking, '') speaking,
    IFNULL(j_ptresult.listening, '') listening,
    IFNULL(j_ptresult.reading, '') reading,
    IFNULL(j_ptresult.writing, '') writing,
    IFNULL(j_ptresult.result, '') result,
    IFNULL(j_ptresult.score, '') score,
    IFNULL(j_ptresult.attended, '') attended,
    IFNULL(j_ptresult.ec_note, '') ec_note,
    IFNULL(j_ptresult.teacher_comment, '') teacher_comment
    FROM j_ptresult
    INNER JOIN contacts ON contacts.id = j_ptresult.student_id AND contacts.deleted = 0
    WHERE (j_ptresult.type_result = 'Placement Test') AND (contacts.id = '$contact_id') AND j_ptresult.deleted = 0";
    return $sql_pt_contact;
}

function get_option_koc(){;
    $q1 = "SELECT DISTINCT
    IFNULL(j_kindofcourse.id, '') primaryid,
    IFNULL(j_kindofcourse.name, '') kind_of_course,
    IFNULL(j_kindofcourse.content, '') content
    FROM j_kindofcourse
    INNER JOIN teams l1 ON j_kindofcourse.team_id = l1.id AND j_kindofcourse.status = 'Active' AND l1.deleted = 0
    WHERE j_kindofcourse.deleted = 0
    ORDER BY CASE WHEN
    (j_kindofcourse.kind_of_course = ''
    OR j_kindofcourse.kind_of_course IS NULL) THEN 0";
    $kocc = $GLOBALS['app_list_strings']['kind_of_course_list'];
    $count_koc = 1;
    foreach($kocc as $koc => $value) $q1 .= " WHEN j_kindofcourse.kind_of_course = '$koc' THEN ".$count_koc++;
    $q1 .= " ELSE $count_koc END ASC, j_kindofcourse.name ASC";
    $rs1 = $GLOBALS['db']->query($q1);
    $arr_koc = array('' => '-none-',);
    while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
        $content = json_decode(html_entity_decode($row['content']),true);
        foreach($content as $key => $value){
            if(!empty($value['levels'])) $level = ' - '.$value['levels'];
            else $level = '';

            $strig = $row['kind_of_course'].$level;
            if(!in_array($strig,$arr_koc))
                $arr_koc[$strig] = $strig;
        }
    }
    $arr_koc = array_unique($arr_koc);
    return $arr_koc;
}

function queryListResult($meetingId){
    global $current_user;
    $ext = '';
    if(!$current_user->isAdminForModule('J_PTResult'))
        $ext = " AND ((l1.team_set_id IN (SELECT
        tst.team_set_id
        FROM team_sets_teams tst
        INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
        AND team_memberships.user_id = '{$current_user->id}'
        AND team_memberships.deleted = 0)) OR (l2.team_set_id IN (SELECT
        tst.team_set_id
        FROM team_sets_teams tst
        INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
        AND team_memberships.user_id = '{$current_user->id}'
        AND team_memberships.deleted = 0)))";

    $q1="SELECT DISTINCT IFNULL(rs.id, '') id, IFNULL(rs.pt_order, 1) pt_order, IFNULL(rs.parent, '') parent_type,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.id,'') ELSE IFNULL(l1.id,'') END) student_id,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.gender,'') ELSE IFNULL(l1.gender,'') END) gender,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.status,'') ELSE IFNULL(l1.status,'') END) status,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.phone_mobile,'') ELSE IFNULL(l1.phone_mobile,'') END) phone_mobile,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.birthdate,'') ELSE IFNULL(l1.birthdate,'') END) birthdate,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.lead_source,'') ELSE IFNULL(l1.lead_source,'') END) lead_source,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.guardian_name,'') ELSE IFNULL(l1.guardian_name,'') END) parent_name,
    (CASE WHEN rs.parent = 'Contacts' THEN CONCAT(IFNULL(l2.last_name, ''), ' ', IFNULL(l2.first_name, '')) ELSE CONCAT(IFNULL(l1.last_name, ''), ' ', IFNULL(l1.first_name, '')) END) full_name,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.first_name, '') ELSE IFNULL(l1.first_name, '') END) first_name,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l2.last_name, '') ELSE IFNULL(l1.last_name, '') END) last_name,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l7.email_address,'') ELSE IFNULL(l6.email_address,'') END) email,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l7.id,'') ELSE IFNULL(l6.id,'') END) email_id,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l5.full_user_name,'') ELSE IFNULL(l4.full_user_name,'') END) assigned_to_name,
    (CASE WHEN rs.parent = 'Contacts' THEN IFNULL(l5.id,'') ELSE IFNULL(l4.id,'') END) assigned_to_id,
    IFNULL(rs.result_koc, '') result_koc, IFNULL(rs.result_lvl, '') result_lvl, IFNULL(rs.result, '') result,
    IFNULL(rs.score, '') score, IFNULL(rs.speaking, '') speaking,
    IFNULL(rs.listening, '') listening, IFNULL(rs.reading, '') reading,
    IFNULL(rs.writing, '') writing, IFNULL(rs.attended, '') attended,
    IFNULL(rs.ec_note, '') ec_note, IFNULL(rs.teacher_comment, '') teacher_comment
    FROM j_ptresult rs
    LEFT JOIN leads l1 ON rs.student_id = l1.id AND l1.deleted = 0 AND rs.parent = 'Leads'
    LEFT JOIN contacts l2 ON rs.student_id = l2.id AND l2.deleted = 0 AND rs.parent = 'Contacts'
    INNER JOIN meetings l3 ON rs.meeting_id = l3.id AND l3.deleted = 0
    LEFT JOIN users l4 ON l1.assigned_user_id = l4.id AND l4.deleted = 0
    LEFT JOIN users l5 ON l2.assigned_user_id = l5.id AND l5.deleted = 0
    LEFT JOIN email_addr_bean_rel l6_1 ON l1.id = l6_1.bean_id AND l6_1.deleted = 0 AND l6_1.primary_address = '1'
    LEFT JOIN email_addresses l6 ON l6.id = l6_1.email_address_id AND l6.deleted = 0
    LEFT JOIN email_addr_bean_rel l7_1 ON l2.id = l7_1.bean_id AND l7_1.deleted = 0 AND l7_1.primary_address = '1'
    LEFT JOIN email_addresses l7 ON l7.id = l7_1.email_address_id AND l7.deleted = 0
    WHERE l3.id = '$meetingId' AND rs.deleted = 0 $ext
    ORDER BY rs.pt_order ASC";
    return $q1;
}
?>
