<?php

class MeetingSubpanelQuery extends Link2
{
    function buildJoinDotbQuery($dotb_query, $options = array()){
     
        $dotb_query->where()->queryOr()->isEmpty('meeting_type')->equals('meeting_type','Meeting');
        return $this->relationship->buildJoinDotbQuery($this, $dotb_query, $options);
    }
}