<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class MeetingsViewDetail extends ViewDetail {



    function _displaySubPanels() {
        require_once('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($this->bean, $this->module);

        //Added by Hieu Pham to Hide basic Sub-Panel
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['history']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['leads']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['users']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['ptresult_link']);

        // Hide PT or Demo Sub-panel
        if ($this->bean->meeting_type == 'Demo'){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['sub_pt_result']);
        } else if ($this->bean->meeting_type == 'Placement Test') {
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['sub_demo_result']);
        } else {
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['sub_pt_result']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['sub_demo_result']);
        }
        echo $subpanel->display();
    }

    function display() {
        if($this->bean->meeting_type == 'Session' && !empty($this->bean->ju_class_id)){
            echo '
            <script type="text/javascript">
            location.href=\'index.php?module=J_Class&action=DetailView&record=' . $this->bean->ju_class_id . '\';
            </script>';
            die();
        }
        parent::display();
    }
}
?>