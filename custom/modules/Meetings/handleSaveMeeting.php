<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
include_once("custom/include/_helper/junior_schedule.php");

class handleSaveMeetings
{
    function handleSaveSession(&$bean, $event, $arguments){
        $bean->duration_cal = $bean->duration_hours + ($bean->duration_minutes / 60);

        //Cap nhat teaching hour va delivery hour
        if (empty($bean->delivery_hour))
            $bean->delivery_hour = $bean->duration_cal;

        if (empty($bean->teaching_hour)) {
            $bean->teaching_hour = $bean->duration_cal;
        }

        //Add Meeting Type = Meeting
        if(empty($bean->meeting_type)) $bean->meeting_type = 'Meeting';

    }


    /////////////////////////////-------------START------------------/////////////////////////////////////

    function handleSavePT(&$bean, $event, $arguments)
    {
        global $timedate;
        /////cover or makeup testing,demo
        if ($bean->date_modified != $bean->date_entered && ($bean->meeting_type == "Placement Test" || $bean->meeting_type == "Demo")) {
            if (isset($_GET['session_status']) && !empty($_GET['session_status']))
                $bean->session_status = $_GET['session_status'];
        } else {///save new meeting
            if (in_array($bean->meeting_type,['Session', 'Meeting'])){

            }elseif(in_array($bean->meeting_type,['Placement Test','Demo']))
                saveDemoPTReturnModule($bean, $_REQUEST['return_module'], $_REQUEST['return_id']);

        }
        //Date & Time Range
        $bean->date = $timedate->to_db_date($timedate->to_display_date($bean->date_start,true),false);
    }

    function addCode(&$bean, $event, $arguments){
        $code_field = 'name';
        if ($_POST['module'] == $bean->module_name
        && $_POST['action'] == 'Save'
        && empty($bean->$code_field)
        && ($bean->meeting_type == "Placement Test" || $bean->meeting_type == "Demo")){
            //AFTER_SAVE: Repeat 10 times to avoid duplicate codes
            for ($x = 0; $x < 10; $x++) {
                //Get Prefix
                $q1 = "SELECT teams.id primary_id, teams.code_prefix code_prefix,
                IFNULL(l2.code_prefix, '') parent_prefix
                FROM teams LEFT JOIN teams l2 ON teams.parent_id = l2.id AND l2.deleted = 0
                WHERE teams.id = '{$bean->team_id}'";

                $res = $GLOBALS['db']->query($q1);
                $row = $GLOBALS['db']->fetchByAssoc($res);
                $sep = '-';
                $prefix = $row['code_prefix'];
                if(!empty($prefix)) $prefix .= $sep;
                $date = date('y');

                $padding = 2;
                $table = $bean->table_name;

                if ($bean->meeting_type == "Demo") $ext = 'DEMO';
                elseif ($bean->meeting_type == "Placement Test") $ext = 'PT';

                $dates = date('d/m/Y', strtotime('+7 hours' . $bean->date_start));
                //Edit by Lap Nguyen
                if (empty($bean->fetched_row[$code_field]) && ($bean->meeting_type == "Demo" || $bean->meeting_type == "Placement Test")) {
                    $left_code = $prefix . $ext . $sep . $dates . $sep;

                    $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}'  AND LEFT($code_field , " . strlen($left_code) . ") = '$left_code' ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";
                    $result = $GLOBALS['db']->query($query);

                    if ($row = $GLOBALS['db']->fetchByAssoc($result)) {
                        $last_code = $row[$code_field];
                    } else {
                        //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                        $last_code = $left_code . '00';
                    }

                    $num = substr($last_code, -$padding, $padding);
                    $num++;
                    $pads = $padding - strlen($num);
                    $new_code = $left_code;

                    //preform the lead padding 0
                    for ($i = 0; $i < $pads; $i++)
                        $new_code .= "0";
                    $new_code .= $num;
                    //check duplicate code
                    $countDup = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM $table WHERE $code_field = '$new_code' AND deleted = 0");
                    if(empty($countDup)){
                        //write to database - Logic: Before Save
                        $GLOBALS['db']->query("UPDATE $table SET $code_field = '$new_code' WHERE id='{$bean->id}' AND deleted = 0");
                        break;
                    }
                }
            }
        }
    }

    function beforeDeleteSchedule(&$bean, $event, $arguments)
    {
        global $timedate, $current_user;
        if ($bean->meeting_type == "Placement Test" || $bean->meeting_type == "Demo") {

            //Delete Result
            if(!empty($bean->id))
                $GLOBALS['db']->query("UPDATE j_ptresult SET deleted=1, date_modified='{$timedate->nowDb()}', modified_user_id='{$current_user->id}' WHERE meeting_id = '{$bean->id}'");

            //Delete Timesheet
            if(!empty($bean->id))
                $GLOBALS['db']->query("UPDATE c_timesheet SET deleted=1, date_modified='{$timedate->nowDb()}', modified_user_id='{$current_user->id}' WHERE meeting_id = '{$bean->id}'");

        }elseif($bean->meeting_type == "Session"){
            echo '<script type="text/javascript">
            alert("You cannot Edit / Delete this session here. Please, action in the Class module !!");
            location.href=\'index.php?module=J_Class&action=DetailView&record=' . $bean->ju_class_id . '\';
            </script>';
            die();
        }
    }

    //Changing color of listview rows according to Session Status by Lap Nguyen
    function listviewcolor_Meetings(&$bean, $event, $arguments){
        // kiểm tra list view có field class_activity không
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[sizeof($arrayUrl) - 1];
        }
        //optimize performance - only load when in list view
        if(!empty($bean->panel_name) || strpos($_REQUEST['view'], 'selection-list') !== false || ($_REQUEST['view'] == 'list' && $url_module == $bean->module_name) ){
            global $timedate, $current_user;
            $date = $timedate->convertToDBDate($timedate->to_display_date_time($bean->date_start), false);
            // $week_date = date('l', strtotime($date));
            $week_date = date('w', strtotime($date));
            // $bean->week_date = $week_date;
            $bean->week_date = $GLOBALS['app_list_strings']['date_week'][$week_date];

            //Count Attended and number of student
            if ($bean->meeting_type == 'Demo' || $bean->meeting_type == 'Placement Test') {
                $now_int = strtotime($timedate->nowDb());
                $start_int = strtotime($timedate->to_db($bean->date_start));
                if ($now_int >= $start_int) {
                    $q1 = "SELECT DISTINCT
                    IFNULL(COUNT(DISTINCT j_ptresult.id), 0) j_ptresult__count
                    FROM j_ptresult
                    INNER JOIN meetings l1 ON l1.id = j_ptresult.meeting_id AND l1.deleted = 0
                    WHERE (l1.id = '{$bean->id}') AND ((j_ptresult.attended LIKE 'Yes' OR j_ptresult.attended = '1')) AND j_ptresult.deleted = 0
                    GROUP BY l1.id";
                    $count_attented = $GLOBALS['db']->getOne($q1);
                }
                if (empty($count_attented)) $count_attented = '0';


                $q2 = "SELECT DISTINCT
                IFNULL(COUNT(DISTINCT j_ptresult.id), 0) j_ptresult__count
                FROM j_ptresult
                INNER JOIN meetings l1 ON l1.id = j_ptresult.meeting_id AND l1.deleted = 0
                WHERE (l1.id = '{$bean->id}')
                AND j_ptresult.deleted = 0
                GROUP BY l1.id";
                $count_student = $GLOBALS['db']->getOne($q2);
                if (empty($count_student)) $count_student = '0';
                $color = '#468931';
                if (($count_attented) < 1)
                    $color = '#DC143C';
                $bean->attended_label = '<span align="center" style="color: '.$color.';font-weight: bold; float:right;">'.$count_attented.'</span>';
                $color = '#468931';
                if (($count_student) < 1)
                    $color = '#DC143C';
                $bean->number_of_student ='<span align="center" style="color: '.$color.';font-weight: bold; float:right;">'.$count_student.'</span>';

                //$bean->number_of_student = $count_student;
            } elseif ($bean->meeting_type == 'Session') {
                //Check filter Dashboard
                $checkFilter = false;
                if(!empty($_REQUEST['filter'])){
                    $test = getkeypath($_REQUEST['filter'], 'meeting_type');
                    if(!empty($test)) $checkFilter = true;
                }

                if(!empty($current_user->teacher_id)) $checkFilter = true;

                if($bean->panel_name == 'j_class_meetings_syllabus' || $checkFilter){
                    //lap nguyen load syllabus
                    $querySyllabus = "SELECT DISTINCT IFNULL(mt.id, '') primaryid, IFNULL(jb.lesson, '') lesson_number, IFNULL(jb.lesson_type, '') syllabus_type,
                    IFNULL(mt.topic_custom, '') topic_custom, IFNULL(mt.syllabus_custom, '') syllabus_custom,IFNULL(mt.objective_custom, '') objective_custom,
                    IFNULL(jb.name, '') theme, IFNULL(jb.description, '') content, IFNULL(jb.note_for_teacher, '') objective, IFNULL(jb.homework, '') homework,
                    IFNULL(mt.type, '') learning_type, IFNULL(mt.join_url, '') join_url, IFNULL(mt.external_id, '') external_id, IFNULL(mt.creator, '') creator,
                    mt.date_end date_end, mt.date_start date_start, IFNULL(mt.observe_note, '') observe_note
                    FROM meetings mt LEFT JOIN j_syllabus jb ON mt.syllabus_id = jb.id AND jb.deleted = 0
                    WHERE (mt.id = '{$bean->id}') AND mt.session_status <> 'Cancelled' AND mt.deleted = 0";
                    $rowS = $GLOBALS['db']->fetchOne($querySyllabus);

                    $bean->syllabus_type       = $GLOBALS['app_list_strings']['syllabus_type_list'][$rowS['syllabus_type']];
                    $bean->syllabus_topic      = (!empty($rowS['topic_custom'])) ? $rowS['topic_custom'] : $rowS['theme'];
                    $bean->syllabus_activities = (!empty($rowS['syllabus_custom'])) ? $rowS['syllabus_custom'] : $rowS['content'];
                    $bean->syllabus_objective  = (!empty($rowS['objective_custom'])) ? $rowS['objective_custom'] : $rowS['objective'];
                    $bean->syllabus_homework   = $rowS['homework'];

                    //Xử lý thêm thẻ a với url
                    foreach(['syllabus_topic','syllabus_activities','syllabus_objective','syllabus_homework'] as $field){
                        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $bean->$field, $matches);
                        foreach(array_unique($matches[0]) as $match){$bean->$field = str_replace($match,"<a href=\"$match\" target=\"_blank\">$match</a>",$bean->$field);}
                    }//END

                    //Generate E-leaning Room
                    $nowDBDate = $timedate->nowDbDate();
                    $dateEnd = $timedate->convertToDBDate($timedate->to_display_date($rowS['date_end']));

                    $meetingLink = '';
                    if(!empty($rowS['learning_type']) && $rowS['learning_type'] != 'Dotb' && $nowDBDate <= $dateEnd){
                        if ( !empty($rowS['external_id'])) {
                            $meetingLink .= '<a class="btn button" style="height: 60px;" title="'.translate('LBL_JOIN_EXT_MEETING','Meetings').'" href="'.$rowS['join_url'].'" target="_blank"><img style="height: 60px;" src="themes/default/images/'.str_replace(' ','_',strtolower($rowS['learning_type'])).'.png">'.'</a>';
                            $bean->join_url = $meetingLink;
                        }else{
                            if(empty($rowS['creator'])) $bean->join_url = '<span style="color: #002bff;" title="'.translate('LBL_SCHEDULED_DES','Meetings').'">'.translate('LBL_SCHEDULED','Meetings').' '.$rowS['learning_type'].'</span>';
                            else $bean->join_url = '<span class="error">'.translate($rowS['creator'],'Meetings').'</span>';
                        }
                    }else $bean->join_url = '';

                    $bean->subpanel_btn3 = '';
                    //Button Create Online Learning
                    if(ACLController::checkAccess('J_Class', 'edit', true)){
                        if(($bean->type == 'Dotb' || empty($bean->type)) && $nowDBDate <= $dateEnd)
                            $bean->subpanel_btn3 = '<button style="white-space: nowrap;" type="button" class="button btn_create_online" date_start="'.$timedate->to_display_date_time($bean->date_start).'" date_end="'.$timedate->to_display_date_time($bean->date_end).'" id="'.$bean->id.'" learning_type="'.$bean->type.'"><i class="fal fa-video-plus"></i> '.translate('LBL_CREATE_LMS_LINK','Meetings').'</button>';

                        if(!empty($bean->type) && $bean->type != 'Dotb' && $nowDBDate <= $dateEnd)
                            $bean->subpanel_btn3 = '<button style="white-space: nowrap;" type="button" class="button btn_cancel_online" id="'.$bean->id.'" learning_type="'.$bean->type.'"><i class="fal fa-video-slash"></i> '.translate('LBL_REMOVE_LMS_LINK','Meetings').'</button>';
                    }

                }

                //Add button delete Session, Cover Session Session on Subpanel
                if($bean->panel_name == 'j_class_meetings'){
                    $bean->subpanel_button = '';
                    if (checkDataLockDate($timedate->to_display_date($bean->date_end))
                        && $bean->session_status != 'Cancelled'
                        && ACLController::checkAccess('J_Class', 'edit', true))
                        $bean->subpanel_button .= '<button style="white-space: nowrap;" type="button" class="button btn_cancel_session" onclick="cancelSession($(this));" id="'.$bean->id.'"><span style="color:#d43f3a"><i class="fal fa-calendar-times"></i> '.translate('LBL_CANCEL_SESSION','Meetings').'</span></button>';


                    if(ACLController::checkAccess('J_Class', 'edit', true)
                        && $bean->session_status == 'Cancelled')
                        $bean->subpanel_button .='<button style="white-space: nowrap;" type="button" class="button btn_deleted_session" onclick="deleteSession($(this));" id="'.$bean->id.'"><i class="fal fa-trash-alt"></i> '. $GLOBALS['app_strings']['LBL_DELETE_BUTTON'].'</button>';
                }
                //j_class_subpanel_class_attendences
                if($bean->panel_name == 'class_attendances'){
                    if($bean->total_sent > 0) $bean->sent_app_label = '<b>'.$bean->total_sent.' </b><i style="color: #1a9c20; font-size:14px" class="fa fa-comment-check"></i>';
                    if($bean->attendance_status) $bean->attended_label = "<i style='color: #1a9c20; font-size:20px' class='fa fa-check'></i>";
                    foreach(['total_attended', 'total_student', 'total_absent'] as $att_field) $bean->$att_field   = (!empty($bean->$att_field) ? $bean->$att_field : '');
                    //Handle Lock Status
                    $sstimedate = $bean->week_date.' '.$timedate->to_display_date($bean->date,false).': '.$timedate->to_display_time($bean->date_start).' - '.$timedate->to_display_time($bean->date_end);
                    if($bean->session_status != 'Cancelled'){
                        $bean->subpanel_btn_edit = '<span style="white-space: nowrap;">';
                        $bean->subpanel_btn_edit .= '<a style="white-space: nowrap; margin-right:10px;" target="_parent" type="button" class="button" href="'.str_replace('index.php','', $_SERVER['SCRIPT_NAME']).'#bwc/index.php?module=J_Class&action=attendance&session_id='.$bean->id.'"><i class="far fa-tasks"></i> '.translate('LBL_CHECK_ATTENDANCE','Meetings').'</a>';
                        switch ($bean->lock_status) {
                            case 'Locked':
                                $bean->subpanel_btn_edit .= '<a style="white-space: nowrap; color: #222;" type="button" class="button btn_request_ss" ssid="'.$bean->id.'" timedate="'.$sstimedate.'"><i class="far fa-check"></i> '.translate('LBL_SEND_UNLOCK_REQUEST','Meetings').'</a>';
                                break;
                            case 'Unlocked':
                                break;
                            case 'Requesting':
                                if(ACLController::checkField('Meetings', 'lock_status', 'edit')){
                                    $bean->subpanel_btn_edit .= '<a style="white-space: nowrap; color:green;" type="button" class="button btn_unlock_ss" ssid="'.$bean->id.'" timedate="'.$sstimedate.'"><i class="far fa-lock-open"></i> '.translate('LBL_BTN_UNLOCK','Meetings').'</a>';
                                    $bean->subpanel_btn_edit .= '<a style="white-space: nowrap; margin-left:10px; color:red;" type="button" class="button btn_lock_ss" ssid="'.$bean->id.'" timedate="'.$sstimedate.'"><i class="far fa-lock"></i> '.translate('LBL_BTN_LOCK','Meetings').'</a>';
                                }
                                break;
                        }
                        $bean->subpanel_btn_edit .= '</span>';
                    }
                    if($bean->lock_status == 'Locked') $bean->lock_status = '<i class="far fa-lock"></i>';
                    if($bean->lock_status == 'Unlocked') $bean->lock_status = '<i class="far fa-lock-open"></i>';
                }
            }
            //Colorzide
            $bean->duration_text = "<span style='white-space: nowrap;'>".durationToText($bean->duration_hours,$bean->duration_minutes)."</span>";

            $date_start = strtotime($bean->date_start);
            $date_end = strtotime($bean->date_end);
            $now = strtotime($timedate->nowDb());

            if ($bean->session_status == 'Cancelled'){
                // $bean->session_status = "<span class='textbg_black'>Cancelled</span>";
                $bean->session_status = "<span class='label-darkgray'>".$GLOBALS['app_list_strings']['session_status_list'][$bean->session_status]."</span>";
                $ss_mk_date = $GLOBALS['db']->getOne("SELECT date_start FROM meetings WHERE id = '{$bean->makeup_session_id}' LIMIT 1;");
                $bean->description = $GLOBALS['app_list_strings']['cancel_reason_list'][$bean->cancel_by]." - Make-up session: ".$timedate->to_display_date($ss_mk_date).".";
                $bean->till_hour = '';
            }elseif ($now < $date_start){
                $bean->session_status = 'Not Started';
                $bean->session_status = "<span class='label-lightgreen'>".$GLOBALS['app_list_strings']['session_status_list'][$bean->session_status]."</span>";
            }
            elseif ($now >= $date_start && $now <= $date_end){
                $bean->session_status = 'In Progress';
                $bean->session_status = "<span class='label-lightblue'>".$GLOBALS['app_list_strings']['session_status_list'][$bean->session_status]."</span>";
            }
            elseif ($now > $date_end){
                $bean->session_status = 'Finished';
                $bean->session_status = "<span class='label-lightred'>".$GLOBALS['app_list_strings']['session_status_list'][$bean->session_status]."</span>";
            }

            //fill TA Name
            if (!empty($bean->teacher_cover_id))
                $bean->teacher_cover_name = $GLOBALS['db']->getOne("SELECT CONCAT(IFNULL(last_name, ''),' ',IFNULL(first_name, '')) teacher_name FROM c_teachers WHERE id = '{$bean->teacher_cover_id}' AND deleted = 0");
            else
                $bean->teacher_cover_name = '';

            //fill Sub Teacher
            if (!empty($bean->sub_teacher_id))
                $bean->sub_teacher_name = $GLOBALS['db']->getOne("SELECT CONCAT(IFNULL(last_name, ''),' ',IFNULL(first_name, '')) teacher_name FROM c_teachers WHERE id = '{$bean->sub_teacher_id}' AND deleted = 0");
            else
                $bean->sub_teacher_name = '';

            if(!empty($bean->teaching_type)) {
                $bean->teacher_name = $bean->teacher_name . '</a><br>(' . $GLOBALS['app_list_strings']['teaching_type_options'][$bean->teaching_type] . ')';
            }

            if (checkDataLockDate($timedate->to_display_date($bean->date_end)) && ACLController::checkAccess('J_Class', 'edit', true)) {
                $bean->late_time = '<select class="sls_late_time" name="late_time" meeting_id = "'.$bean->id.'" style="width:auto;">'.get_select_options_with_id($GLOBALS['app_list_strings']['late_time_list'],$bean->late_time).'</select>';
                $bean->late_time_ta1 = '<select class="sls_late_time_ta1" name="late_time_ta1" meeting_id = "'.$bean->id.'" style="width: 80px;">'.get_select_options_with_id($GLOBALS['app_list_strings']['late_time_list'],$bean->late_time_ta1).'</select>';
                $bean->late_time_ta2 = '<select class="sls_late_time_ta2" name="late_time_ta2" meeting_id = "'.$bean->id.'" style="width: 80px;">'.get_select_options_with_id($GLOBALS['app_list_strings']['late_time_list'],$bean->late_time_ta2).'</select>';
                $bean->planned_absence = '<select class="sls_late_time" name="planned_absence" meeting_id = "'.$bean->id.'" style="width:auto;">'.get_select_options_with_id($GLOBALS['app_list_strings']['planned_absence_list'], $bean->planned_absence).'</select>';
            }


            // change status
            $end = strtotime($bean->date_end);
            $now = strtotime($timedate->nowDb());
            if($end < $now){
                $bean->status = 'Held';
            }
        }
        if($_SESSION['platform'] == 'portal'){
            $student_id = $_SESSION['contact_id'];
            global $timedate;
            $today = $timedate->nowDb();
            $nextday = $timedate->asDbDate($timedate->getNow()->get("+1 day"));
            $date_db = $bean->date_start;
            $DATE_START = $timedate->to_display_date_time($bean->date_start);
            $DATE_END   = $timedate->to_display_date_time($bean->date_end);
            if($date_db < $today) {
                $bean->time_start_end= "<font class='overdueTask' style='white-space: nowrap; color: #E61718'>".substr($DATE_START,0, 10).': '.substr($DATE_START, 11,5).' - '.substr($DATE_END, 11)."</font>";
            }else if($date_db < $nextday) {
                $bean->time_start_end = "<font class='todaysTask' style='white-space: nowrap; color: #FF7800'>".substr($DATE_START,0, 10).': '.substr($DATE_START, 11,5).' - '.substr($DATE_END, 11)."</font>";
            } else {
                $bean->time_start_end = "<font class='futureTask' style='white-space: nowrap; color: #3468C0'>".substr($DATE_START,0, 10).': '.substr($DATE_START, 11,5).' - '.substr($DATE_END, 11)."</font>";
            }
            $bean->duration_text = "<span style='white-space: nowrap;'>".durationToText($bean->duration_hours,$bean->duration_minutes)."</span>";

            $sqlAtt = "SELECT
            IFNULL(l1.id, '') att_id,
            IFNULL(l1.customer_comment, '') student_comment,
            IFNULL(l1.star_rating, '') star_rating,
            IFNULL(l1.homework_comment, '') teacher_comment,
            IFNULL(l1.attendance_type, '') attendance_type
            FROM c_attendance l1
            WHERE l1.meeting_id = '$bean->id' AND l1.student_id = '$student_id' AND l1.deleted = 0";

            $resultAtt = $GLOBALS['db']->fetchOne($sqlAtt);
            $bean->student_feedback = !empty($resultAtt['student_comment']) ? $resultAtt['student_comment'] : '';
            $bean->teacher_comment = !empty($resultAtt['teacher_comment']) ? $resultAtt['teacher_comment'] : '';
            //Attendance status
            $tmp_status = $GLOBALS['app_list_strings']['attendance_type_list'][$resultAtt['attendance_type']];
            switch ($resultAtt['attendance_type']) {
                case 'P':
                    $color = "label-success";
                    $icon  = "fa-check";
                    break;
                case 'L':
                    $color = "label-warning";
                    $icon  = "fa-clock-o";
                    break;
                case 'E':
                    $color = "label-excused";
                    $icon  = "fa-ban";
                    break;
                case 'A':
                    $color = "label-important";
                    $icon  = "fa-times";
                    break;
            }
            //Syllabus
            $querySyllabus = "SELECT DISTINCT IFNULL(mt.id, '') primaryid,
            IFNULL(mt.topic_custom, '') topic_custom,
            IFNULL(jb.name, '') theme,
            IFNULL(jb.homework, '') homework
            FROM meetings mt LEFT JOIN j_syllabus jb ON mt.syllabus_id = jb.id AND jb.deleted = 0
            WHERE (mt.id = '{$bean->id}') AND mt.session_status <> 'Cancelled' AND mt.deleted = 0";
            $rowS = $GLOBALS['db']->fetchOne($querySyllabus);

            $bean->syllabus_topic      = (!empty($rowS['topic_custom'])) ? $rowS['topic_custom'] : $rowS['theme'];
            $bean->syllabus_homework   = $rowS['homework'];

            if(!empty($resultAtt['attendance_type'])) $bean->attendance = "<span class='label ellipsis_inline $color'><i class='far $icon'></i>$tmp_status</span>";
            else $bean->attendance_type_text = '';
            //Rating
            $bean->student_rate = !empty($resultAtt['star_rating']) ? "<div class='star-rating'>{$resultAtt['star_rating']}<span class='star'>★</span></div>" : '';
            //Weeking date
            $date = $timedate->convertToDBDate($timedate->to_display_date_time($bean->date_start), false);
            $week_date = date('w', strtotime($date));
            $bean->week_date = $GLOBALS['app_list_strings']['date_week'][$week_date];

        }
    }
    function changeStatus(&$bean, $event, $arguments){
        global $timedate;
        $end = strtotime($bean->date_end);
        $now = strtotime($timedate->nowDb());
        if($end <= $now){
            $bean->status = 'Held';
        }
    }


    /** Remove filter Session By Lap Nguyen
    * @param DotbBean $team
    * @param $event
    * @param $args
    */
    function addFilter(&$bean, $event, $args) {
        global $current_user;
        //Check filter Dashboard
        $is_teacher = false;
        if(!empty($current_user->teacher_id)) $is_teacher = true;
        $is_listview = false;
        if($_REQUEST['view'] == 'list') $is_listview = true;


        //PT/ Demo Online
        if($is_listview){
            $values = array('Meeting','Demo','Placement Test');
            $args[0]->where()->in('meeting_type',$values);
            /**
            * edit by TKT
            * make sure id_query exist and is a object class of query
            */
            if(method_exists($args[1]['id_query'],'where'))
                $args[1]['id_query']->where()->in('meeting_type', $values);
            //end*****************************************************
        }

        if($is_teacher){
            $teacher = BeanFactory::getBean('C_Teachers', $current_user->teacher_id);
            if(!empty($teacher->id)){
                $args[0]->where()->queryOr()
                ->equals('teacher_id',$teacher->id)
                ->equals('teacher_cover_id',$teacher->id)
                ->equals('sub_teacher_id',$teacher->id);

                if(method_exists($args[1]['id_query'],'where'))
                    $args[1]['id_query']->where()->queryOr()
                    ->equals('teacher_id',$teacher->id)
                    ->equals('teacher_cover_id',$teacher->id)
                    ->equals('sub_teacher_id',$teacher->id);;
            }
        }
    }
}

function saveDemoPTReturnModule($meeting, $return_module, $student_id){
    global $timedate;
    //////////////Save PT/Demo From Create PT Demo From Leads
    if ($return_module == "Leads" || $return_module == 'Contacts') {
        //save PT IN Case Create PT From Lead
        $result = new J_PTResult();
        $result->type_result = $meeting->meeting_type;
        if ($result->type_result == "Placement Test") {
            $first_time_mt = $meeting->date_start;
            $result->time_start = $first_time_mt;
            $result->time_end = date('Y-m-d H:i:s', strtotime("$first_time_mt +10 minutes"));
            $result->pt_order = 1;
        }
        $result->team_id = $meeting->team_id;
        $result->team_set_id = $meeting->team_set_id;
        $result->assigned_user_id = $meeting->assigned_user_id;
        $result->attended = '';
        $result->parent = $return_module;

        $rela_student = BeanFactory::getBean($result->parent, $student_id);
        $result->name = $meeting->id . ' - ' . $rela_student->last_name . ' ' . $rela_student->first_name;

        $result->student_id = $student_id;
        $result->meeting_id = $meeting->id;
        $result->save();
    }
}

/////////////////////////////-------------End------------------/////////////////////////////////////


?>
