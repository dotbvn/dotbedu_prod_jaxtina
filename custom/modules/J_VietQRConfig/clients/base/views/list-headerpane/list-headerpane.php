<?php


$viewdefs['J_VietQRConfig']['base']['view']['list-headerpane'] = array(
    'title' => 'LBL_MODULE_NAME',
    'buttons' => array(
        array(
            'label' => 'LNK_CREATE_VIETQR_CONFIG',
            'tooltip' => 'LNK_CREATE_VIETQR_CONFIG',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_VietQRConfig',
            'route' => "#J_VietQRConfig/create",
            'icon' => 'fa-plus',
        ),
        array(
            'label' => 'LNK_BANK_TRANSACTIONS',
            'tooltip' => 'LNK_BANK_TRANSACTIONS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'J_BankTrans',
            'route' => "#J_BankTrans",
            'icon' => 'fa-bars',
        ),
    ),
);