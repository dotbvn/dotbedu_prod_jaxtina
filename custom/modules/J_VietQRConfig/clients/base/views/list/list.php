<?php
$module_name = 'J_VietQRConfig';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_1',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'name',
                                'label' => 'LBL_NAME',
                                'default' => true,
                                'enabled' => true,
                                'link' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'status',
                                'type' => 'html',
                                'label' => 'LBL_STATUS',
                                'enabled' => true,
                                'default' => true,
                            ),
                            2 =>
                            array (
                                'name' => 'bank_id',
                                'label' => 'LBL_BANK_ID',
                                'enabled' => true,
                                'default' => true,
                            ),
                            3 =>
                            array (
                                'name' => 'account_number',
                                'label' => 'LBL_ACCOUNT_NUMBER',
                                'enabled' => true,
                                'default' => true,
                            ),
                            4 =>
                            array (
                                'name' => 'account_holder_name',
                                'label' => 'LBL_ACCOUNT_HOLDER_NAME',
                                'enabled' => true,
                                'default' => true,
                            ),
                            5 =>
                            array (
                                'name' => 'date_entered',
                                'enabled' => true,
                                'default' => true,
                            ),
                            6 =>
                            array (
                                'name' => 'team_name',
                                'label' => 'LBL_TEAM',
                                'default' => true,
                                'enabled' => true,
                            ),
                            7 =>
                            array (
                                'name' => 'description',
                                'label' => 'LBL_DESCRIPTION',
                                'enabled' => true,
                                'sortable' => false,
                                'default' => false,
                            ),
                            8 =>
                            array (
                                'name' => 'created_by_name',
                                'label' => 'LBL_CREATED',
                                'enabled' => true,
                                'readonly' => true,
                                'id' => 'CREATED_BY',
                                'link' => true,
                                'default' => false,
                            ),
                            9 =>
                            array (
                                'name' => 'modified_by_name',
                                'label' => 'LBL_MODIFIED',
                                'enabled' => true,
                                'readonly' => true,
                                'id' => 'MODIFIED_USER_ID',
                                'link' => true,
                                'default' => false,
                            ),
                            10 =>
                            array (
                                'name' => 'date_modified',
                                'enabled' => true,
                                'default' => false,
                            ),
                            11 =>
                            array (
                                'name' => 'api_key',
                                'label' => 'LBL_API_KEY',
                                'enabled' => true,
                                'default' => false,
                            ),
                            12 =>
                            array (
                                'name' => 'client_id',
                                'label' => 'LBL_CLIENT_ID',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
                'orderBy' =>
                array (
                    'field' => 'date_modified',
                    'direction' => 'desc',
                ),
            ),
        ),
    ),
);
