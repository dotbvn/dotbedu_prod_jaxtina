<?php


class logicBankCons{
    public function handleBankConnect(&$bean, $event, $arguments){
        //non-ASCII remove
        $bean->account_holder_name = strtoupper(preg_replace('/[[:^print:]]/','',viToEn($bean->account_holder_name)));  //remove non ACSII
        $bean->account_number      = formatQRPrefix($bean->account_number);//remove special characters + whitespace
        $bean->ext_content         = strtoupper(preg_replace('/[^a-zA-Z0-9 ]/','',viToEn($bean->ext_content)));  //remove special characters

        //==>To Do: Check duplicate config
        $bean->name = $bean->bank_id.'-'.$bean->account_number;
        $qD = "SELECT IFNULL(id,'') primaryId  FROM j_vietqrconfig WHERE IFNULL(account_number,'')='{$bean->account_number}' AND IFNULL(bank_id,'')='{$bean->bank_id}' AND deleted=0 AND IFNULL(id,'') <> '{$bean->id}' AND status = 'Enable'";
        $id = $GLOBALS['db']->getOne($qD);
        if(!empty($id)){
            $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_VietQRConfig','".$id."'));";
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: \'--Configuration already exists !!--<br>--REDIRECTED TO RECORD--\',
            autoClose: false});
            '.$route.'
            </script>';
            die();
        }

    }

    public function listViewColor(&$bean, $event, $arguments){
        switch ($bean->status) {
            case "Enable":
                $colorClass = "label-success";
                break;
            case "Disable":
                $colorClass = "label-important";
                break;
            default:
                $colorClass = "";
        }
        $bean->status = "<div class='label ellipsis_inline $colorClass'>".$GLOBALS['app_list_strings']['bank_connection_status_list'][$bean->status]."</div>";
    }
}