<?php
$count = 0;

$qStd = $GLOBALS['db']->query("UPDATE j_studentsituations SET deleted=1 WHERE
    total_hour <= 0.2
    AND type IN ('Enrolled', 'Demo', 'OutStanding')
    AND deleted = 0");


//Update Attended
$q1 = "SELECT DISTINCT IFNULL(contacts.id, '') student_id,
l1.id class_student_id, IFNULL(l2.id, '') class_id,
COUNT(l3.id) count_situation
FROM contacts
INNER JOIN j_classstudents l1 ON contacts.id = l1.student_id AND l1.deleted = 0 AND l1.student_type = 'Contacts'
INNER JOIN j_class l2 ON l1.class_id = l2.id AND l2.deleted = 0
LEFT JOIN j_studentsituations l3 ON l2.id = l3.ju_class_id AND contacts.id = l3.student_id AND l3.deleted = 0
AND l3.type IN ('Enrolled', 'OutStanding', 'Demo')
WHERE contacts.deleted = 0
GROUP BY student_id, class_id
HAVING COUNT(l3.id) = 0";
$res1 = $GLOBALS['db']->query($q1);
while($r = $GLOBALS['db']->fetchByAssoc($res1)){
    $GLOBALS['db']->query("UPDATE j_classstudents SET deleted=1 WHERE id='{$r['class_student_id']}' AND deleted = 0");
    $count++;
}

updateStudentStatus(); // update student status

echo('<br><br>CLASS STUDENT - Update done Class : '.$count);

//Class attendance
$count = 0;
//Update Attended
$q1 = "SELECT DISTINCT IFNULL(j_class.id,'') class_id ,IFNULL(j_class.name,'') class_name FROM j_class WHERE ((1=1)) AND j_class.deleted=0";
$res1 = $GLOBALS['db']->query($q1);
while($r = $GLOBALS['db']->fetchByAssoc($res1)){
    $count++;
    updateClassStudent($r['class_id']);
}
echo('<br><br>CLASS STUDENT - Update done Class : '.$count);

//Update Thống nhất chung 1 field Gender
$GLOBALS['db']->query("UPDATE leads SET gender = 'Male' WHERE gender IN ('Nam', 'male');");
$GLOBALS['db']->query("UPDATE leads SET gender = 'Female' WHERE gender IN ('Nữ', 'female');");


$GLOBALS['db']->query("UPDATE prospects SET gender = 'Male' WHERE gender IN ('Nam', 'male');");
$GLOBALS['db']->query("UPDATE prospects SET gender = 'Female' WHERE gender IN ('Nữ', 'female');");


$GLOBALS['db']->query("UPDATE contacts SET gender = 'Male' WHERE gender IN ('Nam', 'male');");
$GLOBALS['db']->query("UPDATE contacts SET gender = 'Female' WHERE gender IN ('Nữ', 'female');");

$GLOBALS['db']->query("UPDATE c_teachers SET gender = 'Male' WHERE gender IN ('Nam', 'male');");
$GLOBALS['db']->query("UPDATE c_teachers SET gender = 'Female' WHERE gender IN ('Nữ', 'female');");

echo '<br><br>Updated Gender Lead/Target/Student/Teacher: DONE!';



//UPDATE GRADEBOOK
$query0 = "UPDATE j_gradebook
            SET type =
                CASE
                    WHEN type = 'P1' THEN 'P01'
                    WHEN type = 'P2' THEN 'P02'
                    WHEN type = 'P3' THEN 'P03'
                    WHEN type = 'P4' THEN 'P04'
                    WHEN type = 'P5' THEN 'P05'
                    WHEN type = 'P6' THEN 'P06'
                    WHEN type = 'P7' THEN 'P07'
                    WHEN type = 'P8' THEN 'P08'
                    WHEN type = 'P9' THEN 'P09'
                    ELSE type
                    END
            WHERE id != '' AND deleted = 0";
$res0 = $GLOBALS['db']->query($query0);
$query1 = "UPDATE j_gradebookconfig
            SET type =
                CASE
                    WHEN type = 'P1' THEN 'P01'
                    WHEN type = 'P2' THEN 'P02'
                    WHEN type = 'P3' THEN 'P03'
                    WHEN type = 'P4' THEN 'P04'
                    WHEN type = 'P5' THEN 'P05'
                    WHEN type = 'P6' THEN 'P06'
                    WHEN type = 'P7' THEN 'P07'
                    WHEN type = 'P8' THEN 'P08'
                    WHEN type = 'P9' THEN 'P09'
                    ELSE type
                    END
            WHERE id != '' AND deleted = 0";
$res1 = $GLOBALS['db']->query($query1);
echo('<br><br>Update done');