<?php
$module_name = 'J_ReferenceLog';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'record' => 
      array (
        'buttons' => 
        array (
        ),
        'panels' => 
        array (
          0 => 
          array (
            'name' => 'panel_header',
            'label' => 'LBL_RECORD_HEADER',
            'header' => true,
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'picture',
                'type' => 'avatar',
                'width' => 42,
                'height' => 42,
                'dismiss_label' => true,
                'readonly' => true,
              ),
              1 => 'name',
              2 => 
              array (
                'name' => 'favorite',
                'label' => 'LBL_FAVORITE',
                'type' => 'favorite',
                'readonly' => true,
                'dismiss_label' => true,
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'panel_body',
            'label' => 'LBL_RECORD_BODY',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'newTab' => false,
            'panelDefault' => 'expanded',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'parent_name',
                'studio' => true,
                'label' => 'LBL_PARENT_NAME',
              ),
              1 => 
              array (
                'name' => 'reference',
                'label' => 'LBL_REFERENCE',
              ),
              2 => 
              array (
                'name' => 'email1',
                'label' => 'LBL_EMAIL',
              ),
              3 => 
              array (
                'name' => 'phone_mobile',
                'label' => 'LBL_PHONE_MOBILE',
              ),
              4 => 
              array (
                'name' => 'hits',
                'comment' => 'Number of times the item has been invoked (e.g., submit)',
                'label' => 'LBL_HITS',
              ),
              5 => 
              array (
                'name' => 'lead_source',
                'label' => 'LBL_LEAD_SOURCE',
              ),
              6 => 
              array (
                'name' => 'channel',
                'label' => 'LBL_CHANNEL',
              ),
              7 => 
              array (
                'name' => 'source_description',
              ),
              8 => 
              array (
                'name' => 'campaign_name',
              ),
              9 => 
              array (
                'name' => 'utm_source',
                'label' => 'LBL_UTM_SOURCE',
              ),
              10 => 
              array (
                'name' => 'utm_content',
                'label' => 'LBL_UTM_CONTENT',
              ),
              11 => 
              array (
                'name' => 'utm_medium',
                'label' => 'LBL_UTM_MEDIUM',
              ),
              12 => 
              array (
                'name' => 'description',
                'span' => 12,
              ),
              13 => 
              array (
                'name' => 'date_modified_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_MODIFIED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_modified',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'modified_by_name',
                  ),
                ),
              ),
              14 => 
              array (
                'name' => 'date_entered_by',
                'readonly' => true,
                'inline' => true,
                'type' => 'fieldset',
                'label' => 'LBL_DATE_ENTERED',
                'fields' => 
                array (
                  0 => 
                  array (
                    'name' => 'date_entered',
                  ),
                  1 => 
                  array (
                    'type' => 'label',
                    'default_value' => 'LBL_BY',
                  ),
                  2 => 
                  array (
                    'name' => 'created_by_name',
                  ),
                ),
              ),
            ),
          ),
        ),
        'templateMeta' => 
        array (
          'useTabs' => false,
        ),
      ),
    ),
  ),
);
