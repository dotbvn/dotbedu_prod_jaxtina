<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will
// be automatically rebuilt in the future.
 $hook_version = 1;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1,'handleBeforeSave','custom/modules/Bugs/logicBugs.php','logicBugs','handleBeforeSave');
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1,'sync Quyettam','custom/modules/Bugs/logicBugs.php','logicBugs','handleAfterSave',);
$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(1,'Color','custom/modules/Bugs/logicBugs.php','logicBugs','handleStatusColor',);
$hook_array['process_record'][] = Array(1,'Color','custom/modules/Bugs/logicBugs.php','logicBugs','handleUnreadComment',);
$hook_array['after_retrieve'] = Array();
$hook_array['after_retrieve'][] = Array(1,'Color','custom/modules/Bugs/logicBugs.php','logicBugs','handleStatusColor',);

?>
