<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class logicBugs {
    function handleAfterSave(&$bean, $event, $arguments){
        require_once('custom/include/utils/DotbWebhook.php');
        $webhook = new DotbWebhook();
        $attachment = [];
        $site_url  = $GLOBALS['dotb_config']['site_url'];
        $is_new_file = false;
        if($bean->file_mime_type !== null){
            $is_new_file = true;
            require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';
            //Check connection S3
            $AWS = new AWSHelper();
            if (!$AWS->getS3()) return array('success' => 0);
            $file_url = 'upload/' . $bean->id;
            $path = $GLOBALS['dotb_config']['brand_id'].'/comments/';
            if(file_exists($file_url)) {
                $result = $AWS->uploadAWS($path, $bean->id . '.' . $bean->file_ext, $file_url);
                if($result['success'] == 1){
                    $file_url = $result['url'];
                    $upload_id = $result['key_name'];
                    $file_size = filesize('upload/' . $bean->id);
                    unlink($file_url);
                }
            }
        }
        $tenant_id = $GLOBALS['license']['lic_tenant_id'];
        $data = array (
            'id' => $bean->id,
            'name' => $bean->name,
            'description' => $bean->description,
            'type' => $bean->type,
            'status' => $bean->status,
            'priority' => $bean->severity,
            'source' => 'ems',
            'product_category' => $bean->product,
            'assigned_user_id' => $GLOBALS['current_user']->id,
            'send_notification_to' => $bean->receive_noti_users,
            'tenant_id' => $tenant_id,
            'file_url' => $file_url,
            'file_name' => $bean->attachment,
            'upload_id' => $upload_id,
            'file_ext' => $bean->file_ext,
            'file_size' => $file_size,
            'is_new_file' => $is_new_file,
            'file_mime_type' => $bean->file_mime_type,
            'chatter_name' => $GLOBALS['current_user']->full_name,
            'rate' => $bean->rate,
            'avatar' => !empty($GLOBALS['current_user']->picture) ? $site_url . '/upload/' . $GLOBALS['current_user']->picture : '',
        );
        $webhook->callQuyettamAPI('ticket-support/create-case-support', 'POST', $data);
    }
    function handleUnreadComment(&$bean, $event, $arguments){
        require_once('custom/include/utils/DotbWebhook.php');
        $webhook = new DotbWebhook();
        $result = $webhook->callQuyettamAPI('ticket-support/getUnreadComment', 'GET', ['id' => $bean->id]);
        $data = $result['data']['data'];
        $comment = $data['comment'];
        $bean->name = '<div><a href="#Bugs/'.$bean->id.'">'.$bean->name.'</a>';
        if(!empty($comment)) {
            if($comment['direction'] == 'inbound'){
                $class = "text-info";
            }elseif($comment['direction'] == 'outbound'){
                $class = "purple-text";
            }
            if(empty($comment['description'])) $comment['description'] =  translate('LBL_SEND_ATTACHMENT', 'Cases');
            $content = $this->get_excerpt($comment['description'],0,55);
            if($data['count_comment'] > 0) $bean->name .='<span style="background: #b280ff; float: right" class="badge"><font style="vertical-align: inherit;">'.$data['count_comment'].'</font></span>';
            $bean->name .='<br><strong style="font-size: 11px;" class="'.$class.'">'.$comment['chatter'].':</strong>
                <span rel="tooltip" data-original-title="'.$comment['description'].'">'.$content.'</span>';
            $bean->name .=' <i rel="tooltip" style="float:right;color: #606060;" data-original-title="'.$GLOBALS['timedate']->to_display_date_time($comment['date_entered']).'">'.$GLOBALS['timedate']->time_Ago($comment['date_entered']);
            if(!$comment['is_read_in_tenant']) $bean->name .= '<i class="unread-notification fa fa-circle" style="font-size: 5px;vertical-align: middle;padding-left: 5px;"></i>';
            $bean->name .= '</i>';
        }
        $bean->name .='</div>';
    }
    function get_excerpt( $string, $start_postion = 0, $max_length = 100 ) {
        if ( strlen( $string ) <= $max_length ) {
            return $string;
        }
        $excerpt    = substr( $string, $start_postion, $max_length - 3 );
        $last_space = strrpos( $excerpt, ' ' );
        $excerpt    = substr( $excerpt, 0, $last_space );
        $excerpt .= '...';

        return $excerpt;
    }

}
