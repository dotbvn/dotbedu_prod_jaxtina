<?php
class logicRevenue{
    //After delete
    function deletedRevenue(&$bean, $event, $arguments){
        require_once("custom/include/_helper/junior_revenue_utils.php");
        $payment_id = $GLOBALS['db']->getOne("SELECT ju_payment_id FROM c_deliveryrevenue WHERE id = '{$bean->id}'");
        if(!empty($payment_id)){
            $payment = BeanFactory::getBean('J_Payment',$payment_id);
            if($payment->payment_type != 'Refund' && $payment->payment_type != 'Enrollment'){
                $q3 = "UPDATE j_payment SET remain_amount={$bean->amount}, remain_hours={$bean->duration} WHERE id='{$payment_id}' AND deleted=0";
                $GLOBALS['db']->query($q3);
            }

            if($payment->payment_type == 'Enrollment'){
                $q3 = "UPDATE j_payment SET paid_amount+={$bean->amount}, paid_hours+={$bean->duration} WHERE id='{$payment_id}' AND deleted=0";
                $GLOBALS['db']->query($q3);
            }
            getPaymentRemain($payment_id);
            header('Location: index.php?module=J_Payment&action=DetailView&record='. $payment_id);
        }
    }

    function handleSaveRevenue(&$bean, $event, $arguments){
        if($_POST['module'] == $bean->module_name && $_POST['action'] == 'Save'){
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: \'Something Wrong, Please, try again !!\',
            autoClose: true
            });
            window.parent.DOTB.App.router.redirect(\'#Home\');
            </script>';
        }
    }
}
?>
