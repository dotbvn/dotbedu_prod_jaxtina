<?php
$popupMeta = array (
    'moduleMain' => 'J_GroupUnit',
    'varName' => 'J_GroupUnit',
    'orderBy' => 'j_groupunit.name',
    'whereClauses' => array (
        'name' => 'j_groupunit.name',
    ),
    'searchInputs' => array (
        1 => 'name',
    ),
    'create' => array (
        'formBase' => 'GroupUnitFormBase.php',
        'formBaseClass' => 'GroupUnitFormBase',
        'getFormBodyParams' => array('','','J_GroupUnitSave'),
        'createButton' => 'LNK_NEW_RECORD',
    ),
    'searchdefs' => array (
        'name' =>
        array (
            'name' => 'name',
            'width' => 10,
        ),
    ),
    'listviewdefs' => array (
        'NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_NAME',
            'default' => true,
            'link' => true,
            'name' => 'name',
        ),
        'DESCRIPTION' =>
        array (
            'type' => 'text',
            'label' => 'LBL_DESCRIPTION',
            'sortable' => false,
            'width' => 10,
            'default' => true,
            'name' => 'description',
        ),
        'DATE_ENTERED' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_ENTERED',
            'width' => 10,
            'default' => true,
            'name' => 'date_entered',
        ),
    ),
);
