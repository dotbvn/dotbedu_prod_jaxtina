<?php
    class LogicGradebookSettingGroup{
        function handleSave(&$bean, $event, $arg) {
            if($bean->copy_gdsetting && $_POST['duplicateSave'] && !empty($_POST['duplicateId']) && $_POST['module'] == $bean->module_name){
                $q1 = "SELECT DISTINCT
                IFNULL(l1.id, '') gbsetting_id
                FROM j_gradebooksettinggroup
                INNER JOIN j_gradebookconfig l1 ON j_gradebooksettinggroup.id = l1.gbsettinggroup_id AND l1.deleted = 0
                WHERE (j_gradebooksettinggroup.id = '{$_POST['duplicateId']}') AND j_gradebooksettinggroup.deleted = 0";
                $rs1 = $GLOBALS['db']->query($q1);
                while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
                    $cp_gbst = BeanFactory::getBean('J_GradebookConfig', $row['gbsetting_id']);
                    $gbst = BeanFactory::newBean('J_GradebookConfig');
                    foreach ($gbst->field_defs as $keyField => $aFieldName)
                        $gbst->$keyField = $cp_gbst->$keyField;
                    $gbst->gbsettinggroup_id = $bean->id;
                    $gbst->gradebook_setting_name = '';
                    $gbst->id = '';
                    $gbst->save();

                }
            }
        }
    }
?>
