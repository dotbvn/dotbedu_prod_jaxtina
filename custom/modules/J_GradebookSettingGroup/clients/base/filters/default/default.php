<?php
// created: 2020-12-28 14:06:00
$viewdefs['J_GradebookSettingGroup']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);