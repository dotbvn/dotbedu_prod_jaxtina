<?php
$module_name = 'J_GradebookSettingGroup';
$viewdefs[$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'copy_gdsetting',
            'customLabel' => '{if $isDuplicate}{$MOD.LBL_COPY_GBSETTING}{/if}',
            'customCode' => '{if $isDuplicate}
            <input type="hidden" name="copy_gdsetting" value="0">
            <input type="checkbox" id="copy_gdsetting" {if $fields.copy_gdsetting.value}checked{/if} name="copy_gdsetting" value="1" title="{$MOD.LBL_COPY_GBSETTING}">{/if}',
          ),
        ),
        1 => 
        array (
          0 => 'description',
          1 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
