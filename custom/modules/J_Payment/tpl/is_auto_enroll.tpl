<link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/simptip.min.css}"/>
<fieldset id="auto-enroll-info" title="{$MOD.LBL_IS_AUTO_ENROLL}" class="fieldset-border" style="width: 90%; display: none;">
<table class="edit view edit508">
<tbody>
<tr>
<td valign="top" width="12.5%" scope="col">{$MOD.LBL_CLASS_NAME}:<span class="required">*</span></td>
<td valign="top" width="37.5%" nowrap>
<input type="text" readonly size="15" name="ju_class_name" id="ju_class_name" class="yui-ac-input" value="{$fields.ju_class_name.value}">
<input type="hidden" name="ju_class_id" id="ju_class_id" value="{$fields.ju_class_id.value}">
<input type="hidden" name="json_sessions" id="json_sessions" value="">
<input type="hidden" name="start_enroll_sgt" id="start_enroll_sgt" value="">
<input type="hidden" name="end_enroll_sgt" id="end_enroll_sgt" value="">
<span class="id-ff multiple">
<button type="button" name="btn_class_name" id="btn_class_name" class="button" class="button firstChild"><img src="themes/default/images/id-ff-select.png"></button>
<button type="button" name="btn_clr_class_name" style="margin-left: 0px;" id="btn_clr_class_name" class="button lastChild"><img src="themes/default/images/id-ff-clear.png"></button>
</span></td>
 <td valign="top" width="12.5%" scope="col">{$MOD.LBL_START_STUDY_EXP}: <span class="required">*</span></td>
<td valign="top" width="37.5%" nowrap>
<span class="dateTime">
<input class="date_input" size="10" autocomplete="off" type="text" name="start_study" id="start_study" value="{$fields.start_study.value}" title="{$MOD.LBL_START_STUDY_EXP}" tabindex="0" maxlength="10" style="vertical-align: top;">
<img src="themes/RacerX/images/jscalendar.png" alt="{$MOD.LBL_START_STUDY_EXP}" style="position:relative; top:0px" border="0" id="start_study_trigger" style="margin-top: 10px;">
</span>
</td>
</tr>
<tr>
 <td valign="top" width="12.5%" scope="col" style="height: 30px;">{$MOD.LBL_TOTAL_ENROLL_EXP}:</td>
<td valign="top" width="37.5%">
<b id='enroll_hours_label' style="color: #002bff;">{dotb_number_format var=$fields.enroll_hours.value precision=2}</b>
<input type="hidden" name="enroll_hours" id="enroll_hours" size="15" value="{dotb_number_format var=$fields.enroll_hours.value precision=2}">
</td>
 <td valign="top" width="12.5%" scope="col" style="height: 30px;">{$MOD.LBL_END_STUDY_EXP}:</td>
<td valign="top" width="37.5%" nowrap>
<b id='end_study_label' style="color: #002bff;">{$fields.end_study.value}</b>
<span class="dateTime" style="display:none;">
<input class="date_input" size="10" autocomplete="off" type="text" name="end_study" id="end_study" value="{$fields.end_study.value}" title="{$MOD.LBL_END_STUDY_EXP}" tabindex="0" maxlength="10" style="vertical-align: top;">
<img src="themes/RacerX/images/jscalendar.png" alt="{$MOD.LBL_END_STUDY_EXP}" style="position:relative; top:0px;" border="0" id="end_study_trigger" style="margin-top: 10px;">
</span>
</td>
</tr>
<tr>
 <td valign="top" width="12.5%" scope="col" style="height: 30px;">{$MOD.LBL_SESSIONS}:</td>
<td valign="top" width="37.5%">
<b id='sessions_label' style="color: #002bff;">{$fields.sessions.value}</b>
<input type="hidden" name="sessions" id="sessions" size="15" value="{$fields.sessions.value}">
</td>
 <td valign="top" width="12.5%" scope="col" style="height: 30px;">{$MOD.LBL_KIND_OF_COURSE}:</td>
<td valign="top" width="37.5%">
<b id='ju_class_koc_label'>{$ju_class_koc_label}</b>
<input type="hidden" name="ju_class_koc" id="ju_class_koc" size="15" value="{$ju_class_koc}">
<input type="hidden" name="ju_class_team_id" id="ju_class_team_id" size="15" value="{$ju_class_team_id}">
<input type="hidden" name="ju_class_team_name" id="ju_class_team_name" size="15" value="{$ju_class_team_name}">
</td>
</tr>
<tr>
<td valign="top" width="12.5%" scope="col" style="height: 30px;">{$MOD.LBL_ALLOW_OST}: <img style="vertical-align: middle;" border="0" onclick="return DOTB.util.showHelpTips(this,'{$MOD.LBL_ALLOW_OST_DES}');" src="themes/RacerX/images/helpInline.png"></td>
<td valign="top" width="37.5%">
<input type="hidden" name="is_allow_ost" value="0">
<input type="checkbox" id="is_allow_ost" {if $fields.is_allow_ost.value == 1}checked{/if} name="is_allow_ost" value="1" title="{$MOD.LBL_ALLOW_OST}">
</td>
<td valign="top" width="12.5%" scope="col" style="height: 30px;"></td>
<td></td>
</tr>
</tbody>
</table>
</fieldset>