<div id="dialog_sponsor" title="Add Sponsor" style="display:none;">
    <table id="table_sponsor" width="100%" class="list view">
        <thead>
            <!--<tr><td colspan="5">
            <button style="float: right;" class="button primary" type="button" id="btnAddSponsor">+</button>
            </td></tr> -->
            <tr>
                <th width="35%" style="text-align:center">{$MOD.LBL_SPONSOR_CODE}</th>
                <th width="20%" style="text-align:center">{$MOD.LBL_SPONSOR_TYPE}</th>
                <th width="20%" style="text-align:center">{$MOD.LBL_SPONSOR_AMOUNT}</th>
                <th width="20%" style="text-align:center">{$MOD.LBL_SPONSOR_PERCENT}</th>
                <th width="1%" style="text-align:center"></th>
                </tr>
        </thead>
<tbody id="tbodysponsor" style="height: 100%; width:100%; overflow:auto;">
        {$html_tpl_spon}
        {$html_spon}
        </tbody>
    </table><br>
    <table width="100%" style="font-size: 13px;">
<!--        <tr>
            <td width="45%" align="right"><span style="font-weight: bold;" class="loy_student_name"></span>:</td>
            <td colspan="2" style="padding-left: 10px;">{if $enable_loyalty}Membership Level <span class="loy_loyalty_mem_level"></span>{/if}</td>
        </tr> -->
        <tr>
            <td width="25%" align="right">1.</td>
            <td width="35%" align="right">{$MOD.LBL_AMOUNT_BEF_DISCOUNT}:</td>
            <td width="25%" align="right" class="sponsor_amount_bef_discount"></td>
            <td width="15%" align="left" scope="col"></td>
        </tr>
        <tr>
            <td align="right">2.</td>
            <td align="right">{$MOD.LBL_FINAL_SPONSOR}:</td>
            <td align="right" class="total_sponsor_amount"></td>
            <td align="left"></td>
        </tr>

        <tr>
            <td align="right">3.</td>
            <td align="right">{$MOD.LBL_FINAL_SPONSOR_PERCENT}:</td>
            <td align="right" class="total_sponsor_percent"></td>
            <input type="hidden" class="total_sponsor_percent_to_amount" value="">
            <td align="left"></td>
        </tr>

        <tr>
            <td align="right">4.</td>
            <td align="right">{$MOD.LBL_TOTAL_SPONSOR} = (2) + (1 - 2)x(3):</td>
            <td align="right" class="final_sponsor"></td>
            <td align="left"><input type="hidden" class="final_sponsor_percent" value=""></td>
        </tr>
    </table>
</div>
<!--ADD SOME CSS -->
{literal}
<style type="text/css" id="jstree-stylesheet">
.multiselect-search{
    text-transform: uppercase;
}
::-webkit-input-placeholder { /* WebKit browsers */
    text-transform: none;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    text-transform: none;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
    text-transform: none;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
    text-transform: none;
}
</style>
{/literal}