<table style="padding:0px!important;white-space: nowrap;">
    <tr>
        <td style="padding: 0px !important;">
            {html_options style="display:none;" name="parent_type" id="parent_type" options=$fields.parent_type.options selected=$fields.parent_type.value}
            <input accesskey="7" tabindex="0" type="text" class="input_readonly" name="parent_name" id="parent_name" maxlength="255" value="{$fields.parent_name.value}" size="25" readonly="">
        </td>
        <td>
            <input type="text" name="parent_id" id="parent_id" value="{$fields.parent_id.value}" style="display:none;">
            <input type="hidden" name="json_student_info" id="json_student_info" value="{$json_student_info}">
            <span class="id-ff multiple">
                <button type="button" id="btn_select_student" style="margin-right: -4px;" tabindex="0" title="{$APPS.LBL_ID_FF_SELECT}" class="button firstChild"><img src="themes/default/images/id-ff-select.png"></button>
                <button type="button" id="btn_clr_select_student" style="margin-right: -4px;" tabindex="0" title="{$APPS.LBL_ID_FF_CLEAR}" class="button lastChild"><img src="themes/default/images/id-ff-clear.png"></button>
            </span>
            <a id="eye_dialog_123" title="{$MOD.LBL_MORE_INFO}" style="cursor:pointer;"><img border="0" src="themes/RacerX/images/info_inline.png" style="margin-left:10px;margin-right:10px;"></a>
            {if $enable_loyalty}{$MOD.LBL_MEMBERSHIP_LEVEL}: <span class="loy_loyalty_mem_level"><label><span class="textbg_nocolor">N/A</span></label></span>{/if}
            <div id="dialog_student_info"></div>
        </td>
        <td id="std_td">

        </td>
    </tr>
</table>
