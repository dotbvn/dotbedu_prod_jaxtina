<script src="custom/modules/J_Payment/js/redeemVoucherTapTap.js"></script>

<h1>{$MOD.LBL_REDEEM_VOUCHER_TAPTAP}</h1><br>
<fieldset>
    <legend>
        <h3>Information</h3>
    </legend>
    <table width="100%" cellpadding="0" cellspacing="3" class="tabForm">
        <tr heigth="20">
            <td class="dataLabel">{$MOD.LBL_MOBILE_PHONE} : <span class="required">*</span></td>
            <td class="dataField"><input type="tel" name="mobile_phone" id="mobile_phone" value="" size="30"></td>
        </tr>
        <tr heigth="20">
            <td class="dataLabel">{$MOD.LBL_SPONSOR_CODE} : <span class="required">*</span></td>
            <td class="dataField"><input type="text" name="sponsor_code" id="sponsor_code" value="" size="30"></td>
        </tr>
        <tr heigth="20">
            <td class="dataLabel">{$MOD.LBL_TEAM} : <span class="required">*</span></td>
            <td class="dataField">{$htmlTeam}</td>
        </tr>
    </table>
    <button class="btn btn-primary" type="button" name="" id="btn_search" size="30">Search</button>
</fieldset>