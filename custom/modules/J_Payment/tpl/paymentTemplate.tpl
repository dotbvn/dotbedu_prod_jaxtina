{literal}
    <style type="text/css">
.alert-box {
    color: #555;
    border :1px solid;
    border-radius: 10px;
    font-family: Tahoma,Geneva,Arial,sans-serif;
    font-size: 11px;
    padding: 10px 36px;
    margin: 10px;
}
.alert-box span {
    font-weight: bold;
    text-transform: uppercase;
}
.warning {
    background: #fff8c4 url('images/warning.png') no-repeat 10px 50%;
    border-color: #f2c779;
}
.warning span {
    color: #9d9c49;
}
table.payment_detail >tbody>tr>td:first-child {
    text-align: right;
    padding-right: 20px;
}
    </style>
{/literal}

{dotb_getscript file="custom/include/javascript/Mask-Input/jquery.mask.min.js"}
<div class="diaglog_payment" title="{$MOD.LBL_DIALOG_RECEIPT}" style="display:none;">
    <table width="100%"  class="payment_detail">
        <tbody>
        <input type="hidden" id="dt_payment_detail_id" value>
        <input type="hidden" id="dt_status" value>
        <input type="hidden" id="dt_unpaid_amount" value>
        <input type="hidden" id="dt_handle_action" value>
            <tr style="height: 40px;">
                <td nowrap ><b> {$MOD_D.LBL_PAYMENT_METHOD}:</b> <span class="required">*</span> </td>
                <td nowrap style="color: blue;font-weight: bold" colspan="3" id="dt_payment_method">
                {html_options name="payment_method" id="payment_method" class="selectpicker" title=$MOD.LBL_PAYMENT_METHOD options=$payment_method selected=''}
                {html_options style="display: none; width:120px;" name="card_type" id="card_type" title=$MOD.LBL_CARD_TYPE options=$card_type selected=''}
                <input style="display: none;  width:110px;" type="text" name="method_note" id="method_note" size="20" maxlength="100" title="{$MOD.LBL_METHOD_NOTE}">
                </td>
            </tr>
            <tr style="height: 40px;" style="display: none;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD.LBL_BANK_ACCOUNT}:</b></td>
                <td nowrap >
                {html_options style="display: none; width:120px;" name="bank_type" id="bank_type" title=$MOD_D.LBL_BANK_NAME options=$bank_type selected=''}
                {html_options name="bank_account" id="bank_account" title=$MOD_D.LBL_BANK_ACCOUNT options=$bank_receive_list selected=$fields.bank_account.value}
                </td>
            </tr>
            <tr style="height: 40px;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD.LBL_INV_CODE}:</b> <span class="required">*</span></td>
                <td nowrap >
                <input class="no-align" size="20" type="text" title="{$MOD.LBL_INV_CODE}" name="inv_code" id="inv_code" style="font-weight: bold; color: rgb(165, 42, 42); text-transform: uppercase;"/>
                </td>
            </tr>
            <tr style="height: 40px;" style="display: none;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD_D.LBL_POS_CODE}:</b> <span class="required">*</span></td>
                <td nowrap >
                <input class="no-align" size="20" type="text" title="{$MOD_D.LBL_POS_CODE}" name="pos_code" id="pos_code" style="font-weight: bold; color: rgb(165, 42, 42); text-transform: uppercase;"/>
                </td>
            </tr>
            <tr style="height: 40px;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD_D.LBL_PAYMENT_AMOUNT}:</b> <span class="required">*</span></td>
                <td nowrap >
                    <input class="currency no-align {if !$can_edit_pmd_amount}input_readonly{/if}" size="20" type="text" title="{$MOD_D.LBL_PAYMENT_AMOUNT}" name="dt_payment_amount" {if !$can_edit_pmd_amount}readonly{/if} id="dt_payment_amount" style="font-weight: bold; color: rgb(165, 42, 42); text-align: left;"/>
                </td>
            </tr>
            <tr style="height: 40px;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD_D.LBL_PAYMENT_DATE}:</b> <span class="required">*</span></td>
                <td nowrap ><span class="dateTime"><input readonly class="input_readonly" title="{$MOD_D.LBL_PAYMENT_DATE}" name="payment_date_collect" size="10" id="payment_date_collect" type="text" value="{$today}">  {if $lock_receipt_date == 0}<img border="0" src="custom/themes/default/images/jscalendar.png" alt="Enter Date" title="Enter Date" id="payment_date_trigger" align="absmiddle"></span>{/if}</td>
            </tr>
            <tr style="height: 40px;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD.LBL_MONEY_RECEIVER}:</b></td>
                <td nowrap ><span id='dt_receiver'></span></td>
            </tr>
            <tr style="height: 40px;"><td nowrap colspan="2"><hr></td></tr>
            <tr style="height: 40px;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD_D.LBL_DESCRIPTION}:</b></td>
                <td nowrap >
                <textarea id="dt_description" title="{$MOD_D.LBL_DESCRIPTION}" name="dt_description" rows="2" cols="30" tabindex="0"></textarea></td>
            </tr>
            <tr style="height: 40px;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD_D.LBL_NOTE}:</b></td>
                <td nowrap >
                <textarea id="dt_note" title="{$MOD_D.LBL_NOTE}" name="dt_note" rows="2" cols="30" tabindex="0"></textarea></td>
            </tr>
<!--            <tr style="height: 40px;">
                <td nowrap style="vertical-align: middle; width: 150px;"><b>{$MOD.LBL_REFERENCE_DOCUMENT}:</b></td>
                <td nowrap ><input title="{$MOD.LBL_REFERENCE_DOCUMENT}" size="10" type="text" name="dt_reference_document" id="dt_reference_document">
                <b>{$MOD.LBL_REFERENCE_NUMBER}</b>
                <input title="{$MOD.LBL_REFERENCE_NUMBER}" size="3" type="text" name="dt_reference_number" id="dt_reference_number">
                </td>
            </tr>-->
        </tbody>
    </table>
</div>
