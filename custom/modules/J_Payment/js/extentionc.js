$(document).ready(function() {
    displaySplitPayment(false);
    $('#number_of_payment, .foc_type').live('change',function(){
        displaySplitPayment(true);
    });

    toggleIsCorporate();
    $('#is_corporate').on('change',function(){
        $('#btn_clr_account_name').trigger('click');
        toggleIsCorporate();
    });
    $('#btn_account_name').live('click',function(){
        open_popup("Accounts", 1000, 700, "", true, true, {
            "call_back_function": "set_return_corp",
            "form_name": "EditView",
            "field_to_name_array": {
                "id": "account_id",
                "name": "account_name",
                "billing_address_street": "company_address",
                "tax_code": "tax_code",
            },
            }, "Select", true);
    });

    $('#btn_clr_account_name').live('click',function(){
        $('#account_id, #account_name, #company_name, #company_address, #tax_code').val('');
    });

    $("input[id^=payment_amount_]").live('blur',function(){
        autoGeneratePayment();
    });

    $("#payment_type").on("change",function(){
        displaySplitPayment(true);
    });
});

function autoGeneratePayment(){
    var number_of_payment 	= $('#number_of_payment').val();
    var grand_total         = Numeric.parse($("#payment_amount").val());
    var discount_amount     = Numeric.parse($("#discount_amount").val());
    var loyalty_amount      = Numeric.parse($("#loyalty_amount").val());
    var final_sponsor       = Numeric.parse($("#final_sponsor").val());

    var payment_amount_1    = Numeric.parse($("#payment_amount_1").val());
    var payment_amount_2    = Numeric.parse($("#payment_amount_2").val());
    var payment_amount_3    = Numeric.parse($("#payment_amount_3").val());
    var payment_amount_4    = Numeric.parse($("#payment_amount_4").val());
    var payment_amount_5    = Numeric.parse($("#payment_amount_5").val());


    switch (number_of_payment){
        case '1':
        case 'Custom':
            payment_amount_1    = grand_total;
            if(payment_amount_1 < 0) payment_amount_1 = 0;
            $("#payment_amount_1").val(Numeric.toFloat(payment_amount_1));
            break;
        case '2':
            if(payment_amount_1 == 0) return ;
            payment_amount_2        = grand_total - payment_amount_1;
            if(payment_amount_2 < 0) payment_amount_2 = 0;
            $("#payment_amount_2").val(Numeric.toFloat(payment_amount_2));
            break;
        case '3':
            if(payment_amount_1 == 0 || payment_amount_2 == 0) return ;
            payment_amount_3 = grand_total - payment_amount_1 - payment_amount_2;
            if(payment_amount_3 < 0) payment_amount_3 = 0;
            $("#payment_amount_3").val(Numeric.toFloat(payment_amount_3));
            break;
        case '4':
            if(payment_amount_1 == 0 || payment_amount_2 == 0 || payment_amount_3 == 0) return ;
            payment_amount_4 = grand_total - payment_amount_1 - payment_amount_2 - payment_amount_3;
            if(payment_amount_4 < 0) payment_amount_4 = 0;
            $("#payment_amount_4").val(Numeric.toFloat(payment_amount_4));
            break;
        case '5':
            if(payment_amount_1 == 0 || payment_amount_2 == 0 || payment_amount_3 == 0 || payment_amount_4 == 0) return ;
            payment_amount_5 = grand_total - payment_amount_1 - payment_amount_2 - payment_amount_3 - payment_amount_4;
            if(payment_amount_5 < 0) payment_amount_5 = 0;
            $("#payment_amount_5").val(Numeric.toFloat(payment_amount_5));
            break;
    }
    //Lock Assigned to
    if($('#lock_assigned_to').val() == 1){
        $('#assigned_user_name').prop('readonly',true).addClass('input_readonly');
        $('#btn_assigned_user_name, #btn_clr_assigned_user_name').prop('disabled',true);
    }
}

// Ẩn hiện fieldset split payment
function displaySplitPayment(clear){
    //Clear Payment Amount
    var payment_date = $('#payment_date').val();
    var number_of_payment = $('#number_of_payment').val();

    if(clear)
        $("input[id^=payment_amount_]").val('');

    $("table[id^=tbl_split_payment_]").hide();

    if(number_of_payment == 'Custom'){
        number_of_payment = '1';
    }

    for(i = 1; i <= number_of_payment; i++ ){
        if(number_of_payment == '1') $('#payment_amount_1').prop('readonly',true).addClass('input_readonly');
        else $('#payment_amount_1').prop('readonly',false).removeClass('input_readonly');

        if($('#pay_dtl_invoice_date_'+i).val() == '')  //In Case Create
            $('#pay_dtl_invoice_date_'+i).val(payment_date).effect("highlight", {color: '#ff9933'}, 1000);

        $('#tbl_split_payment_'+i).show();
    }
    autoGeneratePayment();
}
// Ẩn hiện thông tin corporate
function toggleIsCorporate(){
    if($('#is_corporate').is(':checked')){
        $('#vat-corp-info').slideDown('fast');
        addToValidateBinaryDependency('EditView', 'account_name', 'alpha', true, DOTB.language.get('app_strings', 'ERR_SQS_NO_MATCH_FIELD') + DOTB.language.get('J_Payment','LBL_ACCOUNT_ID') , 'account_id' );
        addToValidate('EditView', 'company_name', 'text', true, DOTB.language.get('J_Payment','LBL_COMPANY_NAME'));
        addToValidate('EditView', 'tax_code', 'text', true, DOTB.language.get('J_Payment','LBL_TAX_CODE'));
        addToValidate('EditView', 'company_address', 'text', true, DOTB.language.get('J_Payment','LBL_COMPANY_ADDRESS'));
    }else{
        $('#vat-corp-info').slideUp('fast');
        removeFromValidate('EditView','company_name');
        removeFromValidate('EditView','tax_code');
        removeFromValidate('EditView','company_address');
        removeFromValidate('EditView','account_name');
    }
}
// Overwirite set_return Parent Type
function set_return_corp(popup_reply_data){
    $('#company_name_temp, #company_id_temp, #company_name, #company_address, #tax_code').val('');
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'account_id':
                    $('#account_id').val(val);
                    break;
                case 'account_name':
                    $('#account_name, #company_name').val(val);
                    break;
                case 'company_address':
                    $('#company_address').val(val);
                    break;
                case 'tax_code':
                    $('#tax_code').val(val);
                    break;
            }
        }
    }
}


//Overwrite check_form to validate
function check_form(formname) {
    //Validate sum amount of split payments
    var payment_amount          = Numeric.parse($('#payment_amount').val());
    var payment_amount_1        = Numeric.parse($('#payment_amount_1').val());
    var payment_amount_2        = Numeric.parse($('#payment_amount_2').val());
    var payment_amount_3        = Numeric.parse($('#payment_amount_3').val());
    var payment_amount_4        = Numeric.parse($('#payment_amount_4').val());
    var payment_amount_5        = Numeric.parse($('#payment_amount_5').val());
    var number_of_payment       = $('#number_of_payment').val();
    var payment_type            = $('#payment_type').val();

    if  (((payment_amount_1 + payment_amount_2 + payment_amount_3 + payment_amount_4 +payment_amount_5 ) != payment_amount) && number_of_payment != "Custom") {
        var mes = DOTB.language.get('J_Payment', 'LBL_ALERT_SUM_SPLIT');
        toastr.error(mes);
        $('#payment_amount_1, #payment_amount_2, #payment_amount_3, #payment_amount_4, #payment_amount_5').effect("highlight", {color: '#FF0000'}, 3000);
        return false;
    }

    if(( payment_type == 'Deposit' || payment_type == 'Other') && payment_amount <= 0  ){
        toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_INVALID_AMOUNT'));
        $('#payment_amount').effect("highlight", {color: '#FF0000'}, 3000);
        return false;
    }
    //check book gift in course-free - Tạo
    if(payment_type == 'Book/Gift'){
        var over_stock = count_item = 0
        var text_os = '';
        var left_items = {};

        $("#tbodybook>tr:first").find('select option').each(function(ind, option) {
            var quantity = $(this).attr('quantity');
            var book_id = $(this).val();
            if (book_id != '' && book_id != null && typeof book_id != 'undefined')
                left_items[book_id] = Number(quantity);
        });
        var remain_items = left_items;

        $('#tbodybook tr:not(:first-child)').each(function (index) {
            var book_id = $(this).find('select option:selected').val();
            var book_name = $(this).find('select option:selected').text();
            var input_quantity = $(this).find('input.book_quantity').val();
            if(book_id != ''){
                remain_items[book_id] -= Number(input_quantity);
                if(remain_items[book_id] < 0){
                    over_stock ++;
                    text_os += '<li>'+ book_name+'</li>';
                }
                count_item +=input_quantity;
            }
        });


        if(over_stock > 0) {
            $.confirm({
                title: DOTB.language.get('J_Payment', 'LBL_ALERT_OUT_OF_STOCK1'),
                content: text_os + '<br>' + DOTB.language.get('J_Payment', 'LBL_ALERT_OUT_OF_STOCK3') ,   //Không cho xuất âm kho
                buttons: {
                    "OK": {
                        btnClass: 'btn-blue',
                        action: function(){

                            /* if (validate_form(formname, '')){
                            ajaxStatus.showStatus('Saving...');
                            var _form = document.getElementById('EditView');
                            _form.action.value = 'Save';
                            DOTB.ajaxUI.submitForm(_form);
                            return false;
                            }    */
                        }
                    },
                    "Cancel": {
                        text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                        action: function(){

                        }
                    },
                }
            });
            return false;
        }
        if(count_item <= 0) {
            toastr.error(app.lang.get('LBL_ERROR_QUANTITY', 'J_Payment'));
            return false;
        }
    }

    if (validate_form(formname, '')){
        DOTB.ajaxUI.showLoadingPanel();
        return true;
    }else return false;
}