$(document).ready(function () {
    $('#btn_search').click(function () {
        var mobile_phone = $('#mobile_phone').val();
        var sponsor_code = $('#sponsor_code').val();
        var center_code = $('#center_code').val();

        if (mobile_phone != '' && sponsor_code != '' && center_code != '') {
            $('#btn_search').attr('disabled', 'disabled');
            DOTB.ajaxUI.showLoadingPanel();
            //Ajax check Sponsor code
            $.ajax({
                url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
                type: "POST",
                async: false,
                data: {
                    type: 'ajaxCheckVoucherFreeEventTAPTAP',
                    mobile_phone: mobile_phone,
                    sponsor_code: sponsor_code,
                    center_code: center_code,
                },
                dataType: "json",
                success: function (res) {
                    DOTB.ajaxUI.hideLoadingPanel();
                    $('#btn_search').removeAttr('disabled');
                    if (res.success == "1") {
                        var discount_amount = '<br>Discount: ' + res.discount_amount;
                        var discount_percent = '<br>Discount %: ' + res.discount_percent;
                        if (res.discount_amount == 0) discount_amount = '';
                        if (res.discount_percent == 0) discount_percent = '';

                        var description = ''
                        if (res.description != '') description = ' (' + res.description + ')';
                        var content = 'Sponsor: ' + res.sponsor_number + res.status_color + '<br>' + description + '<br>Expires: ' + res.end_date + discount_amount + discount_percent + '<br>Total Redemption: ' + res.used_time;
                        if(res.status == "Expired"){
                            $.alert(content);
                        }else showPopupApplyVoucherTAPTAP(content, mobile_phone, sponsor_code, center_code);
                    } else if (res.success == "0") {
                        $.alert(app.lang.get('LBL_SPONSOR_NOT_FOUND','J_Payment'));
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    DOTB.ajaxUI.hideLoadingPanel();
                    $('#btn_search').removeAttr('disabled');
                    toastr.error(app.lang.get('LBL_REDEEM_VOUCHER_ERROR_1','J_Payment'));
                },
            });
            //END
        } else {
            if (center_code == '') {
                toastr.error(app.lang.get('LBL_REDEEM_VOUCHER_ERROR_2','J_Payment'));
                $('#center_code').effect("highlight", { color: 'red' }, 2000);
            } else {
                toastr.error(app.lang.get('LBL_REDEEM_VOUCHER_ERROR_3','J_Payment'));
                if (mobile_phone == '') $('#mobile_phone').effect("highlight", { color: 'red' }, 2000);
                if (sponsor_code == '') $('#sponsor_code').effect("highlight", { color: 'red' }, 2000);
            }
        }
    });
});

function showPopupApplyVoucherTAPTAP(content, mobile_phone, sponsor_code, center_code) {
    $.confirm({
        title: DOTB.language.get('J_Payment', 'LBL_CONFIRM_ALERT_TITLE'),
        content: content,
        buttons: {
            "Apply": {
                btnClass: 'btn-blue',
                action: function () {
                    DOTB.ajaxUI.showLoadingPanel();
                    $.ajax({
                        type: "POST",
                        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
                        data: {
                            type: "ajaxRedeemVoucherFreeEventTAPTAP",
                            mobile_phone: mobile_phone,
                            sponsor_code: sponsor_code,
                            center_code: center_code,
                        },
                        dataType: "json",
                        success: function (data) {
                            if (data.success == "1") {
                                toastr.success(app.lang.get('LBL_REDEEM_VOUCHER_SUCCESS','J_Payment'));
                            } else
                                toastr.error(app.lang.get('LBL_REDEEM_VOUCHER_ERROR_4','J_Payment') + data.message);
                            DOTB.ajaxUI.hideLoadingPanel();
                        },

                    });
                },
                text  : DOTB.language.get('J_Payment','LBL_BUTTON_APPLY'),
            },
            "Cancel": {
                text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                action: function () {

                }
            },
        }
    });
}