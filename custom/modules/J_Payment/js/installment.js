
$(document).ready(function() {
    //Nếu installment_plan có giá trị thì không cho đổi Receipt
    if($('#installment_plan').val() != '') {
        $('[name="pay_dtl_amount[]"]').prop('readonly', true).addClass('input_readonly');
        $('#number_of_payment').find('option').prop("disabled", true);
        $('#number_of_payment option:selected').prop("disabled", false);
    }


    $("#payment_type").live('change',function(){
        var payment_type = $(this).val();
        if(payment_type != 'Cashholder'){
            if($('#is_installment').is(':checked'))
                $('#is_installment').trigger('click');
            $('#is_installment').closest('tr').hide();
        }else{
            $('#is_installment').closest('tr').show();
        }
        //Check loylaty
        setLoyaltyLevel();
    });

    //xu li installment
    $("#is_installment").live('change',function(){
        if($(this).is(':checked')){
            $('[name="pay_dtl_amount[]"]').prop('readonly',true).addClass('input_readonly');
            $('#installment_plan').show();
            addToValidate('EditView', 'installment_plan', 'enum', true, DOTB.language.get('J_Payment', 'LBL_IS_INSTALLMENT'));
        }
        else{
            $('[name="pay_dtl_amount[]"]').prop('readonly',false).removeClass('input_readonly');
            $('#number_of_payment').find('option').prop("disabled", false);
            $('#number_of_payment').val('1').trigger('change');
            $('#installment_plan').hide().val('');
            removeFromValidate('EditView', 'installment_plan');
        }
        caculated();
        submitDiscount();
        caculated();
        //Check loylaty
        setLoyaltyLevel();

    });
});
