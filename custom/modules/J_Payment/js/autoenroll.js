$(document).ready(function() {
    if(action_dotb_grp1 == 'EditView'){
        //Enrollment new rule
        toggleIsAutoEnroll();
        $('#is_auto_enroll').live('change',function(){
            toggleIsAutoEnroll();
        });
    }


    //Open Dialog Auto Enroll - DETAILVIEW
    if(action_dotb_grp1 == 'DetailView'){
        $('#btn_auto_enroll, #btn_edit_auto_enroll').live('click',function(){
            $('#auto-enroll-info').dialog({
                resizable: false,
                width:'800',
                height:'auto',
                modal: true,
                visible: true,
                buttons: {
                    "Help":{
                        click:function() {
                            $.alert(DOTB.language.get('J_Payment','LBL_IS_AUTO_ENROLL_HELP'));
                        },
                        class:'button',
                        text: DOTB.language.get('J_Payment','LBL_HELP'),
                    },
                    "Save":{
                        click:function() {
                            var res = ajaxSaveAutoEnroll(1);
                            if(res) $('#auto-enroll-info').dialog('close');
                        },
                        class: 'button primary',
                        text: DOTB.language.get('J_Payment','LBL_SAVE'),
                    },
                    "Cancel":{
                        click:function() {
                            $(this).dialog('close');
                        },
                        class:'button',
                        text: DOTB.language.get('J_Payment','LBL_CANCEL'),
                    },
                },
                beforeClose: function(event, ui) {
                    $("body").css({ overflow: 'inherit' });
                },
            });
            ajaxLoadSessions();
        });

        //IN-Active
        $('#btn_deactive_auto_enroll').live('click',function(){
            $.confirm({
                title: DOTB.language.get('J_Payment','LBL_CONFIRM_ALERT_TITLE'),
                content: DOTB.language.get('J_Payment','LBL_CONFIRM_UNSET_AUTO_ENROLL'),
                buttons: {
                    "OK": {
                        btnClass: 'btn-blue',
                        action: function(){
                            ajaxSaveAutoEnroll(0);
                        }
                    },
                    "Cancel": {
                        text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                        action: function(){
                            //do nothing
                        }
                    },
                }
            });
        });


    }

    $('#btn_class_name').live('click',function(){
        open_popup("J_Class", 1000, 700, "", true, true, {
            "call_back_function": "set_return_class",
            "form_name": "EditView",
            "field_to_name_array": {
                "id": "ju_class_id",
                "name": "ju_class_name",
                "kind_of_course": "ju_class_koc",
            },
            }, "Select", true);
    });

    $('#end_study, #enroll_hours, #ju_class_koc, #sessions').live('change',function(){
        $('#'+$(this).attr('name')+'_label').text($(this).val());
    });

    $('#start_study').live('change',function(){
        calSession();
    });
    $('#btn_clr_class_name').live('click',function(){
        $('#ju_class_id, #ju_class_name, #ju_class_koc, #json_sessions, #start_study, #end_study, #enroll_hours, #sessions').val('').trigger('change');
    });
});
function ajaxSaveAutoEnroll(active){
    var payment_id  = $('input[name=record]').val();
    var class_id    = $("#ju_class_id").val();
    var start_study = $('#start_study').val();
    var end_study   = $('#end_study').val();
    var enroll_hours= $('#enroll_hours').val();
    var sessions    = $('#sessions').val();
    var is_allow_ost= ($('#is_allow_ost').is(":checked")) ? 1 : 0;
    if(payment_id == '' || class_id == '' || start_study == '' || end_study == '') {
        toastr.error(DOTB.language.get('J_Payment', 'LBL_FILLUP_ERR'));
        return false;
    }
    DOTB.ajaxUI.showLoadingPanel();
    $.ajax({
        type:"POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data:{type     : "ajaxSaveAutoEnroll",
            payment_id : payment_id,
            class_id   : class_id,
            start_study: start_study,
            end_study  : end_study,
            enroll_hours  : enroll_hours,
            is_allow_ost  : is_allow_ost,
            sessions      : sessions,
            active        : active,
        },
        dataType: "json",
        success:function(data){
            DOTB.ajaxUI.hideLoadingPanel();
            if (data.success == "1") {
                $.each(data.enr_success, function( index, value ){ toastr.success(value);});
                $.each(data.enr_fail, function( index, value ){ toastr.warning(value);});
                setTimeout(function() {location.reload();}, 3000);
            }else
                toastr.error(data.error);
        },
    });
    return true;
}
// Ẩn hiện thông tin auto-enroll
function toggleIsAutoEnroll(){
    if($('#is_auto_enroll').is(':checked')){
        $('#auto-enroll-info').slideDown('fast');
        addToValidateBinaryDependency('EditView', 'ju_class_name', 'alpha', true, DOTB.language.get('app_strings', 'ERR_SQS_NO_MATCH_FIELD') + DOTB.language.get('J_Payment','LBL_CLASS_NAME') , 'ju_class_id' );
        addToValidate('EditView', 'start_study', 'date', true, DOTB.language.get('J_Payment','LBL_START_STUDY'));
        ajaxLoadSessions();
    }else{
        $('#auto-enroll-info').slideUp('fast');
        removeFromValidate('EditView','ju_class_name');
        removeFromValidate('EditView','start_study');
    }
}
// Overwirite set_return Parent Type
function set_return_class(popup_reply_data){
    $('#btn_clr_class_name').trigger('click');//clear
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'ju_class_id':
                    $('#ju_class_id').val(val);
                    break;
                case 'ju_class_name':
                    $('#ju_class_name').val(val);
                    break;
                case 'ju_class_koc':
                    $('#ju_class_koc').val(val).trigger('change');
                    break;
            }
        }
    }
    //Ajax cal schedule json
    ajaxLoadSessions();
}
function ajaxLoadSessions(){
    var parent_id   = $('#parent_id').val();
    var parent_type = $('#parent_type').val();
    var payment_type = $('#payment_type').val();
    var class_id    = $("#ju_class_id").val();
    var team_id     = $('input[name=team_id]').val();
    if(parent_id == '' || class_id == '')  return false;
    $.ajax({
        type:"POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data:{type      : "ajaxLoadSessions",
            parent_id   : parent_id,
            parent_type : parent_type,
            class_id    : class_id,
            team_id     : team_id,
            team_id     : team_id,
            payment_type: payment_type,
        },
        dataType: "json",
        success:function(data){
            if (data.success == "1") {
                $('#json_sessions').val(JSON.stringify(data.sessions));
                $('#start_enroll_sgt').val(data.start_enroll_sgt);
                $('#end_enroll_sgt').val(data.end_enroll_sgt);
                calSession();
            }else{
                $('#btn_clr_class_name').trigger('click');//clear
                toastr.error(data.error);
            }

        },
    });
}
function calSession(){
    var start_enroll = $('#start_enroll_sgt').val(); //last study (Type = Enrolled)
    var end_enroll   = $('#end_enroll_sgt').val(); //last study (Type = Enrolled)
    if($('#start_study').val() != '') start_enroll = DOTB.util.DateUtils.formatDate(DOTB.util.DateUtils.parse($('#start_study').val(),cal_date_format),false,"Y-m-d"); //Convert to DB date
    var total_hours  = Numeric.parse($('input[name=total_hours]').val());
    var class_id     = $("#ju_class_id").val();
    var enroll_hours = countss = 0;
    if(start_enroll == '' || typeof start_enroll == 'undefined') return ;

    var json_sessions = $('#json_sessions').val();
    if(json_sessions == '') return ;
    obj = JSON.parse(json_sessions);
    $.each(obj, function( key, value ) {
        if(value.date_start >= start_enroll){
            enroll_hours += parseFloat(value.delivery_hour);
            if(enroll_hours.toFixed(2) > total_hours && total_hours > 0){
                enroll_hours -= parseFloat(value.delivery_hour);
                return false;  //break
            }
            countss++;
            end_enroll = value.date_start;
        }

    });
    if(countss > 0){   //is existing session remain
        if(start_enroll != '') $('#start_study').val(DOTB.util.DateUtils.formatDate(new Date(start_enroll)));
        if(end_enroll != '') $('#end_study').val(DOTB.util.DateUtils.formatDate(new Date(end_enroll))).trigger('change');
        $('#sessions').val(countss).trigger('change');
        $('#enroll_hours').val(Numeric.toFloat(enroll_hours,2,2)).trigger('change');
    }else{
        $('#sessions, #enroll_hours, #end_study').val('').trigger('change');
        $('#start_study').val('');
        toastr.error(DOTB.language.get('J_Payment','LBL_ERR_NO_SESSION'));
    }
}

// Setup date field
Calendar.setup ({
    inputField : "start_study",
    daFormat : cal_date_format,
    button : "start_study_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
    }
);
Calendar.setup ({
    inputField : "end_study",
    daFormat : cal_date_format,
    button : "end_study_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
    }
);