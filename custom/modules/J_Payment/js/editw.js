var record_id       = $('input[name=record]').val();
var student_id      = $('#parent_id').val();
var payment_type_begin    = $('#payment_type').val();
var book_index      = 1;
var lock_coursefee = false;
var duplicate_id = $('input[name=duplicateId]').val();
$(document).ready(function() {
    $('#payment_date').on('change',function(){
        var rs3 = checkDataLockDate($(this).attr('id'));
    });
    //ngan chan loi load trang lien tuc
    if(payment_type_begin == '0' || payment_type_begin == 'Array' || payment_type_begin == '' || typeof payment_type_begin == 'undefined')
        location.reload();
    //Lock Team
    $( "#tab_discount" ).tabs({
        activate: function( event, ui ) {
            var newGroup = ui.newPanel.attr('id');
            var oldGroup = ui.oldPanel.attr('id');
            $('.'+newGroup).show();
            $('.'+oldGroup).hide();
        }
    });
    $( "#tab_discount" ).show();
    //Change Team Action - Fix bug Mutiple User
    $('#btn_team_id').click(function(){
        open_popup('Teams', 1000, 700, "", true, false, {"call_back_function":"set_team_return","form_name":"EditView","field_to_name_array":{"id":"team_id","name":"team_name"}}, "single", true);
    });

    $('#btn_clr_team_id').click(function(){
        $('#team_name,#team_id').val('');
        ajaxGetStudentInfo();
    });

    //Ẩn Option thu tiền theo theo tháng
    $("#number_of_payment").live('change',function(){
        if($(this).val() == 'Custom'){
            $('.custom_split').show();
            addToValidateRange('EditView', 'num_month_pay', 'int', true, DOTB.language.get('J_Payment', 'LBL_NUM_MONTH_PAY'),1,99);
            addToValidateRange('EditView', 'repeat_every_pay', 'int', true, DOTB.language.get('J_Payment', 'LBL_REPEAT_EVERY'),1,999);
            if($('#repeat_every_pay').val() == '') $('#repeat_every_pay').val('1');
            if($('#num_month_pay').val() == '') $('#num_month_pay').val('12');
        }else{
            $('.custom_split').hide();
            removeFromValidate('EditView','num_month_pay');
            removeFromValidate('EditView','repeat_every_pay');
        }
    });
    $("#number_of_payment").trigger('change');

    $('#table_sponsor').multifield({
        section :   '.row_tpl_sponsor', // First element is template
        addTo   :   '#tbodysponsor', // Append new section to position
        btnAdd  :   '#btnAddSponsor', // Button Add id
        btnRemove:  '.btnRemoveSponsor', // Buton remove id
    });
    if($('#parent_id').val() == '') $("#parent_type").show();
    $("#parent_type").live('change',function(){
        $('#parent_name_label').html($(this).find('option:selected').text()+': <span class="required">*</span>');
        $('#std_td').html('');
        if($(this).val() != 'J_Class')
            removeFromValidate('EditView','std_student_list');
    });
    $("#parent_type").trigger('change');
    //Open Popup
    $('#btn_select_student').click(function(){
        open_popup($('#parent_type').val(), 1000, 700, "", true, false, {"call_back_function":"set_contact_return","form_name":"EditView","field_to_name_array":{"id":"parent_id","name":"parent_name"}}, "single", true);
    });

    $('#btn_clr_select_student').click(function(){
        $('#parent_id,#parent_name,#json_student_info').val('');
        $('#std_td').html('');
        $('#parent_type').show();
        $('#tbodypayment').html('<tr><td colspan="100%" style="text-align: center;">'+DOTB.language.get('J_Payment','LBL_ALERT_NO_PAYMENT_LIST')+'</td></tr>');
        showDialogStudent();
    });

    $('#eye_dialog_123').live('click',function(){
        showDialogStudent(true);
    });

    //FIELD DISCOUNT AND GET DISCOUNT
    $('#btn_get_loyalty').live('click',function(){
        calLoyalty();
        showDialogLoyalty();
    });
    $("input#loy_loyalty_points").keyup(function(e){
        calLoyalty();
    });

    $("input#loy_loyalty_points").live('change',function(){
        if($(this).val() < 0) $(this).val(0);
    });

    $('input.currency').live('change',function(){
        check_currency(this);
    });

    $('#btn_get_discount').live('click',function(){
        showDialogDiscount();
        calDiscount();
    });
    $('#btn_add_sponsor').live('click',function(){
        calSponsor();
        showDialogSponsor();
    });
    //Click table rows to select checkbox
    $('#table_discount tr').click(function(event) {
        if ($(this).hasClass("locked")) return;
        if (event.target.type !== 'checkbox' && event.target.type !== 'select-one'){
            $(':checkbox', this).trigger('click');
        }
    });
    $('.dis_check, input.dis_hours').live('change',function(){
        calDiscount();
    });
    //Live Change Course Fee
    generateCourseFee();
    $('#coursefee').live('change',function(){
        clearDiscount();
        generateCourseFee();
    });
    addToValidate('EditView', 'j_coursefee_j_payment_1j_coursefee_ida[]', 'multienum', true,'Course Fee' );
    //HANDLE Payment
    $('#payment_date').live('change',function(){
        var number_of_payment   = $('#number_of_payment').val();
        var payment_date       = $('#payment_date').val();
        for(i = 1; i <= number_of_payment; i++ ){
            if($('#pay_dtl_invoice_date_'+i).val() == '' || record_id == '')  //In Case Create
                $('#pay_dtl_invoice_date_'+i).val(payment_date).effect("highlight", {color: '#ff9933'}, 1000);
        }
    });

    $('.pay_check').live('change',function(){
        var use_type = $(this).closest('tr').find('.use_type').val();
        var pay_checked = $(this);
        if(use_type == "Hour" && $(this).is(":checked")){
            $.confirm({
                title: DOTB.language.get('J_Payment', 'LBL_CONFIRM_ALERT_TITLE'),
                content: DOTB.language.get('J_Payment', 'LBL_ALERT_USE_TYPE'),
                buttons: {
                    "OK": {
                        btnClass: 'btn-blue',
                        action: function(){
                            calculatedAll();
                        }
                    },
                    "Cancel": {
                        action: function(){
                            pay_checked.prop('checked',false);
                        }
                    },
                }
            });
        }else{
            calculatedAll();
        }
    });
    $('.c_package_custom').live('change',function(){
        $(this).val(Numeric.toFloat($(this).val(),2,2));
    });

    $('.c_package_hours, .c_package_select, .c_package_custom').live('change',function(){
        // New rule calculate Courses
        calCourseFee();
        calculatedAll();
    });
    switchPaymentType();
    $('#payment_type').on('change',function(){
        $('.pay_check:checked').prop('checked',false).trigger('change');
        switchPaymentType();
    });

    $('#tblbook').multifield({
        section :   '.row_tpl', // First element is template
        addTo   :   '#tbodybook', // Append new section to position
        btnAdd  :   '#btnAddrow', // Button Add id
        btnRemove:  '.btnRemove', // Buton remove id
        prompt:  false, // prompt
    });
    s2_tblbook();

    $('.book_quantity, .book_price, #is_free_book').live('change',function(){
        calBookPayment();
    });
    $('.book_id').live('change',function(){
        if($(this).val() == 'full-set'){
            var arrSet = [];
            $(this).find(":selected").closest('optgroup').find('option').each(function () {
                if($(this).val() != 'full-set')
                    arrSet.push($(this).val());
            });
            //Xu ly add row
            var countRow = $('select.book_id').length - 1;
            var rowEq    = $(this).closest('tr').index();
            var remainRow= (countRow - rowEq) + 1;
            var countArrSet = arrSet.length;
            if(countArrSet > remainRow)
                for (i = 0; i < (countArrSet - remainRow); i++)
                    $('#btnAddrow').trigger('click');
            $(this).val('');//Clear option
            var startAdd = rowEq;
            $.each(arrSet, function( index, value ){
                $('select.book_id:eq('+startAdd+')').val(value).change();
                $('input.book_quantity:eq('+startAdd+')').val('1');
                startAdd++;
            });
        }
        //        $('input.book_quantity').each(function() {
        //            if($(this).val() == '')
        //                $(this).val('1');
        //        });

        $(this).closest('tr').find('.book_price').val(0);

        calBookPayment();
    });

    if(student_id != '' && student_id != null){
        //Load Student Info agian
        ajaxGetStudentInfo();
    }

    $('input.sponsor_percent, input.sponsor_amount').live('blur',function(){
        calSponsor();
    });

    $('.check_sponsor').live('click',function(){
        var std_id = $('#parent_id').val();
        if( (std_id == '' || typeof std_id == 'undefined')){
            $.alert(DOTB.language.get('J_Payment','LBL_ALERT_ADD_SPONSOR'));
            $(this).closest('tr').find('.sponsor_amount, .sponsor_percent, .voucher_id, .sponsor_number, .foc_type').val('');
            return ;
        }
        ajaxCheckVoucherCode($(this).closest('tr'), std_id);

    });

    $('.sponsor_number').keyup(function(e){
        if(e.keyCode == 13)
            $(this).closest('td').find('.check_sponsor').trigger('click');
    });

    $('#payment_amount').live('change',function(){
        //Bổ sung hàm tự động tính tiền Split Payment
        autoGeneratePayment();
        setLoyaltyLevel();
    });

    //Add SQS
    sqs_objects['EditView_user_pt_demo'] = {
        "form": "EditView",
        "method": "get_user_array",
        "field_list": ["user_name", "id"],
        "populate_list": ["user_pt_demo", "user_pt_demo_id"],
        "required_list": ["user_pt_demo_id"],
        "conditions": [{
            "name": "user_name",
            "op": "like_custom",
            "end": "%",
            "value": ""
        }],
        "limit": "30",
        "no_match_text": "No Match"
    };
    sqs_objects['EditView_user_closed_sale'] = {
        "form": "EditView",
        "method": "get_user_array",
        "field_list": ["user_name", "id"],
        "populate_list": ["user_closed_sale", "user_closed_sale_id"],
        "required_list": ["user_closed_sale_id"],
        "conditions": [{
            "name": "user_name",
            "op": "like_custom",
            "end": "%",
            "value": ""
        }],
        "limit": "30",
        "no_match_text": "No Match"
    };
});
//add select2 tblbook
function s2_tblbook(){
    $('#tbodybook > tr').not(':first').find('.book_id').each(function(index, tr) {
        $(this).select2({
            width: 'resolve' // need to override the changed default
        });
    });
}
function handleAddRow(_tbl) {
    s2_tblbook();
}
//Open Popup
function set_contact_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key){
                case 'parent_name':
                    var parent_name = val;
                    break;
                case 'parent_id':
                    var parent_id = val;
                    $("#parent_id").val(val);
                    break;
            }
        }
    }
    if(parent_id != '') $('#parent_type').hide();
    $('#parent_name').val(parent_name);
    $('#parent_id').val(parent_id);
    ajaxGetStudentInfo();

}

function set_team_return(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key)
            {
                case 'team_name':
                    var team_name = val;
                    break;
                case 'team_id':
                    var team_id = val;
                    break;
            }
        }
    }
    $('#team_name').val(team_name);
    $('#team_id').val(team_id);
    ajaxGetStudentInfo();

}
//Show Dialog
function showDialogStudent(show){
    if (show === undefined) show = false;

    var json = $('input#json_student_info').val();
    var count = 0;
    var payment_type = $('#payment_type').val();
    var htm = htm_std = obj = '';
    if(json != '' && json != null ){
        obj = JSON.parse(json);
        if(typeof obj == 'undefined') return ;
        if(obj['info']['module_name'] == 'Contacts' || obj['info']['module_name'] == 'Leads'){
            htm +=  "<b>"+DOTB.language.get('J_Payment','LBL_STUDENT_INFO')+"</b><br>";
            htm +=  parent.DOTB.App.lang.get('LBL_NAME','Contacts')+": <span id='student_name'>"+ obj['info']['name']+"</span><br>";
            htm +=  parent.DOTB.App.lang.get('LBL_PHONE_MOBILE','Contacts')+": "+ obj['info']['phone_mobile']+"<br>";
            htm +=  parent.DOTB.App.lang.get('LBL_BIRTHDATE','Contacts')+": "+ obj['info']['birthdate']+"<br>";
            htm +=  "<hr>";
            if( obj.left_list != null && typeof obj.left_list != 'undefined'){
                htm +=  "<b>"+DOTB.language.get('J_Payment','LBL_PAYMENT_LIST')+"</b><br>";
                $.each(obj.left_list, function( key, value ) {
                    htm +=  "<br><p><a  style='text-decoration: none;' href='#bwc/index.php?module=J_Payment&action=DetailView&record="+key+"'>"+value+"</a></p>";
                });
            }
            //Assign to First EC
            if(obj['info']['assigned_user_id'] != null && obj['info']['assigned_user_id'] != ''
                && (obj['info']['assigned_user_name'] != null && obj['info']['assigned_user_name'] != '')
                && (record_id == '')){
                $('#assigned_user_name').val(obj['info']['assigned_user_name']);
                $('#assigned_user_id').val(obj['info']['assigned_user_id']);
            }
            //Set Loyalty Point
            $('#loy_total_points').val(Numeric.toFloat(obj.loyalty_points));
            $('#loy_loyalty_rate_out_value').val(Numeric.toFloat(obj.loyalty_rate_out_value));
            $('.loy_student_name').text(obj['info']['name']);
            $('#loy_loyalty_mem_level').val(obj['mem_level']);
            $('#loy_net_amount').val(Numeric.toFloat(obj['net_amount']));
            $('#std_td').html('');
            removeFromValidate('EditView','std_student_list');
            //End: Set Loyalty Point
        }else if(obj['info']['module_name'] == 'J_Class'){
            htm +=  +"<b>"+DOTB.language.get('J_Payment','LBL_IS_AUTO_ENROLL')+': '+DOTB.language.get('J_Payment','LBL_CLASS_INFO')+"</b><br>";
            htm +=  parent.DOTB.App.lang.get('LBL_NAME','J_Class')+": <span id='class_name'>"+ obj['info']['name']+"</span><br>";
            htm +=  parent.DOTB.App.lang.get('LBL_CLASS_CODE','J_Class')+": "+ obj['info']['class_code']+"<br>";
            htm +=  parent.DOTB.App.lang.get('LBL_KIND_OF_COURSE','J_Class')+": "+ obj['info']['kind_of_course']+" - "+ obj['info']['level']+"<br>";
            htm +=  "<hr>";
            htm_std += '<select style="width:350px" name="std_student_list[]" id="std_student_list" multiple>';
            if( obj.left_list != null && typeof obj.left_list != 'undefined'){
                htm +=  "<b>"+DOTB.language.get('J_Payment','LBL_STUDENT_INFO')+"</b><br>";
                $.each(obj.left_list, function( key, value ) {
                    htm +=  "<p><a style='text-decoration: none;' href='#bwc/index.php?module=Contacts&action=DetailView&record="+key+"'>"+value+"</a></p>";
                    //biuld select std
                    htm_std += '<option label="'+value+'" selected value="'+key+'">'+value+'</option>';
                });
                htm_std += '</select>';

                //Set auto-enroll
                if(payment_type == 'Cashholder' && (!$('#is_auto_enroll').is(":checked"))){
                    $('#is_auto_enroll').prop('checked',true).trigger('change');
                    $('#ju_class_id').val(obj['info']['id']);
                    $('#ju_class_name').val(obj['info']['name']);
                    $('#ju_class_koc').val(obj['info']['kind_of_course']).trigger('change');
                    ajaxLoadSessions();
                }
            }else
                toastr.error(DOTB.language.get('J_Payment','LBL_ALERT_MISS_STUDENT'));
            $('#std_td').html(htm_std);
            addToValidate('EditView', 'std_student_list', 'enum', true, DOTB.language.get('J_Payment','LBL_STUDENT_INFO'));
            $('#std_student_list').select2();
        }

        htm +=  "<hr>";
        htm +=  "<a  style='text-decoration: none; float: right; font-weight: bold;' href='#bwc/index.php?module=Contacts&action=DetailView&record="+obj['info']['id']+"'>"+DOTB.language.get('J_Payment','LBL_MORE_INFO')+"</a><br>";
    }else
        htm += "<em font-style:normal;'>"+DOTB.language.get('J_Payment','LBL_ALERT_NO_INFO')+"</em>";

    if (show) {
        $('#dialog_student_info').html(htm);
        $('#dialog_student_info').attr('title',DOTB.language.get('J_Payment','LBL_STUDENT_INFO'))

        $('#dialog_student_info').dialog({
            resizable: false,
            width:'auto',
            height:'300',
            modal: false,
            visible: true,
            position: {
                my: 'top',
                at: 'right',
                of: event
            }
        });

        $('#dialog_student_info').effect("highlight", {color: '#ff9933'}, 1000);
    }

    //Show Payment List
    var html    = '';
    var count   = 0;
    if(typeof obj != 'undefined')
        $.each(obj.top_list, function( key, value ) {
            html += "<tr>";
            if(value['is_expired'])
                html += "<td align='right'>";
            else
                html += "<td align='right'><input type='checkbox' style='vertical-align: baseline;zoom: 1.2;' class='pay_check' value='"+value['payment_id']+"'"+value['checked']+">";


            html += "<input type='hidden' class='assigned_user_id' value='"+value['assigned_user_id']+"'/>";
            html += "<input type='hidden' class='assigned_user_name' value='"+value['assigned_user_name']+"'/>";
            html += "<input type='hidden' class='closed_sale_user_id' value='"+value['closed_sale_user_id']+"'/>";
            html += "<input type='hidden' class='closed_sale_user_name' value='"+value['closed_sale_user_name']+"'/>";
            html += "<input type='hidden' class='pt_demo_user_id' value='"+value['pt_demo_user_id']+"'/>";
            html += "<input type='hidden' class='pt_demo_user_name' value='"+value['pt_demo_user_name']+"'/>";

            html += "<input type='hidden' class='used_discount' value='"+value['used_discount']+"'/>";
            html += "<input type='hidden' class='use_type' value='"+value['use_type']+"'/><input type='hidden' class='pay_course_fee_id' value='"+value['course_fee_id']+"'/></td>";
            html += "<td align='center'><a  style='text-decoration: none;font-weight: bold;' href='#bwc/index.php?module=J_Payment&action=DetailView&record="+value['payment_id']+"'>"+value['payment_code']+"</a></td>";
            html += "<td align='center' class='pay_payment_type'>"+value['payment_type']+"</td>";
            html += "<td align='center'>"+value['payment_date']+"</td>";
            if(value['is_expired'])
                html += "<td align='center' style='color: red;'>"+value['payment_expired']+"</td>";
            else html += "<td align='center'>"+value['payment_expired']+"</td>";

            html += "<td align='center' class='pay_payment_amount'>"+Numeric.toFloat(value['payment_amount'])+"</td>";
            html += "<td align='center' class='pay_total_hours'>"+Numeric.toFloat(value['total_hours'],2,2)+"</td>";
            html += "<td align='center' class='pay_remain_amount'>"+Numeric.toFloat(value['remain_amount'])+"</td>";
            html += "<td align='center' class='pay_remain_hours'>"+Numeric.toFloat(value['remain_hours'],2,2)+"</td>";
            html += "<td align='center' class='pay_course_fee'>"+value['course_fee']+"</td>";
            html += "<td align='center'>"+value['assigned_user_name']+"</td>";
            html += "</tr>";
            count++
        });
    if(count == 0)
        html += '<tr><td colspan="100%" style="text-align: center;">'+DOTB.language.get('J_Payment','LBL_ALERT_NO_PAYMENT_LIST')+'</td></tr>';

    $('#tbodypayment').html(html);
    // Convert Link BWC Frame
    var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
    bwcComponent.rewriteLinks();
}

//Ajax get Student Info
function ajaxGetStudentInfo(){
    $('#payment_type').prop('disabled',true).addClass('input_readonly');
    var current_team_id =  $('input[name=team_id]').val();
    var payment_type    =  $('#payment_type').val();
    var parent_type     =  $('#parent_type').val();
    var parent_id       =  $('#parent_id').val();
    var payment_date    =  $('#payment_date').val();
    var primary_id      =  $('#primary_id').val();
    if( (primary_id == null || typeof primary_id == 'undefined')) primary_id = '';
    DOTB.ajaxUI.showLoadingPanel();
    $.ajax({
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type            : 'ajaxGetStudentInfo',
            record_id       : record_id,
            current_team_id : current_team_id,
            payment_type    : payment_type,
            parent_type     : parent_type,
            parent_id       : parent_id,
            payment_date    : payment_date,
            primary_id      : primary_id,
        },
        dataType: "json",
        success: function(res){
            DOTB.ajaxUI.hideLoadingPanel();

            if(res.success == "1"){
                $('input#json_student_info').val(res.content);
                calculatedAll();
            }else{
                $('input#json_student_info').val('');
                toastr.error(res.messenge);
            }
            //reload book
            if(res.html_book != ''){
                $('#tbodybook').html(res.html_book);
                s2_tblbook();
                calBookPayment();
            }
            //change team
            if(res.team_id != '' && res.team_id != current_team_id){
                $('#team_id').val(res.team_id);
                $('#team_name').val(res.team_name);
            }
            $('#payment_type').prop('disabled',false).removeClass('input_readonly');
            showDialogStudent();
            caculated();
        },
    });
}


function showDialogDiscount(){
    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    if(screenHeight < 550 || screenWidth < 550) dialogClass = '';
    else dialogClass = 'fixed-dialog';

    $( "#dialog_discount" ).dialog({
        resizable: false,
        width: "60%",
        modal: true,
        dialogClass: dialogClass,
        hideCloseButton: true,
        buttons: {
            "Submit":{
                click:function() {
                    if($('#payment_type').val() == 'Book/Gift') calBookPayment();
                    else{
                        calculatedAll();//4
                    }
                    $(this).dialog('close');
                },
                class: 'button primary',
                text: DOTB.language.get('J_Payment', 'LBL_SUBMIT'),
            },
            "Cancel":{
                click:function() {
                    $(this).dialog('close');
                },
                class: 'button',
                text: DOTB.language.get('J_Payment', 'LBL_CANCEL'),
            },
        },
        open: function(){
            $.each(['1','2','3','4' ], function( index, value ) {
                expandDiscount(value); //Show all discount
            });
        },
    });
}
function showDialogSponsor(){
    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    if (screenHeight < 550 || screenWidth < 550) dialogClass = '';
    else dialogClass = 'fixed-dialog';

    $( "#dialog_sponsor" ).dialog({
        resizable: false,
        width: 600,
        height: 300,
        modal: true,
        dialogClass: dialogClass,
        hideCloseButton: true,
        buttons: {
            "Submit":{
                click:function() {
                    if($('#payment_type').val() == 'Book/Gift') calBookPayment();
                    else{
                        calculatedAll();//4
                    }
                    $(this).dialog('close');
                },
                class: 'button primary',
                text  : DOTB.language.get('J_Payment','LBL_SUBMIT'),
            },
            "Cancel":{
                click:function() {
                    var sponsor_list = $('#sponsor_list').val();
                    if(sponsor_list == '' || sponsor_list == '{}' || sponsor_list == '[]'){
                        $('.sponsor_number').not(':eq(0)').val('');
                        $('.foc_type').not(':eq(0)').val('');
                        $('.sponsor_amount').not(':eq(0)').val('');
                        $('.sponsor_percent').not(':eq(0)').val('');
                        $('.btnRemoveSponsor').not(':eq(1)').not(':eq(0)').trigger('click');
                    }else{
                        calculatedAll();//4
                    }
                    $(this).dialog('close');
                },
                class: 'button',
                text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
            }
        },
    });
}
function showDialogLoyalty(){
    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    if(screenHeight < 550 || screenWidth < 550 ) dialogClass = '';
    else dialogClass = 'fixed-dialog';

    $( "#dialog_loyalty" ).dialog({
        resizable: false,
        width: 600,
        modal: true,
        hideCloseButton: true,
        dialogClass: dialogClass,
        buttons: {
            "Submit":{
                click:function() {
                    if($('#payment_type').val() == 'Book/Gift') calBookPayment();
                    else{
                        calculatedAll();//4
                    }
                    $(this).dialog('close');
                },
                class: 'button primary',
                text  : DOTB.language.get('J_Payment','LBL_SUBMIT'),
            },
            "Cancel": {
                click:function() {
                    $(this).dialog('close');
                },
                class: 'button',
                text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
            }


        },
    });
}
function generateCourseFee(){
    var len_course   = $("#coursefee option:selected").length;
    if(len_course > 1){
        app.alert.show('message-id', {
            level: 'success',
            title: parent.DOTB.App.lang.get('LBL_COMBO_PRICE', 'J_Payment'),
            autoClose: true
        });
    }
    $('#coursefee').select2();
    //Handle EDIT
    var cList = $('#coursefee_list').val();
    if(cList != '' && cList != null )
        cObj = JSON.parse(cList);

    var package_list = package_label= '';
    $.each($("#coursefee option:selected"), function(key, value){
        var cID      = $(this).val();
        var cHour    = Numeric.parse($(this).attr('hour'));
        var cPrice   = Numeric.parse($(this).attr('unit_price'));
        var cName    = $(this).attr('course_name');
        var cType    = $(this).attr('type');
        var cKoc     = $(this).attr('kind_of_course');
        var cDration = $(this).attr('duration_exp');
        var isCustom = Numeric.parse($(this).attr('is_custom_hours'));
        var customVal = cSelected= '';

        if(typeof cObj != 'undefined' && typeof cObj[cID] != 'undefined'){
            if(cType == 'Hours') cHour = cObj[cID]["hours"];
            else cSelected = cObj[cID]["select"];
            customVal = cObj[cID]["custom"];
            if(customVal == '' && customVal == null) customVal = cObj[cID]["hours"];
        }
        //BIULD INPUT
        if(cType == 'Hours'){
            var package_html = add_package_hour(cPrice,cHour,cName,cID,cType,cKoc,isCustom);
            package_label   += package_html[0];
            package_list    += package_html[1];
        }else{
            var package_html = add_package_select(cDration,cSelected,customVal,cPrice,cHour,cName,cID,cType,cKoc,isCustom);
            package_label   += package_html[0];
            package_list    += package_html[1];
        }
    });
    //assign
    $("#package_list").html(package_list);
    $("#package_label").html(package_label);
    //Calculate Package
    calCourseFee();
    caculated();
    calSession();//Auto-Enroll
}
//Calculate Course Fee
function calCourseFee(){
    var tuition_amount      = tuition_hours = accrual_rate_value = accumulated_count = selected = 0;
    $.each($("#coursefee option:selected"), function(key, option){
        //Xet tích điểm Rewards
        selected++;
        var is_accumulate = $(this).attr('is_accumulate');
        var ac_percent = $(this).attr('accumulate_percent');
        if(is_accumulate == 1){
            accumulated_count++;
            if((ac_percent < accrual_rate_value) || (accrual_rate_value == 0 && ac_percent > 0)) accrual_rate_value = ac_percent;
        }

        var cPrice = Numeric.parse($(this).attr('unit_price'));
        var cType  = $(this).attr('type');
        var cHour  = Numeric.parse($(this).attr('hour'));
        var cID    = $(this).val();
        var package_amount = package_hours = package_price = 0;
        if(cType == 'Hours'){
            package_hours = Numeric.parse($("tr#"+cID).find('.c_package_hours').val());
            package_price = cPrice / cHour;
            package_amount = package_price * package_hours;
            $("tr#"+cID).find('.c_package_hours').val(Numeric.toFloat(package_hours,2,2));
        }else{
            var package_select = $("tr#"+cID).find('.c_package_select').val();
            if(package_select == 'custom'){
                $("tr#"+cID).find('.c_package_custom').show();
                package_select = $("tr#"+cID).find('.c_package_custom').val();
            }else $("tr#"+cID).find('.c_package_custom').hide();
            package_select = Numeric.parse(package_select);
            package_hours  = package_select * cHour;
            package_amount = package_hours * cPrice;
            $("tr#"+cID).find('.c_package_hours').val(Numeric.toFloat(package_hours,2,2));
        }
        $("tr#"+cID).find('.c_package_amount').val(Numeric.toFloat(package_amount));

        tuition_amount += package_amount;
        tuition_hours  += package_hours;
    });
    if(accumulated_count != selected || accumulated_count == 0) accrual_rate_value = 0;
    $('input#accrual_rate_value').val(accrual_rate_value/100);

    $('#tuition_hours').val(Numeric.toFloat(tuition_hours,2,2));
    $('#tuition_fee').val(Numeric.toFloat(tuition_amount));
    return Numeric.parse(tuition_amount); //Làm tròn số tiền thu
}

function setLoyaltyLevel(){
    var current_level        = $('#loy_loyalty_mem_level').val();
    var net_amount           = Numeric.parse($('#loy_net_amount').val());
    var amount_bef_discount  = Numeric.parse($('#amount_bef_discount').val());
    var discount_amount      = Numeric.parse($('#discount_amount').val());
    var final_sponsor        = Numeric.parse($('#final_sponsor').val());
    var loyalty_amount       = Numeric.parse($('#loyalty_amount').val());
    var payment_amount       = Numeric.parse($('#payment_amount').val());
    var sum_current_amount   = net_amount + payment_amount + discount_amount + final_sponsor + loyalty_amount;

    //add Loyalty Reward
    var accrual_rate_value = Numeric.parse($('input#accrual_rate_value').val());
    $('span#accrual_rate_label').text( '('+(accrual_rate_value * 100)+'%)' );
    $('input#total_rewards_amount').val(Numeric.toFloat(Math.floor((payment_amount * accrual_rate_value)/1000)*1000));

    if(payment_amount <= 0 || payment_amount == '') sum_current_amount = net_amount;

    var rank_link            = [];
    var level                = 'N/A';
    var html                 = '';
    if(typeof DOTB.language.languages['app_list_strings'] != 'undefined')
        rank_link = DOTB.language.languages['app_list_strings']['loyalty_rank_list'];

    if(typeof rank_link != 'undefined'){
        if(sum_current_amount >= parseInt(rank_link['Blue']))
            level = 'Blue';
        if(sum_current_amount >= parseInt(rank_link['Gold']))
            level = 'Gold';
        if(sum_current_amount >= parseInt(rank_link['Platinum']))
            level = 'Platinum';
    }
    //Set HTML
    if(level == 'Platinum'){
        html = '<label><span class="textbg_black">'+level+'</span></label>';
    }else if(level == 'Gold'){
        html = '<label><span class="textbg_yellow">'+level+'</span></label>';
    }else if(level == 'Blue'){
        html = '<label><span class="textbg_bluelight">'+level+'</span></label>';
    }else{
        html = '<label><span class="textbg_nocolor">'+level+'</span></label>';
    }
    $('.loy_loyalty_mem_level').html(html);
    $('#loy_loyalty_mem_level').val(level);
    return level;
}

function caculated(){
    // Caculate Payment list
    var payment_type = $('#payment_type').val();
    var payment_list = {};
    payment_list['paid_list']       =  {};
    payment_list['deposit_list']    =  {};
    var total_used_amount  = 0;
    var total_used_hours  = 0;
    var total_deposit_amount  = 0;
    var tuition_fee         = Numeric.parse($('#tuition_fee').val());
    var amount_bef_discount = Numeric.parse($('#amount_bef_discount').val());
    var discount_amount     = Numeric.parse($('#discount_amount').val());
    var tuition_hours       = Numeric.parse($('#tuition_hours').val());
    var discount_percent    = Numeric.parse($('#discount_percent').val());
    var loyalty_amount      = Numeric.parse($('#loyalty_amount').val());
    var loyalty_percent     = Numeric.parse($('#loyalty_percent').val());
    var payment_amount      = 0;
    var remaining_freebalace      = 0;

    //discount hours
    var dis_hours      = Numeric.parse($('#dis_hours').val());
    var final_sponsor           = Numeric.parse($('#final_sponsor').val());
    var final_sponsor_percent   = Numeric.parse($('#final_sponsor_percent').val());
    var total_after_discount = 0;
    var price_enroll = (tuition_fee) / (tuition_hours); // đơn giá tổng
    if (!isFinite(price_enroll)) price_enroll = 0;
    var total_hours = Numeric.parse(tuition_hours);

    // add paid payment to json
    var count_pay = 0;
    if (payment_type == "Cashholder") amount_bef_discount = calCourseFee();
    //Get - áp dụng cho Discount và Sponsor
    var coursefee_hour = parseInt($("#coursefee option:selected").attr('type'));
    var total_after_discount = amount_bef_discount - discount_amount - final_sponsor - loyalty_amount;
    if(total_after_discount < 0) total_after_discount = 0;
    payment_amount = total_after_discount;
    var total_discount_sponsor = loyalty_amount + discount_amount + final_sponsor;
    // add deposit payment to json
    $('.pay_check:checked').each(function(index, brand){
        var payment_type = $(this).closest('tr').find('.pay_payment_type').text();
        var use_type = $(this).closest('tr').find('.use_type').val();
        var used_amount = Numeric.parse($(this).closest('tr').find('.pay_remain_amount').text());
        if((payment_amount > 0) && (used_amount > 0)){

            payment_amount = payment_amount - used_amount;
            if(payment_amount < 0){
                used_amount = used_amount + payment_amount;
                remaining_freebalace += payment_amount;
                payment_amount = 0;
            }
            total_deposit_amount += used_amount;

            payment_list['deposit_list'][$(this).val()] = {};
            payment_list['deposit_list'][$(this).val()]["id"] = $(this).val();
            payment_list['deposit_list'][$(this).val()]["used_amount"] = used_amount;
            payment_list['deposit_list'][$(this).val()]["used_hours"] = 0;
            count_pay++;
        }
    });
    var str_json_payment_list = '';
    if(count_pay > 0)
        str_json_payment_list = JSON.stringify(payment_list);

    $('#payment_list').val(str_json_payment_list);

    if(total_used_hours > 0)
        $('#paid_amount').closest('table').closest('tr').show();
    else
        $('#paid_amount').closest('table').closest('tr').hide();
    if(total_deposit_amount > 0)
        $('#deposit_amount').closest('table').closest('tr').show();
    else
        $('#deposit_amount').closest('table').closest('tr').hide();
    //Assign money
    $('#tuition_fee').val(Numeric.toFloat(tuition_fee));
    //không làm tròn 2 số này
    $('#paid_amount').val(Numeric.toFloat(total_used_amount));
    $('#deposit_amount').val(Numeric.toFloat(total_deposit_amount));

    $('#amount_bef_discount').val(Numeric.toFloat(amount_bef_discount));
    $('#total_after_discount').val(Numeric.toFloat(total_after_discount));
    $('#total_discount_sponsor').val(Numeric.toFloat(total_discount_sponsor));
    $('#remaining_freebalace').val(Numeric.toFloat(Math.abs(remaining_freebalace)));
    $('#payment_amount').val(Numeric.toFloat(payment_amount));
    //Assign hour
    $('#tuition_hours').val(Numeric.toFloat(tuition_hours,2,2));
    $('#paid_hours').val(Numeric.toFloat(total_used_hours,2,2));

    $('#total_hours').val(Numeric.toFloat(total_hours + dis_hours,2,2));

    $('#payment_amount').val(Numeric.toFloat(payment_amount));
    autoGeneratePayment();
    setLoyaltyLevel();
}

function ajaxCheckVoucherCode(self ,student_id){
    var voucher_code    = self.find('.sponsor_number').val().replace(/ /g,'');

    if(voucher_code != ''){
        //Ajax check Sponsor code
        $.ajax({
            url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
            type: "POST",
            async: false,
            data:  {
                type            : 'ajaxCheckVoucherCode',
                voucher_code    : voucher_code,
                student_id      : student_id,
                payment_id      : record_id,
                team_id         : $('#team_id').val(),
                payment_date    : $('#payment_date').val(),
            },
            dataType: "json",
            success: function(res){
                if(res.success == "1"){

                    var discount_amount = '<br>Discount: '+res.discount_amount;
                    var discount_percent= '<br>Discount %: '+res.discount_percent;
                    if(res.discount_amount == 0) discount_amount = '';
                    if(res.discount_percent == 0) discount_percent = '';

                    var description = ''
                    if(res.description != '') description = ' ('+res.description+')';

                    var student = '';
                    if(res.type == 'Loyalty') student = '<br>Student Name: ' + res.student_name;

                    if(res.status == 'Expired' || res.status == 'Inactive' ){
                        self.find('.sponsor_amount, .sponsor_percent, .voucher_id, .sponsor_number, .foc_type').val('');

                    }else{
                        self.find('.sponsor_amount').val(res.discount_amount);
                        self.find('.sponsor_percent').val(res.discount_percent);
                        self.find('.loyalty_points').val(res.loyalty_points);
                        self.find('.voucher_id').val(res.voucher_id);
                        self.find('.type').val(res.type);
                        self.find('.foc_type').val(res.foc_type);
                        self.find('.sponsor_number').val(res.sponsor_number);
                        self.find('.description').val(res.description);
                        self.find('.priority').val(res.priority);
                    }
                    self.find('.foc_type').trigger('change');

                    $.alert('Sponsor: '+ res.sponsor_number + student + res.status_color+'<br>'+ description +'<br>Expires: ' + res.end_date + discount_amount + discount_percent + '<br>Total Redemption: '+ res.used_time);
                    // Convert Link BWC Frame
                    var bwcComponent = parent.DOTB.App.controller.layout.getComponent("bwc");
                    bwcComponent.rewriteLinks();

                }else if(res.success == "0"){
                    self.find('.sponsor_amount, .sponsor_percent, .voucher_id, .sponsor_number, .loyalty_points, .type, .foc_type, .description').val('');
                    $.alert(DOTB.language.get('J_Payment','LBL_ALERT_SPONSOR1'));
                }
                calSponsor();
            },
        });
        //END
    }

}

function calLoyalty(){
    var payment_type        = $('#payment_type').val();
    var points              = Number(Numeric.parse($('#loy_loyalty_points').val()));
    var max_points          = Number(Numeric.parse($('#loy_total_points').val()));
    var rate_out            = Numeric.parse($('#loy_loyalty_rate_out_value').val());
    var vat_amount          = Numeric.parse($('#vat_amount').val());
    var amount_bef_discount = Numeric.parse($('#amount_bef_discount').val());
    var discount_percent    = Numeric.parse($('#discount_amount').val());
    var final_sponsor       = Numeric.parse($('#final_sponsor').val());
    var amount_max_loylaty  = amount_bef_discount - (discount_percent + final_sponsor);
    if(payment_type == 'Deposit') amount_max_loylaty = 0;

    //var max_policy_points   = Number((limited_discount - discount_amount) / rate_out);
    var max_policy_points   = Math.ceil(amount_max_loylaty/rate_out); //Set tạm sử dụng đc 99999999999điểm /Lần thanh toán
    if(max_points <= 0)
        $('#loy_loyalty_points').val(0).prop('disabled',true).addClass('input_readonly');
    else
        $('#loy_loyalty_points').prop('disabled',false).removeClass('input_readonly');

    if( (points < min_points || points > max_points || isNaN(points)) && max_points > 0 ){
        toastr.error(DOTB.language.get('J_Payment','LBL_ALERT_LOYALTY1')+points+DOTB.language.get('J_Payment','LBL_ALERT_LOYALTY2')+min_points+' -> '+max_points);
        $('#loy_loyalty_points').val(1).effect("highlight", {color: '#ff9933'}, 1000);
        calLoyalty();
        return ;
    }

    if(max_policy_points > 0 && points > max_policy_points){
        toastr.error(DOTB.language.get('J_Payment','LBL_ALERT_LOYALTY3') + limit_discount_percent + DOTB.language.get('J_Payment','LBL_ALERT_LOYALTY4')  + max_policy_points + DOTB.language.get('J_Payment','LBL_POINTS'));
        $('#loy_loyalty_points').val(max_policy_points).effect("highlight", {color: '#ff9933'}, 1000);
        calLoyalty();
        return ;
    }

    //Tính Loyalty
    var amount_to_spend = points * rate_out;
    if(amount_to_spend > amount_max_loylaty) amount_to_spend = amount_max_loylaty;
    $('.loy_points_to_spend').text(Numeric.toFloat(points) + ' '+DOTB.language.get('J_Payment','LBL_POINTS')+' ('+ Numeric.toFloat(amount_to_spend) +' '+default_currency_iso4217+')' );
    $('#loy_points_to_spend').val(Numeric.toFloat(points));
    $('#loy_amount_to_spend').val(Numeric.toFloat(amount_to_spend));
    $('.loy_total_points').text(Numeric.toFloat(max_points) + ' '+DOTB.language.get('J_Payment','LBL_POINTS'));
    $('.loy_loyalty_rate_out_value').text(Numeric.toFloat(rate_out) + ' '+default_currency_iso4217);
}

function submitLoyalty(){
    calLoyalty();
    var amount_bef_discount     = Numeric.parse($('#amount_bef_discount').val());
    var loy_loyalty_mem_level   = $('#loy_loyalty_mem_level').val();
    var loyalty_list            = {};
    loyalty_list['points_to_spend'] = $('#loy_points_to_spend').val();
    loyalty_list['amount_to_spend'] = $('#loy_amount_to_spend').val();
    loyalty_list['max_points']  = $('#loy_total_points').val();
    loyalty_list['min_points']  = min_points;
    loyalty_list['rate_out']    = $('#loy_loyalty_rate_out_value').val();
    if(Numeric.parse(loyalty_list['amount_to_spend']) > 0){
        var loyalty_percent = (Numeric.parse(loyalty_list['amount_to_spend']) / amount_bef_discount) * 100;
        $('#loyalty_list').val(JSON.stringify(loyalty_list));
        $('#loyalty_amount').val(loyalty_list['amount_to_spend']);
        $('#loyalty_percent').val(Numeric.toFloat(loyalty_percent,2,2));
    }else{
        $('#loyalty_list').val('');
        $('#loyalty_amount').val(0);
        $('#loyalty_percent').val(Numeric.toFloat(0,2));
    }
    var payment_type = $('#payment_type').val();
}


function calSponsor(){
    var total_sponsor_percent       = 0;
    var total_sponsor_amount        = 0;
    var count_referal               = 0;
    var priority                    = 'before_discount';
    var amount_bef_discount         = Numeric.parse($('#amount_bef_discount').val());
    // Fix bug không thêm được Sponsor
    $('.row_tpl_sponsor').each(function(index, brand){
        var sponsor_amount = Numeric.parse($(this).find('.sponsor_amount').val());
        var sponsor_percent = Numeric.parse($(this).find('.sponsor_percent').val());

        total_sponsor_amount += (sponsor_amount)

        total_sponsor_percent  += sponsor_percent;
        priority  = $(this).find('.priority').val();
    });

    //Tính Sponsor
    $('.sponsor_amount_bef_discount').text(Numeric.toFloat(amount_bef_discount));
    $('.total_sponsor_amount').text(Numeric.toFloat(total_sponsor_amount));
    $('.total_sponsor_percent').text(Numeric.toFloat(total_sponsor_percent,2,2));
    if(priority =='before_discount'){
        var total_sponsor_percent_to_amount = ((amount_bef_discount - total_sponsor_amount) * total_sponsor_percent / 100);
        var final_sponsor = total_sponsor_amount + total_sponsor_percent_to_amount;
        var final_sponsor_percent = (final_sponsor / amount_bef_discount) * 100;
        if(final_sponsor > amount_bef_discount )  {
            final_sponsor = amount_bef_discount;
            final_sponsor_percent = 100;
        }
    }else{
        var discount_amount = Numeric.parse($('#discount_amount').val());
        var loyalty_amount  = Numeric.parse($('#loyalty_amount').val());
        var total_sponsor_percent_to_amount = ((amount_bef_discount - total_sponsor_amount - discount_amount - loyalty_amount) * total_sponsor_percent / 100);
        var final_sponsor = total_sponsor_amount + total_sponsor_percent_to_amount;
        var final_sponsor_percent = (final_sponsor / amount_bef_discount) * 100;
        if(final_sponsor > amount_bef_discount )  {
            final_sponsor = amount_bef_discount;
            final_sponsor_percent = 100;
        }
    }


    $('.final_sponsor').text(Numeric.toFloat(final_sponsor));
    $('.final_sponsor_percent').val(Numeric.toFloat(final_sponsor_percent,2,2));
    $('.total_sponsor_percent_to_amount').val(Numeric.toFloat(total_sponsor_percent_to_amount));

}

function submitSponsor(){
    calSponsor();
    var sponsor_list = {};
    var count = 0;
    var count_error = 0;
    var total_sponsor_percent_to_amount  = Numeric.parse($('.total_sponsor_percent_to_amount').val());
    var total_sponsor_percent            = Numeric.parse($('.total_sponsor_percent').text());
    var amount_bef_discount         = Numeric.parse($('#amount_bef_discount').val());
    $('.row_tpl_sponsor').each(function(index, brand){
        var total_sponsor_down = 0;
        var sponsor_number  = $(this).find('.sponsor_number').val();
        var foc_type        = $(this).find('.foc_type').val();
        var loyalty_points  = $(this).find('.loyalty_points').val();
        var type            = $(this).find('.type').val();
        var voucher_id      = $(this).find('.voucher_id').val();
        var sponsor_amount  = Numeric.parse($(this).find('.sponsor_amount').val());
        var sponsor_percent = Numeric.parse($(this).find('.sponsor_percent').val());
        var description     = $(this).find('.description').val();
        if(sponsor_amount != 0 || sponsor_percent != 0 ){
            if(sponsor_number == '' || foc_type == ''){
                count_error++;
                return;
            }
        }
        if(sponsor_percent > 100){
            count_error++;
            return;
        }

        total_sponsor_down += (sponsor_amount)

        if(total_sponsor_percent != 0)
            total_sponsor_down += total_sponsor_percent_to_amount * (sponsor_percent/total_sponsor_percent);

        if(amount_bef_discount < total_sponsor_down) total_sponsor_down = amount_bef_discount
        if(sponsor_number != '' && foc_type != ''){
            sponsor_list[count]                     = {};
            sponsor_list[count]['voucher_id']       = voucher_id;
            sponsor_list[count]['sponsor_number']   = sponsor_number;
            sponsor_list[count]['type']             = type;
            sponsor_list[count]['foc_type']         = foc_type;
            sponsor_list[count]['loyalty_points']   = loyalty_points;
            sponsor_list[count]['sponsor_amount']   = Numeric.toFloat(sponsor_amount);
            sponsor_list[count]['sponsor_percent']  = Numeric.toFloat(sponsor_percent,2,2);
            sponsor_list[count]['total_down']       = Numeric.toFloat(total_sponsor_down);
            sponsor_list[count]['description']      = description;
            count++;
        }
    });
    if(count_error > 0){
        toastr.error(DOTB.language.get('J_Payment','LBL_ALERT_FILL_OUT'));
        return false;
    }
    $('#sponsor_list').val(JSON.stringify(sponsor_list));
    $('#final_sponsor').val($('.final_sponsor').text());
    $('#final_sponsor_percent').val($('.final_sponsor_percent').val());
}

function calDiscount(){
    checkAvailableDiscount();
    //Handle schema apply with discount
    $('.dis_amount_bef_discount').text($('#amount_bef_discount').val());
    var dis_amount_bef_discount = Numeric.parse($('#amount_bef_discount').val());
    var dis_total_hours     = Numeric.parse($('#total_hours').val());
    var dis_tuition_hours   = Numeric.parse($('#tuition_hours').val());
    var current_loyalty     = $('#loy_loyalty_mem_level').val();
    var final_sponsor   = Numeric.parse($('#final_sponsor').val());
    var loyalty_amount  = Numeric.parse($('#loyalty_amount').val());


    var dis_discount_amount     = 0;
    var dis_loyalty_point       = 0;
    var dis_discount_percent    = 0;
    var chain_discount_percent  = 0;
    var dis_total_discount      = 0;
    var dis_total_discount_amount      = 0;
    var dis_total_discount_percent     = 0;
    var dis_chain_discount = 0;
    var dis_trade_discount = 0;
    var dis_ratio = 0;
    var dis_hours = 0;
    var payment_type    = $('#payment_type').val();

    var maximum_percent = 0;

    $('.dis_check').each(function(index, brand){
        var dis_type            = $(this).closest('tr').find('input.dis_type').val();
        if(dis_type == 'Other' || dis_type == 'Hour'){
            var dis_content_json    = $(this).closest('tr').find('input.dis_content').val();
            if(dis_content_json != '' && dis_content_json != null )
                var dis_obj = JSON.parse(dis_content_json);

            var cr_maximum = Numeric.parse($(this).attr('maximum_percent'));
            if(cr_maximum > maximum_percent && $(this).is(":checked"))
                maximum_percent = cr_maximum;

            var dis_amount         = Numeric.parse($(this).closest('tr').find('.dis_amount').val());
            var dis_percent        = Numeric.parse($(this).closest('tr').find('.dis_percent').val());
            var has_class          = $(this).closest('tr').find(".dis_hours").hasClass("input_readonly");
            dis_hours          = Numeric.parse($(this).closest('tr').find(".dis_hours").val());
            if(dis_type == 'Hour' && payment_type == 'Cashholder' ){
                if(typeof dis_obj != 'undefined' && has_class){
                    var catch_hour  = 0;
                    var rph = 0;
                    var pmh = 0;
                    $.each(dis_obj.discount_by_hour, function(index, value){
                        if(dis_tuition_hours >= Numeric.parse(value.hours)){
                            catch_hour++;
                            rph = dis_tuition_hours - Numeric.parse(value.hours);
                            pmh = Numeric.parse(value.promotion_hours);
                        }
                    });

                    dis_hours = pmh;
                }
                dis_amount = 0;
                $(this).closest('tr').find("td:eq(3)").text(dis_amount == "0"? ""  : Numeric.toFloat(dis_amount));
                $(this).closest('tr').find(".dis_amount").val(dis_amount == "0" ? ""  : Numeric.toFloat(dis_amount));
                $(this).closest('tr').find(".dis_hours").val(Numeric.toFloat(dis_hours,2,2));
            }
            if(dis_type == 'Other' && $(this).is(":checked")){
                dis_percent = Numeric.parse($(this).closest('tr').find('.dis_percent').val());
            }
            if($(this).is(":checked")){
                var is_chain_discount  = $(this).attr('is_chain_discount');
                var is_trade_discount  = $(this).attr('is_trade_discount');
                var is_ratio           = $(this).attr('is_ratio');

                if(is_chain_discount == 1)
                    dis_chain_discount++;
                if(is_trade_discount == 1)
                    dis_trade_discount++;

                if(is_ratio == 1) dis_ratio++;

                dis_discount_amount     += dis_amount;

                if(chain_discount_percent > 0) //Chain Discount
                    chain_discount_percent *= (1 - (dis_percent/100));
                else
                    chain_discount_percent = (1 - (dis_percent/100));

                dis_discount_percent += (dis_percent / 100);
            }
        }
    });

    if(chain_discount_percent > 0)
        chain_discount_percent = (1 - chain_discount_percent);
    else chain_discount_percent = 0;

    //Apply chain discount
    if(dis_chain_discount > 0)
        dis_discount_percent = chain_discount_percent;
    //END:
    if(dis_ratio > 0){
        // var ratio =
        //dis_total_discount_amount = dis_discount_amount * ;
        var total_package_hours = 0;
        $.each($("#coursefee option:selected"), function(key, option){
            var cType  = $(this).attr('type');
            var cHour  = Numeric.parse($(this).attr('hour'));
            var cID    = $(this).val();
            switch (cType){
                case 'Hours':
                    var package_hours = cHour;
                    break;
                case 'Sessions':
                case 'Hour/Month':
                case 'Hour/Week':
                    var package_select = Numeric.parse($("tr#"+cID).find('.c_package_select').val());
                    var package_hours  = package_select * cHour;
                    break;
            }
            total_package_hours  += package_hours;
        });
        var tuition_hours = Numeric.parse($('#tuition_hours').val());
        var ratio = tuition_hours/total_package_hours;
        if(ratio > 1 || isNaN(ratio)) ratio = 1;
        dis_total_discount_amount = dis_discount_amount * ratio;
        $('.dis_ratio').text('Ratio: '+Numeric.toFloat(ratio,2,2));
    }

    else dis_total_discount_amount = dis_discount_amount;

    if(dis_trade_discount > 0)
        dis_total_discount_percent      = (dis_discount_percent) * dis_amount_bef_discount;
    else
        dis_total_discount_percent      = (dis_discount_percent)*(dis_amount_bef_discount - dis_total_discount_amount - final_sponsor);

    dis_total_discount              = dis_total_discount_amount + dis_total_discount_percent;


    //assign
    $('.dis_total_discount').text(Numeric.toFloat(dis_total_discount));
    $('.dis_discount_percent').text(Numeric.toFloat(dis_discount_percent*100,2,2));
    $('.dis_discount_percent_p').val(Numeric.toFloat(dis_discount_percent*100,2,2));
    $('.dis_discount_amount').text(Numeric.toFloat(dis_total_discount_amount));

    var limit_percent = limit_discount_percent;
    if(maximum_percent > 0)           //Bo dien kien thay doi limit
        limit_percent = maximum_percent;

    //Compare with limit
    var limited_discount = ((limit_percent/100) * dis_amount_bef_discount) + (final_sponsor);

    var catch_limit = false;
    $('#catch_limit').val('0');
    if( (dis_total_discount) >= limited_discount){
        dis_total_discount = limited_discount;
        catch_limit = true;
        $('#catch_limit').val('1');
    }
    var dis_final_discount_percent = Numeric.toFloat((dis_total_discount / (dis_amount_bef_discount)) * 100,2,2);

    if(catch_limit)
        $('.dis_alert_discount').html("&nbsp;&nbsp;&nbsp;(limited "+limit_percent+"%)").show();
    else $('.dis_alert_discount').hide();
    //assign final discount
    $('.dis_final_discount').text(Numeric.toFloat(dis_total_discount));
    $('.dis_final_discount_percent').val(dis_final_discount_percent);
    $('.dis_discount_percent_to_amount').val(Numeric.toFloat(dis_total_discount_percent));
    $('#limited_discount_amount').val();

    //Xu ly clear loyalty
    var limited_discount_amount = Numeric.toFloat(limited_discount);
    $('#limited_discount_amount').val(limited_discount_amount);
}

function submitDiscount(){
    calDiscount();
    var discount_list = {};
    var count = 0;
    var description = 'Chiết khấu: ';
    var catch_limit =  $('#catch_limit').val();
    var dis_discount_percent_to_amount  = Numeric.parse($('.dis_discount_percent_to_amount').val());
    var discount_percent                = Numeric.parse($('.dis_discount_percent_p').val());//Chain Discount

    var total_discount = Numeric.parse($('.dis_total_discount').text());
    var final_discount = Numeric.parse($('.dis_final_discount').text());
    //discount hour
    var total_hours = Numeric.parse($('#total_hours').val());
    var dis_hours   = 0;
    $('.dis_check:checked').each(function(index, brand){
        var dis_type            = $(this).closest('tr').find('input.dis_type').val();
        if(dis_type == 'Other' || dis_type == 'Hour'){
            var dis_percent     = Numeric.parse($(this).closest('tr').find('.dis_percent').val());
            var dis_amount      = Numeric.parse($(this).closest('tr').find('.dis_amount').val());
            var dis_is_catch_limit    = $(this).closest('tr').find('input.dis_is_catch_limit').val();
            var total_down      = dis_amount;

            var row_dis_hours = Numeric.parse($(this).closest('tr').find('.dis_hours').val());

            if(discount_percent != 0)
                total_down += (dis_discount_percent_to_amount * (dis_percent/discount_percent));
            if(catch_limit == '1' && dis_is_catch_limit == 1)
                total_down = (total_down * (final_discount) / (total_discount));

            count++;
            discount_list[$(this).val()] =  {};
            discount_list[$(this).val()]['id']          =  $(this).val();
            discount_list[$(this).val()]['type']        =  'Discount';
            discount_list[$(this).val()]['dis_percent']  = Numeric.toFloat(dis_percent,2,2);
            discount_list[$(this).val()]['dis_amount']   = Numeric.toFloat(dis_amount);
            discount_list[$(this).val()]['total_down']   = Numeric.toFloat(total_down);
            if(count == 1)
                var des =  $(this).closest('tr').find('.dis_name').text();
            else var des = ', '+$(this).closest('tr').find('.dis_name').text();
            if(dis_type == 'Hour'){
                //add to field dis_hours
                dis_hours += row_dis_hours;
            }
            description = description + des;
        }
    });
    var str_json_discount = '';
    var str_json_discount = JSON.stringify(discount_list);
    if (description == "Chiết khấu: ") description = '';
    //Add Sponsor Description
    var sponsor_list = $('#sponsor_list').val();
    if(sponsor_list != '' && typeof sponsor_list != 'undefined'){
        var sponsor_objs = JSON.parse(sponsor_list);
        $.each(sponsor_objs, function( key, sponsor_obj ) {
            description = description + ", Sponsor Code: " +sponsor_obj.sponsor_number;
        });
    }
    //add field dis_hours
    $('#dis_hours').val(Numeric.toFloat(dis_hours,2,2));
    $('#description').val(description);
    $('#discount_list').val(str_json_discount);
    $('#discount_amount').val($('.dis_final_discount').text());
    $('#discount_percent').val($('.dis_final_discount_percent').val());
    $('#sub_discount_amount').val($('.dis_discount_amount').text());
    $('#sub_discount_percent').val($('.dis_discount_percent').text());
}

function switchPaymentType(){
    var type        = $('#payment_type').val();
    var parent_type = $('#parent_type').val();
    if(record_id == '' && (duplicate_id == '' || typeof duplicate_id == 'undefined')){  //In Case Create
        $('#tuition_hours').val('');
        $('#tuition_fee').val('');
        $('#amount_bef_discount').val('');
        $('#payment_amount').val('');
        $('#loy_loyalty_points').val('');

        $('.dis_check').prop('checked',false);
        calculatedAll();//4
    }
    if(parent_type == 'J_Class'){
        //Disable loyalty/sponsor
        $("#tab_discount").tabs( "enable" );
        $('a[href="#discount"]').click();
        $('#tab_discount').tabs("option","disabled", [1,2]);
    }
    switch (type) {
        case 'Cashholder':
            $('#payment_amount').prop('readonly',true).addClass('input_readonly');
            $("#is_free_book_label").closest('tr').hide();
            $('#detailpanel_1').show();
            $('#coursefee').closest('tr').show();
            $('#total_discount_sponsor').closest('table').closest('tr').show().find('#total_hours').closest('td').show().prev().show();
            $('#total_rewards_amount').closest('td').show().prev().show();
            $('#vat_rate').closest('tr').hide();
            $('#vat_amount').closest('tr').hide();
            $('#amount_bef_discount').closest('table').closest('tr').show();
            $('#amount_bef_discount').closest('tr').find('.tuition_hours').show();
            $('#total_after_discount').closest('tr').show();
            $('a[href="#discount"]').click();
            addToValidate('EditView', 'j_coursefee_j_payment_1j_coursefee_ida[]', 'multienum', true,'Course Fee' );
            break;
        case 'Book/Gift':
            $('#tblbook').show();
            $('#detailpanel_1').hide();
            $("#is_free_book_label").closest('tr').show();
            $("#coursefee").closest('tr').hide();
            $('#total_discount_sponsor').closest('table').closest('tr').show().find('#total_hours').closest('td').hide().prev().hide();
            $('#total_after_discount').closest('tr').show();
            $('#amount_bef_discount').closest('table').closest('tr').show();
            $('#amount_bef_discount').closest('tr').find('.tuition_hours').hide();
            $('#vat_rate').closest('tr').show();
            $('#vat_amount').closest('tr').show();
            $('#total_rewards_amount').closest('td').hide().prev().hide()
            $('#payment_amount').prop('readonly',true).addClass('input_readonly');
            removeFromValidate('EditView', 'j_coursefee_j_payment_1j_coursefee_ida[]');
            break;
        default:
            $('#payment_amount').prop('readonly',false).removeClass('input_readonly');
            $("#coursefee").val('').trigger('change').closest('tr').hide();
            $("#is_free_book_label").closest('tr').hide();
            $('#detailpanel_1').hide();
            $('#total_discount_sponsor').closest('table').closest('tr').hide();
            $('#amount_bef_discount').closest('table').closest('tr').hide();
            $('#total_after_discount').closest('tr').hide();
            $('#total_rewards_amount').closest('td').hide().prev().hide();
            $('#vat_rate').closest('tr').hide();
            $('#vat_amount').closest('tr').hide();
            removeFromValidate('EditView', 'j_coursefee_j_payment_1j_coursefee_ida[]');
            break;
    }
    if(type == 'Deposit') $("#kind_of_course").closest('tr').show();
    else  $("#kind_of_course").closest('tr').hide();
    toggleAutoEnroll();
}
function calBookPayment(){
    var total_pay = curr_taxrate_value=  0;
    var curr_taxrate_id = '###';
    $('#tblbook tbody tr:not(:first-child)').each(function(index, brand){
        var book_id = $(this).find('select.book_id option:selected').val();
        var book_price = Numeric.parse($(this).find('.book_price').val());

        var book_cost = 0;
        var book_name = book_quantity= book_unit = '';
        if(book_id != '' && typeof book_id != 'undefined'){
            if(book_price == '')
                book_price = Numeric.parse($(this).find('select.book_id option:selected').attr('price'));

            book_unit       = $(this).find('select.book_id option:selected').attr('unit');
            book_name       = $(this).find('select.book_id option:selected').text();
            book_quantity   = parseInt($(this).find('.book_quantity').val());
            book_cost       = (book_price * book_quantity);
            var taxrate_id      = $(this).find('select.book_id option:selected').attr('taxrate_id');
            var taxrate_value   = $(this).find('select.book_id option:selected').attr('taxrate_value');

            if(typeof taxrate_id != 'undefined' && taxrate_id != curr_taxrate_id && curr_taxrate_id != '###'){
                $(this).remove();
                toastr.error(DOTB.language.get('J_Payment','LBL_ALERT_BOOK1')+' '+book_name + DOTB.language.get('J_Payment','LBL_ALERT_BOOK2') );
                return;
            }

            //VAT handle
            curr_taxrate_id     = taxrate_id;
            curr_taxrate_value  = Numeric.parse(taxrate_value);
        }

        if($('#is_free_book').is(':checked')) book_cost = 0;
        $(this).find('.book_price').val(Numeric.toFloat(book_price));
        $(this).find('.book_amount').val(Numeric.toFloat(book_cost));
        $(this).find('.book_unit').text(book_unit);
        total_pay = total_pay + book_cost;
    });
    $('#amount_bef_discount, #total_book_pay').val(Numeric.toFloat(total_pay));
    calculatedAll();//4
    var total_after_discount = Numeric.parse($('#total_after_discount').val());
    var vat_amount = total_after_discount - (total_after_discount * 100 / (100+curr_taxrate_value));

    $('#vat_rate').val(Numeric.toFloat(curr_taxrate_value,2,2));
    $('#vat_amount').val(Numeric.toFloat(vat_amount));
    autoGeneratePayment();
}
function handleRemoveRow(){
    calBookPayment();
}

// Clear Discount
function clearDiscount(){
    var clear = 0;
    $('.dis_check:checked').each(function(index, focus){
        $(this).prop('checked',false);
        clear++;
    });
    if(clear > 0){
        toastr.info(DOTB.language.get('J_Payment', 'LBL_DISCOUNT_AGAIN'));
        submitDiscount();
    }
}
// check available discount
function checkAvailableDiscount(){
    var payment_type = $('#payment_type').val();
    var coursefee    = $("#coursefee").val();
    //release all
    $('.dis_check').each(function(index, focus){
        var dis_check_id            = $(this).closest('tr').find(".dis_check").val();
        var disable_discount_list   = $(this).closest('tr').find(".disable_discount_list").val();
        $('#row_'+dis_check_id).removeClass('dis_unmatch');
        if(typeof disable_discount_list != 'undefined' && disable_discount_list != '' && disable_discount_list != '[]'){
            var disable_obj = JSON.parse(disable_discount_list);
            $.each(disable_obj, function(index, value){
                $('#row_'+value).removeClass("locked").addClass("unlocked").find("td").css("background","");
                $('#row_'+value).find('.dis_check').show();
                $('#row_'+value).attr("title",'');
            });
        }
    });

    //Check payment type & apw_coursefee | HIDE
    $('.dis_check').each(function(index, focus){
        var selfdc = $(this);
        var apw_payment_type = selfdc.closest('tr').find(".apw_payment_type").val();
        var dis_check_id     = selfdc.closest('tr').find(".dis_check").val();
        var apw_coursefee    = selfdc.closest('tr').find(".apw_coursefee").val();
        var dis_name         = $(this).closest('tr').find(".dis_name").text();
        //Apply with payment type
        if(apw_payment_type != 'All'
            && apw_payment_type != payment_type
            && typeof apw_payment_type != 'undefined'
            && apw_payment_type != ''){
            $('#row_'+dis_check_id).addClass('dis_unmatch');
        }
        //Apply with Course Fees
        else if( apw_coursefee != '[""]' && apw_coursefee != '' && typeof apw_coursefee != 'undefined'){
            cApwC = JSON.parse(apw_coursefee);
            $('#row_'+dis_check_id).addClass('dis_unmatch');
            $.each($("#coursefee option:selected"), function(key, cid){
                if($.inArray(cid.value, cApwC) != -1) $('#row_'+dis_check_id).removeClass('dis_unmatch');
            });
        }
        //Disable DO NOT APPLY WITH DISCOUNT
        if($(this).is(':checked')){
            var disable_discount_list   = $(this).closest('tr').find(".disable_discount_list").val();
            if(typeof disable_discount_list != 'undefined' && disable_discount_list != '' && disable_discount_list != '[]'){
                var disable_obj = JSON.parse(disable_discount_list);
                $.each(disable_obj, function(index, value){
                    if(dis_check_id != value){
                        $('#row_'+value).removeClass("unlocked").addClass("locked").find("td").css("background","bisque");
                        $('#row_'+value).find('.dis_check').prop("checked",false).hide();
                        $('#row_'+value).attr("title",  DOTB.language.get('J_Payment','LBL_ALERT_DISCOUNT1') + ' ' + dis_name);
                    }
                });

            }
        }
    });
}

function collapseDiscount(id){
    $('a#collapseLink'+id).hide();
    $('a#expandLink'+id).show();
    $('tr.discount_group'+id).addClass('dis_collapse');
}
function expandDiscount(id){
    $('a#collapseLink'+id).show();
    $('a#expandLink'+id).hide();
    $('tr.discount_group'+id).removeClass('dis_collapse');
}
Calendar.setup ({
    inputField : "pay_dtl_invoice_date_1",
    daFormat : cal_date_format,
    button : "pay_dtl_invoice_date_1_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});
Calendar.setup ({
    inputField : "pay_dtl_invoice_date_2",
    daFormat : cal_date_format,
    button : "pay_dtl_invoice_date_2_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});
Calendar.setup ({
    inputField : "pay_dtl_invoice_date_3",
    daFormat : cal_date_format,
    button : "pay_dtl_invoice_date_3_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});
Calendar.setup ({
    inputField : "pay_dtl_invoice_date_4",
    daFormat : cal_date_format,
    button : "pay_dtl_invoice_date_4_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});
Calendar.setup ({
    inputField : "pay_dtl_invoice_date_5",
    daFormat : cal_date_format,
    button : "pay_dtl_invoice_date_5_trigger",
    singleClick : true,
    dateStr : "",
    step : 1,
    weekNumbers:false
});

function add_package_hour(amount, hours, course_name, course_id, course_type, course_koc, isCustom){
    var label = '<li title="'+course_name+'" style="color: #002bff;padding: 5px 5px 16px 5px;max-width: 220px;text-overflow: ellipsis;overflow: hidden !important;"><span>×</span>'+course_name+'</li>';
    var list = '<tr id = "'+course_id+'">';
    list += '<td style="padding: 0px !important;" width="40%"><input class="currency input_readonly c_package_amount" readonly type="text" name="c_package_amount[]" size="20" value="'+Numeric.toFloat(amount)+'" title="'+course_name+'" style="font-weight: bold;color: rgb(165, 42, 42);text-align: right;"></td>';
    list += '<td width="25%" scope="col">'+DOTB.language.get('J_Payment', 'LBL_PACKAGE_HOURS')+': <span class="required">*</span></td>';

    list += '<td width="35%">'+"<input type='hidden' class='c_course_koc' name='c_course_koc[]' value='"+course_koc+"'>"+'<input type="hidden" name="c_package_select[]" value=""><input type="hidden" class="c_package_type" name="c_package_type[]" value="'+course_type+'"><input type="hidden" class="c_course_id" name="c_course_id[]" value="'+course_id+'">';
    if(isCustom) list += '<input type="text" class="c_package_hours" name="c_package_hours[]" value="'+Numeric.toFloat(hours,2,2)+'" size="7" maxlength="10" style="color: rgb(165, 42, 42);">';
    else list += '<input type="text" class="c_package_hours input_readonly" readonly name="c_package_hours[]" value="'+Numeric.toFloat(hours,2,2)+'" size="7" maxlength="10" style="color: rgb(165, 42, 42);">';
    list += '</td></tr>';

    return [label,list]
}

function add_package_select(json_exp, exp_selected, customVal, price, hours, course_name, course_id, course_type, course_koc, isCustom){
    var label = '<li title="'+course_name+'" style="color: #002bff;padding: 5px 5px 16px 5px;max-width: 220px;text-overflow: ellipsis;overflow: hidden !important;"><span>×</span>'+course_name+'</li>';
    var unit_label = DOTB.language.languages['app_list_strings']['unit_coursefee_list'][course_type];
    var options = '';
    if(json_exp != '' && json_exp != null) json_exp = JSON.parse(json_exp)

    if(isCustom) json_exp = addToObject(json_exp,_.size(json_exp)+1,'custom');
    $.each(json_exp, function(idx, obj_hour){
        options += '<option cType="'+course_type+'" value="'+obj_hour+'"';
        if(exp_selected === obj_hour) options += ' selected ';
        if(obj_hour == 'custom') options += '>'+DOTB.language.get('J_Payment', 'LBL_CUSTOM')+'</option>';
        else options += '>'+obj_hour+' '+unit_label+'</option>';
    });
    var list = '<tr id = "'+course_id+'">';
    list += '<td style="padding: 0px !important;width: 40%;"><select class="c_package_select" name="c_package_select[]" style="width: 50%;">'+options+'</select><input type="hidden" class="currency c_package_amount" name="c_package_amount[]" value="">';
    list += '<input type="text" style="display:none; width: 30%; margin-left:10px; text-align:center;" class="c_package_custom" name="c_package_custom[]" value="'+customVal+'"></td>';
    list += '<td width="25%" scope="col">'+DOTB.language.get('J_Payment', 'LBL_PACKAGE_HOURS')+': <span class="required">*</span></td>';
    list += '<td width="35%">'+"<input type='hidden' class='c_course_koc' name='c_course_koc[]' value='"+course_koc+"'>"+'<input type="hidden" class="c_package_type" name="c_package_type[]" value="'+course_type+'"><input type="hidden" class="c_course_id" name="c_course_id[]" value="'+course_id+'"><input type="text" class="currency input_readonly c_package_hours" name="c_package_hours[]" value="'+Numeric.toFloat(hours,2,2)+'" size="7" maxlength="10" style="color: rgb(165, 42, 42);"></td>';
    list += '</tr>';
    return [label,list]
}

var addToObject = function (obj, key, value, index) {

    // Create a temp object and index variable
    var temp = {};
    var i = 0;

    // Loop through the original object
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {

            // If the indexes match, add the new item
            if (i === index && key && value) {
                temp[key] = value;
            }

            // Add the current item in the loop to the temp obj
            temp[prop] = obj[prop];

            // Increase the count
            i++;

        }
    }

    // If no index, add to the end
    if (!index && key && value) {
        temp[key] = value;
    }

    return temp;

};

function toggleAutoEnroll(){
    var payment_type = $('#payment_type').val();
    if(payment_type == 'Cashholder'){
        $('#is_auto_enroll').closest('td').show().prev().show();
        $('#is_auto_enroll_label').find('#aelbl2').hide().closest('td').find('#aelbl1').show();
        $('#enroll_hours_label').closest('td').css('visibility', 'visible').prev().css('visibility', 'visible');
        $('#ju_class_koc_label').closest('tr').show();
        $('#is_allow_ost').closest('tr').show();
        $('#end_study_label').show().closest('td').find('.dateTime').hide();
    }else{
        $('#is_auto_enroll').closest('td').show().prev().show();
        $('#is_auto_enroll_label').find('#aelbl1').hide().closest('td').find('#aelbl2').show();
        $('#enroll_hours_label').closest('td').css('visibility', 'hidden').prev().css('visibility', 'hidden');
        $('#ju_class_koc_label').closest('tr').hide();
        $('#is_allow_ost').closest('tr').hide();
        $('#end_study_label').hide().closest('td').find('.dateTime').show();
        removeFromValidate('EditView', 'is_auto_enroll');
    }
}
//Calculate all function
function calculatedAll(){
    caculated();
    submitSponsor();
    submitDiscount();
    submitLoyalty();
    submitSponsor();
    caculated();
}