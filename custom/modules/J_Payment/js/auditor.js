$(document).ready(function() {
    setInterval(ajaxListPaidList, 15000);
    //ajaxListPaidList();
});

function ajaxListPaidList() {
    var listUP  = {};
    $('.receipt_status').each(function(_ind, _op) {
        var rcId     = $(_op).attr('id');
        var rcAa     = $(_op).attr('audited_amount');
        var rcStatus = $(_op).val();
        if(rcStatus == 'Unpaid'){
            listUP[rcId]                   = [];
            listUP[rcId]['id']             = rcId;
            listUP[rcId]['status']         = rcStatus;
            listUP[rcId]['audited_amount'] = rcAa;
        }
    });
    if(Object.keys(listUP).length > 0){
        $.ajax({
            type: "POST",
            url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
            data: {
                type: "ajaxListPaidList",
                payment_id: $("input[name='record']").val(),
            },
            dataType: "json",
            success: function (data) {
                if (data.success == 1) {
                    console.log('ajaxListPaidList');
                    var count_change = 0;
                    $.each(data.rcList, function( _id, _rcNew ) {
                        if(typeof listUP[_id] == 'undefined' || listUP[_id] == null) return;
                        if(listUP[_id]['status'] != _rcNew['status']
                            || listUP[_id]['audited_amount'] != _rcNew['audited_amount']){
                            count_change++;
                            toastr.success(_rcNew['alert'], {
                                positionClass: 'toast-bottom-right',
                                closeButton: true,
                                preventDuplicates: true,
                            });
                        }
                    });

                    if(count_change > 0){
                        $('#pmd_paid_amount').text(data.paid_amount);
                        $('#pmd_unpaid_amount').text(data.unpaid_amount);
                        $('#remain_amount').text(data.remain_amount);
                        $('#remain_hours').text(data.remain_hours);
                        showSubPanel('payment_paymentdetails', null, true);
                    }
                }
            },
        });
    }
}