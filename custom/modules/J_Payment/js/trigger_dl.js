
//TRIGGER DO WHEN
DOTB.util.doWhen(
    function() {
        return $('a[data-subpanel=whole_subpanel_payment_paymentdetails]').length == 1;
    },
    function() {
        $('a[data-subpanel=whole_subpanel_payment_paymentdetails]').trigger('click');
});