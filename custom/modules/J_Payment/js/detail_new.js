var record_id = $('input[name=record]').val();
$(document).ready(function () {
    quickAdminEdit('j_payment', 'sale_type');
    quickAdminEdit('j_payment', 'sale_type_date');
    quickAdminEdit('j_payment', 'assigned_user_id');
    quickAdminEdit('j_payment', 'user_closed_sale_id');
    quickAdminEdit('j_payment', 'user_pt_demo_id');
    quickAdminEdit('j_payment', 'payment_expired');

    $('.inventory_id').live("click", function () {
        var inventory_id = this.getAttribute('inventory_id');
        window.open("index.php?module=J_Inventory&action=exportInventory&record=" + inventory_id, '_blank');
    });

    $("#dt_payment_amount").change(function () {
        var value = Numeric.parse($(this).val());
        checkPaymentAmount(value);
    });

    $("a.btn_view_invoice").live('click', function () {
        $('a[data-subpanel=whole_subpanel_payment_invoices]').trigger('click').focus();
        $('#inv_' + $(this).text()).closest('tr').find('td').effect("highlight", {color: '#ff9933'}, 1000);
    });
    //Mask Input
    $('#inv_code').mask("0000000", {placeholder: "________"});
    $('#pos_code').mask("0000000", {placeholder: "________"});
    $('#bank_account').closest('tr').hide();
    $('#payment_method').live('change', function () {
        $('#card_type').val('').hide();
        $('#bank_type').val('').hide();
        $('#method_note').val('').hide();
        $('#bank_account').val('').closest('tr').hide();
        $('#pos_code').val('').closest('tr').hide();

        switch ($('#payment_method').val()) {
            case 'Bank Transfer':
                $('#bank_type').show();
                $('#bank_account').closest('tr').show();
                break;
            case 'Payoo':
            case 'Card':
            case 'CardMPOS':
                $('#card_type').show();
                $('#pos_code').closest('tr').show();
                break;
            case 'Other':
                $('#method_note').show();
                break;
            default:
            // code block
        }
    });
    $('#payment_date_collect').on('change', function () {
        var rs3 = checkDataLockDate($(this).attr('id'));
    });

    $('#btn_dt_cancel').live("click", function () {
        $('.diaglog_payment').dialog('close');
    });

    //Undo moving/ transfer
    $("#btn_undo").on("click", function () {
        undoPayment();
    });

    $('.container-close').click(function () {

    });

    $('#convert_payment').click(function () {
        $('#diaglog_convert_payment').dialog({
            resizable: false,
            width: 500,
            height: 'auto',
            modal: true,
            visible: true,
            beforeClose: function (event, ui) {
                $("body").css({overflow: 'inherit'});
            },

        });
    });

    $('#recal_remain').click(function () {
        $.confirm({
            title: DOTB.language.get('J_Payment', 'LBL_CONFIRM_ALERT_TITLE'),
            content: DOTB.language.get('J_Payment', 'LBL_CONFIRM_UPDATE_REMAIN'),
            buttons: {
                "OK": {
                    btnClass: 'btn-blue',
                    action: function () {
                        DOTB.ajaxUI.showLoadingPanel();
                        $.ajax({
                            type: "POST",
                            url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
                            data: {
                                type: "ajaxRecalRemain",
                                record_id: record_id,
                            },
                            success: function (data) {
                                data = JSON.parse(data);
                                DOTB.ajaxUI.showLoadingPanel();
                                if (data.success == "1") location.reload();
                                else toastr.error(data.errorLabel);
                            },

                        });
                    }
                },
                "Cancel": {
                    text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                    action: function(){
                        //do nothing
                    }
                },
            }
        });
    });

    // Convert Payment
    $('#cp_convert_type').live('change', function () {
        if ($(this).val() == 'To Amount') {
            $('#cp_tuition_hours, #cp_remain_hours').val('0').prop('disabled', true).addClass('input_readonly');
        } else {
            $('#cp_tuition_hours, #cp_remain_hours').val('0').prop('disabled', false).removeClass('input_readonly');
        }
    });

    //Auto calculate
    $("#cp_tuition_hours").keyup(function () {
        var cp_tuition_hours = Numeric.parse($(this).val());
        var cp_remain_amount = Numeric.parse($('#cp_remain_amount').val());
        var cp_payment_amount = Numeric.parse($('#cp_payment_amount').val());
        $('#cp_remain_hours').val(Numeric.toFloat(cp_remain_amount / (cp_payment_amount / cp_tuition_hours), 2, 2))
    });

    $("#cp_remain_hours").keyup(function () {
        var cp_remain_hours = Numeric.parse($(this).val());
        var cp_remain_amount = Numeric.parse($('#cp_remain_amount').val());
        var cp_payment_amount = Numeric.parse($('#cp_payment_amount').val());
        $('#cp_tuition_hours').val(Numeric.toFloat(cp_payment_amount / (cp_remain_amount / cp_remain_hours), 2, 2))
    });

    $('#cp_tuition_hours, #cp_remain_hours').live('change', function () {
        $(this).val(Numeric.toFloat($(this).val(), 2, 2))
    });

    $('#cp_convert_type').trigger('change');
    $('#btn_submit_convert').live('click', function () {
        if ($("#cp_convert_type").val() == 'To Hour' && ($('#cp_tuition_hours').val() == 0 || $('#cp_remain_hours').val() == 0)) {
            toastr.error('Hour must be greater than 0 !');
            return;
        }
        //Submit class in progress
        DOTB.ajaxUI.showLoadingPanel();
        $.ajax({
            url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
            type: "POST",
            async: true,
            data:
            {
                type: 'ajaxConvertPayment',
                payment_id: $("input[name='record']").val(),
                tuition_hours: $("#cp_tuition_hours").val(),
                remain_hours: $("#cp_remain_hours").val(),
                convert_to_type: $("#cp_convert_type").val(),
            },
            dataType: "json",
            success: function (res) {
                if (res.success == '1') {
                    location.reload();
                    toastr.success(DOTB.language.get('J_Payment', 'LBL_SAVED'));
                } else {
                    toastr.error(DOTB.language.get('J_Payment', 'LBL_SAVING_ERROR'));
                    $('#diaglog_convert_payment').dialog("close");
                    $("#cp_tuition_hours").val('');
                }
                DOTB.ajaxUI.hideLoadingPanel();
            },
        });
    });
    //END: Convert Payment

    $('#btn_add_display_info, #btn_select_display_info').live('click', function () {
        open_popup("Accounts", 1000, 700, "", true, true, {
            "call_back_function": "set_return_corp",
            "form_name": "EditView",
            "field_to_name_array": {
                "id": "account_id",
                "name": "account_name",
            },
            }, "Select", true);
    });

    $('#btn_add_related_payment').live('click', function () {
        var parent_id = $('#parent_id').val();
        var current_payment_id = $('input[name=payment_id]').val();
        open_popup("J_Payment", 1000, 700, "&advanced_parent_id=" + parent_id + "&advanced_not_id=" + current_payment_id, true, true, {
            "call_back_function": "set_related_payment_return",
            "form_name": "EditView",
            "field_to_name_array": {
                "id": "payment_id",
                "name": "payment_name",
                "payment_type": "payment_type",
                "payment_amount": "payment_amount",
            },
            }, "Select", true);
    });

    $('#btn_delete_display_info').live('click', function () {
        addEvatCorporate('');
    });
    //    setTimeout(function() {
    //      $("[data-subpanel=whole_subpanel_payment_paymentdetails]").trigger('click');
    //    }, 1000);

});

function set_return_corp(popup_reply_data) {
    var account_id = '';
    var account_name = '';
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key) {
                case 'account_id':
                    account_id = val;
                    break;
                case 'account_name':
                    account_name = val;
                    break;
            }
        }
    }

    addEvatCorporate(account_id);
}

function set_related_payment_return(popup_reply_data) {
    var payment_id = payment_name = payment_type = payment_amount = '';

    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key) {
                case 'payment_id':
                    payment_id = val;
                    break;
                case 'payment_name':
                    payment_name = val;
                    break;
                case 'payment_type':
                    payment_type = val;
                    break;
                case 'payment_amount':
                    payment_amount = val;
                    break;
            }
        }
    }


    //Check duplicate

    var current_payment_id = $('input[name=payment_id]').val();
    var countDuplicate = 0
    $(".extPaymentId").each(function (i, val) {
        if ($(this).val() == payment_id) countDuplicate++;
    });
    if (current_payment_id == payment_id) countDuplicate++;
    if (countDuplicate > 0) {
        toastr.error(DOTB.language.get('J_Payment', 'LBL_ERROR_LINK_PAYMENT'));
        return false;
    }
    //Add div
    $("#relatedPayment").append('<div class="extPayment">' + payment_name + ' (' + Numeric.toFloat(payment_amount) + ')<span class="link error" style="cursor:pointer;" title="Click to remove" onclick="$(this).parent().remove();autoGetNextInvoice();"> <i class="far fa-minus-circle"></i></span> <input type="hidden" class="extPaymentId" value="' + payment_id + '"> </div>');

    autoGetNextInvoice();
}

function addEvatCorporate(account_id) {
    if (account_id != '')
        var action_type = 'add';
    else
        var action_type = 'delete';

    var payment_id = $('input[name=record]').val();
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data: {
            type: "addEvatCorporate",
            account_id: account_id,
            payment_id: payment_id,
            action_type: action_type,
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data.success == "1") {
                toastr.success(DOTB.language.get('J_Payment', 'LBL_SAVED'));
                autoGetNextInvoice();
            } else {
                toastr.error(DOTB.language.get('J_Payment', 'LBL_SAVING_ERROR'));
            }
        },
    });
}

//Tới đây rồi !!!
function autoGetNextInvoice() {
    if ($('#nextInvoice').length > 0) {
        var payment_id = $('input[name=payment_id]').val();
        var payment_detail_id = $('input[name=payment_detail_id]').val();
        var extPaymentId = {};
        $(".extPaymentId").each(function (i, val) {
            extPaymentId[i] = $(this).val();
        });
        $.ajax({
            type: "POST",
            url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
            data: {
                type: "autoGetNextInvoice",
                team_id: $('#team_id').val(),
                payment_id: payment_id,
                payment_detail_id: payment_detail_id,
                extPaymentId: extPaymentId,
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.success == "1") {
                    $('#nextInvoice').text(' - Serial:'+data.serial + ' - Template:'+data.template_code);
                    if (data.fullStudentName == '') $('.studentRadio').hide();
                    else $('.studentRadio').show();
                    if (data.guardianName == '') $('.guardianRadio').hide();
                    else $('.guardianRadio').show();
                    if (data.guardianName2 == '') $('.guardian2Radio').hide();
                    else $('.guardian2Radio').show();
                    if (data.BuyerDisplayName == '') $('.companyRadio').hide();
                    else $('.companyRadio').show();
                    $('#studentRadio').next().text(data.fullStudentName);
                    $('#guardianRadio').next().text(data.guardianName);
                    $('#guardian2Radio').next().text(data.guardianName2);
                    $('#companyRadio').next().text(data.BuyerDisplayName);
                    $('#buyerNullRadio').next().text(data.BuyerNameNull);
                    $('#BuyerDisplayName').html(data.BuyerDisplayName);
                    $('#buyerAddressLine').html(data.buyerAddressLine);
                    $('#BuyerTaxCode').html(data.BuyerTaxCode);
                    $('#buyerEmail').html(data.buyerEmail);
                    var parent_type = $('#parent_type').val();
                    $('#btn_edit_legal_info').attr("onclick", "window.top.DOTB.App.router.redirect(\'#"+parent_type+"/" + data.buyerID + "\')");
                    if (data.is_corporate) {
                        $('#btn_edit_display_info').show().attr("onclick", "window.top.DOTB.App.router.redirect(\'#Accounts/" + data.displayID + "\')");
                        $('#btn_delete_display_info').show();
                        $('#btn_add_display_info').hide();
                        $('#buyerEmptyRadio').prop('checked', true);
                        $('.buyerEmptyRadio').show();
                    } else {
                        $('#btn_add_display_info').show();
                        $('#btn_delete_display_info').hide();
                        $('#btn_edit_display_info').hide();
                        $('#studentRadio').prop('checked', true);
                        $('.buyerEmptyRadio').hide();
                    }
                    $('#invoice_detail').html(data.htmlRow);
                    if (data.exportType == 'Payment')
                        $('#tr_relatedPayment').show();

                    if(data.supplier == 'Viettel') //Show button preview
                        $('.btn_preview_evat').show();
                    else $('.btn_preview_evat').hide();
                } else {
                    toastr.error(data.label);
                }
            },
        });
    }
}

function cancel_invoice(invoice_id, payment_id) {
    // prompt dialog
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        title: DOTB.language.get('J_Payment', 'LBL_RECEIPT_NOTY_1'),
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '<label>' + DOTB.language.get('J_Payment', 'LBL_RECEIPT_NOTY_2') + ': </label>' +
        '<input type="text" style="width:50%" class="name form-control" required />' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text  : DOTB.language.get('J_Payment','LBL_SUBMIT'),
                btnClass: 'btn-blue',
                action: function () {
                    var str = this.$content.find('.name').val();
                    ajaxStatus.showStatus('Waiting <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
                    $(".cancel_invoice").attr("disabled", true);
                    $.ajax({
                        type: "POST",
                        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
                        data: {
                            type: "ajaxCancelInvoice",
                            invoice_id: invoice_id,
                            payment_id: payment_id,
                            description: str,
                        },
                        success: function (data) {
                            data = JSON.parse(data);
                            if (data.success == "1") {
                                toastr.success(DOTB.language.get('J_Payment', 'LBL_CANCEL_VAT_SUCCESS'));
                            }else
                                toastr.error(data.errorLabel);

                            showSubPanel('j_invoice_j_payment_1', null, true);
                            showSubPanel('payment_paymentdetails', null, true);
                            ajaxStatus.hideStatus();
                            $(".cancel_invoice").attr("disabled", false);
                        },
                    });
                }
            },
            "Cancel": {
                text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                action: function(){
                    //do nothing
                }
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

function cancel_receipt(payment_detail) {
    // prompt dialog
    $.confirm({
        boxWidth: '40%',
        useBootstrap: false,
        title: DOTB.language.get('J_Payment', 'LBL_RECEIPT_NOTY_1'),
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '<label>' + DOTB.language.get('J_Payment', 'LBL_RECEIPT_NOTY_2') + ': </label>' +
        '<input type="text" style="width:50%" class="name form-control" required />' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text  : DOTB.language.get('J_Payment','LBL_SUBMIT'),
                btnClass: 'btn-blue',
                action: function () {
                    var str = this.$content.find('.name').val();
                    ajaxStatus.showStatus('Waiting <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
                    $(".cancel_invoice").attr("disabled", true);
                    $.ajax({
                        type: "POST",
                        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
                        data: {
                            type: "ajaxCancelReceipt",
                            payment_detail: payment_detail,
                            description: str,
                        },
                        success: function (data) {
                            data = JSON.parse(data);
                            if (data.success == "1") {
                                DOTB.ajaxUI.showLoadingPanel();
                                location.reload();
                            } else {
                                toastr.error(data.errorLabel);
                            }
                            ajaxStatus.hideStatus();
                            $(".cancel_invoice").attr("disabled", false);
                        },
                    });
                }
            },
            "Cancel": {
                text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                action: function(){
                    //do nothing
                }
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

function checkPaymentType() {
    if ($("#payment_type").val() != "Cashholder") {
        // Hide Cashholder field
        $("#tuition_hours").closest("tr").hide();
        $("#amount_bef_discount").closest("tr").hide();
        $("#discount_percent").closest("tr").hide();
        $("#discount_amount").closest("tr").hide();
        $("#total_after_discount").closest("tr").hide();
        $("#final_sponsor").closest("tr").hide();
    }
}

function finish_printing(printing_id) {
    DOTB.ajaxUI.showLoadingPanel();
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data: {
            type: "finish_printing",
            printing_id: printing_id,
        },
        success: function (data) {
            DOTB.ajaxUI.hideLoadingPanel();
            data = JSON.parse(data);
            if (data.success == "1") {
                showSubPanel('payment_paymentdetails', null, true);
                toastr.success(DOTB.language.get('J_Payment', 'LBL_SAVED'));
            } else {
                toastr.error(DOTB.language.get('J_Payment', 'LBL_SAVING_ERROR'));
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            DOTB.ajaxUI.hideLoadingPanel();
        }
    });
}

function ex_invoice_pdf(thisButton) {
    var payment_id = thisButton.getAttribute('payment_id');
    var invoice_id = thisButton.getAttribute('invoice_id');
    $(".textbg_greenDotB").attr("disabled", true);
    ajaxStatus.showStatus('Waiting <img src="custom/include/images/loader32.gif" align="absmiddle" width="32">');
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data: {
            type: "ajaxExportInvoice",
            payment_id: payment_id,
            invoice_id: invoice_id,
        },
        success: function (data) {
            //Download Invoice type PDF
            data = JSON.parse(data);
            if (data.success == "1")
                window.open(data.url);
            else toastr.error(data.label);

            ajaxStatus.hideStatus();
            $(".textbg_greenDotB").attr("disabled", false);
        },
    });
}

function ex_invoice(thisButton) {
    var payment_detail = thisButton.getAttribute('payment_detail_id');
    var is_corporate = $('#is_corporate').val();
    var pdf = thisButton.getAttribute('pdf');
    var pdf_id = thisButton.getAttribute('pdf_id');
    if (pdf == "0") {
        if (is_corporate == '1') {
            var ex_corporate = function () {
                window.open('index.php?module=J_Payment&type=corporate&action=invoiceVoucher&record=' + payment_detail + '&dotb_body_only=true', '_blank');
                confirmExportPopup.destroy(document.body);
            };
            var ex_student = function () {
                window.open('index.php?module=J_Payment&type=student&action=invoiceVoucher&record=' + payment_detail + '&dotb_body_only=true', '_blank');
                confirmExportPopup.destroy(document.body);
            };
            var ex_both = function () {
                window.open('index.php?module=J_Payment&type=both&action=invoiceVoucher&record=' + payment_detail + '&dotb_body_only=true', '_blank');
                confirmExportPopup.destroy(document.body);
            };
            var ex_cancel = function () {
                confirmExportPopup.destroy(document.body);
            };

            var confirm_text = DOTB.language.get('J_Payment', 'LBL_CONFIRM_EXPORT');
            var confirmExportPopup = new YAHOO.widget.SimpleDialog("export_vat_popup", {
                width: "400px",
                draggable: true,
                constraintoviewport: true,
                modal: true,
                fixedcenter: true,
                text: confirm_text,
                bodyStyle: "padding:5px",
                buttons: [{
                    text: DOTB.language.get('J_Payment', 'LBL_CORPORATE'),
                    handler: ex_corporate,
                    isDefault: true
                    }, {
                        text: DOTB.language.get('J_Payment', 'LBL_STUDENT'),
                        handler: ex_student
                    }, {
                        text: DOTB.language.get('J_Payment', 'LBL_BOTH'),
                        handler: ex_both
                    }, {
                        text: DOTB.language.get('J_Payment', 'LBL_CANCEL'),
                        handler: ex_cancel
                }]
            });

            confirmExportPopup.setHeader(DOTB.language.get('J_Payment', 'LBL_CONFIRM'));
            confirmExportPopup.render(document.body);
        } else {
            window.open('index.php?module=J_Payment&type=student&action=invoiceVoucher&record=' + payment_detail + '&dotb_body_only=true', '_blank');
        }
    } else {
        window.open('index.php?module=J_PaymentDetail&type=student&action=dotbpdf&dotbpdf=invoice&record=' + payment_detail + '&dotb_body_only=true&view_type=I&pdf_id=' + pdf_id, '_blank');
    }
}

function send_pay_slip(thisBtt) {
    DOTB.ajaxUI.showLoadingPanel();
    $.ajax({
        type: "POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data: {
            type: "sendPaySlip",
            payment_detail_id: thisBtt.getAttribute('payment_detail_id'),
        },
        dataType: "json",
        success: function (data) {
            if (data.success == "1") toastr.success(data.label2,data.label1);
            else toastr.error(data.label2,data.label1);
            if(data.count_change > 0) showSubPanel('payment_paymentdetails', null, true);
            DOTB.ajaxUI.hideLoadingPanel();
        },

    });
}

function pay(thisButton) {
    var today = DOTB.util.DateUtils.formatDate(new Date());
    $('#payment_method').trigger('change');
    $('#dt_handle_action').val('pay');
    $('#dt_unpaid_amount').val(thisButton.getAttribute('pmd_unpaid_amount'));
    $('#dt_payment_amount').val(thisButton.getAttribute('payment_detail_amount'));
    $('#dt_payment_detail_id').val(thisButton.getAttribute('payment_detail_id'));
    $('#payment_date_collect').val(today).trigger('change');
    $('#bank_account').val(thisButton.getAttribute('bank_account'));
    $('#dt_reference_document').val(thisButton.getAttribute('pmd_reference_document'));
    $('#dt_reference_number').val(thisButton.getAttribute('pmd_reference_number'));
    $('#dt_description').val(thisButton.getAttribute('pmd_description'));
    $('#dt_note').val(thisButton.getAttribute('pmd_note'));
    $('#inv_code').val(thisButton.getAttribute('inv_code'));
    $('#dt_receiver').text(window.top.DOTB.App.user.get('full_name'));
    $('#dt_status').val(thisButton.getAttribute('status'));

    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    if ( screenHeight < 550 || screenWidth < 550 || screenWidth < screenHeight ) {
        var dialogWidth = screenWidth * .70;
        dialogClass = '';
    } else {
        var dialogWidth = screenWidth * .32;
        dialogClass = 'fixed-dialog';
    }
    $('.diaglog_payment').dialog({
        title: DOTB.language.get('J_Payment', 'LBL_DIALOG_RECEIPT'),
        resizable: false,
        width: dialogWidth,
        height: 'auto',
        dialogClass: dialogClass,
        modal: true,
        visible: true,
        beforeClose: function (event, ui) {
            $('#payment_method').val('').trigger('change');
        },
        buttons: {
            "save": {
                click: function () {
                    updatePaymentDetail();
                },
                class: 'button primary btn_dt_save',
                text: DOTB.language.get('J_Payment', 'LBL_SAVE'),
            },
            "cancel": {
                click: function () {
                    $(this).dialog('close');
                    //.....
                },
                class: 'button btn_dt_cancel',
                text: DOTB.language.get('J_Payment', 'LBL_CANCEL'),
            },
        },
    });
}

function edit_invoice(thisButton) {
    var payment_detail = thisButton.getAttribute('payment_detail_id');

    var payment_method = thisButton.getAttribute('payment_method');
    var payment_date = thisButton.getAttribute('payment_date');
    var card_type = thisButton.getAttribute('card_type');
    var bank_type = thisButton.getAttribute('bank_type');
    var inv_code = thisButton.getAttribute('inv_code');
    var pos_code = thisButton.getAttribute('pos_code');
    var unpaid_amount = thisButton.getAttribute('pmd_unpaid_amount');
    var description = thisButton.getAttribute('pmd_description');
    var note = thisButton.getAttribute('pmd_note');
    $('#dt_handle_action').val('edit');
    var payment_amount = thisButton.getAttribute('payment_detail_amount');
    $('#dt_payment_detail_id').val(thisButton.getAttribute('payment_detail_id'));
    $('#dt_unpaid_amount').val(Numeric.toFloat(Numeric.parse(unpaid_amount) + Numeric.parse(payment_amount)));
    $('#dt_payment_amount').val(payment_amount);
    $('#payment_method').val(payment_method).trigger('change');
    $('#payment_date_collect').val(payment_date).trigger('change');
    $('#card_type').val(card_type);
    $('#bank_type').val(bank_type);
    $('#pos_code').val(pos_code);
    $('#inv_code').val(inv_code);
    $('#dt_description').val(description);
    $('#dt_note').val(note);
    $('#dt_receiver').text(thisButton.getAttribute('assigned_user_name'));
    $('#dt_status').val(thisButton.getAttribute('status'));

    $('#bank_account').val(thisButton.getAttribute('bank_account'));
    $('#dt_reference_document').val(thisButton.getAttribute('pmd_reference_document'));
    $('#dt_reference_number').val(thisButton.getAttribute('pmd_reference_number'));

    //responsive
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    if ( screenHeight < 550 || screenWidth < 550 || screenWidth < screenHeight ) {
        var dialogWidth = screenWidth * .70;
        dialogClass = '';
    } else {
        var dialogWidth = screenWidth * .32;
        dialogClass = 'fixed-dialog';
    }
    $('.diaglog_payment').dialog({
        title: DOTB.language.get('J_Payment', 'LBL_DIALOG_EDIT_RECEIPT'),
        resizable: false,
        width: dialogWidth,
        minWidth : 500,
        dialogClass : dialogClass,
        resizable: false,
        height: 'auto',
        modal: true,
        visible: true,
        beforeClose: function (event, ui) {
            $('#payment_method').val('').trigger('change');
        },
        buttons: {
            "save": {
                click: function () {
                    updatePaymentDetail();
                },
                class: 'button primary btn_dt_save',
                text: DOTB.language.get('J_Payment', 'LBL_SAVE'),
            },
            "cancel": {
                click: function () {
                    $(this).dialog('close');
                    //.....
                },
                class: 'button btn_dt_cancel',
                text: DOTB.language.get('J_Payment', 'LBL_CANCEL'),
            },
        },
    });
}

function updatePaymentDetail() {
    var payment_date    = $("#payment_date_collect").val();
    var payment_mt      = $('#payment_method').val();
    var card_type       = $('#card_type').val();
    var bank_type       = $('#bank_type').val();
    var bank_account    = $('#bank_account').val();
    var inv_code        = $('#inv_code').val();
    var pos_code        = $('#pos_code').val();
    var description     = $('#dt_description').val();
    var note            = $('#dt_note').val();
    var dt_status       = $('#dt_status').val();
    var value           = Numeric.parse($('#dt_payment_amount').val());
    var handle_action   = $('#dt_handle_action').val();
    if (!checkPaymentAmount(value)) return;
    else if(handle_action == 'pay' || dt_status == 'Paid'){
        if (payment_mt == '' || payment_mt == null) {
            $('#payment_method').effect("highlight", {color: 'red'}, 2000);
            toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_FILL_OUT'));
            return;
        }
        if (payment_date == '') {
            $('#payment_date_collect').effect("highlight", {color: 'red'}, 2000);
            toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_FILL_OUT'));
            return;
        }
        if ((value == '') || (value < 0)) {
            $('#dt_payment_amount').effect("highlight", {color: 'red'}, 2000);
            toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_FILL_OUT'));
            return;
        }

        if (payment_mt == 'Card' || payment_mt == 'CardMPOS') {
            if (card_type == '' || card_type == null) {
                toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_FILL_OUT'));
                $('#card_type').effect("highlight", {color: 'red'}, 2000);
                return;
            }
            if (pos_code.length < 4) {
                toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_FILL_OUT'));
                $('#pos_code').effect("highlight", {color: 'red'}, 2000);
                return;
            }
        }
    }
    $.confirm({
        title: DOTB.language.get('J_Payment', 'LBL_CONFIRM_ALERT_TITLE'),
        content: DOTB.language.get('J_Payment', 'LBL_RECEIPT_NOTY_SAVE'),
        buttons: {
            "OK": {
                btnClass: 'btn-blue',
                action: function () {
                    $(".diaglog_payment").dialog("close");
                    DOTB.ajaxUI.showLoadingPanel();
                    $.ajax({
                        type: "POST",
                        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
                        data: {
                            type: "ajaxUpdatePaymentDetail",
                            payment_detail  : $('#dt_payment_detail_id').val(),
                            payment_method  : payment_mt,
                            card_type       : card_type,
                            bank_type       : bank_type,
                            bank_account    : bank_account,
                            payment_amount  : value,
                            reference_document  : $('#dt_reference_document').val(),
                            reference_number    : $('#dt_reference_number').val(),
                            method_note         : $('#method_note').val(),
                            pos_code            : pos_code,
                            inv_code            : inv_code,
                            description         : description,
                            note                : note,
                            payment_date        : payment_date,
                            handle_action       : handle_action,
                        },
                        success: function (data) {
                            DOTB.ajaxUI.hideLoadingPanel();
                            data = JSON.parse(data);
                            if (data.success == "1") {
                                $('#pmd_paid_amount').text(data.paid);
                                $('#pmd_unpaid_amount').text(data.unpaid);
                                $('#remain_amount').text(data.remain_amount);
                                $('#remain_hours').text(data.remain_hours);
                                showSubPanel('payment_paymentdetails', null, true);

                                if (data.sale_type != '' && data.sale_type != null) {
                                    $('#label_sale_type').text(data.sale_type);
                                    $('#value_sale_type').val(data.sale_type);
                                }
                                if (data.sale_type_date != '' && data.sale_type_date != null) {
                                    $('#label_sale_type_date').text(data.sale_type_date);
                                    $('#value_sale_type_date').val(data.sale_type_date);
                                }
                                $.each(data.enr_success, function (index, value) {
                                    toastr.success(value);
                                });
                                $.each(data.enr_fail, function (index, value) {
                                    toastr.warning(value);
                                });
                            } else $.alert(data.errorLabel);
                        },

                    });
                }
            },
            "Cancel": {
                text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                action: function(){
                    //do nothing
                }
            },
        }
    });
}

Calendar.setup({
    inputField: "payment_date_collect",
    daFormat: cal_date_format,
    button: "payment_date_trigger",
    singleClick: true,
    dateStr: "",
    step: 1,
    weekNumbers: false
});

function undoPayment() {
    var paymentType = $('#payment_type').val();

    if (paymentType == "Transfer Out" || paymentType == "Transfer In") {
        var rsconfirm = confirm(DOTB.language.get('J_Payment', 'LBL_ALERT_TRANSFER'));
    } else if (paymentType == "Moving Out" || paymentType == "Moving In") {
        var rsconfirm = confirm(DOTB.language.get('J_Payment', 'LBL_ALERT_MOVING'));
    } else if (paymentType == "Refund") {
        var rsconfirm = confirm(DOTB.language.get('J_Payment', 'LBL_ALERT_REFUND'))
    }

    if (rsconfirm) {
        DOTB.ajaxUI.showLoadingPanel();
        $.ajax({
            url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
            type: "POST",
            async: true,
            data: {
                type: 'ajaxUndoPayment',
                payment_id: $('input[name="record"]').val(),
                payment_type: paymentType,
            },
            dataType: "json",
            success: function (res) {
                DOTB.ajaxUI.hideLoadingPanel();
                if (res.success == "1") {
                    var parent_type = $('#parent_type').val();
                    parent.DOTB.App.router.redirect("#"+parent_type+"/" + $('#parent_id').val());
                } else {
                    toastr.error(DOTB.language.get('J_Payment', 'LBL_PAYMENT_IN_USED'));
                }
            },
        });
    }
}

function reloadReleaseOptions() {
    DOTB.ajaxUI.showLoadingPanel();
    showSubPanel('payment_paymentdetails', null, true);
    DOTB.ajaxUI.hideLoadingPanel();
}

Calendar.setup({
    inputField: "value_sale_type_date",
    daFormat: cal_date_format,
    button: "sale_type_date_trigger",
    singleClick: true,
    dateStr: "",
    step: 1,
    weekNumbers: false
});

function validateDateIsBetween(check_date, from, to) {
    if (check_date == '') return true;
    check_date = DOTB.util.DateUtils.parse(check_date, cal_date_format);
    if (from != '' && to != '') {
        from_check = DOTB.util.DateUtils.parse(from, cal_date_format).getTime();
        to_check = DOTB.util.DateUtils.parse(to, cal_date_format).getTime();
        if (check_date == false) {
            toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_DATE_RANGE1') + from + ' - ' + to + '.');
            return false;
        } else {
            check_date = check_date.getTime();
            if (check_date < from_check || check_date > to_check) {
                toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_DATE_RANGE1') + from + ' - ' + to + '.');
                return false;
            }
        }
    } else if (from != '') {
        from_check = DOTB.util.DateUtils.parse(from, cal_date_format).getTime();

        if (check_date < from_check) {
            toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_DATE_RANGE2') + from + '.');
            return false;
        }
    } else {
        to_check = DOTB.util.DateUtils.parse(to, cal_date_format).getTime();
        if (check_date > to_check) {
            toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_DATE_RANGE3') + to + '.');
            return false;
        }
    }
    return true;
}

function daydiff(first, second) {
    return Math.round((second - first) / (1000 * 60 * 60 * 24) + 1);
}

Calendar.setup({
    inputField: "value_payment_expired",
    daFormat: cal_date_format,
    button: "payment_expired_trigger",
    singleClick: true,
    dateStr: "",
    step: 1,
    weekNumbers: false
});

function inArray(array, el) {
    for (var i = array.length; i--;) {
        if (array[i] === el) return true;
    }
    return false;
}

function isEqArrays(arr1, arr2) {
    if (arr1.length !== arr2.length) {
        return false;
    }
    for (var i = arr1.length; i--;) {
        if (!inArray(arr2, arr1[i])) {
            return false;
        }
    }
    return true;
}

function checkPaymentAmount(value) {
    var min = 0;
    var max = Numeric.parse($('#dt_unpaid_amount').val());
    if (value > max || value < min) {
        $('#dt_payment_amount').val('').effect("highlight", {color: 'red'}, 2000);
        toastr.error(DOTB.language.get('J_Payment', 'LBL_ALERT_AMOUNT1') + Numeric.toFloat(min) + ' - ' + Numeric.toFloat(max) + ' !!');
        return false;
    } else
        return true;
}

function get_invoice_no(thisButton) {
    var payment_id = thisButton.getAttribute('payment_id');
    var payment_detail_id = thisButton.getAttribute('payment_detail_id');
    var is_admin_inv = $('#is_admin_inv').val();
    $.confirm({
        title: '<span class="jconfirm-title">INVOICE (VAT)<span id="nextInvoice">-none-</span></span>',
        content:
        '<span style="color:red;"><i class="far fa-exclamation-circle"></i>  ' + DOTB.language.get('J_Payment', 'LBL_CONFIRM_E_INVOICE_INFO') + '</span>' +
        '<br><input id="payment_id" name="payment_id" value = "' + payment_id + '"  type="hidden" >' +
        '<input id="payment_detail_id" name="payment_detail_id" value = "' + payment_detail_id + '"  type="hidden" >' +
        '<input id="confirm_send_to" name="confirm_send_to" value = "0"  type="hidden" >' +
        '<table class="edit view tabForm" style="width: 100%">' +
        '<tbody>' +
        '<tr>' +
        '<td scope="row" valign="top" width="35%">' +
        DOTB.language.get('J_Payment', 'LBL_BUYER_DISPLAY_NAME') + ':' +
        '</td>' +
        '<td width="50%" id="buyerLegalName">' +
        '<div class="studentRadio" style="display: none;">' +
        '  <input type="radio" id="studentRadio" name="buyerLegalName" value="full_student_name" checked>\n' +
        '  <label for="studentRadio"></label>&nbsp(Student)<br>\n' +
        '</div>' +
        '<div class="guardianRadio" style="display: none;">' +
        '  <input type="radio" id="guardianRadio" name="buyerLegalName" value="guardian_name">\n' +
        '  <label for="guardianRadio"></label>&nbsp(Parent 1)<br>\n' +
        '</div>' +
        '<div class="guardian2Radio" style="display: none;">' +
        '  <input type="radio" id="guardian2Radio" name="buyerLegalName" value="guardian_name_2">\n' +
        '  <label for="guardian2Radio"></label>&nbsp(Parent 2)' +
        '</div>' +
        '<div class="companyRadio" style="display: none;">' +
        '  <input type="radio" id="companyRadio" name="buyerLegalName" value="company_name">\n' +
        '  <label for="companyRadio"></label>&nbsp(Company)' +
        '</div>' +
        '<div class="buyerNullRadio" >' +
        '  <input type="radio" id="buyerNullRadio" name="buyerLegalName" value="buyer_name_null">\n' +
        '  <label for="buyerNullRadio"></label>&nbsp(Null)' +
        '</div>' +
        '<div class="buyerEmptyRadio" >' +
        '  <input type="radio" id="buyerEmptyRadio" name="buyerLegalName" value="buyer_name_empty">\n' +
        '  <label for="buyerEmptyRadio"></label>&nbsp(-Empty-)' +
        '</div>' +
        '</td>' +
        '<td width="15%">' +
        '<button type="button" style="float: right" class="button primary" href="" id="btn_edit_legal_info" onclick="">' + DOTB.language.get('J_Payment', 'LBL_EDIT') + '</button>' +
        '</td>' +
        '</tr>' +

        '<tr>' +
        '<td scope="row" valign="top" width="35%">' +
        DOTB.language.get('J_Payment', 'LBL_COMPANY_DISPLAY_NAME') + ':' +
        '</td>' +
        '<td width="35%"><span id="BuyerDisplayName"></span>' + '<a style="display:none;" id="btn_delete_display_info"><i style="font-size: 14px;cursor: pointer;color: #dc0000;padding-left: 5px;" title="' + DOTB.language.get('J_Payment', 'LBL_REMOVE') + '" class="far fa-minus-circle"></i></a>' +
        '</td>' +
        '<td width="30%">' +
        '<button type="button" style="float: right;display:none;" class="button" id="btn_add_display_info" onclick="">' + DOTB.language.get('J_Payment', 'LBL_ADD_COMPANY') + '</button>' +

        '<button type="button" style="float: right;display:none;" class="button" href="" id="btn_edit_display_info" onclick="">' + DOTB.language.get('J_Payment', 'LBL_EDIT') + '</button>' +
        '</td>' +
        '</tr>' +

        '<tr>' +
        '<td scope="row" valign="top" width="35%">' +
        DOTB.language.get('J_Payment', 'LBL_TAX_CODE_DISPLAY_NAME') + ':' +
        '</td>' +
        '<td width="65%" id="BuyerTaxCode">' +
        '</td>' +
        '</tr>' +

        '<tr>' +
        '<td scope="row" valign="top" width="35%">' +
        DOTB.language.get('J_Payment', 'LBL_BUYER_ADDRESS_LINE') + ':' +
        '</td>' +
        '<td width="65%" id="buyerAddressLine">' +
        '</td>' +
        '</tr>' +
        '<tr>' +
        '<td scope="row" valign="top" width="35%">' +
        DOTB.language.get('J_Payment', 'LBL_BUYER_EMAIL') + ':' +
        '</td>' +
        '<td width="65%" id="buyerEmail">' +
        '</td>' +
        '</tr>' +

        '<tr>' +
        '<td scope="row" valign="top" width="35%">' +
        DOTB.language.get('J_Payment', 'LBL_INVOICE_DATE') + ':' +
        '</td>' +
        '<td>' +
        '<span class="dateTime">' +
        ((is_admin_inv == '1') ? '<input class="date_input" size="11" autocomplete="off" type="text"  maxlength="10" style="font-size: 1em" name="invoice_date" id="invoice_date" readonly/>&nbsp<img src="themes/RacerX/images/jscalendar.png" alt="Enter Date" id="invoice_date_trigger" style="vertical-align: middle">' : '<span id = "invoiceDate"></span>') +
        '</span>' +
        '</td>' +
        '</tr>' +

        '<tr id="tr_relatedPayment" style="display:none;">' +
        '<td scope="row" valign="top" width="35%">' +
        DOTB.language.get('J_Payment', 'LBL_SELECT_LINK_PAYMENT') + ':' +
        '</td>' +
        '<td width="65%" colspan="2" id="relatedPayment">' +
        '<button type="button" style="float: right;" class="button" id="btn_add_related_payment">' + DOTB.language.get('J_Payment', 'LBL_BTN_LINK_PAYMENT') + '</button>' +
        '</td>' +
        '</tr>' +


        '<tr>' +
        '<td style="margin-left: 5px" width="35%">' + DOTB.language.get('J_Payment', 'LBL_INVOICE_DETAIL_INFO') + ':' + '</td>' +
        '<td></td>' +
        '</tr>' +

        '<tr>' +
        '<td colspan="3" width="100%">' +
        '<table style="width: 100%" id="invoice_detail">\n' +
        '</table>' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>',
        onContentReady: function () {
            autoGetNextInvoice();
            Calendar.setup({
                inputField: "invoice_date",
                ifFormat: cal_date_format,
                daFormat: cal_date_format,
                button: "invoice_date_trigger",
                singleClick: true,
                dateStr: "",
                startWeekday: 0,
                step: 1,
                weekNumbers: false
            });
            var formated_today = $('#sys_today').val();
            $('#invoice_date').val(formated_today);
            $('#invoiceDate').text(formated_today);
        },
        buttons: {
            "Preview PDF": {
                text  : DOTB.language.get('J_Payment','LBL_E_VAT_PREVIEW'),
                btnClass: 'btn-warning btn_preview_evat hidden',
                action: function () {
                    $(".btn_preview_evat" ).bind( "click", function( event ) {
                        $(".btn_preview_evat" ).prop('disabled',false);
                        ajaxGetPreview(this);
                        return false;
                    });
                    $(".btn_preview_evat" ).prop('disabled',false);
                    ajaxGetPreview(this);
                    return false;
                }
            },
            "Push E-Invoice": {
                text  : DOTB.language.get('J_Payment','LBL_E_VAT_PUSH'),
                btnClass: 'btn-blue btn_push_evat',
                action: function () {
                    $(".btn_push_evat" ).prop('disabled',true);
                    ajaxGetInvoiceNo(this);
                    return false;
                }
            },
            "Cancel": {
                text  : DOTB.language.get('J_Payment','LBL_CANCEL'),
                action: function(){
                    //do nothing
                }
            },
        }
    });
}

function created_book_gift(event) {
    var payment_id = app.controller.context.attributes.model.id;
    var type = "Book/Gift";
    var student_id = app.controller.context.attributes.model.attributes.parent_id;
    app.router.redirect('#bwc/index.php?module=J_Payment&action=EditView&return_module=J_Payment&return_action=DetailView&payment_type=' + type + '&student_id=' + student_id + '&primary_id=' + payment_id);
}


function ajaxGetInvoiceNo(dialog) {
    var buyer_legal_type = $('input[name="buyerLegalName"]:checked').val();
    var invoice_date = $('#invoice_date').val();
    var payment_id = $('input[name=payment_id]').val();
    var payment_detail_id = $('input[name=payment_detail_id]').val();
    var confirm_send_to = $('input[name=confirm_send_to]').val();
    if ((confirm_send_to == '' || typeof confirm_send_to == 'undefined')) confirm_send_to = 0;
    DOTB.ajaxUI.showLoadingPanel();

    var extPaymentId = {};
    $(".extPaymentId").each(function (i, val) {
        extPaymentId[i] = $(this).val();
    });

    $.ajax({
        type: "POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data: {
            type: "ajaxGetInvoiceNo",
            payment_id: payment_id,
            buyer_legal_type: buyer_legal_type,
            payment_detail_id: payment_detail_id,
            invoice_date: invoice_date,
            confirm_send_to: confirm_send_to,
            extPaymentId: extPaymentId,
        },
        dataType: "json",
        success: function (data) {
            DOTB.ajaxUI.hideLoadingPanel();
            if (data.success == 1) {
                dialog.close();
                showSubPanel('payment_paymentdetails', null, true);
                showSubPanel('j_invoice_j_payment_1', null, true);
                toastr.success(app.lang.get('LBL_GET_INVOICE_NO_SUCCESS', 'J_Payment'));
            } else if (data.success == 3) {
                app.alert.show('message-id', {
                    level: 'confirmation',
                    messages: data.label,
                    autoClose: false,
                    onConfirm: function () {
                        $('input[name=confirm_send_to]').val(1);
                        ajaxGetInvoiceNo(dialog);
                        dialog.close();
                    },
                    onCancel: function () {
                        dialog.close();
                        return false;
                    }
                });
            } else if (data.success == 99){
                dialog.close();
                toastr.warning(data.label);
            } else {
                dialog.close();
                toastr.error(data.label);
            }
        },
    });
}

function ajaxGetPreview(dialog) {
    var buyer_legal_type = $('input[name="buyerLegalName"]:checked').val();
    var invoice_date = $('#invoice_date').val();
    var payment_id = $('input[name=payment_id]').val();
    var payment_detail_id = $('input[name=payment_detail_id]').val();
    var confirm_send_to = $('input[name=confirm_send_to]').val();
    if ((confirm_send_to == '' || typeof confirm_send_to == 'undefined')) confirm_send_to = 0;
    DOTB.ajaxUI.showLoadingPanel();

    var extPaymentId = {};
    $(".extPaymentId").each(function (i, val) {
        extPaymentId[i] = $(this).val();
    });

    $.ajax({
        type: "POST",
        url: "index.php?module=J_Payment&action=handleAjaxPayment&dotb_body_only=true",
        data: {
            type: "ajaxGetPreview",
            payment_id: payment_id,
            buyer_legal_type: buyer_legal_type,
            payment_detail_id: payment_detail_id,
            invoice_date: invoice_date,
            confirm_send_to: confirm_send_to,
            extPaymentId: extPaymentId,
        },
        dataType: "json",
        success: function (data) {
            DOTB.ajaxUI.hideLoadingPanel();
            if(data.success == 4){
                var pdfWindow = window.open("");
                pdfWindow.document.write("<html><head><title>Preview PDF Invoice</title></head><body><iframe title='My Invoice'  width='100%' height='100%' src='data:application/pdf;base64, " +    encodeURI(data.base64PDF) + "'></iframe></body></html>");
            }
            else toastr.error(data.label);
        },
    });
}

