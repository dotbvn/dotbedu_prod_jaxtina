<?php
require_once("custom/include/_helper/junior_revenue_utils.php");
require_once("custom/include/_helper/junior_class_utils.php");
require_once("custom/modules/J_Payment/HelperPayment.php");
class logicPayment{
    //Tạo hoàn loạt Payment theo lớp
    function generateClassPayment(&$bean, $event, $arguments){
        if($bean->parent_type == 'J_Class'
        && !empty($bean->parent_id)
        && !empty($_POST['std_student_list'])){
            $std_pm_list = array();
            foreach($_POST['std_student_list'] as $stID){
                $student = BeanFactory::getBean('Contacts',$stID, array('disable_row_level_security'=>true));
                $pmStd = new J_Payment();
                foreach ($pmStd->field_defs as $keyField => $aFieldName)
                    $pmStd->$keyField = $bean->$keyField;
                $pmStd->parent_type = 'Contacts';
                $pmStd->parent_id = $stID;
                $pmStd->id = '';
                $pmStd->deleted = 0;
                $pmStd->team_id = $student->team_id;
                $pmStd->team_set_id = $student->team_id;
                $pmStd->save();
                if(!empty($pmStd->id)){
                    $std_pm_list[$pmStd->id]['student_name'] = $GLOBALS['db']->getOne("SELECT full_student_name FROM contacts WHERE id = '$stID'");
                    $std_pm_list[$pmStd->id]['payment_name'] = $pmStd->name;
                }
            }
            foreach($std_pm_list as $pmID => $stdName)
                $link .= '<li><a target="_blank" href="#J_Payment/'.$pmID.'">'.$stdName['student_name'].' - <b>'.$stdName['payment_name'].'</b></a></li>';

            $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('".$bean->parent_type."','".$bean->parent_id."'));";
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'success\',
            messages: \'Created '.count($std_pm_list).' payments.<br>'.$link.'Click on links above for details!\',
            autoClose: false
            });
            '.$route.'
            </script>';
            die();
        }
    }

    function after_delete_payment(&$bean, $event, $arguments){
        //Update student status
        if(!empty($bean->last_parent_id) && $bean->parent_type == 'Contacts') updateStudentStatus($bean->last_parent_id);
    }

    //Before delete
    function deletedPayment(&$bean, $event, $arguments){
        global $current_user, $timedate;
        if(empty($bean->payment_type) || empty($bean->id)){
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: \'Something Wrong, Please, try again !!\',
            autoClose: true
            });
            '.$route.'
            </script>';
            die();
        }
        //        //User:admin -> Smart delete
        //        if($current_user->id == '1'){
        //            //xoá học viên khỏi lớp
        //            $q1 = "SELECT DISTINCT
        //            IFNULL(j_payment.id, '') primaryid, IFNULL(l1.id, '') enr_id
        //            FROM j_payment INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_idb AND l1_1.deleted = 0
        //            INNER JOIN j_payment l1 ON l1.id = l1_1.payment_ida AND l1.deleted = 0
        //            WHERE (j_payment.id = '{$bean->id}') AND (IFNULL(l1.payment_type, '') = 'Enrollment') AND j_payment.deleted = 0";
        //            $enrs = $GLOBALS['db']->fetchArray($q1);
        //            foreach($enrs as $key_ => $enr){
        //                $enr = BeanFactory::getBean('J_Payment', $enr['enr_id'], array('disable_row_level_security'=>true));
        //                $enr->mark_deleted($enr->id);
        //            }
        //            $pmdS = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(id, '') id, IFNULL(status, '') status, IFNULL(payment_amount, 0) payment_amount FROM j_paymentdetail WHERE payment_id = '{$bean->id}' AND status <> 'Cancelled' AND deleted = 0");
        //            foreach($pmdS as $id => $pmd){
        //                //Update deleted query
        //                $GLOBALS['db']->query("UPDATE j_paymentdetail SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE id='{$pmd['id']}'");
        //                $GLOBALS['db']->query("DELETE FROM audit_events WHERE parent_id='{$pmd['id']}'");
        //                $GLOBALS['db']->query("DELETE FROM j_paymentdetail_audit WHERE parent_id='{$pmd['id']}'");
        //            }
        //        }

        //Default Delete
        $pmdS      = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(id, '') id, IFNULL(status, '') status, IFNULL(payment_amount, 0) payment_amount FROM j_paymentdetail WHERE payment_id = '{$bean->id}' AND status <> 'Cancelled' AND deleted = 0");
        $countPAID = 0;
        foreach($pmdS as $id => $pmd) {if($pmd['status'] == 'Paid' && $pmd['payment_amount'] > 0) $countPAID++;}
        $countUsed = $GLOBALS['db']->getOne("SELECT count(id) count FROM j_payment_j_payment_1_c WHERE payment_idb = '{$bean->id}' AND deleted = 0");
        //Check Paid
        if(($countPAID > 0) || ($countUsed > 0)){
            $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Payment','".$bean->id."'));";
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: \''.str_replace(array("\r", "\n"), '', nl2br(translate('LBL_ALERT_DELETE_RECEIPT', 'J_Payment'))).'\',
            autoClose: false
            });'.$route.'</script>';
            die();
        }

        foreach($pmdS as $id => $pmd){
            $beanPmdt = BeanFactory::getBean('J_PaymentDetail', $pmd['id'], array('disable_row_level_security'=>true));
            $beanPmdt->mark_deleted($beanPmdt->id);
        }
        //Invoice
        $j_invID  = $GLOBALS['db']->getOne("SELECT IFNULL(id, '') id FROM j_invoice WHERE payment_id='{$bean->id}' AND deleted = 0");
        if(!empty($j_invID)){
            $j_inv = BeanFactory::getBean('J_Invoice', $j_invID, array('disable_row_level_security'=>true));
            $j_inv->mark_deleted($j_inv->id);
        }

        revokeSaleType($bean->id);
        removeRelatedPayment($bean->id);
        $GLOBALS['db']->query("UPDATE j_coursefee_j_payment_2_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE j_coursefee_j_payment_2j_payment_idb='{$bean->id}'");
        $GLOBALS['db']->query("UPDATE j_payment_j_discount_1_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE j_payment_j_discount_1j_payment_ida = '{$bean->id}'");
        $GLOBALS['db']->query("UPDATE j_sponsor SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE payment_id = '{$bean->id}' AND deleted = 0");
        $GLOBALS['db']->query("UPDATE j_loyalty SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE payment_id = '{$bean->id}' AND deleted = 0");
        $GLOBALS['db']->query("UPDATE c_commission SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE payment_id = '{$bean->id}' AND deleted = 0");
        $GLOBALS['db']->query("UPDATE c_deliveryrevenue SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE ju_payment_id = '{$bean->id}' AND deleted = 0");
        if($bean->payment_type == 'Enrollment' || $bean->payment_type == 'Delay'){
            // Remove Student from Situation Related
            $q1 = "SELECT DISTINCT IFNULL(id, '') id FROM j_studentsituations WHERE payment_id='{$bean->id}' AND deleted = 0";
            $rs1 = $GLOBALS['db']->query($q1);
            while($rowo = $GLOBALS['db']->fetchByAssoc($rs1)){
                $sitChange = BeanFactory::getBean('J_StudentSituations', $rowo['id'], array('disable_row_level_security'=>true));
                $sitChange->mark_deleted($sitChange->id);
            }
        }
        if($bean->payment_type == 'Book/Gift'){
            $bean->kind_of_course = '';
            $q101 = "SELECT DISTINCT IFNULL(l1.id, '') l1_id
            FROM j_payment
            INNER JOIN j_payment_j_inventory_1_c l1_1 ON j_payment.id = l1_1.j_payment_j_inventory_1j_payment_ida AND l1_1.deleted = 0
            INNER JOIN j_inventory l1 ON l1.id = l1_1.j_payment_j_inventory_1j_inventory_idb AND l1.deleted = 0
            WHERE (j_payment.id = '{$bean->id}') AND j_payment.deleted = 0";
            $rs101 = $GLOBALS['db']->query($q101);
            while($row101 = $GLOBALS['db']->fetchByAssoc($rs101)){
                $GLOBALS['db']->query("UPDATE j_inventorydetail SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE inventory_id = '{$row101['l1_id']}'");
                $GLOBALS['db']->query("UPDATE j_payment_j_inventory_1_c SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE j_payment_j_inventory_1j_inventory_idb = '{$row101['l1_id']}'");
                $GLOBALS['db']->query("UPDATE j_inventory SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE id = '{$row101['l1_id']}'");
            }
        }
        //Remove Outstanding
        self::removeOutStanding($bean);
    }
    //before save
    function handleBeforeSave($bean, $event, $arguments) {
        if($bean->payment_type == 'Transfer Out' || $bean->payment_type == 'Moving Out' || $bean->payment_type == 'Refund'){
            $student = BeanFactory::getBean("Contacts", $bean->parent_id);
            $bean->team_id = $student->team_id;
            $bean->team_set_id = $student->team_id;
        }

        $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('".$bean->parent_type."','".$bean->parent_id."'));";
        if(($_POST['module'] == $bean->module_name)
        && ($_POST['action'] == 'Save')
        && ($bean->payment_type != 'Enrollment')){ //FIX LỖI: Auto-Enroll trigger Edit Enrollment vô tình xoá học viên Khỏi lớp
            if(empty($bean->payment_type)){
                if(!empty($bean->parent_id)){
                    echo '<script type="text/javascript">
                    window.parent.DOTB.App.alert.show(\'message-id\', {
                    level: \'error\',
                    messages: \'Something Wrong, Please, try again !!\',
                    autoClose: true
                    });
                    '.$route.'
                    </script>';
                }else{
                    echo '<script type="text/javascript">
                    window.parent.DOTB.App.alert.show(\'message-id\', {
                    level: \'error\',
                    messages: \'Something Wrong, Please, try again !!\',
                    autoClose: true
                    });
                    window.parent.DOTB.App.router.redirect(\'#Home\');
                    </script>';
                }
                die();
            }
            global $timedate, $app_list_strings, $current_user;
            //get list sponsor_number before delete relationship
            $qGetSponsorNumber = "SELECT GROUP_CONCAT(sponsor_number) FROM j_sponsor WHERE payment_id = '{$bean->id}' AND deleted=0";
            $listSponsorNumber = explode(',' , $GLOBALS['db']->getOne($qGetSponsorNumber));

            //Delete relationship before edit
            //FIX LỖI: Auto-Enroll trigger Edit Enrollment vô tình xoá học viên Khỏi lớp
            if(!empty($bean->fetched_row) && ($bean->payment_type != 'Enrollment')) logicPayment::deletedPayment($bean, $event, $arguments);
            if(($bean->payment_type == 'Cashholder')){
                //Add Relationship Course Fee
                $arrCf = $bean->j_coursefee_j_payment_1j_coursefee_ida;
                //DEBUG
                if($bean->load_relationship('j_coursefee_j_payment_1'))
                    $bean->j_coursefee_j_payment_1->add($arrCf[0]);

                if($bean->load_relationship('j_coursefee_j_payment_2'))
                    $bean->j_coursefee_j_payment_2->add($arrCf);

                $expired_term = '0 day';
                $kindOfCourse = array();
                foreach($arrCf as $course_id){
                    $course_fee = BeanFactory::getBean('J_Coursefee', $course_id);
                    if(!empty($course_fee->expired_term))
                        $expired_term .= ' +'.$course_fee->expired_term;
                    $kindOfCourse = array_unique(array_merge($kindOfCourse,unencodeMultienum($course_fee->kind_of_course)), SORT_REGULAR);
                }
                if($expired_term != '0 day') $bean->expired_term = $expired_term;
                if(!empty($kindOfCourse)) $bean->kind_of_course = encodeMultienumValue($kindOfCourse);

                //Set course Fee List
                $courseFees = array();

                $c_course_id = $_POST['c_course_id'];
                for($i = 0; $i < count($c_course_id); $i++){
                    $courseFees[$c_course_id[$i]]['type']   = $_POST['c_package_type'][$i];
                    $courseFees[$c_course_id[$i]]['select'] = $_POST['c_package_select'][$i];
                    $courseFees[$c_course_id[$i]]['hours']  = unformat_number($_POST['c_package_hours'][$i]);
                    $courseFees[$c_course_id[$i]]['amount'] = unformat_number($_POST['c_package_amount'][$i]);
                    $courseFees[$c_course_id[$i]]['custom'] = unformat_number($_POST['c_package_custom'][$i]);
                }
                if(!empty($courseFees)) $bean->coursefee_list = json_encode($courseFees);

                $student = BeanFactory::getBean('Contacts',$bean->parent_id);

                $bean->tuition_fee = $bean->amount_bef_discount;
                //Add Relationship Payment - Payment, save used payments
                if(!empty($bean->payment_list)){
                    $json_payment = json_decode(html_entity_decode($bean->payment_list),true);
                    foreach($json_payment["deposit_list"] as $pay_id => $value){
                        $rs_rm = $GLOBALS['db']->query("SELECT
                            remain_amount,
                            remain_hours,
                            payment_type,
                            payment_date,
                            use_type,
                            IFNULL(((payment_amount+deposit_amount+paid_amount)/total_hours),0) price FROM j_payment WHERE id = '$pay_id' AND deleted = 0");
                        $row_rm         = $GLOBALS['db']->fetchByAssoc($rs_rm);
                        //Sử dụng Cashholder như 1 deposit
                        if($row_rm['use_type'] == 'Hour'){
                            $remain_hours  = $row_rm['remain_hours'] - ($value["used_amount"] / $row_rm['price']);
                            if (empty($remain_hours) || is_nan($remain_hours) || is_infinite($remain_hours) || $remain_hours < '0.1') $remain_hours = 0;
                            $ext_remain_hours = ", remain_hours = $remain_hours";
                        }
                        $remain_amount  = $row_rm['remain_amount'] - $value["used_amount"];
                        if(abs($remain_amount) < 1000) $remain_amount = 0;
                        if($remain_amount < 0){
                            echo '<script type="text/javascript">
                            window.parent.DOTB.App.alert.show(\'message-id\', {
                            level: \'error\',
                            messages: \'Something Wrong, Please, try again !!\',
                            autoClose: true});
                            '.$route.'
                            </script>';
                            die();
                        }
                        $GLOBALS['db']->query("UPDATE j_payment SET remain_amount = $remain_amount $ext_remain_hours WHERE id = '$pay_id'");
                        addRelatedPayment($bean->id, $pay_id, $bean->payment_date, $value["used_amount"], 0);
                    }
                }
                //Calculate Payment Expire
                $bean->payment_expired = date('Y-m-d',strtotime("+{$bean->expired_term} ".$bean->payment_date));
            }
            elseif($bean->payment_type == 'Deposit'){
                $bean->use_type         = 'Amount';
                $bean->start_study      = '';
                $bean->end_study        = '';
                //Calculate Payment Expire
                $bean->payment_expired = date('Y-m-d',strtotime("+{$bean->expired_term} ".$bean->payment_date));
                $bean->tuition_fee              = $bean->payment_amount;
                $bean->amount_bef_discount      = $bean->payment_amount;
                $bean->total_after_discount     = $bean->payment_amount;
            }elseif($bean->payment_type !== 'Book/Gift'){
                $bean->kind_of_course = '';
                $bean->amount_bef_discount      = $bean->payment_amount;
                $bean->total_after_discount     = $bean->payment_amount;
                $bean->payment_expired          = $bean->payment_date;
            }

            if($bean->payment_type == 'Transfer Out' || $bean->payment_type == 'Moving Out') {
                // Set some field in transfers out payment
                $ttotal_amount  = 0;
                $ttotal_hour    = 0;
                // Save relationship to old payments
                $paymentList = json_decode(html_entity_decode($_POST["json_payment_list"]),true);
                foreach($paymentList as $pay_id => $value){
                    $rs_rm = $GLOBALS['db']->query("SELECT
                        remain_amount,
                        remain_hours,
                        team_id, payment_type, payment_date
                        FROM j_payment
                        WHERE id = '$pay_id' AND deleted = 0");
                    $row_rm = $GLOBALS['db']->fetchByAssoc($rs_rm);
                    $rate_reduce    = $row_rm['remain_amount'] / ($bean->payment_amount + $bean->refund_revenue);
                    $pay_amount     = $row_rm['remain_amount'];
                    $total_hours    = $row_rm['remain_hours'];
                    $remain_hours   = $row_rm['remain_hours'] - $value['used_hours'];
                    if($remain_hours < '0.1') $remain_hours = 0;
                    $remain_amount  = $row_rm['remain_amount'] - $value['used_amount'];
                    if(abs($remain_amount) < 1000) $remain_amount = 0;
                    if($remain_amount < 0){
                        echo '<script type="text/javascript">
                        window.parent.DOTB.App.alert.show(\'message-id\', {
                        level: \'error\',
                        messages: \'Something Wrong, Please, try again !!\',
                        autoClose: true});
                        '.$route.'
                        </script>';
                        die();
                    }

                    $ttotal_amount  += $pay_amount;
                    $ttotal_hour    += $total_hours;

                    $bean->team_id      = $row_rm['team_id'];
                    $bean->team_set_id  = $row_rm['team_id'];
                    $payrel_amount = ($pay_amount - ($bean->refund_revenue * $rate_reduce) );
                    $payrel_hours  = ($payrel_amount/ ($pay_amount/$total_hours));
                    $GLOBALS['db']->query("UPDATE j_payment SET
                        remain_amount = $remain_amount,
                        remain_hours = $remain_hours
                        WHERE id = '$pay_id'");
                    addRelatedPayment($bean->id, $pay_id , $bean->payment_date, $payrel_amount, $payrel_hours);
                    //Drop revenue
                    $revenue_name = ($bean->payment_type == 'Transfer Out') ? 'Drop revenue from payment transfer '.$bean->id: 'Drop revenue from payment moving '.$bean->id;
                    $delivery = new C_DeliveryRevenue();
                    $delivery->name = $revenue_name;
                    $delivery->student_id = $bean->parent_id;
                    //Get Payment ID
                    $delivery->ju_payment_id = $pay_id;
                    $delivery->amount = $bean->refund_revenue * $rate_reduce;
                    $delivery->date_input = $timedate->convertToDBDate($_POST["moving_tran_out_date"],false);
                    $delivery->team_id = $bean->team_id;
                    $delivery->team_set_id = $bean->team_id;
                    $delivery->assigned_user_id = $current_user->id;
                    $delivery->revenue_type = 'Admin Charge';
                    if($delivery->amount > 0)
                        $delivery->save();
                }
                // Create tranfer in payment
                $pay_in                     = BeanFactory::newBean("J_Payment");

                if($bean->payment_type == 'Transfer Out'){
                    //Set Use_type
                    $bean->total_hours      = $ttotal_hour;
                    $bean->use_type = 'Amount';
                    $bean->remain_hours = 0;
                    $pay_in->remain_hours = 0;
                    // Load target student
                    $target_student             = BeanFactory::getBean("Contacts", $_POST["transfer_to_student_id"]);
                    $bean->payment_date         = $timedate->convertToDBDate($_POST["moving_tran_out_date"],false);
                    $bean->tuition_hours        = $bean->total_hours;
                    $pay_in->tuition_hours      = $bean->total_hours;
                    $pay_in->total_hours        = $bean->total_hours;
                    $pay_in->payment_type       = 'Transfer In';
                    $pay_in->payment_amount     = $bean->payment_amount;
                    $pay_in->remain_amount      = $pay_in->payment_amount;
                    $pay_in->use_type           = $bean->use_type;
                    $pay_in->description        = $bean->description;
                    $pay_in->payment_date       = $bean->payment_date;
                    $pay_in->assigned_user_id   = $target_student->assigned_user_id;
                    $pay_in->team_id            = $target_student->team_id;
                    $pay_in->team_set_id        = $target_student->team_id;
                    $pay_in->payment_out_id     = $bean->id;
                    $pay_in->parent_id          = $target_student->id;
                    $pay_in->parent_type        = 'Contacts';
                    //Calculate Payment Expire
                    $pay_in->payment_expired = $timedate->to_display_date(date('Y-m-d',strtotime("+6 months ".$timedate->convertToDBDate($pay_in->payment_date))),false);
                    $pay_in->save();
                    //add related
                    addRelatedPayment($pay_in->id, $pay_in->payment_out_id, $pay_in->payment_date, $pay_in->remain_amount, $bean->total_hours);
                    // To Center
                    $bean->move_from_center_id  = $bean->team_id;
                    $bean->move_to_center_id    = $target_student->team_id;
                }

                if($bean->payment_type == 'Moving Out'){
                    $student = BeanFactory::getBean("Contacts", $bean->parent_id);
                    $student->load_relationship("teams");
                    $student->teams->add($bean->move_to_center_id);
                    $student->team_id = $bean->move_to_center_id;
                    $student->save();

                    // Create moving in payment
                    $pay_in = BeanFactory::newBean("J_Payment");
                    //Set Use_type
                    $bean->total_hours      = $ttotal_hour;
                    $bean->use_type = 'Amount';
                    $bean->remain_hours = 0;
                    $pay_in->remain_hours = 0;
                    $bean->payment_date            = $timedate->convertToDBDate($_POST["moving_tran_out_date"],false);
                    $bean->tuition_hours           = $bean->total_hours;
                    $pay_in->tuition_hours          = $bean->total_hours;
                    $pay_in->total_hours            = $bean->total_hours;
                    $pay_in->payment_type          = 'Moving In';
                    $pay_in->payment_amount        = $bean->payment_amount;
                    $pay_in->remain_amount         = $pay_in->payment_amount;
                    $pay_in->use_type              = $bean->use_type;
                    $pay_in->description           = $bean->description;
                    $pay_in->payment_date          = $bean->payment_date;
                    $pay_in->assigned_user_id      = $bean->assigned_user_id;
                    $pay_in->team_id               = $bean->move_to_center_id;
                    $pay_in->team_set_id           = $bean->move_to_center_id;
                    $pay_in->parent_id             = $student->id;
                    $pay_in->parent_type           = 'Contacts';
                    $pay_in->payment_out_id        = $bean->id;
                    //Moving From Center
                    $bean->move_from_center_id     = $bean->team_id;
                    // To Student
                    $bean->transfer_to_student_id  = $student->id;

                    //Calculate Payment Expire
                    $pay_in->payment_expired     = $timedate->to_display_date(date('Y-m-d',strtotime("+6 months ".$timedate->convertToDBDate($pay_in->payment_date))),false);
                    $pay_in->save();
                    //add related
                    addRelatedPayment($pay_in->id, $pay_in->payment_out_id, $pay_in->payment_date , $pay_in->remain_amount, $bean->total_hours);
                }
            }elseif($bean->payment_type == 'Refund') {
                $bean->payment_date     = $timedate->convertToDBDate($_POST["moving_tran_out_date"],false);
                // Save relationship to old payments
                $old_payments = json_decode(html_entity_decode($_POST["json_payment_list"]),true);
                foreach($old_payments as $pay_id => $value){
                    $rs_rm = $GLOBALS['db']->query("SELECT
                        remain_amount, remain_hours,
                        team_id, payment_type, payment_date
                        FROM j_payment WHERE id = '$pay_id' AND deleted = 0");
                    $row_rm = $GLOBALS['db']->fetchByAssoc($rs_rm);

                    $rate_reduce    = $row_rm['remain_amount'] / ($bean->payment_amount + $bean->refund_revenue);
                    $pay_amount     = $row_rm['remain_amount'];
                    $total_hours    = $pay_amount / (($row_rm['remain_amount']) / ($row_rm['remain_hours']));
                    if(empty($total_hours)) $total_hours = 0;

                    $bean->use_type = "Amount";
                    $bean->team_id      = $row_rm['team_id'];
                    $bean->team_set_id  = $row_rm['team_id'];

                    $remain_hours   = $row_rm['remain_hours'] - $value['used_hours'];
                    if($remain_hours < '0.1') $remain_hours = 0;
                    $remain_amount  = $row_rm['remain_amount'] - $value['used_amount'];
                    if(abs($remain_amount) < 1000) $remain_amount = 0;
                    if($remain_amount < 0){
                        echo '<script type="text/javascript">
                        window.parent.DOTB.App.alert.show(\'message-id\', {
                        level: \'error\',
                        messages: \'Something Wrong, Please, try again !!\',
                        autoClose: true});
                        '.$route.'
                        </script>';
                        die();
                    }

                    $GLOBALS['db']->query("UPDATE j_payment
                        SET remain_amount = $remain_amount,
                        remain_hours = $remain_hours
                        WHERE id = '{$pay_id}'");

                    //Cal amount / hour related payment
                    $payrel_amount = ($pay_amount - ($bean->refund_revenue * $rate_reduce) );
                    $payrel_hours  = ($payrel_amount/ ($pay_amount/$total_hours));
                    $bean->total_hours += $payrel_hours;
                    addRelatedPayment($bean->id, $pay_id, $bean->payment_date, $payrel_amount , $payrel_hours);
                    $payment_drop_id = $pay_id;

                    // Load bean of student
                    $student = BeanFactory::getBean("Contacts", $bean->parent_id);
                    //Drop revenue
                    $delivery = new C_DeliveryRevenue();
                    $delivery->name = 'Drop revenue from payment refund '.$bean->id;
                    $delivery->student_id = $student->id;
                    //Get Payment ID
                    $delivery->ju_payment_id = $payment_drop_id;
                    $delivery->amount = $bean->refund_revenue * $rate_reduce;
                    $delivery->date_input = $timedate->convertToDBDate($_POST["moving_tran_out_date"],false);
                    $delivery->team_id = $bean->team_id;
                    $delivery->team_set_id = $bean->team_set_id;
                    $delivery->assigned_user_id = $current_user->id;
                    $delivery->revenue_type = 'Admin Charge';
                    if($delivery->amount > 0)
                        $delivery->save();
                }
            }
            elseif($bean->payment_type == 'Book/Gift'){
                $student = BeanFactory::getBean('Contacts',$bean->parent_id);
                $inventory = BeanFactory::newBean("J_Inventory");
                $inventory->id = create_guid();
                $inventory->new_with_id = true;
                if($bean->parent_type == 'Leads')
                    $inventory->lead_id = $bean->parent_id;
                else
                    $inventory->student_id = $bean->parent_id;

                // ..and create new Inventory
                //$inventory->status      = "Confirmed";
                $inventory->date_create = $bean->payment_date;
                $inventory->type        = "Expense";
                $inventory->description = $bean->description;


                $inventory->team_id             = $bean->team_id;
                $inventory->team_set_id         = $bean->team_id;
                $inventory->assigned_user_id    = $bean->assigned_user_id;
                $inventory->amount_bef_discount = $bean->amount_bef_discount;
                $inventory->total_amount        = $bean->total_after_discount;

                $inventory_total_quantity = 0;
                // First element is null
                for ($i = 1; $i < count($_POST["book_id"]); $i++) {
                    $bookId         = $_POST["book_id"][$i];
                    $bookQuantity   = $_POST["book_quantity"][$i];
                    $bookPrice      = unformat_number($_POST["book_price"][$i]);
                    $bookAmount     = unformat_number($_POST['book_amount'][$i]);
                    if ($bookId != ""){
                        // Create Inventory Detail
                        $inventoryDetail = BeanFactory::newBean("J_Inventorydetail");
                        $inventoryDetail->book_id       = $bookId;
                        $inventoryDetail->inventory_id  = $inventory->id;
                        $inventoryDetail->quantity      = -1 * abs($bookQuantity);
                        $inventoryDetail->price         = $bookPrice;
                        $inventoryDetail->amount        = $bookAmount;
                        $inventoryDetail->team_id       = $inventory->team_id;
                        $inventoryDetail->team_set_id   = $inventory->team_set_id;
                        $inventoryDetail->assigned_user_id = $inventory->assigned_user_id;
                        $inventory_total_quantity += $inventoryDetail->quantity;
                        $inventoryDetail->save();
                    }
                }
                $inventory->total_quantity  = $inventory_total_quantity;
                $inventory->save();

                //Add relationship Inventory
                $bean->load_relationship('j_payment_j_inventory_1');
                $bean->j_payment_j_inventory_1->add($inventory->id);

                $bean->tuition_fee          = $bean->amount_bef_discount;
            }
            if($bean->payment_type == 'Cashholder' || $bean->payment_type == 'Book/Gift'){
                //Add Relationship Payment - Discount
                $json_discount = json_decode(html_entity_decode($_POST['discount_list']),true);
                $bean->load_relationship('j_payment_j_discount_1');
                foreach($json_discount as $dis_id => $value){
                    $bean->j_payment_j_discount_1->add($dis_id);
                    $dis = BeanFactory::getBean('J_Discount',$dis_id);
                    $spon = new J_Sponsor();
                    $spon->name             = $dis->name;
                    $spon->payment_id       = $bean->id;
                    $spon->discount_id      = $dis_id;
                    $spon->amount           = $value['dis_amount'];
                    $spon->percent          = $value['dis_percent'];
                    $spon->total_down       = $value['total_down'];
                    $spon->type             = 'Discount';
                    $spon->team_id          = $bean->team_id;
                    $spon->team_set_id      = $bean->team_id;
                    $spon->save();
                }
                //Create Sponsor
                $json_sponsor = json_decode(html_entity_decode($_POST['sponsor_list']),true);
                require_once "custom/include/_helper/connectTapTapApi.php";
                foreach($json_sponsor as $key => $value){
                    $spon = new J_Sponsor();
                    $spon->name = $value['foc_type'];
                    $spon->payment_id       = $bean->id;
                    $spon->student_id       = $bean->parent_id;
                    if($value['type'] == 'Loyalty' ) $spon->student_id   = $value['voucher_id'];
                    $spon->sponsor_number   = $value['sponsor_number'];
                    $spon->amount           = $value['sponsor_amount'];
                    $spon->percent          = $value['sponsor_percent'];
                    $spon->loyalty_points   = $value['loyalty_points'];
                    $spon->total_down       = $value['total_down'];
                    $spon->foc_type         = $value['foc_type'];
                    $spon->description      = $value['description'];
                    $spon->team_id          = $bean->team_id;
                    $spon->team_set_id      = $bean->team_id;

                    //TAPTAP
                    if(empty($value['voucher_id']) && $value['type'] == 'TAPTAP Sponsor'){
                        if (!in_array($value['sponsor_number'], $listSponsorNumber)) {
                            //get center code
                            $qGetCenterCode = "SELECT code_prefix FROM teams WHERE id = '{$bean->team_id}'";
                            $center_code = $GLOBALS['db']->getOne($qGetCenterCode);

                            $qGetPhone = "SELECT IFNULL(phone_mobile, '') phone_mobile FROM contacts WHERE id='{$bean->parent_id}'";
                            $phone = $GLOBALS['db']->getOne($qGetPhone);
                            $phone_mobile = $phone;
                            if ($phone[0] == '0') {
                                $phone_mobile = "84" . ltrim($phone, "0");
                            }
                            $post = json_encode(array(
                                'mobile' => $phone_mobile,
                                'code' => $value['sponsor_number'],
                                'bill_amount' => $bean->amount_bef_discount,
                                'transaction_number' => $bean->id,
                                'store_code' => $center_code
                            ));
                            $tapTap = new TapTapHelper();
                            $responeToken = $tapTap->requestToken();
                            if ($responeToken['success']) {
                                $responeRedeem = $tapTap->redeemVoucher($post);
                                if (!$responeRedeem['success']) {
                                    die('<b>Unavailable TAPTAP coupon. Please check tag Sponsor</b>');
                                }
                            }
                        }
                        $spon->type = $value['type'];
                    } else {
                        $spon->voucher_id       = $value['voucher_id'];
                        if (empty($bean->fetched_row) && !empty($spon->voucher_id))
                            $GLOBALS['db']->query("UPDATE j_voucher SET used_time=used_time+1 WHERE id = '{$spon->voucher_id}'");
                        $spon->type             = 'Sponsor';
                        if($spon->loyalty_points > 0) $spon->type         = 'Loyalty';
                    }

                    if(unformat_number($value['total_down']) > 0 || !empty($spon->voucher_id))
                        $spon->save();

                }
                //Add Relationship Payment - Loyalty
                if(!empty($_POST['loyalty_list'])){
                    $json_loyalty = json_decode(html_entity_decode($_POST['loyalty_list']),true);

                    $loyalty = new J_Loyalty();
                    $loyalty->point          = abs(unformat_number($json_loyalty['points_to_spend']));
                    $loyalty->discount_amount= $json_loyalty['amount_to_spend'];
                    $loyalty->rate_in_out   = $GLOBALS['app_list_strings']['default_loyalty_rate']['Conversion Rate'];
                    $loyalty->type          = 'Redemption';
                    $loyalty->student_id    = $student->id;
                    $loyalty->payment_id    = $bean->id;
                    $loyalty->target_id     = $json_loyalty['rate_out_id'];
                    $loyalty->team_id       = $bean->team_id;
                    $loyalty->team_set_id   = $bean->team_id;
                    $loyalty->input_date    = $bean->payment_date;
                    $loyalty->description   = 'Redemption Payments.';
                    if($loyalty->point > 0 && unformat_number($json_loyalty['amount_to_spend']) > 0)
                        $loyalty->save();
                }


                if($bean->payment_type == 'Cashholder') {
                    //Xet sponsor 100% - Cashholder
                    $total_discount_sponsor = $bean->final_sponsor_percent + $bean->discount_percent + $bean->loyalty_percent;
                    if($total_discount_sponsor >= 100)
                        $bean->remain_hours = $bean->total_hours;

                    //Xet đơn giá 0 đồng - Cashholder
                    if($bean->tuition_fee == 0 && $bean->total_hours > 0)
                        $bean->remain_hours = $bean->total_hours;
                }


            }
            //Check Company Info
            if($bean->is_corporate){
                $acc = BeanFactory::getBean('Accounts',$bean->account_id);
                if( (($acc->name != $bean->company_name)) || ($acc->billing_address_street != $bean->company_address) || ($acc->tax_code != $bean->tax_code) ){
                    $acc->name                       = $bean->company_name;
                    $acc->billing_address_street     = $bean->company_address;
                    $acc->tax_code                   = $bean->tax_code;
                    $acc->save();
                }
            }
        }
        //TH thay đổi Auto-enroll - Xoá các Outstanding tương ứng
        if(!empty($bean->fetched_row)
            && ($bean->fetched_row['is_auto_enroll'] != $bean->is_auto_enroll|| $bean->fetched_row['is_allow_ost'] != $bean->is_allow_ost)
            && ($bean->is_auto_enroll == 0 || $bean->is_allow_ost == 0))
            self::removeOutStanding($bean);
    }

    function afterSavePayment($bean, $event, $arguments){
        global $timedate;

        $not_pa_type = array('Transfer Out','Moving Out','Refund','Transfer In','Moving In','Enrollment');
        if(!in_array($bean->payment_type,$not_pa_type))  getPaymentRemain($bean->id);    //Nap so remain


        $helper_payment = new HelperPayment();
        if(($_POST['module'] == $bean->module_name)
        && ($_POST['action'] == 'Save')
        && ($bean->payment_type != 'Enrollment')){ //FIX LỖI: Auto-Enroll trigger Edit Enrollment vô tình xoá học viên Khỏi lớp
            $not_pa_type = array('Transfer Out','Moving Out','Refund','Transfer In','Moving In','Book/Gift','Enrollment');
            $discount_amount        = ($bean->discount_amount);
            $final_sponsor          = ($bean->final_sponsor);
            $loyalty_amount         = ($bean->loyalty_amount);
            if(!in_array($bean->payment_type,$not_pa_type)) {
                //Add Receipt
                if($bean->number_of_payment == 'Custom'){
                    $count_pmd = (int)$bean->num_month_pay;
                    $total_pmd = $bean->payment_amount;
                    for($i = 0; $i < $count_pmd; $i++){
                        //Nếu trả góp - Discount/Sponsor qui vào lần 1
                        if($bean->is_installment)
                            $payDtlMax = 1;
                        else{
                            $cr_amount = unformat_number($_POST['pay_dtl_amount'][$i]);
                            if($cr_amount > $payDtlMaxAmount){
                                $payDtlMaxAmount     = $cr_amount;
                                $payDtlMax           = $i + 1;
                            }
                        }

                        $pmd = BeanFactory::newBean('J_PaymentDetail');
                        $index = $i+1;
                        $pmd->payment_no    = $index;
                        $pmd->name          = '-none-';

                        $payDtlAmount       = unformat_number($_POST['pay_dtl_amount'][0]);

                        if($i == 0){
                            $pmd->is_discount       = 1;
                            $pmd->before_discount   = format_number(round($bean->payment_amount / $count_pmd, -3) + $discount_amount + $final_sponsor + $loyalty_amount);
                            $pmd->discount_amount   = format_number($discount_amount);
                            $pmd->sponsor_amount    = format_number($final_sponsor);
                            $pmd->loyalty_amount    = format_number($loyalty_amount);
                        }else{
                            $pmd->before_discount   = format_number(round($bean->payment_amount / $count_pmd, -3));
                            $pmd->discount_amount   = 0;
                            $pmd->sponsor_amount    = 0;
                            $pmd->loyalty_amount    = 0;
                        }

                        $pmd->status                = "Unpaid";

                        $pmd->expired_date          =  date('Y-m-d',strtotime('+'. ($bean->repeat_every_pay * $i).' '.$bean->repeat_option_pay.' '.$timedate->convertToDBDate($_POST['pay_dtl_invoice_date'][0],false)));
                        $pmd->payment_date          =  $pmd->expired_date;
                        if($payDtlAmount == 0){
                            $pmd->status                = "Paid";
                            $pmd->payment_date          = $_POST['pay_dtl_invoice_date'][$i];
                            $pmd->expired_date          = $_POST['pay_dtl_invoice_date'][$i];
                            $pmd->payment_method        = 'Other';
                        }
                        $pmd->type                  = 'Normal';
                        $pmd->payment_amount        = format_number(round($bean->payment_amount / $count_pmd, -3));
                        $pmd->payment_id            = $bean->id;
                        $pmd->parent_name             = $bean->parent_name;
                        $pmd->parent_id             = $bean->parent_id;
                        $pmd->parent_type           = $bean->parent_type;
                        $pmd->assigned_user_id      = '';
                        $pmd->team_id               = $bean->team_id;
                        $pmd->team_set_id           = $bean->team_id;
                        if($i == $count_pmd-1){
                            $pmd->payment_amount    =  $total_pmd;
                            $pmd->before_discount   =  $total_pmd;
                        }else
                            $total_pmd -= round($bean->payment_amount / $count_pmd, -3);

                        //Prepare Array
                        $arr = array(
                            'expired_date' => $pmd->expired_date,
                            'payment_type' => $bean->payment_type,
                            'kind_of_course' => $bean->kind_of_course,
                            'payment_id' => $bean->id,
                            'payment_no' => $pmd->payment_no,
                            'number_of_payment' => $bean->number_of_payment,
                        );
                        $pmd->description = $helper_payment->generateContent($arr);

                        if($pmd->payment_amount == 0){
                            $pmd->status                = "Paid";
                            $pmd->payment_method        = 'Other';
                        }
                        if($pmd->payment_amount >= 0)
                            $pmd->save();
                    }
                }else{
                    $count_pmd = (int)$bean->number_of_payment;
                    //                    $_POST['pay_dtl_invoice_date'][0] = $bean->payment_date;
                    //Find payment max amount
                    $payDtlMax          = 1;
                    $payDtlMaxAmount    = 0;
                    for($i = 0; $i < $count_pmd; $i++){
                        $cr_amount = unformat_number($_POST['pay_dtl_amount'][$i]);
                        if($cr_amount > $payDtlMaxAmount){
                            $payDtlMaxAmount     = $cr_amount;
                            $payDtlMax           = $i + 1;
                        }
                    }
                    for($i = 0; $i < $count_pmd; $i++){
                        $pmd = BeanFactory::newBean('J_PaymentDetail');
                        $index = $i+1;
                        $pmd->payment_no    = $index;
                        $pmd->name          = '-none-';

                        $payDtlAmount       = unformat_number($_POST['pay_dtl_amount'][$i]);
                        if($index == $payDtlMax){
                            $pmd->is_discount       = 1;
                            $pmd->before_discount   = format_number($payDtlAmount + $discount_amount + $final_sponsor + $loyalty_amount);
                            $pmd->discount_amount   = format_number($discount_amount);
                            $pmd->sponsor_amount    = format_number($final_sponsor);
                            $pmd->loyalty_amount    = format_number($loyalty_amount);
                        }else{
                            $pmd->before_discount   = format_number($payDtlAmount);
                            $pmd->discount_amount   = 0;
                            $pmd->sponsor_amount    = 0;
                            $pmd->loyalty_amount    = 0;
                        }

                        //Prepare Array
                        $arr = array(//
                            'expired_date' => $pmd->payment_date,
                            'payment_type' => $bean->payment_type,
                            'kind_of_course' => $bean->kind_of_course,
                            'payment_id' => $bean->id,
                            'payment_no' => $pmd->payment_no,
                            'number_of_payment' => $bean->number_of_payment,
                        );
                        $pmd->description = $helper_payment->generateContent($arr);

                        $pmd->status                = "Unpaid";
                        //$pmd->payment_date          = $_POST['pay_dtl_invoice_date'][$i];
                        $pmd->expired_date          = $_POST['pay_dtl_invoice_date'][$i];
                        $pmd->payment_date          =  $pmd->expired_date;
                        if($payDtlAmount == 0){
                            $pmd->status                = "Paid";
                            $pmd->payment_method        = 'Other';
                        }
                        $pmd->type                  = 'Normal';
                        $pmd->payment_amount        = format_number($payDtlAmount);
                        $pmd->payment_id            = $bean->id;
                        $pmd->parent_name             = $bean->parent_name;
                        $pmd->parent_id             = $bean->parent_id;
                        $pmd->parent_type           = $bean->parent_type;
                        $pmd->assigned_user_id      = '';
                        $pmd->team_id               = $bean->team_id;
                        $pmd->team_set_id           = $bean->team_id;
                        if($pmd->payment_amount == 0){
                            $pmd->status                = "Paid";
                            $pmd->payment_method        = 'Other';
                        }
                        if($pmd->payment_amount >= 0)
                            $pmd->save();
                    }
                }
            }elseif($bean->payment_type == 'Book/Gift'){
                $pmd = BeanFactory::newBean('J_PaymentDetail');
                $pmd->type                  = 'Normal';
                $pmd->loyalty_amount        = format_number($loyalty_amount);
                $pmd->payment_amount        = format_number($bean->payment_amount);
                $pmd->before_discount       = format_number($bean->payment_amount + $loyalty_amount);
                $pmd->vat_rate              = $bean->vat_rate;
                $pmd->vat_amount            = format_number($bean->vat_amount);
                $pmd->payment_id            = $bean->id;
                $pmd->payment_date          = $bean->payment_date;
                $pmd->expired_date          = $pmd->payment_date;
                $pmd->parent_name             = $bean->parent_name;
                $pmd->parent_id             = $bean->parent_id;
                $pmd->parent_type           = $bean->parent_type;
                $pmd->assigned_user_id      = '';
                $pmd->team_id               = $bean->team_id;
                $pmd->team_set_id           = $bean->team_id;
                //Prepare Array
                $arr = array(
                    'expired_date' => $pmd->payment_date,
                    'payment_type' => $bean->payment_type,
                    'kind_of_course' => $bean->kind_of_course,
                    'payment_id' => $bean->id,
                    'payment_no' => $pmd->payment_no,
                    'number_of_payment' => $bean->number_of_payment,
                );
                $pmd->description = $helper_payment->generateContent($arr);
                if($pmd->payment_amount == 0){
                    $pmd->status                = "Paid";
                    $pmd->payment_method        = 'Other';
                }
                $pmd->save();
            }
            //check auto-enroll
            if($bean->is_auto_enroll && $bean->payment_type == 'Cashholder'){
                $payment_no_cache = BeanFactory::getBean('J_Payment', $pmd->payment_id, array('use_cache'=>false));
                $class = BeanFactory::getBean('J_Class', $bean->ju_class_id);
                $res   = processAutoEnroll($payment_no_cache, $class);
            }

            //Update student status
            if(!empty($bean->parent_id) && $bean->parent_type == 'Contacts') updateStudentStatus($bean->parent_id);
        }

        //Import AUTO-Enroll
        if($_POST['module'] == 'Import' && $bean->payment_type == 'Cashholder' && !$bean->is_processing){
            if($bean->is_auto_enroll){
                $payment_no_cache = BeanFactory::getBean('J_Payment', $bean->id, array('use_cache'=>false));
                $payment_no_cache->is_allow_ost = 1;
                $class = BeanFactory::getBean('J_Class', $bean->ju_class_id);
                $res   = processAutoEnroll($payment_no_cache, $class);
                //Update student status
                if(!empty($bean->parent_id) && $bean->parent_type == 'Contacts') updateStudentStatus($bean->parent_id);
                $GLOBALS['db']->query("UPDATE j_payment SET note='{$res['alert']}', date_modified='{$GLOBALS['timedate']->nowDb()}' WHERE id = '{$bean->id}'");
            }
        }
    }

    ///to mau id va status Lap Nguyen
    function listViewColorPayment(&$bean, $event, $arguments){
        if (!empty($_REQUEST['__dotb_url'])){
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $url_module = $arrayUrl[sizeof($arrayUrl) - 1];
        }
        //optimize performance - only load when in list view
        if( ($_REQUEST['view'] == 'list' && $url_module == $bean->module_name)
        || strpos($_REQUEST['view'], 'subpanel') !== false){
            global $timedate;
            //Total paid amount of payment detail
            $q1="SELECT DISTINCT IFNULL(j_paymentdetail.id, '') primaryid,
            j_paymentdetail.payment_date payment_date, j_paymentdetail.status status,
            j_paymentdetail.payment_amount payment_amount
            FROM j_paymentdetail
            INNER JOIN j_payment l1 ON j_paymentdetail.payment_id = l1.id AND l1.deleted = 0
            WHERE (((l1.id = '{$bean->id}')
            AND (j_paymentdetail.status <> 'Cancelled')))
            AND j_paymentdetail.deleted = 0";
            $res = $GLOBALS['db']->query($q1);
            $total = 0;
            $htm_pmd = '<table width="100%" class="dataTable no-border"><tbody>';
            $count_pmd = 0;
            $paidAmount     = 0;
            $unpaidAmount   = 0;
            while($row = $GLOBALS['db']->fetchByAssoc($res)) {
                if($row['status'] == 'Unpaid')
                    $unpaidAmount += $row['payment_amount'];
                if($row['status'] == 'Paid')
                    $paidAmount += $row['payment_amount'];
                if((($row['status'] == 'Paid' && $row['payment_amount'] > 0)) || ($row['status'] == 'Unpaid')){
                    $count_pmd++;
                    $htm_pmd .= "<tr>";
                    $htm_pmd .= "<td style='width: 40%;'>".$timedate->to_display_date($row['payment_date'],false)."</td>";
                    $htm_pmd .= "<td style='width: 40%;'>".format_number($row['payment_amount'])."</td>";
                    $htm_pmd .= "<td style='width: 20%;' ".($row['status'] == 'Unpaid' ? "class='error'" : '').">".$row['status']."</td>";
                    $htm_pmd .= "</tr>";
                }
            }
            $htm_pmd .= "</tbody></table>";

            //add field Paid/Unpaid
            $bean->sum_paid = $paidAmount;
            $bean->sum_unpaid = $unpaidAmount;

            if($count_pmd > 0)
                $bean->related_payment_detail = $htm_pmd;
            else $bean->related_payment_detail = '';

            $bean->class_string = '';
            if($bean->payment_type == 'Enrollment' || $bean->payment_type == 'Delay'){
                $sql_get_class="SELECT DISTINCT IFNULL(l2.id,'') l2_id ,
                IFNULL(l2.name,'') l2_name , IFNULL(l2.class_code,'') class_code , IFNULL(j_payment.id,'') primaryid
                FROM j_payment INNER JOIN j_studentsituations l1 ON j_payment.id=l1.payment_id
                AND l1.deleted=0 INNER JOIN j_class l2 ON l1.ju_class_id=l2.id
                AND l2.deleted=0 WHERE j_payment.id='{$bean->id}'
                AND j_payment.deleted=0
                ORDER BY  l2.name";
                $result_get_class = $GLOBALS['db']->query($sql_get_class);
                while($row = $GLOBALS['db']->fetchByAssoc($result_get_class))
                    $bean->class_string .= '<a href="#J_Class/'.$row['l2_id'].'">'.$row['l2_name'].'</a><br>';
            }

            $img_import = '';
            //Student name
            $q10 = "SELECT
            IFNULL(l2.id, l1.id) primary_id,
            IFNULL(l2.full_student_name, l1.full_lead_name) student_name,
            IFNULL(l2.phone_mobile, l1.phone_mobile) student_phone,
            IFNULL((jp.paid_amount + jp.deposit_amount + jp.payment_amount),'') total_amount,
            IFNULL((jp.paid_amount + jp.deposit_amount + jp.payment_amount)/( ROUND(ROUND(IFNULL(jp.paid_hours,0)+IFNULL(jp.total_hours,0)*60)/60,9) ),'') cost_per_hour,
            IFNULL(jp.parent_type, '')  parent_type
            FROM j_payment jp
            LEFT JOIN leads l1 ON jp.parent_id = l1.id AND jp.parent_type = 'Leads' AND l1.deleted = 0
            LEFT JOIN contacts l2 ON jp.parent_id = l2.id AND jp.parent_type = 'Contacts' AND l2.deleted = 0
            WHERE (((jp.id = '{$bean->id}')))
            AND jp.deleted = 0";
            $rs10 = $GLOBALS['db']->query($q10);
            $rowS = $GLOBALS['db']->fetchByAssoc($rs10);
            if(!empty($rowS['primary_id']))
                $bean->parent_name_text = '<a class="ellipsis_inline" data-original-title="'.$rowS['student_name'].'" href="#'.$rowS['parent_type'].'/'.$rowS['primary_id'].'">'.$rowS['student_name'].'</a>';
            $bean->phone = '<span>'.$rowS['student_phone'].'</span>';

            if(!empty($rowS['cost_per_hour']))  $bean->cost_per_hour = format_number($rowS['cost_per_hour']);

            if(!empty($rowS['total_amount']))
                $bean->total_amount = format_number($rowS['total_amount']);

            //Get Payment Related
            $q2 = "SELECT DISTINCT
            IFNULL(l1.id, '') l1_id,
            IFNULL(l1.name, '') l1_name,
            IFNULL(l1.payment_type, '') l1_payment_type,
            l1_1.hours hours,
            l1_1.amount amount
            FROM j_payment
            INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
            INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0
            WHERE (((j_payment.id = '{$bean->id}')))
            AND j_payment.deleted = 0";

            $count_rel = 0;
            $res = $GLOBALS['db']->query($q2);
            $htm_rel = "<table><tbody>";
            while($row = $GLOBALS['db']->fetchByAssoc($res)) {
                $htm_rel .= "<tr class='oddListRowS1' style='border: none;'><td style='width: 40%;'><a title='{$row['l1_payment_type']}' href='#bwc/index.php?module=J_Payment&action=DetailView&record={$row['l1_id']}'>{$row['l1_name']}</a> (".format_number($row['amount'])." - ".format_number($row['hours'],2,2).")</td>";
                $htm_rel .= "</tr>";
                $count_rel++;
            }
            $htm_rel .= "</tbody></table>";
            if($count_rel > 0)
            $bean->related_payment_list = $htm_rel;
        }
        //Payment type
        switch ($bean->payment_type) {
            case "Enrollment":
                $colorClass = "textbg_green";
                break;
            case "Deposit":
                $colorClass = "textbg_blue";
                break;
            case "Cashholder":
                $colorClass = "textbg_bluelight";
                break;
            case "Delay":
                $colorClass = "textbg_blood";
                break;
            case "Transfer In":
            case "Transfer Out":
            case "Moving In":
            case "Moving Out":
                $colorClass = "textbg_yellow_light";
                break;
            case "Refund":
                $colorClass = "textbg_crimson";
                break;
            case "Book/Gift":
            case "Placement Test":
            case "Tutor Package":
            case "Travelling Fee":
            case "Delay Fee":
            case "Transfer Fee":
                $colorClass = "textbg_violet";
                break;
        }

        if(!empty($bean->is_old)) $img_import = '<img src="custom/images/import.png" style="width: 16px;" title="Data Import">';

        $bean->payment_type = "<span class='full-width visible'><span class='label ellipsis_inline $colorClass' title='{$bean->description}'>". $GLOBALS['app_list_strings']['payment_type_list'][$bean->payment_type] ." $img_import</span></span>";
        if(($bean->panel_name == 'j_payment_j_payment_1')){
            //Get applied amount
            $q3 = "SELECT DISTINCT
            IFNULL(l1.id, '') l1_id, IFNULL(l1.name, '') l1_name,
            IFNULL(l1.payment_type, '') l1_payment_type,
            l1_1.hours hours,
            l1_1.amount amount
            FROM j_payment
            INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_idb AND l1_1.deleted = 0
            INNER JOIN j_payment l1 ON l1.id = l1_1.payment_ida AND l1.deleted = 0 AND l1.id = '{$_REQUEST['record']}'
            WHERE (((j_payment.id = '{$bean->id}')))
            AND j_payment.deleted = 0";
            $res = $GLOBALS['db']->query($q3);
            while($row = $GLOBALS['db']->fetchByAssoc($res)) {
                $bean->applied_amount += $row['amount'];
                $bean->applied_hours  += $row['hours'];
            }

        }
        if(($bean->panel_name == 'j_payment_j_payment_1_right')){
            //Get Allocated amount
            $q4 = "SELECT DISTINCT
            IFNULL(l1.id, '') l1_id, IFNULL(l1.name, '') l1_name,
            IFNULL(l1.payment_type, '') l1_payment_type,
            l1_1.hours hours,
            l1_1.amount amount
            FROM j_payment
            INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
            INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0 AND l1.id = '{$_REQUEST['record']}'
            WHERE (((j_payment.id = '{$bean->id}')))
            AND j_payment.deleted = 0";
            $res = $GLOBALS['db']->query($q4);
            while($row = $GLOBALS['db']->fetchByAssoc($res)) {
                if($row['l1_id'] == $_REQUEST['record']){
                    $bean->applied_amount += $row['amount'];
                    $bean->applied_hours  += $row['hours'];
                }
            }
        }

    }

    function addCode(&$bean, $event, $arguments){
        $code_field = 'name';
        if(empty($bean->$code_field)){
            //AFTER_SAVE: Repeat 10 times to avoid duplicate codes
            for ($x = 0; $x < 10; $x++) {
                //Get Prefix
                $res        = $GLOBALS['db']->query("SELECT code_prefix FROM teams WHERE id = '{$bean->team_id}'");
                $row        = $GLOBALS['db']->fetchByAssoc($res);
                $prefix     = $row['code_prefix'];
                $year       = date('y',strtotime('+ 7hours'. (!empty($bean->date_entered) ? $bean->date_entered : $bean->fetched_row['date_entered'])));
                $table      = $bean->table_name;
                $sep        = '-';
                $first_pad  = '00000';
                $padding    = 5;
                if($bean->payment_type == 'Enrollment') $ext = 'ENR';
                elseif(in_array($bean->payment_type, ['Transfer In','Transfer Out','Moving In','Moving Out','Refund','Delay'])) $ext = 'REF';
                else $ext = 'PAY';

                $str_code = $ext.$prefix.$year.$sep;

                $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, ".strlen($str_code).") = '".$str_code."') AND deleted = 0 ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";
                $result = $GLOBALS['db']->query($query);

                if($row = $GLOBALS['db']->fetchByAssoc($result))
                    $last_code = $row[$code_field];
                else
                    //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $last_code = $str_code. $first_pad;

                $num = substr($last_code, -$padding, $padding);
                $num++;
                $pads = $padding - strlen($num);
                $new_code = $str_code;

                //preform the lead padding 0
                for($i=0; $i < $pads; $i++)
                    $new_code .= "0";
                $new_code .= $num;

                //check duplicate code
                $countDup = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM $table WHERE $code_field = '$new_code' AND deleted = 0");
                if(empty($countDup)){
                    //write to database - Logic: Before Save
                    $GLOBALS['db']->query("UPDATE $table SET $code_field = '$new_code' WHERE id='{$bean->id}' AND deleted = 0");
                    break;
                }
            }
        }
    }

    function createdRelationshipBookGift(&$bean, $event, $arguments){
        $url = $_SERVER['HTTP_REFERER'];
        if(strpos($url,'primary_id=')) {
            $id = substr($url, strpos($url, 'primary_id=') + 11, 36);
            $bean->j_payment_j_payment_2_right->add($id);
        }
    }

    function addFilter(&$bean, $event, $args) {
        if($_REQUEST['view'] == 'list'){
            $values = ['Enrollment'];
            $args[0]->where()->notIn('payment_type',$values);
            if(method_exists($args[1]['id_query'],'where'))
                $args[1]['id_query']->where()->notIn('payment_type', $values);
        }
    }

    function removeOutStanding(&$bean) {
        $q3 = "SELECT DISTINCT IFNULL(jst.id, '') situation_id FROM j_studentsituations jst
        WHERE jst.deleted = 0
        AND (jst.payment_id = '{$bean->id}')
        AND (jst.student_id = '{$bean->parent_id}')
        AND (jst.ju_class_id = '{$bean->ju_class_id}')
        AND (jst.type IN ('OutStanding'))";
        foreach($GLOBALS['db']->fetchArray($q3) as $key => $val){
            $situa = BeanFactory::getBean('J_StudentSituations',$val['situation_id'],array('disable_row_level_security'=>true));
            $situa->mark_deleted($situa->id);
        }
    }
}
?>
