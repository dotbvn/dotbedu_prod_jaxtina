<?php
//export file excel
require_once("custom/include/PHPExcel/Classes/PHPExcel.php");
require_once("custom/include/ConvertMoneyString/convert_number_to_string.php");

global $timedate, $current_user;

$fdir = 'InvoiceExcel';
if (!file_exists("upload/$fdir"))
    mkdir("upload/$fdir", 0777, true);

$fi = new FilesystemIterator("upload/$fdir", FilesystemIterator::SKIP_DOTS);
if(iterator_count($fi) > 10)
    array_map('unlink', glob("upload/$fdir/*"));

$objPHPExcel = new PHPExcel();

$templateUrl = "custom/include/TemplateExcel/center_expenses.xlsx";

//Import Template
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($templateUrl);

// Set properties
$objPHPExcel->getProperties()->setCreator("DotB");
$objPHPExcel->getProperties()->setLastModifiedBy("DotB");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX");

//Add data
if($_REQUEST['module_type'] == 'J_Budget'){
    $sql = "SELECT DISTINCT
    IFNULL(j_budget.id, '') primaryid,
    j_budget.amount amount,
    j_budget.date_entered date_entered,
    j_budget.date_modified date_modified,
    IFNULL(j_budget.description, '') description,
    IFNULL(j_budget.expense_for, '') expense_for,
    IFNULL(l8.code_prefix, '') code_prefix,
    IFNULL(j_budget.expense_options, '') expense_options,
    (CASE WHEN IFNULL(j_budget.money_receiver, '') != '' THEN IFNULL(j_budget.money_receiver, '')
    WHEN j_budget.parent_type = 'Leads' THEN IFNULL(l3.full_lead_name, '')
    WHEN j_budget.parent_type = 'Contacts' THEN IFNULL(l4.full_student_name, '')
    WHEN j_budget.parent_type = 'Users' THEN IFNULL(l5.full_user_name, '')
    WHEN j_budget.parent_type = 'C_Teachers' THEN IFNULL(l6.full_teacher_name, '')
    ELSE '' END) money_receiver,
    CASE WHEN j_budget.parent_type = 'Leads' THEN IFNULL(l3.phone_mobile, '')
    WHEN j_budget.parent_type = 'Contacts' THEN IFNULL(l4.phone_mobile, '')
    WHEN j_budget.parent_type = 'Users' THEN IFNULL(l5.phone_mobile, '')
    WHEN j_budget.parent_type = 'C_Teachers' THEN IFNULL(l6.phone_mobile, '')
    ELSE '' END money_mobile,
    IFNULL(j_budget.expense_code, '') expense_code,
    IFNULL(j_budget.payment_method, '') payment_method,
    j_budget.expense_date expense_date,
    IFNULL(j_budget.name, '') name,
    IFNULL(l2.full_user_name, '') full_user_name,
    IFNULL(l7.full_user_name, '') created_by,
    IFNULL(l1.name,'') team_name,
    IFNULL(l1.description,'') team_address
    FROM j_budget INNER JOIN  teams l1 ON j_budget.team_id=l1.id AND l1.deleted=0
    LEFT JOIN users l2 ON j_budget.assigned_user_id = l2.id AND l2.deleted = 0
    INNER JOIN users l7 ON l7.id = j_budget.created_by AND l7.deleted = 0
    LEFT JOIN teams l8 ON j_budget.team_id = l8.id AND l8.deleted = 0
    LEFT JOIN leads l3 ON j_budget.parent_id = l3.id AND l3.deleted = 0 AND j_budget.parent_type = 'Leads'
    LEFT JOIN contacts l4 ON j_budget.parent_id = l4.id AND l4.deleted = 0 AND j_budget.parent_type = 'Contacts'
    LEFT JOIN users l5 ON j_budget.parent_id = l5.id AND l5.deleted = 0 AND j_budget.parent_type = 'Users'
    LEFT JOIN c_teachers l6 ON j_budget.parent_id = l6.id AND l6.deleted = 0 AND j_budget.parent_type = 'C_Teachers'
    WHERE (((j_budget.id = '{$_REQUEST['record']}')))
    AND j_budget.deleted = 0";
}else{
    $sql = "SELECT
    IFNULL(j_payment.name, '') expense_code,
    IFNULL(j_payment.payment_amount, '') amount,
    IFNULL(j_payment.refund_revenue, '') refund_revenue,
    IFNULL(j_payment.payment_date, '') expense_date,
    IFNULL(j_payment.description, '') expense_for,
    IFNULL(j_payment.note, '') description,
    IFNULL(j_payment.payment_method, '') payment_method,
    IFNULL(l2.id, '') team_id,
    IFNULL(l2.name, '') team_name,
    IFNULL(l2.code_prefix, '') code_prefix,
    IFNULL(l2.description, '') team_address,
    IFNULL(l3.id, '') student_id,
    IFNULL(l3.contact_id, '') money_receiver_id,
    IFNULL(l3.full_student_name, '') money_receiver,
    IFNULL(l3.phone_mobile, '') money_mobile,
    IFNULL(l7.full_user_name, '') full_user_name,
    IFNULL(l3.primary_address_street, '') money_receiver_address
    FROM j_payment
    LEFT JOIN teams l2 ON j_payment.team_id = l2.id AND l2.deleted = 0
    LEFT JOIN contacts l3 ON l3.id = j_payment.parent_id AND j_payment.parent_type = 'Contacts' AND l3.deleted = 0
    INNER JOIN users l7 ON l7.id = j_payment.assigned_user_id AND l7.deleted = 0
    WHERE
    j_payment.id = '{$_REQUEST['record']}'
    AND j_payment.deleted = 0
    LIMIT 1";
}
$res     = $GLOBALS['db']->query($sql);
$r       = $GLOBALS['db']->fetchByAssoc($res);

//Prepare
$date       = explode('-', $r['expense_date']);
$day        = $date[2];
$month      = $date[1];
$year       = $date[0];
$objPHPExcel->getActiveSheet()->SetCellValue('C3', 'Ngày '.$day.' tháng '.$month.' năm '.$year);
$objPHPExcel->getActiveSheet()->SetCellValue('I1', $r['code_prefix']);
$objPHPExcel->getActiveSheet()->SetCellValue('I2', $r['expense_code']);
$objPHPExcel->getActiveSheet()->SetCellValue('I3', '1');
$objPHPExcel->getActiveSheet()->SetCellValue('B8', $r['money_receiver']);
$objPHPExcel->getActiveSheet()->SetCellValue('B9', $r['money_receiver_id']);

$objPHPExcel->getActiveSheet()->SetCellValue('G8', $r['money_mobile']);

$objPHPExcel->getActiveSheet()->SetCellValue('B10', $r['team_name']);

$int = new Integer();
$text = $int->toText($r['amount']);
if($_REQUEST['module_type'] == 'J_Budget')
    $objPHPExcel->getActiveSheet()->SetCellValue('B11', $GLOBALS['app_list_strings']['expense_for_list'][$r['expense_for']]);
else  $objPHPExcel->getActiveSheet()->SetCellValue('B11', $r['expense_for']);
$objPHPExcel->getActiveSheet()->SetCellValue('B12', number_format($r['amount']));
$objPHPExcel->getActiveSheet()->SetCellValue('G12', "Ngày-Giờ: ".$timedate->now());
$objPHPExcel->getActiveSheet()->SetCellValue('B13', $text);

if ($r['payment_method'] == "Cash") $pmmt="TM";
elseif ($r['payment_method'] == "Card") $pmmt="CK";
else $pmmt      ="CK";
$objPHPExcel->getActiveSheet()->SetCellValue('B14', $pmmt);
$objPHPExcel->getActiveSheet()->SetCellValue('B15', $r['description']);
$objPHPExcel->getActiveSheet()->SetCellValue('D27', $r['full_user_name']);



$objPHPExcel->getActiveSheet()->setTitle('Liên 1');
//Clone sheet - Liên 2
$objSheetBase   = $objPHPExcel->getActiveSheet();
$objSheetBase   = clone $objSheetBase;
$objSheetBase->setTitle('Liên 2');
$objPHPExcel->addSheet($objSheetBase);
$objPHPExcel->setActiveSheetIndexByName('Liên 2')->SetCellValue('I3', "2");

//Clone sheet - Liên 3
$objSheetBase   = $objPHPExcel->getActiveSheet();
$objSheetBase   = clone $objSheetBase;
$objSheetBase->setTitle('Liên 3');
$objPHPExcel->addSheet($objSheetBase);
$objPHPExcel->setActiveSheetIndexByName('Liên 3')->SetCellValue('I3', "3");

$objPHPExcel->setActiveSheetIndexByName('Liên 1');

//Lock file
$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
$objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
$objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
$objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
$objPHPExcel->getActiveSheet()->getProtection()->setPassword('7779');

// Save Excel 2007 file
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$section = create_guid_section(6);
$path = 'upload/'.$fdir;
$file = $path.'/'.preg_replace("/[^a-z0-9\_\-\.]/i", '', 'Expense_'.$r['expense_code'].'-'.$section.'.xlsx');

$objWriter->save($file);
header('Location: '.$file);