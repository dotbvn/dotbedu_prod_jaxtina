<?php
echo showRedeemVoucherTapTapScreen();

function showRedeemVoucherTapTapScreen()
{
    global $current_user;
    $htmlTeam = getTeamsForUser($current_user->id);
    $ss = new Dotb_Smarty();
    $ss->assign("MOD", $GLOBALS['mod_strings']);
    $ss->assign("htmlTeam", $htmlTeam);
    return $ss->fetch('custom/modules/J_Payment/tpl/redeemVoucherTapTap.tpl');
}

function getTeamsForUser($user_id = null)
{
    $query = "SELECT DISTINCT
    IFNULL(users.id, '') primaryid, IFNULL(users.user_name, '') users_user_name,
    IFNULL(l1.id, '') defaut_team_id, IFNULL(l1.code_prefix, '') defaut_code_prefix,
    IFNULL(l1.name, '') defaut_team_name, IFNULL(l2.id, '') team_id,
    IFNULL(l2.name, '') team_name, IFNULL(l2.code_prefix, '') code_prefix
    FROM users INNER JOIN teams l1 ON users.default_team = l1.id AND l1.deleted = 0
    INNER JOIN team_memberships l2_1 ON users.id = l2_1.user_id AND l2_1.deleted = 0
    INNER JOIN teams l2 ON l2.id = l2_1.team_id AND l2.deleted = 0
    WHERE (((users.id = '$user_id') AND l2.private = 0)) AND users.deleted = 0";

    $result = $GLOBALS['db']->query($query);
    $html = "<select class='selectpicker select_team' data-width='100%' id='center_code' name='center_code'>";
    while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
        ($row['team_id'] == $row['defaut_team_id']) ? $html .= "<option selected value='{$row['code_prefix']}'>{$row['team_name']}</option>" : $html .= "<option value='{$row['code_prefix']}'>{$row['team_name']}</option>";
    }
    $html .= "</select>";
    return $html;
}
