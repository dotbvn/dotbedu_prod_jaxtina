<?php


class PaymentLink extends Link2 {
    public function buildJoinDotbQuery($dotb_query, $option = array()) {
        $dotb_query->where()->notIn('payment_type', array('Enrollment','Book/Gift'));
        return $this->relationship->buildJoinDotbQuery($this, $dotb_query, $option);
    }
}

class BookGiftLink extends Link2 {
    public function buildJoinDotbQuery($dotb_query, $option = array()) {
        $dotb_query->where()->equals('payment_type','Book/Gift');
        return $this->relationship->buildJoinDotbQuery($this, $dotb_query, $option);
    }
}