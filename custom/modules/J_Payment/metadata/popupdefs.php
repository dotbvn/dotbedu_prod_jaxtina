<?php
$popupMeta = array (
    'moduleMain' => 'J_Payment',
    'varName' => 'J_Payment',
    'orderBy' => 'j_payment.name',
    'whereStatement' => "j_payment.parent_id = '{$_REQUEST['advanced_parent_id']}'
    AND j_payment.payment_type IN ('Cashholder', 'Deposit', 'Book/Gift', 'Delay Fee', 'Transfer Fee', 'Placement Test')
    AND j_payment.id <> '{$_REQUEST['advanced_not_id']}'",
    'whereClauses' => array (
        'name' => 'j_payment.name',
        'payment_type' => 'j_payment.payment_type',
        'payment_date' => 'j_payment.payment_date',
        'date_entered' => 'j_payment.date_entered',
    ),
    'searchInputs' => array (
        1 => 'name',
        4 => 'payment_type',
        5 => 'payment_date',
        6 => 'date_entered',
    ),
    'searchdefs' => array (
        'name' =>
        array (
            'name' => 'name',
            'width' => '10',
        ),
        'payment_type' =>
        array (
            'type' => 'enum',
            'label' => 'LBL_PAYMENT_TYPE',
            'width' => '10',
            'name' => 'payment_type',
        ),
        'payment_date' =>
        array (
            'type' => 'date',
            'label' => 'LBL_PAYMENT_DATE',
            'width' => '10',
            'name' => 'payment_date',
        ),
        'date_entered' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_ENTERED',
            'width' => '10',
            'name' => 'date_entered',
        ),
    ),
    'listviewdefs' => array (
        'NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_NAME',
            'default' => true,
            'link' => true,
            'name' => 'name',
        ),
        'PARENT_NAME' =>
        array (
            'type' => 'parent',
            'studio' => 'visible',
            'label' => 'LBL_PARENT_NAME',
            'sortable' => false,
            'link' => false,
            'ACLTag' => 'PARENT',
            'dynamic_module' => 'PARENT_TYPE',
            'id' => 'PARENT_ID',
            'related_fields' =>
            array (
                0 => 'parent_id',
                1 => 'parent_type',
            ),
            'width' => 10,
            'default' => true,
        ),
        'PAYMENT_TYPE' =>
        array (
            'type' => 'enum',
            'default' => true,
            'label' => 'LBL_PAYMENT_TYPE',
            'width' => 10,
        ),
        'PAYMENT_AMOUNT' =>
        array (
            'type' => 'currency',
            'default' => true,
            'label' => 'LBL_GRAND_TOTAL',
            'currency_format' => true,
            'related_fields' =>
            array (
                0 => 'currency_id',
                1 => 'base_rate',
            ),
            'width' => 10,
        ),
        'REMAIN_AMOUNT' =>
        array (
            'type' => 'currency',
            'default' => true,
            'label' => 'LBL_REMAIN_AMOUNT',
            'currency_format' => true,
            'related_fields' =>
            array (
                0 => 'currency_id',
                1 => 'base_rate',
            ),
            'width' => 10,
        ),
        'SUM_PAID' =>
        array (
            'type' => 'currency',
            'label' => 'LBL_SUM_PAID',
            'currency_format' => true,
            'related_fields' =>
            array (
                0 => 'currency_id',
                1 => 'base_rate',
            ),
            'width' => 10,
            'default' => true,
        ),
        'SUM_UNPAID' =>
        array (
            'type' => 'currency',
            'label' => 'LBL_SUM_UNPAID',
            'currency_format' => true,
            'related_fields' =>
            array (
                0 => 'currency_id',
                1 => 'base_rate',
            ),
            'width' => 10,
            'default' => true,
        ),
        'PAYMENT_DATE' =>
        array (
            'type' => 'date',
            'label' => 'LBL_PAYMENT_DATE',
            'width' => 10,
            'default' => true,
        ),
        'ASSIGNED_USER_NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_ASSIGNED_TO_NAME',
            'module' => 'Employees',
            'id' => 'ASSIGNED_USER_ID',
            'default' => true,
            'name' => 'assigned_user_name',
        ),
        'DATE_MODIFIED' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_MODIFIED',
            'width' => 10,
            'default' => true,
        ),
        'TEAM_NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_TEAM',
            'default' => true,
            'name' => 'team_name',
        ),
    ),
);
//Custom filter By Lap nguyen - Giữ lại filter
if(!empty($_REQUEST['advanced_parent_id']))
    $popupMeta['customInput']['advanced_parent_id'] =  $_REQUEST['advanced_parent_id'];
else unset($popupMeta['whereStatement']);
if(!empty($_REQUEST['advanced_not_id']))
    $popupMeta['customInput']['advanced_not_id'] =  $_REQUEST['advanced_not_id'];


