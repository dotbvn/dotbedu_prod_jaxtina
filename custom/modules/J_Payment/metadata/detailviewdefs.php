<?php
$module_name = 'J_Payment';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 =>
                    array (
                        'customCode' => '{$CUSTOM_DELETE}',
                    ),
                    2 =>
                    array (
                        'customCode' => '{$BUTTON_COPY}',
                    ),
                    3 =>
                    array (
                        'customCode' => '{$EXPORT_FROM_BUTTON} {$CUSTOM_BUTTON}',
                    ),
                    4 =>
                    array (
                        'customCode' => '{$BTN_ENROLL}',
                    ),
                ),
                'hidden' =>
                array (
                    0 => '<input type="hidden" name="opt" value="_origin_payment_type_">',
                    1 => '{include file="custom/modules/J_Payment/tpl/paymentTemplate.tpl"}
                    {include file="custom/modules/J_Payment/tpl/delayPayment.tpl"}
                    {include file="custom/modules/J_Payment/tpl/convert_payment.tpl"}
                    {include file="custom/modules/J_Payment/tpl/is_auto_enroll.tpl"}',
                    2 => '<input type="hidden" name="is_corporate" id="is_corporate" value="{$fields.is_corporate.value}">',
                    3 => '<input type="hidden" name="payment_type" id="payment_type" value="{$fields.payment_type.value}">',
                    4 => '<input type="hidden" name="team_id" id="team_id" value="{$fields.team_id.value}">',
                    6 => '<input type="hidden" name="is_paid" id="is_paid" value="{$is_paid}">',
                    7 => '<input type="hidden" name="parent_type" id="parent_type" value="{$fields.parent_type.value}">',
                    8 => '<input type="hidden" name="end_study" id="end_study" value="{$fields.end_study.value}">',
                    9 => '<input type="hidden" name="total_hours" value="{dotb_number_format var=$fields.total_hours.value precision=2}">
                    <input type="hidden" name="is_admin_inv" id="is_admin_inv" value="{$is_admin_inv}">
                    <input type="hidden" name="sys_today" id="sys_today" value="{$today}">',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '20',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '20',
                ),
                2 =>
                array (
                    'label' => '10',
                    'field' => '20',
                ),
            ),
            'javascript' => '
            {dotb_getscript file="custom/modules/J_Payment/js/detail_new.js"}
            {dotb_getscript file="custom/modules/J_Payment/js/autoenroll.js"}
            {dotb_getscript file="custom/modules/J_Payment/js/trigger_dl.js"}
            {dotb_getscript file="custom/modules/J_Payment/js/auditor.js"}
            ',
            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_ENROLLMENT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PLACE_HOLDER' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_BOOK_PLACEMENT_TEST' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_OTHER' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            //Payment Enrollment
            'LBL_ENROLLMENT' =>
            array (
                0 => array (
                    0 => 'name',
                    1 => '',
                    2 => array (
                        'name' => 'payment_type',
                    ),
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => 'payment_date',
                    2 => 'payment_expired'
                ),
                2 => array (
                    0 => array (
                        'label' => 'LBL_CLASS_NAME',
                        'customCode' => '{$html_class}'
                    ),
                    1 => 'kind_of_course',
                    2 => array (
                        'name' => 'level_string',
                    )
                ),
                3 => array (
                    0 => array (
                        'name' => 'start_study',
                        'label' => 'LBL_START_STUDY',
                    ),
                    1 => array (
                        'name' => 'end_study',
                        'label' => 'LBL_END_STUDY',
                    ),
                    2 => 'sessions'
                ),
                4 => array (
                    0 => array (
                        'name' => 'paid_amount',
                        'customLabel' => '{$MOD.LBL_PAID_AMOUNT}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_PAID_AMOUNT_HELP}\');" src="themes/RacerX/images/helpInline.png" alt="paidAmountHelpTip" class="paidAmountHelpTip">',
                    ),
                    1 => array (
                        'name' => 'paid_hours',
                        'label' => 'LBL_PAID_HOURS',
                    ),
                    2 => 'cost_per_hour',
                ),
            ),
            //Payment Place Holder
            'LBL_PLACE_HOLDER' =>
            array (
                0 => array (
                    0 => 'name',
                    1 => '',
                    2 =>
                    array (
                        'name' => 'sale_type',
                        'customCode' => '{$sale_typeQ}',
                    ),
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => array (
                        'name' => 'payment_type',
                    ),
                    2 =>
                    array (
                        'name' => 'sale_type_date',
                        'customCode' => '{$sale_type_dateQ}',
                    ),
                ),
                2 => array (
                    0 => 'amount_bef_discount',
                    1 => array (
                        'name' => 'tuition_hours',
                        'customLabel' => '{$MOD.LBL_TUITION_HOURS}:',
                    ),
                    2 => array (
                        'name' => 'j_coursefee_j_payment_1_name',
                        //'hideLabel' => true,
                        'customLabel' => '{$MOD.LBL_COURSE_FEE_ID}',
                        'customCode' => '{$j_coursefee}'
                    ),
                ),

                3 => array (
                    0 => 'discount_amount',
                    1 => 'discount_percent',
                    2 => 'payment_date',
                ),
                4 => array (
                    0 => 'final_sponsor',
                    1 => 'final_sponsor_percent',
                    2 =>
                    array(
                        'name' => 'payment_expired',
                        'customCode'   => '{$payment_expiredQ}',
                    )

                ),
                5 => array (
                    0 => 'loyalty_amount',
                    1 => 'loyalty_percent',
                    2 => 'kind_of_course'
                ),
                6 => array (
                    0 => array (
                        'name' => 'total_after_discount',
                    ),
                    1 => 'cost_per_hour',
                    2 => 'use_type'
                ),
                7 => array (
                    0 => array (
                        'name' => 'deposit_amount',
                        'customLabel' => '{$MOD.LBL_DEPOSIT_AMOUNT}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_DEPOSIT_AMOUNT_HELP}\');" src="themes/RacerX/images/helpInline.png" alt="paidAmountHelpTip" class="paidAmountHelpTip">',
                    ),
                    1 => array (
                        'hideLabel' => 'true',
                    ),
                    2 =>  array (
                        'hideLabel' => 'true',
                    ),
                ),
                8 => array (
                    0 =>
                    array (
                        'customLabel'   => '{$MOD.LBL_PAYMENT_AMOUNT}:',
                        'name'          => 'payment_amount',
                    ),
                    1 =>
                    array (
                        'customLabel'   => '{$MOD.LBL_TOTAL_HOURS}:',
                        'name'          => 'total_hours',

                    ),
                    2 => array(
                        'name'       => 'is_auto_enroll',
                        'customLabel'=> '{$MOD.LBL_IS_AUTO_ENROLL}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_AUTO_ENROLL_HELP}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '{if $fields.is_auto_enroll.value == 0}<input type="checkbox" class="checkbox" name="is_auto_enroll" id="is_auto_enroll" disabled>{/if}
                        {if $fields.is_auto_enroll.value == 1}<span class="simptip-position-top simptip-movable simptip-multiline" data-tooltip="{$MOD.LBL_START_STUDY_EXP}: {$fields.start_study.value} '."\n".' {$MOD.LBL_END_STUDY_EXP}: {$fields.end_study.value}'."\n".'{$MOD.LBL_TOTAL_ENROLL_EXP}: {dotb_number_format var=$fields.enroll_hours.value precision=2}'."\n".'{$MOD.LBL_SESSIONS}: {$fields.sessions.value}"><a href="#bwc/index.php?module=J_Class&action=DetailView&record={$fields.ju_class_id.value}">{$fields.ju_class_name.value}</a> {$fields.start_study.value} - {dotb_number_format var=$fields.enroll_hours.value precision=2}  <a id="btn_edit_auto_enroll" title="'.$GLOBALS['mod_strings']['LBL_EDIT'].'"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a> </span>{/if}',
                    ),
                ),
                9 => array (
                    0 => 'remain_amount',
                    1 =>
                    array (
                        'name'          => 'remain_hours',
                    ),
                    2 => array (
                        'name'      => 'accrual_rate_value',
                        'customLabel'=> '{if $fields.is_installment.value}{$MOD.LBL_IS_INSTALLMENT}:{else}{$MOD.LBL_ACCRUAL_RATE_VALUE}:{/if}',
                        'customCode'  => '{if $fields.is_installment.value}<input type="checkbox" class="checkbox" name="is_installment" id="is_installment" disabled {if $fields.is_installment.value}checked{/if}>
                        {$fields.installment_plan.value}
                        {else}<span {if $fields.accrual_rate_value.value > 0}style="color:green;"{/if}>{$fields.accrual_rate_value.value*100}%</span>
                        {/if}',
                    ),
                ),

                10 => array (
                    0 => array(
                        'label' => 'LBL_SUM_PAID',
                        'customCode' => '{$PAID_AMOUNT}{$old_remain_text}',
                    ),
                    1 => array(
                        'label' => 'LBL_SUM_UNPAID',
                        'customCode' => '{$UNPAID_AMOUNT}',
                    ),
                    2 => 'account_name',
                ),
            ),
            //Payment Deposit
            'LBL_DEPOSIT' =>
            array (
                0 => array (
                    0 => 'name',
                    1 => '',
                    2 =>
                    array (
                        'name' => 'sale_type',
                        'customCode' => '{$sale_typeQ}',
                    ),
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => array (
                        'name' => 'payment_type',
                    ),
                    2 =>
                    array (
                        'name' => 'sale_type_date',
                        'customCode' => '{$sale_type_dateQ}',
                    ),
                ),
                2 => array (
                    0 =>
                    array (
                        'customLabel'   => '{$MOD.LBL_PAYMENT_AMOUNT}:',
                        'name'          => 'payment_amount',
                    ),
                    1 => 'kind_of_course',
                    2 => 'payment_date',

                ),
                3 => array (
                    0 => 'remain_amount',
                    1 => 'account_name',
                    2 => array(
                        'name' => 'payment_expired',
                        'customCode' => '{$payment_expiredQ}',
                    ),
                ),
                4 => array (
                    0 => array(
                        'label' => 'LBL_SUM_PAID',
                        'customCode' => '{$PAID_AMOUNT}{$old_remain_text}',
                    ),
                    1 => array(
                        'customLabel' => '{$MOD.LBL_SUM_UNPAID}:<img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_PAYMENT_WARNING}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '{$UNPAID_AMOUNT}',
                    ),
                    2 => array(
                        'name'       => 'is_auto_enroll',
                        'customLabel'=> '{$MOD.LBL_RELATED_TO_CLASS}:',
                        'customCode' => '{if $fields.is_auto_enroll.value == 0}<input type="checkbox" class="checkbox" name="is_auto_enroll" id="is_auto_enroll" disabled>{/if}
                        {if $fields.is_auto_enroll.value == 1}<span class="simptip-position-top simptip-movable simptip-multiline" data-tooltip="{$MOD.LBL_START_STUDY_EXP}: {$fields.start_study.value} '."\n".' {$MOD.LBL_END_STUDY}: {$fields.end_study.value}"><a href="#bwc/index.php?module=J_Class&action=DetailView&record={$fields.ju_class_id.value}">{$fields.ju_class_name.value}</a> {$fields.start_study.value} - {$fields.end_study.value}  </span>{/if}',
                    ),
                ),
            ),
            //Payment BookGift & Payment Placement Test
            'LBL_BOOK_PLACEMENT_TEST' => array (
                0 => array (
                    0 => 'name',
                    1 => 'payment_type',
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => array (
                        'name' => 'payment_date',
                    ),
                ),
                2 => array (
                    0 => array(
                        'customCode' => '
                        <table id="tblbook" style="width: 100%;" border="1" class="list view">
                        <thead > <tr> <th width="30%" style="text-align: center;">{$MOD.LBL_BOOK_NAME}</th>
                        <th width="10%" style="text-align: center;">{$MOD.LBL_UNIT}</th>
                        <th width="20%" style="text-align: center;">{$MOD.LBL_QUATITY}</th>
                        <th width="25%" style="text-align: center;">{$MOD.LBL_PRICE}</th>
                        <th width="25%" style="text-align: center;">{$MOD.LBL_AMOUNT}</th>
                        </tr></thead>
                        <tbody id="tbodybook">{$bookList}</tbody>
                        <tfoot><tr><td style="text-align: center;" colspan="2"><b>{$MOD.LBL_SUB_TOTAL}:</b></td>
                        <td style="text-align: center;"><b>{$total_book_quantity}</b></td><td></td>
                        <td style="text-align: center;"><b>{$total_book_amount}</b></td>
                        </tr></tfoot>
                        </table>',
                        'hideLabel' => 'true',
                    ),
                    1 => array(
                        'name' => 'j_payment_j_payment_2_name',
                        'label' => 'LBL_PRIMARY_PAYMENT',
                   ),
                ),
                3 => array (
                    0 =>'amount_bef_discount',
                    1 => 'account_name'
                ),
                4 => array (
                    0 =>array (
                        'customLabel' => '{$MOD.LBL_TOTAL_DISCOUNT_SPONSOR}:',
                        'customCode'  => '{$total_discount_sponsor}'
                    ),
                    1 => array(
                        'name'       => 'is_auto_enroll',
                        'customLabel'=> '{$MOD.LBL_RELATED_TO_CLASS}:',
                        'customCode' => '{if $fields.is_auto_enroll.value == 0}<input type="checkbox" class="checkbox" name="is_auto_enroll" id="is_auto_enroll" disabled>{/if}
                        {if $fields.is_auto_enroll.value == 1}<span class="simptip-position-top simptip-movable simptip-multiline" data-tooltip="{$MOD.LBL_START_STUDY_EXP}: {$fields.start_study.value} '."\n".' {$MOD.LBL_END_STUDY}: {$fields.end_study.value}"><a href="#bwc/index.php?module=J_Class&action=DetailView&record={$fields.ju_class_id.value}">{$fields.ju_class_name.value}</a> {$fields.start_study.value} - {$fields.end_study.value}  </span>{/if}',
                    ),
                ),
                5 => array (
                    0 =>'total_after_discount',
                    1 =>'vat_rate'
                ),

                7 => array (
                    0 =>  array (
                        'customLabel'   => '{$MOD.LBL_PAYMENT_AMOUNT}:',
                        'name'          => 'payment_amount',
                    ),
                    1 => 'vat_amount'
                ),
                8 => array (
                    0 => array(
                        'label' => 'LBL_SUM_PAID',
                        'customCode' => '{$PAID_AMOUNT}{$old_remain_text}',
                    ),
                    1 => array(
                        'customLabel' => '{$MOD.LBL_SUM_UNPAID}:<img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_PAYMENT_WARNING}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '{$UNPAID_AMOUNT}',
                    ),

                ),
            ),
            //Payment Moving Out
            'LBL_MOVING' => array (
                0 => array (
                    0 => 'name',
                    1 => array (
                        'customLabel' => '{$PAYMENT_RELA_LABEL}:',
                        'customCode' => '{$PAYMENT_RELA}',
                    ),
                    2 => array (
                        'hideLabel' => true,
                    ),
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => 'payment_type',
                    2 => 'payment_date',
                ),
                2 => array (
                    0 =>
                    array (
                        'customLabel'   => '{$MOD.LBL_PAYMENT_AMOUNT}:',
                        'name'          => 'payment_amount',
                    ),
                    1 =>
                    array (
                        'name'          => 'total_hours',
                    ),
                    2 => 'use_type',
                ),
                3 => array (
                    0 => 'remain_amount',
                    1 =>

                    array (
                        'name'          => 'remain_hours',
                    ),
                    2 =>
                    array (
                        'name' => 'payment_expired',
                        'customCode' => '{$payment_expiredQ}',
                    ),
                ),
                4 => array (
                    0 => array (
                        'name' => 'refund_revenue',
                        'label' => 'LBL_DROP_REVENUE',
                    ),
                    1 => '',
                    2 => ''
                ),
            ),
            //Payment Transfer
            'LBL_TRANSFER' => array (
                0 => array (
                    0 => 'name',
                    1 => array (
                        'customLabel' => '{$PAYMENT_RELA_LABEL}',
                        'customCode' => '{$PAYMENT_RELA}',
                    ),
                    2 => 'payment_type',
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => array (
                        'customLabel' => '{$STUDENT_RELA_LABEL}',
                        'customCode' => '{$STUDENT_RELA}',
                    ),
                    2 => 'payment_date',
                ),
                2 => array (
                    0 =>  array (
                        'customLabel'   => '{$MOD.LBL_PAYMENT_AMOUNT}:',
                        'name'          => 'payment_amount',
                    ),

                    1 =>
                     array (
                        'name'          => 'total_hours',
                    ),
                    2 => 'use_type',
                ),
                3 => array (
                    0 =>  'remain_amount',

                    1 =>array (
                        'customLabel'   => '{$MOD.LBL_REMAIN_HOURS}:',
                        'name'          => 'remain_hours',
                    ),
                    2 =>
                    array (
                        'name' => 'payment_expired',
                        'customCode' => '{$payment_expiredQ}',
                    ),
                ),
                4 => array (
                    0 => array (
                        'name' => 'refund_revenue',
                        'label' => 'LBL_DROP_REVENUE',
                    ),
                    1 => '',
                    2 => ''
                ),
            ),
            //Payment Refund
            'LBL_REFUND' => array (
                0 => array (
                    0 => 'name',
                    1 => array (
                        'hideLabel' => true,
                    ),
                    2 => array (
                        'hideLabel' => true,
                    ),
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => array (
                        'hideLabel' => true,
                    ),
                    2 => 'payment_type',
                ),
                2 => array (
                    0 => array (
                        'name' => 'payment_amount',
                        'label' => 'LBL_REFUND_AMOUNT',
                    ),
                    1 => array (
                        'hideLabel' => true,
                    ),
                    2 => array (
                        'name' => 'refund_revenue',
                        'label' => 'LBL_DROP_REVENUE',
                    ),
                ),
                3 => array (
                    0 => array (
                        'name' => 'payment_date',
                        'label' => 'LBL_REFUND_DATE',
                    ),
                    1 => array (
                        'hideLabel' => true,
                    ),
                    2 => array (
                        'name' => 'payment_method',
                        'label' => 'LBL_EXPENSE_METHOD',
                    ),

                ),
            ),
            // Payment Delay
            'LBL_DELAY' => array (
                0 => array (
                    0 => 'name',
                    1 => array (
                        'hideLabel' => true,
                    ),
                    2 => 'payment_type'
                ),
                1 => array (
                    0 => array (
                        'name' => 'parent_name',
                    ),
                    1 => 'use_type',
                    2 => 'payment_date',
                ),
                2 => array (
                    0 =>
                    array (
                        'customLabel'   => '{$MOD.LBL_PAYMENT_AMOUNT}:',
                        'name'          => 'payment_amount',
                    ),

                    1 =>
                    array (
                        'name'          => 'total_hours',
                    ),
                    2 => array(
                        'name' => 'payment_expired',
                        'customCode' => '{$payment_expiredQ}',
                    ),
                ),
                3 => array (
                    0 =>
                    array(
                        'name' => 'remain_amount',
                        'customCode' => '<span id="remain_amount">{dotb_number_format var=$fields.remain_amount.value}</span>{$old_remain_text}',
                    ),
                    1 =>
                    array (
                        'customLabel'   => '{$MOD.LBL_REMAIN_HOURS}:',
                        'name'          => 'remain_hours',
                    ),
                    2 => array (
                        'hideLabel' => true,
                    ),
                ),
            ),
            //Desctiption & Assign To & Team
            'LBL_OTHER' => array (
                0 => array (
                    0 => 'description',
                    1 =>  array (
                        'name' => 'note',
                        'customCode' => '{$fields.note.value}  {$revenue_link}',
                    ),
                ),
                1 => array (
                    0 =>
                    array (
                        'name' => 'assigned_user_name',
                        'customCode' => '{$assigned_user_idQ}',
                    ),
                    1 => array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),
                2 => array (
                    0 => 'team_name',
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
                3 =>
                array (
                    0 =>array (
                        'name' => 'user_closed_sale',
                       'customCode' => '{$user_closed_sale_idQ}',
                    ),
                    1 =>
                    array (
                        'name' => 'user_pt_demo',
                        'customCode' => '{$user_pt_demo_idQ}',
                    ),
                ),
            ),
        ),
    ),
);
