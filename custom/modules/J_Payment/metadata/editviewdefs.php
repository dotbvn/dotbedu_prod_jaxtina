<?php
$module_name = 'J_Payment';
$viewdefs[$module_name] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'enctype' => 'multipart/form-data',
                'hidden' =>
                array (
                    0 => '<input type="hidden" name="opt" value="_origin_payment_type_">',
                    1 => '{if $is_mtf == 1}{else}
                    <input type="hidden" name="sub_discount_amount" id="sub_discount_amount" value="{dotb_number_format var=$fields.sub_discount_amount.value}">
                    <input type="hidden" name="sub_discount_percent" id="sub_discount_percent" value="{dotb_number_format var=$fields.sub_discount_percent.value precision=2}">
                    {include file="custom/modules/J_Payment/tpl/discountTable.tpl"}
                    {include file="custom/modules/J_Payment/tpl/loyatyTable.tpl"}
                    {include file="custom/modules/J_Payment/tpl/sponTable.tpl"}
                    {$discount_list}{$sponsor_list}{$loyalty_list}
                    <input type="hidden" name="coursefee_list" id="coursefee_list" value="{$fields.coursefee_list.value}">
                    <input type="hidden" name="ratio" id="ratio" value="{$ratio}">
                    <input type="hidden" name="catch_limit" id="catch_limit" value="{$fields.catch_limit.value}">
                    <input type="hidden" name="limited_discount_amount" id="limited_discount_amount" value="{$fields.limited_discount_amount.value}">
                    {/if}',
                    3 => '<input type="hidden" name="payment_list" id="payment_list" value="{$fields.payment_list.value}">',
                    7 => '{$lock_assigned_to}',
                    10 => '<input type="hidden" name="current_team_id" id="current_team_id" value="{$fields.team_id.value}">',
                    14 => '<input type="hidden" name="primary_id" id="primary_id" value="{$primary_id}">',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '45',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '35',
                ),
            ),
            'javascript' => '<link rel="stylesheet" href="{dotb_getjspath file=custom/modules/J_Payment/css/custom_styles.css}"/>
            {if $is_mtf == 1}
            {dotb_getscript file="custom/modules/J_Payment/js/edit_mtr.js"}
            {else}
            {$limit_discount_percent}
            {$min_points_loyalty}
            {dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.min.js"}
            {dotb_getscript file="custom/modules/J_Payment/js/autoenroll.js"}
            {dotb_getscript file="custom/modules/J_Payment/js/editw_jax.js"}
            {dotb_getscript file="custom/modules/J_Payment/js/extentionc.js"}
            {/if}
            {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
            <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>
            ',
            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_SELECT_PAYMENT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_OTHER_PAYMENT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PAYMENT_MOVING' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PAYMENT_TRANSFER' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PAYMENT_REFUND' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => false,
        ),
        'panels' =>
        array (
            'LBL_SELECT_PAYMENT' =>
            array (
                0 =>
                array (
                    0 => array (
                        'hideLabel' => true,
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/paymentList.tpl"}'
                    )
                ),
            ),
            // PAYMENT OTHER
            'LBL_OTHER_PAYMENT' =>
            array (
                array (
                    0 =>
                    array (
                        'name' => 'parent_name',
                        'customLabel' => '{$MOD.LBL_STUDENT}:',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldStudent.tpl"}'
                    ),
                    1 =>
                    array (
                        'name' => 'payment_type',
                        'customCode' => '
                        {if !empty($fields.id.value)}
                        {$payment_type_text}<input type="hidden" name="payment_type" id="payment_type" value="{$fields.payment_type.value}"/>
                        {else}
                        {html_options name="payment_type" id="payment_type" options=$payment_type_list selected=$fields.payment_type.value}{/if}
                        ',
                    ),
                ),
                array (
                    0 =>  '',
                    1 => array(
                        'name' => 'kind_of_course',
                        'customCode' => '{html_options name="kind_of_course" id="kind_of_course" options=$fields.kind_of_course.options selected=$fields.kind_of_course.value}'
                    )
                ),
                array (
                    0 => array (
                        'name' => 'j_coursefee_j_payment_1j_coursefee_ida',
                        'customCode' => '{$coursefee}',
                        'customLabel' => '{$MOD.LBL_COURSE_FEE_ID}: <span class="required">*</span>',
                    ),
                    1 => array (
                        'customLabel' => '<table width="100%" style="padding:0px!important;"><tbody id="package_label">
                        {$package_label}
                        </tbody></table>',
                        'customCode' => '<table width="100%" style="padding:0px!important;"><tbody id="package_list">
                        {$package_list}
                        </tbody></table>',
                    ),
                ),
                array (
                    0 => array (
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/BookTemplate.tpl"}',
                    ),
                    1 =>
                    array (
                        'name' => 'is_free_book',
                    ),
                ),
                array (
                    0 => array (
                        'name' => 'discount_amount',
                        'customLabel' => '',
                        'customCode' => '<div id="tab_discount" width="80%" style="display:none;white-space: nowrap;">
                        <ul style="float:left">
                        <li><a href="#discount" group="discount">{$MOD.LBL_DISCOUNT}</a></li>
                        <li><a href="#sponsor" group="sponsor">{$MOD.LBL_SPONSOR}</a></li>
                        <li><a href="#loyalty" group="loyalty">{$MOD.LBL_LOYALTY}</a></li>
                        </ul><input style="float:left;margin: 3px 0px 0px 20px;" class="button discount" type="button" name="btn_get_discount" value="{$MOD.LBL_ADD_DISCOUNT}" id="btn_get_discount">
                        <input style="float:left;margin: 3px 0px 0px 20px;display:none;" class="button sponsor" type="button" name="btn_add_sponsor" value="{$MOD.LBL_ADD_SPONSOR}" id="btn_add_sponsor">
                        <input style="float:left;margin: 3px 0px 0px 20px;display:none;" class="button loyalty" type="button" name="btn_get_loyalty" value="{$MOD.LBL_USE_LOYALTY}" id="btn_get_loyalty">

                        <div id="discount"></div>
                        <div id="sponsor"></div>
                        <div id="loyalty"></div>
                        </div>',
                    ),
                    1 => array (
                        'name' => 'amount_bef_discount',
                        'label' => 'LBL_AMOUNT_BEF_DISCOUNT',
                        'customCode' => '<table width="100%" style="padding:0px!important;">
                        <tbody><tr>
                        <td style="padding: 0px !important;" width = "40%">
                        <input type="hidden" name="tuition_fee" id="tuition_fee" value="{dotb_number_format var=$fields.tuition_fee.value}">
                        <input class="currency input_readonly" readonly type="text" name="amount_bef_discount" id="amount_bef_discount" size="20" maxlength="26" value="{dotb_number_format var=$fields.amount_bef_discount.value}" title="{$MOD.LBL_AMOUNT_BEF_DISCOUNT}" tabindex="0"  style="font-weight: bold;color: rgb(165, 42, 42);"></td>
                        <td width="25%" scope="col" class="tuition_hours">{$MOD.LBL_TUITION_HOURS}: <span class="required">*</span></td>
                        <td width="35%"><input autocomplete="off" type="text" class="input_readonly tuition_hours" readonly name="tuition_hours" id="tuition_hours" value="{dotb_number_format var=$fields.tuition_hours.value precision=2}" tabindex="0" size="7" maxlength="10"  style="color: rgb(165, 42, 42);" ></td>
                        </tr></tbody>
                        </table>',
                    ),
                ),
                array (
                    0 => array (
                        'name' => 'loyalty_amount',
                        'customCode' => '
                        <div class="discount">
                        <table width="100%" style="padding:0px!important;">
                        <tbody><tr>
                        <td width="40%" style="padding: 0px !important;"><input class="currency input_readonly" type="text" name="discount_amount" id="discount_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.discount_amount.value}" title="{$MOD.LBL_DISCOUNT_AMOUNT}" tabindex="0"  style="font-weight: bold;" readonly></td>
                        <td width="25%" scope="col"><label>{$MOD.LBL_DISCOUNT_PERCENT}: </label></td>
                        <td width="35%"><input class="input_readonly" autocomplete="off" type="text" name="discount_percent" id="discount_percent" value="{dotb_number_format var=$fields.discount_percent.value precision=2}" tabindex="0" size="7" maxlength="10" readonly></td>
                        </tr></tbody>
                        </table>
                        </div>
                        <div class="sponsor" style="display:none;">
                        <table width="100%" style="padding:0px!important;">
                        <tbody><tr colspan="3">
                        <td style="padding: 0px !important;" width = "40%">
                        <input readonly size="20" maxlength="26" class="currency input_readonly" name="final_sponsor" type="text" id="final_sponsor" value="{dotb_number_format var=$fields.final_sponsor.value}" tabindex="0"  style="font-weight: bold;"></td>
                        <td width="25%" scope="col"><label>{$MOD.LBL_FINAL_SPONSOR_PERCENT}: </label></td>
                        <td width="35%"><input class="input_readonly" autocomplete="off" type="text" name="final_sponsor_percent" id="final_sponsor_percent" value="{dotb_number_format var=$fields.final_sponsor_percent.value precision=2}" tabindex="0" size="7" maxlength="10" readonly></td>
                        </tr></tbody>
                        </table>
                        </div>
                        <div class="loyalty" style="display:none;">
                        <table width="100%" style="padding:0px!important;">
                        <tbody><tr colspan="3">
                        <td width="40%" style="padding: 0px !important;"><input class="currency input_readonly" type="text" name="loyalty_amount" id="loyalty_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.loyalty_amount.value}" title="{$MOD.LBL_LOYALTY_AMOUNT}" tabindex="0"  style="font-weight: bold;" readonly></td>
                        <td width="25%" scope="col">{$MOD.LBL_LOYALTY_PERCENT}: </td>
                        <td width="35%"><input class="input_readonly" autocomplete="off" type="text" name="loyalty_percent" id="loyalty_percent" value="{dotb_number_format var=$fields.loyalty_percent.value precision=2}" tabindex="0" size="7" maxlength="10" readonly></td>
                        </tr></tbody>
                        </table>
                        </div>
                        ',
                        'customLabel' => '
                        <label class="discount">{$MOD.LBL_DISCOUNT_AMOUNT}: </label>
                        <label class="sponsor" style="display:none;">{$MOD.LBL_FINAL_SPONSOR}: </label>
                        <label class="loyalty" style="display:none;">{$MOD.LBL_LOYALTY_AMOUNT}: </label>
                        '
                    ),
                    1 =>
                    array (
                        'customLabel' => '{$MOD.LBL_TOTAL_DISCOUNT_SPONSOR}:',
                        'customCode'  => '<table width="100%" style="padding:0px!important;">
                        <tbody><tr>
                        <td style="padding: 0px !important;" width = "40%"><input class="currency input_readonly" type="text" name="total_discount_sponsor" id="total_discount_sponsor" size="20" maxlength="26" value="{dotb_number_format var=$fields.total_discount_sponsor.value}" title="{$MOD.LBL_TOTAL_DISCOUNT_SPONSOR}" tabindex="0"  style="font-weight: bold; color: rgb(165, 42, 42);" readonly></td>
                        <td width="25%" scope="col"><label>{$MOD.LBL_TOTAL_HOURS}: </label></td>
                        <td width="35%"><input class="input_readonly" autocomplete="off" type="text" name="total_hours" id="total_hours" value="{dotb_number_format var=$fields.total_hours.value precision=2}" tabindex="0" size="7" maxlength="10" readonly></td>
                        </tr></tbody>
                        </table>'
                    ),
                ),
                array (
                    0 => '',
                    1 =>  array (
                        'name' => 'total_after_discount',
                        'customCode' => '<input class="currency input_readonly" type="text" name="total_after_discount" id="total_after_discount" size="20" maxlength="26" value="{dotb_number_format var=$fields.total_after_discount.value}" title="{$MOD.LBL_TOTAL_AFTER_DISCOUNT}" tabindex="0"  style="font-weight: bold;" readonly>',
                    ),
                ),
                array (
                    0 => array (
                        'hideLabel' => true,
                    ),
                    1 =>
                    array (
                        'name' => 'deposit_amount',
                        'customCode' => '
                        <table width="100%" style="padding:0px!important;">
                        <tbody><tr colspan="3">
                        <td style="padding: 0px !important;" width = "40%"><input class="currency input_readonly" type="text" name="deposit_amount" id="deposit_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.deposit_amount.value}" title="{$MOD.LBL_DISCOUNT_AMOUNT}" tabindex="0"  style="font-weight: bold;" readonly></td>
                        </tr></tbody>
                        </table>',
                        'customLabel' => '<label style="color: green;">{$MOD.LBL_DEPOSIT_AMOUNT}: </label>'
                    ),

                ),
                array (
                    0 => array (
                        'name' => 'vat_rate',
                        'customCode' => '<input class="input_readonly" type="text" name="vat_rate" id="vat_rate" size="20" maxlength="26" value="{dotb_number_format var=$fields.vat_rate.value precision=2}" title="{$MOD.LBL_VAT_AMOUNT}" tabindex="0"  style="font-weight: bold;" readonly>',
                    ),
                    1 =>  array (
                        'name' => 'vat_amount',
                        'customCode' => '<input class="currency input_readonly" type="text" name="vat_amount" id="vat_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.vat_amount.value}" title="{$MOD.LBL_VAT_AMOUNT}" tabindex="0"  style="font-weight: bold;" readonly>',
                    ),
                ),
                array (
                    0 => 'payment_date',
                    1 =>
                    array (
                        'name' => 'payment_amount',
                        'customLabel' => '<b>{$MOD.LBL_GRAND_TOTAL}:</b>',
                        'customCode' => '<table width="100%" style="padding:0px!important;">
                        <tbody><tr colspan="3">
                        <td style="padding: 0px !important;" width = "40%"><input class="currency input_readonly" type="text" name="payment_amount" id="payment_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.payment_amount.value}" title="{$MOD.LBL_GRAND_TOTAL}" tabindex="0"  style="font-weight: bold; color: rgb(165, 42, 42);" readonly></td>
                        <td width="25%" scope="col"><label>{$MOD.LBL_TOTAL_REWARDS_AMOUNT} <span id="accrual_rate_label"></span>: </label><input type="hidden" id="accrual_rate_value" name="accrual_rate_value" value="{$fields.accrual_rate_value.value}"></td>
                        <td width="35%"><input class="input_readonly" autocomplete="off" type="text" name="total_rewards_amount" id="total_rewards_amount" value="{dotb_number_format var=$total_rewards_amount precision=2}" tabindex="0" size="20" maxlength="10" style="font-weight: bold; text-align: right; color: green;" readonly></td>
                        </tr></tbody>
                        </table>
                        ',
                    ),
                ),
                array (
                    0 => array (
                        'name' => 'number_of_payment',
                        'customLabel' => '{$MOD.LBL_SPLIT_PAYMENT}: <span class="required">*</span>',
                        'customCode' => '<table width="100%" style="padding:0px!important;">
                        <tbody><tr>
                        <td style="padding: 0px !important;" width = "20%">{html_options name="number_of_payment" id="number_of_payment" options=$fields.number_of_payment.options selected=$fields.number_of_payment.value}</td>
                        <td width="15%" class="custom_split" scope="col">{$MOD.LBL_REPEAT_EVERY}</td>
                        <td width="25%" class="custom_split">
                        <input type="text" name="repeat_every_pay" id="repeat_every_pay" value="{$fields.repeat_every_pay.value}" size="4" maxlength="10">
                        {html_options name="repeat_option_pay" id="repeat_option_pay" options=$fields.repeat_option_pay.options selected=$fields.repeat_option_pay.value}
                        </td>
                        <td width="15%" scope="col" class="custom_split">{$MOD.LBL_NUM_MONTH_PAY} </td>
                        <td width="20%" class="custom_split"><input type="text" name="num_month_pay" id="num_month_pay" value="{$fields.num_month_pay.value}" size="4" maxlength="10"></td>
                        </tr></tbody>
                        </table>',
                    ),
                    1 => array (
                        'name' => 'is_auto_enroll',
                        'customLabel' => '<span id="aelbl1">{$MOD.LBL_IS_AUTO_ENROLL}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_AUTO_ENROLL_HELP}\');" src="themes/RacerX/images/helpInline.png"></span>
                        <span id="aelbl2" style="display:none;">{$MOD.LBL_RELATED_TO_CLASS}:</span>',
                    ),
                ),
                array (
                    0 => array (
                        'hideLabel' => true,
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/payment_detail.tpl"}'
                    ),
                    1 =>
                    array (
                        'hideLabel' => true,
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/is_auto_enroll.tpl"}'
                    ),
                ),
//                array (
//                    0 => 'is_corporate',
//                    1 => array ('hideLabel' => true)
//                ),
//                array (
//                    0 => array(
//                        'hideLabel' => true,
//                        'customCode' => '{include file="custom/modules/J_Payment/tpl/is_corporate.tpl"}'
//                    ),
//                    1 => array ('hideLabel' => true)
//                ),
            ),
            // Payment Moving
            'LBL_PAYMENT_MOVING' =>
            array (
                1 =>
                array (
                    0 => array (
                        'name' => 'parent_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldStudent.tpl"}',
                        'customLabel' => '{$MOD.LBL_STUDENT}:
                        <input type="hidden" id="json_payment_list" name="json_payment_list">
                        <input type="hidden" name="payment_type" id="payment_type" value="{$fields.payment_type.value}">
                        <input type="hidden" name="total_hours" id="total_hours" value="{dotb_number_format var=$fields.total_hours.value precision=2}">
                        ',
                    ),
                    1 => array (
                        'name' => 'move_to_center_name',
                        'customLabel' => '{$MOD.LBL_MOVE_TO_CENTER_NAME}: <span class="required">*</span>'
                    ),
                ),
                2 => array (
                    0 => array (
                        'name' => 'payment_amount',
                        'customLabel' => '{$MOD.LBL_MOVING_AMOUNT}: <span class="required">*</span> <img border="0" onclick="return DOTB.util.showHelpTips(this,\'Moving Amount much be less than or equal to the Total remain amount of the payment is selected\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input class="currency input_readonly" type="text" name="payment_amount" id="payment_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.payment_amount.value}" title="{$MOD.LBL_MOVING_AMOUNT}" tabindex="0"  style="font-weight: bold;color: rgb(165, 42, 42);" readonly>',
                    ),
                    1 => array (
                        'name' => 'refund_revenue',
                        'customLabel' => '{$MOD.LBL_DROP_REVENUE}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'If the Admin Charge equal 0 system will not generate Revenue drop\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input type="text" name="refund_revenue" class="currency" id="refund_revenue" size="20" maxlength="26" value="{dotb_number_format var=$fields.refund_revenue.value}" title="{$MOD.LBL_REFUND_REVENUE}" tabindex="0"  style="font-weight: bold; text-align:right; color: rgb(165, 42, 42);" >',
                    ),
                ),
                3 =>
                array (
                    0 =>  array(
                        'name' => 'moving_tran_out_date',
                        'label' => 'LBL_MOVING_OUT_DATE',
                    ),
                    1 => ''
                ),
                4 =>
                array (
                    0 =>  array (
                        'name' => 'description',
                        'label' => 'LBL_DESCRIPTION',
                        'displayParams' =>
                        array (
                            'required' => true,
                        ),
                    ),
                ),
                5 =>
                array (
                    0 =>  array (
                        'name' => 'assigned_user_name',
                    ),
                    1 =>
                    array (
                        'name' => 'team_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldTeam.tpl"}',
                    ),
                ),
            ),
            //Panel Transfer
            'LBL_PAYMENT_TRANSFER' =>
            array (
                1 =>
                array (
                    0 => array (
                        'name' => 'parent_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldStudent.tpl"}',
                        'customLabel' => '{$MOD.LBL_STUDENT}:
                        <input type="hidden" id="json_payment_list" name="json_payment_list">
                        <input type="hidden" name="payment_type" id="payment_type" value="{$fields.payment_type.value}">
                        <input type="hidden" name="total_hours" id="total_hours" value="{dotb_number_format var=$fields.total_hours.value precision=2}">
                        ',
                    ),
                    1 => array (
                        'name' => 'transfer_to_student_name',
                        'displayParams' =>
                        array (
                            'field_to_name_array' =>
                            array (
                                'id' => 'transfer_to_student_id',
                                'name' => 'transfer_to_student_name',
                            ),
                            'required' => true,
                            'class' => 'sqsNoAutofill',
                        ),
                    ),
                ),
                2 => array (
                    0 => array (
                        'name' => 'payment_amount',
                        'customLabel' => '{$MOD.LBL_MOVING_AMOUNT}: <span class="required">*</span> <img border="0" onclick="return DOTB.util.showHelpTips(this,\'Transfer Amount much be less than or equal to the Total remain amount of the payment is selected\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input class="currency input_readonly" type="text" name="payment_amount" id="payment_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.payment_amount.value}" title="{$MOD.LBL_MOVING_AMOUNT}" tabindex="0"  style="font-weight: bold;color: rgb(165, 42, 42);" readonly>',
                    ),
                    1 => array (
                        'name' => 'refund_revenue',
                        'customLabel' => '{$MOD.LBL_DROP_REVENUE}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'If the Admin Charge equal 0 system will not generate Revenue drop\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input type="text" name="refund_revenue" class="currency" id="refund_revenue" size="20" maxlength="26" value="{dotb_number_format var=$fields.refund_revenue.value}" title="{$MOD.LBL_REFUND_REVENUE}" tabindex="0"  style="font-weight: bold; text-align:right; color: rgb(165, 42, 42);" >',
                    ),
                ),
                3 =>
                array (
                    0 =>  array(
                        'name' => 'moving_tran_out_date',
                        'label' => 'LBL_TRANSFER_OUT_DATE',
                    ),
                    1 => ''
                ),
                4 =>
                array (
                    0 =>  array (
                        'name' => 'description',
                        'displayParams' =>
                        array (
                            'required' => true,
                        ),

                    ),
                ),
                5 =>
                array (
                    0 =>  array (
                        'name' => 'assigned_user_name',
                    ),
                    1 =>
                    array (
                        'name' => 'team_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldTeam.tpl"}',
                    ),
                ),
            ),
            //Panel Refund
            'LBL_PAYMENT_REFUND' =>
            array (
                1 =>
                array (
                    0 => array (
                        'name' => 'parent_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldStudent.tpl"}',
                        'customLabel' => '{$MOD.LBL_STUDENT}:
                        <input type="hidden" id="json_payment_list" name="json_payment_list">
                        <input type="hidden" name="payment_type" id="payment_type" value="{$fields.payment_type.value}">
                        <input type="hidden" name="total_hours" id="total_hours" value="{dotb_number_format var=$fields.total_hours.value precision=2}">
                        ',
                    ),
                ),
                2 => array (
                    0 => array (
                        'name' => 'payment_amount',
                        'customLabel' => '{$MOD.LBL_REFUND_AMOUNT}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'Refund Amount much be less than or equal to the Total remain amount of the payment is selected\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input class="currency" type="text" name="payment_amount" id="payment_amount" size="20" maxlength="26" value="{dotb_number_format var=$fields.payment_amount.value}" title="{$MOD.LBL_REFUND_AMOUNT}" tabindex="0"  style="font-weight: bold;color: rgb(165, 42, 42);" >',
                    ),

                    1 => array (
                        'name' => 'refund_revenue',
                        'customLabel' => '{$MOD.LBL_DROP_REVENUE}: <img border="0" onclick="return DOTB.util.showHelpTips(this,\'If the Admin Charge equal 0 system will not generate Revenue drop\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input type="text" name="refund_revenue" class="currency" id="refund_revenue" size="20" maxlength="26" value="{dotb_number_format var=$fields.refund_revenue.value}" title="{$MOD.LBL_REFUND_REVENUE}" tabindex="0"  style="font-weight: bold; text-align:right; color: rgb(165, 42, 42);" >',
                    ),

                ),
                3 =>
                array (
                    0 =>  array(
                        'name' => 'moving_tran_out_date',
                        'label' => 'LBL_REFUND_DATE',
                    ),
                    1 =>
                    array (
                        'name' => 'payment_method',
                        'label' => 'LBL_EXPENSE_METHOD',
                    ),
                ),
                4 =>
                array (
                    0 =>  array (
                        'name' => 'description',
                        'displayParams' =>
                        array (
                            'required' => true,
                        ),
                    ),
                ),
                5 =>
                array (
                    0 =>  array (
                        'name' => 'assigned_user_name',
                    ),
                    1 =>
                    array (
                        'name' => 'team_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldTeam.tpl"}',
                    ),
                ),
            ),
            'LBL_OTHER' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'description',
                        'label' => 'LBL_DESCRIPTION',
                        'displayParams' =>
                        array (
                            'rows' => 4,
                            'cols' => 60,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'note',
                        'label' => 'LBL_NOTE',
                        'customCode' => '<textarea id="note" name="note" rows="4" cols="50" title="Note" tabindex="0">{$fields.note.value}</textarea>'
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'assigned_user_name',
                        'displayParams' =>
                        array (
                            'required' => true,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'team_name',
                        'customCode' => '{include file="custom/modules/J_Payment/tpl/fieldTeam.tpl"}',
                    ),
                ),
                                    2 =>
                                    array (
                                        0 => array (
                                            'name' => 'user_closed_sale',
                                            'displayParams' =>
                                            array (
                                                'required' => true,
                                            ),
                                        ),
                                    ),
                                    3 =>
                                    array (
                                        0 => array (
                                            'name' => 'user_pt_demo',
                                            'displayParams' =>
                                            array (
                                                'required' => true,
                                            ),
                                        ),
                                    ),
            ),
        ),
    ),
);
