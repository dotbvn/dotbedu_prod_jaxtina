<?php
// created: 2022-06-12 14:38:02
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '10%',
        'default' => true,
        'link' => true,
    ),
    'payment_type' =>
    array (
        'vname' => 'LBL_PAYMENT_TYPE',
        'width' => '9%',
        'default' => true,
    ),
    'ju_class_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_RELATED_CLASS_NAME',
        'id' => 'JU_CLASS_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'J_Class',
        'target_record_key' => 'ju_class_id',
    ),
    'payment_date' =>
    array (
        'vname' => 'LBL_PAYMENT_DATE',
        'width' => '10%',
        'default' => true,
    ),
    'payment_amount' =>
    array (
        'type' => 'currency',
        'default' => true,
        'vname' => 'LBL_GRAND_TOTAL',
        'currency_format' => true,
        'related_fields' =>
        array (
            0 => 'currency_id',
            1 => 'base_rate',
        ),
        'width' => '10%',
        'sortable' => false,
    ),
    'applied_amount' =>
    array (
        'vname' => 'LBL_ALLOCATED_AMOUNT',
        'width' => '10%',
        'sortable' => false,
        'default' => true,
    ),
    'applied_hours' =>
    array (
        'vname' => 'LBL_ALLOCATED_HOURS',
        'width' => '10%',
        'sortable' => false,
        'default' => true,
    ),
    'sale_type' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_SALE_TYPE',
        'width' => '10%',
    ),
    'sale_type_date' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_SALE_TYPE_DATE',
        'width' => '10%',
        'default' => true,
    ),
    'date_entered' =>
    array (
        'type' => 'datetime',
        'studio' =>
        array (
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
    ),
    'created_by_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'readonly' => true,
        'vname' => 'LBL_CREATED',
        'id' => 'CREATED_BY',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Users',
        'target_record_key' => 'created_by',
    ),
    'paid_amount' =>
    array (
        'name' => 'paid_amount',
        'usage' => 'query_only',
        'sortable' => false,
    ),
    'currency_id' =>
    array (
        'name' => 'currency_id',
        'usage' => 'query_only',
    ),
    'deposit_amount' =>
    array (
        'name' => 'deposit_amount',
        'usage' => 'query_only',
        'sortable' => false,
    ),
    'contract_id' =>
    array (
        'name' => 'contract_id',
        'usage' => 'query_only',
    ),
    'description' =>
    array (
        'name' => 'description',
        'usage' => 'query_only',
    ),
    'old_student_id' => array (
        'name' => 'old_student_id',
        'usage' => 'query_only',
    ),
    'is_old' => array (
        'name' => 'is_old',
        'usage' => 'query_only',
    ),
);