<?php
// created: 2022-06-12 01:41:06
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '10%',
        'default' => true,
        'link' => true,
    ),
    'payment_type' =>
    array (
        'vname' => 'LBL_PAYMENT_TYPE',
        'width' => '9%',
        'default' => true,
    ),
    'payment_date' =>
    array (
        'vname' => 'LBL_PAYMENT_DATE',
        'width' => '10%',
        'default' => true,
    ),
    'payment_amount' =>
    array (
        'vname' => 'LBL_PAYMENT_AMOUNT',
        'width' => '10%',
        'sortable' => false,
        'default' => true,
    ),
    'applied_amount' =>
    array (
        'vname' => 'LBL_APPLIED_AMOUNT',
        'width' => '10%',
        'sortable' => false,
        'default' => true,
    ),
    'applied_hours' =>
    array (
        'vname' => 'LBL_APPLIED_HOURS',
        'width' => '10%',
        'sortable' => false,
        'default' => true,
    ),
    'sale_type' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_SALE_TYPE',
        'width' => '10%',
    ),
    'sale_type_date' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_SALE_TYPE_DATE',
        'width' => '10%',
        'default' => true,
    ),
    'team_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'studio' =>
        array (
            'portallistview' => false,
            'portalrecordview' => false,
        ),
        'vname' => 'LBL_TEAMS',
        'id' => 'TEAM_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Teams',
        'target_record_key' => 'team_id',
    ),
    'assigned_user_name' =>
    array (
        'link' => true,
        'type' => 'relate',
        'vname' => 'LBL_ASSIGNED_TO',
        'id' => 'ASSIGNED_USER_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Users',
        'target_record_key' => 'assigned_user_id',
    ),
    'date_entered' =>
    array (
        'type' => 'datetime',
        'studio' =>
        array (
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
    ),
    'old_student_id' => array (
        'name' => 'old_student_id',
        'usage' => 'query_only',
    ),
    'is_old' => array (
        'name' => 'is_old',
        'usage' => 'query_only',
    ),
);