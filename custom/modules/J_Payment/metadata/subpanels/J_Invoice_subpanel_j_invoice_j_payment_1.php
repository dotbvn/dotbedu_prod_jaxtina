<?php
// created: 2022-06-12 01:25:41
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => 10,
        'default' => true,
        'link' => true,
    ),
    'parent_name' =>
    array (
        'type' => 'parent',
        'studio' => 'visible',
        'vname' => 'LBL_PARENT_NAME',
        'sortable' => false,
        'link' => true,
        'ACLTag' => 'PARENT',
        'dynamic_module' => 'PARENT_TYPE',
        'id' => 'PARENT_ID',
        'related_fields' =>
        array (
            0 => 'parent_id',
            1 => 'parent_type',
        ),
        'width' => 10,
        'default' => true,
    ),
    'payment_type' =>
    array (
        'vname' => 'LBL_PAYMENT_TYPE',
        'width' => 10,
        'default' => true,
    ),
    'payment_amount' =>
    array (
        'type' => 'currency',
        'default' => true,
        'vname' => 'LBL_GRAND_TOTAL',
        'currency_format' => true,
        'related_fields' =>
        array (
            0 => 'currency_id',
            1 => 'base_rate',
        ),
        'width' => 10,
    ),
    'sum_paid' =>
    array (
        'type' => 'currency',
        'vname' => 'LBL_SUM_PAID',
        'currency_format' => true,
        'related_fields' =>
        array (
            0 => 'currency_id',
            1 => 'base_rate',
        ),
        'width' => 10,
        'default' => true,
        'sortable' => false,
    ),
    'sum_unpaid' =>
    array (
        'type' => 'currency',
        'vname' => 'LBL_SUM_UNPAID',
        'currency_format' => true,
        'related_fields' =>
        array (
            0 => 'currency_id',
            1 => 'base_rate',
        ),
        'width' => 10,
        'default' => true,
    ),
    'date_modified' =>
    array (
        'type' => 'datetime',
        'studio' =>
        array (
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname' => 'LBL_DATE_MODIFIED',
        'width' => 10,
        'default' => true,
    ),
    'assigned_user_name' =>
    array (
        'link' => true,
        'type' => 'relate',
        'vname' => 'LBL_ASSIGNED_TO',
        'id' => 'ASSIGNED_USER_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Users',
        'target_record_key' => 'assigned_user_id',
    ),
    'team_name' =>
    array (
        'width' => 10,
        'vname' => 'LBL_TEAM',
        'widget_class' => 'SubPanelDetailViewLink',
        'default' => true,
        'sortable' => false,
    ),
    'currency_id' =>
    array (
        'name' => 'currency_id',
        'usage' => 'query_only',
    ),
    'paid_amount' =>
    array (
        'name' => 'paid_amount',
        'usage' => 'query_only',
        'sortable' => false,
    ),
    'deposit_amount' =>
    array (
        'name' => 'deposit_amount',
        'usage' => 'query_only',
        'sortable' => false,
    ),
    'contract_id' =>
    array (
        'name' => 'contract_id',
        'usage' => 'query_only',
    ),
    'description' =>
    array (
        'name' => 'description',
        'usage' => 'query_only',
    ),
    'old_student_id' => array (
        'name' => 'old_student_id',
        'usage' => 'query_only',
    ),
    'is_old' => array (
        'name' => 'is_old',
        'usage' => 'query_only',
    ),
);