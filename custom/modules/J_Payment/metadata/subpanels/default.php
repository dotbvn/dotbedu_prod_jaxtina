<?php
// created: 2015-12-28 10:05:55
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '10%',
        'default' => true,
        'link' => true,
    ),
    'payment_type' =>
    array (
        'vname' => 'LBL_PAYMENT_TYPE',
        'width' => '7%',
        'default' => true,
    ),
    'payment_date' =>
    array (
        'vname' => 'LBL_PAYMENT_DATE',
        'width' => '7%',
        'default' => true,
    ),
    'payment_amount' =>
    array (
        'vname' => 'LBL_PAYMENT_AMOUNT',
        'width' => '7%',
        'sortable' => false,
        'default' => true,
    ),
    'tuition_hours' =>
    array (
        'vname' => 'LBL_TUITION_HOURS',
        'width' => '5%',
        'default' => true,
        'sortable' => false,
    ),
    'remain_amount' =>
    array (
        'vname' => 'LBL_REMAIN_AMOUNT',
        'width' => '7%',
        'sortable' => false,
        'default' => true,
        'align' => 'left',
    ),
    'remain_hours' =>
    array (
        'vname' =>'LBL_REMAIN_HOURS',
        'width' => '5%',
        'default' => true,
        'sortable' => false,
    ),
    'assigned_user_name' =>
    array (
        'width' => '10%',
        'vname' => 'LBL_ASSIGNED_TO_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'default' => true,
        'sortable' => false,
    ),
    'team_name' =>
    array (
        'width' => '10%',
        'vname' => 'LBL_TEAM',
        'widget_class' => 'SubPanelDetailViewLink',
        'default' => true,
        'sortable' => false,
    ),
    'currency_id' =>
    array (
        'name' => 'currency_id',
        'usage' => 'query_only',
    ),
    'paid_amount' =>
    array (
        'name' => 'paid_amount',
        'usage' => 'query_only',
    ),
    'deposit_amount' =>
    array (
        'name' => 'deposit_amount',
        'usage' => 'query_only',
    ),
    'contract_id' =>
    array (
        'name' => 'contract_id',
        'usage' => 'query_only',
    ),
    'description' =>
    array (
        'name' => 'description',
        'usage' => 'query_only',
    ),
);