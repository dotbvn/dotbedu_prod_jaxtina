<?php
// created: 2021-09-14 16:06:40
$searchFields['J_Payment'] = array (
  'name' =>
  array (
    'query_type' => 'default',
  ),
  'current_user_only' =>
  array (
    'query_type' => 'default',
    'db_field' =>
    array (
      0 => 'assigned_user_id',
    ),
    'my_items' => true,
    'vname' => 'LBL_CURRENT_USER_FILTER',
    'type' => 'bool',
  ),
  'assigned_user_id' =>
  array (
    'query_type' => 'default',
  ),
  'favorites_only' =>
  array (
    'query_type' => 'format',
    'operator' => 'subquery',
    'subquery' => 'SELECT dotbfavorites.record_id FROM dotbfavorites
			                    WHERE dotbfavorites.deleted=0
			                        and dotbfavorites.module = \'J_Payment\'
			                        and dotbfavorites.assigned_user_id = \'{0}\'',
    'db_field' =>
    array (
      0 => 'id',
    ),
  ),
  'range_date_entered' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_entered' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_entered' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_date_modified' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_date_modified' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_date_modified' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_old_remain_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_old_remain_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_old_remain_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_paid_amount_import' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_paid_amount_import' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_paid_amount_import' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_unpaid_amount_import' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_unpaid_amount_import' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_unpaid_amount_import' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_payment_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_payment_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_payment_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_moving_tran_out_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_moving_tran_out_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_moving_tran_out_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_moving_tran_in_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_moving_tran_in_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_moving_tran_in_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_sale_type_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_sale_type_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_sale_type_date' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_start_study' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_start_study' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_start_study' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_end_study' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_end_study' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_end_study' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_sessions' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_sessions' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_sessions' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_tuition_fee' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_tuition_fee' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_tuition_fee' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_base_rate' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_base_rate' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_base_rate' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_paid_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_paid_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_paid_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_amount_bef_discount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_amount_bef_discount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_amount_bef_discount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_discount_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_discount_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_discount_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_discount_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_discount_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_discount_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_ratio' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_ratio' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_ratio' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_total_after_discount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_total_after_discount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_total_after_discount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_deposit_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_deposit_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_deposit_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_payment_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_payment_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_payment_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_vat_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_vat_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_vat_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_tuition_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_tuition_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_tuition_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_paid_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_paid_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_paid_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_total_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_total_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_total_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_payment_expired' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'start_range_payment_expired' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'end_range_payment_expired' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
    'is_date_field' => true,
  ),
  'range_final_sponsor' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_final_sponsor' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_final_sponsor' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_final_sponsor_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_final_sponsor_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_final_sponsor_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_loyalty_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_loyalty_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_loyalty_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_loyalty_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_loyalty_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_loyalty_percent' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_remain_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_remain_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_remain_hours' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_used_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_used_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_used_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_remain_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_remain_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_remain_amount' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_installment_fee' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_installment_fee' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_installment_fee' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'range_refund_revenue' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'start_range_refund_revenue' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
  'end_range_refund_revenue' =>
  array (
    'query_type' => 'default',
    'enable_range_search' => true,
  ),
);