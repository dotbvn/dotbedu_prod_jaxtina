<?php
class logicImport{
    //Import Payment
    function importPayment(&$bean, $event, $arguments){
        global $timedate;

        if($_POST['module'] == 'Import' && !$bean->is_processing){
            if(empty($bean->payment_date)) $bean->payment_date = $timedate->nowDbDate();

            //get user ID
            if(!empty($bean->assigned_user_name)){
                $userId = $GLOBALS['db']->getOne("SELECT id FROM users WHERE full_user_name = '{$bean->assigned_user_name}' AND deleted = 0");
                if(!empty($userId)) $bean->assigned_user_id = $userId;
            }
            $bean->phone_mobile = formatPhoneNumber($bean->phone_mobile);
            if($bean->nick_name)
                $ext_1 = "OR (nick_name = '{$bean->nick_name}' AND phone_mobile = '{$bean->phone_mobile}')";

            if($bean->phone_guardian)
                $ext_2 = "OR (full_student_name = '{$bean->full_student_name}' AND phone_mobile = '{$bean->phone_guardian}')
                OR (full_student_name = '{$bean->full_student_name}' AND phone_guardian = '{$bean->phone_guardian}')
                OR (full_student_name = '{$bean->full_student_name}' AND other_mobile = '{$bean->phone_guardian}')";

            //if($bean->email) //Email sẽ dễ bị trùng dữ liệu khi nhà có 2 bé đi học
            //    $ext_3 = "OR (l2.email_address = '{$bean->email}' AND phone_mobile = '{$bean->phone_mobile}')";

            //Get Student ID
            $q1 = "SELECT contacts.id id, contacts.first_name first_name, contacts.last_name last_name, contacts.assigned_user_id assigned_user_id FROM contacts
            LEFT JOIN email_addr_bean_rel l2_1 ON contacts.id = l2_1.bean_id AND l2_1.deleted = 0 AND l2_1.bean_module = 'Contacts' AND l2_1.primary_address = 1
            LEFT JOIN email_addresses l2 ON l2.id = l2_1.email_address_id AND l2.deleted = 0 WHERE contacts.deleted = 0 AND
            ((full_student_name = '{$bean->full_student_name}'  AND phone_mobile = '{$bean->phone_mobile}')
            OR (full_student_name = '{$bean->full_student_name}' AND phone_guardian = '{$bean->phone_mobile}')
            OR (full_student_name = '{$bean->full_student_name}' AND other_mobile = '{$bean->phone_mobile}')
            $ext_1
            $ext_2
            $ext_3
            )";
            $rs = $GLOBALS['db']->query($q1);
            $row = $GLOBALS['db']->fetchByAssoc($rs);
            if(!empty($row['id']))
                $student = BeanFactory::getBean('Contacts', $row['id']);
            else{
                $student = new Contact();
                $student->first_name = split_fullname($bean->full_student_name,0);
                $student->last_name  = split_fullname($bean->full_student_name);
                $student->phone_mobile  = $bean->phone_mobile;
                $student->email1      = $bean->email;
                $student->nick_name  = $bean->nick_name;
                $student->birthdate  = $bean->birthdate;
                $student->phone_guardian  = $bean->phone_guardian;
                $student->team_id       = $bean->team_id;
                $student->team_set_id = $bean->team_id;
                $student->status = 'In Progress';
                $student->save();
            }

            if(empty($student->id)){
                $bean->deleted = 1;
                $GLOBALS['log']->fatal("[PAYMENT-Import]: Not found student ID {$bean->old_student_id}");
            }else{
                $bean->parent_name = $student->name;
                $bean->parent_id = $student->id;
                $bean->parent_type = 'Contacts';
                //Create Payment
                //if(empty($bean->payment_amount)) $bean->payment_amount = $bean->old_remain_amount;
                if(empty($bean->amount_bef_discount)) $bean->amount_bef_discount = $bean->payment_amount;
                $bean->tuition_fee = $bean->amount_bef_discount;
                if($bean->discount_amount > 0){
                    $bean->discount_percent = ($bean->discount_amount/$bean->amount_bef_discount)* 100;
                    if($bean->discount_percent > 100) $bean->discount_percent = 100;
                }

                //cHECK tEAM
                if($student->team_id != $student->team_set_id) $teams = array_column($GLOBALS['db']->fetchArray("SELECT teams.id team_id FROM teams INNER JOIN team_sets_teams tst ON tst.team_id = teams.id AND tst.deleted=0 WHERE tst.team_set_id = '{$student->team_set_id}' AND teams.deleted = 0"),'team_id');
                else $teams = array(0 => $student->team_id);
                if(!in_array($bean->team_id, $teams)){
                    $student->load_relationship("teams");
                    $student->teams->add($bean->team_id);
                }

                // Tính số giờ theo số buổi
                $class_id = $GLOBALS['db']->getOne("SELECT id FROM j_class WHERE name='{$bean->ju_class_name_text}' AND team_id='{$bean->team_id}' AND deleted=0");
                if(!empty($bean->sessions_import)
                && !empty($class_id)
                && !empty($bean->start_study)
                && empty($bean->total_hours)){
                    $sessions = round($bean->sessions_import);
                    $bean->sessions = $sessions;
                    $sss = get_list_lesson_by_class($class_id, $bean->start_study);
                    foreach($sss as $ss){
                        if($sessions > 0){
                            $bean->total_hours += $ss['delivery_hour'];
                            $sessions--;
                        }
                    }
                }else $bean->note = $bean->ju_class_name_text; //Note thông tin lớp


                if(empty($bean->tuition_hours)) $bean->tuition_hours = $bean->total_hours;

                if(empty($bean->old_remain_amount) || $bean->old_remain_amount < 0){
                    $bean->remain_amount = 0;
                }
                if(empty($bean->remain_hours) || $bean->remain_hours < 0){
                    $bean->remain_hours = $bean->total_hours;
                }

                $bean->is_old               = 0;
                $bean->team_set_id          = $bean->team_id;
                $bean->use_type             = 'Hour';
                $bean->payment_type         = 'Cashholder';
                $bean->assigned_user_id     = $student->assigned_user_id;
                $bean->payment_expired      = date('Y-m-d', strtotime("+12 months " . $bean->payment_date));
                $bean->total_after_discount  = $bean->payment_amount;
                //$bean->payment_amount       = $bean->unpaid_amount_import + $bean->paid_amount_import;
                //$bean->number_of_payment    = '1';
                $bean->date_entered        = $bean->payment_date;
                $bean->date_modified       = $bean->payment_date;
                //Set Auto-Enroll

                if(!empty($class_id)){
                    $class = BeanFactory::getBean('J_Class', $class_id);
                    $bean->is_auto_enroll       = 1;
                    $bean->enroll_hours         = $bean->total_hours; //Thiếu field này không import đc
                    $bean->ju_class_id  = $class_id;
                    if(empty($bean->start_study)) $bean->start_study    = $class->start_date;
                    $bean->end_study      = $class->end_date;
                }

                if(empty($bean->sale_type_date)) $bean->sale_type_date = $bean->payment_date;
                //add relationship
                $bean->parent_id = $student->id;
                $bean->parent_type = 'Contacts';

                //Add Relationship Course Fee
                $coursefee = $bean->j_coursefee_j_payment_1_name;
                $arrCf = $GLOBALS['db']->fetchOne("SELECT IFNULL(id,'') primaryId, IFNULL(kind_of_course,'') kind_of_course FROM j_coursefee WHERE name='$coursefee' AND deleted=0");
                if(!empty($arrCf)){
                    $bean->load_relationship('j_coursefee_j_payment_1');
                    $bean->j_coursefee_j_payment_1->add($arrCf['primaryId']);

                    $bean->load_relationship('j_coursefee_j_payment_2');
                    $bean->j_coursefee_j_payment_2->add($arrCf['primaryId']);
                    $bean->kind_of_course = $arrCf['kind_of_course'];

                }else{
                    $bean->j_coursefee_j_payment_1_name = '';
                    $bean->j_coursefee_j_payment_1j_coursefee_ida = '';
                }
                //Không tạo Unpaid/Paid đối với case Import - Tất cả số dư set vào old_remain_amount
                if(!empty($bean->payment_type_import)) {
                    $bean->name = $bean->payment_type_import."[{$bean->name}]";
                    if($bean->payment_type_import == 'Delay' || $bean->payment_type_import == 'Deposit'){
                        $bean->payment_type = $bean->payment_type_import;
                        if(empty($bean->payment_amount)) $bean->payment_amount = $bean->amount_bef_discount;
                        if(empty($bean->old_remain_amount)) $bean->paid_amount_import = $bean->payment_amount;
                    }
                }

                if($bean->paid_amount_import > 0){
                    $pmd = BeanFactory::newBean('J_PaymentDetail');
                    $pmd->payment_no            = '1';
                    $pmd->type                  = 'Normal';
                    $pmd->payment_amount        = $bean->paid_amount_import;
                    $pmd->before_discount       = $bean->paid_amount_import;
                    $pmd->payment_id            = $bean->id;
                    $pmd->payment_date          = $bean->payment_date;
                    $pmd->expired_date          = $pmd->payment_date;
                    $pmd->parent_name           = $bean->parent_name;
                    $pmd->parent_id             = $bean->parent_id;
                    $pmd->parent_type           = $bean->parent_type;
                    $pmd->assigned_user_id      = $bean->assigned_user_id;
                    $pmd->team_id               = $bean->team_id;
                    $pmd->team_set_id           = $bean->team_id;
                    $pmd->status                = "Paid";
                    $pmd->save();
                }

                //Tạo Unpaid
                if($bean->unpaid_amount_import > 0)   {
                    $pmd = BeanFactory::newBean('J_PaymentDetail');
                    $pmd->payment_no            = '2';
                    $pmd->type                  = 'Normal';
                    $pmd->payment_amount        = $bean->unpaid_amount_import;
                    $pmd->before_discount       = $bean->unpaid_amount_import;
                    $pmd->payment_id            = $bean->id;
                    $pmd->payment_date          = $bean->payment_date;
                    $pmd->expired_date          = $pmd->payment_date;
                    $pmd->parent_name           = $bean->parent_name;
                    $pmd->parent_id             = $bean->parent_id;
                    $pmd->parent_type           = $bean->parent_type;
                    $pmd->assigned_user_id      = $bean->assigned_user_id;
                    $pmd->team_id               = $bean->team_id;
                    $pmd->team_set_id           = $bean->team_id;
                    $pmd->status                = "Unpaid";
                    $pmd->save();
                }
            }
        }
    }
}

?>
