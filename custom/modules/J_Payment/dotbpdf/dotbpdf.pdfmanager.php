<?php


use Dotbcrm\Dotbcrm\Util\Uuid;

class J_PaymentDotbpdfpdfmanager extends DotbpdfPdfmanager
{

    public function preDisplay(){
        parent::preDisplay();
        // check request param
        if (empty($_REQUEST['record'])) {
            $this->Error('Payment not found!!');
            return;
        }

        // Set template
        $templateId = $_REQUEST['pdf_template_id'];
        //        $templateId = $GLOBALS['db']->getOne("SELECT id FROM pdfmanager WHERE base_module = 'J_PaymentDetail' AND deleted = 0 ORDER BY date_entered LIMIT  1");
        $template = BeanFactory::getBean('PdfManager', array('disable_row_level_security' => true));
        $template->retrieve($templateId);
        if (!empty($template)) {
            $this->ss->security_settings['PHP_TAGS'] = false;
            $this->ss->security = true;
            if (defined('DOTB_SHADOW_PATH')) {
                $this->ss->secure_dir[] = DOTB_SHADOW_PATH;
            }

            // settting page
            $this->setPageUnit('mm');
            $this->setPageFormat('A4');
            $this->SetAutoPageBreak(TRUE, 10);
            $this->SetMargins(10, 10, 10);
            $this->setPrintHeader(false);
            $this->setPrintFooter(false);
            $this->SetCreator($template->author);
            $this->SetAuthor($template->author);
            $this->SetTitle($template->title);
            $this->SetSubject($template->subject);
            $this->SetKeywords($template->keywords);

            // build template
            $this->templateLocation = $this->buildTemplateFile($template);

            $headerLogo = '';
            if (!empty($pdfTemplate->header_logo)) {
                // Create a temporary copy of the header logo
                // and append the original filename, so TCPDF can figure the extension
                $headerLogo = 'upload/' . $pdfTemplate->id . $pdfTemplate->header_logo;
                copy('upload/' . $pdfTemplate->id, $headerLogo);
            }

            if (!empty($pdfTemplate->header_logo) ||
            !empty($pdfTemplate->header_title) || !empty($pdfTemplate->header_text)) {
                $this->setHeaderData(
                    $headerLogo,
                    30,
                    $pdfTemplate->header_title,
                    $pdfTemplate->header_text
                );
                $this->setPrintHeader(true);
            }

            if (!empty($pdfTemplate->footer_text)) {
                $this->footerText = $pdfTemplate->footer_text;
            }

            $filenameParts = array();
            if (!empty($this->bean) && !empty($this->bean->name)) {
                $filenameParts[] = $this->bean->name;
            }
            $filenameParts[] = 'INVOICE';

            $cr = array(' ', "\r", "\n", "/");
            $this->pdfFilename = str_replace('Đ', 'D', str_replace($cr, '_', implode("_", $filenameParts) . ".pdf"));

            //Prepare Data
            //Payment
            $fields = PdfManagerHelper::parseBeanFields($this->bean, true);
            $payment = BeanFactory::getBean('J_Payment', $this->bean->id, array('disable_row_level_security' => true));
            $paymentFields = PdfManagerHelper::parseBeanFields($payment, true);
            $this->ss->assign('payment', $paymentFields);
            $this->ss->assign('fields', $fields);
            //Student
            $student = BeanFactory::getBean($payment->parent_type, $payment->parent_id, array('disable_row_level_security' => true));
            $studentFields = PdfManagerHelper::parseBeanFields($student, true);
            $this->ss->assign('student', $studentFields);

            //Center
            $center  = BeanFactory::getBean('Teams', $payment->team_id, array('disable_row_level_security' => true));
            $centerFields = PdfManagerHelper::parseBeanFields($center, true);
            $this->ss->assign('center', $centerFields);

            //Discount
            $q1 = "SELECT DISTINCT
            IFNULL(l1.id, '') sd_id,
            IFNULL(l2.is_chain_discount, 0) is_chain_discount,
            (CASE WHEN IFNULL(l2.name, '') <> '' THEN l2.name
            WHEN IFNULL(l3.name, '') <> '' THEN l3.name ELSE l1.name END) sd_name,
            IFNULL(l2.id, '') l2_id, IFNULL(l1.type, '') sd_type,
            l1.total_down sd_down_amount, l1.amount sd_amount,
            l1.percent sd_percent, IFNULL(j_payment.id, '') primaryid
            FROM j_payment INNER JOIN j_sponsor l1 ON j_payment.id = l1.payment_id AND l1.deleted = 0
            LEFT JOIN j_discount l2 ON l1.discount_id = l2.id AND l2.deleted = 0
            LEFT JOIN j_voucher l3 ON l1.voucher_id = l3.id AND l3.deleted = 0
            WHERE (j_payment.id = '{$payment->id}') AND j_payment.deleted = 0
            ORDER BY primaryid ASC, CASE WHEN IFNULL(l1.type, '') = 'Discount' THEN 0
            WHEN IFNULL(l1.type, '') = 'Sponsor' THEN 1 WHEN IFNULL(l1.type, '') = 'Loyalty' THEN 2
            WHEN IFNULL(l1.type, '') = 'TAPTAP Sponsor' THEN 3 ELSE 4 END ASC";
            $discount_details  = $GLOBALS['db']->fetchArray($q1);
            if(array_sum(array_column($discount_details,'is_chain_discount')) > 0) $is_chain = 1;
            foreach($discount_details as $value){
                $discount[] = array(
                    'id'          => $value['sd_id'],
                    'name'        => $value['sd_name'],
                    'type'        => $value['sd_type'],
                    'down_amount' => $value['sd_down_amount'],
                    'amount'      => $value['sd_amount'],
                    'percent'     => $value['sd_percent'],
                    'is_chain'    => $is_chain,
                );
            }
            $this->ss->assign('discount', $discount);

            //Tính Start-End Study
            if(!empty($payment->ju_class_id) && $payment->is_auto_enroll){
                require_once('custom/include/_helper/junior_revenue_utils.php');
                $sss = get_list_lesson_by_class($payment->ju_class_id);
            }
            //Course Fee
            $courseF_A      = BeanFactory::getBean('J_Coursefee', $payment->j_coursefee_j_payment_1j_coursefee_ida, array('disable_row_level_security' => true));
            $aphaCff = ['A', 'B', 'C', 'D', 'E', 'F','G', 'H', 'I'];
            $q2 = "SELECT DISTINCT IFNULL(j_coursefee.id, '') primaryid,
            IFNULL(j_coursefee.name, '') name,
            IFNULL(l3.id, '') group_unit_id,
            IFNULL(l3.name, '') group_unit_name,
            j_coursefee.type_of_course_fee type_of_course_fee,
            j_coursefee.unit_price unit_price,
            j_coursefee.duration_exp duration_exp
            FROM j_coursefee INNER JOIN j_groupunit l3 ON j_coursefee.group_unit_id = l3.id AND l3.deleted = 0
            WHERE j_coursefee.deleted = 0 AND l3.id = '{$courseF_A->group_unit_id}'
            ORDER BY group_unit_name ASC, CASE
            WHEN (j_coursefee.id = '{$courseF_A->id}') THEN 0
            WHEN duration_exp LIKE '%180%' THEN 1
            WHEN duration_exp LIKE '%90%' THEN 2
            WHEN duration_exp LIKE '%45%' THEN 3
            ELSE 11 END ASC, j_coursefee.name";
            $cfList  = $GLOBALS['db']->fetchArray($q2);
            foreach($cfList as $index_ =>$cff){
                $courseF     = BeanFactory::getBean('J_Coursefee', $cff['primaryid'], array('disable_row_level_security' => true));
                if($index_ == 0){ //Gói A
                    $json_   = json_decode(html_entity_decode($payment->coursefee_list),true)[$cff['primaryid']];
                    $duration= $json_['select'];
                    if($duration == 'custom') $duration = $json_['custom'];
                    $a_duration = $duration;
                    $indexKey = 0;
                }else{
                    $duration= json_decode(html_entity_decode($cff['duration_exp']),true)[1];
                    if($a_duration <= $duration) continue;
                    $indexKey++;
                }
                if(empty($duration)) continue;
                $fee_per_day  = round($courseF->type_of_course_fee * $courseF->unit_price);
                $total_amount = round($duration * ($courseF->type_of_course_fee * $courseF->unit_price));
                $total_amount_= $total_amount;
                $rs = 2; if($payment->deposit_amount > 0) $rs++;
                $appDis = array();
                $total_dis = 0;
                foreach($discount as $kk => $dis){
                    $dis_amount = (($dis['percent'] > 0) ? ($dis['percent']*$total_amount_/100) : $dis['amount']);
                    if($dis['is_chain']) $total_amount_ -= $dis_amount; //Set chain discount
                    $total_dis += $dis_amount;
                    $appDis[] = array(
                        'id'      => $dis['id'],
                        'name'    => $dis['name'],
                        'percent' => $dis['percent'].'%',
                        'dis_amount' => $dis_amount,
                    );
                    $rs++;
                }
                //Tính Start-End Study
                $start = $end = '';
                if(!empty($payment->ju_class_id) && $payment->is_auto_enroll){
                    $total_hours = $duration * $courseF->type_of_course_fee;
                    foreach($sss as $key => $value){
                        $total_hours -= $value['delivery_hour'];
                        if(empty($start)) $start = $value['date'];
                        if(round($total_hours,2) < 0) break;
                        $end = $value['date'];
                    }
                }

                $courseFee[] = array(
                    'rs'            => $rs,
                    'row'           => $aphaCff[$indexKey],
                    'id'            => $courseF->id,
                    'name'          => htmlspecialchars_decode($courseF->name),
                    'product_code'  => htmlspecialchars_decode($courseF->product_code),
                    'description'   => htmlspecialchars_decode($courseF->description),
                    'ext_name'      => htmlspecialchars_decode($courseF->ext_name),
                    'ext_name_1'    => htmlspecialchars_decode($courseF->ext_name_1),
                    'ext_name_2'    => htmlspecialchars_decode($courseF->ext_name_2),
                    'fee_per_day'   => $fee_per_day,
                    'start_study'   => $start,
                    'end_study'     => $end,
                    'duration'      => $duration, //Duration of this payment
                    'total_amount'  => $total_amount,
                    'hour_per_day'  => $courseF->type_of_course_fee,
                    'discount'      => $appDis,
                    'deposit'       => $payment->deposit_amount,
                    'grand_total'   => $total_amount - $total_dis + $payment->deposit_amount,
                    'unit_price'    => $courseF->unit_price,
                    'group_unit_id' => $courseF->group_unit_id,
                    'group_unit_name'=> htmlspecialchars_decode($courseF->group_unit_name),
                );
            }
            //Course Fee Group
            $this->ss->assign('courseFee', $courseFee);

        }
    }

    private function buildTemplateFile($template){
        if (!empty($template)) {

            if (!file_exists(dotb_cached('modules/PdfManager/tpls'))) {
                mkdir_recursive(dotb_cached('modules/PdfManager/tpls'));
            }
            $tpl_filename = dotb_cached('modules/PdfManager/tpls/' . $template->id . '.tpl');

            $template->body_html = str_replace(" ", "", $template->body_html);
            $template->body_html = html_entity_decode($template->body_html);

            //Tìm thẻ PageBreak
            $template->body_html = str_replace( '<!--|','',$template->body_html);
            $template->body_html = str_replace( '|-->','',$template->body_html);


            dotb_file_put_contents($tpl_filename, $template->body_html);

            return $tpl_filename;
        }
        return '';
    }
}
