<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class J_PaymentViewEdit extends ViewEdit
{
    public function display(){
        global $timedate, $current_user;
        $no_cash_list = ['Enrollment','Delay','Transfer In','Transfer Out','Moving In','Moving Out','Refund'];
        if(in_array($this->bean->payment_type, $no_cash_list) && !empty($this->bean->id)){
            $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Payment','".$this->bean->id."'));";
            echo '<script type="text/javascript">
            window.parent.DOTB.App.alert.show(\'message-id\', {
            level: \'error\',
            messages: \'You may not be authorized to edit this payment!\',
            autoClose: true});
            '.$route.'
            </script>';
            die();
        }

        if($_REQUEST['isDuplicate'] != 'true' && !empty($this->bean->id)){
            $pmdS      = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(id, '') id, IFNULL(status, '') status, IFNULL(payment_amount, 0) payment_amount FROM j_paymentdetail WHERE payment_id = '{$this->bean->id}' AND status <> 'Cancelled' AND deleted = 0");
            $countUsed = $GLOBALS['db']->getOne("SELECT count(id) count FROM j_payment_j_payment_1_c WHERE payment_idb = '{$this->bean->id}' AND deleted = 0");
            $countPAID = 0;
            foreach($pmdS as $id => $pmd) if($pmd['status'] == 'Paid' && $pmd['payment_amount'] > 0) $countPAID++;
                //Check Paid
                if(($countPAID > 0) || ($countUsed > 0)){
                $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('J_Payment','".$this->bean->id."'));";
                echo '<script type="text/javascript">
                window.parent.DOTB.App.alert.show(\'message-id\', {
                level: \'error\',
                messages: \''.str_replace(array("\r", "\n"), '', nl2br(translate('LBL_ALERT_DELETE_RECEIPT', 'J_Payment'))).'\',
                autoClose: false
                });
                '.$route.'
                </script>';
                die();
            }
        }


        //Assigned Loyalty Discount
        $this->ss->assign('enable_loyalty', $GLOBALS['dotb_config']['enable_loyalty']);
        if(empty($this->bean->id) && !empty($_GET['payment_type']))
            $this->bean->payment_type = $_GET['payment_type'];
        if(empty($this->bean->payment_type)) $this->bean->payment_type = 'Cashholder';

        //INSTALLMENT CONFIG
        $this->ss->assign('installment', 0);
        //END: INSTALLMENT CONFIG


        //In Case create
        //Assign Student from $_GET
        if(!empty($_REQUEST['student_id'])){
            $this->bean->parent_type = 'Contacts';
            $parent = BeanFactory::getBean('Contacts',$_REQUEST['student_id']);
        }elseif(!empty($_REQUEST['lead_id'])){
            $this->bean->parent_type = "Leads";
            $parent = BeanFactory::getBean('Leads', $_REQUEST['lead_id']);
        }elseif(!empty($_REQUEST['class_id'])){
            $this->bean->parent_type = "J_Class";
            $parent = BeanFactory::getBean('J_Class', $_REQUEST['class_id']);
        }
        if(empty($this->bean->parent_id)){
            $this->bean->parent_name = $parent->name;
            $this->bean->parent_id   = $parent->id;
            $this->bean->team_id     = $parent->team_id;
            if(empty($this->bean->team_id)) $this->bean->team_id = $current_user->team_id;
        }

        //lock team
        if(!in_array($this->bean->payment_type, $no_cash_list))
            $this->ss->assign('lock_team', 0);
        else $this->ss->assign('lock_team', 1);

        $this->ss->assign('limit_discount_percent', "<script>var limit_discount_percent = 100; var default_currency_iso4217 = '".$GLOBALS['dotb_config']['default_currency_iso4217']."';</script>" );
        $this->ss->assign('min_points_loyalty', "<script>var min_points = 0;</script>" );
        //Handle payment type
        $pmtl = translate('payment_type_list');
        if(!empty($_GET['primary_id'])){
          $pmtl = array('Book/Gift' => $pmtl['Book/Gift']);
          $primaryPM = BeanFactory::getBean("J_Payment", $_GET['primary_id']);
          $this->bean->is_auto_enroll = $primaryPM->is_auto_enroll;
          $this->bean->ju_class_name = $primaryPM->ju_class_name;
          $this->bean->ju_class_id = $primaryPM->ju_class_id;
          $this->bean->start_study = $primaryPM->start_study;
          $this->bean->end_study = $primaryPM->end_study;
        }
        else foreach($no_cash_list as $pmt) unset($pmtl[$pmt]);
        $this->ss->assign('payment_type_list', $pmtl);

        if(empty($this->bean->id)){
            $is_create = true;
            $this->ss->assign('discount_list', "<input type='hidden' name='discount_list' id='discount_list' value=''/>" );
            $this->ss->assign('primary_id', $_REQUEST['primary_id']);

            if(!in_array($this->bean->payment_type,$no_cash_list)){
                $book_list  = getBookList($this->bean->team_id);
                //Book Manage
                $html_tpl   = getHtmlAddRow($book_list,'','','1','','',true);
                $html_tpl   .= getHtmlAddRow($book_list,'','','1','','',false);
                //ADD Book Template
                $this->ss->assign('html_tpl',$html_tpl);
            }

            // Assign default value for payment detail - Lap Nguyen
            $this->ss->assign('PAY_DTL_TYPE_OPTIONS', $GLOBALS['app_list_strings']['payment_detail_type_options']);

            $this->ss->assign('loyalty_list', "<input type='hidden' name='loyalty_list' id='loyalty_list' value=''/>" );
            $this->ss->assign('loy_loyalty_points', "0" );
        }
        else{ //In Case edit
            // If payment have payment detail printed, can not edit
            $is_create = false;
            $dis_selected = array();
            //GET DISCOUNT SELETED
            $q5 = "SELECT DISTINCT
            IFNULL(j_discount.id, '') primaryid,
            j_discount.discount_amount discount_amount,
            IFNULL(j_discount.maximum_discount_percent, 0) maximum_discount_percent,
            j_discount.discount_percent discount_percent
            FROM j_discount
            INNER JOIN j_payment_j_discount_1_c l1_1 ON j_discount.id = l1_1.j_payment_j_discount_1j_discount_idb AND l1_1.deleted = 0
            INNER JOIN j_payment l1 ON l1.id = l1_1.j_payment_j_discount_1j_payment_ida AND l1.deleted = 0
            WHERE (((l1.id = '{$this->bean->id}'))) AND j_discount.deleted = 0";
            $rs5 = $GLOBALS['db']->query($q5);
            while($row = $GLOBALS['db']->fetchByAssoc($rs5) ){
                $dis_selected[$row['primaryid']]['id']             = $row['primaryid'];
                $dis_selected[$row['primaryid']]['dis_percent']    = format_number($row['discount_percent'],2,2);
                $dis_selected[$row['primaryid']]['dis_amount']     = format_number($row['discount_amount']);
                $dis_selected[$row['primaryid']]['maximum_percent']= format_number($row['maximum_discount_percent'],2,2);
            }
            $dis_list = json_encode($dis_selected);
            $this->ss->assign('discount_list', "<input type='hidden' name='discount_list' id='discount_list' value='$dis_list'/>" );

            $this->ss->assign('primary_id', $this->bean->j_payment_j_payment_2j_payment_ida);
            //--------------------Add Book Teamplate------////////////////
            if($this->bean->payment_type == 'Book/Gift') {
                $book_list  = getBookList($this->bean->team_id, $this->bean->id);
                $html_tpl   = getHtmlAddRow($book_list,'','','1','','',true);
                $q1 = "SELECT DISTINCT
                IFNULL(l3.id, '') book_id, IFNULL(l3.name, '') book_name,
                IFNULL(j_inventorydetail.id, '') primaryid, ABS(j_inventorydetail.quantity) quantity,
                l3.unit unit, j_inventorydetail.price price, j_inventorydetail.amount amount
                FROM j_inventorydetail
                INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id AND l1.deleted = 0
                INNER JOIN j_payment_j_inventory_1_c l2_1 ON l1.id = l2_1.j_payment_j_inventory_1j_inventory_idb AND l2_1.deleted = 0
                INNER JOIN j_payment l2 ON l2.id = l2_1.j_payment_j_inventory_1j_payment_ida AND l2.deleted = 0
                INNER JOIN product_templates l3 ON j_inventorydetail.book_id = l3.id AND l3.deleted = 0
                WHERE(l2.id = '{$this->bean->id}') AND j_inventorydetail.deleted = 0";
                $details = $GLOBALS['db']->fetchArray($q1);
                foreach($details as $detail)
                    $html_tpl .= getHtmlAddRow($book_list , $detail['book_id'], $detail['unit'], $detail['quantity'],format_number($detail['price']),format_number($detail['amount']), false);

                //ADD Book Template
                $this->ss->assign('html_tpl',$html_tpl);
            }

            // Get Spilt payment - by Lap Nguyen
            $this->ss->assign('PAY_DTL_TYPE_OPTIONS', $GLOBALS['app_list_strings']['payment_detail_type_options']);

            $sqlGetPayDetail = "SELECT DISTINCT
            IFNULL(id, '') primaryId,
            IFNULL(payment_no, '0')         pay_no,
            IFNULL(payment_amount, '0')     pay_amount,
            IFNULL(payment_method, '')      payment_method,
            IFNULL(card_type, '')           card_type,
            IFNULL(before_discount, '0')    before_discount,
            IFNULL(discount_amount, '0')    discount_amount,
            IFNULL(payment_amount, '0')     pay_amount,
            IFNULL(status, '')              pay_status,
            IFNULL(payment_date, '')        pay_date
            FROM j_paymentdetail
            WHERE payment_id = '{$this->bean->id}' AND deleted = 0 AND status <> 'Cancelled'
            ORDER BY payment_no;";
            $rsGetPayDetail = $GLOBALS['db']->query($sqlGetPayDetail);
            $payDtlStatus = array();
            while($payDetail = $GLOBALS['db']->fetchByAssoc($rsGetPayDetail)){
                $this->ss->assign('PAY_DTL_BEF_DISCOUNT_'.$payDetail['pay_no'],$payDetail['before_discount']);
                $this->ss->assign('PAY_DTL_DIS_AMOUNT_'.$payDetail['pay_no'],$payDetail['discount_amount']);
                $this->ss->assign('PAY_DTL_AMOUNT_'.$payDetail['pay_no'],$payDetail['pay_amount']);
                $this->ss->assign('PAY_DTL_TYPE_'.$payDetail['pay_no'],$payDetail['pay_type']);
                $payDtlStatus[$payDetail['pay_no']] = $payDetail['pay_status'];
            }

            $this->ss->assign('loyalty_list', "<input type='hidden' name='loyalty_list' id='loyalty_list' value=''/>" );
            $loy_loyalty_points = $GLOBALS['db']->getOne("SELECT ABS(IFNULL(point, 0)) point FROM j_loyalty WHERE payment_id = '{$this->bean->id}' AND type = 'Redemption' AND deleted = 0");
            if(empty($loy_loyalty_points)) $loy_loyalty_points = 0;
            $this->ss->assign('loy_loyalty_points', $loy_loyalty_points );
        }

        if (!empty($this->bean->j_coursefee_j_payment_1j_coursefee_ida)){
            $currentCourseFeeStatement = "OR j_coursefee.id = '{$this->bean->j_coursefee_j_payment_1j_coursefee_ida}'";
        }
        $ext_team_c = "AND j_coursefee.team_set_id IN (SELECT tst.team_set_id
        FROM team_sets_teams tst
        INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
        AND team_memberships.user_id = '".$current_user->id."' AND team_memberships.deleted = 0)";
        if($current_user->isAdmin()) $ext_team_c = '';
        $today = $timedate->nowDbDate();
        //Get Course Fee
        $q3 = "SELECT DISTINCT IFNULL(j_coursefee.id, '') primaryid,
        IFNULL(j_coursefee.name, '') j_coursefee_name, IFNULL(j_coursefee.type_of_course_fee, '') type_of_course_fee,
        IFNULL(j_coursefee.type, '') type, IFNULL(j_coursefee.duration_exp, '') duration_exp,
        IFNULL(j_coursefee.kind_of_course, '') kind_of_course, IFNULL(l3.name, '') group_unit_name,
        j_coursefee.date_entered date_entered, j_coursefee.unit_price unit_price,
        j_coursefee.is_accumulate is_accumulate, IFNULL(j_coursefee.is_custom_hours, 0) is_custom_hours,
        j_coursefee.accumulate_percent accumulate_percent, j_coursefee.inactive_date j_coursefee_inactive_date,
        j_coursefee.apply_date j_coursefee_apply_date, IFNULL(j_coursefee.payment_plan, '') payment_plan, GROUP_CONCAT(l2.id) default_dis_list,
        j_coursefee.status status
        FROM j_coursefee
        INNER JOIN teams l1 ON j_coursefee.team_id = l1.id
        LEFT JOIN j_coursefee_j_discount_1_c l2_1 ON j_coursefee.id = l2_1.j_coursefee_j_discount_1j_coursefee_ida AND l2_1.deleted = 0
        LEFT JOIN j_discount l2 ON l2.id = l2_1.j_coursefee_j_discount_1j_discount_idb AND l2.deleted = 0
        LEFT JOIN j_groupunit l3 ON j_coursefee.group_unit_id = l3.id AND l3.deleted = 0
        WHERE j_coursefee.deleted = 0 AND ((j_coursefee.status = 'Active' $ext_team_c)
        $currentCourseFeeStatement)
        AND ((j_coursefee.apply_date <= '$today' OR j_coursefee.apply_date IS NULL)
        AND (j_coursefee.inactive_date >= '$today' OR j_coursefee.inactive_date IS NULL))
        GROUP BY IFNULL(j_coursefee.id, '')
        ORDER BY group_unit_name ASC, CASE
        WHEN (duration_exp = '' OR duration_exp IS NULL) THEN 0
        WHEN duration_exp LIKE '%180%' THEN 1
        WHEN duration_exp LIKE '%90%' THEN 2
        WHEN duration_exp LIKE '%45%' THEN 3
        ELSE 11 END ASC, j_coursefee.name, CASE
        WHEN (j_coursefee.kind_of_course = ''
        OR j_coursefee.kind_of_course IS NULL) THEN 0";
        $kocc = $GLOBALS['app_list_strings']['kind_of_course_list'];
        $count_koc = 1;
        foreach($kocc as $koc => $value) $q3 .= " WHEN j_coursefee.kind_of_course LIKE '%$koc%' THEN ".$count_koc++;
        $q3 .= " ELSE $count_koc END ASC";
        $rs3 = $GLOBALS['db']->query($q3);

        $coursefee = '<select style="width: 50%; display:none;" id="coursefee" multiple class="coursefee" name="j_coursefee_j_payment_1j_coursefee_ida[]" >';
        //get selected course fee
        if($this->bean->load_relationship('j_coursefee_j_payment_2'))
            $arrCf = $this->bean->j_coursefee_j_payment_2->getBeans();

        $first_opt  = true;
        $previous   = '####';
        while($_fee = $GLOBALS['db']->fetchByAssoc($rs3)){
            if ($_fee['group_unit_name'] != $previous){
                $coursefee       .= '<optgroup label="' .$_fee['group_unit_name'] . '">';
                $previous   = $_fee['group_unit_name'];
            }
            $fee_seleted = (array_key_exists($_fee['primaryid'],$arrCf) ? 'selected' : '');
            if($_fee['type'] == 'Hours') $ext_cf = "(".format_number($_fee['unit_price'])."/".format_number($_fee['type_of_course_fee'],2,2)." ".translate('LBL_HOURS', 'J_Payment').")";
            else $ext_cf = "(".format_number($_fee['unit_price']*$_fee['type_of_course_fee'])."/".$GLOBALS['app_list_strings']['unit_coursefee_list'][$_fee['type']].")";
            $fee_seleted = (array_key_exists($_fee['primaryid'],$arrCf) ? 'selected' : '');
            $coursefee .= "<option $fee_seleted value='{$_fee['primaryid']}' payment_plan='{$_fee['payment_plan']}' type='{$_fee['type']}' unit_price='{$_fee['unit_price']}' hour='{$_fee['type_of_course_fee']}' duration_exp='".html_entity_decode($_fee['duration_exp'])."' default_dis_list='".json_encode(explode(',',$_fee['default_dis_list']))."' kind_of_course='".json_encode(unencodeMultienum($_fee['kind_of_course']))."' is_accumulate='{$_fee['is_accumulate']}' is_custom_hours='{$_fee['is_custom_hours']}' accumulate_percent='{$_fee['accumulate_percent']}' course_name='{$_fee['j_coursefee_name']}'>{$_fee['j_coursefee_name']} $ext_cf</option>";
        }
        $coursefee .= '</optgroup></select>';

        $this->ss->assign('coursefee',$coursefee);

        $discountRowsHtml = generateDiscountRows($dis_selected);
        //END : Generate
        $this->ss->assign('discount_rows', $discountRowsHtml);

        //Check Access Assigned To - Role First Assigned To
        if(ACLController::checkAccess('J_Payment', 'import', false))
            $this->ss->assign('lock_assigned_to','<input type="hidden" id="lock_assigned_to" value="0">');
        else
            $this->ss->assign('lock_assigned_to','<input type="hidden" id="lock_assigned_to" value="1">');

        //Loại bỏ discount TH Duplicate
        if( $_REQUEST['isDuplicate'] != 'true'){
            $q6 = "SELECT DISTINCT IFNULL(j_sponsor.id, '') primaryid,
            IFNULL(j_sponsor.name, '') name, IFNULL(j_sponsor.sponsor_number, '') sponsor_number,
            IFNULL(j_sponsor.foc_type, '') foc_type, IFNULL(j_sponsor.voucher_id, '') voucher_id,
            IFNULL(j_sponsor.description, '') description,
            j_sponsor.amount j_sponsor_amount,
            j_sponsor.percent j_sponsor_percent, j_sponsor.loyalty_points loyalty_points,
            IFNULL(j_sponsor.type, '') type, j_sponsor.total_down total_down
            FROM j_sponsor
            INNER JOIN j_payment l1 ON j_sponsor.payment_id = l1.id AND l1.deleted = 0
            WHERE (l1.id = '{$this->bean->id}')
            AND (j_sponsor.type = 'Sponsor'
            OR j_sponsor.type = 'TAPTAP Sponsor'
            OR (j_sponsor.type = 'Loyalty'
            AND foc_type <> '' ) ) AND j_sponsor.deleted = 0
            ORDER BY name ASC";
            $rs6 = $GLOBALS['db']->query($q6);
            $count_sp = 0;
            $spon_arr = array();
            while($spon = $GLOBALS['db']->fetchByAssoc($rs6)){
                $spon_arr[$count_sp]['type']   = $spon['type'];
                $spon_arr[$count_sp]['foc_type']         = $spon['foc_type'];
                $spon_arr[$count_sp]['sponsor_amount']   = format_number($spon['j_sponsor_amount']);
                $spon_arr[$count_sp]['loyalty_points']   = $spon['loyalty_points'];
                $spon_arr[$count_sp]['sponsor_percent']  = format_number($spon['j_sponsor_percent'],2,2);
                $spon_arr[$count_sp]['total_down']       = format_number($spon['total_down']);
                $spon_arr[$count_sp]['description']      = $spon['description'];
                $html_spon = getSponsorAddRow($spon['type'],$spon['voucher_id'], $spon['sponsor_number'], $spon['foc_type'], $spon_arr[$count_sp]['sponsor_amount'], $spon_arr[$count_sp]['sponsor_percent'], $spon_arr[$count_sp]['loyalty_points'], $spon_arr[$count_sp]['description']);
                $count_sp++;
            }
        }
        if($count_sp > 0)
            $this->ss->assign('html_spon',$html_spon);
        else
            $this->ss->assign('html_spon',getSponsorAddRow());

        $spon_list = json_encode($spon_arr);
        $this->ss->assign('sponsor_list',"<input type='hidden' name='sponsor_list' id='sponsor_list' value='$spon_list'/>");

        //Add User Closed Sale By User & Add PT/Demo By User
        if(empty($this->bean->user_closed_sale_id)){
            $this->bean->user_closed_sale_id = $current_user->id;
            $this->bean->user_closed_sale = $current_user->name;
        }
        if(empty($this->bean->assigned_user_id)){
            $this->bean->assigned_user_id = $current_user->id;
            $this->bean->assigned_user_name = $current_user->name;
        }

        if(empty($this->bean->user_pt_demo_id) && !empty($this->bean->parent_id)){
            //Get User PT
            $q3 = "SELECT IFNULL(j_ptresult.id, '') primaryid,
            j_ptresult.date_entered j_ptresult_date_entered,
            IFNULL(l2.id, '') created_by_id,
            IFNULL(l2.full_user_name, '') full_user_name
            FROM j_ptresult
            INNER JOIN contacts l1 ON j_ptresult.student_id = l1.id AND l1.deleted = 0 AND j_ptresult.parent = 'Contacts'
            INNER JOIN users l2 ON j_ptresult.created_by = l2.id AND l2.deleted = 0
            WHERE (((l1.id = '{$this->bean->parent_id}'))) AND j_ptresult.deleted = 0
            ORDER BY j_ptresult_date_entered DESC LIMIT 1";
            $rs3 = $GLOBALS['db']->query($q3);
            $row3 = $GLOBALS['db']->fetchByAssoc($rs3);
            if(!empty($row3)){
                $this->bean->user_pt_demo_id = $row3['created_by_id'];
                $this->bean->user_pt_demo = $row3['full_user_name'];
            }
            //Get User Demo
            if(empty($this->bean->user_pt_demo_id)){
                $this->bean->user_pt_demo_id  = $this->bean->user_closed_sale_id;
                $this->bean->user_pt_demo  = $this->bean->user_closed_sale;
            }
        }
        if(!empty($this->bean->ju_class_id)){
            $koc = $GLOBALS['db']->getOne("SELECT IFNULL(kind_of_course, '') kind_of_course FROM j_class WHERE id='{$this->bean->ju_class_id}' AND deleted = 0");
            $this->ss->assign('ju_class_koc_label',$GLOBALS['app_list_strings']['kind_of_course_list'][$koc] );
            $this->ss->assign('ju_class_koc', $koc );
        }
        $this->ss->assign('payment_type_text', $GLOBALS['app_list_strings']['payment_type_list'][$this->bean->payment_type]);
        $is_mtf = (int)in_array($this->bean->payment_type, ['Transfer In', 'Transfer Out', 'Moving In', 'Moving Out', 'Refund']);
        $this->ss->assign('is_mtf', $is_mtf);

        //Dirty trick to clear cache, a must for EditView:
        if( $this->bean->payment_type == 'Moving Out'){
            unset($this->ev->defs['panels']['LBL_OTHER_PAYMENT']);
            unset($this->ev->defs['panels']['LBL_BOOK_LIST']);
            unset($this->ev->defs['panels']['LBL_OTHER']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_TRANSFER']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_REFUND']);
        }elseif($this->bean->payment_type == 'Transfer Out'){
            unset($this->ev->defs['panels']['LBL_OTHER_PAYMENT']);
            unset($this->ev->defs['panels']['LBL_BOOK_LIST']);
            unset($this->ev->defs['panels']['LBL_OTHER']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_MOVING']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_REFUND']);
        }elseif($this->bean->payment_type == 'Refund'){
            unset($this->ev->defs['panels']['LBL_OTHER_PAYMENT']);
            unset($this->ev->defs['panels']['LBL_BOOK_LIST']);
            unset($this->ev->defs['panels']['LBL_OTHER']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_MOVING']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_TRANSFER']);
        }else{
            unset($this->ev->defs['panels']['LBL_PAYMENT_MOVING']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_TRANSFER']);
            unset($this->ev->defs['panels']['LBL_PAYMENT_REFUND']);
        }

        //Set origin_payment_type - Đánh dấu trang để không xoá cache
        $opt = ($is_mtf) ? $this->bean->payment_type : 'Cashholder';
        $opt_md5 = md5($opt);
        $this->ev->defs['templateMeta']['form']['hidden'][0] = str_replace('_origin_payment_type_', $opt_md5 , $this->ev->defs['templateMeta']['form']['hidden'][0]);

        $path_tpl = dotb_cached('modules/') . $this->bean->module_dir . '/EditView.tpl';
        if(file_exists($path_tpl)){
            $file_content = file_get_contents($path_tpl);
            if(strpos($file_content, $opt_md5) == false) unlink($path_tpl);
        }

        parent::display();
    }
}

// Generate Add row template
function getHtmlAddRow( $book_list = [], $book_id = '', $book_unit  = '', $book_quantity  = '', $book_price  = '', $book_amount  = '', $showing  = ''){
    if($showing)
        $display = 'style="display:none;"';
    $tpl_addrow = "<tr class='row_tpl' $display>";
    $htm_sel    = '<select name="book_id[]" class="book_id" style="width: 100%"><option value="" price="0" unit="">-none-</option>';
    $first_opt  = true;
    $previous   = '';
    foreach($book_list as $key => $value){
        if ($value['category'] != $previous){
            if(!$first_opt)
                $htm_sel .= '</optgroup>';
            else
                $first_opt = false;

            $htm_sel       .= '<optgroup label="' .$value['category'] . '">';
            $htm_sel       .= '<option style="color: green;" value="full-set">-- select full-set --</option>';
            $previous   = $value['category'];
        }
        $sel = '';
        if(!empty($book_id) && $book_id == $value["primaryid"])
            $sel = 'selected';

        //if($value["sum_stock"] > 0) // Xuất ÂM kho
        // Fix lỗi không nhận sản phẩm có tồn kho là 0
        if(!empty($value["sum_stock"]) || $value["sum_stock"] == "0")
            $ext_stor = "- " . $value["sum_stock"];
        $htm_sel .= '<option ' . $sel . ' quantity="' . $value["sum_stock"] . '" taxrate_id="' . $value["taxrate_id"] . '" taxrate_value="' . $value["taxrate_value"] . '" value="' . $value["primaryid"] . '" price="' . format_number($value['list_price']) . '" unit="' . $GLOBALS['app_list_strings']['unit_ProductTemplates_list'][$value['unit']] . '">' . $value["name"] . $ext_stor . ' left</option>';

    }
    $htm_sel .= '</optgroup></select>';
    $tpl_addrow .= '<td scope="col" align="center">'.$htm_sel.'</td>';
    $tpl_addrow .= '<td align="center"><span class="book_unit">'.$GLOBALS['app_list_strings']['unit_ProductTemplates_list'][$book_unit].'</span></td>';
    $tpl_addrow .= '<td align="center"><input type="number" class="book_quantity" name="book_quantity[]" value="'.$book_quantity.'" min="1" max="100"></td>';
    $tpl_addrow .= '<td nowrap align="center"><input class="currency input_readonly book_price" type="text" name="book_price[]" size="13" value="'.$book_price.'" style="font-weight: bold;text-align: right;" readonly></td>';
    $tpl_addrow .= '<td nowrap align="center"><input class="currency input_readonly book_amount" type="text" name="book_amount[]" size="13" value="'.$book_amount.'" style="font-weight: bold;text-align: right;" readonly></td>';
    $tpl_addrow .= "<td align='center'><button type='button' class='btn btn-danger btnRemove'>x</button></td>";
    $tpl_addrow .= '</tr>';
    return $tpl_addrow;
}

// Generate Discount Rows for "Get Discount" table - by Lap Nguyen
function generateDiscountRows($dis_selected = ''){
    global $current_user;
    $html = '';
    $countTr = 0;
    $ext_team = "AND j_discount.team_set_id IN
    (SELECT tst.team_set_id
    FROM team_sets_teams tst
    INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id
    AND team_memberships.user_id = '{$current_user->id}'
    AND team_memberships.deleted = 0)";
    if($current_user->isAdmin()){
        $ext_team = '';
    }
    $today = date('Y-m-d');

    //get Discount Other/Gift
    $ext_other_dis = '';
    if(!empty($dis_selected) && $_REQUEST['isDuplicate'] != 'true')//Loại bỏ discount TH Duplicate
        $ext_other_dis = "OR (j_discount.id IN ('".implode("','",array_keys($dis_selected))."') AND (j_discount.type IN('Gift', 'Hour', 'Other')))";

    $sqlGetDiscount = "SELECT DISTINCT IFNULL(j_discount.id, '') primaryid,
    IFNULL(j_discount.name, '') discount_name, j_discount.discount_amount discount_amount,
    j_discount.discount_percent discount_percent, IFNULL(j_discount.policy, '') policy,
    IFNULL(j_discount.category, '') category, j_discount.is_auto_set is_auto_set,
    j_discount.type type, j_discount.content content,
    j_discount.is_chain_discount is_chain_discount,
    j_discount.is_trade_discount is_trade_discount, j_discount.is_ratio is_ratio,
    IFNULL(j_discount.description, '') description, IFNULL(j_discount.maximum_discount_percent, 0) maximum_discount_percent,
    j_discount.start_date j_discount_start_date,
    GROUP_CONCAT(l3.j_discount_j_discount_1j_discount_idb) disable_list,
    GROUP_CONCAT(l4.j_coursefee_j_discount_1j_coursefee_ida) apw_coursefee,
    IFNULL(j_discount.apply_with_payment_type, 'All') apw_payment_type
    FROM j_discount
    INNER JOIN teams l2 ON j_discount.team_id = l2.id AND l2.deleted = 0
    LEFT JOIN j_discount_j_discount_1_c l3 ON j_discount.id = l3.j_discount_j_discount_1j_discount_ida AND l3.deleted = 0
    LEFT JOIN j_coursefee_j_discount_1_c l4 ON j_discount.id = l4.j_coursefee_j_discount_1j_discount_idb AND l4.deleted = 0
    WHERE (((j_discount.status = 'Active') $ext_team
    AND ((j_discount.start_date <= '$today' OR j_discount.start_date IS NULL)
    AND (j_discount.end_date >= '$today' OR j_discount.end_date IS NULL)))
    $ext_other_dis) AND j_discount.deleted = 0
    GROUP BY j_discount.id
    ORDER BY CASE WHEN (category = '' OR category IS NULL) THEN 0
    WHEN category = 'Seasonal Discount' THEN 1 WHEN category = 'Standard Discount' THEN 2
    WHEN category = 'Special Discount' THEN 3 ELSE 4 END ASC";
    $resultDiscount = $GLOBALS['db']->query($sqlGetDiscount);
    $runCat = '###';
    $catCount = 0;
    // Create tr for Discount
    while($rowDiscount = $GLOBALS['db']->fetchByAssoc($resultDiscount)){
        if($runCat != $rowDiscount['category']){
            $runCat = $rowDiscount['category'];
            $catCount++;
            if(!empty($runCat)){
                $html .= "<tr><th bgcolor='#FF0000' colspan='6'>
                <a href='javascript:void(0)' id='collapseLink$catCount' onclick='collapseDiscount($catCount);'>
                <img border='0' id='img_hide' src='themes/default/images/basic_search.gif'></a>
                <a href='javascript:void(0)' style='display:none;' id='expandLink$catCount' onclick='expandDiscount($catCount);'>
                <img border='0' id='img_show' src='themes/default/images/advanced_search.gif'></a>
                {$GLOBALS['app_list_strings']['category_discount_list'][$rowDiscount['category']]}</th></tr>";
            }
        }
        if($rowDiscount['type'] == 'Hour'){
            $discount_amount = '';
            $discount_percent = '';
            $maximum_discount_percent = format_number($rowDiscount['maximum_discount_percent'],2,2);
            $strRewardSelected = (isset($dis_selected[$rowDiscount['primaryid']])) ? 'checked' : '';
            $countTr++;
            $discountTrClass = (($countTr % 2) == 0) ? 'evenListRowS1' : 'oddListRowS1';
            $html .= '<tr style="cursor:pointer" class="discount_group'.$catCount.' '.$discountTrClass.' unlocked" id="row_'.$rowDiscount['primaryid'].'" colspan = "6">';
            $html .= "<td><input ".$strRewardSelected." type='checkbox' is_auto_set='{$rowDiscount['is_auto_set']}' is_chain_discount='{$rowDiscount['is_chain_discount']}' is_trade_discount='{$rowDiscount['is_trade_discount']}' class='dis_check' maximum_percent='$maximum_discount_percent' value='{$rowDiscount['primaryid']}'><input type='hidden' class='dis_content' value='{$rowDiscount['content']}'><input type='hidden' class='dis_type' value='Hour'>";
            $html .= "<input type='hidden' class='disable_discount_list' value='".json_encode(explode(',',$rowDiscount['disable_list']))."'>";
            $html .= "<input type='hidden' class='apw_coursefee' value='".json_encode(explode(',',$rowDiscount['apw_coursefee']))."'>";
            $html .= "<input type='hidden' class='apw_payment_type' value='{$rowDiscount['apw_payment_type']}'>";
            $html .= "<input type='hidden' class='dis_amount' value='$discount_amount'>
            <input type='hidden' class='dis_percent' value='$discount_percent'></td>";
            $html .= "<td class='dis_name'>{$rowDiscount['discount_name']}</td>";
            $html .= "<td align='center'>$discount_percent</td>";
            $html .= "<td align='center'>$discount_amount</td>";
            $strReadonly = '"';
            if($rowDiscount['is_auto_set'])  $strReadonly = ' input_readonly" readonly';
            $html .= "<td align='center'><i style='padding-right: 5px;'>Promotion Hours:</i>".'<input tabindex="0" autocomplete="off" type="text" name="dis_hours[]" class="dis_hours'.$strReadonly.' value="" size="4" maxlength="10" style="text-align: center;color: rgb(165, 42, 42);">'."</td>";
            $html .= "<td>".$rowDiscount['policy'] . nl2br($rowDiscount['description'])."</td>";
            $html .= "</tr>";
        }else{
            $discount_amount = (intval($rowDiscount['discount_amount']) == 0) ? '' : format_number($rowDiscount['discount_amount']);
            $discount_percent = (intval($rowDiscount['discount_percent']) == 0) ? '' : format_number($rowDiscount['discount_percent'],2,2);
            $maximum_discount_percent = format_number($rowDiscount['maximum_discount_percent'],2,2);
            $strRewardSelected = (isset($dis_selected[$rowDiscount['primaryid']])) ? 'checked' : '';
            $countTr++;
            //TH discount 100% - Không áp dụng Chain Discount
            if($discount_percent >= 100) $rowDiscount['is_chain_discount'] = 0;
            $discountTrClass = (($countTr % 2) == 0) ? 'evenListRowS1' : 'oddListRowS1';
            $html .= '<tr style="cursor:pointer" class="discount_group'.$catCount.' '.$discountTrClass.' unlocked" id="row_'.$rowDiscount['primaryid'].'" colspan = "6">';
            $html .= "<td><input ".$strRewardSelected." type='checkbox' is_auto_set='{$rowDiscount['is_auto_set']}' is_chain_discount='{$rowDiscount['is_chain_discount']}' is_trade_discount='{$rowDiscount['is_trade_discount']}' is_ratio='{$rowDiscount['is_ratio']}' class='dis_check' maximum_percent='$maximum_discount_percent' value='{$rowDiscount['primaryid']}'><input type='hidden' class='dis_type' value='Other'>";
            $html .= "<input type='hidden' class='disable_discount_list' value='".json_encode(explode(',',$rowDiscount['disable_list']))."'>";
            $html .= "<input type='hidden' class='disable_discount_list' value='".json_encode(explode(',',$rowDiscount['disable_list']))."'>";
            $html .= "<input type='hidden' class='apw_coursefee' value='".json_encode(explode(',',$rowDiscount['apw_coursefee']))."'>";
            $html .= "<input type='hidden' class='apw_payment_type' value='{$rowDiscount['apw_payment_type']}'>";
            $html .= "</td>";
            $html .= "<td class='dis_name'>{$rowDiscount['discount_name']}</td>";
            $html .= "<td align='center'>$discount_percent <input type='hidden' class='dis_percent' value='$discount_percent'></td>";
            $html .= "<td align='center'>$discount_amount <input type='hidden' class='dis_amount' value='$discount_amount'></td>";
            $html .= "<td>".$rowDiscount['policy']."</td>";
            $html .= "<td>".nl2br($rowDiscount['description'])."</td>";
            $html .= "</tr>";
        }
    }
    return $html;
}

// Generate Add row Sponsor
function getSponsorAddRow($type = '' ,$voucher_id = '', $sponsor_number = '', $foc_type = '', $sponsor_amount = '', $sponsor_percent = '', $loyalty_points = '',$description= '', $priority= ''){
    $tpl_addrow  = "<tr class='row_tpl_sponsor'>";
    $tpl_addrow .= '<td scope="col" align="center" nowrap>
    <input type="hidden" name="voucher_id[]" class="voucher_id" value="'.$voucher_id.'">
    <input type="hidden" name="type[]" class="type" value="'.$type.'">
    <input type="hidden" name="loyalty_points[]" class="loyalty_points" value="'.$loyalty_points.'">
    <input type="hidden" name="priority[]" class="priority" value="'.$priority.'">
    <input type="hidden" name="description[]" class="description" value="'.$description.'">';

    $tpl_addrow .= '<input size="18" name="sponsor_number[]" style="text-transform: uppercase;float: left;margin-right: 5px;" class="sponsor_number" value="'.$sponsor_number.'" type="text">
    <button style="float: left;" class="button primary check_sponsor" type="button"><i class="far fa-search" style="font-size: 12px;"></i></button></td>';
    $tpl_addrow .= '<td nowrap align="center"><input readonly class="input_readonly foc_type" type="text" name="foc_type[]" size="15" value="'.$GLOBALS['app_list_strings']['foc_type_payment_list'][$foc_type].'" style="font-weight: bold;"></td>';
    $tpl_addrow .= '<td nowrap align="center"><input readonly class="input_readonly currency sponsor_amount" type="text" name="sponsor_amount[]" size="10" value="'.$sponsor_amount.'" style="font-weight: bold;"></td>';
    $tpl_addrow .= '<td nowrap align="center"><input readonly class="input_readonly currency sponsor_percent" type="text" name="sponsor_percent[]" size="10" value="'.$sponsor_percent.'" style="font-weight: bold;"></td>';
    $tpl_addrow .= "<td align='center'><button type='button' class='btnRemoveSponsor'><img src='themes/default/images/id-ff-remove-nobg.png' alt='Remove'></button></td>";
    $tpl_addrow .= '</tr>';
    return $tpl_addrow;
}

function getBookList($team_id = '', $not_id = ''){
    $ext_not_payment = '';
    if(!empty($not_id))
        $ext_not_payment = "AND j_inventorydetail.id NOT IN (SELECT DISTINCT
        IFNULL(j_inventorydetail.id, '') primaryid
        FROM j_inventorydetail
        INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id AND l1.deleted = 0
        INNER JOIN j_payment_j_inventory_1_c l2_1 ON l1.id = l2_1.j_payment_j_inventory_1j_inventory_idb AND l2_1.deleted = 0
        INNER JOIN j_payment l2 ON l2.id = l2_1.j_payment_j_inventory_1j_payment_ida AND l2.deleted = 0
        WHERE (((l2.id = '$not_id')))
        AND j_inventorydetail.deleted = 0)";

    $q_list = "SELECT DISTINCT IFNULL(l1.id, '') team_id,
    IFNULL(l2.id, '') primaryid, IFNULL(l2.name, '') name,
    IFNULL(l2.code, '') code, IFNULL(l2.unit, '') unit,
    l2.list_price list_price, IFNULL(l4.name, '') parent,
    IFNULL(l4.id, '') parent_id, l4.list_order parent_order,
    IFNULL(l3.name, '') category, IFNULL(l3.id, '') category_id,
    IFNULL(l5.id, '') taxrate_id, IFNULL(l5.value, '') taxrate_value,
    l3.list_order list_order, SUM(IFNULL(j_inventorydetail.quantity, 0)) sum_stock
    FROM j_inventorydetail
    INNER JOIN teams l1 ON j_inventorydetail.team_id = l1.id AND l1.deleted = 0
    INNER JOIN product_templates l2 ON j_inventorydetail.book_id = l2.id AND l2.deleted = 0 AND l2.status2 = 'Active'
    INNER JOIN j_inventory l6 ON j_inventorydetail.inventory_id = l6.id AND l6.deleted= 0
    LEFT JOIN product_categories l3 ON l2.category_id = l3.id AND l3.deleted = 0
    LEFT JOIN product_categories l4 ON l3.parent_id = l4.id AND l4.deleted = 0
    LEFT JOIN taxrates l5 ON l5.id = l2.taxrate_id AND l5.deleted = 0
    WHERE ((  (l1.id = '$team_id') AND (j_inventorydetail.coursefee_id IS NULL OR j_inventorydetail.coursefee_id = '')))
    AND j_inventorydetail.deleted = 0
    $ext_not_payment
    GROUP BY l1.id, l2.id
    ORDER BY l3.name ASC, l3.id, name ASC";
    $book_list = array_flip_with_key($GLOBALS['db']->fetchArray($q_list),'primaryid');
    return $book_list;
}

function getBookListDefaultByCourse($fromCourseFeeIds = array()){
    if(empty($fromCourseFeeIds)) return array();
    $q1_book = "SELECT DISTINCT
    IFNULL(l3.id, '') primaryid, IFNULL(l3.code, '') code,
    IFNULL(l3.name, '') name,
    IFNULL(l3.unit, '') unit,
    IFNULL(ABS(l2.quantity), 0) quantity,
    IFNULL(l2.price, 0) list_price,
    IFNULL(l4.name, '') category, IFNULL(l4.id, '') category_id,
    IFNULL(l2.amount, 0) amount
    FROM j_coursefee l1
    INNER JOIN j_inventorydetail l2 ON l1.id = l2.coursefee_id AND l2.deleted = 0
    INNER JOIN product_templates l3 ON l2.book_id = l3.id AND l3.deleted = 0
    LEFT JOIN product_categories l4 ON l3.category_id = l4.id AND l4.deleted = 0
    WHERE (l1.id IN ('".implode("','",$fromCourseFeeIds)."')) AND l1.deleted = 0";
    $book_in_course  = $GLOBALS['db']->fetchArray($q1_book);
    return $book_in_course;
}
