<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
class J_PaymentViewDetail extends ViewDetail{
    function _displaySubPanels(){
        require_once ('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($this->bean, $this->module);

        if($this->bean->payment_type == 'Delay' || $this->bean->payment_type == 'Enrollment'){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['payment_loyaltys']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['payment_paymentdetails']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_discount_1']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_sponsor']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_invoice_j_payment_1']);
        }elseif($this->bean->payment_type == 'Moving In' || $this->bean->payment_type == 'Transfer In'){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['payment_paymentdetails']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_discount_1']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_payment_1']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_sponsor']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['payment_loyaltys']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_studentsituations']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_invoice_j_payment_1']);
        }elseif($this->bean->payment_type == 'Moving Out' || $this->bean->payment_type == 'Transfer Out' || $this->bean->payment_type == 'Refund'){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['payment_paymentdetails']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_discount_1']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_payment_1_right']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_sponsor']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['payment_loyaltys']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_studentsituations']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_invoice_j_payment_1']);
        }elseif($this->bean->payment_type == 'Book/Gift'){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_payment_2']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_studentsituations']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_payment_1']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_payment_1_right']);
        }elseif($this->bean->payment_type == 'Cashholder'){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_studentsituations']);
        }else{
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_sponsor']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_discount_1']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['payment_loyaltys']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_payment_1']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_studentsituations']);
        }
        if($this->bean->payment_type != 'Cashholder')
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_payment_j_payment_2']);
        echo $subpanel->display();
    }
    public function display(){
        global $locale, $current_user, $timedate;
        require_once('custom/include/_helper/junior_schedule.php');
        $this->options ['show_subpanels'] = true;

        $this->ss->assign('MOD_D', return_module_language($GLOBALS['current_language'], 'J_PaymentDetail'));

        //Show paid amount, unpaid amount - Lap Nguyen
        $sqlPayDtl = "SELECT DISTINCT
        IFNULL(id, '') primaryId,
        IFNULL(payment_no, '') payment_no,
        IFNULL(status, '') status,
        IFNULL(payment_amount, '0') payment_amount
        FROM j_paymentdetail
        WHERE payment_id = '{$this->bean->id}'
        AND deleted = 0
        AND status <> 'Cancelled'
        ORDER BY payment_no";
        $resultPayDtl = $GLOBALS['db']->query($sqlPayDtl);

        $paidAmount     = 0;
        $unpaidAmount   = 0;
        $countVAT       = 0;
        while($rowPayDtl = $GLOBALS['db']->fetchByAssoc($resultPayDtl)){
            if($rowPayDtl['status'] == "Unpaid") $unpaidAmount += $rowPayDtl['payment_amount'];
            else $paidAmount   += $rowPayDtl['payment_amount'];
            if($rowPayDtl['status'] == 'Paid') $countVAT++;
        }
        //Migrate
        if($this->bean->old_remain_amount > 0)
            $old_remain_text = ' <br><br><span class="old_remain_label">'.translate('LBL_OLD_REMAIN_AMOUNT').': </span> <b> </b><span class="simptip-position-top simptip-movable" style="color:#115CAB;" data-tooltip="'.translate('LBL_OLD_REMAIN_AMOUNT_DES').'"><b>'.format_number($this->bean->old_remain_amount).'</b> <img src="custom/images/import.png" style="width: 12px;"></span>';

        $this->ss->assign('old_remain_text',$old_remain_text);
        $this->ss->assign('PAID_AMOUNT','<span class="textbg_green" id="pmd_paid_amount">'.format_number($paidAmount).'</span>');
        $this->ss->assign('total_discount_sponsor',format_number($this->bean->loyalty_amount + $this->bean->discount_amount + $this->bean->final_sponsor));
        $this->ss->assign('UNPAID_AMOUNT','<span class="textbg_orange" id="pmd_unpaid_amount">'.format_number($unpaidAmount).'</span>');

        //        if($unpaidAmount == 0)
        if($paidAmount !== 0 || $unpaidAmount == 0 || $this->bean->paid_amount !== 0 ) // cho add hoc vien vao class khi da dong 1 phan hoc phi
            $this->ss->assign('is_paid', '1');
        else $this->ss->assign('is_paid', '0');

        $this->ss->assign('today',$timedate->nowDate());

        $this->ss->assign('payment_method', $GLOBALS['app_list_strings']['payment_method_list']);
        $this->ss->assign('card_type', $GLOBALS['app_list_strings']['card_name_list']);
        $this->ss->assign('bank_type', $GLOBALS['app_list_strings']['bank_name_list']);
        $this->ss->assign('bank_receive_list', $GLOBALS['app_list_strings']['bank_receive_list']);
        $this->ss->assign('current_user_name',$current_user->name);
        //QUick Edit
        $arr_Q = array( '_sale_type', '_sale_type_date', '_assigned_user_id');

        if( ACLController::checkAccess('J_Payment', 'import', true) || ($current_user->isAdmin())){
            $sale_typeQ .= '<img id="loading_sale_type" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>';
            $sale_typeQ .= '<div id="panel_1_sale_type"><label id="label_sale_type">'.$this->bean->sale_type.'</label>&nbsp&nbsp<a id="btnedit_sale_type" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>';
            $sale_typeQ .= '<div id="panel_2_sale_type" style="display: none;"><select id="value_sale_type">'.get_select_options($GLOBALS['app_list_strings']['sale_type_list'],$this->bean->sale_type).'</select>';
            $sale_typeQ .=  '&nbsp&nbsp<a title="Save" id="btnsave_sale_type"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a> <a title="Cancel" id="btncancel_sale_type"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a></div>';
            $sale_type_dateQ = '
            <img id="loading_sale_type_date" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>
            <div id="panel_1_sale_type_date"><label id="label_sale_type_date">'.$timedate->to_display_date($this->bean->sale_type_date,false).'</label>&nbsp&nbsp<a id="btnedit_sale_type_date" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>
            <div id="panel_2_sale_type_date" style="display: none;"><input disabled="" name="value_sale_type_date" size="10" id="value_sale_type_date" type="text" value="'.$timedate->to_display_date($this->bean->sale_type_date,false).'">
            <img border="0" src="custom/themes/default/images/jscalendar.png" alt="Sale Type Date" id="sale_type_date_trigger" align="absmiddle">
            &nbsp&nbsp<a title="Save" id="btnsave_sale_type_date"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a>
            <a title="Cancel" id="btncancel_sale_type_date"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a>
            </div>';

            $payment_expiredQ = '
            <img id="loading_payment_expired" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>
            <div id="panel_1_payment_expired"><label id="label_payment_expired">'.$timedate->to_display_date($this->bean->payment_expired,false).'</label>&nbsp&nbsp<a id="btnedit_payment_expired" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>
            <div id="panel_2_payment_expired" style="display: none;"><input disabled="" name="value_payment_expired" size="10" id="value_payment_expired" type="text" value="'.$timedate->to_display_date($this->bean->payment_expired,false).'">
            <img border="0" src="custom/themes/default/images/jscalendar.png" alt="Payment Expired" id="payment_expired_trigger" align="absmiddle">
            &nbsp&nbsp<a title="Save" id="btnsave_payment_expired"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a>
            <a title="Cancel" id="btncancel_payment_expired"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a>
            </div>';
        }else{
            $sale_typeQ         = '<label id="label_sale_type">'.$GLOBALS['app_list_strings']['sale_type_list'][$this->bean->sale_type].'</label>';
            $sale_type_dateQ    = '<label id="label_sale_type_date">'.$timedate->to_display_date($this->bean->sale_type_date,false).'</label>';
            $payment_expiredQ    = '<label id="label_payment_expired">'.$timedate->to_display_date($this->bean->payment_expired,false).'</label>';
        }

        if(ACLController::checkAccess('J_Payment', 'import', true) || ($current_user->isAdmin())){
            $q100 = "SELECT IFNULL(l3.id, '') user_id,
            CONCAT(IFNULL(l3.full_user_name, ''),' - ', IFNULL(l3.title, ''), ' - (', IFNULL(l4.name, ''), ')') name
            FROM users l3 LEFT JOIN  teams l4 ON l3.default_team=l4.id AND l4.deleted=0
            WHERE l3.deleted = 0 AND l3.status = 'Active'
            ORDER BY l4.id, l3.full_user_name";
            $user_list = $GLOBALS['db']->fetchArray($q100);
            $user_arr = array($current_user->id => $current_user->name);
            foreach($user_list as $key => $user)
                $user_arr[$user['user_id']] = $user['name'];

            $assigned_user_idQ = '<img id="loading_assigned_user_id" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>
            <div id="panel_1_assigned_user_id"><label id="label_assigned_user_id">'.$this->bean->assigned_user_name.'</label>&nbsp&nbsp<a id="btnedit_assigned_user_id" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>
            <div id="panel_2_assigned_user_id" style="display: none;"><select id="value_assigned_user_id">'.get_select_options($user_arr, $this->bean->assigned_user_id).'</select>
            &nbsp&nbsp<a title="Save" id="btnsave_assigned_user_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a> <a title="Cancel" id="btncancel_assigned_user_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a></div>';

            $user_closed_sale_idQ = '<img id="loading_user_closed_sale_id" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>
            <div id="panel_1_user_closed_sale_id"><label id="label_user_closed_sale_id">'.$this->bean->user_closed_sale.'</label>&nbsp&nbsp<a id="btnedit_user_closed_sale_id" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>
            <div id="panel_2_user_closed_sale_id" style="display: none;"><select id="value_user_closed_sale_id">'.get_select_options($user_arr, $this->bean->user_closed_sale_id).'</select>
            &nbsp&nbsp<a title="Save" id="btnsave_user_closed_sale_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a> <a title="Cancel" id="btncancel_user_closed_sale_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a></div>';

            $user_pt_demo_idQ = '<img id="loading_user_pt_demo_id" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>
            <div id="panel_1_user_pt_demo_id"><label id="label_user_pt_demo_id">'.$this->bean->user_pt_demo.'</label>&nbsp&nbsp<a id="btnedit_user_pt_demo_id" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>
            <div id="panel_2_user_pt_demo_id" style="display: none;"><select id="value_user_pt_demo_id">'.get_select_options($user_arr, $this->bean->user_pt_demo_id).'</select>
            &nbsp&nbsp<a title="Save" id="btnsave_user_pt_demo_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a> <a title="Cancel" id="btncancel_user_pt_demo_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a></div>';

        }else{
            $assigned_user_idQ    = '<label id="label_assigned_user_id">'.$this->bean->assigned_user_name.'</label>';
            $user_closed_sale_idQ = '<label id="label_user_closed_sale_id">'.$this->bean->user_closed_sale.'</label>';
            $user_pt_demo_idQ     = '<label id="label_user_pt_demo_id">'.$this->bean->user_pt_demo.'</label>';
        }

        $this->ss->assign('sale_typeQ',$sale_typeQ);
        $this->ss->assign('sale_type_dateQ',$sale_type_dateQ);
        $this->ss->assign('assigned_user_idQ',$assigned_user_idQ);
        $this->ss->assign('user_closed_sale_idQ',$user_closed_sale_idQ);
        $this->ss->assign('user_pt_demo_idQ',$user_pt_demo_idQ);
        $this->ss->assign('payment_expiredQ',$payment_expiredQ);

        $is_admin_inv = $current_user->isAdminForModule('J_Invoice');
        // $is_admin_inv = false;
        $this->ss->assign('is_admin_inv', (int)$is_admin_inv);
        //Load Payment Course Fees
        if($this->bean->payment_type == 'Cashholder'){
            $cfOjb = paymentCourseFees($this->bean);
            if(!empty($cfOjb)){
                $htmTable = "<table id='courseFee' style='width: 100%;' border='1' class='list view' Cellpadding='10'>
                <thead><tr>
                <th width='30%' style='text-align: center;'>".translate('LBL_COURSE_FEE_ID')."</th>
                <th width='10%' style='text-align: center;'>".translate('LBL_UNIT')."</th>
                <th width='10%' style='text-align: center;'>".translate('LBL_QUATITY')."</th>
                <th width='15%' style='text-align: center;'>".translate('LBL_SUB_TOTAL')."</th>
                <th width='15%' style='text-align: center;'>".translate('LBL_DISCOUNT')."</th>
                <th width='15%' style='text-align: center;'>".translate('LBL_NET_TOTAL')."</th>
                </tr></thead><tbody>";
                foreach($cfOjb as $CfID => $Cf){
                    $htmTable .= "<tr class='row_tpl'>";
                    $htmTable .= '<td style="padding: 5px;">'.$Cf['name_'].'</td>';
                    $htmTable .= '<td style="text-align: center;padding: 5px;">'.$Cf['type_'].'</td>';
                    $htmTable .= '<td style="text-align: right;padding: 5px;">'.$Cf['quantity'].'</td>';
                    $htmTable .= '<td style="text-align: right;padding: 5px;">'.$Cf['subtotal_'].'</td>';
                    $htmTable .= '<td style="text-align: right;padding: 5px;">'.$Cf['dis_amount_'].'</td>';
                    $htmTable .= '<td style="text-align: right;padding: 5px;">'.$Cf['net_amount_'].'</td>';
                    $htmTable .= '</tr>';
                    $j_coursefee .='<li style="margin-left:10px;"><a href=index.php?module=J_Coursefee&&record='.$CfID.'>'.$Cf['name_'].'</a></li>';

                }
                $htmTable .= '</tbody></table>';
            }else{
                if($this->bean->load_relationship('j_coursefee_j_payment_2')){
                    $arrCf = $this->bean->j_coursefee_j_payment_2->getBeans();
                    foreach($arrCf as $keyCf => $valueCf)
                        $j_coursefee .='<li style="margin-left:10px;"><a href=index.php?module=J_Coursefee&&record='.$keyCf.'>'.$valueCf->name.'</a></li>';
                }
            }
            //$this->ss->assign('j_coursefee',$htmTable);
            $this->ss->assign('j_coursefee',$j_coursefee);
        }
        //END
        // Dirty trick to clear cache, a must for DetailView:

        if($this->bean->payment_type == 'Enrollment'){
            unset($this->dv->defs['panels']['LBL_PLACE_HOLDER']);
            unset($this->dv->defs['panels']['LBL_DEPOSIT']);
            unset($this->dv->defs['panels']['LBL_BOOK_PLACEMENT_TEST']);
            unset($this->dv->defs['panels']['LBL_TRANSFER']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_REFUND']);
            unset($this->dv->defs['panels']['LBL_DELAY']);
            if($this->bean->paid_hours <= 0 )
                unset($this->dv->defs['panels']['LBL_ENROLLMENT'][7]);
            if($this->bean->deposit_amount <= 0 )
                unset($this->dv->defs['panels']['LBL_ENROLLMENT'][12]);
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]);

            $sql_get_class="SELECT
            DISTINCT IFNULL(l2.id,'') l2_id ,
            IFNULL(l2.name,'') l2_name ,
            IFNULL(j_payment.id,'') primaryid,
            MIN(l1.start_study) start_study,
            MAX(l1.end_study) end_study
            FROM j_payment INNER JOIN j_studentsituations l1 ON j_payment.id=l1.payment_id
            AND l1.deleted=0 INNER JOIN j_class l2 ON l1.ju_class_id=l2.id
            AND l2.deleted=0 WHERE j_payment.id='{$this->bean->id}'
            AND j_payment.deleted=0
            GROUP BY l2.id";
            $result_get_class = $GLOBALS['db']->query($sql_get_class);
            $html_class='';
            $first_set = true;
            $start_study = '';
            $end_study = '';
            while($row = $GLOBALS['db']->fetchByAssoc($result_get_class)){
                $html_class.='<li style="margin-left:10px;"><a href=index.php?module=J_Class&offset=1&stamp=1441785563066827100&return_module=J_Class&action=DetailView&record='.$row['l2_id'].'>'.$row['l2_name'].'</a></li>';
                if($first_set){
                    $start_study  = $row['start_study'];
                    $end_study    = $row['end_study'];
                    $first_set    = false;
                }else{
                    if($start_study > $row['start_study'])
                        $start_study = $row['start_study'];

                    if($end_study < $row['end_study'])
                        $end_study = $row['end_study'];
                }
            }
            if(!empty($start_study))
                $this->bean->start_study = $timedate->to_display_date($start_study,false);

            if(!empty($end_study))
                $this->bean->end_study = $timedate->to_display_date($end_study,false);

            $this->ss->assign('html_class',$html_class);

        }elseif($this->bean->payment_type == 'Cashholder'){
            unset($this->dv->defs['panels']['LBL_ENROLLMENT']);
            unset($this->dv->defs['panels']['LBL_DEPOSIT']);
            unset($this->dv->defs['panels']['LBL_BOOK_PLACEMENT_TEST']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_TRANSFER']);
            unset($this->dv->defs['panels']['LBL_REFUND']);
            unset($this->dv->defs['panels']['LBL_DELAY']);
            if($this->bean->deposit_amount <= 0 )
                unset($this->dv->defs['panels']['LBL_PLACE_HOLDER'][7]);
        }
        elseif($this->bean->payment_type == 'Book/Gift'){
            unset($this->dv->defs['panels']['LBL_ENROLLMENT']);
            unset($this->dv->defs['panels']['LBL_DEPOSIT']);
            unset($this->dv->defs['panels']['LBL_PLACE_HOLDER']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_TRANSFER']);
            unset($this->dv->defs['panels']['LBL_REFUND']);
            unset($this->dv->defs['panels']['LBL_DELAY']);

            $book_list = getHtmlAddRow($this->bean->id);
            $this->ss->assign("bookList", $book_list['html']);
            $this->ss->assign("total_book_amount", format_number($book_list['amount_bef_discount']));
            $this->ss->assign("total_book_quantity", format_number($book_list['total_quantity']));
        }elseif($this->bean->payment_type == 'Moving In' || $this->bean->payment_type == 'Moving Out'){
            unset($this->dv->defs['panels']['LBL_ENROLLMENT']);
            unset($this->dv->defs['panels']['LBL_BOOK_PLACEMENT_TEST']);
            unset($this->dv->defs['panels']['LBL_PLACE_HOLDER']);
            unset($this->dv->defs['panels']['LBL_DEPOSIT']);
            unset($this->dv->defs['panels']['LBL_TRANSFER']);
            unset($this->dv->defs['panels']['LBL_REFUND']);
            unset($this->dv->defs['panels']['LBL_DELAY']);
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]);

            if ($this->bean->payment_type == 'Moving In') {
                $this->ss->assign("PAYMENT_RELA_LABEL", "Moving Out Payment");
                $this->ss->assign("PAYMENT_RELA", '<a href="#bwc/index.php?module=J_Payment&action=DetailView&record='.$this->bean->payment_out_id.'">'.$this->bean->payment_out_name."</a>");
            }
            else{
                $this->ss->assign("PAYMENT_RELA_LABEL", "Moving In Payment");
                $this->bean->load_relationship("ju_payment_in");
                $moving_in_payment = reset($this->bean->ju_payment_in->getBeans());
                $this->ss->assign("PAYMENT_RELA", '<a href="#bwc/index.php?module=J_Payment&action=DetailView&record='.$moving_in_payment->id.'">'.$moving_in_payment->name."</a>");
            }
        }elseif($this->bean->payment_type == 'Transfer Out' || $this->bean->payment_type == 'Transfer In'){
            unset($this->dv->defs['panels']['LBL_ENROLLMENT']);
            unset($this->dv->defs['panels']['LBL_BOOK_PLACEMENT_TEST']);
            unset($this->dv->defs['panels']['LBL_PLACE_HOLDER']);
            unset($this->dv->defs['panels']['LBL_DEPOSIT']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_REFUND']);
            unset($this->dv->defs['panels']['LBL_DELAY']);
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]);

            if ($this->bean->payment_type == 'Transfer In') {
                $this->ss->assign("PAYMENT_RELA_LABEL", "Transfer Out Payment");
                $this->ss->assign("STUDENT_RELA_LABEL", "Transfer From Student");
                $this->ss->assign("PAYMENT_RELA", '<a href="#bwc/index.php?module=J_Payment&action=DetailView&record='.$this->bean->payment_out_id.'">'.$this->bean->payment_out_name."</a>");
                $transfer_out_payment = BeanFactory::getBean("J_Payment", $this->bean->payment_out_id);
                $transfer_from_student = BeanFactory::getBean("Contacts", $transfer_out_payment->parent_id);
                $this->ss->assign("STUDENT_RELA", '<a href="#bwc/index.php?module=Contacts&action=DetailView&record='.$transfer_from_student->id.'">'.$transfer_from_student->name."</a>");
            }
            else{
                $this->ss->assign("PAYMENT_RELA_LABEL", "Transfer In Payment:");
                $this->ss->assign("STUDENT_RELA_LABEL", "Transfer To Student:");
                $this->bean->load_relationship("ju_payment_in");
                $transfer_in_payment = reset($this->bean->ju_payment_in->getBeans());
                $this->ss->assign("PAYMENT_RELA", '<a href="#bwc/index.php?module=J_Payment&action=DetailView&record='.$transfer_in_payment->id.'">'.$transfer_in_payment->name."</a>");
                $transfer_to_student = BeanFactory::getBean("Contacts", $transfer_in_payment->parent_id);
                $this->ss->assign("STUDENT_RELA", '<a href="#bwc/index.php?module=Contacts&action=DetailView&record='.$transfer_to_student->id.'">'.$transfer_to_student->name."</a>");
            }
        }elseif($this->bean->payment_type == 'Refund'){
            unset($this->dv->defs['panels']['LBL_ENROLLMENT']);
            unset($this->dv->defs['panels']['LBL_BOOK_PLACEMENT_TEST']);
            unset($this->dv->defs['panels']['LBL_PLACE_HOLDER']);
            unset($this->dv->defs['panels']['LBL_DEPOSIT']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_TRANSFER']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_DELAY']);
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]);

        }elseif($this->bean->payment_type == 'Delay' ){
            unset($this->dv->defs['panels']['LBL_ENROLLMENT']);
            unset($this->dv->defs['panels']['LBL_BOOK_PLACEMENT_TEST']);
            unset($this->dv->defs['panels']['LBL_PLACE_HOLDER']);
            unset($this->dv->defs['panels']['LBL_DEPOSIT']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_TRANSFER']);
            unset($this->dv->defs['panels']['LBL_REFUND']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['templateMeta']['form']['buttons'][0]);
        }else{
            unset($this->dv->defs['panels']['LBL_ENROLLMENT']);
            unset($this->dv->defs['panels']['LBL_BOOK_PLACEMENT_TEST']);
            unset($this->dv->defs['panels']['LBL_PLACE_HOLDER']);
            unset($this->dv->defs['panels']['LBL_MOVING']);
            unset($this->dv->defs['panels']['LBL_TRANSFER']);
            unset($this->dv->defs['panels']['LBL_REFUND']);
            unset($this->dv->defs['panels']['LBL_DELAY']);
        }

        //Xử lý button Delete Phân quyền xóa Unpaid
        $arr_Undel = array('Delay', 'Transfer In', 'Transfer Out', 'Moving In', 'Moving Out', 'Refund', 'Enrollment');
        $countUsed = $GLOBALS['db']->getOne("SELECT count(id) count FROM j_payment_j_payment_1_c WHERE payment_idb = '{$this->bean->id}' AND deleted = 0");
        if( $current_user->isAdmin()
            || $current_user->isAdminForModule('J_Payment')
            || ((!in_array($this->bean->payment_type, $arr_Undel))
                && ($countUsed == 0)
                && (ACLController::checkAccess('J_Payment', 'delete', true))) )
            $custom_delete = '<input title="'.$GLOBALS['mod_strings']['LBL_BTN_DELETE'].'" accesskey="d" class="button" onclick="var _form = document.getElementById(\'formDetailView\'); _form.return_module.value=\'J_Payment\'; _form.return_action.value=\'ListView\'; _form.action.value=\'Delete\'; if(confirm(\'Are you sure you want to delete this payment?\')) DOTB.ajaxUI.submitForm(_form);" type="submit" name="Delete" value="'.$GLOBALS['mod_strings']['LBL_BTN_DELETE'].'" id="delete_button">';
        else $custom_delete = '';

        $this->ss->assign('CUSTOM_DELETE',$custom_delete);
        //Create button duplicate
        if($this->bean->payment_type == 'Cashholder' || $this->bean->payment_type == 'Deposit' || $this->bean->payment_type == 'Book/Gift') {
            $btn_copy = '<input title="'.$GLOBALS['mod_strings']['LBL_BTN_COPY'].'" class="button" onclick="var _form = document.getElementById(\'formDetailView\'); _form.return_module.value=\'J_Payment\'; _form.return_action.value=\'DetailView\'; _form.isDuplicate.value=true; _form.action.value=\'EditView\'; _form.return_id.value=\'' . $this->bean->id . '\';DOTB.ajaxUI.submitForm(_form);" " type="button" name="Copy" value="'.$GLOBALS['mod_strings']['LBL_BTN_COPY'].'" id="copy_button">';
            $this->ss->assign("BUTTON_COPY", $btn_copy);
        }

        //Create list Button
        switch ($this->bean->payment_type) {
            case "Cashholder":
            case "Deposit":
            case "Moving In":
            case "Transfer In":
            case "Delay":
                if($this->bean->remain_amount > 0 && DotbACL::checkField('J_Payment', 'use_type', 'edit', array("owner_override" => true))){//Hiện tại chỉ có Status = Closed là Dropped Revenue
                    $bt_admin_li .= '<li><a id="btn_delay_payment" href="#" >'.$GLOBALS['mod_strings']['LBL_DROP_PAYMENT'].'</a></li>';
                    $bt_admin_li .= '<li><a id="convert_payment" href="#" >'.$GLOBALS['mod_strings']['LBL_CONVERT_PAYMENT'].'</a></li>';
                }
                if($this->bean->use_type == 'Hour'){
                    if( !$this->bean->is_auto_enroll)
                        $btnEnroll = '<input class="button primary" id="btn_auto_enroll" type="button" value="'.$GLOBALS['mod_strings']['LBL_IS_AUTO_ENROLL'].'">';
                    if($this->bean->is_auto_enroll)
                        $btnEnroll = '<input class="button" id="btn_deactive_auto_enroll" type="button" value="'.$GLOBALS['mod_strings']['LBL_BTN_UNSET_AUTO_ENROLL'].'">';

                }

                // Recalculate remain
                $bt_admin_li .= '<li><a id="recal_remain" href="#" >'.$GLOBALS['mod_strings']['LBL_BTN_RECAL_REMAIN'].'</a></li>';
                break;
        }
        if(!empty($bt_admin_li))
            $bt_admin_ul='<ul class="clickMenu fancymenu" style="margin-left: 5px !important;"><li class="dotb_action_button"><a>'.$GLOBALS['mod_strings']['LBL_ADMIN_ACTION'].'</a><ul class="subnav" style="display: none;">'.$bt_admin_li.'</ul><span class="ab subhover"></span></li></ul> ';
        // Delete btn created
        if(in_array($this->bean->payment_type , ['Moving Out','Transfer Out','Moving In','Transfer In','Refund']) && $current_user->isAdminForModule('J_Payment'))
            $btnUndo = '<input class="button btn-danger" id="btn_undo" type="button" value="'.$GLOBALS['mod_strings']['LBL_UNDO'].' '.explode(' ',$this->bean->payment_type)[0].'"> ';

        if(in_array($this->bean->payment_type , ['Cashholder']))
            $btnCreateBookgift = '<input type="button" id="book_gift" value="'.translate('LBL_CREATE_BOOK_GIFT').'" onclick="created_book_gift($(this));"/>';
        $btn_customize = $btnUndo.$bt_admin_ul.$btnCreateBookgift;

        if($this->bean->parent_type == 'Leads') $btn_customize = '';
        $this->ss->assign('CUSTOM_BUTTON',$btn_customize);
        $this->ss->assign('BTN_ENROLL',$btnEnroll);

        //Show button export form
        $exportPaymentTypes = array("Delay","Moving Out","Moving In","Transfer Out","Transfer In", "Refund");
        $btnExportForm = "";
        if(in_array($this->bean->payment_type,$exportPaymentTypes)){
            $btnExportForm .= '<input class="button" type="button" value="'.$GLOBALS['mod_strings']['BTN_EXPORT_FORM'].'" id="btn_export_form" onclick="location.href=\'index.php?module=J_Payment&action=exportform&record='. $this->bean->id .'\'"> ';
        }

        //Form phiếu chi - Refund
        if($this->bean->payment_type == "Refund"){
            $btnExportForm .= '<input style="margin-left: 5px !important;" class="button" type="button" value="'.$GLOBALS['mod_strings']['BTN_EXPORT_EXPENSE_REFUND'].'" id="btn_export_form" onclick="location.href=\'index.php?module=J_Payment&module_type=J_Payment&action=centerExpenses&record='. $this->bean->id .'\'"> ';
        }
        $this->ss->assign('EXPORT_FROM_BUTTON',$btnExportForm);

        //Show link doanh thu Drop
        $revenue_link = '';
        $de_res     = $GLOBALS['db']->query("SELECT id, date_input, amount FROM c_deliveryrevenue WHERE ju_payment_id='{$this->bean->id}' AND deleted = 0 AND passed = 0");
        $amount_rev = 0;
        while($row_des = $GLOBALS['db']->fetchByAssoc($de_res)){
            $amount_rev += $row_des['amount'];
            $delivery_id= $row_des['id'];
            $date_rev   = $row_des['date_input'];
        }
        if(!empty($delivery_id)){
            if($current_user->isAdminForModule('J_Payment'))
                $revenue_link =  ' <a  href="#bwc/index.php?action=DetailView&module=C_DeliveryRevenue&record='.$delivery_id.'"> >>Drop revenue tháng '.date('m-Y',strtotime($date_rev)).' - '.format_number($amount_rev).'<<</a>';
            else
                $revenue_link = " Drop doanh thu tháng ".date('m-Y',strtotime($date_rev)).' - '.format_number($amount_rev);
        }

        $this->ss->assign("revenue_link",$revenue_link );

        //Convert Payment Type
        if($this->bean->use_type == 'Amount')
            $convertType = 'To Hour';
        elseif($this->bean->use_type == 'Hour')
            $convertType = 'To Amount';
        $cp_payment_amount = $this->bean->payment_amount + $this->bean->deposit_amount + $this->bean->paid_amount;
        $this->ss->assign('convertType', $convertType );
        $this->ss->assign('cp_payment_amount', $cp_payment_amount );
        $this->ss->assign('convertTypeList', $GLOBALS['app_list_strings']['convert_type_list']);

        if(!DotbACL::checkField('J_Payment', 'payment_date', 'edit', array("owner_override" => true))
            || !DotbACL::checkField('J_PaymentDetail', 'payment_date', 'edit', array("owner_override" => true)))
            $this->ss->assign('lock_receipt_date', 1);
        else $this->ss->assign('lock_receipt_date', 0);
        //Tính lại Paid Hours cho đúng nhất
        if($this->bean->payment_type == 'Enrollment') $this->bean->paid_hours = $GLOBALS['db']->getOne("SELECT ROUND(SUM(total_minute)/60,6) total_hours FROM j_studentsituations WHERE payment_id = '{$this->bean->id}' AND type = 'Enrolled' AND deleted = 0 GROUP BY payment_id");
        $this->bean->cost_per_hour = format_number($cp_payment_amount / round(round(($this->bean->paid_hours + $this->bean->total_hours)*60)/60,9)) ;

        if (DotbACL::checkField('J_Payment', 'sum_paid', 'edit', array("owner_override" => true)))
            $this->ss->assign('can_edit_pmd_amount', 1);
        else $this->ss->assign('can_edit_pmd_amount', 0);

        $is_mtf = (int)in_array($this->bean->payment_type, ['Transfer In', 'Transfer Out', 'Moving In', 'Moving Out', 'Refund']);
        $this->ss->assign('is_mtf', $is_mtf);

        //Set origin_payment_type - Đánh dấu trang để không xoá cache
        $opt_md5 = md5($this->bean->payment_type);
        $this->dv->defs['templateMeta']['form']['hidden'][0] = str_replace('_origin_payment_type_', $opt_md5 , $this->dv->defs['templateMeta']['form']['hidden'][0]);

        $path_tpl = dotb_cached('modules/') . $this->bean->module_dir . '/DetailView.tpl';
        if(file_exists($path_tpl)){
            $file_content = file_get_contents($path_tpl);
            if(strpos($file_content, $opt_md5) == false) unlink($path_tpl);
        }


        parent::display();
    }
}
function getHtmlAddRow($payment_id){
    $q1 = "SELECT DISTINCT
    IFNULL(l3.id, '') book_id,
    IFNULL(l3.name, '') book_name,
    IFNULL(j_inventorydetail.id, '') primaryid,
    ABS(j_inventorydetail.quantity) quantity,
    l3.unit unit,
    j_inventorydetail.price price,
    j_inventorydetail.amount amount,
    IFNULL(l1.id, '') l1_id,
    l1.amount_bef_discount amount_bef_discount,
    ABS(l1.total_quantity) total_quantity
    FROM
    j_inventorydetail
    INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id AND l1.deleted = 0
    INNER JOIN j_payment_j_inventory_1_c l2_1 ON l1.id = l2_1.j_payment_j_inventory_1j_inventory_idb AND l2_1.deleted = 0
    INNER JOIN j_payment l2 ON l2.id = l2_1.j_payment_j_inventory_1j_payment_ida AND l2.deleted = 0
    INNER JOIN product_templates l3 ON j_inventorydetail.book_id = l3.id  AND l3.deleted = 0
    WHERE
    (((l2.id = '$payment_id')))
    AND j_inventorydetail.deleted = 0";
    $rs1 = $GLOBALS['db']->query($q1);
    $tpl_addrow = '';
    while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
        $tpl_addrow .= "<tr class='row_tpl'>";
        $tpl_addrow .= '<td style="text-align: center;">'.$row['book_name'].'</td>';
        $tpl_addrow .= '<td style="text-align: center;">'.$GLOBALS['app_list_strings']['unit_ProductTemplates_list'][$row['unit']].'</td>';
        $tpl_addrow .= '<td style="text-align: center;">'.$row['quantity'].'</td>';
        $tpl_addrow .= '<td nowrap style="text-align: center;">'.format_number($row['price']).'</td>';
        $tpl_addrow .= '<td nowrap style="text-align: center;">'.format_number($row['amount']).'</td>';
        $tpl_addrow .= '</tr>';
        $amountBefDiscount = $row['amount_bef_discount'];
        $total_quantity = $row['total_quantity'];
    }

    return array(
        'html' => $tpl_addrow,
        'amount_bef_discount' => $amountBefDiscount,
        'total_quantity' => $total_quantity,
    );;
}