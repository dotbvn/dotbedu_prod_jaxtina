<?php
require_once 'custom/include/ViettelEInvoice/ViettelEInvoice.php';

class HelperPayment
{
    function generateContent($r, $generate = true)
    {
        $today = date('Y-m-d');
        if (!empty($r['expired_date'])) $expiredDate = $r['expired_date'];

        if ($generate) if ($today < $r['expired_date']) $expiredDate = $today;

        //$monthT = ' - tháng '.date('m/Y',strtotime($expiredDate)).'.';
        //if(empty($expiredDate)) $monthT = ' - lần '.$r['payment_no'].'.';

        switch ($r['payment_type']) {
            case "Enrollment":
            case "Cashholder":
                $content = "Thu tiền khóa học " . decodeMultienum($r['kind_of_course']);
                break;
            case "Deposit":
                $content = "Thu tiền đặt cọc khóa học " . decodeMultienum($r['kind_of_course']);
                break;
            case "Placement Test":
                $content = "Thu tiền kiểm tra trình độ.";
                break;
            case "Delay Fee":
                $content = "Thu phí bảo lưu khóa học tiếng anh.";
                break;
            case "Transfer Fee":
                $content = "Thu phí chuyển nhượng khoá học.";
                break;
            case "Book/Gift":
                $q1 = "SELECT DISTINCT
                IFNULL(l3.id, '') book_id, IFNULL(l3.name, '') book_name,
                IFNULL(j_inventorydetail.id, '') primaryid, ABS(j_inventorydetail.quantity) quantity,
                l3.unit unit, j_inventorydetail.price price,
                j_inventorydetail.amount amount, IFNULL(l1.id, '') l1_id,
                l1.total_amount total_amount, ABS(l1.total_quantity) total_quantity
                FROM j_inventorydetail
                INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id AND l1.deleted = 0
                INNER JOIN j_payment_j_inventory_1_c l2_1 ON l1.id = l2_1.j_payment_j_inventory_1j_inventory_idb AND l2_1.deleted = 0
                INNER JOIN j_payment l2 ON l2.id = l2_1.j_payment_j_inventory_1j_payment_ida AND l2.deleted = 0
                INNER JOIN product_templates l3 ON j_inventorydetail.book_id = l3.id AND l3.deleted = 0
                WHERE (l2.id = '{$r['payment_id']}') AND j_inventorydetail.deleted = 0";
                $rs1 = $GLOBALS['db']->query($q1);
                $content = "";
                while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) $content .= $row['book_name'] . " ({$row['quantity']}). ";
                break;
        }
        return $content;
    }

    //E-Invoice
    function get_EvatToken($evat){
        $y = curl_init();
        curl_setopt_array($y, array(
            CURLOPT_URL => $evat->host . '/auth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode(array(
                'appid' => $evat->accpass,
                'taxcode' => $evat->account,
                'username' => $evat->user_name,
                'password' => $evat->password
            )),
            CURLOPT_HTTPHEADER => array(
                'CompanyTaxcode: '.$evat->account,
                'Content-Type: application/json'
            ),
        ));

        $err = '';
        $result = curl_exec($y);
        if (curl_errno($y))
            $err = 'Error: ' . curl_error($y);

        curl_close($y);
        $result = json_decode($result, true);
        if (!$result['Success'] || empty($result) || !empty($result['ErrorCode']))
            return array(
                'success' => 0,
                'errorCode' => $result['ErrorCode'],
            );
        else
            return array(
                'success' => $result['Success'],
                'token' => trim($result['Data']),
                'errorCode' => $result['ErrorCode'],
                'errorCodeDetail' => $result['ErrorCodeDetail'],
                'errorMes' => $err,
            );
    }

    function get_IptemplatePublish($evat, $token)
    {
        //Lấy danh sách các hóa đơn đã phát hành
        $y = curl_init();

        curl_setopt_array($y, array(
            CURLOPT_URL => $evat->host . '/code/itg/InvoicePublishing/templates?invyear=' . date('Y'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer  ' . $token,
                'Content-Type: application/json'
            ),
        ));

        $err = '';
        $result = curl_exec($y);
        if (curl_errno($y)) {
            $err = 'Error:' . curl_error($y);
        }
        curl_close($y);
        $result = json_decode($result, true);

        if (!$result['Success'] || empty($result))
            return array(
                'success' => 0,
                'errorCode' => $result['ErrorCode'],
                'errorMes' => $err,
            );

        $templates = json_decode($result['Data'], true);
        foreach ($templates as $tmp) {
            if ($tmp['InvTemplateNo'] == $evat->pattern && $tmp['InvSeries'] == $evat->serial_no) {
                return array(
                    'success' => 1,
                    'InvTemplateNo' => $tmp['InvTemplateNo'],
                    'InvSeries' => $tmp['InvSeries'],
                    'InvoiceType' => $tmp['InvoiceType'],
                );
            }
        }
        //Xu ly loi khong tim thay
        return array(
            'success' => 0,
            'errorCode' => 'InvoiceTemplateNotFound',
            'errorMes' => $err,
        );
    }

    function PublishInvoiceHSM($evat, $param, $token, $resPublish)
    {
        //Phat hanh hoa don
        $ph = curl_init();
        $param = json_decode($param, true);
        $invoice = $param['InvoiceDataList'];
        //Change param BODY theo hướng dẫn: https://doc.meinvoice.vn/api/Document/InvoicePublishHSM.html
        $invoice['InvoiceType'] = $resPublish['InvoiceType'];
        $invoice['TemplateCode'] = $resPublish['InvTemplateNo'];
        $invoice['InvoiceSeries'] = $resPublish['InvSeries'];
        $data[] = array(
            'RefID' => $invoice['RefID'],
            'OriginalInvoiceData' => $invoice,
            'IsSendEmail' => $invoice['IsSendEmail'],
            'ReceiverName' => $invoice['ReceiverName'],
            'ReceiverEmail' => $invoice['ReceiverEmail']
        );

        curl_setopt_array($ph, array(
            CURLOPT_URL => $evat->host . '/code/itg/invoicepublishing/publishhsm',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $token,
                'CompanyTaxcode: ' . $evat->account,
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($ph);
        curl_close($ph);
        $result = json_decode($response, true);
        if (!$result['Success'] || empty($result) || !empty($result['ErrorCode']))
            return array(
                'success' => 0,
                'errorCode' => $result['ErrorCode'],
                'errors' => $result['Errors'],
                'data_request' => json_encode($data),
                'Content' => $result['Data'],
            );


        $res = json_decode($result['Data'], true);
        //Xử lý TH hóa đơn lỗi Ký: SignSoftDream78Exception, CallSignServiceFail ....
        foreach ($res as $val1) {
            if(!empty($val1['ErrorCode']))
                return array(
                    'success'       => 0,
                    'errorCode'     => $val1['ErrorCode'],
                    'data_request'  => json_encode($data),
                    'Content' => $result['Data'],
                );
        }
        foreach ($res as $val) {
            return array(
                'success'   => 1,
                'RefID'     => $val['RefID'],
                'TransactionID' => $val['TransactionID'],
                'InvoiceNumber' => $val['InvNo'],
                'Content' => $result['Data'],
                'data_request' => json_encode($data),
            );
        }
    }

    function DeleteInvoice($evat, $param, $token)
    {
        $x = curl_init();
        curl_setopt($x, CURLOPT_URL, "{$evat->host}/code/invoiceprocessing/cancelvouchers");
        $param = json_decode($param, true);
        $param['CancelReason'] = $param['DeletedReason'];

        $h = array();
        $h[] = 'Content-Type: application/json';
        $h[] = 'Authorization: Bearer ' . $token;
        $h[] = 'CompanyTaxcode: ' . $evat->account;
        curl_setopt($x, CURLOPT_HTTPHEADER, $h);

        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_POSTFIELDS, json_encode(array($param)));
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($x);
        curl_close($x);

        $result = json_decode($response, true);
        if (!$result['Success'] || empty($result) || !empty($result['ErrorCode']))
            return array(
                'success' => 0,
                'errorCode' => $result['ErrorCode'],
            );

        return array(
            'success' => $result['Success'],
            'data' => $result['Data'],
        );
    }

    function DownloadInvoice($evat, $param, $token)
    {
        $del = curl_init();
        curl_setopt($del, CURLOPT_URL, "{$evat->host}/code/itg/invoicepublished/downloadinvoice?downloadDataType=pdf");

        $h = array();
        $h[] = 'Content-Type: application/json';
        $h[] = 'Authorization: Bearer ' . $token;
        $h[] = 'CompanyTaxcode: ' . $evat->account;
        curl_setopt($del, CURLOPT_HTTPHEADER, $h);

        curl_setopt($del, CURLOPT_POST, true);
        curl_setopt($del, CURLOPT_POSTFIELDS, $param);
        curl_setopt($del, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($del, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($del);
        curl_close($del);

        $result = json_decode($response, true);

        if (!$result['Success'] || empty($result) || !empty($result['ErrorCode']))
            return array(
                'success' => 0,
                'errorCode' => $result['ErrorCode'],
            );

        $data = json_decode($result['Data'], true);
        foreach ($data as $val) {
            return array(
                'success' => 1,
                'transactionID' => $val['TransactionID'],
                'data' => $val['Data'],
            );
        }
    }

    // E-invoice Bkav
    function CreateEinvoice($evat, $param)
    { // Tao hoa don
        $ph = curl_init();

        curl_setopt($ph, CURLOPT_URL, "{$evat->host}/ExecCommand");

        $h = array();
        $h[] = 'Content-Type: application/json';
        curl_setopt($ph, CURLOPT_HTTPHEADER, $h);

        curl_setopt($ph, CURLOPT_POST, true);
        curl_setopt($ph, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ph, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ph, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ph);
        curl_close($ph);

        $result = json_decode($response, true);
        $result_decode = json_decode(base64_decode($result['d']), true);
        $content = $result_decode;
        if (empty($result_decode)) {
            return array(
                'Status' => 1,
                'isError' => 'API Error',
            );
        } elseif ($result_decode['Status'] != 0) {
            return array(
                'Status' => $result_decode['Status'],
                'isError' => $result_decode['Object'],
            );
        }
        $data = json_decode($result_decode['Object'], true);
        if ($data[0]['Status'] != 0) {
            return array(
                'Status' => $data[0]['Status'],
                'isError' => $data[0]['MessLog'],
            );
        }
        foreach ($data as $val) {
            return array(
                'Status' => $val['Status'],
                'MessLog' => $val['MessLog'],
                'PartnerInvoiceID' => $val['PartnerInvoiceID'],
                'PartnerInvoiceStringID' => $val['PartnerInvoiceStringID'],
                'InvoiceGUID' => $val['InvoiceGUID'],
                'InvoiceForm' => $val['InvoiceForm'],
                'InvoiceSerial' => $val['InvoiceSerial'],
                'InvoiceNo' => $val['InvoiceNo'],
                'MTC' => $val['MTC'],
                'Content' => json_encode($content),
            );
        }
    }

    function GeneralEinvoice($evat, $param)
    { //cau truc gui api cho bkav thuong dung
        $ph = curl_init();

        curl_setopt($ph, CURLOPT_URL, "{$evat->host}/ExecCommand");

        $h = array();
        $h[] = 'Content-Type: application/json';
        curl_setopt($ph, CURLOPT_HTTPHEADER, $h);

        curl_setopt($ph, CURLOPT_POST, true);
        curl_setopt($ph, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ph, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ph, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ph);
        curl_close($ph);

        $result = json_decode($response, true);
        $result_decode = json_decode(base64_decode($result['d']), true);
        if ($result_decode['Status'] != 0)
            return array(
                'Status' => $result_decode['Status'],
                'isError' => $result_decode['isError'],
            );
        else return array(
            'Status' => $result_decode['Status'],
            'Content' => $result_decode['Object'],
            );
    }

    function getTotalAmountPmdt($payment, $payExtId = array())
    {
        //$payExtId xử lý các Payment add thêm vào
        $payExtId[] = $payment->id;

        //lấy tất cả receipt chưa xuất hóa đơn của Payment Related & Thanh toán gốc
        $q1 = "(SELECT DISTINCT
        IFNULL(l1.id, '') rel_payment_id,
        IFNULL(l1.name, '') rel_payment_name,
        IFNULL(l1.payment_type, '') rel_payment_type,
        (l1.discount_amount + l1.final_sponsor + l1.loyalty_amount) discount_amount,
        GROUP_CONCAT(l2.id) rel_pmd_ids,
        SUM(l2.payment_amount) total_amount
        FROM j_payment INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
        INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0
        INNER JOIN j_paymentdetail l2 ON l2.payment_id = l1.id AND l2.deleted = 0 AND l2.status = 'Paid' AND (l2.invoice_id IS NULL OR l2.invoice_id = '')
        WHERE (j_payment.id = '{$payment->id}') AND j_payment.deleted = 0
        GROUP BY IFNULL(l1.id, ''))
        UNION (SELECT DISTINCT
        IFNULL(l1.id, '') rel_payment_id,
        IFNULL(l1.name, '') rel_payment_name,
        IFNULL(l1.payment_type, '') rel_payment_type,
        (l1.discount_amount + l1.final_sponsor + l1.loyalty_amount) discount_amount,
        GROUP_CONCAT(l2.id) rel_pmd_ids,
        SUM(l2.payment_amount) total_amount
        FROM j_payment l1
        INNER JOIN j_paymentdetail l2 ON l2.payment_id = l1.id AND l2.deleted = 0 AND l2.status = 'Paid'
        WHERE (l1.id IN ('" . implode("','", $payExtId) . "')) AND l1.deleted = 0 AND (l2.invoice_id IS NULL OR l2.invoice_id = '')
        GROUP BY IFNULL(l1.id, ''))";
        $rs1 = $GLOBALS['db']->query($q1);
        $pmdId = $payId = array();
        $totalAmount = $discountAmount = 0;
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
            $payId[] = array(
                'id' => $row['rel_payment_id'],
                'name' => $row['rel_payment_name'],
                'payment_type' => $row['rel_payment_type'],
                'total_amount' => $row['total_amount'],
            );
            $totalAmount += $row['total_amount'];
            foreach (explode(',', $row['rel_pmd_ids']) as $pmd_id)
                $pmdId[] = $pmd_id;
            $discountAmount += $row['discount_amount'];
        }

        if (!in_array($payment->id, $payId))
            $payId[] = $payment->id;

        return array(
            'payId' => $payId,
            'pmdId' => $pmdId,
            'discountAmount' => $discountAmount,
            'totalAmount' => $totalAmount,
        );
    }

    function getDescriptionCourseFee($payment_id)
    {
        $qDescriptionCourseFee = "SELECT IFNULL(j_payment.id, '') payment_id,
        IFNULL(l1.product_code, '') product_code,
        IFNULL(l1.ext_name, '') ext_name,
        IFNULL(l1.ext_unit, '') ext_unit,
        IFNULL(l1.ext_quatity, 1) ext_quatity,
        IFNULL(l1.ext_content_1, '') ext_content_1,
        IFNULL(l1.ext_content_2, '') ext_content_2
        FROM j_payment
        INNER JOIN j_coursefee_j_payment_2_c l1_1 ON j_payment.id = l1_1.j_coursefee_j_payment_2j_payment_idb AND l1_1.deleted = 0
        INNER JOIN j_coursefee l1 ON l1.id = l1_1.j_coursefee_j_payment_2j_coursefee_ida AND l1.deleted = 0
        LEFT JOIN j_payment_j_discount_1_c l2_1 ON j_payment.id = l2_1.j_payment_j_discount_1j_payment_ida AND l2_1.deleted = 0
        LEFT JOIN j_discount l2 ON l2.id = l2_1.j_payment_j_discount_1j_discount_idb AND l2.deleted = 0
        WHERE j_payment.id = '{$payment_id}' AND j_payment.deleted = 0
        GROUP BY j_payment.id";
        $rowInv = $GLOBALS['db']->fetchOne($qDescriptionCourseFee);
        //prepair data
        $rowInv['ext_name'] = htmlspecialchars_decode($rowInv['ext_name']);
        $rowInv['product_code'] = htmlspecialchars_decode($rowInv['product_code']);
        $rowInv['ext_content_1'] = htmlspecialchars_decode($rowInv['ext_content_1']);
        $rowInv['ext_content_2'] = htmlspecialchars_decode($rowInv['ext_content_2']);

        //Xử lý chèn biến vào text
        if (strpos($rowInv['ext_name'], '{payment_') !== false)
            $rowInv['ext_name'] = parseInvField('J_Payment', $payment_id, $rowInv['ext_name'], '{payment_');
        return $rowInv;
    }

    function getInvoiceBookGiftDetail($payment_id, $payExtId = array()){
        $qGetInvoiceDetail = "SELECT DISTINCT IFNULL(l3.id, '') product_id,
        IFNULL(l3.name, '') product_name,
        IFNULL(l3.code, '') product_code,
        IFNULL(l3.description, '') product_description,
        IFNULL(l3.unit, '') unit,
        IFNULL(j_inventorydetail.id, '') primaryid,
        ABS(j_inventorydetail.quantity) quantity,
        l3.unit unit, j_inventorydetail.price price,
        j_inventorydetail.amount amount,
        IFNULL(l2.id, '') payment_id,
        IFNULL(l4.id, '') category_id,
        IFNULL(l4.name, '') category_name,
        IFNULL(l5.id, '') product_type_id,
        IFNULL(l5.name, '') product_type_name,
        IFNULL(l1.id, '') product_id
        FROM j_inventorydetail
        INNER JOIN j_inventory l1 ON j_inventorydetail.inventory_id = l1.id AND l1.deleted = 0
        INNER JOIN j_payment_j_inventory_1_c l2_1 ON l1.id = l2_1.j_payment_j_inventory_1j_inventory_idb AND l2_1.deleted = 0
        INNER JOIN j_payment l2 ON l2.id = l2_1.j_payment_j_inventory_1j_payment_ida AND l2.deleted = 0
        INNER JOIN product_templates l3 ON j_inventorydetail.book_id = l3.id AND l3.deleted = 0
        LEFT JOIN product_categories l4 ON l3.category_id = l4.id AND l4.deleted = 0
        LEFT JOIN product_types l5 ON l3.type_id = l5.id AND l5.deleted = 0
        WHERE (l2.id IN ('$payment_id','" . implode("','", $payExtId)."')) AND j_inventorydetail.deleted = 0
        ORDER BY CASE";
        $qGetInvoiceDetail .= " WHEN l2.id='$payment_id' THEN 0";
        foreach($payExtId as $key => $pid) $qGetInvoiceDetail .= " WHEN l2.id='$pid' THEN ".($key+1);
        $qGetInvoiceDetail .= " ELSE ".($key+2)." END ASC ";
        $rows = $GLOBALS['db']->fetchArray($qGetInvoiceDetail);
        foreach($rows as $key => $row){
            if (strpos($row['product_description'], '{product_') !== false)
                $rows[$key]['product_description'] = parseInvField('J_Inventorydetail', $row['primaryid'], $row['product_description'], '{product_');
            if (strpos($row['product_description'], '{payment_') !== false)
                $rows[$key]['product_description'] = parseInvField('J_Payment', $row['payment_id'], $row['product_description'], '{payment_');
        }
        return $rows;
    }

    function getLoyaltyPointByPaymentID($pm_id)
    {
        $q1 = "SELECT IFNULL(ABS(j_loyalty.point), 0) point
        FROM j_loyalty
        INNER JOIN j_payment l1 ON j_loyalty.payment_id = l1.id
        AND l1.deleted = 0
        WHERE j_loyalty.deleted = 0
        AND l1.id = '{$pm_id}'";
        $point = $GLOBALS['db']->getOne($q1);
        if (empty($point)) $point = 0;
        return $point;
    }

    function getBkavTaxRateID($vat): int
    {
        switch ($vat) {
            case 5:
                return 2;
            case 10:
                return 3;
            case 8:
                return 9;
            case -1:
            case 0:
                return 4;
            default:
                return 6;
        }
    }

    function checkErrorBeforeGenerateContent($evat, $payment, $pmdt_id = '')
    {

        // Check evat_id
        if (empty($evat->id)) {
            return array(
                "success" => 0,
                "label" => translate('LBL_EVAT_ERR', 'J_Payment'),
            );
        }

        // Get bean
        $paymentDetail = BeanFactory::getBean('J_PaymentDetail', $pmdt_id);
        $contact = BeanFactory::getBean($payment->parent_type, $payment->parent_id);

        // Check get J_PaymentDetail
        if ($evat->exportType == "Receipt") {
            if (empty($paymentDetail->id)) {
                return array(
                    "success" => 0,
                    "label" => translate('LBL_EVAT_ERR_1', 'J_Payment'),
                );
            }
        }

        // Check validate email
        if ((empty($contact->email1) && empty($evat->no_email_send_to)))
            return array(
                "success" => 0,
                "label" => translate('LBL_EVAT_ERR_8', 'J_Payment'),
            );


        if ( (!empty($contact->email1) && (!filter_var($contact->email1, FILTER_VALIDATE_EMAIL)))  //Invalid email format
            || (empty($contact->email1) && !empty($evat->no_email_send_to) && !$evat->confirm_send_to))
            return array(
                "success" => 3,
                "label" => translate('LBL_EVAT_ERR_3', 'J_Payment') . ' <b>' . $evat->no_email_send_to . '</b>',
            );

        if (empty($contact->email1) && !empty($evat->no_email_send_to) && !$evat->confirm_send_to)
            $contact->email1 = $evat->no_email_send_to;

        //        // Check validate buyer name
        //        $kfj = $evat->buyer_legal_type;
        //        if (($evat->buyer_legal_type !== 'company_name'
        //        && $evat->buyer_legal_type !== 'buyer_name_null')
        //        && $evat->buyer_legal_type !== 'buyer_name_empty'
        //        && empty(mb_strtoupper($contact->$kfj, 'UTF-8'))) {
        //            return array(
        //                "success" => 0,
        //                "label" => translate('LBL_EVAT_ERR_6', 'J_Payment'),
        //            );
        //        }
        return array("success" => 1);
    }

    function generateInvContent($evat, $payment, $pmdt_id = '', $payExtId = array())
    {
        global $timedate;

        // Get bean
        $content = array();
        $contacts = BeanFactory::getBean($payment->parent_type, $payment->parent_id);
        if (!empty($pmdt_id)) $pmdt = BeanFactory::getBean('J_PaymentDetail', $pmdt_id);

        // Thông tin seller //////////////////////////////////////////////////////////////////////////
        $content['seller_infor'] = array(
            'SellerName' => $evat->seller_legal_name,
            'SellerAddress' => $evat->seller_address_line,
            'SellerTaxCode' => (!empty($evat->seller_tax_code) ? $evat->seller_tax_code : $evat->account),
        );

        // Thông tin customer //////////////////////////////////////////////////////////////////////////
        if ($payment->is_corporate){
            $account = BeanFactory::getBean('Accounts', $payment->account_id);
            $account->name = htmlspecialchars_decode($account->name);
            $account->billing_address_street = htmlspecialchars_decode($account->billing_address_street);
        }
        $contacts->name = htmlspecialchars_decode($contacts->name);
        $contacts->guardian_name = htmlspecialchars_decode($contacts->guardian_name);
        $contacts->guardian_name_2 = htmlspecialchars_decode($contacts->guardian_name_2);
        $contacts->buyer_null = htmlspecialchars_decode($contacts->buyer_null);
        $contacts->primary_address_street = htmlspecialchars_decode($contacts->primary_address_street);

        ///// Get options buyer name
        if ($evat->buyer_legal_type == 'company_name') $buyer_name = $account->name;
        elseif ($evat->buyer_legal_type == 'buyer_name_null') $buyer_name = ($evat->buyer_null) ? $evat->buyer_null : '';
        else {
            $blt = $evat->buyer_legal_type;
            if($blt == 'full_student_name') $blt = 'name';   //Fix lỗi tên Lead
            $buyer_name = $contacts->$blt;
        }
        $content['customer_infor'] = array(
            'StudentId' => $contacts->id,
            'FullStudentName' => $contacts->name,
            'GuardianName' => $contacts->guardian_name,
            'GuardianName2' => $contacts->guardian_name_2,
            'BuyerNameNull' => ($evat->buyer_null) ? $evat->buyer_null : '',
            'CompanyId' => ($payment->is_corporate) ? $account->id : '',
            'CompanyName' => ($payment->is_corporate) ? $account->name : '',
            'BuyerLegalName' => ($payment->is_corporate) ? mb_strtoupper($account->name, 'UTF-8') : '',
            'BuyerDisplayName' => mb_strtoupper($buyer_name, 'UTF-8'),
            'BuyerPhoneNumber' => $contacts->phone_mobile,
            'BuyerAddress' => ($payment->is_corporate) ? $account->billing_address_street : $contacts->primary_address_street,
            'BuyerEmail' => $contacts->email1,
            'BuyerTaxCode' => ($payment->is_corporate) ? $account->tax_code : '',
        );

        // Thông tin invoice //////////////////////////////////////////////////////////////////////////
        $discountAmount = $totalVATAmount = $totalAmountWithoutVAT = $vatPercentage = $lineNumber = $beforeDiscount = 0;
        $int = new Integer();
        if ($evat->exportType == 'Receipt') {
            if (!empty($pmdt->invoice_id)) //==> đã xuất hóa đơn
                return array(
                    "success" => 0,
                    "label" => translate('LBL_EVAT_ERR_7', 'J_Payment'),
                );
            $payId[] = $pmdt->payment_id;
            $pmdId[] = $pmdt->id;
            $totalAmount = $pmdt->payment_amount;
            $discountAmount = $pmdt->discount_amount + $pmdt->sponsor_amount + $pmdt->loyalty_amount;
            $beforeDiscount = $totalAmount + $discountAmount;
            $refID = create_guid();
            $invoiceNote = $pmdt->description;
        } else {
            //// Get total amount Payment Detail
            $resPdt = $this->getTotalAmountPmdt($payment, $payExtId);
            $payId = $resPdt['payId'];
            $pmdId = $resPdt['pmdId'];
            if (empty($pmdId)) //==> đã xuất hóa đơn
                return array(
                    "success" => 0,
                    "label" => translate('LBL_EVAT_ERR_7', 'J_Payment'),
                );

            $totalAmount = $resPdt['totalAmount'];
            $discountAmount = $resPdt['discountAmount'];
            $beforeDiscount = $totalAmount + $discountAmount;
            $refID = create_guid();
            $invoiceNote = $payment->description;
        }

        //// Invoice Detail nếu là Book/Gift    -----------------------------------------------------
        if ($payment->payment_type == 'Book/Gift') {
            $row_b = $this->getInvoiceBookGiftDetail($payment->id, $payExtId);
            if (!empty($payment->vat_rate)) $vatPercentage = $payment->vat_rate;

            /**
            * empty() chỉ trả về true cho trường hợp
            * 1. chuỗi rỗng
            * 2. biến không xác định, chưa khai báo
            * 3. Số 0 hoặc false
            */
            if (empty($vatPercentage) || floatval($vatPercentage) <= 0) $vat_rate = 'KCT';
            else $vat_rate = round($vatPercentage) . '%';

            //biến khai báo nhưng không xài
            $books = $categories = array();

            //// Get list product
            foreach ($row_b as $row) {
                $discount_rate = 0;
                $lineNumber++;
                if ($discountAmount > 0) $discount_rate = $discountAmount / $beforeDiscount;

                $amount = $row['amount'] - ($row['amount'] * $discount_rate);
                $price = $row['price'] - ($row['price'] * $discount_rate);

                $VATamount = $amount - ($amount * 100 / (100 + $vatPercentage));
                $amountWithoutVAT = $amount - $VATamount;

                //biến khai báo nhưng không xài
                $priceWithoutVAT = ($price * 100 / (100 + $vatPercentage)); //Price without VAT

                if (!in_array($row['category_name'], $categories))
                    $categories[] = $row['category_name'];
                if ($evat->bookgift_content == 'By Products' || $evat->bookgift_content == 'By Descriptions') {
                    //Xuất theo sản phẩm
                    $content['invoice_detail_infor']['InvoiceDetail'][] = array(
                        'ItemType' => 1,
                        'LineNumber' => $lineNumber,
                        'ItemCode'   => $row['product_code'],
                        'ItemName'   => ($evat->bookgift_content == 'By Products' ? $row['product_name'] : $row['product_description']),
                        'UnitName'   => $row['unit'],
                        'Quantity'   => (int)$row['quantity'],
                        'UnitPrice'     => round($priceWithoutVAT,2),   //Đơn giá bán bằng giá sau khi giảm
                        'Amount'        => round($amountWithoutVAT,2),
                        'VatPercentage' => $vatPercentage,
                        'VatAmount'     => $VATamount,
                        'VATRateName'   => $vat_rate,
                    );
                }

                $totalVATAmount += $VATamount;
                $totalAmountWithoutVAT += $amountWithoutVAT;
            }
            if ($evat->bookgift_content == 'By Categories') {
                //Xuất theo category
                $content['invoice_detail_infor']['InvoiceDetail'][] = array(
                    'ItemType' => 1, //1 HĐDV, 2: Khuyến mại , 3: Chiết khấu, 4: Ghi chú
                    'LineNumber' => 1,
                    'SortOrder' => 1,
                    'ItemCode' => '',
                    'ItemName' => implode($categories, ", "),
                    'UnitName' => 'Set',
                    'Quantity' => 1,
                    'UnitPrice' => 0, //Để đơn giá RỖNG nếu xuất theo category
                    'AmountOC' => round($totalAmountWithoutVAT,2),
                    'Amount' => round($totalAmountWithoutVAT,2),
                    'AmountWithoutVATOC' => round($totalAmountWithoutVAT,2),
                    'AmountWithoutVAT' => round($totalAmountWithoutVAT,2),
                    'VatAmount' => round($totalVATAmount,2),
                    'VatAmountOC' => round($totalVATAmount,2),
                    'VATRateName' => $vat_rate,
                    'VatPercentage' => $vatPercentage,
                );
            }

            $totalAmountWithVAT = $totalAmountWithoutVAT + $totalVATAmount;
            $text = $int->toText($totalAmountWithVAT);
        } else {  //Invoice Detail nếu là cashholder, deposit -----------------------------------------------------

            //Không lấy discount vào hóa đơn Khóa học
            $discountAmount = $VATamount = $vatPercentage = $totalVATAmount = $totalAmountWithVAT = 0;
            $totalAmountWithVAT = $totalAmount - $discountAmount;

            //Kiểm tra VAT Rate của cấu hình trong Course Fee
            $q1 = "SELECT IFNULL(l2.value, '') tax_rate
            FROM j_payment
            INNER JOIN j_coursefee_j_payment_1_c l1_1 ON j_payment.id = l1_1.j_coursefee_j_payment_1j_payment_idb AND l1_1.deleted = 0
            INNER JOIN j_coursefee l1 ON l1.id = l1_1.j_coursefee_j_payment_1j_coursefee_ida AND l1.deleted = 0
            INNER JOIN taxrates l2 ON l2.id = l1.taxrate_id AND l2.deleted = 0
            WHERE (j_payment.id = '{$payment->id}') AND j_payment.deleted = 0";
            $vatPercentage = $GLOBALS['db']->getOne($q1);
            if (empty($vatPercentage) || floatval($vatPercentage) <= 0) {
                $vat_rate = 'KCT';
                $totalAmountWithoutVAT = $totalAmountWithVAT;
            } else {
                $vat_rate = round($vatPercentage) . '%';

                $totalVATAmount = $totalAmountWithVAT - ($totalAmountWithVAT * 100 / (100 + $vatPercentage));
                $totalAmountWithoutVAT = $totalAmountWithVAT - $totalVATAmount;  //without VAT
            }
            //END
            $totalAmountWithVAT = $totalAmountWithoutVAT + $totalVATAmount;
            $text = $int->toText($totalAmountWithVAT);

            //// Lay danh sach item
            $rowInv = $this->getDescriptionCourseFee($payment->id);
            if ($payment->payment_type == 'Deposit') $rowInv['ext_name'] = $evat->deposit_content;
            if ($payment->payment_type == 'Placement Test') $rowInv['ext_name'] = $evat->placement_test_content;
            if ($evat->preview) $rowInv['ext_name'] = nl2br($rowInv['ext_name']);
            if (empty($rowInv['ext_quatity'])) $rowInv['ext_quatity'] = 1;
            if (empty($rowInv['ext_unit'])) $rowInv['ext_unit'] = 'Khóa';

            $lineNumber++;
            $content['invoice_detail_infor']['InvoiceDetail'][] = array(
                'ItemType' => 1, //1 HĐDV, 2: Khuyến mại , 3: Chiết khấu, 4: Ghi chú
                'LineNumber' => $lineNumber,
                'SortOrder' => $lineNumber,
                'ItemCode' => $rowInv['product_code'],
                'ItemName' => $rowInv['ext_name'],
                'UnitName' => $rowInv['ext_unit'],
                'Quantity' => (int)$rowInv['ext_quatity'],
                'UnitPrice' => round($totalAmountWithoutVAT / (int)$rowInv['ext_quatity'],2),
                'AmountOC' => round($totalAmountWithoutVAT,2),
                'Amount' => round($totalAmountWithoutVAT,2),
                'AmountWithoutVATOC' => round($totalAmountWithoutVAT,2),
                'AmountWithoutVAT' => round($totalAmountWithoutVAT,2),
                'VatAmount' => round($totalVATAmount,2),
                'VatAmountOC' => round($totalVATAmount,2),
                'VATRateName' => $vat_rate,
                'VatPercentage' => $vatPercentage,
            );
            $ext_content_1 = json_decode(html_entity_decode($rowInv['ext_content_1']));
            $ext_content_2 = json_decode(html_entity_decode($rowInv['ext_content_2']));
            $ext = [$ext_content_1, $ext_content_2];
            foreach ($ext as $ind => $ext) {
                if (!empty($ext->name)) {
                    $lineNumber++;
                    $ext->amount = round($ext->amount,2);
                    $content['invoice_detail_infor']['InvoiceDetail'][] = array(
                        'ItemType' => 4, //1 HĐDV, 2: Khuyến mại , 3: Chiết khấu, 4: Ghi chú
                        'LineNumber' => $lineNumber,
                        'SortOrder' => $lineNumber,
                        'ItemCode' => $rowInv['product_code'] . "_$ind",
                        'ItemName' => $ext->name,
                        'UnitName' => $ext->unit,
                        'Quantity' => (!empty($ext->quantity) ? (int)$ext->quantity : 1),
                        'UnitPrice' => (!empty($ext->amount) ? (float)$ext->amount : 0),
                        'AmountOC' => (!empty($ext->amount) ? (float)$ext->amount : 0),
                        'Amount' => (!empty($ext->amount) ? (float)$ext->amount : 0),
                        'AmountWithoutVATOC' => (!empty($ext->amount) ? (float)$ext->amount : 0),
                        'AmountWithoutVAT' => (!empty($ext->amount) ? (float)$ext->amount : 0),
                        'VatAmount' => 0,
                        'VatAmountOC' => 0,
                        'VATRateName' => $vat_rate,
                        'VatPercentage' => $vatPercentage,
                    );
                }
            }
        }

        //// Get invoice date
        $evat->invoice_date = $timedate->convertToDBDate($evat->invoice_date);

        $content['invoice_detail_infor']['Invoice'] = array(
            'RefID' => $refID,
            'InvoiceDate' => $evat->invoice_date,
            'PaymentId' => $payment->id,
            'PaymentType' => $payment->payment_type, //Cashholder, book/gift
            'PaymentMethod' => 'TM/CK',
            'Is_corporate' => $payment->is_corporate,
            'InvoiceNote' => $invoiceNote,
            'VatPercentage' => $vatPercentage,
            'VATRateName' => $vat_rate,
            'TotalAmountWithoutVAT' => round($totalAmountWithoutVAT,2),
            'TotalVATAmount' => round($totalVATAmount,2),
            'TotalAmountWithVAT' => round($totalAmountWithVAT,2),
            'DiscountAmount' => 0,//Không lấy Discount
            'TotalAmountWithVATInWords' => $text,
        );

        // Thông tin để tạo invoice new trong J_Invoice-------------------------------
        $content['create_invoice'] = array(
            'ExportIvoiceType' => $evat->exportType,
            'PayId' => $payId,
            'PmdId' => $pmdId,
            'Before_discount' => round($totalAmountWithoutVAT,2),
            'Total_discount_amount' => 0,//Không lấy Discount
            'Invoice_amount' => round($totalAmount,2),
            'Vat' => $vatPercentage,
            'VATRateName' => $vat_rate,
            'Payment_id' => $payment->id,
            'Account_id' => ($payment->is_corporate) ? $account->id : '',
            'InvoiceDate' => $evat->invoice_date,
            'Team_id' => $payment->team_id,
            'Team_set_id' => $payment->team_id,
            'Assigned_user_id' => $payment->assigned_user_id,
        );

        return array(
            "success" => "1",
            "content" => $content,
        );
    }

    function generateJSONContentInvoice($evat, $res_content)
    {
        $seller = $res_content["content"]["seller_infor"];
        $customer = $res_content["content"]["customer_infor"];
        $invoice = $res_content["content"]['invoice_detail_infor'];
        $string_json = '';

        switch ($evat->supplier) {
            case 'Bkav':
                $partnerInvoiceStringID = create_guid();
                $res_content["content"]['create_invoice']['PartnerInvoiceStringID'] = $partnerInvoiceStringID;
                foreach ($invoice['InvoiceDetail'] as $key => $value) {
                    $bkavTaxRateID = $this->getBkavTaxRateID($value['VatPercentage']);
                    $data_list['ListInvoiceDetailsWS'][$key] = array(
                        "ItemName" => $value['ItemName'],
                        "UnitName" => $value['UnitName'],
                        "Qty" => $value['Quantity'],
                        "Price" => $value['UnitPrice'],
                        "Amount" => $value['Amount'],
                        "TaxRateID" => $bkavTaxRateID,
                        "TaxRate" => $value['VATRateName'] == 'KCT' ? -1 : (int)$value['VatPercentage'],
                        "TaxAmount" => $value['VatAmount'],
                        "IsDiscount" => false,
                        "IsIncrease" => null,
                        "ItemTypeID" => 0
                    );
                }
                $param = array(
                    "CmdType" => 112,
                    "CommandObject" => array(
                        array(
                            "Invoice" => array(
                                "InvoiceTypeID" => 1,
                                "InvoiceDate" => $invoice['Invoice']['InvoiceDate'],
                                "BuyerName" => $customer['BuyerDisplayName'],
                                "BuyerTaxCode" => $customer['BuyerTaxCode'],
                                "BuyerUnitName" => $customer['BuyerLegalName'],
                                "BuyerAddress" => $customer['BuyerAddress'],
                                "BuyerBankAccount" => "",
                                "PayMethodID" => 3,
                                "ReceiveTypeID" => 3,
                                "ReceiverEmail" => $customer['BuyerEmail'],
                                "ReceiverMobile" => $customer['BuyerPhoneNumber'],
                                "ReceiverAddress" => $customer['BuyerAddress'],
                                "ReceiverName" => $customer['FullStudentName'],
                                "Note" => '',
                                "BillCode" => $invoice['Invoice']['PaymentId'],
                                "CurrencyID" => "VND",
                                "ExchangeRate" => 1.0,
                                "InvoiceForm" => $evat->pattern,
                                "InvoiceSerial" => $evat->serial_no,
                                "InvoiceNo" => 0,
                            ),
                            "ListInvoiceDetailsWS" => $data_list['ListInvoiceDetailsWS'],
                            "PartnerInvoiceStringID" => $partnerInvoiceStringID,
                        )
                    )
                );
                $string_json = json_encode(array(
                    "partnerGUID" => $evat->partnerGUID,
                    "CommandData" => base64_encode(json_encode($param, JSON_UNESCAPED_UNICODE)),
                ));
                break;
            case 'Misa':
                $data_list = array(
                    'RefID' => $invoice['Invoice']['RefID'], //ID hóa đơn
                    'InvSeries' => $evat->serial_no, //Ký hiệu hóa đơn
                    'InvoiceName' => 'Hóa đơn giá trị gia tăng',
                    'InvDate' => $invoice['Invoice']['InvoiceDate'],
                    //'InvoiceIssuedDate'=> $invoice['Invoice']['InvoiceDate'],
                    'CurrencyCode' => 'VND',
                    "ExchangeRate" => 1,
                    'PaymentMethodName' => $invoice['Invoice']['PaymentMethod'],

                    'BuyerLegalName' => $customer['BuyerLegalName'],
                    'BuyerFullName' => $customer['BuyerDisplayName'],
                    //'BuyerDisplayName'  => $customer['BuyerDisplayName'],
                    'BuyerTaxCode' => $customer['BuyerTaxCode'],
                    'BuyerAddress' => $customer['BuyerAddress'],
                    'BuyerCode' => $customer['StudentId'],
                    'BuyerPhoneNumber' => $customer['BuyerPhoneNumber'],
                    'BuyerEmail' => $customer['BuyerEmail'],

                    'BuyerBankAccount' => '',
                    'BuyerBankName' => '',

                    'ReferenceType' => null,
                    //'OrgInvoiceType' => null, //Tạo mới Truyền: Null Loại hóa đơn(1: Hóa đơn 123; 3: Hóa đơn 51)
                    //'OrgInvTemplateNo' => null,
                    //'OrgInvSeries' => null,
                    //'OrgInvNo' => null,
                    //'OrgInvDate' => null,
                    'TotalSaleAmountOC' => $invoice['Invoice']['TotalAmountWithoutVAT'], // tổng tiền hàng hóa
                    'TotalAmountWithoutVATOC' => $invoice['Invoice']['TotalAmountWithoutVAT'], //tổng tiền thanh toán trước thuế
                    'TotalVATAmountOC' => $invoice['Invoice']['TotalVATAmount'],  //tổng tiền thuế
                    'TotalDiscountAmountOC' => $invoice['Invoice']['DiscountAmount'],  //tổng tiền chiết khấu
                    'TotalAmountOC' => $invoice['Invoice']['TotalAmountWithVAT'],  //tổng tiền thanh toán (TotalAmountWithoutVATOC+TotalDiscountAmountOC)

                    'TotalSaleAmount' => $invoice['Invoice']['TotalAmountWithoutVAT'], // tổng tiền hàng hóa*ExchangeRate(tỷ giá)
                    'TotalAmountWithoutVAT' => $invoice['Invoice']['TotalAmountWithoutVAT'], //tổng tiền thanh toán trước thuế*ExchangeRate(tỷ giá)
                    'TotalVATAmount' => $invoice['Invoice']['TotalVATAmount'],  //tổng tiền thuế*ExchangeRate(tỷ giá)
                    'TotalDiscountAmount' => $invoice['Invoice']['DiscountAmount'],  //tổng tiền chiết khấu*ExchangeRate(tỷ giá)
                    'TotalAmount' => $invoice['Invoice']['TotalAmountWithVAT'],  //tổng tiền thanh toán (TotalAmountWithoutVATOC+TotalDiscountAmountOC)*ExchangeRate(tỷ giá)

                    'TotalAmountInWords' => $invoice['Invoice']['TotalAmountWithVATInWords'],
                    // danh sách hàng hóa, dịch vụ
                    // 'OriginalInvoiceDetail'

                    'SellerTaxCode' => $seller['SellerTaxCode'],
                    'SellerLegalName' => $seller['SellerName'],
                    'SellerAddressLine' => $seller['SellerAddress'],
                    'SellerPhoneNumber' => '',
                    'SellerWebsite' => '',
                    'SellerEmail' => '',
                    'SellerBankAccount' => '',
                    'SellerBankName' => '',

                    'IsTaxReduction43' => false,

                    'CustomField1' => $evat->seller_legal_name_1,
                    'CustomField2' => $evat->seller_address_line_1,
                    'CustomField3' => (!empty($evat->seller_tax_code) ? $evat->seller_tax_code : $evat->account),

                    'IsSendEmail' => true,
                    'ReceiverName' => $customer['FullStudentName'],
                    'ReceiverEmail' => $customer['BuyerEmail'],
                    'IsInvoiceSummary' => false, //dùng cho hóa đơn không mã,nếu gửi bảng tổng hợp lên thuế thì bằng :true
                );
                if ($invoice['Invoice']['VatPercentage'] > 0) {
                    $data_list['IsTaxReduction43'] = false;
                }
                //TaxRateInfo
                $data_list['TaxRateInfo'][0] = array(
                    'VATRateName' => $invoice['Invoice']['VATRateName'],
                    'AmountWithoutVATOC' => $invoice['Invoice']['TotalAmountWithoutVAT'],
                    'VATAmountOC' => $invoice['Invoice']['TotalVATAmount'],
                );
                //OriginalInvoiceDetail
                foreach ($invoice['InvoiceDetail'] as $index => $detail) {
                    $data_list['OriginalInvoiceDetail'][] = array(
                        'ItemType' => $detail['ItemType'],
                        'LineNumber' => $detail['LineNumber'],
                        'SortOrder' => $detail['LineNumber'],
                        'ItemCode' => $detail['ItemCode'],
                        'ItemName' => $detail['ItemName'],
                        'UnitName' => $detail['UnitName'],
                        'Quantity' => $detail['Quantity'],
                        'UnitPrice' => $detail['UnitPrice'],
                        'AmountOC' => $detail['Amount'],
                        'Amount' => $detail['Amount'],
                        'AmountWithoutVATOC' => $detail['Amount'],
                        'AmountWithoutVAT' => $detail['Amount'],
                        'VATRateName' => $invoice['Invoice']['VATRateName'],
                        'VATAmountOC' => $detail['VatAmount'],
                        'VATAmount' => $detail['VatAmount'],
                    );
                }

                $data_list['OptionUserDefined'] = array(
                    'MainCurrency' => 'VND',
                    'AmountDecimalDigits' => '0',
                    'AmountOCDecimalDigits' => '2',
                    'UnitPriceOCDecimalDigits' => '0',
                    'UnitPriceDecimalDigits' => '0',
                    'QuantityDecimalDigits' => '2',
                    'CoefficientDecimalDigits' => '2',
                    'ExchangRateDecimalDigits' => '0',
                );
                $param['InvoiceDataList'] = $data_list;

                $string_json = json_encode($param, JSON_UNESCAPED_UNICODE);
                break;
            case 'Viettel':
                $buyerInfo = array(
                    "buyerName" => $customer['BuyerDisplayName'],
                    "buyerLegalName" => $customer['BuyerLegalName'],
                    "buyerTaxCode" => $customer['BuyerTaxCode'],
                    "buyerAddressLine" => $customer['BuyerAddress'],
                    "buyerPhoneNumber" => $customer['BuyerPhoneNumber'],
                    "buyerEmail" => $customer['BuyerEmail']
                );
                $payments = [[
                    "paymentMethod" => "3",
                    "paymentMethodName" => "TM/CK"
                ]];

                $payId = $res_content["content"]['create_invoice']['Payment_id'];
                $transactionUuid = create_guid();
                if(!empty($payId)){
                    //Giữ cố định Transaction ID trong TH Exception - Nếu lần gần nhất là Exception thì lấy lại ID cũ xuất lại
                    $last_invoice = $GLOBALS['db']->fetchOne("SELECT DISTINCT IFNULL(l1.invoiceGUID,'') invoiceGUID, IFNULL(l1.status,'') status
                        FROM j_payment INNER JOIN j_invoice_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_id AND l1_1.deleted = 0
                        INNER JOIN j_invoice l1 ON l1.id = l1_1.invoice_id AND l1.deleted = 0
                        WHERE (j_payment.id = '$payId') AND j_payment.deleted = 0
                        ORDER BY l1.date_entered DESC LIMIT 1");
                    if($last_invoice['status'] == 'Published' && !empty($last_invoice['invoiceGUID']))
                        $transactionUuid = $last_invoice['invoiceGUID'];
                }

                //END
                $generalInvoiceInfo = [
                    "transactionUuid" => $transactionUuid,
                    "invoiceType" => "1",
                    "templateCode" => $evat->pattern,
                    "invoiceSeries" => $evat->serial_no,
                    "invoiceIssuedDate" => time() * 1000,
                    "currencyCode" => "VND",
                    "adjustmentType" => "1",
                    "paymentStatus" => true,
                    "cusGetInvoiceRight" => true
                ];
                $itemInfo = array();
                $summarizeInfo = array(
                    "sumOfTotalLineAmountWithoutTax" => $invoice['Invoice']['TotalAmountWithoutVAT'],
                    "totalAmountWithoutTax" => $invoice['Invoice']['TotalAmountWithoutVAT'],
                    "totalTaxAmount" => $invoice['Invoice']['TotalVATAmount'],
                    "totalAmountWithTax" => $invoice['Invoice']['TotalAmountWithVAT'],
                    "totalAmountAfterDiscount" => $invoice['Invoice']['TotalAmountWithoutVAT'],
                    "totalAmountWithTaxInWords" => $invoice['Invoice']['TotalAmountWithVATInWords'],
                    "discountAmount" => $invoice['Invoice']['DiscountAmount'],
                    "taxPercentage" => (int)$invoice['Invoice']['VATRateName']
                );
                foreach ($invoice['InvoiceDetail'] as $key => $value) {
                    $itemInfo[] = array(
                        "lineNumber" => $value['LineNumber'],
                        "itemCode" => $value['ItemCode'],
                        "itemName" => $value['ItemName'],
                        "unitName" => $value['UnitName'],
                        "itemNote" => "",
                        "unitPrice" => $value['UnitPrice'],
                        "quantity" => $value['Quantity'],
                        "itemTotalAmountWithoutTax" => $value['Amount'],
                        "itemTotalAmountWithTax" => $value['Amount'] + $value['VatAmount'],
                        "itemTotalAmountAfterDiscount" => $value['Amount'],
                        "taxPercentage" => (float)$value['VatPercentage'],
                        "taxAmount" => $value['VatAmount'],
                        "customTaxAmount" => 0,
                        "discount" => 0,
                        "itemDiscount" => 0,
                        "batchNo" => "",
                        "expDate" => ""
                    );
                }
                $taxBreakdowns[0]['taxPercentage'] = $value['VatPercentage'];
                $taxBreakdowns[0]['taxableAmount'] += $value['Amount'];
                $taxBreakdowns[0]['taxAmount'] += $value['VatAmount'];
                if ($invoice['Invoice']['VATRateName'] == 'KCT') {
                    $taxBreakdowns = array(array(
                        "taxPercentage" => -2,
                        "taxableAmount" => $invoice['Invoice']['TotalAmountWithoutVAT']
                    ));
                } else {
                    $taxBreakdowns = array(array(
                        "taxPercentage" => (float)$invoice['Invoice']['VatPercentage'],
                        "taxableAmount" => $invoice['Invoice']['TotalAmountWithoutVAT'],
                        "taxAmount" => $invoice['Invoice']['TotalVATAmount']
                    ));
                }
                $string_json = json_encode(array(
                    "generalInvoiceInfo" => $generalInvoiceInfo,
                    "buyerInfo" => $buyerInfo,
                    //                    "sellerInfo" => $sellerInfo,
                    "payments" => $payments,
                    "itemInfo" => $itemInfo,
                    "summarizeInfo" => $summarizeInfo,
                    "taxBreakdowns" => $taxBreakdowns
                    ), JSON_UNESCAPED_UNICODE);
                break;
        }
        return array(
            "success" => 1,
            "string_json" => $string_json,
            "bean_invoice" => $res_content["content"]['create_invoice'],
        );
    }

    function sendApiAndCreateInvoice($evat, $inv_new, $string_json = '', $preview = 'push'){
        switch ($evat->supplier) {
            case 'Bkav':        /////   BKAV    /////
                // Gửi api tạo einvoice
                $res = $this->CreateEinvoice($evat, $string_json);
                if ($res['Status'] != 0) {    //// Failed
                    $return = array(
                        "success" => 0,
                        "label" => translate('LBL_EVAT_ERR', 'J_Payment') . '</br>' . $res['isError'],
                    );
                } else { //// Successful
                    // Tạo j_invoice
                    $newInvoiceNo = trim($res['InvoiceNo']);
                    while (strlen($newInvoiceNo) < 7) $newInvoiceNo = "0" . $newInvoiceNo;
                    $newContent = $res['Content'];
                    $newTranID = $res['MTC'];
                    $newSerial = $res['InvoiceSerial'];
                    $newPattern = $res['InvoiceForm'];
                    $invNew = new J_Invoice();
                    $invNew->invoiceGUID = $res['InvoiceGUID'];
                    $invNew->PartnerInvoiceStringID = $inv_new['PartnerInvoiceStringID'];
                    $inv_status = 'Paid';
                    $return = array("success" => 1);
                }
                break;
            case 'Viettel':
                // Gửi api tạo einvoice
                $e = new ViettelEInvoice($evat);
                if (empty($e->getToken())) {
                    $return = array(
                        "success" => 0,
                        "label" => translate('LBL_EVAT_ERR', 'J_Payment') . '</br>Incorrect User/password in config!',
                    );
                } else {
                    if ($preview == 'preview') {
                        $ch = curl_init($evat->host . '/services/einvoiceapplication/api/InvoiceAPI/InvoiceUtilsWS/createInvoiceDraftPreview/' . $evat->account);
                    } elseif ($evat->publish_type == 'issued_signed') {
                        $ch = curl_init($evat->host . '/services/einvoiceapplication/api/InvoiceAPI/InvoiceWS/createInvoice/' . $evat->account);
                    } else {
                        $ch = curl_init($evat->host . '/services/einvoiceapplication/api/InvoiceAPI/InvoiceWS/createOrUpdateInvoiceDraft/' . $evat->account);
                    }
                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Content-Type: application/json",
                        "Cookie: access_token=" . $e->getToken()
                    ));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $string_json);
                    $res = curl_exec($ch);
                    $res = json_decode($res, true);
                    //PREVIEW INVOICE
                    if($preview == 'preview'){
                        if (empty($res['errorCode']) && !empty($res['fileToBytes'])) {
                            return array(
                                "success" => 4,
                                "base64PDF"   => $res['fileToBytes'],
                            );
                        } else {
                            return array(
                                "success" => 0,
                                "label" => translate('LBL_EVAT_ERR', 'J_Payment') . $res['data']
                            );
                        }
                    }else{
                        //PUSH INVOICE
                        //issued_signed
                        $_json          = json_decode($string_json, 1);
                        $invNew         = new J_Invoice();
                        $invNew->invoiceGUID = $_json['generalInvoiceInfo']['transactionUuid']; //transactionUuid
                        $newSerial      = $evat->serial_no;
                        $newPattern     = $evat->pattern;
                        if (empty($res['errorCode']) && !empty($res['result']['invoiceNo'])) {
                            $return = array("success" => 1);
                            $newInvoiceNo = $res['result']['invoiceNo'];
                            $newContent   = json_encode($res['result']);
                            $newTranID    = $res['result']['reservationCode'];
                            $inv_status   = 'Paid';
                        }
                        //Invoice draft
                        elseif($evat->publish_type == 'draft' && isset($res['result'])) {
                            $return = array("success" => 1);
                            $newInvoiceNo   = '-Draft-';
                            $newContent     = json_encode($res['result']);
                            $newTranID      = $invNew->invoiceGUID;
                            $inv_status     = 'Draft';
                        }
                        //Exception (Error)
                        else{
                            $return = array("success" => 0,"label" => translate('LBL_EVAT_ERR', 'J_Payment').'<br>'.$res['data']);
                            $newInvoiceNo = $res['result']['invoiceNo'];
                            $newContent   = json_encode($res);
                            $newTranID    = $res['result']['reservationCode'];
                            $inv_status   = 'Published';//Status: Exception (lỗi)
                        }
                    }
                }
                break;
            case 'Misa':        /////   MISA    /////
                //Get Token
                $res = $this->get_EvatToken($evat);
                if (!$res['success']) {
                    $return = array(
                        "success" => 0,
                        "label" => translate('LBL_EVAT_ERR', 'J_Payment') . '</br> Kết nối API Misa không thành công.!',
                        "error" => json_encode($res),
                    );
                } else {
                    $token = $res['token'];
                    //Get API phat hanh
                    $res1 = $this->get_IptemplatePublish($evat, $token);
                    if (!$res1['success']) {
                        $return = array(
                            "success" => 0,
                            "label" => translate('LBL_EVAT_ERR', 'J_Payment') . '</br> Kiểm tra thông tin Phát hành không thành công. Invalid Template Number (InvoicePublishing) !!.',
                            "error" => json_encode($res1),
                        );
                    } else {
                        $invNew         = new J_Invoice();
                        $inv_status     = 'Published';//Status: Exception (lỗi)
                        $newInvoiceNo   = '-none-';
                        $res = $this->PublishInvoiceHSM($evat, $string_json, $token, $res1);
                        if (!$res['success']) {
                            $return = array(
                                "success" => 0,
                                "label" => translate('LBL_EVAT_ERR', 'J_Payment') . '</br> Hóa đơn Phát hành và Ký số HSM không thành công, vui lòng thử lại sau. Error Code: '.$res['errorCode'],
                                "error" => json_encode($res),
                            );
                            $string_json = $res['data_request'];
                        } else {
                            $inv_status     = 'Paid';
                            //Xu ly lay so hoa don
                            $newSerial      = $evat->serial_no;
                            $newPattern     = $evat->pattern;

                            $newInvoiceNo   = $res['InvoiceNumber'];
                            $newContent     = $res['Content'];
                            $newTranID      = $res['TransactionID'];
                            $invNew->vat    = $inv_new['Vat'];
                            $return = array("success" => 1);
                            if (empty($newTranID)) {
                                $inv_status = 'Published';
                                $return = array(
                                    "success" => 0,
                                    "label" => translate('LBL_EVAT_ERR', 'J_Payment') . '</br> Hóa đơn Phát hành và Ký số HSM không thành công.',
                                    "error" => json_encode($res),
                                );
                            }
                        }
                    }
                }
                break;
        }
        if (!$return['success']) $invNew->content_vat_invoice = $return['error'];
        else $invNew->content_vat_invoice = $newContent;
        if (empty($inv_status)) return $return;
        if (empty($newInvoiceNo)) $newInvoiceNo = '-none-';


        // Tạo invoice new trong J_Invoice
        $invNew->is_manual  = 0; // hoá đơn hệ thống tạo ra
        $invNew->name       = $newInvoiceNo;
        $invNew->supplier = $evat->supplier;
        $invNew->invoice_date = $inv_new['InvoiceDate'];
        $invNew->before_discount = $inv_new['Before_discount'];
        $invNew->total_discount_amount = $inv_new['Total_discount_amount'];
        $invNew->invoice_amount = $inv_new['Invoice_amount'];
        $invNew->serial_no = $newSerial;
        $invNew->pattern = $newPattern;
        $invNew->description = $string_json; //DATA REQUEST
        $invNew->payment_id = $inv_new['Payment_id'];
        $invNew->account_id = $inv_new['Account_id'];
        $invNew->status = $inv_status;
        $invNew->team_id = $inv_new['Team_id'];
        $invNew->team_set_id = $inv_new['Team_set_id'];
        $invNew->assigned_user_id = $inv_new['Assigned_user_id'];
        $invNew->transaction_id = $newTranID;
        $invNew->save();
        //Add Relationship Invoice - Payment
        if (!empty($inv_new['Payment_id'])) {
            $invNew->load_relationship('j_invoice_j_payment_1');
            $invNew->j_invoice_j_payment_1->add($inv_new['Payment_id']);
        }
        //Update Receipt
        if (!empty($invNew->name) && $invNew->name != '-none-')
            $GLOBALS['db']->query("UPDATE j_paymentdetail SET invoice_id = '{$invNew->id}' WHERE id IN ('" . implode("','", $inv_new['PmdId']) . "') AND deleted = 0");

        //Update invoice_no_current for all childs of team
        $GLOBALS['db']->query("UPDATE j_configinvoiceno SET invoice_no_current = '{$invNew->name}' WHERE serial_no = '{$evat->serial_no}' AND pattern = '{$evat->pattern}' AND deleted = 0");
        return $return;
    }
}

?>
