<?php
require_once("custom/include/_helper/junior_revenue_utils.php");
require_once("custom/include/_helper/junior_class_utils.php");
//E-Invoice
require_once("custom/include/ConvertMoneyString/convert_number_to_string.php");
require_once("custom/modules/J_Payment/HelperPayment.php");
require_once 'custom/include/ViettelEInvoice/ViettelEInvoice.php';

switch ($_POST['type']) {
    case 'ajaxGetStudentInfo':
        $result = ajaxGetStudentInfo($_POST['parent_id'], $_POST['parent_type'], $_POST['record_id'], $_POST['payment_type'], $_POST['payment_date'], $_POST['current_team_id']);
        echo $result;
        break;
    case 'ajaxUpdatePaymentDetail':
        $result = ajaxUpdatePaymentDetail($_POST['payment_detail'], $_POST['payment_amount'], $_POST['payment_method'], $_POST['payment_date'], $_POST['handle_action']);
        echo $result;
        break;
    case 'ajaxListPaidList':
        $result = ajaxListPaidList($_POST['payment_id']);
        echo $result;
        break;
    case 'ajaxUndoPayment':
        $result = ajaxUndoPayment($_POST['payment_id'], $_POST['payment_type']);
        echo $result;
        break;
    case 'finish_printing':
        $result = finish_printing($_POST['printing_id']);
        echo $result;
        break;
    case 'caculateDropPayment':
        $result = caculateDropPayment($_POST['payment_id'], $_POST['dl_date']);
        echo $result;
        break;
    case 'createDropPayment':
        $result = createDropPayment();
        echo $result;
        break;
    case 'ajaxConvertPayment':
        $result = ajaxConvertPayment();
        echo $result;
        break;
    case 'autoGetNextInvoice':
        $result = autoGetNextInvoice($_POST['team_id'], $_POST['payment_id'], $_POST['payment_detail_id'], $_POST['extPaymentId']);
        echo $result;
        break;
        break;
    case 'ajaxCheckVoucherCode':
        $result = ajaxCheckVoucherCode($_POST['voucher_code'], $_POST['team_id']);
        echo $result;
        break;
    case 'ajaxCancelReceipt':
        $result = ajaxCancelReceipt($_POST['payment_detail'], $_POST['description']);
        echo $result;
        break;
    case 'ajaxCancelInvoice':
        $result = ajaxCancelInvoice($_POST['invoice_id'], $_POST['payment_id'], $_POST['description']);
        echo $result;
        break;
    case 'ajaxGetInvoiceNo':
        $result = ajaxGetInvoiceNo($_POST['payment_id'], $_POST['buyer_legal_type'], $_POST['payment_detail_id'], $_POST['invoice_date'], $_POST['extPaymentId'], 'push');
        //Update Invoing - TH đang xuất hoá đơn Ngăn chặn xuất Trùng hoá đơn
        if(!empty($result['evat_id'])) $GLOBALS['db']->query("UPDATE j_configinvoiceno SET invoicing=0 WHERE id='{$result['evat_id']}'");
        echo json_encode($result);
        break;
    case 'ajaxGetPreview':
        $result = ajaxGetInvoiceNo($_POST['payment_id'], $_POST['buyer_legal_type'], $_POST['payment_detail_id'], $_POST['invoice_date'], $_POST['extPaymentId'], 'preview');
        if(!empty($result['evat_id'])) $GLOBALS['db']->query("UPDATE j_configinvoiceno SET invoicing=0 WHERE id='{$result['evat_id']}'");
        echo json_encode($result);
        break;
    case 'ajaxExportInvoice':
        $result = ajaxExportInvoice($_POST['payment_id'], $_POST['invoice_id']);
        echo $result;
        break;
    case 'addEvatCorporate':
        $result = addEvatCorporate($_POST['payment_id'], $_POST['account_id'], $_POST['action_type']);
        echo $result;
        break;
    case 'ajaxCheckVoucherFreeEventTAPTAP':
        $result = ajaxCheckVoucherFreeEventTAPTAP($_POST['mobile_phone'], $_POST['sponsor_code'], $_POST['center_code']);
        echo $result;
        break;
    case 'ajaxRedeemVoucherFreeEventTAPTAP':
        $result = ajaxRedeemVoucherFreeEventTAPTAP($_POST['mobile_phone'], $_POST['sponsor_code'], $_POST['center_code']);
        echo $result;
        break;
    case 'ajaxRecalRemain':
        $result = ajaxRecalRemain($_POST['record_id']);
        echo $result;
        break;
    case 'ajaxLoadSessions':
        $result = ajaxLoadSessions();
        echo $result;
        break;
    case 'ajaxSaveAutoEnroll':
        $result = ajaxSaveAutoEnroll();
        echo $result;
        break;
    case 'sendPaySlip':
        $result = sendPaySlip($_POST['payment_detail_id']);
        echo $result;
        break;
}

// ----------------------------------------------------------------------------------------------------------\\

function ajaxGetStudentInfo($parent_id, $parent_type = 'Contacts', $record_id = '', $payment_type = 'Cashholder', $payment_date = '', $current_team_id = '')
{
    global $timedate, $current_user;
    if (empty($payment_date)) $payment_date = $timedate->nowDate();

    //Change team_id
    if (empty($current_team_id) /*|| checkParentTeam($current_team_id)*/) {
        $student = BeanFactory::getBean('Contacts', $parent_id);
        $current_team_id = $student->team_id;
    }
    $crTeam = BeanFactory::getBean('Teams', $current_team_id, array('disable_row_level_security' => true));
    $parentBean = BeanFactory::getBean($parent_type, $parent_id, array('disable_row_level_security' => true));

    foreach (['module_name', 'id', 'name', 'class_code', 'class_code', 'kind_of_course', 'level',
        'phone_mobile', 'birthdate',
        'assigned_user_id', 'assigned_user_name',
        'team_id', 'team_name'] as $keyField)
        $info[$keyField] = $parentBean->$keyField;

    if ($parent_type == 'J_Class') {
        //Load student list
        $sqlStd = "SELECT IFNULL(l1.id, '') student_id,
        IFNULL(l1.contact_id, '') student_code,  IFNULL(l1.full_student_name, '') student_name,
        IFNULL(l1.phone_mobile, '') phone_mobile, l1.birthdate birthdate
        FROM j_studentsituations jst
        INNER JOIN contacts l1 ON jst.student_id = l1.id AND l1.deleted = 0 AND jst.student_type = 'Contacts'
        INNER JOIN j_class l2 ON jst.ju_class_id = l2.id AND l2.deleted = 0
        WHERE (l2.id = '{$parentBean->id}') AND jst.deleted = 0
        GROUP BY student_id";
        $rows = $GLOBALS['db']->fetchArray($sqlStd);
        foreach ($rows as $row) {
            $left_list[$row['student_id']] = $row['student_name'];
        }

    } else {
        $qTeam = "AND j_payment.team_set_id IN
        (SELECT tst.team_set_id
        FROM team_sets_teams tst
        INNER JOIN team_memberships team_memberships ON tst.team_id = team_memberships.team_id AND team_memberships.user_id = '{$current_user->id}' AND team_memberships.deleted = 0)";
        if ($GLOBALS['current_user']->isAdmin()) $qTeam = "";

        $q3 = "SELECT DISTINCT
        IFNULL(j_payment.id, '') payment_id, IFNULL(j_payment.name, '') payment_code,
        IFNULL(j_payment.payment_type, '') payment_type, j_payment.payment_date payment_date,
        j_payment.payment_expired payment_expired, j_payment.payment_amount payment_amount,
        j_payment.paid_amount paid_amount, j_payment.deposit_amount deposit_amount,
        j_payment.total_hours total_hours, j_payment.remain_amount remain_amount,
        j_payment.remain_hours remain_hours, j_payment.use_type use_type,
        j_payment.start_study start_study, j_payment.end_study end_study,
        j_payment.description description, IFNULL(l2.id, '') assigned_user_id,
        IFNULL(l2.full_user_name, '') assigned_user_name, IFNULL(l7.id, '') closed_sale_user_id,
        IFNULL(l7.full_user_name, '') closed_sale_user_name, IFNULL(l8.id, '') pt_demo_user_id,
        IFNULL(l8.full_user_name, '') pt_demo_user_name, IFNULL(l3.id, '') team_id,
        IFNULL(l4.id, '') course_fee_id, IFNULL(l3.name, '') team_name, IFNULL(l4.name, '') course_fee
        FROM j_payment
        INNER JOIN contacts l1 ON l1.id = j_payment.parent_id AND j_payment.parent_type = 'Contacts' AND l1.deleted = 0
        LEFT JOIN users l2 ON j_payment.assigned_user_id = l2.id AND l2.deleted = 0
        LEFT JOIN users l7 ON j_payment.user_closed_sale_id = l7.id AND l7.deleted = 0
        LEFT JOIN users l8 ON j_payment.user_pt_demo_id = l8.id AND l8.deleted = 0
        INNER JOIN teams l3 ON j_payment.team_id = l3.id AND l3.deleted = 0
        LEFT JOIN j_coursefee_j_payment_1_c l4_1 ON j_payment.id = l4_1.j_coursefee_j_payment_1j_payment_idb AND l4_1.deleted = 0
        LEFT JOIN j_coursefee l4 ON l4.id = l4_1.j_coursefee_j_payment_1j_coursefee_ida AND l4.deleted = 0
        WHERE (l1.id = '$parent_id') $qTeam AND j_payment.deleted = 0
        ORDER BY payment_date ASC";
        $result = $GLOBALS['db']->query($q3);
        $top_list = array();
        $old_payments = array();

        if (!empty($record_id)) {
            $old_pmsql = "SELECT DISTINCT
            IFNULL(l2_1.payment_idb, '') primaryid,
            SUM(IFNULL(l2_1.amount, 0)) amount,
            SUM(IFNULL(l2_1.hours, 0)) hours
            FROM j_payment LEFT JOIN j_payment_j_payment_1_c l2_1 ON j_payment.id = l2_1.payment_ida AND l2_1.deleted = 0
            INNER JOIN j_payment l5 ON l5.id = l2_1.payment_ida AND l5.deleted = 0
            WHERE (j_payment.id IN ('$record_id')) AND j_payment.deleted = 0
            GROUP BY j_payment.id";
            $re_opm = $GLOBALS['db']->query($old_pmsql);
            while ($o_pm = $GLOBALS['db']->fetchByAssoc($re_opm)) {
                $old_payments[$o_pm['primaryid']]['used_amount'] += $o_pm['amount'];
                $old_payments[$o_pm['primaryid']]['used_hours'] += $o_pm['hours'];
            }
        }
        while ($row = $GLOBALS['db']->fetchByAssoc($result)) {
            //get payment in student info
            if (in_array($row['payment_type'], ['Cashholder', 'Deposit', 'Book/Gift']))
                $left_list[$row['payment_id']] = $row['payment_type'] . ': ' . $row['payment_code'] . ' - ' . format_number($row['payment_amount']) . (($current_user->team_id != $row['team_id']) ? ("({$row['team_name']})") : '') . ((!empty($row['description'])) ? ('<br> --' . $row['description']) : '');


            $is_old_payment = false;
            if (!empty($old_payments[$row['payment_id']])) {
                $row['remain_amount'] = $row['remain_amount'] + $old_payments[$row['payment_id']]['used_amount'];
                $row['remain_hours'] = $row['remain_hours'] + $old_payments[$row['payment_id']]['used_hours'];
                $is_old_payment = true;
            }
            // get payment in top list
            if ($record_id != $row['payment_id']) {
                if (($row['remain_amount'] > 0) && (!in_array($row['payment_type'], array('Enrollment', 'Refund', 'Placement Test', 'Transfer Fee', 'Delay Fee', 'Other', 'Book/Gift'))) && ($current_team_id == $row['team_id'])) {
                    //Payment Amount = 0 can not moving/transfer/refund
                    if (in_array($payment_type, ['Refund', 'Moving Out', 'Transfer Out']) && $row['remain_amount'] <= 0 && $row['remain_hours'] > 0)
                        continue;
                    $top_list[$row['payment_id']]['payment_id'] = $row['payment_id'];
                    $top_list[$row['payment_id']]['used_discount'] = $selectedDiscount;
                    $top_list[$row['payment_id']]['payment_code'] = $row['payment_code'];
                    $top_list[$row['payment_id']]['payment_type'] = $row['payment_type'];
                    $top_list[$row['payment_id']]['payment_date'] = $timedate->to_display_date($row['payment_date']);
                    $top_list[$row['payment_id']]['payment_expired'] = $timedate->to_display_date($row['payment_expired']);

                    $top_list[$row['payment_id']]['is_expired'] = false;
                    if (!array_key_exists($row['payment_id'], $old_payments["deposit_list"]) && !array_key_exists($row['payment_id'], $old_payments["paid_list"])) {
                        if ($row['payment_expired'] < $timedate->convertToDBDate($payment_date))
                            $top_list[$row['payment_id']]['is_expired'] = true;
                    }

                    $top_list[$row['payment_id']]['payment_amount'] = $row['payment_amount'];
                    $top_list[$row['payment_id']]['total_hours'] = $row['total_hours'];
                    $top_list[$row['payment_id']]['remain_amount'] = $row['remain_amount'];
                    $top_list[$row['payment_id']]['remain_hours'] = $row['remain_hours'];
                    $top_list[$row['payment_id']]['use_type'] = $row['use_type'];
                    $top_list[$row['payment_id']]['course_fee'] = $row['course_fee'];
                    $top_list[$row['payment_id']]['course_fee_id'] = $row['course_fee_id'];
                    $top_list[$row['payment_id']]['assigned_user_id'] = $row['assigned_user_id'];
                    $top_list[$row['payment_id']]['assigned_user_name'] = $row['assigned_user_name'];

                    $top_list[$row['payment_id']]['closed_sale_user_id'] = $row['closed_sale_user_id'];
                    $top_list[$row['payment_id']]['closed_sale_user_name'] = $row['closed_sale_user_name'];

                    $top_list[$row['payment_id']]['pt_demo_user_id'] = $row['pt_demo_user_id'];
                    $top_list[$row['payment_id']]['pt_demo_user_name'] = $row['pt_demo_user_name'];

                    $top_list[$row['payment_id']]['team_id'] = $row['team_id'];
                    $top_list[$row['payment_id']]['team_name'] = $row['team_name'];
                    $top_list[$row['payment_id']]['checked'] = $is_old_payment ? " checked" : "";
                }
            }
        }

        //Get loyalty Point
        $loyalty = getLoyaltyPoint($info['id'], $record_id);
        $year = date('Y', strtotime($timedate->convertToDBDate($payment_date)));
        $loyalty_rate = getLoyaltyRateOut($loyalty['level'], $current_team_id, $year);
        //END: Get loyalty Point
    }


    //Reload book gift
    $html_tpl = '';
    if (!empty($current_team_id) && empty($record_id)) {
        //Loat danh sách sách còn lại trong kho
        require_once('custom/modules/J_Payment/views/view.edit.php');
        //Lấy danh sách Book/Gift Default
        //Chỉ load ở màn hình tạo mới
        $book_list = getBookList($current_team_id);

        if (!empty($_POST['primary_id'])) {
            $paymentC = BeanFactory::getBean("J_Payment", $_POST['primary_id']);
            if ($paymentC->load_relationship('j_coursefee_j_payment_2'))
                $courseFeeIDs = array_keys($paymentC->j_coursefee_j_payment_2->getBeans());
            $bookDefault = getBookListDefaultByCourse($courseFeeIDs);
        }


        //Book Manage
        $html_tpl .= getHtmlAddRow($book_list, '', '', '1', '', '', true);
        if (!empty($bookDefault)) {
            foreach ($bookDefault as $df)
                $html_tpl .= getHtmlAddRow($bookDefault, $df['primaryid'], $df['unit'], $df['quantity'], format_number($df['list_price']), format_number($df['amount']), false);
        } else
            $html_tpl .= getHtmlAddRow($book_list, '', '', '1', '', '', false);
    }

    $content = json_encode(array(
        'success' => '1',
        'info' => $info,
        'loyalty_points' => $loyalty['points'],
        'mem_code' => $loyalty['code'],
        'mem_level' => $loyalty['level'],
        'net_amount' => $loyalty['net_amount'],
        'mem_level_html' => $mem_level_html,
        'loyalty_rate_out_value' => $loyalty_rate['value'],
        'left_list' => $left_list,
        'top_list' => $top_list,
    ));
    return json_encode(array(
        "success" => "1",
        "content" => $content,
        'html_book' => $html_tpl,
        'team_id' => $crTeam->id,
        'team_name' => $crTeam->name,
    ));
}


function autoGetNextInvoice($team_id = '', $payment_id = '', $pmdt_id = '', $extPaymentId = '')
{
    $_helper = new HelperPayment();
    global $timedate;
    // Get Invoice Config
    $payment = BeanFactory::getBean('J_Payment', $payment_id, array('disable_row_level_security' => true));
    $evat_id = $GLOBALS['db']->getOne("SELECT id FROM j_configinvoiceno WHERE team_id = '{$payment->team_id}' AND deleted = 0 AND active = 1");
    if (empty($evat_id))
        return json_encode(array("success" => 0));

    $evat = BeanFactory::getBean('J_ConfigInvoiceNo', $evat_id, array('disable_row_level_security' => true));

    $exportType = 'Payment';
    $admin = new Administration();
    $admin->retrieveSettings();
    if (!empty($admin->settings['einvoice_setting_export_einvoice_type'])) $exportType = $admin->settings['einvoice_setting_export_einvoice_type'];

    //Assgin param
    $evat->exportType = $exportType;
    $evat->buyer_legal_type = '';
    $evat->invoice_date = $timedate->nowDate();
    $evat->preview = true;
    //Get Invoice Detail
    $res_content = $_helper->generateInvContent($evat, $payment, $pmdt_id, $extPaymentId);

    if (!$res_content["success"])
        return json_encode(array(
            "success" => 0,
            "label" => $res_content["label"],
        ));
    $invoice  = $res_content["content"]['invoice_detail_infor'];
    $customer = $res_content["content"]["customer_infor"];
    $listItem = $invoice['InvoiceDetail'];

    $htmlRow = '<tr>
    <th width="5%" style="font-weight: normal;">STT<br>(No.)</th>
    <th width="40%" style="font-weight: normal;">Tên hàng hóa, dịch vụ<br>(Description)</th>
    <th width="12.5%" style="font-weight: normal;">ĐVT<br>(Unit)</th>
    <th width="12.5%" style="font-weight: normal;">Số lượng<br>(Quantity)</th>
    <th width="15%" style="font-weight: normal;">Đơn giá<br>(Unit price)</th>
    <th width="15%" style="font-weight: normal;">Thành tiền<br>(Amount)</th>
    </tr>';
    foreach ($listItem as $item) {
        $htmlRow .= '<tr class="item">
        <td class="no" style="text-align: center;">' . $item['LineNumber'] . '</td>
        <td class="item_name">' . $item['ItemName'] . '</td>
        <td class="unit" style="text-align: center;">' . $item['UnitName'] . '</td>
        <td class=quantity" style="text-align: right;">' . $item['Quantity'] . '</td>
        <td class="unit_price" style="text-align: right;">' . format_number($item['UnitPrice'], 1, 1) . '</td>
        <td class="amount" style="text-align: right;">' . format_number($item['Amount']) . '</td>
        </tr>';
    }
    //LINE
    $htmlRow .= '<tr class="item"><td></td></tr>';
    $htmlRow .= '<tr class="item"><td colspan=6 style="border-bottom: 1px solid #aeaeae; "></td></tr>';
    $htmlRow .= '<tr class="item"><td></td></tr>';
    $htmlRow .= '<tr class="item">
    <td colspan=2></td>
    <td colspan=3>Cộng tiền hàng (Total amount): </td>
    <td style="text-align: right;"><b>' . format_number($invoice['Invoice']['TotalAmountWithoutVAT']) . '</b></td>
    </tr>';

    $htmlRow .= '<tr class="item">
    <td colspan=2>Thuế suất GTGT (VAT rate): &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>' . $invoice['Invoice']['VATRateName'] . '</b></td>
    <td colspan=3>Tiền thuế GTGT (VAT amount): </td>
    <td style="text-align: right;"><b>' . format_number($invoice['Invoice']['TotalVATAmount']) . '</b></td>
    </tr>';
    $htmlRow .= '<tr class="item">
    <td colspan=2></td>
    <td colspan=3>Tổng tiền thanh toán (Total amount): </td>
    <td style="text-align: right;"><b>' . format_number($invoice['Invoice']['TotalAmountWithVAT']) . '</b></td>
    </tr>';

    return json_encode(array(
        "success" => "1",
        "template_code"=>$evat->pattern,
        "serial" => $evat->serial_no,
        "status" => $evat->active,
        "fullStudentName" => $customer['FullStudentName'],
        "guardianName" => $customer['GuardianName'],
        "guardianName2" => $customer['GuardianName2'],
        "BuyerNameNull" => $customer['BuyerNameNull'],
        "BuyerDisplayName" => $customer['CompanyName'],
        "buyerAddressLine" => $customer['BuyerAddress'],
        "BuyerTaxCode" => $customer['BuyerTaxCode'],
        "buyerEmail" => $customer['BuyerEmail'],
        "is_corporate" => $invoice['Invoice']['Is_corporate'],
        "displayID" => $customer['CompanyId'],
        "buyerID" => $customer['StudentId'],
        "htmlRow" => $htmlRow,
        "exportType" => $evat->exportType,
        "supplier" => $evat->supplier,
    ));
}

function addEvatCorporate($payment_id = '', $account_id = '', $action = '')
{
    if (!empty($payment_id)) {
        $payment = BeanFactory::getBean('J_Payment', $payment_id);
        if ($action == 'add') {
            $payment->is_corporate = '1';
            $payment->account_id = $account_id;
            $payment->save();
        } elseif ($action == 'delete') {
            $payment->is_corporate = '0';
            $payment->account_id = '';
            $payment->save();
        }
        return json_encode(array(
            "success" => "1",
        ));
    } else {
        return json_encode(array(
            "success" => "0",
        ));
    }
}

function ajaxUpdatePaymentDetail($payment_detail_id = '', $payment_amount = '', $payment_method = '', $payment_date = '', $handle_action = '')
{
    global $timedate;
    if (empty($payment_detail_id))
        return json_encode(array(
            "success" => "0",
            "errorLabel" => translate('LBL_ERR_RECEIPT1', 'J_PaymentDetail'),
        ));
    //check void e-invoice
    if (!empty($pmd->invoice_id)) {
        return json_encode(array(
            "success" => "0",
            "errorLabel" => translate('LBL_ERR_RECEIPT2', 'J_PaymentDetail'),
        ));
    }
    $pmd = BeanFactory::getBean('J_PaymentDetail', $payment_detail_id);

    $payment_date = $timedate->convertToDBDate($payment_date);
    //TH thay đổi Amount - Xóa tạo lại Unpaid
    if ($payment_amount != $pmd->payment_amount) {
        $diffAmount = $pmd->payment_amount - $payment_amount;

        $pmd->payment_amount = $payment_amount;
        $pmd->before_discount = $payment_amount + $pmd->discount_amount + $pmd->sponsor_amount + $pmd->loyalty_amount;

        //Cộng tiền Difff
        $q2 = "SELECT DISTINCT
        IFNULL(id, '') primaryId,
        IFNULL(payment_amount, '0') payment_amount,
        IFNULL(before_discount, '0') before_discount,
        IFNULL(discount_amount, '0') discount_amount,
        IFNULL(sponsor_amount, '0') sponsor_amount,
        IFNULL(loyalty_amount, '0') loyalty_amount,
        IFNULL(payment_date, '') payment_date,
        IFNULL(serial_no, '') serial_no,
        IFNULL(numeric_vat_no, '0') numeric_vat_no,
        IFNULL(invoice_id, '') invoice_id,
        IFNULL(is_discount, '0') is_discount
        FROM j_paymentdetail
        WHERE payment_id = '{$pmd->payment_id}' AND id <> '$payment_detail_id' AND deleted = 0 AND status = 'Unpaid'
        ORDER BY payment_no";
        $res2 = $GLOBALS['db']->query($q2);

        while ($rowPayDtl = $GLOBALS['db']->fetchByAssoc($res2)) {
            $newP = BeanFactory::getBean('J_PaymentDetail', $rowPayDtl['primaryId']);
            $newP->payment_amount += $diffAmount;
            if ($newP->payment_amount > 0) {
                $newP->before_discount = $newP->payment_amount + $newP->discount_amount + $newP->sponsor_amount + $newP->loyalty_amount;
                $newP->save();
                $diffAmount = 0;
                break;
            }

            if ($newP->payment_amount <= 0) {
                $newP->mark_deleted($newP->id);
                $diffAmount = $newP->payment_amount;
            }
        }
        if ($diffAmount > 0) { // Create Payment Unpaid
            //$res2 = $GLOBALS['db']->query($q2);
            $pmdn = BeanFactory::newBean('J_PaymentDetail');
            foreach ($pmdn->field_defs as $keyField => $aFieldName)
                $pmdn->$keyField = $pmd->$keyField;

            $payNo = $GLOBALS['db']->getOne("SELECT DISTINCT IFNULL(jsd.payment_no, '') payment_no
                FROM j_paymentdetail jsd INNER JOIN j_payment l1 ON jsd.payment_id = l1.id AND l1.deleted = 0
                WHERE (l1.id = '{$pmd->payment_id}') AND (jsd.status <> 'Cancelled') AND jsd.deleted = 0
                ORDER BY jsd.id DESC LIMIT 1");
            $pmdn->payment_no = $payNo+1;
            $pmdn->id           = '';
            $pmdn->name         = '-none-';
            $pmdn->payment_date = $payment_date;
            $pmdn->expired_date = $pmd->expired_date;
            $pmdn->is_discount  = 0;
            $pmdn->discount_amount = 0;
            $pmdn->sponsor_amount  = 0;
            $pmdn->loyalty_amount  = 0;
            //Reset Audit Amount
            $pmdn->audited_amount  = null;
            $pmdn->reconcile_status  = '';
            $pmdn->payment_method  = $pmdn->bank_name = $pmdn->bank_account =  '';
            $pmdn->pos_code = $pmdn->card_type = '';

            $pmdn->before_discount = format_number($diffAmount);
            $pmdn->payment_amount = format_number($diffAmount);

            $pmdn->status = 'Unpaid';
            $pmdn->type = 'Normal';
            if (unformat_number($pmdn->payment_amount) > 0)
                $pmdn->save();
        }
    }
    $pmd->payment_amount = format_number(unformat_number($payment_amount));
    $pmd->payment_date = $payment_date;
    $pmd->payment_method = $payment_method;
    $pmd->method_note    = $_POST['method_note'];
    $pmd->card_type      = $_POST['card_type'];
    $pmd->bank_account   = $_POST['bank_account'];
    $pmd->bank_name      = $_POST['bank_type'];
    $pmd->pos_code = strtoupper($_POST['pos_code']);
    $pmd->inv_code = strtoupper($_POST['inv_code']);
    $pmd->reference_document = $_POST['reference_document'];
    $pmd->reference_number = $_POST['reference_number'];

    $pmd->description = $_POST['description'];
    $pmd->note = $_POST['note'];

    //Check thông tin đối soát
    $pmd->reconcile_status = reconcileState(unformat_number($pmd->audited_amount),unformat_number($pmd->payment_amount) );
    if($handle_action == 'pay' || ($pmd->audited_amount >= $pmd->payment_amount && $pmd->status == 'Unpaid')){
        $pmd->assigned_user_id = $GLOBALS['current_user']->id;
        $pmd->status = 'Paid';
    }
    $pmd->save();

    //Tính tổng số tiền đã trả
    $q2 = "SELECT DISTINCT
    IFNULL(pmd.id, '') primaryid, IFNULL(pmd.name, '') receipt_id,
    IFNULL(pmd.status, '') status, pmd.payment_amount payment_amount
    FROM j_paymentdetail pmd INNER JOIN j_payment l1 ON pmd.payment_id = l1.id AND l1.deleted = 0
    WHERE (l1.id = '{$pmd->payment_id}') AND pmd.status <> 'Cancelled' AND pmd.deleted = 0";
    $res2 = $GLOBALS['db']->query($q2);
    $paidAmount = $unpaidAmount = $countpmd = 0;
    while ($rowPayDtl = $GLOBALS['db']->fetchByAssoc($res2)) {
        if ($rowPayDtl['status'] == 'Unpaid') $unpaidAmount += $rowPayDtl['payment_amount'];
        else $paidAmount += $rowPayDtl['payment_amount'];
        $countpmd++;
    }
    $GLOBALS['db']->query("UPDATE j_payment SET number_of_payment = $countpmd WHERE id = '{$pmd->payment_id}'");
    $enr_success[] = translate('LBL_SUCCESS_SAVED', 'J_Payment');

    //check sale type receipt
    $payment = BeanFactory::getBean('J_Payment', $pmd->payment_id, array('use_cache' => false));
    $sale_type = $payment->sale_type;
    $sale_type_date = $payment->sale_type_date;
    //check auto-enroll
    //Process Auto-Enroll
    if($payment->parent_type == 'Contacts' && $handle_action == 'pay'){
        if ($payment->is_auto_enroll && $payment->payment_type == 'Cashholder') {
            $payment->pmd_id = $pmd->id; //cache start_study/end_study
            $class = BeanFactory::getBean('J_Class', $payment->ju_class_id, array('use_cache' => false));
            $res = processAutoEnroll($payment, $class);
            if ($res['success']) $enr_success[] = $res['alert'];
            else $enr_fail[] = $res['alert'];
        }
        updateStudentStatus($payment->parent_id);//Update Student Status
    }


    return json_encode(array(
        "success" => "1",
        "paid" => format_number($paidAmount),
        "unpaid" => format_number($unpaidAmount),
        "remain_amount" => format_number($payment->remain_amount),
        "remain_hours" => format_number($payment->remain_hours, 2, 2),
        "enr_fail" => $enr_fail,
        "enr_success" => $enr_success,
        "sale_type" => $sale_type,
        "sale_type_date" => $timedate->to_display_date($sale_type_date, false),
    ));

}

//Auto-load List Receipt Paid
function ajaxListPaidList($payment_id = ''){
    if(!empty($payment_id)){
        $q1 = "SELECT DISTINCT
        IFNULL(pmd.id, '') primaryid, IFNULL(pmd.name, '') receipt_id,
        IFNULL(pmd.status, '') status, pmd.payment_amount payment_amount,
        IFNULL(pmd.audited_amount, '') audited_amount, pmd.payment_date payment_date,
        l1.remain_amount remain_amount, l1.remain_hours remain_hours
        FROM j_paymentdetail pmd INNER JOIN j_payment l1 ON pmd.payment_id = l1.id AND l1.deleted = 0
        WHERE (l1.id = '$payment_id') AND pmd.status <> 'Cancelled' AND pmd.deleted = 0";
        $rs1 = $GLOBALS['db']->query($q1);
        $rcList = [];
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
            $rcList[$row['primaryid']]['id']             = $row['primaryid'];
            $rcList[$row['primaryid']]['status']         = $row['status'];
            $rcList[$row['primaryid']]['payment_amount'] = $row['payment_amount'];
            $rcList[$row['primaryid']]['audited_amount'] = $row['audited_amount'];
            $rcList[$row['primaryid']]['payment_date']   = $row['payment_date'];
            $rcList[$row['primaryid']]['alert']          = str_replace('{$receipt}', $row['receipt_id'], translate('LBL_ALERT_RECEIPT_PAID', 'J_PaymentDetail'));

            if($row['status'] == 'Paid') $paid_amount += $row['payment_amount'];
            if($row['status'] == 'Unpaid') $unpaid_amount += $row['payment_amount'];
            $remain_amount  = $row['remain_amount'];
            $remain_hours   = $row['remain_hours'];
        }
        if(empty($rcList)) json_encode(array('success' => 0));
        return json_encode(array(
            "success"       => 1,
            "rcList"        => $rcList,
            "remain_amount" => format_number($remain_amount),
            "remain_hours"  => format_number($remain_hours,2,2),
            "paid_amount"   => format_number($paid_amount),
            "unpaid_amount" => format_number($unpaid_amount),
            "enr_success"   => $enr_success
        ));
    }return json_encode(array('success' => 0));
}



function ajaxUndoPayment($paymentId = '', $paymentType = '')
{
    //Get bean of payemtn out & payment in
    if ($paymentType == "Refund") {
        $paymentOutId = $paymentId;
        $paymentOutBean = BeanFactory::getBean("J_Payment", $paymentId);
    } else {
        if (in_array($paymentType, array("Transfer In", "Moving In"))) {
            $paymentInId = $paymentId;
            $paymentInBean = BeanFactory::getBean("J_Payment", $paymentId);
            $paymentOutId = $paymentInBean->payment_out_id;
            $paymentOutBean = BeanFactory::getBean("J_Payment", $paymentOutId);
        } else { //Transfer out, moving out
            $paymentOutId = $paymentId;
            $paymentOutBean = BeanFactory::getBean("J_Payment", $paymentId);
            $paymentOutBean->load_relationship("ju_payment_in");
            $paymentInBean = reset($paymentOutBean->ju_payment_in->getBeans());
            $paymentInId = $paymentInBean->id;
        }
    }

    $fromStudentId = $paymentOutBean->parent_id;

    //TODO - check remain amount of payment in, if remain != payment amount -> die();
    if ($paymentInBean->remain_amount != $paymentInBean->payment_amount) {
        return json_encode(array(
            "success" => "0",
        ));
    }

    //TODO - restore ralated payment
    $sqlRelatedPay = "SELECT DISTINCT payment_idb id, amount, hours
    FROM j_payment_j_payment_1_c
    WHERE payment_ida = '{$paymentOutId}'
    AND deleted = 0";
    $resultRealtedPay = $GLOBALS['db']->query($sqlRelatedPay);
    while ($rowRelatedPay = $GLOBALS['db']->fetchByAssoc($resultRealtedPay)) {
        $pay_id = $rowRelatedPay["id"];
        $hours = $rowRelatedPay["hours"];
        $amount = $rowRelatedPay["amount"];

        $payment_drop_id = $pay_id;
        // Cộng giờ và tiền revenue

        $sqlDelRevenue = "SELECT amount, duration FROM c_deliveryrevenue WHERE ju_payment_id = '$payment_drop_id' AND deleted = 0";
        $rs_re = $GLOBALS['db']->query($sqlDelRevenue);
        while ($row_re = $GLOBALS['db']->fetchByAssoc($rs_re)) {
            $hours += $row_re['duration'];
            $amount += $row_re['amount'];
        }
        $hours = unformat_number(format_number($hours, 2, 2));
        $amount = unformat_number(format_number($amount));
        $sqlUpdatePay = "UPDATE j_payment
        SET
        remain_amount   = remain_amount + $amount,
        remain_hours    = remain_hours + $hours
        WHERE id = '$pay_id'
        AND deleted = 0";

        $GLOBALS['db']->query($sqlUpdatePay);

        //UNDO REFUND: delete revenue record

        $sqlDelRevenue = "UPDATE c_deliveryrevenue SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}'
        WHERE ju_payment_id = '{$payment_drop_id}'
        AND deleted = 0 AND passed = 0";
        $GLOBALS['db']->query($sqlDelRevenue);
    }

    //Delete relationship payment - related payment record
    removeRelatedPayment($paymentOutId);

    //Delete payment out & payment in  , Remove Relationship Contact - Payment
    $sqlDelPayment = "UPDATE j_payment SET deleted = 1 WHERE id = '{$paymentOutId}'";
    $GLOBALS['db']->query($sqlDelPayment);

    if (!empty($paymentInId)) {
        $sqlDelPayment_2 = "UPDATE j_payment SET deleted = 1 WHERE id = '{$paymentInId}'";
        $GLOBALS['db']->query($sqlDelPayment_2);
    }

    //UNDO MOVING: Set primary team for student
    if (in_array($paymentType, array("Moving Out", "Moving In"))) {
        $studentBean = BeanFactory::getBean("Contacts", $fromStudentId);
        $studentBean->team_id = $paymentOutBean->team_id;
        $studentBean->save();
    }

    updateStudentStatus($fromStudentId);

    return json_encode(array(
        "success" => "1",
    ));
}

function finish_printing($printing_id = '')
{
    if (!empty($printing_id)) {
        //Update finish finish printing
        $GLOBALS['db']->query("UPDATE j_configinvoiceno SET finish_printing = 1, pmd_id_printing='' WHERE deleted = 0 AND id= '$printing_id'");

        return json_encode(array(
            "success" => "1",
        ));
    } else
        return json_encode(array(
            "success" => "0",
        ));

}

function caculateDropPayment($payment_id, $dl_date = '')
{
    //Tính toán lại số dư trước để hạn chế lỗi
    if (empty($payment_id)) return json_encode(array("success" => 0));

    getPaymentRemain($payment_id);
    $payment = BeanFactory::getBean('J_Payment', $payment_id, array('use_cache' => false));

    //TH import dữ liệu cũ lấy số dư từ old_remain_amount
    if (!empty($payment->old_student_id)) {
        $total_amount = $payment->old_remain_amount;
        $price = ($payment->old_remain_amount / $payment->total_hours);
        if ($payment->total_hours <= 0) {
            $price = $drop_hour = 0;
        } else {
            $price = ($payment->old_remain_amount / $payment->total_hours);
            $drop_hour = $payment->remain_amount / $price;
        }
    } else {
        // Get Total amount
        if (in_array($payment->payment_type, ['Cashholder', 'Deposit'])) {
            $row1 = get_list_payment_detail($payment_id);
            for ($i = 0; $i < count($row1); $i++) $total_amount += $row1[$i]['payment_amount'];
            $total_amount += $payment->paid_amount + $payment->deposit_amount;
        }
        if (in_array($payment->payment_type, ['Delay', 'Transfer In', 'Moving In'])) $total_amount = $payment->payment_amount;
        if ($payment->total_hours <= 0) {
            $price = $drop_hour = 0;
        } else {
            $price = (($payment->payment_amount + $payment->paid_amount + $payment->deposit_amount) / ($payment->total_hours));
            $drop_hour = (($payment->remain_amount) / $price);
        }

    }
    if ($total_amount < 0 || $payment->remain_amount <= 0) return json_encode(array("success" => 0));

    return json_encode(array(
        "success" => 1,
        "total_amount" => $total_amount,
        "used_amount" => $total_amount - $payment->remain_amount,
        "drop_amount" => ($payment->remain_amount),
        "drop_hour" => $drop_hour,
        "price" => $price,
    ));
}

function createDropPayment()
{
    global $current_user, $timedate;
    $res = json_decode(caculateDropPayment($_POST['payment_id']), true);
    if (!$res['success']) return json_encode(array("success" => 0));
    $payment = BeanFactory::getBean('J_Payment', $_POST['payment_id']);

    if ($_POST['drop_type'] == 'drop_to_delay') {
        //Payment Delay
        $pm_delay = new J_Payment();
        $pm_delay->id = create_guid();
        $pm_delay->new_with_id = true;
        $pm_delay->parent_id = $payment->parent_id;
        $pm_delay->parent_type = 'Contacts';
        $pm_delay->payment_type = 'Delay';
        $pm_delay->use_type = 'Amount';

        $pm_delay->payment_date = $timedate->convertToDBDate($_POST['dl_date']);
        $pm_delay->payment_expired = date('Y-m-d', strtotime("+6 months " . $pm_delay->payment_date, false));
        $pm_delay->payment_amount = format_number($res['drop_amount']);
        $pm_delay->remain_amount = format_number($res['drop_amount']);
        $pm_delay->tuition_hours = format_number($res['drop_hour'], 2, 2);
        $pm_delay->total_hours = format_number($res['drop_hour'], 2, 2);
        $pm_delay->remain_hours = 0; //fIX SO GIO rEMAIN
        $pm_delay->description = $_POST['dl_reason'];
        $pm_delay->assigned_user_id = $current_user->id;
        $pm_delay->team_id = $payment->team_id;
        $pm_delay->team_set_id = $payment->team_id;

        //Tạo quan hệ để Để update remain
        addRelatedPayment($pm_delay->id, $payment->id, $pm_delay->payment_date, $_POST['drop_amount'], $_POST['drop_hour']);
        $pm_delay->save();

    } elseif ($_POST['drop_type'] == 'drop_to_revenue') {
        $delivery = new C_DeliveryRevenue();
        $delivery->name = 'Drop revenue from payment ' . $payment->name;
        $delivery->student_id = $payment->parent_id;
        //Get Payment ID
        $delivery->ju_payment_id = $payment->id;
        $delivery->amount = format_number($res['drop_amount']);
        $delivery->duration = format_number($res['drop_hour'], 2, 2);
        $delivery->date_input = $timedate->convertToDBDate($_POST['dl_date'], false);
        $delivery->description = $_POST['dl_reason'];
        $delivery->team_id = $payment->team_id;
        $delivery->team_set_id = $payment->team_id;
        $delivery->cost_per_hour = $unit_price;
        $delivery->assigned_user_id = $current_user->id;
        $delivery->revenue_type = 'Drop';
        $delivery->save();

    }
    getPaymentRemain($payment->id);
    $GLOBALS['db']->query("UPDATE j_payment SET remain_hours = 0, date_modified='{$GLOBALS['timedate']->nowDb()}',  modified_user_id='{$GLOBALS['current_user']->id}' WHERE id = '{$payment->id}'");
    return json_encode(array(
        "success" => "1",
    ));
}

function ajaxConvertPayment()
{
    global $timedate, $current_user;
    $tuition_hours = unformat_number($_POST['tuition_hours']);
    $remain_hours = unformat_number($_POST['remain_hours']);
    $convert_to_type = $_POST['convert_to_type'];
    $payment_id = $_POST['payment_id'];
    $payment = BeanFactory::getBean('J_Payment', $payment_id);
    if (!empty($payment_id) || empty($payment)) {
        $new_payment_type = $payment->payment_type;
        if ($convert_to_type == 'To Hour') {
            if ($payment->payment_type == 'Deposit')
                $new_payment_type = 'Cashholder';

            $GLOBALS['db']->query("UPDATE j_payment SET tuition_hours=$tuition_hours, total_hours=$tuition_hours, remain_hours=$remain_hours, use_type='Hour', payment_type='$new_payment_type', note='{$payment->payment_type} has been Converted to $new_payment_type - " . $timedate->now() . " by {$current_user->user_name}' WHERE id='$payment_id'");
        } else {
            if ($payment->payment_type == 'Cashholder')
                $new_payment_type = 'Deposit';
            if ((($new_payment_type == 'Deposit') && ($payment->payment_amount != $payment->remain_amount)) || $payment->use_type == 'Amount')
                return json_encode(array(
                    "success" => "0",
                ));
            $GLOBALS['db']->query("UPDATE j_payment SET tuition_hours=$tuition_hours, total_hours=$tuition_hours, remain_hours=$remain_hours, use_type='Amount', payment_type='$new_payment_type', note='{$payment->payment_type} has been Converted to $new_payment_type - (Old Total Hour - Remain Hour : {$payment->tuition_hours} - {$payment->remain_hours}) " . $timedate->now() . " by {$current_user->user_name}' WHERE id='$payment_id'");
        }

        return json_encode(array(
            "success" => "1",
        ));

    } else
        return json_encode(array(
            "success" => "0",
        ));
}

// ------------------------------------NEW--------------------------------------------------\\
function ajaxCheckVoucherCode($code, $team_id)
{
    global $timedate, $app_list_strings;
    $today = $timedate->nowDbDate();

    if (!empty($code)) {
        $admin = new Administration();
        $admin->retrieveSettings();
        $sponsor_type = empty($admin->settings['sponsor_setting_sponsor_type']) ? [] : $admin->settings['sponsor_setting_sponsor_type'];
        if ($sponsor_type == [] || in_array('default', $sponsor_type)) {
            $setReferral = $app_list_strings['default_loyalty_rate']['Referral Program'];
            $referralPoint = $app_list_strings['default_loyalty_rate']['Referral Point'];
            if ($setReferral == 'Yes' || $setReferral == '1') {
                $ref_query = "UNION DISTINCT
                (SELECT DISTINCT
                IFNULL(contacts.id, '') voucher_id,
                IFNULL(contacts.contact_id, '') sponsor_number,
                'before_discount' priority,
                'Referral' foc_type,
                'Available' status,
                IFNULL(contacts.full_student_name, '') student_name,
                'Loyalty' type,
                'N' use_time,
                '' voucher_used_time,
                0 discount_amount,
                0 discount_percent,
                $referralPoint loyalty_points,
                'Phụ huynh giới thiệu bạn thành công - Tặng $referralPoint điểm tích lũy' description,
                'N/A' start_date,
                'N/A' end_date,
                COUNT(l2.id) used_time
                FROM contacts
                LEFT JOIN  j_sponsor l2 ON contacts.contact_id=l2.sponsor_number AND l2.deleted=0
                WHERE (contacts.contact_id = '$code') AND (contacts.id <> '{$_POST['student_id']}') AND contacts.deleted = 0
                GROUP BY sponsor_number)";
            }
            $q1 = "(SELECT DISTINCT
            IFNULL(j_voucher.id, '') voucher_id,
            IFNULL(j_voucher.name, '') sponsor_number,
            IFNULL(j_voucher.priority, '') priority,
            IFNULL(j_voucher.foc_type, '') foc_type,
            IFNULL(j_voucher.status, '') status,
            '' student_name,
            'Sponsor' type,
            IFNULL(j_voucher.use_time, '') use_time,
            j_voucher.used_time voucher_used_time,
            IFNULL(j_voucher.discount_amount, 0) discount_amount,
            IFNULL(j_voucher.discount_percent, 0) discount_percent,
            0 loyalty_points,
            j_voucher.description description,
            j_voucher.start_date start_date,
            j_voucher.end_date end_date,
            COUNT(l2.id) used_time
            FROM j_voucher
            LEFT JOIN  j_sponsor l2 ON j_voucher.id=l2.voucher_id AND l2.deleted=0
            WHERE (j_voucher.name = '$code') AND (j_voucher.team_set_id IN (SELECT DISTINCT tst.team_set_id team_set_id FROM team_sets_teams tst WHERE tst.team_id = '$team_id'
            AND tst.deleted = 0) OR j_voucher.team_id ='1')  AND (j_voucher.end_date >= '$today') AND j_voucher.deleted = 0
            GROUP BY j_voucher.id)
            $ref_query";
            $rs1 = $GLOBALS['db']->query($q1);
            $row = $GLOBALS['db']->fetchByAssoc($rs1);

            if (!empty($row)) {

                if (!empty($row['student_name']))
                    $row['student_name'] = '<a  href="#bwc/index.php?module=Contacts&action=DetailView&record=' . $row['student_id'] . '">' . $row['student_name'] . '</a>';

                $status = $row['status'];
                if ($row['use_time'] != 'N' && $row['used_time'] >= $row['use_time'])
                    $status = 'Expired';

                if (($row['end_date'] != 'N/A') && !empty($row['end_date']) && ($today > $row['end_date']))
                    $status = 'Expired';

                if ($status != $row['status'] && $row['type'] == 'Sponsor') {
                    $row['status'] = $status;
                    $GLOBALS['db']->query("UPDATE j_voucher SET status='$status' WHERE id = '{$row['voucher_id']}'");
                }

                if ($row['status'] == 'Expired' || $row['status'] == 'Inactive') $color = 'red';
                else $color = 'green';


                //Update Used !
                if ($row['voucher_used_time'] != $row['used_time'] && $row['type'] == 'Sponsor')
                    $GLOBALS['db']->query("UPDATE j_voucher SET used_time={$row['used_time']} WHERE id = '{$row['voucher_id']}'");

                return json_encode(array(
                    "success" => "1",
                    "voucher_id" => $row['voucher_id'],
                    "voucher_code" => $row['voucher_code'],
                    "priority" => $row['priority'],
                    "sponsor_number" => $row['sponsor_number'],
                    "loyalty_points" => $row['loyalty_points'],
                    "foc_type" => $row['foc_type'],
                    "status_color" => '<br>Status: <b style="color:' . $color . ';"> ' . $app_list_strings['voucher_status_dom'][$row['status']] . '</b>',
                    "status" => $row['status'],
                    "type" => $row['type'],
                    "student_name" => $row['student_name'],
                    "used_time" => $row['used_time'] . ' / ' . $row['use_time'],
                    "discount_amount" => format_number($row['discount_amount']),
                    "discount_percent" => format_number($row['discount_percent'], 2, 2),
                    "description" => $row['description'],
                    "start_date" => $row['start_date'],
                    "end_date" => $row['end_date'],
                ));
            }
        }

        //TAPTAP
        if (in_array('TAPTAP Sponsor', $sponsor_type)) {
            //get center code
            $qGetCenterCode = "SELECT code_prefix FROM teams WHERE id = '{$team_id}'";
            $center_code = $GLOBALS['db']->getOne($qGetCenterCode);

            $qGetPhone = "SELECT IFNULL(phone_mobile, '') phone_mobile
            FROM contacts WHERE id='{$_POST['student_id']}'";
            $phone = $GLOBALS['db']->getOne($qGetPhone);
            $phone_mobile = $phone;
            if ($phone[0] == '0') {
                $phone_mobile = "84" . ltrim($phone, "0");
            }
            $status = 'Activated';
            $color = 'green';

            require_once "custom/include/_helper/connectTapTapApi.php";
            $tapTap = new TapTapHelper();
            $responeToken = $tapTap->requestToken();
            if ($responeToken['success']) {
                $responeValidateVoucher = $tapTap->validateVoucher($code, $phone_mobile, $center_code);
                if ($responeValidateVoucher['success']) {
                    if (empty($responeValidateVoucher['data']['valid_till']) && empty($responeValidateVoucher['data']['discount_amount']) && empty($responeValidateVoucher['data']['discount_percent']))
                        $used = 1;
                    else $used = 0;
                    if ($today > $responeValidateVoucher['data']['valid_till']) {
                        $status = 'Expired';
                        $color = 'red';
                    }
                    return json_encode(array(
                        "success" => "1",
                        "voucher_id" => '',
                        "voucher_code" => $responeValidateVoucher['data']['code'],
                        "sponsor_number" => $responeValidateVoucher['data']['code'],
                        "loyalty_points" => 0,
                        "foc_type" => 'TAPTAP Coupon',
                        "status_color" => '<br>Status: <b style="color:' . $color . ';"> ' . $app_list_strings['voucher_status_dom'][$status] . '</b>',
                        "status" => $status,
                        "type" => 'TAPTAP Sponsor',
                        "student_name" => '',
                        "used_time" => $used . '/1',
                        "discount_amount" => format_number($responeValidateVoucher['data']['discount_amount']),
                        "discount_percent" => format_number($responeValidateVoucher['data']['discount_percent'], 2, 2),
                        "description" => $responeValidateVoucher['data']['description'],
                        "start_date" => '',
                        "end_date" => $responeValidateVoucher['data']['valid_till'],
                    ));
                }
            }
        }

    }
    return json_encode(array("success" => "0",));
}

function ajaxCancelInvoice($invoice_id = '', $payment_id = '', $description = ''){
    $_helper = new HelperPayment();
    $invoice = BeanFactory::getBean('J_Invoice', $invoice_id);

    if (empty($invoice->id) || $invoice->status == 'Cancelled') {
        $return = json_encode(array(
            "success" => "0",
            "errorLabel" => "'Something went wrong. Please refresh and try again!!'",
        ));
    }

    //Send API Delete
    if (!empty($invoice->id)) {
        $evat_id = $GLOBALS['db']->getOne("SELECT id FROM j_configinvoiceno WHERE team_id = '{$invoice->team_id}' AND deleted = 0");
        if (!empty($evat_id) && !$invoice->is_manual) {
            //Call API
            $evat = BeanFactory::getBean('J_ConfigInvoiceNo', $evat_id);
            if ($evat->supplier == 'Bkav') {
                // Lay trang thai e-invoice
                $param2 = array(
                    "CmdType" => 801,
                    "CommandObject" => $invoice->invoiceGUID
                );
                $param3 = array(
                    "partnerGUID" => $evat->partnerGUID,
                    "CommandData" => base64_encode(json_encode($param2)),
                );
                $res = $_helper->GeneralEinvoice($evat, json_encode($param3));
                if ($res['Status'] != 0) {
                    $label_error = translate('LBL_CANCEL_VAT_ERR', 'J_Payment') . '</br>';
                    $return = json_encode(
                        array(
                            "success" => "0",
                            "label" => $label_error,
                        )
                    );
                } else {
                    // Xoa HD chua phat hanh hoac huy HD da phat hanh
                    $param2 = array(
                        "CmdType" => 202,
                        "CommandObject" => array(
                            array(
                                "PartnerInvoiceStringID" => $invoice->PartnerInvoiceStringID
                            )
                        )
                    );
                    if ($res['Content'] == 11 || $res['Content'] == 2) {
                        $param3 = array(
                            "partnerGUID" => $evat->partnerGUID,
                            "CommandData" => base64_encode(json_encode($param2)),
                        );
                        $res = $_helper->GeneralEinvoice($evat, json_encode($param3));
                        if ($res['Status'] != 0) {
                            $label_error = translate('LBL_CANCEL_VAT_ERR', 'J_Payment') . '</br>';
                            $return = json_encode(
                                array(
                                    "success" => "0",
                                    "label" => $label_error,
                                )
                            );
                        }
                    }

                }
            } elseif ($evat->supplier == 'Misa') {
                //Get token
                $res = $_helper->get_EvatToken($evat);
                if ($res['success']) {
                    $token = $res['token'];
                    $param['TransactionID'] = $invoice->transaction_id;
                    $param['RefDate'] = date('Y-m-d');
                    $param['RefNo'] = $invoice->name;
                    $param['DeletedReason'] = $description;

                    $param = json_encode($param);

                    $result = $_helper->DeleteInvoice($evat, $param, $token);
                    if ($result['success']) {
                        $license_remove_id = $result['data'];
                    } else
                        $return = json_encode(
                            array(
                                "success" => "0",
                                "errorLabel" => translate('LBL_CANCEL_VAT_ERR', 'J_Payment') . "</br>" . $res['errorCode'],
                        ));
                } else {
                    $label_error = translate('LBL_CANCEL_VAT_ERR', 'J_Payment') . '</br>' . $res['errorCode'];
                    if (!empty($res['errorCodeDetail'])) $label_error = $label_error . ' : ' . $res['errorCodeDetail'];
                    $return = json_encode(
                        array(
                            "success" => "0",
                            "errorLabel" => $label_error,
                    ));
                }
            } elseif ($evat->supplier == 'Viettel') {
                $e = new ViettelEInvoice($evat);
                if (empty($e->getToken())) {
                    $return = json_encode(array(
                        "success" => 0,
                        "errorLabel" => 'Incorrect User/password in config' . '<br>'. translate('LBL_CANCEL_VAT_ERR', 'J_Payment'),
                    ));
                } else {
                    $c = $GLOBALS['db']->getOne("SELECT description FROM j_invoice WHERE id='$invoice_id'");
                    $c = json_decode(html_entity_decode($c), true);
                    $ch = curl_init($evat->host . '/services/einvoiceapplication/api/InvoiceAPI/InvoiceWS/searchInvoiceByTransactionUuid');
                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Content-Type: application/x-www-form-urlencoded",
                        "Cookie: access_token=" . $e->getToken()
                    ));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                        'supplierTaxCode' => $evat->account,
                        'transactionUuid' => $c['generalInvoiceInfo']['transactionUuid']
                    )));
                    $res = curl_exec($ch);
                    $res = json_decode($res, true);
                    if ($res['code'] == 400) {
                        $return = json_encode(array(
                            "success" => 0,
                            "errorLabel" => $res['data'] . '<br>'. translate('LBL_CANCEL_VAT_ERR', 'J_Payment')
                        ));
                    } else {
                        $ch = curl_init($evat->host . '/services/einvoiceapplication/api/InvoiceAPI/InvoiceWS/cancelTransactionInvoice');
                        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            "Content-Type: application/x-www-form-urlencoded",
                            "Cookie: access_token=" . $e->getToken()
                        ));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                            'supplierTaxCode' => $evat->account,
                            'transactionUuid' => $c['generalInvoiceInfo']['transactionUuid'],
                            'invoiceNo' => $res['result'][0]['invoiceNo'],
                            'strIssueDate' => $res['result'][0]['issueDate'],
                            'additionalReferenceDesc' => "Huỷ hoá đơn",
                            'additionalReferenceDate' => $res['result'][0]['issueDate'],
                            'reasonDelete' => $description
                        )));
                        $res = curl_exec($ch);
                        $res = json_decode($res, true);
                        if ($res['code'] == 400) {
                            $return = json_encode(array(
                                "success" => 0,
                                "errorLabel" => $res['data'] . '<br>'. translate('LBL_CANCEL_VAT_ERR', 'J_Payment')
                            ));
                        }
                    }
                }
            }
        }
        $invoice->status = 'Cancelled';
        $invoice->license_remove_id = $license_remove_id;
        $invoice->reason_cancel     = $description;
        $invoice->save();
        if(empty($return))
            return json_encode(array("success" => "1",));
        else return $return;
    } else {
        return json_encode(array("success" => "0",));
    }
}

function ajaxCancelReceipt($payment_detail_id = '', $description = '')
{
    $pmOld = BeanFactory::getBean('J_PaymentDetail', $payment_detail_id);
    $payment = BeanFactory::getBean('J_Payment', $pmOld->payment_id);

    //check void e-invoice
    if (!empty($pmOld->invoice_id)) {
        return json_encode(array(
            "success" => "0",
            "errorLabel" => "Failed to void e-invoice. Please void e-invoice before void receipt!",
        ));
    }

    if (empty($pmOld->id) || $pmOld->status != 'Paid') {
        return json_encode(array(
            "success" => "0",
            "errorLabel" => "Something went wrong. Invalid payment type!!",
        ));
    }

    if ($pmOld->payment_amount <= 0) {
        return json_encode(array(
            "success" => "0",
            "errorLabel" => "Invalid payment amount!!",
        ));
    }

    //Check remain_amount for cancel

    if ($payment->remain_amount < $pmOld->payment_amount
        && in_array($payment->payment_type, ['Cashholder', 'Deposit']))
        return json_encode(array(
            "success" => "0",
            "errorLabel" => "This payment has been used. <br>If this payment has already Enrolled in the class you need to Delay student from the class !! Then you can void this invoice.'",
        ));


    //Create New Unpaid
    $pmNew = new J_PaymentDetail();
    foreach ($pmOld->field_defs as $keyField => $aFieldName)
        $pmNew->$keyField = $pmOld->$keyField;

    //Update Old Receipt
    $pmOld->status = "Cancelled";
    $pmOld->description = $description;
    $pmOld->save();

    //update invoice
    $pmNew->id = $pmNew->invoice_id = $pmNew->external_connection = '';
    //$pmNew->assigned_user_id = '';

    $pmNew->status = "Unpaid";

    //Reset Audit Amount
    $pmNew->audited_amount = null;
    $pmNew->reconcile_status = '';
    $pmNew->payment_method = $pmNew->bank_name = $pmNew->bank_account =  '';
    $pmNew->pos_code = $pmNew->card_type = '';

    $pmNew->method_note = $pmOld->method_note;
    $pmNew->payment_no = $pmOld->payment_no;
    $pmNew->save();

    //UPDATE Payment - Sale Type
    $GLOBALS['db']->query("UPDATE j_payment SET sale_type = 'Not set' WHERE id = '{$pmNew->payment_id}' AND deleted = 0 AND sale_type = 'New Sale'");

    return json_encode(array(
        "success" => "1",
    ));

}

function generateInvNo($team_id = '', $inv_date = '')
{
    //Get Prefix
    // Số hóa đơn thì ko có chữ
    $prefix = '#';
    $year = date('ym', strtotime($inv_date));
    $table = 'j_invoice';
    $code_field = 'name';
    $sep = '';
    $first_pad = '0000';
    $padding = 4;
    $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$id}' AND (LEFT($code_field, " . strlen($prefix . $year) . ") = '" . $prefix . $year . "') ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";

    $result = $GLOBALS['db']->query($query);
    if ($row = $GLOBALS['db']->fetchByAssoc($result))
        $last_code = $row[$code_field];
    else
        //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
        $last_code = $prefix . $year . $sep . $first_pad;

    $num = substr($last_code, -$padding, $padding);
    $num++;
    $pads = $padding - strlen($num);
    $new_code = $prefix . $year . $sep;

    //preform the lead padding 0
    for ($i = 0; $i < $pads; $i++)
        $new_code .= "0";
    $new_code .= $num;

    //return
    return $new_code;
}

function ajaxGetInvoiceNo($payment_id = '', $buyer_legal_type = '', $pmdt_id = '', $invoice_date = '', $extPaymentId = array(), $preview='push'){

    $admin = new Administration();
    $admin->retrieveSettings();
    if (!empty($admin->settings['einvoice_setting_export_einvoice_type']))
        $exportType = $admin->settings['einvoice_setting_export_einvoice_type'];
    else $exportType = 'Payment';

    //Count payment đã có hoá đơn chưa
    if($exportType == 'Payment') $countInv = $GLOBALS['db']->getOne("SELECT DISTINCT COUNT(DISTINCT l1.id) count_inv FROM j_payment INNER JOIN j_invoice_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_id AND l1_1.deleted = 0 INNER JOIN j_invoice l1 ON l1.id = l1_1.invoice_id AND l1.deleted = 0 WHERE (j_payment.id = '$payment_id') AND (l1.status = 'Paid') AND j_payment.deleted = 0");
    else $countInv = $GLOBALS['db']->getOne("SELECT DISTINCT COUNT(DISTINCT l1.id) count_inv FROM j_paymentdetail INNER JOIN j_invoice l1 ON j_paymentdetail.invoice_id = l1.id AND l1.deleted = 0 WHERE (j_paymentdetail.id = '$pmdt_id') AND (l1.status = 'Paid') AND j_paymentdetail.deleted = 0");
    if($countInv > 0) return array("success" => 0, "label" => translate('LBL_ERR_INVOICED', 'J_Payment'));

    //Check Invoice Invoicing - TH đang xuất hoá đơn & Cấu hính
    $evatRow   = $GLOBALS['db']->fetchOne("SELECT DISTINCT IFNULL(l2.id, '') evat_id, IFNULL(invoicing,0) invoicing FROM j_payment
        INNER JOIN teams l1 ON j_payment.team_id = l1.id AND l1.deleted = 0
        INNER JOIN j_configinvoiceno l2 ON l1.id = l2.team_id AND l2.deleted = 0
        WHERE (j_payment.id = '$payment_id') AND (l2.active = 1) AND j_payment.deleted = 0 LIMIT 1");
    $evat_id   = $evatRow['evat_id'];
    $invoicing = $evatRow['invoicing'];

    if(!empty($evat_id)){
        if($invoicing) return array(
            "success" => 99,
            "label" => translate('LBL_ERR_INVOICING', 'J_Payment'));
        else $GLOBALS['db']->query("UPDATE j_configinvoiceno SET invoicing=1 WHERE id='$evat_id'");
    }else return array(
        "success" => 0,
        "label" => translate('LBL_EVAT_ERR', 'J_Payment'));


    /**
    * trường hợp rule xuất hoá đơn của KH khác rule chung,
    * override function trong helperpaymentcustom extend helperpayment
    * mặc định không có thay đổi thì xoá file helperpaymentcustom trong source KH
    */

    if (file_exists('custom/modules/J_Payment/HelperPaymentCustom.php')) {
        require_once 'custom/modules/J_Payment/HelperPaymentCustom.php';
        $_helper = new HelperPaymentCustom();
    } else $_helper = new HelperPayment();

    //end

    // Get bean payment, evat
    $payment = BeanFactory::getBean('J_Payment', $payment_id);
    $evat = BeanFactory::getBean('J_ConfigInvoiceNo', $evat_id);
    //Assgin param
    $evat->exportType = $exportType;
    $evat->buyer_legal_type = $buyer_legal_type;
    $evat->invoice_date = $invoice_date;
    $evat->confirm_send_to = $_POST['confirm_send_to'];
    // Check get J_PaymentDetail ($exportType = "Receipt"), validate email, validate buyer name -------
    if($preview == 'push'){
        $res_check = $_helper->checkErrorBeforeGenerateContent($evat, $payment, $pmdt_id);
        if ($res_check["success"] != 1)
            return array(
                "success" => $res_check["success"],
                "label" => $res_check["label"],
                "evat_id" => $evat_id,
            );
    }


    // Get content ---------------------------------------------------------------
    //Get Invoice Detail
    $res_content = $_helper->generateInvContent($evat, $payment, $pmdt_id, $extPaymentId);
    if (!$res_content["success"])
        return array(
            "success" => 0,
            "label" => $res_content["label"],
            "evat_id" => $evat_id,
        );

    // Get json de gui api -------------------------------------------------------
    $res_json = $_helper->generateJSONContentInvoice($evat, $res_content);
    if (!$res_json["success"])
        return array(
            "success" => 0,
            "label" => $res_json["label"],
            "evat_id" => $evat_id,
        );

    // Gui api va tao invoice trong J_Invoice ------------------------------------
    $res_api = $_helper->sendApiAndCreateInvoice($evat, $res_json["bean_invoice"], $res_json["string_json"], $preview);
    if (!$res_api["success"])
        return array(
            "success" => 0,
            "label" => $res_api["label"],
            "evat_id" => $evat_id,
        );
    else{
        $res_api['evat_id'] = $evat_id;
        return $res_api;
    }
}

function ajaxExportInvoice($payment_id = '', $invoice_id = '')
{
    $invoice = BeanFactory::getBean('J_Invoice', $invoice_id, array('disable_row_level_security' => true));
    if ($invoice->supplier == 'Misa') {
        $transactionID = $GLOBALS['db']->getOne("SELECT transaction_id FROM j_invoice WHERE id = '{$invoice_id}' AND deleted = 0");
        $URL = "https://www.meinvoice.vn/tra-cuu/downloadhandler.ashx?type=pdf&code=$transactionID";
        if (!empty($transactionID)) {
            return json_encode(array(
                "success" => "1",
                "transactionID" => $transactionID,
                "url" => $URL,
            ));
        } else return json_encode(array(
            "success" => "0",
            "label" => translate('LBL_EVAT_ERR', 'J_Payment'),
            ));
    } elseif ($invoice->supplier == 'Viettel') {
        $payment_team_id = $GLOBALS['db']->getOne("SELECT team_id FROM j_payment WHERE id = '{$payment_id}' AND deleted = 0");
        $evat_id = $GLOBALS['db']->getOne("SELECT id FROM j_configinvoiceno WHERE team_id = '{$payment_team_id}' AND deleted = 0");
        $evat = BeanFactory::getBean('J_ConfigInvoiceNo', $evat_id);
        $e = new ViettelEInvoice($evat);
        $description = json_decode(html_entity_decode($invoice->description), true);

        $ch = curl_init($evat->host . '/services/einvoiceapplication/api/InvoiceAPI/InvoiceUtilsWS/getInvoiceRepresentationFile');
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Cookie: access_token=" . $e->getToken()
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
            'supplierTaxCode' => $evat->account,
            "invoiceNo" => $invoice->name,
            "templateCode" => $invoice->pattern,
            "transactionUuid" => $description['generalInvoiceInfo']['transactionUuid'],
            "fileType" => "PDF"
            ), JSON_UNESCAPED_UNICODE));
        $res = curl_exec($ch);
        $res = json_decode($res, true);
        if ($res['code'] == 400) {
            return json_encode(array(
                "success" => 0,
                "label" => translate('LBL_EVAT_ERR', 'J_Payment') . $res['data']
            ));
        } else {
            if (!$fp = fopen(dotb_cached($res['fileName']) , 'w')) {
                return json_encode(array(
                    "success" => "0",
                    "label" => translate('LBL_EVAT_ERR', 'J_Payment'),
                ));
            }

            if (fwrite($fp, base64_decode($res['fileToBytes'])) === FALSE) {
                return json_encode(array(
                    "success" => "0",
                    "label" => translate('LBL_EVAT_ERR', 'J_Payment'),
                ));
            }
            fclose($fp);
            return json_encode(array(
                "success" => "1",
                "transactionID" => $description['generalInvoiceInfo']['transactionUuid'],
                "url" => $GLOBALS['dotb_config']['site_url'] . '/'. dotb_cached($res['fileName'])
            ));
        }
    } elseif ($invoice->supplier == 'BKAV') {
        $payment_id = $GLOBALS['db']->getOne("SELECT payment_id FROM j_invoice WHERE id = '$invoice_id' AND deleted = 0");
        $payment_team_id = $GLOBALS['db']->getOne("SELECT team_id FROM j_payment WHERE id = '{$payment_id}' AND deleted = 0");
        $evat_id = $GLOBALS['db']->getOne("SELECT id FROM j_configinvoiceno WHERE team_id = '{$payment_team_id}' AND deleted = 0");
        $evat = BeanFactory::getBean('J_ConfigInvoiceNo', $evat_id);

        $string_json = json_encode(array(
            "partnerGUID" => $evat->partnerGUID,
            "CommandData" => base64_encode(json_encode(array(
                'CmdType' => "811",
                "CommandObject" => $GLOBALS['db']->getOne("SELECT transaction_id FROM j_invoice WHERE id = '$invoice_id' AND deleted = 0")
                ), JSON_UNESCAPED_UNICODE)),
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$evat->host/ExecCommand");
        $h = array();
        $h[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $h);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $string_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response, true);
        $result_decode = json_decode(base64_decode($result['d']), true);
        if ($result_decode['Status'] == 0) {
            $content = json_decode($result_decode['Object'], true);
            $content = base64_decode($content['PDF']);
            if (!$fp = fopen(dotb_cached($payment_id . '.pdf'), 'w')) {
                return json_encode(array(
                    "success" => "0",
                    "label" => translate('LBL_EVAT_ERR', 'J_Payment'),
                ));
            }

            if (fwrite($fp, $content) === FALSE) {
                return json_encode(array(
                    "success" => "0",
                    "label" => translate('LBL_EVAT_ERR', 'J_Payment'),
                ));
            }
            fclose($fp);
            return json_encode(array(
                "success" => "1",
                "transactionID" => '',
                "url" => $GLOBALS['dotb_config']['site_url'] . '/' . dotb_cached($payment_id . '.pdf')
            ));
        }
    }
    return json_encode(array(
        "success" => "0",
        "label" => translate('LBL_EVAT_ERR', 'J_Payment'),
    ));
}

function ajaxCheckVoucherFreeEventTAPTAP($mobile_phone, $sponsor_code, $center_code)
{
    if (!empty($mobile_phone) && !empty($sponsor_code) && !empty($center_code)) {
        global $timedate, $app_list_strings;
        $today = $timedate->nowDbDate();
        //TAPTAP
        if ($mobile_phone[0] == '0') {
            $mobile_phone = "84" . ltrim($mobile_phone, "0");
        }
        $status = 'Activated';
        $color = 'green';

        require_once "custom/include/_helper/connectTapTapApi.php";
        $tapTap = new TapTapHelper();
        $responeToken = $tapTap->requestToken();
        if ($responeToken['success']) {
            $responeValidateVoucher = $tapTap->validateVoucher($sponsor_code, $mobile_phone, $center_code);
            if ($responeValidateVoucher['success']) {
                if (empty($responeValidateVoucher['data']['valid_till']) && empty($responeValidateVoucher['data']['discount_amount']) && empty($responeValidateVoucher['data']['discount_percent']))
                    $used = 1;
                else $used = 0;
                if ($today > $responeValidateVoucher['data']['valid_till']) {
                    $status = 'Expired';
                    $color = 'red';
                }
                return json_encode(array(
                    "success" => "1",
                    "voucher_code" => $responeValidateVoucher['data']['code'],
                    "sponsor_number" => $responeValidateVoucher['data']['code'],
                    "foc_type" => 'TAPTAP Coupon',
                    "status_color" => '<br>Status: <b style="color:' . $color . ';"> ' . $app_list_strings['voucher_status_dom'][$status] . '</b>',
                    "status" => $status,
                    "type" => 'TAPTAP Sponsor',
                    "used_time" => $used . '/1',
                    "discount_amount" => format_number($responeValidateVoucher['data']['discount_amount']),
                    "discount_percent" => format_number($responeValidateVoucher['data']['discount_percent'], 2, 2),
                    "description" => $responeValidateVoucher['data']['description'],
                    "start_date" => '',
                    "end_date" => $responeValidateVoucher['data']['valid_till'],
                ));
            }
        }
    }
    return json_encode(array("success" => "0",));
}

function ajaxRedeemVoucherFreeEventTAPTAP($mobile_phone, $sponsor_code, $center_code)
{
    if (!empty($mobile_phone) && !empty($sponsor_code) && !empty($center_code)) {
        global $timedate;
        $today = $timedate->nowDbDate();
        //TAPTAP
        if ($mobile_phone[0] == '0') {
            $mobile_phone = "84" . ltrim($mobile_phone, "0");
        }

        require_once "custom/include/_helper/connectTapTapApi.php";
        $tapTap = new TapTapHelper();
        $responeToken = $tapTap->requestToken();
        if ($responeToken['success']) {
            $post = json_encode(array(
                'mobile' => $mobile_phone,
                'code' => $sponsor_code,
                'bill_amount' => 1,
                'transaction_number' => 'SYLVAN_FREECLASS',
                'store_code' => $center_code
            ));
            $responeValidateVoucher = $tapTap->redeemVoucher($post);
            return json_encode($responeValidateVoucher);
        } else return json_encode(array("success" => "0", "message" => "Error: " . $responeToken['message']));
    } else return json_encode(array("success" => "0", "message" => "Error: Please resolve any errors before proceeding."));
}


function ajaxRecalRemain($payment_id)
{
    $payment = BeanFactory::getBean('J_Payment', $payment_id);
    if (!empty($payment->id)) {
        getPaymentRemain($payment->id);
        return json_encode(array("success" => "1"));
    } else {
        return json_encode(array("success" => "0"));
    }
}

//Load schedule class - Auto Enrollment
function ajaxLoadSessions(){
    global $timedate;
    if (!empty($_POST['class_id']) && !empty($_POST['parent_id']) && !empty($_POST['parent_type'])){
        $student = BeanFactory::getBean($_POST['parent_type'], $_POST['parent_id']);

        $class   = BeanFactory::getBean('J_Class', $_POST['class_id']);
        if($_POST['payment_type'] == 'Cashholder'){
            if($_POST['parent_type'] == 'Contacts'){ //Điều kiện max size chỉ sử dụng trong TH Enroll học viên
                //Check Max size
                $res = getClassSize($class->id);
                if(!in_array($student->id, $res['student_list'])){
                    if(!empty($res['max_size'])){
                        if($res['current_size'] <= $res['max_size'] ) $res['current_size']++;
                        if($res['current_size'] > $res['max_size'])
                            return json_encode(array("success" => "0", 'error' => $student->name. translate('LBL_ADD_FAIL', 'J_Class').'<br>'. translate('LBL_OVER_CLASS_SIZE', 'J_Class')." <b>(".($res['current_size']-1)."/{$res['max_size']} ".translate('LBL_STUDENTS', 'J_Class').")</b>"));
                    }
                }
            }
            if (empty($class->id)) return json_encode(array("success" => "0", 'error' => translate('LBL_AJAX_ERR', 'Teams')));
        }
        if (empty($_POST['team_id'])) return json_encode(array("success" => "0", 'error' => translate('LBL_ERR_CLASS_ENROLL_TEAM', 'J_Class')));

        //Load Team class
        //Get List Center Class
        if ($class->team_set_id != $class->team_id) {
            $teamSetBean = new TeamSet();
            $teams = $teamSetBean->getTeams($class->team_set_id);
            $class_teams = array_keys($teams);
        } else $class_teams = array($class->team_id);

        if (!in_array($_POST['team_id'], $class_teams))
            return json_encode(array("success" => "0", 'error' => $student->name. translate('LBL_ADD_FAIL', 'J_Class').'<br>'. translate('LBL_ERR_CLASS_ENROLL_TEAM', 'J_Class')));

        //Get list SS by class
        $q2 = "SELECT DISTINCT
        IFNULL(meetings.id, '') primaryid,
        DATE_FORMAT(convert_tz(meetings.date_start,'+0:00','+7:00'),'%Y-%m-%d') date_start,
        meetings.lesson_number lesson_number,
        ROUND(meetings.duration_hours+(meetings.duration_minutes/60),9) delivery_hour
        FROM meetings INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
        WHERE ((l1.id = '{$_POST['class_id']}') AND (meetings.session_status <> 'Cancelled')) AND meetings.deleted = 0
        ORDER BY meetings.date_start ASC";
        $rs2 = $GLOBALS['db']->query($q2);
        $sessions = array();
        while ($ss = $GLOBALS['db']->fetchByAssoc($rs2))
            $sessions[] = array(
                'primaryid' => $ss['primaryid'],
                'date_start' => $ss['date_start'],
                'delivery_hour' => $ss['delivery_hour'],
            );

        //Alert Class Infor
        $_alert = "<br>" . translate('LBL_START_DATE', 'J_Class') . ': ' . $timedate->to_display_date($class->start_date, false) . '<br><br>';
        $_alert .= translate('LBL_END_DATE', 'J_Class') . ': ' . $timedate->to_display_date($class->end_date, false) . '<br><br>';
        $_alert .= translate('LBL_KIND_OF_COURSE', 'J_Class') . ': ' . $GLOBALS['app_list_strings']['kind_of_course_list'][$class->kind_of_course] . '  |  ' . translate('LBL_LEVEL', 'J_Class') . ': ' . $class->level . '<br><br>';
        $_alert .= translate('LBL_STATUS', 'J_Class') . ': ' . $GLOBALS['app_list_strings']['status_class_list'][$class->status] . '<br><br>';
        $_alert .= translate('LBL_TEAM', 'J_Class') . ': ' . $class->team_name . '<br>';

        return json_encode(
            array(
                "success" => "1",
                "sessions" => $sessions,
                "class_name" => $class->name,
                "class_koc" => $class->kind_of_course,
                "class_info" => $_alert,
                "start_enroll_sgt" => (!empty($row3['last_end_study'])) ? date('Y-m-d', strtotime("+1 day " . $row3['last_end_study'])) : $class->start_date,
                "end_enroll_sgt" => $class->end_date,
        ));
    } else json_encode(array("success" => "0", 'error' => translate('LBL_AJAX_ERR', 'Teams')));
}

//Load schedule class - Auto Enrollment
function ajaxSaveAutoEnroll(){
    global $timedate;
    if (!empty($_POST['payment_id']) && !empty($_POST['class_id']) && !empty($_POST['start_study']) && !empty($_POST['end_study'])) {
        //Update auto-enroll
        $_POST['is_allow_ost'] = filter_var($_POST['is_allow_ost'], FILTER_VALIDATE_BOOLEAN);
        $start_study = $timedate->convertToDBDate($_POST['start_study']);
        $end_study = $timedate->convertToDBDate($_POST['end_study']);
        $class = BeanFactory::getBean('J_Class', $_POST['class_id']);
        $payment = BeanFactory::getBean('J_Payment', $_POST['payment_id']);
        //process Auto-Enroll
        if ($_POST['active']) {
            $payment->is_auto_enroll = 1;
            $payment->ju_class_id = $class->id;
            $payment->start_study = $start_study;
            $payment->end_study = $end_study;
            $payment->enroll_hours = $_POST['enroll_hours'];
            $payment->sessions = $_POST['sessions'];
            $payment->is_allow_ost = $_POST['is_allow_ost'];
            $payment->save();
            $enr_success[] = translate('LBL_SET_AUTO_ENROLL', 'J_Payment');
        } else {
            $payment->is_auto_enroll = 0;
            $payment->save();
            $enr_success[] = translate('LBL_UNSET_AUTO_ENROLL', 'J_Payment');
        }

        //Process Auto-Enroll
        if ($payment->is_auto_enroll && $payment->parent_type == 'Contacts') {
            $res = processAutoEnroll($payment, $class);
            if ($res['success']){
                $enr_success[] = $res['alert'];
                updateStudentStatus($payment->parent_id); //Update Student Status
            }
            else $enr_fail[] = $res['alert'];
        }
        return json_encode(array("success" => 1, 'enr_fail' => $enr_fail, 'enr_success' => $enr_success));
    } else return json_encode(array("success" => 0, 'error' => translate('LBL_AJAX_ERR', 'Teams')));
}

//Send Payment Request
function sendPaySlip($payment_detail_id){
    $pmd = BeanFactory::getBean('J_PaymentDetail', $payment_detail_id);
    if(empty($pmd->id) || in_array($pmd->status,['Paid','Cancelled']))
        return json_encode(array("success" => 0, "count_change" => $_count_change, "label1" => translate('LBL_ERR_PMD_NOT_FOUND', 'J_PaymentDetail'), "label2" => translate('LBL_ERR_PMD_NOT_FOUND_DES', 'J_PaymentDetail')));
    else{
        $pmd->is_sent = 1;
        $pmd->save();
        $_alert = translate('LBL_PAYMENT_REQUEST_SENT_DES', 'J_PaymentDetail');
        $_alert = str_replace('{$student_name}',$pmd->parent_name,$_alert);
        $_alert = str_replace('{$payment_amount}',format_number($pmd->payment_amount),$_alert);
        return json_encode(array("success" => 1, "count_change" => $_count_change, "label1" => translate('LBL_PAYMENT_REQUEST_SENT', 'J_PaymentDetail'), "label2" => $_alert));
    }
}
