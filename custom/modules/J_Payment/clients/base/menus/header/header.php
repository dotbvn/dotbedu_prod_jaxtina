<?php



$moduleName = 'J_Payment';
$viewdefs[$moduleName]['base']['menu']['header'] = array(
//    array(
//        'route' => "#bwc/index.php?module=$moduleName&action=EditView&payment_type=Enrollment",
//        'label' => 'LNK_CREATE_ENROLLMENT',
//        'acl_action' => 'create',
//        'acl_module' => $moduleName,
//        'icon' => 'fa-plus',
//    ),
    array(
        'route' => "#bwc/index.php?module=$moduleName&action=EditView&payment_type=Cashholder",
        'label' => 'LNK_CREATE_PAYMENT',
        'acl_action' => 'create',
        'acl_module' => $moduleName,
        'icon' => 'fa-plus',
    ),
    array(
        'route' => "#$moduleName",
        'label' => 'LNK_LIST',
        'acl_action' => 'list',
        'acl_module' => $moduleName,
        'icon' => 'fa-bars',
    ),
    array(
        'route' => "#bwc/index.php?module=Import&action=Step1&import_module=$moduleName&return_module=$moduleName&return_action=index",
        'label' => 'LBL_IMPORT',
        'acl_action' => 'import',
        'acl_module' => $moduleName,
        'icon' => 'fa-cloud-upload',
    ),
);

$admin = new Administration();
$admin->retrieveSettings();
if(!empty($admin->settings['taptap_config_keycloak_host'])){
    $viewdefs[$moduleName]['base']['menu']['header'][] = array(
        'route' => "#bwc/index.php?module=$moduleName&action=redeemVoucherTapTap",
        'label' => 'LBL_REDEEM_VOUCHER_TAPTAP',
        'acl_action' => 'create',
        'acl_module' => $moduleName,
        'icon' => 'fa-cloud-upload-alt',
    );
}