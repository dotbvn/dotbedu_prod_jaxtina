<?php
// created: 2021-09-14 16:06:40
$viewdefs['J_Payment']['base']['filter']['default'] = array (
    'default_filter' => 'all_records',
    'fields' =>
    array (
        'name' =>
        array (
        ),
        'assigned_user_name' =>
        array (
        ),
        'sale_type' =>
        array (
        ),
        'payment_date' =>
        array (
        ),
        'payment_expired' =>
        array (
        ),
        'parent_name' =>array (),
        'parent_type' =>array (),
        'payment_amount' =>
        array (
        ),
        'sale_type_date' =>
        array (
        ),
        'j_coursefee_j_payment_1_name' =>
        array (
        ),
        'payment_type' =>
        array (
        ),
        'kind_of_course' =>
        array (
        ),
        'total_hours' =>
        array (
        ),
        'tuition_hours' =>
        array (
        ),
        'remain_amount' =>
        array (
        ),
        'remain_hours' =>
        array (
        ),
        'start_study' =>
        array (
        ),
        'end_study' =>
        array (
        ),
        'tuition_fee' =>
        array (
        ),
        'amount_bef_discount' =>
        array (
        ),
        'loyalty_percent' =>
        array (
        ),
        'loyalty_amount' =>
        array (
        ),
        'final_sponsor_percent' =>
        array (
        ),
        'final_sponsor' =>
        array (
        ),
        'discount_amount' =>
        array (
        ),
        'discount_percent' =>
        array (
        ),
        'total_after_discount' =>
        array (
        ),
        'deposit_amount' =>
        array (
        ),
        'paid_amount' =>
        array (
        ),
        'paid_hours' =>
        array (
        ),
        'is_free_book' =>
        array (
        ),
        'status' =>
        array (
        ),
        'old_student_id' =>
        array (
        ),
        'is_auto_enroll' =>
        array (
        ),
        'ju_class_name' =>
        array (
        ),
        'team_name' =>
        array (
        ),
        'date_modified' =>
        array (
        ),
        'date_entered' =>
        array (
        ),
        'modified_by_name' =>
        array (
        ),
        'created_by_name' => array (),
        'is_old' => array (),
        '$owner' =>
        array (
            'predefined_filter' => true,
            'vname' => 'LBL_CURRENT_USER_FILTER',
        ),
        '$favorite' =>
        array (
            'predefined_filter' => true,
            'vname' => 'LBL_FAVORITES_FILTER',
        ),
    ),
);