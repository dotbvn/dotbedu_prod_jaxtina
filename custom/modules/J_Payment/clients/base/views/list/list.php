<?php
$module_name = 'J_Payment';
$viewdefs[$module_name] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
                'width' => 'large',
              ),
              1 =>
              array (
                'name' => 'parent_name_text',
                'label' => 'LBL_STUDENT',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
                'width' => 'large',
              ),
              2 =>
              array (
                'name' => 'phone',
                'label' => 'LBL_PHONE',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
              ),
              3 =>
              array (
                'name' => 'payment_type',
                'label' => 'LBL_PAYMENT_TYPE',
                'enabled' => true,
                'default' => true,
                'type' => 'html',

              ),
              4 =>
              array (
                'name' => 'payment_amount',
                'label' => 'LBL_GRAND_TOTAL',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
              ),
              5 =>
              array (
                'name' => 'sum_paid',
                'label' => 'LBL_SUM_PAID',
                'enabled' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'sum_unpaid',
                'label' => 'LBL_SUM_UNPAID',
                'enabled' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'remain_amount',
                'label' => 'LBL_REMAIN_AMOUNT',
                'enabled' => true,
                'related_fields' =>
                array (
                  0 => 'currency_id',
                  1 => 'base_rate',
                ),
                'currency_format' => true,
                'default' => true,
                'width' => 'small',
              ),
              8 =>
              array (
                'name' => 'remain_hours',
                'label' => 'LBL_REMAIN_HOURS',
                'enabled' => true,
                'default' => true,
              ),
              9 =>
              array (
                'name' => 'payment_date',
                'label' => 'LBL_PAYMENT_DATE',
                'enabled' => true,
                'default' => true,
              ),
              10 =>
              array (
                'name' => 'sale_type',
                'label' => 'LBL_SALE_TYPE',
                'enabled' => true,
                'default' => true,
                'width' => 'small',
              ),
              11 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              12 =>
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              13 =>
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => false,
              ),
              14 =>
              array (
                'name' => 'old_student_id',
                'enabled' => true,
                'default' => false,
              ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
