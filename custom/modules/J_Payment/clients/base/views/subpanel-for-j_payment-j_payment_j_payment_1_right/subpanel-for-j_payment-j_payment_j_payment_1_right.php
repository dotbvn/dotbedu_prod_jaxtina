<?php
// created: 2022-06-12 14:38:02
$viewdefs['J_Payment']['base']['view']['subpanel-for-j_payment-j_payment_j_payment_1_right'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                0 =>
                array (
                    'label' => 'LBL_NAME',
                    'enabled' => true,
                    'default' => true,
                    'name' => 'name',
                    'link' => true,
                ),
                1 =>
                array (
                    'name' => 'payment_type',
                    'label' => 'LBL_PAYMENT_TYPE',
                    'enabled' => true,
                    'default' => true,
                ),
                2 =>
                array (
                    'name' => 'payment_date',
                    'label' => 'LBL_PAYMENT_DATE',
                    'enabled' => true,
                    'default' => true,
                ),
                3 =>
                array (
                    'name' => 'payment_amount',
                    'label' => 'LBL_GRAND_TOTAL',
                    'enabled' => true,
                    'currency_format' => true,
                    'default' => true,
                ),
                4 =>
                array (
                    'name' => 'applied_amount',
                    'label' => 'LBL_APPLIED_AMOUNT',
                    'enabled' => true,
                    'default' => true,
                ),
                5 =>
                array (
                    'name' => 'applied_hours',
                    'label' => 'LBL_APPLIED_HOURS',
                    'enabled' => true,
                    'default' => true,
                ),
                6 =>
                array (
                    'name' => 'sale_type',
                    'label' => 'LBL_SALE_TYPE',
                    'enabled' => true,
                    'default' => true,
                ),
                7 =>
                array (
                    'name' => 'sale_type_date',
                    'label' => 'LBL_SALE_TYPE_DATE',
                    'enabled' => true,
                    'default' => true,
                ),
                8 =>
                array (
                    'name' => 'cost_per_hour',
                    'label' => 'LBL_COST_PER_HOUR',
                    'enabled' => true,
                    'default' => true,
                ),
                9 =>
                array (
                    'name' => 'ju_class_name',
                    'label' => 'LBL_CLASS_NAME',
                    'enabled' => true,
                    'id' => 'JU_CLASS_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                10 =>
                array (
                    'name' => 'start_study',
                    'label' => 'LBL_START_STUDY',
                    'enabled' => true,
                    'default' => true,
                ),
                11 =>
                array (
                    'name' => 'end_study',
                    'label' => 'LBL_END_STUDY',
                    'enabled' => true,
                    'default' => true,
                ),
                12 =>
                array (
                    'name' => 'date_entered',
                    'label' => 'LBL_DATE_ENTERED',
                    'enabled' => true,
                    'readonly' => true,
                    'default' => true,
                ),
                13 =>
                array (
                    'name' => 'created_by_name',
                    'label' => 'LBL_CREATED',
                    'enabled' => true,
                    'readonly' => true,
                    'id' => 'CREATED_BY',
                    'link' => true,
                    'default' => true,
                ),
                14 =>
                array (
                    'name' => 'is_old',
                    'default' => false,
                ),
            ),
        ),
    ),
    'orderBy' =>
    array (
        'field' => 'date_modified',
        'direction' => 'desc',
    ),
    'type' => 'subpanel-list',
);