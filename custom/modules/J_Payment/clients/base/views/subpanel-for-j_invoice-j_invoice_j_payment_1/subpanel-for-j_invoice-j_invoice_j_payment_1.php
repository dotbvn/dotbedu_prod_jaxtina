<?php
// created: 2022-06-12 01:25:41
$viewdefs['J_Payment']['base']['view']['subpanel-for-j_invoice-j_invoice_j_payment_1'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 =>
        array (
          'name' => 'parent_name',
          'label' => 'LBL_PARENT_NAME',
          'enabled' => true,
          'id' => 'PARENT_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 =>
        array (
          'name' => 'phone',
          'label' => 'LBL_PHONE',
          'enabled' => true,
          'default' => true,
        ),
        3 =>
        array (
          'name' => 'payment_type',
          'label' => 'LBL_PAYMENT_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        4 =>
        array (
          'name' => 'payment_amount',
          'label' => 'LBL_GRAND_TOTAL',
          'enabled' => true,
          'currency_format' => true,
          'default' => true,
        ),
        5 =>
        array (
          'name' => 'sum_paid',
          'label' => 'LBL_SUM_PAID',
          'enabled' => true,
          'currency_format' => true,
          'default' => true,
        ),
        6 =>
        array (
          'name' => 'sum_unpaid',
          'label' => 'LBL_SUM_UNPAID',
          'enabled' => true,
          'currency_format' => true,
          'default' => true,
        ),
        7 =>
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
        8 =>
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        9 =>
        array (
          'name' => 'team_name',
          'label' => 'LBL_TEAMS',
          'enabled' => true,
          'id' => 'TEAM_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' =>
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);