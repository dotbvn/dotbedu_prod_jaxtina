<?php
$module_name = 'J_Payment';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'selection-list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'parent_name',
                'label' => 'LBL_PARENT_NAME',
                'enabled' => true,
                'id' => 'PARENT_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'payment_type',
                'label' => 'LBL_PAYMENT_TYPE',
                'enabled' => true,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'payment_amount',
                'label' => 'LBL_GRAND_TOTAL',
                'enabled' => true,
                'currency_format' => true,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'remain_amount',
                'label' => 'LBL_REMAIN_AMOUNT',
                'enabled' => true,
                'currency_format' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'sum_paid',
                'label' => 'LBL_SUM_PAID',
                'enabled' => true,
                'currency_format' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'sum_unpaid',
                'label' => 'LBL_SUM_UNPAID',
                'enabled' => true,
                'currency_format' => true,
                'default' => true,
              ),
              7 => 
              array (
                'name' => 'payment_date',
                'label' => 'LBL_PAYMENT_DATE',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              9 => 
              array (
                'label' => 'LBL_DATE_MODIFIED',
                'enabled' => true,
                'default' => true,
                'name' => 'date_modified',
                'readonly' => true,
              ),
              10 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
