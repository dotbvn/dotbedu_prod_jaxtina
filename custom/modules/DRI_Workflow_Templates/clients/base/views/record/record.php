<?php

$viewdefs['DRI_Workflow_Templates']['base']['view']['record'] = array(
  'buttons' =>
  array(
    0 =>
    array(
      'type' => 'button',
      'name' => 'cancel_button',
      'label' => 'LBL_CANCEL_BUTTON_LABEL',
      'css_class' => 'btn-invisible btn-link',
      'showOn' => 'edit',
      'events' => array(
          'click' => 'button:cancel_button:click',
      ),
    ),
    1 =>
    array(
      'type' => 'rowaction',
      'event' => 'button:save_button:click',
      'name' => 'save_button',
      'label' => 'LBL_SAVE_BUTTON_LABEL',
      'css_class' => 'btn btn-primary',
      'showOn' => 'edit',
      'acl_action' => 'edit',
    ),
    2 =>
    array(
      'type' => 'actiondropdown',
      'name' => 'main_dropdown',
      'primary' => true,
      'showOn' => 'view',
      'buttons' =>
      array(
        0 =>
        array(
          'type' => 'rowaction',
          'event' => 'button:edit_button:click',
          'name' => 'edit_button',
          'label' => 'LBL_EDIT_BUTTON_LABEL',
          'acl_action' => 'edit',
        ),
        1 =>
        array(
          'type' => 'divider',
        ),
        2 =>
        array(
          'type' => 'rowaction',
          'event' => 'button:duplicate_button:click',
          'name' => 'duplicate_button',
          'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
          'acl_action' => 'create',
        ),
        3 =>
        array(
          'type' => 'rowaction',
          'event' => 'button:export_button:click',
          'name' => 'export_button',
          'label' => 'LBL_EXPORT_BUTTON_LABEL',
          'acl_action' => 'export',
        ),
        4 =>
        array(
          'type' => 'divider',
        ),
        5 =>
        array(
          'type' => 'rowaction',
          'event' => 'button:delete_button:click',
          'name' => 'delete_button',
          'label' => 'LBL_DELETE_BUTTON_LABEL',
          'acl_action' => 'delete',
        ),
      ),
    ),
    3 =>
    array(
      'name' => 'sidebar_toggle',
      'type' => 'sidebartoggle',
    ),
  ),
  'panels' =>
  array(
    0 =>
    array(
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_HEADER',
      'header' => true,
      'fields' =>
      array (
        0 =>
        array (
          'name' => 'picture',
          'type' => 'avatar',
          'width' => 42,
          'height' => 42,
          'dismiss_label' => true,
          'readonly' => true,
        ),
        1 => 'name',
        2 =>
        array (
          'name' => 'favorite',
          'label' => 'LBL_FAVORITE',
          'type' => 'favorite',
          'dismiss_label' => true,
        ),
        3 =>
        array (
          'name' => 'follow',
          'label' => 'LBL_FOLLOW',
          'type' => 'follow',
          'readonly' => true,
          'dismiss_label' => true,
        ),
      ),
    ),
    1 =>
    array(
      'name' => 'panel_body',
      'label' => 'LBL_PANEL_BODY',
      'columns' => 2,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' =>
      array (
        0 => 'available_modules',
        1 => 'copied_template_name',
        2 => 'assignee_rule',
        3 => 'disabled_stage_actions',
        4 => 'target_assignee',
        5 => 'disabled_activity_actions',
        6 => 'points',
        7 => 'active',
        8 => 'related_activities',
        9 =>
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'span' => 12,
        ),
      ),
    ),
    2 =>
    array(
      'columns' => 2,
      'name' => 'panel_hidden',
      'label' => 'LBL_RECORD_SHOWMORE',
      'hide' => true,
      'labelsOnTop' => true,
      'placeholders' => true,
      'newTab' => false,
      'panelDefault' => 'expanded',
      'fields' =>
      array(
        0 =>
        array (
          'name' => 'date_entered_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_ENTERED',
          'fields' =>
          array (
            0 =>
            array (
              'name' => 'date_entered',
            ),
            1 =>
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 =>
            array (
              'name' => 'created_by_name',
            ),
          ),
        ),
        1 => 'team_name',
        2 =>
        array (
          'name' => 'date_modified_by',
          'readonly' => true,
          'inline' => true,
          'type' => 'fieldset',
          'label' => 'LBL_DATE_MODIFIED',
          'fields' =>
          array (
            0 =>
            array (
              'name' => 'date_modified',
            ),
            1 =>
            array (
              'type' => 'label',
              'default_value' => 'LBL_BY',
            ),
            2 =>
            array (
              'name' => 'modified_by_name',
            ),
          ),
        ),
      ),
    ),
  ),
  'templateMeta' =>
  array(
    'useTabs' => false,
  ),
);