<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicLeadImport
{
    public function handleBeforeImport(&$bean, $event, $arguments)
    {
        //save full name
        $bean->last_name = mb_convert_case(trim(preg_replace('/\s+/', ' ', str_replace(['ㅤ', 'ᅠ'], '', $bean->last_name))), MB_CASE_TITLE, "UTF-8");
        $bean->first_name = mb_convert_case(trim(preg_replace('/\s+/', ' ', str_replace(['ㅤ', 'ᅠ'], '', $bean->first_name))), MB_CASE_TITLE, "UTF-8");
        $bean->name = trim($bean->last_name . ' ' . $bean->first_name);
    }
}