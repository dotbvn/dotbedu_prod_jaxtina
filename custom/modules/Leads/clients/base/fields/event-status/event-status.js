/**
 * EventStatusField is a field for Meetings/Calls that show the status field of the model as a badge field.
 *
 * FIXME: This component will be moved out of clients/base folder as part of MAR-2274 and SC-3593
 *
 * @class View.Fields.Base.EventStatusField
 * @alias DOTB.App.view.fields.BaseEventStatusField
 * @extends View.Fields.Base.BadgeSelectField
 */

 ({
    extendsFrom: 'BadgeSelectField',

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super('initialize', [options]);

        /**
         * An object where its keys map to specific status and color to matching
         * CSS classes.
         */
        this.statusClasses = {
            'New': 'textbg_green',
            'In Progress': 'textbg_blue',
            'In Process': 'textbg_blue',
            'Converted': 'textbg_red',
            'Ready to PT': 'textbg_bluelight',
            'Ready to Demo': 'textbg_bluelight',
            'Appointment': 'textbg_bluelight',
            'PT/Demo': 'textbg_violet',
            "Deposit": "textbg_crimson",
            'Dead': 'textbg_black',
            'Visited'    : 'textbg_roselight',
        };

        this.type = 'badge-select';
    },

    /**
     * @inheritdoc
     */
    _loadTemplate: function() {
        var action = this.action || this.view.action;

        if (action == "disabled" && this.def.name === "status") {
            this.action = "visible";
        }
        //Xử lý Disabled nếu Status = 'Converted'
        if(action == 'edit' && this.def.name == "status" && this.model.get(this.name) == 'Converted'){
            this.action = "disabled";
        }

        if (action === 'edit') {
            this.type = 'enum';
        }

        this._super('_loadTemplate');
        this.type = 'badge-select';
    }
})

