
/**
 * @extends View.Fields.Base.EnumField
 */
({
    extendsFrom: 'EnumField',

    loadEnumOptions: function(fetch, callback) {
        if (this.name === 'lead_source' &&  this.view.action == 'create' && this.module == 'Leads') {
            this.items = app.lang.getAppListStrings('lead_source2_list');
        }else
            this._super('loadEnumOptions', [fetch, callback]);
    }
})
