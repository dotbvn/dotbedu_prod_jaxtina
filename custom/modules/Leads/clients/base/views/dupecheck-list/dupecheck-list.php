<?php
$viewdefs['Leads'] =
    array(
        'base' =>
            array(
                'view' =>
                    array(
                        'dupecheck-list' =>
                            array(
                                'panels' =>
                                    array(

                                        array(
                                            'name' => 'panel_header',
                                            'label' => 'LBL_PANEL_1',
                                            'fields' =>
                                                array(

                                                    array(
                                                        'name' => 'name',
                                                        'type' => 'fullname',
                                                        'fields' =>
                                                            array(
                                                                'salutation',
                                                                'first_name',
                                                                'last_name',
                                                            ),
                                                        'link' => true,
                                                        'label' => 'LBL_LIST_NAME',
                                                        'enabled' => true,
                                                        'default' => true,
                                                        'width' => 'large',
                                                    ),


                                                    array(
                                                        'name' => 'phone_mobile',
                                                        'label' => 'LBL_MOBILE_PHONE',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),

                                                    array(
                                                        'name' => 'assigned_user_name',
                                                        'label' => 'LBL_LIST_ASSIGNED_USER',
                                                        'enabled' => true,
                                                        'default' => true,
                                                    ),


                                                    array(
                                                        'name' => 'team_name',
                                                        'label' => 'LBL_TEAMS',
                                                        'enabled' => true,
                                                        'id' => 'TEAM_ID',
                                                        'link' => true,
                                                        'sortable' => false,
                                                        'default' => true,
                                                    ),
                                                ),
                                        ),
                                    ),
                            ),
                    ),
            ),
    );
