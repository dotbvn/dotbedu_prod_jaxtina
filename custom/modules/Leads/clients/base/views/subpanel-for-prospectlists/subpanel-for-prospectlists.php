<?php

$viewdefs['Leads']['base']['view']['subpanel-for-prospectlists'] = array(
    'type' => 'subpanel-list',
    'panels' =>
        array(
            array(
                'name' => 'panel_header',
                'label' => 'LBL_PANEL_1',
                'fields' => array(
                    array(
                        'name' => 'name',
                        'label' => 'LBL_NAME',
                        'enabled' => true,
                        'default' => true,
                        'link' => true,
                    ),
                    array (
                        'name' => 'refered_by',
                        'label' => 'LBL_REFERED_BY',
                        'enabled' => true,
                        'default' => true,
                    ),
                    array (
                        'name' => 'lead_source',
                        'label' => 'LBL_LEAD_SOURCE',
                        'enabled' => true,
                        'default' => true,
                    ),
                    array (
                        'name' => 'phone_work',
                        'label' => 'LBL_LIST_PHONE',
                        'enabled' => true,
                        'default' => true,
                    ),
                    array (
                        'name' => 'email',
                        'label' => 'LBL_LIST_EMAIL_ADDRESS',
                        'enabled' => true,
                        'default' => true,
                    ),
                    array (
                        'name' => 'source_description',
                        'label' => 'LBL_LIST_LEAD_SOURCE_DESCRIPTION',
                        'enabled' => true,
                        'default' => true,
                    ),
                    array(
                        'label' => 'LBL_LIST_ASSIGNED_TO_NAME',
                        'enabled' => true,
                        'default' => true,
                        'name' => 'assigned_user_name',
                    ),
                ),
            ),
        ),
    'rowactions' =>
        array (
            'actions' => array(
                1 => array(
                    'type' => 'unlink-action',
                    'icon' => 'fa-chain-broken',
                    'tooltip' => 'LBL_UNLINK_BUTTON',
                ),
            ),
        ),
);
