<?php
$viewdefs['Leads'] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'record' =>
            array (
                'buttons' =>
                array (
                    0 =>
                    array (
                        'type' => 'button',
                        'name' => 'cancel_button',
                        'label' => 'LBL_CANCEL_BUTTON_LABEL',
                        'css_class' => 'btn',
                        'showOn' => 'edit',
                        'tooltip' => 'LBL_CANCEL_BUTTON_LABEL',
                        'icon' => 'fa-times',
                        'customButton' => true,
                        'dismiss_label' => true,
                        'events' =>
                        array (
                            'click' => 'button:cancel_button:click',
                        ),
                    ),
                    1 =>
                    array (
                        'type' => 'rowaction',
                        'event' => 'button:save_button:click',
                        'name' => 'save_button',
                        'label' => 'LBL_SAVE_BUTTON_LABEL',
                        'css_class' => 'btn btn-primary',
                        'showOn' => 'edit',
                        'tooltip' => 'LBL_SAVE_BUTTON_LABEL',
                        'icon' => 'fa-save',
                        'customButton' => true,


                        'acl_action' => 'edit',
                    ),
                    2 =>
                    array (
                        'type' => 'actiondropdown',
                        'name' => 'main_dropdown',
                        'primary' => true,
                        'showOn' => 'view',
                        'buttons' =>
                        array (
                            0 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:edit_button:click',
                                'name' => 'edit_button',
                                'label' => 'LBL_EDIT_BUTTON_LABEL',
                                'tooltip' => 'LBL_EDIT_BUTTON_LABEL',
                                'icon' => 'fa-pencil',
                                'customButton' => true,
                                'acl_action' => 'edit',
                            ),
                            1 =>
                            array (
                                'type' => 'shareaction',
                                'name' => 'share',
                                'label' => 'LBL_RECORD_SHARE_BUTTON',
                                'acl_action' => 'view',
                            ),
                            2 =>
                            array (
                                'type' => 'pdfaction',
                                'name' => 'download-pdf',
                                'label' => 'LBL_PDF_VIEW',
                                'action' => 'download',
                                'acl_action' => 'view',
                            ),
                            3 =>
                            array (
                                'type' => 'pdfaction',
                                'name' => 'email-pdf',
                                'label' => 'LBL_PDF_EMAIL',
                                'action' => 'email',
                                'acl_action' => 'view',
                            ),
                            4 =>
                            array (
                                'type' => 'divider',
                            ),
                            5 =>
                            array (
                                'type' => 'convertbutton',
                                'name' => 'lead_convert_button',
                                'label' => 'LBL_CONVERT_BUTTON_LABEL',
                                'acl_action' => 'edit',
                            ),
                            7 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:find_duplicates_button:click',
                                'name' => 'find_duplicates_button',
                                'label' => 'LBL_DUP_MERGE',
                                'acl_action' => 'edit',
                            ),
                            10 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:duplicate_button:click',
                                'name' => 'duplicate_button',
                                'label' => 'LBL_DUPLICATE_BUTTON_LABEL',
                                'acl_module' => 'Leads',
                                'acl_action' => 'create',
                            ),
                            //              11 =>
                            //              array (
                            //                'type' => 'rowaction',
                            //                'event' => 'button:historical_summary_button:click',
                            //                'name' => 'historical_summary_button',
                            //                'label' => 'LBL_HISTORICAL_SUMMARY',
                            //                'acl_action' => 'view',
                            //              ),
                            12 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:audit_button:click',
                                'name' => 'audit_button',
                                'label' => 'LNK_VIEW_CHANGE_LOG',
                                'acl_action' => 'view',
                            ),
                            13 =>
                            array (
                                'type' => 'divider',
                            ),
                            14 =>
                            array (
                                'type' => 'rowaction',
                                'event' => 'button:delete_button:click',
                                'name' => 'delete_button',
                                'label' => 'LBL_DELETE_BUTTON_LABEL',
                                'acl_action' => 'delete',
                            ),
                        ),
                    ),
                    3 =>
                    array (
                        'name' => 'sidebar_toggle',
                        'type' => 'sidebartoggle',
                    ),
                ),
                'panels' =>
                array (
                    0 =>
                    array (
                        'name' => 'panel_header',
                        'header' => true,
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'picture',
                                'type' => 'avatar',
                                'size' => 'large',
                                'dismiss_label' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'name',
                                'type' => 'fullname',
                                'label' => 'LBL_NAME',
                                'dismiss_label' => true,
                                'fields' =>
                                array (
                                    0 => 'last_name',
                                    1 => 'first_name',
                                ),
                            ),
                            2 =>
                            array (
                                'type' => 'favorite',
                            ),
                            3 =>
                            array (
                                'name' => 'status',
                                'type' => 'event-status',
                                'enum_width' => '150px',
                                'dropdown_width' => '150px',
                                'dropdown_class' => 'select2-menu-only',
                                'container_class' => 'select2-menu-only',
                            ),
                            4 =>
                            array (
                                'name' => 'converted',
                                'type' => 'badge',
                                'dismiss_label' => true,
                                'readonly' => true,
                                'related_fields' =>
                                array (
                                    0 => 'account_id',
                                    1 => 'account_name',
                                    2 => 'contact_id',
                                    3 => 'contact_name',
                                    4 => 'opportunity_id',
                                    5 => 'opportunity_name',
                                    6 => 'converted_opp_name',
                                ),
                            ),
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'panel_body',
                        'label' => 'LBL_RECORDVIEW_PANEL1',
                        'columns' => 3,
                        'labels' => true,
                        'labelsOnTop' => true,
                        'placeholders' => true,
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                        'fields' =>
                        array (
                            //Row 0
                            0 =>'nick_name',
                            1 => array(
                                'name' => 'label',

                                'type' => 'label',
                                'value' => 'LBL_GENDER',
                            ),
                            2 => array(
                                'name' => 'gender',
                                'studio' => 'visible',
                                'label' => 'LBL_GENDER',
                            ),

                            // Row 1
                            3 =>'phone_mobile',
                            4 => array(
                                'name' => 'email',
                                'span' => 8,
                            ),

                            // Row 2
                            5 => 'guardian_name',
                            6 => 'guardian_name_2',
                            7 => 'birthdate',
                            8 => 'relationship',
                            9 => 'relationship2',
                            10 => 'age',
                            // Row 3
                            11 =>'phone_guardian',
                            12 =>'other_mobile',
                            13 => 'career',

                            14=>
                            array (
                                'name' => 'description',
                                'span' => 8,
                            ),
                            15=>
                            array(
                                'name' => 'primary_address',
                                'type' => 'fieldset',
                                'css_class' => 'address',
                                'label' => 'LBL_PRIMARY_ADDRESS',
                                'fields' =>
                                array(
                                    array(
                                        'name' => 'primary_address_street',
                                        'css_class' => 'address_street',
                                        'placeholder' => 'LBL_PRIMARY_ADDRESS_STREET',
                                    ),
                                    array(
                                        'name' => 'primary_address_postalcode',
                                        'css_class' => 'address_zip',
                                        'placeholder' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
                                    ),
                                    array(
                                        'name' => 'primary_address_state',
                                        'css_class' => 'address_state',
                                        'placeholder' => 'LBL_PRIMARY_ADDRESS_STATE',
                                    ),

                                    array(
                                        'name' => 'primary_address_city',
                                        'css_class' => 'address_city',
                                        'placeholder' => 'LBL_PRIMARY_ADDRESS_CITY',
                                    ),
                                    array(
                                        'name' => 'primary_address_country',
                                        'css_class' => 'address_country',
                                        'placeholder' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
                                    ),
                                    array(
                                        'name' => 'pri_latitude',
                                        'css_class' => 'hidden',
                                    ),
                                    array(
                                        'name' => 'pri_longitude',
                                        'css_class' => 'hidden',
                                    )
                                ),
                            ),
                            16 => 'eng_level',
                            17 => 'target',
                            18 => 'object',
                        ),
                    ),

                    2 =>
                    array (
                        'name' => 'panel_hidden',
                        'label' => 'LBL_RECORD_SHOWMORE',
                        'hide' => true,
                        'columns' => 3,
                        'labels' => true,
                        'labelsOnTop' => true,
                        'placeholders' => true,
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                        'fields' =>
                        array (
                            'school_name',
                            'grade',
                            'prefer_level',

                            'lead_source',
                            'channel',
                            'campaign_name',

                            'source_description',
                            'facebook',
                            'reference',

                            'utm_source',
                            'utm_medium',
                            'utm_content',

                            array (
                                'name' => 'date_performed',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_PERFORMED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_performed',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'performed_by_name',
                                    ),
                                ),
                            ),
                            'utm_agent',
                            'assigned_user_name',

                            array (
                                'name' => 'date_entered_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_ENTERED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_entered',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'created_by_name',
                                    ),
                                ),
                            ),
                            array (
                                'name' => 'date_modified_by',
                                'readonly' => true,
                                'inline' => true,
                                'type' => 'fieldset',
                                'label' => 'LBL_DATE_MODIFIED',
                                'fields' =>
                                array (
                                    0 =>
                                    array (
                                        'name' => 'date_modified',
                                    ),
                                    1 =>
                                    array (
                                        'type' => 'label',
                                        'default_value' => 'LBL_BY',
                                    ),
                                    2 =>
                                    array (
                                        'name' => 'modified_by_name',
                                    ),
                                ),
                            ),
                            'team_name'
                        ),
                    ),
                ),
                'templateMeta' =>
                array (
                    'useTabs' => false,
                ),
            ),
        ),
    ),
);
