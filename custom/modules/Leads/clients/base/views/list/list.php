<?php
$viewdefs['Leads'] =
array (
  'base' =>
  array (
    'view' =>
    array (
      'list' =>
      array (
        'panels' =>
        array (
          0 =>
          array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
              0 =>
              array (
                'name' => 'name',
                'type' => 'fullname',
                'fields' =>
                array (
                  0 => 'salutation',
                  1 => 'first_name',
                  2 => 'last_name',
                ),
                'link' => true,
                'label' => 'LBL_LIST_NAME',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
              ),
              1 =>
              array (
                'name' => 'phone_mobile',
                'label' => 'LBL_MOBILE_PHONE',
                'enabled' => true,
                'default' => true,
                'width' => 'large',
              ),
              2 =>
              array (
                'name' => 'class_activity',
                'label' => 'LBL_CLASS_ACTIVITY',
                'enabled' => true,
                'default' => true,
                'type' => 'html',
                'width' => '500px',
              ),
              3 =>
              array (
                'name' => 'email',
                'label' => 'LBL_LIST_EMAIL_ADDRESS',
                'enabled' => true,
                'default' => true,
              ),
              4 =>
              array (
                'name' => 'status',
                'label' => 'LBL_LIST_STATUS',
                'type' => 'event-status',
                'link' => false,
                'default' => true,
                'enabled' => true,
                'css_class' => 'full-width',
              ),
              5 =>
              array (
                'name' => 'lead_source',
                'label' => 'LBL_LEAD_SOURCE',
                'enabled' => true,
                'default' => true,
              ),
              6 =>
              array (
                'name' => 'channel',
                'label' => 'LBL_CHANNEL',
                'enabled' => true,
                'default' => true,
              ),
              7 =>
              array (
                'name' => 'campaign_name',
                'label' => 'LBL_CAMPAIGN',
                'enabled' => true,
                'id' => 'CAMPAIGN_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              8 =>
              array (
                'name' => 'utm_agent',
                'label' => 'LBL_UTM_AGENT',
                'enabled' => true,
                'id' => 'UTM_AGENT_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              9 =>
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_LIST_ASSIGNED_USER',
                'enabled' => true,
                'default' => true,
              ),
              10 =>
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAMS',
                'enabled' => true,
                'id' => 'TEAM_ID',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              11 =>
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => true,
              ),
              12 =>
              array (
                'name' => 'date_entered',
                'label' => 'LBL_DATE_ENTERED',
                'enabled' => true,
                'default' => true,
                'readonly' => true,
              ),
              13 =>
              array (
                'name' => 'grade',
                'label' => 'LBL_GRADE',
                'enabled' => true,
                'default' => false,
              ),
              14 =>
              array (
                'name' => 'school_name',
                'label' => 'LBL_SCHOOL_NAME',
                'enabled' => true,
                'id' => 'SCHOOL_ID',
                'link' => true,
                'sortable' => false,
                'default' => false,
              ),
              15 =>
              array (
                'name' => 'performed_by_name',
                'label' => 'LBL_PERFORMED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'PERFORMED_USER_ID',
                'link' => true,
                'default' => false,
              ),
              16 =>
              array (
                'name' => 'date_performed',
                'label' => 'LBL_DATE_PERFORMED',
                'enabled' => true,
                'readonly' => true,
                'default' => false,
              ),
              17 =>
              array (
                'name' => 'phone_guardian',
                'label' => 'LBL_PHONE_GUARDIAN',
                'enabled' => true,
                'default' => false,
              ),
              18 =>
              array (
                'name' => 'prefer_level',
                'label' => 'LBL_PREFER_LEVEL',
                'enabled' => true,
                'default' => false,
              ),
              19 =>
              array (
                'name' => 'guardian_name_2',
                'label' => 'LBL_GUARDIAN_NAME_2',
                'enabled' => true,
                'default' => false,
              ),
              20 =>
              array (
                'name' => 'gender',
                'label' => 'LBL_GENDER',
                'enabled' => true,
                'default' => false,
              ),
              21 =>
              array (
                'name' => 'other_mobile',
                'label' => 'LBL_OTHER_MOBILE',
                'enabled' => true,
                'default' => false,
              ),
              22 =>
              array (
                'name' => 'assistant',
                'label' => 'LBL_ASSISTANT',
                'enabled' => true,
                'default' => false,
              ),
              23 =>
              array (
                'name' => 'primary_address_street',
                'label' => 'LBL_PRIMARY_ADDRESS_STREET',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              24 =>
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              25 =>
              array (
                'name' => 'created_by_name',
                'label' => 'LBL_CREATED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'CREATED_BY',
                'link' => true,
                'default' => false,
              ),
              26 =>
              array (
                'name' => 'modified_by_name',
                'label' => 'LBL_MODIFIED',
                'enabled' => true,
                'readonly' => true,
                'id' => 'MODIFIED_USER_ID',
                'link' => true,
                'default' => false,
              ),
              27 =>
              array (
                'name' => 'guardian_name',
                'label' => 'LBL_GUARDIAN_NAME',
                'enabled' => true,
                'default' => false,
              ),
              28 =>
              array (
                'name' => 'birthdate',
                'label' => 'LBL_BIRTHDATE',
                'enabled' => true,
                'default' => false,
              ),
              29 =>
              array (
                'name' => 'age',
                'label' => 'LBL_AGE',
                'enabled' => true,
                'default' => false,
              ),
              30 =>
              array (
                'name' => 'source_description',
                'label' => 'LBL_SOURCE_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              31 =>
              array (
                'name' => 'facebook',
                'label' => 'LBL_FACEBOOK',
                'enabled' => true,
                'default' => false,
              ),
              32 =>
              array (
                'name' => 'phone_other',
                'label' => 'LBL_OTHER_PHONE',
                'enabled' => true,
                'default' => false,
              ),
              33 =>
              array (
                'name' => 'reference',
                'label' => 'LBL_REFERENCE',
                'enabled' => true,
                'default' => false,
              ),
              34 =>
              array (
                'name' => 'utm_medium',
                'label' => 'LBL_UTM_MEDIUM',
                'enabled' => true,
                'default' => false,
              ),
              35 =>
              array (
                'name' => 'utm_source',
                'label' => 'LBL_UTM_SOURCE',
                'enabled' => true,
                'default' => false,
              ),
              36 =>
              array (
                'name' => 'utm_content',
                'label' => 'LBL_UTM_CONTENT',
                'enabled' => true,
                'default' => false,
              ),
                37 =>
                array (
                    'name' => 'eng_level',
                    'label' => 'LBL_ENG_LEVEL',
                    'enabled' => true,
                    'default' => false,
                ),
                38 =>
                array (
                    'name' => 'target',
                    'label' => 'LBL_TARGET',
                    'enabled' => true,
                    'default' => false,
                ),
                39 =>
                array (
                    'name' => 'object',
                    'label' => 'LBL_OBJECT',
                    'enabled' => true,
                    'default' => false,
                ),
            ),
          ),
        ),
        'orderBy' =>
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
