<?php
// created: 2019-03-17 13:31:35
$viewdefs = array (
  'Leads' =>
  array (
    'base' =>
    array (
      'layout' =>
      array (
        'convert-main' =>
        array (
          'modules' =>
          array (
            0 =>
            array (
              'copyData' => true,
              'required' => true,
              'moduleName' => 'Contacts',
              'module' => 'Contacts',
              'duplicateCheckOnStart' => true,
            ),
          ),
        ),
      ),
    ),
  ),
);