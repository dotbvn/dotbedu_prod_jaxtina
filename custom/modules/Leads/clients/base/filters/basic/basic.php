<?php
//Edit LBL filter by nnamnguyen
    $viewdefs['Leads']['base']['filter']['basic'] = array(
        'create' => true,
        'quicksearch_field' => array('name'),
        'quicksearch_priority' => 1,
        'quicksearch_split_terms' => true,
        'filters' => array(
            array(
                'id' => 'my_new',
                'name' => 'LBL_NEWLEAD',

                'filter_definition' => array(
                    array('status' => array('$in' => array('New'))),
                    array('$owner' => ''),

                ),
                'is_template' => true,
                'order' => 1,
                'orderBy' => array(
                    'field' => 'date_performed',
                    'direction' => 'desc'
                ),
            ),
            array(
                'id' => 'my_pt_demo_list',
                'name' => 'LBL_PTDEMO',
                'filter_definition' => array(
                    array('last_pt_status' => array('$in' => array('Registered'))),
                    array('last_pt_date' => array('$dateRange' => 'today')),
                    array('status' => array('$not_in' => array('Converted', 'Dead', 'New'))),
                    array('$owner' => '')
                ),
                'is_template' => true,
                'order' => 2,
                'orderBy' => array(
                    'field' => 'last_pt_date',
                    'direction' => 'asc'
                ),
            ),
            array(
                'id' => 'my_schedule_list',
                'name' => 'LBL_SCHEDULELIST',
                'filter_definition' => array(
                    array('last_call_status' => array('$in' => array('Planned'))),
                    array('last_call_date' => array('$dateRange' => 'today')),
                    array('status' => array('$not_in' => array('Converted', 'Dead', 'New'))),
                    array('$owner' => ''),

//                    array('last_pt_status' => array('$not_in' => array('Registered'))),
//                    array('last_pt_date' => array('$dateRange' => 'before_today')),
//                    array('last_pt_date' => array('$dateRange' => 'after_today')),
                ),
                'is_template' => true,
                'order' => 3,
                'orderBy' => array(
                    'field' => 'last_call_date',
                    'direction' => 'asc'
                ),
            ),
            array(
                'id' => 'my_no_schedule_list',
                'name' => 'LBL_NOTSCHEDULELIST',
                'filter_definition' => array(

                    array('last_call_date' => array('$dateRange' => 'before_today')),
                    array('last_call_date' => array('$dateRange' => 'after_today')),
                    array('date_modified' => array('$lt' => 'last_7_days')),
                    array('status' => array('$not_in' => array('Converted', 'Dead', 'New'))),
                    array('$owner' => ''),

                ),
                'is_template' => true,
                'order' => 4,
                'orderBy' => array(
                    'field' => 'date_modified',
                    'direction' => 'desc'
                ),
            ),
            array(
                'id' => 'my_no_answer',
                'name' => 'LBL_NOANSWER',
                'filter_definition' => array(
                    array('last_call_result' => array('$in' => array('Busy/No Answer', 'Invalid Number', 'Deny'))),
                    array('status' => array('$not_in' => array('Converted', 'Dead', 'New'))),
                    array('$owner' => ''),

                //    array('group_source' => array('')),
                ),
                'is_template' => true,
                'order' => 5,
                'orderBy' => array(
                    'field' => 'last_pt_date',
                    'direction' => 'asc'
                ),
            ),
            array(
                'id' => 'lead_no_call_in_30days',
                'name' => 'LBL_LEADNOCALL',
                'filter_definition' => array(
                    array('last_call_date' => array('$dateRange' => 'before_last_30_days')),
                    array('status' => array('$not_in' => array('Converted', 'Dead', 'New'))),
                ),
                'is_template' => true,
                'order' => 6,
                'orderBy' => array(
                    'field' => 'last_call_date',
                    'direction' => 'asc'
                ),
            ),
            array(
                'id' => 'favorites',
                'name' => 'LBL_FAVORITES',
                'filter_definition' => array(
                    '$favorite' => '',
                ),
                'editable' => false,
                'order' => 6,
            ),
            array(
                'id' => 'all_records',
                'name' => 'LBL_LISTVIEW_FILTER_ALL',
                'filter_definition' => array(),
                'editable' => false,
                'order' => 7,
            ),
        ),
    );
