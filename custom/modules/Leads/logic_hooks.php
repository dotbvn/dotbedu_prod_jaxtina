<?php
/**
 * Add by Tuan Anh
 */
$hook_version = 1;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(2, 'Save full name', 'custom/modules/Leads/logicLead.php', 'logicLead', 'saveFullName');

$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1, 'Handle CJ After Save', 'custom/modules/Leads/logicCJLead.php', 'logicCJLead', 'handleCJAfterSave');
$hook_array['after_save'][] = Array(2, 'After save', 'custom/modules/Leads/logicLead.php', 'logicLead', 'handleAfterSave');

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(1, 'Handle delete lead', 'custom/modules/Leads/logicLead.php', 'logicLead', 'handleDelete');

$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(1, 'Color Status', 'custom/modules/Leads/logicLead.php','logicLead', 'listviewcolor');
$hook_array['process_record'][] = Array(2, 'Contact Activity', 'custom/modules/Contacts/logicContact.php','logicContact', 'contactActivity');

$hook_array['before_import'] = Array();
$hook_array['before_import'][] = Array(1, 'Handle Before Import', 'custom/modules/Leads/logicLeadImport.php','logicLeadImport', 'handleBeforeImport');
?>
