<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicCJLead
{
    function handleCJAfterSave(&$bean, $event, $arguments){
        // Auto-complete Customer Journey Task - Add by phgiahannn
        global $dotb_config;
        $enabled_mods = $dotb_config['additional_js_config']['customer_journey']['enabled_modules'];
        if (!empty($enabled_mods)) {
            if ($bean->status == 'Dead') markDeadLead($bean);
            elseif (hasInProgressCJ($bean->module_dir, $bean->id))
                autoCompleteCJTask($bean);
        }
    }
}


?>