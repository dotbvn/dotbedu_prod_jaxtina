<?php
// created: 2019-04-11 11:54:05
$subpanel_layout['list_fields'] = array (
  'name' =>
  array (
    'vname' => 'LBL_LIST_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'sort_order' => 'asc',
    'sort_by' => 'last_name',
    'module' => 'Leads',
    'width' => 10,
    'default' => true,
  ),
  'birthdate' =>
  array (
    'type' => 'date',
    'vname' => 'LBL_BIRTHDATE',
    'width' => 10,
    'default' => true,
  ),
  'guardian_name' =>
  array (
    'type' => 'varchar',
    'vname' => 'LBL_GUARDIAN_NAME',
    'width' => 10,
    'default' => true,
  ),
  'phone_mobile' =>
  array (
    'type' => 'phone',
    'vname' => 'LBL_MOBILE_PHONE',
    'width' => 10,
    'default' => true,
  ),
  'status' =>
  array (
      //Add By Tuan Anh to display status color
      'widget_class' => 'SubPanelActivitiesStatusField',
    'default' => true,
    'vname' => 'LBL_STATUS',
    'width' => 10,
  ),
  'lead_source' =>
  array (
    'vname' => 'LBL_LIST_LEAD_SOURCE',
    'width' => 10,
    'default' => true,
  ),
  'campaign_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CAMPAIGN',
    'id' => 'CAMPAIGN_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Campaigns',
    'target_record_key' => 'campaign_id',
  ),
  'assigned_user_name' =>
  array (
    'name' => 'assigned_user_name',
    'vname' => 'LBL_LIST_ASSIGNED_TO_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'target_record_key' => 'assigned_user_id',
    'target_module' => 'Employees',
    'width' => 10,
    'default' => true,
  ),
  'date_entered' =>
  array (
    'type' => 'datetime',
    'studio' =>
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
  ),
  'team_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'studio' =>
    array (
      'portallistview' => false,
      'portalrecordview' => false,
    ),
    'vname' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Teams',
    'target_record_key' => 'team_id',
  ),
  'first_name' =>
  array (
    'usage' => 'query_only',
  ),
  'last_name' =>
  array (
    'usage' => 'query_only',
  ),
  'salutation' =>
  array (
    'name' => 'salutation',
    'usage' => 'query_only',
  ),
);