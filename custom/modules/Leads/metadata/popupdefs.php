<?php
$popupMeta = array (
    'moduleMain' => 'Lead',
    'varName' => 'LEAD',
    'orderBy' => 'last_name, first_name',
    'whereClauses' => array (
  'lead_source' => 'leads.lead_source',
  'assigned_user_id' => 'leads.assigned_user_id',
  'full_lead_name' => 'leads.full_lead_name',
  'phone_mobile' => 'leads.phone_mobile',
  'guardian_name' => 'leads.guardian_name',
  'email' => 'leads.email',
  'campaign_name' => 'leads.campaign_name',
  'team_name' => 'leads.team_name',
),
    'searchInputs' => array (
  2 => 'lead_source',
  5 => 'assigned_user_id',
  6 => 'full_lead_name',
  7 => 'phone_mobile',
  8 => 'guardian_name',
  9 => 'email',
  10 => 'campaign_name',
  11 => 'team_name',
),
    'searchdefs' => array (
  'full_lead_name' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_FULL_LEAD_NAME',
    'width' => 10,
    'name' => 'full_lead_name',
  ),
  'phone_mobile' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => 10,
    'name' => 'phone_mobile',
  ),
  'lead_source' => 
  array (
    'name' => 'lead_source',
    'width' => '10',
  ),
  'guardian_name' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_GUARDIAN_NAME',
    'width' => 10,
    'name' => 'guardian_name',
  ),
  'email' => 
  array (
    'name' => 'email',
    'width' => 10,
  ),
  'campaign_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CAMPAIGN',
    'id' => 'CAMPAIGN_ID',
    'width' => 10,
    'name' => 'campaign_name',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'type' => 'enum',
    'label' => 'LBL_ASSIGNED_TO',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10',
  ),
  'team_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'portallistview' => false,
      'portalrecordview' => false,
    ),
    'label' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => 10,
    'name' => 'team_name',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
    'name' => 'name',
  ),
  'GUARDIAN_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_GUARDIAN_NAME',
    'width' => 10,
    'default' => true,
    'name' => 'guardian_name',
  ),
  'STATUS' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_STATUS',
    'default' => true,
    'name' => 'status',
  ),
  'BIRTHDATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_BIRTHDATE',
    'width' => 10,
    'default' => true,
    'name' => 'birthdate',
  ),
  'PHONE_MOBILE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => 10,
    'default' => true,
    'name' => 'phone_mobile',
  ),
  'LEAD_SOURCE' => 
  array (
    'width' => 10,
    'label' => 'LBL_LEAD_SOURCE',
    'default' => true,
    'name' => 'lead_source',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
  'TEAM_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'portallistview' => false,
      'portalrecordview' => false,
    ),
    'label' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => 10,
    'default' => true,
    'name' => 'team_name',
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'label' => 'LBL_DATE_ENTERED',
    'width' => 10,
    'default' => true,
    'name' => 'date_entered',
  ),
),
);
