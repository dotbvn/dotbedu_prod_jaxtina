<?php
require_once('custom/include/_helper/junior_class_utils.php');
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicLead
{
    public function saveFullName(&$bean, $event, $arguments)
    {
        //save full name
        $bean->last_name = mb_convert_case(trim(preg_replace('/\s+/', ' ', str_replace(['ㅤ', 'ᅠ'], '', $bean->last_name))), MB_CASE_TITLE, "UTF-8");
        $bean->first_name = mb_convert_case(trim(preg_replace('/\s+/', ' ', str_replace(['ㅤ', 'ᅠ'], '', $bean->first_name))), MB_CASE_TITLE, "UTF-8");
        $bean->full_lead_name = trim($bean->last_name . ' ' . $bean->first_name);
        $bean->phone_mobile = formatPhoneNumber($bean->phone_mobile);
        $bean->phone_guardian = formatPhoneNumber($bean->phone_guardian);
        $bean->other_mobile = formatPhoneNumber($bean->other_mobile);
        //save age and update date&month filter
        if (!empty($bean->birthdate)) {
            $diff = date_diff(date_create($bean->birthdate), date_create(date("Y-m-d")));
            $bean->age = $diff->format('%y');
            $bean->birth_day = date('d', strtotime($bean->birthdate));
            $bean->birth_month = date('m', strtotime($bean->birthdate));
        }
        $bean->assistant = viToEn($bean->full_lead_name);
        //handle add (Text)
        $bean->team_name_text = $GLOBALS['db']->getOne("SELECT name FROM teams WHERE id='{$bean->team_id}'");


        //Xu ly Import user_name
        if ($_POST['module'] == 'Import') {
            $user_id = $GLOBALS['db']->getOne("SELECT id FROM users WHERE user_name = '{$bean->assigned_user_name}' AND deleted = 0");
            if (!empty($user_id)) {
                $bean->assigned_user_id = $user_id;
            }
            // nếu import mà để trống status -> mặc định là 'New'
            if (empty($bean->status))
                $bean->status = 'New';
        }

        if (!empty($_REQUEST['__dotb_url'])) {
            $arrayUrl = explode('/', $_REQUEST['__dotb_url']);
            $action = $arrayUrl[sizeof($arrayUrl) - 1];
            $module = $arrayUrl[sizeof($arrayUrl) - 2];
        }
        if ($module == $bean->module_name && $action == 'MassUpdate') {
            // Mass update auto converted to contact /////////
            if ($bean->converted || ($bean->fetched_row['status']) != $bean->status && $bean->status == 'Converted') {
                //Check dulicate
                $q1 = "SELECT DISTINCT
                IFNULL(id, '') primaryid
                FROM contacts
                WHERE (assistant = '{$bean->assistant}') AND (RIGHT(phone_mobile,9) = '" . substr($bean->phone_mobile, -9) . "') AND contacts.deleted = 0";
                $contact_id = $GLOBALS['db']->getOne($q1);
                if (empty($bean->contact_id) && empty($contact_id)) {
                    $contact = new Contact();
                    foreach ($contact->field_defs as $keyField => $aFieldName)
                        $contact->$keyField = $bean->$keyField;
                    $contact->status = 'Converted';
                    $contact->id = '';
                    $contact->save();
                    $bean->contact_id = $contact->id;
                } else $bean->contact_id = $contact_id;
            }
        }
        //xử lí bàn giao
        if ($bean->fetched_row['assigned_user_id'] != $bean->assigned_user_id) {
            $bean->performed_user_id = $bean->modified_user_id;
            $bean->date_performed = $bean->date_modified;
        }
        // update converted
        if (!empty($bean->contact_id)) {
            $bean->status = 'Converted';
            $bean->converted = 1;

            //Copy Log Call
            $GLOBALS['db']->query("UPDATE calls SET parent_id = '{$bean->contact_id}', parent_type = 'Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0");

            $GLOBALS['db']->query("INSERT INTO calls_contacts (id, call_id, contact_id, required, accept_status, date_modified, deleted)
                SELECT id, id, '{$bean->contact_id}', 1, 'none', date_modified, deleted FROM calls WHERE parent_id = '{$bean->contact_id}' AND deleted = 0;");

            $GLOBALS['db']->query("DELETE FROM calls_leads WHERE lead_id ='{$bean->id}'");

            //Copy Task
            $GLOBALS['db']->query("UPDATE tasks SET parent_id = '{$bean->contact_id}', parent_type = 'Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0");

            //Copy Session
            $GLOBALS['db']->query("INSERT INTO meetings_contacts (id, meeting_id, contact_id, required, accept_status, date_modified, deleted, situation_id)
                SELECT UUID() id, mt.meeting_id, '{$bean->contact_id}' contact_id, mt.required, mt.accept_status, mt.date_modified, mt.deleted, mt.situation_id FROM meetings_leads mt
                INNER JOIN j_studentsituations st ON mt.situation_id = st.id AND st.deleted = 0
                WHERE mt.lead_id = '{$bean->id}' AND mt.deleted = 0");

            //Copy situation Demo
            $GLOBALS['db']->query("UPDATE j_studentsituations SET student_type='Contacts', student_id = '{$bean->contact_id}' WHERE student_id = '{$bean->id}' AND deleted = 0");
            //update classStudent
            $GLOBALS['db']->query("UPDATE j_classstudents SET student_type='Contacts', student_id = '{$bean->contact_id}' WHERE student_id = '{$bean->id}' AND deleted = 0");

            $GLOBALS['db']->query("DELETE FROM meetings_leads WHERE lead_id ='{$bean->id}'");

            //Copy Meeting
            $GLOBALS['db']->query("UPDATE meetings SET parent_id = '{$bean->contact_id}', parent_type = 'Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0 AND meeting_type = 'Meeting'");

            //Copy Picture
            if(!empty($bean->picture))
                $GLOBALS['db']->query("UPDATE contacts SET picture = '{$bean->picture}' WHERE id = '{$bean->contact_id}' AND deleted = 0 AND (picture='' OR picture IS NULL)");

            //Copy PT/Demo
            $bean->load_relationship('j_ptresults');
            $pt_id = $bean->j_ptresults->get();
            for ($i = 0; $i < count($pt_id); $i++) {
                $pt = BeanFactory::getBean('J_PTResult', $pt_id[$i]);
                if ($pt->id) {
                    $pt->parent = "Contacts";
                    $pt->student_id = $bean->contact_id;
                    $pt->save();
                }
            }
            //Copy Payment & Receipt
            $GLOBALS['db']->query("UPDATE j_payment SET parent_id='{$bean->contact_id}', parent_type='Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0");
            $GLOBALS['db']->query("UPDATE j_paymentdetail SET parent_id='{$bean->contact_id}', parent_type='Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0");

            //Copy Attendance
            $GLOBALS['db']->query("UPDATE c_attendance SET student_type='Contacts', student_id = '{$bean->contact_id}', date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE student_id = '{$bean->id}' AND deleted = 0 ");

            $update = "UPDATE contacts SET is_converted_from_lead = 1 WHERE id = '{$bean->contact_id}' AND deleted = 0 ";
            $GLOBALS['db']->query($update);

            //Copy Reference
            $GLOBALS['db']->query("UPDATE j_referencelog SET parent_id = '{$bean->contact_id}', parent_type = 'Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0");

            //Siblings
            $GLOBALS['db']->query("UPDATE c_siblings SET parent_id = '{$bean->contact_id}', parent_type = 'Contacts' WHERE parent_id = '{$bean->id}' AND deleted = 0");
            $GLOBALS['db']->query("UPDATE c_siblings SET student_id = '{$bean->contact_id}' WHERE student_id = '{$bean->id}' AND deleted = 0");

            // Copy Customer Journey
            $GLOBALS['db']->query("UPDATE dri_workflows SET contact_id = '{$bean->contact_id}' WHERE lead_id = '{$bean->id}' AND deleted = 0");
        }
        if($bean->modified_by_name == 'apps_admin')   //apps_admin
            saveReference($bean);

    }

    public function handleDelete(&$bean, $event, $arguments)
    {

        if ($bean->load_relationship('prospect')) {
            $prospect = $bean->prospect->getBeans();
            $prospect = reset($prospect);
            if (!empty($prospect)) {
                $prospect_id = $prospect->id;
                $prospect->status = 'In Process';
                $prospect->converted = 0;
                $prospect->lead_id = '';
                $prospect->save();

                //revert Lead --> target
                //Update call
                $sql_update_call = "UPDATE calls
                SET parent_type = 'Prospects', parent_id = '{$prospect_id}'
                WHERE parent_id = '{$bean->id}' AND deleted = 0";
                $GLOBALS['db']->query($sql_update_call);
                //Delete calls_leads
                $sql_delete_call = "DELETE FROM calls_leads WHERE lead_id = '{$bean->id}' AND deleted = 0";
                $GLOBALS['db']->query($sql_delete_call);

                //Update meeting
                $sql_update_meeting = "UPDATE meetings
                SET parent_type = 'Prospects' , parent_id = '{$prospect_id}'
                WHERE parent_id = '{$bean->id}' AND deleted = 0 AND schedule_type='Meeting'";
                $GLOBALS['db']->query($sql_update_meeting);
                //Delete meetings_leads
                $sql_delete_meeting = "DELETE FROM meetings_leads WHERE lead_id = '{$bean->id}'";
                $GLOBALS['db']->query($sql_delete_meeting);

                //Update Task
                $sql_update_task = "UPDATE tasks
                SET parent_type = 'Prospects' , parent_id = '{$prospect_id}'
                WHERE parent_id ='{$bean->id}' AND deleted = 0";
                $GLOBALS['db']->query($sql_update_task);

                //Siblings
                $GLOBALS['db']->query("UPDATE c_siblings SET parent_id = '$prospect_id', parent_type = 'Prospects' WHERE parent_id = '{$bean->id}' AND deleted = 0");
                $GLOBALS['db']->query("UPDATE c_siblings SET student_id = '$prospect_id' WHERE student_id = '{$bean->id}' AND deleted = 0");
            }
        }
        //Delete PT/Demo
        $sql1 = "DELETE FROM j_ptresult WHERE deleted=0 AND parent = 'Leads' AND student_id = '{$bean->id}'";
        $GLOBALS['db']->query($sql1);

    }

    function listviewcolor(&$bean, $event, $arguments)
    {
        if ($_REQUEST["action"] == "Popup") {
            $colorClass = '';
            switch ($bean->status) {
                case 'New':
                    $colorClass = "textbg_green";
                    break;
                case 'Ready to PT':
                case 'Ready to Demo':
                case 'Appointment':
                    $colorClass = "textbg_bluelight";
                    break;
                case 'In Process':
                case 'Deposit':
                    $colorClass = "textbg_blue";
                    break;
                case 'Converted':
                    $colorClass = "textbg_red";
                    break;
                case 'PT/Demo':
                    $colorClass = "textbg_violet";
                    break;
                case 'Dead':
                    $colorClass = "textbg_black";
                    break;
                default :
                    $colorClass = "No_color";
            }
            $tmp_status = translate('lead_status_dom', '', $bean->status);
            $bean->status = "<span class=$colorClass >$tmp_status</span>";
        }
    }

    function handleAfterSave(&$bean, $event, $arguments)
    {
        require_once('include/externalAPI/ClassIn/utils.php');
        // CLASSIN
        // Xử lý trường hợp lead đã được đồng bộ qua ClassIn sau đó update phone_mobile của lead
        if (ExtAPIClassIn::hasAPIConfig() == true) {
            //Đã có đăng ký classin rồi thì SDT có được update sẽ đăng ký lại SDT mới
            if ($arguments['isUpdate'] == true && !empty($bean->onl_uid)) {
                $phone_mobile = !empty($arguments['dataChanges']['phone_mobile']) ? $arguments['dataChanges']['phone_mobile'] : '';
                $team_id = !empty($arguments['dataChanges']['team_id']) ? $arguments['dataChanges']['team_id'] : '';
                $full_lead_name = !empty($arguments['dataChanges']['full_lead_name']) ? $arguments['dataChanges']['full_lead_name'] : '';

                if (!empty($phone_mobile) && $phone_mobile['before'] !== $phone_mobile['after']) {
                    //stored old classin uid
                    $oldClassInUid = $bean->onl_uid;
                    // create new ClassIn account with new phone mobile number
                    $bean->classin_nickname = formatCINickName($bean);
                    $r = registerClassInStudent($bean);
                    // xử lý cập nhật thay đổi sdt của học sinh trong các course ClassIn
                    // remove sdt cũ khỏi course, đồng thời thêm sdt mới vào course
                    // remove sdt cũ khỏi các lesson đã tạo ClassIn, đồng thời thêm sdt mới vào các lesson đó
                    if ($r){
                        //Lấy ClassIn uid mới bằng câu query
                        $newClassInUid = $GLOBALS['db']->getOne("SELECT onl_uid FROM leads WHERE id = '{$bean->id}'");
                        $bean->classin_nickname = formatCINickName($bean);
                        updateStudentPhoneInClassIn($bean, $newClassInUid, $oldClassInUid);
                    }
                    return;
                }
                //Thay đổi team hoặc tên học sinh thì chỉ cần update nickname
                if (!empty($team_id) || !empty($full_lead_name)){
                    $nickname = formatCINickName($bean);
                    editStudentName($bean->onl_uid, $nickname);
                }
            }

        }
    }
}
