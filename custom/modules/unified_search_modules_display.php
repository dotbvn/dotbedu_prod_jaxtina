<?php
// created: 2023-06-07 11:53:37
$unified_search_modules_display = array (
  '' =>
  array (
    'visible' => true,
  ),
  'Accounts' =>
  array (
    'visible' => false,
  ),
  'Bugs' =>
  array (
    'visible' => false,
  ),
  'EMS_Settings' =>
  array (
    'visible' => false,
  ),
  'C_Attendance' =>
  array (
    'visible' => false,
  ),
  'C_Commission' =>
  array (
    'visible' => false,
  ),
  'C_Contacts' =>
  array (
    'visible' => false,
  ),
  'C_DeliveryRevenue' =>
  array (
    'visible' => false,
  ),
  'C_Rooms' =>
  array (
    'visible' => false,
  ),
  'C_SMS' =>
  array (
    'visible' => false,
  ),
  'C_Teachers' =>
  array (
    'visible' => false,
  ),
  'C_Timesheet' =>
  array (
    'visible' => false,
  ),
  'Calls' =>
  array (
    'visible' => false,
  ),
  'Campaigns' =>
  array (
    'visible' => false,
  ),
  'Cases' =>
  array (
    'visible' => false,
  ),
  'Contacts' =>
  array (
    'visible' => false,
  ),
  'Contracts' =>
  array (
    'visible' => false,
  ),
  'DRI_SubWorkflow_Templates' =>
  array (
    'visible' => false,
  ),
  'DRI_Workflow_Task_Templates' =>
  array (
    'visible' => false,
  ),
  'DRI_Workflow_Templates' =>
  array (
    'visible' => false,
  ),
  'Documents' =>
  array (
    'visible' => false,
  ),
  'J_Budget' =>
  array (
    'visible' => false,
  ),
  'J_ConfigInvoiceNo' =>
  array (
    'visible' => false,
  ),
  'J_Coursefee' =>
  array (
    'visible' => false,
  ),
  'J_Discount' =>
  array (
    'visible' => false,
  ),
  'J_Feedback' =>
  array (
    'visible' => false,
  ),
  'J_Gradebook' =>
  array (
    'visible' => false,
  ),
  'J_GradebookConfig' =>
  array (
    'visible' => false,
  ),
  'J_GradebookDetail' =>
  array (
    'visible' => false,
  ),
  'J_Inventory' =>
  array (
    'visible' => false,
  ),
  'J_Inventorydetail' =>
  array (
    'visible' => false,
  ),
  'J_Invoice' =>
  array (
    'visible' => false,
  ),
  'J_Kindofcourse' =>
  array (
    'visible' => false,
  ),
  'J_Loyalty' =>
  array (
    'visible' => false,
  ),
  'J_PTResult' =>
  array (
    'visible' => false,
  ),
  'J_Payment' =>
  array (
    'visible' => false,
  ),
  'J_PaymentDetail' =>
  array (
    'visible' => false,
  ),
  'J_School' =>
  array (
    'visible' => false,
  ),
  'J_Sponsor' =>
  array (
    'visible' => false,
  ),
  'J_StudentSituations' =>
  array (
    'visible' => false,
  ),
  'J_Targetconfig' =>
  array (
    'visible' => false,
  ),
  'J_Teachercontract' =>
  array (
    'visible' => false,
  ),
  'J_Voucher' =>
  array (
    'visible' => false,
  ),
  'KBContents' =>
  array (
    'visible' => false,
  ),
  'Leads' =>
  array (
    'visible' => false,
  ),
  'Manufacturers' =>
  array (
    'visible' => false,
  ),
  'Meetings' =>
  array (
    'visible' => false,
  ),
  'Notes' =>
  array (
    'visible' => false,
  ),
  'Opportunities' =>
  array (
    'visible' => false,
  ),
  'ProductCategories' =>
  array (
    'visible' => false,
  ),
  'Products' =>
  array (
    'visible' => false,
  ),
  'ProspectLists' =>
  array (
    'visible' => false,
  ),
  'Prospects' =>
  array (
    'visible' => false,
  ),
  'Quotes' =>
  array (
    'visible' => false,
  ),
  'RevenueLineItems' =>
  array (
    'visible' => false,
  ),
  'Tasks' =>
  array (
    'visible' => false,
  ),
  'fte_UsageTracking' =>
  array (
    'visible' => false,
  ),
);