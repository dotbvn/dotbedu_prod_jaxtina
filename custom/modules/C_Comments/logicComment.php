<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class logicComment{
    function handleBeforeSave(&$bean, $event, $arguments){
        if (!empty($bean->attachment_list)) {
            foreach($bean->attachment_list as $note_object){
                if($note_object['action'] == 'add' && !empty($note_object['id'])) {
                    $noteAdd = BeanFactory::getBean('Notes', $note_object['id']);
                    $noteAdd->parent_id = $bean->id;
                    $noteAdd->save();
                } else {
                    $noteDel = BeanFactory::getBean('Notes', $note_object['id']);
                    $noteDel->mark_deleted($note_object['id']);
                    $noteDel->save();
                }
            }
        }
        if (isset($_SESSION['type']) && $_SESSION['type'] == 'support_portal') {
            //Set for student portal
            $sqlCount = "SELECT COUNT(1) 
                FROM c_comments 
                WHERE parent_id = '$bean->parent_id' AND parent_type = 'Cases' AND deleted = 0";

            $result_count = $GLOBALS['db']->getOne($sqlCount);
            if($result_count == 0 || $bean->first_created_by_portal && $result_count == 1) {
                $admin_user = new User();
                $admin_user->retrieve_by_string_fields(array('is_admin' => 1));
                $bean->direction = 'outbound';
                $bean->created_by = $admin_user->id;
                $bean->first_created_by_portal = true;
            } else {
                $bean->direction = 'inbound';
            }
        }
        //Create Record
        if(empty($bean->fetched_row)){
            if($bean->direction == 'outbound') $bean->is_read_inapp = 0;
            elseif($bean->direction == 'inbound') $bean->is_read_inems = 0;
            //update field
            $GLOBALS['db']->query("UPDATE cases
                SET last_comment_date='{$bean->date_entered}',
                date_modified='{$bean->date_entered}'
                WHERE id = '{$bean->parent_id}' AND deleted = 0");
        }
        $bean->commitAuditedStateChanges(null);
    }
    function handleAfterSave(&$bean, $event, $arguments){
        if($bean->parent_type == 'Cases') {
            if($bean->direction == 'inbound'){
                $key = '/loadBaseMessage/'.$bean->parent_id;
                $sqlContactsName = "SELECT IFNULL(full_student_name,'') full_student_name 
                                    FROM contacts 
                                    INNER JOIN contacts_cases_1_c ON contacts.id = contacts_cases_1_c.contacts_cases_1contacts_ida
                                    WHERE contacts_cases_1_c.contacts_cases_1cases_idb = '{$bean->parent_id}' AND contacts.deleted = 0";
                $personName = $GLOBALS['db']->getOne($sqlContactsName);;
            } else {
                $key = '/loadPortalMessage/'.$bean->parent_id;
                $sqlUserName = "SELECT IFNULL(full_user_name,'') full_user_name FROM users WHERE id = '{$bean->created_by}'";
                $personName = $GLOBALS['db']->getOne($sqlUserName);
            }
            $dateEntered = TimeDate::getInstance()->fromDb($bean->date_entered);
            $formatted_attachment_list = $this->getFile($bean);
            $client = new Client();
            $url = 'https://socket.dotb.cloud/send-event-phenikaa';
            $headers = [
                'Content-Type' => 'application/json'
            ];
            $body = '{
              "to": "' .$GLOBALS['dotb_config']['unique_key'] . $key .'",
              "data": {
                "id": "'. $bean->id .'",
                "message": "'. $bean->description .'",
                "direction": "'. $bean->direction .'",
                "person_name": "'. $personName .'",
                "date_sent": "'. TimeDate::getInstance()->asUser($dateEntered) .'",
                "attachments": '. json_encode($formatted_attachment_list) .'
              }
            }';
            if($bean->source == 'mobile'){
                $body1 = '{
              "to": "' .$GLOBALS['dotb_config']['unique_key'] . '/loadBaseMessage/' . $bean->parent_id . '",
              "data": {
                "id": "'. $bean->id .'",
                "message": "'. $bean->description .'",
                "direction": "'. $bean->direction .'",
                "person_name": "'. $personName .'",
                "date_sent": "'. TimeDate::getInstance()->asUser($dateEntered) .'",
                "source": "'. 'mobile' .'",
                "attachments": '. json_encode($formatted_attachment_list) .'
              }
            }';
                $body2 = '{
              "to": "' .$GLOBALS['dotb_config']['unique_key'] . '/loadPortalMessage/' . $bean->parent_id . '",
              "data": {
                "id": "'. $bean->id .'",
                "message": "'. $bean->description .'",
                "direction": "'. $bean->direction .'",
                "person_name": "'. $personName .'",
                "date_sent": "'. TimeDate::getInstance()->asUser($dateEntered) .'",
                "source": "'. 'mobile' .'",
                "attachments": '. json_encode($formatted_attachment_list) .'
              }
            }';
                $request1 = new Request('POST', $url, $headers, $body1);
                $res = $client->sendAsync($request1)->wait ();
                $request2 = new Request('POST', $url, $headers, $body2);
                $res = $client->sendAsync($request2)->wait ();
            } else {
                $request = new Request('POST', $url, $headers, $body);
                $res = $client->sendAsync($request)->wait ();
            }
        }
    }
    function getFile($bean) {
        //Get the attachment of each comment
        $sqlNote = "SELECT IFNULL(l1.id, '') pic_id,
                        IFNULL(l1.name, '') name,
                        IFNULL(l1.upload_id, '') upload_id,
                        IFNULL(l1.filename, '') filename,
                        IFNULL(l1.file_size, '') file_size,
                        IFNULL(l1.file_source, '') file_source,
                        IFNULL(l1.file_mime_type, '') file_mime_type,
                        IFNULL(l1.file_ext, '') file_ext
                        FROM notes l1 
                        WHERE l1.parent_id = '{$bean->id}' AND l1.parent_type = 'C_Comments' AND l1.deleted = 0";
        $attachment_list = $GLOBALS['db']->fetchArray($sqlNote);
        $formatted_attachment_list = [];
        //Format img list
        foreach($attachment_list as $attachment){
            $srcImg = $attachment['file_source'] == 'S3'
                ? $GLOBALS['dotb_config']['storage_service']['host_url'] . $attachment['upload_id']
                : $GLOBALS['dotb_config']['site_url'].'/upload/s3_storage/'.$attachment['name'];
            if($attachment['file_source'] == 'S3'){
                $sourceFile = 'upload/s3_storage/' . $attachment['name'];
                if(!file_exists($sourceFile)){
                    $fileContent = file_get_contents($srcImg);
                    if ($fileContent) file_put_contents($sourceFile, $fileContent);
                }
                $srcImg = $sourceFile;
            }
            if($_SESSION['platform'] == 'portal'){
                $srcImg = '../'.$srcImg;
            }
            $fileType = explode('/', $attachment['file_mime_type'])[0]; //image or video
            $icon = $this->getFileIcon($attachment['filename'], $fileType);
            $preview = '';
            if (strpos($fileType, "image") === 0) {
                $preview = '<img class="img-preview" src="' . htmlspecialchars($srcImg) . '" alt="' . htmlspecialchars($attachment['filename']) . '" height="30">';
            } else {
                $preview = '<i class="fa ' . htmlspecialchars($icon) . '"></i>';
            }
            $formatted_attachment_list[] = array(
                'attachmentId' => $attachment['pic_id'],
                'fileSource' => empty($attachment['file_source']) ? 'local' : $attachment['file_source'],
                'fileType' => $fileType,
                'fileSize' => round(intVal($attachment['file_size']) / 1000, 2),
                'fileName' => $attachment['filename'],
                'fileExt' => $attachment['file_ext'],
                'fileUrl' => $srcImg,
                'preview' => $preview,
            );
        }
        return $formatted_attachment_list;
    }
    function getFileExtension($filename) {
        // Check if the filename includes 'zip' directly
        if (strpos($filename, 'zip') !== false) {
            return 'zip';
        }
        // Use pathinfo to get extension for other cases
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        return $extension;
    }
    function getFileIcon($fileName, $fileType) {
        $iconList = [
            'docx' => 'fa-file-word-o',
            'txt' => 'fa-file-text',
            'excel' => 'fa-file-excel-o',
            'video' => 'fa-file-video-o',
            'pptx' => 'fa-file-powerpoint-o',
            'audio' => 'fa-file-audio-o',
            'pdf' => 'fa-file-pdf-o',
            'zip' => 'fa-file-archive-o',
            'others' => 'fa-file'
        ];

        $fileIcon = 'others';
        if (strpos($fileType, "video") === 0) {
            $fileIcon = 'video';
        }

        $extension = $this->getFileExtension($fileName); // Assuming getFileExtension is defined elsewhere

        switch ($extension) {
            case 'csv':
            case 'xls':
            case 'xlsx':
                $fileIcon = 'excel';
                break;
            case 'doc':
            case 'docx':
                $fileIcon = 'docx';
                break;
            case 'txt':
                $fileIcon = 'txt';
                break;
            case 'pptx':
                $fileIcon = 'pptx';
                break;
            case 'pdf':
                $fileIcon = 'pdf';
                break;
            case 'zip':
            case 'rar':
                $fileIcon = 'zip';
                break;
        }

        // Assuming iconList is defined elsewhere and accessible in this scope
        return $iconList[$fileIcon]; // Ensure $iconList is defined and accessible
    }
    function handleCommentBox(&$bean, $event, $arguments){
        global $timedate, $current_user;
        if($_REQUEST["view"] == 'subpanel-for-cases-comments_link') {
            if($bean->direction == 'inbound'){
                $class = "purple-text";
                $div_img = '<div style="border: solid 1px;" class="label-module label-module-lg '.$class.'"><i class="far fa-graduation-cap"></i></div>';
                $row = $GLOBALS['db']->fetchOne("SELECT DISTINCT IFNULL(l1.id, '') student_id,
                IFNULL(l1.picture, '') picture, IFNULL(l1.full_student_name, '') student_name
                FROM cases INNER JOIN contacts_cases_1_c l1_1 ON cases.id = l1_1.contacts_cases_1cases_idb AND l1_1.deleted = 0
                INNER JOIN contacts l1 ON l1.id = l1_1.contacts_cases_1contacts_ida AND l1.deleted = 0
                WHERE (cases.id = '{$bean->parent_id}') AND cases.deleted = 0");

                if (isset($_SESSION['type']) && $_SESSION['type'] == 'support_portal') {
                    $chatter = translate('LBL_YOU', 'C_Comments');
                } else {
                    $chatter = $row['student_name'];
                }

                if(!empty($row['picture'])) $div_img = "<div class='image_rounded' style='width: 42px;'><img src='rest/v11_3/Contacts/{$row['student_id']}/file/picture?format=dotb-html-json&platform=base&_hash=/resize/{$row['picture']}' ></div>";
                $a_link = "#Contacts/{$row['student_id']}";
            }elseif($bean->direction == 'outbound') {
                $class = "text-info";
                $div_img = '<div style="border: solid 1px;" class="label-module label-module-lg '.$class.'"><i class="far fa-user"></i></div>';
                //$row = $GLOBALS['db']->fetchOne("SELECT IFNULL(id,'') id, IFNULL(full_user_name,'') full_user_name FROM users FROM id = '{$bean->created_by}'");
                if($current_user->id == $bean->created_by) $chatter = translate('LBL_YOU', 'C_Comments');
                else $chatter = $bean->created_by_name;
                $a_link = "#Cases/{$bean->parent_id}";
            }
            $bean->namee  = "<li style='white-space: normal;'>
                            <a href='$a_link' class='pull-left' style='padding-right: 10px;'>$div_img</a>
                            <div><strong class='$class'>$chatter</strong>
                            <p class='text-muted' style='color: #6d6d6d;' rel='tooltip' data-original-title='".$timedate->to_display_date_time($bean->date_entered)."'>".$timedate->time_Ago($bean->date_entered);
            if(!$bean->is_read_inems) $bean->namee .= '<i class="unread-notification fa fa-circle" style="font-size: 5px;vertical-align: middle;padding-left: 5px;"></i>';
            $bean->namee .= "</p></div></li>" ;

            $sqlNote = "SELECT IFNULL(id,'') id, 
            IFNULL(name,'') notes_name,  
            IFNULL(upload_id,'') upload_id,  
            IFNULL(file_source,'') file_source,  
            IFNULL(filename,'') name,  
            IFNULL(file_mime_type,'') type,  
            IFNULL(file_ext,'') file_ext,  
            IFNULL(file_size,'') size 
            FROM notes 
            WHERE parent_id = '$bean->id' 
              AND parent_type = 'C_Comments'
              AND deleted = 0";

            $result = $GLOBALS['db']->fetchArray($sqlNote);
            foreach($result as $key => $value){
                $file_name = $value['notes_name'];
                $srcImg = $value['file_source'] == 'S3' ? $GLOBALS['dotb_config']['storage_service']['host_url'] . $value['upload_id'] : 'upload/s3_storage/' . $file_name;
                // This part is used to cache the video file from S3 to local
                if($value['file_source'] == 'S3'){
                    $sourceFile = 'upload/s3_storage/' . $file_name;
                    if(!file_exists($sourceFile)){
                        $fileContent = file_get_contents($srcImg);
                        if ($fileContent) file_put_contents($sourceFile, $fileContent);
                    }
                    $srcImg = $sourceFile;
                }
                $image = $image . "<li style='white-space: normal;'>
                <a href=".$srcImg." class='pull-left' style='padding-right: 10px;' target='_blank' download='{$value['name']}'>
                {$value['name']}</a></li>";
            }

            if (!$current_user->is_admin && $bean->is_unsent) {
                $bean->uploadfile = '<span class="normal"><p style="white-space: normal; font-style: italic; color:#555;">'.translate('LBL_UNSENT_MESSAGE','C_Comments').'</p></span>';
                $bean->description = '<span class="normal"><p style="white-space: normal; font-style: italic; color:#555;">'.translate('LBL_UNSENT_MESSAGE','C_Comments').'</p></span>';
            } else {
                $bean->uploadfile = $image;
                $edited = ($bean->is_updated) ? '<p style="white-space: normal; color: #555; font-style: italic;">'.translate('LBL_MESSAGE_EDITED','C_Comments').'</p>' : '';
                $bean->description = '<span class="normal">
                            <p style="white-space: normal;">'.nl2br($bean->description, false).'</p>
                            '.$edited.'
                            </span>';
            }
            //update is_read
            if(!$bean->is_read_inems) $GLOBALS['db']->query("UPDATE c_comments SET is_read_inems=1 WHERE parent_id = '{$bean->parent_id}' AND parent_type = 'Cases' AND deleted = 0");

        } else {
            if(!empty($bean->student_id)) {
                $sqlStudent = $GLOBALS['db']->fetchOne("SELECT DISTINCT IFNULL(l1.id, '') student_id,
                IFNULL(l1.picture, '') picture, 
                IFNULL(l1.full_student_name, '') student_name
                FROM contacts l1
                WHERE l1.id = '{$bean->student_id}'");

                $chatter = $sqlStudent['student_name'];
                $class = "purple-text";
                $div_img = '<div style="border: solid 1px;" class="label-module label-module-lg '.$class.'"><i class="far fa-graduation-cap"></i></div>';
                if(!empty($sqlStudent['picture'])) $div_img = "<div class='image_rounded' style='width: 42px;'><img src='rest/v11_3/Contacts/{$sqlStudent['student_id']}/file/picture?format=dotb-html-json&platform=base&_hash=/resize/{$sqlStudent['picture']}' ></div>";
                $a_link = "#Contacts/{$sqlStudent['student_id']}";
            } else {
                $sqlTeacher = $GLOBALS['db']->fetchOne("SELECT DISTINCT IFNULL(l1.id, '') teacher_id,
                IFNULL(l2.picture, '') picture, 
                IFNULL(l1.full_teacher_name, '') teacher_name
                FROM c_teachers l1
                INNER JOIN users l2 ON l1.id = l2.teacher_id
                WHERE l2.id = '{$bean->created_by}'");

                $chatter = $sqlTeacher['teacher_name'];
                $class = "text-info";
                $div_img = '<div style="border: solid 1px;" class="label-module label-module-lg '.$class.'"><i class="far fa-user"></i></div>';
                if(!empty($sqlTeacher['picture'])) $div_img = "<div class='image_rounded' style='width: 42px;'><img src='rest/v11_3/Contacts/{$sqlTeacher['teacher_id']}/file/picture?format=dotb-html-json&platform=base&_hash=/resize/{$sqlTeacher['picture']}' ></div>";
                $a_link = "#C_Teachers/{$bean->created_by}";
            }
            $bean->namee  = "<li style='white-space: normal;'>
                            <a href='$a_link' class='pull-left' style='padding-right: 10px;'>$div_img</a>
                            <div><strong class='$class'>$chatter</strong>
                            <p class='text-muted' style='color: #6d6d6d;' rel='tooltip' 
                            data-original-title='".$timedate->to_display_date_time($bean->date_entered)."'>".$timedate->time_Ago($bean->date_entered)."</p></div></li>";
        }
    }
    public function handleBeforeDelete($bean, $event, $arguments)
    {
        if (!empty($bean->attachment_list)) {
            foreach($bean->attachment_list as $note_id){
                $note = BeanFactory::getBean('Notes', $note_id);
                $note->mark_deleted();
                $note->save();
            }
        }
    }

}
