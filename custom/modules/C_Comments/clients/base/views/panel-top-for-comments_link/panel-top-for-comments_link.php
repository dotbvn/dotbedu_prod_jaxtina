<?php

$viewdefs['C_Comments']['base']['view']['panel-top-for-comments_link'] = array(
    'type' => 'panel-top',
    'template' => 'panel-top',
    'buttons' => array(
        array(
            'type' => 'button',
            'css_class' => 'btn',
            'icon' => 'fa fa-comments',
            'name' => 'create_button',
            'label' => 'LBL_CREATE_COMMENTS',
            'acl_action' => 'create',
            'tooltip' => 'LBL_CREATE_COMMENTS',
        ),

    ),

);



