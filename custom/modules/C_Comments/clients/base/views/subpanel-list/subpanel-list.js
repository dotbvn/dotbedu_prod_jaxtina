({
    //Handle comments
    extendsFrom: 'SubpanelListView',

    initialize: function(options){
        this._super('initialize', [options]);
        //Auto Refresh Comments - By Lap Nguyen
        // window.setTimeout(_.bind(this.refreshComments, this), 30*1000);
    },
    refreshComments: function(){
        var self = this;
        var parent = self.context.parent.get('model'),
        detailsLink='comments_link',
        commentsCollection = parent.getRelatedCollection(detailsLink);
        commentsCollection.fetch({relate:true});
        window.setTimeout(_.bind(self.refreshComments, self), 30*1000);
    },

    _dispose: function() {
        this._super('_dispose');
    },
})
