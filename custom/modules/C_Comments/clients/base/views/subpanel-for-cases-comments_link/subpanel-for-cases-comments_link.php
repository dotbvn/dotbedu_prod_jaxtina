<?php
// created: 2020-05-17 13:14:13
$viewdefs['C_Comments']['base']['view']['subpanel-for-cases-comments_link'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                array (
                    'name' => 'namee',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'small',
                    'type' => 'html',
                ),
                array (
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'xlarge',
                    'type' => 'html',
                ),
                array (
                    'name' => 'uploadfile',
                    'label' => 'LBL_FILE_UPLOAD',
                    'enabled' => true,
                    'default' => true,
                    'sortable' => false,
                    'width' => 'small',
                    'type' => 'html',
                ),
                array (
                    'name' => 'direction',
                    'label' => 'LBL_DIRECTION',
                    'enabled' => true,
                    'default' => false,
                ),
                array (
                    'name' => 'created_by_name',
                    'label' => 'LBL_CREATED',
                    'enabled' => true,
                    'readonly' => true,
                    'sortable' => false,
                    'id' => 'CREATED_BY',
                    'link' => true,
                    'default' => false,
                ),
                array (
                    'name' => 'date_entered',
                    'default' => false,
                ),
                array (
                    'name' => 'created_by',
                    'default' => false,
                ),
                array (
                    'name' => 'parent_id',
                    'default' => false,
                ),
                array (
                    'name' => 'parent_type',
                    'default' => false,
                ),
                array (
                    'name' => 'parent_name',
                    'default' => false,
                ),
                array (
                    'name' => 'is_read_inapp',
                    'default' => false,
                ),
                array (
                    'name' => 'is_read_inems',
                    'default' => false,
                ),
                array (
                    'name' => 'is_unsent',
                    'default' => false,
                ),
                array (
                    'name' => 'is_updated',
                    'default' => false,
                ),
            ),
        ),
    ),
    'rowactions' =>
    array (
        'actions' => array(
            1 => array(
                'type' => 'unlink-action',
                'icon' => 'fa-chain-broken',
                'tooltip' => 'LBL_UNLINK_BUTTON',
            ),
        ),
    ),
    'orderBy' => array (
        'field' => 'date_entered',
        'direction' => 'DESC',
    ),
    'type' => 'subpanel-list',
);