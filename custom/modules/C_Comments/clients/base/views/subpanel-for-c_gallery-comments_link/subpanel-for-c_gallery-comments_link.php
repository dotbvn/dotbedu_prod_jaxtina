<?php
// created: 2020-05-17 13:14:13
$viewdefs['C_Comments']['base']['view']['subpanel-for-c_gallery-comments_link'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                array (
                    'name' => 'namee',
                    'label' => 'LBL_NAME',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'small',
                    'type' => 'html',
                ),
                array (
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'xlarge',
                    'type' => 'html',
                ),
                array (
                    'name' => 'date_entered',
                    'default' => false,
                ),
                array (
                    'name' => 'student_id',
                    'default' => false,
                ),
            ),
        ),
    ),
    'orderBy' => array (
        'field' => 'date_entered',
        'direction' => 'DESC',
    ),
    'type' => 'subpanel-list',
);