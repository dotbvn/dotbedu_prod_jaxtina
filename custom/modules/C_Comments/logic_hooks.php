<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will
// be automatically rebuilt in the future.
 $hook_version = 1;
$hook_array = Array();
// position, file, function
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1,'handle save','custom/modules/C_Comments/logicComment.php','logicComment','handleBeforeSave');
$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(10,'delete Notification','custom/modules/Notifications/LogicHook/Notification.php','LogicHook_Notification','deleteNotification',);
$hook_array['after_retrieve'] = Array();
$hook_array['after_retrieve'][] = Array(10,'update Read Notification','custom/modules/Notifications/LogicHook/Notification.php','LogicHook_Notification','updateNotification',);
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(10,'send Socket','custom/modules/Notifications/LogicHook/Notification.php','logicComment','handleAfterSave',);
$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(1,'Handle Comment Box','custom/modules/C_Comments/logicComment.php','logicComment','handleCommentBox',);

?>
