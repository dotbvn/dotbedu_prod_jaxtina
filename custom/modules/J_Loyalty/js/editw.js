var record_id   = $('#j_class_j_loyalty_1j_class_ida').closest('form').find('input[name=record]').val();
var loyalty_form= $('#j_class_j_loyalty_1j_class_ida').closest('form');
DOTB.util.doWhen(
    function() {
        return $('#j_class_j_loyalty_1j_class_ida').length == 1;
    },
    function() {
        loyalty_form.find('#btn_student_name').attr("onclick", "").unbind("click");
        //Change Team Action - Fix bug Mutiple User
        loyalty_form.find('#btn_student_name').click(function(){
            var class_id = $('#j_class_j_loyalty_1j_class_ida').val();
            if(class_id != '')
                open_popup('Contacts', 1000, 700, "&advanced_filter_class_id="+class_id, true, false, {"call_back_function":"set_return_student","form_name":$(this).closest('form').attr('name'),"field_to_name_array":{"id":"student_id","name":"student_name"}}, "single", true);
            else
                open_popup('Contacts', 1000, 700, "", true, false, {"call_back_function":"set_return_student","form_name":$(this).closest('form').attr('name'),"field_to_name_array":{"id":"student_id","name":"student_name"}}, "single", true);
        });
        loadStudentRemain();
});

function check_form(formname) {
    if (typeof (siw) != 'undefined' && siw && typeof (siw.selectingSomething) != 'undefined' && siw.selectingSomething){
        return false;
    }

    if(document.getElementById(formname).module.value == 'J_Loyalty') {
        var total_point   = parseInt(loyalty_form.find('input[name=total_point]').val());
        var point         = parseInt(loyalty_form.find('#point').val());
        var type          = loyalty_form.find('#type').val();
        var language      = parent.DOTB.App.lang;
        var loyalty_out   = language.getAppListStrings('loyalty_out_list');
        var remain_point  = total_point - point;
        var checkLoyalty  = true;
        if(($.inArray(type, Object.keys(loyalty_out)) != -1) && (remain_point < 0)){
            checkLoyalty = false;
            $('#point').effect("highlight", {color: '#ff9933'}, 2000);
            toastr.error(language.get('LBL_LIMITED_POINT','J_Loyalty'));
        }
        if(validate_form(formname, '') && checkLoyalty) return true;
        else return false;
    } else {
        if(validate_form(formname, '')) return true; else return false;
    }
    return false;
}

function set_return_student(popup_reply_data){
    var form_name = popup_reply_data.form_name;
    var name_to_value_array = popup_reply_data.name_to_value_array;
    for (var the_key in name_to_value_array) {
        if (the_key == 'toJSON') {
            continue;
        } else {
            var val = name_to_value_array[the_key].replace(/&amp;/gi, '&').replace(/&lt;/gi, '<').replace(/&gt;/gi, '>').replace(/&#039;/gi, '\'').replace(/&quot;/gi, '"');
            switch (the_key) {

                case 'student_id':
                    $('input#student_id').val(val);

                    break;
                case 'student_name':
                    $('input#student_name').val(val);
                    break;
            }
        }
    }
    loadStudentRemain();
}
//AJAX load Student Remain Points
function loadStudentRemain(){
    var student_id = loyalty_form.find('input[name=student_id]').val();
    if(student_id == '') return ;
    else {
        $('#total_point_text').text('');
        $('#total_point').val('');
    }
    $.ajax({
        url: "index.php?module=J_Loyalty&action=handleAjaxLoyalty&dotb_body_only=true",
        type: "POST",
        async: true,
        data:  {
            type       : 'ajaxgetTotalLoyalty',
            student_id : student_id,
            record_id  : record_id,
        },
        dataType: "json",
        success: function(res){
            if(res.success == 1){
                $('#total_point_text').text(res.total_point);
                $('input[name=total_point]').val(res.total_point);
            }else{
                $('#total_point_text').text('');
                $('input[name=total_point]').val('');
            }
        },
    });
}