<?php
// created: 2024-07-30 18:14:48
$viewdefs['J_Loyalty']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'tag' => 
    array (
    ),
    'student_name' => 
    array (
    ),
    'type' => 
    array (
    ),
    'j_class_j_loyalty_1_name' => 
    array (
    ),
    'point' => 
    array (
    ),
    'input_date' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    'modified_by_name' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);