<?php

switch ($_POST['type']) {
    case 'ajaxgetTotalLoyalty':
        $result = ajaxgetTotalLoyalty($_POST['student_id'], $_POST['record_id']);
        echo $result;
        break;
}

//get total loyalty points
function ajaxgetTotalLoyalty($student_id, $record_id = ''){
    $total_point = (int)$GLOBALS['db']->getOne("SELECT DISTINCT SUM(l3.point) total_point
    FROM contacts l2 INNER JOIN j_loyalty l3 ON l3.student_id = l2.id AND l3.deleted = 0
    WHERE (l2.id = '$student_id') AND (l3.id <> '$record_id') AND l2.deleted = 0");
    return json_encode(
        array('success' => 1, 'total_point' => $total_point)
    );
}
