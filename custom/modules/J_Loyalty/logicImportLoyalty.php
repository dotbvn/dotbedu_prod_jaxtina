<?php
class logicImportLoyalty{
    //Import Loyalty
    function importLoyalty(&$bean, $event, $arguments){
        if($_POST['module'] == 'Import'){
            //Get Student ID
            $q1 = "SELECT IFNULL(id, '') id, IFNULL(team_id, '') team_id FROM contacts WHERE ((old_student_id = '{$bean->old_student_id}') OR (contact_id = '{$bean->old_student_id}')) AND deleted = 0 LIMIT 1";
            $student = $GLOBALS['db']->fetchOne($q1);
            if(empty($student['id'])){
                $bean->deleted = 1;
                $GLOBALS['log']->fatal("[LOYALTY-Import]: Not found student ID {$bean->old_student_id}");
            }
            $bean->student_id     = $student['id'];
            $bean->team_id        = $student['team_id'];
            $bean->team_set_id    = $student['team_id'];
        }
    }
}

?>
