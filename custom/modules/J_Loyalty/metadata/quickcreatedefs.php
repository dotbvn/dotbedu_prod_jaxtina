<?php
$module_name = 'J_Loyalty';
$viewdefs[$module_name] =
array (
    'QuickCreate' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'hidden' =>
                array (
                    0 => '<input type="hidden" name="total_point" value="0">',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'javascript' => '{dotb_getscript file="custom/modules/J_Loyalty/js/editw.js"}',
        ),
        'panels' =>
        array (
            'default' =>
            array (
                0 =>
                array (
                    '',
                    1 => 'j_class_j_loyalty_1_name',
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'type',
                        'studio' => 'visible',
                        'label' => 'LBL_TYPE',
                        'customCode' => '{html_options name="type" id="type" options=$typeOptions selected=$fields.type.value}'
                    ),
                    1 =>
                    array (
                        'name' => 'student_name',
                        'label' => 'LBL_STUDENT_NAME',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'point',
                        'label' => 'LBL_POINT',
                        'customCode'=> '<input tabindex="0" type="text" name="point" id="point" value="{$fields.point.value}" size="4" maxlength="10" style="color: rgb(165, 42, 42);">',
                        'displayParams' =>
                        array (
                            'required' => true,
                            'min' => 1,
                            'max' => 999,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'input_date',
                        'label' => 'LBL_INPUT_DATE',
                    ),
                ),
                3 =>
                array (
                    0 =>array (
                        'name' => 'description',
                        'displayParams' =>
                        array (
                            'rows' => 4,
                            'cols' => 60,
                        ),
                    ),
                    1 =>
                    array (
                        'hideLabel' => true,
                        'customCode' => '<span style="color:red; font-size:20px">{$MOD.LBL_TOTAL_LOYALTY_POINT}: </span><span id="total_point_text" style="color:red; font-size:20px"></span>',
                    ),
                ),
            ),
        ),
    ),
);
