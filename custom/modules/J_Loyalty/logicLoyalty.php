<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicLoyalty
{
    function beforeSaveLogic(&$bean, $event, $arguments){
        //Set Team
        $student = BeanFactory::getBean('Contacts', $bean->student_id);
        if (empty($bean->team_id)) {
            $bean->team_id = '1';
            $bean->team_set_id = '1';
        }

        //Changing the sign of a number
        $loyalty_in_list = $GLOBALS['app_list_strings']['loyalty_in_list'];
        $loyalty_out_list = $GLOBALS['app_list_strings']['loyalty_out_list'];
        if (in_array($bean->type, $loyalty_in_list)){
            $bean->point = abs($bean->point);

            //Set Expired
            $expire_time = $GLOBALS['app_list_strings']['default_loyalty_rate']['Expire Time'];
            if(empty($expire_time)) $expire_time = '1 year';
            $max_exp = $GLOBALS['db']->getOne("SELECT MAX(exp_date) exp_date FROM j_loyalty WHERE deleted = 0 AND (student_id = '{$bean->student_id}') GROUP BY student_id");
            $exp_date= date('Y-m-d', strtotime("+ $expire_time" . $bean->input_date));

            if($max_exp < $exp_date){
                $bean->exp_date = $exp_date;
                $GLOBALS['db']->query("UPDATE j_loyalty SET exp_date = '{$bean->exp_date}' WHERE student_id = '{$bean->student_id}' AND type IN ('".implode("','",array_keys($loyalty_in_list))."') AND deleted = 0");

            }else $bean->exp_date = $max_exp;
        }

        if (in_array($bean->type, $loyalty_out_list)){
            $bean->point = -1 * abs($bean->point);
            $bean->exp_date = '';
            //Xử lý logic trừ điểm âm vượt quá số điểm hiện có
            if(!empty($bean->id)) $ext_loyalty = " AND l3.id <> '{$bean->id}'";
            $total_point = (int)$GLOBALS['db']->getOne("SELECT DISTINCT SUM(l3.point) total_point
                FROM contacts l2 INNER JOIN j_loyalty l3 ON l3.student_id = l2.id AND l3.deleted = 0
                WHERE (l2.id = '{$bean->student_id}') $ext_loyalty AND l2.deleted = 0");
            $remain = $total_point - $bean->point;

            if($remain < 0){
                $route = "window.parent.DOTB.App.router.redirect(window.parent.DOTB.App.router.buildRoute('{$_REQUEST['return_module']}','".$_REQUEST['return_id']."'));";
                echo '<script type="text/javascript">
                window.parent.DOTB.App.alert.show(\'message-id\', {
                level: \'error\',
                messages: \' '.translate('LBL_LIMITED_POINT', 'J_Loyalty').'!!\',
                autoClose: true
                });
                '.$route.'
                </script>';
                die();
            }
        }
    }
    /*AFTER_SAVE*/
    function addCode(&$bean, $event, $arguments){
        $code_field = 'name';
        if (empty($bean->$code_field)) {
            //AFTER_SAVE: Repeat 10 times to avoid duplicate codes
            for ($x = 0; $x < 10; $x++) {
                //Get Prefix
                $res = $GLOBALS['db']->query("SELECT contact_id FROM contacts WHERE id = '{$bean->student_id}'");
                $row = $GLOBALS['db']->fetchByAssoc($res);
                $prefix = $row['contact_id'] . '/' . date('y');
                $year = date('y', strtotime('+ 7hours' . (!empty($bean->date_entered) ? $bean->date_entered : $bean->fetched_row['date_entered'])));
                $table = $bean->table_name;
                $sep = '/';
                $first_pad = '000';
                $padding = 3;
                $query = "SELECT $code_field FROM $table WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, " . (strlen($prefix) + 1) . ") = '#" . $prefix . "') ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";

                $result = $GLOBALS['db']->query($query);
                if ($row = $GLOBALS['db']->fetchByAssoc($result))
                    $last_code = $row[$code_field];
                else
                    //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $last_code = $prefix . $sep . $first_pad;


                $num = substr($last_code, -$padding, $padding);
                $num++;
                $pads = $padding - strlen($num);
                $new_code = "#".$prefix . $sep;

                //preform the lead padding 0
                for ($i = 0; $i < $pads; $i++)
                    $new_code .= "0";
                $new_code .= $num;

                //check duplicate code
                $countDup = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM $table WHERE $code_field = '$new_code' AND deleted = 0");
                if(empty($countDup)){
                    //write to database - Logic: Before Save
                    $GLOBALS['db']->query("UPDATE $table SET $code_field = '$new_code' WHERE id='{$bean->id}' AND deleted = 0");
                    break;
                }
            }
        }
    }

    function listViewColor(&$bean, $event, $arguments){
        if ($bean->point < 0) $bean->point_label = "<b style='color: #BB1212'>" . $bean->point . "</b>";
        if ($bean->point > 0) $bean->point_label = "<b style='color: #468931'>+" . $bean->point . "</b>";
    }
}

?>