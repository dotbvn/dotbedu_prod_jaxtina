<?php

class logicParent
{
    function handleSaveParent(&$bean, $event, $arguments)
    {
        if (!empty($bean->birthdate)) {
            $bean->birth_day = date('d', strtotime($bean->birthdate));
            $bean->birth_month = date('m', strtotime($bean->birthdate));
        }
    }
}
