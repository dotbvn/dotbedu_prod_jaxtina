<?php
require_once "custom/modules/Schedulers/callcenter.php";

$job_strings[] = 'autoUpdateStudentStatus';
$job_strings[] = 'updateClassStatus';
$job_strings[] = 'updateAge';
$job_strings[] = 'updateSessionStatus';
$job_strings[] = 'generateOnlineLearning';
$job_strings[] = 'notificationOnlineLearning';
$job_strings[] = 'recheckOnlineLearning';
$job_strings[] = 'recordingVOIP24h';
$job_strings[] = 'changeStatusReNew';
$job_strings[] = 'recordingVihat';
$job_strings[] = 'mergeSchoolName';
$job_strings[] = 'recordingVoiceCloud';
$job_strings[] = 'recordingVOIP24h2';
$job_strings[] = 'updateReconcileStatus';
$job_strings[] = 'callCenterJob';//for all site/ all customer need and run just this one be enough
$job_strings[] = 'updateSessionStatusByMinute';
$job_strings[] = 'metrikalAlert';
$job_strings[] = 's3Upload';
$job_strings[] = 'checkTenantLicense';
//############## STUDENT ########################
$job_strings[] = 'recheckvoiprecording';
$job_strings[] = 'recodingVoip24Hipphone';
$job_strings[] = 'voip24hfixcallduration';
$job_strings[] = 'voip24hrecordingv4'; //final stable - trankhanhtoan

//############## Customize Jaxtina ########################
$job_strings[] = 'fillAttendanceJaxtina';
$job_strings[] = 'lockAttendanceStatus';


function voip24hrecordingv4()
{
    $admin = new Administration();
    $admin->retrieveSettings();
    $voip = $admin->settings['callcenter_username'];
    $secret = $admin->settings['callcenter_password'];
    $resultSQL = $GLOBALS['db']->query("select id,call_destination, call_source, date_start, date_end,cj_url from calls where (cj_url is null or cj_url <> '-1') and deleted=0 order by date_entered DESC limit 100");
    while ($row = $GLOBALS['db']->fetchByAssoc($resultSQL)) {
        if (strlen($row['call_source']) < 6 && strlen($row['call_destination']) < 6) continue;
        $datestart = strtotime($row['date_start'])+24600;
        $dateend = strtotime($row['date_end'])+25800;
        $result = callApi('GET', "http://dial.voip24h.vn/dial/search?voip=$voip&secret=$secret&destination={$row['call_destination']}&date_start=$datestart&date_end=$dateend");
        $jsonApiSync = $result;
        $GLOBALS['db']->query("update calls set  api_sync='$jsonApiSync'  where id='{$row['id']}'");
        $result = json_decode($result, 1);
        $finded = false;
        if ((int)$result['result']['recordsDisplay'] == 1) {
            foreach ($result['result']['data'] as $call) {
                $GLOBALS['db']->query("update calls set cj_url='-1',call_recording='{$call['eplay']}',outlook_id='{$call['callid']}', duration={$call['duration']},voip_status='{$call['status']}'  where id='{$row['id']}'");
                $finded = true;
            }
        } elseif ((int)$result['result']['recordsDisplay'] > 0) {
            foreach ($result['result']['data'] as $call) {
                if (abs(strtotime("+7 hours", strtotime($row['date_start'])) - strtotime($call['calldate'])) < 120) {
                    $GLOBALS['db']->query("update calls set cj_url='-1',call_recording='{$call['eplay']}',outlook_id='{$call['callid']}', duration={$call['duration']},voip_status='{$call['status']}'  where id='{$row['id']}'");
                    $finded = true;
                    break;
                }
            }
        }
        if ($finded == false) {
            switch ($row['cj_url']) {
                case '1':
                    $GLOBALS['db']->query("update calls set cj_url='2' where id='{$row['id']}'");
                    break;
                case '2':
                    $GLOBALS['db']->query("update calls set cj_url='3' where id='{$row['id']}'");
                    break;
                case '3':
                    $GLOBALS['db']->query("update calls set cj_url='4' where id='{$row['id']}'");
                    break;
                case '4':
                    $GLOBALS['db']->query("update calls set cj_url='5' where id='{$row['id']}'");
                    break;
                case '5':
                    $GLOBALS['db']->query("update calls set cj_url='-1' where id='{$row['id']}'");
                    break;
                default:
                    $GLOBALS['db']->query("update calls set cj_url='1' where id='{$row['id']}'");
                    break;
            }
        }
    }
    return true;
}
function voip24hfixcallduration()
{
    $admin = new Administration();
    $admin->retrieveSettings();
    $voip = $admin->settings['callcenter_username'];
    $secret = $admin->settings['callcenter_password'];
    $resultSQL = $GLOBALS['db']->query("select id,outlook_id,date_start from calls where call_recording is not null and call_recording <> '' and duration=0 and deleted=0 and outlook_id like '%.%' order by date_entered DESC limit 1000");
    while ($row = $GLOBALS['db']->fetchByAssoc($resultSQL)) {
        $datestart = strtotime("+1 hours", strtotime($row['date_start']));
        $dateend = strtotime("+10 hours", strtotime($row['date_start']));
        $result = callApi('GET', "http://dial.voip24h.vn/dial/search?voip=$voip&secret=$secret&callid={$row['outlook_id']}&date_start=$datestart&date_end=$dateend");
        $result = json_decode($result, 1);
        if ((int)$result['result']['recordsDisplay'] == 1) {
            $callinfo = $result['result']['data'][0];
            $GLOBALS['db']->query("update calls set call_recording='{$callinfo['eplay']}', outlook_id='{$callinfo['eplay']}', call_recording='{$callinfo['eplay']}', duration={$callinfo['duration']} where id='{$row['id']}'");
        }
    }
    return true;
}
function recodingVoip24Hipphone()
{
    $admin = new Administration();
    $admin->retrieveSettings();
    $voip = $admin->settings['callcenter_username'];
    $secret = $admin->settings['callcenter_password'];

    //get token
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://webhook.dotb.vn/login',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "UserName": "admin",
        "UserPass": "12355880@Za"
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    $response=json_decode($response, true);
    $token=$response['token'];

    //get voip_logs
    $curl = curl_init();
    global $dotb_config;
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://webhook.dotb.vn/voip_logs?limit=100&search='.$dotb_config['unique_key'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: '.$token
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    $response=json_decode($response, true);
    foreach ($response['records'] as $record) {
        $cdata = json_decode($record['body'], true);
        if ($cdata['state'] == 'Cdr' && $cdata['cdr']['disposition']=='ANSWERED') {
            //handle
            $callDate = date("Y-m-d H:i", strtotime($cdata['cdr']['starttime']) - 3600 * 7);
            $s = "select id from calls where call_destination = '{$cdata['phone']}'  and deleted = 0 and timestampdiff(second,'{$callDate}', date_entered) between 0 and 1000 limit 1";
            $resultSQL = $GLOBALS['db']->query($s);
            while ($row = $GLOBALS['db']->fetchByAssoc($resultSQL)) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://dial.voip24h.vn/dial/search?voip=$voip&secret=$secret&callid={$cdata['callid']}",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET'
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                $response=json_decode($response, true);
                if(count($response['result']['data'])>0){
                    $callRecording = $response['result']['data'][0]['eplay'];
                    $GLOBALS['db']->query("update calls set outlook_id='{$cdata['callid']}',call_entrysource='VOIP24H',call_recording='$callRecording', duration={$response['result']['data'][0]['duration']} where id='{$row['id']}'");
                }
            }
        }

        //delete
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://webhook.dotb.vn/voip_logs/' . $record['id'] . '/hard_delete',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'DELETE',
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . $token
            ),
        ));
        curl_exec($curl);
        curl_close($curl);
    }
    return true;
}
function recheckvoiprecording()
{
    $GLOBALS['db']->query("update calls set cj_url=1 where call_recording like '/upload/%.ogg' and date_entered>=DATE_SUB(NOW(), INTERVAL 1 DAY) and deleted=0");
    return true;
}
function recordingVOIP24h()
{
    $admin = new Administration();
    $admin->retrieveSettings();
    $voip = $admin->settings['callcenter_username'];
    $secret = $admin->settings['callcenter_password'];
    $resultSQL = $GLOBALS['db']->query("select id,call_destination, call_source, date_start,cj_url from calls where (cj_url is null or cj_url <> '-1') and deleted=0 order by date_entered DESC limit 1000");
    while ($row = $GLOBALS['db']->fetchByAssoc($resultSQL)) {
        if (strlen($row['call_source']) < 6 && strlen($row['call_destination']) < 6) continue;
        $datestart = strtotime("+1 hours", strtotime($row['date_start']));
        $dateend = strtotime("+10 hours", strtotime($row['date_start']));
        $result = callApi('GET', "http://dial.voip24h.vn/dial/search?voip=$voip&secret=$secret&destination={$row['call_destination']}&date_start=$datestart&date_end=$dateend");
        $result = json_decode($result, 1);
        $finded = false;
        if ((int)$result['result']['recordsDisplay'] == 1) {
            foreach ($result['result']['data'] as $call) {
                $GLOBALS['db']->query("update calls set cj_url='-1',call_recording='{$call['eplay']}' where id='{$row['id']}'");
                $finded = true;
            }
        } elseif ((int)$result['result']['recordsDisplay'] > 0) {
            foreach ($result['result']['data'] as $call) {
                if (abs(strtotime("+7 hours", strtotime($row['date_start'])) - strtotime($call['calldate'])) < 100) {
                    $GLOBALS['db']->query("update calls set cj_url='-1',call_recording='{$call['eplay']}',outlook_id='{$call['callid']}', duration={$call['duration']}  where id='{$row['id']}'");
                    $finded = true;
                    break;
                }
            }
        }
        if ($finded == false) {
            switch ($row['cj_url']) {
                case '1':
                    $GLOBALS['db']->query("update calls set cj_url='2' where id='{$row['id']}'");
                    break;
                case '2':
                    $GLOBALS['db']->query("update calls set cj_url='3' where id='{$row['id']}'");
                    break;
                case '3':
                    $GLOBALS['db']->query("update calls set cj_url='4' where id='{$row['id']}'");
                    break;
                case '4':
                    $GLOBALS['db']->query("update calls set cj_url='5' where id='{$row['id']}'");
                    break;
                case '5':
                    $GLOBALS['db']->query("update calls set cj_url='-1' where id='{$row['id']}'");
                    break;
                default:
                    $GLOBALS['db']->query("update calls set cj_url='1' where id='{$row['id']}'");
                    break;
            }
        }
    }
    return true;
}

/**
* Cập nhật Status Session - Chỉ chạy vào lúc 8h sáng
* Add by HieuPham
*/
function updateSessionStatus()
{
    //Update Finished
    $today = $GLOBALS['timedate']->nowDbDate();
    $Finished = date('Y-m-d', strtotime("-1 day " . $today));
    $sss = $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(meetings.id, '') primaryid, IFNULL(meetings.meeting_type, '') meeting_type
        FROM meetings
        WHERE (meetings.session_status <> 'Cancelled')
        AND meetings.meeting_type IN ('Session' , 'Placement Test', 'Demo')
        AND (meetings.date = '$Finished') AND meetings.deleted = 0");
    foreach ($sss as $key => $value) {
        $bean = BeanFactory::getBean('Meetings', $value['primaryid'], array('disable_row_level_security' => true));
        $bean->session_status = 'Finished';
        $bean->process_trigger = 1;
        $bean->update_date_modified = false;
        $bean->update_modified_by = false;
        $bean->save();
        if($value['meeting_type'] == 'Session'){
            //Trigger kết quả điểm danh
            $rowAtts = $GLOBALS['db']->fetchArray("SELECT DISTINCT
                IFNULL(c_attendance.id, '') primaryid
                FROM c_attendance INNER JOIN
                meetings l1 ON c_attendance.meeting_id = l1.id AND l1.deleted = 0
                WHERE (l1.id='{$bean->id}') AND c_attendance.deleted = 0");
            foreach ($rowAtts as $key => $rAtt) {
                $att = BeanFactory::getBean('C_Attendance', $rAtt['primaryid'], array('disable_row_level_security' => true));
                $att->process_trigger = 1;
                $att->update_date_modified = false;
                $att->update_modified_by = false;
                $att->save();
            }
        }
    }

    //Update In Progress
    $sss2 = $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(meetings.id, '') primaryid, IFNULL(meetings.meeting_type, '') meeting_type
        FROM meetings
        WHERE (meetings.session_status <> 'Cancelled') AND (meetings.date = '$today')
        AND meetings.meeting_type IN ('Session' , 'Placement Test', 'Demo')
        AND meetings.deleted = 0");
    foreach ($sss2 as $key => $value) {
        $bean = BeanFactory::getBean('Meetings', $value['primaryid'], array('disable_row_level_security' => true));
        $bean->session_status = 'In Progress';
        $bean->process_trigger = 1;
        $bean->update_date_modified = false;
        $bean->update_modified_by = false;
        $bean->save();
        if($value['meeting_type'] == 'Session'){
            //Trigger Class-Student
            $rowSdt = $GLOBALS['db']->fetchArray("SELECT DISTINCT
                IFNULL(l3.id, '') primaryid, IFNULL(l1.id, '') student_id,
                IFNULL(l1.full_student_name, '') student_name, IFNULL(l2.id, '') class_id, IFNULL(l3.status, '') status,
                MIN(jst.start_study) start_study, MAX(jst.end_study) end_study,
                SUM(CASE WHEN (mt.date <= '$today' AND jst.type = 'OutStanding') THEN 1 ELSE 0 END) count_ost,
                SUM(CASE WHEN (mt.date <= '$today' AND jst.type = 'Enrolled') THEN 1 ELSE 0 END) count_ern,
                SUM(CASE WHEN (mt.date <= '$today' AND jst.type = 'Demo') THEN 1 ELSE 0 END) count_demo
                FROM meetings
                INNER JOIN meetings_contacts l1_1 ON meetings.id = l1_1.meeting_id AND l1_1.deleted = 0
                INNER JOIN contacts l1 ON l1.id = l1_1.contact_id AND l1.deleted = 0
                INNER JOIN j_class l2 ON meetings.ju_class_id = l2.id AND l2.deleted = 0
                INNER JOIN j_classstudents l3 ON l3.class_id = l2.id AND l3.student_id = l1.id AND l3.deleted = 0
                INNER JOIN j_studentsituations jst ON l3.class_id = jst.ju_class_id AND l1.id = jst.student_id AND jst.deleted = 0
                INNER JOIN meetings_contacts mt_1 ON mt_1.situation_id = jst.id AND mt_1.deleted = 0
                INNER JOIN meetings mt ON mt_1.meeting_id = mt.id AND mt.session_status <> 'Cancelled' AND mt.deleted = 0
                WHERE (meetings.id = '{$bean->id}') AND jst.type IN ('OutStanding', 'Enrolled', 'Demo') AND meetings.deleted = 0
                GROUP BY l1.id");
            foreach ($rowSdt as $key => $std) {
                $_Obj = BeanFactory::getBean('J_ClassStudents', $std['primaryid'], array('disable_row_level_security' => true));
                $_Obj->process_trigger = 1;
                $_Obj->count_ost       = $std['count_ost'];
                $_Obj->count_ern       = $std['count_ern'];
                $_Obj->count_demo      = $std['count_demo'];
                $_Obj->run_time        = $bean->date;
                $_Obj->meeting_id      = $bean->id;
                $_Obj->update_date_modified = false;
                $_Obj->update_modified_by = false;
                $_Obj->save();
            }
        }
    }

    //Update Not Started
    $NotStated1 = date('Y-m-d', strtotime("+1 day " . $today));
    $NotStated2 = date('Y-m-d', strtotime("+8 day " . $today));
    $sss2 = $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(meetings.id, '') primaryid, IFNULL(meetings.meeting_type, '') meeting_type
        FROM meetings
        WHERE (meetings.session_status <> 'Cancelled')
        AND meetings.meeting_type IN ('Session' , 'Placement Test', 'Demo')
        AND (meetings.date >= '$NotStated1' AND meetings.date <= '$NotStated2') AND meetings.deleted = 0");
    foreach ($sss2 as $key => $value) {
        $bean = BeanFactory::getBean('Meetings', $value['primaryid'], array('disable_row_level_security' => true));
        $bean->session_status = 'Not Started';
        $bean->process_trigger = 1;
        $bean->update_date_modified = false;
        $bean->update_modified_by = false;
        $bean->save();
    }

    return true;
}

/**
* Auto-Update Student Status
*/
function autoUpdateStudentStatus()
{
    //Update Invoicing - Bổ sung chức năng Reset các hoá đơn Invoicing
    $GLOBALS['db']->query("UPDATE j_configinvoiceno SET invoicing=0 WHERE deleted=0");
    ///================================================================================

    ///================================================================================
    sleep(rand(0, 20));//Wait for not to affect other sites
    updateStudentStatus(); // update student status
    return true;
}

/**
* UPDATE AGE
*/
function updateAge()
{
    $q1 = "SELECT IFNULL(id, '') id, (YEAR(CURDATE())-YEAR(birthdate)-(DATE_FORMAT(CURDATE(),'%m%d')<DATE_FORMAT(birthdate,'%m%d'))) age FROM contacts WHERE DATE_FORMAT(NOW(), '%m-%d') = DATE_FORMAT(birthdate, '%m-%d') AND deleted = 0";
    $row1 = $GLOBALS['db']->fetchArray($q1);
    foreach ($row1 as $r) {
        $stu = BeanFactory::getBean('Contacts', $r['id']);
        $stu->age = $r['age'];
        $stu->update_date_modified = false;
        $stu->update_modified_by = false;
        $stu->save();
    }
    //update leads/prospects
    $arrMod = ['leads', 'prospects'];
    foreach ($arrMod as $mod) {
        $q1 = "UPDATE $mod SET age = (YEAR(CURDATE())-YEAR(birthdate)-(DATE_FORMAT(CURDATE(),'%m%d')<DATE_FORMAT(birthdate,'%m%d')))
        WHERE DATE_FORMAT(NOW(), '%m-%d') = DATE_FORMAT(birthdate, '%m-%d') AND deleted = 0";
        $GLOBALS['db']->query($q1);
    }
    return true;
}

//End STUDENT

//############## CLASS ########################
/**
* Auto-Update Finish Class
*/
function updateClassStatus(){
    updateClassStatusUtil();

    //Update Class Student
    $range1 = date('Y-m-d', strtotime('-1 day ' . $GLOBALS['timedate']->nowDbDate()));
    $range2 = date('Y-m-d', strtotime('+1 day ' . $GLOBALS['timedate']->nowDbDate()));
    $q2 = "SELECT DISTINCT IFNULL(l1.id, '') class_id
    FROM meetings INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
    WHERE (((meetings.date >= '$range1' AND meetings.date <= '$range2')
    AND (IFNULL(meetings.session_status, '') <> 'Cancelled'
    OR (IFNULL(meetings.session_status, '') IS NULL
    AND 'Cancelled' IS NOT NULL))))
    AND meetings.deleted = 0";
    $rows = $GLOBALS['db']->fetchArray($q2);
    foreach($rows as $row) updateClassStudent($row['class_id']);
    return true;
}


//End CLASS

//Add global team to lead over 30days not take care
function addGlobalTeamToLead()
{
    $date_check = date('Y-m-d H:i:s', strtotime('-30days' . $GLOBALS['timedate']->nowDb()));
    $sql = "SELECT DISTINCT
    IFNULL(leads.id, '') primaryid
    FROM leads
    WHERE (((IFNULL(leads.status, '') NOT IN ('Converted'))
    AND (leads.last_call_date <= '$date_check')))
    AND leads.deleted = 0";
    $rs = $GLOBALS['db']->query($sql);
    while ($row = $GLOBALS['db']->fetchByAssoc($rs)) {
        $lead = BeanFactory::getBean('Leads', $row['primaryid']);
        $lead->load_relationship('teams');
        //Add the teams Global
        $lead->teams->add('1');
    }
    return true;
}


//############## LMS/ONLINE LEARNING ########################

/**
* Generate Online Learning
*/
function generateOnlineLearning()
{
    require_once("custom/include/lms/alovip-ggc/utils.php");
    require_once("include/externalAPI/ClassIn/utils.php");
    $limitAccessAPI = 30; //Giới hạn số lượt gọi API mỗi Cron - Ko bao giờ đc vượt quá 30

    global $timedate;
    $startEnd = $timedate->getDayStartEndGMT($timedate->getNow(true));
    $start_tz = $startEnd['start'];
    $end_tz   = date('Y-m-d H:i:s', strtotime("+14 day " . $startEnd['end']));

    //CREATE ONLINE CLASS
    $q1 = "SELECT DISTINCT
    IFNULL(meetings.id, '') primaryid,
    IFNULL(meetings.external_id, '') external_id,
    IFNULL(meetings.type, '') type,
    meetings.date_start date_start,
    meetings.date_end date_end,
    IFNULL(l1.id, '') class_id,
    IFNULL(l1.name, '') class_name,
    IFNULL(l1.status, '') status
    FROM meetings
    INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
    WHERE (meetings.date_start >= '$start_tz' AND meetings.date_start <= '$end_tz')
    AND (meetings.session_status <> 'Cancelled')
    AND (IFNULL(meetings.type, '') IN ('Google Meet' , 'ClassIn', 'Zoom'))
    AND (l1.status <> 'Closed') AND (meetings.external_id IS NULL OR meetings.external_id = '')
    AND meetings.deleted = 0
    ORDER BY meetings.date_end ASC";
    $rows = $GLOBALS['db']->fetchArray($q1);
    foreach ($rows as $key => $row) {
        if ($limitAccessAPI > 0) {
            $meeting = BeanFactory::getBean('Meetings', $row['primaryid'], array('disable_row_level_security' => true));
            if (!empty($meeting->id) && empty($meeting->external_id)) {
                //============GOOGLE MEET================
                if ($meeting->type == 'Google Meet') {
                    //API
                    $res = createGM($meeting);
                    if ($res['success']) $limitAccessAPI--;
                }
                //============CLASSIN================
                if ($meeting->type == 'ClassIn') {
                    $classInResult = createClassInLesson($meeting);
                    if ($classInResult) $limitAccessAPI--;
                }
            }
        } else break;
    }

    $nowDB = $timedate->nowDb();
    //handle deleted meeeting
    $limitAccessAPI = 30;
    $q2 = "SELECT DISTINCT
    IFNULL(meetings.id, '') primaryid,
    IFNULL(meetings.external_id, '') external_id,
    IFNULL(meetings.type, '') type,
    meetings.date_start date_start,
    meetings.date_end date_end,
    IFNULL(l1.id, '') class_id,
    IFNULL(l1.name, '') class_name,
    IFNULL(l1.status, '') status
    FROM meetings
    INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
    WHERE (meetings.date_start >= '$start_tz' AND meetings.date_start <= '$end_tz')
    AND (IFNULL(meetings.type, '') IN ('Google Meet' , 'ClassIn', 'Zoom'))
    AND (IFNULL(meetings.external_id, '') <> '')
    AND ((meetings.deleted = 1) OR (meetings.session_status = 'Cancelled'))";
    $rows = $GLOBALS['db']->fetchArray($q2);
    foreach ($rows as $key => $row) {
        if ($limitAccessAPI > 0) {
            $meeting = BeanFactory::retrieveBean('Meetings', $row['primaryid'], array(), false);
            $time_modified = $timedate->asDb($timedate->fromDb($meeting->date_modified)->modify("+5 minutes"));
            if (!empty($meeting->external_id)) {
                //============GOOGLE MEET================
                if ($meeting->type == 'Google Meet' && $time_modified < $nowDB) { //set thg đợi giữa 2 lần chỉnh sửa hạn chế gọi API liên tục
                    $res = deleteGM($meeting);
                    if ($res['success']) $limitAccessAPI--;
                }
                //============CLASSIN================
                if ($meeting->type == 'ClassIn') {
                    $res = deleteClassInLesson($meeting);
                    if ($res) $limitAccessAPI--;
                }
            }
        } else break;
    }
    return true;
}

function recheckOnlineLearning()
{
    require_once('include/externalAPI/Google/ExtAPIGoogle.php');
    global $timedate;
    $startEnd = $timedate->getDayStartEndGMT($timedate->getNow(true));
    $start_tz = $startEnd['start'];
    $end_tz   = date('Y-m-d H:i:s', strtotime("+7 days " . $startEnd['end']));

    //LIST EMS LINK
    $q1 = "SELECT DISTINCT
    IFNULL(meetings.id, '') primaryid,
    IFNULL(meetings.external_id, '') external_id
    FROM meetings
    INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
    WHERE (meetings.date_start >= '$start_tz' AND meetings.date_start <= '$end_tz')
    AND (meetings.session_status <> 'Cancelled')
    AND (IFNULL(meetings.type, '') IN ('Google Meet'))
    AND (l1.status <> 'Closed') AND (meetings.external_id IS NOT NULL AND meetings.external_id <> '') AND meetings.deleted = 0
    ORDER BY meetings.date_end ASC";
    $rows = $GLOBALS['db']->fetchArray($q1);
    $emsList = array_column($rows, 'external_id');
    //get event list On Google Calendar
    $apiList = array();
    $ExtAPIGoogle = new ExtAPIGoogle();
    $resAPI = $ExtAPIGoogle->getListEvent();
    foreach ($resAPI['events']['items'] as $_key => $event) {
        $apiList[] = $event['id'];
    }
    //remove redundant event
    foreach ($apiList as $key => $eventId) {
        if (!in_array($eventId, $emsList)) {
            $xBean = new stdClass();
            $xBean->external_id = $eventId;
            $res = $ExtAPIGoogle->deleteEvent($xBean);
        }
    }

    return true;
}

function notificationOnlineLearning()
{
    require_once("custom/include/lms/alovip-ggc/utils.php");
    require_once('custom/clients/mobile/helper/NotificationHelper.php');
    global $timedate;
    $startEnd = $timedate->getDayStartEndGMT($timedate->getNow(true));
    $start_tz = $startEnd['start'];
    $end_tz   = date('Y-m-d H:i:s', strtotime("+1 day " . $startEnd['end']));

    //NOTIFICATION MOBILE APPS
    $q1 = "SELECT DISTINCT
    IFNULL(meetings.id, '') primaryid,
    IFNULL(meetings.external_id, '') external_id,
    IFNULL(meetings.type, '') type,
    meetings.date_start date_start,
    meetings.date_end date_end,
    IFNULL(l1.id, '') class_id,
    IFNULL(l1.name, '') class_name,
    IFNULL(l1.status, '') status,
    IFNULL(meetings.join_url, '') join_url,
    IFNULL(l2.full_teacher_name, '') teacher_name
    FROM meetings
    INNER JOIN j_class l1 ON meetings.ju_class_id = l1.id AND l1.deleted = 0
    LEFT JOIN c_teachers l2 ON l2.id = meetings.teacher_id AND meetings.deleted = 0
    LEFT JOIN meetings_contacts mc ON meetings.id = mc.meeting_id and mc.deleted=0
    WHERE (meetings.date_start >= '$start_tz' AND meetings.date_start <= '$end_tz')
    AND (meetings.session_status <> 'Cancelled')
    AND (IFNULL(meetings.type, '') IN ('Google Meet' , 'ClassIn', 'Zoom'))
    AND (l1.status <> 'Closed') AND (meetings.external_id IS NOT NULL AND meetings.external_id <> '')
    AND meetings.deleted = 0
    ORDER BY meetings.date_end ASC";
    $rows = $GLOBALS['db']->fetchArray($q1);
    $now = substr_replace($timedate->nowDb(), '00', -2);
    foreach ($rows as $key => $row) {
        //10 minute perious & 60 minute perious
        foreach (['10', '60'] as $minute) {
            $checkTimeStart = $timedate->asDb($timedate->fromDb($row['date_start'])->modify("-$minute minutes"));
            //Get List Student
            if ($now == $checkTimeStart) {
                $listAttendees = getAttendees($row['primaryid']);
                $timeStartEnd = substr($timedate->to_display_time($row['date_start']), 0, 5) . ' - ' . $timedate->to_display_time($row['date_end']);
                foreach ($listAttendees['students'] as $_idx => $student) {
                    $notify = new NotificationMobile();
                    $notify->pushNotification("{$row['class_name']} - {$row['type']}", "Thời gian: $timeStartEnd \nGiáo viên: {$row['teacher_name']} \nUrl: {$row['join_url']}", 'Meetings', $row['primaryid'], $student['id']);
                }
            }
        }
    }
    return true;
}

//##############  LMS/ONLINE LEARNING ########################

//CUSTOM
function changeStatusReNew()
{
    $before30 = date('Y-m-d', strtotime("-90 day" . $GLOBALS['timedate']->nowDbDate()));

    $q1 = "SELECT DISTINCT IFNULL(leads.id, '') primaryid
    FROM leads WHERE (((leads.last_call_date < '$before30')
    AND (leads.last_pt_date < '$before30')
    AND (leads.last_visited < '$before30')))
    AND leads.deleted = 0 AND leads.status NOT IN ('Dead', 'Converted')";
    $leads = $GLOBALS['db']->fetchArray($q1);
    foreach ($leads as $key => $lead) {
        $bean = BeanFactory::getBean('Leads', $lead['primaryid'], array('disable_row_level_security' => true));
        $bean->status = 'Re-new';
        $bean->update_date_modified = false;
        $bean->update_modified_by = false;
        $bean->save();
    }

    return true;
}

function recordingVihat()
{
    global $db;
    $admin = new Administration();
    $admin->retrieveSettings();
    $voip = $admin->settings['callcenter_username'];
    $res = callApi('GET', 'https://public-v1-stg.omicall.com/api/auth?apiKey=' . $voip);
    $res = json_decode($res, 1);
    if (!empty($res['payload']['access_token'])) {
        $page = 1;
        $from_date = strtotime(date('Y-m-d', time() - 3600) . ' 00:00:00') * 1000;
        $to_date = strtotime(date('Y-m-d', time()) . ' 23:59:59') * 1000;
        loopGetLogCalls:
        $callsRes = callApi('GET', 'https://public-v1-stg.omicall.com/api/call_transaction/list?from_date=' . $from_date . '&to_date=' . $to_date . '&page=' . $page, array(), array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $res['payload']['access_token']
        ));
        $callsRes = json_decode($callsRes, 1);
        if (!empty($callsRes['payload']['items'])) {
            foreach ($callsRes['payload']['items'] as $call) {
                $call_id = '';
                if ($call['disposition'] == 'answered' && $call['direction'] == 'outbound') {
                    $call_id = $db->getOne("select id from calls where  (cj_url is null or cj_url<>'-1')
                        and deleted=0
                        and call_destination='{$call['destination_number']}'
                        order by date_entered DESC");
                }
                if ($call['disposition'] == 'answered' && $call['direction'] == 'inbound') {
                    $call_id = $db->getOne("select id from calls where  (cj_url is null or cj_url<>'-1')
                        and deleted=0
                        and call_source='{$call['source_number']}'
                        order by date_entered DESC");
                }
                if (!empty($call_id)) {
                    $db->query("update calls set cj_url='-1', call_recording='{$call['recording_file']}'
                        where id='$call_id'");
                }
            }
            $page++;
            goto loopGetLogCalls;
        }
    }
    return true;
}

//Tự động merge các trường có Trùng tên lại với nhau
function mergeSchoolName()
{
    global $timedate;
    $q1 = "SELECT  l1.name school_name, COUNT(l1.id) count, GROUP_CONCAT( DISTINCT l1.id ORDER BY l1.date_entered) school_ids
    FROM j_school l1 WHERE l1.deleted = 0
    GROUP BY l1.name
    HAVING COUNT(l1.id) > 1";
    $rs1 = $GLOBALS['db']->query($q1);
    while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
        $schoolIDs = explode(",", $row['school_ids']);
        foreach ($schoolIDs as $ind_ => $schoolId) {
            if ($ind_ > 0) {
                $GLOBALS['db']->query("UPDATE leads SET school_id='{$schoolIDs[0]}' WHERE school_id='$schoolId'");
                $GLOBALS['db']->query("UPDATE contacts SET school_id='{$schoolIDs[0]}' WHERE school_id='$schoolId'");
                $GLOBALS['db']->query("UPDATE prospects SET school_id='{$schoolIDs[0]}' WHERE school_id='$schoolId'");
                $GLOBALS['db']->query("UPDATE j_school SET description = 'Duplicated Merge by Scheduler' WHERE id='$schoolId' AND deleted=0");
                $GLOBALS['db']->query("UPDATE j_school SET deleted=1, date_modified='{$timedate->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE id='$schoolId' AND deleted=0");
            }
        }
    }
    return true;
}

function recordingVoiceCloud()
{
    global $db;
    $admin = new Administration();
    $admin->retrieveSettings();
    $callcenter_supplier = $admin->settings['callcenter_supplier'];

    if ($callcenter_supplier == 'VoiceCloudV2') {
        $logs = getSocketLogsCallTKT(['name' => 'voicecloud-' . strtolower($GLOBALS['dotb_config']['unique_key'])]);
        foreach ($logs as $log) {
            if ($log["body"]["data"]["status"] == 'ANSWER') {
                $calls = $db->fetchArray("select id from calls where call_destination = '{$log['body']['data']['phone']}' and call_source = '{$log['body']['data']['extension']}' and deleted = 0 and date_add(date_start, interval 8 hour) > '{$log['body']['data']['calldate']}' and (call_recording is null or call_recording = '')");
                if (count($calls) == 1) {
                    $db->query("update calls set outlook_id='{$log['body']['data']['callid']}', call_recording='{$log['body']['data']['recordingfile']}' where id='{$calls[0]['id']}'");
                    removeSocketLogsCallTKT($log['_id']);
                    continue;
                }
            }
            if (time() - strtotime($log['body']['data']['calldate']) > 900) {
                removeSocketLogsCallTKT($log['_id']);
            }
        }
    }

    return true;
}

//Recording VOIP24H by Hoang Hvy
function recordingVOIP24h2()
{
    $admin = new Administration();
    $admin->retrieveSettings();
    $center_array = array('Voip24h', 'Rtc', 'Newrtc', 'Webrtc');
    if (in_array($admin->settings['callcenter_supplier'], $center_array)) {
        global $dotb_config;
        $name = 'voip24h-' . $dotb_config['brand_id'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://socket.dotb.cloud/get-logs-call?token=dotb&limit=500',
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS => json_encode(array(
                'name' => $name,
                'sync' => false
            )),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));
        $respone = curl_exec($curl);
        if ($respone == true) {
            $http_info = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($http_info == 200) {
                $result = json_decode($respone, true);
                foreach ($result as $call) {
                    if ($call['body']['state'] == 'Cdr' && !empty($call['body']['phone'])) {
                        $call_time_bot = date('Y-m-d H-i-s', strtotime($call['body']['cdr']['starttime'] . '-7 hours -1 minutes'));
                        $call_time_top = date('Y-m-d H-i-s', strtotime($call['body']['cdr']['starttime'] . '-7 hours +1 minutes'));
                        if ($call['body']['type'] == 'outbound') {
                            $ext = "AND call_destination LIKE '%" . $call['body']['phone'] . "%' AND call_source LIKE '%" . $call['body']['extend'] . "%'";
                        } else if ($call['body']['type'] == 'inbound') {
                            $ext = "AND call_source LIKE '%" . $call['body']['phone'] . "%' AND call_destination LIKE '%" . $call['body']['extend'] . "%'";
                        }
                        $GLOBALS['db']->query("UPDATE calls SET outlook_id = '{$call['body']['callid']}' WHERE date_start > '{$call_time_bot}' AND date_start < '{$call_time_top}' {$ext} AND deleted = 0 AND (outlook_id IS NULL OR outlook_id = '')");
                    }
                    $curl1 = curl_init();
                    curl_setopt_array($curl1, array(
                        CURLOPT_URL => 'https://socket.dotb.cloud/update-webhook?token=dotb',
                        CURLOPT_CUSTOMREQUEST => 'POST',
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_POSTFIELDS => json_encode(array(
                            '_id' => $call['_id']
                        )),
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json',
                        ),
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => false,
                    ));
                    curl_exec($curl1);
                    curl_close($curl1);
                }
            }
        }
    }
    curl_close($curl);
    return true;
}

//Update Reconcile Status
function updateReconcileStatus()
{
    global $timedate;
    $timeback = $timedate->asDb($timedate->getNow()->modify("-5 minutes"));
    //$GLOBALS['db']->query("UPDATE j_paymentdetail SET reconcile_status = '' WHERE reconcile_status = 'processing' AND deleted = 0 AND date_modified <= '$timeback'");
    $rows = $GLOBALS['db']->fetchArray("SELECT IFNULL(id, '') primaryid FROM j_paymentdetail WHERE reconcile_status = 'processing' AND deleted = 0 AND date_modified <= '$timeback'");
    foreach ($rows as $key => $row) {
        $pmd = BeanFactory::getBean('J_PaymentDetail', $row['primaryid'], array('disable_row_level_security' => true));
        $pmd->reconcile_status = '';
        $pmd->update_date_modified = false;
        $pmd->update_modified_by = false;
        $pmd->save();
    }
    return true;
}

function callCenterJob()
{
    global $db;
    $admin = new Administration();
    $admin->retrieveSettings();
    $callcenter_supplier = $admin->settings['callcenter_supplier'];
    $user = $admin->settings['callcenter_username'];
    $pass = $admin->settings['callcenter_password'];

    if ($callcenter_supplier == 'VoiceCloudV2') {
        $logs = getSocketLogsCallTKT(['name' => 'voicecloud-' . strtolower($GLOBALS['dotb_config']['unique_key'])]);
        foreach ($logs as $log) {
            if ($log["body"]["data"]["status"] == 'ANSWER') {
                $calls = $db->fetchArray("select id from calls where call_destination = '{$log['body']['data']['phone']}' and call_source = '{$log['body']['data']['extension']}' and deleted = 0 and date_add(date_start, interval 8 hour) > '{$log['body']['data']['calldate']}' and (call_recording is null or call_recording = '')");
                if (count($calls) == 1) {
                    $db->query("update calls set outlook_id='{$log['body']['data']['callid']}', call_recording='{$log['body']['data']['recordingfile']}' where id='{$calls[0]['id']}'");
                    removeSocketLogsCallTKT($log['_id']);
                    continue;
                }
            }
            if (time() - strtotime($log['body']['data']['calldate']) > 900) {
                removeSocketLogsCallTKT($log['_id']);
            }
        }
    } else if ($callcenter_supplier == 'Voip24h' || $callcenter_supplier == 'RTC') {
        $logs = getSocketLogsCallTKT(['name' => 'voip24h-' . strtolower($GLOBALS['dotb_config']['unique_key'])]);
        foreach ($logs as $log) {
            if ($log["body"]["state"] == 'Cdr') {
                $startTime = strtotime($log["body"]["cdr"]["starttime"]) - 10;
                $endTime = strtotime($log["body"]["cdr"]["endtime"]) + 10;
                $result = callApi('GET', "http://dial.voip24h.vn/dial/search?voip=$user&secret=$pass&source={$log["body"]["extend"]}&destination={$log["body"]["cdr"]["destination"]}&date_start=$startTime&date_end=$endTime");
                $result = json_decode($result, 1);
                if (empty($result['result'])) continue;
                if (count($result["result"]["data"]) == 1) {
                    $calls = $db->fetchArray("select id from calls where call_destination = '{$log["body"]["cdr"]["destination"]}' and call_source = '{$log["body"]["extend"]}' and deleted = 0 and date_add(date_start, interval 6 hour) < '{$log["body"]["cdr"]["starttime"]}' and date_add(date_start, interval 8 hour) > '{$log["body"]["cdr"]["endtime"]}' and (call_recording is null or call_recording = '' or call_recording like '/upload/%')");
                    if (count($calls) == 1) {
                        $db->query("update calls set outlook_id='{$result["result"]["data"][0]["callid"]}', call_recording='{$result["result"]["data"][0]["recording"]}', duration={$result["result"]["data"][0]["duration"]} where id='{$calls[0]['id']}'");
                    }
                    removeSocketLogsCallTKT($log['_id']);
                    continue;
                }
            }
            if (in_array($log['body']['state'], ['Ring', 'Up', 'Hangup'])) {
                removeSocketLogsCallTKT($log['_id']);
            }
        }
    } else if($callcenter_supplier == 'FPTOncall'){
        //login get token
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://fti.oncall.vn:8899/api/account/credentials/verify',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "name": "' . $user . '",
            "password": "' . $pass . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        $token = $response['access_token'];
        //get calls
        $startTime = date("Y-m-d%20H:i:00",time()-600);
        $endTime = date("Y-m-d%20H:i:00");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://fti.oncall.vn:8899/api/recordfiles/extension/list?pagesize=1000&start_time=$startTime&end_time=$endTime",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'access_token: '.$token
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response=json_decode($response, true);
        $calls = $response["items"];
        foreach($calls as $call){
            $callStartTime = substr($call['start_time'],0,19);
            $callStartTime=date("Y-m-d H:i:s",strtotime($callStartTime)-25200);
            $recording="https://fti.oncall.vn:8883/filedown/{$call['filepath']}?filename={$call['filename']}";
            $calldbs = $db->fetchArray("select id from calls where call_destination = '{$call['callee']}' and deleted = 0 and cj_url is null and date_modified>'$callStartTime'");
            if (count($calldbs) == 1) {
                $db->query("update calls set outlook_id='{$call['id']}', call_recording='$recording', cj_url='-1' where id='{$calldbs[0]['id']}'");
            }
        }
    }

    return true;
}


function updateSessionStatusByMinute(){
    //Update Finished
    $today = date('Y-m-d H:i:00', strtotime($GLOBALS['timedate']->nowDb()));

    $sss = $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(meetings.id, '') primaryid
        FROM meetings
        WHERE (meetings.session_status <> 'Cancelled')
        AND meetings.meeting_type IN ('Session' , 'Placement Test', 'Demo')
        AND (meetings.date_end = '$today') AND meetings.deleted = 0");
    foreach ($sss as $key => $value) {
        $bean = BeanFactory::getBean('Meetings', $value['primaryid'], array('disable_row_level_security' => true));
        $bean->session_status = 'Finished';
        $bean->process_trigger = 1;
        //Thêm dòng này sẽ khi lưu sẽ không thay đổi Date Modified & Modified By
        $bean->update_date_modified = false;
        $bean->update_modified_by = false;
        $bean->save();
    }

    //Update In Progress
    $sss2 = $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(meetings.id, '') primaryid
        FROM meetings
        WHERE (meetings.session_status <> 'Cancelled')
        AND meetings.meeting_type IN ('Session' , 'Placement Test', 'Demo')
        AND (meetings.date_start = '$today')
        AND meetings.deleted = 0");
    foreach ($sss2 as $key => $value) {
        $bean = BeanFactory::getBean('Meetings', $value['primaryid'], array('disable_row_level_security' => true));
        $bean->session_status = 'In Progress';
        $bean->process_trigger = 1;
        $bean->update_date_modified = false;
        $bean->update_modified_by = false;
        $bean->save();
    }

    return true;
}
//Metrikal Cronjob
function metrikalAlert() {
    require_once ('custom/clients/mobile/helper/NotificationHelper.php');
    $notify = new NotificationMobile();
    $GLOBALS['current_user'] = BeanFactory::getBean('Users', '1');

    $q = "SELECT
    IFNULL(m_metrikalalert.id, '') AS primaryid,
    IFNULL(m_metrikalalert.name, '') AS primaryname,
    m_metrikalalert.date_entered,
    IFNULL(m_metrikalalert.report_id, '') AS report_id,
    IFNULL(m_metrikalalert.report_name, '') AS report_name,
    IFNULL(m_metrikalalert.target, '') AS target,
    IFNULL(m_metrikalalert.assigned_user_id, '') AS user_id,
    IFNULL(sr.content, '') AS report_content
    FROM m_metrikalalert
    INNER JOIN saved_reports sr ON m_metrikalalert.report_id = sr.id AND sr.deleted = 0
    WHERE m_metrikalalert.deleted = 0
    AND DATEDIFF(NOW(), m_metrikalalert.date_entered) < 90";
    $result = $GLOBALS['db']->fetchArray($q);
    $report_user_array = [];
    foreach($result as $item){
        $report_user_array[$item['report_id']]['user_list'][$item['user_id']][] = ['value' => $item['target'], 'name' => $item['primaryname'], 'alert_id' => $item['primaryid']];
        $report_user_array[$item['report_id']]['content'] = $item['report_content'];
        $report_user_array[$item['report_id']]['name'] = $item['report_name'];
    }
    foreach($report_user_array as $report_id => $report_info){
        $compare_value = calculateCompareValue($report_id, $report_info['content']);
        if($compare_value != 0){
            foreach ($report_info['user_list'] as $user_id => $target_info){
                foreach($target_info as $target_detail){
                    if($target_detail['value'] >= $compare_value){
                        $sent_noti = $notify->pushNotificationMetrikal($report_info['name'], $target_detail['name'] .' '. $compare_value, $user_id, $report_id);
                        if($sent_noti){
                            $GLOBALS['db']->query("UPDATE m_metrikalalert SET deleted = 1 WHERE id = '{$target_detail['alert_id']}'");
                        }
                    }
                }
            }
        }
    }
    return true;
}
function calculateCompareValue($report_id, $content){
    $scaleReporter = new Report(str_replace('&quot;', '"', $content));
    $scaleReporter->is_saved_report = true;
    $scaleReporter->saved_report_id = $report_id;
    $scaleReporter->run_summary_query();
    $result = $GLOBALS['db']->fetchArray($scaleReporter->summary_query);
    foreach($scaleReporter->report_def['summary_columns'] as $summary){
        if(!empty($summary['group_function']) || isset($summary['group_function'])) $groupFunction = $summary['group_function'];
    }
    $compare_value = 0;
    $found_current_month = false;
    foreach($result as $summary_value){
        if($found_current_month) break;
        foreach($summary_value as $key => $value2){
            if(strpos(strtolower($key), '_month')){
                if(date('Y-m', strtotime($value2)) == date('Y-m', strtotime($GLOBALS['timedate']->nowDbDate()))){
                    $found_current_month = true;
                } else {
                    break;
                }
            } else if(strpos(strtolower($key), '_'.$groupFunction) && $found_current_month) {
                $compare_value = (double)$value2;
                break;
            }
        }
    }
    return $compare_value;
}

function s3Upload() {

    require_once 'custom/include/AwsSdkPhp/class.aws_sdk.php';
    //Check connection S3
    $AWS = new AWSHelper();
    if (!$AWS->getS3()) return array('success' => 0);

    $module_upload = [
        'C_Gallery',
        'C_Comments',
        'C_News'
    ];
    $dir_map = [
        'C_Gallery' => 'gallery',
        'C_Comments' => 'comments',
        'C_News' => 'news'
    ];
    //Module that have the S3 field to upload image
    $module_image = [
        'C_News'
    ];

    $module_list_str = implode("','", $module_upload);

    //Delete file:
    // - Case 1: File_source = 'S3' && upload_id is not empty -> Delete file on S3
    // - Case 2: File_source = '' && upload_id is empty -> Delete file on local
    $deleteNoteSql = "DELETE FROM notes WHERE parent_id IS NULL";
    $GLOBALS['db']->query($deleteNoteSql);

    $sqlNoteDelete = "SELECT IFNULL(id,'') id,
    IFNULL(name,'') note_name,
    IFNULL(upload_id,'') upload_id,
    IFNULL(file_ext,'') file_ext,
    IFNULL(file_mime_type,'') file_mime_type
    FROM notes
    WHERE parent_type IN ('$module_list_str') AND deleted = 1 AND upload_id <> ''";
    $resNoteDelete = $GLOBALS['db']->fetchArray($sqlNoteDelete);

    //Case 1: Delete on S3
    $upload_list = array_column($resNoteDelete, 'upload_id');
    $result = $AWS->delete($upload_list);
    if($result['success']){
        $deleteNoteSql = "DELETE FROM notes WHERE upload_id IN ('" . implode("','", $upload_list) . "')";
        $GLOBALS['db']->query($deleteNoteSql);
    }

    //Push file to S3
    $sqlNote = "SELECT IFNULL(id,'') id,
    IFNULL(name,'') note_name,
    IFNULL(file_ext,'') file_ext,
    IFNULL(parent_type,'') parent_type,
    IFNULL(parent_id,'') parent_id,
    IFNULL(file_mime_type,'') file_mime_type
    FROM notes
    WHERE parent_type IN ('$module_list_str')
    AND deleted = 0 AND (file_source = '' OR file_source IS NULL) AND (upload_id = '' OR upload_id IS NULL)
    AND parent_id <> ''
    LIMIT 50";
    $resNote = $GLOBALS['db']->fetchArray($sqlNote);

    $uncompleted = [];
    foreach($resNote as $note){
        $path = $GLOBALS['dotb_config']['brand_id'].'/'.$dir_map[$note['parent_type']];
        $fileName = $note['note_name'];
        //Field image dont have extension
        if(in_array($note['parent_type'], $module_image)) $fileName = $note['id'];
        $sourceFile = 'upload://s3_storage/' . $fileName;

        if(file_exists($sourceFile)) {
            $result = $AWS->uploadAWS($path, $fileName, $sourceFile);
            if ($result['success']) {
                $upload_id = $result['key_name'];
                $sqlUpdateNote = "UPDATE notes
                SET file_source = 'S3', upload_id = '$upload_id'
                WHERE id = '" . $note['id'] . "'";
                $GLOBALS['db']->query($sqlUpdateNote);

                if (in_array($note['parent_type'], $module_image)) {
                    $focus = BeanFactory::getBean($note['parent_type'], $note['parent_id']);
                    foreach ($focus->field_defs as $field => $def) {
                        if ($def['type'] == 'image' && $def['s3_upload']) {
                            $focus->$field = $upload_id;
                            $focus->update_date_modified = false;
                            $focus->update_modified_by = false;
                            $focus->save();
                            break;
                        }
                    }
                }
                $completedList[] = $fileName;
            } else {
                $uncompleted[] = $fileName;
            }
        } else {
            $sqlNoteDelete = "DELETE FROM notes WHERE id = '" . $note['id'] . "'";
            $GLOBALS['db']->query($sqlNoteDelete);
        }
    }
    //To empty the s3_storage folder
    $dirPath = 'upload/s3_storage/';
    $files = scandir('upload/s3_storage');
    foreach ($files as $file) {
        if ($file != "." && $file != ".." && !in_array($file, $uncompleted) && in_array($file, $completedList)) {
            if ($file != "." && $file != ".." && !in_array($file, $uncompleted)) {
                $filePath = $dirPath . $file;
                unlink($filePath); // Delete file
            }
        }
    }

    return true;
}

function checkTenantLicense() {
    global $license;
    if (isset($license['lic_tenant_id'])) {
        require_once 'custom/clients/base/api/TenantApi.php';
        $licenseAPI = new TenantApi();
        $api = new RestService();
        $rest = $licenseAPI->getLicense($api, array(
            'lic_tenant_name' => $license['lic_tenant_name']
        ));

        if ($rest['success'] == true) {

            /// Lấy license từ quyettam
            $quyettamLicense = new LicenseBean();
            $quyettamLicense->fromJson($rest['data']);
            //updateActiveStudent(); //update Active student
            /// Lấy license của client
            $currentLicense = new LicenseBean();
            $currentLicense->fromClient();
            $currentLicense->countTotalUser();
            $currentLicense->countTotalSubscription();

            /// update active  subscription and user
            $quyettamLicense->lic_activated_users = $currentLicense->lic_activated_users;
            $quyettamLicense->lic_activated_subscription = $currentLicense->lic_activated_subscription;


            /// cập nhật ngày validate cho quyettam
            $quyettamLicense->lic_last_validation_success = date('Y-m-d H:i:s');

            $quyettamLicense->save();

            $licenseAPI->validateLicense($api, array(
                'tenant_name' => $currentLicense->lic_tenant_name,
                'lic_activated_users' => $quyettamLicense->lic_activated_users,
                'lic_activated_subscription' => $quyettamLicense->lic_activated_subscription,
                'lic_last_validation_success' => $quyettamLicense->lic_last_validation_success,
            ));
        }
    }

    return true;
}


//############## Customize Jaxtina ########################
function fillAttendanceJaxtina() {
    require ('custom/include/_helper/junior_class_utils.php');

    $today = date('Y-m-d');
    $Finished = date('Y-m-d', strtotime("-7 day " . $today));
    //lấy tất cả kết quả điểm danh theo lớp
    $sss = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(l2.id, '') class_id,
        IFNULL(l3.id, '') meeting_id, IFNULL(l3.lesson_number, '') lesson_number,
        IFNULL(l4.id, '') student_id, IFNULL(l4.lms_user_name, '') lms_user_name,
        IFNULL(att.id, '') attendance_id
        FROM c_attendance att
        INNER JOIN j_class l2 ON att.class_id = l2.id AND l2.deleted = 0
        INNER JOIN meetings l3 ON att.meeting_id = l3.id AND l3.deleted = 0
        INNER JOIN contacts l4 ON att.student_id = l4.id AND l4.deleted = 0 AND att.student_type = 'Contacts'
        WHERE (l3.date >= '$Finished' AND l3.date <= '$today') AND (l3.session_status <> 'Cancelled')
        AND (IFNULL(l4.lms_user_name, '') <> '') AND att.deleted = 0
        ORDER BY class_id");
    $classId = '###';
    foreach ($sss as $key => $value){
        if($classId != $value['class_id']){
            // goi api lay ket qua
            $classId = $value['class_id'];
            $class_res = loadClassLMSResult($classId);
        }
        //Get Student result
        $result = $class_res[$value['lms_user_name']][$value['lesson_number']];
        if(!empty($result)){
            $u1 = "UPDATE c_attendance SET
            total_score = {$result['totalScores']},
            number_of_part = {$result['numberPart']},
            number_of_question = ".($result['totalQuestions']).",
            number_of_right = {$result['rightAns']},
            number_of_wrong = {$result['wrongAns']},
            total_time = {$result['timeOnline']},
            wrong_ans_list = '{$result['wrongAnsDetail']}'
            WHERE id = '{$value['attendance_id']}'";
            $GLOBALS['db']->query($u1);
        }
    }
    return true;
}
//Lock lock_status
function lockAttendanceStatus() {
    $GLOBALS['db']->query("UPDATE meetings SET lock_status = 'Locked' WHERE date <= '2024-08-15' AND deleted = 0");
    $today = date('Y-m-d');
    //lấy tất cả các buổi quá khứ chưa lock
    $sss = $GLOBALS['db']->fetchArray("SELECT DISTINCT
        IFNULL(meetings.id, '') primaryid
        FROM meetings WHERE (meetings.session_status <> 'Cancelled')
        AND meetings.meeting_type = 'Session'
        AND meetings.lock_status = 'Unlocked'
        AND (meetings.date < '$today') AND meetings.deleted = 0");
    foreach ($sss as $key => $value){
        $bean = BeanFactory::getBean('Meetings', $value['primaryid'], array('disable_row_level_security' => true));
        $bean->lock_status = 'Locked';
        $bean->save();
    }
    return true;
}