<?php

$mod_strings = array(
    //LMS/Online learning
    'LBL_GENERATEONLINELEARNING' => '*b* Generate LMS/Online Learning',
    'LBL_NOTIFICATIONONLINELEARNING' => '*b* Notification LMS/Online Learning',
    'LBL_RECHECKONLINELEARNING' => '*b* Re-check and remove LMS/Online Learning',
    'LBL_RECORDINGVOIP24H' => '*b* Get Callcenter Recording VOIP24H',
    'LBL_GENERATECANVASAPI' => '*b* Generate Canvas Learning',
    'LBL_CHANGESTATUSRENEW' => '*b* Change Lead Status to Re-new: After 30 days no activities',
    'LBL_RECORDINGVIHAT' => '*b* get recording callcenter VIHAT',

    'LBL_AUTOUPDATESTUDENTSTATUS' => '*.b* Auto-Update Student Status',
    'LBL_UPDATEAGE' => '*.b* Update Age Student/Lead/Target ',
    'LBL_UPDATESESSIONSTATUS' => '*.b* Update Session Status - By Day',
    'LBL_UPDATECLASSSTATUS' => '*.b* Auto-Update Finish Class',
    'LBL_MERGESCHOOLNAME' => '*.b* Auto Merge School Name',
    'LBL_RECORDINGVOICECLOUD' => '*.b* Get recording voice cloud',
    'LBL_RECORDINGVOIP24H2' => '*b* Get Callcenter Recording VOIP24Hv2',
    'LBL_UPDATERECONCILESTATUS' => '*b* Update Reconcile Status Processing -> -none-',
    'LBL_CALLCENTERJOB' => '*b* Callcenter job - require for all site',
    'LBL_RECHECKVOIPRECORDING' => '*b* Callcenter Recording VOIP24H - Re-check missing',
    'LBL_UPDATESESSIONSTATUSBYMINUTE' => '*.b* Update Session Status - By Minute',
    'LBL_METRIKALALERT' => '*.b* Metrikal Alert',
    'LBL_S3UPLOAD' => '*.b* Upload S3',
    'LBL_RECODINGVOIP24HIPPHONE' => '*.b* Recoding Voip24H ipphone',
    'LBL_CHECKTENANTLICENSE' => '*.b* Check Tenant License',
    'LBL_VOIP24HRECORDINGV4' => '*.b* Recoding Voip24H V4 - final',
    'LBL_CHECKTENANTLICENSE' => '*.b* Check Tenant License',
    'LBL_VOIP24HFIXCALLDURATION' => 'fix call duration',
    'LBL_LOCKATTENDANCESTATUS' => '*.b* Lock Session Attendance',
);
