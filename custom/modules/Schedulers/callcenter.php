<?php
/**
 * Add By TranKhanhToan
 */
if (!function_exists('getSocketLogsCallTKT')) {
    function getSocketLogsCallTKT($body)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://socket.dotb.cloud/get-logs-call?token=dotb&limit=1000',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response, true);
    }
}

if (!function_exists('removeSocketLogsCallTKT')) {
    function removeSocketLogsCallTKT($_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://socket.dotb.cloud/remove-webhook?token=dotb',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{ "_id":"' . $_id . '" }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        curl_exec($curl);
        curl_close($curl);
        return true;
    }
}

if (!function_exists('callApi')) {
    function callApi($method, $url, $params = array(), $header = array("Content-Type: application/json"))
    {
        $auth_request = curl_init($url);
        curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($auth_request, CURLOPT_HEADER, false);
        curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
        if ($method == 'POST') {
            curl_setopt($auth_request, CURLOPT_POST, 1);
        } elseif ($method == 'GET') {
            curl_setopt($auth_request, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($auth_request, CURLOPT_HTTPHEADER, $header);

        if ($method != 'GET') {
            if (count($params) > 0) {
                $json_arguments = json_encode($params);
                curl_setopt($auth_request, CURLOPT_POSTFIELDS, $json_arguments);
            }
        } else {
            $tmp = '?';
            foreach ($params as $key => $value) {
                $tmp = $key . '=' . $value . '&';
            }
            $tmp = rtrim($tmp, '&');
            if ($tmp != '?') {
                $auth_request = curl_init($url . $tmp);
                curl_setopt($auth_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
                curl_setopt($auth_request, CURLOPT_HEADER, false);
                curl_setopt($auth_request, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($auth_request, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($auth_request, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($auth_request, CURLOPT_FOLLOWLOCATION, 0);
                curl_setopt($auth_request, CURLOPT_HTTPHEADER, $header);
            }
        }

        $oauth2_token_response = curl_exec($auth_request);
        return $oauth2_token_response;
    }
}
