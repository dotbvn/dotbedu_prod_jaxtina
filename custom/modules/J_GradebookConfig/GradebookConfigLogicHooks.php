<?php
class GradebookConfigLogicHooks{
    function setName(&$bean, $event, $arg) {
            $code_field = 'gradebook_setting_name';
            if (empty($bean->$code_field) || $bean->$code_field == 'Auto-Generate') {
                $prefix = 'GS';
                $first_pad = '000000';
                $padding = 6;
                $query = "SELECT $code_field FROM {$bean->table_name} WHERE ( $code_field <> '' AND $code_field IS NOT NULL) AND id != '{$bean->id}' AND (LEFT($code_field, " . (strlen($prefix)) . ") = '" . $prefix . "') ORDER BY RIGHT($code_field, $padding) DESC LIMIT 1";

                $result = $GLOBALS['db']->query($query);
                if ($row = $GLOBALS['db']->fetchByAssoc($result))
                    $last_code = $row[$code_field];
                else
                    //no codes exist, generate default - PREFIX + CURRENT YEAR +  SEPARATOR + FIRST NUM
                    $last_code = $prefix . $sep . $first_pad;


                $num = substr($last_code, -$padding, $padding);
                $num++;
                $pads = $padding - strlen($num);
                $new_code = $prefix . $sep;

                //preform the lead padding 0
                for ($i = 0; $i < $pads; $i++)
                    $new_code .= "0";
                $new_code .= $num;

                //write to database - Logic: Before Save
                $bean->$code_field = $new_code;
            }

            //Copy Content trong TH duplicate
            if($_POST['duplicateSave'] && !empty($_POST['duplicateId']) && $_POST['module'] == $bean->module_name){
                $beanDup = BeanFactory::getBean('J_GradebookConfig', $_POST['duplicateId']);
                $bean->content = html_entity_decode($beanDup->content);
            }
        }

    function processRecording(&$bean, $event, $arguments){

        switch ($bean->type) {
            case "Overall":
                $bean->type = "<span style='white-space: nowrap;' class='textbg_orange'>{$bean->type}</span>";
                break;
            case "P01":case "P02":case "P03":case "P04":case "P05":case "P06":case "P07":case "P08":case "P09":case "P10":case "P11":case "P12":
                $bean->type = "<span style='white-space: nowrap;' class='textbg_bluelight'>".$GLOBALS['app_list_strings']['gradebook_type_options'][$bean->type]."</span>";
                break;
            case "Commitment":
                $bean->type = "<span style='white-space: nowrap;' class='textbg_green'>{$bean->type}</span>";
                break;
            default:
                $bean->type = "<b>{$bean->type}</b>";
                break;
        }
    }

}
?>
