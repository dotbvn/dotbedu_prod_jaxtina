<?php
    $module_name = 'J_GradebookConfig';
    $viewdefs[$module_name] =
    array (
        'QuickCreate' =>
        array (
            'templateMeta' =>
            array (
                'maxColumns' => '2',
                'widths' =>
                array (
                    0 =>
                    array (
                        'label' => '10',
                        'field' => '30',
                    ),
                    1 =>
                    array (
                        'label' => '10',
                        'field' => '30',
                    ),
                ),
                'javascript' => '{dotb_getscript file="custom/modules/J_GradebookConfig/js/editview.js"}',
                'useTabs' => false,
                'tabDefs' =>
                array (
                    'DEFAULT' =>
                    array (
                        'newTab' => false,
                        'panelDefault' => 'expanded',
                    ),
                ),
            ),
            'panels' =>
            array (
                'default' =>
                array (
                    0 =>
                    array (
                        0 =>
                        array (
                            'name' => 'gradebook_setting_name',
                            'label' => 'LBL_GRADEBOOK_SETTING_NAME',
                            'customCode' => '<input type="text" class="input_readonly" readonly name="gradebook_setting_name" id="gradebook_setting_name" maxlength="255" value="{$code}" title="{$MOD.LBL_GRADEBOOK_SETTING_NAME}" size="30">',
                        ),
                        1 =>
                        array (
                            'name' => 'type',
                            'label' => 'LBL_TYPE',
                        ),
                    ),
                    1 =>
                    array (
                        0 => 'name',
//                        1 =>
//                        array (
//                            'name' => 'gbsettinggroup_name',
//                            'label' => 'LBL_GBSETTINGGROUP',
//                        ),
                    ),
                    2 =>
                    array (
                        0 => 'description',
                    ),
                    3 =>
                    array (
                        0 => 'assigned_user_name',
                    ),
                    4 =>
                    array (
                        0 =>
                        array (
                            'name' => 'date_entered',
                            'comment' => 'Date record created',
                            'studio' =>
                            array (
                                'portaleditview' => false,
                            ),
                            'label' => 'LBL_DATE_ENTERED',
                        ),
                        1 =>
                        array (
                            'name' => 'date_modified',
                            'comment' => 'Date record last modified',
                            'studio' =>
                            array (
                                'portaleditview' => false,
                            ),
                            'label' => 'LBL_DATE_MODIFIED',
                        ),
                    ),
                ),
            ),
        ),
    );
