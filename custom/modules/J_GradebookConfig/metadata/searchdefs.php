<?php
$module_name = 'J_GradebookConfig';
$searchdefs[$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10',
      ),
      'koc_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_KOC_NAME',
        'width' => '10',
        'default' => true,
        'id' => 'KOC_ID',
        'name' => 'koc_name',
      ),
      'type' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE',
        'width' => '10',
        'default' => true,
        'name' => 'type',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'type' => 'name',
        'link' => true,
        'default' => true,
        'width' => '10',
        'label' => 'LBL_NAME',
        'name' => 'name',
      ),
      'koc_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_KOC_NAME',
        'id' => 'KOC_ID',
        'width' => '10',
        'default' => true,
        'name' => 'koc_name',
      ),
      'level' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_LEVEL',
        'width' => '10',
        'name' => 'level',
      ),
      'type' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE',
        'width' => '10',
        'default' => true,
        'name' => 'type',
      ),
      'created_by' => 
      array (
        'type' => 'assigned_user_name',
        'readonly' => true,
        'label' => 'LBL_CREATED',
        'width' => '10',
        'default' => true,
        'name' => 'created_by',
      ),
      'date_modified' => 
      array (
        'type' => 'datetime',
        'studio' => 
        array (
          'portaleditview' => false,
        ),
        'readonly' => true,
        'label' => 'LBL_DATE_MODIFIED',
        'width' => '10',
        'default' => true,
        'name' => 'date_modified',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'studio' => 
        array (
          'portaleditview' => false,
        ),
        'readonly' => true,
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10',
        'default' => true,
        'name' => 'date_entered',
      ),
      'team_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'studio' => 
        array (
          'portallistview' => false,
          'portaldetailview' => false,
          'portaleditview' => false,
        ),
        'label' => 'LBL_TEAMS',
        'id' => 'TEAM_ID',
        'width' => '10',
        'default' => true,
        'name' => 'team_name',
      ),
      'favorites_only' => 
      array (
        'name' => 'favorites_only',
        'label' => 'LBL_FAVORITES_FILTER',
        'type' => 'bool',
        'width' => '10',
        'default' => true,
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
