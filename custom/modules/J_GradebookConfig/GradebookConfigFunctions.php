<?php

    function getKOCOfCenter($center_id, $koc_id = ''){
        if (empty($center_id)) return '<option json="[{&quot;levels&quot;:&quot;-none-&quot;}]" value = "" >-none-</option>';
        global  $current_user;
        if(!($current_user->isAdmin())){
            $exteam = " AND ((j_kindofcourse.team_set_id IN (SELECT
            tst.team_set_id
            FROM
            team_sets_teams tst
            INNER JOIN
            team_memberships team_memberships ON tst.team_id = team_memberships.team_id
            AND team_memberships.team_id = '{$center_id}'
            AND team_memberships.deleted = 0)))";
        } else $exteam ='';
        $q1 = "SELECT DISTINCT
        IFNULL(j_kindofcourse.id, '') primaryid,
        IFNULL(j_kindofcourse.name, '') name,
        IFNULL(j_kindofcourse.content, '') content,
        IFNULL(l1.id, '') team_id,
        IFNULL(l1.name, '') team_name,
        j_kindofcourse.date_entered j_kindofcourse_date_entered
        FROM
        j_kindofcourse
        INNER JOIN
        teams l1 ON j_kindofcourse.team_id = l1.id
        AND j_kindofcourse.status = 'Active'
        AND l1.deleted = 0
        WHERE j_kindofcourse.deleted = 0 $exteam
        ORDER BY CASE
        WHEN
        (j_kindofcourse.kind_of_course = ''
        OR j_kindofcourse.kind_of_course IS NULL) THEN 0";
        $kocc = $GLOBALS['app_list_strings']['kind_of_course_list'];
        $count_koc = 1;
        foreach($kocc as $koc => $value) $q1 .= " WHEN j_kindofcourse.kind_of_course = '$koc' THEN ".$count_koc++;
        $q1 .= " ELSE $count_koc END ASC";
        $rs1 = $GLOBALS['db']->query($q1);
        //Generate html option
        $htm_koc .= '<option value = "" >-none-</option>';
        while ($row = $GLOBALS['db']->fetchByAssoc($rs1)) {
            if ($koc_id == $row['primaryid']) $str_selected = 'selected';
            else $str_selected = '';
            $htm_koc .= '<option ' . $str_selected . ' json="' . $row['content'] . '" value="' . $row['primaryid'] . '">' . $row['name'] . '</option>';
        }
        return $htm_koc;
    }

    function saveConfig($_data){
        $thisConfig = new J_GradebookConfig();
        $thisConfig->retrieve($_data['gb_setting_id']);
        $config_name = 'gradebook_config_'.$thisConfig->type;

        if($thisConfig->type != 'Overall'){
            $type = preg_replace('/[0-9]+/', '', $thisConfig->type);
            $config_name = 'gradebook_config_'.$type;
        }

        $config_def = $thisConfig->$config_name;
        $content = array();

        foreach ($_data['alias'] as $index => $alias) {
            if($alias != 'TT'){
                $content[$alias] = array(
                    'name'              => (!empty($config_def[$alias]['name'])) ? $config_def[$alias]['name'] : ('col_'.$alias),
                    'alias'             => $alias,
                    'label'             => $_data['label'][$index],
                    'group'             => $_data['group'][$index],
                    'type'              => $_data['type'][$index],

                    'visible'           => ($_data['visibled'][$index]) ? 1 : 0,
                    'max_mark'          => unformat_number($_data['max_mark'][$index]),
                    'options'           => $_data[$_data['type'][$index]. '_list'][$index],
                    'readonly'          => (!empty($_data['formula'][$index])) ? 1 : 0,
                    'formula'           => strtoupper($_data['formula'][$index]),
                    'custom_btn_label'  => $config_def[$alias]['custom_btn_label'],
                    'custom_btn_function'=> $config_def[$alias]['custom_btn_function'],
                );
                //Replace NULL from Array
                $content[$alias] = array_map(function($v){
                    return (is_null($v)) ? "" : $v;
                    },$content[$alias]);
            }
        }

        $thisConfig->content = json_encode($content);
        return $thisConfig->save();
    }

    function getConfigContent($_data){
        $thisConfig = new J_GradebookConfig();
        $thisConfig->retrieve($_data['record']);
        if(empty($thisConfig->id))
            return json_encode(array(
                'success'        => '0',
            ));
        else
            return json_encode(array(
                'success'        => '1',
                'html'          => $thisConfig->loadConfigContent(),
            ));

    }

    function getTypeOfKOC($_data){
        $list_level = array();
        if(empty($_data['koc_id']))
            $option = array('' => '-none-');
        else{
            $option = $GLOBALS['app_list_strings']['gradebook_type_options'];
            $sql_get_level = "SELECT content FROM j_kindofcourse WHERE id = '{$_data['koc_id']}'";
            $content = json_decode(html_entity_decode($GLOBALS['db']->getOne($sql_get_level)),true);
            if(!empty($content)){
                foreach($content as $key => $value){
                    $list_level[$value['levels']] = $value['levels'];
                }
            }
        }
        return json_encode(array(
            'html' => get_select_options_with_id($option, ''),
            'level' => get_select_options_with_id($list_level, ''),
        ));
    }
    function deleteConfig($config_id = ''){
        if($config_id){
            $GLOBALS['db']->query("UPDATE j_gradebookconfig SET deleted=1, date_modified='{$GLOBALS['timedate']->nowDb()}', modified_user_id='{$GLOBALS['current_user']->id}' WHERE id = '{$config_id}' AND deleted = 0");
            return true;
        }
        return false;
    }
    function checkDuplicateGradebookSetting($gradebook_config_id, $gbsettinggroup_id, $type){
        $sql = "SELECT count(id)
    FROM j_gradebookconfig
    WHERE deleted = 0
    AND gbsettinggroup_id = '$gbsettinggroup_id'
    AND id != '$gradebook_config_id'
    AND type = '$type'";
        $count = $GLOBALS['db']->getOne($sql);
        if($count > 0)
            return true;
        return false;
    }
?>
