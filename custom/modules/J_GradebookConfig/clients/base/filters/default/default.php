<?php
// created: 2020-12-28 14:06:32
$viewdefs['J_GradebookConfig']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'name' => 
    array (
    ),
    'gradebook_setting_name' => 
    array (
    ),
    'gbsettinggroup_name' => 
    array (
    ),
    'description' => 
    array (
    ),
    'koc_name' => 
    array (
    ),
    'type' => 
    array (
    ),
    'level' => 
    array (
    ),
    'date_modified' => 
    array (
    ),
    'date_entered' => 
    array (
    ),
    'created_by_name' => 
    array (
    ),
    'modified_by_name' => 
    array (
    ),
    'assigned_user_name' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);