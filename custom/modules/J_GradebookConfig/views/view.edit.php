<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');
    class J_GradebookConfigViewEdit extends ViewEdit {
    var $useModuleQuickCreateTemplate = true;
    var $useForSubpanel = true;
        public function display(){
            if(empty($this->bean->id) || $_REQUEST['isDuplicate'] == "true")
                $this->bean->gradebook_setting_name = 'Auto-Generate';

            $this->ss->assign("code", $this->bean->gradebook_setting_name);
            parent::display();


        }
    }

?>