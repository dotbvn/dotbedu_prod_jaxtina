function checkDuplicateGradebookSetting(from_id){
    ajaxStatus.showStatus('Proccessing...');
    var gradebook_config_id        = $('#'+from_id).find('input[name=record]').val();
    var gbsettinggroup_id          = $('#'+from_id).find('input[name=gbsettinggroup_id]').val();
    var gradebook_config_type      = $('#'+from_id).find('select[name=type]').val();
    var check = false;
    var module_name = $('input[name=module]').val();
    if(typeof (gbsettinggroup_id) == 'undefined' && module_name == 'J_GradebookSettingGroup')
        gbsettinggroup_id = $('input[name=record]').val();
    jQuery.ajax({
        url: "index.php?module=J_GradebookConfig&dotb_body_only=true&action=ajaxGradebookConfig",
        type: "POST",
        async: false,
        data:{
            process_type            : "checkDuplicateGradebookSetting",
            gradebook_config_id     : gradebook_config_id,
            gbsettinggroup_id       : gbsettinggroup_id,
            gradebook_config_type   : gradebook_config_type
        },
        success: function(data){
            ajaxStatus.hideStatus();
            if(data) {
                toastr.error('Type của Gradebook Settings này đã có trong Gradebook Setting Group!!');
                check = false;
            }else
                check = true;
        },
        error: function(){
            ajaxStatus.hideStatus();
            toastr.error("There are errors. Please try again!");
            check = false;
        },
    });
    return check;
}
function check_form(formname) {
    if (typeof (siw) != 'undefined' && siw && typeof (siw.selectingSomething) != 'undefined' && siw.selectingSomething)
    {
        return false;
    }

    if(document.getElementById(formname).module.value == 'J_GradebookConfig') {
        if(validate_form(formname, '') && checkDuplicateGradebookSetting(formname)) {
            setTimeout(function() {
                showSubPanel('gbsettinggroup_gbconfig', null, true);
            },2000);
            return true;
        } else {
            return false;
        }
    } else {
        if(validate_form(formname, '')) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}
$(document).ready(function(){
    $('#gbsettinggroup_id,#type').live('change',function() {
        var record_id = $('input[name=record]').val();
        if(record_id == ''){
            checkDuplicateGradebookSetting($('#gbsettinggroup_id').closest('form').attr('id'));
        }
    });
});
//end
