var formname = "GradeConfig";
$(document).ready(function () {
    $(".max_mark").live('keydown', function (e) {
        this.value = numberFormat(this.value);
    });
    $(".input_mark").live("change", function () {
        $(this).val(Numeric.toFloat($(this).val(), 2, 2));
    });
    $(".type").live("change", function () {
        var alias = $(this).attr('alias');
        var _thistype = $(this).val();
        if (_thistype == 'score' || _thistype == 'percent') {
            var def_value = (_thistype == 'percent') ? '100' : '10';
            $('.max_mark[alias="' + alias + '"]').val(def_value).show().effect("highlight", {color: '#ff9933'}, 500);
            $('.formula[alias="' + alias + '"]').val('').hide();
            $('.band[alias="' + alias + '"]').val('').hide();
            $('.comment[alias="' + alias + '"]').val('').hide();
        }
        if (_thistype == 'formula' || _thistype == 'total') {
            $('.max_mark[alias="' + alias + '"]').val('').show().effect("highlight", {color: '#ff9933'}, 500);
            $('.formula[alias="' + alias + '"]').val('').show();
            $('.band[alias="' + alias + '"]').val('').hide();
            $('.comment[alias="' + alias + '"]').val('').hide();
        }
        if (_thistype == 'band') {
            $('.max_mark[alias="' + alias + '"]').val('').hide();
            $('.formula[alias="' + alias + '"]').val('').show();
            $('.band[alias="' + alias + '"]').val('').show();
            $('.comment[alias="' + alias + '"]').val('').hide();
        }

        if (_thistype == 'comment') {
            $('.max_mark[alias="' + alias + '"]').val('').hide();
            $('.formula[alias="' + alias + '"]').val('').hide();
            $('.band[alias="' + alias + '"]').val('').hide();
            $('.comment[alias="' + alias + '"]').val('').show();
        }
    });

    $('.show_tip').live('click', function () {
        $('.formula_tip').css('display','');
    });
    $('.hide_tip').live('click', function () {
        $('.formula_tip').css('display','none');
    });

    $(".max_mark, .formula, .visible, .type").live("change", function(){
        autoUpdateGradebookConfig();
    });

    $('.visible').live("click", function () {
        var show = $(this).is(':checked');
        var alias = $(this).attr('alias');
        var _arrR = ['max_mark', 'title', 'group', 'formula']; //Change visibled
        var _arrD = ['comment', 'band', 'type'];
        if (show) {
            $.each(_arrR, function (index, val) {
                $("." + val + "[alias=" + alias + "]").prop('readonly', false).removeClass("readonly");
            });
            $.each(_arrD, function (index, val) {
                $("." + val + "[alias=" + alias + "]").removeClass("readonly");
            });
            //Change visibled
            $(".visibled[alias=" + alias + "]").val('1');
        } else {
            $.each(_arrR, function (index, val) {
                $("." + val + "[alias=" + alias + "]").prop('readonly', true).addClass("readonly");
            });
            $.each(_arrD, function (index, val) {
                $("." + val + "[alias=" + alias + "]").addClass("readonly");
            });
            //Change visibled
            $(".visibled[alias=" + alias + "]").val('0');
        }
    });

    $("#save").live('click', function () {
        if ($('#config_content').length) {
            var form = $("#" + formname);
            saveGradebookConfig(form, 1);
        }
    });

    $('.add_col').live('click', function () {
        var rows = ['r_alias', 'r_group', 'r_title', 'r_type', 'r_visible', 'r_max_mark', 'r_formula', 'r_add'];
        $.each(rows, function (i, val) {
            var template = $('table#config_content').find('tr.' + val).find('td.template').clone();
            template.removeClass('template');
            $(template).insertBefore($('table#config_content').find('tr.' + val).find('td:last-child'));
            var cr_ind = $('table#config_content').find('tr.' + val).find('td:last-child').index() - 1;
            //remove button remove col
            if (val == 'r_add') for (var i = 2; i < cr_ind; i++) $('table#config_content').find('tr.' + val).find('td:eq(' + i + ')').find('.remove_col').hide();

            var chr = String.fromCharCode(97 + cr_ind - 2).toUpperCase();
            $('table#config_content').find('tr.' + val).find('td').eq(cr_ind).find('input,textarea,select,img,b').attr('alias', chr);
            $('table#config_content').find('tr.' + val).find('td').eq(cr_ind).find("input.alias").val(chr);
            $('table#config_content').find('tr.' + val).find('td').eq(cr_ind).find('b').text('(' + chr + ')');
        });
    });

    $('.remove_col').live('click', function () {
        var rows = ['r_alias', 'r_group', 'r_title', 'r_type', 'r_visible', 'r_max_mark', 'r_weight', 'r_formula', 'r_add'];
        var collumn = $(this).closest('td').index();
        $.each(rows, function (i, val) {
            $('table#config_content').find('tr.' + val).find('td').eq(collumn).remove();
            //remove button remove col
            if (val == 'r_add') $('table#config_content').find('tr.' + val).find('td:eq(' + (collumn - 1) + ')').find('.remove_col').show();
        });
    });

    loadGradebookConfig();
});

function loadGradebookConfig(){
    var record = $('input[name=record]').val();
    ajaxStatus.showStatus('Proccessing...');
    $.ajax({
        url: "index.php?module=J_GradebookConfig&dotb_body_only=true&action=ajaxGradebookConfig",
        type: "POST",
        async: true,
        data: {
            process_type: "getConfigContent",
            record      : record,
        },
        success: function (data) {
            data = JSON.parse(data);
            ajaxStatus.hideStatus();
            $('input[name=gradebook_name]').val(data.name);
            $('textarea[name=description]').val(data.description);
            toastr.success("Completed!");
            $("#gb_config").html(data.html);
            $("input[name=save]").show();
            $("input[name=delete]").show();
            $("#config_loading").remove();
            //Remove btn remove_col
            var cr_ind = $('table#config_content').find('tr.r_add').find('td:last-child').index() - 1;
            for (var i = 2; i < cr_ind; i++) $('table#config_content').find('tr.r_add').find('td:eq(' + i + ')').find('.remove_col').hide();
            // Tự động cập nhật thay đổi của bảng điểm có liên quan
            autoUpdateGradebookConfig(1);
        },
        error: function () {
            ajaxStatus.hideStatus();
            toastr.error("There are errors. Please try again!");
            $("#gb_config").html("");
        },
    });
}

function saveGradebookConfig(form, alert = 0) {
    DOTB.ajaxUI.showLoadingPanel();
    $("#save").html('<img src="custom/include/images/loading.gif" align="absmiddle">&nbsp;Saving data...');
    $.ajax({  //Make the Ajax Request
        url: "index.php?module=J_GradebookConfig&dotb_body_only=true&action=ajaxGradebookConfig",
        type: "POST",
        async: true,
        data: form.serialize(),
        error: function () {
            DOTB.ajaxUI.hideLoadingPanel();
            alert("AJAX - error()");
            $('.timecard-popup').remove();
        },
        success: function (data) {
            DOTB.ajaxUI.hideLoadingPanel();
            if (data) {
                if(alert)
                    toastr.success("Saved successfully! ");
                window.onbeforeunload = null;
            } else {
                $("#result").html(data.msg);
                toastr.error("Error Save! Please try again!");
            }
        }
    });
}

function autoUpdateGradebookConfig(auto_save = 0) {
    var visible_list = [];
    var column_number_list = [];
    //Visible  Collumn
    $(".visible:checked").each(function () {
        visible_list.push($(this).attr('alias'));
        var alias = $(this).attr('alias');
        var _thisconfig_type = $('.type[alias="' + alias + '"]').val();
        if(_thisconfig_type != 'band' && _thisconfig_type != 'comment')
            column_number_list.push($(this).attr('alias'));
    });
    $(".visible:checked").each(function () {
        var alias = $(this).attr('alias');
        var gradebookconfig_progress = $('#type').val();
        if ($('.type[alias="' + alias + '"]').val() == 'formula' || $('.type[alias="' + alias + '"]').val() == 'total') {
            var _thisformula = $('.formula[alias="' + alias + '"]');
            var _thismaxmark = $('.max_mark[alias="' + alias + '"]');
            var _thisconfig_type = $('.type[alias="' + alias + '"]').val();
            var formula = _thisformula.val().toUpperCase();
            formula = formula.replace("=", '');
            var formula_list = [];
            var countErr = 0;
            var eval_formula = formula;
            var json_progress= JSON.parse($('div#data_progress').text());
            var map_obj_progress = new Map(Object.entries(json_progress));
            var max_mark = 0;
            //Handle formula
            for (var i = 0; i < formula.length; i++) {
                var next_c = formula[i+1];
                var col_in_progress = '';
                var formula_slice = formula.slice(i);
                var position_count = formula_slice.indexOf('COUNT');
                var position_sum = formula_slice.indexOf('SUM');
                var position_average = formula_slice.indexOf('AVERAGE');
                if(position_count === 0 || position_sum === 0 || position_average === 0){
                    var end_count = formula_slice.indexOf(')');
                    var colon = formula_slice.indexOf(':');
                    var bracket = formula_slice.indexOf('[');
                    var count_col = 0;
                    var sum_col = 0.00;
                    var replace = '';
                    if(formula_slice[end_count-1] >= '0' && formula_slice[end_count-1] <= '9'){
                        var begin = '';
                        if(formula_slice[colon-2]==='P') begin += formula_slice[colon-1];
                        else begin += formula_slice[colon-2] + formula_slice[colon-1];
                        begin = parseFloat(begin);
                        var end = '';
                        if(formula_slice[end_count-2]==='P') end += formula_slice[end_count-1];
                        else end += formula_slice[end_count-2] + formula_slice[end_count-1];
                        end = parseFloat(end);
                        replace = 'P'+begin+':P'+end;
                        var j = 0;
                        for( j = begin; j <= end; j++){
                            var progress = map_obj_progress.get('P'+j); //get value by key
                            if(typeof progress != 'undefined'){
                                count_col++;
                                var map_obj_mark = new Map(Object.entries(progress));
                                var result_progress = map_obj_mark.get('final');
                                sum_col += parseFloat(result_progress);
                            }
                        }
                    }
                    else {
                        var begin_word = formula_slice[colon-1].charCodeAt() ;
                        var end_word = formula_slice[colon+1].charCodeAt() ;
                        var j = 0;
                        replace += formula_slice[colon-1] + ':' + formula_slice[colon+1];
                        for( j = begin_word; j <= end_word; j++){
                            var word = String.fromCharCode(j);
                            if ($.inArray(word, column_number_list) !== -1){
                                count_col++;
                                var max_mark_col = Numeric.parse($('input.max_mark[alias="' + word + '"]').val());
                                sum_col += parseFloat(max_mark_col);
                            }
                        }
                    }
                    var name_function = '';
                    var result_function = 0.00;
                    if(position_count === 0) {
                        name_function = 'COUNT(';
                        result_function = count_col;
                    } else if(position_sum === 0) {
                        name_function = 'SUM(';
                        result_function = sum_col;
                    } else {
                        name_function = 'AVERAGE(';
                        if(count_col !== 0)
                            result_function = Numeric.toFloat(sum_col/count_col, 2, 2);
                    }
                    eval_formula = eval_formula.replace(name_function + replace + ')', result_function);
                    i += end_count;
                }
                if(formula[i]=='P' && next_c >= '1' && next_c <= '9' ){
                    if(next_c == '1' && formula[i+2] >= '0' && formula[i+2] < '3'){
                        next_c += formula[i+2];
                        if(formula[i+3]==='[')
                            col_in_progress = formula[i+4];
                    }
                    var key_progress = 'P'+ next_c;
                    var progress = map_obj_progress.get(key_progress); //get value by key
                    var map_obj_maxmark = new Map(Object.entries(progress));
                    if(formula[i+2]==='[')
                        col_in_progress = formula[i+3];
                    if(col_in_progress === ''){
                        max_mark = parseFloat(map_obj_maxmark.get('final')); //get value by key
                    }
                    else {
                        max_mark = parseFloat(map_obj_maxmark.get(col_in_progress)); //get value by key
                        next_c += '['+col_in_progress+']';
                    }

                    if(max_mark==0){
                        toastr.error("Column (" + formula[i]+next_c + ") is not visible.");
                    }
                    eval_formula = eval_formula.replace(formula[i]+next_c, max_mark);
                } else
                if (formula[i].charCodeAt() >= 65 && formula[i].charCodeAt() <= 90 && formula[i-1] !=='[') {
                    //form A->Z
                    if ($.inArray(formula[i], visible_list) != -1) {
                        max_mark = Numeric.parse($('input.max_mark[alias="' + formula[i] + '"]').val());
                        eval_formula = eval_formula.replace(formula[i], max_mark);
                    } else {
                        toastr.error("Column (" + formula[i] + ") is not visible.");
                        countErr++;
                    }
                }
            }
            if (countErr > 0) {
                _thisformula.effect("highlight", {color: '#FF0000'}, 2000);
            } else {
                _thismaxmark.val(Numeric.toFloat(math.eval(eval_formula), 2, 2));
                if(auto_save){
                    // check giá trị để tự động lưu
                    var type = $('#type').val();
                    var is_save = false;
                    var progress = map_obj_progress.get(type); //get value by key
                    var map_obj_maxmark = new Map(Object.entries(progress));
                    var before_max_mark = parseFloat(map_obj_maxmark.get('final')); //get value by key
                    var after_max_mark  = parseFloat(_thismaxmark.val());
                    if(after_max_mark !== before_max_mark)
                        is_save = true;
                    if(is_save){
                        var form = $("#" + formname);
                        saveGradebookConfig(form);
                    }
                }
            }
        }
    });
}
