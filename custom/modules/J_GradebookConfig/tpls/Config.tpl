{literal}
    <style type="text/css">
        .table-border td, .table-border th {
            border: 1px solid #ddd;
            border-collapse: collapse;
            line-height: 18px;
            padding: 6px;
            vertical-align: middle;
            text-align: center;
        }
        .table-custom td{
            padding: 6px;
            vertical-align: middle;
            text-align: center;
        }
        .table-border {
            border-collapse:collapse;
        }
        .no-bottom-border {
            border-bottom: 0px solid #ddd !important;
        }
        .no-top-border {
            border-top: 0px solid #ddd !important;
        }
        textarea.label{
            text-align-last: center;
            text-align: justify;
            font-weight: bold;
            width: 90%;
            resize: none;
        }
        .tip_{
            cursor: pointer;
            color: blue;
            text-decoration: underline;
        }
    </style>
{/literal}

<html>
<head>
    <title></title>
    {dotb_getscript file="custom/include/javascript/Numeric.js"}
    {dotb_getscript file="custom/modules/J_GradebookConfig/js/configs.js"}
    {dotb_getscript file="custom/include/javascript/math.min.js"}
    <link rel="stylesheet" type="text/css" href="{dotb_getjspath file='custom/modules/J_GradebookConfig/css/config.css'}"/>
</head>
<body>
<br/>
<form action="index.php" name="GradeConfig" id ="GradeConfig">
        <input name="process_type" value="saveConfig" type="hidden">
        <input name="gb_setting_id" value="{$fields.id.value}" type="hidden">
        <div style="height: 50px;text-align: center;"><input type="button" style="" class="button primary" id="save" name="save" value="SAVE SETTING"></div>
        <div id='gb_config'>
            {$CONFIG_CONTENT}
        </div>
    </div>
</form>
</body>
</html>
