var timer = null;
var vertical = false;
var socketURL = "https://socket.dotb.cloud/";

$(document).ready(function () {
    if(document.body.clientWidth < document.body.clientHeight){
        vertical = true;
        $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-waiting-vertical');
        $('#content_waiting').css('left', '9%');
        $('.container').css('padding-top', '20%');
        $htmlStudentAvatar = '' +
            '<div class="col-xs-4"></div>\n' +
            '<div class="col-xs-4">\n' +
            '<img id="student_avatar" class="img-responsive" src="">\n' +
            '</div>\n' +
            '<div class="col-xs-4"></div>';
        $('#div_student_avatar').removeClass().addClass('col-xs-12').css('padding-bottom', '4.905vw').html($htmlStudentAvatar);
        $('#attendance_content').removeClass().addClass('col-xs-12').find('.row-value').removeClass('col-xs-6').addClass('col-xs-7');
        $('#attendance_content').find('.row-label').removeClass('col-xs-3').addClass('col-xs-5');
        $('#guardian').removeClass('col-xs-4').addClass('col-xs-7').css('left', '21%');

        //replace class vertical
        $('.w-title-5').removeClass('w-title-5').addClass('w-title-5-vertical');
        $('.w-content-1').removeClass('w-content-1').addClass('w-content-1-vertical');
        $('.clock').removeClass('clock').addClass('clock-vertical');
        $('.clock-container').removeClass('clock-container').addClass('clock-container-vertical');
        $('.clock-col').removeClass('clock-col').addClass('clock-col-vertical');
        $('.clock-timer').removeClass('clock-timer').addClass('clock-timer-vertical');
        $('.clock-label').removeClass('clock-label').addClass('clock-label-vertical');
        $('.w-title-6').removeClass('w-title-6').addClass('w-title-6-vertical');
        $('.w-content-2').removeClass('w-content-2').addClass('w-content-2-vertical');
        $('.group-field').removeClass('group-field').addClass('group-field-vertical');
        $('.guardian').removeClass('guardian').addClass('guardian-vertical');
        $('.guardian1').addClass('sub-guardian-vertical');
        $('.guardian2').addClass('sub-guardian-vertical');
        $('.guardian3').addClass('sub-guardian-vertical');
        $('.w-salutation').removeClass('w-salutation').addClass('w-salutation-vertical');
        $('.row-content').removeClass('row-content').addClass('row-content-vertical');
    }
        var socket = io.connect(socketURL, {"path": "", "transports": ["websocket"], "reconnection": true});
        socket.on('connect', function () {
            console.log('Socket server is live!');
            socket.emit('join','LoadAttendance')

        });
        socket.on('error', function () {
            console.log('Cannot connect to socket server!')
        })
        socket.on('event-phenikaa', function (msg) {
            console.log(msg)
            $('#SubDiv, #mainDiv').hide();
            if (msg.status == 1) {
                $('#content').show();
                $('#guardian').show().html('');
                $('#content_waiting').hide();
                $('#content_error').hide();
                $('#bg_attendance').removeClass().addClass('blur-img').addClass(msg.background);
                $('#welcom_title').html(msg.header);

                if (msg.background == 'bg-junior_check_in') {
                    $('.room_name').show();
                    $('#session-group').show();
                    $('#inout-label-en').text('Time In');
                    $('#inout-label-vn').text('Giờ vào');
                } else if (msg.background == 'bg-junior_check_out') {
                    $('.room_name').show();
                    $('#session-group').show();
                    $('#inout-label-en').text('Time Out');
                    $('#inout-label-vn').text('Giờ ra');
                } else if (msg.background == 'bg-junior_check_in_wrong') {
                    $('.room_name').hide();
                    $('#session-group').hide();
                    $('#inout-label-en').text('Time In');
                    $('#inout-label-vn').text('Giờ vào');
                }

                $('#student_avatar').attr('src', msg.student_avatar);
                $('#student_name').text(msg.student_name);
                $('#birthdate').text(msg.birthdate);
                $('#student_code').text(msg.student_code);
                $('#room_name').text(msg.room_name);
                $('#session_time').text(msg.startEnd);
                $('#inouttime').text(msg.time_check);

                if(vertical){
                    $('#welcom_title').find('h1').css('font-size', '4.34vw');
                    $('.w-content').removeClass('w-content').addClass('w-content-vertical');
                }
            } else {
                $('#content').hide();
                $('#guardian').hide();
                $('#content_waiting').hide();
                $('#content_error').show();
                $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-error');
            }
            $('#card_number').val('');

            if (timer) {
                clearTimeout(timer); //cancel the previous timer.
                timer = null;
            }
            timer = setTimeout(function () {
                //back to waiting
                $('#content').hide();
                $('#guardian').hide();
                $('#content_waiting').show();
                $('#content_error').hide();
                if (vertical) $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-waiting-vertical');
                else $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-waiting');
            }, 15000);
        })



        
    //set time clock
    startTime();

    $('#btn_back').click(function () {
        parent.history.back();
        return false;
    });


    $('div#toggle_div_2').hover(function () {
        $(this).stop().animate({marginLeft: '-1px'}, 200);
        $('td#artd_2').css('width', '70%');

    }, function () {
        setTimeout(function () {
            $('div#toggle_div_2').stop().animate({'margin-left': '-402px'}, 200);
            $('td#artd_2').css('width', '100%');
        }, 3000);

    });
});

function startTime() {
    var today = new Date();
    var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    var d = days[today.getDay()];
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('.clock-day').text(d);
    $('.clock-hours').text(h);
    $('.clock-minutes').text(m);
    $('.clock-seconds').text(s);
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function autoChangeScreen(){
        if((vertical && document.body.clientWidth > document.body.clientHeight)
            || (!vertical && document.body.clientWidth < document.body.clientHeight)){
        location.reload();
    }
}