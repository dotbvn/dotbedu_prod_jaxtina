<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <link rel="shortcut icon" href="{dotb_getjspath file='themes/default/images/logo.png'}">
    <meta charset="UTF-8">
    <link type="text/css" rel="stylesheet" href="custom/modules/C_Attendance/tpls/template2/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="custom/modules/C_Attendance/tpls/template2/css/style.css">

    <script type="text/javascript" src="include/javascript/jquery/jquery-min.js"></script>
    <script type="text/javascript" src="custom/include/javascript/Bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="custom/include/socket.io.min.js"></script>
    <script type="text/javascript" src="custom/modules/C_Attendance/tpls/template2/js/modernizr.custom.js"></script>
    <script type="text/javascript" src="custom/modules/C_Attendance/tpls/template2/js/monitor.js"></script>

    <title>Attendance</title>
</head>
<body>
<div class="main-page-area">
    <div id="pt-main" class="pt-perspective">
        <div class="pt-page pt-page-1 pt-page-current">
            <div class="container-fluid bg-main-image">

                <div id="bg_attendance" class="blur-img bg-waiting"></div>
                <div id="content" class="page-fixed" hidden>
                    <div class="container" style="padding-top:7%">
                        <div id="welcom_title" class="row">
                            <div class="col-12">
                                <h1 class="w-title-4">
                                    Welcome to DOTB!
                                </h1>
                                <h1 class="w-title-3">
                                    Please wait for a minutes, your child is coming!
                                </h1>
                                <p class="w-content">
                                    DOTB chào Quý Phụ Huynh!<br/>
                                    Vui lòng đợi trong giây lát, Bé sẽ ra ngay đây ạ!
                                </p>
                            </div>
                        </div>
                        <div class="row row-content">
                            <div class="col-xs-2" id="div_student_avatar">
                                <img id="student_avatar" class="img-responsive"
                                     src="themes/default/images/noimage.png">
                            </div>
                            <div class="col-xs-10" id="attendance_content">
                                <div class="row group-field">
                                    <div class="col-xs-3 row-label">
                                    <label>Name</label><br/>
                                    <span>Họ và tên</span>
                                    </div>
                                    <div class="col-xs-6 row-value">
                                        <span id="student_name" style="font-weight: bold;"></span>
                                    </div>
                                </div>
                                <div class="row group-field">
                                    <div class="col-xs-3 row-label">
                                        <label>DOB</label><br/>
                                        <span>Ngày sinh</span>
                                    </div>
                                    <div class="col-xs-6 row-value">
                                        <span id="birthdate"></span>
                                    </div>
                                </div>
                                <div class="row group-field">
                                    <div class="col-xs-3 row-label">
                                        <label>Student Code</label><br/>
                                        <span>Mã học viên</span>
                                    </div>
                                    <div class="col-xs-6 row-value">
                                        <span id="student_code"></span>
                                    </div>
                                </div>
                                <div class="row group-field room_name">
                                    <div class="col-xs-3 row-label">
                                        <label>Today Classroom</label><br/>
                                        <span>Phòng học</span>
                                    </div>
                                    <div class="col-xs-6 row-value">
                                        <span id="room_name"></span>
                                    </div>
                                </div>
                                <div id="session-group" class="row group-field">
                                    <div class="col-xs-3 row-label">
                                        <label>Study Time</label><br/>
                                        <span>Giờ học</span>
                                    </div>
                                    <div class="col-xs-6 row-value">
                                        <span id="session_time"></span>
                                    </div>
                                </div>
                                <div id="inoutime-group" class="row group-field">
                                    <div class="col-xs-3 row-label">
                                        <label id="inout-label-en"></label><br/>
                                        <span id="inout-label-vn" ></span>
                                    </div>
                                    <div class="col-xs-6 row-value">
                                        <span id="inouttime"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <input name="card_number" type="text" class="card_number" id="card_number" value="" size="10" readonly>

                <div id="clock" class="clock">
                    <div class="clock-container">
                        <div class="clock-col">
                            <p class="clock-day clock-timer">TH
                            </p>
                            <p class="clock-label">
                                Day
                            </p>
                        </div>
                        <div class="clock-col">
                            <p class="clock-hours clock-timer">01
                            </p>
                            <p class="clock-label">
                                Hours
                            </p>
                        </div>
                        <div class="clock-col">
                            <p class="clock-minutes clock-timer">01
                            </p>
                            <p class="clock-label">
                                Minutes
                            </p>
                        </div>
                        <div class="clock-col">
                            <p class="clock-seconds clock-timer">01
                            </p>
                            <p class="clock-label">
                                Seconds
                            </p>
                        </div>
                    </div>
                </div>

                <div id="guardian" class="col-xs-4 guardian" hidden>

                </div>

                <div id="hiddenGuardian" hidden>
                    <div class="col-xs-4 guardian1">
                        <img class="img-responsive"
                             src="themes/default/images/noimage.png"/><br/>
                        <span class="w-salutation"></span>
                    </div>
                    <div class="col-xs-4 guardian2">
                        <img class="img-responsive"
                             src="themes/default/images/noimage.png"/><br/>
                        <span  class="w-salutation"></span>
                    </div>
                    <div class="col-xs-4 guardian3">
                        <img class="img-responsive"
                             src="themes/default/images/noimage.png"/><br/>
                        <span  class="w-salutation"></span>
                    </div>
                </div>

                <div id="content_waiting">
                    <h1 class="w-title-5">
                        Please scan your face or QR Code <br>on your Student App
                    </h1>
                    <p class="w-content-1">
                        Vui lòng quét khuôn mặt hoặc mã QR trên Ứng dụng học viên
                    </p>
                </div>

                <div id="content_error" hidden>
                    <h1 class="w-title-6">
                        Oh No!<br/>
                        Some thing went wrong.<br/>
                        We apologize for this inconvenience.<br/>
                        Please contact DOTB staffs for supporting.
                    </h1>
                    <p class="w-content-2">
                        Ôi không! Đã có một chút lỗi xảy ra.<br/>
                        Rất xin lỗi vì sự bất tiện này.<br/>
                        Vui lòng liên hệ nhân viên DOTB để được hỗ trợ.
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="device-breakpoints">
    <div class="device-xs visible-xs" data-breakpoint="xs"></div>
    <div class="device-sm visible-sm" data-breakpoint="sm"></div>
    <div class="device-md visible-md" data-breakpoint="md"></div>
    <div class="device-lg visible-lg" data-breakpoint="lg"></div>
</div>
</body>
</html>
<div id="mainDiv" style="filter: alpha(opacity = 50);display:none;z-index: 1000; border: medium none; margin: 0pt; padding: 0pt; width: 100%; height: 100%; top: 0pt; left: 0pt; background-color: rgb(0, 0, 0); opacity: 0.2; cursor: wait; position: fixed;" ></div>
<div id="SubDiv" style="z-index: 1001; position: fixed; padding: 0px; margin: 0px; width: 30%; top: 40%; left: 35%; text-align: center; color: rgb(0, 0, 0); border: 3px solid #000000; background-color: rgb(255, 255, 255); cursor: wait; display: none;" >
    <img style="margin-top: 10px;" src="custom/include/images/loader32.gif">
    <br/>
    <h2>Please Wait...</h2>
</div>






