var timer = null;
$(document).ready(function(){
    $('#btn_back').click(function(){
        parent.history.back();
        return false;
    });

    $('#card_number').focusout(function(){
        $('#card_number').focus();
    });

    $('#card_number').change(ajax_post_code);

    $('#btn_search').click(ajax_post_code);
    $('#card_number').focus();

    //    $('div#toggle_div').on('mouseenter',function(){
    //        $(this).stop().animate({marginLeft:'0px'},200);
    //    });
    //    $('div#toggle_div').on('mouseleave',function(){
    //        $(this).stop().animate({'margin-left':'-90px'},200);
    //    });

    $('div#toggle_div_2').hover(function(){
        $(this).stop().animate({marginLeft:'-1px'},200);
        $('td#artd_2').css('width', '70%');

        },function(){
            setTimeout(function () {
                $('div#toggle_div_2').stop().animate({'margin-left':'-402px'},200);
                $('td#artd_2').css('width', '100%');
                }, 3000);

    });
});
function ajax_post_code(){
    var code = $('#card_number').val();
    if(code != ''){
        $('#SubDiv').show();
        $('#mainDiv').show();
        $('div#defaut_info').hide();
        $('div#check_info').show();
        $.ajax({
            url: "index.php?entryPoint=AjaxMonitor&dotb_body_only=true",
            type: "POST",
            async: true,
            data:
            {
                type: 'checkAttendance',
                code: code,
            },
            dataType: "json",
            success: function(res){
                $('#SubDiv').hide();
                $('#mainDiv').hide();
                $('div#check_info').html(res.html);
                $('#card_number').val('').focus();

                if (timer) {
                    clearTimeout(timer); //cancel the previous timer.
                    timer = null;
                }
                timer = setTimeout(function(){
                    $('div#defaut_info').show();
                    $('div#check_info').html('').hide();
                    }, 5000);
            },
            error:function (xhr, ajaxOptions, thrownError){
                $('#SubDiv').hide();
                $('#mainDiv').hide();
                var html = '<table width="100%" height = "100%" border="0" id = "tbl_result">';
                html += '<tr><td colspan="2" rowspan="1" id="image" nowrap><img style="width: 30%;" src="custom/themes/default/images/company_logo.png"/><br><br></td></tr>';
                html += '<tr><td align="center" nowrap> 😑 <br><br>There was a problem connecting to the server.<br><br>PLEASE TRY AGAIN LATER!</td></tr></table>';

                $('div#check_info').html(html);
                $('#card_number').val('').focus();

                if (timer) {
                    clearTimeout(timer); //cancel the previous timer.
                    timer = null;
                }
                timer = setTimeout(function(){
                    $('div#defaut_info').show();
                    $('div#check_info').html('').hide();
                    }, 5000);
            }
        });
    }

}