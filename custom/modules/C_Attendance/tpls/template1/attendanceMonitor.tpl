<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{$brand} - Attendance Tracker</title>
<link rel="shortcut icon" href="themes/default/images/logo.png">
<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
<script type="text/javascript" src="include/javascript/jquery/jquery-min.js"></script>
<script type="text/javascript" src="custom/modules/C_Attendance/tpls/template1/monitor.js"></script>
<link rel="stylesheet" type="text/css" href="custom/modules/C_Attendance/tpls/template1/view_custom.css">
<link rel='stylesheet' type='text/css' href='custom/include/javascript/Bootstrap/css/bootstrap.css'>

{literal}
<style>
    span
    {
    font-size: 25px;
    }
    #check_info{
    width: 100%;
    height: 100%;
    display:none;
    }
    #defaut_info{
    width: 100%;
    height: 100%;
    }
    #tbl_result td{
    vertical-align: top;
    height: 20px;
    color: white;
    font-size: 30px;
    }
    #tbl_result td div{
    width: 100%;
    height: 100%;
    overflow:hidden;
    }
    #tbl_result td#image{
    padding:20px;
    max-height: 20px;
    text-align:center;
    }

</style>

{/literal}
<table id="attendance_table" width="100%" height = "100%" <tr>
        <td id="artd_1" style="width:0%">
        <div id="toggle_div_2" style="border-right:1px dashed white;">
        <table width="100%" height = "100%">
                <tr>
                    <td align="left" nowrap>
                        <div id="toggle_div">
                        <button class="bbtn-all bbtn dsize  btn-wonka-land-1 " id="btn_back" style="margin-top:5px;margin-bottom:5px;margin-right:5px;margin-left:10px;">
                            <span class="bbtn-img-right bbtn-custom-txt"><img src="themes/default/images/icon_back.gif"></span>
                            <span class="bbtn-txt-center bbtn-custom-txt" style='font-size: 15px'>Back</span>
                        </button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" nowrap><img style="width: 30%;" src="{$filename}"></td>
                </tr>
                <tr>
                    <td align="center" nowrap><label for="card_number"></label>
                        <input name="card_number" type="password" id="card_number" value="" size="50" style="width:60%;font-size:18pt;text-align:center;color:#3c3c3b"/></td>
                    <td colspan="2" align="center" nowrap></td>
                </tr>


                <tr>
                    <td  align="center" nowrap>
                        <button class="bbtn-all bbtn dsize btn-limekiln-3 " id="btn_search" style="margin-top:10px;margin-bottom:10px;margin-right:0px;margin-left:0px;">
                            <span class="bbtn-img-right bbtn-custom-txt"><img src="custom/themes/default/images/search_icon.png"></span><span style="clear:both;display:none"></span>
                            <span class="bbtn-txt-center bbtn-custom-txt">Search</span>
                        </button>
                        &nbsp;&nbsp;
                    </td>
                </tr>
                <tr><td height = "30%"></td></tr>
            </table>
        </div>
        </td>

        <td id="artd_2" style="width:100%" colspan="2">
            <div id="check_info">
            </div>
            <div id="defaut_info">
                <table width="100%" height = "100%" border="0" id = "tbl_result" >
                    <tr><td align="center" nowrap style="vertical-align: bottom;">WELCOME TO</td></tr>
                    <tr>
                        <td align="center" nowrap>
                            <img src="{$filename}"/>
                            <br><br>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>

</table>
<div id="mainDiv" style="filter: alpha(opacity = 50);display:none;z-index: 1000; border: medium none; margin: 0pt; padding: 0pt; width: 100%; height: 100%; top: 0pt; left: 0pt; background-color: rgb(0, 0, 0); opacity: 0.2; cursor: wait; position: fixed;" ></div>
<div id="SubDiv" style="z-index: 1001; position: fixed; padding: 0px; margin: 0px; width: 30%; top: 40%; left: 35%; text-align: center; color: rgb(0, 0, 0); border: 3px solid #000000; background-color: rgb(255, 255, 255); cursor: wait; display: none;" >
    <img style="margin-top: 10px;" src="custom/include/images/loader32.gif">
    <br/>
    <h2>Please Wait...</h2>
</div>

