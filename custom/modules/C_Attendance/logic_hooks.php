<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will
// be automatically rebuilt in the future.
 $hook_version = 1;
$hook_array = Array();
// position, file, function
$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(1,'Color','custom/modules/C_Attendance/logicAttendance.php','logicAttendance','listViewColor',);
$hook_array['process_record'][] = Array(2,'handleProcessRecord','custom/modules/C_Attendance/logicAttendance.php','logicAttendance','handleProcessRecord',);
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(1,'Handle before save','custom/modules/C_Attendance/logicAttendance.php','logicAttendance','handleBeforeSave',);
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(1,'Handle CJ After Save','custom/modules/C_Attendance/logicCJC_Attendance.php','logicCJC_Attendance','handleCJAfterSave',);
$hook_array['after_save'][] = Array(2,'Handle after save','custom/modules/C_Attendance/logicAttendance.php','logicAttendance','handleAfterSave',);



?>