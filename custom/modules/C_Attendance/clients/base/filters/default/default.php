<?php
// created: 2022-12-18 16:43:04
$viewdefs['C_Attendance']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' => 
  array (
    'class_name' => 
    array (
    ),
    'student_name' => 
    array (
    ),
    'meeting_name' => 
    array (
    ),
    'check_in_time' => 
    array (
    ),
    'check_out_time' => 
    array (
    ),
    'team_name' => 
    array (
    ),
    'attended' => 
    array (
    ),
    'attendance_type' => 
    array (
    ),
    '$owner' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' => 
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
  ),
);