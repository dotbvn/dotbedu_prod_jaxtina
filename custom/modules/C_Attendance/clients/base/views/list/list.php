<?php
$module_name = 'C_Attendance';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_1',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'student_name',
                                'label' => 'LBL_STUDENT_NAME',
                                'enabled' => true,
                                'id' => 'STUDENT_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => true,
                                'type' => 'html',
                            ),
                            1 =>
                            array (
                                'name' => 'meeting_name',
                                'label' => 'LBL_MEETING_NAME',
                                'enabled' => true,
                                'id' => 'MEETING_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => true,
                            ),
                            2 =>
                            array (
                                'name' => 'class_name',
                                'label' => 'LBL_CLASS_NAME',
                                'enabled' => true,
                                'id' => 'CLASS_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => true,
                            ),
                            3 =>
                            array (
                                'name' => 'attendance_type_text',
                                'label' => 'LBL_ATTENDANCE_TYPE',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                                'align' => 'center',
                            ),
                            4 =>
                            array (
                                'name' => 'date_input',
                                'label' => 'LBL_DATE_INPUT',
                                'enabled' => true,
                                'default' => true,
                            ),
                            5 =>
                            array (
                                'name' => 'check_in_time',
                                'label' => 'LBL_CHECK_IN_TIME',
                                'enabled' => true,
                                'default' => true,
                            ),
                            6 =>
                            array (
                                'name' => 'check_out_time',
                                'label' => 'LBL_CHECK_OUT_TIME',
                                'enabled' => true,
                                'default' => true,
                            ),
                            7 =>
                            array (
                                'name' => 'date_modified',
                                'label' => 'LBL_DATE_MODIFIED',
                                'enabled' => true,
                                'readonly' => true,
                                'default' => true,
                            ),
                            8 =>
                            array (
                                'name' => 'student_type',
                                'label' => 'LBL_STUDENT_TYPE',
                                'enabled' => true,
                                'default' => false,
                            ),
                            9 =>
                            array (
                                'name' => 'attendance_type',
                                'label' => 'LBL_ATTENDANCE_TYPE',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
                'orderBy' =>
                array (
                    'field' => 'date_modified',
                    'direction' => 'desc',
                ),
            ),
        ),
    ),
);
