<?php
// created: 2020-06-29 14:54:01
$viewdefs['C_Attendance']['base']['view']['subpanel-for-j_class-class_attendances'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'name' => 'date_input',
          'label' => 'LBL_DATE_INPUT',
          'enabled' => true,
          'default' => true,
        ),
        1 => 
        array (
          'name' => 'student_type',
          'label' => 'LBL_STUDENT_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'student_name',
          'label' => 'LBL_STUDENT_NAME',
          'enabled' => true,
          'id' => 'STUDENT_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'attendance_type',
          'label' => 'LBL_ATTENDANCE_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'homework',
          'label' => 'LBL_DO_HOMEWORK',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'homework_comment',
          'label' => 'LBL_HOMEWORK_COMMENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'care_comment',
          'label' => 'LBL_CARE_COMMENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'homework_score',
          'label' => 'LBL_HOMEWORK_SCORE',
          'enabled' => true,
          'currency_format' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'star_rating',
          'label' => 'LBL_STAR_RATING',
          'enabled' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);