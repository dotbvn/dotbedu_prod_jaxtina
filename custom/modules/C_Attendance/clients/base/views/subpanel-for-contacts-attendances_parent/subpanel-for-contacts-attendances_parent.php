<?php
// created: 2020-06-29 14:53:25
$viewdefs['C_Attendance']['base']['view']['subpanel-for-contacts-attendances_parent'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                0 =>
                array (
                    'name' => 'date_input',
                    'label' => 'LBL_DATE_INPUT',
                    'enabled' => true,
                    'default' => true,
                ),
                1 =>
                array (
                    'name' => 'class_name',
                    'label' => 'LBL_CLASS_NAME',
                    'enabled' => true,
                    'id' => 'CLASS_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'large',
                ),
                2 =>
                array (
                    'name' => 'attendance_type_text',
                    'label' => 'LBL_ATTENDANCE_TYPE',
                    'enabled' => true,
                    'default' => true,
                    'type' => 'html',
                ),
                3 =>
                array (
                    'name' => 'homework',
                    'label' => 'LBL_DO_HOMEWORK',
                    'enabled' => true,
                    'default' => true,
                ),
                4 =>
                array (
                    'name' => 'homework_comment',
                    'label' => 'LBL_HOMEWORK_COMMENT',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                5 =>
                array (
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                6 =>
                array (
                    'name' => 'care_comment',
                    'label' => 'LBL_CARE_COMMENT',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                7 =>
                array (
                    'name' => 'homework_score',
                    'label' => 'LBL_HOMEWORK_SCORE',
                    'enabled' => true,
                    'default' => true,
                    'currency_format' => true,
                ),
                8 =>
                array (
                    'name' => 'star_rating',
                    'label' => 'LBL_STAR_RATING',
                    'enabled' => true,
                    'default' => true,
                ),
                9 =>
                array (
                    'name' => 'customer_comment',
                    'label' => 'LBL_CUSTOMER_COMMENT',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                10 =>
                array (
                    'name' => 'attendance_type',
                    'enabled' => true,
                    'default' => false,
                ),
                11 =>
                array (
                    'name' => 'student_type',
                    'enabled' => true,
                    'default' => false,
                ),
                12 =>
                array (
                    'name' => 'student_id',
                    'enabled' => true,
                    'default' => false,
                ),
                13 =>
                array (
                    'name' => 'student_name',
                    'enabled' => true,
                    'default' => false,
                ),
            ),
        ),
    ),
    'orderBy' =>
    array (
        'field' => 'date_modified',
        'direction' => 'desc',
    ),
    'type' => 'subpanel-list',
);