<?php

$viewdefs['C_Attendance']['base']['view']['preview'] = array(
    'panels' => array(
        array(
            'name' => 'panel_header',
            'fields' => array(
                array(
                    'name' => 'picture',
                    'type' => 'avatar',
                    'size' => 'large',
                    'dismiss_label' => true,
                    'readonly' => true,
                ),
                array(
                    'name' => 'date_input',
                    'label' => 'LBL_DATE_INPUT',
                    'enabled' => true,
                    'default' => true,
                ),
                array(
                    'name' => 'class_name',
                    'label' => 'LBL_CLASS_NAME',
                    'enabled' => true,
                    'default' => true,
                    'id' => 'CLASS_ID',
                    'link' => true,
                    'sortable' => false,
                ),
            ),
        ),
        array(
            'name' => 'panel_body',
            'fields' => array(
                array(
                    'name' => 'attendance_type',
                    'label' => 'LBL_ATTENDANCE_TYPE',
                    'enabled' => true,
                    'default' => true,
                ),
                array(
                    'name' => 'homework',
                    'label' => 'LBL_DO_HOMEWORK',
                    'enabled' => true,
                    'default' => true,
                ),

                array(
                    'name' => 'homework_comment',
                    'label' => 'LBL_HOMEWORK_COMMENT',
                    'enabled' => true,
                    'default' => true,
                ),

                array(
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'default' => true,
                ),              
                array(
                    'name' => 'care_comment',
                    'label' => 'LBL_CARE_COMMENT',
                    'enabled' => true,
                    'default' => true,
                ),                
                array(
                    'name' => 'homework_score',
                    'label' => 'LBL_HOMEWORK_SCORE',
                    'enabled' => true,
                    'default' => true,
                ),                
                array(
                    'name' => 'star_rating',
                    'label' => 'LBL_STAR_RATING',
                    'enabled' => true,
                    'default' => true,
                ),                
                'assigned_user_name',
                'team_name',
                array(
                    'name' => 'tag',
                    'span' => 12,
                ),
            ),
        ),
        array(
            'name' => 'panel_hidden',
            'hide' => true,
            'fields' => array(
                array(
                    'name' => 'date_entered_by',
                    'readonly' => true,
                    'inline' => true,
                    'type' => 'fieldset',
                    'label' => 'LBL_DATE_MODIFIED',
                    'fields' => array(
                        array(
                            'name' => 'date_modified',
                        ),
                        array(
                            'type' => 'label',
                            'default_value' => 'LBL_BY'
                        ),
                        array(
                            'name' => 'modified_by_name',
                        ),
                    ),
                ),
                array(
                    'name' => 'date_modified_by',
                    'readonly' => true,
                    'inline' => true,
                    'type' => 'fieldset',
                    'label' => 'LBL_DATE_ENTERED',
                    'fields' => array(
                        array(
                            'name' => 'date_entered',
                        ),
                        array(
                            'type' => 'label',
                            'default_value' => 'LBL_BY'
                        ),
                        array(
                            'name' => 'created_by_name',
                        ),
                    ),
                ),
            ),
        ),
    ),
);
