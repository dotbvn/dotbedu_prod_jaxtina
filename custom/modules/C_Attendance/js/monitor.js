var timer = null;
var vertical = false;
$(document).ready(function () {
    if(document.body.clientWidth < document.body.clientHeight){
        vertical = true;
        $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-waiting-vertical');
        $('#content_waiting').css('left', '9%');
        $('.container').css('padding-top', '20%');
        $htmlStudentAvatar = '' +
            '<div class="col-xs-4"></div>\n' +
            '<div class="col-xs-4">\n' +
            '<img id="student_avatar" class="img-responsive" src="">\n' +
            '</div>\n' +
            '<div class="col-xs-4"></div>';
        $('#div_student_avatar').removeClass().addClass('col-xs-12').css('padding-bottom', '4.905vw').html($htmlStudentAvatar);
        $('#attendance_content').removeClass().addClass('col-xs-12').find('.row-value').removeClass('col-xs-6').addClass('col-xs-7');
        $('#attendance_content').find('.row-label').removeClass('col-xs-3').addClass('col-xs-5');
        $('#guardian').removeClass('col-xs-4').addClass('col-xs-7').css('left', '21%');

        //replace class vertical
        $('.w-title-5').removeClass('w-title-5').addClass('w-title-5-vertical');
        $('.w-content-1').removeClass('w-content-1').addClass('w-content-1-vertical');
        $('.clock').removeClass('clock').addClass('clock-vertical');
        $('.clock-container').removeClass('clock-container').addClass('clock-container-vertical');
        $('.clock-col').removeClass('clock-col').addClass('clock-col-vertical');
        $('.clock-timer').removeClass('clock-timer').addClass('clock-timer-vertical');
        $('.clock-label').removeClass('clock-label').addClass('clock-label-vertical');
        $('.w-title-6').removeClass('w-title-6').addClass('w-title-6-vertical');
        $('.w-content-2').removeClass('w-content-2').addClass('w-content-2-vertical');
        $('.group-field').removeClass('group-field').addClass('group-field-vertical');
        $('.guardian').removeClass('guardian').addClass('guardian-vertical');
        $('.guardian1').addClass('sub-guardian-vertical');
        $('.guardian2').addClass('sub-guardian-vertical');
        $('.guardian3').addClass('sub-guardian-vertical');
        $('.w-salutation').removeClass('w-salutation').addClass('w-salutation-vertical');
        $('.row-content').removeClass('row-content').addClass('row-content-vertical');
    }

    //set time clock
    startTime();

    $('#btn_back').click(function () {
        parent.history.back();
        return false;
    });

    // $('#card_number').focusout(function () {
    //     $('#card_number').focus();
    // });
    $('#card_number').focusin(function () {
        $('#card_number').blur();
    });

    // $('#card_number').change(ajax_post_code);

    $('#btn_search').click(ajax_post_code);
    // $('#card_number').focus();
    $('#card_number').blur();

    //    $('div#toggle_div').on('mouseenter',function(){
    //        $(this).stop().animate({marginLeft:'0px'},200);
    //    });
    //    $('div#toggle_div').on('mouseleave',function(){
    //        $(this).stop().animate({'margin-left':'-90px'},200);
    //    });

    $('div#toggle_div_2').hover(function () {
        $(this).stop().animate({marginLeft: '-1px'}, 200);
        $('td#artd_2').css('width', '70%');

    }, function () {
        setTimeout(function () {
            $('div#toggle_div_2').stop().animate({'margin-left': '-402px'}, 200);
            $('td#artd_2').css('width', '100%');
        }, 3000);

    });
});

function ajax_post_code() {
    var code = $('#card_number').val();
    if (code != '') {
        $('#SubDiv, #mainDiv').show();
        $.ajax({
            url: "index.php?entryPoint=AjaxMonitor&dotb_body_only=true",
            type: "POST",
            async: true,
            data:
                {
                    type: 'checkAttendance2',
                    code: code,
                },
            dataType: "json",
            success: function (res) {
                $('#SubDiv, #mainDiv').hide();
                if (res.success == '1') {
                    $('#content').show();
                    $('#guardian').show().html('');
                    $('#content_waiting').hide();
                    $('#content_error').hide();
                    $('#bg_attendance').removeClass().addClass('blur-img').addClass(res.data.background);
                    $('#welcom_title').html(res.data.header);
                    if (res.data.background == 'bg-kid_check_in') {
                        $('.room_name').show();
                    } else if (res.data.background == 'bg-kid_check_out') {
                        $('.room_name').hide();
                    } else if (res.data.background == 'bg-kid_check_in_wrong') {
                        $('.room_name').show();
                    } else if (res.data.background == 'bg-junior_check_in') {
                        $('.room_name').show();
                    } else if (res.data.background == 'bg-junior_check_out') {
                        $('.room_name').show();
                    } else if (res.data.background == 'bg-junior_check_in_wrong') {
                        $('.room_name').show();
                    }

                    $('#student_avatar').attr('src', res.data.student_avatar);
                    $('#student_name').text(res.data.student_name);
                    $('#birthdate').text(res.data.birthdate);
                    $('#student_code').text(res.data.student_code);
                    $('#room_name').text(res.data.room_name);
                    $('#session_time').text(res.data.startEnd);
                    if (res.data.avatar_guardian_3 != '') $('#guardian').append($('#hiddenGuardian').find('.guardian3')[0].outerHTML);
                    if (res.data.avatar_guardian_2 != '') $('#guardian').append($('#hiddenGuardian').find('.guardian2')[0].outerHTML);
                    if (res.data.avatar_guardian_1 != '') $('#guardian').append($('#hiddenGuardian').find('.guardian1')[0].outerHTML);
                    $('.guardian1').find('img').attr('src', res.data.avatar_guardian_1);
                    $('.guardian2').find('img').attr('src', res.data.avatar_guardian_2);
                    $('.guardian3').find('img').attr('src', res.data.avatar_guardian_3);
                    $('.guardian1').find('span').text(res.data.guardian1);
                    $('.guardian2').find('span').text(res.data.guardian2);
                    $('.guardian3').find('span').text(res.data.guardian3);

                    //vertical
                    if(vertical){
                        $('#welcom_title').find('h1').css('font-size', '4.34vw');
                        $('.w-content').removeClass('w-content').addClass('w-content-vertical');
                    }
                } else {
                    $('#content').hide();
                    $('#guardian').hide();
                    $('#content_waiting').hide();
                    $('#content_error').show();
                    $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-error');
                }
                $('#card_number').val('');

                if (timer) {
                    clearTimeout(timer); //cancel the previous timer.
                    timer = null;
                }
                timer = setTimeout(function () {
                    //back to waiting
                    $('#content').hide();
                    $('#guardian').hide();
                    $('#content_waiting').show();
                    $('#content_error').hide();
                    if (vertical) $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-waiting-vertical');
                    else $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-waiting');
                }, 15000);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#SubDiv, #mainDiv').hide();
                $('#content').hide();
                $('#guardian').hide();
                $('#content_waiting').hide();
                $('#content_error').show();
                $('#bg_attendance').removeClass().addClass('blur-img').addClass('bg-error');
                $('#card_number').val('');
            }
        });
    }

}

function startTime() {
    var today = new Date();
    var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
    var d = days[today.getDay()];
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    $('.clock-day').text(d);
    $('.clock-hours').text(h);
    $('.clock-minutes').text(m);
    $('.clock-seconds').text(s);
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function autoChangeScreen(){
    if((vertical && document.body.clientWidth > document.body.clientHeight)
        || (!vertical && document.body.clientWidth < document.body.clientHeight)){
        location.reload();
    }
}
function keypressHandler(evt)  {
    var arrayAlphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-"];
    if(arrayAlphabet.indexOf(evt.key) != -1){
        $('#card_number').val($('#card_number').val() + evt.key);
    }
    if(evt.key == "Enter"){
        ajax_post_code();
    }
}
