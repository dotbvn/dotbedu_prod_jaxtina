<?php

switch ($_POST['type']) {
    case 'checkAttendance':
        echo checkAttendance();
        break;
    case 'checkAttendance2':
        echo checkAttendance2();
        break;
    default:
        echo false;
        die;
}

//------------------------------------------------------------------------------------------------------------------------------
function checkAttendance()
{
    global $timedate;
    $html = '<table width="100%" height = "100%" border="0" id = "tbl_result">';

    $q1 = "SELECT DISTINCT IFNULL(l1.id,'') l1_id ,CONCAT(IFNULL(l1.last_name,''),' ',IFNULL(l1.first_name,'')) l1_full_name, IFNULL(l1.picture,'') l1_picture,IFNULL(l1.team_id,'') l1_team_id, IFNULL(l1.team_set_id,'') l1_team_set_id  FROM contacts l1 WHERE (((l1.member_card='{$_POST['code']}' ))) AND l1.deleted=0";
    $rs1 = $GLOBALS['db']->query($q1);
    $row1 = $GLOBALS['db']->fetchByAssoc($rs1);


    if (!isset($row1['l1_id']) || empty($row1['l1_id'])) {
        $html .= '<tr><td colspan="2" rowspan="1" id="image" nowrap><img style="width: 30%;" src="custom/themes/default/images/company_logo.png"/><br><br></td></tr>';
        $html .= '<tr><td align="center" nowrap>CARD NUMBER NOT FOUND !</td></tr></table>';

        return json_encode(array(
            "success" => "0",
            "html" => $html,
        ));
    } else {
        if (empty($GLOBALS['current_user']->id))
            $GLOBALS['current_user'] = BeanFactory::getBean('Users', '1');
        if (empty($row1['l1_picture']))
            $picture_url = "themes/default/images/noimage.png";
        else $picture_url = "upload/origin/" . $row1['l1_picture'];
        $html .= '<tr><td colspan="2" id="image" nowrap><img src="' . $picture_url . '" style="border: 0; width: 150px; height: auto;"><br><br></td></tr>';
        $html .= '<tr><td width="50%" align="right" nowrap>HAVE A NICE DAY:</td><td width="50%" align="left" nowrap style="font-weight: bold;">' . $row1['l1_full_name'] . '</td></tr>';

        //Lấy trong 30 phut gần đầy
        $now = date('Y-m-d H:i:s',strtotime("-7 hours ".date('Y-m-d H:i:s')));
        $nowDate = date('Y-m-d');
        $arr_sh[1]['start'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 00:00:00"));
        $arr_sh[1]['end'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 11:59:59"));
        $arr_sh[2]['start'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 12:00:00"));
        $arr_sh[2]['end'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 23:59:59"));

        $keyShift = 1;
        foreach($arr_sh as $key => $shift){
            if($shift['start'] >= $now &&  $now <= $shift['end'])
                $keyShift = $key;
        }

        $q2 = "SELECT DISTINCT
        IFNULL(l1.id,'') class_id,
        IFNULL(l1.name,'') class_name,
        IFNULL(l2.id, '') meeting_id,
        IFNULL(l2.lesson_number, '') lesson_number,
        IFNULL(l2.duration_cal, 0) duration_cal,
        l2.date_start date_start,
        l2.date_end date_end,
        IFNULL(l3.id, '') l3_id,
        IFNULL(l3.full_teacher_name, '') teacher_name,
        IFNULL(l4.id, '') l4_id,
        IFNULL(l4.full_teacher_name, '') ta_name,
        IFNULL(l5.id, '') l5_id,
        IFNULL(l5.name, '') room_name,
        IFNULL(ca.id, '') attend_id,
        IFNULL(l10.id, '') team_id,
        IFNULL(ca.attendance_type, '') atten_type
        FROM contacts
        INNER JOIN j_class_contacts_1_c l1_1 ON contacts.id = l1_1.j_class_contacts_1contacts_idb AND l1_1.deleted = 0
        INNER JOIN j_class l1 ON l1.id = l1_1.j_class_contacts_1j_class_ida AND l1.deleted = 0
        INNER JOIN meetings l2 ON l1.id = l2.ju_class_id AND l2.deleted = 0
        LEFT JOIN c_teachers l3 ON l2.teacher_id = l3.id AND l3.deleted = 0
        LEFT JOIN c_teachers l4 ON l2.teacher_cover_id = l4.id AND l4.deleted = 0
        LEFT JOIN c_rooms l5 ON l2.room_id = l5.id AND l5.deleted = 0
        LEFT JOIN c_attendance ca ON contacts.id = ca.student_id
        AND l2.id = ca.meeting_id
        AND ca.deleted = 0
        INNER JOIN teams l10 ON l1.team_id = l10.id
        AND l10.deleted = 0
        WHERE (((contacts.id = '{$row1['l1_id']}')
        AND (l2.date_start >= '{$arr_sh[$keyShift]['start']}'
        AND l2.date_start <= '{$arr_sh[$keyShift]['end']}')
        AND (IFNULL(l2.session_status, '') <> 'Cancelled' OR (IFNULL(l2.session_status, '') IS NULL
        AND 'Cancelled' IS NOT NULL))))
        AND contacts.deleted = 0
        LIMIT 1";
        $rs2 = $GLOBALS['db']->query($q2);
        $count = 0;
        while ($row = $GLOBALS['db']->fetchByAssoc($rs2)) {
            if (!isset($row['class_id']) || empty($row['class_id'])) {
                $html .= '<tr><td align="center" colspan="2" nowrap>YOU DON\'T HAVE ANY CLASS TODAY</td></tr>';
                $html .= '</table>';
                return json_encode(array(
                    "success" => "1",
                    "html" => $html,
                ));
            } else {
                //put in Attendance
                if (empty($row['attend_id']))
                    $attend = BeanFactory::newBean('C_Attendance');
                else
                    $attend = BeanFactory::retrieveBean('C_Attendance', $row['attend_id'], array('disable_row_level_security' => true));



                $attend->student_id = $row1['l1_id'];
                $attend->meeting_id = $row['meeting_id'];
                $attend->class_id = $row['class_id'];
                $attend->student_type = 'Contacts';
                $attend->date_input = $timedate->to_display_date($row['date_end']);
                $attend->attendance_type = 'P';
                $attend->attended = 1;
                $attend->in_class = 1;
                $attend->absent_for_hour = $row['duration_cal'];

                $attend->team_id = $row['team_id'];
                $attend->team_set_id = $attend->team_id;

                $attend->save();

                $DATE_START = $timedate->to_display_date_time($row['date_start']);
                $DATE_END = $timedate->to_display_date_time($row['date_end']);
                $count += 1;
                if ($count > 1) {
                    $startEnd .= "<br>";
                    $class_name .= "<br>";
                    $teacher_name .= "<br>";
                    $room_name .= "<br>";
                }
                $startEnd .= substr($DATE_START, 11, 5) . ' - ' . substr($DATE_END, 11);
                $class_name .= $row['class_name'];
                $teacher_name .= $row['teacher_name'];
                $room_name .= $row['room_name'];
            }
        }
        $html .= '<tr><td align="right" nowrap>CLASS NAME: </td><td align="left" nowrap>' . $class_name . '</td></tr>';
        $html .= '<tr><td align="right" nowrap>TEACHER: </td><td align="left" nowrap>' . $teacher_name . '</td></tr>';
        $html .= '<tr><td align="right" nowrap>ROOM: </td><td align="left" nowrap>' . $room_name . '</td></tr>';
        $html .= '<tr><td align="right" nowrap>TIME: </td><td align="left" nowrap>' . $startEnd . '</td></tr>';
        $html .= '<tr><td colspan="2" align="center" nowrap> <span> THANK YOU FOR CHECKIN ! <img style="width: 30px;" id="checked_icon" src="custom/themes/default/images/checked_icon.png"/></span></td></tr>';
        $html .= '</table>';
        return json_encode(array(
            "success" => "1",
            "html" => $html,
        ));
    }
}

function checkAttendance2()
{
    if (empty($GLOBALS['current_user']->id))
        $GLOBALS['current_user'] = BeanFactory::getBean('Users', '1');
    require_once('custom/modules/C_Attendance/_helper.php');

    global $timedate, $db;
    $time_check_in_out = '1 hour';//khoảng thời gian check in out
    $interval = 60;//khoảng thời gian giữa 2 buổi học xét interval
    $list_kid_koc = ['JOY ENGLISH'];
    $now = $timedate->nowDb();

    $qStudent = "SELECT DISTINCT IFNULL(l1.id,'') l1_id,
    CONCAT(IFNULL(l1.last_name,''),' ',IFNULL(l1.first_name,'')) student_name,
    IFNULL(l1.picture,'') picture,
    l1.birthdate birthdate,
    IFNULL(l1.contact_id, 0) student_code,
    IFNULL(l1.guardian1,'') guardian1,
    IFNULL(l1.guardian_type_1,'') guardian_type_1,
    IFNULL(l2.picture,'') avatar_guardian_1,
    IFNULL(l1.guardian2,'') guardian2,
    IFNULL(l1.guardian_type_2,'') guardian_type_2,
    IFNULL(l3.picture,'') avatar_guardian_2,
    IFNULL(l1.guardian3,'') guardian3,
    IFNULL(l1.guardian_type_3,'') guardian_type_3,
    IFNULL(l4.picture,'') avatar_guardian_3
    FROM contacts l1
    LEFT JOIN j_membership l2 ON l1.guardian1 = l2.id AND l2.deleted=0
    LEFT JOIN j_membership l3 ON l1.guardian2 = l3.id AND l3.deleted=0
    LEFT JOIN j_membership l4 ON l1.guardian3 = l4.id AND l4.deleted=0
    WHERE ((l1.contact_id='{$_POST['code']}') OR (l1.id='{$_POST['code']}') )
    AND l1.deleted=0";
    $rs1 = $GLOBALS['db']->query($qStudent);
    $rowStudent = $GLOBALS['db']->fetchByAssoc($rs1);


    if (!isset($rowStudent['l1_id']) || empty($rowStudent['l1_id'])) {
        return json_encode(array(
            "success" => "0",
            "mes_code" => 'student_not_found',
        ));
    } else {
        $picture_url = resizePicture('Contacts', $rowStudent['l1_id'], $rowStudent['picture']);
        if (empty($picture_url)) $picture_url = 'themes/default/images/noimage.png';

        $data = array();
        $data['student_avatar'] = $picture_url;
        $data['student_name'] = $rowStudent['student_name'];
        $data['student_code'] = $rowStudent['student_code'];
        $data['birthdate'] = $timedate->to_display_date($rowStudent['birthdate']);
        $data['guardian1'] = replaceGuardianType($rowStudent['guardian_type_1']);
        $data['avatar_guardian_1'] = resizePicture('J_Membership', $rowStudent['guardian1'], $rowStudent['avatar_guardian_1']);
        $data['guardian2'] = replaceGuardianType($rowStudent['guardian_type_2']);
        $data['avatar_guardian_2'] = resizePicture('J_Membership', $rowStudent['guardian2'], $rowStudent['avatar_guardian_2']);
        $data['guardian3'] = replaceGuardianType($rowStudent['guardian_type_3']);
        $data['avatar_guardian_3'] = resizePicture('J_Membership', $rowStudent['guardian3'], $rowStudent['avatar_guardian_3']);
        $data['room_name'] = '';
        $data['startEnd'] = '';
        $room_name = '';
        $startEnd = '';

        //get student info
        if (!empty($rowStudent)) {
            //Lấy trong thoi gian $time_check_in_out gần đầy
            $now = date('Y-m-d H:i:s',strtotime("-7 hours ".date('Y-m-d H:i:s')));
            $nowDate = date('Y-m-d');
            $arr_sh[1]['start'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 00:00:00"));
            $arr_sh[1]['end'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 11:59:59"));
            $arr_sh[2]['start'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 12:00:00"));
            $arr_sh[2]['end'] = date('Y-m-d H:i:s',strtotime("-7 hours ".$nowDate." 23:59:59"));

            foreach($arr_sh as $key => $shift){
                if($shift['start'] <= $now &&  $now <= $shift['end']){
                    $startDB = $arr_sh[$key]['start'];
                    $endDB = $arr_sh[$key]['end'];
                    break;
                }

            }

            $q1 = getStringQueryClass($rowStudent['l1_id'], $startDB, $endDB, 'date_start');
            $q2 = getStringQueryClass($rowStudent['l1_id'], $startDB, $endDB, 'date_end');
            $row1 = $db->fetchOne($q1);
            $row2 = $db->fetchOne($q2);
            if (!empty($row1)) {
                //xu li luu check-in
                $check_in = 1;
                saveAttendance($row1, $rowStudent['l1_id'], 'Check in');
            }
            if (!empty($row2)) {
                $startDB = date('Y-m-d H:i:s', strtotime("-7 hours " . $timedate->convertToDBDate($timedate->to_display_date($now)) . " 00:00:00"));

                $qCheckInterval = getStringQueryClass($rowStudent['l1_id'], $startDB, $endDB, 'interval');
                $resCheckInterval = $db->query($qCheckInterval);
                $i = 0;
                $preDateStart = '';
                $afterFilter = array();
                $checkInterval = 0;
                while ($row = $db->fetchByAssoc($resCheckInterval)) {
                    if ($i != 0 && (strtotime($preDateStart) - strtotime($row['date_end'])) > ($interval * 60)) {
                        break;
                    }
                    $afterFilter[] = $row;
                    if (!empty($row['check_in_time'])) {
                        $checkInterval = 1;
                        break;
                    }
                    $preDateStart = $row['date_start'];
                    $i++;
                }
                if ($checkInterval) {
                    $countAfterFilter = count($afterFilter);
                    if ($countAfterFilter > 0) {
                        for ($i = 0; $i < $countAfterFilter; $i++) {
                            $row = $afterFilter[$i];
                            if ($i == 0) {
                                saveAttendance($row, $rowStudent['l1_id'], 'Check out');
                            } else saveAttendance($row, $rowStudent['l1_id'], 'Interval');
                        }
                        $check_out = 1;
                    }
                }
            }

            //show screen
            if ($check_in) {
                getDataScreen($row1, $data);
                $data['background'] = 1;
                $kind_of_course = $row1['kind_of_course'];
            } elseif ($check_out) {
                getDataScreen($row2, $data);
                $data['background'] = 2;
                $kind_of_course = $row2['kind_of_course'];
            } else {
                $startDB = date('Y-m-d H:i:s', strtotime("-7 hours " . $timedate->convertToDBDate($timedate->to_display_date($now)) . " 00:00:00"));
                $endDB = date('Y-m-d H:i:s', strtotime("-7 hours " . $timedate->convertToDBDate($timedate->to_display_date($now)) . " 23:59:59"));

                $q3 = getStringQueryClass($rowStudent['l1_id'], $startDB, $endDB);
                $res = $db->query($q3);
                $count_session = 0;
                while ($row = $db->fetchByAssoc($res)) {
                    $DATE_START = $timedate->to_display_date_time($row['date_start']);
                    $DATE_END = $timedate->to_display_date_time($row['date_end']);
                    $count_session++;
                    if ($count_session > 1) {
                        if(!empty($startEnd)) $startEnd .= ", ";
                        if(!empty($room_name)) $room_name .= ", ";
                    }
                    $startEnd .= substr($DATE_START, 11, 5) . ' - ' . substr($DATE_END, 11);
                    $room_name .= $row['room_name'];
                    $kind_of_course = $row['kind_of_course'];
                }
                if ($count_session > 0) {
                    $data['room_name'] = $room_name;
                    $data['startEnd'] = $startEnd;
                }
                $data['background'] = 3;
            }
            if (!in_array($kind_of_course, $list_kid_koc)) {//junior
                $data['background'] += 3;
            }

            //gan thong tin background
            switch ($data['background']) {
                case 1:
                    $data['background'] = 'bg-kid_check_in';
                    $data['header'] = '' .
                    '<div class="col-12">' .
                    '                                <h1 class="w-title-4">' .
                    '                                    Welcome to ALAB!' .
                    '                                </h1><br>' .
                    '                                <h1 class="w-title-3">' .
                    '                                    Wish you an awesome day!' .
                    '                                </h1>' .
                    '                                <p class="w-content">' .
                    '                                    Chào mừng Bé đã đến ALAB.<br/>' .
                    '                                    Chúc bé một buổi học thật tuyệt vời nhé!' .
                    '                                </p>' .
                    '                            </div>';
                    break;
                case 2:
                    $data['background'] = 'bg-kid_check_out';
                    $data['header'] = '' .
                    '<div class="col-12">' .
                    '                                <h1 class="w-title-4">' .
                    '                                    Welcome to ALAB!' .
                    '                                </h1>' .
                    '                                <h1 class="w-title-3">' .
                    '                                    Please wait for a minutes, your child is coming!' .
                    '                                </h1>' .
                    '                                <p class="w-content">' .
                    '                                    ALAB chào Quý Phụ Huynh!<br/>' .
                    '                                    Vui lòng đợi trong giây lát, Bé sẽ ra ngay đây ạ!' .
                    '                                </p>' .
                    '                            </div>';
                    break;
                case 3:
                    $data['background'] = 'bg-kid_check_in_wrong';
                    $data['header'] = '' .
                    '<div class="col-12">' .
                    '                                                <h1 class="w-title-4">' .
                    '                                                    Oh no! Your child have no class today!<br/>' .
                    '                                                    Please contact our staffs for supporting.' .
                    '                                                </h1>' .
                    '                                                <p class="w-content">' .
                    '                                                    Ôi không! Bé không có lớp học hôm nay!<br/>' .
                    '                                                    Vui lòng liên hệ ngay nhân viên của ALAB để được hỗ trợ!' .
                    '                                                </p>' .
                    '                                            </div>';
                    break;
                case 4:
                    $data['background'] = 'bg-junior_check_in';
                    $data['header'] = '' .
                    '<div class="col-12">' .
                    '                                                <h1 class="w-title">Welcome to ALAB!</h1><br/>' .
                    '                                                <h1 class="w-title-2">Wish you an awesome day!</h1>' .
                    '                                                <p class="w-content">' .
                    '                                                    Chào mừng Bé đã đến ALAB.<br/>' .
                    '                                                    Chúc Bé một buổi học thật tuyệt vời nhé!' .
                    '                                                </p>' .
                    '                                            </div>';
                    break;
                case 5:
                    $data['background'] = 'bg-junior_check_out';
                    $data['header'] = '' .
                    ' <div class="col-12">' .
                    '                                                <h1 class="w-title-3">' .
                    '                                                    Bye Bye!<br/>' .
                    '                                                    See you next time!' .
                    '                                                </h1>' .
                    '                                                <p class="w-content">' .
                    '                                                    Chào Bé! Hẹn gặp lại trong buổi học sau nhé!' .
                    '                                                </p>' .
                    '                                            </div>';
                    break;
                case 6:
                    $data['background'] = 'bg-junior_check_in_wrong';
                    $data['header'] = '' .
                    '<div class="col-12">' .
                    '                                                <h1 class="w-title-4">' .
                    '                                                    Oh no! You have no class today!<br/>' .
                    '                                                    Please come to your teachers for instructions.' .
                    '                                                </h1>' .
                    '                                                <p class="w-content">' .
                    '                                                    Tiếc quá! Bé chưa có lớp học hôm nay rồi!<br/>' .
                    '                                                    Đến chỗ các thầy cô để được hướng dẫn nhé!' .
                    '                                                </p>' .
                    '                                            </div>';
                    break;
                default:
                    break;
            }

            return json_encode(array(
                'success' => 1,
                'data' => $data,
            ));
        }
        return json_encode(array(
            'success' => 0,
        ));
    }

}

?>
