<?php

//get query class
function getStringQueryClass($student_id, $startDB, $endDB, $typeDate = '')
{
    $foot = 'ORDER BY date_start ASC LIMIT 1';
    if ($typeDate == 'interval') {
        $typeDate = 'date_end';
        $foot = ' ORDER BY date_start DESC ';
    } elseif (empty($typeDate)) {
        $typeDate = 'date_start';
        $foot = '';
    }
    return $qClass = "SELECT DISTINCT
    IFNULL(l1.id,'') class_id,
    IFNULL(l1.name,'') class_name,
    IFNULL(l1.kind_of_course,'') kind_of_course,
    IFNULL(l2.id, '') meeting_id,
    IFNULL(l2.lesson_number, '') lesson_number,
    IFNULL(l2.duration_cal, 0) duration_cal,
    IFNULL(l2.date_start, '') date_start,
    IFNULL(l2.date_end, '') date_end,
    IFNULL(l5.id, '') l5_id,
    IFNULL(l5.name, '') room_name,
    IFNULL(ca.id, '') attend_id,
    IFNULL(ca.check_in_time, '') check_in_time,
    IFNULL(ca.check_out_time, '') check_out_time,
    IFNULL(l10.id, '') team_id,
    IFNULL(ca.attendance_type, '') atten_type
    FROM contacts
    INNER JOIN j_classstudents l1_1 ON contacts.id = l1_1.student_id AND l1_1.deleted = 0
    INNER JOIN j_class l1 ON l1.id = l1_1.class_id AND l1.deleted = 0
    INNER JOIN meetings l2 ON l1.id = l2.ju_class_id AND l2.deleted = 0
    LEFT JOIN c_rooms l5 ON l2.room_id = l5.id AND l5.deleted = 0
    LEFT JOIN c_attendance ca ON contacts.id = ca.student_id
    AND l2.id = ca.meeting_id
    AND ca.deleted = 0
    INNER JOIN teams l10 ON l1.team_id = l10.id
    AND l10.deleted = 0
    WHERE (((contacts.id = '{$student_id}')
    AND (l2.{$typeDate} >= '$startDB'
    AND l2.{$typeDate} <= '$endDB')
    AND (IFNULL(l2.session_status, '') <> 'Cancelled' OR (IFNULL(l2.session_status, '') IS NULL
    AND 'Cancelled' IS NOT NULL))))
    AND contacts.deleted = 0
    $foot";
}

//resize picture 3x4
function resizePicture($module, $id, $picture)
{
    if (empty($picture)) {
        $picture_url = '';
    } elseif (file_exists('upload/image3x4/' . $picture)) {
        $picture_url = 'upload/image3x4/' . $picture;
    } else {
        require_once('include/DotbFields/Fields/Image/ImageHelper.php');
        if (!is_dir('upload/image3x4')) {
            mkdir('upload/image3x4');
        }
        $pictureOri = 'upload/origin/' . $picture;
        if (file_exists($pictureOri)) {
            $imgOri = new ImageHelper($pictureOri);
            $imgOri->resize(220, 293.3);
            $imgOri->save('upload/image3x4/' . $picture);
            $picture_url = 'upload/image3x4/' . $picture;
        } else $picture_url = '';
    }
    return $picture_url;
}

//lay label theo key GuardianType
function replaceGuardianType($guardian_type)
{
    if ($guardian_type == 'Dad') return 'Dad/Bố';
    if ($guardian_type == 'Mom') return 'Mom/Mẹ';
    if ($guardian_type == 'Grandfather') return 'Grandfa/Ông';
    if ($guardian_type == 'Grandmother') return 'Grandma/Bà';
    if ($guardian_type == 'Other' || empty($guardian_type)) return 'Other/Khác';
}

//xu li luu attendance khi hoc vien quet the
function saveAttendance($row = array(), $studentID, $typeUpdate = '')
{
    global $timedate;
    if (empty($row['attend_id']))
        $attend = BeanFactory::newBean('C_Attendance');
    else {
        $attend = BeanFactory::retrieveBean('C_Attendance', $row['attend_id'], array('disable_row_level_security' => true));
    }

    if(empty($attend->check_in_time)) {
        $attend->student_id = $studentID;
        $attend->meeting_id = $row['meeting_id'];
        $attend->class_id = $row['class_id'];
        $attend->student_type = 'Contacts';
        $attend->date_input = $timedate->to_display_date($row['date_end']);
        $attend->attendance_type = 'P';
        $attend->team_id = $row['team_id'];
        $attend->team_set_id = $attend->team_id;
        $attend->check_in_time = $timedate->nowDb(); //Check in
    }else
        $attend->check_out_time = $timedate->nowDb(); //check out

    $attend->notificated = 1; //Send notification
    $attend->save();
}

//lay 1 so thong tin xuat len man hinh
function getDataScreen($row = array(), &$data)
{
    global $timedate;
    $DATE_START = $timedate->to_display_time($row['date_start']);
    $DATE_END   = $timedate->to_display_time($row['date_end']);

    $data['startEnd'] = $DATE_START. ' - ' . $DATE_END;
    $data['room_name'] = $row['room_name'];
}
?>
