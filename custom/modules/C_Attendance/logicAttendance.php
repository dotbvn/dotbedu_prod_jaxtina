<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicAttendance
{
    function listViewColor(&$bean, $event, $arguments){
        $tmp_status = $GLOBALS['app_list_strings']['attendance_type_list'][$bean->attendance_type];
        switch ($bean->attendance_type) {
            case 'P':
                $color = "label-success";
                $icon  = "fa-check";
                break;
            case 'L':
                $color = "label-warning";
                $icon  = "fa-clock-o";
                break;
            case 'E':
                $color = "label-excused";
                $icon  = "fa-ban";
                break;
            case 'A':
                $color = "label-important";
                $icon  = "fa-times";
                break;
        }
        if(!empty($bean->attendance_type)) $bean->attendance_type_text = "<span class='label ellipsis_inline $color'><i class='far $icon'></i>$tmp_status</span>";
        else $bean->attendance_type_text = '';
    }

    function handleProcessRecord(&$bean, $event, $arguments){
        if(!empty($bean->student_id) && !empty($bean->student_type)) {
            if ($bean->student_type == 'Leads') {
                $lead = BeanFactory::getBean($bean->student_type, $bean->student_id);
                $bean->student_name = $lead->full_lead_name;
            } else {
                $student = BeanFactory::getBean($bean->student_type, $bean->student_id);
                $bean->student_name = $student->full_student_name;
            }
        }
    }


    function handleBeforeSave(&$bean, $event, $arguments){
        $bean->attended =  ($bean->attendance_type == 'P' || $bean->attendance_type == 'L') ? 1 : 0;
        if($bean->notificated) $bean->datetime_notify_sent = $GLOBALS['timedate']->nowDb();
        //Set lead Status
        if(empty($bean->student_type)) $bean->student_type = $_POST['parent_type'];
        //Change status of lead from Ready PT/Demo to PT/Demo
        if($bean->student_type == 'Leads'){
            $leadbean = BeanFactory::getBean('Leads', $bean->student_id);
            if(!empty($leadbean->id)
            && ($bean->attendance_type == 'P' || $bean->attendance_type == 'L')
            && ($bean->fetched_row['attendance_type'] != $bean->attendance_type)
            && (!$leadbean->converted)
            && ($leadbean->status != 'PT/Demo')) {
                $leadbean->status = 'PT/Demo';
                $leadbean->save();
            }
        }

        //kiểm tra dữ liệu thay đổi để tối ưu performance
        $cacheFields = ['attendance_type', 'homework', 'datetime_notify_sent'];
        foreach($cacheFields as $field) if($bean->fetched_row[$field] != $bean->$field) $countC++;
        if(empty($countC)) $bean->no_cache = true;
    }

    function handleAfterSave(&$bean, $event, $arguments){
        if(!empty($bean->meeting_id)){
            //send notifi
            if($bean->notificated) {
                require_once('custom/clients/mobile/helper/NotificationHelper.php');
                $notify = new NotificationMobile();
                if (!empty($bean->student_id)) {
                    if(!empty($bean->date_input)){
                        $date = date('d/m', strtotime($bean->date_input));
                        $ext1 = " ngày $date";
                    }
                    $notify->pushNotification('Sổ liên lạc điện tử', 'Thông tin sổ liên lạc'.$ext1.' đã được cập nhật', $bean->module_name, $bean->id, $bean->student_id);
                }
            }
            //Set Count Attendances
            if(($bean->date_entered != $bean->date_modified) && (!$bean->no_cache)){
                updateMeetingAttendance($bean->meeting_id);
                updateClassStudent_ATT($bean->class_id, $bean->student_id);
            }

        }
    }
}


?>
