<?php
// created: 2020-06-29 14:53:25
$subpanel_layout['list_fields'] = array (
  'date_input' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DATE_INPUT',
    'width' => 10,
    'default' => true,
  ),
  'class_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_CLASS_NAME',
    'id' => 'CLASS_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'J_Class',
    'target_record_key' => 'class_id',
  ),
  'attendance_type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_ATTENDANCE_TYPE',
    'width' => 10,
    'default' => true,
  ),
  'homework' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'vname' => 'LBL_DO_HOMEWORK',
    'width' => 10,
    'default' => true,
  ),
  'homework_comment' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_HOMEWORK_COMMENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'care_comment' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_CARE_COMMENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'homework_score' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_HOMEWORK_SCORE',
    'width' => 10,
  ),
  'star_rating' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_STAR_RATING',
    'width' => 10,
  ),
  'customer_comment' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_CUSTOMER_COMMENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
);