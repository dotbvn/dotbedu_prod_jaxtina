<?php
$module_name = 'C_Attendance';
$viewdefs[$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'meeting_name',
            'label' => 'LBL_MEETING_NAME',
          ),
          1 => 
          array (
            'name' => 'student_name',
            'studio' => true,
            'label' => 'LBL_STUDENT_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'attendance_type',
            'studio' => 'visible',
            'label' => 'LBL_ATTENDANCE_TYPE',
          ),
          1 => 
          array (
            'name' => 'homework_score',
            'label' => 'LBL_HOMEWORK_SCORE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'class_name',
            'label' => 'LBL_CLASS_NAME',
          ),
          1 => 
          array (
            'name' => 'check_in_time',
            'label' => 'LBL_CHECK_IN_TIME',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'date_input',
            'label' => 'LBL_DATE_INPUT',
          ),
          1 => 
          array (
            'name' => 'check_out_time',
            'label' => 'LBL_CHECK_OUT_TIME',
          ),
        ),
        4 => 
        array (
          0 => 'description',
          1 => 
          array (
            'name' => 'homework_comment',
            'label' => 'LBL_HOMEWORK_COMMENT',
          ),
        ),
      ),
    ),
  ),
);
