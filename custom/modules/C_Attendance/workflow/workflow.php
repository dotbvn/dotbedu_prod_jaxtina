<?php

use Dotbcrm\Dotbcrm\Util\Arrays\ArrayFunctions\ArrayFunctions;
include_once("include/workflow/alert_utils.php");
include_once("include/workflow/action_utils.php");
include_once("include/workflow/time_utils.php");
include_once("include/workflow/trigger_utils.php");
//BEGIN WFLOW PLUGINS
include_once("include/workflow/custom_utils.php");
//END WFLOW PLUGINS
	class C_Attendance_workflow {
	function process_wflow_triggers(& $focus){
		include("custom/modules/C_Attendance/workflow/triggers_array.php");
		include("custom/modules/C_Attendance/workflow/alerts_array.php");
		include("custom/modules/C_Attendance/workflow/actions_array.php");
		include("custom/modules/C_Attendance/workflow/plugins_array.php");
		if(isset($focus->fetched_row['id']) && $focus->fetched_row['id']!=""){ 
 
 if((
 	 ( 
 		isset($focus->attended) && $focus->attended === false ||
 		isset($focus->attended) && $focus->attended === 'false' ||
 		isset($focus->attended) && $focus->attended === 'off' ||
 		isset($focus->attended) && $focus->attended === 0 ||
 		isset($focus->attended) && $focus->attended === '0'
 	 )  
)){ 
 

	 //Frame Secondary 

	 $secondary_array = array(); 
	 //Secondary Triggers 
	 //Secondary Trigger number #1
	 if((
 	 ( 
 		isset($focus->in_class) && $focus->in_class === true ||
 		isset($focus->in_class) && $focus->in_class === 'true' ||
 		isset($focus->in_class) && $focus->in_class === 'on' ||
 		isset($focus->in_class) && $focus->in_class === 1 ||
 		isset($focus->in_class) && $focus->in_class === '1'
 	 )  
)	 ){ 
	 

	 //Secondary Trigger number #2
	 $secondary_array = check_rel_filter($focus, $secondary_array, 'meeting_attendances', $trigger_meta_array['trigger_0_secondary_1'], 'all'); 
	 if(($secondary_array['results']==true)	 ){ 
	 


	global $triggeredWorkflows;
	if (!isset($triggeredWorkflows['c858a82a_fb71_11ed_84e2_067482011a2a'])){
		$triggeredWorkflows['c858a82a_fb71_11ed_84e2_067482011a2a'] = true;
		$action_meta_array['C_Attendance0_action0']['trigger_id'] = 'c858a82a_fb71_11ed_84e2_067482011a2a';
	$action_meta_array['C_Attendance0_action0']['action_id'] = 'daa966c4-0980-11ed-ab67-067482011a2a';
	 $action_meta_array['C_Attendance0_action0']['workflow_id'] = '35d4c4ea-0980-11ed-8063-067482011a2a';
	 process_workflow_actions($focus, $action_meta_array['C_Attendance0_action0']); 
 	$_SESSION['WORKFLOW_ALERTS'] = isset($_SESSION['WORKFLOW_ALERTS']) && ArrayFunctions::is_array_access($_SESSION['WORKFLOW_ALERTS']) ? $_SESSION['WORKFLOW_ALERTS'] : array();
		$_SESSION['WORKFLOW_ALERTS']['C_Attendance'] = isset($_SESSION['WORKFLOW_ALERTS']['C_Attendance']) && ArrayFunctions::is_array_access($_SESSION['WORKFLOW_ALERTS']['C_Attendance']) ? $_SESSION['WORKFLOW_ALERTS']['C_Attendance'] : array();
		$_SESSION['WORKFLOW_ALERTS']['C_Attendance'] = ArrayFunctions::array_access_merge($_SESSION['WORKFLOW_ALERTS']['C_Attendance'],array ());	}
 

	 //End Frame Secondary 

	 // End Secondary Trigger number #1
 	 } 

	 // End Secondary Trigger number #2
 	 } 

	 unset($secondary_array); 
 

 //End if trigger is true 
 } 

		 //End if new, update, or all record
 		} 


	//end function process_wflow_triggers
	}

	//end class
	}

?>