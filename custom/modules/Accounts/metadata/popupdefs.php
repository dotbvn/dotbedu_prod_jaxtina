<?php
$popupMeta = array (
    'moduleMain' => 'Account',
    'varName' => 'ACCOUNT',
    'orderBy' => 'name',
    'whereClauses' => array (
        'name' => 'accounts.name',
        'tax_code' => 'accounts.tax_code',
        'assigned_user_id' => 'accounts.assigned_user_id',
        'phone_office' => 'accounts.phone_office',
        'billing_address_street' => 'accounts.billing_address_street',
        'date_entered' => 'accounts.date_entered',
    ),
    'searchInputs' => array (
        0 => 'name',
        3 => 'tax_code',
        8 => 'assigned_user_id',
        11 => 'phone_office',
        12 => 'billing_address_street',
        13 => 'date_entered',
    ),
    'create' => array (
        'formBase' => 'AccountFormBase.php',
        'formBaseClass' => 'AccountFormBase',
        'getFormBodyParams' =>
        array (
            0 => '',
            1 => '',
            2 => 'AccountSave',
        ),
        'createButton' => 'LNK_NEW_ACCOUNT',
    ),
    'searchdefs' => array (
        'name' =>
        array (
            'name' => 'name',
            'width' => '10',
        ),
        'tax_code' =>
        array (
            'type' => 'varchar',
            'label' => 'LBL_TAX_CODE',
            'width' => '10',
            'name' => 'tax_code',
        ),
        'phone_office' =>
        array (
            'type' => 'phone',
            'label' => 'LBL_PHONE_OFFICE',
            'width' => '10',
            'name' => 'phone_office',
        ),
        'billing_address_street' =>
        array (
            'type' => 'text',
            'label' => 'LBL_BILLING_ADDRESS_STREET',
            'sortable' => false,
            'width' => '10',
            'name' => 'billing_address_street',
        ),
        'assigned_user_id' =>
        array (
            'name' => 'assigned_user_id',
            'label' => 'LBL_ASSIGNED_TO',
            'type' => 'enum',
            'function' =>
            array (
                'name' => 'get_user_array',
                'params' =>
                array (
                    0 => false,
                ),
            ),
            'width' => '10',
        ),
        'date_entered' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_ENTERED',
            'width' => '10',
            'name' => 'date_entered',
        ),
    ),
    'listviewdefs' => array (
        'NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_LIST_ACCOUNT_NAME',
            'link' => true,
            'default' => true,
            'name' => 'name',
        ),
        'TAX_CODE' =>
        array (
            'type' => 'varchar',
            'label' => 'LBL_TAX_CODE',
            'width' => 10,
            'default' => true,
            'name' => 'tax_code',
        ),
        'BILLING_ADDRESS_STREET' =>
        array (
            'width' => 10,
            'label' => 'LBL_BILLING_ADDRESS_STREET',
            'default' => true,
            'name' => 'billing_address_street',
        ),
        'PHONE_OFFICE' =>
        array (
            'width' => 10,
            'label' => 'LBL_LIST_PHONE',
            'default' => true,
            'name' => 'phone_office',
        ),
        'DATE_ENTERED' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_ENTERED',
            'width' => 10,
            'default' => true,
            'name' => 'date_entered',
        ),
        'TEAM_NAME' =>
        array (
            'type' => 'relate',
            'link' => true,
            'studio' =>
            array (
                'portallistview' => false,
                'portalrecordview' => false,
            ),
            'label' => 'LBL_TEAMS',
            'id' => 'TEAM_ID',
            'width' => 10,
            'default' => true,
            'name' => 'team_name',
        ),
    ),
);
