<?php
class unitLogicHook{
    function handleBeforeSave($bean, $event, $arguments){
        if($bean->is_primary){
            $bean->quantity =1;
            $bean->quantity_base_unit=1;
            $bean->base_unit_id= '';
        }
        if(!empty($bean->base_unit_id)){
            $bean->quantity_base_unit  = $bean->quantity*$GLOBALS['db']->getOne("SELECT quantity_base_unit FROM j_unit where deleted=0 and id ='{$bean->base_unit_id}'");
        }
        else
            $bean->quantity_base_unit = $bean->quantity;

        //Share all team defaut -HED r2
        $bean->team_id      = '1';
        $bean->team_set_id  = '1';
    }

    function handleBeforeDelete($bean, $event, $arguments){
        $sql = "UPDATE j_unit SET deleted = 1, date_modified='{$GLOBALS['timedate']->nowDb()}',modified_user_id='{$GLOBALS['current_user']->id}' WHERE group_unit_id = '{$bean->id}'";
        $GLOBALS['db']->query($sql);
    }
}
?>
