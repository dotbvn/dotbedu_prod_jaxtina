<?php
class logicCheckInOut
{
    //Handle check api from phenikaa device - Hoang Hvy
    public function checkapi($bean, $event, $arguments){
        $data = array();
        if($bean->verify_status == -1){
            $data['status'] = -1;
        } else {
            $data['status'] = 1;
            $time = $bean->time_check;
            $current_time = date('Y-m-d H:i:s', strtotime($bean->time_check));
            $compare = date('Y-m-d 5:00:00',strtotime($bean->time_check));
            $time_bot = date('Y-m-d 22:00:00', strtotime($bean->time_check .'-1 day') );
            $check_in = 0;
            $check_out = 0;
            global $timedate;


            if(strtotime($current_time) > strtotime($compare)){
                $session = 1;
                $time_top = date('Y-m-d 15:00:00', strtotime($bean->time_check));
            } else {
                $session = 0;
                $time_top = date('Y-m-d 5:00:00',strtotime($bean->time_check));
            }
            //If session = 0 (morning) we will get the meeting that have start_date in range 5am -> 12am
            //else session = 1 (evening) we will get the meeting in range 5am -> 22pm
            $bean->time_check = $time;
            $data['time_check'] = date('H:i:s', strtotime($bean->time_check . ' +7 hours'));
            $q = "SELECT DISTINCT
            IFNULL(contacts.id, '') student_id,
            IFNULL(contacts.last_name, '') student_last_name,
            IFNULL(contacts.first_name, '') student_first_name,
            IFNULL(l1.id, '') meeting_id,
            IFNULL(l3.id, '') class_id,
            IFNULL(l5.name, '') room_name,
            IFNULL(l2.id, '') att_id,
            IFNULL(l2.attendance_type, '') att_type,
            IFNULL(l4.team_id, '') team_id,
            l1.date_start,
            l1.date_end
            FROM contacts 
              INNER JOIN meetings_contacts l1_1 ON contacts.id = l1_1.contact_id AND l1_1.deleted = 0
              INNER JOIN meetings l1 ON l1.id = l1_1.meeting_id AND l1.deleted = 0
              LEFT JOIN c_attendance l2 ON contacts.id = l2.student_id AND l2.deleted = 0 AND l1.id = l2.meeting_id
              INNER JOIN j_class l3 ON l1.ju_class_id = l3.id AND l3.deleted = 0
              INNER JOIN team_sets_teams l4 ON l1.team_set_id = l4.team_set_id AND l4.team_id = '{$bean->team_id}'
              LEFT JOIN c_rooms l5 ON l1.room_id = l5.id AND l5.deleted = 0
            WHERE (((contacts.id = '{$bean->student_id}')
                AND (l1.meeting_type = 'Session') AND (l1.session_status <> 'Cancelled')
                AND (l1.date_start >= '{$time_bot}' AND l1.date_start <= '{$time_top}')))
                AND contacts.deleted = 0";
            $row = $GLOBALS['db']->fetchArray($q);
            if(!empty($row)){
                foreach ($row as $r){
                   $is_exist = 0;
                    if(empty($r['att_id'])){
                        $attendance = BeanFactory::getBean('C_Attendance');
                    } else {
                        $attendance = BeanFactory::getBean('C_Attendance', $r['att_id']);
                        $is_exist = 1;
                    }
                    //In case student does not check out at morning we will check out for it
                    if($is_exist == 1 && empty($attendance->check_out_time)
                        && (strtotime($r['date_start']) < strtotime($compare) && strtotime($r['date_start']) > strtotime($time_bot)) ){
                        $GLOBALS['db']->query("UPDATE c_attendance SET check_out_time = '{$bean->time_check}', check_out_id = '{$bean->id}' WHERE id = '{$attendance->id}'");
                    }
                    $attendance->student_id = $bean->student_id;
                    $attendance->meeting_id = $r['meeting_id'];
                    $attendance->date_input = date('Y-m-d', strtotime($bean->time_check));
                    $attendance->class_id = $r['class_id'];
                    $attendance->student_type = 'Contacts';
                    if(empty($attendance->attendance_type)){
                        $attendance->attendance_type = 'P';
                    }
                    if(empty($attendance->check_in_id)){
                        $attendance->check_in_id = $bean->id;
                        $attendance->check_in_time = $bean->time_check;
//                        $check_in = 1;
                    } else {
                        $attendance->check_out_id = $bean->id;
                        $attendance->check_out_time = $bean->time_check;
//                        $check_out = 1;
                    }
                    //If the session and time is match we will save
                    if(($session == 0 && strtotime($r['date_start']) < strtotime($compare) && strtotime($r['date_start']) > strtotime($time_bot))
                        || ($session == 1 && strtotime($r['date_start']) > strtotime($compare) && strtotime($r['date_start']) < strtotime($time_top))) {
                        $attendance->save();
                    }
                    $data['startEnd'] = $timedate->to_display_time($r['date_start']). ' - ' . $timedate->to_display_time($r['date_end']);
                    $data['room_name'] = $r['room_name'];
                    if((intVal($bean->name) % 2) == 0){
                        $check_in = 1;
                    } else {
                        $check_out = 1;
                    }
                }
                //Match the class
                $bean->verify_status = 1;
            } else {
                //Not match the class
                $bean->verify_status = 0;
                $data['background'] = 3;
            }
            $this->prepareData($data ,array('student_id' => $bean->student_id, 'check_in' => $check_in, 'check_out' => $check_out));
        }
        //Send socket for realtime load
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://socket.dotb.cloud/send-event-phenikaa',
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode(array(
                'to' => 'LoadAttendance',
                'data' => $data
            )),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ));
        curl_exec($curl);
        curl_close($curl);
    }
    public function deletePicture($bean)
    {
        $destination ='/FaceID/CheckInOutImage/'.$bean->pic_id;
        $upload_file = new UploadFile();
        $upload_file->unlink_file($destination);
    }
    public function HandleHyperLink($bean){
        switch ($bean->verify_status) {
            case 1:
                $colorClass = "label-success";
                break;
            case 0:
                $colorClass = "label-important";
                break;
            default:
                $colorClass = "";
        }
        $bean->verify_status = "<div class='label ellipsis_inline $colorClass'>".$GLOBALS['app_list_strings']['checkinout_verify_result'][$bean->verify_status]."</div>";
    }
    public function prepareData(&$data, $args)
    {
        require_once('custom/modules/C_Attendance/_helper.php');

        global $timedate, $db;
        $now = $timedate->nowDb();

        $qStudent = "SELECT DISTINCT IFNULL(l1.id,'') l1_id,
            CONCAT(IFNULL(l1.last_name,''),' ',IFNULL(l1.first_name,'')) student_name,
            IFNULL(l1.picture,'') picture,
            l1.birthdate birthdate,
            IFNULL(l1.contact_id, 0) student_code,
            IFNULL(l1.guardian1,'') guardian1,
            IFNULL(l1.guardian_type_1,'') guardian_type_1,
            IFNULL(l2.picture,'') avatar_guardian_1,
            IFNULL(l1.guardian2,'') guardian2,
            IFNULL(l1.guardian_type_2,'') guardian_type_2,
            IFNULL(l3.picture,'') avatar_guardian_2,
            IFNULL(l1.guardian3,'') guardian3,
            IFNULL(l1.guardian_type_3,'') guardian_type_3,
            IFNULL(l4.picture,'') avatar_guardian_3
            FROM contacts l1
            LEFT JOIN j_membership l2 ON l1.guardian1 = l2.id AND l2.deleted=0
            LEFT JOIN j_membership l3 ON l1.guardian2 = l3.id AND l3.deleted=0
            LEFT JOIN j_membership l4 ON l1.guardian3 = l4.id AND l4.deleted=0
            WHERE l1.id='{$args['student_id']}'
            AND l1.deleted=0";
        $rs1 = $GLOBALS['db']->query($qStudent);
        $rowStudent = $GLOBALS['db']->fetchByAssoc($rs1);

        $picture_url = resizePicture('Contacts', $rowStudent['l1_id'], $rowStudent['picture']);
        if (empty($picture_url)) $picture_url = 'custom/images/no_avatar.png';

        $data['student_avatar'] = $picture_url;
        $data['student_name'] = $rowStudent['student_name'];
        $data['student_code'] = $rowStudent['student_code'];
        $data['birthdate'] = $timedate->to_display_date($rowStudent['birthdate']);
        $data['guardian1'] = replaceGuardianType($rowStudent['guardian_type_1']);
        $data['avatar_guardian_1'] = resizePicture('J_Membership', $rowStudent['guardian1'], $rowStudent['avatar_guardian_1']);
        $data['guardian2'] = replaceGuardianType($rowStudent['guardian_type_2']);
        $data['avatar_guardian_2'] = resizePicture('J_Membership', $rowStudent['guardian2'], $rowStudent['avatar_guardian_2']);
        $data['guardian3'] = replaceGuardianType($rowStudent['guardian_type_3']);
        $data['avatar_guardian_3'] = resizePicture('J_Membership', $rowStudent['guardian3'], $rowStudent['avatar_guardian_3']);

        if ($args['check_in']) {
            $data['background'] = 1;
        } elseif ($args['check_out']) {
            $data['background'] = 2;
        }
        //gan thong tin background
        switch ($data['background']) {
            case 1:
                $data['background'] = 'bg-junior_check_in';
                $data['header'] = '' .
                    '<div class="col-12">' .
                    '                                                <h1 class="w-title">Welcome to DOTB!</h1><br/>' .
                    '                                                <h1 class="w-title-2">Wish you an awesome day!</h1>' .
                    '                                                <p class="w-content">' .
                    '                                                    Chào mừng Bé đã đến DOTB.<br/>' .
                    '                                                    Chúc Bé một buổi học thật tuyệt vời nhé!' .
                    '                                                </p>' .
                    '                                            </div>';
                break;
            case 2:
                $data['background'] = 'bg-junior_check_out';
                $data['header'] = '' .
                    ' <div class="col-12">' .
                    '                                                <h1 class="w-title-3">' .
                    '                                                    Bye Bye!<br/>' .
                    '                                                    See you next time!' .
                    '                                                </h1>' .
                    '                                                <p class="w-content">' .
                    '                                                    Chào Bé! Hẹn gặp lại trong buổi học sau nhé!' .
                    '                                                </p>' .
                    '                                            </div>';
                break;
            case 3:
                $data['background'] = 'bg-junior_check_in_wrong';
                $data['header'] = '' .
                    '<div class="col-12">' .
                    '                                                <h1 class="w-title-4">' .
                    '                                                    Oh no! You have no class today!<br/>' .
                    '                                                    Please come to your teachers for instructions.' .
                    '                                                </h1>' .
                    '                                                <p class="w-content">' .
                    '                                                    Tiếc quá! Bé chưa có lớp học hôm nay rồi!<br/>' .
                    '                                                    Đến chỗ các thầy cô để được hướng dẫn nhé!' .
                    '                                                </p>' .
                    '                                            </div>';
                break;
            default:
                break;
        }
    }
}