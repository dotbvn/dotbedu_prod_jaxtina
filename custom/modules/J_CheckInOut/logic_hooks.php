<?php
$hook_version = 1;
$hook_array = Array();
$hook_array['before_save'] = Array();
$hook_array['before_save'][] = Array(0, 'Check Api Logs', 'custom/modules/J_CheckInOut/logicCheckInOut.php', 'logicCheckInOut', 'checkapi');

$hook_array['process_record'] = Array();
$hook_array['process_record'][] = Array(0, 'Process Record', 'custom/modules/J_CheckInOut/logicCheckInOut.php', 'logicCheckInOut', 'HandleHyperLink');

$hook_array['before_delete'] = Array();
$hook_array['before_delete'][] = Array(0, 'Handle Delete', 'custom/modules/J_CheckInOut/logicCheckInOut.php', 'logicCheckInOut', 'deletePicture');

$hook_array['after_retrieve'] = Array();
$hook_array['after_retrieve'][] = Array(0, 'Retrieve Record', 'custom/modules/J_CheckInOut/logicCheckInOut.php', 'logicCheckInOut', 'HandleHyperLink');
