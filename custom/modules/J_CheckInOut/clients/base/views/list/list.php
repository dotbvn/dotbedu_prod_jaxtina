<?php
$module_name = 'J_CheckInOut';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_RECORD_ID',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
                1 =>
                array (
                    'name' => 'student_name',
                    'label' => 'LBL_STUDENT_NAME',
                ),
                2 =>
                array (
                    'name' => 'time_check',
                    'label' => 'LBL_TIME'
                ),
                3 =>
                array (
                    'name' => 'verify_status',
                    'label' => 'LBL_VERIFY_STATUS',
                    'type' => 'html'
                ),
                4 =>
                array (
                    'name' => 'similarity_percent',
                    'label' => 'LBL_SIMILARITY_PERCENT'
                ),
                5 =>
                array (
                    'name' => 'device_name',
                    'label' => 'LBL_DEVICE_NAME',
                ),
                6 =>
                array (
                    'name' => 'team_name',
                    'label' => 'LBL_TEAM',
                    'default' => true,
                    'enabled' => true,
                ),

            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
