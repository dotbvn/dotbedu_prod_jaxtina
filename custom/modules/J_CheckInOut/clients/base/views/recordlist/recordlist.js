
({
    extendsFrom: 'RecordlistView',

    initialize: function (options) {
        this._super('initialize', [options]);
        var self = this
        if(self.action === 'list'){
            var socketURL = "https://socket.dotb.cloud/";
            var socket = io.connect(socketURL, {"path": "", "transports": ["websocket"], "reconnection": true});
            socket.on('connect', function () {
                console.log('Socket server is live!');
                socket.emit('join', App.config.uniqueKey + 'LoadAttendance')

            });
            socket.on('error', function () {
                console.log('Cannot connect to socket server!')
            })
            socket.on('event-phenikaa', function (msg) {
                if (self.context) {
                    console.log(msg)

                    setTimeout(function () {
                        self.context.reloadData({
                            recursive: false,
                            error: function () {
                                app.alert.show('server-error', {
                                    level: 'error',
                                    messages: 'ERR_GENERIC_SERVER_ERROR'
                                });
                            }
                        });
                    }, 1000);
                }
            });

        }

    },
})