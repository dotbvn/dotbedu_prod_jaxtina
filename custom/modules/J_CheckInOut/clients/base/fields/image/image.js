
({
    extendsFrom: 'BaseImageField',


    initialize: function(options) {
        this._super('initialize', [options]);
        // if this image exists in the Quotes QLI quote data section, force it
        // to use a detail template and don't allow the image field to be editable
        if (this.view.module === 'J_CheckInOut') {
            this.action = 'detail';
            this.options.viewName = 'detail';
            this.def.width = 200;
            this.def.height = 200;
        }
    },
    /**
     * @override
     * @param value
     * @return value
     */
    format: function (value) {
        if(this.module === 'J_CheckInOut'){
            var str = window.location.pathname
            if (str.slice(-1) === '/') {
                str = str.slice(0, -1);
            }
            var site = window.location.origin + str
            value = site + '/upload/FaceID/CheckInOutImage/' + this.model.attributes.pic_id;
        }
        return value;
    },
})
