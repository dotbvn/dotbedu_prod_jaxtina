<?php
$module_name = 'J_TeacherQE';
$thisClassId = $_REQUEST['j_teacherqe_j_classj_class_ida'];
if(empty($thisClassId)) $thisClassId =  $_REQUEST['j_class_id'];
if(empty($thisClassId)) $thisClassId =  $this->bean->j_teacherqe_j_classj_class_ida;
if(empty($thisClassId)) die('Pls, Create TeacherQE From A Class!');
$thisClass = BeanFactory::getBean("J_Class", $thisClassId);
$count_times = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM j_teacherqe_j_class_c WHERE j_teacherqe_j_classj_class_ida ='$thisClass->id' AND deleted = 0");
if(empty($count_times)) $count_times = 0;
$count_times += 1;

$this->ss->assign('class_name', $thisClass->name);
$this->ss->assign('class_id', $thisClass->id);
$this->ss->assign('count_times', $count_times);
$viewdefs[$module_name] =
array (
    'QuickCreate' =>
    array (
        'templateMeta' =>
        array (
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'DEFAULT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
        ),
        'panels' =>
        array (
            'default' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'name',
                        'label' => 'LBL_NAME',
                        ),
                    1 =>
                    array (
                        'name' => 'class_observation_date',
                        'label' => 'LBL_CLASS_OBSERVATION_DATE',
                        ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'j_teacherqe_j_class_name',
                        'label' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_CLASS_TITLE',
                        'customCode' => '<input type="text" name="j_teacherqe_j_class_name" class="input_readonly" id="j_teacherqe_j_class_name" value="{$class_name}" readonly>
                        <input type="hidden" name="j_teacherqe_j_classj_class_ida" id="j_teacherqe_j_classj_class_ida" value="{$class_id}">',
                        ),
                    1 =>
                    array (
                        'name' => 'j_teacherqe_c_teachers_name',
                        'label' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_C_TEACHERS_TITLE',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'times',
                        'label' => 'LBL_TIMES',
                        'customCode' => '<input type="number" name="times" id="times" value="{$count_times}" >',
                    ),
                    1 =>
                    array (
                        'name' => 'teacher_mark',
                        'label' => 'LBL_TEACHER_MARK',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'student_comment',
                        'label' => 'LBL_STUDENT_COMMENT',
                    ),
                    1 =>
                    array (
                        'name' => 'teacher_comment',
                        'label' => 'LBL_TEACHER_COMMENT',
                    ),
                ),
                4 =>
                array (
                    0 => 'description',
                    1 => 'assigned_user_name',
                ),
            ),
        ),
    ),
);
