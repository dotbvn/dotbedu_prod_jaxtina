<?php
$module_name = 'J_TeacherQE';
$viewdefs[$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'j_teacherqe_c_teachers_name',
            'label' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_C_TEACHERS_TITLE',
          ),
          1 => 
          array (
            'name' => 'j_teacherqe_j_class_name',
            'label' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_CLASS_TITLE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'times',
            'label' => 'LBL_TIMES',
          ),
          1 => 
          array (
            'name' => 'class_observation_date',
            'label' => 'LBL_CLASS_OBSERVATION_DATE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'teacher_mark',
            'label' => 'LBL_TEACHER_MARK',
          ),
          1 => 'description',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'teacher_comment',
            'label' => 'LBL_TEACHER_COMMENT',
          ),
          1 => 
          array (
            'name' => 'student_comment',
            'label' => 'LBL_STUDENT_COMMENT',
          ),
        ),
        5 => 
        array (
          0 => 'team_name',
          1 => 'assigned_user_name',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'created_by_name',
            'readonly' => true,
            'label' => 'LBL_CREATED',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'date_modified',
            'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
            'label' => 'LBL_DATE_MODIFIED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'readonly' => true,
            'label' => 'LBL_MODIFIED',
          ),
        ),
      ),
    ),
  ),
);
