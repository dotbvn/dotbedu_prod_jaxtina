<?php
// created: 2020-10-01 10:21:39
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => 10,
    'default' => true,
  ),
  'times' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_TIMES',
    'width' => 10,
    'default' => true,
  ),
  'j_teacherqe_c_teachers_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_C_TEACHERS_TITLE',
    'id' => 'J_TEACHERQE_C_TEACHERSC_TEACHERS_IDA',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'C_Teachers',
    'target_record_key' => 'j_teacherqe_c_teachersc_teachers_ida',
  ),
  'class_observation_date' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_CLASS_OBSERVATION_DATE',
    'width' => 10,
    'default' => true,
  ),
  'student_comment' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_STUDENT_COMMENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'teacher_comment' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_TEACHER_COMMENT',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'teacher_mark' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_TEACHER_MARK',
    'width' => 10,
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => 10,
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_TO',
    'id' => 'ASSIGNED_USER_ID',
    'width' => 10,
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'date_modified' => 
  array (
    'type' => 'datetime',
    'studio' => 
    array (
      'portaleditview' => false,
    ),
    'readonly' => true,
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => 10,
    'default' => true,
  ),
);