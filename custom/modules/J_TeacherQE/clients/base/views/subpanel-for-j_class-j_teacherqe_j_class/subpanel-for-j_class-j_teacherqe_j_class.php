<?php
// created: 2020-10-01 10:21:39
$viewdefs['J_TeacherQE']['base']['view']['subpanel-for-j_class-j_teacherqe_j_class'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'times',
          'label' => 'LBL_TIMES',
          'enabled' => true,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'j_teacherqe_c_teachers_name',
          'label' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_C_TEACHERS_TITLE',
          'enabled' => true,
          'id' => 'J_TEACHERQE_C_TEACHERSC_TEACHERS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'class_observation_date',
          'label' => 'LBL_CLASS_OBSERVATION_DATE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'student_comment',
          'label' => 'LBL_STUDENT_COMMENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'teacher_comment',
          'label' => 'LBL_TEACHER_COMMENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'teacher_mark',
          'label' => 'LBL_TEACHER_MARK',
          'enabled' => true,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        9 => 
        array (
          'name' => 'date_modified',
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'readonly' => true,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);