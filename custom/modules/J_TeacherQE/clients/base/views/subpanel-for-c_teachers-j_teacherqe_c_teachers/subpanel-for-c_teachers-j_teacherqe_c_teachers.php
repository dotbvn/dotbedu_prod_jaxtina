<?php
// created: 2020-10-01 11:12:25
$viewdefs['J_TeacherQE']['base']['view']['subpanel-for-c_teachers-j_teacherqe_c_teachers'] = array (
  'panels' => 
  array (
    0 => 
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' => 
      array (
        0 => 
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 => 
        array (
          'name' => 'j_teacherqe_j_class_name',
          'label' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_CLASS_TITLE',
          'enabled' => true,
          'id' => 'J_TEACHERQE_J_CLASSJ_CLASS_IDA',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 => 
        array (
          'name' => 'times',
          'label' => 'LBL_TIMES',
          'enabled' => true,
          'default' => true,
        ),
        3 => 
        array (
          'name' => 'class_observation_date',
          'label' => 'LBL_CLASS_OBSERVATION_DATE',
          'enabled' => true,
          'default' => true,
        ),
        4 => 
        array (
          'name' => 'teacher_mark',
          'label' => 'LBL_TEACHER_MARK',
          'enabled' => true,
          'default' => true,
        ),
        5 => 
        array (
          'name' => 'teacher_comment',
          'label' => 'LBL_TEACHER_COMMENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        6 => 
        array (
          'name' => 'student_comment',
          'label' => 'LBL_STUDENT_COMMENT',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        7 => 
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
        8 => 
        array (
          'name' => 'assigned_user_name',
          'label' => 'LBL_ASSIGNED_TO',
          'enabled' => true,
          'id' => 'ASSIGNED_USER_ID',
          'link' => true,
          'default' => true,
        ),
        9 => 
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' => 
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);