<?php
$module_name = 'J_TeacherQE';
$viewdefs[$module_name] = 
array (
  'base' => 
  array (
    'view' => 
    array (
      'list' => 
      array (
        'panels' => 
        array (
          0 => 
          array (
            'label' => 'LBL_PANEL_1',
            'fields' => 
            array (
              0 => 
              array (
                'name' => 'name',
                'label' => 'LBL_NAME',
                'default' => true,
                'enabled' => true,
                'link' => true,
              ),
              1 => 
              array (
                'name' => 'times',
                'label' => 'LBL_TIMES',
                'enabled' => true,
                'default' => true,
              ),
              2 => 
              array (
                'name' => 'j_teacherqe_c_teachers_name',
                'label' => 'LBL_J_TEACHERQE_C_TEACHERS_FROM_C_TEACHERS_TITLE',
                'enabled' => true,
                'id' => 'J_TEACHERQE_C_TEACHERSC_TEACHERS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              3 => 
              array (
                'name' => 'j_teacherqe_j_class_name',
                'label' => 'LBL_J_TEACHERQE_J_CLASS_FROM_J_CLASS_TITLE',
                'enabled' => true,
                'id' => 'J_TEACHERQE_J_CLASSJ_CLASS_IDA',
                'link' => true,
                'sortable' => false,
                'default' => true,
              ),
              4 => 
              array (
                'name' => 'class_observation_date',
                'label' => 'LBL_CLASS_OBSERVATION_DATE',
                'enabled' => true,
                'default' => true,
              ),
              5 => 
              array (
                'name' => 'teacher_mark',
                'label' => 'LBL_TEACHER_MARK',
                'enabled' => true,
                'default' => true,
              ),
              6 => 
              array (
                'name' => 'team_name',
                'label' => 'LBL_TEAM',
                'default' => true,
                'enabled' => true,
              ),
              7 => 
              array (
                'name' => 'date_entered',
                'enabled' => true,
                'default' => true,
              ),
              8 => 
              array (
                'name' => 'date_modified',
                'enabled' => true,
                'default' => false,
              ),
              9 => 
              array (
                'name' => 'student_comment',
                'label' => 'LBL_STUDENT_COMMENT',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              10 => 
              array (
                'name' => 'teacher_comment',
                'label' => 'LBL_TEACHER_COMMENT',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              11 => 
              array (
                'name' => 'description',
                'label' => 'LBL_DESCRIPTION',
                'enabled' => true,
                'sortable' => false,
                'default' => false,
              ),
              12 => 
              array (
                'name' => 'assigned_user_name',
                'label' => 'LBL_ASSIGNED_TO_NAME',
                'default' => false,
                'enabled' => true,
                'link' => true,
              ),
            ),
          ),
        ),
        'orderBy' => 
        array (
          'field' => 'date_modified',
          'direction' => 'desc',
        ),
      ),
    ),
  ),
);
