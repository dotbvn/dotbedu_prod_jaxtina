<?php
$module_name = 'J_Voucher';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_1',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'name',
                                'label' => 'LBL_NAME',
                                'default' => true,
                                'enabled' => true,
                                'link' => true,
                            ),
                            1 =>
                            array (
                                'name' => 'foc_type',
                                'label' => 'LBL_FOC_TYPE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            2 =>
                            array (
                                'name' => 'status',
                                'label' => 'LBL_STATUS',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'event-status',
                            ),
                            3 =>
                            array (
                                'name' => 'start_date',
                                'label' => 'LBL_START_DATE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            4 =>
                            array (
                                'name' => 'end_date',
                                'label' => 'LBL_END_DATE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            5 =>
                            array (
                                'name' => 'priority',
                                'label' => 'LBL_PRIORITY',
                                'enabled' => true,
                                'default' => true,
                            ),
                            6 =>
                            array (
                                'name' => 'use_time',
                                'label' => 'LBL_USE_TIME',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            7 =>
                            array (
                                'name' => 'team_name',
                                'enabled' => true,
                                'default' => false,
                            ),
                            8 =>
                            array (
                                'name' => 'date_modified',
                                'enabled' => true,
                                'default' => false,
                            ),
                            9 =>
                            array (
                                'name' => 'used_time',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
                'orderBy' =>
                array (
                    'field' => 'date_modified',
                    'direction' => 'desc',
                ),
            ),
        ),
    ),
);
