<?php
$popupMeta = array (
    'moduleMain' => 'J_Voucher',
    'varName' => 'J_Voucher',
    'orderBy' => 'j_voucher.name',
    'whereClauses' => array (
  'name' => 'j_voucher.name',
  'status' => 'j_voucher.status',
  'foc_type' => 'j_voucher.foc_type',
  'team_name' => 'j_voucher.team_name',
  'use_time' => 'j_voucher.use_time',
  'used_time' => 'j_voucher.used_time',
),
    'searchInputs' => array (
  1 => 'name',
  2 => 'status',
  3 => 'foc_type',
  4 => 'team_name',
  5 => 'use_time',
  6 => 'used_time',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10',
  ),
  'team_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'studio' => 
    array (
      'portallistview' => false,
      'portaldetailview' => false,
      'portaleditview' => false,
    ),
    'label' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => '10',
    'name' => 'team_name',
  ),
  'use_time' => 
  array (
    'type' => 'varchar',
    'studio' => 'visible',
    'label' => 'LBL_USE_TIME',
    'width' => 10,
    'name' => 'use_time',
  ),
  'used_time' => 
  array (
    'type' => 'int',
    'label' => 'LBL_USED_TIME',
    'width' => 10,
    'name' => 'used_time',
  ),
  'status' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => '10',
    'name' => 'status',
  ),
  'foc_type' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_FOC_TYPE',
    'width' => '10',
    'name' => 'foc_type',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'STATUS' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_STATUS',
    'width' => 10,
    'name' => 'status',
  ),
  'FOC_TYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_FOC_TYPE',
    'width' => 10,
    'name' => 'foc_type',
  ),
  'START_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_START_DATE',
    'width' => 10,
    'default' => true,
    'name' => 'start_date',
  ),
  'END_DATE' => 
  array (
    'type' => 'date',
    'label' => 'LBL_END_DATE',
    'width' => 10,
    'default' => true,
    'name' => 'end_date',
  ),
  'USED_TIME' => 
  array (
    'type' => 'int',
    'default' => true,
    'label' => 'LBL_USED_TIME',
    'width' => 10,
    'name' => 'used_time',
  ),
  'TEAM_NAME' => 
  array (
    'width' => 10,
    'label' => 'LBL_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
),
);
