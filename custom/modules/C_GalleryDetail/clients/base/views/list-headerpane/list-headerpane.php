<?php


$viewdefs['C_GalleryDetail']['base']['view']['list-headerpane'] = array(

    'title' => 'LBL_C_GALLERY_DETAIL',
    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_C_GALLERY_DETAIL',
			'tooltip' => 'LNK_NEW_C_GALLERY_DETAIL',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'C_GalleryDetail',
            'route'=>'#C_GalleryDetail/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_C_GALLERY_DETAIL_REPORTS',
			'tooltip' => 'LNK_C_GALLERY_DETAIL_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=C_GalleryDetail',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);