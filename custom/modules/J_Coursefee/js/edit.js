$(document).ready(function(){

    $('#kind_of_course').select2({width: '300px'});
    $('#status').select2({width: '150px'});
    $('#expired_term').select2({width: '150px'});
    $('#type').select2();
    $('#bonus_course').select2();

    toggleCourseFee();
    $('select#type').on('change',function(){
        $('#type_of_course_fee').val('');
        toggleCourseFee();
    });

    $('#is_accumulate').live('change',function(){
        if(!$(this).is(':checked')) $('#accumulate_percent').val('');
        if($('#is_accumulate').is(':checked'))
            addToValidate('EditView', 'accumulate_percent', 'enum', true, DOTB.language.get('J_Coursefee', 'LBL_ACCUMULATE_PERCENT'));
        else removeFromValidate('EditView', 'accumulate_percent');
    });

    $('#tblbook').multifield({
        section :   '.row_tpl', // First element is template
        addTo   :   '#tbodybook', // Append new section to position
        btnAdd  :   '#btnAddrow', // Button Add id
        btnRemove:  '.btnRemove', // Buton remove id
    });

    $('#tbl_duration_exp').multifield({
        section :   '.duration_row', // First element is template
        addTo   :   '#tbody_duration_exp', // Append new section to position
        btnAdd  :   '#btn_duration_add', // Button Add id
        btnRemove:  '.btn_duration_remove', // Buton remove id
        prompt:  false, // prompt remove
    });
    $('.book_id').live('change',function(){
        if($(this).val() == 'full-set'){
            var arrSet = [];
            $(this).find(":selected").closest('optgroup').find('option').each(function () {
                if($(this).val() != 'full-set')
                    arrSet.push($(this).val());
            });
            //Xu ly add row
            var countRow = $('select.book_id').length - 1;
            var rowEq    = $(this).closest('tr').index();
            var remainRow= (countRow - rowEq) + 1;
            var countArrSet = arrSet.length;
            if(countArrSet > remainRow)
                for (i = 0; i < (countArrSet - remainRow); i++)
                    $('#btnAddrow').trigger('click');
            $(this).val('');//Clear option
            var startAdd = rowEq;
            $.each(arrSet, function( index, value ){
                $('select.book_id:eq('+startAdd+')').val(value);
                $('input.book_quantity:eq('+startAdd+')').val('1');
                startAdd++;
            });
        }
        $('input.book_quantity').each(function() {
            if($(this).val() == '')
                $(this).val('1');
        });
        calBookPayment();
    });
    $('.book_quantity, .book_amount, .book_quantity').live('change',function(){
        calBookPayment();
    });

    $('#unit_price').on('change',function(){
        var val = Numeric.parse($(this).val());
        if(Number.isInteger(val) && val > 0)
            $(this).val(Numeric.toFloat(val));
        else if(val < 0){
            toastr.error(app.lang.get('LBL_CORRECT_FORMAT', 'J_Kindofcourse'));
            $(this).val('').effect("highlight", {color: '#FF0000'}, 2000);
        }else $(this).val(Numeric.toFloat(val,6,6));
    });
    $('#type_of_course_fee').on('change',function(){
        var val = Numeric.parse($(this).val());
        if(Number.isInteger(val) && val > 0)
            $(this).val(Numeric.toFloat(val));
        else if(val == 0){
            toastr.error(app.lang.get('LBL_CORRECT_FORMAT', 'J_Kindofcourse'));
            $(this).val('').effect("highlight", {color: '#FF0000'}, 2000);
        }else $(this).val(Numeric.toFloat(val,6,6));
    });

    //caculate ext content amount
    $('.ext_quantity_1, .ext_price_1').live('change', function(){
        calExtContentAmount('ext_content_1');
    });
    $('.ext_quantity_2, .ext_price_2').live('change', function(){
        calExtContentAmount('ext_content_2');
    });
});
function toggleCourseFee(){
    var type = $('select#type').val();
    //Hide Some row
    if(type == 'Hours'){
        var unit_label = DOTB.language.languages['app_list_strings']['unit_coursefee_list']['Hour/Month'];
        $(".duration_exp_td").hide();
        $('#fee_hours_label').text('');
        $('#per_hour_label').text('');
        $("#tbody_duration_exp").find("tr:gt(1)").remove();
        $("#btn_duration_add, .btn_duration_remove").hide();
        $('.duration_exp_label').text(unit_label);
    }else{
        var unit_label = DOTB.language.languages['app_list_strings']['unit_coursefee_list'][type];
        $(".duration_exp_td").show();
        var fee_hours_label = DOTB.language.get('J_Coursefee', 'LBL_PER')+' '+unit_label;
        var per_hour_label = DOTB.language.get('J_Coursefee', 'LBL_PER_HOUR');
        $('.duration_exp_label').text(unit_label);
        $('#fee_hours_label').text(fee_hours_label);
        $('#per_hour_label').text(per_hour_label);
        $("#btn_duration_add, .btn_duration_remove").show();
    }
}

function calBookPayment(){
    var total_pay = 0;
    $('#tblbook tbody tr').each(function(index, brand){
        var book_price      = Numeric.parse($(this).find('select.book_id option:selected').attr('price'));
        var book_unit       = $(this).find('select.book_id option:selected').attr('unit');
        var book_quantity   = parseInt($(this).find('.book_quantity').val());
        $(this).find('.book_price').val(Numeric.toInt(book_price));
        //add free book default
        var book_amount     =  book_price * book_quantity;
        if(book_amount == '' || isNaN(book_amount)) book_amount = 0
        $(this).find('.book_amount').val(Numeric.toInt(book_amount));
        $(this).find('.book_unit').text(book_unit);
        total_pay           = total_pay + book_amount;
    });
    $('#total_book_pay').val(Numeric.toInt(total_pay));
}

function calExtContentAmount(type){
    if(type == 'ext_content_1'){
        $('.ext_amount_1').val(Numeric.toFloat(Numeric.parse($('.ext_quantity_1').val()) * Numeric.parse($('.ext_price_1').val())));
    } else $('.ext_amount_2').val(Numeric.toFloat(Numeric.parse($('.ext_quantity_2').val()) * Numeric.parse($('.ext_price_2').val())));
}