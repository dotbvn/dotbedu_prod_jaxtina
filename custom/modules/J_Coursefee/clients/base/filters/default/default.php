<?php
// created: 2022-08-31 16:27:57
$viewdefs['J_Coursefee']['base']['filter']['default'] = array (
  'default_filter' => 'all_records',
  'fields' =>
  array (
    'name' =>
    array (
    ),
    'status' =>
    array (
    ),
    'type' =>
    array (
    ),
    'type_of_course_fee' =>
    array (
    ),
    'taxrate_name' =>
    array (
    ),
    'apply_date' =>
    array (
    ),
    'inactive_date' =>
    array (
    ),
    'unit_price' =>
    array (
    ),
    'kind_of_course' =>
    array (
    ),
    'is_external_accumulate' =>
    array (
    ),
    'accumulate_percent' =>
    array (
    ),
    'product_code' =>
    array (
    ),
    'duration_exp' =>
    array (
    ),
    'expired_term' =>
    array (
    ),
    'ext_name' =>
    array (
    ),
    'ext_unit' =>
    array (
    ),
    'team_name' =>
    array (
    ),
    'date_modified' =>
    array (
    ),
    'is_accumulate' =>
    array (
    ),
    'is_custom_hours' =>
    array (
    ),
     'group_unit_name' =>
    array (
    ),
    '$owner' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_CURRENT_USER_FILTER',
    ),
    '$favorite' =>
    array (
      'predefined_filter' => true,
      'vname' => 'LBL_FAVORITES_FILTER',
    ),
    'created_by_name' =>
    array (
    ),
    'modified_by_name' =>
    array (
    ),
  ),
);