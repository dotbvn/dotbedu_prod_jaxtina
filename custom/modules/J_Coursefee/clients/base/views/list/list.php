<?php
$module_name = 'J_Coursefee';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_1',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'name',
                                'label' => 'LBL_NAME',
                                'default' => true,
                                'enabled' => true,
                                'link' => true,
                                'width' => 'xlarge',
                            ),
                            1 =>
                            array (
                                'name' => 'status',
                                'label' => 'LBL_STATUS',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            2 =>
                            array (
                                'name' => 'type',
                                'label' => 'LBL_TYPE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            3 =>
                            array (
                                'name' => 'type_of_course_fee',
                                'label' => 'LBL_COURSE_FEE_HOUR',
                                'enabled' => true,
                                'default' => true,
                            ),
                            4 =>
                            array (
                                'name' => 'unit_price',
                                'label' => 'LBL_UNIT_PRICE',
                                'enabled' => true,
                                'currency_format' => true,
                                'default' => true,
                            ),
                            5 =>
                            array (
                                'name' => 'apply_date',
                                'label' => 'LBL_APPLY_DATE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            6 =>
                            array (
                                'name' => 'kind_of_course',
                                'label' => 'LBL_KIND_OF_COURSE',
                                'enabled' => true,
                                'default' => true,
                            ),
                            7 =>
                            array (
                                'name' => 'team_name',
                                'label' => 'LBL_TEAM',
                                'default' => true,
                                'enabled' => true,
                            ),
                            8 =>
                            array (
                                'name' => 'group_unit_name_text',
                                'label' => 'LBL_GROUP_UNIT_NAME',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            9 =>
                            array (
                                'name' => 'date_modified',
                                'enabled' => true,
                                'default' => true,
                            ),
                            10 =>
                            array (
                                'name' => 'is_accumulate',
                                'label' => 'LBL_IS_ACCUMULATE',
                                'enabled' => true,
                                'default' => false,
                            ),
                            11 =>
                            array (
                                'name' => 'group_unit_id',
                                'enabled' => true,
                                'default' => false,
                            ),
                            11 =>
                            array (
                                'name' => 'group_unit_name',
                                'enabled' => true,
                                'default' => false,
                            ),
                            13 =>
                            array (
                                'name' => 'expired_term',
                                'label' => 'LBL_EXPIRED_TERM',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
                'orderBy' =>
                array (
                    'field' => 'date_modified',
                    'direction' => 'desc',
                ),
            ),
        ),
    ),
);
