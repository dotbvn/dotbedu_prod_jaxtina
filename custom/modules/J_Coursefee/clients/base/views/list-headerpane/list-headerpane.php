<?php


$viewdefs['J_Coursefee']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

        array(

            
            'label' => 'LNK_NEW_J_COURSEFEE',
			'tooltip' => 'LNK_NEW_J_COURSEFEE',
            'acl_action' => 'create',
            'type' => 'button',
            'acl_module' => 'J_Coursefee',
            'route'=>'#J_Coursefee/create',
            'icon' => 'fa-plus',
        ),
        array(

            
            'label' => 'LNK_J_COURSEFEE_REPORTS',
			'tooltip' => 'LNK_J_COURSEFEE_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_Coursefee',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);