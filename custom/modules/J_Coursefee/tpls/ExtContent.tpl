<table id="tblExtContent" width="70%" border="1" class="">
    <thead>
        <tr>
            <th width="20%"></th>
            <th width="40%" style="text-align: center;">{$MOD.LBL_NAME}<span class="required">*</span></th>
            <th width="10%" style="text-align: center;">{$MOD.LBL_UNIT}</th>
            <th width="10%" style="text-align: center;">{$MOD.LBL_QUANTITY}</th>
            <th width="10%" style="text-align: center;">{$MOD.LBL_PRICE}</th>
            <th width="10%" style="text-align: center;">{$MOD.LBL_AMOUNT}</th>
        </tr>
    </thead>
    <tbody id="tbodyExtContent">
        {$html_ext_content}
    </tbody>
</table>