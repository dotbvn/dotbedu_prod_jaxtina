<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicCoursefee {
    function handleSave(&$bean, $event, $arguments){
        $_POST['team_set_id']   = $bean->fetched_row['team_set_id'];
        if(!empty($_REQUEST['__dotb_url'])){
            $post = explode("/",$_REQUEST['__dotb_url']);
            $_POST['module'] = $post[1];
            $_POST['action'] = $post[2];
        }

        if($_POST['action'] != 'MassUpdate'){
            //ADD Ext Content
            $ext_content_1 = array(
                'name' => $_POST['ext_name_1'],
                'unit' => $_POST['ext_unit_1'],
                'quantity' => $_POST['ext_quantity_1'],
                'price' => unformat_number($_POST['ext_price_1']),
                'amount' => unformat_number($_POST['ext_amount_1']),
            );
            $ext_content_2 = array(
                'name' => $_POST['ext_name_2'],
                'unit' => $_POST['ext_unit_2'],
                'quantity' => $_POST['ext_quantity_2'],
                'price' => unformat_number($_POST['ext_price_2']),
                'amount' => unformat_number($_POST['ext_amount_2']),
            );
            $ext_content_1  = json_encode($ext_content_1);
            $ext_content_2  = json_encode($ext_content_2);
            if(html_entity_decode($bean->fetched_row['ext_content_1']) != $ext_content_1)
                $bean->ext_content_1 = $ext_content_1;
            if(html_entity_decode($bean->fetched_row['ext_content_2']) != $ext_content_2)
                $bean->ext_content_2 = $ext_content_2;

            //Save to json
            if($bean->type == 'Hours'){
                if(is_string($bean->duration_exp))
                    $bean->duration_exp = json_encode([$bean->duration_exp]);
                if(is_array($bean->duration_exp))
                    $bean->duration_exp = json_encode([$bean->duration_exp[1]]);
            }else
                $bean->duration_exp = json_encode(array_filter($bean->duration_exp, function($value) { return !is_null($value) && !empty($value) && $value > 0 ; }));
        }
    }
    function afterSave(&$bean, $event, $arguments){
        //Check MassUpdate by Lumia
        if(!empty($_REQUEST['__dotb_url'])){
            $post = explode("/",$_REQUEST['__dotb_url']);
            $_POST['module'] = $post[1];
            $_POST['action'] = $post[2];
        }

        if($_POST['module'] == $bean->module_name && ($_POST['action'] == 'Save' || $_POST['action'] == 'MassUpdate')){
            if($_POST['team_set_id'] != $bean->team_set_id){
                $team_list = array();
                // Get all team set
                $teamSetBean = new TeamSet();
                $teams = $teamSetBean->getTeams($bean->team_set_id);
                // Add all team set to  $team_list
                foreach ($teams as $key => $value) {
                    $team_list[] = $key;
                }
                // Add children of team set to $team_list
                foreach ($teams as $key => $value) {
                    // Get children of team
                    $q1 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$key}'";
                    $rs1 = $GLOBALS['db']->query($q1);

                    while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
                        if (!isset($teams[$row['id']])) $team_list[] = $row['id'];
                        $q2 = "SELECT id, name, parent_id FROM teams WHERE private <> 1 AND deleted = 0 AND parent_id = '{$row['id']}'";
                        $rs2 = $GLOBALS['db']->query($q2);
                        while($row2 = $GLOBALS['db']->fetchByAssoc($rs2))
                            if (!isset($teams[$row['id']])) $team_list[] = $row2['id'];
                    }
                }

                if(!empty($team_list)){
                    $bean->load_relationship('teams');
                    //Add the teams
                    $bean->teams->replace($team_list);
                }
            }
            //Save Book/Gift
            if(!empty($bean->fetched_row) && !empty($bean->id) && $_POST['action']!='MassUpdate')
                $GLOBALS['db']->query("DELETE FROM j_inventorydetail WHERE coursefee_id = '{$bean->id}' AND deleted = 0");

            // First element is null
            for ($i = 1; $i < count($_POST["book_id"]); $i++) {
                $bookId         = $_POST["book_id"][$i];
                $bookQuantity   = $_POST["book_quantity"][$i];
                $bookLevel      = $_POST["book_level"][$i];
                $bookPrice      = unformat_number($_POST["book_price"][$i]);
                $book_amount    = unformat_number($_POST["book_amount"][$i]);
                if (!empty($bookId)){
                    // Create Inventory Detail
                    $inventoryDetail = BeanFactory::newBean("J_Inventorydetail");
                    $inventoryDetail->book_id       = $bookId;
                    $inventoryDetail->coursefee_id  = $bean->id;
                    $inventoryDetail->quantity      = -1 * abs($bookQuantity);
                    $inventoryDetail->price         = $bookPrice;
                    $inventoryDetail->level         = $bookLevel;
                    $inventoryDetail->amount        = $book_amount;
                    $inventoryDetail->team_id       = $bean->team_id;
                    $inventoryDetail->team_set_id   = $bean->team_set_id;
                    $inventoryDetail->assigned_user_id = $bean->assigned_user_id;
                    $inventoryDetail->save();
                }
            }

        }


    }
    function listViewColor(&$bean, $event, $arguments){
        switch ($bean->status) {
            case "Active":
                $bean->status = '<span class="textbg_dream">'.$bean->status.'</span>';
                break;
            case "Inactive":
                $bean->status = '<span class="textbg_crimson">'.$bean->status.'</span>';
                break;
        }

        if(empty(!$bean->group_unit_id)){
            $name = $GLOBALS['db']->getOne("SELECT name FROM j_groupunit WHERE id = '{$bean->group_unit_id}'");
            $bean->group_unit_name_text = '<a data-original-title="'.$name.'" rel="tooltip" href="#J_GroupUnit/'.$bean->group_unit_id.'">'.$name.'</a>';
        }else  $bean->group_unit_name_text = '';

    }

}
?>