<?php
class J_CoursefeeViewDetail extends ViewDetail {

    function display() {
        $duration_exp = json_decode(html_entity_decode($this->bean->duration_exp), true);
        $unit_label = $GLOBALS['app_list_strings']['unit_coursefee_list'][$this->bean->type];
        if($this->bean->type != 'Hours')
            foreach($duration_exp as $exp)
                $html_exp .= '<li style="margin-left:5px;">'.$exp.' '.$unit_label.'</li>';
        $this->ss->assign('type_of_course_fee','<table width="70%" style="padding:0px!important;background-color: #fff;width: auto;"><tbody><tr colspan="2"><td style="vertical-align: middle;">'.format_number($this->bean->type_of_course_fee,6,6).(($this->bean->type != 'Hours') ?  ' '.translate('LBL_HOURS','J_Payment').'/' : ' ' ).$unit_label.' </td><td width="10%"></td><td style="vertical-align: middle;">'.$html_exp.'</td></tr></tbody></table>');


        $bhtml = getHtmlAddRow($this->bean);
        $this->ss->assign("bookList", $bhtml['html']);
        $this->ss->assign("total_book_amount", format_number($bhtml['total_amount']));
        $this->ss->assign("total_book_quantity", format_number($bhtml['total_quantity']));
        $this->ss->assign("extContentList", $bhtml['htmlExtContent']);

        if(empty($this->bean->taxrate_name)) $this->bean->taxrate_name = '-KCT-';

        $this->ss->assign("accumulate_percent", ' | '.$GLOBALS['app_list_strings']['accumulate_percent_list'][$this->bean->accumulate_percent]);
        parent::display();

    }
}

function getHtmlAddRow($bean){
    $q1 = "SELECT DISTINCT
    IFNULL(l3.id, '') book_id,
    IFNULL(l3.name, '') book_name,
    IFNULL(j_inventorydetail.id, '') primaryid,
    ABS(j_inventorydetail.quantity) quantity,
    IFNULL(j_inventorydetail.level, '') level,
    l3.unit unit,
    j_inventorydetail.price price,
    j_inventorydetail.amount amount
    FROM j_inventorydetail
    INNER JOIN j_coursefee l1 ON j_inventorydetail.coursefee_id = l1.id AND l1.deleted = 0
    INNER JOIN product_templates l3 ON j_inventorydetail.book_id = l3.id AND l3.deleted = 0
    WHERE (l1.id = '{$bean->id}') AND j_inventorydetail.deleted = 0";
    $rs1 = $GLOBALS['db']->query($q1);
    $tpl_addrow = '';
    while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
        $tpl_addrow .= "<tr class='row_tpl'>";
        $tpl_addrow .= '<td style="text-align: center;">'.$row['book_name'].'</td>';
        $tpl_addrow .= '<td style="text-align: center;">'.$GLOBALS['app_list_strings']['unit_ProductTemplates_list'][$row['unit']].'</td>';
        //        $tpl_addrow .= '<td style="text-align: center;">'.$row['level'].'</td>';
        $tpl_addrow .= '<td style="text-align: center;">'.$row['quantity'].'</td>';
        $tpl_addrow .= '<td nowrap style="text-align: center;">'.format_number($row['price']).'</td>';
        $tpl_addrow .= '<td nowrap style="text-align: center;">'.format_number($row['amount']).'</td>';
        $tpl_addrow .= '</tr>';
        $totalAmount += $row['amount'];
        $total_quantity += $row['quantity'];
    }

    //ADD ext content
    if(!empty($bean->ext_content_1)) {$ext_content_1 = json_decode(html_entity_decode($bean->ext_content_1));}
    if(!empty($bean->ext_content_2)) {$ext_content_2 = json_decode(html_entity_decode($bean->ext_content_2));}
    if(!empty($bean->ext_name)) $tpl_ext_content = '<tr>
        <td scope="col" style="text-align: center;">1</td>
        <td style="text-align: center;">'.$bean->ext_name.'</td>
        <td style="text-align: center;">'.$bean->ext_unit.'</td>
        <td style="text-align: center;">'.$bean->ext_quatity.'</td>
        <td nowrap style="text-align: center;"></td>
        <td nowrap style="text-align: center;"></td>
        </tr>';
    if(!empty($ext_content_1->name)) $tpl_ext_content .= '<tr>
        <td scope="col" style="text-align: center;">2</td>
        <td style="text-align: center;">'.$ext_content_1->name.'</td>
        <td style="text-align: center;">'.$ext_content_1->unit.'</td>
        <td style="text-align: center;">'.$ext_content_1->quantity.'</td>
        <td nowrap style="text-align: center;">'.($ext_content_1->price > 0 ? format_number($ext_content_1->price) : '').'</td>
        <td nowrap style="text-align: center;">'.($ext_content_1->amount > 0 ? format_number($ext_content_1->amount) : '').'</td>
        </tr>';
    if(!empty($ext_content_2->name)) $tpl_ext_content .= '<tr>
        <td scope="col" style="text-align: center;">3</td>
        <td style="text-align: center;">'.$ext_content_2->name.'</td>
        <td style="text-align: center;">'.$ext_content_2->unit.'</td>
        <td style="text-align: center;">'.$ext_content_2->quantity.'</td>
        <td nowrap style="text-align: center;">'.($ext_content_2->price > 0 ? format_number($ext_content_2->price) : '').'</td>
        <td nowrap style="text-align: center;">'.($ext_content_2->amount > 0 ? format_number($ext_content_2->amount) : '').'</td>
        </tr>';

    return array(
        'html' => $tpl_addrow,
        'total_amount' => $totalAmount,
        'total_quantity' => $total_quantity,
        'htmlExtContent' => $tpl_ext_content,
    );
}
?>