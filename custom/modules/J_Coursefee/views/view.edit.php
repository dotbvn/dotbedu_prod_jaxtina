<?php
class J_CoursefeeViewEdit extends ViewEdit {
    public function display(){
        if(empty($this->bean->id)){ //In Case create new

            $duration_exp = getDurationRow(false);
            $duration_exp .= getDurationRow();
            $this->ss->assign('duration_exp',$duration_exp);

            //Book Manage
            $book_list  = getBookList();
            $html_book  = getHtmlAddRow($book_list,true,'1');
            $html_book  .= getHtmlAddRow($book_list,false,'1');
            //ADD Book Template
            $this->ss->assign('html_book',$html_book);

            $html_ext_content = getHtmlExtContent($this->bean);
            //ADD Ext Content
            $this->ss->assign('html_ext_content',$html_ext_content);

        }else{ //In Case edit
            $book_list  = getBookList();
            $html_book   = getHtmlAddRow($book_list,true,'1');
            $q1 = "SELECT DISTINCT
            IFNULL(l3.id, '') book_id,
            IFNULL(l3.name, '') book_name,
            IFNULL(j_inventorydetail.id, '') primaryid,
            ABS(j_inventorydetail.quantity) quantity,
            IFNULL(j_inventorydetail.level, '') level,
            l3.unit unit,
            j_inventorydetail.price price,
            j_inventorydetail.amount amount
            FROM j_inventorydetail
            INNER JOIN j_coursefee l1 ON j_inventorydetail.coursefee_id = l1.id AND l1.deleted = 0
            INNER JOIN product_templates l3 ON j_inventorydetail.book_id = l3.id AND l3.deleted = 0
            WHERE (l1.id = '{$this->bean->id}') AND j_inventorydetail.deleted = 0";
            $details = $GLOBALS['db']->fetchArray($q1);
            foreach($details as $detail)
                $html_book .= getHtmlAddRow($book_list ,false, $detail['quantity'], $detail['book_id'], $detail['unit'],format_number($detail['price']),format_number($detail['amount']), $detail['level'] );

            //ADD Book Template
            $this->ss->assign('html_book',$html_book);

            //Ext Content
            $ext_content_1 = $this->bean->ext_content_1;
            $ext_content_2 = $this->bean->ext_content_2;
            $html_ext_content = getHtmlExtContent($this->bean);
            //ADD Ext Content
            $this->ss->assign('html_ext_content',$html_ext_content);

            //Duration
            $duration_exp = getDurationRow(false);
            $duration_obj = json_decode(html_entity_decode($this->bean->duration_exp), true);
            foreach($duration_obj as $duration)
                $duration_exp .= getDurationRow(true,$duration,$this->bean->type);
            $this->ss->assign('duration_exp',$duration_exp);
        }

        //get tax rate
        $rs1 = $GLOBALS['db']->query("SELECT id, name, value, status FROM taxrates WHERE status='Active' ORDER BY list_order ASC, name ASC");
        $taxrate = array('' => '-KCT-');
        while($row = $GLOBALS['db']->fetchByAssoc($rs1))
            $taxrate[$row['id']] = $row['name'];
        $this->ss->assign("taxrate_name_options", $taxrate);

        parent::display();
    }
}

// Generate Add row template
function getHtmlAddRow( $book_list, $showing,  $book_quantity = '', $book_id= '', $book_unit= '', $book_price= '', $book_amount= '', $book_level= ''){
    if($showing)
        $display = 'style="display:none;"';
    $tpl_addrow = "<tr class='row_tpl' $display>";
    $htm_sel    = '<select name="book_id[]" class="book_id" style="width:199px;"><option value="" price="0" unit="">-none-</option>';
    $first_opt  = true;
    $previous   = '';
    foreach($book_list as $key => $value){
        if ($value['category'] != $previous){
            if(!$first_opt)
                $htm_sel .= '</optgroup>';
            else
                $first_opt = false;

            $htm_sel       .= '<optgroup label="' .$value['category'] . '">';
            $htm_sel       .= '<option style="color: green;" value="full-set">-- select full-set --</option>';
            $previous   = $value['category'];
        }
        $sel = '';
        if(!empty($book_id) && $book_id == $value["primaryid"])
            $sel = 'selected';

        $htm_sel .= '<option '.$sel.' value="'.$value["primaryid"].'" price="'.format_number($value['list_price']).'" unit="'.$value['unit'].'">'.$value["name"].' - '.$value["sum_stock"].' left</option>';
    }
    $htm_sel .= '</optgroup></select>';
    $tpl_addrow .= '<td scope="col" align="center">'.$htm_sel.'</td>';
    $tpl_addrow .= '<td align="center"><span class="book_unit">'.$GLOBALS['app_list_strings']['unit_ProductTemplates_list'][$book_unit].'</span></td>';
    $tpl_addrow .= '<td align="center"><input type="number" class="book_quantity" name="book_quantity[]" value="'.$book_quantity.'" min="1" max="100"></td>';
    $levels = get_list_level();
    //    $tpl_addrow .= '<td align="center"><select name="book_level[]" id="book_level">'.get_select_options($levels, $book_level).'</select></td>';
    $tpl_addrow .= '<td nowrap align="center"><input class="currency input_readonly book_price" type="text" name="book_price[]" size="13" value="'.$book_price.'" readonly></td>';
    $tpl_addrow .= '<td nowrap align="center"><input class="currency book_amount" type="text" name="book_amount[]" size="13" value="'.$book_amount.'"></td>';
    $tpl_addrow .= "<td align='center'><button type='button' class='btn btn-danger btnRemove'> - </button></td>";
    $tpl_addrow .= '</tr>';
    return $tpl_addrow;
}

function getBookList(){
    $q_list = "SELECT DISTINCT
    IFNULL(l2.id, '') primaryid,
    IFNULL(l2.name, '') name,
    IFNULL(l2.code, '') code,
    IFNULL(l2.unit, '') unit,
    l2.list_price list_price,
    IFNULL(l4.name, '') parent,
    IFNULL(l4.id, '') parent_id,
    l4.list_order parent_order,
    IFNULL(l3.name, '') category,
    IFNULL(l3.id, '') category_id,
    l3.list_order list_order
    FROM product_templates l2
    INNER JOIN product_categories l3 ON l2.category_id = l3.id AND l3.deleted = 0
    LEFT JOIN product_categories l4 ON l3.parent_id = l4.id AND l4.deleted = 0
    WHERE  l2.deleted = 0 AND l2.status2 = 'Active'
    GROUP BY l2.name, l2.id
    ORDER BY l3.name ASC, l3.id, name ASC";
    return $GLOBALS['db']->fetchArray($q_list);
}


function getHtmlExtContent($bean){
    if(!empty($bean->ext_content_1))
        $ext_content_1 = json_decode(html_entity_decode($bean->ext_content_1));
    if(!empty($bean->ext_content_2))
        $ext_content_2 = json_decode(html_entity_decode($bean->ext_content_2));

    $html_ext_content = '<tr>
    <td scope="col"><span>' . $GLOBALS['mod_strings']['LBL_EXT_CONTENT_1'] . '</span></td>
    <td nowrap align="center"><input class="ext_name" type="text" name="ext_name" size="24"
    value="'. $bean->ext_name . '"></td>
    <td nowrap align="center"><input class="ext_unit" type="text" name="ext_unit" size="7"
    value="'. $bean->ext_unit. '"></td>
    <td align="center"><input type="number" class="ext_quatity" name="ext_quatity" value="'. $bean->ext_quatity . '" min="1" max="100"></td>
    <td nowrap align="center"></td>
    <td nowrap align="center"></td>
    </tr>
    <tr>
    <td scope="col"><span>' . $GLOBALS['mod_strings']['LBL_EXT_CONTENT_2'] . '</span></td>
    <td nowrap align="center"><input class="ext_name_1" type="text" name="ext_name_1" size="24"
    value="'. $ext_content_1->name . '"></td>
    <td nowrap align="center"><input class="ext_unit_1" type="text" name="ext_unit_1" size="7"
    value="'. $ext_content_1->unit . '"></td>
    <td align="center"><input type="number" class="ext_quantity_1" name="ext_quantity_1" value="'. $ext_content_1->quantity . '" min="1"
    max="100"></td>
    <td nowrap align="center"><input class="currency input_readonly ext_price_1" type="text" name="ext_price_1" size="10"
    value="'. format_number($ext_content_1->price) . '" readonly></td>
    <td nowrap align="center"><input class="currency input_readonly ext_amount_1" type="text" name="ext_amount_1" size="10"
    value="'. format_number($ext_content_1->amount) . '" readonly></td>
    </tr>
    <tr>
    <td scope="col"><span>' . $GLOBALS['mod_strings']['LBL_EXT_CONTENT_3'] . '</span></td>
    <td nowrap align="center"><input class="ext_name_2" type="text" name="ext_name_2" size="24"
    value="'. $ext_content_2->name . '"></td>
    <td nowrap align="center"><input class="ext_unit_2" type="text" name="ext_unit_2" size="7"
    value="'. $ext_content_2->unit . '"></td>
    <td align="center"><input type="number" class="ext_quantity_2" name="ext_quantity_2" value="'. $ext_content_2->quantity . '" min="1"
    max="100"></td>
    <td nowrap align="center"><input class="currency ext_price_2 input_readonly" type="text" name="ext_price_2" size="10"
    value="'. format_number($ext_content_2->price) . '" readonly></td>
    <td nowrap align="center"><input class="currency input_readonly ext_amount_2" type="text" name="ext_amount_2" size="10"
    value="'. format_number($ext_content_2->amount) . '" readonly></td>
    </tr>';
    return $html_ext_content;
}


function getDurationRow($visible = true, $duration = '', $type = ''){
    if(!$visible) $display='style="display:none;"';
    $tpl_ = "<tr class='duration_row' $display>";
    $tpl_ .= "<td><input type='text' name='duration_exp[]' value='$duration' class='duration_exp' size='4'></td>";
    $tpl_ .= "<td align='center'><label class='duration_exp_label' style='vertical-align: middle'>".$GLOBALS['app_list_strings']['unit_coursefee_list'][$type]."</label></td>";
    $tpl_ .= "<td align='center'><button type='button' class='btn btn_duration_remove'> - </button></td>";
    $tpl_ .= '</tr>';
    return $tpl_;
}

?>