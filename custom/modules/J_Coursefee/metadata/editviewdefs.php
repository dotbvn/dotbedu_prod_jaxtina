<?php
$module_name = 'J_Coursefee';
$viewdefs[$module_name] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'maxColumns' => '2',
            'javascript' => '
            {dotb_getscript file="custom/include/javascript/Multifield/jquery.multifield.min.js"}
            {dotb_getscript file="custom/modules/J_Coursefee/js/edit.js"}
            {dotb_getscript file="custom/include/javascript/Select2/select2.min.js"}
            <link rel="stylesheet" href="{dotb_getjspath file=custom/include/javascript/Select2/select2.css}"/>
            ',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL3' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'name',
                        'label' => 'LBL_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'status',
                        'studio' => 'visible',
                        'label' => 'LBL_STATUS',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'type',
                        'label' => 'LBL_TYPE',
                        'customCode' => '
                        <table width="100%" style="padding:0px!important;"><tbody><tr>
                        <td width="20%">{html_options name="type" id="type" options=$fields.type.options selected=$fields.type.value}</td>
                        <td width="10%" scope="col"><label>{$MOD.LBL_COURSE_FEE_HOUR}</label><span class="required">*</span><p id="fee_hours_label"></p></td>
                        <td width="20%"><input autocomplete="off" type="text" name="type_of_course_fee" id="type_of_course_fee" value="{dotb_number_format var=$fields.type_of_course_fee.value precision=6}" tabindex="0" size="8" maxlength="10" style="color: rgb(165, 42, 42);"></td>
                        <td width="10%" scope="col" class="duration_exp_td"><label>{$MOD.LBL_DURATION_EXP}</label> <span class="required">*</span></td>
                        <td width="35%" style="vertical-align: middle;" class="duration_exp_td"><table style="padding:0px!important;" id="tbl_duration_exp"><tbody id="tbody_duration_exp">{$duration_exp}</tbody></table></td>
                        <td width="5%" class="duration_exp_td"><button style="margin-top:4px" class="button primary" id="btn_duration_add">+</button></td>
                        </tr></tbody></table>',
                    ),
                    1 => array(
                        'name' => 'kind_of_course',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'unit_price',
                        'studio' => 'visible',
                        'customLabel' => '<label>{$MOD.LBL_UNIT_PRICE}</label> <label id="per_hour_label"> </label>',
                        'customCode' => '<input type="text" value="{dotb_number_format var=$fields.unit_price.value precision=6}" name="unit_price" title="{$MOD.LBL_UNIT_PRICE}" id="unit_price" size="30" style="font-weight: bold; color: rgb(165, 42, 42);">',
                    ),
                    1 => 'expired_term',
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'product_code',
                    ),
                    1 =>
                    array (
                        'name' => 'apply_date',
                        'label' => 'LBL_APPLY_DATE',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'customLabel' => '{$MOD.LBL_EXT_CONTENT}',
                        'customCode' => '{include file="custom/modules/J_Coursefee/tpls/ExtContent.tpl"}',
                    ),
                    1 => 'inactive_date'
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'customLabel' => '{$MOD.LBL_BOOK_GIFT}',
                        'customCode' => '{include file="custom/modules/J_Coursefee/tpls/BookTemplate.tpl"}',

                    ),
                    1 =>
                    array (
                        'name' => 'taxrate_name',
                        'customCode' => '{html_options name="taxrate_id" id="taxrate_id" options=$taxrate_name_options selected=$fields.taxrate_id.value}',
                    ),

                ),
                6 =>
                array (
                    array (
                        'name' => 'is_custom_hours',
                        'customLabel' => '{$MOD.LBL_IS_CUSTOM_HOURS}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_CUSTOM_HOURS_DES}\');" src="themes/RacerX/images/helpInline.png">',
                        'customCode' => '<input type="hidden" name="is_custom_hours" value="0">
                        <input type="checkbox" id="is_custom_hours" name="is_custom_hours" value="1" {if $fields.is_custom_hours.value == 1}checked{/if}>',
                    ),
                    array (
                        'name' => 'is_accumulate',
                        'customLabel' => '{$MOD.LBL_IS_ACCUMULATE}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_ACCUMULATE_HELP}\');" src="themes/RacerX/images/helpInline.png" alt="paidAmountHelpTip" class="paidAmountHelpTip">',
                        'customCode' => '<input type="hidden" name="is_accumulate" value="0">
                        <input type="checkbox" id="is_accumulate" name="is_accumulate" value="1" {if $fields.is_accumulate.value == 1}checked{/if}>
                        {html_options name="accumulate_percent" id="accumulate_percent" options=$fields.accumulate_percent.options selected=$fields.accumulate_percent.value title="% Accumulate"}',
                    ),
                ),
                7 =>
                array (
                    '',
                    array (
                        'name' => 'is_external_accumulate',
                        'customLabel' => '{$MOD.LBL_IS_EXTERNAL_ACCUMULATE}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_EXTERNAL_ACCUMULATE_HELP}\');" src="themes/RacerX/images/helpInline.png" alt="HelpTip" class="HelpTip">',
                    ),
                ),
            ),
            'lbl_editview_panel3' =>
            array (
                0 =>
                array (
                    0 => 'description',
                    1 => 'group_unit_name'
                ),
                1 =>
                array (
                    0 => 'team_name',
                    1 => 'assigned_user_name'
                ),
                2 =>
                array (
                    0 => array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
        ),
    ),
);
