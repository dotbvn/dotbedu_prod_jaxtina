<?php
$module_name = 'J_Coursefee';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                ),
                'hidden' =>
                array (
                    0 => '<input type="hidden" name="type" id="type" value="{$fields.type.value}">',
                ),
            ),
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'useTabs' => false,
            'tabDefs' =>
            array (
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL3' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 => 'name',
                    1 => 'status'
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'type',
                        'studio' => 'visible',
                        'label' => 'LBL_TYPE',
                        'customCode' => '{$type_of_course_fee}',
                    ),
                    1 => 'kind_of_course',
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'unit_price',
                        'customLabel' => '{$MOD.LBL_UNIT_PRICE}{if $fields.type.value != "Hours"} {$MOD.LBL_PER_HOUR}{/if}',
                        'customCode' => '{dotb_number_format var=$fields.unit_price.value precision=6}',
                    ),
                    1 => 'expired_term',
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'product_code',
                    ),
                    1 =>
                    array (
                        'name' => 'apply_date',
                        'label' => 'LBL_APPLY_DATE',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'customLabel' => '{$MOD.LBL_EXT_CONTENT}',
                        'customCode' => '
                        <table id="tblExtContent" style="width: 100%;" border="1" class="list view">
                        <thead>
                        <tr><th width="5%"></th>
                        <th width="45%" style="text-align: center;">{$MOD.LBL_NAME}</th>
                        <th width="10%" style="text-align: center;">{$MOD.LBL_UNIT}</th>
                        <th width="10%" style="text-align: center;">{$MOD.LBL_QUANTITY}</th>
                        <th width="15%" style="text-align: center;">{$MOD.LBL_PRICE}</th>
                        <th width="15%" style="text-align: center;">{$MOD.LBL_AMOUNT}</th>
                        </tr></thead>
                        <tbody id="tbodyExtContent">
                        {$extContentList}
                        </tbody>
                        </table>',
                    ),
                    1 =>
                    array (
                        'name' => 'inactive_date',
                        'label' => 'LBL_INACTIVE_DATE',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'customLabel' => '{$MOD.LBL_BOOK_GIFT}',
                        'customCode' => '
                        <table id="tblbook" style="width: 100%;" border="1" class="list view">
                        <thead><tr>
                        <th width="30%" style="text-align: center;">{$MOD.LBL_BOOK_NAME}</th>
                        <th width="10%" style="text-align: center;">{$MOD.LBL_UNIT}</th>
                        <th width="20%" style="text-align: center;">{$MOD.LBL_QUATITY}</th>
                        <th width="25%" style="text-align: center;">{$MOD.LBL_PRICE}</th>
                        <th width="25%" style="text-align: center;">{$MOD.LBL_AMOUNT}</th>
                        </tr></thead>
                        <tbody id="tbodybook">{$bookList}</tbody>
                        <tfoot><tr>
                        <td style="text-align: center;" colspan="2"><b>{$MOD.LBL_TOTAL_AMOUNT}:</b></td>
                        <td style="text-align: center;"><b>{$total_book_quantity}</b></td><td></td>
                        <td style="text-align: center;"><b>{$total_book_amount}</b></td>
                        </tr></tfoot></table>',
                    ),
                    1 =>'taxrate_name'
                ),
                6 =>
                array (
                    array (
                        'name' => 'is_custom_hours',
                        'customLabel' => '{$MOD.LBL_IS_CUSTOM_HOURS}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_CUSTOM_HOURS_DES}\');" src="themes/RacerX/images/helpInline.png" alt="paidAmountHelpTip" class="paidAmountHelpTip">',
                        'customCode' => '<input type="checkbox" class="checkbox" name="is_custom_hours" id="is_custom_hours" value="$fields.is_accumulate.value" {if $fields.is_custom_hours.value == 1}checked{/if} disabled="true">',
                    ),
                    array (
                        'name' => 'is_accumulate',
                        'customLabel' => '{$MOD.LBL_IS_ACCUMULATE}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_ACCUMULATE_HELP}\');" src="themes/RacerX/images/helpInline.png" alt="paidAmountHelpTip" class="paidAmountHelpTip">',
                        'customCode' => '<input type="checkbox" class="checkbox" name="is_accumulate" id="is_accumulate" value="$fields.is_accumulate.value" {if $fields.is_accumulate.value == 1}checked{/if} disabled="true">{$accumulate_percent}',
                    ),
                ),
                7 =>
                array (
                    '',
                    array (
                        'name' => 'is_external_accumulate',
                        'customLabel' => '{$MOD.LBL_IS_EXTERNAL_ACCUMULATE}:
                        <img border="0" onclick="return DOTB.util.showHelpTips(this,\'{$MOD.LBL_IS_EXTERNAL_ACCUMULATE_HELP}\');" src="themes/RacerX/images/helpInline.png" alt="HelpTip" class="HelpTip">',
                        'customCode' => '<input type="checkbox" class="checkbox" name="is_external_accumulate" id="is_external_accumulate" value="$fields.is_external_accumulate.value" {if $fields.is_external_accumulate.value == 1}checked{/if} disabled="true">',
                    ),
                ),
            ),
            'lbl_editview_panel3' =>
            array (
                0 =>
                array (
                    0 => 'description',
                    1 => 'group_unit_name'
                ),
                1 =>
                array (
                    0 => 'team_name',
                    1 => 'assigned_user_name'
                ),
                2 =>
                array (
                    0 => array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
        ),
    ),
);
