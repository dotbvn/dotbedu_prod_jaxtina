<?php
// created: 2022-06-14 13:20:31
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '20%',
        'default' => true,
    ),
    'status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_STATUS',
        'width' => '10%',
    ),
    'type' =>
    array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_TYPE',
        'width' => '10%',
    ),
    'type_of_course_fee' =>
    array (
        'type' => 'decimal',
        'vname' => 'LBL_COURSE_FEE_HOUR',
        'width' => '10%',
        'default' => true,
    ),
    'unit_price' =>
    array (
        'type' => 'currency',
        'default' => true,
        'vname' => 'LBL_UNIT_PRICE',
        'currency_format' => true,
        'related_fields' =>
        array (
            0 => 'currency_id',
            1 => 'base_rate',
        ),
        'width' => '10%',
    ),
    'apply_date' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_APPLY_DATE',
        'width' => '10%',
        'default' => true,
    ),
    'team_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'studio' =>
        array (
            'portallistview' => false,
            'portalrecordview' => false,
        ),
        'vname' => 'LBL_TEAMS',
        'id' => 'TEAM_ID',
        'width' => '10%',
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Teams',
        'target_record_key' => 'team_id',
    ),
    'date_modified' =>
    array (
        'vname' => 'LBL_DATE_MODIFIED',
        'width' => '10%',
        'default' => true,
    ),
    'remove_button'=>array(
        'vname' => 'LBL_REMOVE',
        'widget_class' => 'SubPanelRemoveButton',
        'module' => 'J_Coursefee',
        'width' => '5%',
    ),
);