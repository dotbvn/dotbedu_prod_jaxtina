<?php
// created: 2020-08-24 10:08:47
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '20%',
        'default' => true,
    ),
    'type' =>
    array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_TYPE',
        'width' => '10%',
    ),
    'status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_STATUS',
        'width' => '10%',
    ),
    'type_of_course_fee' =>
    array (
        'type' => 'decimal',
        'vname' => 'LBL_COURSE_FEE_HOUR',
        'width' => '10%',
        'default' => true,
    ),
    'kind_of_course' =>
    array (
        'type' => 'multienum',
        'default' => true,
        'studio' => 'visible',
        'vname' => 'LBL_KIND_OF_COURSE',
        'width' => '10%',
    ),
    'date_modified' =>
    array (
        'vname' => 'LBL_DATE_MODIFIED',
        'width' => '10%',
        'default' => true,
    ),
    'remove_button'=>array(
        'vname' => 'LBL_REMOVE',
        'widget_class' => 'SubPanelRemoveButton',
        'module' => 'J_Coursefee',
        'width' => '5%',
    ),
);