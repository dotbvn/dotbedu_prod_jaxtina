<?php
$popupMeta = array (
    'moduleMain' => 'J_Coursefee',
    'varName' => 'J_Coursefee',
    'orderBy' => 'j_coursefee.name',
    'whereClauses' => array (
        'name' => 'j_coursefee.name',
        'team_name' => 'j_coursefee.team_name',
        'status' => 'j_coursefee.status',
        'assigned_user_id' => 'j_coursefee.assigned_user_id',
        'favorites_only' => 'j_coursefee.favorites_only',
    ),
    'searchInputs' => array (
        1 => 'name',
        3 => 'status',
        4 => 'team_name',
        5 => 'assigned_user_id',
        6 => 'favorites_only',
    ),
    'searchdefs' => array (
        'name' =>
        array (
            'name' => 'name',
            'width' => 10,
        ),
        'team_name' =>
        array (
            'type' => 'relate',
            'link' => true,
            'studio' =>
            array (
                'portallistview' => false,
                'portalrecordview' => false,
            ),
            'label' => 'LBL_TEAMS',
            'id' => 'TEAM_ID',
            'width' => 10,
            'name' => 'team_name',
        ),
        'status' =>
        array (
            'type' => 'enum',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
            'width' => 10,
            'name' => 'status',
        ),
        'assigned_user_id' =>
        array (
            'name' => 'assigned_user_id',
            'label' => 'LBL_ASSIGNED_TO',
            'type' => 'enum',
            'function' =>
            array (
                'name' => 'get_user_array',
                'params' =>
                array (
                    0 => false,
                ),
            ),
            'width' => 10,
        ),
        'favorites_only' =>
        array (
            'name' => 'favorites_only',
            'label' => 'LBL_FAVORITES_FILTER',
            'type' => 'bool',
            'width' => 10,
        ),
    ),
    'listviewdefs' => array (
        'NAME' =>
        array (
            'width' => '30%',
            'label' => 'LBL_NAME',
            'default' => true,
            'link' => true,
            'name' => 'name',
        ),
        'STATUS' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
            'width' => 10,
            'name' => 'status',
        ),
        'TYPE' =>
        array (
            'type' => 'enum',
            'default' => true,
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
            'width' => 10,
            'name' => 'type',
        ),
        'APPLY_DATE' =>
        array (
            'type' => 'date',
            'label' => 'LBL_APPLY_DATE',
            'width' => 10,
            'default' => true,
            'name' => 'apply_date',
        ),
        'UNIT_PRICE' =>
        array (
            'type' => 'currency',
            'default' => true,
            'label' => 'LBL_UNIT_PRICE',
            'currency_format' => true,
            'related_fields' =>
            array (
                0 => 'currency_id',
                1 => 'base_rate',
            ),
            'width' => 10,
            'name' => 'unit_price',
        ),
        'TEAM_NAME' =>
        array (
            'width' => 10,
            'label' => 'LBL_TEAM',
            'default' => true,
            'name' => 'team_name',
        ),
        'DATE_MODIFIED' =>
        array (
            'type' => 'datetime',
            'studio' =>
            array (
                'portaleditview' => false,
            ),
            'readonly' => true,
            'label' => 'LBL_DATE_MODIFIED',
            'width' => 10,
            'default' => true,
            'name' => 'date_modified',
        ),
    ),
);
