<?php
/**
 * @author Tran Le Khanh Hong
 */
require_once ('include/externalAPI/ClassIn/utils.php');

class TeamsViewClassindrive extends DotbView
{
    private $driveView;
    private $templateFilePath = 'custom/modules/Teams/tpls/ClassInDrive.tpl';
    private $rootFolderList = array(); // Array of Folder
    private $rootDrive; // Folder

    public function __construct($bean = null, $view_object_map = array(), \Dotbcrm\Dotbcrm\Security\InputValidation\Request $request = null)
    {
        parent::__construct($bean, $view_object_map, $request);
        $this->getClassInDriveFolderList();
    }

    public function preDisplay()
    {
        parent::preDisplay();
        ///////////////////////////
        //Custom code here
        $this->driveView = new Dotb_Smarty();
        $this->driveView->assign("MOD", $GLOBALS['mod_strings']);
        $this->driveView->assign("ROOT_FOLDERS", $this->rootFolderList);
        $this->driveView->assign("ROOT", $this->rootDrive);
    }

    public function display()
    {
        //Custom code here
        $html= $this->driveView->fetch($this->templateFilePath);
        echo $html;
        //////////////////////
        parent::display();
    }

    private function getClassInDriveFolderList(): bool
    {
        $rootFolderListRes = getCloudListInSpecifiedFolder();
        $otherFolderListRes = getDriveFolderList();
        if ($rootFolderListRes['success'] == true && $otherFolderListRes['success'] == true){
            /////////////////////// Root folder //////////////////////////////////
            $rootFolder = array();
            $rootFolder['id'] = $otherFolderListRes['data'][0][0]['id'];
            $rootFolder['parentId'] = "";
            $rootFolder['name'] = "Root drive";
            $this->rootDrive = $rootFolder;

            ////////////////////// Folders in root level ////////////////////
            foreach ($rootFolderListRes['data'] as $folder){
                $folderTemp = array();
                $folderTemp['id'] = $folder['folder_id'];
                $folderTemp['name'] = $folder['folder_name'];
                $folderTemp['parentId'] = $this->rootDrive['id']; // root's id

                $this->rootFolderList[] = $folderTemp;
            }


            return true;
        }

        return false;
    }

}
