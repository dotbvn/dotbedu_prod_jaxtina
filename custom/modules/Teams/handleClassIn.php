<?php
switch ($_REQUEST['type']) {
    case 'ajaxGetClassInDrive':
        $result = ajaxGetClassInDrive();
        break;
    default:

}
echo $result;
die;

function ajaxGetClassInDrive()
{
    require_once ('include/externalAPI/ClassIn/utils.php');

    $result = array();
    $otherFolderListRes = getDriveFolderList();
    if ($otherFolderListRes['success'] == true){
        foreach ($otherFolderListRes['data'] as $parent => $children){
            if ($parent !== 0) { // '0' is root id, but root folder is stored in another variable
                foreach ($children as $child) {
                    $folderTemp = array();
                    $folderTemp['id'] = $child['id'];
                    $folderTemp['parentId'] = $child['pid'];
                    $folderTemp['name'] = $child['name'];

                    $result[] = $folderTemp;
                }
            }
        }
    }
    return json_encode($result);
}


?>
