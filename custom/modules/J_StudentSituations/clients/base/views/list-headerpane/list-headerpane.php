<?php


$viewdefs['J_StudentSituations']['base']['view']['list-headerpane'] = array(

    'buttons' => array(

//        array(
//
//            
//            'label' => 'LNK_NEW_CLASS',
//			'tooltip' => 'LNK_NEW_CLASS',
//            'acl_action' => 'create',
//            'type' => 'button',
//            'acl_module' => 'J_Class',
//            'route'=>'#J_Class/create',
//            'icon' => 'fa-plus',
//        ),
        array(

            
            'label' => 'LNK_J_STUDENT_SITUATIONS_REPORTS',
			'tooltip' => 'LNK_J_STUDENT_SITUATIONS_REPORTS',
            'acl_action' => 'list',
            'type' => 'button',
            'acl_module' => 'Reports',
            'route'=>'#Reports?filterModule=J_StudentSituations',
            'icon' => 'fa-user-chart',
        ),
        array(
            'name' => 'sidebar_toggle',
            'type' => 'sidebartoggle',
        ),
    ),
);