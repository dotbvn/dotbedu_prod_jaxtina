<?php
$module_name = 'J_StudentSituations';
$viewdefs[$module_name] =
array (
    'base' =>
    array (
        'view' =>
        array (
            'list' =>
            array (
                'panels' =>
                array (
                    0 =>
                    array (
                        'label' => 'LBL_PANEL_1',
                        'fields' =>
                        array (
                            0 =>
                            array (
                                'name' => 'name',
                                'label' => 'LBL_NAME',
                                'default' => true,
                                'enabled' => true,
                                'link' => true,
                                'width' => 'xlarge',
                            ),
                            1 =>
                            array (
                                'name' => 'payment_name',
                                'label' => 'LBL_PAYMENT_NAME',
                                'enabled' => true,
                                'id' => 'PAYMENT_ID',
                                'link' => true,
                                'sortable' => false,
                                'default' => true,
                                'width' => 'xlarge',
                            ),
                            2 =>
                            array (
                                'name' => 'type',
                                'label' => 'LBL_TYPE',
                                'enabled' => true,
                                'default' => true,
                                'type' => 'html',
                            ),
                            3 =>
                            array (
                                'name' => 'start_hour',
                                'label' => 'LBL_START_HOUR',
                                'enabled' => true,
                                'default' => true,
                            ),
                            4 =>
                            array (
                                'name' => 'start_study',
                                'label' => 'LBL_START_STUDY',
                                'enabled' => true,
                                'default' => true,
                            ),
                            5 =>
                            array (
                                'name' => 'end_study',
                                'label' => 'LBL_END_STUDY',
                                'enabled' => true,
                                'default' => true,
                            ),
                            6 =>
                            array (
                                'name' => 'total_hour',
                                'label' => 'LBL_TOTAL_HOUR',
                                'enabled' => true,
                                'default' => true,
                            ),
                            7 =>
                            array (
                                'name' => 'total_amount',
                                'label' => 'LBL_TOTAL_AMOUNT',
                                'enabled' => true,
                                'related_fields' =>
                                array (
                                    0 => 'currency_id',
                                    1 => 'base_rate',
                                ),
                                'currency_format' => true,
                                'default' => true,
                            ),
                            8 =>
                            array (
                                'name' => 'team_name',
                                'label' => 'LBL_TEAM',
                                'default' => true,
                                'enabled' => true,
                            ),
                            9 =>
                            array (
                                'name' => 'date_entered',
                                'enabled' => true,
                                'default' => true,
                            ),
                            10 =>
                            array (
                                'name' => 'assigned_user_name',
                                'label' => 'LBL_ASSIGNED_TO_NAME',
                                'default' => false,
                                'enabled' => true,
                                'link' => true,
                            ),
                            11 =>
                            array (
                                'name' => 'date_modified',
                                'enabled' => true,
                                'default' => false,
                            ),
                        ),
                    ),
                ),
                'orderBy' =>
                array (
                    'field' => 'date_modified',
                    'direction' => 'desc',
                ),
            ),
        ),
    ),
);
