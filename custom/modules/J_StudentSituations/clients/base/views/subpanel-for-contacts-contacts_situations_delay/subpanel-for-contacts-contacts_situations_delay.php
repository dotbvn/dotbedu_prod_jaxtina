<?php
// created: 2021-08-10 15:32:19
$viewdefs['J_StudentSituations']['base']['view']['subpanel-for-contacts-contacts_situations_delay'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                0 =>
                array (
                    'name' => 'ju_class_name',
                    'label' => 'LBL_CLASS_NAME',
                    'enabled' => true,
                    'id' => 'JU_CLASS_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'large',
                ),
                1 =>
                array (
                    'name' => 'type',
                    'label' => 'LBL_TYPE',
                    'enabled' => true,
                    'default' => true,
                    'type' => 'html',
                ),
                2 =>
                array (
                    'name' => 'settle_date',
                    'label' => 'LBL_DELAY_DATE',
                    'enabled' => true,
                    'default' => true,
                ),
                3 =>
                array (
                    'name' => 'description',
                    'label' => 'LBL_DESCRIPTION',
                    'enabled' => true,
                    'default' => true,
                    'width' => 'large',
                ),
                4 =>
                array (
                    'name' => 'payment_name',
                    'label' => 'LBL_PAYMENT_DELAY_ID',
                    'enabled' => true,
                    'id' => 'PAYMENT_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                5 =>
                array (
                    'name' => 'total_hour',
                    'label' => 'LBL_TOTAL_HOUR',
                    'enabled' => true,
                    'default' => true,
                ),
                6 =>
                array (
                    'name' => 'total_amount',
                    'label' => 'LBL_TOTAL_AMOUNT',
                    'enabled' => true,
                    'related_fields' =>
                    array (
                        0 => 'currency_id',
                        1 => 'base_rate',
                    ),
                    'currency_format' => true,
                    'default' => true,
                    'width' => 'small',
                ),
                7 =>
                array (
                    'name' => 'start_study',
                    'label' => 'LBL_FROM',
                    'enabled' => true,
                    'default' => true,
                ),
                8 =>
                array (
                    'name' => 'end_study',
                    'label' => 'LBL_TO',
                    'enabled' => true,
                    'default' => true,
                ),
                9 =>
                array (
                    'name' => 'team_name',
                    'label' => 'LBL_TEAMS',
                    'enabled' => true,
                    'id' => 'TEAM_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                ),
                10 =>
                array (
                    'name' => 'assigned_user_name',
                    'label' => 'LBL_ASSIGNED_TO',
                    'enabled' => true,
                    'id' => 'ASSIGNED_USER_ID',
                    'link' => true,
                    'default' => true,
                ),
                11 =>
                array (
                    'label' => 'LBL_DATE_MODIFIED',
                    'enabled' => true,
                    'default' => true,
                    'name' => 'date_modified',
                ),
            ),
        ),
    ),
    'orderBy' =>
    array (
        'field' => 'ju_class_name:asc,start_study',
        'direction' => 'desc',
    ),
    'type' => 'subpanel-list',
);