<?php
// created: 2022-03-11 12:21:16
$viewdefs['J_StudentSituations']['base']['view']['subpanel-for-contacts-contacts_situations_enrollment'] = array (
    'panels' =>
    array (
        0 =>
        array (
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_1',
            'fields' =>
            array (
                1 =>
                array (
                    'name' => 'payment_name',
                    'label' => 'LBL_ENROLLMENT_PAYMENT',
                    'enabled' => true,
                    'id' => 'PAYMENT_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'large',
                ),
                2 =>
                array (
                    'name' => 'ju_class_name',
                    'label' => 'LBL_CLASS_NAME',
                    'enabled' => true,
                    'id' => 'JU_CLASS_ID',
                    'link' => true,
                    'sortable' => false,
                    'default' => true,
                    'width' => 'large',
                ),
                3 =>
                array (
                    'name' => 'progress',
                    'label' => 'LBL_PROGRESS',
                    'enabled' => true,
                    'sortable' => false,
                    'default' => true,

                    'type' => 'html',
                    'width' => '1000',
                ),
                7 =>
                array (
                    'name' => 'student_type',
                    'default' => false,
                ),
                8 =>
                array (
                    'name' => 'student_id',
                    'default' => false,
                ),
                9 =>
                array (
                    'name' => 'total_hour',
                    'default' => false,
                ),
                10 =>
                array (
                    'name' => 'total_amount',
                    'default' => false,
                ),
                11 =>
                array (
                    'name' => 'start_study',
                    'default' => false,
                ),
            ),
        ),
    ),
    'orderBy' =>
    array (
        'field' => 'end_study',
        'direction' => 'desc',
    ),
    'type' => 'subpanel-list',
);