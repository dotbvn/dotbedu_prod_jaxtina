<?php
// created: 2022-02-11 12:52:40
$viewdefs['J_StudentSituations']['base']['view']['subpanel-for-j_class-j_class_studentsituations'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'name' => 'num',
          'label' => 'LBL_NO',
          'enabled' => true,
          'default' => true,
        ),
        1 =>
        array (
          'name' => 'student_name',
          'label' => 'LBL_STUDENT',
          'enabled' => true,
          'id' => 'STUDENT_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
        ),
        2 =>
        array (
          'name' => 'nick_name',
          'label' => 'LBL_NICK_NAME',
          'enabled' => true,
          'default' => true,
        ),
        3 =>
        array (
          'name' => 'phone_mobile',
          'label' => 'LBL_PHONE_MOBILE',
          'enabled' => true,
          'default' => true,
        ),
        4 =>
        array (
          'name' => 'birthdate',
          'label' => 'LBL_BIRTHDATE',
          'enabled' => true,
          'default' => true,
        ),
        5 =>
        array (
          'name' => 'progress',
          'label' => 'LBL_PROGRESS',
          'enabled' => true,
          'default' => true,
          'width' => 'xxlarge',
        ),
        6 =>
        array (
          'name' => 'status',
          'label' => 'LBL_STATUS',
          'enabled' => true,
          'default' => true,
        ),
        7 =>
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
        ),
      ),
    ),
  ),
  'orderBy' =>
  array (
    'field' => 'date_modified',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);