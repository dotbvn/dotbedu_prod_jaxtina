<?php
// created: 2019-03-30 17:23:53
$viewdefs['J_StudentSituations']['base']['view']['subpanel-list-for-demo-class'] = array(
    'panels' =>
        array(
            0 =>
                array(
                    'name' => 'panel_header',
                    'label' => 'LBL_PANEL_1',
                    'fields' =>
                        array(

                            1 =>
                                array(
                                    'label' => 'LBL_NAME',
                                    'enabled' => true,
                                    'default' => true,
                                    'name' => 'name',
                                    'link' => true,
                                ),
                            2 =>

                                array(
                                    'name' => 'ju_class_name',
                                    'type' => 'relate',
                                    'link' => true,
                                    'vname' => 'LBL_JU_CLASS_NAME',
                                    'id' => 'JU_CLASS_ID',
                                    'default' => true,
                                    'widget_class' => 'SubPanelDetailViewLink',
                                    'target_module' => 'J_Class',
                                    'target_record_key' => 'ju_class_id',
                                ),

                            6 =>
                                array(
                                    'name' => 'type',
                                    'label' => 'LBL_TYPE',
                                    'enabled' => true,
                                    'default' => true,
                                ),
                            7 =>
                                array(
                                    'name' => 'status',
                                    'label' => 'LBL_STATUS',
                                    'enabled' => true,
                                    'default' => true,
                                ),
                            8 =>
                                array(
                                    'name' => 'start_study',
                                    'label' => 'LBL_START_STUDY',
                                    'enabled' => true,
                                    'default' => true,
                                ),
                            9 =>
                                array(
                                    'name' => 'end_study',
                                    'label' => 'LBL_END_STUDY',
                                    'enabled' => true,
                                    'default' => true,
                                ),
                            10 =>
                                array(
                                    'name' => 'total_hour',
                                    'label' => 'LBL_TOTAL_HOUR',
                                    'enabled' => true,
                                    'default' => true,
                                ),
                            11 =>
                                array(
                                    'name' => 'total_amount',
                                    'label' => 'LBL_TOTAL_AMOUNT',
                                    'enabled' => true,
                                    'related_fields' =>
                                        array(
                                            0 => 'currency_id',
                                            1 => 'base_rate',
                                        ),
                                    'currency_format' => true,
                                    'default' => true,
                                ),

                        ),
                ),
        ),
    'orderBy' =>
        array(
            'field' => 'date_modified',
            'direction' => 'desc',
        ),
    'type' => 'subpanel-list',
);