<?php
// created: 2020-01-07 02:20:36
$viewdefs['J_StudentSituations']['base']['view']['subpanel-for-contacts-ju_studentsituations'] = array (
  'panels' =>
  array (
    0 =>
    array (
      'name' => 'panel_header',
      'label' => 'LBL_PANEL_1',
      'fields' =>
      array (
        0 =>
        array (
          'label' => 'LBL_NAME',
          'enabled' => true,
          'default' => true,
          'name' => 'name',
          'link' => true,
        ),
        1 =>
        array (
          'name' => 'ju_class_name',
          'label' => 'LBL_JU_CLASS_NAME',
          'enabled' => true,
          'id' => 'JU_CLASS_ID',
          'link' => true,
          'sortable' => false,
          'default' => true,
          'width' => 'large',
        ),
        2 =>
        array (
          'name' => 'description',
          'label' => 'LBL_DESCRIPTION',
          'enabled' => true,
          'sortable' => false,
          'default' => true,
          'width' => 'xlarge',
        ),
        3 =>
        array (
          'name' => 'type',
          'label' => 'LBL_TYPE',
          'enabled' => true,
          'default' => true,
        ),
        4 =>
        array (
          'name' => 'start_study',
          'label' => 'LBL_START_STUDY',
          'enabled' => true,
          'default' => true,
        ),
        5 =>
        array (
          'name' => 'end_study',
          'label' => 'LBL_END_STUDY',
          'enabled' => true,
          'default' => true,
        ),
        6 =>
        array (
          'name' => 'total_hour',
          'label' => 'LBL_TOTAL_HOUR',
          'enabled' => true,
          'default' => true,
        ),
/*        7 =>
        array (
          'name' => 'total_amount',
          'label' => 'LBL_TOTAL_AMOUNT',
          'enabled' => true,
          'related_fields' =>
          array (
            0 => 'currency_id',
            1 => 'base_rate',
          ),
          'currency_format' => true,
          'default' => true,
        ),  */
        8 =>
        array (
          'label' => 'LBL_DATE_MODIFIED',
          'enabled' => true,
          'default' => true,
          'name' => 'date_modified',
        ),
      ),
    ),
  ),
  'orderBy' =>
  array (
    'field' => 'start_study',
    'direction' => 'desc',
  ),
  'type' => 'subpanel-list',
);