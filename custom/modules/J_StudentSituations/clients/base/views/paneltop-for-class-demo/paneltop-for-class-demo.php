<?php
/**
 * Create By: Hiếu Phạm
 * DateTime: 12:56 PM 13/04/2019
 * To: Override panel-top for demo class
 */

$viewdefs['J_StudentSituations']['base']['view']['paneltop-for-class-demo'] = array(
    'type' => 'panel-top',
    'template' => 'panel-top',

    'buttons' => array(
        array(
            'type' => 'button',
            'name' => 'add_demo_to_class',
            'icon' => 'fa-plus',
            'css_class' => 'btn',
            'label' => 'LBL_ADD_CLASS_DEMO',
            'event' => 'button:add_demo_to_class:click',
            'acl_action' => 'import',
            'acl_module' => 'J_StudentSituations',
        ),



    ),


);