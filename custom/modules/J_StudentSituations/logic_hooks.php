<?php
    // Do not store anything in this file that is not part of the array or the hook version.  This file will
    // be automatically rebuilt in the future.
    $hook_version = 1;
    $hook_array = Array();
    $hook_array['process_record'] = Array();
    $hook_array['process_record'][] = Array(1, 'Add button on subpanel', 'custom/modules/J_StudentSituations/logicStudentSituations.php','logicStudentSituations', 'processRecording');

    $hook_array['after_save'] = Array();
    $hook_array['after_save'][] = Array(1, 'Handle CJ After Save', 'custom/modules/J_StudentSituations/logicCJJ_StudentSituation.php','logicCJJ_StudentSituation', 'handleCJAfterSave');
    $hook_array['after_save'][] = Array(2, 'Handle after Save', 'custom/modules/J_StudentSituations/logicStudentSituations.php','logicStudentSituations', 'after_save_situa');
    $hook_array['after_save'][] = Array(3, 'Cancel Receipt', 'custom/modules/J_StudentSituations/cancelReceipt.php','logicCustom', 'cancelReceipt');

    $hook_array['before_save'] = Array();
    $hook_array['before_save'][] = Array(1, 'Handle Before Save', 'custom/modules/J_StudentSituations/logicStudentSituations.php','logicStudentSituations', 'before_save_situa');

    $hook_array['before_delete'] = Array();
    $hook_array['before_delete'][] = Array(1, 'Handle before Delete', 'custom/modules/J_StudentSituations/logicStudentSituations.php','logicStudentSituations', 'before_delete_situa');

    $hook_array['after_delete'] = Array();
    $hook_array['after_delete'][] = Array(1, 'Handle Delete Situation', 'custom/modules/J_StudentSituations/logicStudentSituations.php','logicStudentSituations', 'after_delete_situa');
