<?php


class EnrollmentLink extends Link2 {
    public function buildJoinDotbQuery($dotb_query, $option = array()) {
        $dotb_query->where()->notIn('type',array('Delayed'));
        $dotb_query->groupBy('ju_class_id');
        $dotb_query->groupBy('student_id');
        $dotb_query->orderBy('student_id');
        return $this->relationship->buildJoinDotbQuery($this, $dotb_query, $option);
    }
}

class DelayLink extends Link2 {
    public function buildJoinDotbQuery($dotb_query, $option = array()) {
        $dotb_query->where()->in('type',array('Delayed'));
        return $this->relationship->buildJoinDotbQuery($this, $dotb_query, $option);
    }
}