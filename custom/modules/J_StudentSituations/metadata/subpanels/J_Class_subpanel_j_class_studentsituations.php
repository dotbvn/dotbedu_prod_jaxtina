<?php
// created: 2022-02-11 12:52:40
$subpanel_layout['list_fields'] = array (
    'student_name' =>
    array (
        'type' => 'parent',
        'studio' => true,
        'vname' => 'LBL_STUDENT',
        'sortable' => false,
        'link' => true,
        'ACLTag' => 'PARENT',
        'dynamic_module' => 'STUDENT_TYPE',
        'id' => 'STUDENT_ID',
        'related_fields' =>
        array (
            0 => 'student_id',
            1 => 'student_type',
        ),
        'width' => '10%',
        'default' => true,
    ),
    'avg_attendance' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'vname' => 'LBL_ATTENDANCE_OVERALL',
        'width' => '5%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'student_type' =>
    array (
        'type' => 'enum',
        'studio' => 'visible',
        'vname' => 'LBL_TYPE',
        'width' => '7%',
        'default' => true,
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'nick_name' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'vname' => 'LBL_NICK_NAME',
        'width' => '7%',
        'default' => true,
        'sortable' => false,
    ),
    'phone_mobile' =>
    array (
        'type' => 'varchar',
        'default' => true,
        'vname' => 'LBL_PHONE_MOBILE',
        'width' => '7%',
        'sortable' => false,
    ),
    'birthdate' =>
    array (
        'type' => 'date',
        'vname' => 'LBL_BIRTHDATE',
        'width' => '7%',
        'default' => true,
        'sortable' => false,
    ),
    'progress' =>
    array (
        'type' => 'varchar',
        'studio' => 'visible',
        'vname' => 'LBL_PROGRESS',
        'width' => '35%',
        'default' => true,
        'sortable' => false,
    ),
    'status' =>
    array (
        'type' => 'enum',
        'default' => true,
        'vname' => 'LBL_STATUS',
        'width' => '7%',
        'sortable' => false,
        'css_class' => 'align-center',
    ),
    'description' =>
    array (
        'type' => 'text',
        'vname' => 'LBL_DESCRIPTION',
        'width' => '16%',
        'default' => true,
        'sortable' => false,
    ),
    'custom_button' =>
    array (
        'type' => 'varchar',
        'width' => '5%',
        'default' => true,
        'align' => 'right',
        'sortable' => false,
    ),
    'team_id' =>
    array (
        'name' => 'team_id',
        'usage' => 'query_only',
    ),
    'date_entered' =>
    array (
        'name' => 'date_entered',
        'usage' => 'query_only',
    ),
    'ju_class_id' =>
    array (
        'name' => 'ju_class_id',
        'usage' => 'query_only',
    ),
    'total_attended' =>
    array (
        'name' => 'total_attended',
        'usage' => 'query_only',
    ),
    'total_absent' =>
    array (
        'name' => 'total_absent',
        'usage' => 'query_only',
    ),
    'class_student_id' =>
    array (
        'name' => 'class_student_id',
        'usage' => 'query_only',
    ),
);