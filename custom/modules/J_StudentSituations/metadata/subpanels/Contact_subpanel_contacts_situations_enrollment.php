<?php
// created: 2022-03-11 12:21:16
$subpanel_layout['list_fields'] = array (
    'team_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'studio' =>
        array (
            'portallistview' => false,
            'portaldetailview' => false,
            'portaleditview' => false,
        ),
        'vname' => 'LBL_TEAMS',
        'id' => 'TEAM_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'Teams',
        'target_record_key' => 'team_id',
    ),
    'payment_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_PAYMENT_NAME',
        'id' => 'PAYMENT_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'J_Payment',
        'target_record_key' => 'payment_id',
    ),
    'ju_class_name' =>
    array (
        'type' => 'relate',
        'link' => true,
        'vname' => 'LBL_CLASS_NAME',
        'id' => 'JU_CLASS_ID',
        'width' => 10,
        'default' => true,
        'widget_class' => 'SubPanelDetailViewLink',
        'target_module' => 'J_Class',
        'target_record_key' => 'ju_class_id',
    ),
    'total_hour' =>
    array (
        'type' => 'varchar',
        'vname' => 'Total Hours',
        'width' => 10,
        'default' => true,
    ),
    'total_amount' =>
    array (
        'type' => 'currency',
        'default' => true,
        'vname' => 'LBL_TOTAL_AMOUNT',
        'currency_format' => true,
        'width' => 10,
        'sortable' => false,
    ),
    'date_entered' =>
    array (
        'type' => 'datetime',
        'studio' =>
        array (
            'portaleditview' => false,
        ),
        'readonly' => true,
        'vname' => 'LBL_DATE_ENTERED',
        'width' => 10,
        'default' => true,
    ),
    'currency_id' =>
    array (
        'name' => 'currency_id',
        'usage' => 'query_only',
    ),
);