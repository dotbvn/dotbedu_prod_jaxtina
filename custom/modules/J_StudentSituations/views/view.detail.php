<?php
if(!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class J_StudentSituationsViewDetail extends ViewDetail {


    function display() {
        global $current_user;
        if($current_user->isAdmin())
            $this->ss->assign('is_admin',true);

        //Fix student name
        $this->bean->student_name = $this->bean->name;
        parent::display();
    }
    function _displaySubPanels(){
        require_once ('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($this->bean, $this->module);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['j_studentsituations_c_sms']);
        echo $subpanel->display();
    }
}
?>