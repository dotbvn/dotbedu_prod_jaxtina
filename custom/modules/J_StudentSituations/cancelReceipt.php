<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicCustom
{
    function cancelReceipt(&$bean, $event, $arguments){
        //Cancel những receipt Unpaid theo học viên Stopped khỏi lớp -- By Huy Hoàng
        if($bean->dl_reason_for == 'Stopped' && !empty($bean->payment_id)
            && !empty($bean->ju_class_id) && !empty($bean->student_id)){
            $query = "SELECT IFNULL(pmd.id, '') receipt_id
                FROM j_paymentdetail pmd
                INNER JOIN j_payment pm ON pm.id = pmd.payment_id AND pm.deleted = 0
                INNER JOIN j_studentsituations situa ON situa.payment_id = pm.id AND situa.ju_class_id = '$bean->ju_class_id'
                WHERE pmd.parent_id = '$bean->student_id' AND pmd.payment_id = '$bean->payment_id'
                AND pmd.status = 'Unpaid' AND pmd.deleted = 0";
            $res = $GLOBALS['db']->query($query);
            if(!empty($res)){
                while($row = $GLOBALS['db']->fetchByAssoc($res)){
                    $receipt_bean = BeanFactory::getBean('J_PaymentDetail', $row['receipt_id']);
                    $receipt_bean->status = 'Cancelled';
                    $receipt_bean->save();
                }
            }
        }
    }
}


?>