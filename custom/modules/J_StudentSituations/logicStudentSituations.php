<?php
if (!defined('dotbEntry') || !dotbEntry) die('Not A Valid Entry Point');

class logicStudentSituations
{

    function processRecording(&$bean, $event, $arguments)
    {
        global $timedate, $current_user;
        if ($_REQUEST['module'] == 'J_Class' && ($_REQUEST['action'] == 'DetailView' || $_REQUEST['action'] == 'SubPanelViewer')) {
            $process_json = $bean->progress;
            $bean->progress = "<div class='sts_process'></div>";
            $BUTTON = '';
            //Thêm phân quyền button Enroll & Delay - Lưu ý Không có quyền Edit Situation cũng ko đc phép Delay
            if (ACLController::checkAccess('J_StudentSituations', 'edit', true) || ($current_user->isAdmin()))
                $can_enroll = true;
            if (ACLController::checkAccess('J_StudentSituations', 'edit', true) || ($current_user->isAdmin()))
                $can_delay = true;
            if ($can_enroll) {
                if ($bean->student_type == 'Contacts') {
                    $BUTTON .= '<ul class="clickMenu fancymenu button">';
                    $BUTTON .= '<li class="dotb_action_button primary"><a title="' . translate('LBL_BTN_ENROLL', 'J_StudentSituations') . '" class="btn_enroll" onclick="open_popup_add_students(\'' . $bean->student_id . '\');" style="margin-right: 4px;color: #1462d0;"><i class="far fa-plus"></i> ' . translate('LBL_BTN_ENROLL', 'J_StudentSituations') . '</a><ul class="subnav" style="display: none;">';
                    if ($can_delay) $BUTTON .= '<li><a id="id2" class="btn_reverse" style="margin-right: 4px;color:#E61718" class_student_id="' . $bean->class_student_id . '" student_id="' . $bean->student_id . '" student_name="' . $bean->student_name . '" total_hour="' . $bean->total_hour . '" start_study="' . $bean->start_study . '" end_study="' . $bean->end_study . '" value="' . translate('LBL_BTN_DELAY', 'J_StudentSituations') . '"> <i class="far fa-minus-circle"></i> ' . translate('LBL_BTN_DELAY', 'J_StudentSituations') . '</a></li>';
                    $BUTTON .= '</ul><span class="ab subhover"></span></li></ul>';
                } else {
                    if ($can_delay) {
                        $BUTTON .= '<ul class="clickMenu fancymenu button">';
                        $BUTTON .= '<li class="dotb_action_button primary"><a title="' . translate('LBL_BTN_DELAY', 'J_StudentSituations') . '" class="btn_reverse" style="color:#E61718;" class_student_id="' . $bean->class_student_id . '" student_id="' . $bean->student_id . '" student_name="' . $bean->student_name . '" total_hour="' . $bean->total_hour . '" start_study="' . $bean->start_study . '" end_study="' . $bean->end_study . '" value="' . translate('LBL_BTN_DELAY', 'J_StudentSituations') . '"><i class="far fa-minus-circle"></i></i> ' . translate('LBL_BTN_DELAY', 'J_StudentSituations') . '</a><ul class="subnav" style="display: none;">';
                        $BUTTON .= '</ul><span class="ab subhover"></span></li></ul>';
                    }
                }
            }
            $PROCESS = '<input type="hidden" class="sts_json" student_id="' . $bean->student_id . '" value="' . $process_json . '">';
            if (!empty($BUTTON)) $bean->custom_button = '<div style="width: 120px;text-align: right;">' . $BUTTON . $PROCESS . '</div>';
            else $bean->custom_button = $PROCESS;
            //END xử lý Phân quyền

            //Add description
            $bean->description = '<textarea sts_id = "' . $bean->class_student_id . '" class="sts_description" style="position: relative; width:100%;" title="Note">' . $bean->description . '</textarea>';
            //Add link student
            $bean->student_name = '<a style="white-space: nowrap;" href="#bwc/index.php?module=' . $bean->student_type . '&action=DetailView&record=' . $bean->student_id . '">' . $bean->student_name . '</a>';

            switch ($bean->status) {
                case '':
                case 'Not Started':
                    $colorClass = "label-lightgreen";
                    break;
                case 'In Progress':
                    $colorClass = "label-lightblue";
                    break;
                case 'Finished':
                    $colorClass = "label-lightred";
                    break;
                case 'Delayed':
                    $colorClass = "label-lightgray";
                    break;
            }
            $_json = json_decode(html_entity_decode($process_json), true);
            $status = $GLOBALS['app_list_strings']['situation_status_list'][$bean->status];
            $total_text = translate('LBL_TOTAL', 'J_StudentSituations') . ": " . format_number($_json['total_hour'], 2, 2) . ' ' . translate('LBL_HOURS', 'J_StudentSituations');
            $total_text .= "\n" . translate('LBL_STUDIED', 'J_StudentSituations') . ": " . format_number($_json['hours_now'], 2, 2) . ' ' . translate('LBL_HOURS', 'J_StudentSituations');
            $total_text .= "\n" . translate('LBL_REMAINING', 'J_StudentSituations') . ": " . format_number($_json['remain_hour'], 2, 2) . ' ' . translate('LBL_HOURS', 'J_StudentSituations');

            $bean->status = '<span class="simptip-position-top simptip-movable simptip-multiline" data-tooltip="' . $total_text . '" ><span style="white-space: nowrap;" class="' . $colorClass . '">' . $status . '</span></span>';
            //Overall attendance
            $tool_tip = translate('LBL_TOTAL_ATTENDED', 'J_ClassStudents') . ": " . $bean->total_attended . ' / ' . ($bean->total_absent + $bean->total_attended);
            $tool_tip .= "\n" . translate('LBL_TOTAL_ABSENT', 'J_ClassStudents') . ": " . $bean->total_absent . ' / ' . ($bean->total_absent + $bean->total_attended);

            $bean->avg_attendance = labelAttOverall($bean->avg_attendance, $tool_tip);

        } elseif (strpos($_REQUEST['view'], 'subpanel') !== false) {
            //HIỂN THỊ SUPANEL STUDENT/LEAD
            //Get Payment Related of Enrollment
            $q2 = "SELECT DISTINCT IFNULL(l1.id, '') payment_id, IFNULL(l1.name, '') payment_name,
            IFNULL(l1.payment_type, '') payment_type, l1.payment_date payment_date,
            l1_1.hours hours, l1_1.amount amount, IFNULL(l1_1.situation_id, '') situation_id
            FROM j_payment
            INNER JOIN j_payment_j_payment_1_c l1_1 ON j_payment.id = l1_1.payment_ida AND l1_1.deleted = 0
            INNER JOIN j_payment l1 ON l1.id = l1_1.payment_idb AND l1.deleted = 0
            WHERE (j_payment.parent_id = '{$bean->student_id}') AND j_payment.deleted = 0
            ORDER BY l1_1.issue_date DESC";
            $res = $GLOBALS['db']->query($q2);
            while ($row = $GLOBALS['db']->fetchByAssoc($res)) {
                $sit[$row['situation_id']][] = array(
                    'id' => $row['payment_id'],
                    'name' => $row['payment_name'],
                    'type' => $row['payment_type'],
                    'date' => $row['payment_date'],
                    'hours' => $row['hours'],
                    'amount' => $row['amount']
                );
            }

            if (!empty($bean->student_type) && !empty($bean->student_id)) {
                $colorzing = array(
                    'Delayed' => 'textbg_blood',
                    'Demo' => 'textbg_bluelight',
                    'OutStanding' => 'textbg_orange',
                    'Enrolled' => 'textbg_dream');

                $q1 = "SELECT DISTINCT IFNULL(jst.id, '') primaryid,
                IFNULL(l2.id,'') student_id, IFNULL(jst.type, '') type,
                jst.start_hour start_hour, jst.start_study start_study,
                jst.end_study end_study, ROUND(jst.total_minute/60,6) total_hour,
                jst.total_amount total_amount, jst.settle_date settle_date,
                SUM(CASE WHEN l3.date_start <= '{$timedate->nowDb()}' THEN ROUND(l3.duration_hours+(l3.duration_minutes/60),9) ELSE 0 END) hour_till_now
                FROM j_studentsituations jst
                INNER JOIN j_class l1 ON jst.ju_class_id = l1.id AND l1.deleted = 0
                INNER JOIN " . strtolower($bean->student_type) . " l2 ON jst.student_id = l2.id AND l2.deleted = 0 AND (l2.id = '{$bean->student_id}')
                INNER JOIN meetings_" . strtolower($bean->student_type) . " l3_1 ON jst.id = l3_1.situation_id AND l3_1.deleted = 0
                INNER JOIN meetings l3 ON l3.id = l3_1.meeting_id AND l3.deleted = 0 AND l3.session_status <> 'Cancelled'
                WHERE (((l1.id = '{$bean->ju_class_id}') AND (jst.type IN ('Enrolled','OutStanding','Demo')))) AND jst.deleted = 0
                GROUP BY start_hour ASC, jst.id";
                $res = $GLOBALS['db']->query($q1);
                $htm_rel .= "<table class='table'>";
                $htm_rel .= "<thead><tr>
                <th width='10%'>" . translate('LBL_TYPE', 'J_StudentSituations') . "</th>
                <th width='5%'>" . translate('LBL_ENROLLMENT_DATE', 'J_Class') . "</th>
                <th width='5%'>" . translate('LBL_TOTAL_HOUR', 'J_StudentSituations') . "</th>
                <th width='10%'>" . translate('LBL_TOTAL_AMOUNT', 'J_StudentSituations') . "</th>
                <th width='20%'>" . translate('LBL_START_END', 'J_StudentSituations') . "</th>
                <th width='50%'>" . translate('LBL_RELATED_PAYMENT', 'J_StudentSituations') . "</th>
                </thead><tbody>";
                while ($row = $GLOBALS['db']->fetchByAssoc($res)) {
                    $htm_rel .= "<tr>
                    <td width='10%'><a href='#bwc/index.php?module=J_StudentSituations&action=DetailView&record={$row['primaryid']}'><span class='label ellipsis_inline {$colorzing[$row['type']]}' style='white-space: nowrap;'>" . $GLOBALS['app_list_strings']['situation_type_list'][$row['type']] . "</span></a></td>
                    <td width='5%'>" . date('d/m/Y', strtotime($row['settle_date'])) . "</td>
                    <td width='5%'>" . format_number($row['total_hour'], 2, 2) . "</td>
                    <td width='10%'>" . format_number($row['total_amount']) . "</td>
                    <td width='15%'>" . date('d/m/Y', strtotime($row['start_study'])) . "→" . date('d/m/Y', strtotime($row['end_study'])) . "</td>
                    <td width='50%'><table class='dataTable no-border'><tbody>";
                    foreach ($sit[$row['primaryid']] as $payment) {
                        $htm_rel .= "<tr>
                        <td width='40%'><a href='#bwc/index.php?module=J_Payment&action=DetailView&record={$payment['id']}'>{$payment['name']}</a></td>
                        <td width='30%'>" . $payment['type'] . "</td>
                        <td width='10%'>" . format_number($payment['hours'], 2, 2) . "</td>
                        <td width='20%'>" . format_number($payment['amount']) . "</td></tr>";
                    }
                    $htm_rel .= "</tbody></table></td></tr>";
                    $count_si++;
                    $total_hours += $row['total_hour'];
                    $total_amount += $row['total_amount'];
                    $hour_till_now += $row['hour_till_now'];
                }
                $hour_till_now = round($hour_till_now, 2);
                $total_hours = round($total_hours, 2);
                $remain_hours = round($total_hours - $hour_till_now, 2);
                if ($hour_till_now == 0) $aa = "<span style='color:#468931'>" . translate('LBL_NOT_STARTED', 'J_StudentSituations') . ": " . format_number($hour_till_now, 2, 2) . " " . translate('LBL_HOURS', 'J_StudentSituations') . "</span>";
                if ($hour_till_now > 0 && $hour_till_now < $total_hours) $aa = "<span style='color:#115CAB'>" . translate('LBL_IN_PROGRESS', 'J_StudentSituations') . ": " . format_number($hour_till_now, 2, 2) . " " . translate('LBL_HOURS', 'J_StudentSituations') . "</span>";
                if ($hour_till_now >= $total_hours) $aa = "<span style='color:#DC143C'>" . translate('LBL_FINISHED', 'J_StudentSituations') . ": " . format_number($hour_till_now, 2, 2) . " " . translate('LBL_HOURS', 'J_StudentSituations') . "</span>";


                $htm_rel .= "</tbody>
                <tfoot><tr><td></td>
                <td>" . translate('LBL_TOTAL', 'J_StudentSituations') . "</td>
                <td>" . format_number($total_hours, 2, 2) . "</td>
                <td>" . format_number($total_amount) . "</td>
                <td>$aa</td>
                <td>" . translate('LBL_REMAINING', 'J_StudentSituations') . ": " . format_number($remain_hours, 2, 2) . " " . translate('LBL_HOURS', 'J_StudentSituations') . "</td>
                <td></td></tr>
                </tfoot>
                </table>";
                if ($count_si > 0) $bean->progress = $htm_rel;
            }
        }
    }

    function after_save_situa(&$bean, $event, $arguments)
    {
        //Update Status Lead - Demo
        if ($bean->student_type == 'Leads' && $bean->type == 'Demo') {
            $student = BeanFactory::getBean('Leads', $bean->student_id, array('disable_row_level_security' => true));
            $notInStatus = array('Import', 'New', 'In Process', 'Dead');
            if (in_array($student->status, $notInStatus)) {
                if (array_key_exists('Appointment', $GLOBALS['app_list_strings']['lead_status_dom']))
                    $status = 'Appointment';
                else $status = 'Ready to Demo';
                $student->status = $status;
                $student->save();
            }
        }
    }

    function before_delete_situa(&$bean, $event, $arguments)
    {
        global $timedate, $current_user;
        require_once("custom/include/_helper/junior_class_utils.php");

        //Dùng cho TH xoá trong Situation
        if ($_REQUEST['action'] == 'Delete' && $_REQUEST['return_module'] == 'J_StudentSituations') {
            if (!empty($bean->id) && !empty($bean->payment_id)) {
                $enr = BeanFactory::getBean('J_Payment', $bean->payment_id, array('disable_row_level_security' => true));
                if (empty($enr->id)) return;
                $rows = $GLOBALS['db']->fetchArray("SELECT IFNULL(id, '') id, IFNULL(payment_idb, '') payment_idb, amount, hours FROM  j_payment_j_payment_1_c WHERE situation_id='{$bean->id}' AND payment_ida='{$enr->id}' AND deleted = 0");
                foreach ($rows as $pms) {
                    $GLOBALS['db']->query("UPDATE j_payment_j_payment_1_c SET deleted=1, date_modified='{$timedate->nowDb()}' WHERE id='{$pms['id']}'");
                    $enr->paid_hours -= $pms['hours'];
                    $enr->paid_amount -= $pms['amount'];
                    //Recalculate
                    getPaymentRemain($pms['payment_idb']);
                }
                //Tránh đệ quy
                $count_situation = $GLOBALS['db']->getOne("SELECT COUNT(id) id FROM j_studentsituations WHERE id <> '{$bean->id}' AND payment_id = '{$enr->id}' AND deleted = 0 GROUP BY payment_id");
                if ($enr->paid_hours <= 0.1 || empty($count_situation)) $GLOBALS['db']->query("UPDATE j_payment SET deleted=1, date_modified='{$timedate->nowDb()}', modified_user_id='{$current_user->id}' WHERE id='{$enr->id}'");
                else $GLOBALS['db']->query("UPDATE j_payment SET paid_hours={$enr->paid_hours}, paid_amount={$enr->paid_amount}, date_modified='{$timedate->nowDb()}', modified_user_id='{$current_user->id}' WHERE id='{$enr->id}'");
            }
        }

        //Xoá học viên khỏi buổi học
        removeJunFromSession($bean->id, $bean->student_type);

        //Cache POST Dùng cho After Delete
        $_POST['ST_ju_class_id'] = $bean->ju_class_id;
        $_POST['ST_student_type'] = $bean->student_type;
        $_POST['ST_student_id'] = $bean->student_id;
    }

    function after_delete_situa(&$bean, $event, $arguments)
    {
        //Xoá khỏi lớp bằng Bean: Mục đích để Kích hoạt API Class In/Canvas... Remove Student
        //Dùng After Save để hạn chế lỗi do API gây ra
        $student_id = $_POST['ST_student_id'];
        $class_id = $_POST['ST_ju_class_id'];
        $student_type = $_POST['ST_student_type'];
        if (!empty($student_id) && !empty($class_id) && (!$bean->no_cache)) {
            $countStd = $GLOBALS['db']->getOne("SELECT COUNT(id) FROM j_studentsituations WHERE type IN ('Enrolled', 'OutStanding', 'Demo') AND deleted = 0 AND ju_class_id = '$class_id' AND student_id = '$student_id'");
            if (empty($countStd)) {
                $rowsrelcs = $GLOBALS['db']->fetchArray("SELECT IFNULL(id, '') id FROM j_classstudents WHERE class_id = '$class_id' AND student_id = '$student_id' AND deleted = 0");
                foreach ($rowsrelcs as $key => $rel) {
                    $relcs = BeanFactory::getBean('J_ClassStudents', $rel['id'], array('disable_row_level_security' => true));
                    $relcs->mark_deleted($relcs->id);
                }
            }
            //Set Count Class Number
            if ($student_type == 'Contacts') updateClassAttendance($class_id);
        }
    }

    function before_save_situa(&$bean, $event, $arguments)
    {
        if ($bean->start_hour >= 0) $bean->start_minute = round($bean->start_hour * 60);
        if ($bean->total_hour >= 0) $bean->total_minute = round($bean->total_hour * 60);
    }
}

?>
