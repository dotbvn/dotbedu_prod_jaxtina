<?php
if (!defined('dotbEntry')) define('dotbEntry', true);
chdir(dirname(__FILE__));
define('ENTRY_POINT_TYPE', 'api');
require_once('include/entryPoint.php');

$product_name = $_POST['product_name'];
$number_of_student = $_POST['number_of_student'];
$number_of_user = $_POST['number_of_user'];
$expired_date = $_POST['expired_date'];
$service_duration_unit = $_POST['service_duration_unit'];
$service_duration_value = $_POST['service_duration_value'];

if(!empty($product_name) && !empty($expired_date)){

    //Update ActiveStudent
    $GLOBALS['db']->query("ALTER TABLE contacts   add COLUMN ems_active_state bool  DEFAULT 1 NULL ,  add COLUMN ems_end_date date  NULL ,  add COLUMN ems_end_field varchar(50)  NULL ,  add COLUMN ems_expired_date date  NULL ,  add COLUMN ems_active_update datetime  NULL ;");
    if (function_exists('updateActiveStudent')){}
    else require_once("custom/Extension/application/Ext/Utils/ems_license_student.php");
    updateActiveStudent();

    $parsed_url = parse_url($GLOBALS['dotb_config']['site_url']);
    $tenant_name = $parsed_url['host'];
    $url = $GLOBALS['dotb_config']['saas_url'] . '/rest/v11_3/tenant/migrate-license';
    $data = [
        'tenant_name' => $tenant_name,
        'tenant_url' => $GLOBALS['dotb_config']['site_url'],
        'product_name' => $product_name,
        'number_of_student' => $number_of_student,
        'number_of_user' => $number_of_user,
        'number_used_student' => $GLOBALS['db']->getOne("SELECT COUNT(id) count_student FROM contacts WHERE ems_active_state = 1 AND deleted = 0"),
        'number_used_user' => $GLOBALS['db']->getOne("SELECT COUNT(id) count_user FROM users WHERE user_name NOT IN ('admin' , 'app_admin') AND (teacher_id IS NULL OR teacher_id = '') AND status = 'Active' AND (user_name IS NOT NULL AND user_name <> '') AND deleted = 0"),
        'expired_date' => $expired_date,
        'service_duration_unit' => $service_duration_unit,
        'service_duration_value' => $service_duration_value
    ];
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
    ));
    $response = json_decode(curl_exec($curl), true);
    curl_close($curl);

    if ($response['success'] === true) {
        $_REQUEST['enforced_license'] = 1;
        $_REQUEST['tenant_license'] = $response['data']['tenant']['license'];

        header("Location: ". $GLOBALS['dotb_config']['site_url'] . '#bwc/index.php?module=EMS_Settings&action=subscription');

    }

    if (class_exists('DBManagerFactory')) {
        $GLOBALS['db'] = DBManagerFactory::getInstance();
        $GLOBALS['db']->disconnect();
    }
} else {
if(isset($_POST)) {?>
        <form id='form_license' method="POST" action="input_license.php">
            <input type="hidden" name="enforced_license" value="1">
            <table style="line-height: 2; width: 500px;">
                <tr>
                    <td width="45%">Gói sử dụng:</td>
                    <td colspan="2">
                        <select name="product_name" id="product_name" style="width: 100%;">
                            <option value="Standard Plan">Standard Plan</option>
                            <option value="Pro Plan" selected>Pro Plan</option>
                            <option value="Unlimited Plan">Unlimited Plan</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Số lượng học viên:</td>
                    <td colspan="2"><input type="number" name="number_of_student" min="0" value="0" style="width: 100%;"></td>
                </tr>
                <tr>
                    <td>Số lượng user:</td>
                    <td colspan="2"><input type="number" name="number_of_user" min=0 value="0" style="width: 100%;"></td>
                </tr>
                <tr>
                    <td width="45%">Chu kỳ thanh toán:</td>
                    <td width="20%"><input type="number" name="service_duration_value" min="1" value="1"></td>
                    <td>
                        <select name="service_duration_unit" id="service_duration_unit" style="width: 100%;">
                            <option value="month" >tháng</option>
                            <option value="year" selected>năm</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Ngày hết hạn:</td>
                    <td colspan="2"><input type="date" name="expired_date" style="width: 100%;"></td>
                </tr>
                <tr><td style="text-align: center; line-height: 1" colspan="3"><br><input type="submit" name="submit" value="Submit"></td></tr>
            </table>
        </form>
        <?php }
}


function cleanup($dir) {
    if (strpos($dir, '/*') !== false){  //TH chỉ xoá file /* giữ lại folder gốc
        $dir = str_replace('/*', '', $dir);
        $no_rmdir = $dir;
    }
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file)) cleanup($file);
        else unlink($file);
    }
    if($dir != $no_rmdir) rmdir($dir);
}