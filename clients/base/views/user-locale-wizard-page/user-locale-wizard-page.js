
/**
 * User Locale wizard page for the FirstLoginWizard.
 *
 * @class View.Views.Base.UserLocaleWizardPageView
 * @alias DOTB.App.view.views.BaseUserLocaleWizardPageView
 * @extends View.Views.Base.UserWizardPageView
 */
({
    extendsFrom: "UserWizardPageView",
    TIME_ZONE_KEY: 'timezone',
    TIME_PREF_KEY: 'timepref',
    DATE_PREF_KEY: 'datepref',
    NAME_FORMAT_KEY: 'default_locale_name_format',

    /**
     * @override
     * @param options
     */
    initialize: function(options) {
        var self = this;
        options.template = app.template.getView('wizard-page');

        /// hide option sample data using for non-admin user
        if (_.isUndefined(App.user.attributes.type)) {
            self._hideSampleDB();
        } else {
            if (App.user.attributes.type !== 'admin') {
                self._hideSampleDB();
            }
        }

        this._super('initialize', [options]);
        // Preset the user prefs for formats
        if (this.model) {
            this.model.set(this.TIME_ZONE_KEY, (app.user.getPreference(this.TIME_ZONE_KEY) || ''));
            this.model.set(this.TIME_PREF_KEY, (app.user.getPreference(this.TIME_PREF_KEY) || ''));
            this.model.set(this.DATE_PREF_KEY, (app.user.getPreference(this.DATE_PREF_KEY) || ''));
            this.model.set(this.NAME_FORMAT_KEY, (app.user.getPreference(this.NAME_FORMAT_KEY) || ''));
        }
        this.action = 'edit';
    },

    _render: function(){
        var self = this;
        // Prepare the metadata so we can prefetch select2 locale options
        this._prepareFields(function() {
            if (!self.disposed) {
                self.fieldsToValidate = self._fieldsToValidate(self.meta);
                self._super("_render");
            }
        });
    },
    _prepareFields: function(callback) {
        var self = this;
        // Fixme this doesn't belong in user. See TY-526.
        app.user.loadLocale(function(localeOptions) {
            // Populate each field def of type enum with returned locale options and use user's pref as displayed
            _.each(self.meta.panels[0].fields, function(fieldDef) {
                var opts = localeOptions[fieldDef.name];
                if (opts) {
                    fieldDef.options = opts;
                }
            });
            callback();
        });
    },
    /**
     * Called before we allow user to proceed to next wizard page. Does the validation and locale update.
     * @param {Function} callback The callback to call once HTTP request is completed.
     * @override
     */
    beforeNext: function(callback) {
        let self = this;
        let footer_content = $('.wizard-footer').html();
        let loading = '<span class="btn btn-primary"><span class="send-message-loader"></span></span>';
        $('.wizard-footer').html(loading);
        self.getField("next_button").setDisabled(true);  //temporarily disable
        if (!_.isUndefined(self.model.get('sample_db')) && self.model.get('sample_db') === true) {
            /// todo: run script here
            var logging = false;
            app.api.call("read", app.api.buildURL('tenant/sample-db'), null, {
                success: function (response) {
                    if (response['success'] === true) {
                        self.model.doValidate(self.fieldsToValidate,
                            _.bind(function(isValid) {
                                var self2 = this;
                                if (isValid) {
                                    var payload = self2._prepareRequestPayload();
                                    // 'ut' is, historically, a special flag in user's preferences that is
                                    // generally marked truthy upon timezone getting saved. It's also used
                                    // to semantically represent "is the user's instance configured"
                                    payload['ut'] = true;
                                    app.user.updatePreferences(payload, function(err) {
                                        $('.wizard-footer').html(footer_content);
                                        self2.updateButtons();  //re-enable buttons
                                        if (err) {
                                            app.logger.debug("Wizard locale update failed: " + err);
                                            callback(false);
                                        } else {
                                            callback(true);
                                        }
                                    });
                                } else {
                                    $('.wizard-footer').html(footer_content);
                                    callback(false);
                                }
                            }, self)
                        );
                    }
                    else {
                        app.logger.debug("Wizard locale update failed");
                        callback(false);
                    }
                },
                error: function (error) {
                    app.logger.debug("Wizard locale update failed: " + error);
                    callback(false);
                }
            });
        } else {
            self.model.doValidate(self.fieldsToValidate,
                _.bind(function(isValid) {
                    var self2 = this;
                    if (isValid) {
                        var payload = self2._prepareRequestPayload();
                        // 'ut' is, historically, a special flag in user's preferences that is
                        // generally marked truthy upon timezone getting saved. It's also used
                        // to semantically represent "is the user's instance configured"
                        payload['ut'] = true;
                        app.user.updatePreferences(payload, function(err) {
                            $('.wizard-footer').html(footer_content);
                            self2.updateButtons();  //re-enable buttons
                            if (err) {
                                app.logger.debug("Wizard locale update failed: " + err);
                                callback(false);
                            } else {
                                callback(true);
                            }
                        });
                    } else {
                        callback(false);
                    }
                }, self)
            );
        }
    },

    _hideSampleDB: function () {
        self = this;
        if (!_.isUndefined(self.options.meta.panels[0].fields)) {
            self.options.meta.panels[0].fields.splice(4, 1);
        }
    }
})
