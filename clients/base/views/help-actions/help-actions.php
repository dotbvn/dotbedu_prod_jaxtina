<?php
$viewdefs['base']['view']['help-actions'] = array(
    'actionList' => array(
        array(
            'type' => 'help',
            'name' => 'documentation',
            'label' => 'LBL_DOCUMENTATION',
            'url' => 'https://help.dotb.vn',
            'redirect' => true,
        ),
        array(
            'type' => 'feedback',
            'name' => 'giveFeedback',
            'label' => 'LBL_GIVE_FEEDBACK',
        )
    ),
);
