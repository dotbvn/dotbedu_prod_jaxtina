
/**
 * @class View.Views.Base.LanguageActionsView
 * @alias DOTB.App.view.views.BaseLanguageActionsView
 * @extends View.View
 */
({
    events: {
        'click [data-action=helpList] .dropdown-menu a' : 'handleHelpAction',
        'click .feedback-popup' : 'handleSendFeedback'
    },
    tagName: "span",
    plugins: ['Dropdown'],
    /**
     * @override
     * @param {Object} options
     */
    initialize: function(options) {
        app.events.on("app:sync:complete", this.render, this);
        app.events.on("app:login:success", this.render, this);
        app.events.on("app:logout", this.render, this);
        app.view.View.prototype.initialize.call(this, options);
        $(window).on('resize', _.debounce(_.bind(this.adjustMenuHeight, this), 100));
    },
    /**
     * @override
     * @private
     */
    _renderHtml: function() {
        this.isAuthenticated = app.api.isAuthenticated();
        this.helpActionList = this.getHelpActions();
        app.view.View.prototype._renderHtml.call(this);
        this.$('[data-toggle="dropdown"]').dropdown();
        this.adjustMenuHeight();
    },

    adjustMenuHeight: function(){
        if (this.$('[data-action=helpList]').length === 0) {
            return;
        }
        var linkButton = this.$('[data-action=helpList]'),
            dropupMenu = this.$('[data-action=helpList] .dropdown-menu.bottom-up'),
            linkBottomPosition = parseInt($('footer').height() - linkButton.height() - linkButton.position().top, 10),
            dropupOffset = parseInt(dropupMenu.css('bottom'), 10),
            borderTop = parseInt(dropupMenu.css('border-top-width'), 10),
            menuHeight = Math.round($(window).height() - borderTop - dropupOffset - linkBottomPosition);
        dropupMenu.css('max-height', menuHeight);
    },
    /**
     * Formats the language list for the template
     *
     * @return {Array} of languages
     */

    getHelpActions: function() {
        // Format the list of languages for the template
        var list = []
        _.each(this.meta.actionList, function (element){
            list.push(
                {
                    key: element.name,
                    label: element.label,
                    redirect: element.redirect,
                    url: element.url,
                    icon: element.icon
                }
            )
        })
        return list;
    },
    /**
     * @inheritdoc
     */
    _dispose: function() {
        $(window).off('resize');
        app.view.View.prototype._dispose.call(this);
    },
    handleHelpAction: function(e) {
        var $li = this.$(e.currentTarget),
            action = $li.data("action");
        if (action === "giveFeedback") {
            this.handleGiveFeedback();
        }
    },
    handleGiveFeedback: function(){
        var self = this;
        app.drawer.open({
            layout: 'create',
            context: {
                create: true,
                module: "Bugs"
            }
        }, function(context, model) {
            if (!model) {
                return;
            }
            self.context.resetLoadFlag();
            self.context.set('skipFetch', false);
            if (_.isFunction(self.loadData)) {
                self.loadData();
            } else {
                self.context.loadData();
            }
            app.router.navigate('Bugs/' + model.id, {trigger: true});
        });
    }
})
