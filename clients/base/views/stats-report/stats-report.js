({
    plugins: ['Dashlet', 'CssLoader'],
    css: ["custom/include/css/stats-report.css"],
    data_stats: null,
    is_list: true,
    columns: 3,
    count: 0,
    initialize: function(options) {
        this._super('initialize', [options]);
        if(!this.meta.id_dashlet){
            this.meta.id_dashlet = this.generateUUID();
        }
        if (!this.meta.config) {
            this._fetchAndRenderData();
        }
    },

    initDashlet: function(view) {
        // check if we're on the config screen
        if (this.meta.config) {
            this.meta.panels = this.dashletConfig.dashlet_config_panels;
        } else {
            this.is_list = (this.settings.get('display_type') === 'grid') ? false : true;
            this._handleAutoRefresh();
        }
    },

    _handleAutoRefresh: function() {
        const autoRefresh = this.settings.get('auto_refresh');
        if (autoRefresh > 0) {
            this.timerId && clearTimeout(this.timerId);
            this._scheduleReload(autoRefresh * 1000 * 60);
        }
    },

    _fetchAndRenderData: function() {
        let self = this;
        this._retrieveData(function(data) {
            if(self){
                self.id_dashlet = self.meta.id_dashlet;
                self.id_dashboard = self.model.attributes.id;
            }
            self.data_stats = data;
            self._render();
        });
    },

    _getAvailableStats(callback) {
        app.api.call('GET', app.api.buildURL('Reports/getStats'), null, {
            success: (data) => {
                const stats = _.reduce(data, (acc, stat) => ({...acc, [stat.id]: stat.report_name}), {});
                callback(stats);
            },
            error: () => console.log('Error getting go to data')
        });
    },

    _scheduleReload(delay) {
        this.timerId = setInterval(() => this._fetchAndRenderData(), delay);
    },

    _retrieveData(callback) {
        const url = app.api.buildURL(`Reports/getStatsDetail?id_list=${this.arrayToString(this.meta.display_stats)}`);
        app.api.call('GET', url, null, {
            success: (data) => callback(data.empty ? [] : data.return_data),
            error: () => {
                console.log('Error getting data');
                callback([]);
            }
        });
    },
    generateUUID: function (){
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0,
                v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },

    arrayToString(arr, separator = ',') {
        if(arr != undefined){
            return arr.join(separator);
        } else {
            return '';
        }
    },

    _render() {
        this.columns = this._calculateColumnsBasedOnWidth(this.$el.width());
        this._super('_render');
    },

    _calculateColumnsBasedOnWidth(width) {
        if (width >= 1600) return 5;
        if (width >= 1000 && width <= 1600) return 4;
        if (width >= 600 && width <= 1000) return 2;
        return 1;
    },

    _renderField(field) {
        if (field.def.name === 'display_stats' && this.action === 'edit') {
            this._getAvailableStats((stats) => {
                field.def.options = stats;
                field.items = stats;
                this._super('_renderField', [field]);
            });
        } else {
            this._super('_renderField', [field]);
        }
    }
})
