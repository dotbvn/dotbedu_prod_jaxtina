<?php


$viewdefs['base']['view']['stats-report'] = array(
    'dashlets' => array(
        array(
            'label' => 'LBL_DASHLET_STATS',
            'description' => 'LBL_DASHLET_STATS_DESC',
            'config' => array(

            ),
            'preview' => array(

            ),
        )
    ),
    'custom_toolbar' => array(
        'buttons' => array(
            array(
                "type" => "dashletaction",
                "css_class" => "btn btn-invisible dashlet-toggle minify",
                "icon" => "fa-chevron-up",
                "action" => "toggleMinify",
                "tooltip" => "LBL_DASHLET_TOGGLE",
            ),
            array(
                'dropdown_buttons' => array(
                    array(
                        'type' => 'dashletaction',
                        'action' => 'editClicked',
                        'label' => 'LBL_DASHLET_CONFIG_EDIT_LABEL',
                    ),
                    array(
                        'type' => 'dashletaction',
                        'action' => 'removeClicked',
                        'label' => 'LBL_DASHLET_REMOVE_LABEL',
                    ),
                ),
            ),
        ),
    ),
    'dashlet_config_panels' => array(
        array(
            'name' => 'panel_body',
            'columns' => 2,
            'labelsOnTop' => true,
            'placeholders' => true,
            'fields' => array(
                array(
                    'name' => 'display_stats',
                    'label' => 'LBL_DISPLAY_STATS',
                    'type' => 'enum',
                    'isMultiSelect' => true,
                    'ordered' => true,
                    'span' => 12,
                    'hasBlank' => true,
                    'options' => array('' => ''),
                    'required' => true
                ),
                array(
                    'name' => 'auto_refresh',
                    'label' => 'LBL_REPORT_AUTO_REFRESH',
                    'type' => 'enum',
                    'options' => 'dotb7_dashlet_reports_auto_refresh_options'
                ),
                array(
                    'name' => 'display_type',
                    'label' => 'LBL_DISPLAY_TYPE',
                    'type' => 'enum',
                    'options' => 'display_type_dashlet_option'
                ),
            )
        )
    ),
);
