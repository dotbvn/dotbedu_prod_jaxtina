({
    events: {
        'click #create-ticket': 'handleCreateTicket'
    },
    initialize: function (options) {
        this._super('initialize', [options]);
        // if (!window.autorefreshdashboard) {
        //     window.autorefreshdashboard = function () {
        //         $('[data-dashletaction="refreshClicked"]').click()
        //     }
        //     setInterval(window.autorefreshdashboard, 60000);
        // }
    },
    _renderHtml: function () {
        var _this = this;
        _this.callcenterInit();
        //init callcenter
        if (app.isSynced) {
            app.api.call("read", app.api.buildURL('callcenter/getScript'), null, {
                success: function (data) {
                    if (data.success === 1) {
                        window.callcenter_supplier = data.supplier;
                        window.show_phone_key_board = data.show_phone_key_board;
                        window.callcenter_manual_dial = data.manual_dial;
                        window.callcenter_list_ext = data.callcenter_list_ext;
                        window.has_callcenter = true;
                        if (data.supplier === 'Asterisk') {
                            $("body").append(data.html);
                        } else if (data.supplier === 'VoiceCloud') {
                            window.callcenter_callcenter_domain = data.callcenter_domain;
                            window.callcenter_domain = data.domain;
                            window.callcenter_key = data.key;
                            window.callcenter_ext = data.ext;
                            _this.callcenterInitVoiceCloud();
                        } else if (data.supplier === 'VoiceCloudV2') {
                            window.callcenter_callcenter_domain = data.callcenter_domain;
                            window.callcenter_domain = data.domain;
                            window.callcenter_key = data.key;
                            window.callcenter_ext = data.ext;
                            _this.callcenterInitVoiceCloudV2();
                        } else if (data.supplier === 'Voip24h') {
                            window.callcenter_ext = data.ext;
                            window.callcenter_ip = data.ip;
                            window.callcenter_key = data.key;
                            window.callcenter_secret = data.secret;
                            _this.callcenterInitVoip24h();
                        } else if (data.supplier === 'RTC') {
                            window.callcenter_ext = data.ext;
                            window.callcenter_ip = data.ip;
                            window.callcenter_key = data.key;
                            window.callcenter_secret = data.secret;
                            window.callcenter_realm = data.realm;
                            window.callcenter_context = data.context;
                            window.callcenter_chanel = data.chanel;
                            window.callcenter_list_ext = data.callcenter_list_ext;
                            _this.callcenterInitRTC();
                        } else if (data.supplier === 'newRTC') {
                            window.callcenter_ext = data.ext;
                            window.callcenter_ip = data.ip;
                            window.callcenter_key = data.key;
                            window.callcenter_secret = data.secret;
                            window.callcenter_realm = data.realm;
                            window.callcenter_context = data.context;
                            window.callcenter_chanel = data.chanel;
                            window.callcenter_list_ext = data.callcenter_list_ext;
                            _this.callcenterInitnewRTC();
                        } else if (data.supplier === 'omicall') {
                            window.callcenter_ext = data.ext;
                            window.callcenter_ip = data.ip;
                            window.callcenter_key = data.key;
                            window.callcenter_secret = data.secret;
                            window.callcenter_realm = data.realm;
                            window.callcenter_context = data.context;
                            window.callcenter_chanel = data.chanel;
                            window.callcenter_list_ext = data.callcenter_list_ext;
                            _this.callcenterInitOmicall();
                        } else if (data.supplier === 'SouthTelecom') {
                            window.callcenter_ext = data.ext;
                            window.callcenter_secret = data.secret;
                            window.callcenter_domain = data.callcenter_domain;
                            _this.callcenterInitSouthTelecom();
                        } else if (data.supplier === 'VCS') {
                            window.callcenter_ext = data.ext;
                            window.callcenter_secret = data.secret;
                            _this.callcenterInitVCS();
                        } else if (data.supplier === 'VCS3CX') {
                            _this.callcenterInitVCS3CX();
                        }
                    } else {
                        window.has_callcenter = false;
                    }
                    _this.show_phone_key_board = data.show_phone_key_board;
                    _this.callcenter_supplier = data.supplier;
                    _this._super('_renderHtml');
                }
            });
        }

        this.isAuthenticated = app.api.isAuthenticated();
        this._super('_renderHtml');
    },
    callcenterInit: function () {
        window.callcenter = {
            currentISOTime: function () {
                var dt;
                dt = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000);
                dt = dt.toISOString();
                dt = dt.replace('T', ' ');
                dt = dt.replace('Z', '');
                dt = dt.substr(0, 19);
                return dt;
            },
            getNowUTC: function () {
                var dt;
                dt = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000);
                dt = dt.toISOString();
                dt = dt.replace('T', ' ');
                dt = dt.replace('Z', '');
                dt = dt.substr(0, 19);
                return dt;
            },
            exist: function (phone) {
                var result = false;
                $('.callbox').each(function () {
                    if ($(this).is(':visible')) {
                        if ($(this).find('#phoneNumber').val() === phone) result = true;
                    }
                });
                return result;
            },
            createCallbox: function (phone, call_status, call_direction) {
                App.view.createView({type: 'callCenter', call_direction: call_direction, call_status: call_status, data: {phoneNumber: phone, beanName: '', beanId: ''}});
            },
            createCallbox2: function (phone, call_status, call_direction, callId) {
                App.view.createView({type: 'callCenter2', callId: callId, call_direction: call_direction, call_status: call_status, data: {phoneNumber: phone, beanName: '', beanId: ''}});
            },
            changeCallStatus: function (phone, call_status) {
                $('.callbox').each(function () {
                    var $this = $(this);
                    if ($this.find('#phoneNumber').val() === phone) {
                        $this.find('.call_status_callbox').text(call_status);
                        $this.find('.callStatus_callbox').val(call_status);

                        if (call_status === 'Connected') {
                            $this.find('.call_status_callbox').removeClass('label-inverse').addClass('label-success');
                            window.callcenter.countCallDuration($this);
                        } else if (call_status === 'Hangup' || call_status === "Rejected" || call_status === 'Canceled' || call_status === 'Busy') {
                            $this.find('.call_status_callbox').removeClass('label-success').addClass('label-important');
                            window.callcenter.stopCountCallDuration();
                        }
                    }
                });
            },
            changeCallStatusByBoxId: function (boxid, call_status) {
                var $this = $('#' + boxid);
                $this.find('.call_status_callbox').text(call_status);
                $this.find('.callStatus_callbox').val(call_status);

                if (call_status === 'Connected') {
                    $this.find('.call_status_callbox').removeClass('label-inverse').addClass('label-success');
                } else if (call_status === 'Hangup') {
                    $this.find('.call_status_callbox').removeClass('label-success').addClass('label-important');
                }
            },
            getCallboxWithPhoneAndDirection: function (phone, direction) {
                var result = false;
                $('.callbox').each(function () {
                    var $this = $(this);
                    if ($this.find('#phoneNumber').val() === phone && $this.find('#direction').val() === direction && $this.find('#callId').val() === '') {
                        result = $this.attr('id');
                    }
                });
                return result;
            },
            getCallboxWithCallid: function (callid) {
                var result = false;
                $('.callbox').each(function () {
                    var $this = $(this);
                    if ($this.find('#callId').val() === callid) {
                        result = $this.attr('id');
                    }
                });
                return result;
            },
            changeCallStatusByCallId: function (callId, call_status) {
                var $this = $('#' + callId);
                $this.find('.call_status_callbox').text(call_status);
                $this.find('.callStatus_callbox').val(call_status);

                if (call_status === 'Connected') {
                    $this.find('.call_status_callbox').removeClass('label-inverse').addClass('label-success');
                    window.callcenter.countCallDuration($this);
                } else if (call_status === 'Hangup') {
                    $this.find('.call_status_callbox').removeClass('label-success').addClass('label-important');
                    window.callcenter.stopCountCallDuration();
                }
            },
            setDataStart: function (phone, data) {
                $('.callbox').each(function () {
                    var $this = $(this);
                    if ($this.find('#phoneNumber').val() === phone) {
                        $this.find('#callId').val(data.callId);
                        $this.find('#ext').val(data.ext);
                        $this.find('#start').val(data.start);
                        $this.find('#start_timestamp').val(data.start_timestamp);
                        $this.find('#source').val(data.source);
                    }
                });
            },
            setDataEnd: function (phone, data) {
                $('.callbox').each(function () {
                    var $this = $(this);
                    if ($this.find('#phoneNumber').val() === phone) {
                        $this.find('#end').val(data.end);
                        $this.find('#end_timestamp').val(data.end_timestamp);
                        $this.find('#recording').val(data.recording);
                        $this.find('#duration').val(parseInt((parseInt(data.end_timestamp, 10) - parseInt($this.find('#start_timestamp').val(), 10)) / 1000, 10));
                        if (data.hasOwnProperty('callId')) $this.find('#callId').val(data.callId);
                    }
                });
            },
            setDataStartByBoxId: function (boxid, data) {
                var $this = $('#' + boxid);
                $this.find('#callId').val(data.callId);
                $this.find('#ext').val(data.ext);
                $this.find('#start').val(data.start);
                $this.find('#start_timestamp').val(data.start_timestamp);
                $this.find('#source').val(data.source);
            },
            setDataEndByBoxId: function (boxid, data) {
                var $this = $('#' + boxid);
                $this.find('#end').val(data.end);
                $this.find('#end_timestamp').val(data.end_timestamp);
                $this.find('#recording').val(data.recording);
                $this.find('#duration').val(parseInt((parseInt(data.end_timestamp, 10) - parseInt($this.find('#start_timestamp').val(), 10)) / 1000, 10));
            },
            save: function (phone) {
                $('.callbox').each(function () {
                    var $this = $(this);
                    if ($this.find('#phoneNumber').val() === phone) {
                        $this.find('.save_callbox').prop('disabled', false);
                        $this.find('.save_callbox').attr('data-is-auto-save', '1').click();
                    }
                });
            },
            saveByBoxId: function (boxid) {
                console.log(boxid);
                var $this = $('#' + boxid);
                if ($this.find('#id').val() !== '') {
                    $this.find('.save_callbox').prop('disabled', false);
                    $this.find('.save_callbox').attr('data-is-auto-save', '1').click();
                }
            },
            countCallDuration: function ($box) {
                var starttime = new Date().getTime();
                window.callcenter_count_duration = setInterval(function () {
                    var now = new Date().getTime();
                    var distance = now - starttime;
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    $box.find('.duration_callbox').text((distance / 1000 > 3600 ? ((hours < 10 ? '0' : '') + hours + ":") : '') + (minutes < 10 ? '0' : '') + minutes + ":" + (seconds < 10 ? '0' : '') + seconds);
                }, 1000);
            },
            stopCountCallDuration: function () {
                if (window.callcenter_count_duration) clearInterval(window.callcenter_count_duration);
            },
            setCallId: function (boxid, value) {
                $('#' + boxid).find('#callId').val(value);
            }
        };
    },
    callcenterInitVoip24h: function () {
        window.callcenter_supplier_voip24h = {
            clickToCall: function (phoneNumber) {
                $.ajax({
                    url: 'https://dial.voip24h.vn/dial?voip=' + window.callcenter_key + '&secret=' + window.callcenter_secret + '&sip=' + window.callcenter_ext + '&phone=' + phoneNumber,
                    type: 'get',
                    async: true,
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    },
                    success: function (res) {
                        console.log('click to call ' + phoneNumber + ' success!');
                    }
                });
            },
            initServer: function () {
                window.callcenter_socketio = io('https://voip24h-teh.s.dotb.vn', {"path": "", "transports": ["websocket"], "reconnection": true});
                window.callcenter_socketio.on('connect', function (e) {
                    window.callcenter_socketio.emit("login", window.callcenter_key + new Date().getTime());
                });
                window.callcenter_socketio.on('login', function () {
                    console.log('callcenter connected success!');
                });
                window.callcenter_socketio.on("message", function (response) {
                    response = JSON.parse(response);
                    console.log(response);
                    if (parseInt(response.extend, 10) === parseInt(window.callcenter_ext, 10)) {
                        var res = response;
                        if (res.state == 'Ring' && res.type == "inbound") {
                            App.view.createView({type: 'callCenter', data: {phoneNumber: res.phone, beanName: '', beanId: '', call_direction: 'Inbound'}});
                        } else if (!window.callcenter.exist(res.phone)) {
                            App.view.createView({type: 'callCenter', data: {phoneNumber: res.phone, beanName: '', beanId: '', call_direction: 'Inbound'}});
                        } else {
                            var hasphonet = false;
                            $('.callbox').each(function () {
                                if ($(this).find('#phoneNumber').val() == res.phone) {
                                    hasphonet = true;
                                }
                            });
                            if (hasphonet == false) {
                                App.view.createView({type: 'callCenter', data: {phoneNumber: res.phone, beanName: '', beanId: '', call_direction: 'Inbound'}});
                            }
                        }
                        var call_status = 'Waiting';
                        switch (res.state.toLowerCase()) {
                            case 'up':
                                call_status = 'Connected';
                                break;
                            case 'hangup':
                                call_status = 'Hangup';
                                window.call_saving = true;
                                break;
                        }

                        window.callcenter.changeCallStatus(res.phone, call_status);

                        if (call_status === 'Connected') {
                            window.callcenter.setDataStart(res.phone, {
                                callId: res.callid,
                                ext: res.extend,
                                source: 'Voip24h',
                                start_timestamp: new Date().getTime(),
                                start: window.callcenter.currentISOTime()
                            });
                        } else if (call_status === 'Hangup') {
                            window.callcenter.setDataEnd(res.phone, {
                                end_timestamp: new Date().getTime(),
                                end: window.callcenter.currentISOTime(),
                                recording: '::Voip24h::' + window.location.protocol + '//' + window.callcenter_ip + '/dial/searching?voip=' + window.callcenter_key + '&secret=' + window.callcenter_secret + '&callid=' + res.callid
                            });

                            //window.callcenter.save(res.phone);
                        }
                    }
                });
            }
        };
        window.callcenter_supplier_voip24h.initServer();
    },
    callcenterInitRTC: function () {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            console.log('getUserMedia supported.');
        } else {
            console.log('getUserMedia not supported on your browser!');
        }
        let socket = new JsSIP.WebSocketInterface(window.callcenter_ip);
        let configuration = {
            sockets: [socket],
            uri: window.callcenter_ext + '@' + window.callcenter_realm,
            password: window.callcenter_context
        };
        window.ua = new JsSIP.UA(configuration);
        window.ua.start();

        window.callcenter_supplier_RTC = {
            clickToCall: function (phone_number) {
                let eventHandlers = {
                    'progress': function (e) {
                        console.log('progress', e);
                        window.ua_audioFileID = '';
                    },
                    'failed': function (e) {
                        if (e.cause == "Canceled") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Canceled');
                        } else if (e.cause == "Busy") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Busy');
                        } else if (e.cause == "Rejected") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, "Rejected");
                        } else if (e.cause == "SIP Failure Code") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, "Rejected");
                        }
                        console.log('failed', e);
                        window.callcenter_supplier_RTC.removeHangup();
                        window.ua_audioFileID = '';
                    },
                    'ended': function (e) {
                        console.log('ended', e);
                        window.callcenter_supplier_RTC.removeHangup();
                        window.callcenter.changeCallStatus(window.current_ua_session._request.from._uri._user, 'Hangup');
                        window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Hangup');
                        window.callcenter.setDataEnd(window.current_ua_session._request.from._uri._user, {
                            end_timestamp: new Date().getTime(),
                            end: window.callcenter.currentISOTime(),
                            recording: '/upload/' + window.current_ua_session._request.call_id + '.ogg'
                        });
                        window.callcenter.setDataEnd(window.current_ua_session._request.to._uri._user, {
                            end_timestamp: new Date().getTime(),
                            end: window.callcenter.currentISOTime(),
                            recording: '/upload/' + window.current_ua_session._request.call_id + '.ogg'
                        });
                    },
                    'accepted': function (e) {
                        console.log('accepted', e);
                        $('.phoneboxnumber').remove();
                        window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Connected');
                        window.callcenter.setDataStart(window.current_ua_session._request.to._uri._user, {
                            callId: window.current_ua_session._request.call_id,
                            ext: window.callcenter_ext,
                            source: 'RTC',
                            start_timestamp: new Date().getTime(),
                            start: window.callcenter.currentISOTime()
                        });
                        window.ua_audioFileID = '';
                    }
                };

                let options = {
                    'eventHandlers': eventHandlers,
                    'mediaConstraints': {'audio': true}
                };

                window.current_ua_session = window.ua.call(phone_number, options);
                window.current_ua_session.connection.onaddstream = (e) => {
                    var audio = document.getElementById('audio');
                    audio.srcObject = e.stream;
                    audio.play();
                };
            },
            removeAnswer: function () {
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
                $('.call_answer_callbox').remove();
            },
            removeHangup: function () {
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
                $('.call_hangup_callbox').closest('table').remove();
                var $this = $('.callbox');
                $this.css('left', $(window).width() - $this.width() - 16);
                $this.css('top', -2 - $this.height());
            }
        };

        window.ua.on("newRTCSession", function (data) {
            window.current_ua_session = data.session;
            if (window.current_ua_session.direction === "incoming") {
                App.view.createView({type: 'callCenter', data: {phoneNumber: window.current_ua_session._request.from._uri._user, beanName: '', beanId: '', call_direction: 'Inbound'}});
                window.current_ua_session.on("confirmed", function (e) {
                    console.log('confirmed', e);
                    window.callcenter_supplier_RTC.removeAnswer();
                    window.callcenter.changeCallStatus(window.current_ua_session._request.from._uri._user, 'Connected');
                    window.callcenter.setDataStart(window.current_ua_session._request.from._uri._user, {
                        callId: window.current_ua_session._request.call_id,
                        ext: window.callcenter_ext,
                        source: 'RTC',
                        start_timestamp: new Date().getTime(),
                        start: window.callcenter.currentISOTime()
                    });
                    window.ua_audioFileID = '';
                });
                window.current_ua_session.on("ended", function (e) {
                    console.log('ended', e);
                    window.callcenter_supplier_RTC.removeHangup();
                    window.callcenter.changeCallStatus(window.current_ua_session._request.from._uri._user, 'Hangup');
                    window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Hangup');
                    window.callcenter.setDataEnd(window.current_ua_session._request.from._uri._user, {
                        end_timestamp: new Date().getTime(),
                        end: window.callcenter.currentISOTime(),
                        recording: '/upload/' + window.current_ua_session._request.call_id + '.ogg'
                    });
                    window.callcenter.setDataEnd(window.current_ua_session._request.to._uri._user, {
                        end_timestamp: new Date().getTime(),
                        end: window.callcenter.currentISOTime(),
                        recording: '/upload/' + window.current_ua_session._request.call_id + '.ogg'
                    });
                });
                window.current_ua_session.on("failed", function (e) {
                    console.log('failed', e);
                    window.callcenter_supplier_RTC.removeHangup();
                });
            }
        });
    },
    callcenterInitnewRTC: function () {
        /**
         * check browser support
         */
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        } else {
            window.console.error("getUserMedia not supported on your browser");
        }

        if (navigator.webkitGetUserMedia) {
        } else if (navigator.mozGetUserMedia) {
        } else if (navigator.getUserMedia) {
        } else {
            window.console.error("WebRTC support not found");
        }

        /**
         * config and start SIP UA
         */
        window.ua = new SIP.UA({
            password: window.callcenter_context,
            displayName: window.callcenter_ext,
            uri: 'sip:' + window.callcenter_ext + '@' + window.callcenter_realm,
            wsServers: window.callcenter_ip,
            registerExpires: 600,
            inviteWithoutSdp: true,
            keepAliveInterval: 1,
            connectionTimeout: 5,
            keepAliveDebounce: 10,
            renectionTimeout: 3,
            iceCheckingTimeout: 200,
            maxReconnectionAttempts: 10,
            traceSip: true,
            log: {
                level: 0,
                rel100: SIP.C.supported.SUPPORTED
            }
        });
        window.ua.on('registered', function (e) {
            console.log("Callcenter registered success!!!");
            if (SIP.WebRTC.isSupported()) {
                SIP.WebRTC.getUserMedia({
                    audio: true,
                    video: false
                }, function (stream) {
                    window.callcenter_stream = stream;
                }, function (e) {
                    window.console.error('getUserMedia failed:', e);
                });
            }
        });

        /**
         * in coming call
         */
        window.ua.on('invite', function (incomingSession) {
            window.callcenter_current_session = incomingSession;
            window.callcenter_current_phone = incomingSession.remoteIdentity.uri.user;
            App.view.createView({type: 'callCenter', data: {phoneNumber: window.callcenter_current_phone, beanName: '', beanId: '', call_direction: 'Inbound'}});
            incomingSession.on('accepted', function (e) {
                window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Connected');
                window.callcenter.setDataStart(window.callcenter_current_phone, {
                    callId: e.call_id,
                    ext: window.callcenter_ext,
                    source: 'newRTC',
                    start_timestamp: new Date().getTime(),
                    start: window.callcenter.currentISOTime()
                });
            });
            incomingSession.on('cancel', function (e) {
                window.callcenter_supplier_newRTC.removeHangup();
                window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Canceled');
                incomingSession = null;
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
            });
            incomingSession.on('bye', function (e) {
                window.callcenter_supplier_newRTC.removeHangup();
                window.callcenter_supplier_newRTC.removeAnswer();
                window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Hangup');
                window.callcenter.setDataEnd(window.callcenter_current_phone, {
                    end_timestamp: new Date().getTime(),
                    end: window.callcenter.currentISOTime(),
                    recording: ''
                });
                incomingSession = null;
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
            });
            incomingSession.on('failed', function (e) {
                window.callcenter_supplier_newRTC.removeHangup();
                window.callcenter_supplier_newRTC.removeAnswer();
                window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Canceled');
                incomingSession = null;
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
            });
            incomingSession.on('rejected', function (e) {
                window.callcenter_supplier_newRTC.removeHangup();
                window.callcenter_supplier_newRTC.removeAnswer();
                window.callcenter.changeCallStatus(window.callcenter_current_phone, "Rejected");
                incomingSession = null;
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
            });
        });

        /**
         * init click2call and handle outbound call
         */
        window.callcenter_supplier_newRTC = {
            clickToCall: function (phone_number) {
                window.callcenter_current_phone = phone_number;
                try {
                    var s = window.ua.invite(phone_number, {
                        media: {
                            stream: window.callcenter_stream,
                            constraints: {
                                audio: true,
                                video: false
                            },
                            render: {
                                remote: $('#audio').get()[0]
                            },
                            RTCConstraints: {
                                "optional": [{
                                    'DtlsSrtpKeyAgreement': 'true'
                                }]
                            }
                        }
                    });
                    s.direction = 'outgoing';

                    const newSession = function (newSess) {
                        window.callcenter_current_session = newSess;
                        newSess.on('accepted', function (e) {
                            console.log(e);
                            $('.phoneboxnumber').remove();
                            window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Connected');
                            window.callcenter.setDataStart(window.callcenter_current_phone, {
                                callId: e.call_id,
                                ext: window.callcenter_ext,
                                source: 'newRTC',
                                start_timestamp: new Date().getTime(),
                                start: window.callcenter.currentISOTime()
                            });
                        });
                        newSess.on('cancel', function (e) {
                            console.log(e);
                            window.callcenter_supplier_newRTC.removeHangup();
                            window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Canceled');
                            newSess = null;
                        });
                        newSess.on('bye', function (e) {
                            console.log(e);
                            window.callcenter_supplier_newRTC.removeHangup();
                            window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Hangup');
                            window.callcenter.setDataEnd(window.callcenter_current_phone, {
                                end_timestamp: new Date().getTime(),
                                end: window.callcenter.currentISOTime(),
                                recording: ''
                            });
                            newSess = null;
                        });
                        newSess.on('failed', function (e) {
                            console.log(e);
                            window.callcenter_supplier_newRTC.removeHangup();
                            window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Canceled');
                            newSess = null;
                        });
                        newSess.on('rejected', function (e) {
                            console.log(e);
                            window.callcenter_supplier_newRTC.removeHangup();
                            window.callcenter.changeCallStatus(window.callcenter_current_phone, "Rejected");
                            newSess = null;
                        });
                    };
                    newSession(s);
                } catch (e) {
                    throw (e);
                }
            },
            removeAnswer: function () {
                $('.call_answer_callbox').remove();
            },
            removeHangup: function () {
                $('.call_hangup_callbox').closest('table').remove();
                var $this = $('.callbox');
                $this.css('left', $(window).width() - $this.width() - 16);
                $this.css('top', -2 - $this.height());
            }
        };
    },
    callcenterInitOmicall: function () {
        //socket webhook
        window.callcenter_socketio = io.connect('https://socket.dotb.cloud', {"path": "", "transports": ["websocket"], "reconnection": true});
        window.callcenter_socketio.on('connect', function () {
            console.log('socket connected: ' + window.callcenter_socketio.id);
            window.callcenter_socketio.emit('login', App.config.uniqueKey + App.user.id);
        });
        window.callcenter_socketio.on('message', function (res) {
            if (res.dotb_status === 'log') {
                window.callcenter.setDataEnd(res.phone_number, {
                    end_timestamp: new Date().getTime(),
                    end: window.callcenter.currentISOTime(),
                    recording: res.recording_file,
                    callId: res.call_uuid
                });
                if (res.disposition === "answered") {
                    window.callcenter.save(res.phone_number);
                }
            }
        });

        //call RTC
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            console.log('getUserMedia supported.');
        } else {
            console.log('getUserMedia not supported on your browser!');
        }
        window.ua = new JsSIP.UA({
            sockets: [new JsSIP.WebSocketInterface(window.callcenter_ip)],
            uri: window.callcenter_realm,
            username: window.callcenter_ext,
            realm: window.callcenter_chanel,
            password: window.callcenter_context
        });

        window.ua.start();
        let isArray = arr => Array.isArray(arr) && !!arr.length;
        let sdpModifier = (sdp) => {
            let sdpObj = window.omiSdpParse(sdp);
            sdpObj.media.forEach(media => {
                let validCodecs = [];
                ['red', 'g711', 'pcma', 'pcmu', 'transport-cc', 'telephone-event'].forEach(codec => {
                    let codecs = media.rtp.filter(i => i.codec.toLowerCase() === codec);
                    if (isArray(codecs)) validCodecs.push(...codecs);
                });
                media.protocol = 'RTP/SAVPF';
                media.rtp = validCodecs;
                if (isArray(media.fmtp) && media.fmtp[0]) media.fmtp[0].payload = validCodecs[0].payload;
                if (isArray(media.rtcpFb) && media.rtcpFb[0]) media.rtcpFb[0].payload = validCodecs[0].payload;
                media.payloads = validCodecs.map(i => i.payload).join(' ');
            });
            return window.omiSdpWrite(sdpObj);
        };
        window.callcenter_supplier_omicall = {
            clickToCall: function (phone_number) {
                let eventHandlers = {
                    'sending': (e) => {
                        try {
                            let {request} = e;
                            let updateHeaderIdx = request.extraHeaders.findIndex(i => i.startsWith('Session-Expires'));
                            if (updateHeaderIdx > -1) request.extraHeaders[updateHeaderIdx] = `Session-Expires: 120`;
                            request.body = sdpModifier(request.body);
                        } catch (error) {
                            console.log(`🚀  Kds: error`, error);
                        }
                    },
                    'progress': function (e) {
                        window.ua_audioFileID = '';
                    },
                    'failed': function (e) {
                        console.log(e);
                        if (e.cause == "Canceled") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Canceled');
                        } else if (e.cause == "Busy") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Busy');
                        } else if (e.cause == "Rejected") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, "Rejected");
                        } else if (e.cause == "SIP Failure Code") {
                            window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, "Rejected");
                        }
                        window.callcenter_supplier_omicall.removeHangup();
                        window.ua_audioFileID = '';
                    },
                    'ended': function (e) {
                        console.log(e);
                        window.callcenter_supplier_omicall.removeHangup();
                        window.callcenter.changeCallStatus(window.current_ua_session._request.from._uri._user, 'Hangup');
                        window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Hangup');
                        window.callcenter.setDataEnd(window.current_ua_session._request.from._uri._user, {
                            end_timestamp: new Date().getTime(),
                            end: window.callcenter.currentISOTime(),
                            recording: '/upload/' + window.current_ua_session._request.call_id + '.ogg'
                        });
                        window.callcenter.setDataEnd(window.current_ua_session._request.to._uri._user, {
                            end_timestamp: new Date().getTime(),
                            end: window.callcenter.currentISOTime(),
                            recording: '/upload/' + window.current_ua_session._request.call_id + '.ogg'
                        });
                    },
                    'accepted': function (e) {
                        $('.phoneboxnumber').remove();
                        window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Connected');
                        window.callcenter.setDataStart(window.current_ua_session._request.to._uri._user, {
                            callId: window.current_ua_session._request.call_id,
                            ext: window.callcenter_ext,
                            source: 'omicall',
                            start_timestamp: new Date().getTime(),
                            start: window.callcenter.currentISOTime()
                        });
                        window.ua_audioFileID = '';
                    }
                };

                let options = {
                    'eventHandlers': eventHandlers,
                    'extraHeaders': [ 'User-Agent: OmiWebSDK'],
                    'mediaConstraints': {'audio': true},
                    'pcConfig': {
                        'iceServers': [
                            { urls: 'stun:stun.omicrm.com:3478' },
                            {urls: 'turn:turn.omicrm.com:2222?transport=tcp', username: 'vihat', credential: '834610100',},
                        ]
                    },
                };

                window.current_ua_session = window.ua.call('sip:' + phone_number + '@' + window.callcenter_chanel, options);
                window.current_ua_session.connection.onaddstream = (e) => {
                    var audio = document.getElementById('audio');
                    audio.srcObject = e.stream;
                    audio.play();
                };
            },
            removeAnswer: function () {
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
                $('.call_answer_callbox').remove();
            },
            removeHangup: function () {
                if (window.hasOwnProperty('callcenter_ringtone')) window.callcenter_ringtone.pause();
                $('.call_hangup_callbox').closest('table').remove();
                var $this = $('.callbox');
                $this.css('left', $(window).width() - $this.width() - 16);
                $this.css('top', -2 - $this.height());
            }
        };

        window.ua.on("newRTCSession", function (data) {
            if (data.session.direction === "incoming") {

                let userCurrentInCall = false
                let $boxs = $('.callbox');
                for (var i = 0; i < $boxs.length; i++) {
                    if ($($boxs[i]).find('.call_hangup_callbox').length > 0) {
                        userCurrentInCall = true;
                    }
                }
                if (userCurrentInCall) return;

                window.current_ua_session = data.session;
                App.view.createView({type: 'callCenter', data: {phoneNumber: window.current_ua_session._request.from._uri._user, beanName: '', beanId: '', call_direction: 'Inbound'}});
                window.current_ua_session.on("confirmed", function (e) {
                    window.callcenter_supplier_omicall.removeAnswer();
                    window.callcenter.changeCallStatus(window.current_ua_session._request.from._uri._user, 'Connected');
                    window.callcenter.setDataStart(window.current_ua_session._request.from._uri._user, {
                        callId: window.current_ua_session._request.call_id,
                        ext: window.callcenter_ext,
                        source: 'omicall',
                        start_timestamp: new Date().getTime(),
                        start: window.callcenter.currentISOTime()
                    });
                    window.ua_audioFileID = '';
                });
                window.current_ua_session.on("ended", function (e) {
                    window.callcenter_supplier_omicall.removeHangup();
                    window.callcenter.changeCallStatus(window.current_ua_session._request.from._uri._user, 'Hangup');
                    window.callcenter.changeCallStatus(window.current_ua_session._request.to._uri._user, 'Hangup');
                    window.callcenter.setDataEnd(window.current_ua_session._request.from._uri._user, {
                        end_timestamp: new Date().getTime(),
                        end: window.callcenter.currentISOTime(),
                        recording: ''
                    });
                    window.callcenter.setDataEnd(window.current_ua_session._request.to._uri._user, {
                        end_timestamp: new Date().getTime(),
                        end: window.callcenter.currentISOTime(),
                        recording: ''
                    });
                });
                window.current_ua_session.on("failed", function (e) {
                    window.callcenter_supplier_omicall.removeHangup();
                });
            }
        });
    },
    callcenterInitVoiceCloud: function () {
        window.callcenter_supplier_voicecloud = {
            clickToCall: function (phoneNumber) {
                $.ajax({
                    url: window.callcenter_callcenter_domain + '/api/CallControl/dial/from_number/' + window.callcenter_ext + '/to_number/' + phoneNumber + '/key/' + window.callcenter_key + '/domain/' + window.callcenter_domain,
                    type: 'get',
                    async: true,
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    },
                    success: function (res) {
                        if (res !== 'Success') {
                            window.callcenter.changeCallStatus(phoneNumber, 'Hangup');
                            return;
                        }
                        if (window.call_saving) return;
                        window.callcenter.changeCallStatus(phoneNumber, 'Connected');
                        window.callcenter.setDataStart(phoneNumber, {
                            callId: new Date().getTime(),
                            ext: window.callcenter,
                            source: 'VoiceCloud',
                            start_timestamp: new Date().getTime(),
                            start: window.callcenter.currentISOTime()
                        });
                    }
                });
            },
            initServer: function () {
                window.callcenter_socketio = io.connect('https://voicecloud-sylvan.s.dotb.vn');
                window.callcenter_socketio.emit('login', JSON.stringify({user: App.user.id, key: App.config.uniqueKey}));
                window.callcenter_socketio.on('login', function (res) {
                    res = JSON.parse(res);
                });
                window.callcenter_socketio.on('message', function (res) {
                    res = JSON.parse(res);

                    if (window.call_saving) return;
                    window.callcenter.changeCallStatus(res.phone, 'Hangup');
                    if (res.status !== "ANSWER") {
                        toastr.warning('Call log not saved because call is not connected yet!');
                        return;
                    }

                    window.callcenter.setDataEnd(res.phone, {
                        end_timestamp: new Date().getTime(),
                        end: window.callcenter.currentISOTime(),
                        recording: res.recordingfile,
                    });

                    window.callcenter.save(res.phone);
                });
            }
        };
        window.callcenter_supplier_voicecloud.initServer();
    },
    callcenterInitVoiceCloudV2: function () {
        window.callcenter_supplier_voicecloudv2 = {
            clickToCall: function (phoneNumber) {
                $.ajax({
                    url: window.callcenter_callcenter_domain + '/api/CallControl/dial/from_number/' + window.callcenter_ext + '/to_number/' + phoneNumber + '/key/' + window.callcenter_key + '/domain/' + window.callcenter_domain,
                    type: 'get',
                    async: true,
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    },
                    success: function (res) {
                        if (res !== 'Success') {
                            window.callcenter.changeCallStatus(phoneNumber, 'Hangup');
                            return;
                        }
                        if (window.call_saving) return;
                        window.callcenter.changeCallStatus(phoneNumber, 'Connected');
                        window.callcenter.setDataStart(phoneNumber, {
                            callId: new Date().getTime(),
                            ext: window.callcenter_ext,
                            source: 'VoiceCloudV2',
                            start_timestamp: new Date().getTime(),
                            start: window.callcenter.currentISOTime()
                        });
                    }
                });
            },
            initServer: function () {
                window.callcenter_socketio = io.connect('https://socket.dotb.cloud', {"path": "", "transports": ["websocket"], "reconnection": true});
                window.callcenter_socketio.on('connect', function () {
                    window.callcenter_socketio.emit('join', App.config.uniqueKey + App.user.id);
                    console.log('Socket connected: ' + window.callcenter_socketio.id);
                });
                window.callcenter_socketio.on('event', function (res) {
                    if (res.dotb_site === 'sylvan') {
                        if (res.dotb_status === 'log') {//save call
                            if (window.call_saving) return;
                            window.callcenter.changeCallStatus(res.phone, 'Hangup');
                            window.callcenter.setDataEnd(res.phone, {
                                end_timestamp: new Date().getTime(),
                                end: window.callcenter.currentISOTime(),
                                recording: res.recordingfile,
                                callId: res.callid
                            });
                            if (res.status === "ANSWER") {//có nghe máy mới tự động lưu
                                window.callcenter.save(res.phone);
                            }
                        } else if (res.dotb_status === 'callin' && res.event === 'Ring') {//open popup call-in
                            App.view.createView({type: 'callCenter', data: {phoneNumber: res.phone, beanName: '', beanId: '', call_direction: 'Inbound'}});
                            window.callcenter.setDataStart(res.phone, {
                                callId: res.callid,
                                ext: window.callcenter_ext,
                                source: 'VoiceCloudV2',
                                start_timestamp: new Date().getTime(),
                                start: window.callcenter.currentISOTime()
                            });
                        } else if (res.dotb_status === 'callin' && res.event === "Answer") {
                            window.callcenter.changeCallStatus(res.phone, 'Connected');
                        }
                    }
                });
            }
        };
        window.callcenter_supplier_voicecloudv2.initServer();
    },
    callcenterInitSouthTelecom: function () {
        window.callcenter_supplier_southtelecom = {
            clickToCall: function (phoneNumber) {
                window.callcenter_current_phone = phoneNumber;
                window.callcenter_current_status = 'waiting';
                $.ajax({
                    url: window.callcenter_domain + '?callernum=' + window.callcenter_ext + '&destnum=' + phoneNumber + '&secrect=' + window.callcenter_secret,
                    type: 'get',
                    async: true,
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    },
                    success: function (res) {
                    }
                });
            },
            initServer: function () {
                window.callcenter_socketio = io.connect('https://socket.dotb.cloud', {"path": "", "transports": ["websocket"], "reconnection": true});
                window.callcenter_socketio.on('connect',()=>{
                    console.log('Socket connected: '+window.callcenter_socketio.id);
                    window.callcenter_socketio.emit('join', window.callcenter_secret + '_' + window.callcenter_ext);
                });
                window.callcenter_socketio.on('event', function (res) {
                    if (res.callstatus === 'Dialing' && window.callcenter_current_status === 'waiting' && res.agentname === window.callcenter_ext) {
                        window.callcenter_current_status = 'Connected';
                        window.callcenter.changeCallStatus(res.destinationnumber, 'Connected');
                        window.callcenter.setDataStart(res.destinationnumber, {
                            callId: res.calluuid,
                            ext: window.callcenter_ext,
                            source: 'SouthTelecom',
                            start_timestamp: new Date().getTime(),
                            start: window.callcenter.currentISOTime()
                        });
                        window.current_callid = res.calluuid;
                    } else if (res.callstatus === 'HangUp' && window.callcenter_current_status === 'Connected' && window.current_callid === res.calluuid) {
                        if (window.call_saving) return;
                        window.callcenter.changeCallStatus(window.callcenter_current_phone, 'Hangup');

                        window.callcenter.setDataEnd(window.callcenter_current_phone, {
                            end_timestamp: new Date().getTime(),
                            end: window.callcenter.currentISOTime(),
                            recording: 'https://apps.worldfone.vn/externalcrm/playback2.php?calluuid=' + res.calluuid + '&secrect=' + window.callcenter_secret,
                        });
                        window.callcenter.save(window.callcenter_current_phone);
                    }
                });
            }
        };
        window.callcenter_supplier_southtelecom.initServer();
    },
    callcenterInitVCS: function () {
        window.callcenter_supplier_vcs = {
            clickToCall: function (phoneNumber) {
                window.callcenter_current_phone = phoneNumber;
                app.api.call("create", app.api.buildURL('callcenter/vcs/click2call'), {
                    ext: window.callcenter_ext,
                    phone: phoneNumber
                }, {
                    success: function (res) {
                    }
                });
            },
            initServer: function () {
                window.callcenter_socketio = io.connect('https://service.dotb.cloud:3005');
                window.callcenter_socketio.emit('login', JSON.stringify({user: App.user.id, key: App.config.uniqueKey}));
                window.callcenter_socketio.on('login', function (res) {
                    res = JSON.parse(res);
                });
                window.callcenter_socketio.on('message', function (res) {
                    res = JSON.parse(res);
                    res = res.data;

                    if (res.from === window.callcenter_ext || res.to === window.callcenter_ext || res.event === 'cancel' || res.event === 'busy_reject') {
                        var callboxId = window.callcenter.getCallboxWithCallid(res.callid);
                        if (res.from === window.callcenter_ext) {
                            if (callboxId === false) {
                                callboxId = window.callcenter.getCallboxWithPhoneAndDirection(res.to, 'Outbound');
                            }
                            if (res.event === 'ringing' && callboxId === false) {
                                window.callcenter.createCallbox2(res.to, 'Ringing', 'Outbound', res.callid);
                            }
                            if (callboxId) {
                                $('#' + callboxId).find('#callId').val(res.callid);
                            }
                        } else if (res.from.length > 5) {
                            if (callboxId === false) {
                                callboxId = window.callcenter.getCallboxWithPhoneAndDirection(res.from, 'Inbound');
                            }
                            if (res.event === 'ringing' && callboxId === false) {
                                window.callcenter.createCallbox2(res.from, 'Ringing', 'Inbound', res.callid);
                            }
                            if (callboxId) {
                                $('#' + callboxId).find('#callId').val(res.callid);
                            }
                        }

                        switch (res.event) {
                            case 'connected':
                                window.callcenter.changeCallStatusByBoxId(callboxId, 'Connected');
                                window.callcenter.setDataStartByBoxId(callboxId, {
                                    callId: res.callid,
                                    ext: window.callcenter_ext,
                                    source: 'VCS',
                                    start_timestamp: new Date().getTime(),
                                    start: window.callcenter.getNowUTC()
                                });
                                break;
                            case 'bye':
                                window.callcenter_current_status = 'HangUp';
                                if (window.call_saving) return;
                                window.callcenter.changeCallStatusByBoxId(callboxId, 'Hangup');
                                window.callcenter.setDataEndByBoxId(callboxId, {
                                    end_timestamp: new Date().getTime(),
                                    end: window.callcenter.getNowUTC(),
                                    recording: '',
                                });
                                window.callcenter.saveByBoxId(callboxId);
                                break;
                            case 'cancel':
                                window.callcenter.changeCallStatusByBoxId(callboxId, 'Cancel');
                                break;
                            case 'busy_reject':
                                window.callcenter.changeCallStatusByBoxId(callboxId, 'Busy');
                                break;
                        }
                    }
                });
            }
        };
        window.callcenter_supplier_vcs.initServer();
    },
    callcenterInitVCS3CX: function () {
        window.callcenter_supplier_vcs3cx = {
            clickToCall: function (phoneNumber) {
                window.callcenter_current_phone = phoneNumber;
                window.callcenter3cx.Send_MakeCall_3CX(phoneNumber);
            },
            initServer: function () {
                window.callcenter3cx = new pbx_3CX();

                window.callcenter3cx.Event_Dialing_3CX = function (orther, current) {
                    var callboxId = window.callcenter.getCallboxWithCallid(orther.CallID);
                    if (callboxId === false) {
                        callboxId = window.callcenter.getCallboxWithPhoneAndDirection(orther.OtherPartyNumber, 'Outbound');
                        if (callboxId === false) {
                            window.callcenter.createCallbox2(orther.OtherPartyNumber, 'Ringing', 'Outbound', orther.CallID);
                        }
                    }
                    window.callcenter_current_status = 'Dialing';
                }
                window.callcenter3cx.Event_Ringing_3CX = function (orther, current) {
                    var callboxId = window.callcenter.getCallboxWithCallid(orther.CallID);
                    if (callboxId === false) {
                        callboxId = window.callcenter.getCallboxWithPhoneAndDirection(orther.OtherPartyNumber, 'Inbound');
                        if (callboxId === false) {
                            window.callcenter.createCallbox2(orther.OtherPartyNumber, 'Ringing', 'Inbound', orther.CallID);
                        }
                    }
                    window.callcenter_current_status = 'Ringing';
                }
                window.callcenter3cx.Event_Connected_3CX = function (orther, current) {
                    console.log(orther);
                    console.log(current);
                    var callboxId = window.callcenter.getCallboxWithCallid(orther.CallID);
                    if (callboxId === false) {
                        callboxId = window.callcenter.getCallboxWithPhoneAndDirection(orther.OtherPartyNumber, 'Outbound');
                    }
                    if (callboxId === false) {
                        callboxId = window.callcenter.getCallboxWithPhoneAndDirection(orther.OtherPartyNumber, 'Inbound');
                    }
                    if (callboxId === false) return;
                    window.callcenter.changeCallStatusByBoxId(callboxId, 'Connected');
                    window.callcenter.setDataStartByBoxId(callboxId, {
                        callId: orther.CallID,
                        ext: current.Number,
                        source: 'VCS3CX',
                        start_timestamp: new Date().getTime(),
                        start: window.callcenter.getNowUTC()
                    });
                    window.callcenter_current_status = 'Connected';
                }
                window.callcenter3cx.Event_Ended_3CX = function (orther, current) {
                    var callboxId = window.callcenter.getCallboxWithCallid(orther.CallID);
                    if (callboxId === false) {
                        callboxId = window.callcenter.getCallboxWithPhoneAndDirection(orther.OtherPartyNumber, 'Outbound');
                    }
                    if (callboxId === false) {
                        callboxId = window.callcenter.getCallboxWithPhoneAndDirection(orther.OtherPartyNumber, 'Inbound');
                    }
                    if (callboxId === false || window.call_saving) return;
                    if (window.callcenter_current_status === 'Connected') {
                        window.callcenter.changeCallStatusByBoxId(callboxId, 'Hangup');
                        window.callcenter.setDataEndByBoxId(callboxId, {
                            end_timestamp: new Date().getTime(),
                            end: window.callcenter.getNowUTC(),
                            recording: '',
                        });
                        window.callcenter.saveByBoxId(callboxId);
                    } else if (window.callcenter_current_status === 'Dialing') {
                        window.callcenter.changeCallStatusByBoxId(callboxId, 'Cancel');
                    } else if (window.callcenter_current_status === 'Ringing') {
                        window.callcenter.changeCallStatusByBoxId(callboxId, 'Cancel');
                    }
                    window.callcenter_current_status = '';
                }

                window.callcenter3cx.Start();
            }
        };
        window.callcenter_supplier_vcs3cx.initServer();
    },
    handleCreateTicket: function (){
        var self = this;
        app.drawer.open({
            layout: 'create',
            context: {
                create: true,
                module: "Bugs"
            }
        }, function(context, model) {
            if (!model) {
                return;
            }
            self.context.resetLoadFlag();
            self.context.set('skipFetch', false);
            if (_.isFunction(self.loadData)) {
                self.loadData();
            } else {
                self.context.loadData();
            }
            app.router.navigate('Bugs/' + model.id, {trigger: true});
        });
    }
})
