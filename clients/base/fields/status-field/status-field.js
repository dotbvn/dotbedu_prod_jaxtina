/**
 * EventStatusField is a field for Meetings/Calls that show the status field of the model as a badge field.
 *
 * FIXME: This component will be moved out of clients/base folder as part of MAR-2274 and SC-3593
 *
 * @class View.Fields.Base.EventStatusField
 * @alias DOTB.App.view.fields.BaseEventStatusField
 * @extends View.Fields.Base.BadgeSelectField
 */
({
    extendsFrom: 'BadgeSelectField',

    /**
     * @inheritdoc
     */
    initialize: function (options) {
        this._super('initialize', [options]);
        this.statusClasses = {
            'Held': 'label-success',
            'Not Held': 'label-important',
            'Planned': 'label-pending',
        };
        if(this.def.colorClass) {
            this.statusClasses = this.def.colorClass;
        }
        this.type = 'badge-select';
    },

    /**
     * @inheritdoc
     */
    _loadTemplate: function () {
        var action = this.action || this.view.action;
        if (action === 'edit') {
            this.type = 'enum';
        }

        this._super('_loadTemplate');
        this.type = 'badge-select';
    }
})
