/**
 * @class View.Fields.Base.ImageField
 * @alias DOTB.App.view.fields.BaseImageField
 * @extends View.Fields.Base.BaseField
 */
({
    fileUploadDiv: '#fileUpload',
    countFile: 0,
    tableBody: '#table-file tbody',
    iconList: {
        'docx': 'fa-file-word-o',
        'txt': 'fa-file-text',
        'excel': 'fa-file-excel-o',
        'video': 'fa-file-video-o',
        'pptx': 'fa-file-powerpoint-o',
        'audio': 'fa-file-audio-o',
        'pdf': 'fa-file-pdf-o',
        'zip': 'fa-file-archive-o',
        'others': 'fa-file'
    },
    events: {
        'change #file-s3-input': 'handleFileChanged',
        'dragover .file-upload': 'dragOverEvt',
        'dragleave .file-upload': 'dragLeaveEvt',
        'drop .file-upload': 'dropEvt',
        'click .delete-file': 'handleDeleteFile',
        'click .browseButton': 'handleFileBrowsed',
        'click .img-preview': 'popupImage',
        'click #download_all': 'downloadZip',
        'click .file-download': 'handleDownloadFile',
    },
    initialize: function (options) {
        app.view.Field.prototype.initialize.call(this, options);
        if(this.view.action === 'edit'){
            this.checkConnectS3Storage();
        }
        this.delete_list = [];
    },

    _render: function () {
        this._super('_render')
    },
    bindDomChange: function() {
        if(this.context.attributes.modelId && !this.onLoad){ // if model is saved -> load note list
            $('#loading-note').show();
            this.delete_list = [];
            this.countFile = 0;
            this.renderNoteList();
        }
    },
    renderNoteList: function () {
        this.onLoad = true;
        this._post({
            formData: {
                action: 'getNote',
                parentModule: this.context.attributes.module,
                parentId: this.context.attributes.modelId
            },
            successCallback: function (response) {
                let noteList = response.data.note_list;
                $('#loading-note').hide();
                if (noteList.length > 0) {
                    $('#download_all').show();
                    this.hideNoneSelected();
                    noteList.forEach((note, index) => {
                        this.addFileRow(note, index);
                    });
                } else if (noteList.length === 0) {
                    $('#download_all').hide();
                }
                if (this.countFile === noteList.length) {
                    this.onLoad = false;
                }
            }.bind(this),
            errorCallback: function (error) {
                console.error('Error retrieving:', error);
            }
        });
    },
    handleFileChanged: function (e) {
        let files = e.target.files;
        this.handleFileSelected(files);
    },
    handleFileBrowsed: function (e) {
        $('#file-s3-input').click();
    },
    handleFileSelected: function (files) {
        if (files.length > 0) {
            this.hideNoneSelected();
            $.each(files, _.bind(function (index, file) {
                var fileSize = (this.convertKBtoMB((file.size / 1024))).toFixed(2);
                var fileType = file.type;
                if (this.def.imageAndVideoOnly && !fileType.startsWith("image") && !fileType.startsWith("video")) {
                    app.alert.show('error', {
                        level: 'error',
                        messages: app.lang.get('LBL_ALERT_IMAGE_ONLY'),
                        autoClose: true
                    });
                    return;
                }

                if (fileSize > 100) {
                    app.alert.show('error', {
                        level: 'error',
                        messages: app.lang.get('LBL_ALERT_FILE_SIZE_LIMIT'),
                        autoClose: true
                    });
                    return;
                }

                this.addFileRow(file, this.countFile);
            }, this));
        }
    },
    setStartTime: function(countFile){
        var currentTime = new Date().getTime();
        $(`#file-upload-${countFile}`).attr("data-startTime", currentTime);
    },
    addFileRow: function (file, countFile) {
        var fileName = file.name;
        var fileSize = (this.convertKBtoMB((file.size / 1024))).toFixed(2);
        var fileType = file.type;
        var icon = this.getFileIcon(fileName, fileType);
        let fileSrc = "";

        this.countFile++;

        if (this.onLoad) {
            fileSrc = _.isEmpty(file.src) ? '' : file.src;
        } else {
            if (!file.id) { // file is not saved
                fileSrc = URL.createObjectURL(file);
            }
        }

        var preview = fileType.startsWith("image")
            ? `<img class="img-preview" src="${fileSrc}" alt="${fileName}" height="30">`
            : `<i class="fa ${icon}"></i>`;

        let fileModel = {
            fileName: fileName,
            fileSize: fileSize,
            fileType: fileType,
            preview: preview,
            countFile: countFile,
            fileSrc: fileSrc,
            showDownload: this.view.action === 'detail'
        };

        $.get('clients/base/fields/s3-multi/row.hbs', function (template) {
            var compiledTemplate = Handlebars.compile(template);
            var htmlRow = compiledTemplate(fileModel);
            $(this.tableBody).append(htmlRow);
            this.setStartTime(countFile);
            if (file.id !== undefined) {
                let fileSelector = "#file-upload-" + countFile;
                $(fileSelector).attr("data-noteId", file.id);
                $(fileSelector).attr("data-saved", true);
            }
        }.bind(this));

        if (!this.onLoad) {
            this.uploadFile(file, countFile);
        }
    },
    getFileExtension: function (filename) {
        if(filename.includes('zip')){
            return 'zip';
        }
        return filename.slice(((filename.lastIndexOf(".") - 1) >>> 0) + 2);
    },
    getFileIcon: function (fileName, fileType) {
        let fileIcon = 'others';
        if (fileType.startsWith("video")) {
            fileIcon = 'video';
        }
        let extension = this.getFileExtension(fileName);
        switch (extension) {
            case 'csv':
            case 'xls':
            case 'xlsx':
                fileIcon = 'excel';
                break;
            case 'docx':
            case 'doc':
                fileIcon = 'docx';
                break;
            case 'txt':
                fileIcon = 'txt';
                break;
            case 'pptx':
                fileIcon = 'pptx';
                break;
            case 'pdf':
                fileIcon = 'pdf';
                break;
            case 'zip':
            case 'rar':
                fileIcon = 'zip';
                break;
        }
        return this.iconList[fileIcon];
    },
    checkConnectS3Storage: function () {
        this._post({
            formData: {
                action: 'checkConnect'
            },
            successCallback: function (response) {
                if (!response.success) {
                    $('.file-upload').html(`<div>Can't connect to storage service please contact your administrator</div>`)
                }
            },
            errorCallback: function (error) {
                console.error('Connect failed', error);
            }
        });
    },
    updateModelS3: function () {
        let fileList = $('#table-file tbody tr');
        let noteList = [];
        fileList.each(function(index, row) {
            var fileElement = $(row).find('.file-element');
            var noteObject = {
                id: fileElement.data('noteid'),
                action: 'add'
            };
            noteList.push(noteObject);
        });
        _.each(this.delete_list, function (file) {
            var noteObject = {
                id: file,
                action: 'delete'
            };
            noteList.push(noteObject);
        })
        this.model.set(this.name, noteList);
    },
    hideNoneSelected: function () {
        $('#no-file').hide();
    },
    showNoneSelected: function () {
        $('#no-file').show();
    },
    uploadFile: function (file, countFile) {
        var formData = new FormData();
        let self = this;

        formData.append('file', file);
        formData.append('action', 'createNote');
        formData.append('parentModule', self.context.attributes.module);
        formData.append('userId', app.user.id);

        $.ajax({
            url: 'index.php?entryPoint=uploadFile',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            xhr: _.bind(function () {
                var xhr = new XMLHttpRequest();
                // Track upload progress
                xhr.upload.addEventListener("progress", _.bind(function (event) {
                    this.updateProgress(event, countFile);
                }, this), false);
                return xhr;
            }, this),
            success: _.bind(function (response) {
                // Handle successful upload
                var response = JSON.parse(response);
                this.completeHandler(response, countFile);
            }, this),
            error: _.bind(function (error) {
                // Handle upload error
                console.error('Upload failed:', error);
                this.uploadFailed(countFile);
            }, this)
        });
    },
    uploadFailed: function (countFile) {
        let progressSelector = "#progressBar-" + countFile;
        $(progressSelector).hide();
        $(`#file-upload-${countFile} .file-size-contain`).text('Failed to upload')
    },
    updateProgress: function (event, countFile) {
        if (event.lengthComputable) {
            let progressSelector = "#progressBar-" + countFile;
            let timeSelector = "#time-left-" + countFile;
            let percentSelector = "#upload-percent-" + countFile;

            var currentTime = new Date().getTime();
            var elapsedTime = (currentTime - $(`#file-upload-${countFile}`).data('starttime')) / 1000; // in seconds
            var totalSize = event.total;
            var loadedSize = event.loaded;
            var speed = loadedSize / elapsedTime; // bytes per second
            var remainingTime = (totalSize - loadedSize) / speed; // in seconds

            // Display time left in a readable format (e.g., MM:SS)
            var minutes = Math.floor(remainingTime / 60);
            var seconds = Math.floor(remainingTime % 60);
            var timeLeft = minutes + ':' + (seconds < 10 ? '0' : '') + seconds;

            var percentage = (loadedSize / totalSize) * 100;
            var percent = (event.loaded / event.total) * 100;
            if(percentage === 100) {
                percentage = 99; // to avoid 100% when upload complete
            }

            $(timeSelector).html(timeLeft + " second left");
            $(progressSelector).val(Math.round(percent));
            $(percentSelector).html(percentage+"%");
        }
    },
    dragOverEvt: function(evt) {
        evt.preventDefault();
        $('.file-upload').addClass("onDragover");
        $(this.fileUploadDiv).toggleClass("dragover", evt.type === "dragover");
    },
    dragLeaveEvt: function (evt) {
        evt.preventDefault();
        $('.file-upload').removeClass("onDragover");
        $(this.fileUploadDiv).removeClass("dragover");
    },
    dropEvt: function (evt) {
        evt.preventDefault();
        $('.file-upload').removeClass("onDragover");
        $(this.fileUploadDiv).removeClass("dragover");
        this.handleFileSelected(evt.originalEvent.dataTransfer.files);
    },
    completeHandler: function (response, countFile) {
        let progressSelector = "#progressBar-" + countFile;
        let percentSelector = "#upload-percent-" + countFile;
        let timeLeft = "#time-left-" + countFile;
        let fileSelector = "#file-upload-" + countFile;
        // Successful upload
        var id = response.id;
        if (response.success === true) {
            $(progressSelector).val(100)
            $(percentSelector).html("100%");
            $(timeLeft).html("Upload successfully")
            $(fileSelector).attr("data-noteId", id);
            this.updateModelS3();
        }
        if (response.error_num === 102) {
            this.uploadFailed(countFile);
        }
    },
    removeRow: function (target) {
        target.closest("tr").remove();
        this.countFile--;
    },
    handleDeleteFile: function (evt) {
        let noteId = evt.target.parentElement.parentElement.parentElement.dataset.noteid;
        let saved = evt.target.parentElement.parentElement.parentElement.dataset.saved;

        this.removeRow(evt.target);

        if(!saved){
            this._post({
                formData: {
                    action: 'deleteNote',
                    noteId: noteId
                },
                successCallback: _.bind(function (response) {
                    if(response.success){
                        console.log('Delete successfully')
                        this.updateModelS3();
                    }
                }, this),
                errorCallback: function (error) {
                    console.log('Delete failed')
                }
            });
        } else {
            this.delete_list.push(noteId)
            this.updateModelS3();
        }
        if(this.countFile === 0){
            this.showNoneSelected();
        }

    },
    popupImage: function (evt) {
        var sourceImage = evt.target.src;
        var width = evt.target.naturalWidth;
        var height = evt.target.naturalHeight;
        if(width > 1000 || height > 1000){
            width = width / 2;
            height = height / 2;
        }
        var data = `<div style="background-color: #fff;
                height: ${height}px; width: ${width}px;
                border-radius: 3px;
                box-shadow: 5px 5px 20px rgba(0,0,0,0.05);
                position: relative;"><img style="height: ${height}px; width: ${width}px;" src="${sourceImage}"></div>`;
        window.top.$.openPopupLayer({
            name: "imagePopup",
            html: data
        });
    },
    downloadZip: function (evt) {
        var zip = new JSZip();
        var self = this; // Capture 'this' to use in callbacks
        $(evt.target).hide();
        $('#zip-downloading').show(); // Ensure this ID matches your loading element

        this._post({
            formData: {
                action: 'downloadZip',
                parentModule: this.context.attributes.module,
                parentId: this.context.attributes.modelId,
                parentName: this.context.attributes.model.get('name')
            },
            successCallback: function (response) {
                var promises = []; // Collect all promises here
                _.each(response.data.file_list, function (file) {
                    var promise = fetch(file.file_source)
                        .then(response => response.blob())
                        .then(blob => {
                            zip.file(file.file_name, blob);
                        }).catch(error => console.error('Error fetching file:', error));
                    promises.push(promise);
                });

                Promise.all(promises).then(function() {
                    let zipName = self.context.attributes.model.get('name') + '.zip'; // Moved zipName here
                    zip.generateAsync({type: "blob"})
                        .then(function(content) {
                            // Assuming you have FileSaver.js included in your project
                            saveAs(content, zipName);
                            $(evt.target).show();
                            $('#zip-downloading').hide(); // Hide loading indicator
                        });
                });
            },
            errorCallback: function (error) {
                console.log('Download failed');
                $(evt.target).show();
                $('#zip-downloading').hide(); // Ensure proper UI response on error
            }
        });
    },
    _post: function ({ formData, successCallback, errorCallback }) {
        $.ajax({
            url: 'index.php?entryPoint=uploadFile',
            type: 'POST',
            dataType: 'json',
            data: formData,
            success: successCallback,
            error: errorCallback
        });
    },
    convertKBtoMB: function (kilobytes) {
        return kilobytes / 1024;
    },
    handleDownloadFile: function (evt) {
        $(evt.target).hide();
        $(evt.target.nextSibling.nextSibling).show();
        this._post(     {
            formData: {
                action: 'downloadNotes',
                noteId: evt.target.parentNode.parentNode.parentNode.parentNode.dataset.noteid
            },
            successCallback: function (response) {
                var a = document.createElement('a');
                a.download = evt.target.dataset.download;
                a.href = response.data.file_source;
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                $(evt.target).show();
                $(evt.target.nextSibling.nextSibling).hide();
                console.log('Download successfully');
            },
            errorCallback: function (error) {
                console.log('Download failed')
            }
        });
    }
})
