<?php
/// Add by Lê Minh
class CustomSuggestLocationAPI extends DotbApi
{
    public function registerApiRest()
    {
        return array(
            'lead-viet-nam-location' => array(
                'reqType' => 'PUT',
                'path' => array('leads', 'vietnam-location'),
                'pathVars' => array(),
                'method' => 'GetVietnamLocation'
            ),
        );
    }

    public function GetVietnamLocation(ServiceBase $api, $args)
    {
        $cachePath = dotb_cached("include/vietnam.json");
        if (!dotb_is_file($cachePath)) {
            dotb_touch($cachePath);
        }

        if (empty(file_get_contents($cachePath))) {
            $content = file_get_contents("custom/include/vietnam.json");
            file_put_contents($cachePath, $content);
        }

        $json = dotb_file_get_contents($cachePath);

        return array('success' => 1, "data" => $json);
    }
}
