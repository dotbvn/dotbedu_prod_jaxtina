<?php


/**
 *
 * GlobalSearch API for portal
 *
 *  The /globalsearch and //globalsearch endpoints for portal are currently not supported.
 *
 */
class GlobalSearchPortalApi extends DotbApi
{
    /**
     * Register endpoints
     * @return array
     */
    public function registerApiRest()
    {
        return [
            // /globalsearch
            'globalSearch' => [
                'reqType' => ['GET', 'POST'],
                'path' => ['globalsearch'],
                'pathVars' => [''],
                'method' => 'globalSearchPortal',
                'shortHelp' => 'Global search',
                'exceptions' => [
                    'DotbApiExceptionNoMethod',
                ],
            ],

            // /<module>/globalsearch
            'modulesGlobalSearch' => [
                'reqType' => ['GET', 'POST'],
                'path' => ['<module>', 'globalsearch'],
                'pathVars' => ['module', ''],
                'method' => 'globalSearchPortal',
                'shortHelp' => 'Global search',
                'exceptions' => [
                    'DotbApiExceptionNoMethod',
                ],
            ],
        ];
    }

    /**
     * GlobalSearch endpoint for portal (currently not supported)
     * @param ServiceBase $api
     * @param array $args
     * @return array
     */
    public function globalSearchPortal(ServiceBase $api, array $args)
    {
        throw new DotbApiExceptionNoMethod();
    }
}
