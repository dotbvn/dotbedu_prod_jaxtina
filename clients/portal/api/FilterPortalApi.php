<?php



class FilterPortalApi extends FilterApi
{
    protected static function addFilter($field, $filter, DotbQuery_Builder_Where $where, DotbQuery $q)
    {
        // Check if Portal user is trying to access an unauthorized filter
        $portalUnauthorizedFilters = [
            '$tracker',
            '$favorite',
        ];
        if (in_array($field, $portalUnauthorizedFilters)) {
            throw new DotbApiExceptionNotAuthorized('No access to ' . $field . ' filter');
        }

        parent::addFilter($field, $filter, $where, $q);
    }
}
