<?php declare(strict_types=1);


use Dotbcrm\Dotbcrm\Portal\Factory as PortalFactory;

/**
 *
 * PortalSearch API
 *
 *
 */
class SearchPortalApi extends DotbApi
{
    /**
     * Register endpoints
     * @return array
     */
    public function registerApiRest()
    {
        return [
            // /portalsearch
            'portalSearch' => [
                'reqType' => ['GET', 'POST'],
                'path' => ['portalsearch'],
                'pathVars' => [''],
                'method' => 'portalSearch',
                'shortHelp' => 'Portal search',
                'longHelp' => 'include/api/help/portal_search_get_help.html',
                'minVersion' => '11.6',
                'exceptions' => [
                    'DotbApiExceptionNotAuthorized',
                    'DotbApiExceptionSearchUnavailable',
                    'DotbApiExceptionSearchRuntime',
                    'DotbApiExceptionMissingParameter',
                    'DotbApiExceptionRequestMethodFailure',
                    'DotbApiExceptionInvalidParameter',
                ],
            ],
        ];
    }

    /**
     * portalSearch endpoint
     *
     * @param ServiceBase $api
     * @param array $args
     * @throws DotbApiExceptionMissingParameter
     * @throws DotbApiExceptionRequestMethodFailure
     * @deprecated Since 10.2.0.
     * @return array
     */
    public function portalSearch(ServiceBase $api, array $args) : array
    {
        $msg = 'This endpoint is deprecated as of 10.2.0 and will be removed in a future release.';
        $msg .= ' Use genericsearch instead.';
        LoggerManager::getLogger()->deprecated($msg);

        $this->requireArgs($args, ['q']);

        $dotbConfig = \DotbConfig::getInstance();
        $settings = $dotbConfig->get('portal');
        // No modules configured for search
        if (empty($settings['modules'])) {
            throw new DotbApiExceptionRequestMethodFailure('Portal search modules not configured');
        }
        if (!empty($args['module_list'])) {
            $modulesToSearch = explode(',', $args['module_list']);
            foreach ($modulesToSearch as $module) {
                // ensure all passed in modules are suppoorted
                if (!in_array($module, $settings['modules'])) {
                    throw new DotbApiExceptionInvalidParameter('Module not supported: ' . $module);
                }
            }
        } else {
            // default to search all supported modules
            $args['module_list'] = implode(',', $settings['modules']);
        }

        // At the moment we only support one service.
        // If we are to support multiple services at the same time in the future,
        // we need to be able to handle pagination, etc.
        if (empty($settings['services'])) {
            $providerType = 'Elastic';
        } else {
            $providerType = $settings['services'][0];
        }

        // get the service provider
        $provider = PortalFactory::getInstance('Search\\' . $providerType);
        if ($provider && $provider instanceof Dotbcrm\Dotbcrm\Portal\Search\Search) {
            // call the provider to get data
            return $provider->getData($api, $args);
        } else {
            throw new DotbApiExceptionRequestMethodFailure(
                sprintf('Portal search provider, %s not available', $providerType)
            );
        }
    }
}
