<?php



$viewdefs['portal']['view']['forgotpassword'] = [
    'buttons' => [
        [
            'name' => 'forgotPassword_button',
            'type' => 'button',
            'label' => 'LBL_RESET_PASSWORD',
            'primary' => true,
        ],
    ],
    'panels' => [
        [
            'label' => 'LBL_PANEL_DEFAULT',
            'fields' => [
                [
                    'name' => 'username',
                    'type' => 'username',
                    'placeholder' => 'LBL_PORTAL_LOGIN_USERNAME',
                    'required' => true,
                    'no_required_placeholder' => true,
                ],
            ],
        ],
    ],
];
