
/**
 * @class View.Views.Portal.HomeHeaderpaneView
 * @alias DOTB.App.view.views.PortalHomeHeaderpaneView
 * @extends View.Views.Base.HeaderpaneView
 */
({
    extendsFrom: 'HeaderpaneView',

    className: 'preview-headerbar',

    events: {
        'click [name=collapse_button]': 'collapseClicked',
        'click [name=expand_button]': 'expandClicked'
    },

    collapseClicked: function() {
        this.context.trigger('dashboard:collapse:fire', true);
    },

    expandClicked: function() {
        this.context.trigger('dashboard:collapse:fire', false);
    },
})
