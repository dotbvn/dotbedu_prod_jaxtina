<?php

$viewdefs['portal']['view']['home-headerpane'] = [
    'buttons' => [
        [
            'type' => 'actiondropdown',
            'buttons' => [
                [
                    'name' => 'collapse_button',
                    'type' => 'rowaction',
                    'label' => 'LBL_DASHLET_MINIMIZE_ALL',
                ],
                [
                    'name' => 'expand_button',
                    'type' => 'rowaction',
                    'label' => 'LBL_DASHLET_MAXIMIZE_ALL',
                ],
            ],
        ],
    ],
    'content' => [
        [
            'name' => 'header',
            'fields' => [
                [
                    'name' => 'name',
                    'type' => 'dashboardtitle',
                    'placeholder' => 'LBL_DASHBOARD_TITLE',
                ],
            ],
        ],
    ],
];
