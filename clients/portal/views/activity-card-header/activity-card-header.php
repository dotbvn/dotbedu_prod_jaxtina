<?php


$viewdefs['portal']['view']['activity-card-header'] = [
    'panels' => [
        [
            'name' => 'panel_users',
            'label' => 'LBL_PANEL_USERS',
            'css_class' => 'panel-users',
            'template' => 'user-single',
            'fields' => [
                [
                    'label' => 'LBL_CREATED',
                    'name' => 'created_by_name',
                    'link' => false,
                ],
            ],
        ],
        [
            'name' => 'panel_header',
            'label' => 'LBL_PANEL_HEADER',
            'css_class' => 'panel-header',
            'fields' => [
                [
                    'name' => 'name',
                    'type' => 'name',
                    'link' => false,
                ],
            ],
        ],
    ],
];
