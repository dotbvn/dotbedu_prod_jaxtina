
/**
 * @class View.Views.Portal.ModuleMenuView
 * @alias DOTB.App.view.views.PortalModuleMenuView
 * @extends View.Views.Base.ModuleMenuView
 */
({
    /**
     * Methods called when a `show.bs.dropdown` event occurs. Overrides the base
     * functions to remove population of the favorites and recently viewed records,
     * even if the _defaultSettings metadata is changed
     */
    populateMenu: function() {},
    populate: function(tplName, filter, limit) {},
    getCollection: function(tplName) {}
});
