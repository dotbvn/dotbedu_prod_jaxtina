({
    classList: null,
    events: {
        'click #minimizeButton': 'minimizeSideBar',
        'click #data-view': 'renderCalendar',
        'click .btn-view': 'changeViewState',
        'click #btn-save-settings': 'renderCalendar',
        'click .navi-icon ': 'handleClickNavigate',
        'click .today-navigate': 'triggerTodayNavigate',
        'click .nav-custom': 'triggerNav',
        'change #session_class': 'renderCalendar',
        'click .star-input': 'handleRating',
    },
    //App string that use when initialize
    languageList: {
        'LBL_SELECT_ALL_TITLE': App.lang.getAppString('LBL_SELECT_ALL_TITLE'),
        'LBL_APPLY_BUTTON': App.lang.getAppString('LBL_APPLY_BUTTON'),
        'LBL_CLASS': App.lang.getAppString('LBL_CLASS'),
        'LBL_TODAY_LESSON': App.lang.getAppString('LBL_TODAY_LESSON'),
        'LBL_DAY': App.lang.getAppString('LBL_DAY'),
        'LBL_WEEK': App.lang.getAppString('LBL_WEEK'),
        'LBL_MONTH': App.lang.getAppString('LBL_MONTH'),
        'LBL_TODAY': App.lang.getAppString('LBL_CALENDAR_TODAY'),
    },

    initialize: function (options) {
        this._super('initialize', [options]);

        $("#alerts").empty();
        $('[data-module="Calendar"]').addClass('active')

        this.customData = this.customData || {};
        this.is_init = true;
        this.customData.language = this.languageList;
        this.customData.dateNow = this.getCurrentDate();
        cal_date_format = this.getUserDateFormat();
    },
    triggerNav: function(evt){
        let target = $(evt.currentTarget);
        let dataTarget = [];
        if(target.hasClass('next')){
            dataTarget = $('.navi-next')[0].dataset;
        } else {
            dataTarget = $('.navi-prev')[0].dataset;
        }
        this.handleClickNavigate(dataTarget);
    },
    handleRating: function(evt){
        //Coming soon
        let target = $(evt.currentTarget);
    },
    handleClickNavigate: function(dataset){
        this.createTimetable($('#' + dataset.id), dataset.type, dataset.startdate, dataset.firstweekmonth, dataset.firstweekyear);
    },
    changeViewState: function(event){
        $('.btn-group').each(function() {
            var buttonView = $(this).find('.btn-view');
            if(buttonView.hasClass('btn-active')) buttonView.removeClass('btn-active');
        })
        $(event.currentTarget).addClass('btn-active')
    },
    triggerTodayNavigate: function(event){
        $(".xdsoft_today_button").trigger("touchend")
    },
    getFormatedDate: function(ct){
        var originalDate = new Date(ct);

        var day = originalDate.getDate();
        var month = originalDate.getMonth() + 1; // Months are zero-based
        var year = originalDate.getFullYear();

        day = day < 10 ? '0' + day : day;
        month = month < 10 ? '0' + month : month;

        var formattedDate = day + '/' + month + '/' + year;
        return formattedDate;
    },
    getCurrentDate: function(){
        var currentDate = new Date();
        return this.getFormatedDate(currentDate);
    },
    getUserDateFormat: function(){
        let date_arr = window.parent.DOTB.App.user.attributes.preferences.datepref.split('/')
        let date_format = '';
        let count = 1;
        let arrLenght = date_arr.length;
        date_arr.forEach(function(value){
            date_format += count != arrLenght ? '%'+value+'/' : '%'+value;
            count++;
        })
        return date_format;
    },
    _renderHtml: function() {
        $("#alerts").empty();

        this._getClassList((_classList) => {
            this.renderCalendar();
        });
        this._super('_renderHtml')

        this._initDateTimePicker();
    },
    _initDateTimePicker: function(){
        let self = this;
        var lang = App.lang.getLanguage().substr(0, 2) === 'vn' ? 'vi' : window.top.App.lang.getLanguage().substr(0, 2);
        $.tdtpicker.setLocale(lang);
        $('#picked_date').tdtpicker({
            format: App.user.getPreference('datepref'),
            dayOfWeekStart: 1,
            step: 15,
            inline: true,
            timepicker: false,
            onSelectDate:function(current_time,$input){
                self.getDataChangeDate(current_time);
            },
            onChangeMonth:function(current_time,$input){
                self.getDataChangeDate(current_time);
            },
            onChangeYear:function(current_time,$input){
                self.getDataChangeDate(current_time);
            }
        });
        $('[data-module="Calendar"]').addClass('active');
    },
    loadData: function (options) {
        options = options || {};
        _.extend(options, {
            success: _.bind(function (res) {
                console.log(res)
            }, this)
        });
    },
    _getClassList(callback){
        app.api.call('GET', app.api.buildURL('calendar/get-class-list'), null, {
            success: (data) => {
                let select = '';
                _.each(data.returnData, function(val, key) {
                    select += `<option value="${val.class_id}">${val.class_name}</option>`
                })
                if(this.$el){
                    this.$el.find('#session_class').append(select)
                }
                $('#session_class').multipleSelect({
                    width: 146,
                    height: 30,
                    filter: true,
                });
                $('#session_class').multipleSelect('checkAll');
                $('[data-module="Calendar"]').addClass('active');

                callback(data);
            },
            error: () => console.log('Error getting go to data')
        });
    },
    minimizeSideBar: function(){
        let sidebar = $('#sidebar')
        if (sidebar.hasClass('col-2')) {
            $(this).attr('title',App.lang.get('Calendar','LBL_SHOW_PANEL'));
            $(this).attr('nav_status','0');
            $('.col-10').stop().animate({'margin-left':'0'},300).attr("class","col-12");$('.col-2').hide("slide",{direction: "right"}, 300);
            sidebar.removeClass('col-2');
        } else {
            sidebar.addClass('col-2');
            $(this).attr('title',App.lang.get('Calendar','LBL_SHOW_PANEL'));
            $(this).attr('nav_status','1');
            $('.col-12').stop().animate({'margin-left':'0'},300).attr("class","col-10");$('.col-2').show("slide",{direction: "right"}, 300);
        }
    },
    renderCalendar: function (){
        session_class   = $('#session_class').val();
        this.getDateSeleted();
        $('.tiva-timetable').empty();
        this.loadTimetable(true);
    },
    getDateSeleted: function() {
        tiva_current_date   = DOTB.util.DateUtils.parse($('#picked_date').val(), cal_date_format);
        if(tiva_current_date){
            tiva_date_seleted   = tiva_current_date;
            tiva_current_month  = tiva_current_date.getMonth() + 1;
            tiva_current_year   = tiva_current_date.getFullYear();
        }
    },
    loadTimetable: function(loading = false) {
        let self = this;
        $('.tiva-timetable').each(function (index) {
            // Set id for timetable
            $(this).attr('id', 'timetable-' + (index + 1));

            // Get timetables from json file or ajax php
            var source = (typeof $(this).attr('data-source') != 'undefined') ? $(this).attr('data-source') : 'json';
            var timetable_contain = $(this);
            globalContain = $(this);

            if (source == 'json') { // Get timetables from json file
                if (mode == 'day') {
                    var timetable_json = 'timetable/timetables_day.json';
                } else {
                    var timetable_json = 'index.php?module=Calendar&action=getjson&dotb_body_only=true';
                }

                $.getJSON(timetable_json, function (data) {
                    // Init timetables variable
                    tiva_timetables = [];

                    for (var i = 0; i < data.items.length; i++) {
                        tiva_timetables.push(data.items[i]);
                    }

                    // Sort timetables by date
                    tiva_timetables.sort(this.sortByTime);

                    for (var j = 0; j < tiva_timetables.length; j++) {
                        tiva_timetables[j].id = j;
                    }

                    // Create timetable
                    var todayDate = tiva_date_seleted;
                    var date_start = (typeof timetable_contain.attr('data-start') != "undefined") ? timetable_contain.attr('data-start') : 'sunday';
                    if (date_start == 'sunday') {
                        var tiva_current_week = new Date(todayDate.setDate(tiva_current_date.getDate() - todayDate.getDay()));
                    } else {
                        var today_date = (todayDate.getDay() == 0) ? 7 : todayDate.getDay();
                        var tiva_current_week = new Date(todayDate.setDate(tiva_current_date.getDate() - today_date + 1));
                    }
                    self.createTimetable(timetable_contain, 'current', tiva_current_week, tiva_current_month, tiva_current_year);
                });
            } else {
                var view = $('#data-view').find('.btn-active').attr('data-view');
                if (loading) $('.lds-ellipsis').show();
                let data = {
                    session_class: session_class,
                    dataType: dataType,
                    select_date: {
                        day: tiva_date_seleted.getDate(),
                        month: tiva_current_month,
                        year: tiva_current_year,
                        view: view,
                        week: tiva_current_date.getWeek()
                    },
                }
                app.api.call('create', app.api.buildURL('calendar/get-calendar-data'), data, {
                    success: function (data) {
                        $('.lds-ellipsis').hide();
                        if (data.success == 1) {
                            tiva_timetables = data.timetables;
                            array_day_list = data.showmore;

                            for (var j = 0; j < tiva_timetables.length; j++) {
                                tiva_timetables[j].id = j;
                            }
                            // Create timetable
                            self.createTimetable(timetable_contain, 'current', tiva_date_seleted, tiva_current_month, tiva_current_year);
                            if(data.count == 0) toastr.error(App.lang.get('LBL_NO_RESULTS_DES', 'Calendar'),App.lang.get('LBL_NO_RESULTS', 'Calendar'), {
                                positionClass: 'toast-bottom-right',
                                closeButton: true,
                                preventDuplicates: true,
                            });
                            else toastr.info('',data.count+App.lang.get('LBL_RESULTS_FOUND', 'Calendar'), {
                                positionClass: 'toast-bottom-right',
                                closeButton: true,
                                preventDuplicates: true,
                            });
                        } else
                            toastr.error(App.lang.get('LBL_CONNECTION_ERROR'),{
                                positionClass: 'toast-bottom-right',
                                closeButton: true,
                                preventDuplicates: true,
                            });
                    },
                    error: function (){
                        console.log('Error retrieve data')
                    }
                });
            }
        });
    },
    createTimetable: function (el, btn, start_date, monthNum, yearNum) {
        var view = $('#data-view').find('.btn-active').attr('data-view');
        var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
        switch (btn) {
            case 'nextmo':
                var next_month = new Date(yearNum + '-' + monthNum + '-01').addMonths(1);
                this.getDataChangeDate(next_month);
                break;
            case 'prevmo':
                var last_month = new Date(yearNum + '-' + monthNum + '-01').addMonths(-1);
                this.getDataChangeDate(last_month);
                break;
            case 'prevwe':
                var last_week = new Date(start_date).addDays(-7);
                $('#picked_date').tdtpicker({ value: last_week });
                this.getDataChangeDate(last_week);
                break;
            case 'nextwe':
                var next_week = new Date(start_date).addDays(7);
                $('#picked_date').tdtpicker({ value: next_week });
                this.getDataChangeDate(next_week);
                break;
            case 'prevda':
                var yesterday = new Date(start_date).addDays(-1);
                this.getDataChangeDate(yesterday);
                break;
            case 'nextda':
                var tomorrow = new Date(start_date).addDays(1);
                this.getDataChangeDate(tomorrow);
                break;
        }
        if (btn == 'current') {
            $('.lds-ellipsis').show()
            switch (view) {
                case 'day':
                    this.timetableList(el, tiva_timetables, this.getFirstDateOfWeek(start_date));
                    break;
                case 'week':
                    if (date_start === 'sunday'){
                        if(new Date(start_date).is().sunday()) firstDate = start_date;
                        else firstDate = new Date(start_date).last().sunday();
                    }else{
                        if(new Date(start_date).is().monday()) firstDate = start_date;
                        else firstDate = new Date(start_date).last().monday();
                    }
                    this.timetableWeek(el, tiva_timetables, firstDate);
                    break;
                case 'month':
                    var firstDate = new Date(start_date).moveToFirstDayOfMonth();
                    var lastDate = new Date(start_date).moveToLastDayOfMonth();
                    var numbDays = lastDate.getDate();
                    if (date_start === 'sunday') {
                        firstDate = firstDate.getDay() + 1;
                    } else {
                        firstDate = firstDate.getDay() === 0 ? 7 : firstDate.getDay();
                    }
                    this.timetableMonth(el, tiva_timetables, firstDate, numbDays, monthNum, yearNum);
                    break;
            }
            $('.lds-ellipsis').hide()
        }
        if(this.is_init){
            let current_date = new Date();
            this.renderTodayLesson(el,this.getFirstDateOfWeek(current_date))
        }
    },
    getDataChangeDate: function (ct) {
        tiva_current_year = ct.getFullYear();
        tiva_current_month = ct.getMonth()+1;
        tiva_current_date = ct;
        $('#picked_date').tdtpicker({value: tiva_current_date});
        this.renderCalendar();
    },
    getFirstDateOfWeek: function (date) {
        let start = new Date(date);
        let day = start.getDay();
        let adjust = day === 0 ? 6 : day - 1;
        start.setDate(date.getDate() - adjust);
        return start;
    },
    timetableWeek: function(el, tiva_timetables, firstDayWeek) {
        var dataListType = $('.tiva-timetable').attr('data-view');
        var firstWeek = new Date(firstDayWeek);
        var firstWeekDate = firstWeek.getDate();
        var firstWeekMonth = firstWeek.getMonth() + 1;
        var firstWeekYear = firstWeek.getFullYear();

        var lastWeek = this.getDayAfter(firstWeek, 6);
        var lastWeekDate = lastWeek.getDate();
        var lastWeekMonth = lastWeek.getMonth() + 1;
        var lastWeekYear = lastWeek.getFullYear();
        var view = $('#data-view').find('.btn-active').attr('data-view');

        var d;
        var date;
        var month;
        var year;

        var first;
        var last;
        var header_time;

        // Set wordDay
        var wordDay;
        var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
        var timetableString = "";

        if (date_start == 'sunday') {
            wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
        } else { // Start with Monday
            wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
        }

        //lay tuan custom
        var week_nav;
        var cr_language = app.lang.getLanguage();
       if ((firstWeekMonth != lastWeekMonth) && (firstWeekYear != lastWeekYear)) {
            week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ', ' + firstWeekYear
                + ' - ' + wordMonth[lastWeekMonth - 1].substring(0, 3) + ' ' + lastWeekDate + ', ' + lastWeekYear;
       } else if (firstWeekMonth != lastWeekMonth) {
            week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ' - ' + wordMonth[lastWeekMonth - 1].substring(0, 3) + ' ' + lastWeekDate + ', ' + firstWeekYear;
       } else {
           week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ' - ' + lastWeekDate + ', ' + firstWeekYear;
       }

        if (!(el.attr('data-nav') === 'hide') && view != 'day') {
            $('#default_header').html('<div class="time-navigation">'
                + `<span class="navi-icon navi-prev" data-id="${el.attr('id')}" data-type="prevwe" data-startDate="${firstWeek}" data-firstWeekMonth="${firstWeekMonth}" data-firstWeekYear="${firstWeekYear}">&#10094;</span>`
                + '<span class="navi-time">' + week_nav + '</span>'
                + `<span class="navi-icon navi-next" data-id="${el.attr('id')}" data-type="nextwe" data-startDate="${firstWeek}" data-firstWeekMonth="${firstWeekMonth}" data-firstWeekYear="${firstWeekYear}">&#10095;</span>`
                + '</div>');
        }
        // Get min, max time
        var min_time = this.getMinTime(tiva_timetables);
        var max_time = this.getMaxTime(tiva_timetables);
        var show_time = (el.attr('data-header-time') === 'hide') ? '' : 'show-time';
        if(min_time == 0 && max_time == 23){
            this.alertEmptyLesson(el, 'week');
            return;
        }
        timetableString += '<div class="timetable-week ' + show_time + '">';
        timetableString += '<div class="timetable-axis">';
        for (let n = min_time; n <= max_time; n++) {
            if (n === 0) timetableString += '<div class="axis-item">GMT+07</div>';
            else {
                var hour = n;
                if (n > 12) hour = hour - 12;
                timetableString += '<div class="axis-item">' + hour + (n >= 12 ? ' PM' : ' AM') + ' </div>';
            }
        }
        timetableString += '</div>';

        timetableString += '<div class="timetable-columns">';
        for (var m = 0; m < wordDay.length; m++) {
            // Caculate date of week
            d = this.getDayAfter(firstWeek, m);
            date = d.getDate();
            month = d.getMonth() + 1;
            year = d.getFullYear();
            header_time = (el.attr('data-header-time') == 'hide') ? '' : '<p></p><span>' + ' ' + date + '</span>';
            first = (m == 0) ? 'first-column' : '';
            last = (m == wordDay.length - 1) ? 'last-column' : '';

            var current_date = new Date();
            var todaysDate = current_date.getDate();
            var current_month = current_date.getMonth() + 1;
            var current_year = current_date.getFullYear();
            var class_today = ((todaysDate == date) && (current_month == month) && (current_year == year)) ? 'week-today' : '';
            timetableString += '<div class="timetable-column">';
            // Header

            if (screen.width > 768) {
                timetableString += '<div class="timetable-header ' + class_today + ' ' + last + '">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]] + header_time + '</div>';
            } else {
                timetableString += '<div class="timetable-header ' + class_today + ' ' + last + '">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]] + header_time + '</div>';
            }

            // Content
            timetableString += '<div class="timetable-column-content">';

            // Get timetables of day
            var timetables;
            if (el.attr('data-mode') == 'day') {
                timetables = this.getTimetablesDay(tiva_timetables, dayArr[m]);
            } else {
                timetables = this.getTimetables(tiva_timetables, date, month, year);
            }
            for (var t = 0; t < timetables.length; t++) {
                if (timetables[t].start_time && timetables[t].end_time) {
                    var position = this.getPosition(min_time, timetables[t].start_time_2, 'week') + 20
                    let height = parseFloat(timetables[t].ss_duration_cal) * 100;
                    var _row_day = timetables[t].row_day.replace('__top','top:'+position+'px;height:'+height+'px');
                    timetableString += _row_day;
                    timetableString += timetables[t].popup_modal;
                }
            }
            timetableString += '</div>';

            // Grid
            timetableString += '<div class="timetable-column-grid">';
            for (var n = min_time; n < max_time; n++) {
                timetableString += '<div class="grid-item ' + first + last + '"></div>';
            }
            timetableString += '</div>';
            timetableString += '</div>';
        }
        timetableString += '</div>';

        timetableString += '</div>';

        el.html(timetableString);

        // Popup
        el.find('.timetable-title').magnificPopup({
            type: 'inline',
            removalDelay: 500,
            mainClass: 'my-mfp-zoom-in'
        });
        //showmore
        el.find('.showmore-action').magnificPopup({
            type: 'inline',
            removalDelay: 500,
            mainClass: 'my-mfp-zoom-in',
            key: 'showmore',
            modal: true
        });
    },
    renderTodayLesson: function (el,firstOfWeek){

        var current_date = new Date();
        var todaysDate = current_date.getDate();
        var current_month = current_date.getMonth() + 1;
        var current_year = current_date.getFullYear();

        let data = {
            session_class: session_class,
            dataType: dataType,
            select_date: {
                day: todaysDate,
                month: current_month,
                year: current_year,
                view: 'day',
            },
        }
        let self = this;
        app.api.call('create', app.api.buildURL('calendar/get-calendar-data'), data, {
            success: function (data) {
                if (data.success == 1) {
                    tiva_timetables = data.timetables;
                    array_day_list = data.showmore;

                    for (var j = 0; j < tiva_timetables.length; j++) {
                        tiva_timetables[j].id = j;
                    }
                    // Create timetable
                    self.displayTodayLesson(el,tiva_timetables, firstOfWeek);
                    this.is_init = false;
                }
            },
            error: function (){
                console.log('loi roiiiii')
            }
        });
    },
    displayTodayLesson: function (el,tiva_timetables, firstDayWeek){
        var firstWeek = new Date(firstDayWeek);
        var dayArr;
        var d;
        var date;
        var month;
        var year;

        // Set wordDay
        var wordDay;
        var date_start = (typeof $('#data-view-source').attr('data-start') != "undefined") ? $('#data-view-source').attr('data-start') : 'sunday';
        var timetableString = "";

        var current_date = new Date();
        const dayOfWeek = current_date.getDay() != 0 ? current_date.getDay() - 1 : 6;

        d = this.getDayAfter(firstWeek, dayOfWeek);
        date = d.getDate();
        month = d.getMonth() + 1;
        year = d.getFullYear();

        if (date_start == 'sunday') {
            dayArr = new Array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
        } else { // Start with Monday
            dayArr = new Array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
        }
        // Get timetables of day
        var timetables;
        if (el.attr('data-mode') == 'day') {
            timetables = this.getTimetablesDay(tiva_timetables, dayArr[dayOfWeek]);
        } else {
            timetables = this.getTimetables(tiva_timetables, date, month, year);
        }
        for (var t = 0; t < timetables.length; t++) {
            if (timetables[t].start_time && timetables[t].end_time) {
                timetableString += timetables[t].row_day;
                timetableString += timetables[t].popup_modal;
            }
        }

        $('.col-2 .today-class-section').html(timetableString);

        // Popup
        $('.col-2 .today-class-section').find('.timetable-title').magnificPopup({
            type: 'inline',
            removalDelay: 500,
            mainClass: 'my-mfp-zoom-in'
        });
    },
    timetableList: function(el, tiva_timetables, firstDayWeek) {
        var dataListType = $('.tiva-timetable').attr('data-view');
        var firstWeek = new Date(firstDayWeek);
        var firstWeekDate = firstWeek.getDate();
        var firstWeekMonth = firstWeek.getMonth() + 1;
        var firstWeekYear = firstWeek.getFullYear();

        var lastWeek = this.getDayAfter(firstWeek, 6);
        var lastWeekDate = lastWeek.getDate();
        var lastWeekMonth = lastWeek.getMonth() + 1;
        var lastWeekYear = lastWeek.getFullYear();
        var view = $('#data-view').find('.btn-active').attr('data-view');

        var dayArr;
        var d;
        var date;
        var month;
        var year;

        var header_time;
        //lay ngay da chon
        var date_selected = DOTB.util.DateUtils.parse($('#picked_date').val(), cal_date_format); //Format by User DateFormat
        var year_selected = date_selected.getFullYear();
        var month_selected = date_selected.getMonth();
        var day_selected = date_selected.getDate();

        // Set wordDay
        var wordDay;
        var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
        var timetableString = "";

        if (date_start == 'sunday') {
            wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
            dayArr = new Array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
        } else { // Start with Monday
            wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
            dayArr = new Array("monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday");
        }

        var week_nav;
        var cr_language = app.lang.getLanguage();
        if (dataListType == 'day') {
            if(cr_language == 'vn_vn')
                week_nav = day_selected + ' ' + app.lang.getAppListStrings('list_month')[wordMonth[month_selected]] + ', ' + year_selected;
            else
                week_nav = app.lang.getAppListStrings('list_month')[wordMonth[month_selected]] + ' ' + day_selected + ', ' + year_selected;
        } else if ((firstWeekMonth != lastWeekMonth) && (firstWeekYear != lastWeekYear)) {
            week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ', ' + firstWeekYear
                + ' - ' + wordMonth[lastWeekMonth - 1].substring(0, 3) + ' ' + lastWeekDate + ', ' + lastWeekYear;
        } else if (firstWeekMonth != lastWeekMonth) {
            week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + firstWeekDate + ' - ' + wordMonth[lastWeekMonth - 1].substring(0, 3) + ' ' + lastWeekDate + ', ' + firstWeekYear;
        } else {
            week_nav = wordMonth[firstWeekMonth - 1].substring(0, 3) + ' ' + day_selected + ', ' + firstWeekYear;
        }

        if (!(el.attr('data-nav') == 'hide') && view== 'day') {
            $('#default_header').html('<div class="time-navigation">'
                + `<span class="navi-icon navi-prev" data-id="${el.attr('id')}" data-type="prevda" data-startDate="${date_selected}" data-firstWeekMonth="${firstWeekMonth}" data-firstWeekYear="${firstWeekYear}">&#10094;</span>`
                + '<span class="navi-time">' + week_nav + '</span>'
                + `<span class="navi-icon navi-next" data-id="${el.attr('id')}" data-type="nextda" data-startDate="${date_selected}" data-firstWeekMonth="${firstWeekMonth}" data-firstWeekYear="${firstWeekYear}">&#10095;</span>`
                + '</div>');
        }
        var timetables;
        var min_time = parseInt(this.getMinTime(tiva_timetables));
        var max_time = parseInt(this.getMaxTime(tiva_timetables));
        var show_time = (el.attr('data-header-time') === 'hide') ? '' : 'show-time';

        const date_of_week = new Date(date_selected);
        const dayOfWeek = date_of_week.getDay() != 0 ? date_of_week.getDay() - 1 : 6;

        if(tiva_timetables.length == 0){
            this.alertEmptyLesson(el, 'day');
            return;
        }
        min_time -= 2;
        max_time += 2;
        month_selected += 1;
        header_time = (el.attr('data-header-time') == 'hide') ? '' : '<p></p><span>' + ' ' + day_selected + '</span>';

        var current_date = new Date();
        var todaysDate = current_date.getDate();
        var current_month = current_date.getMonth() + 1;
        var current_year = current_date.getFullYear();
        var class_today = ((todaysDate == day_selected) && (current_month == month_selected) && (current_year == year_selected)) ? 'today' : '';

        timetableString += '<div class="timetable-day ' + show_time + '">';
        timetableString += '<div class="timetable-header' + ' ' + class_today + '">' + app.lang.getAppListStrings('list_date_long')[wordDay[dayOfWeek]] + header_time + '</div>';
        timetableString += '<div class="timetable-rows">';
        let vertical_height = 20;
        for (let n = min_time; n <= max_time; n++) {
            vertical_height += 60;
            d = this.getDayAfter(firstWeek, dayOfWeek);
            date = d.getDate();
            month = d.getMonth() + 1;
            year = d.getFullYear();
            first = (n == min_time) ? 'first-row' : '';
            last = (n == max_time) ? 'last-row' : '';

            timetableString += '<div class="timetable-row">';
            // Header
            if (n === 0) timetableString += '<div class="axis-item">GMT+07</div>';
            else {
                var hour = n;
                if (n > 12) hour = hour - 12;
                timetableString += '<div class="axis-item">' + hour + (n >= 12 ? ' PM' : ' AM') + ' </div>';
            }
            timetableString += `<div class="timetable-row-content ${first} ${last}">`;
            // Get timetables of day
            var timetables;
            if (el.attr('data-mode') == 'day') {
                timetables = this.getTimetablesDay(tiva_timetables, dayArr[dayOfWeek]);
            } else {
                timetables = this.getTimetables(tiva_timetables, date, month, year);
            }
            let previous_end_time = '';

            for (var t = 0; t < timetables.length; t++) {
                if (timetables[t].start_time && timetables[t].end_time) {
                    var position = this.getPosition(min_time, timetables[t].start_time_2, 'day') + 145
                    if(previous_end_time == timetables[t].start_time) position += 1;
                    let height = parseFloat(timetables[t].ss_duration_cal) * 60;
                    var _row_day = timetables[t].row_day.replace('__top','top:'+position+'px;height:'+height+'px');
                    timetableString += _row_day;
                    timetableString += timetables[t].popup_modal;
                    previous_end_time = timetables[t].end_time;
                }
            }
            timetableString += '</div>';
            timetableString += '</div>';
        }
        timetableString += '</div>';
        timetableString += '</div>';

        el.html(timetableString);

        // Popup
        el.find('.timetable-title').magnificPopup({
            type: 'inline',
            removalDelay: 500,
            mainClass: 'my-mfp-zoom-in'
        });
        //showmore
        el.find('.showmore-action').magnificPopup({
            type: 'inline',
            removalDelay: 500,
            mainClass: 'my-mfp-zoom-in',
            key: 'showmore',
            modal: true
        });
    },
    timetableMonth: function(el, tiva_timetables, firstDay, numbDays, monthNum, yearNum) {
        var current_date = new Date();
        var todaysDate = current_date.getDate();
        var current_month = current_date.getMonth() + 1;
        var current_year = current_date.getFullYear();
        var wordDay;
        var date_start = (typeof el.attr('data-start') != "undefined") ? el.attr('data-start') : 'sunday';
        var timetableString = "";
        var daycounter = 0;

        if (date_start == 'sunday') {
            wordDay = new Array(wordDay_sun, wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat);
        } else { // Start with Monday
            wordDay = new Array(wordDay_mon, wordDay_tue, wordDay_wed, wordDay_thu, wordDay_fri, wordDay_sat, wordDay_sun);
        }

        if (!(el.attr('data-nav') == 'hide')) {
            $('#default_header').html('<div class="time-navigation">'
                + `<span class="navi-icon navi-prev" data-id="${el.attr('id')}" data-type="prevmo" data-startDate="" data-monthNum="${monthNum}" data-yearNum="${yearNum}">&#10094;</span>`
                + '<span class="navi-time">' + app.lang.getAppListStrings('list_month')[wordMonth[monthNum - 1]] + '&nbsp;&nbsp;' + yearNum + '</span>'
                + `<span class="navi-icon navi-next" data-id="${el.attr('id')}" data-type="nextmo" data-startDate="" data-monthNum="${monthNum}" data-yearNum="${yearNum}">&#10095;</span>`
                + '</div>');
        }

        if(tiva_timetables.length == 0){
            this.alertEmptyLesson(el, 'month');
            return;
        }

        timetableString += '<table class="timetable-month">';
        timetableString += '<thead>';
        for (var m = 0; m < wordDay.length; m++) {
            if (screen.width > 768) {
                timetableString += '<th class="timetable-header">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]] + '</th>';
            } else {
                timetableString += '<th class="timetable-header">' + app.lang.getAppListStrings('list_date_long')[wordDay[m]].substring(0, 3) + '</th>';
            }
        }
        timetableString += '</thead>';
        timetableString += '<tbody>';
        timetableString += '</tr>';
        var thisDate = 1;
        var class_today;

        for (var i = 1; i <= 6; i++) {
            var k = (i - 1) * 7 + 1;
            if (k < (firstDay + numbDays)) {
                timetableString += '<tr>';
                for (var x = 1; x <= 7; x++) {
                    daycounter = (thisDate - firstDay) + 1;
                    thisDate++;
                    class_today = ((todaysDate == daycounter) && (current_month == monthNum) && (current_year == yearNum)) ? 'today' : '';
                    timetableString += '<td class="calendar-day ' + class_today + '">';
                    if ((daycounter <= numbDays) && (daycounter >= 1)) {
                        timetableString += '<div class="calendar-daycounter">' + daycounter + '</div>';
                    }

                    // Get timetables of day
                    var timetables = this.getTimetablesDay(tiva_timetables, yearNum + '-' + (monthNum.toString().padStart(2, '0')) + '-' + (daycounter.toString().padStart(2, 0)));
                    for (var t = 0; t < timetables.length; t++) {
                        if (t < 5) {
                            timetableString += timetables[t].row_day;
                        } else {
                            if (t === 5) {
                                timetableString += '<div class="timetable-item"><a class="open-popup-link showmore-action" href="#modal_showmore_' + timetables[t].date_db + '">'+App.lang.get('LBL_MORE','Calendar')+'(' + timetables.length + ')</a>';
                                timetableString += '<div id="modal_showmore_' + timetables[t].date_db + '" class="timetable-popup zoom-anim-dialog mfp-hide">'
                                timetableString += '<div style="border-top-left-radius:10px;border-top-right-radius: 10px" class="popup-header" data-value="dotb-mint">' + timetables[t].week_date_l+', '+timetables[t].date + '</div>';
                                timetableString += '<div class="popup-body tiva-timetable" style="border-top-left-radius: 0px;border-top-right-radius: 0;">';
                                timetableString += '<div class="popup-body timetable-month">'
                                timetableString += array_day_list[timetables[t].date_db]
                                timetableString += '</div>';
                                timetableString += '</div>';
                                timetableString += '<button title="Close (Esc)" type="button" class="mfp-close">×</button>';
                                timetableString += '</div>';
                                timetableString += '</div>';
                            }
                        }
                        timetableString += timetables[t].popup_modal;
                    }
                    timetableString += '</td>';
                }
                timetableString += '</tr>';
            }
        }
        timetableString += '</tbody>';
        timetableString += '</table>';

        el.html(timetableString);

        // Popup
        el.find('.timetable-title').magnificPopup({
            index: 1,
            type: 'inline',
            removalDelay: 500,
            mainClass: 'my-mfp-zoom-in',
            key: 'detail',
        });
        //showmore
        el.find('.showmore-action').magnificPopup({
            type: 'inline',
            removalDelay: 500,
            mainClass: 'my-mfp-zoom-in',
            key: 'showmore',
            modal: true
        });
    },
    sortByEndTime: function(a, b) {
        if (a.end_time < b.end_time) {
            return -1;
        } else if (a.end_time > b.end_time) {
            return 1;
        } else {
            return 0;
        }
    },

    getMinTime: function (timetables) {
        function sortOnlyTime(a, b) {
            if (parseInt(a.start_hour, 10) < parseInt(b.start_hour, 10)) {
                return -1;
            } else if (parseInt(a.start_hour, 10) > parseInt(b.start_hour, 10)) {
                return 1;
            } else {
                return 0;
            }
        }

        timetables.sort(sortOnlyTime);
        if (timetables.length > 0) return parseInt(timetables[0].start_time_2, 10);
        return 0;
    },

    getMaxTime: function (timetables) {
        timetables.sort(this.sortByEndTime);
        for (var i = timetables.length - 1; i >= 0; i--) {
            if (timetables[i].end_time) {
                var time = timetables[i].end_time.split(':');
                if (time[1] == '00') {
                    return time[0];
                } else {
                    return parseInt(time[0], 10) + 1;
                }
            }
        }
        return 23;
    },

    calHour: function (time) {
        var t = time.split(':');
        return parseInt(t[0], 10) + (t[1] / 60);
    },

    getPosition: function (axis, start_time, type) {
        let gap = type == 'week' ? 100 : 60;
        return parseInt((this.calHour(start_time) - axis) * gap, 10); // 1 cell top = 55px;
    },
    alertEmptyLesson: function(el, type) {
        let alertStr = App.lang.get('LBL_ALERT_EMPTY_LESSON','Calendar');
        if(window.parent.DOTB.App.user.attributes.preferences.language == 'en_us'){
             alertStr += ' ' + type;
        } else {
            let typeStr = App.lang.get('LBL_'+ type.toUpperCase(),'Calendar');
            alertStr = alertStr.replace('$type', typeStr)
        }
        el.html(`<div class="alert-empty-container"><img src="images/panda.png" style="width: 100px;"><div class='alert-empty'>${alertStr}</div></div>`);
    },
    getHeight: function (start_time, end_time) {
        return parseInt((this.calHour(end_time) - thiscalHour(start_time)) * 100, 10); // 1 cell = 55px;
    },

    getDayAfter: function (day, num) {
        var d = tiva_date_seleted;
        return new Date(d.setTime(day.getTime() + (num * 24 * 60 * 60 * 1000)));
    },

    sortByTime: function (a, b) {
        if (new Date(a.date_time) < new Date(b.date_time)) {
            return -1;
        } else if (new Date(a.date_time) > new Date(b.date_time)) {
            return 1;
        } else {
            return 0;
        }
    },
    // Get timetables of day
    getTimetables: function (tiva_timetables, day, month, year) {
        var timetables = [];
        for (var i = 0; i < tiva_timetables.length; i++) {
            var t = new Date(tiva_timetables[i].date_time);
            if ((t.getDate() == day) && (t.getMonth() + 1 == month) && (t.getFullYear() == year)) {
                timetables.push(tiva_timetables[i]);
            }
        }
        return timetables.sort(this.sortByTime);
    },
    // Get timetables of day
    getTimetablesDay: function (tiva_timetables, day) {
        var timetables = [];
        for (var i = 0; i < tiva_timetables.length; i++) {
            if (tiva_timetables[i].date_db === day) {
                timetables.push(tiva_timetables[i]);
            }
        }
        return timetables.sort(this.sortByTime);
    }
})
