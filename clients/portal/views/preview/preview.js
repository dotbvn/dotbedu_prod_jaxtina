
/**
 * @class View.Views.Portal.PreviewView
 * @alias DOTB.App.view.views.PortalPreviewView
 * @extends View.Views.Base.PreviewView
 */
({
    extendsFrom: 'PreviewView',

    initialize: function(options) {
        this._super('initialize', [options]);
        // we need to force to use record layout for portal, in case base preview layout file exists.
        // this restriction should be removed in the future when its supported to edit portal preview layout in studio.
        this.dataView = 'record';
        var recordMeta = app.metadata.getView(options.module, 'record');
        this.meta = _.extend(this.meta, this._previewifyMetadata(_.extend({}, recordMeta)));
    },
})
