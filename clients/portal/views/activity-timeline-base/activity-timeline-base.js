
/**
 * @class View.Views.Portal.ActivityTimelineBaseView
 * @alias DOTB.App.view.views.PortalActivityTimelineBaseView
 * @extends View.Views.Base.ActivityTimelineBaseView
 */
({
    extendsFrom: 'ActivityTimelineBaseView',

    /**
     * Do we need to hide the activity panel
     */
    hideActivity: false,

    /**
     * @inheritdoc
     */
    initialize: function(options) {
        this._super('initialize', [options]);
        var baseId = this.baseRecord.get('id');

        if (baseId) {
            // fetch the complete record using id
            var baseBean = app.data.createBeanCollection(this.baseModule);
            baseBean.fetch({
                params: {filter: [{'id': baseId}]},
                success: _.bind(function(data) {
                    this.baseRecord = data.models ? data.models[0] : this.baseRecord;
                }, this),
                complete: _.bind(function(data) {
                    this._render();
                }, this)
            });
        }

        this.events = _.extend({}, this.events, {
            'click [data-action=addNote]': 'openNoteDrawer'
        });

        this.filter.module = 'all_modules';
    },

    /**
     * @inheritdoc
     */
    _getBaseModel: function(options) {
        var model;
        var currContext = options.context;
        var baseModule = currContext.get('module');
        var contextModel = currContext.get('model');

        if (contextModel && contextModel.module === baseModule) {
            model = contextModel;
        }
        return model || {};
    },

    /**
     * Open a drawer for writing a note.
     *
     * @param {Event} event
     */
    openNoteDrawer: function(event) {
        var model = this.createLinkModel(this.model, this.moduleLinkMapping.Notes);

        app.drawer.open({
            layout: 'create',
            context: {
                create: true,
                module: 'Notes',
                model: model
            }
        }, _.bind(function(context, model) {
            if (model) {
                this.reloadData();
            }
        }, this));
    },
})
