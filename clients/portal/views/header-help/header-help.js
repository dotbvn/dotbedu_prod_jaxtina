
/**
 * @class View.Layouts.Portal.HeaderHelpView
 * @alias DOTB.App.view.views.PortalHeaderHelpView
 * @extends View.Views.Base.HeaderHelpView
 */
({
    extendsFrom: 'HeaderHelpView',

    /**
     * @inheritdoc
     */
    bindDataChange: function() {
        if (this.layout) {
            this.layout.on('button:help_button:click', this._createNewCase, this);
        }
    },

    /**
     * Create a new case
     * @private
     */
    _createNewCase: function() {
        app.drawer.open({
            layout: 'create',
            context: {
                create: true,
                module: 'Cases'
            }
        });
    },

    /**
     * @inheritdoc
     */
    _dispose() {
        if (this.layout) {
            this.layout.off('button:help_button:click', null, this);
        }
        this._super('_dispose');
    }
})
