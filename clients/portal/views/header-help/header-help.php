<?php

$viewdefs['portal']['view']['header-help'] = [
    'css_class' => 'header-help',
    'default_label' => 'LBL_NEED_HELP',
    'buttons' => [[
        'type' => 'button',
        'name' => 'help_button',
        'css_class' => 'btn-primary',
        'label' => 'LBL_NEW_CASE',
        'events' => [
            'click' => 'button:help_button:click',
        ],
    ]],
];
