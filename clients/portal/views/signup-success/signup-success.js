
/**
 * @class View.Views.Portal.SignupSuccessView
 * @alias DOTB.App.view.views.PortalSignupSuccessView
 * @extends View.View
 */
({
    /**
     * @inheritdoc
     */
    _renderHtml: function() {
        this.logoUrl = app.metadata.getLogoUrl();
        this._super('_renderHtml');
    },
})
