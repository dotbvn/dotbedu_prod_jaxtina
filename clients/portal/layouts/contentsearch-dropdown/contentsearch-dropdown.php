<?php


$viewdefs['portal']['layout']['contentsearch-dropdown'] = [
    'components' => [
        [
            'view' => 'contentsearch-results',
        ],
        [
            'view' => 'pagination',
        ],
        [
            'view' => 'contentsearch-footer',
        ],
    ],
];
