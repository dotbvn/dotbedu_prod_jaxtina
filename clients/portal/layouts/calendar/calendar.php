<?php
$viewdefs['portal']['layout']['calendar'] = array(
    'type' => 'simple',
    'components' => array(
        array(
            'view' => 'calendar',
        ),
    ),
);