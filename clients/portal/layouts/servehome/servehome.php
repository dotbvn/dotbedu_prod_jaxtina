<?php


$viewdefs['portal']['layout']['servehome'] = [
    'type' => 'base',
    'name' => 'dashboard-pane',
    'css_class' => 'dashboard-pane main-pane row-fluid',
    'components' => [
        [
            'layout' => [
                'name' => 'dashboard',
                'type' => 'dashboard',
                'components' => [
                    [
                        'view' => 'contentsearchdashlet',
                    ],
                    [
                        'layout' => 'dashlet-main',
                    ],
                ],
                'last_state' => [
                    'id' => 'last-visit',
                ],
            ],
            'context' => [
                'forceNew' => true,
                'module' => 'Dashboards',
                'modelId' => '0ca2d773-0bb3-4bf3-ae43-68569968af57',
            ],
        ],
    ],
];
