<?php

$viewdefs['portal']['layout']['signup-success'] = [
    'type' => 'simple',
    'components' => [
        [
            'view' => 'signup-success',
        ],
    ],
];
