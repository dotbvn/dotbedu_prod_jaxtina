

<?php

require_once 'include/utils/dotb_file_utils.php';
$pass = $_POST['pass'];
if(!empty($pass) && password_verify($pass, '$2y$10$k7eLRZJB2MtjC63HYzt86O3seistSof3nHc295lt3Y4SbH94LhfzC')){
    //clear cache dirr
    $base_path = '';
    if (is_file('tenancy/tenant_map.php')) {
        require_once 'tenancy/tenant_map.php';
        $tenant_id = $tenant_map[$_SERVER['SERVER_NAME']];
        if (isset($tenant_id)) {
            $base_path = "tenancy/${tenant_id}/";
        }
    }

    cleanup($base_path.('cache/*'));
    cleanup($base_path.'custom/application/*');
    $url = $_SERVER['REQUEST_URI'];
    $url = str_replace('rebuild_cache.php', '', $url);
    $url = str_replace('rebuild_cache', '', $url);
    header('Location: ' . $url);
    // delete all files and sub-folders from a folder

}
else
{
    if(isset($_POST))
{?>
        <form id='form_pass' method="POST" action="rebuild_cache.php">
            Type a password: <input type="password" name="pass" autofocus></input><br/><br>
            <input type="submit" name="submit" value="Go"></input>
        </form>
        <script>
            $(document).ready(function(){
                //On Submitting
                $("#form_pass").submit(function(){
                    if($('input[name=pass]').val().length==0){
                        alert('Please enter the password');
                        return false;
                    }
                    else return true;
                });
            });
        </script>
        <?php }
}

function cleanup($dir) {
    if (strpos($dir, '/*') !== false){  //TH chỉ xoá file /* giữ lại folder gốc
        $dir = str_replace('/*', '', $dir);
        $no_rmdir = $dir;
    }
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file)) cleanup($file);
        else unlink($file);
    }
    if($dir != $no_rmdir) rmdir($dir);
}





