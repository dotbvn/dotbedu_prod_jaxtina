<?php declare(strict_types=1);

namespace Dotbcrm\Dotbcrm\Portal\Search;

/**
 * Class Elastic
 */
class Elastic extends Search
{
    /**
     * @var array
     */
    protected $propertiesToCopy = [
        'next_offset',
        'total',
    ];

    /**
     * {@inheritDoc}
     * @throws \DotbApiExceptionSearchRuntime
     */
    public function getData(\ServiceBase $api, array $args)
    {
        // unset unneeded params if provided
        unset($args['tags']);
        unset($args['tag_filters']);
        unset($args['module']);

        $searchApi = new \GlobalSearchApi();
        $data = $searchApi->globalSearch($api, $args);
        return $this->formatData($data);
    }

    /**
     * @return array
     */
    public function getPropertiesToCopy() : array
    {
        return $this->propertiesToCopy ?? [];
    }

    /**
     * {@inheritDoc}
     */
    public function formatData(array $data) : array
    {
        $newData = [];

        // unset the properties we don't need
        foreach ($this->getPropertiesToCopy() as $property) {
            if (isset($data[$property])) {
                $newData[$property] = $data[$property];
            }
        }

        $newData['records'] = [];

        // massage each record
        foreach ($data['records'] as $record) {
            $newRecord = [];

            // handle mapping
            global $dictionary;
            $module = $record['_module'];
            $objectName = \BeanFactory::getObjectName($module);
            if (!isset($dictionary[$objectName])) {
                \VardefManager::loadVardef($module, $objectName, true);
            }
            $settings = \VardefManager::getModuleProperty($objectName, 'portal_search');
            $mapping = $settings['Elastic']['mapping'] ?? [];
            if (!empty($mapping) && is_array($mapping)) {
                foreach ($mapping as $new => $original) {
                    if (isset($record[$original])) {
                        $newRecord[$new] = $record[$original];
                    }
                }
            }

            // add url
            $newRecord['url'] = 'portal/index.php#' . $record['_module'] . '/' . $record['id'];

            $newData['records'][] = $newRecord;
        }
        return $newData;
    }
}
