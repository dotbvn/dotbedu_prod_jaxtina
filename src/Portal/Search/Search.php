<?php declare(strict_types=1);

namespace Dotbcrm\Dotbcrm\Portal\Search;

/**
 * Class Search
 */
abstract class Search
{
    /**
     * @param \ServiceBase $api
     * @param array $args
     * @return mixed
     */
    abstract public function getData(\ServiceBase $api, array $args);

    /**
     * @param array $data
     * @return array
     */
    public function formatData(array $data) : array
    {
        return $data;
    }
}
