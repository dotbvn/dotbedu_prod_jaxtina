<?php


namespace Dotbcrm\Dotbcrm\Portal;

class Factory
{
    /**
     * @var array
     */
    protected static $instances;

    /**
     * Returns a singleton instance of the Portal object passed as a parameter to the method
     *
     * @param String $className
     *
     * @return 'Dotbcrm\\Dotbcrm\\Portal\\' . $className
     */
    public static function getInstance(String $className)
    {
        if (empty($className)) {
            return false;
        }

        if (!empty(self::$instances[$className])) {
            return self::$instances[$className];
        }

        $classFullName = \DotbAutoLoader::customClass('Dotbcrm\\Dotbcrm\\Portal\\' . $className);
        self::$instances[$className] = new $classFullName();
        return self::$instances[$className];
    }

    /**
     * Removes cached instances
     */
    public static function removeCache()
    {
        self::$instances = [];
    }
}
