<?php 
if (!defined('dotbEntry')) define('dotbEntry', true);
chdir(dirname(__FILE__));
define('ENTRY_POINT_TYPE', 'api');
require_once('include/entryPoint.php');
DotbMetric_Manager::getInstance()->setMetricClass('background')->setTransactionName('cron');
if (empty($current_language)) {
    $current_language = $dotb_config['default_language'];
}
$app_list_strings = return_app_list_strings_language($current_language);
$app_strings = return_application_language($current_language);
global $current_user;
$current_user = BeanFactory::newBean('Users');
$current_user->getSystemUser();
$cron_driver = !empty($dotb_config['cron_class']) ? $dotb_config['cron_class'] : 'DotbCronJobs';
DotbAutoLoader::requireWithCustom("include/DotbQueue/$cron_driver.php");
$jobq = new $cron_driver();
$jobq->runCycle();
$exit_on_cleanup = true;
dotb_cleanup();
if (class_exists('DBManagerFactory')) {
    $db = DBManagerFactory::getInstance();
    $db->disconnect();
}
if (session_id()) {
    session_destroy();
}
if ($exit_on_cleanup) exit($jobq->runOk() ? 0 : 1);
