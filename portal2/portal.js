(function(app) {
    let custom_view = {
        'Calendar': 'calendar'
    }
    app.events.on('router:init', function() {
        var routes = [
            {
                name: 'portal-index',
                route: '',
                callback: function() {
                    app.controller.loadView({
                        layout: 'home'
                    });
                }
            },
            {
                name: 'home',
                route: 'Home',
                callback: function() {
                    app.controller.loadView({
                        layout: 'home'
                    });
                }
            },
            {
                name: 'logout',
                route: 'logout/?clear=:clear'
            },
            {
                name: 'logout',
                route: 'logout'
            },
            {
                name: 'signup',
                route: 'signup',
                callback: function() {
                    app.controller.loadView({
                        module: 'Signup',
                        layout: 'signup',
                        create: true
                    });
                }
            },
            {
                name: 'profile',
                route: 'profile',
                callback: function() {
                    app.controller.loadView({
                        layout: 'record',
                        module: 'Contacts',
                        modelId: app.user.get('id')
                    });
                }
            },
            {
                name: "forgotpassword",
                route: "forgotpassword",
                callback: function() {
                    app.controller.loadView({
                        module: "Forgotpassword",
                        layout: "forgotpassword",
                        create: true
                    });
                }
            },
            {
                name: 'search',
                route: 'search(/)(:term)',
                callback: function(term, urlParams) {
                    var searchTerm = term ? term : '';
                    var params = {
                        modules: [],
                        tags: []
                    };
                    if (urlParams) {
                        var paramsArray = urlParams.split('&');
                        _.each(paramsArray, function(paramPair) {
                            var keyValueArray = paramPair.split('=');
                            if (keyValueArray.length > 1) {
                                params[keyValueArray[0]] = keyValueArray[1].split(',');
                            }
                        });
                    }
                    var appContext = app.controller.context;
                    var termHasChanged = appContext.get('searchTerm') !== searchTerm;
                    var modulesHaveChanged = !_.isEqual(appContext.get('module_list'), params.modules);
                    params.tags = _.map(params.tags, function(tag) {
                        return decodeURIComponent(tag);
                    });
                    var tagsHaveChanged = !_.isEqual(appContext.get('tagParams'), params.tags);
                    if (termHasChanged) {
                        appContext.set('searchTerm', searchTerm);
                    }
                    if (modulesHaveChanged) {
                        appContext.set('module_list', params.modules);
                    }
                    if (tagsHaveChanged) {
                        appContext.set('tagParams', params.tags);
                    }
                    if (tagsHaveChanged) {
                        appContext.trigger('tagsearch:fire:new')
                    } else if (termHasChanged || modulesHaveChanged) {
                        appContext.trigger('search:fire:new');
                    }
                    var header = app.additionalComponents.header;
                    var quicksearch = header && header.getComponent('quicksearch');
                    if (quicksearch) {
                        quicksearch.trigger('route:search');
                    }
                    if (appContext && appContext.get('search')) {
                        return;
                    }
                    app.controller.loadView({
                        layout: 'search',
                        searchTerm: searchTerm,
                        module_list: params.modules,
                        tagParams: params.tags,
                        mixed: true
                    });
                }
            },
            {
                name: "list",
                route: ":module"
            },
            {
                name: 'create',
                route: ':module/create',
                callback: function(module) {
                    if (!app.router._moduleExists(module)) {
                        return;
                    }
                    var prevLayout = app.controller.context.get('layout');
                    if (prevLayout && prevLayout !== 'login') {
                        app.drawer.open({
                            layout: 'create',
                            context: {
                                module: module,
                                create: true,
                                fromRouter: true
                            }
                        }, function(context, model) {
                            if (model && model.module === app.controller.context.get('module')) {
                                app.controller.context.reloadData();
                            }
                        });
                        return;
                    }
                    app.router.record(module, 'create');
                }
            },
            {
                name: "layout",
                route: "layout/:view",
                callback: function(view) {
                    app.controller.loadView({
                        layout: view,
                        module: 'PT_Views'
                    });
                }
            },
            {
                name: 'record',
                route: ':module/:id(/:action)'
            },
        ];
        app.router.addRoutes(routes);
    });
    app.events.on('app:init', function() {
        app.api.setRefreshTokenSuccessCallback(function(callback) {
            callback();
            app.events.trigger("api:refreshtoken:success");
        });
    });
    app.routing.before('route', function(options) {
        var hasAccess = app.router.hasAccessToModule(options) !== false
        return hasAccess;
    });
    var titles = {
        'records': 'TPL_BROWSER_DOTB7_RECORDS_TITLE',
        'record': 'TPL_BROWSER_DOTB7_RECORD_TITLE',
        'about': 'TPL_BROWSER_DOTB7_ABOUT_TITLE',
        'activities': 'TPL_BROWSER_DOTB7_RECORD_TITLE'
    };
    var getTitle = function(model) {
        var context = app.controller.context,
            module = context.get('module'),
            template = Handlebars.compile(app.lang.get(titles[context.get('layout')], module) || ''),
            moduleName = app.lang.getModuleName(module, {
                plural: true
            }),
            title;
        var titleInfo = _.extend({
            module: moduleName,
            appId: app.config.systemName || app.config.appId
        }, model ? model.attributes : {});
        if (titleInfo.name) {
            if (moduleName === 'Dashboards' && titleInfo.dashboard_module) {
                titleInfo.name = app.lang.get(titleInfo.name, titleInfo.dashboard_module);
            } else {
                titleInfo.name = app.lang.get(titleInfo.name, moduleName);
            }
        }
        title = template(titleInfo);
        return $('<span/>').html(title).text();
    };
    var setTitle = function(model) {
        var title = getTitle(model);
        document.title = title || document.title;
    };
    var prevModel;
    app.events.on("app:view:change", function() {
        var context = app.controller.context,
            module = context.get("module"),
            metadata = app.metadata.getModule(module),
            title;
        if (prevModel) {
            prevModel.off("change", setTitle);
        }
        var hmods = app.config.hybrid_modules;
        if (_.isEmpty(metadata) || metadata.isBwcEnabled || _.contains(hmods, module)) {
            title = $('#bwc-frame').get(0) ? $('#bwc-frame').get(0).contentWindow.document.title : getTitle();
        } else {
            var currModel = context.get('model');
            if (!_.isEmpty(currModel)) {
                title = getTitle(currModel);
                currModel.on("change", setTitle, this);
                prevModel = currModel;
            } else {
                title = getTitle();
            }
        }
        document.title = title || document.title;
        if (custom_view[module]) {
            var redirect = '#layout/' + custom_view[module];
            app.router.navigate(redirect, {trigger: true, replace: true});
        }
    }, this);
    var refreshExternalLogin = function() {
        var config = app.metadata.getConfig();
        app.api.setExternalLogin(config && config['externalLogin']);
        if (config && (_.isNull(config['externalLoginSameWindow']) || config['externalLoginSameWindow'] === false)) {
            app.api.setExternalLoginUICallback(window.open);
        }
    };
    app.Router = app.Router.extend({
        hasAccessToModule: function(options) {
            options = options || {};
            var checkAccessRoutes = {
                    'record': 'view',
                    'create': 'create',
                    'vcardImport': 'create'
                },
                route = options.route || '',
                args = options.args || [],
                module = args[0],
                accessCheck = checkAccessRoutes[route];
            if (accessCheck && !app.acl.hasAccess(accessCheck, module)) {
                _.defer(function() {
                    app.controller.loadView({
                        layout: 'access-denied'
                    });
                });
                return false;
            }
            var showWizard = false;
            if (app.user && app.user.has('show_wizard')) {
                showWizard = app.user.get('show_wizard');
                if (showWizard) {
                    var system_config = app.metadata.getConfig();
                    if (system_config.system_status && system_config.system_status.level && system_config.system_status.level === 'admin_only') {
                        showWizard = false;
                    }
                }
            }
            if (showWizard) {
                var callbacks = {
                    complete: function() {
                        var module = app.utils.getWindowLocationParameterByName('module', window.location.search),
                            action = app.utils.getWindowLocationParameterByName('action', window.location.search);
                        if (_.isString(module) && _.isString(action) && module.toLowerCase() === 'users' && action.toLowerCase() === 'authenticate') {
                            window.location = window.location.pathname;
                        } else {
                            window.location.reload();
                        }
                    }
                };
                app.controller.loadView({
                    layout: 'first-login-wizard',
                    module: 'Users',
                    modelId: app.user.get('id'),
                    callbacks: callbacks,
                    wizardName: app.user.get('type')
                });
                app.additionalComponents.header.hide();
                return false;
            }
            if (route && route !== 'logout' && app.user && app.user.get('is_password_expired')) {
                app.controller.loadView({
                    layout: 'password-expired',
                    module: 'Users',
                    callbacks: {
                        complete: function() {
                            window.location.reload();
                        }
                    },
                    modelId: app.user.get('id')
                });
                app.additionalComponents.header.hide();
                return false;
            }
        }
    });

    app.events.on("app:logout:success", function(data) {
        if (app.config && app.config.externalLogin && data && data.url) {
            if (!$('#logoutframe').length) {
                $('#dotbcrm').append('<iframe id="logoutframe" name="logoutframe" />');
                $('#logoutframe').hide();
            }
            $('#logoutframe').load(function() {
                $('#logoutframe').off('load');
                $('#logoutframe').attr('src', '');
            });
            if (typeof data.url == 'string') {
                $('#logoutframe').attr('src', data.url);
            } else if (typeof data.url == 'object') {
                var formHTML = '<form id="externalLogoutForm" method="POST" target="logoutframe" action="' + data.url.url + '">';
                _.each(data.url.params, function(value, key, list) {
                    formHTML += '<input type="hidden" name="' + _.escape(key) + '" value="' + _.escape(value) + '" />';
                });
                formHTML += '</form>' + '<script type="text/javascript">document.getElementById("externalLogoutForm").submit();</script>';
                $('#dotbcrm').append(formHTML);
            }
        }
    });
    app.events.on('app:logout', function() {
        var filters = app.data.getCollectionClasses().Filters;
        if (filters) {
            filters.prototype.resetFiltersCacheAndRequests();
        }
    });
    app.user.on('change:show_wizard', function(user, show_wizard) {
        if (show_wizard) {
            app.shortcuts.disable();
        } else {
            app.shortcuts.enable();
        }
    });
})(DOTB.App)