<div class="timetable-item" title="{$data.class_name} {if $data.teacher_name != ''} | {$data.teacher_name} {/if}{if $data.ta_name != ''} | {$data.ta_name} {/if}{if $data.room_name != ''} | {$data.room_name} {/if}" style="__top">
<a class="timetable-title open-popup-link label-light{$data.class_color}" href="#modal-{$data.meeting_id}" {if $data.type_view == 'day' && $data.duration_cal < 1.00}style="flex-direction: row"{/if}>
    <div class="vertical-bar" style="display: none"></div>
    <div class="timetable-custom-title"  style="display:none">
        <div class="timetable-content-title" style="font-weight: bold">{$data.class_name}</div>
        <div class="timetable-content" style="opacity: 0.75">{$data.time_range}</div>
    </div>
    <span class="timetable-content-title">{$data.class_name}</span>
    <span class="timetable-content" {if $data.type_view == 'day' && $data.duration_cal < 1.0}style="padding-left: 10px"{/if}>{$data.time_range}</span>
    <span class="timetable-content {if $data.type_view == 'day' || $data.duration_cal < 1.0}hide{/if}" title="{$MOD.LBL_TEACHER_NAME}">{$data.ss_teacher_name}</span>
    <span class="timetable-content {if $data.type_view == 'day' || $data.duration_cal < 1.0}hide{/if}" title="{$MOD.LBL_ROOM_NAME}">{$data.ss_room_name}</span>
</a>
</div>
