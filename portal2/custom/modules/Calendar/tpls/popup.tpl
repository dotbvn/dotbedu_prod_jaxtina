<div id="modal-{$data.meeting_id}" class="timetable-popup zoom-anim-dialog mfp-hide">
    <div style="border-top-left-radius:20px;border-top-right-radius: 20px" class="popup-header"><span class="class-name">{$data.class_name}</span> <span class="syllabus-topic">{$data.ss_syllabus_topic}</span>
    </div>
    <div class="popup-body">
        <div class="popup-row">
            <div class="timetable-value"><span class="normal">
                    <i class="far fa-calendar-alt"></i>{$MOD.LBL_DATE_TIME_2}:</span><span class="timetable-value-content">{$data.week_date_s}, {$data.date}</span>
            </div>
            <div class="timetable-value"><span class="normal">
                    <i class="far fa-pencil"></i>{$MOD.LBL_HOMEWORK_COMMENT}:</span><span class="timetable-value-content">{if $data.ss_syllabus_homework == ''}{$data.homework_comment}{else}{$data.ss_syllabus_homework}{/if}</span>
            </div>
        </div>
        <div class="popup-row">
            <div class="timetable-value"><span class="normal">
                    <i class="far fa-graduation-cap"></i>{$MOD.LBL_TEACHER_NAME}:</span>
                    <span class="timetable-value-content">{$data.ss_teacher_name}</span>
            </div>
            <div class="timetable-value"><span class="normal">
                    <i class="far fa-comment"></i></i>{$MOD.LBL_TEACHER_COMMENT}:</span>
                    <span class="timetable-value-content">{$data.teacher_comment}</span>
            </div>
        </div>
        <div class="popup-row">
            <div>
                <div class="timetable-value"><span class="normal">
                        <i class="far fa-graduation-cap"></i>{$MOD.LBL_TEACHER_ASSISTANT}:</span>
                        <span class="timetable-value-content">{$data.ss_ta_name}</span></div>
                <div class="timetable-value"><span class="normal">
                        <i class="far fa-clock-o"></i>{$MOD.LBL_DURATION}:</span>
                        <span class="timetable-value-content">{$data.duration}</span></div>
                <div class="timetable-value"><span class="normal"><i class="far fa-door-open"></i>{$MOD.LBL_ROOM_NAME}: </span>
                        <span class="timetable-value-content">{$data.room_name}</span></div>
            </div>
            <div class="timetable-value"><span class="normal">
                    <i class="far fa-book"></i>{$MOD.LBL_SYLLABUS}:</span><span class="timetable-value-content"> {$data.ss_syllabus_content}</span>
            </div>
        </div>
        <div class="popup-row">
            <div class="timetable-value" style="display: grid; grid-template-columns: 25% 50%;">
                <span class="normal"><i class="far fa-star"></i>
                    {$MOD.LBL_RATE}:
                </span>
                <div class="rate">
                    <label class="label-star" for="star5" title="text" {if $data.star_rating >= 5}style="color:#ffc700""{/if}>5 stars</label>
                    <label class="label-star" for="star4" title="text" {if $data.star_rating >= 4}style="color:#ffc700""{/if}>4 stars</label>
                    <label class="label-star" for="star3" title="text" {if $data.star_rating >= 3}style="color:#ffc700""{/if}>3 stars</label>
                    <label class="label-star" for="star2" title="text" {if $data.star_rating >= 2}style="color:#ffc700""{/if}>2 stars</label>
                    <label class="label-star" for="star1" title="text" {if $data.star_rating >= 1}style="color:#ffc700""{/if}>1 star</label>
                </div>
            </div>
            <div class="timetable-value"><span class="normal">
                    <i class="far fa-video-camera"></i>{$MOD.LBL_ONLINE_LMS}: </span> <span class="timetable-value-content">{$data.join_url}</span></div>
        </div>
    </div>
    <button title="{$MOD.LBL_CLOSE_ESC}" type="button" class="mfp-close">×</button>
</div>
