// ========== Language ===========
// Day, Month
var wordMonth = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var wordDay_sun = "Sunday";
var wordDay_mon = "Monday";
var wordDay_tue = "Tuesday";
var wordDay_wed = "Wednesday";
var wordDay_thu = "Thursday";
var wordDay_fri = "Friday";
var wordDay_sat = "Saturday";
// ===============================

// Global Variable
dataType = {};
dataColor = {};
var session_class = '';
var session_teacher = '';
var session_ta = '';
var session_room = '';
var centers = '';
var display_type = '';

var tiva_timetables = [];
var tiva_current_date   = new Date();
var tiva_date_seleted   = tiva_current_date;
var tiva_current_month  = tiva_current_date.getMonth() + 1;
var tiva_current_year   = tiva_current_date.getFullYear();
var array_day_list = [];

var minTime = '6:00am';
var maxTime = '9:30pm';
var step             = 15;
var disableTextInput = true;
var showOn           = ["click","focus"];
var defaultTimeDelta = 90;
var TimeDelta = defaultTimeDelta * 60000;
var targetAction ='';
var targetColor = ''
var globalContain;
