function checkDataLockDate(id_field_date, has_trigger, has_clear){
    var check_date_str = $('#'+id_field_date).val();
    if(typeof check_date_str == 'undefined' || check_date_str == '') return false;
    if(typeof has_trigger == 'undefined' || has_trigger == '') has_trigger = true;
    if(typeof has_clear == 'undefined' || has_clear == '') has_clear = true;

    if(!dotb_config_lock_info) return true;  //except lock user admin
    switch (dotb_config_lock_type) {
        case 'day_after':
            var check_date = DOTB.util.DateUtils.parse(check_date_str,cal_date_format);
            var date_last =  new Date()  //tomorrow
            date_last.setDate(date_last.getDate() + 1)
            date_last.setHours(0,0,0,0); // next midnight
            var tl_date    = YAHOO.widget.DateMath.subtract(date_last,'D',dotb_config_lock_back);
            var suggestion = DOTB.util.DateUtils.formatDate( tl_date );
            break;
        case 'last_month':
            var splitted    = dotb_config_lock_date.split("-");
            var tl_date    = new Date();
            var input_date = DOTB.util.DateUtils.parse(check_date_str,cal_date_format);
            var check_date = new Date(input_date.getFullYear(), input_date.getMonth()+1, parseInt(splitted[0]), parseInt(splitted[1]));
            var suggestion = DOTB.util.DateUtils.formatDate(  new Date(tl_date.getFullYear(), tl_date.getMonth(), 1) );
            break;
        case 'this_month':
            var splitted    = dotb_config_lock_date.split("-");
            var tl_date    = new Date();
            var input_date = DOTB.util.DateUtils.parse(check_date_str,cal_date_format);
            var check_date = new Date(tl_date.getFullYear(), tl_date.getMonth(), parseInt(splitted[0]), parseInt(splitted[1]));
            var last_month = new Date(tl_date.getFullYear(), tl_date.getMonth()-1, parseInt(splitted[0]), parseInt(splitted[1]));
            if(input_date < last_month){//Xét ngày của tháng trước
              var ov_return = false;
              check_date = last_month;
            }
            else if(input_date > check_date) return true; // TH ngày chọn hiện tại lớn hơn ngày chốt thì vẫn cho chỉnh sửa
            var suggestion = DOTB.util.DateUtils.formatDate( check_date);
            break;
    }
    if(typeof check_date == 'undefined' || check_date == '') return true;

    //neu ngay hien tai dang thao tac sua Out > ngay lock cua thang do thi false
    if(tl_date.getTime() > check_date.getTime() || ov_return === false){
        if(has_clear){
            toastr.error('Date: '+check_date_str +' has been prevented by data-lock funtion. <br> Date should be greater than '+suggestion);
            //$('#'+id_field_date).val(DOTB.util.DateUtils.formatDate(tl_date));
            $('#'+id_field_date).val('').effect("highlight", {color: 'red'}, 2000);
        }

        if(has_trigger) $('#'+id_field_date).trigger('change');
        return false;
    }else return true;

}